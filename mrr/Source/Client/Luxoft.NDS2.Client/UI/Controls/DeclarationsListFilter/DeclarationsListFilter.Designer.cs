﻿namespace Luxoft.NDS2.Client.UI.Controls.DeclarationsListFilter
{
    partial class DeclarationsListFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.panelBack = new Infragistics.Win.Misc.UltraPanel();
            this.pnlCommonParameters = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.txt_SUBSCRIBER_NAME = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbxSortFieldName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.cbxSortFieldOrder = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbxRowCount = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.pnl_Sum = new Infragistics.Win.Misc.UltraPanel();
            this.txt_DISCREP_SELL_BOOK_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_TOTAL_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_TOTAL_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_MIN_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txt_DISCREP_SELL_BOOK_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_MIN_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txt_DISCREP_BUY_BOOK_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_MAX_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txt_DISCREP_BUY_BOOK_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_DISCREP_MAX_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.txt_NDS_WEIGHT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_NDS_WEIGHT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.pnl_Rashod = new Infragistics.Win.Misc.UltraPanel();
            this.txt_TOTAL_DISCREP_COUNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_TOTAL_DISCREP_COUNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.txt_COMPENSATION_AMNT_max = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_COMPENSATION_AMNT_min = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel8 = new Infragistics.Win.Misc.UltraPanel();
            this.cbInspection = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.pnl_FOR = new Infragistics.Win.Misc.UltraPanel();
            this.cbDistrict = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.cbRegion = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.pnl_PZI = new Infragistics.Win.Misc.UltraPanel();
            this.ultraComboEditor16 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel10 = new Infragistics.Win.Misc.UltraPanel();
            this.cbAllTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.sui_Unknown = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.sui_Low = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.sui_Medium = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.sui_Hight = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.txt_INN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_INN = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbx_KPP = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txt_KPP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_SEOD_DECL_ID = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txt_SEOD_DECL_ID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbx_DECL_SIGN = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txt_NAME = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dte_DECL_DATE_end = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_DECL_TYPE_CODE = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.dte_DECL_DATE_begin = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_STATUS = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbx_SUR_CODE_4 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_SUR_CODE_3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbx_CATEGORY = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbx_SUR_CODE_2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.comboTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txt_CORRECTION_NUMBER = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.cbx_FISCAL_YEAR = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbx_SUR_CODE_1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.pnl_MyDecl = new Infragistics.Win.Misc.UltraPanel();
            this.rbMyDeclarations = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.gbExtraParemeters = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel2 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.panelBack.ClientArea.SuspendLayout();
            this.panelBack.SuspendLayout();
            this.pnlCommonParameters.ClientArea.SuspendLayout();
            this.pnlCommonParameters.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SUBSCRIBER_NAME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSortFieldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSortFieldOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRowCount)).BeginInit();
            this.pnl_Sum.ClientArea.SuspendLayout();
            this.pnl_Sum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_SELL_BOOK_AMNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_TOTAL_AMNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_TOTAL_AMNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MIN_AMNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_SELL_BOOK_AMNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MIN_AMNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_BUY_BOOK_AMNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MAX_AMNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_BUY_BOOK_AMNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MAX_AMNT_max)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NDS_WEIGHT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NDS_WEIGHT_min)).BeginInit();
            this.pnl_Rashod.ClientArea.SuspendLayout();
            this.pnl_Rashod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TOTAL_DISCREP_COUNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TOTAL_DISCREP_COUNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CONTROL_RATIO_DISCREP_COUNT_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CONTROL_RATIO_DISCREP_COUNT_max)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_COMPENSATION_AMNT_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_COMPENSATION_AMNT_min)).BeginInit();
            this.ultraPanel8.ClientArea.SuspendLayout();
            this.ultraPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbInspection)).BeginInit();
            this.pnl_FOR.ClientArea.SuspendLayout();
            this.pnl_FOR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRegion)).BeginInit();
            this.pnl_PZI.ClientArea.SuspendLayout();
            this.pnl_PZI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor16)).BeginInit();
            this.ultraPanel10.ClientArea.SuspendLayout();
            this.ultraPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_INN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_INN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_KPP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_KPP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_SEOD_DECL_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SEOD_DECL_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_DECL_SIGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NAME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte_DECL_DATE_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_DECL_TYPE_CODE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte_DECL_DATE_begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_STATUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_CATEGORY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CORRECTION_NUMBER)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_FISCAL_YEAR)).BeginInit();
            this.pnl_MyDecl.ClientArea.SuspendLayout();
            this.pnl_MyDecl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbMyDeclarations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbExtraParemeters)).BeginInit();
            this.gbExtraParemeters.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.AutoScroll = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.panelBack);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(828, 754);
            this.ultraPanel1.TabIndex = 0;
            // 
            // panelBack
            // 
            this.panelBack.AutoScroll = true;
            // 
            // panelBack.ClientArea
            // 
            this.panelBack.ClientArea.Controls.Add(this.pnlCommonParameters);
            this.panelBack.ClientArea.Controls.Add(this.gbExtraParemeters);
            this.panelBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBack.Location = new System.Drawing.Point(0, 0);
            this.panelBack.Name = "panelBack";
            this.panelBack.Size = new System.Drawing.Size(828, 754);
            this.panelBack.TabIndex = 2;
            // 
            // pnlCommonParameters
            // 
            this.pnlCommonParameters.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            // 
            // pnlCommonParameters.ClientArea
            // 
            this.pnlCommonParameters.ClientArea.Controls.Add(this.ultraPanel2);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.pnl_Sum);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.ultraPanel4);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.pnl_Rashod);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.ultraPanel3);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.ultraPanel8);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.pnl_FOR);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.pnl_PZI);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.ultraPanel10);
            this.pnlCommonParameters.ClientArea.Controls.Add(this.pnl_MyDecl);
            this.pnlCommonParameters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCommonParameters.Location = new System.Drawing.Point(0, 0);
            this.pnlCommonParameters.MinimumSize = new System.Drawing.Size(0, 650);
            this.pnlCommonParameters.Name = "pnlCommonParameters";
            this.pnlCommonParameters.Size = new System.Drawing.Size(828, 715);
            this.pnlCommonParameters.TabIndex = 2;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.txt_SUBSCRIBER_NAME);
            this.ultraPanel2.ClientArea.Controls.Add(this.cbxSortFieldName);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel36);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel39);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel38);
            this.ultraPanel2.ClientArea.Controls.Add(this.cbxSortFieldOrder);
            this.ultraPanel2.ClientArea.Controls.Add(this.cbxRowCount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel37);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 652);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(828, 55);
            this.ultraPanel2.TabIndex = 85;
            // 
            // txt_SUBSCRIBER_NAME
            // 
            this.txt_SUBSCRIBER_NAME.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_SUBSCRIBER_NAME.Location = new System.Drawing.Point(122, 3);
            this.txt_SUBSCRIBER_NAME.Name = "txt_SUBSCRIBER_NAME";
            this.txt_SUBSCRIBER_NAME.Size = new System.Drawing.Size(703, 21);
            this.txt_SUBSCRIBER_NAME.TabIndex = 0;
            // 
            // cbxSortFieldName
            // 
            this.cbxSortFieldName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxSortFieldName.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbxSortFieldName.Location = new System.Drawing.Point(489, 30);
            this.cbxSortFieldName.Name = "cbxSortFieldName";
            this.cbxSortFieldName.Size = new System.Drawing.Size(336, 21);
            this.cbxSortFieldName.TabIndex = 1;
            // 
            // ultraLabel36
            // 
            this.ultraLabel36.AutoSize = true;
            this.ultraLabel36.Location = new System.Drawing.Point(3, 7);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(61, 14);
            this.ultraLabel36.TabIndex = 77;
            this.ultraLabel36.Text = "Подписант";
            // 
            // ultraLabel39
            // 
            this.ultraLabel39.AutoSize = true;
            this.ultraLabel39.Location = new System.Drawing.Point(420, 34);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(63, 14);
            this.ultraLabel39.TabIndex = 83;
            this.ultraLabel39.Text = "Параметра";
            // 
            // ultraLabel38
            // 
            this.ultraLabel38.AutoSize = true;
            this.ultraLabel38.Location = new System.Drawing.Point(3, 34);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(160, 14);
            this.ultraLabel38.TabIndex = 79;
            this.ultraLabel38.Text = "Количество записей в списке";
            // 
            // cbxSortFieldOrder
            // 
            this.cbxSortFieldOrder.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbxSortFieldOrder.Location = new System.Drawing.Point(308, 30);
            this.cbxSortFieldOrder.Name = "cbxSortFieldOrder";
            this.cbxSortFieldOrder.Size = new System.Drawing.Size(93, 21);
            this.cbxSortFieldOrder.TabIndex = 82;
            // 
            // cbxRowCount
            // 
            this.cbxRowCount.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbxRowCount.Location = new System.Drawing.Point(169, 30);
            this.cbxRowCount.Name = "cbxRowCount";
            this.cbxRowCount.Size = new System.Drawing.Size(93, 21);
            this.cbxRowCount.TabIndex = 80;
            // 
            // ultraLabel37
            // 
            this.ultraLabel37.AutoSize = true;
            this.ultraLabel37.Location = new System.Drawing.Point(285, 34);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(17, 14);
            this.ultraLabel37.TabIndex = 81;
            this.ultraLabel37.Text = "по";
            // 
            // pnl_Sum
            // 
            // 
            // pnl_Sum.ClientArea
            // 
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_SELL_BOOK_AMNT_max);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel27);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_TOTAL_AMNT_min);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel26);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_TOTAL_AMNT_max);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel29);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel34);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_MIN_AMNT_min);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_SELL_BOOK_AMNT_min);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel28);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel35);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_MIN_AMNT_max);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_BUY_BOOK_AMNT_max);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel31);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel32);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_MAX_AMNT_min);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_BUY_BOOK_AMNT_min);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel30);
            this.pnl_Sum.ClientArea.Controls.Add(this.ultraLabel33);
            this.pnl_Sum.ClientArea.Controls.Add(this.txt_DISCREP_MAX_AMNT_max);
            this.pnl_Sum.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Sum.Location = new System.Drawing.Point(0, 517);
            this.pnl_Sum.Name = "pnl_Sum";
            this.pnl_Sum.Size = new System.Drawing.Size(828, 135);
            this.pnl_Sum.TabIndex = 89;
            // 
            // txt_DISCREP_SELL_BOOK_AMNT_max
            // 
            this.txt_DISCREP_SELL_BOOK_AMNT_max.Location = new System.Drawing.Point(524, 111);
            this.txt_DISCREP_SELL_BOOK_AMNT_max.Name = "txt_DISCREP_SELL_BOOK_AMNT_max";
            this.txt_DISCREP_SELL_BOOK_AMNT_max.NullText = "до";
            this.txt_DISCREP_SELL_BOOK_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_SELL_BOOK_AMNT_max.TabIndex = 9;
            // 
            // ultraLabel27
            // 
            this.ultraLabel27.AutoSize = true;
            this.ultraLabel27.Location = new System.Drawing.Point(2, 7);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(229, 14);
            this.ultraLabel27.TabIndex = 57;
            this.ultraLabel27.Text = "Общая сумма расхождений в НД/журнале ";
            // 
            // txt_DISCREP_TOTAL_AMNT_min
            // 
            this.txt_DISCREP_TOTAL_AMNT_min.Location = new System.Drawing.Point(294, 3);
            this.txt_DISCREP_TOTAL_AMNT_min.Name = "txt_DISCREP_TOTAL_AMNT_min";
            this.txt_DISCREP_TOTAL_AMNT_min.NullText = "от";
            this.txt_DISCREP_TOTAL_AMNT_min.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_TOTAL_AMNT_min.TabIndex = 0;
            // 
            // ultraLabel26
            // 
            this.ultraLabel26.AutoSize = true;
            this.ultraLabel26.Location = new System.Drawing.Point(510, 7);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel26.TabIndex = 59;
            this.ultraLabel26.Text = "-";
            // 
            // txt_DISCREP_TOTAL_AMNT_max
            // 
            this.txt_DISCREP_TOTAL_AMNT_max.Location = new System.Drawing.Point(524, 3);
            this.txt_DISCREP_TOTAL_AMNT_max.Name = "txt_DISCREP_TOTAL_AMNT_max";
            this.txt_DISCREP_TOTAL_AMNT_max.NullText = "до";
            this.txt_DISCREP_TOTAL_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_TOTAL_AMNT_max.TabIndex = 1;
            // 
            // ultraLabel29
            // 
            this.ultraLabel29.AutoSize = true;
            this.ultraLabel29.Location = new System.Drawing.Point(2, 34);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(266, 14);
            this.ultraLabel29.TabIndex = 61;
            this.ultraLabel29.Text = "Минимальная сумма расхождений в НД/журнале ";
            // 
            // ultraLabel34
            // 
            this.ultraLabel34.AutoSize = true;
            this.ultraLabel34.Location = new System.Drawing.Point(510, 115);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel34.TabIndex = 75;
            this.ultraLabel34.Text = "-";
            // 
            // txt_DISCREP_MIN_AMNT_min
            // 
            this.txt_DISCREP_MIN_AMNT_min.Location = new System.Drawing.Point(294, 30);
            this.txt_DISCREP_MIN_AMNT_min.Name = "txt_DISCREP_MIN_AMNT_min";
            this.txt_DISCREP_MIN_AMNT_min.NullText = "от";
            this.txt_DISCREP_MIN_AMNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_DISCREP_MIN_AMNT_min.TabIndex = 2;
            // 
            // txt_DISCREP_SELL_BOOK_AMNT_min
            // 
            this.txt_DISCREP_SELL_BOOK_AMNT_min.Location = new System.Drawing.Point(294, 111);
            this.txt_DISCREP_SELL_BOOK_AMNT_min.Name = "txt_DISCREP_SELL_BOOK_AMNT_min";
            this.txt_DISCREP_SELL_BOOK_AMNT_min.NullText = "от";
            this.txt_DISCREP_SELL_BOOK_AMNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_DISCREP_SELL_BOOK_AMNT_min.TabIndex = 8;
            // 
            // ultraLabel28
            // 
            this.ultraLabel28.AutoSize = true;
            this.ultraLabel28.Location = new System.Drawing.Point(510, 34);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel28.TabIndex = 63;
            this.ultraLabel28.Text = "-";
            // 
            // ultraLabel35
            // 
            this.ultraLabel35.AutoSize = true;
            this.ultraLabel35.Location = new System.Drawing.Point(2, 115);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(286, 14);
            this.ultraLabel35.TabIndex = 73;
            this.ultraLabel35.Text = "Сумма расхождений по книге продаж (раздел 9 и 12) ";
            // 
            // txt_DISCREP_MIN_AMNT_max
            // 
            this.txt_DISCREP_MIN_AMNT_max.Location = new System.Drawing.Point(524, 30);
            this.txt_DISCREP_MIN_AMNT_max.Name = "txt_DISCREP_MIN_AMNT_max";
            this.txt_DISCREP_MIN_AMNT_max.NullText = "до";
            this.txt_DISCREP_MIN_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_MIN_AMNT_max.TabIndex = 3;
            // 
            // txt_DISCREP_BUY_BOOK_AMNT_max
            // 
            this.txt_DISCREP_BUY_BOOK_AMNT_max.Location = new System.Drawing.Point(524, 84);
            this.txt_DISCREP_BUY_BOOK_AMNT_max.Name = "txt_DISCREP_BUY_BOOK_AMNT_max";
            this.txt_DISCREP_BUY_BOOK_AMNT_max.NullText = "до";
            this.txt_DISCREP_BUY_BOOK_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_BUY_BOOK_AMNT_max.TabIndex = 7;
            // 
            // ultraLabel31
            // 
            this.ultraLabel31.AutoSize = true;
            this.ultraLabel31.Location = new System.Drawing.Point(2, 61);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(271, 14);
            this.ultraLabel31.TabIndex = 65;
            this.ultraLabel31.Text = "Максимальная сумма расхождений в НД/журнале ";
            // 
            // ultraLabel32
            // 
            this.ultraLabel32.AutoSize = true;
            this.ultraLabel32.Location = new System.Drawing.Point(510, 88);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel32.TabIndex = 71;
            this.ultraLabel32.Text = "-";
            // 
            // txt_DISCREP_MAX_AMNT_min
            // 
            this.txt_DISCREP_MAX_AMNT_min.Location = new System.Drawing.Point(294, 57);
            this.txt_DISCREP_MAX_AMNT_min.Name = "txt_DISCREP_MAX_AMNT_min";
            this.txt_DISCREP_MAX_AMNT_min.NullText = "от";
            this.txt_DISCREP_MAX_AMNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_DISCREP_MAX_AMNT_min.TabIndex = 4;
            // 
            // txt_DISCREP_BUY_BOOK_AMNT_min
            // 
            this.txt_DISCREP_BUY_BOOK_AMNT_min.Location = new System.Drawing.Point(294, 84);
            this.txt_DISCREP_BUY_BOOK_AMNT_min.Name = "txt_DISCREP_BUY_BOOK_AMNT_min";
            this.txt_DISCREP_BUY_BOOK_AMNT_min.NullText = "от";
            this.txt_DISCREP_BUY_BOOK_AMNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_DISCREP_BUY_BOOK_AMNT_min.TabIndex = 6;
            // 
            // ultraLabel30
            // 
            this.ultraLabel30.AutoSize = true;
            this.ultraLabel30.Location = new System.Drawing.Point(510, 61);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel30.TabIndex = 67;
            this.ultraLabel30.Text = "-";
            // 
            // ultraLabel33
            // 
            this.ultraLabel33.AutoSize = true;
            this.ultraLabel33.Location = new System.Drawing.Point(2, 88);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(264, 14);
            this.ultraLabel33.TabIndex = 69;
            this.ultraLabel33.Text = "Сумма расхождений по книге покупок (раздел 8) ";
            // 
            // txt_DISCREP_MAX_AMNT_max
            // 
            this.txt_DISCREP_MAX_AMNT_max.Location = new System.Drawing.Point(524, 57);
            this.txt_DISCREP_MAX_AMNT_max.Name = "txt_DISCREP_MAX_AMNT_max";
            this.txt_DISCREP_MAX_AMNT_max.NullText = "до";
            this.txt_DISCREP_MAX_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_DISCREP_MAX_AMNT_max.TabIndex = 5;
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.txt_NDS_WEIGHT_max);
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraLabel25);
            this.ultraPanel4.ClientArea.Controls.Add(this.txt_NDS_WEIGHT_min);
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraLabel24);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 490);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(828, 27);
            this.ultraPanel4.TabIndex = 87;
            // 
            // txt_NDS_WEIGHT_max
            // 
            this.txt_NDS_WEIGHT_max.Location = new System.Drawing.Point(524, 3);
            this.txt_NDS_WEIGHT_max.Name = "txt_NDS_WEIGHT_max";
            this.txt_NDS_WEIGHT_max.NullText = "до";
            this.txt_NDS_WEIGHT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_NDS_WEIGHT_max.TabIndex = 1;
            // 
            // ultraLabel25
            // 
            this.ultraLabel25.AutoSize = true;
            this.ultraLabel25.Location = new System.Drawing.Point(2, 7);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(78, 14);
            this.ultraLabel25.TabIndex = 53;
            this.ultraLabel25.Text = "Удельный вес";
            // 
            // txt_NDS_WEIGHT_min
            // 
            this.txt_NDS_WEIGHT_min.Location = new System.Drawing.Point(294, 3);
            this.txt_NDS_WEIGHT_min.Name = "txt_NDS_WEIGHT_min";
            this.txt_NDS_WEIGHT_min.NullText = "от";
            this.txt_NDS_WEIGHT_min.Size = new System.Drawing.Size(210, 21);
            this.txt_NDS_WEIGHT_min.TabIndex = 0;
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.AutoSize = true;
            this.ultraLabel24.Location = new System.Drawing.Point(510, 7);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel24.TabIndex = 55;
            this.ultraLabel24.Text = "-";
            // 
            // pnl_Rashod
            // 
            // 
            // pnl_Rashod.ClientArea
            // 
            this.pnl_Rashod.ClientArea.Controls.Add(this.txt_TOTAL_DISCREP_COUNT_max);
            this.pnl_Rashod.ClientArea.Controls.Add(this.ultraLabel21);
            this.pnl_Rashod.ClientArea.Controls.Add(this.txt_TOTAL_DISCREP_COUNT_min);
            this.pnl_Rashod.ClientArea.Controls.Add(this.ultraLabel20);
            this.pnl_Rashod.ClientArea.Controls.Add(this.ultraLabel23);
            this.pnl_Rashod.ClientArea.Controls.Add(this.txt_CONTROL_RATIO_DISCREP_COUNT_min);
            this.pnl_Rashod.ClientArea.Controls.Add(this.ultraLabel22);
            this.pnl_Rashod.ClientArea.Controls.Add(this.txt_CONTROL_RATIO_DISCREP_COUNT_max);
            this.pnl_Rashod.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Rashod.Location = new System.Drawing.Point(0, 435);
            this.pnl_Rashod.Name = "pnl_Rashod";
            this.pnl_Rashod.Size = new System.Drawing.Size(828, 55);
            this.pnl_Rashod.TabIndex = 88;
            // 
            // txt_TOTAL_DISCREP_COUNT_max
            // 
            this.txt_TOTAL_DISCREP_COUNT_max.Location = new System.Drawing.Point(524, 3);
            this.txt_TOTAL_DISCREP_COUNT_max.Name = "txt_TOTAL_DISCREP_COUNT_max";
            this.txt_TOTAL_DISCREP_COUNT_max.NullText = "до";
            this.txt_TOTAL_DISCREP_COUNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_TOTAL_DISCREP_COUNT_max.TabIndex = 1;
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.AutoSize = true;
            this.ultraLabel21.Location = new System.Drawing.Point(2, 7);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(234, 14);
            this.ultraLabel21.TabIndex = 45;
            this.ultraLabel21.Text = "Кол-во расхождений по СФ в НД / журнале ";
            // 
            // txt_TOTAL_DISCREP_COUNT_min
            // 
            this.txt_TOTAL_DISCREP_COUNT_min.Location = new System.Drawing.Point(294, 3);
            this.txt_TOTAL_DISCREP_COUNT_min.Name = "txt_TOTAL_DISCREP_COUNT_min";
            this.txt_TOTAL_DISCREP_COUNT_min.NullText = "от";
            this.txt_TOTAL_DISCREP_COUNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_TOTAL_DISCREP_COUNT_min.TabIndex = 0;
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.AutoSize = true;
            this.ultraLabel20.Location = new System.Drawing.Point(510, 7);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel20.TabIndex = 47;
            this.ultraLabel20.Text = "-";
            // 
            // ultraLabel23
            // 
            this.ultraLabel23.AutoSize = true;
            this.ultraLabel23.Location = new System.Drawing.Point(2, 34);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(177, 14);
            this.ultraLabel23.TabIndex = 49;
            this.ultraLabel23.Text = "Кол-во расхождений по КС в НД ";
            // 
            // txt_CONTROL_RATIO_DISCREP_COUNT_min
            // 
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Location = new System.Drawing.Point(294, 30);
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Name = "txt_CONTROL_RATIO_DISCREP_COUNT_min";
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min.NullText = "от";
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_CONTROL_RATIO_DISCREP_COUNT_min.TabIndex = 2;
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.AutoSize = true;
            this.ultraLabel22.Location = new System.Drawing.Point(510, 34);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel22.TabIndex = 51;
            this.ultraLabel22.Text = "-";
            // 
            // txt_CONTROL_RATIO_DISCREP_COUNT_max
            // 
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Location = new System.Drawing.Point(524, 30);
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Name = "txt_CONTROL_RATIO_DISCREP_COUNT_max";
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max.NullText = "до";
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_CONTROL_RATIO_DISCREP_COUNT_max.TabIndex = 3;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.txt_COMPENSATION_AMNT_max);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel18);
            this.ultraPanel3.ClientArea.Controls.Add(this.txt_COMPENSATION_AMNT_min);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel19);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 408);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(828, 27);
            this.ultraPanel3.TabIndex = 86;
            // 
            // txt_COMPENSATION_AMNT_max
            // 
            this.txt_COMPENSATION_AMNT_max.Location = new System.Drawing.Point(524, 3);
            this.txt_COMPENSATION_AMNT_max.Name = "txt_COMPENSATION_AMNT_max";
            this.txt_COMPENSATION_AMNT_max.NullText = "до";
            this.txt_COMPENSATION_AMNT_max.Size = new System.Drawing.Size(210, 21);
            this.txt_COMPENSATION_AMNT_max.TabIndex = 1;
            // 
            // ultraLabel18
            // 
            this.ultraLabel18.AutoSize = true;
            this.ultraLabel18.Location = new System.Drawing.Point(2, 7);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(211, 14);
            this.ultraLabel18.TabIndex = 40;
            this.ultraLabel18.Text = "Сумма НДС (к уплате / к возмещению)  ";
            // 
            // txt_COMPENSATION_AMNT_min
            // 
            this.txt_COMPENSATION_AMNT_min.Location = new System.Drawing.Point(294, 3);
            this.txt_COMPENSATION_AMNT_min.Name = "txt_COMPENSATION_AMNT_min";
            this.txt_COMPENSATION_AMNT_min.NullText = "от";
            this.txt_COMPENSATION_AMNT_min.Size = new System.Drawing.Size(209, 21);
            this.txt_COMPENSATION_AMNT_min.TabIndex = 0;
            // 
            // ultraLabel19
            // 
            this.ultraLabel19.AutoSize = true;
            this.ultraLabel19.Location = new System.Drawing.Point(510, 7);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel19.TabIndex = 43;
            this.ultraLabel19.Text = "-";
            // 
            // ultraPanel8
            // 
            // 
            // ultraPanel8.ClientArea
            // 
            this.ultraPanel8.ClientArea.Controls.Add(this.cbInspection);
            this.ultraPanel8.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel8.Location = new System.Drawing.Point(0, 381);
            this.ultraPanel8.Name = "ultraPanel8";
            this.ultraPanel8.Size = new System.Drawing.Size(828, 27);
            this.ultraPanel8.TabIndex = 91;
            // 
            // cbInspection
            // 
            this.cbInspection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInspection.DisplayMember = "Name";
            this.cbInspection.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbInspection.Location = new System.Drawing.Point(119, 3);
            this.cbInspection.Name = "cbInspection";
            this.cbInspection.Size = new System.Drawing.Size(706, 21);
            this.cbInspection.TabIndex = 0;
            this.cbInspection.ValueMember = "Code";
            this.cbInspection.ValueChanged += new System.EventHandler(this.cbInspection_ValueChanged);
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.AutoSize = true;
            this.ultraLabel15.Location = new System.Drawing.Point(3, 7);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(62, 14);
            this.ultraLabel15.TabIndex = 34;
            this.ultraLabel15.Text = "Инспекция";
            // 
            // pnl_FOR
            // 
            // 
            // pnl_FOR.ClientArea
            // 
            this.pnl_FOR.ClientArea.Controls.Add(this.cbDistrict);
            this.pnl_FOR.ClientArea.Controls.Add(this.ultraLabel16);
            this.pnl_FOR.ClientArea.Controls.Add(this.ultraLabel17);
            this.pnl_FOR.ClientArea.Controls.Add(this.cbRegion);
            this.pnl_FOR.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_FOR.Location = new System.Drawing.Point(0, 326);
            this.pnl_FOR.Name = "pnl_FOR";
            this.pnl_FOR.Size = new System.Drawing.Size(828, 55);
            this.pnl_FOR.TabIndex = 90;
            // 
            // cbDistrict
            // 
            this.cbDistrict.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDistrict.DisplayMember = "FullName";
            this.cbDistrict.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbDistrict.Location = new System.Drawing.Point(119, 3);
            this.cbDistrict.Name = "cbDistrict";
            this.cbDistrict.Size = new System.Drawing.Size(706, 21);
            this.cbDistrict.TabIndex = 0;
            this.cbDistrict.ValueMember = "Id";
            this.cbDistrict.ValueChanged += new System.EventHandler(this.cbDistrict_ValueChanged);
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.AutoSize = true;
            this.ultraLabel16.Location = new System.Drawing.Point(2, 34);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(41, 14);
            this.ultraLabel16.TabIndex = 36;
            this.ultraLabel16.Text = "Регион";
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.AutoSize = true;
            this.ultraLabel17.Location = new System.Drawing.Point(3, 7);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(110, 14);
            this.ultraLabel17.TabIndex = 38;
            this.ultraLabel17.Text = "Федеральный округ";
            // 
            // cbRegion
            // 
            this.cbRegion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRegion.DisplayMember = "RegionFullName";
            this.cbRegion.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbRegion.Location = new System.Drawing.Point(119, 30);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(706, 21);
            this.cbRegion.TabIndex = 1;
            this.cbRegion.ValueMember = "Id";
            this.cbRegion.ValueChanged += new System.EventHandler(this.cbRegion_ValueChanged);
            // 
            // pnl_PZI
            // 
            // 
            // pnl_PZI.ClientArea
            // 
            this.pnl_PZI.ClientArea.Controls.Add(this.ultraComboEditor16);
            this.pnl_PZI.ClientArea.Controls.Add(this.ultraLabel40);
            this.pnl_PZI.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_PZI.Location = new System.Drawing.Point(0, 299);
            this.pnl_PZI.Name = "pnl_PZI";
            this.pnl_PZI.Size = new System.Drawing.Size(828, 27);
            this.pnl_PZI.TabIndex = 92;
            // 
            // ultraComboEditor16
            // 
            this.ultraComboEditor16.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.ultraComboEditor16.Location = new System.Drawing.Point(181, 3);
            this.ultraComboEditor16.Name = "ultraComboEditor16";
            this.ultraComboEditor16.Size = new System.Drawing.Size(93, 21);
            this.ultraComboEditor16.TabIndex = 0;
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.AutoSize = true;
            this.ultraLabel40.Location = new System.Drawing.Point(3, 7);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(172, 14);
            this.ultraLabel40.TabIndex = 34;
            this.ultraLabel40.Text = "Признак значимых измененний";
            // 
            // ultraPanel10
            // 
            // 
            // ultraPanel10.ClientArea
            // 
            this.ultraPanel10.ClientArea.Controls.Add(this.cbAllTaxPeriod);
            this.ultraPanel10.ClientArea.Controls.Add(this.sui_Unknown);
            this.ultraPanel10.ClientArea.Controls.Add(this.sui_Low);
            this.ultraPanel10.ClientArea.Controls.Add(this.sui_Medium);
            this.ultraPanel10.ClientArea.Controls.Add(this.sui_Hight);
            this.ultraPanel10.ClientArea.Controls.Add(this.txt_INN);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_INN);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_KPP);
            this.ultraPanel10.ClientArea.Controls.Add(this.txt_KPP);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_SEOD_DECL_ID);
            this.ultraPanel10.ClientArea.Controls.Add(this.txt_SEOD_DECL_ID);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_DECL_SIGN);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel14);
            this.ultraPanel10.ClientArea.Controls.Add(this.txt_NAME);
            this.ultraPanel10.ClientArea.Controls.Add(this.dte_DECL_DATE_end);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_DECL_TYPE_CODE);
            this.ultraPanel10.ClientArea.Controls.Add(this.dte_DECL_DATE_begin);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_STATUS);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_SUR_CODE_4);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_SUR_CODE_3);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_CATEGORY);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_SUR_CODE_2);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel8);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanel10.ClientArea.Controls.Add(this.comboTaxPeriod);
            this.ultraPanel10.ClientArea.Controls.Add(this.txt_CORRECTION_NUMBER);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel10.ClientArea.Controls.Add(this.ultraLabel10);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_FISCAL_YEAR);
            this.ultraPanel10.ClientArea.Controls.Add(this.cbx_SUR_CODE_1);
            this.ultraPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel10.Location = new System.Drawing.Point(0, 27);
            this.ultraPanel10.Name = "ultraPanel10";
            this.ultraPanel10.Size = new System.Drawing.Size(828, 272);
            this.ultraPanel10.TabIndex = 93;
            // 
            // cbAllTaxPeriod
            // 
            this.cbAllTaxPeriod.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbAllTaxPeriod.Checked = true;
            this.cbAllTaxPeriod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllTaxPeriod.Location = new System.Drawing.Point(438, 139);
            this.cbAllTaxPeriod.Name = "cbAllTaxPeriod";
            this.cbAllTaxPeriod.Size = new System.Drawing.Size(146, 20);
            this.cbAllTaxPeriod.TabIndex = 37;
            this.cbAllTaxPeriod.Text = "Все отчетные периоды";
            this.cbAllTaxPeriod.CheckedValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // sui_Unknown
            // 
            this.sui_Unknown.BackColor = System.Drawing.Color.Transparent;
            this.sui_Unknown.Code = null;
            this.sui_Unknown.Location = new System.Drawing.Point(486, 192);
            this.sui_Unknown.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.sui_Unknown.MaximumSize = new System.Drawing.Size(20, 20);
            this.sui_Unknown.MinimumSize = new System.Drawing.Size(20, 20);
            this.sui_Unknown.Name = "sui_Unknown";
            this.sui_Unknown.Size = new System.Drawing.Size(20, 20);
            this.sui_Unknown.TabIndex = 36;
            // 
            // sui_Low
            // 
            this.sui_Low.BackColor = System.Drawing.Color.Transparent;
            this.sui_Low.Code = null;
            this.sui_Low.Location = new System.Drawing.Point(341, 192);
            this.sui_Low.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.sui_Low.MaximumSize = new System.Drawing.Size(20, 20);
            this.sui_Low.MinimumSize = new System.Drawing.Size(20, 20);
            this.sui_Low.Name = "sui_Low";
            this.sui_Low.Size = new System.Drawing.Size(20, 20);
            this.sui_Low.TabIndex = 35;
            // 
            // sui_Medium
            // 
            this.sui_Medium.BackColor = System.Drawing.Color.Transparent;
            this.sui_Medium.Code = null;
            this.sui_Medium.Location = new System.Drawing.Point(216, 192);
            this.sui_Medium.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.sui_Medium.MaximumSize = new System.Drawing.Size(20, 20);
            this.sui_Medium.MinimumSize = new System.Drawing.Size(20, 20);
            this.sui_Medium.Name = "sui_Medium";
            this.sui_Medium.Size = new System.Drawing.Size(20, 20);
            this.sui_Medium.TabIndex = 34;
            // 
            // sui_Hight
            // 
            this.sui_Hight.BackColor = System.Drawing.Color.Transparent;
            this.sui_Hight.Code = null;
            this.sui_Hight.Location = new System.Drawing.Point(99, 192);
            this.sui_Hight.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.sui_Hight.MaximumSize = new System.Drawing.Size(20, 20);
            this.sui_Hight.MinimumSize = new System.Drawing.Size(20, 20);
            this.sui_Hight.Name = "sui_Hight";
            this.sui_Hight.Size = new System.Drawing.Size(20, 20);
            this.sui_Hight.TabIndex = 33;
            // 
            // txt_INN
            // 
            this.txt_INN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_INN.Location = new System.Drawing.Point(137, 3);
            this.txt_INN.Name = "txt_INN";
            this.txt_INN.Size = new System.Drawing.Size(688, 21);
            this.txt_INN.TabIndex = 0;
            this.txt_INN.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(3, 7);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(29, 14);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "ИНН";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(3, 34);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(28, 14);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "КПП";
            // 
            // cbx_INN
            // 
            this.cbx_INN.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_INN.Location = new System.Drawing.Point(38, 3);
            this.cbx_INN.Name = "cbx_INN";
            this.cbx_INN.Size = new System.Drawing.Size(93, 21);
            this.cbx_INN.TabIndex = 2;
            this.cbx_INN.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // cbx_KPP
            // 
            this.cbx_KPP.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_KPP.Location = new System.Drawing.Point(38, 30);
            this.cbx_KPP.Name = "cbx_KPP";
            this.cbx_KPP.Size = new System.Drawing.Size(93, 21);
            this.cbx_KPP.TabIndex = 3;
            this.cbx_KPP.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // txt_KPP
            // 
            this.txt_KPP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_KPP.Location = new System.Drawing.Point(137, 30);
            this.txt_KPP.Name = "txt_KPP";
            this.txt_KPP.Size = new System.Drawing.Size(688, 21);
            this.txt_KPP.TabIndex = 1;
            this.txt_KPP.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Location = new System.Drawing.Point(3, 61);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(113, 14);
            this.ultraLabel3.TabIndex = 6;
            this.ultraLabel3.Text = "Регистрационный №";
            // 
            // cbx_SEOD_DECL_ID
            // 
            this.cbx_SEOD_DECL_ID.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_SEOD_DECL_ID.Location = new System.Drawing.Point(122, 57);
            this.cbx_SEOD_DECL_ID.Name = "cbx_SEOD_DECL_ID";
            this.cbx_SEOD_DECL_ID.Size = new System.Drawing.Size(93, 21);
            this.cbx_SEOD_DECL_ID.TabIndex = 7;
            this.cbx_SEOD_DECL_ID.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // txt_SEOD_DECL_ID
            // 
            this.txt_SEOD_DECL_ID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_SEOD_DECL_ID.Location = new System.Drawing.Point(221, 57);
            this.txt_SEOD_DECL_ID.Name = "txt_SEOD_DECL_ID";
            this.txt_SEOD_DECL_ID.Size = new System.Drawing.Size(604, 21);
            this.txt_SEOD_DECL_ID.TabIndex = 2;
            this.txt_SEOD_DECL_ID.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // cbx_DECL_SIGN
            // 
            this.cbx_DECL_SIGN.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_DECL_SIGN.Location = new System.Drawing.Point(92, 245);
            this.cbx_DECL_SIGN.Name = "cbx_DECL_SIGN";
            this.cbx_DECL_SIGN.Size = new System.Drawing.Size(93, 21);
            this.cbx_DECL_SIGN.TabIndex = 16;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(3, 88);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(83, 14);
            this.ultraLabel4.TabIndex = 9;
            this.ultraLabel4.Text = "Наименование";
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.AutoSize = true;
            this.ultraLabel14.Location = new System.Drawing.Point(5, 249);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(69, 14);
            this.ultraLabel14.TabIndex = 32;
            this.ultraLabel14.Text = "Признак НД";
            // 
            // txt_NAME
            // 
            this.txt_NAME.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_NAME.Location = new System.Drawing.Point(122, 84);
            this.txt_NAME.Name = "txt_NAME";
            this.txt_NAME.Size = new System.Drawing.Size(703, 21);
            this.txt_NAME.TabIndex = 3;
            this.txt_NAME.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // dte_DECL_DATE_end
            // 
            this.dte_DECL_DATE_end.DateTime = new System.DateTime(2015, 12, 4, 0, 0, 0, 0);
            this.dte_DECL_DATE_end.Location = new System.Drawing.Point(294, 218);
            this.dte_DECL_DATE_end.Name = "dte_DECL_DATE_end";
            this.dte_DECL_DATE_end.Size = new System.Drawing.Size(116, 21);
            this.dte_DECL_DATE_end.TabIndex = 15;
            this.dte_DECL_DATE_end.Value = new System.DateTime(2015, 12, 4, 0, 0, 0, 0);
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.AutoSize = true;
            this.ultraLabel5.Location = new System.Drawing.Point(3, 115);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(83, 14);
            this.ultraLabel5.TabIndex = 11;
            this.ultraLabel5.Text = "Тип документа";
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.AutoSize = true;
            this.ultraLabel13.Location = new System.Drawing.Point(276, 222);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(8, 14);
            this.ultraLabel13.TabIndex = 30;
            this.ultraLabel13.Text = "-";
            // 
            // cbx_DECL_TYPE_CODE
            // 
            this.cbx_DECL_TYPE_CODE.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_DECL_TYPE_CODE.Location = new System.Drawing.Point(92, 111);
            this.cbx_DECL_TYPE_CODE.Name = "cbx_DECL_TYPE_CODE";
            this.cbx_DECL_TYPE_CODE.Size = new System.Drawing.Size(93, 21);
            this.cbx_DECL_TYPE_CODE.TabIndex = 12;
            this.cbx_DECL_TYPE_CODE.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // dte_DECL_DATE_begin
            // 
            this.dte_DECL_DATE_begin.DateTime = new System.DateTime(2015, 12, 4, 0, 0, 0, 0);
            this.dte_DECL_DATE_begin.Location = new System.Drawing.Point(154, 218);
            this.dte_DECL_DATE_begin.Name = "dte_DECL_DATE_begin";
            this.dte_DECL_DATE_begin.Size = new System.Drawing.Size(116, 21);
            this.dte_DECL_DATE_begin.TabIndex = 14;
            this.dte_DECL_DATE_begin.Value = new System.DateTime(2015, 12, 4, 0, 0, 0, 0);
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.AutoSize = true;
            this.ultraLabel6.Location = new System.Drawing.Point(221, 115);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(67, 14);
            this.ultraLabel6.TabIndex = 13;
            this.ultraLabel6.Text = "Статус КНП";
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.AutoSize = true;
            this.ultraLabel12.Location = new System.Drawing.Point(5, 222);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(143, 14);
            this.ultraLabel12.TabIndex = 28;
            this.ultraLabel12.Text = "Дата представления в НО";
            // 
            // cbx_STATUS
            // 
            this.cbx_STATUS.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_STATUS.Location = new System.Drawing.Point(294, 111);
            this.cbx_STATUS.Name = "cbx_STATUS";
            this.cbx_STATUS.Size = new System.Drawing.Size(93, 21);
            this.cbx_STATUS.TabIndex = 14;
            this.cbx_STATUS.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // cbx_SUR_CODE_4
            // 
            this.cbx_SUR_CODE_4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_SUR_CODE_4.Location = new System.Drawing.Point(522, 192);
            this.cbx_SUR_CODE_4.Name = "cbx_SUR_CODE_4";
            this.cbx_SUR_CODE_4.Size = new System.Drawing.Size(116, 20);
            this.cbx_SUR_CODE_4.TabIndex = 13;
            this.cbx_SUR_CODE_4.Text = "Неопределенный";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.AutoSize = true;
            this.ultraLabel7.Location = new System.Drawing.Point(438, 115);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(173, 14);
            this.ultraLabel7.TabIndex = 15;
            this.ultraLabel7.Text = "Крупнейший/не крупнейший НП";
            // 
            // cbx_SUR_CODE_3
            // 
            this.cbx_SUR_CODE_3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_SUR_CODE_3.Location = new System.Drawing.Point(377, 192);
            this.cbx_SUR_CODE_3.Name = "cbx_SUR_CODE_3";
            this.cbx_SUR_CODE_3.Size = new System.Drawing.Size(93, 20);
            this.cbx_SUR_CODE_3.TabIndex = 12;
            this.cbx_SUR_CODE_3.Text = "Низкий";
            // 
            // cbx_CATEGORY
            // 
            this.cbx_CATEGORY.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_CATEGORY.Location = new System.Drawing.Point(617, 111);
            this.cbx_CATEGORY.Name = "cbx_CATEGORY";
            this.cbx_CATEGORY.Size = new System.Drawing.Size(93, 21);
            this.cbx_CATEGORY.TabIndex = 16;
            this.cbx_CATEGORY.ValueChanged += new System.EventHandler(this.ctrl_ValueChanged);
            // 
            // cbx_SUR_CODE_2
            // 
            this.cbx_SUR_CODE_2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_SUR_CODE_2.Location = new System.Drawing.Point(252, 192);
            this.cbx_SUR_CODE_2.Name = "cbx_SUR_CODE_2";
            this.cbx_SUR_CODE_2.Size = new System.Drawing.Size(73, 20);
            this.cbx_SUR_CODE_2.TabIndex = 11;
            this.cbx_SUR_CODE_2.Text = "Средний";
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.AutoSize = true;
            this.ultraLabel8.Location = new System.Drawing.Point(3, 142);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(98, 14);
            this.ultraLabel8.TabIndex = 17;
            this.ultraLabel8.Text = "Отчетный период";
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.AutoSize = true;
            this.ultraLabel11.Location = new System.Drawing.Point(3, 195);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(81, 14);
            this.ultraLabel11.TabIndex = 24;
            this.ultraLabel11.Text = "Значение СУР";
            // 
            // comboTaxPeriod
            // 
            this.comboTaxPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTaxPeriod.Location = new System.Drawing.Point(107, 138);
            this.comboTaxPeriod.Name = "comboTaxPeriod";
            this.comboTaxPeriod.Size = new System.Drawing.Size(93, 21);
            this.comboTaxPeriod.TabIndex = 7;
            // 
            // txt_CORRECTION_NUMBER
            // 
            this.txt_CORRECTION_NUMBER.Location = new System.Drawing.Point(152, 165);
            this.txt_CORRECTION_NUMBER.Name = "txt_CORRECTION_NUMBER";
            this.txt_CORRECTION_NUMBER.Size = new System.Drawing.Size(100, 21);
            this.txt_CORRECTION_NUMBER.TabIndex = 9;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.AutoSize = true;
            this.ultraLabel9.Location = new System.Drawing.Point(223, 142);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(83, 14);
            this.ultraLabel9.TabIndex = 19;
            this.ultraLabel9.Text = "Налоговый год";
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.AutoSize = true;
            this.ultraLabel10.Location = new System.Drawing.Point(3, 169);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(143, 14);
            this.ultraLabel10.TabIndex = 22;
            this.ultraLabel10.Text = "№ корректировки / версии";
            // 
            // cbx_FISCAL_YEAR
            // 
            this.cbx_FISCAL_YEAR.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbx_FISCAL_YEAR.Location = new System.Drawing.Point(312, 138);
            this.cbx_FISCAL_YEAR.Name = "cbx_FISCAL_YEAR";
            this.cbx_FISCAL_YEAR.Size = new System.Drawing.Size(93, 21);
            this.cbx_FISCAL_YEAR.TabIndex = 8;
            // 
            // cbx_SUR_CODE_1
            // 
            this.cbx_SUR_CODE_1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_SUR_CODE_1.Location = new System.Drawing.Point(122, 192);
            this.cbx_SUR_CODE_1.Name = "cbx_SUR_CODE_1";
            this.cbx_SUR_CODE_1.Size = new System.Drawing.Size(78, 20);
            this.cbx_SUR_CODE_1.TabIndex = 10;
            this.cbx_SUR_CODE_1.Text = "Высокий";
            // 
            // pnl_MyDecl
            // 
            // 
            // pnl_MyDecl.ClientArea
            // 
            this.pnl_MyDecl.ClientArea.Controls.Add(this.rbMyDeclarations);
            this.pnl_MyDecl.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_MyDecl.Location = new System.Drawing.Point(0, 0);
            this.pnl_MyDecl.Name = "pnl_MyDecl";
            this.pnl_MyDecl.Size = new System.Drawing.Size(828, 27);
            this.pnl_MyDecl.TabIndex = 94;
            this.pnl_MyDecl.Visible = false;
            // 
            // rbMyDeclarations
            // 
            this.rbMyDeclarations.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.rbMyDeclarations.CheckedIndex = 0;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "Мои";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Не взятые в работу";
            valueListItem3.DataValue = "ValueListItem2";
            valueListItem3.DisplayText = "Все";
            this.rbMyDeclarations.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.rbMyDeclarations.Location = new System.Drawing.Point(5, 5);
            this.rbMyDeclarations.Name = "rbMyDeclarations";
            this.rbMyDeclarations.Size = new System.Drawing.Size(667, 19);
            this.rbMyDeclarations.TabIndex = 0;
            this.rbMyDeclarations.Text = "Мои";
            // 
            // gbExtraParemeters
            // 
            this.gbExtraParemeters.Controls.Add(this.ultraExpandableGroupBoxPanel2);
            this.gbExtraParemeters.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbExtraParemeters.Expanded = false;
            this.gbExtraParemeters.ExpandedSize = new System.Drawing.Size(828, 45);
            this.gbExtraParemeters.Location = new System.Drawing.Point(0, 733);
            this.gbExtraParemeters.Name = "gbExtraParemeters";
            this.gbExtraParemeters.Size = new System.Drawing.Size(828, 21);
            this.gbExtraParemeters.TabIndex = 1;
            this.gbExtraParemeters.Text = "Дополнительные параметры";
            this.gbExtraParemeters.Visible = false;
            // 
            // ultraExpandableGroupBoxPanel2
            // 
            this.ultraExpandableGroupBoxPanel2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraExpandableGroupBoxPanel2.Name = "ultraExpandableGroupBoxPanel2";
            this.ultraExpandableGroupBoxPanel2.Size = new System.Drawing.Size(822, 23);
            this.ultraExpandableGroupBoxPanel2.TabIndex = 0;
            this.ultraExpandableGroupBoxPanel2.Visible = false;
            // 
            // DeclarationsListFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.ultraPanel1);
            this.Name = "DeclarationsListFilter";
            this.Size = new System.Drawing.Size(828, 754);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.panelBack.ClientArea.ResumeLayout(false);
            this.panelBack.ResumeLayout(false);
            this.pnlCommonParameters.ClientArea.ResumeLayout(false);
            this.pnlCommonParameters.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ClientArea.PerformLayout();
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_SUBSCRIBER_NAME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSortFieldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSortFieldOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRowCount)).EndInit();
            this.pnl_Sum.ClientArea.ResumeLayout(false);
            this.pnl_Sum.ClientArea.PerformLayout();
            this.pnl_Sum.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_SELL_BOOK_AMNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_TOTAL_AMNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_TOTAL_AMNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MIN_AMNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_SELL_BOOK_AMNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MIN_AMNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_BUY_BOOK_AMNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MAX_AMNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_BUY_BOOK_AMNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DISCREP_MAX_AMNT_max)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ClientArea.PerformLayout();
            this.ultraPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_NDS_WEIGHT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NDS_WEIGHT_min)).EndInit();
            this.pnl_Rashod.ClientArea.ResumeLayout(false);
            this.pnl_Rashod.ClientArea.PerformLayout();
            this.pnl_Rashod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_TOTAL_DISCREP_COUNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TOTAL_DISCREP_COUNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CONTROL_RATIO_DISCREP_COUNT_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CONTROL_RATIO_DISCREP_COUNT_max)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ClientArea.PerformLayout();
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_COMPENSATION_AMNT_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_COMPENSATION_AMNT_min)).EndInit();
            this.ultraPanel8.ClientArea.ResumeLayout(false);
            this.ultraPanel8.ClientArea.PerformLayout();
            this.ultraPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbInspection)).EndInit();
            this.pnl_FOR.ClientArea.ResumeLayout(false);
            this.pnl_FOR.ClientArea.PerformLayout();
            this.pnl_FOR.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRegion)).EndInit();
            this.pnl_PZI.ClientArea.ResumeLayout(false);
            this.pnl_PZI.ClientArea.PerformLayout();
            this.pnl_PZI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor16)).EndInit();
            this.ultraPanel10.ClientArea.ResumeLayout(false);
            this.ultraPanel10.ClientArea.PerformLayout();
            this.ultraPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_INN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_INN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_KPP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_KPP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_SEOD_DECL_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SEOD_DECL_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_DECL_SIGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NAME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte_DECL_DATE_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_DECL_TYPE_CODE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte_DECL_DATE_begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_STATUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_CATEGORY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CORRECTION_NUMBER)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbx_FISCAL_YEAR)).EndInit();
            this.pnl_MyDecl.ClientArea.ResumeLayout(false);
            this.pnl_MyDecl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbMyDeclarations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbExtraParemeters)).EndInit();
            this.gbExtraParemeters.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbxSortFieldName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbxSortFieldOrder;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbxRowCount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_SUBSCRIBER_NAME;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_SELL_BOOK_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_SELL_BOOK_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_BUY_BOOK_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_BUY_BOOK_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_MAX_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_MAX_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_MIN_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_MIN_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_TOTAL_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_DISCREP_TOTAL_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_NDS_WEIGHT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_NDS_WEIGHT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_CONTROL_RATIO_DISCREP_COUNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_CONTROL_RATIO_DISCREP_COUNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_TOTAL_DISCREP_COUNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_TOTAL_DISCREP_COUNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_COMPENSATION_AMNT_max;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_COMPENSATION_AMNT_min;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbRegion;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbDistrict;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbInspection;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_DECL_SIGN;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dte_DECL_DATE_end;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dte_DECL_DATE_begin;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbx_SUR_CODE_4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbx_SUR_CODE_3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbx_SUR_CODE_2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_CORRECTION_NUMBER;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbx_SUR_CODE_1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_FISCAL_YEAR;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTaxPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_CATEGORY;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_STATUS;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_DECL_TYPE_CODE;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_NAME;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_SEOD_DECL_ID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_SEOD_DECL_ID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_KPP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txt_INN;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_KPP;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbx_INN;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox gbExtraParemeters;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel pnl_Sum;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel pnl_Rashod;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel8;
        private Infragistics.Win.Misc.UltraPanel pnl_FOR;
        private Infragistics.Win.Misc.UltraPanel pnl_PZI;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.Misc.UltraPanel ultraPanel10;
        private Infragistics.Win.Misc.UltraPanel pnl_MyDecl;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet rbMyDeclarations;
        private Infragistics.Win.Misc.UltraPanel panelBack;
        private Infragistics.Win.Misc.UltraPanel pnlCommonParameters;
        private Sur.SurIcon sui_Hight;
        private Sur.SurIcon sui_Unknown;
        private Sur.SurIcon sui_Low;
        private Sur.SurIcon sui_Medium;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbAllTaxPeriod;
    }
}
