﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.DeclarationsListFilter
{
    public interface IFilterProvider
    {
        QueryConditions QueryConditions { get; }
        void ResetFilter();

    }
}
