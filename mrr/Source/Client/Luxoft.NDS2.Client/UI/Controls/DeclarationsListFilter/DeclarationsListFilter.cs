﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Principal;
using CommonComponents.Communication;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.TransformQueryCondition;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Collections;
using ColumnFilter = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Services;


namespace Luxoft.NDS2.Client.UI.Controls.DeclarationsListFilter
{
    public partial class DeclarationsListFilter : UserControl, IFilterProvider
    {
        #region Constants

        private static string _taxPeriodCode1 = "01/02/03/21/51/71/72/73";
        private static string _taxPeriodCode2 = "04/05/06/22/54/74/75/76";
        private static string _taxPeriodCode3 = "07/08/09/23/55/77/78/79";
        private static string _taxPeriodCode4 = "10/11/12/24/56/80/81/82";


        #endregion

        private ServiceCollection _serviceCollection;
        private DeclarationsListFilterMode _filterMode;
        private dynamic _parentPresenter;

        private bool _frezeUi = false;

        private List<FederalDistrict> _federalDistrictList = null;
        List<Region> _regionList = null;

        public DeclarationsListFilter()
        {
            InitializeComponent();
        }

        //public void InitializeFilter(ServiceCollection serviceCollection, DeclarationsListFilterMode filterMode)
        //{
        //    _serviceCollection = serviceCollection;
        //    _filterMode = filterMode;
        //    ApplyFilterMode();
        //    InitializeDictionaries();
        //    SetDefaultValues();
        //    LoadConfiguration();
        //    ActualizeUiState();
        //}

        public void InitializeFilter(dynamic parentPresenter, DeclarationsListFilterMode filterMode)
        {
            _frezeUi = true;

            _parentPresenter = parentPresenter;
            _serviceCollection = _parentPresenter.WorkItem.Services;
            _filterMode = filterMode;
            ApplyFilterMode();

            InitializeDictionaries();
            SetDefaultValues();
            LoadConfiguration();
            ActualizeUiState();

            _frezeUi = false;
        }

        private void InitializeDictionaries()
        {
            cbx_INN.Items.AddRange(UiRoutines.EnumToValueListItems<ConstraintsEnumeration>());
            cbx_KPP.Items.AddRange(UiRoutines.EnumToValueListItems<ConstraintsEnumeration>());
            cbx_SEOD_DECL_ID.Items.AddRange(UiRoutines.EnumToValueListItems<ConstraintsEnumeration>());
            cbx_DECL_TYPE_CODE.Items.AddRange(UiRoutines.EnumToValueListItems<DocType>());
            cbx_STATUS.Items.AddRange(UiRoutines.EnumToValueListItems<KnpState>());
            cbx_CATEGORY.Items.AddRange(UiRoutines.EnumToValueListItems<LpImportance>());

            comboTaxPeriod.Items.AddRange(UiRoutines.EnumToValueListItems<TaxPeriodEnum>());

            var taxPeriodConfiguration = (TaxPeriodConfiguration)_parentPresenter.LoadTaxPeriodConfiguration();
            if (taxPeriodConfiguration != null)
            {
                var year = taxPeriodConfiguration.FiscalYearBegin;
                while (year <= DateTime.Now.Year)
                {
                    cbx_FISCAL_YEAR.Items.Add(new ValueListItem(year, year.ToString()));
                    year++;
                }
            }

            cbx_DECL_SIGN.Items.AddRange(UiRoutines.EnumToValueListItems<NdSymptom>());

            DictionarySur surs = _parentPresenter.Sur;
            sui_Hight.SetSurDictionary(surs);
            sui_Hight.Code = 1;
            sui_Medium.SetSurDictionary(surs);
            sui_Medium.Code = 2;
            sui_Low.SetSurDictionary(surs);
            sui_Low.Code = 3;
            sui_Unknown.SetSurDictionary(surs);
            sui_Unknown.Code = 4;

            this.cbxRowCount.Items.AddRange(UiRoutines.EnumToValueListItems<RowCountConstraint>());
            this.cbxSortFieldOrder.Items.AddRange(UiRoutines.EnumToValueListItems<SortDirection>());
            
            UpdateDistinctList();
            UpdateRegionList();




        }

        private void SetDefaultValues()
        {
            cbx_INN.SelectedIndex = 0;
            txt_INN.Text = string.Empty;
            cbx_KPP.SelectedIndex = 0;
            txt_INN.Text = string.Empty;

            cbx_SEOD_DECL_ID.SelectedIndex = 0;
            txt_SEOD_DECL_ID.Text = string.Empty;
            txt_NAME.Text = string.Empty;

            cbx_DECL_TYPE_CODE.SelectedIndex = 0;
            cbx_STATUS.SelectedIndex = 0;
            cbx_CATEGORY.SelectedIndex = 0;

            comboTaxPeriod.SelectedIndex = 0;
            cbx_FISCAL_YEAR.SelectListItemByValue(DateTime.Now.Year);

            txt_CORRECTION_NUMBER.Text = string.Empty;
            cbx_SUR_CODE_1.Checked = false;
            cbx_SUR_CODE_2.Checked = false;
            cbx_SUR_CODE_3.Checked = false;
            cbx_SUR_CODE_4.Checked = false;

            dte_DECL_DATE_begin.Value = null;
            dte_DECL_DATE_end.Value = null;

            cbx_DECL_SIGN.SelectedIndex = 0;
            txt_SUBSCRIBER_NAME.Text = string.Empty;
            //if (comboTaxPeriod.Items.Count > 0)
            //{
            //    var taxPeriodModel = (TaxPeriodModel)_parentPresenter.TaxPeriodModel;
            //    comboTaxPeriod.SelectListItemByValue(taxPeriodModel.TaxPeriodDefault);
            //}

            cbxRowCount.SelectedIndex = 0;


        }

        public void ResetFilter()
        {
           SetDefaultValues();
        }

        private void LoadConfiguration()
        {

            //var settingsProvider = (ISettingsProvider) _parentPresenter.SettingsProviderPredFilter;
            //if (settingsProvider == null) return;

            //var taxPeriodValueSettings = settingsProvider.LoadSettings(comboTaxPeriod.Name);
            //var taxPeriodModel = (TaxPeriodModel)_parentPresenter.TaxPeriodModel;

            //if (!string.IsNullOrWhiteSpace(taxPeriodValueSettings)&& taxPeriodModel!=null
            //    && taxPeriodModel.TaxPeriods.Any(item=> item.Id.Equals(taxPeriodValueSettings)))
            //{
            //    comboTaxPeriod.SelectListItemByValue(taxPeriodModel.TaxPeriods.First(item => item.Id.Equals(taxPeriodValueSettings)));
            //}


    }

        private void PersistConfiguration()
        {
        //    var settingsProvider = (ISettingsProvider)_parentPresenter.SettingsProviderPredFilter;
        //    if (settingsProvider == null) return;
            
        //    if (comboTaxPeriod.SelectedIndex > -1)
        //    {
        //        var itemCurrent = (TaxPeriodBase)comboTaxPeriod.SelectedItem.ListObject;
        //        settingsProvider.SaveSettings(itemCurrent.Id, comboTaxPeriod.Name);
        //}
        //    else
        //        settingsProvider.SaveSettings(string.Empty, comboTaxPeriod.Name);

        }

        private void ApplyFilterMode()
        {
            switch (this._filterMode)
            {
                case DeclarationsListFilterMode.DeclarationsList:
                    this.pnl_MyDecl.Visible = false;
                    this.pnl_PZI.Visible = false;
                    this.pnl_FOR.Visible = true;
                    this.pnl_Rashod.Visible = true;
                    this.pnl_Sum.Visible = true;
                    break;
                case DeclarationsListFilterMode.InspectorDecrarationsList:
                    this.pnl_MyDecl.Visible = true;
                    this.pnl_PZI.Visible = true;
                    this.pnl_FOR.Visible = false;
                    this.pnl_Rashod.Visible = false;
                    this.pnl_Sum.Visible = false;
                    break;
                default:
                    break;
            }
        }

        private void ActualizeUiState()
        {
            if (cbx_INN.SelectedItem != null && cbx_INN.SelectedItem.DataValue is ConstraintsEnumeration)
            {
                switch ((ConstraintsEnumeration)cbx_INN.SelectedItem.DataValue)
                {
                    case ConstraintsEnumeration.Contain:
                    {
                        txt_INN.Enabled = true;

                    }
                        break;
                    case ConstraintsEnumeration.IncludeList:
                        {
                            txt_INN.Enabled = true;

                        }
                        break;

                    default:
                        txt_INN.Enabled = false;
                        break;

                }
            }
            if (cbx_KPP.SelectedItem != null && cbx_KPP.SelectedItem.DataValue is ConstraintsEnumeration)
            {
                switch ((ConstraintsEnumeration)cbx_KPP.SelectedItem.DataValue)
                {
                    case ConstraintsEnumeration.Contain:
                        {
                            txt_KPP.Enabled = true;
                        }
                        break;
                    case ConstraintsEnumeration.IncludeList:
                        {
                            txt_KPP.Enabled = true;
                        }
                        break;

                    default:
                        txt_KPP.Enabled = false;
                        break;
                }
            }
            if (cbx_SEOD_DECL_ID.SelectedItem != null && cbx_SEOD_DECL_ID.SelectedItem.DataValue is ConstraintsEnumeration)
            {
                switch ((ConstraintsEnumeration)cbx_SEOD_DECL_ID.SelectedItem.DataValue)
                {
                    case ConstraintsEnumeration.Contain:
                        {
                            txt_SEOD_DECL_ID.Enabled = true;
                        }
                        break;
                    case ConstraintsEnumeration.IncludeList:
                        {
                            txt_SEOD_DECL_ID.Enabled = true;
                        }
                        break;

                    default:
                        txt_SEOD_DECL_ID.Enabled = false;
                        break;
                }
            }

            comboTaxPeriod.Enabled = cbx_FISCAL_YEAR.Enabled = !cbAllTaxPeriod.Checked;

            if (cbx_DECL_TYPE_CODE.SelectedItem != null && cbx_DECL_TYPE_CODE.SelectedItem.DataValue is DocType)
            {
                switch ((DocType)cbx_DECL_TYPE_CODE.SelectedItem.DataValue)
                {
                    case DocType.All:
                    case DocType.Declaration:
                        {
                            //this.cbx_SUR_CODE_1.Checked = false;
                            this.cbx_SUR_CODE_1.Enabled = true;
                            //this.cbx_SUR_CODE_2.Checked = false;
                            this.cbx_SUR_CODE_2.Enabled = true;
                            //this.cbx_SUR_CODE_3.Checked = false;
                            this.cbx_SUR_CODE_3.Enabled = true;
                            //this.cbx_SUR_CODE_4.Checked = false;
                            this.cbx_SUR_CODE_4.Enabled = true;

                            this.cbx_DECL_SIGN.Enabled = true;

                            this.cbxSortFieldName.Items.Clear();
                            this.cbxSortFieldName.Items.AddRange(UiRoutines.EnumToValueListItems<SortFieldNameDecl>());
                        }
                        break;
                    case DocType.Magazine:
                        {
                            //this.cbx_SUR_CODE_1.Checked = false;
                            this.cbx_SUR_CODE_1.Enabled = false;
                            //this.cbx_SUR_CODE_2.Checked = false;
                            this.cbx_SUR_CODE_2.Enabled = false;
                            //this.cbx_SUR_CODE_3.Checked = false;
                            this.cbx_SUR_CODE_3.Enabled = false;
                            //this.cbx_SUR_CODE_4.Checked = false;
                            this.cbx_SUR_CODE_4.Enabled = false;

                            this.cbx_DECL_SIGN.Enabled = false;

                            this.cbxSortFieldName.Items.Clear();
                            this.cbxSortFieldName.Items.AddRange(UiRoutines.EnumToValueListItems<SortFieldNameMagazine>());
                        }
                        break;

                    default:
                        break;
                }
            }


        }



        #region Region Inspections Districts


        private void GetDistrictList()
        {
            ExecuteServiceCall(
                () => GetServiceProxy<IDataService>().GetFederalDistricts(), x => _federalDistrictList = x.Result);
            _federalDistrictList =
                _federalDistrictList.OrderBy(TypeHelper<FederalDistrict>.GetMemberName(x => x.FullName), false).ToList();

        }

        private void GetRegionList()
        {

            ExecuteServiceCall(
                () => GetServiceProxy<ISelectionService>().GetRegions(), x => _regionList = x.Result);
            _regionList = _regionList.OrderBy(TypeHelper<Region>.GetMemberName(x => x.Code), false).ToList();

            _regionList.ForAll(region =>
                region.Inspections =
                    region.Inspections.OrderBy(TypeHelper<Inspection>.GetMemberName(x => x.Code), false).ToList());
        }

        private void UpdateDistinctList()
        {
            _frezeUi = true;
            if (_federalDistrictList == null)
            {
                GetDistrictList();
                cbDistrict.DataSource = _federalDistrictList;
            }
            cbDistrict.Update();

            _frezeUi = false;

            if (cbDistrict.SelectedItem != null)
            {
                UpdateRegionList();
            }


        }

        private void cbDistrict_ValueChanged(object sender, EventArgs e)
        {
            if (_frezeUi) return;
            UpdateRegionList();

        }

        private void UpdateRegionList()
        {
            _frezeUi = true;
            if (_regionList == null) GetRegionList();

            if (cbDistrict.SelectedItem != null)
            {
                var district = (FederalDistrict)cbDistrict.SelectedItem.ListObject;

                var regions = (from region in _regionList where district.RegionCodes.Contains(region.Code) select region).ToList();

                cbRegion.DataSource = regions;
            }
            else
            {
                cbRegion.DataSource = _regionList;
            }

            cbRegion.Update();

            _frezeUi = false;
            UpdateInpectionList();


        }

        private void cbRegion_ValueChanged(object sender, EventArgs e)
        {
            if (_frezeUi) return;
            UpdateInpectionList();


        }

        private void UpdateInpectionList()
        {
            _frezeUi = true;

            if (cbRegion.SelectedItem != null)
            {
                var region = (Region)cbRegion.SelectedItem.ListObject;
                cbInspection.DataSource = region.Inspections;

            }
            else if (cbDistrict.SelectedItem != null)
            {
                var district = (FederalDistrict)cbDistrict.SelectedItem.ListObject;
                var inspections = new List<Inspection>();

                (from region in _regionList where district.RegionCodes.Contains(region.Code) select region).ForAll(
                    region => inspections.AddRange(region.Inspections));

                cbInspection.DataSource = inspections;
            }
            else
            {
                var inspections = new List<Inspection>();
                _regionList.ForAll(region => inspections.AddRange(region.Inspections));
                cbInspection.DataSource = inspections;
            }


            cbInspection.Update();

            _frezeUi = false;

        }

        private void cbInspection_ValueChanged(object sender, EventArgs e)
        {
            if (_frezeUi) return;

        }


        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемой сущности(ссылочный)</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <returns>признак успешности исполнения</returns>
        public bool ExecuteServiceCall<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess = null,
            Action<TResult> doOnError = null) where TResult : IOperationResult
        {
          //  Contract.Requires(callDelegate != null);

            bool ret = false;

            TResult result = callDelegate();

            bool isRealyOk = true;
                isRealyOk = CheckOperationResult(result, doOnError == null);

            if (isRealyOk)
            {
                if (doOnSuccess != null)
                    doOnSuccess(result);

                ret = true;
            }
            else
            {
                if (doOnError != null)
                    doOnError(result);
            }

            return ret;
        }


        protected T GetServiceProxy<T>(string context = null) where T : class
        {
         
            return _serviceCollection.Get<IClientContextService>()
                .CreateCommunicationProxy<T>(string.IsNullOrEmpty(context) ? Constants.EndpointName : context);
        }


        private bool CheckOperationResult(IOperationResult result, bool isNotQuite)
        {
            bool ret = true;

            if (result == null)
            {
                _parentPresenter.LogError("Сервер вернул пустой результат", "ExecuteServiceCall");
                if (isNotQuite) _parentPresenter.View.ShowError("Сервер вернул пустой результат");
                ret = false;
            }
            else if (result.Status == ResultStatus.Error)
            {
                _parentPresenter.LogError("Ошибка выполнения на сервере", "ExecuteServiceCall", new Exception(result.Message));
                if (isNotQuite) _parentPresenter.View.ShowError(result.Message);
                ret = false;
            }
            else if (result.Status == ResultStatus.Denied)
            {
                _parentPresenter.LogError("Отказано в доступе", "ExecuteServiceCall", new Exception(result.Message));
                if (isNotQuite) _parentPresenter.View.ShowError(result.Message);
                ret = false;
            }
            else if (result.Status == ResultStatus.NoDataFound)
            {
                _parentPresenter.LogError("Данные не найдены", "ExecuteServiceCall", new Exception(result.Message));
                ret = false;
            }

            return ret;
        }

        #endregion


//        public QueryConditions QueryConditions => BuildQueryConditions();
        public QueryConditions QueryConditions { get { return BuildQueryConditions(); } }


        private QueryConditions BuildQueryConditions()
        {
            PersistConfiguration();

            var wi = WindowsIdentity.GetCurrent();
            var userSid = wi != null ? wi.User.Value : null;

            var conditions = new QueryConditions();

            if (_filterMode == DeclarationsListFilterMode.InspectorDecrarationsList)
            {
                if (rbMyDeclarations.CheckedIndex == 0)
                {
                    if (!string.IsNullOrEmpty(userSid))
                        conditions.Filter.AndF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.INSPECTOR_SID),
                            ColumnFilter.FilterComparisionOperator.Equals, userSid);
                }
                if (rbMyDeclarations.CheckedIndex == 1)
                {
                    conditions.Filter.AndF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.INSPECTOR_SID),
                        ColumnFilter.FilterComparisionOperator.Equals, null);
                }

                #region INN

                if (cbx_INN.SelectedItem != null && cbx_INN.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_INN.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.AndF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.INN),
                                ColumnFilter.FilterComparisionOperator.Contains, txt_INN.Value);
                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {
                            var target = txt_INN.Value.ToString().Split(',');
                            if (target.Any())
                                conditions.Filter.OrF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.INN),
                                    from val in target
                                    select new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = val
                                    });

                        }
                            break;

                    }
                }

                #endregion

                #region KPP

                if (cbx_KPP.SelectedItem != null && cbx_KPP.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_KPP.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.KPP),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = txt_KPP.Value
                                            }
                                        }
                                });

                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {

                            var target = txt_KPP.Value.ToString().Split(',');
                            if (target.Any())
                            {
                                conditions.Filter.Add(
                                    new FilterQuery
                                    {
                                        ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.KPP),
                                        FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                                        Filtering =
                                            (from val in target
                                                select
                                                    new ColumnFilter
                                                    {
                                                        ComparisonOperator =
                                                            ColumnFilter.FilterComparisionOperator.Equals,
                                                        Value = val
                                                    }).ToList()

                                    });
                            }
                        }
                            break;

                    }
                }

                #endregion

                #region SEOD_DECL_ID

                if (cbx_SEOD_DECL_ID.SelectedItem != null &&
                    cbx_SEOD_DECL_ID.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_SEOD_DECL_ID.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SEOD_DECL_ID),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = txt_SEOD_DECL_ID.Value
                                            }
                                        }
                                });

                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {

                            var target = txt_SEOD_DECL_ID.Value.ToString().Split(',');
                            if (target.Any())
                            {
                                conditions.Filter.Add(
                                    new FilterQuery
                                    {
                                        ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SEOD_DECL_ID),
                                        FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                                        Filtering =
                                            (from val in target
                                                select
                                                    new ColumnFilter
                                                    {
                                                        ComparisonOperator =
                                                            ColumnFilter.FilterComparisionOperator.Equals,
                                                        Value = val
                                                    }).ToList()

                                    });
                            }
                        }
                            break;

                    }
                }

                #endregion

                #region DECL_TYPE_CODE

                if (cbx_DECL_TYPE_CODE.SelectedItem != null
                    && cbx_DECL_TYPE_CODE.SelectedItem.DataValue is DocType
                    && (DocType) cbx_DECL_TYPE_CODE.SelectedItem.DataValue != DocType.All)
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_TYPE_CODE),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = (int) (DocType) cbx_DECL_TYPE_CODE.SelectedItem.DataValue
                                    }
                                }
                        });
                }

                #endregion

                #region STATUS

                if (cbx_STATUS.SelectedItem != null
                    && cbx_STATUS.SelectedItem.DataValue is KnpState
                    && (KnpState) cbx_STATUS.SelectedItem.DataValue != KnpState.All)
                {
                    switch ((KnpState) cbx_STATUS.SelectedItem.DataValue)
                    {
                        case KnpState.All:
                            break;
                        case KnpState.IsFucUp:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.STATUS),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                                Value = "Открыта" //(int)(KnpState)cbx_STATUS.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case KnpState.IsShutUp:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.STATUS),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                                Value = "Закрыта" //(int)(KnpState)cbx_STATUS.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        default:
                            break;
                    }
                }

                #endregion

                //#region CATEGORY

                //if (cbx_CATEGORY.SelectedItem != null
                //    && cbx_CATEGORY.SelectedItem.DataValue is LpImportance
                //    && (LpImportance)cbx_CATEGORY.SelectedItem.DataValue != LpImportance.All)
                //{
                //    conditions.Filter.Add(
                //        new FilterQuery
                //        {
                //            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.ca),
                //            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                //            Filtering =
                //                new List<ColumnFilter>
                //                {
                //                    new ColumnFilter
                //                    {
                //                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                //                        Value = (int) (LpImportance) cbx_CATEGORY.SelectedItem.DataValue
                //                    }
                //                }
                //        });
                //}

                //#endregion

                #region NAME

                if (txt_NAME.TextLength > 0)
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.NAME),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                        Value = txt_NAME.Value
                                    }
                                }
                        });
                }

                #endregion

                #region Отчетный период| Налоговый год

                if (!cbAllTaxPeriod.Checked)
                {

                    if (comboTaxPeriod.SelectedItem != null)
                    {

                        var period = (TaxPeriodEnum) comboTaxPeriod.SelectedItem.DataValue;

                        string[] periodCodeList = null;

                        switch (period)
                        {
                            case TaxPeriodEnum.First:
                                periodCodeList = _taxPeriodCode1.Split('/');
                                break;
                            case TaxPeriodEnum.Second:
                                periodCodeList = _taxPeriodCode2.Split('/');
                                break;
                            case TaxPeriodEnum.Third:
                                periodCodeList = _taxPeriodCode3.Split('/');
                                break;
                            case TaxPeriodEnum.Fourth:
                                periodCodeList = _taxPeriodCode4.Split('/');
                                break;
                        }

                        if (periodCodeList != null && periodCodeList.Any())

                            conditions.Filter.OrF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.TAX_PERIOD),
                                from val in periodCodeList
                                select new ColumnFilter
                                {
                                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                    Value = val
                                });
                    }

                    if (cbx_FISCAL_YEAR.SelectedItem != null)
                    {
                        var year = (int) cbx_FISCAL_YEAR.SelectedItem.DataValue;
                        if (year > 1950 && year < 2050)
                            conditions.Filter.AndF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.FISCAL_YEAR),
                                ColumnFilter.FilterComparisionOperator.Equals, year.ToString());
                    }

                }

                #endregion

                #region SUR_CODE

                if (cbx_SUR_CODE_1.Checked
                    || cbx_SUR_CODE_2.Checked
                    || cbx_SUR_CODE_3.Checked
                    || cbx_SUR_CODE_4.Checked)
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (cbx_SUR_CODE_1.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 1
                        });
                    if (cbx_SUR_CODE_2.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 2
                        });
                    if (cbx_SUR_CODE_3.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 3
                        });
                    if (cbx_SUR_CODE_4.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 4
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SUR_CODE),
                            FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                            Filtering = l
                        });
                }

                #endregion

                #region Region Inspection District

                if (cbInspection.SelectedItem != null)
                {
                    var inspection = (Inspection) cbInspection.SelectedItem.ListObject;
                    conditions.Filter.AndF(TypeHelper<DeclarationBrief>.GetMemberName(x => x.SOUN_CODE),
                        ColumnFilter.FilterComparisionOperator.Equals, inspection.Code);
                }
                //else if (cbRegion.SelectedItem != null)
                //{
                //    var region = (Region)cbRegion.SelectedItem.ListObject;
                //    conditions.Filter.AndF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.REGION_NAME),
                //        ColumnFilter.FilterComparisionOperator.Equals, region.RegionFullName);
                //}
                //else if (cbDistrict.SelectedItem != null)
                //{
                //    var district = (FederalDistrict)cbDistrict.SelectedItem.ListObject;

                //}

                #endregion

                #region CORRECTION_NUMBER

                if (!string.IsNullOrWhiteSpace(txt_CORRECTION_NUMBER.Text))
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.CORRECTION_NUMBER),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = txt_CORRECTION_NUMBER.Value
                                    }
                                }
                        });
                }

                #endregion

                #region DECL_SIGN

                if (cbx_DECL_SIGN.SelectedItem != null
                    && cbx_DECL_SIGN.SelectedItem.DataValue is NdSymptom
                    && (NdSymptom) cbx_DECL_SIGN.SelectedItem.DataValue != NdSymptom.All)
                {
                    switch ((NdSymptom) cbx_DECL_SIGN.SelectedItem.DataValue)
                    {
                        case NdSymptom.All:
                            break;
                        case NdSymptom.ToPay:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "К уплате"
                                                //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case NdSymptom.ToRevenge:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "К возмещению"
                                                //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case NdSymptom.GoBy:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "Нулевая" //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        default:
                            break;
                    }
                }

                #endregion

                #region SUBSCRIBER_NAME

                if (!string.IsNullOrWhiteSpace(txt_SUBSCRIBER_NAME.Text))
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SUBSCRIBER_NAME),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                        Value = txt_SUBSCRIBER_NAME.Value
                                    }
                                }
                        });
                }

                #endregion

                #region COMPENSATION_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_COMPENSATION_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_COMPENSATION_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.COMPENSATION_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region NDS_WEIGHT

                if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_NDS_WEIGHT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_NDS_WEIGHT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.NDS_WEIGHT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion



            }
            else
            {



                #region INN

                if (cbx_INN.SelectedItem != null && cbx_INN.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_INN.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.AndF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.INN),
                                ColumnFilter.FilterComparisionOperator.Contains, txt_INN.Value);
                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {
                            var target = txt_INN.Value.ToString().Split(',');
                            if (target.Any())
                                conditions.Filter.OrF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.INN),
                                    from val in target
                                    select new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = val
                                    });

                        }
                            break;

                    }
                }

                #endregion

                #region KPP

                if (cbx_KPP.SelectedItem != null && cbx_KPP.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_KPP.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.KPP),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = txt_KPP.Value
                                            }
                                        }
                                });

                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {

                            var target = txt_KPP.Value.ToString().Split(',');
                            if (target.Any())
                            {
                                conditions.Filter.Add(
                                    new FilterQuery
                                    {
                                        ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.KPP),
                                        FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                                        Filtering =
                                            (from val in target
                                                select
                                                    new ColumnFilter
                                                    {
                                                        ComparisonOperator =
                                                            ColumnFilter.FilterComparisionOperator.Equals,
                                                        Value = val
                                                    }).ToList()

                                    });
                            }
                        }
                            break;

                    }
                }

                #endregion

                #region SEOD_DECL_ID

                if (cbx_SEOD_DECL_ID.SelectedItem != null &&
                    cbx_SEOD_DECL_ID.SelectedItem.DataValue is ConstraintsEnumeration)
                {
                    switch ((ConstraintsEnumeration) cbx_SEOD_DECL_ID.SelectedItem.DataValue)
                    {
                        case ConstraintsEnumeration.Contain:
                        {
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SEOD_DECL_ID),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = txt_SEOD_DECL_ID.Value
                                            }
                                        }
                                });

                        }
                            break;
                        case ConstraintsEnumeration.IncludeList:
                        {

                            var target = txt_SEOD_DECL_ID.Value.ToString().Split(',');
                            if (target.Any())
                            {
                                conditions.Filter.Add(
                                    new FilterQuery
                                    {
                                        ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SEOD_DECL_ID),
                                        FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                                        Filtering =
                                            (from val in target
                                                select
                                                    new ColumnFilter
                                                    {
                                                        ComparisonOperator =
                                                            ColumnFilter.FilterComparisionOperator.Equals,
                                                        Value = val
                                                    }).ToList()

                                    });
                            }
                        }
                            break;

                    }
                }

                #endregion

                #region DECL_TYPE_CODE

                if (cbx_DECL_TYPE_CODE.SelectedItem != null
                    && cbx_DECL_TYPE_CODE.SelectedItem.DataValue is DocType
                    && (DocType) cbx_DECL_TYPE_CODE.SelectedItem.DataValue != DocType.All)
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DECL_TYPE_CODE),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = (int) (DocType) cbx_DECL_TYPE_CODE.SelectedItem.DataValue
                                    }
                                }
                        });
                }

                #endregion

                #region STATUS

                if (cbx_STATUS.SelectedItem != null
                    && cbx_STATUS.SelectedItem.DataValue is KnpState
                    && (KnpState) cbx_STATUS.SelectedItem.DataValue != KnpState.All)
                {
                    switch ((KnpState) cbx_STATUS.SelectedItem.DataValue)
                    {
                        case KnpState.All:
                            break;
                        case KnpState.IsFucUp:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.STATUS),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                                Value = "Открыта" //(int)(KnpState)cbx_STATUS.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case KnpState.IsShutUp:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.STATUS),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                                Value = "Закрыта" //(int)(KnpState)cbx_STATUS.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        default:
                            break;
                    }
                }

                #endregion

                #region CATEGORY

                if (cbx_CATEGORY.SelectedItem != null
                    && cbx_CATEGORY.SelectedItem.DataValue is LpImportance
                    && (LpImportance) cbx_CATEGORY.SelectedItem.DataValue != LpImportance.All)
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.CATEGORY),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = (int) (LpImportance) cbx_CATEGORY.SelectedItem.DataValue
                                    }
                                }
                        });
                }

                #endregion

                #region NAME

                if (txt_NAME.TextLength > 0)
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.NAME),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                        Value = txt_NAME.Value
                                    }
                                }
                        });
                }

                #endregion

                #region Отчетный период| Налоговый год

                if (!cbAllTaxPeriod.Checked)
                {

                    if (comboTaxPeriod.SelectedItem != null)
                    {

                        var period = (TaxPeriodEnum) comboTaxPeriod.SelectedItem.DataValue;

                        string[] periodCodeList = null;

                        switch (period)
                        {
                            case TaxPeriodEnum.First:
                                periodCodeList = _taxPeriodCode1.Split('/');
                                break;
                            case TaxPeriodEnum.Second:
                                periodCodeList = _taxPeriodCode2.Split('/');
                                break;
                            case TaxPeriodEnum.Third:
                                periodCodeList = _taxPeriodCode3.Split('/');
                                break;
                            case TaxPeriodEnum.Fourth:
                                periodCodeList = _taxPeriodCode4.Split('/');
                                break;
                        }

                        if (periodCodeList != null && periodCodeList.Any())

                            conditions.Filter.OrF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.TAX_PERIOD),
                                from val in periodCodeList
                                select new ColumnFilter
                                {
                                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                    Value = val
                                });
                    }

                    if (cbx_FISCAL_YEAR.SelectedItem != null)
                    {
                        var year = (int) cbx_FISCAL_YEAR.SelectedItem.DataValue;
                        if (year > 1950 && year < 2050)
                            conditions.Filter.AndF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.FISCAL_YEAR),
                                ColumnFilter.FilterComparisionOperator.Equals, year.ToString());
                    }

                }

                #endregion

                #region SUR_CODE

                if (cbx_SUR_CODE_1.Enabled &&
                    (cbx_SUR_CODE_1.Checked
                     || cbx_SUR_CODE_2.Checked
                     || cbx_SUR_CODE_3.Checked
                     || cbx_SUR_CODE_4.Checked))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (cbx_SUR_CODE_1.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 1
                        });
                    if (cbx_SUR_CODE_2.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 2
                        });
                    if (cbx_SUR_CODE_3.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 3
                        });
                    if (cbx_SUR_CODE_4.Checked)
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 4
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SUR_CODE),
                            FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                            Filtering = l
                        });
                }

                #endregion

                #region Region Inspection District

                if (cbInspection.SelectedItem != null)
                {
                    var inspection = (Inspection) cbInspection.SelectedItem.ListObject;
                    conditions.Filter.AndF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.SOUN),
                        ColumnFilter.FilterComparisionOperator.Equals, inspection.Code);
                }
                else if (cbRegion.SelectedItem != null)
                {
                    var region = (Region) cbRegion.SelectedItem.ListObject;
                    conditions.Filter.AndF(TypeHelper<DeclarationSummary>.GetMemberName(x => x.REGION_NAME),
                        ColumnFilter.FilterComparisionOperator.Equals, region.RegionFullName);
                }
                else if (cbDistrict.SelectedItem != null)
                {
                    var district = (FederalDistrict) cbDistrict.SelectedItem.ListObject;

                }

                #endregion

                #region CORRECTION_NUMBER

                if (!string.IsNullOrWhiteSpace(txt_CORRECTION_NUMBER.Text))
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.CORRECTION_NUMBER),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                        Value = txt_CORRECTION_NUMBER.Value
                                    }
                                }
                        });
                }

                #endregion

                #region DECL_SIGN

                if (cbx_DECL_SIGN.SelectedItem != null
                    && cbx_DECL_SIGN.SelectedItem.DataValue is NdSymptom
                    && (NdSymptom) cbx_DECL_SIGN.SelectedItem.DataValue != NdSymptom.All)
                {
                    switch ((NdSymptom) cbx_DECL_SIGN.SelectedItem.DataValue)
                    {
                        case NdSymptom.All:
                            break;
                        case NdSymptom.ToPay:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "К уплате"
                                                //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case NdSymptom.ToRevenge:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "К возмещению"
                                                //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        case NdSymptom.GoBy:
                            conditions.Filter.Add(
                                new FilterQuery
                                {
                                    ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DECL_SIGN),
                                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                                    Filtering =
                                        new List<ColumnFilter>
                                        {
                                            new ColumnFilter
                                            {
                                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                                Value = "Нулевая" //(int)(NdSymptom)cbx_DECL_SIGN.SelectedItem.DataValue
                                            }
                                        }
                                });
                            break;
                        default:
                            break;
                    }
                }

                #endregion

                #region SUBSCRIBER_NAME

                if (!string.IsNullOrWhiteSpace(txt_SUBSCRIBER_NAME.Text))
                {
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SUBSCRIBER_NAME),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering =
                                new List<ColumnFilter>
                                {
                                    new ColumnFilter
                                    {
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains,
                                        Value = txt_SUBSCRIBER_NAME.Value
                                    }
                                }
                        });
                }

                #endregion

                #region COMPENSATION_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_COMPENSATION_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_COMPENSATION_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_COMPENSATION_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.COMPENSATION_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region TOTAL_DISCREP_COUNT

                if (!string.IsNullOrWhiteSpace(this.txt_TOTAL_DISCREP_COUNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_TOTAL_DISCREP_COUNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_TOTAL_DISCREP_COUNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_TOTAL_DISCREP_COUNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_TOTAL_DISCREP_COUNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_TOTAL_DISCREP_COUNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.TOTAL_DISCREP_COUNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region CONTROL_RATIO_DISCREP_COUNT

                if (!string.IsNullOrWhiteSpace(this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_CONTROL_RATIO_DISCREP_COUNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_CONTROL_RATIO_DISCREP_COUNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.CONTROL_RATIO_DISCREP_COUNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region NDS_WEIGHT

                if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_NDS_WEIGHT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_NDS_WEIGHT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_NDS_WEIGHT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.NDS_WEIGHT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region DISCREP_TOTAL_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_TOTAL_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_DISCREP_TOTAL_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_TOTAL_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_DISCREP_TOTAL_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_TOTAL_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_DISCREP_TOTAL_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DISCREP_TOTAL_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region DISCREP_MIN_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MIN_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_DISCREP_MIN_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MIN_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_DISCREP_MIN_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MIN_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_DISCREP_MIN_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DISCREP_MIN_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region DISCREP_MAX_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MAX_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_DISCREP_MAX_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MAX_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_DISCREP_MAX_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_MAX_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_DISCREP_MAX_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DISCREP_MAX_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region DISCREP_BUY_BOOK_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_BUY_BOOK_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_DISCREP_BUY_BOOK_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_BUY_BOOK_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_DISCREP_BUY_BOOK_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_BUY_BOOK_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_DISCREP_BUY_BOOK_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DISCREP_BUY_BOOK_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion

                #region DISCREP_SELL_BOOK_AMNT

                if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_SELL_BOOK_AMNT_min.Text)
                    || !string.IsNullOrWhiteSpace(this.txt_DISCREP_SELL_BOOK_AMNT_max.Text))
                {
                    List<ColumnFilter> l = new List<ColumnFilter>();
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_SELL_BOOK_AMNT_min.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = this.txt_DISCREP_SELL_BOOK_AMNT_min.Text
                        });
                    if (!string.IsNullOrWhiteSpace(this.txt_DISCREP_SELL_BOOK_AMNT_max.Text))
                        l.Add(new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = this.txt_DISCREP_SELL_BOOK_AMNT_max.Text
                        });
                    conditions.Filter.Add(
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.DISCREP_SELL_BOOK_AMNT),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = l
                        });
                }

                #endregion
            }
            ColumnSort columnSort = null;
            if (cbxSortFieldName.SelectedItem != null)
            {
                if (cbxSortFieldName.SelectedItem.DataValue is SortFieldNameMagazine)
                {
                    columnSort = new ColumnSort();
                    columnSort.ColumnKey = ((SortFieldNameMagazine) cbxSortFieldName.SelectedItem.DataValue).ToString();
                }
                if (cbxSortFieldName.SelectedItem.DataValue is SortFieldNameDecl)
                {
                    columnSort = new ColumnSort();
                    columnSort.ColumnKey = ((SortFieldNameDecl) cbxSortFieldName.SelectedItem.DataValue).ToString();
                }
            }
            if (cbxSortFieldOrder.SelectedItem != null && cbxSortFieldOrder.SelectedItem.DataValue is SortDirection)
            {
                if (columnSort == null)
                    columnSort = new ColumnSort();
                columnSort.Order = (ColumnSort.SortOrder) (int) (SortDirection) cbxSortFieldOrder.SelectedItem.DataValue;

            }
            if (columnSort != null)
                conditions.Sorting.Add(columnSort);

            if (cbxRowCount.SelectedItem != null && cbxRowCount.SelectedItem.DataValue is RowCountConstraint)
            {
                var rcc = (RowCountConstraint) cbxRowCount.SelectedItem.DataValue;
                switch (rcc)
                {
                    case RowCountConstraint.TwoThousands:
                        conditions.PaginationDetails.RowsToTake = 2000;
                        break;
                    case RowCountConstraint.Over2000:
                        conditions.PaginationDetails.RowsToTake = null;
                        break;
                    default:
                        conditions.PaginationDetails.RowsToTake = 1000;
                        break;
                }
            }



            return conditions;
        }

        private void ctrl_ValueChanged(object sender, EventArgs e)
        {
            if (_frezeUi) return;
            ActualizeUiState();
        }



        public bool IsFilterChanged { get; set; }




        public enum DeclarationsListFilterMode
        {
            DeclarationsList,
            InspectorDecrarationsList
        }

        public enum ConstraintsEnumeration
        {
            [Description("Все")]
            All = 0,
            [Description("Содержит")]
            Contain = 1,
            [Description("Из перечня")]
            IncludeList = 2
        }

        public enum DocType
        {
            [Description("Все")]
            All = -1,
            [Description("НД по НДС")]
            Declaration = 0,
            [Description("Журнал учета")]
            Magazine = 1,
        }

        public enum KnpState
        {
            [Description("Все")]
            All = 0,
            [Description("КНП открыта")]
            IsFucUp = 1,
            [Description("КНП закрыта")]
            IsShutUp = 2,


        }

        public enum LpImportance
        {
            [Description("Все")]
            All = -1,
            [Description("Крупнейший")]
            Foremost = 1,
            [Description("Не крупнейший")]
            Looser = 0,
        }

        /// <summary>
        /// Признак НД
        /// </summary>
        public enum NdSymptom
        {
            [Description("Все")]
            All = 0,
            [Description("К уплате")]
            ToPay = 1,
            [Description("К возмещению")]
            ToRevenge = 2,
            [Description("Нулевая")]
            GoBy = 3,
        }


        /// <summary>
        /// Признак значимых изменений
        /// </summary>
        public enum SeriousChangeToken
        {
            [Description("Все")]
            All = 0,
            [Description("И(есть несколько изменений)")]
            And = 1,
            [Description("Н1(новое расхождение для КНП)")]
            N1 = 2,
            [Description("АТ1(направлено автотребование)")]
            At1 = 3,
            [Description("АТ2(получено пояснение по автотребованию, расхождение не устранено)")]
            At2 = 4,
            [Description("АТ3(пояснение по автотребованию не получено, расхождение не устранено)")]
            At3 = 5,
            [Description("АТ4(получена уточненная НД после отправки автотребования, расхождение не устранено)")]
            At4 = 6,
            [Description("АИ1(создано автоистребование)")]
            Ai1 = 7,
            [Description("Н2(направлено автоистребование)")]
            N2 = 8,
            [Description("АИ2(получен ответ по автоистребованию, расхождение не устранено)")]
            Ai2 = 9,
            //[Description("АТ3(ответ по автоистребованию не получен)")]
            //At3 = 0,
            [Description("Н3(прочие МНК, расхождение не устранено)")]
            N3 = 11,
            [Description("У(расхождения для КНП устранено)")]
            U = 12,
        //    [Description("И если вы долистали сюда... - вам подарок. };>")] Iddqd=13
            

        }

        /// <summary>
        /// Отчетный период
        /// </summary>
        public enum TaxPeriodEnum
        {
            [Description("Все")]
            All = 0,
            [Description("1 кв")]
            First = 1,
            [Description("2 кв")]
            Second = 2,
            [Description("3 кв")]
            Third = 3,
            [Description("4 кв")]
            Fourth = 4,

        }


        /// <summary>
        /// Кол-во записей в списке
        /// </summary>
        public enum RowCountConstraint
        {
            [Description("1000")]
            OneThousand = 0,
            [Description("2000")]
            TwoThousands = 1,
            [Description(">2000")]
            Over2000 = 2,
        }

        /// <summary>
        /// направление сортировки
        /// </summary>
        public enum SortDirection
        {
            [Description("убыванию")]
            Decrease = 2,
            [Description("возрастанию")]
            Increase = 1
        }
        /// <summary>
        /// Сортировка По для НД
        /// </summary>
        public enum SortFieldNameDecl
        {
            [Description("сумма НДС (к уплате / к вомещению)")]
            COMPENSATION_AMNT = -1,
            [Description("удельный вес")]
            NDS_WEIGHT = 2,
            [Description("кол-во расхождений по СФ ")]
            TOTAL_DISCREP_COUNT = 3,
            [Description("кол-во расхождений по КС")]
            CONTROL_RATIO_DISCREP_COUNT = 4,
            [Description("сумма расхождений")]
            DISCREP_TOTAL_AMNT = 5,
            [Description("сумма расхождений по книгам покупок (раздел 8)")]
            DISCREP_BUY_BOOK_AMNT = 6,
            [Description("сумма расхождений по книгам продаж (раздел 9 и 12)")]
            DISCREP_SELL_BOOK_AMNT = 7,
            [Description("дата представления в НО")]
            DECL_DATE = 8

        }

        /// <summary>
        /// направление сортировки
        /// </summary>
        public enum SortFieldNameMagazine
        {
            [Description("кол-во расхождений по СФ ")]
            TOTAL_DISCREP_COUNT = 0,
            [Description("сумма расхождений")]
            DISCREP_TOTAL_AMNT = 1,
            [Description("дата представления в НО")]
            DECL_DATE = -1

        }

    }


    public static class UiRoutines
    {
        public static ValueListItem[] EnumToValueListItems<TEnum>()
        {
            var enumType = typeof(TEnum);
            if (!enumType.IsEnum)
                return null;

            var res = (from enumItem in Enum.GetValues(enumType).Cast<TEnum>()
                       orderby enumItem
                       select
                    new ValueListItem(enumItem, GetEnumItemDescription<TEnum>(enumItem))).ToArray();
            return res;
        }

        public static string GetEnumItemDescription<TEnum>(object enumItem)
        {
            var enumType = typeof(TEnum);
            if (enumType.IsEnum)
            {
                var enumItemName = Enum.GetName(enumType, enumItem);
                if (!string.IsNullOrEmpty(enumItemName))
                {
                    var field = enumType.GetField(enumItemName);
                    if (field != null)
                    {
                        var descrAttr = (DescriptionAttribute)
                            Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute));

                        if (descrAttr != null) return descrAttr.Description;
                    }
                }
            }
            return string.Empty;

        }

        public static void SelectListItemByValue(this UltraComboEditor dropDown, object value)
        {
            if (dropDown == null) return;
            if (value == null) return;

            foreach (var listItem in dropDown.Items)
            {
                if (listItem.DataValue.Equals(value))
                {
                    dropDown.SelectedItem = listItem;
                    return;
                }
            }
        }

        public static List<FilterQuery> AndF(this List<FilterQuery> queryList, string columnName,
            ColumnFilter.FilterComparisionOperator comparisionOperator, object value)
        {

            queryList.Add(new FilterQuery
            {
                ColumnName = columnName,
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering =
                            new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                    ComparisonOperator = comparisionOperator,
                                    Value = value
                                }
                            }
            });
            return queryList;
        }
        public static List<FilterQuery> OrF(this List<FilterQuery> queryList, string columnName,
            ColumnFilter.FilterComparisionOperator comparisionOperator, object value)
        {

            queryList.Add(new FilterQuery
            {
                ColumnName = columnName,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                Filtering =
                            new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                    ComparisonOperator = comparisionOperator,
                                    Value = value
                                }
                            }
            });
            return queryList;
        }

        public static List<FilterQuery> OrF(this List<FilterQuery> queryList, string columnName, IEnumerable<ColumnFilter> subConditions)
        {

            queryList.Add(new FilterQuery
            {
                ColumnName = columnName,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                Filtering = new List<ColumnFilter>(subConditions)
            });
            return queryList;
        }


    }

}
