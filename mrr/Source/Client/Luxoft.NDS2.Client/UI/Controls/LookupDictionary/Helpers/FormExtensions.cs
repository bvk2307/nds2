﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Helpers
{
    public static class FormExtensions
    {
        /// <summary>выполнение в потоке UI
        public static void SafeAction<TControl>(this TControl control, Action<TControl> action) where TControl : Control
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke(action, control);
            }
            else
            {
                action(control);
            }
        }

        public static TValue SafeReadValue<TControl, TValue>(this TControl control, Func<TControl, TValue> action) where TControl : Control
        {
            if (control.InvokeRequired)
            {
                return (TValue)control.Invoke(action, control);
            }

            return action(control);
        }
    }
}
