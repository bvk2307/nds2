﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Helpers
{
    public interface IProgressIndicatorSupportable
    {
        Control SurfaceControl { get; }
        Control ControlToDisable { get; }
    }

    public class ControlLoadingProgressHelper
    {
        private IProgressIndicatorSupportable _target;
        private PictureBox _progressIndicator;

        private Func<Control.ControlCollection> GetControlsCollection { get; set; }

        public ControlLoadingProgressHelper(IProgressIndicatorSupportable target, Func<Control.ControlCollection> getControlsCollection = null)
        {
            _target = target;
            GetControlsCollection = getControlsCollection;
            InitLoadingBox();
        }

        private void InitLoadingBox()
        {
            var controlsConteiner = GetControlsCollection == null ? _target.SurfaceControl.Controls : GetControlsCollection();
            _progressIndicator = new PictureBox();
            _progressIndicator.Image = Luxoft.NDS2.Client.Properties.Resources.progressIndicator;

            controlsConteiner.Add(_progressIndicator);
            _progressIndicator.SizeMode = PictureBoxSizeMode.StretchImage;
            _progressIndicator.ClientSize = new Size(_progressIndicator.Image.Width,
                                                     _progressIndicator.Image.Height);
        }

        public void ShowProgress()
        {
            _target.SurfaceControl.SafeAction(c => ShowProgressInternal(c));
        }

        public void HideProgress()
        {
            _target.SurfaceControl.SafeAction(c => HideProgressInternal(c));
        }

        private void ShowProgressInternal(Control ctl)
        {
            _target.ControlToDisable.Enabled = false;
            _progressIndicator.Left = 0
                + (ctl.Width / 2) - (_progressIndicator.Width / 2);
            _progressIndicator.Top = 0
                + (ctl.Height / 2) - (_progressIndicator.Height / 2);
            _progressIndicator.BringToFront();

            _progressIndicator.Visible = true;
        }

        private void HideProgressInternal(Control ctl)
        {
            _target.ControlToDisable.Enabled = true;
            _progressIndicator.Visible = false;
        }
    }
}
