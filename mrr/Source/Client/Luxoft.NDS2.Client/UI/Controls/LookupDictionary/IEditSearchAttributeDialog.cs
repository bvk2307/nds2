﻿using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary
{
    public interface IEditSearchAttributeDialog
    {
        GridParameterItem ResultData { get; set; }
    }
}
