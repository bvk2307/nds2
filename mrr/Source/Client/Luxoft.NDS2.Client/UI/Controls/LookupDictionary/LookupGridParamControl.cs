﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Bibliography;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.ControlContracts;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Helpers;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Entity;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary
{
    public partial class LookupGridParamControl : Form, IEditSearchAttributeDialog, IProgressIndicatorSupportable
    {
        private ControlLoadingProgressHelper _loadingProgressHelper;
        private GridParameterItem _parameterItem = null;
        private ILookupSuitableView _parentView = null;
        private Point _location;
        private BindingSource _bindingSource = new BindingSource();
        private NsiMetadataEntry _metadata;
        private NsiEntryDataLocationType _locationType;

        public LookupGridParamControl(GridParameterItem parameterItem, ILookupSuitableView parent, Point location)
        {
            InitializeComponent();

            Name = "Выбор региона";
            _parameterItem = parameterItem;
            _parentView = parent;
            _location = location;
            _bindingSource.DataSource = typeof(LookupResponse);
            gridResult.DataSource = _bindingSource;
            SurfaceControl = upnRoot;
            ControlToDisable = gridResult;
            _loadingProgressHelper = new ControlLoadingProgressHelper(this, () => { return upnRoot.ClientArea.Controls; });
            ulNsiName.Text = _parameterItem.Settings.Name;
            InitCompareKind();
            btnOk.Enabled = GetSelectedItems().Any();
        }

        private void InitCompareKind()
        {
            lvOperator.SelectedItems.Clear();
            switch (_parameterItem.ComparerFirstFilter)
            {
                case CompareOperator.Equals:
                    lvOperator.SelectedItems.Add(lvOperator.Items[0]);
                    break;
                case CompareOperator.NotEquals:
                    lvOperator.SelectedItems.Add(lvOperator.Items[1]);
                    break;
            }

            lvOperator.ActiveItem = lvOperator.SelectedItems[0];
        }

        #region Implementation of IEditSearchAttributeDialog

        public GridParameterItem ResultData
        {
            get { return GetResultData(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        private void LookupGridParamControl_Load(object sender, EventArgs e)
        {
            this.Location = _location;
            _loadingProgressHelper.ShowProgress();
            try
            {
                _metadata = _parentView.GetLookupMetadata(_parameterItem.Settings.LookupSource);
                if (_metadata != null)
                {
                    InitGridColumns();
                    _locationType = _metadata.LocationType;

                    List<LookupResponse> selectedData = _parameterItem.GetChildItem().Select(ci => ci.ValueOne).Cast<LookupResponse>().ToList();
                    selectedData = selectedData.Where(elm => elm != null).ToList();
                    var emptyElement = selectedData.FirstOrDefault(elm => elm.Code != null);
                    if (emptyElement != null)
                    {
                        selectedData.Remove(emptyElement);
                    }

                    if (_locationType == NsiEntryDataLocationType.Remote)
                    {
                        selectedData.ForEach(response => response.IsSelected = true);
                        _bindingSource.DataSource = selectedData;
                        if (!string.IsNullOrWhiteSpace(_parameterItem.FilterSearchString))
                        {
                            tbFilterSearchString.Text = _parameterItem.FilterSearchString;
                            FindProcessing();
                        }
                    }
                    else
                    {
                        btnFind.Visible = false;
                        tbFilterSearchString.Visible = false;
                        if (_metadata.EmbeddedData != null)
                        {
                            foreach (var lookupResponse in _metadata.EmbeddedData)
                            {
                                lookupResponse.IsSelected = selectedData.Any(sd => sd.Code == lookupResponse.Code);
                            }

                            _bindingSource.DataSource = _metadata.EmbeddedData;
                        }
                        else
                        {
                            _bindingSource.DataSource = (object)typeof(LookupResponse);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                _loadingProgressHelper.HideProgress();
                gridResult.ClickCell += GridResult_ClickCell;
            }
        }

        private void GridResult_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (e.Cell.Column.Key != "IsSelected")
            {
                return;
            }
            bool isButtonOKEnabled = false;
            var selectedItem = e.Cell.Row.ListObject as LookupResponse;
            if (selectedItem != null)
            {
                foreach (var row in gridResult.Rows)
                {
                    var d = row.ListObject as LookupResponse;
                    if (d == null)
                        continue;
                    if (d.Code != selectedItem.Code)
                    {
                        row.Cells["IsSelected"].Value = false;
                    }
                }
                isButtonOKEnabled = true;
            }
            gridResult.Refresh();
            btnOk.Enabled = isButtonOKEnabled || GetSelectedItems().Any();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            FindProcessing();
        }

        private void FindProcessing()
        {
            var request = new LookupRequest();
            request.CodeFieldName = _metadata.CodeField;
            request.FilterValue = string.Format("%{0}%", tbFilterSearchString.Text);
            request.SourceName = _metadata.TableName;
            request.DataSourceKey = _metadata.SourceTarget;
            request.ValueFieldName = _metadata.DescriptionField;
            request.IdentityFieldName = _metadata.KeyField;

            //var filterResult = _parentView.GetLookupData(request);
            try
            {
                var w = new AsyncWorker<List<LookupResponse>>();

                _loadingProgressHelper.ShowProgress();
                tbFilterSearchString.Enabled = false;
                btnFind.Enabled = false;
                w.DoWork += (o, args) =>
                    {
                        args.Result = _parentView.GetLookupData(request);
                    };
                w.Failed += (o, args) => this.SafeAction(c =>
                    {
                        _loadingProgressHelper.HideProgress();
                        tbFilterSearchString.Enabled = true;
                        btnFind.Enabled = true;
                    });
                w.Complete += (o, args) =>
                    {
                        _loadingProgressHelper.HideProgress();
                        tbFilterSearchString.Enabled = true;
                        btnFind.Enabled = true;

                        var filterResult = args.Result;
                        var alreadySelectedItems = GetSelectedItems();
                        if (filterResult.Any())
                        {
                            var match = filterResult.FirstOrDefault(i => alreadySelectedItems.Any(a => a.Code == i.Code));

                            while (match != null)
                            {
                                filterResult.Remove(match);
                                match = filterResult.FirstOrDefault(i => alreadySelectedItems.Any(a => a.Code == i.Code));
                            }

                            filterResult.InsertRange(0, alreadySelectedItems);

                            _bindingSource.DataSource = filterResult;
                            foreach (var row in gridResult.Rows)
                            {
                                var d = row.ListObject as LookupResponse;
                                if (d == null)
                                    continue;
                                row.Cells["IsSelected"].Value = d.IsSelected;
                            }
                            btnOk.Enabled = GetSelectedItems().Any();
                        }
                        else
                        {
                            if (alreadySelectedItems.Count > 0)
                            {
                                _bindingSource.DataSource = alreadySelectedItems;
                            }
                            else
                            {
                                _bindingSource.DataSource = typeof(LookupResponse);
                            }
                        }
                    };

                w.Start();
            }
            finally
            {

            }
        }

        private void InitGridColumns()
        {
            gridResult.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            gridResult.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;
            gridResult.DisplayLayout.Bands[0].Columns["Value1"].Hidden = true;
            gridResult.DisplayLayout.Bands[0].Columns["Value2"].Hidden = true;
            gridResult.DisplayLayout.Bands[0].Columns["IsSelected"].Header.Caption = string.Empty;
            gridResult.DisplayLayout.Bands[0].Columns["IsSelected"].Width = 10;
            gridResult.DisplayLayout.Bands[0].Columns["IsSelected"].LockedWidth = true;
            gridResult.DisplayLayout.Bands[0].Columns["DisplayValue"].Header.Caption = _metadata.DescriptionFieldComment;
        }

        private void ubClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        #region Implementation of IProgressIndicatorSupportable

        public Control SurfaceControl { get; private set; }
        public Control ControlToDisable { get; private set; }

        #endregion

        public GridParameterItem GetResultData()
        {
            CompareOperator oper = lvOperator.SelectedItems[0].Text == "<>"
                                       ? CompareOperator.NotEquals
                                       : CompareOperator.Equals;

            List<GridParameterDetailItem> seletedItems = new List<GridParameterDetailItem>();
            if (gridResult.Rows.Count > 0)
            {
                foreach (UltraGridRow row in gridResult.Rows)
                {
                    var data = row.ListObject as LookupResponse;
                    if (data != null && data.IsSelected)
                    {
                        FilterValue filter = new FilterValue(_parameterItem.Settings.CriteriaName, oper, ParameterType.Lookup, data);
                        GridParameterDetailItem item = new GridParameterDetailItem(filter, ParameterType.Lookup);
                        seletedItems.Add(item);
                    }
                }
            }
            else
            {
                FilterValue filter = new FilterValue(_parameterItem.Settings.CriteriaName, oper, ParameterType.Lookup, new LookupResponse() { Code = null, Value1 = string.Empty });
                GridParameterDetailItem item = new GridParameterDetailItem(filter, ParameterType.Lookup);
                seletedItems.Add(item);
            }

            _parameterItem.FilterSearchString = tbFilterSearchString.Text.Trim();
            _parameterItem.SetChildItem(seletedItems);
            return _parameterItem;
        }

        private List<LookupResponse> GetSelectedItems()
        {
            var result = new List<LookupResponse>();

            foreach (UltraGridRow row in gridResult.Rows)
            {
                var data = row.ListObject as LookupResponse;
                if (data != null && data.IsSelected)
                {
                    result.Add(data);
                }
            }

            return result;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
