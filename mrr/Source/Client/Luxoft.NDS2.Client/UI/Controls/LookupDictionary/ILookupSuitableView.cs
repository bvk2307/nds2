﻿using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.ControlContracts
{
    public interface ILookupSuitableView
    {
        List<LookupResponse> GetLookupData(LookupRequest request);

        NsiMetadataEntry GetLookupMetadata(string lookupName);
    }
}
