﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Entity
{
    public class GridParameterItem
    {
        private ParameterSettings _settings;
        private string _parameterDisplayName;
        private List<GridParameterDetailItem> _values;
        public string FilterSearchString { get; set; }

        public GridParameterItem(ParameterSettings settings, string parameterDisplayName)
        {
            _settings = settings;
            _parameterDisplayName = parameterDisplayName;
            GridParameterDetailItem val = new GridParameterDetailItem(_settings.CriteriaName, ComparerHelper.DefaultComparer(_settings.LeafType), _settings.LeafType);
            val.InitValue();
            _values = new List<GridParameterDetailItem> { val};
        }
        public string ParameterDisplayName
        {
            get { return _parameterDisplayName; }
        }
        public string ComparerDisplayName
        {
            get { return ComparerFirstFilter.DisplayNameString(); }
        }
        public string ParameterDisplayValue
        {
            get
            {
                List<GridParameterDetailItem> items = GetChildItem();
                string res = items.Select(i => i.GetDisplayValue()).Aggregate((work, next) => work + " | " + next);
                return res;
            }
        }
        public ParameterSettings Settings
        {
            get { return _settings; }
        }
        public CompareOperator ComparerFirstFilter
        {
            get
            {
                GridParameterDetailItem first = GetChildItem().FirstOrDefault();
                if (first != null)
                    return first.Comparer;
                return ComparerHelper.DefaultComparer(_settings.LeafType);
            }
            set
            {
                _values.ForEach((v) => { v.Comparer = value; });
            }
        }
        public List<GridParameterDetailItem> GetChildItem()
        {
            return _values;
        }
        public void SetChildItem(List<GridParameterDetailItem> items)
        {
            _values = items;
        }
        public GridParameterItem Clone()
        {
            GridParameterItem result = new GridParameterItem(_settings, _parameterDisplayName);
            result._values.Clear();
            _values.ForEach(v => result._values.Add(v.Clone()));
            return result;
        }
        public FilterNode CreateFilterNode()
        {
            FilterNode newNode = new FilterNode();
            newNode.IsAggregate = _settings.IsAggregate;
            foreach (GridParameterDetailItem item in GetChildItem())
            {
                newNode.Child.Add(new LinkNode(BoolOperator.OR, item.CreateFilterValue()));
            }
            return newNode;
        }
    }
}
