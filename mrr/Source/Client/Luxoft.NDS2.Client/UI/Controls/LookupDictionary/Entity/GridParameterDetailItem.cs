﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.ControlContracts;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;

namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Entity
{
    public class GridParameterDetailItem
    {
        private ParameterType _valueType;

        public string Criteria { get; set; }
        public CompareOperator Comparer { get; set; }
        public object ValueOne { get; set; }
        public object ValueTwo { get; set; }

        public GridParameterDetailItem(string criteria, CompareOperator comparer, ParameterType valueType) 
        {
            Criteria = criteria;
            Comparer = comparer;
            _valueType = valueType;
        }
        public GridParameterDetailItem(FilterValue value, ParameterType valueType) 
        {
            Criteria = value.Criteria;
            Comparer = value.Comparer;
            _valueType = valueType;
            ValueOne = value.ValueOne;
            ValueTwo = value.ValueTwo;
        }
        public void InitValue()
        {
            ValueOne = ValueTwo = 
                _valueType == ParameterType.DateTime ? DateTime.Today //Обязательная не null инициализация для типа даты
                :FilterValue.GetDefault(_valueType);
        }
        public string GetDisplayValue()
        {
            ValueKind kind = ComparerHelper.GetValueKind(_valueType, Comparer);
            switch (kind)
            {
                case ValueKind.SingleValue:
                case ValueKind.SingleCollection:
                    return GetDisplayValue(ValueOne);
                case ValueKind.DoubleCollection:
                    return string.Format("<{0}-{1}>", GetDisplayValue(ValueOne), GetDisplayValue(ValueTwo));
            }
            return string.Empty;
        }
        
        public GridParameterDetailItem Clone()
        {
            GridParameterDetailItem res = new GridParameterDetailItem(this.Criteria, this.Comparer, this._valueType);
            res.ValueOne = this.ValueOne;
            res.ValueTwo = this.ValueTwo;
            return res;
        }
        public FilterValue CreateFilterValue()
        {
            return new FilterValue(Criteria, Comparer, _valueType, ValueOne, ValueTwo);
        }
        private string GetDisplayValue(object val)
        {
            switch (_valueType)
            {
                case ParameterType.Boolean:
                    int data = (int)val;
                    return data == 1 ? "Да" : "Нет";
                case ParameterType.DateTime:
                    if(val == null || val == DBNull.Value)
                    {
                        return string.Empty;
                    }

                    DateTime data2 = (DateTime)val;
                    return data2.ToShortDateString();
                case ParameterType.Int64:
                case ParameterType.String:
                    return val.ToString();
                case ParameterType.Money:
                    return ((decimal)(val ?? 0m)).ToString("N2");
                case ParameterType.Decimal:
                    return ((decimal)(val ?? 0m)).ToString("N8");
                case ParameterType.Lookup:
                    if (val == null) return string.Empty;
                    return ((LookupResponse) val).DisplayValue;
                default:
                    return string.Empty;
            }
        }
    }
}
