﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupDictionary
{
    partial class LookupGridParamControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinListView.UltraListViewItem ultraListViewItem1 = new Infragistics.Win.UltraWinListView.UltraListViewItem("=", null, null);
            Infragistics.Win.UltraWinListView.UltraListViewItem ultraListViewItem2 = new Infragistics.Win.UltraWinListView.UltraListViewItem("<>", null, null);
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ulNsiName = new Infragistics.Win.Misc.UltraLabel();
            this.gridResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lvOperator = new Infragistics.Win.UltraWinListView.UltraListView();
            this.btnFind = new Infragistics.Win.Misc.UltraButton();
            this.ubClose = new Infragistics.Win.Misc.UltraButton();
            this.tbFilterSearchString = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.upnRoot = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterSearchString)).BeginInit();
            this.upnRoot.ClientArea.SuspendLayout();
            this.upnRoot.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ulNsiName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gridResult, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lvOperator, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnFind, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ubClose, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbFilterSearchString, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(471, 223);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.tableLayoutPanel2.Controls.Add(this.btnOk, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCancel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(55, 191);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(384, 32);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnOk.Location = new System.Drawing.Point(3, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(86, 26);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Сохранить";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(289, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 26);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ulNsiName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.ulNsiName, 2);
            this.ulNsiName.Dock = System.Windows.Forms.DockStyle.Top;
            this.ulNsiName.Location = new System.Drawing.Point(3, 3);
            this.ulNsiName.Name = "ulNsiName";
            this.ulNsiName.Size = new System.Drawing.Size(433, 19);
            this.ulNsiName.TabIndex = 2;
            // 
            // gridResult
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gridResult, 2);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridResult.DisplayLayout.Appearance = appearance1;
            this.gridResult.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.gridResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridResult.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.gridResult.DisplayLayout.MaxColScrollRegions = 1;
            this.gridResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridResult.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridResult.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.gridResult.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridResult.DisplayLayout.Override.CellAppearance = appearance11;
            this.gridResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridResult.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.gridResult.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.gridResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.gridResult.DisplayLayout.Override.RowAppearance = appearance14;
            this.gridResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.gridResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridResult.Location = new System.Drawing.Point(58, 54);
            this.gridResult.Name = "gridResult";
            this.gridResult.Size = new System.Drawing.Size(410, 134);
            this.gridResult.TabIndex = 4;
            this.gridResult.UseAppStyling = false;
            // 
            // lvOperator
            // 
            this.lvOperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvOperator.Items.AddRange(new Infragistics.Win.UltraWinListView.UltraListViewItem[] {
            ultraListViewItem1,
            ultraListViewItem2});
            this.lvOperator.ItemSettings.AllowEdit = Infragistics.Win.DefaultableBoolean.False;
            this.lvOperator.ItemSettings.HideSelection = false;
            this.lvOperator.Location = new System.Drawing.Point(3, 54);
            this.lvOperator.Name = "lvOperator";
            this.lvOperator.ShowGroups = false;
            this.lvOperator.Size = new System.Drawing.Size(49, 134);
            this.lvOperator.TabIndex = 5;
            this.lvOperator.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.List;
            this.lvOperator.ViewSettingsList.ImageSize = new System.Drawing.Size(0, 0);
            this.lvOperator.ViewSettingsList.MultiColumn = false;
            // 
            // btnFind
            // 
            appearance4.Image = global::Luxoft.NDS2.Client.Properties.Resources.find16;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnFind.Appearance = appearance4;
            this.btnFind.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFind.Location = new System.Drawing.Point(439, 25);
            this.btnFind.Margin = new System.Windows.Forms.Padding(0);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(32, 26);
            this.btnFind.TabIndex = 6;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // ubClose
            // 
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources._109_AllAnnotations_Error_16x16_72;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.ubClose.Appearance = appearance3;
            this.ubClose.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Flat;
            this.ubClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ubClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ubClose.Location = new System.Drawing.Point(439, 0);
            this.ubClose.Margin = new System.Windows.Forms.Padding(0);
            this.ubClose.Name = "ubClose";
            this.ubClose.Size = new System.Drawing.Size(32, 25);
            this.ubClose.TabIndex = 7;
            this.ubClose.UseAppStyling = false;
            this.ubClose.UseFlatMode = Infragistics.Win.DefaultableBoolean.True;
            this.ubClose.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.ubClose.Click += new System.EventHandler(this.ubClose_Click);
            // 
            // tbFilterSearchString
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tbFilterSearchString, 2);
            this.tbFilterSearchString.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFilterSearchString.Location = new System.Drawing.Point(3, 28);
            this.tbFilterSearchString.Name = "tbFilterSearchString";
            this.tbFilterSearchString.Size = new System.Drawing.Size(433, 21);
            this.tbFilterSearchString.TabIndex = 3;
            // 
            // upnRoot
            // 
            this.upnRoot.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            // 
            // upnRoot.ClientArea
            // 
            this.upnRoot.ClientArea.Controls.Add(this.tableLayoutPanel1);
            this.upnRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upnRoot.Location = new System.Drawing.Point(0, 0);
            this.upnRoot.Name = "upnRoot";
            this.upnRoot.Size = new System.Drawing.Size(475, 227);
            this.upnRoot.TabIndex = 2;
            // 
            // LookupGridParamControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(475, 227);
            this.Controls.Add(this.upnRoot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LookupGridParamControl";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "LookupParamControl";
            this.Load += new System.EventHandler(this.LookupGridParamControl_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterSearchString)).EndInit();
            this.upnRoot.ClientArea.ResumeLayout(false);
            this.upnRoot.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraLabel ulNsiName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFilterSearchString;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridResult;
        private Infragistics.Win.UltraWinListView.UltraListView lvOperator;
        private Infragistics.Win.Misc.UltraButton btnFind;
        public Infragistics.Win.Misc.UltraButton ubClose;
        private Infragistics.Win.Misc.UltraPanel upnRoot;
    }
}