﻿using System;

namespace Luxoft.NDS2.Client.UI
{
    [Obsolete]
    /// <summary> A manager of separate calls to server services as data flow. </summary>
    public interface IServiceCallDataManager { }
}