﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.ControlRatios.Card
{
    public sealed class Model
    {
        public ControlRatio ControlRatio { get; set; }

        public DiscrepancyDocumentInfo DiscrepancyDocument { get; set; }

        public DeclarationSummary Declaration { get; set; }
    }
}