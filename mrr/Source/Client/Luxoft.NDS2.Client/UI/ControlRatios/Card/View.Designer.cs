﻿namespace Luxoft.NDS2.Client.UI.ControlRatios.Card
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel upHeader;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCalculationDateCaption;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulNumberCaption;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulTaxPayerNameCaption;
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulTaxPayerINNCaption;
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulTaxPeriodCaption;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCorrectionNumberCaption;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulClaimNumberCaption;
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulClaimRegisterDateCaption;
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulClaimStatusChangeDateCaption;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulClaimStatusCaption;
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulUserCommentCaption;
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCodeCaption;
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulFormulationCaption;
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCalculationConditionCaption;
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCalculatedDataCaption;
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulTaxPayerKPPCaption;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.ulHasDiscrepancy = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this._calculatedAt = new Infragistics.Win.Misc.UltraLabel();
            this.ulNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uegbCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.ufllCorrectionNumber = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ufllTaxPayerName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.uteTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteTaxPayerINN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ugbDiscrepancyInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.upDiscrepancyInfo = new Infragistics.Win.Misc.UltraPanel();
            this.uteUserComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteClaimStatusChangeDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteClaimStatus = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteClaimRegisterDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ufllClaimNumber = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ugbControlRatioInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.upControlRatioInfo = new Infragistics.Win.Misc.UltraPanel();
            this.uteCalculationRightSide = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulCalculationOperator = new Infragistics.Win.Misc.UltraLabel();
            this.uteCalculationLeftSide = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteCalculationCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteFormulation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteTaxPayerKPP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            upHeader = new Infragistics.Win.Misc.UltraPanel();
            ulCalculationDateCaption = new Infragistics.Win.Misc.UltraLabel();
            ulNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            ulTaxPayerNameCaption = new Infragistics.Win.Misc.UltraLabel();
            ulTaxPayerINNCaption = new Infragistics.Win.Misc.UltraLabel();
            ulTaxPeriodCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCorrectionNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            ulClaimNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            ulClaimRegisterDateCaption = new Infragistics.Win.Misc.UltraLabel();
            ulClaimStatusChangeDateCaption = new Infragistics.Win.Misc.UltraLabel();
            ulClaimStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            ulUserCommentCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCodeCaption = new Infragistics.Win.Misc.UltraLabel();
            ulFormulationCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCalculationConditionCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCalculatedDataCaption = new Infragistics.Win.Misc.UltraLabel();
            ulTaxPayerKPPCaption = new Infragistics.Win.Misc.UltraLabel();
            upHeader.ClientArea.SuspendLayout();
            upHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).BeginInit();
            this.uegbCommonInfo.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPayerINN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbDiscrepancyInfo)).BeginInit();
            this.ugbDiscrepancyInfo.SuspendLayout();
            this.upDiscrepancyInfo.ClientArea.SuspendLayout();
            this.upDiscrepancyInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uteUserComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimStatusChangeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimRegisterDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbControlRatioInfo)).BeginInit();
            this.ugbControlRatioInfo.SuspendLayout();
            this.upControlRatioInfo.ClientArea.SuspendLayout();
            this.upControlRatioInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationRightSide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationLeftSide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteFormulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPayerKPP)).BeginInit();
            this.SuspendLayout();
            // 
            // upHeader
            // 
            // 
            // upHeader.ClientArea
            // 
            upHeader.ClientArea.Controls.Add(this.ulHasDiscrepancy);
            upHeader.ClientArea.Controls.Add(this.ultraLabel4);
            upHeader.ClientArea.Controls.Add(ulCalculationDateCaption);
            upHeader.ClientArea.Controls.Add(this._calculatedAt);
            upHeader.ClientArea.Controls.Add(ulNumberCaption);
            upHeader.ClientArea.Controls.Add(this.ulNumber);
            upHeader.Dock = System.Windows.Forms.DockStyle.Top;
            upHeader.Location = new System.Drawing.Point(0, 0);
            upHeader.Name = "upHeader";
            upHeader.Size = new System.Drawing.Size(1054, 26);
            upHeader.TabIndex = 0;
            // 
            // ulHasDiscrepancy
            // 
            appearance2.ForeColor = System.Drawing.Color.Firebrick;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ulHasDiscrepancy.Appearance = appearance2;
            this.ulHasDiscrepancy.Location = new System.Drawing.Point(495, 3);
            this.ulHasDiscrepancy.Name = "ulHasDiscrepancy";
            this.ulHasDiscrepancy.Size = new System.Drawing.Size(215, 20);
            this.ulHasDiscrepancy.TabIndex = 4;
            this.ulHasDiscrepancy.Text = "Выявлено расхождение";
            // 
            // ultraLabel4
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance1;
            this.ultraLabel4.Location = new System.Drawing.Point(604, 3);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel4.TabIndex = 5;
            // 
            // ulCalculationDateCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            ulCalculationDateCaption.Appearance = appearance3;
            ulCalculationDateCaption.Location = new System.Drawing.Point(280, 3);
            ulCalculationDateCaption.Name = "ulCalculationDateCaption";
            ulCalculationDateCaption.Size = new System.Drawing.Size(103, 20);
            ulCalculationDateCaption.TabIndex = 2;
            ulCalculationDateCaption.Text = "Дата расчета";
            // 
            // _calculatedAt
            // 
            appearance5.FontData.BoldAsString = "True";
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this._calculatedAt.Appearance = appearance5;
            this._calculatedAt.Location = new System.Drawing.Point(389, 3);
            this._calculatedAt.Name = "_calculatedAt";
            this._calculatedAt.Size = new System.Drawing.Size(100, 20);
            this._calculatedAt.TabIndex = 3;
            // 
            // ulNumberCaption
            // 
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            ulNumberCaption.Appearance = appearance8;
            ulNumberCaption.Location = new System.Drawing.Point(3, 3);
            ulNumberCaption.Name = "ulNumberCaption";
            ulNumberCaption.Size = new System.Drawing.Size(165, 20);
            ulNumberCaption.TabIndex = 0;
            ulNumberCaption.Text = "Контрольное соотношение №";
            // 
            // ulNumber
            // 
            appearance6.FontData.BoldAsString = "True";
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ulNumber.Appearance = appearance6;
            this.ulNumber.Location = new System.Drawing.Point(174, 3);
            this.ulNumber.Name = "ulNumber";
            this.ulNumber.Size = new System.Drawing.Size(100, 20);
            this.ulNumber.TabIndex = 1;
            // 
            // ulTaxPayerNameCaption
            // 
            appearance37.TextHAlignAsString = "Right";
            ulTaxPayerNameCaption.Appearance = appearance37;
            ulTaxPayerNameCaption.Location = new System.Drawing.Point(23, 7);
            ulTaxPayerNameCaption.Name = "ulTaxPayerNameCaption";
            ulTaxPayerNameCaption.Size = new System.Drawing.Size(91, 16);
            ulTaxPayerNameCaption.TabIndex = 1;
            ulTaxPayerNameCaption.Text = "Наименование:";
            // 
            // ulTaxPayerINNCaption
            // 
            appearance13.TextHAlignAsString = "Right";
            ulTaxPayerINNCaption.Appearance = appearance13;
            ulTaxPayerINNCaption.Location = new System.Drawing.Point(23, 27);
            ulTaxPayerINNCaption.Name = "ulTaxPayerINNCaption";
            ulTaxPayerINNCaption.Size = new System.Drawing.Size(91, 16);
            ulTaxPayerINNCaption.TabIndex = 3;
            ulTaxPayerINNCaption.Text = "ИНН:";
            // 
            // ulTaxPeriodCaption
            // 
            appearance7.TextHAlignAsString = "Right";
            ulTaxPeriodCaption.Appearance = appearance7;
            ulTaxPeriodCaption.Location = new System.Drawing.Point(386, 27);
            ulTaxPeriodCaption.Name = "ulTaxPeriodCaption";
            ulTaxPeriodCaption.Size = new System.Drawing.Size(128, 16);
            ulTaxPeriodCaption.TabIndex = 7;
            ulTaxPeriodCaption.Text = "Отчетный период:";
            // 
            // ulCorrectionNumberCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            ulCorrectionNumberCaption.Appearance = appearance4;
            ulCorrectionNumberCaption.Location = new System.Drawing.Point(386, 47);
            ulCorrectionNumberCaption.Name = "ulCorrectionNumberCaption";
            ulCorrectionNumberCaption.Size = new System.Drawing.Size(128, 16);
            ulCorrectionNumberCaption.TabIndex = 9;
            ulCorrectionNumberCaption.Text = "Номер корректировки:";
            // 
            // ulClaimNumberCaption
            // 
            appearance27.TextHAlignAsString = "Right";
            ulClaimNumberCaption.Appearance = appearance27;
            ulClaimNumberCaption.Location = new System.Drawing.Point(3, 7);
            ulClaimNumberCaption.Name = "ulClaimNumberCaption";
            ulClaimNumberCaption.Size = new System.Drawing.Size(209, 16);
            ulClaimNumberCaption.TabIndex = 0;
            ulClaimNumberCaption.Text = "Автотребование №:";
            ulClaimNumberCaption.WrapText = false;
            // 
            // ulClaimRegisterDateCaption
            // 
            appearance15.TextHAlignAsString = "Right";
            ulClaimRegisterDateCaption.Appearance = appearance15;
            ulClaimRegisterDateCaption.Location = new System.Drawing.Point(3, 27);
            ulClaimRegisterDateCaption.Name = "ulClaimRegisterDateCaption";
            ulClaimRegisterDateCaption.Size = new System.Drawing.Size(209, 16);
            ulClaimRegisterDateCaption.TabIndex = 2;
            ulClaimRegisterDateCaption.Text = "Дата регистрации автотребования:";
            ulClaimRegisterDateCaption.WrapText = false;
            // 
            // ulClaimStatusChangeDateCaption
            // 
            appearance10.TextHAlignAsString = "Right";
            ulClaimStatusChangeDateCaption.Appearance = appearance10;
            ulClaimStatusChangeDateCaption.Location = new System.Drawing.Point(3, 67);
            ulClaimStatusChangeDateCaption.Name = "ulClaimStatusChangeDateCaption";
            ulClaimStatusChangeDateCaption.Size = new System.Drawing.Size(209, 16);
            ulClaimStatusChangeDateCaption.TabIndex = 6;
            ulClaimStatusChangeDateCaption.Text = "Дата изменения статуса:";
            ulClaimStatusChangeDateCaption.WrapText = false;
            // 
            // ulClaimStatusCaption
            // 
            appearance14.TextHAlignAsString = "Right";
            ulClaimStatusCaption.Appearance = appearance14;
            ulClaimStatusCaption.Location = new System.Drawing.Point(3, 47);
            ulClaimStatusCaption.Name = "ulClaimStatusCaption";
            ulClaimStatusCaption.Size = new System.Drawing.Size(209, 16);
            ulClaimStatusCaption.TabIndex = 4;
            ulClaimStatusCaption.Text = "Статус автотребования:";
            ulClaimStatusCaption.WrapText = false;
            // 
            // ulUserCommentCaption
            // 
            appearance23.TextHAlignAsString = "Left";
            ulUserCommentCaption.Appearance = appearance23;
            ulUserCommentCaption.Location = new System.Drawing.Point(492, 7);
            ulUserCommentCaption.Name = "ulUserCommentCaption";
            ulUserCommentCaption.Size = new System.Drawing.Size(129, 16);
            ulUserCommentCaption.TabIndex = 8;
            ulUserCommentCaption.Text = "Комментарий:";
            ulUserCommentCaption.WrapText = false;
            // 
            // ulCodeCaption
            // 
            appearance29.TextHAlignAsString = "Right";
            ulCodeCaption.Appearance = appearance29;
            ulCodeCaption.Location = new System.Drawing.Point(3, 7);
            ulCodeCaption.Name = "ulCodeCaption";
            ulCodeCaption.Size = new System.Drawing.Size(51, 16);
            ulCodeCaption.TabIndex = 0;
            ulCodeCaption.Text = "Код КС:";
            ulCodeCaption.WrapText = false;
            // 
            // ulFormulationCaption
            // 
            appearance33.TextHAlignAsString = "Left";
            ulFormulationCaption.Appearance = appearance33;
            ulFormulationCaption.Location = new System.Drawing.Point(3, 42);
            ulFormulationCaption.Name = "ulFormulationCaption";
            ulFormulationCaption.Size = new System.Drawing.Size(209, 33);
            ulFormulationCaption.TabIndex = 2;
            ulFormulationCaption.Text = "Формулировка нарушения:";
            ulFormulationCaption.WrapText = false;
            // 
            // ulCalculationConditionCaption
            // 
            appearance31.TextHAlignAsString = "Left";
            ulCalculationConditionCaption.Appearance = appearance31;
            ulCalculationConditionCaption.Location = new System.Drawing.Point(3, 157);
            ulCalculationConditionCaption.Name = "ulCalculationConditionCaption";
            ulCalculationConditionCaption.Size = new System.Drawing.Size(209, 16);
            ulCalculationConditionCaption.TabIndex = 4;
            ulCalculationConditionCaption.Text = "Формула расчета:";
            ulCalculationConditionCaption.WrapText = false;
            ulCalculationConditionCaption.Click += new System.EventHandler(this.ulCalculationConditionCaption_Click);
            // 
            // ulCalculatedDataCaption
            // 
            appearance16.TextHAlignAsString = "Left";
            ulCalculatedDataCaption.Appearance = appearance16;
            ulCalculatedDataCaption.Location = new System.Drawing.Point(3, 275);
            ulCalculatedDataCaption.Name = "ulCalculatedDataCaption";
            ulCalculatedDataCaption.Size = new System.Drawing.Size(209, 16);
            ulCalculatedDataCaption.TabIndex = 6;
            ulCalculatedDataCaption.Text = "Рассчитанные значения соотношений:";
            ulCalculatedDataCaption.WrapText = false;
            // 
            // uegbCommonInfo
            // 
            this.uegbCommonInfo.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uegbCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.uegbCommonInfo.ExpandedSize = new System.Drawing.Size(1054, 102);
            this.uegbCommonInfo.Location = new System.Drawing.Point(0, 26);
            this.uegbCommonInfo.Name = "uegbCommonInfo";
            this.uegbCommonInfo.Size = new System.Drawing.Size(1054, 102);
            this.uegbCommonInfo.TabIndex = 1;
            this.uegbCommonInfo.Text = "Общие сведения";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._surIndicator);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ufllCorrectionNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ufllTaxPayerName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(ulCorrectionNumberCaption);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uteTaxPeriod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(ulTaxPeriodCaption);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(ulTaxPayerKPPCaption);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(ulTaxPayerINNCaption);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uteTaxPayerKPP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uteTaxPayerINN);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(ulTaxPayerNameCaption);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1048, 80);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(4, 5);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 0;
            // 
            // ufllCorrectionNumber
            // 
            appearance21.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance21.FontData.Name = "Microsoft Sans Serif";
            appearance21.FontData.SizeInPoints = 8.25F;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Middle";
            this.ufllCorrectionNumber.Appearance = appearance21;
            this.ufllCorrectionNumber.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ufllCorrectionNumber.Location = new System.Drawing.Point(520, 43);
            this.ufllCorrectionNumber.Name = "ufllCorrectionNumber";
            this.ufllCorrectionNumber.Size = new System.Drawing.Size(234, 21);
            this.ufllCorrectionNumber.TabIndex = 10;
            this.ufllCorrectionNumber.TabStop = true;
            this.ufllCorrectionNumber.Value = "";
            this.ufllCorrectionNumber.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.ufllCorrectionNumber_LinkClicked);
            // 
            // ufllTaxPayerName
            // 
            appearance30.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance30.FontData.Name = "Microsoft Sans Serif";
            appearance30.FontData.SizeInPoints = 8.25F;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Middle";
            this.ufllTaxPayerName.Appearance = appearance30;
            this.ufllTaxPayerName.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ufllTaxPayerName.Location = new System.Drawing.Point(120, 3);
            this.ufllTaxPayerName.Name = "ufllTaxPayerName";
            this.ufllTaxPayerName.Size = new System.Drawing.Size(234, 21);
            this.ufllTaxPayerName.TabIndex = 2;
            this.ufllTaxPayerName.TabStop = true;
            this.ufllTaxPayerName.Value = "";
            this.ufllTaxPayerName.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.ufllTaxPayerName_LinkClicked);
            // 
            // uteTaxPeriod
            // 
            appearance36.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.uteTaxPeriod.Appearance = appearance36;
            this.uteTaxPeriod.Location = new System.Drawing.Point(520, 23);
            this.uteTaxPeriod.Name = "uteTaxPeriod";
            this.uteTaxPeriod.ReadOnly = true;
            this.uteTaxPeriod.Size = new System.Drawing.Size(234, 21);
            this.uteTaxPeriod.TabIndex = 8;
            // 
            // uteTaxPayerINN
            // 
            appearance34.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance34.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteTaxPayerINN.Appearance = appearance34;
            this.uteTaxPayerINN.Location = new System.Drawing.Point(120, 23);
            this.uteTaxPayerINN.Name = "uteTaxPayerINN";
            this.uteTaxPayerINN.ReadOnly = true;
            this.uteTaxPayerINN.Size = new System.Drawing.Size(234, 21);
            this.uteTaxPayerINN.TabIndex = 4;
            this.uteTaxPayerINN.Text = " ";
            // 
            // ugbDiscrepancyInfo
            // 
            this.ugbDiscrepancyInfo.Controls.Add(this.upDiscrepancyInfo);
            this.ugbDiscrepancyInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.ugbDiscrepancyInfo.Location = new System.Drawing.Point(0, 128);
            this.ugbDiscrepancyInfo.Name = "ugbDiscrepancyInfo";
            this.ugbDiscrepancyInfo.Size = new System.Drawing.Size(1054, 109);
            this.ugbDiscrepancyInfo.TabIndex = 2;
            this.ugbDiscrepancyInfo.Text = "Сведения о расхождении";
            // 
            // upDiscrepancyInfo
            // 
            // 
            // upDiscrepancyInfo.ClientArea
            // 
            this.upDiscrepancyInfo.ClientArea.Controls.Add(this.uteUserComment);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(ulUserCommentCaption);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(this.uteClaimStatusChangeDate);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(this.uteClaimStatus);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(this.uteClaimRegisterDate);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(ulClaimStatusChangeDateCaption);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(ulClaimStatusCaption);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(ulClaimRegisterDateCaption);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(this.ufllClaimNumber);
            this.upDiscrepancyInfo.ClientArea.Controls.Add(ulClaimNumberCaption);
            this.upDiscrepancyInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upDiscrepancyInfo.Location = new System.Drawing.Point(3, 16);
            this.upDiscrepancyInfo.Name = "upDiscrepancyInfo";
            this.upDiscrepancyInfo.Size = new System.Drawing.Size(1048, 90);
            this.upDiscrepancyInfo.TabIndex = 0;
            // 
            // uteUserComment
            // 
            this.uteUserComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance22.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteUserComment.Appearance = appearance22;
            this.uteUserComment.Location = new System.Drawing.Point(492, 23);
            this.uteUserComment.Multiline = true;
            this.uteUserComment.Name = "uteUserComment";
            this.uteUserComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.uteUserComment.Size = new System.Drawing.Size(553, 60);
            this.uteUserComment.TabIndex = 9;
            this.uteUserComment.Text = " ";
            // 
            // uteClaimStatusChangeDate
            // 
            appearance11.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteClaimStatusChangeDate.Appearance = appearance11;
            this.uteClaimStatusChangeDate.Location = new System.Drawing.Point(215, 63);
            this.uteClaimStatusChangeDate.Name = "uteClaimStatusChangeDate";
            this.uteClaimStatusChangeDate.ReadOnly = true;
            this.uteClaimStatusChangeDate.Size = new System.Drawing.Size(234, 21);
            this.uteClaimStatusChangeDate.TabIndex = 7;
            this.uteClaimStatusChangeDate.Text = " ";
            // 
            // uteClaimStatus
            // 
            appearance24.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance24.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance24.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteClaimStatus.Appearance = appearance24;
            this.uteClaimStatus.Location = new System.Drawing.Point(215, 43);
            this.uteClaimStatus.Name = "uteClaimStatus";
            this.uteClaimStatus.ReadOnly = true;
            this.uteClaimStatus.Size = new System.Drawing.Size(234, 21);
            this.uteClaimStatus.TabIndex = 5;
            this.uteClaimStatus.Text = " ";
            // 
            // uteClaimRegisterDate
            // 
            appearance12.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance12.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance12.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteClaimRegisterDate.Appearance = appearance12;
            this.uteClaimRegisterDate.Location = new System.Drawing.Point(215, 23);
            this.uteClaimRegisterDate.Name = "uteClaimRegisterDate";
            this.uteClaimRegisterDate.ReadOnly = true;
            this.uteClaimRegisterDate.Size = new System.Drawing.Size(234, 21);
            this.uteClaimRegisterDate.TabIndex = 3;
            this.uteClaimRegisterDate.Text = " ";
            // 
            // ufllClaimNumber
            // 
            appearance26.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance26.FontData.Name = "Microsoft Sans Serif";
            appearance26.FontData.SizeInPoints = 8.25F;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Middle";
            this.ufllClaimNumber.Appearance = appearance26;
            this.ufllClaimNumber.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ufllClaimNumber.Location = new System.Drawing.Point(215, 3);
            this.ufllClaimNumber.Name = "ufllClaimNumber";
            this.ufllClaimNumber.Size = new System.Drawing.Size(234, 21);
            this.ufllClaimNumber.TabIndex = 1;
            this.ufllClaimNumber.TabStop = true;
            this.ufllClaimNumber.Value = "";
            this.ufllClaimNumber.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.ufllClaimNumber_LinkClicked);
            // 
            // ugbControlRatioInfo
            // 
            this.ugbControlRatioInfo.Controls.Add(this.upControlRatioInfo);
            this.ugbControlRatioInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugbControlRatioInfo.Location = new System.Drawing.Point(0, 237);
            this.ugbControlRatioInfo.Name = "ugbControlRatioInfo";
            this.ugbControlRatioInfo.Size = new System.Drawing.Size(1054, 353);
            this.ugbControlRatioInfo.TabIndex = 3;
            this.ugbControlRatioInfo.Text = "Данные контрольного соотношения";
            // 
            // upControlRatioInfo
            // 
            // 
            // upControlRatioInfo.ClientArea
            // 
            this.upControlRatioInfo.ClientArea.Controls.Add(this.uteCalculationRightSide);
            this.upControlRatioInfo.ClientArea.Controls.Add(this.ulCalculationOperator);
            this.upControlRatioInfo.ClientArea.Controls.Add(this.uteCalculationLeftSide);
            this.upControlRatioInfo.ClientArea.Controls.Add(ulCalculatedDataCaption);
            this.upControlRatioInfo.ClientArea.Controls.Add(this.uteCalculationCondition);
            this.upControlRatioInfo.ClientArea.Controls.Add(ulCalculationConditionCaption);
            this.upControlRatioInfo.ClientArea.Controls.Add(this.uteFormulation);
            this.upControlRatioInfo.ClientArea.Controls.Add(ulFormulationCaption);
            this.upControlRatioInfo.ClientArea.Controls.Add(ulCodeCaption);
            this.upControlRatioInfo.ClientArea.Controls.Add(this.uteCode);
            this.upControlRatioInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upControlRatioInfo.Location = new System.Drawing.Point(3, 16);
            this.upControlRatioInfo.Name = "upControlRatioInfo";
            this.upControlRatioInfo.Size = new System.Drawing.Size(1048, 334);
            this.upControlRatioInfo.TabIndex = 0;
            // 
            // uteCalculationRightSide
            // 
            appearance25.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance25.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance25.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteCalculationRightSide.Appearance = appearance25;
            this.uteCalculationRightSide.Location = new System.Drawing.Point(228, 297);
            this.uteCalculationRightSide.Name = "uteCalculationRightSide";
            this.uteCalculationRightSide.ReadOnly = true;
            this.uteCalculationRightSide.Size = new System.Drawing.Size(152, 21);
            this.uteCalculationRightSide.TabIndex = 9;
            this.uteCalculationRightSide.Text = " ";
            // 
            // ulCalculationOperator
            // 
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            this.ulCalculationOperator.Appearance = appearance18;
            this.ulCalculationOperator.Location = new System.Drawing.Point(171, 298);
            this.ulCalculationOperator.Name = "ulCalculationOperator";
            this.ulCalculationOperator.Size = new System.Drawing.Size(51, 21);
            this.ulCalculationOperator.TabIndex = 8;
            this.ulCalculationOperator.WrapText = false;
            // 
            // uteCalculationLeftSide
            // 
            appearance28.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance28.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteCalculationLeftSide.Appearance = appearance28;
            this.uteCalculationLeftSide.Location = new System.Drawing.Point(13, 297);
            this.uteCalculationLeftSide.Name = "uteCalculationLeftSide";
            this.uteCalculationLeftSide.ReadOnly = true;
            this.uteCalculationLeftSide.Size = new System.Drawing.Size(152, 21);
            this.uteCalculationLeftSide.TabIndex = 7;
            this.uteCalculationLeftSide.Text = " ";
            // 
            // uteCalculationCondition
            // 
            this.uteCalculationCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance17.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteCalculationCondition.Appearance = appearance17;
            this.uteCalculationCondition.Location = new System.Drawing.Point(3, 173);
            this.uteCalculationCondition.Multiline = true;
            this.uteCalculationCondition.Name = "uteCalculationCondition";
            this.uteCalculationCondition.ReadOnly = true;
            this.uteCalculationCondition.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.uteCalculationCondition.Size = new System.Drawing.Size(1042, 83);
            this.uteCalculationCondition.TabIndex = 5;
            this.uteCalculationCondition.Text = " ";
            this.uteCalculationCondition.ValueChanged += new System.EventHandler(this.uteCalculationCondition_ValueChanged);
            // 
            // uteFormulation
            // 
            this.uteFormulation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance32.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance32.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteFormulation.Appearance = appearance32;
            this.uteFormulation.Location = new System.Drawing.Point(3, 58);
            this.uteFormulation.Multiline = true;
            this.uteFormulation.Name = "uteFormulation";
            this.uteFormulation.ReadOnly = true;
            this.uteFormulation.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.uteFormulation.Size = new System.Drawing.Size(1042, 77);
            this.uteFormulation.TabIndex = 3;
            this.uteFormulation.Text = " ";
            // 
            // uteCode
            // 
            appearance35.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance35.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteCode.Appearance = appearance35;
            this.uteCode.Location = new System.Drawing.Point(60, 3);
            this.uteCode.Name = "uteCode";
            this.uteCode.ReadOnly = true;
            this.uteCode.Size = new System.Drawing.Size(152, 21);
            this.uteCode.TabIndex = 1;
            this.uteCode.Text = " ";
            // 
            // ulTaxPayerKPPCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            ulTaxPayerKPPCaption.Appearance = appearance9;
            ulTaxPayerKPPCaption.Location = new System.Drawing.Point(23, 47);
            ulTaxPayerKPPCaption.Name = "ulTaxPayerKPPCaption";
            ulTaxPayerKPPCaption.Size = new System.Drawing.Size(91, 16);
            ulTaxPayerKPPCaption.TabIndex = 5;
            ulTaxPayerKPPCaption.Text = "КПП:";
            // 
            // uteTaxPayerKPP
            // 
            appearance20.BorderColor = System.Drawing.SystemColors.ControlDark;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance20.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.uteTaxPayerKPP.Appearance = appearance20;
            this.uteTaxPayerKPP.Location = new System.Drawing.Point(120, 43);
            this.uteTaxPayerKPP.Name = "uteTaxPayerKPP";
            this.uteTaxPayerKPP.ReadOnly = true;
            this.uteTaxPayerKPP.Size = new System.Drawing.Size(234, 21);
            this.uteTaxPayerKPP.TabIndex = 6;
            this.uteTaxPayerKPP.Text = " ";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.ugbControlRatioInfo);
            this.Controls.Add(this.ugbDiscrepancyInfo);
            this.Controls.Add(this.uegbCommonInfo);
            this.Controls.Add(upHeader);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1054, 590);
            this.Load += new System.EventHandler(this.View_Load);
            upHeader.ClientArea.ResumeLayout(false);
            upHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).EndInit();
            this.uegbCommonInfo.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPayerINN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbDiscrepancyInfo)).EndInit();
            this.ugbDiscrepancyInfo.ResumeLayout(false);
            this.upDiscrepancyInfo.ClientArea.ResumeLayout(false);
            this.upDiscrepancyInfo.ClientArea.PerformLayout();
            this.upDiscrepancyInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uteUserComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimStatusChangeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteClaimRegisterDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbControlRatioInfo)).EndInit();
            this.ugbControlRatioInfo.ResumeLayout(false);
            this.upControlRatioInfo.ClientArea.ResumeLayout(false);
            this.upControlRatioInfo.ClientArea.PerformLayout();
            this.upControlRatioInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationRightSide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationLeftSide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCalculationCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteFormulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteTaxPayerKPP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ulHasDiscrepancy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel _calculatedAt;
        private Infragistics.Win.Misc.UltraLabel ulNumber;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uegbCommonInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteTaxPayerINN;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ufllCorrectionNumber;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ufllTaxPayerName;
        private Infragistics.Win.Misc.UltraGroupBox ugbDiscrepancyInfo;
        private Infragistics.Win.Misc.UltraPanel upDiscrepancyInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteUserComment;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteClaimStatusChangeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteClaimStatus;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteClaimRegisterDate;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ufllClaimNumber;
        private Infragistics.Win.Misc.UltraGroupBox ugbControlRatioInfo;
        private Infragistics.Win.Misc.UltraPanel upControlRatioInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteCalculationRightSide;
        private Infragistics.Win.Misc.UltraLabel ulCalculationOperator;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteCalculationLeftSide;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteCalculationCondition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteFormulation;
        private Controls.Sur.SurIcon _surIndicator;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteTaxPayerKPP;


    }
}
