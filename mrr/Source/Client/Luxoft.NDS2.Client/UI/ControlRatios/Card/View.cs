﻿using System;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win.FormattedLinkLabel;
using Luxoft.NDS2.Client.UI.Base;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Client.UI.ControlRatios.Card
{
    public partial class View : BaseView
    {
        private readonly ControlRatioCardPresenter _presenter;

        public View(PresentationContext ctx, WorkItem wi, long ratioId)
            : base(ctx, wi)
        {
            InitializeComponent();
            _presenter = new ControlRatioCardPresenter(this, ctx, wi, ratioId);
        }

        private void View_Load(object sender, EventArgs e)
        {
            if (_presenter != null)
            {
                this.LoadModel(_presenter.GetModel());
            }
        }


        internal void LoadModel(Model model)
        {
            ulNumber.Text = AsString(model.ControlRatio.Id);

            _calculatedAt.Text = model.ControlRatio.CalculationDate.HasValue ? model.ControlRatio.CalculationDate.Value.ToString(ResourceManagerNDS2.UiFormats.SHORT_DATE) : string.Empty;

            ulHasDiscrepancy.Visible = ugbDiscrepancyInfo.Visible = model.ControlRatio.HasDiscrepancy;

            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = model.Declaration.SUR_CODE;

            ufllTaxPayerName.Value = string.Format("<a href = '#'>{0}</a>", model.Declaration.NAME);
            uteTaxPeriod.Text = model.Declaration.FULL_TAX_PERIOD;
            ufllCorrectionNumber.Value = string.Format("<a href = '#'>{0}</a>", String.IsNullOrEmpty(model.Declaration.CORRECTION_NUMBER_EFFECTIVE) 
                                                                                            ? model.Declaration.CORRECTION_NUMBER
                                                                                            : model.Declaration.CORRECTION_NUMBER_EFFECTIVE);

            uteTaxPayerINN.Text = model.Declaration.INN;
            uteTaxPayerKPP.Text = model.Declaration.KPP;

            if (model.ControlRatio.HasDiscrepancy && model.DiscrepancyDocument.Id > 0)
            {
                ufllClaimNumber.Value = string.Format("<a href = '#'>{0}</a>", model.DiscrepancyDocument.Id);
                uteClaimRegisterDate.Text = AsString(model.DiscrepancyDocument.CreateDate);
                uteClaimStatus.Text = model.DiscrepancyDocument.Status == null ? null : model.DiscrepancyDocument.Status.EntryValue;
                uteClaimStatusChangeDate.Text = AsString(model.DiscrepancyDocument.StatusChangeDate);
            }
            uteUserComment.Text = model.ControlRatio.UserComment;

            uteCode.Text = model.ControlRatio.TypeCode;
            uteFormulation.Text = model.ControlRatio.TypeFormulation;
            uteCalculationCondition.Text = model.ControlRatio.TypeCalculationCondition;
            uteCalculationLeftSide.Text = AsString(model.ControlRatio.DisparityLeft);
            uteCalculationRightSide.Text = AsString(model.ControlRatio.DisparityRight);
            ulCalculationOperator.Text = model.ControlRatio.Type.CalculationOperator;

            uegbCommonInfo.Expanded = false;
        }

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            base.OnClosed(closeContext);
            _presenter.UpdateComment(uteUserComment.Text);
        }

        private void ufllTaxPayerName_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.OpenTaxPayerCard();
            e.AddToVisitedLinks = false;
        }

        private void ufllCorrectionNumber_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.OpenDeclarationCard();
            e.AddToVisitedLinks = false;
        }

        private void ufllClaimNumber_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.OpenClaimCard();
            e.AddToVisitedLinks = false;
        }

        private string AsString(long? value)
        {
            return value.HasValue ? AsString(value.Value) : null;
        }

        private string AsString(long value)
        {
            return value.ToString();
        }

        private string AsString(decimal value)
        {
            return value.ToString("N2");
        }

        private string AsString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToShortDateString() : null;
        }

        private void uteCalculationCondition_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ulCalculationConditionCaption_Click(object sender, EventArgs e)
        {

        }

    }
}