﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.ControlRatios.Card
{
    public sealed class ControlRatioCardPresenter : BasePresenter<View>
    {
        private readonly Model _model;
        private readonly IControlRatioService _service;

        private readonly IDeclarationCardOpener _declarationCardOpener;

        public ControlRatioCardPresenter(View view, PresentationContext ctx, WorkItem wi, long id)
            : base(ctx, wi, view)
        {
            _service = GetServiceProxy<IControlRatioService>();
            _declarationCardOpener = GetDeclarationCardOpener();
            _model = LoadModel(id);            
        }

        public Model GetModel()
        {
            return _model;
        }

        private Model LoadModel(long id)
        {
            var result = new Model();
            ExecuteServiceCall<OperationResult<ControlRatio>>(
                () => _service.GetControlRatio(id),
                opResult => result.ControlRatio = opResult.Result,
                opResult => result.ControlRatio = new ControlRatio());

            ExecuteServiceCall<OperationResult<DiscrepancyDocumentInfo>>(
                () => _service.GetDocument(id),
                opResult => result.DiscrepancyDocument = opResult.Result,
                opResult => result.DiscrepancyDocument = new DiscrepancyDocumentInfo());

            ExecuteServiceCall<OperationResult<DeclarationSummary>>(
                () => _service.GetDeclarationVersion(result.ControlRatio.Decl_Version_Id),
                opResult => result.Declaration = opResult.Result,
                opResult => result.Declaration = new DeclarationSummary());
            
            return result;
        }

        public void OpenClaimCard()
        {
            ViewClaimDetails(_model.DiscrepancyDocument);
        }

        public void OpenDeclarationCard()
        {
            var result = _declarationCardOpener.Open(_model.Declaration.DECLARATION_VERSION_ID);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public void OpenTaxPayerCard()
        {
            ViewTaxPayerByKppEffective(_model.Declaration.INN, _model.Declaration.KPP_EFFECTIVE);
        }

        public void UpdateComment(string comment)
        {
            var input = comment.Trim();
            if (input != _model.ControlRatio.UserComment)
            {
                _service.UpdateComment(_model.ControlRatio.Id, comment);
            }
        }

    }
}