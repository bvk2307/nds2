﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;

namespace Luxoft.NDS2.Client.UI
{
    /// <summary> A notifier of <see cref="MessageInfoContext"/>. </summary>
    public interface IMessageInfoNotifier
    {
        void Show( MessageInfoContext message );
    }
}