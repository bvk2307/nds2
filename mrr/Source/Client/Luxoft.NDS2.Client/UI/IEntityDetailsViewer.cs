﻿namespace Luxoft.NDS2.Client.UI
{
    public interface IEntityDetailsViewer
    {
        void Show(long entityId);
    }
}
