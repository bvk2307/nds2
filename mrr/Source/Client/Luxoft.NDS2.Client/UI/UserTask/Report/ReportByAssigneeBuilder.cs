﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public class ReportByAssigneeBuilder : ReportBuilderBase<CompletedByAssignee, InProgressByAssignee, UnassignedSummaryModel, UserTaskInspectorSummary, string>
    {
        private readonly Dictionary<string, string> _assignees;

        public ReportByAssigneeBuilder(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<UserTaskInspectorSummary> dataLoader,
            IExporterCreator<CompletedByAssignee, InProgressByAssignee, UnassignedSummaryModel> exporterCreator,
            Dictionary<string, string> assignees)
            : base(model, reportFinder, dataLoader, exporterCreator)
        {
            _assignees = assignees;
        }

        protected override Dictionary<string, string> GetGroups()
        {
            return _assignees;
        }

        protected override string GetGroupKey(UserTaskInspectorSummary dataItem)
        {
            return dataItem.UserSid;
        }

        protected override int GetUnassigned(UserTaskInspectorSummary dataItem)
        {
            throw new NotSupportedException();
        }

        protected override CompletedByAssignee CreateCompleted(string group, string taskType)
        {
            return new CompletedByAssignee(taskType, group);
        }

        protected override InProgressByAssignee CreateInProgress(string group, string taskType)
        {
            return new InProgressByAssignee(taskType, group);
        }

        protected override UnassignedSummaryModel CreateUnassigned(string group, string taskType)
        {
            throw new NotSupportedException();
        }
    }
}
