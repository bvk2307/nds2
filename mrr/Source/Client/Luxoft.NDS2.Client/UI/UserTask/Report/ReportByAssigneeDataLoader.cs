﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByAssigneeDataLoader : ServiceProxyBase<UserTaskInspectorSummary[]>, IReportDataLoader<UserTaskInspectorSummary>
    {
        private readonly IUserTaskReportService _service;

        private readonly int[] _taskTypes;

        private readonly string[] _userSids;

        public ReportByAssigneeDataLoader(
            INotifier notifier,
            IClientLogger logger,
            IUserTaskReportService service,
            int[] taskTypes,
            string[] userSids)
            : base(notifier, logger)
        {
            _service = service;
            _taskTypes = taskTypes;
            _userSids = userSids;
        }

        public bool Load(long reportId, out UserTaskInspectorSummary[] data)
        {
            return Invoke(() => _service.ReportByUsers(reportId, _userSids, _taskTypes), out data);
        }
    }
}
