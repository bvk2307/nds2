﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using System;
using System.Collections;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public class TaskTypeSelectedChanged : EventArgs
    {
        public TaskTypeSelectedChanged(bool isSelected, object taskType)
        {
            IsSelected = isSelected;
            TaskType = taskType;
        }

        public bool IsSelected
        {
            get;
            private set;
        }

        public object TaskType
        {
            get;
            private set;
        }
    }

    public interface IReportDialogView
    {
        # region Период формирования отчета

        event EventHandler OnDateFromChanged;        

        DateTime DateFrom { get; set; }

        event EventHandler OnDateToChanged;

        DateTime DateTo { get; set; }

        bool DateToValid { set; }

        DateTime MaxDate { set; }

        # endregion

        # region Выбор отчета (детализация)

        void SetReports(IEnumerable items);

        event EventHandler SelectedReportChanged;

        object SelectedReport { get; set; }

        # endregion

        # region Типы пользовательских заданий

        event EventHandler<TaskTypeSelectedChanged> TaskTypeSelectionChanged;

        void SetTaskTypes(IEnumerable taskTypes);

        # endregion

        # region Выбор регионов / инспекторов

        bool AllowSelectRegions { set; }

        ILookupViewer Regions { get; }

        bool AllowSelectInspectors { set; }

        ILookupViewer Inspectors { get; }

        # endregion

        # region Настройка набора данных

        event EventHandler CompletedOnScheduleChanged;

        bool CompletedOnShedule
        {
            get;
            set;
        }

        event EventHandler CompletedOverdueChanged;

        bool CompletedOverdue
        {
            get;
            set;
        }

        event EventHandler InProgressOnScheduleChanged;

        bool InProgressOnShedule
        {
            get;
            set;
        }

        event EventHandler InProgressOverdueChanged;

        bool InProgressOverdue
        {
            get;
            set;
        }

        event EventHandler UnassignedChanged;

        bool Unassigned 
        { 
            get; 
            set; 
        }

        bool AllowEditUnassigned 
        { 
            set; 
        }

        bool AllowViewUnassigned
        {
            set;
        }

        # endregion

        # region Генерация отчета

        bool AllowConfirm { set; }

        event EventHandler Confirmed;

        event EventHandler Cancelled;

        # endregion

        # region Открытие/закрытие окна

        bool ShowDialogView();

        void CancelDialog();

        bool GetSaveFileDialog(ref string fileName);

        # endregion
    }
}
