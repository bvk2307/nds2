﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByRegionDataLoader : ServiceProxyBase<UserTaskRegionSummary[]>, IReportDataLoader<UserTaskRegionSummary>
    {
        private readonly IUserTaskReportService _service;

        private readonly int[] _taskTypes;

        private readonly string[] _regions;

        public ReportByRegionDataLoader(
            INotifier notifier,
            IClientLogger logger,
            IUserTaskReportService service,
            int[] taskTypes,
            string[] regions)
            : base(notifier, logger)
        {
            _service = service;
            _taskTypes = taskTypes;
            _regions = regions;
        }

        public bool Load(long reportId, out UserTaskRegionSummary[] data)
        {
            return Invoke(() => _service.ReportByRegion(reportId, _regions, _taskTypes), out data);
        }    
    }
}
