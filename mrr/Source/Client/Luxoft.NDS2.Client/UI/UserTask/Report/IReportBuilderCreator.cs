﻿namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public interface IReportBuilderCreator
    {
        IReportBuilder Create(ReportParametersViewModel model);
    }
}
