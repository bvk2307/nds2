﻿using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(Exception error)
        {
            Error = error;
        }

        public Exception Error
        {
            get;
            private set;
        }
    }

    public interface IReportBuilder
    {
        bool HasData();

        void StartExport(string filename);

        event EventHandler Exported;

        event EventHandler<ErrorEventArgs> Error;
    }
}
