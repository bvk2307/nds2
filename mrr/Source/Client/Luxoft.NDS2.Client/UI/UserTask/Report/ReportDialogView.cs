﻿using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed partial class ReportDialogView : Form, IReportDialogView
    {
        # region .ctor

        public ReportDialogView()
        {
            InitializeComponent();

            _regionsLookup.WithEditor(new UI.Controls.LookupEditor.LookupFull.Editor());
            _inspectorsLookup.WithEditor(new UI.Controls.LookupEditor.LookupFull.Editor());
            _dateFrom.DisableManualInput();
            _dateTo.DisableManualInput();
        }

        # endregion

        # region Период формирования отчета

        public event EventHandler OnDateFromChanged;

        public DateTime DateFrom
        {
            get 
            { 
                return _dateFrom.DateTime; 
            }
            set
            {
                _dateFrom.Value = value;
            }
        }

        private void DateFromValueChanged(object sender, EventArgs e)
        {
            if (OnDateFromChanged != null)
            {
                OnDateFromChanged(sender, e);
            }
        }

        public event EventHandler OnDateToChanged;

        public DateTime DateTo
        {
            get 
            { 
                return _dateTo.DateTime; 
            }
            set
            {
                _dateTo.Value = value;
            }
        }

        private void DateToValueChanged(object sender, EventArgs e)
        {
            if (OnDateToChanged != null)
            {
                OnDateToChanged(sender, e);
            }
        }

        public bool DateToValid 
        {
            set
            {
                _dateTo.SetValidState(value);
                _dateFrom.SetValidState(value);
            }
        }

        public DateTime MaxDate
        {
            set
            {
                _dateFrom.MaxDate = value;
                _dateTo.MaxDate = value;
            }
        }

        # endregion

        # region Выбор отчета (детализация)

        public void SetReports(IEnumerable reports)
        {
            if (reports == null)
            {
                throw new ArgumentNullException("reports");
            }

            _selectReport.SetDataSource(reports);
        }

        public event EventHandler SelectedReportChanged;

        public object SelectedReport
        {
            get 
            { 
                return _selectReport.GetValue(); 
            }
            set 
            {
                _selectReport.SetValue(value);
            }
        }

        private void SelectReportValueChanged(object sender, EventArgs e)
        {
            if (SelectedReportChanged != null)
            {
                SelectedReportChanged(this, new EventArgs());
            }
        }

        # endregion

        # region Типы пользовательских заданий

        public void SetTaskTypes(IEnumerable taskTypes)
        {
            if (taskTypes == null)
            {
                throw new ArgumentNullException("taskTypes");
            }

            var taskTypeIndex = 0;
            _taskTypesArea.ClientArea.Controls.Clear();
            foreach (var taskType in taskTypes)
            {
                var taskTypeCheckBox =
                    new UltraCheckEditor
                    {
                        Text = taskType.ToString(),
                        Checked = false,
                        Tag = taskType,
                        Dock = DockStyle.Bottom,
                        TabIndex = taskTypeIndex
                    };
                taskTypeCheckBox.CheckedChanged += TaskTypeCheckBoxCheckedChanged;
                taskTypeIndex++;
                _taskTypesArea.ClientArea.Controls.Add(taskTypeCheckBox);
            }
            _taskTypesArea.ClientArea.Padding = new Padding(3, 3, 3, 10);
        }

        public event EventHandler<TaskTypeSelectedChanged> TaskTypeSelectionChanged;

        private void TaskTypeCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            var taskItemEditor = (UltraCheckEditor)sender;

            if (TaskTypeSelectionChanged != null)
            {
                TaskTypeSelectionChanged(
                    sender, 
                    new TaskTypeSelectedChanged(taskItemEditor.Checked, taskItemEditor.Tag));
            }
        }

        public IEnumerable<DictionaryItem> SelectedTaskTypes 
        {
            get
            {
                foreach (UltraCheckEditor checkEditor in _taskTypesArea.ClientArea.Controls)
                {
                    if (checkEditor.Checked)
                    {
                        yield return (DictionaryItem)checkEditor.Tag;
                    }
                }
            }
        }

        # endregion

        # region Настройка наборов данных

        public event EventHandler CompletedOnScheduleChanged;

        public bool CompletedOnShedule
        {
            get { return _completedOnSchedule.Checked; }
            set { _completedOnSchedule.Checked = value; }
        }

        private void CompletedOnScheduleCheckedChanged(object sender, EventArgs e)
        {
            if (CompletedOnScheduleChanged != null)
            {
                CompletedOnScheduleChanged(this, e);
            }
        }

        public event EventHandler CompletedOverdueChanged;

        public bool CompletedOverdue
        {
            get { return _completedOverdue.Checked; }
            set { _completedOverdue.Checked = value; }
        }

        private void CompletedOverdueCheckedChanged(object sender, EventArgs e)
        {
            if (CompletedOverdueChanged != null)
            {
                CompletedOverdueChanged(this, e);
            }
        }

        public event EventHandler InProgressOnScheduleChanged;

        public bool InProgressOnShedule
        {
            get { return _inProgressOnSchedule.Checked; }
            set { _inProgressOnSchedule.Checked = value; }
        }

        private void InProgressOnScheduleCheckedChanged(object sender, EventArgs e)
        {
            if (InProgressOnScheduleChanged != null)
            {
                InProgressOnScheduleChanged(this, e);
            }
        }

        public event EventHandler InProgressOverdueChanged;

        public bool InProgressOverdue
        {
            get { return _inProgressOverdue.Checked; }
            set { _inProgressOverdue.Checked = value; }
        }

        private void InProgressOverdueCheckedChanged(object sender, EventArgs e)
        {
            if (InProgressOverdueChanged != null)
            {
                InProgressOverdueChanged(this, e);
            }
        }

        public event EventHandler UnassignedChanged;

        public bool Unassigned
        {
            get 
            { 
                return _unassigned.Checked; 
            }
            set
            {
                _unassigned.Checked = value;
            }
        }

        private void UnassignedCheckedChanged(object sender, EventArgs e)
        {
            if (UnassignedChanged != null)
            {
                UnassignedChanged(sender, e);
            }
        }

        public bool AllowEditUnassigned
        {
            set { _unassigned.Enabled = value; }
        }

        public bool AllowViewUnassigned
        {
            set { _unassigned.Visible = value; }
        }

        # endregion

        # region Выбор Регионов

        public bool AllowSelectRegions
        {
            set
            {
                _regionSelectionArea.Visible = value;
            }
        }

        public ILookupViewer Regions 
        {
            get
            {
                return _regionsLookup;
            }
        }

        # endregion

        # region Выбор инспекторов

        public bool AllowSelectInspectors
        {
            set 
            {
                _inspectorSelectArea.Visible = value; 
            }
        }

        public ILookupViewer Inspectors
        {
            get
            {
                return _inspectorsLookup;
            }
        }

        # endregion       

        # region Генерация отчета

        public bool AllowConfirm
        {
            set 
            { 
                __acceptButton.Enabled = value; 
            }
        }

        public event EventHandler Confirmed;

        private void ConfirmButtonClick(object sender, EventArgs e)
        {
            if (Confirmed != null)
            {
                Confirmed(this, e);
            }
        }

        public event EventHandler Cancelled;

        private void CancelButtonClick(object sender, EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        # endregion

        # region Диалог

        public bool GetSaveFileDialog(ref string fileName)
        {
            return DialogHelper.SelectFileToOverwrite(SelectFileType.XLSX, ref fileName);
        }

        public bool ShowDialogView()
        {
            return ShowDialog() == DialogResult.OK;
        }

        public void CancelDialog()
        {
            DialogResult = DialogResult.Cancel;
        }

        #endregion
    }
}
