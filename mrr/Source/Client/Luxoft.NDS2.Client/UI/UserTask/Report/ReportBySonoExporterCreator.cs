﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public class ReportBySonoExporterCreator : IExporterCreator<CompletedBySono, InProgressBySono, UnassignedSummaryModel>
    {
        private readonly ICatalogService _service;

        public ReportBySonoExporterCreator(ICatalogService service)
        {
            _service = service;
        }

        public IReportExporter<CompletedBySono, InProgressBySono, UnassignedSummaryModel> Create(DateTime startDate, DateTime endDate)
        {
            return new ReportBySonoExporter(startDate, endDate, _service);
        }
    }
}