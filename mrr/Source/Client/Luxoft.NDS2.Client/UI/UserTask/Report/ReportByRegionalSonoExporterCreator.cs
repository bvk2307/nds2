﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByRegionalSonoExporterCreator : IExporterCreator<CompletedByRegionalSono, InProgressByRegionalSono, UnassignedByRegionalSono>
    {
        private readonly ICatalogService _service;

        public ReportByRegionalSonoExporterCreator(ICatalogService service)
        {
            _service = service;
        }

        public IReportExporter<CompletedByRegionalSono, InProgressByRegionalSono, UnassignedByRegionalSono> Create(DateTime startDate, DateTime endDate)
        {
            return new ReportByRegionalSonoExporter(startDate, endDate, _service);
        }
    }
}
