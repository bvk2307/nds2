﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportFinder : ServiceProxyBase<ReportIdSearchResult>, IReportFinder
    {
        private readonly IUserTaskReportService _service;

        public ReportFinder(
            IUserTaskReportService service,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public bool TryGetReportId(DateTime reportDate, out long? id)
        {
            id = null;
            ReportIdSearchResult result;

            if (Invoke(() => _service.GetReportId(reportDate), out result))
            {
                if (result.Status == SearchStatus.NoAggregate)
                {
                    _notifier.ShowError(string.Format("Ошибка при построении отчета на {0:d}, обратитесь в службу поддержки", reportDate));
                }

                if (result.Status == SearchStatus.Found)
                {
                    id = result.Id;
                }
                
                return result.Status != SearchStatus.NoAggregate;
            }

            return false;
        }
    }
}
