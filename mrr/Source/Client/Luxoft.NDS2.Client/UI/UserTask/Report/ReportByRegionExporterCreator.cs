﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByRegionExporterCreator : IExporterCreator<CompletedByRegion, InProgressByRegion, UnassignedByRegion>
    {
        private readonly ICatalogService _service;

        public ReportByRegionExporterCreator(ICatalogService service)
        {
            _service = service;
        }

        public IReportExporter<CompletedByRegion, InProgressByRegion, UnassignedByRegion> Create(DateTime startDate, DateTime endDate)
        {
            return new ReportByRegionExporter(startDate, endDate, _service);
        }
    }
}
