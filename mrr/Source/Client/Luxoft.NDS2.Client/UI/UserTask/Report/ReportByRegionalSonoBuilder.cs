﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByRegionalSonoBuilder : ReportBuilderBase<CompletedByRegionalSono, InProgressByRegionalSono, UnassignedByRegionalSono, UserTaskSonoSummary, Sono>
    {
        private readonly Dictionary<string, Sono> _sonoCodes;

        public ReportByRegionalSonoBuilder(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<UserTaskSonoSummary> dataLoader,
            IExporterCreator<CompletedByRegionalSono, InProgressByRegionalSono, UnassignedByRegionalSono> exporterCreator,
            string regionCode,
            IEnumerable<Sono> allSonoCodes)
            : base(model, reportFinder, dataLoader, exporterCreator)
        {
            var sonoTypes = new SonoType[]{ SonoType.MILarge, SonoType.Ifns };
            _sonoCodes =
                allSonoCodes
                    .Where(x => x.RegionCode == regionCode)
                    .Where(x => sonoTypes.Contains(x.SonoType))
                    .ToDictionary(x => x.Code);
        }

        protected override Dictionary<string, Sono> GetGroups()
        {
            return _sonoCodes;
        }

        protected override string GetGroupKey(UserTaskSonoSummary dataItem)
        {
            return dataItem.SonoCode;
        }

        protected override int GetUnassigned(UserTaskSonoSummary dataItem)
        {
            return dataItem.Unassigned;
        }

        protected override CompletedByRegionalSono CreateCompleted(Sono group, string taskType)
        {
            return new CompletedByRegionalSono(taskType, group);
        }

        protected override InProgressByRegionalSono CreateInProgress(Sono group, string taskType)
        {
            return new InProgressByRegionalSono(taskType, group);
        }

        protected override UnassignedByRegionalSono CreateUnassigned(Sono group, string taskType)
        {
            return new UnassignedByRegionalSono(taskType, group);
        }
    }
}
