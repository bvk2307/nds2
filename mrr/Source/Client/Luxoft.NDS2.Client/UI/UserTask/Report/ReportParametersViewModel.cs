﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models;
using Luxoft.NDS2.Common.Models.AccessContext;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportParametersViewModel
    {
        # region .ctor

        public ReportParametersViewModel(
            IEnumerable<DictionaryItem> allUserTasks,
            IEnumerable<Region> allRegions,
            IInspectorsDataProxy inspectorsDataProxy,
            AccessContext accessContext)
        {
            # region Валидация параметров

            if (accessContext == null)
            {
                throw new ArgumentNullException("accessContext");
            }

            if ((accessContext.Operations == null) || (!accessContext.Operations.Any()))
            {
                throw new ArgumentException(
                    "Отсутствую права на доступ ко всем возможным детализациям отчета", 
                    "accessContext");
            }

            if (allUserTasks == null)
            {
                throw new ArgumentNullException("allUserTasks");
            }

            if (!allUserTasks.Any())
            {
                throw new ArgumentException("Отсутствуют сведения о типах ПЗ", "allUserTasks");
            }

            if (allRegions == null)
            {
                throw new ArgumentNullException("allRegions");
            }

            if (inspectorsDataProxy == null ) 
            {
                throw new ArgumentNullException("inspectorsDataProxy");
            }

            # endregion

            _allTaskTypes = allUserTasks.OrderBy(x => x.Description).ToArray();
            _allRegions = allRegions;
            _inspectorsDataProxy = inspectorsDataProxy;
            _reportTypes = accessContext.ToModel().OrderBy(GetReportTypeIndex).ToArray();

            DateToValid = new Changeable<bool>();

            Regions = new LookupModel(new RegionLookupItem[0]);
            Regions.SelectionChanged += (sender, e) => Validate();
            AllowSelectRegions = new Changeable<bool>();

            Inspectors = new LookupModel(new UserLookupItem[0]);
            Inspectors.SelectionChanged += (sender, e) => Validate();
            AllowSelectInspectors = new Changeable<bool>();

            UnassignedEditable = new Changeable<bool>();
            Unassigned = new Changeable<bool>();

            Unassigned.PropertyChanged += (sender, e) => Validate();
            InitDefauls();
        }

        private void InitDefauls()
        {
            SelectedReport = _reportTypes.First();
            Unassigned.Value = UnassignedEditable.Value;
            MaxDate = DateTime.Now.AddDays(-1).Date;
            DateStart = MaxDate;
            DateEnd = MaxDate;
        }

        # endregion

        #region Сортировка типов отчёта

        private readonly Dictionary<string, int> _reportTypeIndexDict = new Dictionary<string, int>
        {
            {MrrOperations.UserTask.UserTaskReportByCountry, 1},
            {MrrOperations.UserTask.UserTaskReportByRegion, 2},
            {MrrOperations.UserTask.UserTaskReportByInspections, 3}
        };

        private int? GetReportTypeIndex(OperationModel reportType)
        {
            int index;
            if (_reportTypeIndexDict.TryGetValue(reportType.Name, out index))
            {
                return index;
            }
            return null;
        }
        
        #endregion

        # region Разграничение прав на доступ

        public event EventHandler ErrorLoadInspectorList;

        private void UpdateAccess()
        {
            AllowSelectInspectors.Value =
                SelectedReport.Name == MrrOperations.UserTask.UserTaskReportByInspector;

            if (AllowSelectInspectors.Value && Inspectors.Empty())
            {
                try
                {
                    var sonoCode = SelectedReport.GetSonoCodes().First();
                    _inspectors = _inspectorsDataProxy.GetUsers(sonoCode);

                    Inspectors.UpdateDictionary(
                            _inspectors
                            .Select(dto => new UserLookupItem(dto.EmployeeNum, dto.Name))
                            .ToArray());
                }
                catch (Exception e)
                {
                    if (ErrorLoadInspectorList != null)
                    {
                        ErrorLoadInspectorList(e, EventArgs.Empty);
                    }
                }
            }

            AllowSelectRegions.Value =
                new[] { 
                    MrrOperations.UserTask.UserTaskReportByRegion, 
                    MrrOperations.UserTask.UserTaskReportByInspections,
                    MrrOperations.UserTask.UserTaskReportByRegionInspection }
                .Contains(SelectedReport.Name);

            if (AllowSelectRegions.Value)
            {
                var lookupItems = SelectedReport.Intersect(_allRegions).ToLookupItems();
                Regions.UpdateDictionary(lookupItems);

                if (lookupItems.Count() == 1)
                {
                    Regions.SelectAll();
                }
            }

            UnassignedEditable.Value = 
                SelectedReport.Name == MrrOperations.UserTask.UserTaskReportByRegion
               || SelectedReport.Name == MrrOperations.UserTask.UserTaskReportByRegionInspection;
        }

        # endregion

        # region Период формирования отчета

        private DateTime _dateStart;

        public DateTime DateStart
        {
            get
            {
                return _dateStart;
            }
            set
            {
                if (_dateStart != value)
                {
                    _dateStart = value;
                    Validate();
                }
            }
        }

        public DateTime DataStartDate
        {
            get
            {
                return ReportOnSigleDate ? DateStart.AddDays(-1) : DateStart;
            }
        }

        private DateTime _dateEnd;

        public DateTime DateEnd 
        { 
            get
            {
                return _dateEnd;
            }
            set
            {
                if (_dateEnd != value)
                {
                    _dateEnd = value;
                    Validate();
                }
            }
        }

        public Changeable<bool> DateToValid
        {
            get;
            private set;
        }

        public DateTime MaxDate
        {
            get;
            private set;
        }

        public bool ReportOnSigleDate
        {
            get
            {
                return DateEnd.Date == DateStart.Date;
            }
        }

        public DateTime CompletedDateStart
        {
            get
            {
                return DateStart.AddDays(-1);
            }
        }

        # endregion

        # region Уровни детализации отчета

        private readonly OperationModel[] _reportTypes;

        public IEnumerable<OperationModel> AvailableReports 
        {
            get
            {
                return _reportTypes;
            }
        }

        private OperationModel _selectedReport;

        public OperationModel SelectedReport 
        {
            get
            {
                return _selectedReport;
            }
            set
            {
                if (_selectedReport != value)
                {
                    _selectedReport = value;
                    Unassigned.Value = false;
                    Regions.SelectNone();
                    UpdateAccess();
                    Validate();
                }
            }
        }

        # endregion

        # region Фильтр по типам пользовательских заданий

        private readonly IEnumerable<DictionaryItem> _allTaskTypes;

        private readonly List<DictionaryItem> _selectedTaskTypes = new List<DictionaryItem>();

        public IEnumerable<DictionaryItem> AllTaskTypes
        {
            get
            {
                return _allTaskTypes;
            }
        }

        public IEnumerable<DictionaryItem> SelectedTaskTypes
        {
            get 
            { 
                return _allTaskTypes.Where(x => _selectedTaskTypes.Any(y => y.Id == x.Id)); 
            }
        }

        public void SetTaskTypeSelected(DictionaryItem taskType, bool selected)
        {
            if (!_allTaskTypes.Contains(taskType))
            {
                throw new ArgumentException("Недопустимый тип ПЗ", "taskType");
            }

            if (selected)
            {
                _selectedTaskTypes.Add(taskType);
            }
            else if (_selectedTaskTypes.Contains(taskType))
            {
                _selectedTaskTypes.Remove(taskType);
            }
            Validate();
        }

        public int[] GetTaskTypeIds()
        {
            return _selectedTaskTypes.Select(x => x.Id).ToArray();
        }

        # endregion

        # region Фильтр по колонкам отчета

        private bool _completedOnSchedule = true;

        public bool CompletedOnSchedule 
        {
            get
            {
                return _completedOnSchedule;
            }
            set
            {
                if (_completedOnSchedule != value)
                {
                    _completedOnSchedule = value;
                    Validate();
                }
            }
        }

        private bool _completedOverdue = true;

        public bool CompletedOverdue 
        {
            get
            {
                return _completedOverdue;
            }
            set
            {
                if (_completedOverdue != value)
                {
                    _completedOverdue = value;
                    Validate();
                }
            }
        }

        private bool _inProgressOnSchedule = true;

        public bool InProgressOnSchedule 
        {
            get
            {
                return _inProgressOnSchedule;
            }
            set
            {
                if (_inProgressOnSchedule != value)
                {
                    _inProgressOnSchedule = value;
                    Validate();
                }
            }
        }

        private bool _inProgressOverdue = true;

        public bool InProgressOverdue 
        {
            get
            {
                return _inProgressOverdue;
            }
            set
            {
                if (_inProgressOverdue != value)
                {
                    _inProgressOverdue = value;
                    Validate();
                }
            }
        }

        public Changeable<bool> Unassigned
        {
            get;
            private set;
        }

        public Changeable<bool> UnassignedEditable
        {
            get;
            private set;
        }

        public bool IncludeCompleted
        {
            get
            {
                return CompletedOnSchedule || CompletedOverdue;
            }
        }

        public bool IncludeInProgress
        {
            get
            {
                return InProgressOnSchedule || InProgressOverdue;
            }
        }

        public bool IncludeInProgressOrUnassigned
        {
            get
            {
                return IncludeInProgress || Unassigned.Value;
            }
        }

        public bool UnassignedAvailable
        {
            get
            {
                return _reportTypes.Any(
                    x => x.Name == MrrOperations.UserTask.UserTaskReportByRegion
                    || x.Name == MrrOperations.UserTask.UserTaskReportByRegionInspection);
            }
        }

        # endregion

        # region Фильтр по Регионам

        private IEnumerable<Region> _allRegions;

        public LookupModel Regions
        {
            get;
            private set;
        }

        public string[] GetRegionCodes()
        {
            return Regions.Selection.Select(x => x.Title).ToArray();
        }

        public Changeable<bool> AllowSelectRegions
        {
            get;
            private set;
        }

        # endregion

        # region Фильтр по инспекторам

        private readonly IInspectorsDataProxy _inspectorsDataProxy;

        public Changeable<bool> AllowSelectInspectors
        {
            get;
            private set;
        }

        private IEnumerable<UserInformation> _inspectors;

        public LookupModel Inspectors 
        { 
            get; 
            private set; 
        }

        private IEnumerable<UserInformation> SelectedUsers()
        {
            if (_inspectors == null)
            {
                return Enumerable.Empty<UserInformation>();
            }

            return _inspectors
                .Where(x => Inspectors.Selection.Any(y => x.EmployeeNum == y.Title));
        }

        public string[] GetSelectedUserSids()
        {
            return SelectedUsers().Select(x => x.Sid).ToArray();
        }

        public Dictionary<string, string> GetSelectedUsers()
        {
            return SelectedUsers().ToDictionary(x => x.Sid, x => x.Name);
        }

        # endregion

        # region Валидация

        public event EventHandler ValidChanged;

        private void RaiseValidChanged()
        {
            if (ValidChanged != null)
            {
                ValidChanged(this, new EventArgs());
            }
        }

        private bool _valid;

        public bool Valid
        {
            get
            {
                return _valid;
            }
            private set
            {
                if (_valid != value)
                {
                    _valid = value;
                    RaiseValidChanged();
                }
            }
        }

        private void Validate()
        {
            DateToValid.Value = DateStart <= DateEnd;
            Valid = DateToValid.Value
                &&  SelectedTaskTypes.Any()
                && (!AllowSelectRegions.Value || Regions.AnyChecked)
                && (!AllowSelectInspectors.Value || Inspectors.AnyChecked)
                && AnyTabSelected();
        }

        private bool AnyTabSelected()
        {
            return Unassigned.Value
                || CompletedOnSchedule
                || CompletedOverdue
                || InProgressOnSchedule
                || InProgressOverdue;
        }

        # endregion
    }

    public static class RegionConvertor
    {
        private static RegionLookupItem ToLookupItem(this Region regionData)
        {
            return new RegionLookupItem(regionData.Code, regionData.Name);
        }

        public static RegionLookupItem[] ToLookupItems(this IEnumerable<Region> regions)
        {
            return regions.Select(dto => dto.ToLookupItem()).ToArray();
        }  
    }
}
