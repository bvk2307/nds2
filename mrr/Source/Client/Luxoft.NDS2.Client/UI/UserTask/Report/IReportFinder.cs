﻿using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public interface IReportFinder
    {
        bool TryGetReportId(DateTime reportDate, out long? id);
    }
}
