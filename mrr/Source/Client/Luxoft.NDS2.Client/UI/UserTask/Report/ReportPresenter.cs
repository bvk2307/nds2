﻿using System;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Models.AccessContext;
using Luxoft.NDS2.Client.Infrastructure;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportPresenter
    {
        # region .ctor

        public ReportPresenter(
            IReportDialogView view, 
            ReportParametersViewModel model, 
            IReportBuilderCreator reportCreator,
            INotifier notifier,
            IQuestionBox questionBox,
            IClientLogger logger,
            IFileOpener fileOpener)
        {
            _view = view;
            _viewModel = model;
            _reportCreator = reportCreator;
            _notifier = notifier;
            _logger = logger;
            _quetionBox = questionBox;
            _fileOpener = fileOpener;

            var regionsPresenter = new LookupPresenter(_view.Regions, _viewModel.Regions);
            var inspectorsPresenter = new LookupPresenter(_view.Inspectors, _viewModel.Inspectors);

            InitView();
            BindView();
            BindModel();
        }

        # endregion

        # region View

        private readonly IReportDialogView _view;

        private void InitView()
        {
            _view.MaxDate = _viewModel.MaxDate;
            _view.DateFrom = _viewModel.DateStart;
            _view.DateTo = _viewModel.DateEnd;            
            _view.CompletedOnShedule = _viewModel.CompletedOnSchedule;
            _view.CompletedOverdue = _viewModel.CompletedOverdue;
            _view.InProgressOnShedule = _viewModel.InProgressOnSchedule;
            _view.InProgressOverdue = _viewModel.InProgressOverdue;
            _view.Unassigned = _viewModel.Unassigned.Value;
            _view.AllowViewUnassigned = _viewModel.UnassignedAvailable;

            _view.SetReports(_viewModel.AvailableReports);
            _view.SetTaskTypes(_viewModel.AllTaskTypes);

            _view.SelectedReport = _viewModel.SelectedReport;

            SetDateToValid();
            AllowSelectInspectorChanged();
            AllowSelectRegionsChanged();
            AllowGenerateReport();
            SetAllowUnassigned();
            SetUnassignedToView();
        }

        private void BindView()
        {
            _view.SelectedReportChanged += ReportChanged;
            _view.UnassignedChanged += ViewUnassginedChanged;
            _view.Confirmed += Confirmed;
            _view.OnDateFromChanged += ViewOnDateStartChanged;
            _view.OnDateToChanged += ViewOnDateEndChanged;
            _view.Cancelled += Cancelled;
            _view.TaskTypeSelectionChanged += ViewTaskTypeCheckBoxChanged;
            _view.CompletedOnScheduleChanged += ViewCompletedOnScheduleChanged;
            _view.CompletedOverdueChanged += ViewCompletedOverdueChanged;
            _view.InProgressOnScheduleChanged += ViewInProgressOnScheduleChanged;
            _view.InProgressOverdueChanged += ViewInProgressOverdueChanged;
        }

        private void ViewTaskTypeCheckBoxChanged(object sender, TaskTypeSelectedChanged e)
        {
            _viewModel.SetTaskTypeSelected((DictionaryItem)e.TaskType, e.IsSelected);
        }

        private void ViewUnassginedChanged(object sender, EventArgs e)
        {
            _viewModel.Unassigned.Value = _view.Unassigned;
        }

        private void ViewOnDateEndChanged(object sender, EventArgs e)
        {
            _viewModel.DateEnd = _view.DateTo;
        }

        private void ViewOnDateStartChanged(object sender, EventArgs e)
        {
            _viewModel.DateStart = _view.DateFrom;
        }

        private void ReportChanged(object sender, EventArgs e)
        {
            _viewModel.SelectedReport = (OperationModel)_view.SelectedReport;
        }

        private void ViewCompletedOnScheduleChanged(object sender, EventArgs e)
        {
            _viewModel.CompletedOnSchedule = _view.CompletedOnShedule;
        }

        private void ViewCompletedOverdueChanged(object sender, EventArgs e)
        {
            _viewModel.CompletedOverdue = _view.CompletedOverdue;
        }

        private void ViewInProgressOnScheduleChanged(object sender, EventArgs e)
        {
            _viewModel.InProgressOnSchedule = _view.InProgressOnShedule;
        }

        private void ViewInProgressOverdueChanged(object sender, EventArgs e)
        {
            _viewModel.InProgressOverdue = _view.InProgressOverdue;
        }

        private void Confirmed(object sender, EventArgs e)
        {
            GenerateReport();
        }

        # endregion

        # region ViewModel

        private readonly ReportParametersViewModel _viewModel;

        private void BindModel()
        {
            _viewModel.Unassigned.PropertyChanged += (sender, e) => SetUnassignedToView();
            _viewModel.UnassignedEditable.PropertyChanged += (sernder, e) => SetAllowUnassigned();
            _viewModel.AllowSelectInspectors.PropertyChanged += (sernder, e) => AllowSelectInspectorChanged();
            _viewModel.AllowSelectRegions.PropertyChanged += (sernder, e) => AllowSelectRegionsChanged();
            _viewModel.ValidChanged += (sender, e) => AllowGenerateReport();
            _viewModel.DateToValid.PropertyChanged += (sender, e) => SetDateToValid();
            _viewModel.ErrorLoadInspectorList += ErrorLoadInspectorList;
        }

        private void SetUnassignedToView()
        {
            _view.Unassigned = _viewModel.Unassigned.Value;
        }

        private void SetAllowUnassigned()
        {
            _view.AllowEditUnassigned = _viewModel.UnassignedEditable.Value;
        }

        private void AllowSelectRegionsChanged()
        {
            _view.AllowSelectRegions = _viewModel.AllowSelectRegions.Value;
        }

        private void AllowSelectInspectorChanged()
        {
            _view.AllowSelectInspectors = _viewModel.AllowSelectInspectors.Value;
        }

        private void AllowGenerateReport()
        {
            _view.AllowConfirm = _viewModel.Valid;
        }

        private void ErrorLoadInspectorList(object sender, EventArgs e)
        {
        }

        private void SetDateToValid()
        {
            _view.DateToValid = _viewModel.DateToValid.Value;
        }
        
        # endregion

        # region Открытие дилогового окна

        public bool ShowDialog()
        {
            return _view.ShowDialogView();
        }

        # endregion   

        # region Генерация отчета

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        private readonly IFileOpener _fileOpener;

        private readonly IQuestionBox _quetionBox;

        private readonly IReportBuilderCreator _reportCreator;

        private void Cancelled(object sender, EventArgs e)
        {
            _view.CancelDialog();
        }

        private void GenerateReport()
        {
            var fileName =
                string.Format(
                    "{0:yyyyMMdd_hh_mm}_Отчет ПЗ_{1}.xlsx",
                    DateTime.Now,
                    _viewModel.SelectedReport.Description);
            if (!_view.GetSaveFileDialog(ref fileName) 
                || string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            var reportBuilder = _reportCreator.Create(_viewModel);
            reportBuilder.Exported += (sender, e) => ExportCompleted(fileName);
            reportBuilder.Error += (sender, e) =>
                {
                    _logger.LogError(e.Error);
                    _notifier.ShowError("Ошибка формирования отчета");
                };

            if (!reportBuilder.HasData())
            {
                return;
            }

            reportBuilder.StartExport(fileName);
            _view.CancelDialog();
        }

        private void ExportCompleted(string fileName)
        {
            if (_quetionBox.Ask(
                "Отчет о выполнении ПЗ", 
                string.Format("Файл \"{0}\" сформирован. Открыть файл?", fileName)))
            {
                try
                {
                    _fileOpener.Open(fileName);
                }
                catch (Exception error)
                {
                    _notifier.ShowError(string.Format("Не удалось открыть файл {0}", fileName));
                    _logger.LogError(error, "UserTaskExport.OpenFile");
                }
            }
        }

        # endregion
    }
}
