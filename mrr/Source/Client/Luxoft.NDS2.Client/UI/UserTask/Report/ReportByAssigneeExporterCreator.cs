﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByAssigneeExporterCreator : IExporterCreator<CompletedByAssignee, InProgressByAssignee, UnassignedSummaryModel>
    {
        private readonly ICatalogService _catalogService;

        public ReportByAssigneeExporterCreator(ICatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        public IReportExporter<CompletedByAssignee, InProgressByAssignee, UnassignedSummaryModel> Create(DateTime startDate, DateTime endDate)
        {
            return new ReportByAssigneeExporter(startDate, endDate, _catalogService);
        }
    }
}
