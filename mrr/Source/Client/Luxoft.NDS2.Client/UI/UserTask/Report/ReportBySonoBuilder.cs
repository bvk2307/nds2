﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportBySonoBuilder : ReportBuilderBase<CompletedBySono, InProgressBySono, UnassignedSummaryModel, UserTaskSonoSummary, Sono>
    {
        private readonly Dictionary<string, Region> _regions;

        private readonly Dictionary<string, Sono> _sonoCodes;

        public ReportBySonoBuilder(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<UserTaskSonoSummary> dataLoader,
            IExporterCreator<CompletedBySono, InProgressBySono, UnassignedSummaryModel> exportCreator,
            Region[] allRegions,
            Sono[] allSono)
            : base(model, reportFinder, dataLoader, exportCreator)
        {
            _regions =
                allRegions
                    .Where(x => model.Regions.IsChecked(x.Code))
                    .ToDictionary(x => x.Code);
            _sonoCodes =
                allSono
                    .Where(x => x.SonoType == SonoType.Ifns || x.SonoType == SonoType.MILarge)
                    .Where(x => _regions.ContainsKey(x.RegionCode))
                    .ToDictionary(x => x.Code);
        }

        protected override Dictionary<string, Sono> GetGroups()
        {
            return _sonoCodes;
        }

        protected override string GetGroupKey(UserTaskSonoSummary dataItem)
        {
            return dataItem.SonoCode;
        }

        protected override int GetUnassigned(UserTaskSonoSummary dataItem)
        {
            throw new NotSupportedException();
        }

        protected override CompletedBySono CreateCompleted(Sono group, string taskType)
        {
            return new CompletedBySono(taskType, _regions[group.RegionCode], group);
        }

        protected override InProgressBySono CreateInProgress(Sono group, string taskType)
        {
            return new InProgressBySono(taskType, _regions[group.RegionCode], group);
        }

        protected override UnassignedSummaryModel CreateUnassigned(Sono group, string taskType)
        {
            throw new NotSupportedException();
        }
    }
}

