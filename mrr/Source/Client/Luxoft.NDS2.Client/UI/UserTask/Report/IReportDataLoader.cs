﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public interface IReportDataLoader<TDto>
        where TDto : UserTaskSummary
    {
        bool Load(long reportId, out TDto[] data);
    }
}
