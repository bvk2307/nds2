﻿namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    partial class ReportDialogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPeriod;
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraButton _cancelButton;
            Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTask;
            Infragistics.Win.Misc.UltraLabel ultraLabelTaskUnAssigned;
            Infragistics.Win.Misc.UltraLabel ultraLabelTaskInProgress;
            Infragistics.Win.Misc.UltraLabel ultraLabelTaskCompleted;
            this._selectReport = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._dateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._dateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelTo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelFrom = new Infragistics.Win.Misc.UltraLabel();
            this._unassigned = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._inProgressOverdue = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._inProgressOnSchedule = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._completedOverdue = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._completedOnSchedule = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.@__acceptButton = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBoxTaskKind = new Infragistics.Win.Misc.UltraGroupBox();
            this._taskTypesArea = new Infragistics.Win.Misc.UltraPanel();
            this._regionSelectionArea = new Infragistics.Win.Misc.UltraGroupBox();
            this._regionsLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            this._inspectorSelectArea = new Infragistics.Win.Misc.UltraGroupBox();
            this._inspectorsLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            ultraGroupBoxPeriod = new Infragistics.Win.Misc.UltraGroupBox();
            _cancelButton = new Infragistics.Win.Misc.UltraButton();
            ultraGroupBoxTask = new Infragistics.Win.Misc.UltraGroupBox();
            ultraLabelTaskUnAssigned = new Infragistics.Win.Misc.UltraLabel();
            ultraLabelTaskInProgress = new Infragistics.Win.Misc.UltraLabel();
            ultraLabelTaskCompleted = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(ultraGroupBoxPeriod)).BeginInit();
            ultraGroupBoxPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._selectReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ultraGroupBoxTask)).BeginInit();
            ultraGroupBoxTask.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTaskKind)).BeginInit();
            this.ultraGroupBoxTaskKind.SuspendLayout();
            this._taskTypesArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._regionSelectionArea)).BeginInit();
            this._regionSelectionArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inspectorSelectArea)).BeginInit();
            this._inspectorSelectArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBoxPeriod
            // 
            ultraGroupBoxPeriod.Controls.Add(this._selectReport);
            ultraGroupBoxPeriod.Controls.Add(this._dateTo);
            ultraGroupBoxPeriod.Controls.Add(this._dateFrom);
            ultraGroupBoxPeriod.Controls.Add(this.ultraLabel1);
            ultraGroupBoxPeriod.Controls.Add(this.ultraLabelTo);
            ultraGroupBoxPeriod.Controls.Add(this.ultraLabelFrom);
            ultraGroupBoxPeriod.Dock = System.Windows.Forms.DockStyle.Top;
            ultraGroupBoxPeriod.Location = new System.Drawing.Point(0, 0);
            ultraGroupBoxPeriod.Name = "ultraGroupBoxPeriod";
            ultraGroupBoxPeriod.Size = new System.Drawing.Size(574, 59);
            ultraGroupBoxPeriod.TabIndex = 0;
            ultraGroupBoxPeriod.Text = "Период и детализация:";
            // 
            // _selectReport
            // 
            this._selectReport.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._selectReport.Location = new System.Drawing.Point(409, 27);
            this._selectReport.Name = "_selectReport";
            this._selectReport.Size = new System.Drawing.Size(144, 21);
            this._selectReport.TabIndex = 2;
            this._selectReport.ValueChanged += new System.EventHandler(this.SelectReportValueChanged);
            // 
            // _dateTo
            // 
            this._dateTo.AutoFillDate = Infragistics.Win.UltraWinMaskedEdit.AutoFillDate.MonthAndYear;
            this._dateTo.DateTime = new System.DateTime(2017, 3, 4, 0, 0, 0, 0);
            this._dateTo.Location = new System.Drawing.Point(180, 29);
            this._dateTo.Name = "_dateTo";
            this._dateTo.Size = new System.Drawing.Size(115, 21);
            this._dateTo.TabIndex = 1;
            this._dateTo.Value = new System.DateTime(2017, 3, 4, 0, 0, 0, 0);
            this._dateTo.ValueChanged += new System.EventHandler(this.DateToValueChanged);
            // 
            // _dateFrom
            // 
            this._dateFrom.AutoFillDate = Infragistics.Win.UltraWinMaskedEdit.AutoFillDate.MonthAndYear;
            this._dateFrom.DateTime = new System.DateTime(2017, 3, 4, 0, 0, 0, 0);
            this._dateFrom.Location = new System.Drawing.Point(27, 29);
            this._dateFrom.Name = "_dateFrom";
            this._dateFrom.Nullable = false;
            this._dateFrom.Size = new System.Drawing.Size(115, 21);
            this._dateFrom.TabIndex = 0;
            this._dateFrom.Value = new System.DateTime(2017, 3, 4, 0, 0, 0, 0);
            this._dateFrom.ValueChanged += new System.EventHandler(this.DateFromValueChanged);
            // 
            // ultraLabel1
            // 
            appearance27.TextHAlignAsString = "Right";
            this.ultraLabel1.Appearance = appearance27;
            this.ultraLabel1.Location = new System.Drawing.Point(323, 29);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(79, 20);
            this.ultraLabel1.TabIndex = 12;
            this.ultraLabel1.Text = "Детализация:";
            // 
            // ultraLabelTo
            // 
            appearance1.TextHAlignAsString = "Center";
            this.ultraLabelTo.Appearance = appearance1;
            this.ultraLabelTo.Location = new System.Drawing.Point(153, 29);
            this.ultraLabelTo.Name = "ultraLabelTo";
            this.ultraLabelTo.Size = new System.Drawing.Size(20, 20);
            this.ultraLabelTo.TabIndex = 10;
            this.ultraLabelTo.Text = "по";
            // 
            // ultraLabelFrom
            // 
            appearance14.TextHAlignAsString = "Right";
            this.ultraLabelFrom.Appearance = appearance14;
            this.ultraLabelFrom.Location = new System.Drawing.Point(6, 29);
            this.ultraLabelFrom.Name = "ultraLabelFrom";
            this.ultraLabelFrom.Size = new System.Drawing.Size(14, 21);
            this.ultraLabelFrom.TabIndex = 8;
            this.ultraLabelFrom.Text = "с";
            // 
            // _cancelButton
            // 
            _cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            _cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            _cancelButton.Location = new System.Drawing.Point(456, 398);
            _cancelButton.Name = "_cancelButton";
            _cancelButton.Size = new System.Drawing.Size(106, 25);
            _cancelButton.TabIndex = 28;
            _cancelButton.Text = "Отменить";
            _cancelButton.Click += new System.EventHandler(this.CancelButtonClick);
            // 
            // ultraGroupBoxTask
            // 
            ultraGroupBoxTask.Controls.Add(this._unassigned);
            ultraGroupBoxTask.Controls.Add(this._inProgressOverdue);
            ultraGroupBoxTask.Controls.Add(this._inProgressOnSchedule);
            ultraGroupBoxTask.Controls.Add(this._completedOverdue);
            ultraGroupBoxTask.Controls.Add(this._completedOnSchedule);
            ultraGroupBoxTask.Controls.Add(ultraLabelTaskUnAssigned);
            ultraGroupBoxTask.Controls.Add(ultraLabelTaskInProgress);
            ultraGroupBoxTask.Controls.Add(ultraLabelTaskCompleted);
            ultraGroupBoxTask.Dock = System.Windows.Forms.DockStyle.Top;
            ultraGroupBoxTask.Location = new System.Drawing.Point(0, 159);
            ultraGroupBoxTask.Name = "ultraGroupBoxTask";
            ultraGroupBoxTask.Size = new System.Drawing.Size(574, 100);
            ultraGroupBoxTask.TabIndex = 2;
            ultraGroupBoxTask.Text = "Задание:";
            // 
            // _unassigned
            // 
            this._unassigned.Location = new System.Drawing.Point(132, 66);
            this._unassigned.Name = "_unassigned";
            this._unassigned.Size = new System.Drawing.Size(100, 20);
            this._unassigned.TabIndex = 24;
            this._unassigned.Text = "          ";
            this._unassigned.CheckedChanged += new System.EventHandler(this.UnassignedCheckedChanged);
            // 
            // _inProgressOverdue
            // 
            this._inProgressOverdue.Location = new System.Drawing.Point(238, 45);
            this._inProgressOverdue.Name = "_inProgressOverdue";
            this._inProgressOverdue.Size = new System.Drawing.Size(100, 20);
            this._inProgressOverdue.TabIndex = 23;
            this._inProgressOverdue.Text = "Срок истек";
            this._inProgressOverdue.CheckedChanged += new System.EventHandler(this.InProgressOverdueCheckedChanged);
            // 
            // _inProgressOnSchedule
            // 
            this._inProgressOnSchedule.Location = new System.Drawing.Point(132, 45);
            this._inProgressOnSchedule.Name = "_inProgressOnSchedule";
            this._inProgressOnSchedule.Size = new System.Drawing.Size(100, 20);
            this._inProgressOnSchedule.TabIndex = 22;
            this._inProgressOnSchedule.Text = "В срок";
            this._inProgressOnSchedule.CheckedChanged += new System.EventHandler(this.InProgressOnScheduleCheckedChanged);
            // 
            // _completedOverdue
            // 
            this._completedOverdue.Location = new System.Drawing.Point(238, 24);
            this._completedOverdue.Name = "_completedOverdue";
            this._completedOverdue.Size = new System.Drawing.Size(100, 20);
            this._completedOverdue.TabIndex = 21;
            this._completedOverdue.Text = "Срок истек";
            this._completedOverdue.CheckedChanged += new System.EventHandler(this.CompletedOverdueCheckedChanged);
            // 
            // _completedOnSchedule
            // 
            this._completedOnSchedule.Location = new System.Drawing.Point(132, 24);
            this._completedOnSchedule.Name = "_completedOnSchedule";
            this._completedOnSchedule.Size = new System.Drawing.Size(100, 20);
            this._completedOnSchedule.TabIndex = 20;
            this._completedOnSchedule.Text = "В срок";
            this._completedOnSchedule.CheckedChanged += new System.EventHandler(this.CompletedOnScheduleCheckedChanged);
            // 
            // ultraLabelTaskUnAssigned
            // 
            ultraLabelTaskUnAssigned.Location = new System.Drawing.Point(26, 69);
            ultraLabelTaskUnAssigned.Name = "ultraLabelTaskUnAssigned";
            ultraLabelTaskUnAssigned.Size = new System.Drawing.Size(100, 21);
            ultraLabelTaskUnAssigned.TabIndex = 2;
            ultraLabelTaskUnAssigned.Text = "Не назначено:";
            // 
            // ultraLabelTaskInProgress
            // 
            ultraLabelTaskInProgress.Location = new System.Drawing.Point(26, 48);
            ultraLabelTaskInProgress.Name = "ultraLabelTaskInProgress";
            ultraLabelTaskInProgress.Size = new System.Drawing.Size(100, 18);
            ultraLabelTaskInProgress.TabIndex = 1;
            ultraLabelTaskInProgress.Text = "На исполнении:";
            // 
            // ultraLabelTaskCompleted
            // 
            ultraLabelTaskCompleted.Location = new System.Drawing.Point(26, 27);
            ultraLabelTaskCompleted.Name = "ultraLabelTaskCompleted";
            ultraLabelTaskCompleted.Size = new System.Drawing.Size(100, 18);
            ultraLabelTaskCompleted.TabIndex = 0;
            ultraLabelTaskCompleted.Text = "Выполнено:";
            // 
            // __acceptButton
            // 
            this.@__acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.@__acceptButton.Location = new System.Drawing.Point(344, 398);
            this.@__acceptButton.Name = "__acceptButton";
            this.@__acceptButton.Size = new System.Drawing.Size(106, 25);
            this.@__acceptButton.TabIndex = 27;
            this.@__acceptButton.Text = "Сформировать";
            this.@__acceptButton.Click += new System.EventHandler(this.ConfirmButtonClick);
            // 
            // ultraGroupBoxTaskKind
            // 
            this.ultraGroupBoxTaskKind.Controls.Add(this._taskTypesArea);
            this.ultraGroupBoxTaskKind.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBoxTaskKind.Location = new System.Drawing.Point(0, 59);
            this.ultraGroupBoxTaskKind.Name = "ultraGroupBoxTaskKind";
            this.ultraGroupBoxTaskKind.Size = new System.Drawing.Size(574, 100);
            this.ultraGroupBoxTaskKind.TabIndex = 1;
            this.ultraGroupBoxTaskKind.Text = "Тип задания:";
            // 
            // _taskTypesArea
            // 
            this._taskTypesArea.AutoScroll = true;
            this._taskTypesArea.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._taskTypesArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taskTypesArea.Location = new System.Drawing.Point(3, 16);
            this._taskTypesArea.Name = "_taskTypesArea";
            this._taskTypesArea.Size = new System.Drawing.Size(568, 81);
            this._taskTypesArea.TabIndex = 0;
            // 
            // _regionSelectionArea
            // 
            this._regionSelectionArea.Controls.Add(this._regionsLookup);
            this._regionSelectionArea.Dock = System.Windows.Forms.DockStyle.Top;
            this._regionSelectionArea.Location = new System.Drawing.Point(0, 259);
            this._regionSelectionArea.Name = "_regionSelectionArea";
            this._regionSelectionArea.Size = new System.Drawing.Size(574, 59);
            this._regionSelectionArea.TabIndex = 3;
            this._regionSelectionArea.Text = "Регион:";
            // 
            // _regionsLookup
            // 
            this._regionsLookup.Location = new System.Drawing.Point(26, 26);
            this._regionsLookup.Name = "_regionsLookup";
            this._regionsLookup.Size = new System.Drawing.Size(535, 25);
            this._regionsLookup.TabIndex = 25;
            // 
            // _inspectorSelectArea
            // 
            this._inspectorSelectArea.Controls.Add(this._inspectorsLookup);
            this._inspectorSelectArea.Dock = System.Windows.Forms.DockStyle.Top;
            this._inspectorSelectArea.Location = new System.Drawing.Point(0, 318);
            this._inspectorSelectArea.Name = "_inspectorSelectArea";
            this._inspectorSelectArea.Size = new System.Drawing.Size(574, 59);
            this._inspectorSelectArea.TabIndex = 4;
            this._inspectorSelectArea.Text = "Ф.И.О. инспектора:";
            // 
            // _inspectorsLookup
            // 
            this._inspectorsLookup.Location = new System.Drawing.Point(26, 26);
            this._inspectorsLookup.Name = "_inspectorsLookup";
            this._inspectorsLookup.Size = new System.Drawing.Size(535, 25);
            this._inspectorsLookup.TabIndex = 26;
            // 
            // ReportDialogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = _cancelButton;
            this.ClientSize = new System.Drawing.Size(574, 435);
            this.Controls.Add(this._inspectorSelectArea);
            this.Controls.Add(this._regionSelectionArea);
            this.Controls.Add(ultraGroupBoxTask);
            this.Controls.Add(this.ultraGroupBoxTaskKind);
            this.Controls.Add(ultraGroupBoxPeriod);
            this.Controls.Add(_cancelButton);
            this.Controls.Add(this.@__acceptButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportDialogView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Отчет о выполнении ПЗ";
            ((System.ComponentModel.ISupportInitialize)(ultraGroupBoxPeriod)).EndInit();
            ultraGroupBoxPeriod.ResumeLayout(false);
            ultraGroupBoxPeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._selectReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ultraGroupBoxTask)).EndInit();
            ultraGroupBoxTask.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTaskKind)).EndInit();
            this.ultraGroupBoxTaskKind.ResumeLayout(false);
            this._taskTypesArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._regionSelectionArea)).EndInit();
            this._regionSelectionArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inspectorSelectArea)).EndInit();
            this._inspectorSelectArea.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton __acceptButton;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabelTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabelFrom;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTaskKind;
        private Infragistics.Win.Misc.UltraGroupBox _regionSelectionArea;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _unassigned;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _inProgressOverdue;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _inProgressOnSchedule;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _completedOverdue;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _completedOnSchedule;
        private Infragistics.Win.Misc.UltraGroupBox _inspectorSelectArea;
        private Controls.LookupEditor.LookupBase _regionsLookup;
        private Controls.LookupEditor.LookupBase _inspectorsLookup;
        private Infragistics.Win.Misc.UltraPanel _taskTypesArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _dateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _dateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _selectReport;
    }
}