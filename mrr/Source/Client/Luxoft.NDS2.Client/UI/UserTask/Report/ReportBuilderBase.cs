﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.UserTask;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public abstract class ReportBuilderBase<TCompleted,TInProgress,TUnassigned,TDto,TGroup> : IReportBuilder
        where TCompleted : CompletedSummaryModel
        where TInProgress : InProgressSummaryModel
        where TUnassigned : UnassignedSummaryModel
        where TDto : UserTaskSummary
    {
        # region .ctor

        protected ReportBuilderBase(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<TDto> dataLoader,
            IExporterCreator<TCompleted, TInProgress, TUnassigned> exporterCreator)
        {
            _model = model;
            _dataLoader = dataLoader;
            _exporterCreator = exporterCreator;
            _reportFinder = reportFinder;
        }

        # endregion

        # region Построение отчета

        protected readonly ReportParametersViewModel _model;

        private readonly IReportFinder _reportFinder;

        private readonly IReportDataLoader<TDto> _dataLoader;

        private readonly IExporterCreator<TCompleted, TInProgress, TUnassigned> _exporterCreator;

        private long? _completedStartReportId;

        private long? _inProgressStartReportId;

        private long? _endReportId;

        /// <summary>
        /// Запрашивает данные для построения отчета, производит расчеты (группировку) и экспортирует данные в Excel
        /// </summary>
        /// <param name="filename">Имя excel файла</param>
        /// <returns>Признак успешности выполнения экспорта</returns>
        public bool Build(string filename)
        {
            if (!_dataVerified)
            {
                return HasData();
            }

            TDto[] _dataCompletedStart;
            TDto[] _dataInProgressStart;
            TDto[] _dataEnd;           

            if (!GetData(_completedStartReportId, out _dataCompletedStart)
                || !GetData(_inProgressStartReportId, out _dataInProgressStart)
                || !GetData(_endReportId, out _dataEnd))
            {
                return false;
            }

            Dictionary<string, TGroup> groups = GetGroups();
            var completedDictionary = new Dictionary<string, Dictionary<int, TCompleted>>();
            var inProgressDictionary = new Dictionary<string, Dictionary<int, TInProgress>>();
            var unassignedDictionary = new Dictionary<string, Dictionary<int, TUnassigned>>();
            var completedList = new List<TCompleted>();
            var inProgressList = new List<TInProgress>();
            var unassginedList = new List<TUnassigned>();

            foreach (var group in groups)
            {
                completedDictionary.Add(group.Key, new Dictionary<int, TCompleted>());
                inProgressDictionary.Add(group.Key, new Dictionary<int, TInProgress>());
                unassignedDictionary.Add(group.Key, new Dictionary<int, TUnassigned>());

                foreach (var task in _model.SelectedTaskTypes)
                {
                    var completed = CreateCompleted(group.Value, task.Description);
                    completedDictionary[group.Key].Add(task.Id, completed);
                    completedList.Add(completed);

                    var inProgress = CreateInProgress(group.Value, task.Description);
                    inProgressDictionary[group.Key].Add(task.Id, inProgress);
                    inProgressList.Add(inProgress);

                    if (_model.Unassigned.Value)
                    {
                        var unassigned = CreateUnassigned(group.Value, task.Description);
                        unassignedDictionary[group.Key].Add(task.Id, unassigned);
                        unassginedList.Add(unassigned);
                    }
                }
            }

            foreach (var data in _dataEnd)
            {
                completedDictionary[GetGroupKey(data)][data.TaskType].ApplyTo(data.CompletedOnSchedule, data.CompletedOverdue);
                inProgressDictionary[GetGroupKey(data)][data.TaskType].ApplyTo(data.InProgressOnSchedule, data.InProgressOverdue);

                if (_model.Unassigned.Value)
                {
                    unassignedDictionary[GetGroupKey(data)][data.TaskType].ApplyTo(GetUnassigned(data), 0);
                }
            }

            foreach (var data in _dataInProgressStart)
            {
                inProgressDictionary[GetGroupKey(data)][data.TaskType].ApplyFrom(data.InProgressOnSchedule, data.InProgressOverdue);

                if (_model.Unassigned.Value)
                {
                    unassignedDictionary[GetGroupKey(data)][data.TaskType].ApplyFrom(GetUnassigned(data), 0);
                }
            }

            foreach (var data in _dataCompletedStart)
            {
                completedDictionary[GetGroupKey(data)][data.TaskType].ApplyFrom(data.CompletedOnSchedule, data.CompletedOverdue);                
            }

            var exporter = _exporterCreator.Create(_model.DateStart, _model.DateEnd);

            if (_model.IncludeCompleted)
            {
                exporter.AddWorksheetData(completedList, CompletedHiddenColumns().ToArray());
            }

            if (_model.IncludeInProgress)
            {
                exporter.AddWorksheetData(inProgressList, InProgressHiddenColumns().ToArray());
            }

            if (_model.Unassigned.Value)
            {
                exporter.AddWorksheetData(unassginedList, UnassignedHiddenColumns().ToArray());
            }

            exporter.Export(filename);

            return true;
        }

        protected abstract Dictionary<string, TGroup> GetGroups();

        protected abstract string GetGroupKey(TDto dataItem);

        protected abstract int GetUnassigned(TDto dataItem);

        protected abstract TCompleted CreateCompleted(TGroup group, string taskType);

        protected abstract TInProgress CreateInProgress(TGroup group, string taskType);

        protected abstract TUnassigned CreateUnassigned(TGroup group, string taskType);

        private bool GetData(long? reportId, out TDto[] data)
        {
            data = new TDto[0];

            return !reportId.HasValue
                || _dataLoader.Load(reportId.Value, out data);
        }

        private IEnumerable<string> CompletedHiddenColumns()
        {
            if (!_model.CompletedOnSchedule)
            {
                yield return TypeHelper<CompletedSummaryModel>.GetMemberName(x => x.OnSchedule);
            }

            if (!_model.CompletedOverdue)
            {
                yield return TypeHelper<CompletedSummaryModel>.GetMemberName(x => x.Overdue);
            }
        }

        private IEnumerable<string> InProgressHiddenColumns()
        {
            if (!_model.InProgressOnSchedule || _model.ReportOnSigleDate)
            {
                yield return TypeHelper<InProgressSummaryModel>.GetMemberName(x => x.OnSchedulePeriodStart);
            }

            if (!_model.InProgressOnSchedule)
            {
                yield return TypeHelper<InProgressSummaryModel>.GetMemberName(x => x.OnSchedulePeriodEnd);
            }

            if (_model.ReportOnSigleDate)
            {
                yield return TypeHelper<InProgressSummaryModel>.GetMemberName(x => x.TotalStart);
            }

            if (!_model.InProgressOverdue || _model.ReportOnSigleDate)
            {
                yield return TypeHelper<InProgressSummaryModel>.GetMemberName(x => x.OverdueStart);
            }

            if (!_model.InProgressOverdue)
            {
                yield return TypeHelper<InProgressSummaryModel>.GetMemberName(x => x.OverdueEnd);
            }
        }

        private IEnumerable<string> UnassignedHiddenColumns()
        {
            if (_model.ReportOnSigleDate)
            {
                yield return TypeHelper<UnassignedSummaryModel>.GetMemberName(x => x.UnassignedStart);
            }
        }

        # endregion

        # region IReportBuilder

        private bool _dataVerified;

        public bool HasData()
        {
            _dataVerified = true;
            return HasDataTo()
                && HasCompletedDataFrom()
                && HasInProgressDataFrom();
        }

        private bool HasDataTo()
        {
            return _reportFinder.TryGetReportId(_model.DateEnd, out _endReportId);
        }

        private bool HasCompletedDataFrom()
        {
            return !_model.IncludeCompleted
                || _reportFinder.TryGetReportId(_model.CompletedDateStart, out _completedStartReportId);
        }

        private bool HasInProgressDataFrom()
        {
            return !_model.IncludeInProgressOrUnassigned
                || _reportFinder.TryGetReportId(_model.DateStart, out _inProgressStartReportId);
        }

        public void StartExport(string filename)
        {
            var asyncWorker = new AsyncWorker<bool>();
            asyncWorker.DoWork +=
                (sender, e) =>
                {
                    try
                    {
                        if (Build(filename)
                            && Exported != null)
                        {
                            Exported(this, new EventArgs());
                        }
                    }
                    catch (Exception error)
                    {
                        if (Error != null)
                        {
                            Error(this, new ErrorEventArgs(error));
                        }
                    }
                };
            asyncWorker.Start();
        }

        public event EventHandler Exported;

        public event EventHandler<ErrorEventArgs> Error;

        # endregion
    }
}
