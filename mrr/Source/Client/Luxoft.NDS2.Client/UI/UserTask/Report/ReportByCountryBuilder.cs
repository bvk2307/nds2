﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByCountryBuilder : ReportBuilderBase<CompletedByCountry, InProgressByCountry, UnassignedSummaryModel, UserTaskRegionSummary, string>
    {
        public ReportByCountryBuilder(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<UserTaskRegionSummary> dataLoader,
            IExporterCreator<CompletedByCountry,InProgressByCountry,UnassignedSummaryModel> exporterCreator)
            : base(model, reportFinder, dataLoader, exporterCreator)
        {
        }

        protected override Dictionary<string, string> GetGroups()
        {
            return new Dictionary<string, string> { { string.Empty, string.Empty } };
        }

        protected override string GetGroupKey(UserTaskRegionSummary dataItem)
        {
            return string.Empty;
        }

        protected override CompletedByCountry CreateCompleted(string group, string taskType)
        {
            return new CompletedByCountry(taskType);
        }

        protected override InProgressByCountry CreateInProgress(string group, string taskType)
        {
            return new InProgressByCountry(taskType);
        }

        protected override UnassignedSummaryModel CreateUnassigned(string group, string taskType)
        {
            throw new NotSupportedException();
        }

        protected override int GetUnassigned(UserTaskRegionSummary dataItem)
        {
            throw new NotSupportedException();
        }
    }
}
