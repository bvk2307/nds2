﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByRegionBuilder : ReportBuilderBase<CompletedByRegion, InProgressByRegion, UnassignedByRegion, UserTaskRegionSummary, Region>
    {
        private readonly Dictionary<string, Region> _regions;

        public ReportByRegionBuilder(
            ReportParametersViewModel model,
            IReportFinder reportFinder,
            IReportDataLoader<UserTaskRegionSummary> dataLoader,
            IExporterCreator<CompletedByRegion, InProgressByRegion, UnassignedByRegion> exportCreator,
            Region[] allRegions)
            : base(model, reportFinder, dataLoader, exportCreator)
        {
            _regions =
                allRegions
                    .Where(x => model.Regions.IsChecked(x.Code))
                    .ToDictionary(x => x.Code);
        }

        protected override Dictionary<string, Region> GetGroups()
        {
            return _regions;
        }

        protected override string GetGroupKey(UserTaskRegionSummary dataItem)
        {
            return dataItem.RegionCode;
        }

        protected override int GetUnassigned(UserTaskRegionSummary dataItem)
        {
            return dataItem.Unassigned;
        }

        protected override CompletedByRegion CreateCompleted(Region group, string taskType)
        {
            return new CompletedByRegion(taskType, group);
        }

        protected override InProgressByRegion CreateInProgress(Region group, string taskType)
        {
            return new InProgressByRegion(taskType, group);
        }

        protected override UnassignedByRegion CreateUnassigned(Region group, string taskType)
        {
            return new UnassignedByRegion(taskType, group);
        }
    }
}
