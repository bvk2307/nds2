﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportByCountryExporterCreator : IExporterCreator<CompletedByCountry, InProgressByCountry, UnassignedSummaryModel>
    {
        private readonly ICatalogService _service;

        public ReportByCountryExporterCreator(ICatalogService service)
        {
            _service = service;
        }

        public IReportExporter<CompletedByCountry, InProgressByCountry, UnassignedSummaryModel> Create(DateTime startDate, DateTime endDate)
        {
            return new ReportByCountryExporter(startDate, endDate, _service);
        }
    }
}
