﻿using CommonComponents.Catalog;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.CommonFunctions;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public sealed class ReportDialogOpener : IUserTaskReportOpener
    {
        private readonly WorkItem _workItem;

        public ReportDialogOpener(WorkItem workItem)
        {
            _workItem = workItem;
        }

        public void Open(DictionaryItem[] taskTypes, AccessContext context)
        {
            var logger = _workItem.Services.Get<IClientLogger>(true);
            var notifier = _workItem.Services.Get<INotifier>(true);
            var dictionaryService = _workItem.GetWcfServiceProxy<IDictionaryDataService>();
            var reportService = _workItem.GetWcfServiceProxy<IUserTaskReportService>();
            var inspectorsDataProxy = new InspectorsDataProxy(
                _workItem.GetWcfServiceProxy<IUserInformationService>(),
                notifier,
                logger);

            Region[] regions = null;
            Sono[] sonoCodes = null;

            if (!dictionaryService.ExecuteNoHandling(
                    x => x.GetRegionDictionary(),
                    out regions)
                || !dictionaryService.ExecuteNoHandling(
                    x => x.GetSonoDictionary(),
                    out sonoCodes))
            {
                notifier.ShowError("Ошибка загрузки данных");
                return;
            }

            var model = new ReportParametersViewModel(taskTypes, regions, inspectorsDataProxy, context);
            var creator = 
                new ReportBuilderCreator(
                    notifier, 
                    logger, 
                    _workItem.Services.Get<ICatalogService>(), 
                    reportService,
                    regions,
                    sonoCodes);
            var presenter = new ReportPresenter(
                new ReportDialogView(), 
                model, 
                creator,
                notifier,
                new QuestionBox(_workItem.Services.Get<IUcMessageService>()),
                logger,
                new FileOpener());

            presenter.ShowDialog();
        }
    }

    public class ReportBuilderCreator : IReportBuilderCreator
    {
        private Dictionary<string, Func<ReportParametersViewModel, IReportBuilder>> _creators =
            new Dictionary<string, Func<ReportParametersViewModel, IReportBuilder>>();

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        private readonly ICatalogService _catalog;

        private readonly IUserTaskReportService _service;

        private readonly Region[] _regions;

        private readonly Sono[] _sonoCodes;

        public ReportBuilderCreator(
            INotifier notifier,
            IClientLogger logger,
            ICatalogService catalog,
            IUserTaskReportService service,
            Region[] regions,
            Sono[] sonoCodes)
        {
            _notifier = notifier;
            _logger = logger;
            _catalog = catalog;
            _service = service;
            _regions = regions;
            _sonoCodes = sonoCodes;

            _creators.Add(MrrOperations.UserTask.UserTaskReportByCountry, ByCountry);
            _creators.Add(MrrOperations.UserTask.UserTaskReportByRegion, ByRegion);
            _creators.Add(MrrOperations.UserTask.UserTaskReportByInspections, BySono);
            _creators.Add(MrrOperations.UserTask.UserTaskReportByRegionInspection, ByRegionalSono);
            _creators.Add(MrrOperations.UserTask.UserTaskReportByInspector, ByAssignee);
        }

        public IReportBuilder Create(ReportParametersViewModel model)
        {
            return _creators[model.SelectedReport.Name](model);
        }

        private IReportBuilder ByCountry(ReportParametersViewModel model)
        {
            return new ReportByCountryBuilder(
                model,
                new ReportFinder(
                    _service,
                    _notifier,
                    _logger),
                new ReportByRegionDataLoader(
                    _notifier, 
                    _logger, 
                    _service, 
                    model.GetTaskTypeIds(),
                    new string[0]),
                new ReportByCountryExporterCreator(_catalog));
        }

        private IReportBuilder ByRegion(ReportParametersViewModel model)
        {
            return new ReportByRegionBuilder(
                model,
                new ReportFinder(
                    _service,
                    _notifier,
                    _logger),
                new ReportByRegionDataLoader(
                    _notifier,
                    _logger,
                    _service,
                    model.GetTaskTypeIds(),
                    model.GetRegionCodes()),
                new ReportByRegionExporterCreator(_catalog),
                _regions);
        }

        private IReportBuilder BySono(ReportParametersViewModel model)
        {
            return new ReportBySonoBuilder(
                model,
                new ReportFinder(
                    _service,
                    _notifier,
                    _logger),
                new ReportBySonoDataLoader(
                    _notifier,
                    _logger,
                    _service,
                    model.GetTaskTypeIds(),
                    model.GetRegionCodes()),
                new ReportBySonoExporterCreator(_catalog),
                _regions,
                _sonoCodes);
        }

        private IReportBuilder ByRegionalSono(ReportParametersViewModel model)
        {
            return new ReportByRegionalSonoBuilder(
                model,
                new ReportFinder(
                    _service,
                    _notifier,
                    _logger),
                new ReportBySonoDataLoader(
                    _notifier,
                    _logger,
                    _service,
                    model.GetTaskTypeIds(),
                    model.GetRegionCodes()),
                new ReportByRegionalSonoExporterCreator(_catalog),
                model.GetRegionCodes()[0],
                _sonoCodes);
        }

        private IReportBuilder ByAssignee(ReportParametersViewModel model)
        {
            return new ReportByAssigneeBuilder(
                model,
                new ReportFinder(
                    _service,
                    _notifier,
                    _logger),
                new ReportByAssigneeDataLoader(
                    _notifier,
                    _logger,
                    _service,
                    model.GetTaskTypeIds(),
                    model.GetSelectedUserSids()),
                new ReportByAssigneeExporterCreator(_catalog),
                model.GetSelectedUsers());
        }
    }
}
