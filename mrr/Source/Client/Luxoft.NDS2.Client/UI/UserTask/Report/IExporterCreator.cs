﻿using Luxoft.NDS2.Common.Excel.ReportExport.UserTask;
using Luxoft.NDS2.Common.Models.UserTask;
using System;

namespace Luxoft.NDS2.Client.UI.UserTask.Report
{
    public interface IExporterCreator<TCompleted, TInProgress, TUnassigned>
        where TCompleted : CompletedSummaryModel
        where TInProgress : InProgressSummaryModel
        where TUnassigned : UnassignedSummaryModel
    {
        IReportExporter<TCompleted, TInProgress, TUnassigned> Create(DateTime startDate, DateTime endDate);
    }
}
