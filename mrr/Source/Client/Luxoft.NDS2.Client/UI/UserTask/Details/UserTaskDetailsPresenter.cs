﻿using System;
using System.Drawing;
using FLS.CommonComponents.App.Services.Exrtensions;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Client.UI.UserTask.Details.FormTune;
using Luxoft.NDS2.Client.UI.UserTask.List;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Microsoft.Practices.CompositeUI.Collections;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public class UserTaskDetailsPresenter
    {

        private IServiceRequestWrapper _commonServiceRequestWrapper;
        private ServiceCollection _infrastructureServiceCollection;
        private IUserTaskService _serviceProxy;
        private IUserTaskDetailsView _view;
        private UserTaskModel _model;
        private UserRights _userRights;

        /// <summary>
        /// настройщик формы по типу документа
        /// </summary>
        private IDocumentTypeTuner _documentTypeTuner;
        /// <summary>
        /// настройщик формы по типу задания
        /// </summary>
        private TypeTuner _taskTypeTuner;
        private ICardOpener _cardOpener;

        public UserTaskDetailsPresenter(
            IServiceRequestWrapper commonServiceRequestWrapper, 
            ServiceCollection infrastructureServiceCollection, 
            DictionarySur sur, 
            UserTaskListModel userTaskListModel, 
            UserRights userRights,
            ICardOpener cardOpener)
        {
            _commonServiceRequestWrapper = commonServiceRequestWrapper;
            _infrastructureServiceCollection = infrastructureServiceCollection;
            _serviceProxy = infrastructureServiceCollection.GetServiceProxy<IUserTaskService>(Constants.EndpointName);

            _userRights = userRights;
            _cardOpener = cardOpener;

            _model = new UserTaskModel
            {
                UserTaskListModel = userTaskListModel,
                Sur = sur,
            };

        }

        public void OpenForm()
        {
            _commonServiceRequestWrapper.ExecuteAsync(
                _ => _serviceProxy.GetCardData(
                    _model.UserTaskListModel.GetId(),
                    _model.UserTaskListModel.GetTaskTypeCode(),
                    _model.UserTaskListModel.GetDocumentZip()
                ),
                (_, execRes) =>
                {
                    _model.UserTaskDetailsModel = new UserTaskDetailsModel(
                        _model.UserTaskListModel,
                        execRes.Result,
                        _model.Sur);

                    _view = new UserTaskDetailsView();
                    FormTunerInit();
                    FillForm(_model, _userRights.UserTaskRole);

                    _view.OnOpenCard += OpenCard;
                    _view.OnSave += Save;
                    _view.OnSetAnswer += SetAnswer;
                    _view.OnShowAnswer += ShowAnswer;

                    _view.ShowForm();
                }
        );
    }

        private void FormTunerInit()
        {
            switch (_model.UserTaskDetailsModel.DocumentTypeCode)
            {
                case 0:
                    _documentTypeTuner = new DeclarationTuner();
                    break;
                case 1:
                    _documentTypeTuner = new JournalTuner();
                    break;
                default:
                    throw new ApplicationException("Неизвестный тип документа");
            }
            _documentTypeTuner.View = _view;

            switch (_model.UserTaskDetailsModel.TaskType)
            {
                case (int)TaskType.Demand:
                    _taskTypeTuner = new DemandTuner();
                    break;
                case (int)TaskType.Answer:
                    _taskTypeTuner = new AnswerTuner();
                    BtSetAnswerTune();
                    break;
                case (int)TaskType.Analysis:
                    _taskTypeTuner = new AnalysisTuner();
                    _view.CbTaxPayerSolvencyDataSource = _model.UserTaskDetailsModel.TaxPayerSolvencyList;
                    break;
                default:
                    throw new ApplicationException("Неизвестный тип ПЗ");
            }
            _taskTypeTuner.UserTaskRole = _userRights.UserTaskRole;
            _taskTypeTuner.View = _view;
        }

        /// <summary>
        /// отдельная настройка кнопки "Ввести ответ"
        /// </summary>
        private void BtSetAnswerTune()
        {
            //DICT_EXPLAIN_REPLY_STATUS         
            //1- 'Необработано');
            //2- 'Отправлено в МС');
            //3- 'Обработано в МС')

            //Просмотреть ответ - Видима и доступна, если статус ответа «Отправлено в МС» или «Обработано в МС» .
            _view.BtShowAnswerVisible = 
                _model.UserTaskDetailsModel.AnswerStatusCode == 2 || 
                _model.UserTaskDetailsModel.AnswerStatusCode == 3;

            //Ввести ответ - Видима для пользователя с ролью «Инспектор» (на которого назначено данное задание), 
            //если статус  ответа «Необработано»
            //и корреткировка не является аннулированной
            _view.BtSetAnswerVisible = 
                _model.UserTaskDetailsModel.AnswerStatusCode == 1 &&
                _userRights.UserTaskRole == UserTaskRole.Inspector &&
                !_model.UserTaskDetailsModel.IsCorrectionKnpClosedByAnnulment;


            //Ввести ответ - Доступна, если ранее созданные ПЗ с данным типом по данному связанному документу  выполнены 
            //(т.е. если на одно автоистребование было создано несколько ПЗ данного типа и эти задания не выполнены, 
            //то кнопка «Ввести ответ» будет доступна только на первом задании). 

            //Задания данного типа должны выполняться последовательно.
            //Система закрывает окно и открывает вкладку с ответом на истребование в режиме «редактирование».
            _view.BtSetAnswerEnabled = _model.UserTaskDetailsModel.OtherAnswerNotExists == 1;

        }

        public void FillForm(UserTaskModel model, UserTaskRole userTaskRole)
        {
            _view.Caption = string.Format("{0} ({1})", 
                _model.UserTaskDetailsModel.TaxPayerInn,
                _model.UserTaskDetailsModel.TaskTypeName);
            _view.LoadActorList(_model.UserTaskDetailsModel.ActorList);

            //Сведения о пользовательском задании
            _view.LbCreateDateText = model.UserTaskDetailsModel.CreateDate.NullFormat();
            _view.LbDuePeriodText = model.UserTaskDetailsModel.DuePeriod.ToString();
            _view.LbDaysAfterExpireText = model.UserTaskDetailsModel.DaysAfterExpire.ToString();
            _view.LbDaysAfterExpireForeColor =
                (model.UserTaskDetailsModel.DaysAfterExpire > 0) ? Color.Red : Color.Black;
            _view.LbStatusText = model.UserTaskDetailsModel.StatusName;
            _view.LbStatusForeColor = 
                (model.UserTaskDetailsModel.DaysAfterExpire > 0) ? Color.Red : Color.Black;
            _view.LbActualCompleteDateText =
                model.UserTaskDetailsModel.ActualCompleteDate.NullFormat();

            //Общие сведения
            _view.LbTaxPayerNameText = model.UserTaskDetailsModel.TaxPayerName;
            _view.LbTaxPayerInnKppText = 
                model.UserTaskDetailsModel.TaxPayerInn + 
                model.UserTaskDetailsModel.TaxPayerKpp.If(str => "/" + str);
            _view.LbDocumentPeriodNameText = model.UserTaskDetailsModel.DocumentPeriodName;

            _documentTypeTuner.Process(model.UserTaskDetailsModel);

            //Сведения о выполнении пользовательского задания
            //Список исполнителей
            _view.SetTab((TaskType) model.UserTaskDetailsModel.TaskType);
            _taskTypeTuner.Process(_model.UserTaskDetailsModel);

            //кнопки
            _taskTypeTuner.TuneButtonBlock();
        }


        private void ShowAnswer()
        {
            if (_model.UserTaskDetailsModel.ExplainId != null)
            {
                _view.Close();
                _commonServiceRequestWrapper.ExecuteAsync(
                    _ => new OperationResult<ExplainReplyCardData>(
							  _cardOpener.GetExplainCardRcd(_model.UserTaskDetailsModel.ExplainId.Value)),
                    (_, execRes) =>
                    {
								var rcd = execRes.Result;
								rcd.Regim = ExplainRegim.View;
								_cardOpener.ViewExplainReplyDetails(rcd);
						  });
            }
        }

        private void SetAnswer()
        {
            if (_model.UserTaskDetailsModel.ExplainId != null)
            {
                _view.Close();
                _commonServiceRequestWrapper.ExecuteAsync(
                    _ => new OperationResult<ExplainReplyCardData>(
							  _cardOpener.GetExplainCardRcd(_model.UserTaskDetailsModel.ExplainId.Value)),
                    (_, execRes) =>
                    {
								var rcd = execRes.Result;
								rcd.Regim = ExplainRegim.Edit;
								_cardOpener.ViewExplainReplyDetails(rcd);
                    });
            }
        }

        private void Save()
        {
            if (! _view.CbTaxPayerSolvencyValue.HasValue) return;
            var solvencyStatusId = _view.CbTaxPayerSolvencyValue.Value;
            var comment = _view.TxtCommentText;
            _commonServiceRequestWrapper.ExecuteAsync(
                _ => _serviceProxy.SetSolvency(
                    _model.UserTaskListModel.GetId(),
                    solvencyStatusId,
                    comment
                    ),
                (_, __) => _view.Close());
        }

        private void OpenCard()
        {
            if (_model.UserTaskDetailsModel.ClaimdocCreateNumber != null)
            {
                _view.Close();
                _commonServiceRequestWrapper.ExecuteAsync(
                    _ => _infrastructureServiceCollection.
							  GetServiceProxy<IDiscrepancyDocumentService>(Constants.EndpointName).
							  GetDocument(_model.UserTaskDetailsModel.ClaimdocCreateNumber.Value),
                    (_, execRes) =>
                    {
                        _cardOpener.ViewReclaimDetails(execRes.Result);
                    });
            }
        }
    }

    public enum TaskType
    {
        /// <summary>
        /// Истребование документов
        /// </summary>
        Demand = 1	,
        /// <summary>
        ///	Ввод ответа на истребование
        /// </summary>
        Answer = 2,
        /// <summary>
        /// Анализ налогоплательщика
        /// </summary>
        Analysis = 3
    }
}