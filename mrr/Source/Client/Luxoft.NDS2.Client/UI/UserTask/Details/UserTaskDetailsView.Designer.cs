﻿using Infragistics.Win.Misc;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    partial class UserTaskDetailsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;



        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel _detailsBoxArea;
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.lbDemandSeod = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ULSizeLabel = new Infragistics.Win.Misc.UltraLabel();
            this.lbAnswerDate = new Infragistics.Win.Misc.UltraLabel();
            this.lbAnswerSeod = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.lbAnswerStatus = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.Label33 = new Infragistics.Win.Misc.UltraLabel();
            this.cbTaxPayerSolvency = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.label66 = new Infragistics.Win.Misc.UltraLabel();
            this.txtComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.Label55 = new Infragistics.Win.Misc.UltraLabel();
            this.lbAnalysisSeod = new Infragistics.Win.Misc.UltraLabel();
            this.lbTaxPayerName = new Infragistics.Win.Misc.UltraLabel();
            this.lbKnpStatus = new Infragistics.Win.Misc.UltraLabel();
            this.lbNdsTotal = new Infragistics.Win.Misc.UltraLabel();
            this.lbSign = new Infragistics.Win.Misc.UltraLabel();
            this.lbDocumentCorrectionNumber = new Infragistics.Win.Misc.UltraLabel();
            this.lbDocumentPeriodName = new Infragistics.Win.Misc.UltraLabel();
            this.lbTaxPayerInnKpp = new Infragistics.Win.Misc.UltraLabel();
            this.ReorgSonoLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ReorgInnKppLabel = new Infragistics.Win.Misc.UltraLabel();
            this.btSave = new Infragistics.Win.Misc.UltraButton();
            this.btOpenCard = new Infragistics.Win.Misc.UltraButton();
            this.btShowAnswer = new Infragistics.Win.Misc.UltraButton();
            this.DetailsBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.btSetAnswer = new Infragistics.Win.Misc.UltraButton();
            this.ultraBoxPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel22 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel33 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel111 = new Infragistics.Win.Misc.UltraPanel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.uegbUserTaskInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uegbUserTaskInfoPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.lbActualCompleteDate = new Infragistics.Win.Misc.UltraLabel();
            this.ULRegistrationDataCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            this.lbStatus = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ULAccountDataCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            this.lbDaysAfterExpire = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lbDuePeriod = new Infragistics.Win.Misc.UltraLabel();
            this.uegbCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ugbCommonInfoPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tlpCommonInfo = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.uegbUserTaskProcessingDemand = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uegbUserTaskProcessingDemandPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.uegbUserTaskProcessingAnswer = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uegbUserTaskProcessingAnswerPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uegbUserTaskProcessingAnalisis = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uegbUserTaskProcessingAnalisisPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uegbAssign = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uegbAssignPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurView();
            this.gridActor = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            _detailsBoxArea = new Infragistics.Win.Misc.UltraPanel();
            _detailsBoxArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTaxPayerSolvency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsBox)).BeginInit();
            this.DetailsBox.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraBoxPanel1.SuspendLayout();
            this.ultraPanel22.SuspendLayout();
            this.ultraPanel33.SuspendLayout();
            this.ultraPanel111.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskInfo)).BeginInit();
            this.uegbUserTaskInfo.SuspendLayout();
            this.uegbUserTaskInfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).BeginInit();
            this.uegbCommonInfo.SuspendLayout();
            this.ugbCommonInfoPanel.SuspendLayout();
            this.tlpCommonInfo.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingDemand)).BeginInit();
            this.uegbUserTaskProcessingDemand.SuspendLayout();
            this.uegbUserTaskProcessingDemandPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingAnswer)).BeginInit();
            this.uegbUserTaskProcessingAnswer.SuspendLayout();
            this.uegbUserTaskProcessingAnswerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingAnalisis)).BeginInit();
            this.uegbUserTaskProcessingAnalisis.SuspendLayout();
            this.uegbUserTaskProcessingAnalisisPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbAssign)).BeginInit();
            this.uegbAssign.SuspendLayout();
            this.uegbAssignPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _detailsBoxArea
            // 
            _detailsBoxArea.Location = new System.Drawing.Point(0, 0);
            _detailsBoxArea.Name = "_detailsBoxArea";
            _detailsBoxArea.Size = new System.Drawing.Size(200, 100);
            _detailsBoxArea.TabIndex = 0;
            // 
            // lbDemandSeod
            // 
            this.lbDemandSeod.AutoSize = true;
            this.lbDemandSeod.Location = new System.Drawing.Point(240, 8);
            this.lbDemandSeod.Name = "lbDemandSeod";
            this.lbDemandSeod.Size = new System.Drawing.Size(80, 14);
            this.lbDemandSeod.TabIndex = 12;
            this.lbDemandSeod.Text = "№ {*} <номер>";
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.AutoSize = true;
            this.ultraLabel8.Location = new System.Drawing.Point(8, 8);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(226, 14);
            this.ultraLabel8.TabIndex = 11;
            this.ultraLabel8.Text = "Требование о представлении документов";
            // 
            // ULSizeLabel
            // 
            this.ULSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.TextHAlignAsString = "Right";
            appearance43.TextVAlignAsString = "Middle";
            this.ULSizeLabel.Appearance = appearance43;
            this.ULSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULSizeLabel.Location = new System.Drawing.Point(784, 15);
            this.ULSizeLabel.Name = "ULSizeLabel";
            this.ULSizeLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULSizeLabel.Size = new System.Drawing.Size(128, 14);
            this.ULSizeLabel.TabIndex = 2;
            this.ULSizeLabel.Text = "Крупнейший";
            // 
            // lbAnswerDate
            // 
            this.lbAnswerDate.AutoSize = true;
            this.lbAnswerDate.Location = new System.Drawing.Point(296, 28);
            this.lbAnswerDate.Name = "lbAnswerDate";
            this.lbAnswerDate.Size = new System.Drawing.Size(45, 14);
            this.lbAnswerDate.TabIndex = 15;
            this.lbAnswerDate.Text = " <дата>";
            // 
            // lbAnswerSeod
            // 
            this.lbAnswerSeod.AutoSize = true;
            this.lbAnswerSeod.Location = new System.Drawing.Point(240, 8);
            this.lbAnswerSeod.Name = "lbAnswerSeod";
            this.lbAnswerSeod.Size = new System.Drawing.Size(80, 14);
            this.lbAnswerSeod.TabIndex = 14;
            this.lbAnswerSeod.Text = "№ {*} <номер>";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.AutoSize = true;
            this.ultraLabel5.Location = new System.Drawing.Point(8, 8);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(226, 14);
            this.ultraLabel5.TabIndex = 13;
            this.ultraLabel5.Text = "Требование о представлении документов";
            // 
            // lbAnswerStatus
            // 
            this.lbAnswerStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.lbAnswerStatus.Appearance = appearance6;
            this.lbAnswerStatus.AutoSize = true;
            this.lbAnswerStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbAnswerStatus.Location = new System.Drawing.Point(549, 28);
            this.lbAnswerStatus.Name = "lbAnswerStatus";
            this.lbAnswerStatus.Padding = new System.Drawing.Size(2, 0);
            this.lbAnswerStatus.Size = new System.Drawing.Size(88, 14);
            this.lbAnswerStatus.TabIndex = 10;
            this.lbAnswerStatus.Text = "Необработано";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance7;
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(495, 28);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel3.Size = new System.Drawing.Size(48, 14);
            this.ultraLabel3.TabIndex = 9;
            this.ultraLabel3.Text = "Статус:";
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Location = new System.Drawing.Point(11, 28);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(282, 14);
            this.Label33.TabIndex = 8;
            this.Label33.Text = "Ответ на требование о представлении пояснения от";
            // 
            // cbTaxPayerSolvency
            // 
            this.cbTaxPayerSolvency.DisplayMember = "Description";
            this.cbTaxPayerSolvency.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbTaxPayerSolvency.Location = new System.Drawing.Point(162, 23);
            this.cbTaxPayerSolvency.Name = "cbTaxPayerSolvency";
            this.cbTaxPayerSolvency.Size = new System.Drawing.Size(255, 21);
            this.cbTaxPayerSolvency.TabIndex = 12;
            this.cbTaxPayerSolvency.ValueMember = "Id";
            this.cbTaxPayerSolvency.SelectionChanged += new System.EventHandler(this.cbTaxPayerSolvency_SelectionChanged);
            // 
            // label66
            // 
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.label66.Appearance = appearance46;
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(8, 55);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(80, 14);
            this.label66.TabIndex = 11;
            this.label66.Text = "Комментарий:";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComment.Location = new System.Drawing.Point(8, 75);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.NullText = "Введите комментарий к заданию";
            appearance1.FontData.ItalicAsString = "True";
            this.txtComment.NullTextAppearance = appearance1;
            this.txtComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComment.Size = new System.Drawing.Size(681, 100);
            this.txtComment.TabIndex = 10;
            // 
            // Label55
            // 
            appearance53.TextHAlignAsString = "Left";
            appearance53.TextVAlignAsString = "Middle";
            this.Label55.Appearance = appearance53;
            this.Label55.AutoSize = true;
            this.Label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label55.Location = new System.Drawing.Point(8, 32);
            this.Label55.Name = "Label55";
            this.Label55.Size = new System.Drawing.Size(153, 14);
            this.Label55.TabIndex = 2;
            this.Label55.Text = "Статус  налогоплательщика";
            // 
            // lbAnalysisSeod
            // 
            appearance55.TextHAlignAsString = "Left";
            appearance55.TextVAlignAsString = "Middle";
            this.lbAnalysisSeod.Appearance = appearance55;
            this.lbAnalysisSeod.AutoSize = true;
            this.lbAnalysisSeod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnalysisSeod.Location = new System.Drawing.Point(8, 8);
            this.lbAnalysisSeod.Name = "lbAnalysisSeod";
            this.lbAnalysisSeod.Size = new System.Drawing.Size(249, 14);
            this.lbAnalysisSeod.TabIndex = 0;
            this.lbAnalysisSeod.Text = "Автотребование по СФ №  <номер> от <дата>";
            // 
            // lbTaxPayerName
            // 
            this.lbTaxPayerName.AutoSize = true;
            this.tlpCommonInfo.SetColumnSpan(this.lbTaxPayerName, 3);
            this.lbTaxPayerName.Location = new System.Drawing.Point(29, 3);
            this.lbTaxPayerName.Name = "lbTaxPayerName";
            this.lbTaxPayerName.Size = new System.Drawing.Size(315, 14);
            this.lbTaxPayerName.TabIndex = 40;
            this.lbTaxPayerName.Text = "Китайская вечнозеленая аглоонема и нефритовое дерево";
            // 
            // lbKnpStatus
            // 
            this.lbKnpStatus.AutoSize = true;
            this.lbKnpStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKnpStatus.Location = new System.Drawing.Point(293, 43);
            this.lbKnpStatus.Name = "lbKnpStatus";
            this.lbKnpStatus.Size = new System.Drawing.Size(78, 14);
            this.lbKnpStatus.TabIndex = 39;
            this.lbKnpStatus.Text = "{Статус  КНП}";
            // 
            // lbNdsTotal
            // 
            this.lbNdsTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNdsTotal.Location = new System.Drawing.Point(494, 43);
            this.lbNdsTotal.Name = "lbNdsTotal";
            this.lbNdsTotal.Size = new System.Drawing.Size(190, 14);
            this.lbNdsTotal.TabIndex = 36;
            this.lbNdsTotal.Text = "{Сумма НДС}";
            // 
            // lbSign
            // 
            this.lbSign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSign.Location = new System.Drawing.Point(494, 23);
            this.lbSign.Name = "lbSign";
            this.lbSign.Size = new System.Drawing.Size(190, 14);
            this.lbSign.TabIndex = 35;
            this.lbSign.Text = "{Признак НД}";
            // 
            // lbDocumentCorrectionNumber
            // 
            this.lbDocumentCorrectionNumber.AutoSize = true;
            this.lbDocumentCorrectionNumber.Location = new System.Drawing.Point(293, 23);
            this.lbDocumentCorrectionNumber.Name = "lbDocumentCorrectionNumber";
            this.lbDocumentCorrectionNumber.Size = new System.Drawing.Size(125, 14);
            this.lbDocumentCorrectionNumber.TabIndex = 21;
            this.lbDocumentCorrectionNumber.Text = "Номер {корр.}{версии}: ";
            // 
            // lbDocumentPeriodName
            // 
            this.lbDocumentPeriodName.AutoSize = true;
            this.lbDocumentPeriodName.Location = new System.Drawing.Point(110, 3);
            this.lbDocumentPeriodName.Name = "lbDocumentPeriodName";
            this.lbDocumentPeriodName.Size = new System.Drawing.Size(57, 14);
            this.lbDocumentPeriodName.TabIndex = 11;
            this.lbDocumentPeriodName.Text = "1 кв. 2016";
            // 
            // lbTaxPayerInnKpp
            // 
            this.lbTaxPayerInnKpp.AutoSize = true;
            this.lbTaxPayerInnKpp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTaxPayerInnKpp.Location = new System.Drawing.Point(74, 3);
            this.lbTaxPayerInnKpp.Name = "lbTaxPayerInnKpp";
            this.lbTaxPayerInnKpp.Size = new System.Drawing.Size(133, 14);
            this.lbTaxPayerInnKpp.TabIndex = 10;
            this.lbTaxPayerInnKpp.Text = "1234567890 / 123456789";
            // 
            // ReorgSonoLabel
            // 
            this.ReorgSonoLabel.AutoSize = true;
            this.ReorgSonoLabel.Location = new System.Drawing.Point(3, 3);
            this.ReorgSonoLabel.Name = "ReorgSonoLabel";
            this.ReorgSonoLabel.Size = new System.Drawing.Size(101, 14);
            this.ReorgSonoLabel.TabIndex = 8;
            this.ReorgSonoLabel.Text = "Отчетный период:";
            // 
            // ReorgInnKppLabel
            // 
            this.ReorgInnKppLabel.AutoSize = true;
            this.ReorgInnKppLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReorgInnKppLabel.Location = new System.Drawing.Point(3, 3);
            this.ReorgInnKppLabel.Name = "ReorgInnKppLabel";
            this.ReorgInnKppLabel.Size = new System.Drawing.Size(65, 14);
            this.ReorgInnKppLabel.TabIndex = 6;
            this.ReorgInnKppLabel.Text = "ИНН / КПП:";
            // 
            // btSave
            // 
            this.btSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btSave.AutoSize = true;
            this.btSave.Location = new System.Drawing.Point(3, 3);
            this.btSave.MinimumSize = new System.Drawing.Size(150, 0);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(150, 24);
            this.btSave.TabIndex = 9;
            this.btSave.Text = "Сохранить";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btOpenCard
            // 
            this.btOpenCard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btOpenCard.AutoSize = true;
            this.btOpenCard.Location = new System.Drawing.Point(671, 3);
            this.btOpenCard.MinimumSize = new System.Drawing.Size(250, 0);
            this.btOpenCard.Name = "btOpenCard";
            this.btOpenCard.Size = new System.Drawing.Size(250, 24);
            this.btOpenCard.TabIndex = 8;
            this.btOpenCard.Text = "Просмотреть карточку автоистребования";
            this.btOpenCard.Click += new System.EventHandler(this.btOpenCard_Click);
            // 
            // btShowAnswer
            // 
            this.btShowAnswer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btShowAnswer.AutoSize = true;
            this.btShowAnswer.Location = new System.Drawing.Point(159, 3);
            this.btShowAnswer.MinimumSize = new System.Drawing.Size(250, 0);
            this.btShowAnswer.Name = "btShowAnswer";
            this.btShowAnswer.Size = new System.Drawing.Size(250, 24);
            this.btShowAnswer.TabIndex = 10;
            this.btShowAnswer.Text = "Просмотреть ответ";
            this.btShowAnswer.Click += new System.EventHandler(this.btShowAnswer_Click);
            // 
            // DetailsBox
            // 
            this.DetailsBox.Controls.Add(this.ultraPanel3);
            this.DetailsBox.Location = new System.Drawing.Point(0, 0);
            this.DetailsBox.Name = "DetailsBox";
            this.DetailsBox.Size = new System.Drawing.Size(200, 185);
            this.DetailsBox.TabIndex = 0;
            // 
            // ultraPanel3
            // 
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(3, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(194, 182);
            this.ultraPanel3.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(2, 21);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(196, 77);
            // 
            // btSetAnswer
            // 
            this.btSetAnswer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btSetAnswer.AutoSize = true;
            this.btSetAnswer.Location = new System.Drawing.Point(415, 3);
            this.btSetAnswer.MinimumSize = new System.Drawing.Size(250, 0);
            this.btSetAnswer.Name = "btSetAnswer";
            this.btSetAnswer.Size = new System.Drawing.Size(250, 24);
            this.btSetAnswer.TabIndex = 11;
            this.btSetAnswer.Text = "Ввести ответ";
            this.btSetAnswer.Click += new System.EventHandler(this.btSetAnswer_Click);
            // 
            // ultraBoxPanel1
            // 
            this.ultraBoxPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraBoxPanel1.Name = "ultraBoxPanel1";
            this.ultraBoxPanel1.Size = new System.Drawing.Size(200, 100);
            this.ultraBoxPanel1.TabIndex = 0;
            // 
            // ultraPanel22
            // 
            this.ultraPanel22.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel22.Name = "ultraPanel22";
            this.ultraPanel22.Size = new System.Drawing.Size(200, 100);
            this.ultraPanel22.TabIndex = 0;
            // 
            // ultraPanel33
            // 
            this.ultraPanel33.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel33.Name = "ultraPanel33";
            this.ultraPanel33.Size = new System.Drawing.Size(200, 100);
            this.ultraPanel33.TabIndex = 0;
            // 
            // ultraPanel111
            // 
            this.ultraPanel111.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel111.Name = "ultraPanel111";
            this.ultraPanel111.Size = new System.Drawing.Size(200, 100);
            this.ultraPanel111.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 6);
            this.tlpMain.Controls.Add(this.uegbUserTaskInfo, 0, 0);
            this.tlpMain.Controls.Add(this.uegbCommonInfo, 0, 1);
            this.tlpMain.Controls.Add(this.uegbUserTaskProcessingDemand, 0, 2);
            this.tlpMain.Controls.Add(this.uegbUserTaskProcessingAnswer, 0, 3);
            this.tlpMain.Controls.Add(this.uegbUserTaskProcessingAnalisis, 0, 4);
            this.tlpMain.Controls.Add(this.uegbAssign, 0, 5);
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.MaximumSize = new System.Drawing.Size(709, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 7;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.Size = new System.Drawing.Size(709, 705);
            this.tlpMain.TabIndex = 13;
            // 
            // tlpButtons
            // 
            this.tlpButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpButtons.AutoSize = true;
            this.tlpButtons.ColumnCount = 4;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtons.Controls.Add(this.btOpenCard, 3, 0);
            this.tlpButtons.Controls.Add(this.btSave, 0, 0);
            this.tlpButtons.Controls.Add(this.btSetAnswer, 2, 0);
            this.tlpButtons.Controls.Add(this.btShowAnswer, 1, 0);
            this.tlpButtons.Location = new System.Drawing.Point(0, 675);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpButtons.Size = new System.Drawing.Size(709, 30);
            this.tlpButtons.TabIndex = 7;
            // 
            // uegbUserTaskInfo
            // 
            this.uegbUserTaskInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbUserTaskInfo.Controls.Add(this.uegbUserTaskInfoPanel);
            this.uegbUserTaskInfo.ExpandedSize = new System.Drawing.Size(703, 69);
            this.uegbUserTaskInfo.ExpansionIndicator = Infragistics.Win.Misc.GroupBoxExpansionIndicator.None;
            this.uegbUserTaskInfo.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.None;
            this.uegbUserTaskInfo.Location = new System.Drawing.Point(3, 3);
            this.uegbUserTaskInfo.Name = "uegbUserTaskInfo";
            this.uegbUserTaskInfo.Size = new System.Drawing.Size(703, 69);
            this.uegbUserTaskInfo.TabIndex = 8;
            this.uegbUserTaskInfo.Text = "Сведения о пользовательском задании";
            // 
            // uegbUserTaskInfoPanel
            // 
            this.uegbUserTaskInfoPanel.AutoSize = true;
            this.uegbUserTaskInfoPanel.Controls.Add(this.lbActualCompleteDate);
            this.uegbUserTaskInfoPanel.Controls.Add(this.ULRegistrationDataCaptionLabel);
            this.uegbUserTaskInfoPanel.Controls.Add(this.lbStatus);
            this.uegbUserTaskInfoPanel.Controls.Add(this.ultraLabel6);
            this.uegbUserTaskInfoPanel.Controls.Add(this.ULAccountDataCaptionLabel);
            this.uegbUserTaskInfoPanel.Controls.Add(this.lbDaysAfterExpire);
            this.uegbUserTaskInfoPanel.Controls.Add(this.ultraLabel2);
            this.uegbUserTaskInfoPanel.Controls.Add(this.lbCreateDate);
            this.uegbUserTaskInfoPanel.Controls.Add(this.ultraLabel4);
            this.uegbUserTaskInfoPanel.Controls.Add(this.lbDuePeriod);
            this.uegbUserTaskInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uegbUserTaskInfoPanel.Location = new System.Drawing.Point(3, 16);
            this.uegbUserTaskInfoPanel.Name = "uegbUserTaskInfoPanel";
            this.uegbUserTaskInfoPanel.Padding = new System.Windows.Forms.Padding(5);
            this.uegbUserTaskInfoPanel.Size = new System.Drawing.Size(697, 50);
            this.uegbUserTaskInfoPanel.TabIndex = 0;
            // 
            // lbActualCompleteDate
            // 
            this.lbActualCompleteDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbActualCompleteDate.AutoSize = true;
            this.lbActualCompleteDate.Location = new System.Drawing.Point(602, 28);
            this.lbActualCompleteDate.Name = "lbActualCompleteDate";
            this.lbActualCompleteDate.Size = new System.Drawing.Size(61, 14);
            this.lbActualCompleteDate.TabIndex = 30;
            this.lbActualCompleteDate.Text = "01.02.2010";
            // 
            // ULRegistrationDataCaptionLabel
            // 
            this.ULRegistrationDataCaptionLabel.AutoSize = true;
            this.ULRegistrationDataCaptionLabel.Location = new System.Drawing.Point(8, 8);
            this.ULRegistrationDataCaptionLabel.Name = "ULRegistrationDataCaptionLabel";
            this.ULRegistrationDataCaptionLabel.Size = new System.Drawing.Size(86, 14);
            this.ULRegistrationDataCaptionLabel.TabIndex = 21;
            this.ULRegistrationDataCaptionLabel.Text = "Дата создания:";
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Location = new System.Drawing.Point(545, 8);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(66, 14);
            this.lbStatus.TabIndex = 26;
            this.lbStatus.Text = "Выполнено";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel6.AutoSize = true;
            this.ultraLabel6.Location = new System.Drawing.Point(495, 28);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(101, 14);
            this.ultraLabel6.TabIndex = 29;
            this.ultraLabel6.Text = "Дата выполнения:";
            // 
            // ULAccountDataCaptionLabel
            // 
            this.ULAccountDataCaptionLabel.AutoSize = true;
            this.ULAccountDataCaptionLabel.Location = new System.Drawing.Point(8, 28);
            this.ULAccountDataCaptionLabel.Name = "ULAccountDataCaptionLabel";
            this.ULAccountDataCaptionLabel.Size = new System.Drawing.Size(192, 14);
            this.ULAccountDataCaptionLabel.TabIndex = 23;
            this.ULAccountDataCaptionLabel.Text = "Срок выполнения задания (в днях):";
            // 
            // lbDaysAfterExpire
            // 
            this.lbDaysAfterExpire.AutoSize = true;
            this.lbDaysAfterExpire.Location = new System.Drawing.Point(432, 8);
            this.lbDaysAfterExpire.Name = "lbDaysAfterExpire";
            this.lbDaysAfterExpire.Size = new System.Drawing.Size(17, 14);
            this.lbDaysAfterExpire.TabIndex = 28;
            this.lbDaysAfterExpire.Text = "10";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(495, 8);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(44, 14);
            this.ultraLabel2.TabIndex = 25;
            this.ultraLabel2.Text = "Статус:";
            // 
            // lbCreateDate
            // 
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lbCreateDate.Appearance = appearance3;
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Location = new System.Drawing.Point(100, 8);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(61, 14);
            this.lbCreateDate.TabIndex = 22;
            this.lbCreateDate.Text = "01.02.2010";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(275, 8);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(151, 14);
            this.ultraLabel4.TabIndex = 27;
            this.ultraLabel4.Text = "Кол-во просроченных дней:";
            // 
            // lbDuePeriod
            // 
            this.lbDuePeriod.AutoSize = true;
            this.lbDuePeriod.Location = new System.Drawing.Point(206, 28);
            this.lbDuePeriod.Name = "lbDuePeriod";
            this.lbDuePeriod.Size = new System.Drawing.Size(17, 14);
            this.lbDuePeriod.TabIndex = 24;
            this.lbDuePeriod.Text = "28";
            // 
            // uegbCommonInfo
            // 
            this.uegbCommonInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbCommonInfo.Controls.Add(this.ugbCommonInfoPanel);
            this.uegbCommonInfo.ExpandedSize = new System.Drawing.Size(703, 92);
            this.uegbCommonInfo.Location = new System.Drawing.Point(3, 78);
            this.uegbCommonInfo.Name = "uegbCommonInfo";
            this.uegbCommonInfo.Size = new System.Drawing.Size(703, 92);
            this.uegbCommonInfo.TabIndex = 9;
            this.uegbCommonInfo.Text = "Общие сведения";
            // 
            // ugbCommonInfoPanel
            // 
            this.ugbCommonInfoPanel.AutoSize = true;
            this.ugbCommonInfoPanel.Controls.Add(this.tlpCommonInfo);
            this.ugbCommonInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugbCommonInfoPanel.Location = new System.Drawing.Point(3, 19);
            this.ugbCommonInfoPanel.Name = "ugbCommonInfoPanel";
            this.ugbCommonInfoPanel.Padding = new System.Windows.Forms.Padding(5);
            this.ugbCommonInfoPanel.Size = new System.Drawing.Size(697, 70);
            this.ugbCommonInfoPanel.TabIndex = 0;
            // 
            // tlpCommonInfo
            // 
            this.tlpCommonInfo.AutoSize = true;
            this.tlpCommonInfo.ColumnCount = 4;
            this.tlpCommonInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpCommonInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpCommonInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpCommonInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpCommonInfo.Controls.Add(this.lbNdsTotal, 3, 2);
            this.tlpCommonInfo.Controls.Add(this._surIndicator, 0, 0);
            this.tlpCommonInfo.Controls.Add(this.lbSign, 3, 1);
            this.tlpCommonInfo.Controls.Add(this.lbKnpStatus, 2, 2);
            this.tlpCommonInfo.Controls.Add(this.lbTaxPayerName, 1, 0);
            this.tlpCommonInfo.Controls.Add(this.tableLayoutPanel5, 1, 1);
            this.tlpCommonInfo.Controls.Add(this.lbDocumentCorrectionNumber, 2, 1);
            this.tlpCommonInfo.Controls.Add(this.tableLayoutPanel6, 1, 2);
            this.tlpCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.tlpCommonInfo.Location = new System.Drawing.Point(5, 5);
            this.tlpCommonInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCommonInfo.Name = "tlpCommonInfo";
            this.tlpCommonInfo.RowCount = 3;
            this.tlpCommonInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCommonInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCommonInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCommonInfo.Size = new System.Drawing.Size(687, 60);
            this.tlpCommonInfo.TabIndex = 0;
            this.tlpCommonInfo.SizeChanged += new System.EventHandler(this.tlpCommonInfo_SizeChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.ReorgInnKppLabel, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lbTaxPayerInnKpp, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(26, 20);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(264, 20);
            this.tableLayoutPanel5.TabIndex = 41;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.ReorgSonoLabel, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.lbDocumentPeriodName, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(26, 40);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(264, 20);
            this.tableLayoutPanel6.TabIndex = 42;
            // 
            // uegbUserTaskProcessingDemand
            // 
            this.uegbUserTaskProcessingDemand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbUserTaskProcessingDemand.Controls.Add(this.uegbUserTaskProcessingDemandPanel);
            this.uegbUserTaskProcessingDemand.ExpandedSize = new System.Drawing.Size(703, 49);
            this.uegbUserTaskProcessingDemand.ExpansionIndicator = Infragistics.Win.Misc.GroupBoxExpansionIndicator.None;
            this.uegbUserTaskProcessingDemand.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.None;
            this.uegbUserTaskProcessingDemand.Location = new System.Drawing.Point(3, 176);
            this.uegbUserTaskProcessingDemand.Name = "uegbUserTaskProcessingDemand";
            this.uegbUserTaskProcessingDemand.Size = new System.Drawing.Size(703, 49);
            this.uegbUserTaskProcessingDemand.TabIndex = 10;
            this.uegbUserTaskProcessingDemand.Text = "Сведения о выполнении пользовательского задания";
            this.uegbUserTaskProcessingDemand.Visible = false;
            // 
            // uegbUserTaskProcessingDemandPanel
            // 
            this.uegbUserTaskProcessingDemandPanel.AutoSize = true;
            this.uegbUserTaskProcessingDemandPanel.Controls.Add(this.ULSizeLabel);
            this.uegbUserTaskProcessingDemandPanel.Controls.Add(this.lbDemandSeod);
            this.uegbUserTaskProcessingDemandPanel.Controls.Add(this.tableLayoutPanel3);
            this.uegbUserTaskProcessingDemandPanel.Controls.Add(this.ultraLabel8);
            this.uegbUserTaskProcessingDemandPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uegbUserTaskProcessingDemandPanel.Location = new System.Drawing.Point(3, 16);
            this.uegbUserTaskProcessingDemandPanel.Name = "uegbUserTaskProcessingDemandPanel";
            this.uegbUserTaskProcessingDemandPanel.Padding = new System.Windows.Forms.Padding(5);
            this.uegbUserTaskProcessingDemandPanel.Size = new System.Drawing.Size(697, 30);
            this.uegbUserTaskProcessingDemandPanel.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(687, 0);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // uegbUserTaskProcessingAnswer
            // 
            this.uegbUserTaskProcessingAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbUserTaskProcessingAnswer.Controls.Add(this.uegbUserTaskProcessingAnswerPanel);
            this.uegbUserTaskProcessingAnswer.ExpandedSize = new System.Drawing.Size(703, 69);
            this.uegbUserTaskProcessingAnswer.ExpansionIndicator = Infragistics.Win.Misc.GroupBoxExpansionIndicator.None;
            this.uegbUserTaskProcessingAnswer.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.None;
            this.uegbUserTaskProcessingAnswer.Location = new System.Drawing.Point(3, 231);
            this.uegbUserTaskProcessingAnswer.Name = "uegbUserTaskProcessingAnswer";
            this.uegbUserTaskProcessingAnswer.Size = new System.Drawing.Size(703, 69);
            this.uegbUserTaskProcessingAnswer.TabIndex = 11;
            this.uegbUserTaskProcessingAnswer.Text = "Сведения о выполнении пользовательского задания";
            this.uegbUserTaskProcessingAnswer.Visible = false;
            // 
            // uegbUserTaskProcessingAnswerPanel
            // 
            this.uegbUserTaskProcessingAnswerPanel.AutoSize = true;
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.lbAnswerStatus);
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.lbAnswerDate);
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.ultraLabel3);
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.ultraLabel5);
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.lbAnswerSeod);
            this.uegbUserTaskProcessingAnswerPanel.Controls.Add(this.Label33);
            this.uegbUserTaskProcessingAnswerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uegbUserTaskProcessingAnswerPanel.Location = new System.Drawing.Point(3, 16);
            this.uegbUserTaskProcessingAnswerPanel.Name = "uegbUserTaskProcessingAnswerPanel";
            this.uegbUserTaskProcessingAnswerPanel.Padding = new System.Windows.Forms.Padding(5);
            this.uegbUserTaskProcessingAnswerPanel.Size = new System.Drawing.Size(697, 50);
            this.uegbUserTaskProcessingAnswerPanel.TabIndex = 0;
            // 
            // uegbUserTaskProcessingAnalisis
            // 
            this.uegbUserTaskProcessingAnalisis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbUserTaskProcessingAnalisis.Controls.Add(this.uegbUserTaskProcessingAnalisisPanel);
            this.uegbUserTaskProcessingAnalisis.ExpandedSize = new System.Drawing.Size(703, 202);
            this.uegbUserTaskProcessingAnalisis.ExpansionIndicator = Infragistics.Win.Misc.GroupBoxExpansionIndicator.None;
            this.uegbUserTaskProcessingAnalisis.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.None;
            this.uegbUserTaskProcessingAnalisis.Location = new System.Drawing.Point(3, 306);
            this.uegbUserTaskProcessingAnalisis.Name = "uegbUserTaskProcessingAnalisis";
            this.uegbUserTaskProcessingAnalisis.Size = new System.Drawing.Size(703, 202);
            this.uegbUserTaskProcessingAnalisis.TabIndex = 12;
            this.uegbUserTaskProcessingAnalisis.Text = "Сведения о выполнении пользовательского задания";
            this.uegbUserTaskProcessingAnalisis.Visible = false;
            // 
            // uegbUserTaskProcessingAnalisisPanel
            // 
            this.uegbUserTaskProcessingAnalisisPanel.AutoSize = true;
            this.uegbUserTaskProcessingAnalisisPanel.Controls.Add(this.txtComment);
            this.uegbUserTaskProcessingAnalisisPanel.Controls.Add(this.label66);
            this.uegbUserTaskProcessingAnalisisPanel.Controls.Add(this.cbTaxPayerSolvency);
            this.uegbUserTaskProcessingAnalisisPanel.Controls.Add(this.lbAnalysisSeod);
            this.uegbUserTaskProcessingAnalisisPanel.Controls.Add(this.Label55);
            this.uegbUserTaskProcessingAnalisisPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uegbUserTaskProcessingAnalisisPanel.Location = new System.Drawing.Point(3, 16);
            this.uegbUserTaskProcessingAnalisisPanel.Name = "uegbUserTaskProcessingAnalisisPanel";
            this.uegbUserTaskProcessingAnalisisPanel.Padding = new System.Windows.Forms.Padding(5);
            this.uegbUserTaskProcessingAnalisisPanel.Size = new System.Drawing.Size(697, 183);
            this.uegbUserTaskProcessingAnalisisPanel.TabIndex = 0;
            // 
            // uegbAssign
            // 
            this.uegbAssign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uegbAssign.Controls.Add(this.uegbAssignPanel);
            this.uegbAssign.ExpandedSize = new System.Drawing.Size(703, 158);
            this.uegbAssign.Location = new System.Drawing.Point(3, 514);
            this.uegbAssign.Name = "uegbAssign";
            this.uegbAssign.Size = new System.Drawing.Size(703, 158);
            this.uegbAssign.TabIndex = 13;
            this.uegbAssign.Text = "Список исполнителей";
            // 
            // uegbAssignPanel
            // 
            this.uegbAssignPanel.AutoSize = true;
            this.uegbAssignPanel.Controls.Add(this.gridActor);
            this.uegbAssignPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uegbAssignPanel.Location = new System.Drawing.Point(3, 19);
            this.uegbAssignPanel.Name = "uegbAssignPanel";
            this.uegbAssignPanel.Padding = new System.Windows.Forms.Padding(5);
            this.uegbAssignPanel.Size = new System.Drawing.Size(697, 136);
            this.uegbAssignPanel.TabIndex = 0;
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Location = new System.Drawing.Point(3, 3);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this.tlpCommonInfo.SetRowSpan(this._surIndicator, 3);
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 34;
            // 
            // gridActor
            // 
            this.gridActor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridActor.GridContextMenuStrip = null;
            this.gridActor.Location = new System.Drawing.Point(8, 8);
            this.gridActor.Margin = new System.Windows.Forms.Padding(0);
            this.gridActor.Name = "gridActor";
            this.gridActor.Size = new System.Drawing.Size(681, 120);
            this.gridActor.TabIndex = 1;
            // 
            // UserTaskDetailsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(743, 741);
            this.Controls.Add(this.tlpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserTaskDetailsView";
            this.Text = "UserTaskDetailsForm";
            _detailsBoxArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbTaxPayerSolvency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsBox)).EndInit();
            this.DetailsBox.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraBoxPanel1.ResumeLayout(false);
            this.ultraPanel22.ResumeLayout(false);
            this.ultraPanel33.ResumeLayout(false);
            this.ultraPanel111.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.tlpButtons.ResumeLayout(false);
            this.tlpButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskInfo)).EndInit();
            this.uegbUserTaskInfo.ResumeLayout(false);
            this.uegbUserTaskInfo.PerformLayout();
            this.uegbUserTaskInfoPanel.ResumeLayout(false);
            this.uegbUserTaskInfoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).EndInit();
            this.uegbCommonInfo.ResumeLayout(false);
            this.uegbCommonInfo.PerformLayout();
            this.ugbCommonInfoPanel.ResumeLayout(false);
            this.ugbCommonInfoPanel.PerformLayout();
            this.tlpCommonInfo.ResumeLayout(false);
            this.tlpCommonInfo.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingDemand)).EndInit();
            this.uegbUserTaskProcessingDemand.ResumeLayout(false);
            this.uegbUserTaskProcessingDemand.PerformLayout();
            this.uegbUserTaskProcessingDemandPanel.ResumeLayout(false);
            this.uegbUserTaskProcessingDemandPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingAnswer)).EndInit();
            this.uegbUserTaskProcessingAnswer.ResumeLayout(false);
            this.uegbUserTaskProcessingAnswer.PerformLayout();
            this.uegbUserTaskProcessingAnswerPanel.ResumeLayout(false);
            this.uegbUserTaskProcessingAnswerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbUserTaskProcessingAnalisis)).EndInit();
            this.uegbUserTaskProcessingAnalisis.ResumeLayout(false);
            this.uegbUserTaskProcessingAnalisis.PerformLayout();
            this.uegbUserTaskProcessingAnalisisPanel.ResumeLayout(false);
            this.uegbUserTaskProcessingAnalisisPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbAssign)).EndInit();
            this.uegbAssign.ResumeLayout(false);
            this.uegbAssign.PerformLayout();
            this.uegbAssignPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lbKnpStatus;
        private Infragistics.Win.Misc.UltraLabel lbNdsTotal;
        private Infragistics.Win.Misc.UltraLabel lbSign;
        private Infragistics.Win.Misc.UltraLabel lbDocumentCorrectionNumber;
        private Infragistics.Win.Misc.UltraLabel lbDocumentPeriodName;
        private Infragistics.Win.Misc.UltraLabel lbTaxPayerInnKpp;
        private Infragistics.Win.Misc.UltraLabel ReorgSonoLabel;
        private Infragistics.Win.Misc.UltraLabel ReorgInnKppLabel;
        private Controls.Grid.V2.GridView gridActor;
        private Infragistics.Win.Misc.UltraButton btSave;
        private Infragistics.Win.Misc.UltraButton btOpenCard;
        private Infragistics.Win.Misc.UltraLabel lbTaxPayerName;
        private Infragistics.Win.Misc.UltraButton btShowAnswer;
        //private Infragistics.Win.UltraWinTabControl.UltraTabControl TaskTypeTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.Misc.UltraLabel ULSizeLabel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtComment;
        private Infragistics.Win.Misc.UltraLabel lbAnswerStatus;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private UltraLabel lbDemandSeod;
        private UltraLabel lbAnswerSeod;
        private UltraLabel ultraLabel8;
        private UltraLabel ultraLabel3;
        private UltraLabel Label33;
        private UltraLabel ultraLabel5;
        private UltraLabel lbAnswerDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbTaxPayerSolvency;
        private UltraLabel lbAnalysisSeod;
        private UltraButton btSetAnswer;
        private UltraLabel label66;
        private UltraLabel Label55;
        private UltraPanel ultraBoxPanel1;
        private UltraPanel ultraPanel22;
        private UltraPanel ultraPanel33;
        private UltraPanel ultraPanel111;
        private UltraGroupBox DetailsBox;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private UltraExpandableGroupBox uegbUserTaskInfo;
        private UltraExpandableGroupBoxPanel uegbUserTaskInfoPanel;
        private UltraLabel lbActualCompleteDate;
        private UltraLabel ULRegistrationDataCaptionLabel;
        private UltraLabel lbStatus;
        private UltraLabel ultraLabel6;
        private UltraLabel ULAccountDataCaptionLabel;
        private UltraLabel lbDaysAfterExpire;
        private UltraLabel ultraLabel2;
        private UltraLabel lbCreateDate;
        private UltraLabel ultraLabel4;
        private UltraLabel lbDuePeriod;
        private SurView _surIndicator;
        private UltraExpandableGroupBox uegbCommonInfo;
        private UltraExpandableGroupBoxPanel ugbCommonInfoPanel;
        private UltraExpandableGroupBox uegbUserTaskProcessingDemand;
        private UltraExpandableGroupBoxPanel uegbUserTaskProcessingDemandPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private UltraExpandableGroupBox uegbUserTaskProcessingAnswer;
        private UltraExpandableGroupBoxPanel uegbUserTaskProcessingAnswerPanel;
        private UltraExpandableGroupBox uegbUserTaskProcessingAnalisis;
        private UltraExpandableGroupBoxPanel uegbUserTaskProcessingAnalisisPanel;
        private UltraExpandableGroupBox uegbAssign;
        private UltraExpandableGroupBoxPanel uegbAssignPanel;
        private System.Windows.Forms.TableLayoutPanel tlpCommonInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
    }
}