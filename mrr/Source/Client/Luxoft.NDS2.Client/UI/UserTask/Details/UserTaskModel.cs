﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.TaxPayer.Details;
using Luxoft.NDS2.Client.UI.UserTask.List;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public class UserTaskModel
    {
        /// <summary>
        /// данные из списка - входные
        /// </summary>
        public UserTaskListModel UserTaskListModel { get; set; }


        /// <summary>
        /// Сведения о пользовательском задании
        /// </summary>
        public UserTaskDetailsModel UserTaskDetailsModel { get; set; }

        /// <summary>
        /// sur src
        /// </summary>
        public DictionarySur Sur { get; set; }

    }
}
