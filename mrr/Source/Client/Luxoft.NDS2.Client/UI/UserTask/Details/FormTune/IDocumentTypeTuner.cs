﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public interface IDocumentTypeTuner
    {
        IUserTaskDetailsView View { get; set; }
        void Process(UserTaskDetailsModel model);
    }
}