﻿namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public class DemandTuner : TypeTuner
    {
        public override void Process(UserTaskDetailsModel cardData)
        {
            View.LbDemandSeodText = cardData.GetDemandSeod();
        }

        public override void TuneButtonBlock()
        {
            View.BtOpenCardVisible = true;
            View.BtShowAnswerVisible = false;
            View.BtSetAnswerVisible = false;
            View.BtSaveVisible = false;
        }
    }
}