﻿namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public class AnswerTuner : TypeTuner
    {
        public override void Process(UserTaskDetailsModel cardData)
        {
            View.LbAnswerSeodText = cardData.GetDemandSeod();
            View.LbAnswerDateText = cardData.AnswerDate.NullFormat();
            View.LbAnswerStatusText = cardData.AnswerStatus;
        }

        public override void TuneButtonBlock()
        {
            View.BtOpenCardVisible = true;
            //View.BtShowAnswer, View.BtSetAnswer.Visible - настраивается отдельно
            View.BtSaveVisible = false;
        }
    }
}