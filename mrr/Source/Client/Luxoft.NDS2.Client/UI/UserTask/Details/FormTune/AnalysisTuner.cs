﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public class AnalysisTuner : TypeTuner
    {
        private bool _solvencyIsSelected;
        private bool _correctionIsAnnulment;

        public override void Process(UserTaskDetailsModel cardData)
        {
            View.LbAnalysisSeodText = cardData.GetAnalysisSeod();
            View.CbTaxPayerSolvencyValue = cardData.TaxPayerSolvency;
            View.TxtCommentText = cardData.Comment;

            _solvencyIsSelected = cardData.TaxPayerSolvency != null;
            _correctionIsAnnulment = cardData.IsCorrectionKnpClosedByAnnulment;

            //редактирование Доступно для пользователя с ролью «Инспектор» 
            //если статус НП ещё не был выбран.
            if (UserTaskRole == UserTaskRole.Inspector && !_solvencyIsSelected && !_correctionIsAnnulment)
            {
                View.CbTaxPayerSolvencyEnabled = true;
                View.TxtCommentEnabled = true;
            }
            else
            {
                View.CbTaxPayerSolvencyEnabled = false;
                View.TxtCommentEnabled = false;
            }
        }

        public override void TuneButtonBlock()
        {
            View.BtOpenCardVisible = false;
            View.BtShowAnswerVisible = false;
            View.BtSetAnswerVisible = false;

            //BtSave Видима для пользовтаеля с ролью «Инспектор» на которого назначено данное задание, 
            //если статус НП ещё не был выбран
            //и корреткировка не является аннулированной
            if (UserTaskRole == UserTaskRole.Inspector && !_solvencyIsSelected && !_correctionIsAnnulment)
            {
                View.BtSaveVisible = true;
                //Доступно, если пользователь выбрал значение в ниспадающем списке «Статус НП» (любое, кроме пустой сторки).                
                View.BtSaveEnabled = (View.CbTaxPayerSolvencyValue != null);
            }
            else
                View.BtSaveVisible = false;
        }
    }
}