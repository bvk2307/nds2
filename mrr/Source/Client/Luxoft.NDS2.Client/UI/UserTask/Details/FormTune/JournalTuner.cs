﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public class JournalTuner : IDocumentTypeTuner
    {
        private const string VerNum = "Номер версии: ";
        public IUserTaskDetailsView View { get; set; }
        public void Process(UserTaskDetailsModel model)
        {
            View.LbDocumentCorrectionNumberText = VerNum + model.CorrectionNumber;
            View.LbKnpStatusText = "";
            View.LbSignText = "";
            View.LbNdsTotalText = "";
        }
    }
}