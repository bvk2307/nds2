﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public abstract class TypeTuner
    {
        /// <summary>
        /// роль в польз заданиях
        /// </summary>
        public UserTaskRole UserTaskRole { get; set; }

        public IUserTaskDetailsView View { get; set; }

        public abstract void Process(UserTaskDetailsModel cardData);

        /// <summary>
        /// настройка кнопок в нижней части формы
        /// </summary>
        public abstract void TuneButtonBlock();
    }
}