﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details.FormTune
{
    public class DeclarationTuner : IDocumentTypeTuner
    {
        private const string CorrectionNumber = "Номер корр.:   ";
        private const string KnpStatus = "Статус  КНП:   ";
        private const string Sign = "Признак НД:   ";
        private const string NdsTotal = "Сумма НДС:   ";

        public IUserTaskDetailsView View { get; set; }
        public void Process(UserTaskDetailsModel model)
        {
            View.SurIndicator.ARGB = model.SurARGB;
            View.SurIndicator.Description = model.SurDescription;

            View.LbDocumentCorrectionNumberText = CorrectionNumber + model.CorrectionNumber;
            View.LbKnpStatusText = KnpStatus + model.KnpStatus;
            View.LbSignText = Sign + model.Sign;
            View.LbNdsTotalText = NdsTotal + model.NdsTotal.SumFormat();
        }
    }
}