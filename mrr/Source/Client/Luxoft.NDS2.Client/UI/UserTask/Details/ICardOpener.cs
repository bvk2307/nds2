﻿using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public interface ICardOpener
    {
        void ViewReclaimDetails(DiscrepancyDocumentInfo documentInfo);
        void ViewExplainReplyDetails(ExplainReplyCardData documentInfo);
		  ExplainReplyCardData GetExplainCardRcd(
			  long explainReplayId,
			  UserAccessOpearation userAccessOpearation = null);
    }
}