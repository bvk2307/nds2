﻿using System.Collections.Generic;
using System.Drawing;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
	public interface IUserTaskDetailsView
	{
		ISurView SurIndicator { get; }
		string LbDuePeriodText { set; }
		string LbCreateDateText { set; }
		string LbKnpStatusText { set; }
		string LbNdsTotalText { set; }
		string LbSignText { set; }
		string LbDocumentCorrectionNumberText { set; }
		string LbDocumentPeriodNameText { set; }
		string LbTaxPayerInnKppText { set; }
		string LbTaxPayerNameText { set; }
		string LbDemandSeodText { set; }
		string LbAnswerSeodText { set; }
		string LbAnswerDateText { set; }
		string LbAnswerStatusText { set; }
		string LbAnalysisSeodText { set; }
		long? CbTaxPayerSolvencyValue { get; set; }
		IEnumerable<TaxPayerSolvency> CbTaxPayerSolvencyDataSource { set; }
		bool CbTaxPayerSolvencyEnabled { set; }
		bool TxtCommentEnabled { set; }
		string TxtCommentText { get; set; }
		string Caption { set; }
		bool BtSaveVisible { set; }
		bool BtSaveEnabled { set; }
		bool BtOpenCardVisible { set; }
		bool BtShowAnswerVisible { set; }
		bool BtSetAnswerVisible { set; }
		bool BtSetAnswerEnabled { set; }
		string LbActualCompleteDateText { set; }
		string LbDaysAfterExpireText { set; }
		Color LbDaysAfterExpireForeColor { set; }
		Color LbStatusForeColor { set; }
		string LbStatusText { set; }

		/// <summary>
		///  настройка видимости полей формы по тип ПЗ
		/// </summary>
		void SetTab(TaskType taskType);

		event ParameterlessEventHandler OnSave;
		event ParameterlessEventHandler OnShowAnswer;
		event ParameterlessEventHandler OnSetAnswer;
		event ParameterlessEventHandler OnOpenCard;
		event ParameterlessEventHandler OnTaxPayerSolvencySelection;

		/// <summary>
		/// заполнить исполнителей
		/// </summary>
		/// <param name="summaryModelList"></param>
		void LoadActorList(List<Actor> summaryModelList);

		void ShowForm();
		void Close();
	}
}