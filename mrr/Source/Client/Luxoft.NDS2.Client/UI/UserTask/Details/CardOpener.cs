﻿using System;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Client.UI.UserTask.List;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public class CardOpener : BasePresenter<IUserTaskListView>, ICardOpener
    {
        public CardOpener(
            PresentationContext presentationContext, 
            WorkItem wi, 
            IUserTaskListView view) : base(presentationContext, wi, view){}
    }
}