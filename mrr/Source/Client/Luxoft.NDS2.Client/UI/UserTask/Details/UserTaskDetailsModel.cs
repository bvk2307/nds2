﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.UserTask.List;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public class UserTaskDetailsModel
    {
        private const string StatusOverdueName = "Просрочено";
        private const int StatusCreatedId = 3;
        /// <summary>
        /// Сведения о пользовательском задании - из списка
        /// </summary>
        private UserTaskListModel _userTaskListModel;

        /// <summary>
        /// Сведения о пользовательском задании - Details
        /// </summary>
        private UserTaskInDetails _userTaskInDetails;

        public DictionarySur Sur
        {
            get;
            private set;
        }



        public UserTaskDetailsModel(
            UserTaskListModel userTaskListModel, 
            UserTaskInDetails userTaskInDetails, 
            DictionarySur sur)
        {
            _userTaskListModel = userTaskListModel;
            _userTaskInDetails = userTaskInDetails;
            Sur = sur;
        }


        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreateDate { get { return _userTaskListModel.CreateDate; } }

        /// <summary>
        /// Срок выполнения задания (в днях)
        /// </summary>
        public int DuePeriod { get { return _userTaskInDetails.DuePeriod; } }

        /// <summary>
        /// Кол-во просроченных дней 
        /// </summary>
        public int DaysAfterExpire { get { return _userTaskInDetails.DaysAfterExpire; } }

        /// <summary>
        /// Статус задания - идентификатор
        /// </summary>
        public int Status { get { return _userTaskInDetails.Status; } }

        /// <summary>
        /// Статус задания
        /// </summary>
        public string StatusName
        {
            get
            {
                return _userTaskInDetails.Status == StatusCreatedId &&
                                 _userTaskInDetails.PlannedCompleteDate.GetValueOrDefault().Date <
                                 _userTaskInDetails.ActualCompleteDate.GetValueOrDefault(DateTime.Now).Date
                          ? StatusOverdueName
                          : _userTaskInDetails.StatusName;
            }
        }

        /// <summary>
        /// Дата выполнения: Фактическая
        /// </summary>
        public DateTime? ActualCompleteDate { get { return _userTaskInDetails.ActualCompleteDate; } }




        /// <summary>
        ///  Связанный документ: Code Типа
        /// </summary>
        public int DocumentTypeCode { get { return _userTaskListModel.GetDocumentTypeCode(); } }

        /// <summary>
        ///  Тип задания
        /// </summary>
        public string TaskTypeName { get { return _userTaskListModel.TaskType; } }

        public int TaskType { get { return _userTaskListModel.GetTaskTypeCode(); } }

        #region общие сведения

        /// <summary>
        /// Отчетный период
        /// </summary>
        public string DocumentPeriodName { get { return _userTaskListModel.DocumentPeriodName; } }


        /// <summary>
        /// СУР ARGB
        /// </summary>
        public int SurARGB { get { return Sur.GetARGB(_userTaskInDetails.SurCode); } }
        /// <summary>
        /// СУР Description
        /// </summary>
        public string SurDescription { get { return Sur.Description(_userTaskInDetails.SurCode); } }

        /// <summary>
        /// Налогоплательщик: ИНН
        /// </summary>
        public string TaxPayerInn { get { return _userTaskListModel.TaxPayerInn; } }

        /// <summary>
        /// Налогоплательщик: КПП
        /// </summary>
        public string TaxPayerKpp { get { return _userTaskListModel.TaxPayerKpp; } }

        /// <summary>
        /// Налогоплательщик: Наименование
        /// </summary>
        public string TaxPayerName { get { return _userTaskListModel.TaxPayerName; } }




        /// <summary>
        /// Номер корр./версии 
        /// </summary>
        public string CorrectionNumber { get { return _userTaskListModel.DocumentCorrectionNumber; } }

        /// <summary>
        /// Статус  КНП
        /// </summary>
        public string KnpStatus { get { return _userTaskInDetails.KnpStatus; } }

        /// <summary>
        /// Признак НД
        /// </summary>
        public string Sign { get { return _userTaskInDetails.Sign; } }

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal? NdsTotal { get { return _userTaskInDetails.NdsTotal; } }

        #endregion


        //Истребование документов/Ввод ответа на истребование

        /// <summary>
        /// тип связанного документа СЭОД
        /// </summary>
        public string ClaimdocTypeName { get { return _userTaskInDetails.ClaimdocTypeName; } }

        /// <summary>
        /// Дата связанного документа СЭОД
        /// </summary>
        public DateTime? ClaimdocDate { get { return _userTaskInDetails.ClaimdocDate; } }

        /// <summary>
        /// Номер связанного документа СЭОД
        /// </summary>
		  public string ClaimdocNumber { get { return _userTaskInDetails.SeodNumber; } }

        /// <summary>
        /// статус из связанного  ответа 
        /// </summary>
        public string AnswerStatus { get { return _userTaskInDetails.AnswerStatus; } }

        /// <summary>
        /// статус из связанного  ответа  - код
        /// </summary>
        public int? AnswerStatusCode { get { return _userTaskInDetails.AnswerStatusCode; } }

        /// <summary>
        /// id связанного ответа 
        /// </summary>
        public long? ExplainId { get { return _userTaskInDetails.ExplainId; } }

        /// <summary>
        /// дата из связанного ответа 
        /// </summary>
        public DateTime? AnswerDate { get { return _userTaskInDetails.AnswerDate; } }

        /// <summary>
        /// «Номер» данный при создании в АСК НДС-2
        /// </summary>
        public int? ClaimdocCreateNumber { get { return _userTaskInDetails.ClaimdocCreateNumber; } }


        //Если в соответствующей сущности, с которой связано ПЗ, 
        //«Автоистребование по 93» / «Автоистребование по 93.1» значение атрибута «Номер регистрации в СЭОД» отсутствует, 
        //то указывается «Номер» данный при создании в АСК НДС-2 (со звездочкой), например «* 112»

        public string GetDemandSeod()
        {
            if (!string.IsNullOrWhiteSpace(ClaimdocNumber)) return string.Format("№ {0} от {1}", ClaimdocNumber, ClaimdocDate.NullFormat());
				return string.Format("№ * {0}", ClaimdocCreateNumber);
        }


        //Анализ налогоплательщика


        public string GetAnalysisSeod()
        {
            return string.Format("{0} № {1} от {2}", ClaimdocTypeName, ClaimdocNumber, ClaimdocDate.NullFormat());
        }

        /// <summary>
        /// Статус  налогоплательщика
        /// </summary>
        public int? TaxPayerSolvency { get { return _userTaskInDetails.TaxPayerSolvency; } }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get { return _userTaskInDetails.Comment; } }


        /// <summary>
        /// Список исполнителей -источник данных для grid
        /// </summary>
        public List<Actor> ActorList { get { return _userTaskInDetails.ActorList; } }

        /// <summary>
        /// Статус налогоплательщика -источник данных для combo
        /// </summary>
        public List<TaxPayerSolvency> TaxPayerSolvencyList { get { return _userTaskInDetails.TaxPayerSolvencyList; } }

        /// <summary>
        /// Ввести ответ - Доступна
        /// </summary>
        public int OtherAnswerNotExists { get { return _userTaskInDetails.OtherAnswerNotExists; } }

        /// <summary>
        /// Признак того, что корректировка аннулирована
        /// </summary>
        public bool IsCorrectionKnpClosedByAnnulment
        {
            get { return Utils.IsKnpClosedByAnnulment(_userTaskInDetails.KnpCloseReasonId); }
        }
    }
}