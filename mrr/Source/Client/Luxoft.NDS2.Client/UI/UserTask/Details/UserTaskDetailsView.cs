﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.Details
{
    public partial class UserTaskDetailsView : Form, IUserTaskDetailsView
    {
        const int DemandReduction = 100;
        const int AnswerReduction = 80;

        public UserTaskDetailsView()
        {
            InitializeComponent();
            //todo: тз? -или сначала недоступна
            InitTaxPayerSolvencySelectionCetch();
            InitActorGrid();
        }

        public string Caption
        {
            set { this.Text = value; }
        }
		  public string LbActualCompleteDateText
        {
            set { lbActualCompleteDate.Text = value; }
        }

		  public string LbDaysAfterExpireText
        {
			  set { lbDaysAfterExpire.Text = value; }
        }

		  public Color LbDaysAfterExpireForeColor
        {
			  set { lbDaysAfterExpire.ForeColor = value; }
        }

		  public Color LbStatusForeColor
        {
			  set { lbStatus.ForeColor = value; }
        }

		  public string LbStatusText
        {
			  set { lbStatus.Text = value; }
        }

		  public string LbDuePeriodText
        {
			  set { lbDuePeriod.Text = value; }
        }

		  public string LbCreateDateText
        {
			  set { lbCreateDate.Text = value; }
        }

		  public string LbKnpStatusText
        {
			  set { lbKnpStatus.Text = value; }
        }

		  public string LbNdsTotalText
        {
			  set { lbNdsTotal.Text = value; }
        }

		  public string LbSignText
        {
			  set { lbSign.Text = value; }
        }

		  public string LbDocumentCorrectionNumberText
        {
			  set { lbDocumentCorrectionNumber.Text = value; }
        }

		  public string LbDocumentPeriodNameText
        {
			  set { lbDocumentPeriodName.Text = value; }
        }

		  public string LbTaxPayerInnKppText
        {
			  set { lbTaxPayerInnKpp.Text = value; }
        }

		  public string LbTaxPayerNameText
        {
			  set { lbTaxPayerName.Text = value; }
        }

		  public string LbDemandSeodText
        {
			  set { lbDemandSeod.Text = value; }
        }

		  public string LbAnswerSeodText
        {
			  set { lbAnswerSeod.Text = value; }
        }

		  public string LbAnswerDateText
        {
			  set { lbAnswerDate.Text = value; }
        }
		  public string LbAnswerStatusText
        {
			  set { lbAnswerStatus.Text = value; }
        }

		  public string LbAnalysisSeodText
        {
			  set { lbAnalysisSeod.Text = value; }
        }

		  public long? CbTaxPayerSolvencyValue
        {
			   get { return cbTaxPayerSolvency.Value as long? ; }
            set { cbTaxPayerSolvency.Value = value; }
        }

		  public IEnumerable<TaxPayerSolvency> CbTaxPayerSolvencyDataSource
        {
            set { cbTaxPayerSolvency.DataSource = value; }
        }

		  public bool CbTaxPayerSolvencyEnabled
        {
            set { cbTaxPayerSolvency.Enabled = value; }
        }

		  public bool TxtCommentEnabled
        {
			  set { txtComment.Enabled = value; }
        }

		  public string TxtCommentText
        {
			  get { return txtComment.Text ; }
			  set { txtComment.Text = value; }
        }



		  public bool BtSaveVisible
        {
            set { btSave.Visible = value; }
        }

		  public bool BtSaveEnabled
        {
            set { btSave.Enabled = value; }
        }

		  public bool BtOpenCardVisible
        {
			  set { btOpenCard.Visible = value; ; }
        }

		  public bool BtShowAnswerVisible
        {
			  set { btShowAnswer.Visible = value; }
        }

		  public bool BtSetAnswerVisible
        {
			  set { btSetAnswer.Visible = value; }
        }

		  public bool BtSetAnswerEnabled
        {
			  set { btSetAnswer.Enabled = value; }
        }



        public ISurView SurIndicator
        {
            get { return _surIndicator; }
        }


        /// <summary>
        ///  настройка видимости полей формы по тип ПЗ
        /// </summary>
        public void SetTab(TaskType taskType)
        {
            switch (taskType)
            {
                case TaskType.Demand:
                    uegbUserTaskProcessingDemand.Visible = true;
                    break;
                case TaskType.Answer:
                    uegbUserTaskProcessingAnswer.Visible = true;
                    break;
                case TaskType.Analysis:
                    uegbUserTaskProcessingAnalisis.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// добавить отлов смены 'Статус  налогоплательщика'
        /// </summary>
        public void InitTaxPayerSolvencySelectionCetch()
        {
            OnTaxPayerSolvencySelection += () =>
            {
                //Доступно, если пользователь выбрал значение в ниспадающем списке «Статус НП» (любое, кроме пустой сторки).
                BtSaveEnabled = CbTaxPayerSolvencyValue != null;
            };
        }


        # region Обработка событий
        public event ParameterlessEventHandler OnSave;
        public event ParameterlessEventHandler OnShowAnswer;
        public event ParameterlessEventHandler OnSetAnswer;
        public event ParameterlessEventHandler OnOpenCard;
        public event ParameterlessEventHandler OnTaxPayerSolvencySelection;


        private void btSave_Click(object sender, EventArgs e)
        {
            if (OnSave != null) OnSave();
        }


        private void btShowAnswer_Click(object sender, EventArgs e)
        {
            if (OnShowAnswer != null) OnShowAnswer();
        }

        private void btSetAnswer_Click(object sender, EventArgs e)
        {
            if (OnSetAnswer != null) OnSetAnswer();
        }

        private void btOpenCard_Click(object sender, EventArgs e)
        {
            if (OnOpenCard != null) OnOpenCard();
        }

        private void cbTaxPayerSolvency_SelectionChanged(object sender, EventArgs e)
        {
            if (OnTaxPayerSolvencySelection != null) OnTaxPayerSolvencySelection();
        }

        private void tlpCommonInfo_SizeChanged(object sender, EventArgs e)
        {
            uegbCommonInfo.Height += tlpCommonInfo.Height + ugbCommonInfoPanel.Padding.Top + ugbCommonInfoPanel.Padding.Bottom - ugbCommonInfoPanel.Height;
        }

        # endregion

        # region Список исполнителей

        private void InitActorGrid()
        {
            gridActor.InitColumns(SetupGrid());
            gridActor.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        public void LoadActorList(List<Actor> summaryModelList)
        {
            gridActor.PushData(summaryModelList);
        }

	    /// <summary>
        ///     Общие сведения -назначение колонок
        /// </summary>
        /// <returns></returns>
        private GridColumnSetup SetupGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<Actor>(gridActor);

            setup
                .WithColumn(helper.CreateTextColumn(o => o.AssignedName).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }
                ))
                .WithColumn(helper.CreateTextColumn(o => o.AssignDate).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                    c.CellTextAlign = HorizontalAlign.Center;
                }));

            return setup;
        }

        # endregion

		  public void ShowForm()
		  {
			  ShowDialog();
		  }

    }
}
