﻿using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.UserTask
{
    public interface IUserTaskReportOpener
    {
        void Open(DictionaryItem[] taskTypes, AccessContext context);
    }
}
