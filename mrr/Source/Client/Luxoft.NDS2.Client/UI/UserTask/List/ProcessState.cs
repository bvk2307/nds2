namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    /// <summary>
    /// �����������
    /// </summary>
    public enum ProcessState
    {
        /// <summary>
        /// ������� ����������
        /// </summary>
        Expired = 1,
        /// <summary>
        /// �������� ���� ���������� �������
        /// </summary>
        NearDeadline = 2,
        /// <summary>
        /// �����
        /// </summary>
        New = 3,
        /// <summary>
        /// ������� ���������
        /// </summary>
        Ready = 4,  
        /// <summary>
        /// ������� �������
        /// </summary>
        Closed = 5

    }
}