﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using System;
using System.Collections.Generic;
using System.Drawing;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    public interface IUserTaskListView : IViewBase
    {
        # region События формы

        event EventHandler Loaded;
        event ParameterlessEventHandler OnRefresh;

        event EventHandler<GridItemSelectEventArgs> UserTaskSelected;
        event EventHandler<GridItemSelectEventArgs> OpenDeclCardClicked;
        event EventHandler<GridItemSelectEventArgs> OpenTaxPayerCardClicked;
        event EventHandler<GridItemSelectEventArgs> OpenUserTaskClicked;

        event EventHandler OnReportGenerating;

        # endregion

        /// <summary>
        /// Список пользовательских заданий
        /// </summary>
        IDataGridView UserTasks
        {
            get;
        }

        List<Period> PeriodRange { get; set; }
        Dictionary<int, string> ProcessStateDescriptions { set; }
        Dictionary<int, string> ProcessStateDescriptionsForFilter { set; }

        Dictionary<int, string> StatusDescriptions { set; }

        Dictionary<int, string> TypeDescriptions { set; }

        Dictionary<int, Bitmap> StatusIconList { get; set; }

        void InitOperations(bool allowGenerateReport);

        void InitUserTasksGridView(IPager pager, bool showAdminCols, bool showCentralCols, bool showManagementCols);
        void LoadSummaryData(List<SummaryModel> summaryModelList);
    }
}