﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    /// <summary>
    /// Пользовательское задание - Общие сведения - модель
    /// </summary>
    public class SummaryModel
    {
        /// <summary>
        /// dto-Пользовательское задание - Общие сведения
        /// </summary>
        private readonly Summary _summary;

        /// <summary>
        /// .tor - Пользовательское задание - Общие сведения - модель
        /// </summary>
        /// <param name="summary">dto-Пользовательское задание - Общие сведения</param>
        public SummaryModel(Summary summary)
        {
            _summary = summary;
        }

        [DisplayName("Тип задания")]
        [Description("Тип задания")]
        public string TaskTypeName { get { return _summary.TaskTypeName; } }

        [DisplayName("На исполнении")]
        [Description("Кол-во необработанных пользовательских заданий всего (стату задания = «Создано» и «Просрочено»)")]
        public int UnprocessedQty { get { return _summary.UnprocessedQty; } }

        [DisplayName("Из них просроченных")]
        [Description("Кол-во необработанных просроченных пользовательских заданий (стату задания =«Просрочено»)")]
        public int ExpiredQty { get { return _summary.ExpiredQty; } }

    }
}