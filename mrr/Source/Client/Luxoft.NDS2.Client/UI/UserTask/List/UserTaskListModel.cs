﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    /// <summary>
    /// Модель элемента списка пользовательских заданий (ПЗ)
    /// </summary>
    public class UserTaskListModel
    {
        #region Данные ПЗ
        private const string StatusOverdueName = "Просрочено";
        private const int StatusCreatedId = 3;
        /// <summary>
        /// dto Пользовательское задание
        /// </summary>
        private readonly UserTaskInList _userTaskInList;

        /// <summary>
        /// dto список периодов
        /// </summary>
        private readonly List<DictTaxPeriod> _dictTaxPeriod;

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает новый экземпляр модели элемента списка ПЗ
        /// </summary>
        /// <param name="userTaskInList">Объект с данными ПЗ</param>
        public UserTaskListModel(UserTaskInList userTaskInList, List<DictTaxPeriod> dictTaxPeriod)
        {
            _userTaskInList = userTaskInList;
            _dictTaxPeriod = dictTaxPeriod;
        }

        # endregion       

        # region Атрибуты списка ПЗ

        /// <summary>
        /// Текущее состояние отработки ПЗ
        /// 1 - Просрочено
        /// 2 - Истекает
        /// 3 - Создано
        /// 4 - Выполнено
        /// 5 - Закрыто
        /// </summary>
        [DisplayName] 
        [Description("Напоминание о сроке задания")]
        public int CurrentProcessState { get { return _userTaskInList.CurrentProcessState; } }

        [DisplayName("Статус")]
        [Description("Статус пользовательского задания")]
        public string Status
        {
            get
            {
                return _userTaskInList.Status == StatusCreatedId &&
                                 _userTaskInList.PlannedCompleteDate.GetValueOrDefault().Date <
                                 _userTaskInList.ActualCompleteDate.GetValueOrDefault(DateTime.Now).Date
                          ? StatusOverdueName
                          : _userTaskInList.StatusName;
            }
        }

        [DisplayName("Тип задания")]
        [Description("Тип пользовательского задания")]
        public string TaskType{get{return _userTaskInList.TaskTypeName;}}

        [DisplayName("Дата создания")]
        [Description("Дата создания  пользовательского задания")]
        public DateTime? CreateDate{get{return _userTaskInList.CreateDate;}}

        /// <summary>
        /// Дата выполнения: Плановая
        /// </summary>
        [DisplayName("Назначенная")]
        [Description("Назначенная дата выполнения задания")]
        public DateTime? PlannedCompleteDate { get { return _userTaskInList.PlannedCompleteDate; } }

        /// <summary>
        /// Дата выполнения: Фактическая
        /// </summary>
        [DisplayName("Фактическая")]
        [Description("Фактическая дата выполнения задания")]
        public DateTime? ActualCompleteDate { get { return _userTaskInList.ActualCompleteDate; } }

        /// <summary>
        /// Кол-во просроченных дней
        /// </summary>
        [DisplayName("Кол-во просроченных дней")]
        [Description("Кол-во просроченных рабочих дней")]
        public int DaysAfterExpire { get { return _userTaskInList.DaysAfterExpire; } }

        /// <summary>
        /// Налогоплательщик: ИНН
        /// </summary>
        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string TaxPayerInn { get { return _userTaskInList.TaxPayerInn; } }

        /// <summary>
        /// Налогоплательщик: КПП
        /// </summary>
        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string TaxPayerKpp { get { return _userTaskInList.TaxPayerKpp; } }

        /// <summary>
        /// Налогоплательщик: Наименование
        /// </summary>
        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string TaxPayerName { get { return _userTaskInList.TaxPayerName; } }


        /// <summary>
        /// Регистрационный №
        /// </summary>
        [DisplayName("Регистрационный №")]
        [Description("Регистрационный № СЭОД")]
        public long DocumentNumber { get { return _userTaskInList.DocumentNumber; } }

        /// <summary>
        /// № корр./ версии
        /// </summary>
        [DisplayName("№ корр.")]
        [Description("Номер корректировки декларации")]
        public string DocumentCorrectionNumber { get { return _userTaskInList.DocumentCorrectionNumber; } }

        /// <summary>
        /// Отчетный период
        /// </summary>
        [DisplayName("Отчетный период")]
        [Description("Период за который подается декларация")]
        public string DocumentPeriodName { get { return string.Format("{0} {1}", _dictTaxPeriod.First(x=> x.Code == _userTaskInList.DocumentPeriodCode).Description, _userTaskInList.FiscalYear); } }

        /// <summary>
        /// Инспектор
        /// </summary>
        [DisplayName("Инспектор")]
        [Description("ФИО инспектора")]
        public string AssignedName { get { return _userTaskInList.AssignedName; } }

        /// <summary>
        /// Инспекция: Код
        /// </summary>
        [DisplayName("Код")]
        [Description("Код инспекции")]
        public string SonoCode { get { return _userTaskInList.SonoCode; } }

        /// <summary>
        /// Инспекция: Наименование
        /// </summary>
        [DisplayName("Наименование")]
        [Description("Наименование инспекции")]
        public string SonoName { get { return _userTaskInList.SonoName; } }

        /// <summary>
        /// Регион: Код
        /// </summary>
        [DisplayName("Код")]
        [Description("Код региона")]
        public string RegionCode { get { return _userTaskInList.RegionCode; } }

        /// <summary>
        /// Регион: Наименование
        /// </summary>
        [DisplayName("Наименование")]
        [Description("Наименование региона")]
        public string RegionName { get { return _userTaskInList.RegionName; } }

        # endregion

        # region Методы

        /// <summary>
        /// идентификатор
        /// </summary>
        public long GetId() { return _userTaskInList.Id; }

        /// <summary>
        /// идентификатор документа
        /// </summary>
        public long GetDocumentZip() { return _userTaskInList.DocumentId; }

        /// <summary>
        ///  Связанный документ: Code Типа
        /// </summary>
        public int GetDocumentTypeCode() { return _userTaskInList.DocumentTypeCode; }

        /// <summary>
        ///  Тип задания - Code
        /// </summary>
        public int GetTaskTypeCode() { return _userTaskInList.TaskType; }
    
        # endregion
    }
}