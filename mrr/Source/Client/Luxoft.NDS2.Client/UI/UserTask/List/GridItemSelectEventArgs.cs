﻿using System;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    public class GridItemSelectEventArgs : EventArgs
    {
        public GridItemSelectEventArgs(object selectedDataItem)
        {
            SelectedDataItem = selectedDataItem;
        }

        public object SelectedDataItem
        {
            get;
            private set;
        }
    }
}
