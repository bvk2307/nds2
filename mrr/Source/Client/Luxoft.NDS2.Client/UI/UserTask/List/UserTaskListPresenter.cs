﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Instrumentation;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.UserTask.Details;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using Microsoft.Practices.CompositeUI.Collections;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;


namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    /// <summary>
    /// Presenter для формы «Пользовательские задания»
    /// </summary>
    public class UserTaskListPresenter
    {
        private readonly IUserTaskListView _view;
        private IUserTaskService _serviceProxy;
        private readonly UserTasksModel _model;

        private readonly ISettingsProviderCreator _settingsProviderCreator;

        private PagedDataGridPresenter<UserTaskListModel> _gridPresenterUserTask;
        private ServiceCollection _infrastructureServiceCollection;
        private IServiceRequestWrapper _commonServiceRequestWrapper;
        private UserRights _userRights = new UserRights();
        private DictionarySur _sur;
        private readonly List<DictTaxPeriod> _dictTaxPeriod;
        private ICardOpener _cardOpener;

        private readonly IUserTaskReportOpener _reportOpener;

        private AccessContext _reportAccessContext;

        public UserTaskListPresenter(
            IUserTaskListView view, 
            ISettingsProviderCreator settingsProviderCreator, 
            ServiceCollection infrastructureServiceCollection, 
            ITaxPayerDetails taxPayerDetailsViewer, 
            IDeclarationCardOpener declarationCardOpener, 
            DictionarySur sur,
            List<DictTaxPeriod> dictTaxPeriod,
            ICardOpener cardOpener,
            IUserTaskReportOpener reportOpener)
        {
            _infrastructureServiceCollection = infrastructureServiceCollection;
            _serviceProxy = infrastructureServiceCollection.GetServiceProxy<IUserTaskService>();
            _model = new UserTasksModel();
            _settingsProviderCreator = settingsProviderCreator;
            _taxPayerDetailsViewer = taxPayerDetailsViewer;
            _declarationCardOpener = declarationCardOpener;
            _sur = sur;
            _dictTaxPeriod = dictTaxPeriod;
            _cardOpener = cardOpener;
            _reportOpener = reportOpener;
            _view = view;            
            _commonServiceRequestWrapper = new CommonServiceRequestWrapper(_view, new Logger(new ClientLogger(LogService)));

            InitView();
            BindView();

            LoadData();
        }

        private void InitView()
        {
            _view.PeriodRange = _model.GetPeriodRange().ToList();
            _view.ProcessStateDescriptions = _model.GetProcessStateDescriptionList();
            _view.ProcessStateDescriptionsForFilter = _model.GetProcessStateDescriptionListForFilter();
            _view.StatusIconList = _model.GetStatusIconList();
        }

        private void BindView()
        {
            _view.OnRefresh += RefreshFormData;
            _view.UserTaskSelected +=
                (sender, args) => OpenUserTaskDetails((UserTaskListModel)args.SelectedDataItem);
            _view.OpenDeclCardClicked +=
                (sender, args) => OpenDeclarationCard((UserTaskListModel)args.SelectedDataItem);
            _view.OpenTaxPayerCardClicked +=
                (sender, args) => OpenTaxPayerCard((UserTaskListModel)args.SelectedDataItem);
            _view.OpenUserTaskClicked +=
                (sender, args) => OpenUserTaskDetails((UserTaskListModel)args.SelectedDataItem);
            _view.OnReportGenerating += OpenReportDialog;
        }

        private void LoadData()
        {
            _commonServiceRequestWrapper.ExecuteAsync(
                _ => _serviceProxy.GetUserRights(),
                (_, execRes) =>
                {
                    execRes.Result.IfNotNull(res => _userRights = res);
                    _reportAccessContext =
                        _infrastructureServiceCollection
                            .GetServiceProxy<IUserTaskReportService>()
                            .GetAccesContext().Result;

                    _view.InitOperations(_reportAccessContext.Operations.Any());
                    _view.InitUserTasksGridView(
                    _model.UserTasksPager,
                    _userRights.UserTaskRole == UserTaskRole.Administrator,
                    _userRights.IsCentral,
                    _userRights.IsManagement);
                    
                    InitTasksListPresenter();
                    RefreshFormData();
                });

            _view.StatusDescriptions = GetUserTaskStatusDictionary();
            _view.TypeDescriptions = GetUserTaskTypeDictionary();
        }

        private void RefreshFormData()
        {
            _gridPresenterUserTask.ResetTotalsCache();
            _gridPresenterUserTask.Load();
            _view.LoadSummaryData(SearchSummary());
        }

        public IInstrumentationService LogService
        {
            get
            {
                return _infrastructureServiceCollection.Get<IInstrumentationService>();
            }
        }

        public IUcMessageService MessageService
        {
            get
            {
                return _infrastructureServiceCollection.Get<IUcMessageService>();
            }
        }


	    /// <summary>
	    /// Настройка grid-Presenter для списка ПЗ
	    /// </summary>
	    /// <remarks>изначальная тройная сортировка производится на сервере</remarks>
	    private void InitTasksListPresenter()
	    {
		    _gridPresenterUserTask =
			    new PagedDataGridPresenter<UserTaskListModel>(
				    _view.UserTasks,
				    _model.UserTasksPager,
				    SearchUserTasks
				    );

		    var gridOptions = new GridOptions();
            if (_userRights.UserTaskRole == UserTaskRole.Administrator)
			    gridOptions.DefaultFilter.ApplyFilter(
                    TypeHelper<UserTaskListModel>.GetMemberName(t => t.AssignedName),
                    new FilterCondition(FilterComparisionOperator.Equals, "")
                    );
		    gridOptions.AllowManageFilterSettings = true;
		    gridOptions.AllowManageSettings = true;

            _gridPresenterUserTask.Init(null, gridOptions);
	    }

        /// <summary>
        ///     Пользовательское задание -источник данных для grid
        /// </summary>
        private PageResult<UserTaskListModel> SearchUserTasks(QueryConditions conditions)
        {


            var result = new PageResult<UserTaskListModel>();
                _commonServiceRequestWrapper.Execute(
                    () => _serviceProxy.Search(conditions, _userRights.IsCentral || _userRights.IsManagement),
                    execRes =>
                    {
                        if (execRes != null)
                        {
                            result.Rows = execRes.Result.Rows.Select(dto => new UserTaskListModel(dto, _dictTaxPeriod)).ToList();
                            result.TotalMatches = execRes.Result.TotalMatches;
                            result.TotalAvailable = execRes.Result.TotalAvailable;
                            _model.UserTaskList = result.Rows;
                        }
                    }
                );
             

            return result;
        }

        /// <summary>
        ///     Общие сведения -источник данных для grid
        /// </summary>
        private List<SummaryModel> SearchSummary()
        {
            var result = new List<SummaryModel>();

            _commonServiceRequestWrapper.Execute(
                () => _serviceProxy.GetSummaryList(),
                execRes =>
                {
                    if (execRes != null)
                        result = execRes.Result.Select(summary => new SummaryModel(summary)).ToList();
                }
                );

            return result;
        }

        # region Навигация к другим экранным формам

        private readonly ITaxPayerDetails _taxPayerDetailsViewer;

        private readonly IDeclarationCardOpener _declarationCardOpener;


        public void OpenUserTaskDetails(UserTaskListModel userTask)
        {
            OpenForm(userTask, () =>
            {
                try
                {
                    var pr = new UserTaskDetailsPresenter(
                        _commonServiceRequestWrapper,
                        _infrastructureServiceCollection,
                        _sur,
                        userTask,
                        _userRights,
                        _cardOpener
                        );
                    pr.OpenForm();
                    return new CardOpenSuccess();
                }
                catch (Exception e)
                {
                    return new CardOpenError(e.Message);
                }
            });
        }

        /// <summary>
        /// Открывает карточку декларации
        /// </summary>
        private void OpenDeclarationCard(UserTaskListModel userTask)
        {
            OpenForm(userTask, () =>_declarationCardOpener.Open(userTask.GetDocumentZip()));
        }

        /// <summary>
        /// Открывает карточку НП
        /// </summary>
        private void OpenTaxPayerCard(UserTaskListModel userTask)
        {
            OpenForm(userTask, () => _taxPayerDetailsViewer.ViewByKppOriginal(
                        userTask.TaxPayerInn,
                        userTask.TaxPayerKpp));
        }

        private void OpenForm(UserTaskListModel userTask, Func<CardOpenResult> OpenFunc)
        {
            if (userTask == null)
            {
                _view.ShowNotification("Выберите задание в списке");
                return;
            }

            var tabOpenResult = OpenFunc();
            if (!tabOpenResult.IsSuccess)
                _view.ShowError(tabOpenResult.Message);
        }

        private DictionaryItem[] _taskTypes;

        private Dictionary<int, string> GetUserTaskTypeDictionary()
        {
            var result = new Dictionary<int, string>();

            _commonServiceRequestWrapper.Execute(
                () => _serviceProxy.GetUserTaskTypeDictionary(),
                execRes =>
                {
                    if (execRes != null)
                    {
                        _taskTypes = execRes.Result.ToArray();
                        result = execRes.Result.ToDictionary(x => x.Id, x => x.Description);
                    }
                }
                );

            return result;
        }

        private Dictionary<int, string> GetUserTaskStatusDictionary()
        {
            var result = new Dictionary<int, string>();

            _commonServiceRequestWrapper.Execute(
                () => _serviceProxy.GetUserTaskStatusDictionary(),
                execRes =>
                {
                    if (execRes != null)
                        result = execRes.Result.ToDictionary(x => x.Id, x => x.Description);
                }
                );

            return result;
        }

        private void OpenReportDialog(object sender, EventArgs e)
        {
            try
            {
                _reportOpener.Open(_taskTypes, _reportAccessContext);
            }
            catch
            {
                _view.ShowError("Ошибка загрузки данных");
            }
        }

        # endregion
    }
}