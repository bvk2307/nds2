﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    public class UserTasksModel
    {
        private readonly PagerStateViewModel _userTasksPager = new PagerStateViewModel();

        /// <summary>
        /// List Пользовательское задание - Pager
        /// </summary>
        public IPager UserTasksPager{ get{return _userTasksPager;}}
        public IEnumerable<UserTaskListModel> UserTaskList { get; set; }

        private const string _ExpiredNote = "Задание просрочено";
        private const string _NearDeadlineNote = "Истекает срок выполнения задания";
        private const string _NewNote = "Новое";
        private const string _ReadyNote = "Задание выполнено";
        private const string _CloseNote = "Задание закрыто";

        private const string _ExpiredNoteShortName = "Просрочено";
        private const string _NearDeadlineNoteShortName = "Истекает срок";
        private const string _NewNoteShortName = "Новое";
        private const string _ReadyNoteShortName = "Выполнено";
        private const string _CloseNoteShortName = "Закрыто";

        private static readonly Period _startPeriod = new Period(1, 2015);

        /// <summary>
        /// список фильтра для колонки "Отчетный период"
        /// </summary>
        public IEnumerable<Period> GetPeriodRange()
        {
            var result = new List<Period>();
            for (int year = _startPeriod.Year; year <= DateTime.Today.Year; year++)
            {
                for (var quater = 1; quater <= 4; quater++)
                {
                    if (year == _startPeriod.Year && quater < _startPeriod.QuarterNumber) continue;
                    if (year == DateTime.Today.Year && quater > Math.Ceiling(DateTime.Today.Month / 4m))
                        yield break;

                    yield return (new Period(quater, year));

                }
            }
        }

        /// <summary>
        /// список всплывающих подсказок к колонке "Напоминание"
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> GetProcessStateDescriptionList()
        {
            return new Dictionary<int, string>
            {
                {(int) ProcessState.Expired, _ExpiredNote},
                {(int) ProcessState.NearDeadline, _NearDeadlineNote},
                {(int) ProcessState.New, _NewNote},
                {(int) ProcessState.Ready, _ReadyNote},
                {(int) ProcessState.Closed, _CloseNote}
            };
        }

        /// <summary>
        /// список значений в фильтре к колонке "Напоминание"
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> GetProcessStateDescriptionListForFilter()
        {
            return new Dictionary<int, string>
            {
                {(int) ProcessState.Expired, _ExpiredNoteShortName},
                {(int) ProcessState.NearDeadline, _NearDeadlineNoteShortName},
                {(int) ProcessState.Ready, _ReadyNoteShortName},
                {(int) ProcessState.New, _NewNoteShortName}
            };
        }

        /// <summary>
        /// Статус - иконки
        /// </summary>
        public Dictionary<int, Bitmap> GetStatusIconList()
        {
            return new Dictionary<int, Bitmap>
            {
                {(int) ProcessState.Expired, Properties.Resources.Expired},
                {(int) ProcessState.NearDeadline, Properties.Resources.NearDeadline},
                {(int) ProcessState.New, null},
                {(int) ProcessState.Ready, Properties.Resources.Ready},
                {(int) ProcessState.Closed, Properties.Resources.Ready},
            };
        }
    }
}
