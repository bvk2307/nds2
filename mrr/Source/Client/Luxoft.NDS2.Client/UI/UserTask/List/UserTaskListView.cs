﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Microsoft.Practices.CompositeUI.Commands;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    public partial class UserTaskListView : BaseView, IUserTaskListView
    {
        # region Constructor

        public UserTaskListView(PresentationContext presentationContext)
        {
            InitializeComponent();
            _presentationContext = presentationContext;
            InitSummaryGridView();
        }

        # endregion

        # region Сводные данные

        private void InitSummaryGridView()
        {
            gridSummary.InitColumns(SetupGrid_Summary());
            gridSummary.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        public void LoadSummaryData(List<SummaryModel> summaryModelList)
        {
            gridSummary.PushData(summaryModelList);
        }

        /// <summary>
        ///     Общие сведения -назначение колонок
        /// </summary>
        /// <returns></returns>
        private GridColumnSetup SetupGrid_Summary()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<SummaryModel>(gridSummary);

            setup
                .WithColumn(helper.CreateTextColumn(o => o.TaskTypeName).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }
                ))
                .WithColumn(helper.CreateTextColumn(o => o.UnprocessedQty).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.ExpiredQty).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }));

            return setup;
        }

        # endregion

        # region Presenter init

        [CreateNew]
        public UserTaskListPresenterCreator Presenter
        {
            set
            {
                var presenterCreator = value;

                WorkItem = presenterCreator.WorkItem;
                presenterCreator.View = this;
                presenterCreator.Create();
            }
        }

        # endregion

        # region Обработка событий

        private void RaiseRefresh()
        {
            if (OnRefresh != null) OnRefresh();
        }

        private void UserTaskListView_Load(object sender, EventArgs e)
        {
            if (Loaded != null) Loaded(sender, e);
        }

        private void cmiOpenDeclCard_Click(object sender, EventArgs e)
        {
            if (OpenDeclCardClicked == null) return;
            OpenDeclCardClicked(sender, 
                new GridItemSelectEventArgs(gridUserTask.GetCurrentItem<UserTaskListModel>()));
        }

        private void cmiOpenTaxPayerCard_Click(object sender, EventArgs e)
        {
            if (OpenTaxPayerCardClicked == null) return;
            OpenTaxPayerCardClicked(sender, 
                new GridItemSelectEventArgs(gridUserTask.GetCurrentItem<UserTaskListModel>()));
        }

        private void cmiOpenUserTask_Click(object sender, EventArgs e)
        {
            if (OpenUserTaskClicked == null) return;
            OpenUserTaskClicked(sender, 
                new GridItemSelectEventArgs(gridUserTask.GetCurrentItem<UserTaskListModel>()));
        }
        private void mainContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var currentItem = gridUserTask.GetCurrentItem<UserTaskListModel>();

            if (currentItem != null)
            {
                cmiOpenDeclCard.Visible = true;
                cmiOpenDeclCard.Text = 
                    currentItem.GetDocumentTypeCode() == (int) DeclarationTypeCode.Declaration ? 
                    "Открыть декларацию" : 
                    "Открыть журнал";
                cmiOpenTaxPayerCard.Visible = true;
                cmiOpenUserTask.Visible = true;
            }
            else
            {
                cmiOpenDeclCard.Visible = false;
                cmiOpenTaxPayerCard.Visible = false;
                cmiOpenUserTask.Visible = false;
            }

        }

        # endregion

        # region Создание ribbon-меню

        private IRibbonAdapter _ribbon;

        private const string RibbonName = "NDS2UserTaskListViewRibbon";
        private const string RibbonTitle = "Пользовательские задания";
        private const string RibbonToolTip = "Пользовательские задания";

        private const string GroupName = "NDS2UserTaskListViewManage";
        private const string GroupText = "Функции";

        private const string ReportButtonCaption = "Отчет";
        private const string ReportButtonToolTip = "Сформировать отчет о выполнении ПЗ";
        private const string ReportCommandName = "NDS2UserTaskListReport";

        private const string RefreshButtonCaption = "Обновить";
        private const string RefreshButtonToolTip = "Обновить данные на форме";
        private const string RefreshCommandName = "NDS2UserTaskListRefresh";

        private const string StatusOverdueName = "Просрочено";
        private const int StatusOverdueId = 4;

        /// <summary>
        /// Создание ribbon-меню
        /// </summary>
        public void InitOperations(bool allowGenerateReport)
        {
            _ribbon =
                new RibbonCreator(_presentationContext, WorkItem)
                    .WithRibbon(
                        RibbonName,
                        RibbonTitle,
                        RibbonToolTip);

            var mainGroup = _ribbon.WithGroup(GroupName, GroupText);

            mainGroup.WithButton(UcRibbonToolSize.Large, RefreshCommandName)
                        .WithText(RefreshButtonCaption)
                        .WithToolTip(RefreshButtonToolTip)
                        .WithResourse(ResourceManagerNDS2.NDS2ClientResources)
                        .WithImage("report_create", "report_create");

            if (allowGenerateReport)
            {
                mainGroup.WithButton(UcRibbonToolSize.Large, ReportCommandName)
                            .WithText(ReportButtonCaption)
                            .WithToolTip(ReportButtonToolTip)
                            .WithResourse(ResourceManagerNDS2.NDS2ClientResources)
                            .WithImage("excel_create", "excel_create");
            }

            _ribbon.Init();
            _ribbon.ToggleVisibility(new[] { RefreshCommandName });
        }

        public event EventHandler OnReportGenerating;

        [CommandHandler(ReportCommandName)]
        public virtual void OnUserTaskListReport(object sender, EventArgs e)
        {
            if (OnReportGenerating != null)
            {
                OnReportGenerating(this, new EventArgs());
            }
        }

        [CommandHandler(RefreshCommandName)]
        public virtual void OnRaiseRefresh(object sender, EventArgs e)
        {
            RaiseRefresh();
        }

        # endregion

        # region IUserTaskListView

        public event EventHandler Loaded;
        public event ParameterlessEventHandler OnRefresh;

        public event EventHandler<GridItemSelectEventArgs> UserTaskSelected;
        public event EventHandler<GridItemSelectEventArgs> OpenDeclCardClicked;
        public event EventHandler<GridItemSelectEventArgs> OpenTaxPayerCardClicked;
        public event EventHandler<GridItemSelectEventArgs> OpenUserTaskClicked;

        public IDataGridView UserTasks{get { return gridUserTask; }}
        public List<Period> PeriodRange { get; set; }
        public static Dictionary<int, string> ProcessStateDescriptionList { get; set; }
        public static Dictionary<int, string> ProcessStateDescriptionListForFilter { get; set; }
        public static Dictionary<int, string> StatusDescriptionList { get; set; }
        public static Dictionary<int, string> TypeDescriptionList { get; set; }
        public Dictionary<int, string> ProcessStateDescriptions
        {
            set { ProcessStateDescriptionList = value; }
        }
        public Dictionary<int, string> ProcessStateDescriptionsForFilter
        {
            set { ProcessStateDescriptionListForFilter = value; }
        }

        public Dictionary<int, string> StatusDescriptions
        {
            set { StatusDescriptionList = value; }
        }

        public Dictionary<int, string> TypeDescriptions
        {
            set { TypeDescriptionList = value; }
        }
        public Dictionary<int, Bitmap> StatusIconList { get; set; }


        public void InitUserTasksGridView(
			    IPager pager
			  , bool showAdminCols
			  , bool showCentralCols
			  , bool showManagementCols)
        {
            gridUserTask
                .WithPager(new ExtendedDataGridPageNavigator(pager))
                .InitColumns(SetupGrid_UserTask(showAdminCols, showCentralCols, showManagementCols));

            gridUserTask.RowDoubleClicked +=
                dataItem =>
                {
                    if (UserTaskSelected != null)
                    {
                        UserTaskSelected(this, new GridItemSelectEventArgs(dataItem));
                    }
                };



            // Статусбар
            gridUserTask.PanelLoadingVisible = true;
            //перенос названий в колонках
            //gridUserTask.Grid.DisplayLayout.Override.WrapHeaderText = DefaultableBoolean.True;
        }

        private GridColumnSetup SetupGrid_UserTask(bool showAdminCols, bool showCentralCols, bool showManagementCols)
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<UserTaskListModel>(gridUserTask);


            setup
                .WithColumn(helper.CreateImageColumn(
                        o => o.CurrentProcessState,
                        this,
                        StatusIconList,
                        4,
                        GetProcessStateOptionsBuilder()
                    ).Configure(c =>
                    {
                        c.ToolTipViewer = new NotificationToolTipViewer();
                    }))
                .WithColumn(helper.CreateTextColumn(o => o.Status, GetStatusOptionsBuilder(), 2, 70))
                .WithColumn(helper.CreateTextColumn(o => o.TaskType, GetTypeOptionsBuilder(), 2, 160))
                .WithColumn(helper.CreateTextColumn(o => o.CreateDate, 2, 80))
                .WithGroup(helper.CreateGroup("Дата выполнения")
                    .WithColumn(helper.CreateTextColumn(o => o.PlannedCompleteDate, 1, 80))
                    .WithColumn(helper.CreateTextColumn(o => o.ActualCompleteDate, 1, 80))
                )
                .WithColumn(helper.CreateTextColumn(o => o.DaysAfterExpire, 2, 100))
                .WithGroup(helper.CreateGroup("Налогоплательщик")
                    .WithColumn(helper.CreateTextColumn(o => o.TaxPayerInn, 1, 90))
                    .WithColumn(helper.CreateTextColumn(o => o.TaxPayerKpp, 1, 90))
                    .WithColumn(helper.CreateTextColumn(o => o.TaxPayerName, 1, 140))
                )
                .WithColumn(helper.CreateTextColumn(o => o.DocumentNumber, 2, 80))
                .WithColumn(helper.CreateTextColumn(o => o.DocumentCorrectionNumber, 2, 80))
                .WithColumn(helper.CreateTextColumn(o => o.DocumentPeriodName, GetPeriodOptionsBuilder(PeriodRange), 2, 80));

            //Инспектор - Видим только для пользователя с ролью «Администратор».
            //Если задача не выполнена (не закрыта) и  Инспектор, который ее выполняет,  был удален из ЦСУД, то необходимо:
            //- подсвечивать ячейку розовым фоном 
            //- выводить подсказку, при наведении курсора на ячейку - «Пользователь удален из ЦСУД»
            if (showAdminCols)
                setup.WithColumn(helper.CreateTextColumn(o => o.AssignedName, 2));

            //Инспекция - Видим только для пользователя ЦА и УФНС
            if (showCentralCols || showManagementCols)
                setup.WithGroup(helper.CreateGroup("Инспекция")
                    .WithColumn(helper.CreateTextColumn(o => o.SonoCode, 1, 60))
                    .WithColumn(helper.CreateTextColumn(o => o.SonoName, 1, 110)));

            //Регион - Видим только для пользователя ЦА
            if (showCentralCols)
                setup.WithGroup(helper.CreateGroup("Регион")
                    .WithColumn(helper.CreateTextColumn(o => o.RegionCode, 1, 60))
                    .WithColumn(helper.CreateTextColumn(o => o.RegionName, 1, 110)));

            return setup;
        }

        /// <summary>
        /// строки фильтра для колонки "Напоминание"
        /// </summary>
        /// <returns></returns>
        private static BooleanAutoCompleteOptionsBuilder GetProcessStateOptionsBuilder()
        {
            var processStateCodes = ProcessStateDescriptionListForFilter.
                Select(stateDescription =>
                    new BooleanCode
                    {
                        Index = stateDescription.Key,
                        Code = stateDescription.Key,
                        Description = stateDescription.Value
                    }).ToList();
            var processStateDictionary = new DictionaryBoolean(processStateCodes);
            return new BooleanAutoCompleteOptionsBuilder(processStateDictionary);
        }
        /// <summary>
        /// строки фильтра для колонки "Статус"
        /// </summary>
        /// <returns></returns>
        private static BooleanAutoCompleteOptionsBuilder GetStatusOptionsBuilder()
        {
            StatusDescriptionList.Add(StatusOverdueId, StatusOverdueName);

            var statusDict = new DictionaryBoolean(StatusDescriptionList);
            return new BooleanAutoCompleteOptionsBuilder(statusDict);
        }

        private static BooleanAutoCompleteOptionsBuilder GetTypeOptionsBuilder()
        {
            var typeCodes = TypeDescriptionList.
                Select(x =>
                    new BooleanCode
                    {
                        Index = x.Key,
                        Code = x.Key,
                        Description = x.Value
                    }).ToList();
            var typeDico = new DictionaryBoolean(typeCodes);
            return new BooleanAutoCompleteOptionsBuilder(typeDico);
        }
        /// <summary>
        /// строки фильтра для колонки "Отчетный период"
        /// </summary>
        /// <returns></returns>
        private BooleanAutoCompleteOptionsBuilder GetPeriodOptionsBuilder(List<Period> PeriodRange)
        {
            int rowNo = 1;
            var processStateCodes = PeriodRange.
                Select(period =>
                    new BooleanCode
                    {
                        Index = rowNo++,
                        Code = period.Year * 10 + period.QuarterNumber,
                        Description = period.ToString()
                    }).ToList();
            var processStateDictionary = new DictionaryBoolean(processStateCodes);
            return new BooleanAutoCompleteOptionsBuilder(processStateDictionary);
        }

        # endregion

        class NotificationToolTipViewer : ToolTipViewerBase
        {
            public override string GetToolTip(object listObject)
            {
                var userTaskListModel = listObject as UserTaskListModel;
                if (userTaskListModel == null) return null;
                var currentProcessState = userTaskListModel.CurrentProcessState;
                return ProcessStateDescriptionList.
                    Single(stateDescription => stateDescription.Key == currentProcessState).Value;
            }
        }
    }
}
