﻿using System;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    partial class UserTaskListView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.grpCommonInfoPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.gridSummary = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this.gridUserTask = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.mainContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenDeclCard = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenTaxPayerCard = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenUserTask = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grpCommonInfo)).BeginInit();
            this.grpCommonInfo.SuspendLayout();
            this.grpCommonInfoPanel.SuspendLayout();
            this.mainContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCommonInfo
            // 
            this.grpCommonInfo.Controls.Add(this.grpCommonInfoPanel);
            this.grpCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpCommonInfo.ExpandedSize = new System.Drawing.Size(951, 126);
            this.grpCommonInfo.Location = new System.Drawing.Point(0, 0);
            this.grpCommonInfo.Name = "grpCommonInfo";
            this.grpCommonInfo.Size = new System.Drawing.Size(951, 126);
            this.grpCommonInfo.TabIndex = 1;
            this.grpCommonInfo.Text = "Количество необработанных пользовательских  заданий";
            // 
            // grpCommonInfoPanel
            // 
            this.grpCommonInfoPanel.AutoSize = true;
            this.grpCommonInfoPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpCommonInfoPanel.Controls.Add(this.gridSummary);
            this.grpCommonInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCommonInfoPanel.Location = new System.Drawing.Point(3, 19);
            this.grpCommonInfoPanel.Name = "grpCommonInfoPanel";
            this.grpCommonInfoPanel.Size = new System.Drawing.Size(945, 104);
            this.grpCommonInfoPanel.TabIndex = 0;
            // 
            // gridSummary
            // 
            this.gridSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSummary.GridContextMenuStrip = null;
            this.gridSummary.Location = new System.Drawing.Point(14, 16);
            this.gridSummary.Margin = new System.Windows.Forms.Padding(0);
            this.gridSummary.Name = "gridSummary";
            this.gridSummary.Size = new System.Drawing.Size(919, 77);
            this.gridSummary.TabIndex = 0;
            // 
            // gridUserTask
            // 
            this.gridUserTask.AggregatePanelVisible = true;
            this.gridUserTask.AllowFilterReset = false;
            this.gridUserTask.AllowMultiGrouping = true;
            this.gridUserTask.AllowResetSettings = false;
            this.gridUserTask.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridUserTask.BackColor = System.Drawing.Color.Transparent;
            this.gridUserTask.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridUserTask.ColumnVisibilitySetupButtonVisible = true;
            this.gridUserTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUserTask.ExportExcelCancelHint = "";
            this.gridUserTask.ExportExcelCancelVisible = false;
            this.gridUserTask.ExportExcelHint = "";
            this.gridUserTask.ExportExcelVisible = false;
            this.gridUserTask.FilterResetVisible = false;
            this.gridUserTask.FooterVisible = true;
            this.gridUserTask.GridContextMenuStrip = this.mainContextMenu;
            this.gridUserTask.Location = new System.Drawing.Point(0, 126);
            this.gridUserTask.Name = "gridUserTask";
            this.gridUserTask.PanelExportExcelStateVisible = false;
            this.gridUserTask.PanelLoadingVisible = false;
            this.gridUserTask.PanelPagesVisible = true;
            this.gridUserTask.RowDoubleClicked = null;
            this.gridUserTask.Size = new System.Drawing.Size(951, 348);
            this.gridUserTask.TabIndex = 7;
            this.gridUserTask.Title = "Список пользовательских заданий";
            // 
            // mainContextMenu
            // 
            this.mainContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenDeclCard,
            this.cmiOpenTaxPayerCard,
            this.cmiOpenUserTask});
            this.mainContextMenu.Name = "mainContextMenu";
            this.mainContextMenu.Size = new System.Drawing.Size(291, 70);
            this.mainContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.mainContextMenu_Opening);
            // 
            // cmiOpenDeclCard
            // 
            this.cmiOpenDeclCard.Name = "cmiOpenDeclCard";
            this.cmiOpenDeclCard.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenDeclCard.Text = "Открыть декларацию/журнал";
            this.cmiOpenDeclCard.Click += new System.EventHandler(this.cmiOpenDeclCard_Click);
            // 
            // cmiOpenTaxPayerCard
            // 
            this.cmiOpenTaxPayerCard.Name = "cmiOpenTaxPayerCard";
            this.cmiOpenTaxPayerCard.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenTaxPayerCard.Text = "Открыть карточку налогоплательщика";
            this.cmiOpenTaxPayerCard.Click += new System.EventHandler(this.cmiOpenTaxPayerCard_Click);
            // 
            // cmiOpenUserTask
            // 
            this.cmiOpenUserTask.Name = "cmiOpenUserTask";
            this.cmiOpenUserTask.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenUserTask.Text = "Открыть карточку задания";
            this.cmiOpenUserTask.Click += new System.EventHandler(this.cmiOpenUserTask_Click);
            // 
            // UserTaskListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.gridUserTask);
            this.Controls.Add(this.grpCommonInfo);
            this.Name = "UserTaskListView";
            this.Size = new System.Drawing.Size(951, 474);
            this.Load += new System.EventHandler(this.UserTaskListView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpCommonInfo)).EndInit();
            this.grpCommonInfo.ResumeLayout(false);
            this.grpCommonInfo.PerformLayout();
            this.grpCommonInfoPanel.ResumeLayout(false);
            this.mainContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox grpCommonInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel grpCommonInfoPanel;
        internal Controls.Grid.V2.GridView gridSummary;
        internal Controls.Grid.V2.DataGridView gridUserTask;
        private System.Windows.Forms.ContextMenuStrip mainContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclCard;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTaxPayerCard;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenUserTask;
    }
}
