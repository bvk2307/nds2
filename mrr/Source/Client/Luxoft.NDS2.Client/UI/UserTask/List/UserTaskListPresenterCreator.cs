﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.UserTask.Details;
using Luxoft.NDS2.Client.UI.UserTask.Report;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.UserTask.List
{
    public class UserTaskListPresenterCreator : BasePresenter<IUserTaskListView>
    {
        public UserTaskListPresenter Create()
        {
            var cardOpener = new CardOpener(this.PresentationContext, this.WorkItem, null);

            return new UserTaskListPresenter(
                View,
                new CommonSettingsProviderCreator(WorkItem),
                WorkItem.Services,
                GetTaxPayerDetailsViewer(),
                GetDeclarationCardOpener(),
                Sur,
                LoadDictonaryTaxPeriods(),
                cardOpener,
                new ReportDialogOpener(WorkItem));
        }
    }
}