﻿using System;
using System.Globalization;
using Luxoft.NDS2.Client.Helpers;

namespace Luxoft.NDS2.Client.UI.UserTask
{
    public static class FormatHelper
    {
        public const string DateViewFormat = "dd.MM.yyyy";

        public static string If(this string unput, Func<string, string> notNullFunction, string nullValue = "")
        {
            if (string.IsNullOrEmpty(unput)) return nullValue;
            return notNullFunction(unput);
        }

        public static string NullFormat(
            this int? unput,
            Func<int, string> notNullFunction = null, 
            string nullValue = "")
        {
            if (!unput.HasValue) return nullValue;
            if (notNullFunction == null) return unput.Value.ToString();
            return notNullFunction(unput.Value);
        }

        public static string NullFormat(
            this DateTime? unput,
            Func<DateTime, string> notNullFunction = null, 
            string nullValue = "")
        {
            if (!unput.HasValue) return nullValue;
            if (notNullFunction == null) return unput.Value.ToString(DateViewFormat);
            return notNullFunction(unput.Value);
        }

	    public static string SumFormat(
		    this decimal? input,
		    Func<decimal, string> notNullFunction = null,
		    string nullValue = "")
	    {
		    if (!input.HasValue) return nullValue;
		    if (notNullFunction == null) return string.Format("{0:+#,0.00;-#,0.00;0.00}", input);
			 return notNullFunction(input.Value);
	    }
    }
}