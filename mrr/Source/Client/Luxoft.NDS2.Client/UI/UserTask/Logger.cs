﻿using System;
using Luxoft.NDS2.Client.Infrastructure;

namespace Luxoft.NDS2.Client.UI.UserTask
{
    public class Logger : ILogger
    {
        private ClientLogger _clientLogger;

        public Logger(ClientLogger clientLogger)
        {
            _clientLogger = clientLogger;
        }

        public void LogError(string message, string methodName = null, Exception ex = null)
        {
            _clientLogger.LogError(message, ex, methodName);
        }

        public void LogWarning(string message, string methodName = null)
        {
            _clientLogger.LogWarning(message, methodName);
        }

        public void LogNotification(string message, string methodName = null)
        {
            _clientLogger.LogInfo(message, methodName);
        }
    }
}