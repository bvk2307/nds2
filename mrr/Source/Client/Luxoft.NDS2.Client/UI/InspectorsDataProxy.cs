﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI
{
    public sealed class InspectorsDataProxy : ServiceProxyBase<List<UserInformation>>, IInspectorsDataProxy
    {
        private readonly IUserInformationService _service;

        public InspectorsDataProxy(IUserInformationService service, INotifier notifier, IClientLogger clientLogger)
            : base(notifier, clientLogger)
        {
            _service = service;
        }

        public IEnumerable<UserInformation> GetUsers(string sonoCode)
        {
            List<UserInformation> result;

            if (Invoke(() => _service.GetInspectorListBySono(sonoCode), out result))
            {
                return result;
            }

            return new List<UserInformation>();
        }


        protected override List<UserInformation> FailureResult()
        {
            return new List<UserInformation>();
        }

        protected override void CallBack(List<UserInformation> result)
        {
            throw new NotImplementedException();
        }
    }
}
