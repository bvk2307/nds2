using System;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI
{
    [Obsolete]
    public interface IServiceCallWrapper
    {
        /// <summary>
        /// ������� ���������� ������. ������������ ���������� ������ ���������.
        /// </summary>
        /// <typeparam name="TResult">��� �������� ������������ ����� ����������</typeparam>
        /// <param name="callDelegate">������� ���������� ������</param>
        /// <param name="cancelToken">����� ��� ���������� ��������. �����������, �� ��������� �������� �� �����������</param>
        /// <returns>��������� ���������� ��������</returns>
        TResult ExecuteServiceCall<TResult>( 
            Func<CancellationToken, TResult> callDelegate, CancellationToken cancelToken = default(CancellationToken) ) 
            where TResult : IOperationResult, new();

        /// <summary>
        /// ������� ���������� ����������� ������. ������������ ���������� ������ ���������.
        /// </summary>
        /// <typeparam name="TResult">��� �������� ������������ ����� ����������</typeparam>
        /// <param name="callDelegate">������� ���������� ������</param>
        /// <param name="doOnSuccess">�������, ����������� � ������ ��������� ������</param>
        /// <param name="doOnError">������� ����������� � ������ ��������� ������ �������</param>
        /// <returns>������� ���������� ����������</returns>
        bool ExecuteServiceCall<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess = null,
            Action<TResult> doOnError = null ) where TResult : IOperationResult, new();
    }
}