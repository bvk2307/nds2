﻿using Infragistics.Excel;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoring.Models.Common.TnoMonitorData;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.Security;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoring.Reports
{
	public class CommonReport
	{
		private string _formatString = "#,###";
		private string _formatString2 = "#,##0.00";

		private string _tabNameRF = "Отчет ФНС";
		private string _tabNameUFNS = "Отчет УФНС";
		private string _headerText = "Форма №2 - МЭ";
		private string _dateTemplate = "по состоянию на {0} г.";
		private string _measureText = "руб.";
		private int[] _measurePosition = new int[] { 3, 4 };
		private int[] _headerPosition = new int[] { 0, 4 };
		private int[] _datePosition = new int[] { 2, 0 };
		private int[] _reportNamePosition = new int[] { 1, 0 };
		private int[] _columnsWidth = new int[] { 3000, 15000, 8000, 8000, 5000 };
		private int[] _dataRFStartPosition = new int[] { 6, 0 };
		private int[] _dataParentStartPosition = new int[] { 7, 0 };
		private int[] _dataStartPosition = new int[] { 8, 0 };
		private int[] _dataHeaderStartPosition = new int[] { 4, 0 };


		private string[] _dataColumnHeader = new string[] {
			"Код налогового органа",
			"Наименование налогового органа",
			"Сумма не устраненных расхождений",
			"Общая сумма НДС, подлежащая вычету",
			"Доля расхождений в сумме вычетов, (%)"
		};
		private string[] _dataColumnLetter = new string[] {"A", "B", "1", "2", "3"};

		private string _reportName = "ОТЧЕТ\nО результатах эффективности деятельности территориальных налоговых органов\n\"Удельный вес суммы расхождений по декларациям по НДС в общей сумме налоговых вычетов по НДС\"";
		Workbook _wb;
		Worksheet _ws;

		public CommonReport(string fileName, DateTime reportDate, List<TnoMonitorDataInfoModel> data, TnoMonitorDataInfoModel dataParent, TnoMonitorDataInfoModel dataRF = null)
		{

			_wb = new Workbook();
			_ws = _wb.Worksheets.Add(
				(dataRF == null) ? _tabNameRF : _tabNameUFNS
				);
			SetCellValue(_headerPosition, _headerText, HorizontalCellAlignment.Right);
			SetCellValue(_datePosition, String.Format(_dateTemplate, DateTime.Now.ToString("dd.MM.yyyy")), HorizontalCellAlignment.Center);
			SetCellValue(_measurePosition, _measureText, HorizontalCellAlignment.Right);
			SetCellValue(_reportNamePosition, _reportName, HorizontalCellAlignment.Center, true);
			MergeCells();
			SetRowHeight();
			SetDataColumnHeader();
			SetColumnWidth();

			if (dataRF == null)
			{
				_dataParentStartPosition[0]--;
				_dataStartPosition[0]--;
			}
			else
			{
				SetDataItem(_dataRFStartPosition[0], dataRF);
			}
			if (dataParent != null)
			{
				SetDataItem(_dataParentStartPosition[0], dataParent);
			}
			else
			{
				_dataStartPosition[0]--;
			}
			SetData(data, _dataStartPosition);

			_wb.Save(fileName);
		}

		private void SetRowHeight()
		{
			_ws.Rows[1].Height = 1000;
			_ws.Rows[4].Height = 1000;
		}

		private void SetDataColumnHeader()
		{
			int indexRow = _dataHeaderStartPosition[0];
			int indexColumn = 0;
			foreach (string header in _dataColumnHeader)
			{
				SetCellValue(new int[] { indexRow, indexColumn }, header, HorizontalCellAlignment.Center);
				indexColumn++;
			}
			indexColumn = 0;
			foreach (string letter in _dataColumnLetter)
			{
				SetCellValue(new int[] { indexRow + 1, indexColumn }, letter, HorizontalCellAlignment.Center);
				indexColumn++;
			}
		}
		private void SetData(List<TnoMonitorDataInfoModel> data, int[] position)
		{
			List<TnoMonitorDataInfoModel> sorted = data.Where(d => d.RatingType == "1" || d.RatingType == null).OrderBy(d => d.TnoCode).ToList();

			int indexRow = position[0];
			foreach (TnoMonitorDataInfoModel item in sorted)
			{
				SetDataItem(indexRow, item);
				indexRow++;
			}
		}
		private void SetDataItem(int indexRow, TnoMonitorDataInfoModel item)
		{
			string tnoCode = item.TnoCode;
			if (tnoCode == "0")
			{
				tnoCode = "0000";
			}
			else if (tnoCode.Length == 2)
			{
				tnoCode = tnoCode + "00";
			}

			SetCellValue(new int[] { indexRow, 0 }, tnoCode, HorizontalCellAlignment.Center);
			SetCellValue(new int[] { indexRow, 1 }, item.TnoName);
			SetCellValue(new int[] { indexRow, 2 }, item.DiscrepancyAllAmount, HorizontalCellAlignment.Right);
			SetCellValue(new int[] { indexRow, 3 }, item.DeclarationDeductionAmount, HorizontalCellAlignment.Right);
			SetCellValue(new int[] { indexRow, 4 }, item.EffIndex24, HorizontalCellAlignment.Right);

			_ws.Rows[indexRow].Cells[2].CellFormat.FormatString = _formatString;
			_ws.Rows[indexRow].Cells[3].CellFormat.FormatString = _formatString;
			_ws.Rows[indexRow].Cells[4].CellFormat.FormatString = _formatString2;
		}

		private void SetColumnWidth()
		{
			int index = 0;
			foreach (int w in _columnsWidth)
			{
				_ws.Columns[index].Width = w;
				index++;
			}
		}

		private void MergeCells()
		{
			_ws.MergedCellsRegions.Add(1, 0, 1, 4);
			_ws.MergedCellsRegions.Add(2, 0, 2, 4);
		}
		private void SetCellValue(int[] position, object value, HorizontalCellAlignment alignment = HorizontalCellAlignment.Left, bool bold = false ) {
			if (position[0] >= _dataHeaderStartPosition[0])
			{
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.BottomBorderColor = System.Drawing.Color.Black;
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.LeftBorderColor = System.Drawing.Color.Black;
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.RightBorderColor = System.Drawing.Color.Black;
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.TopBorderColor = System.Drawing.Color.Black;
			}
			if (String.IsNullOrEmpty(value.ToString()) || value.ToString() == "0")
			{
				value = "-";
			}
			_ws.Rows[position[0]].Cells[position[1]].Value = value;
			if (bold)
			{
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
			}
			if (alignment != HorizontalCellAlignment.Left)
			{
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.Alignment = alignment;
			}
			if (alignment == HorizontalCellAlignment.Center)
			{
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
				_ws.Rows[position[0]].Cells[position[1]].CellFormat.WrapText = ExcelDefaultableBoolean.True;
			}
		}
	}
}
