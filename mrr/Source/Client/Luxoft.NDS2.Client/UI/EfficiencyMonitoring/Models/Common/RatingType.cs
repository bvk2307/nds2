﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoring.Models.Common
{
    [DataContract]
    public class RatingType
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        public static RatingType Create(int id, string name)
        {
			RatingType rating = new RatingType();
            rating.Id = id;
            rating.Name = name;
            return rating;
        }

		public static List<RatingType> GetAllRatingTypes()
        {
			List<RatingType> ratings = new List<RatingType>
            {
                Create(1, "Рейтинг ИФНС по УФНС"),
                Create(2, "Рейтинг УФНС по РФ"),
                Create(3, "Рейтинг УФНС по ФО")
            };
            return ratings;
        }
    }
}