﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public class RequestStateAsync
    {
        public QueryConditions QueryConditionsSend { get; set; }
        public long TotalRowsNumber { get; set; }
        public object ResultData { get; set; }
        public StateAsync State { get; set; }
        public CancellationTokenSource OperationCTS { get; set; }
        public long TaskId { get; set; }

        public RequestStateAsync()
        {
            QueryConditionsSend = null;
            TotalRowsNumber = 0;
            ResultData = null;
            State = StateAsync.None;
            TaskId = -1;
        }

        public void Reset<T>() where T : new()
        {
            ResultData = new T();
            TotalRowsNumber = 0;
        }

        public static void SetRuqestStateAsyncInvoceEmpty<T>(RequestStateAsync state)
        {
            state.ResultData = new List<T>();
            state.TotalRowsNumber = 0;
        }
    }

    public enum StateAsync
    {
        None,
        Start,
        Send,
        Complete
    }
}
