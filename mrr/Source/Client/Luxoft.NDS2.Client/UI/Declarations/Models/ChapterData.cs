﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Declarations.Models
{
    public class ChapterData
    {
        public GridControl GridControl 
        { 
            get; 
            set; 
        }

        public int Priority 
        { 
            get; 
            set; 
        }

        public long RecordCount 
        { 
            get; 
            set; 
        }

        public long? RequestId
        {
            get;
            set;
        }

        public InvoiceSovOperation InvoiceSovOperation 
        { 
            get; 
            set; 
        }
    }
}
