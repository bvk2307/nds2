﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.Models
{
    public class DeclarationParameters
    {
        public int ChapterGridNumber {get; set; }
        public Invoice Invoice {get; set; }
        public DateTime? DateSF {get; set; }
        public string InnContrAgent { get; set; }
    }
}
