﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.Models
{
    public class ChapterDestinationParameters
    {
        public int SelectedTabIndex { get; set; }
        public int ChapterGridNumber { get; set; }
        public string ColumnNameINN { get; set; }

        public ChapterDestinationParameters()
        {
            SelectedTabIndex = 0;
            ChapterGridNumber = -1;
            ColumnNameINN = String.Empty;
        }
    }
}
