﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.Models
{
    public class DeclarationInfoSource
    {
        public DeclarationSummary declarationSource { get; set; }
        public int SelectedTabIndex { get; set; }
        public int ChapterGridNumber { get; set; }
        public bool IsDestination { get; set; }
        public int ChapterGridNumberDestination { get; set; }
        public string InvoiceRowKey { get; set; }
        public string InvoiceNum { get; set; }

        public DeclarationInfoSource()
        {
            IsDestination = false;
            ChapterGridNumberDestination = -1;
            InvoiceRowKey = String.Empty;
            InvoiceNum = String.Empty;
        }
    }
}
