﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.Helpers;
using System.Drawing;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinCalcManager;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {
        #region Пока используются старые колонки для выгрузки в эксель

        public List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> GetColumnsChapter12()
        {
            GridSetupHelper<Invoice> helper = new GridSetupHelper<Invoice>();

            ColumnDefinition c00 = helper.CreateColumnDefinition(o => o.IS_CHANGED, GetDrawPenAction());
            c00.RowSpan = 2;
            c00.AllowRowFiltering = false;
            c00.WidthColumnExcel = 5;

            var c00a = helper.CreateColumnDefinition(o => o.CONTRACTOR_DECL_IN_MC, d => { d.Caption = "К"; d.ToolTip = "Котрагент подал декларацию / журнал учета"; d.RowSpan = 2; d.WidthColumnExcel = 8; });
            var c00b = helper.CreateColumnDefinition(o => o.IS_MATCHING_ROW, d => { d.Caption = "С"; d.ToolTip = "Запись сопоставлена"; d.RowSpan = 2; d.WidthColumnExcel = 8; });

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Caption = "Номер\n\r(стр. 020)"; d.ToolTip = "Номер счета-фактуры продавца"; d.Width = WidthInvoice; }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_INVOICE_DATE, d => { d.Caption = "Дата\n\r(стр. 030)"; d.ToolTip = "Дата счета-фактуры продавца"; d.Width = WidthDate; }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Caption = "ИНН\n\r(стр. 040)"; d.ToolTip = "ИНН покупателя"; d.Width = WidthInn; }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Caption = "КПП\n\r(стр. 040)"; d.ToolTip = "КПП покупателя"; d.Width = WidthKpp; }));

            var c31 = helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.Caption = "Код валюты\n\r(стр. 050)"; d.ToolTip = "Код валюты по ОКВ"; d.RowSpan = 2; });
            var c32 = helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d =>
            {
                d.Caption = "Стоимость без НДС\n\r(стр. 060)"; 
                d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав без налога - всего"; 
                d.RowSpan = 2;
                d.FormatInfo = formatPrices; 
                d.Align = ColumnAlign.Right;
            });
            var c33 = helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d =>
            {
                d.Caption = "Сумма НДС\n\r(стр. 070)"; 
                d.ToolTip = "Сумма налога, предъявляемая покупателю"; 
                d.RowSpan = 2;
                d.FormatInfo = formatPrices;
                d.Align = ColumnAlign.Right;
            });
            var c34 = helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d =>
            {
                d.Caption = "Стоимость с НДС\n\r(стр. 080)"; 
                d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав с налогом - всего"; 
                d.RowSpan = 2;
                d.FormatInfo = formatPrices;
                d.Align = ColumnAlign.Right;
            });

            return new List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> { c00, c00a, c00b, g1, g2, c31, c32, c33, c34 };
        }

        # endregion

        private void RefreshGridChapter12()
        {
            cmiR12OpenDecl.Enabled = _model.IsOpenDeclAccessible();

            //TODO: Необходимо найти альтернативный способ скрывать/показывать отдельные колонки в уже проинициализированном гриде. Так же необходимо учесть сохранение пользовательских настроек грида.
            //dataGridR12.ClearColumns();
            //dataGridR12.InitColumns(SetupGridChapter12());
            //_presenter.InitializeGridChapter12(dataGridR12, _pagerChapter12);
        }

        private GridColumnSetup SetupGridChapter12()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InvoiceModel>(dataGridR12);
            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            var priznakIcons = new Dictionary<int?, Bitmap>
            {
                { INVOICE_IS_PROCESSED, Properties.Resources.invoice_confirmed_16 },
                { INVOICE_IS_CHANGED, Properties.Resources.invoice_modified_16 },
            };

            setup.Columns.Add(helper.CreateImageColumn(o => o.IS_CHANGED, this, priznakIcons, 4)
                .Configure(d =>
                {
                    d.Caption = String.Empty;
                    d.HeaderToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
                    d.DisableFilter = true; 
                    d.ToolTipViewer = _explainToolTipViewer;
                }));

            #region Флаг К и С

            if (_model.IsChaptersGridIsMatchingColumnVisible())
            {
                var dictionaryBooleanIsMatching = new DictionaryBoolean();
                var optionsBuilderIsMatching = new BooleanAutoCompleteOptionsBuilder(dictionaryBooleanIsMatching);

                setup.Columns.Add(helper.CreateBoolColumn(o => o.CONTRACTOR_DECL_IN_MC, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "К";
                    d.HeaderToolTip = "Котрагент подал декларацию / журнал учета";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.DisableEdit = true;
                    d.MinWidth = WIDTH_CONTRACTOR_DECL_IN_MC;
                    d.Width = WIDTH_CONTRACTOR_DECL_IN_MC;
                }));
                setup.Columns.Add(helper.CreateBoolColumn(o => o.IS_MATCHING_ROW, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "С";
                    d.HeaderToolTip = "Запись сопоставлена";
                    d.MinWidth = WIDTH_IS_MATCHING_ROW;
                    d.Width = WIDTH_IS_MATCHING_ROW;
                }));
            }

            #endregion

            #region СФ

            var group = helper.CreateGroup("СФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_NUM, 3)
                .Configure(d => { d.Caption = "Номер\n\r(стр. 020)"; d.HeaderToolTip = "Номер счета-фактуры продавца"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_INVOICE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 030)"; d.HeaderToolTip = "Дата счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion            

            #region Сведения о покупателе

            group = helper.CreateGroup("Сведения о покупателе");
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 040)"; d.HeaderToolTip = "ИНН покупателя"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 040)"; d.HeaderToolTip = "КПП покупателя"; d.MinWidth = WidthKpp; }));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.OKV_CODE, 4)
                .Configure(d =>
                {
                    d.Caption = "Код валюты\n\r(стр. 050)";
                    d.HeaderToolTip = "Код валюты по ОКВ";
                    d.MinWidth = 100;
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_TAX_FREE, 4)
                .Configure(d =>
                {
                    d.Caption = "Стоимость без НДС\n\r(стр. 060)";
                    d.HeaderToolTip = "Стоимость товаров (работ, услуг), имущественных прав без налога - всего";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_BUY_NDS_AMOUNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Сумма НДС\n\r(стр. 070)";
                    d.HeaderToolTip = "Сумма налога, предъявляемая покупателю";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_BUY_AMOUNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Стоимость с НДС\n\r(стр. 080)";
                    d.HeaderToolTip = "Стоимость товаров (работ, услуг), имущественных прав с налогом - всего";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            return setup;
        }

        private void InitializeGridChapter12()
        {
            dataGridR12
                 .WithPager(new ExtendedDataGridPageNavigator(_pagerChapter12))
                 .InitColumns(SetupGridChapter12());
            dataGridR12.Grid.CalcManager = new UltraCalcManager(dataGridR12.Grid.Container); ;
            dataGridR12.RegisterTableAddin(new ExpansionIndicatorRemover());
            dataGridR12.SelectRow += (sender, args) => _presenter.RefreshEKP();
            dataGridR12.AfterDataSourceChanged += dataGridR12_AfterDataSourceChanged;
            SetupSummaryRow(dataGridR12);
            _presenter.InitializeGridChapter12(dataGridR12, _pagerChapter12);
        }

        void dataGridR12_AfterDataSourceChanged()
        {
            if (dataGridR12.Grid.DisplayLayout.UIElement.GetDescendant(typeof(RowScrollbarUIElement)) != null)
            {
                dataGridR12.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Default;
            }
            else
            {
                dataGridR12.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Bottom;
            }           
        }

        private readonly PagerStateViewModel _pagerChapter12 = new PagerStateViewModel();


        private void R12ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            var currentItem = dataGridR12.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
            {
                cmiR12OpenBuyer.Visible = !string.IsNullOrEmpty(currentItem.BUYER_INN);

                cmiR12OpenDecl.Visible = true;
            }
            else
            {
                cmiR12OpenBuyer.Visible = false;
                cmiR12OpenDecl.Visible = false;
            }
        }

        private void cmiR12OpenBuyer_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR12.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppOriginal(currentItem.BUYER_INN, currentItem.BUYER_KPP, Direction.Sales);
        }

        private void cmiR12OpenDecl_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR12.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewDeclarationDetailsFromInvoice(currentItem);
        }

    }
}
