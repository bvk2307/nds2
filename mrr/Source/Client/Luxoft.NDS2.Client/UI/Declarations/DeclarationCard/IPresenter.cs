﻿using System;
using Luxoft.NDS2.Client.Model.Bargain;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public interface IPresenter
    {
        string GetTemplateRnivc();
        void InitChapter(DeclarationPart declarationPart);
        void SetRevision(long curRev);
        int GetChapterNumberNeed_R8_R11(string inn);
        int GetChapterNumberNeed_R9_R10_R12(string inn);
        object GetData(DeclarationPart declarationPart, QueryConditions qc, out long totalRowsNumber);
        object GetInvoiceData(DeclarationInvoiceChapterNumber part, QueryConditions qc, out long totalRowsNumber);

        // open cards
        void ViewPyramidReport();

        void ViewTaxPayerDetails();

        void ViewTaxPayerByKppEffective(string inn, string kpp, Direction direction);       

        void ViewDiscrepancyDetails(long discrepancyId);

        CardOpenResult ViewDeclarationDetails(long id);

        void ViewDeclarationDetailsFromInvoice(Invoice currentItem);

        void ViewDeclarationDetailsFromInvoice(InvoiceModel currentItem);

        void ViewControlRatioDetails(Common.Contracts.DTO.Business.ControlRatio.ControlRatio controlRatio);

        void ViewDiscrepancyDetails(Common.Contracts.DTO.Business.Selection.Discrepancy d);

        // base
        void RefreshEKP();

        Controls.Grid.V1.Setup.GridSetup CommonSetup(string p);

        bool IsUserEligibleForRole(params string[] roleNames);

        bool IsUserEligibleForOperation(string p);

        /*Пользовательские настройки*/
        bool GetSavedCommonInfoExpandedStateOrDefault();

        DictionarySur Sur { get; }

        DictionaryOper Oper { get; }

        DictionaryCurr Curr { get; }

        DictionaryBargain Barg { get; }

        void ExportExcelStart(DeclarationPart part, string filePath);

        void ExportExcelCancel(DeclarationPart part);

        void OpenInvoiceExcelFile(string reportExcelFilePath);

        string GenerateInvoiceExcelFileName(DeclarationPart part);

        void ExportExcelCancelAll();

        bool AllowExportToExcel();

        bool IsTreeRelationEligible();

        void LogError(string message, string methodName = null, Exception ex = null);

        void InitializeGridChapter8(IDataGridView grid, IPager pager);

        void InitializeGridChapter9(IDataGridView grid, IPager pager);

        void InitializeGridChapter10(IDataGridView grid, IPager pager);

        void InitializeGridChapter11(IDataGridView grid, IPager pager);

        void InitializeGridChapter12(IDataGridView grid, IPager pager);

        bool IsUseDataGridView(DeclarationInvoiceChapterNumber chapter);

        bool IsUseDataGridView(DeclarationPart chapter);
    }
}
