﻿using CommonComponents.Catalog;
using CommonComponents.Communication;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils;
using CommonComponents.Utils.Async;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.App.Execution;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Helpers.EventArguments;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Client.UI.Base.TransformQueryCondition;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ChapterGrid;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Discrepancy;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Popups;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Common.Helpers.IO;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;


namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public class DeclarationCardPresenter : BasePresenter<View>, IPresenter
    {
        #region делегаты

        public delegate void AfterGetChapterData(IDataGridView grid, DeclarationInvoiceChapterNumber chanperNum);
        public AfterGetChapterData GetChapterDataCallback;

        #endregion

        # region Поля

        private readonly DeclarationModel _model;

        private readonly Dictionary<int, Action<DiscrepancyDocumentInfo, ChainAccessModel>> _actions;

        private CancellationTokenSource _ctsInit;

        private readonly IDeclarationCardOpener _declarationCardOpener;

        private readonly ITaxPayerDetails _taxPayerCardOpener;

        private readonly UserAccessOpearation _userAccessOpearation;
        private readonly Dictionary<DeclarationInvoiceChapterNumber, ChapterPagedDataGridPresenter> _gridPresenterChapters =
            new Dictionary<DeclarationInvoiceChapterNumber, ChapterPagedDataGridPresenter>();

        private readonly Dictionary<DeclarationPart, CancellationTokenSource> _cancellationTokenSources =
            new Dictionary<DeclarationPart, CancellationTokenSource>
        {
            {DeclarationPart.R8, null},
            {DeclarationPart.R9, null},
            {DeclarationPart.R10, null},
            {DeclarationPart.R11, null},
            {DeclarationPart.R12, null},
            {DeclarationPart.KC, null},
            {DeclarationPart.DOCKNP, null},
            {DeclarationPart.Discrepancy, null}
        };

        private ActDiscrepancyEditPresenter _actEditPresenter;
        private DecisionDiscrepancyEditPresenter _decisionEditPresenter;
        private KnpDocumentsPresenter _knpDocumentsPresenter;
        private readonly ComparisonProcessValidator _comparasionProcessValidator;
        private readonly DiscrepancyListDataLoader _discrepancyListDataLoader;

        # endregion

        # region Конструктор

        public DeclarationCardPresenter(PresentationContext presentationContext, WorkItem wi, View view, DeclarationModel model)
            : base(presentationContext, wi, view)
        {
            _model = model;
            _invoiceService = GetServiceProxy<IInvoiceDataService>();
            _userAccessOpearation = new UserAccessOpearation(SecurityService);
            _declarationCardOpener = GetDeclarationCardOpener(_model.ChainAccess);
            _taxPayerCardOpener =
                new TaxPayerDetailsViewer(
                    WorkItem,
                    model.ChainAccess);

            var config = GetConfiguration();

            _warningThreshold = (config != null)
                ? Convert.ToInt32(config.Number_invoice_warning.Value)
                : 1000;

            _comparasionProcessValidator = new ComparisonProcessValidator(
                GetServiceProxy<IDeclarationsDataService>(),
                _requestExecutor,
                _model.ComparisonDataVersion);

            _actions =
                new Dictionary<int, Action<DiscrepancyDocumentInfo, ChainAccessModel>>
                {
                    {DocType.ClaimKS, 		ViewClaimDetails},
                    {DocType.ClaimSF, 		ViewClaimDetails},
                    {DocType.Reclaim93, 	ViewReclaimDetails},
                    {DocType.Reclaim93_1, 	ViewReclaimDetails}
                };

            View.AcceptPresenter(this);
            var operations = new UserOperationListDataLoader(GetServiceProxy<IDiscrepancyForDeclarationCardService>(), Notifier, ClientLogger)
                .LoadUserOperations();
            var securitySvc = WorkItem.Services.Get<IClientContextService>().CreateCommunicationProxy<ISecurityService>(Constants.EndpointName);
            var result = securitySvc.ValidateAccess(new[] {Constants.SystemPermissions.Operations.DiscrepancyAllView});
            if (result.Result != null)
            {
                operations.AddRange(result.Result);
                operations = operations.Distinct().ToList();
            }

            var hiveRequest = new HiveRequestDiscrepanciesList(
                _model.Inn,
                _model.KppEffective,
                _model.InnReorganized,
                PeriodEffectiveHelper.PeriodEffectiveByPeriodCode[_model.TaxPeriodCode],
                _model.Year.ToString(),
                _model.CorrectionNumber);
            _discrepancyListDataLoader = new DiscrepancyListDataLoader(
                                            GetServiceProxy<IDiscrepancyForDeclarationCardService>(), 
                                            Notifier, 
                                            ClientLogger,
                                            hiveRequest, 
                                            operations);

            _discrepancyListDataLoader.DataLoaded += OnDiscrepancyListLoaded;

            LogActivity(
                new ActivityLogEntry
                {
                    LogType = ActivityLogType.Chapter17OpenCount,
                    Count = _model.ChapterModel.GetTotalInvoiceQuantity()
                });

            //подписка на события представления
            View.OnCommonInfoExpandStateChanged += ViewOnOnCommonInfoExpandStateChanged;

            View.CustomDeclarationNumbersRequested += OnCustomDeclarationNumbers;
        }

        private void OnCustomDeclarationNumbers(InvoiceModel invoice)
        {
            if (invoice == null
                || string.IsNullOrWhiteSpace(invoice.CUSTOMS_DECLARATION_NUM)
                || string.IsNullOrWhiteSpace(invoice.ROW_KEY))
            {
                View.ViewCustomDeclarationNumbers(string.Empty);
                return;
            }

            if (invoice.CUSTOMS_DECLARATION_NUM_FULL != null)
            {
                View.ViewCustomDeclarationNumbers(NumberListToString(invoice.CUSTOMS_DECLARATION_NUM_FULL));
                return;
            }

            if (!invoice.CUSTOMS_DECLARATION_NUM.Contains("..."))
            {
                View.ViewCustomDeclarationNumbers(NumberListToString(invoice.CUSTOMS_DECLARATION_NUM.Split(',', ';')));
                return;
            }

            ExecuteServiceCallAsync(
                r => _invoiceService.SearchCustomDeclarationNumbers(invoice.ROW_KEY),
                (c, r) =>
                {
                    invoice.CUSTOMS_DECLARATION_NUM_FULL = r.Result.ToArray();
                    UIThreadExecutor.CurrentDispatcher.BeginInvoke(
                        new Action(() => View.ViewCustomDeclarationNumbers(NumberListToString(invoice.CUSTOMS_DECLARATION_NUM_FULL))));
                }
                );
        }

        private static string NumberListToString(IEnumerable<string> list)
        {
            return string.Join("\r\n", list.Select(s => s.Trim()));
        }

        // TODO Вынести или в базовый класс, или в хелпер/расширение
        private Configuration GetConfiguration()
        {
            Configuration config = null;
            if (ExecuteServiceCall(() => GetServiceProxy<IDataService>().ReadConfiguration(),
                res =>
                {
                    config = res.Result;
                }))
            {
                return config;
            }
            return new Configuration();
        }

        # endregion

        # region Навигация к другим экранным формам

        public CardOpenResult ViewDeclarationDetails(long id)
        {
            return _declarationCardOpener.Open(id);
        }

        public CardOpenResult ViewDeclarationDetails(string rowKey, Direction direction, string inn)
        {
            return _declarationCardOpener.Open(rowKey, direction, inn);
        }

        public override void ViewTaxPayerByKppEffective(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            var result = _taxPayerCardOpener.ViewByKppEffective(inn, kpp, direction);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public override void ViewTaxPayerByKppOriginal(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            var result = _taxPayerCardOpener.ViewByKppOriginal(inn, kpp, direction);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }
        #endregion

        #region Новый грид для СФ

        private GridOptions GetDefaultGridOptions(DeclarationPart part)
        {
            var settingsGrid = GridOptions.Default();
            settingsGrid.AllowManageFilterSettings = false;

            var filter = View.GetDefaultFilter(part);
            if (filter.Count > 0)
            {
                settingsGrid = new GridOptions();
                foreach (var fq in filter)
                {
                    foreach (var columnFilter in fq.Filtering)
                        settingsGrid.DefaultFilter.ApplyFilter(fq.ColumnName,
                        columnFilter.ToCondition());
                }
            }

            return settingsGrid;
        }

        public void InitializeGridChapter8(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort>(),
                Filter = View.GetDefaultFilter(DeclarationPart.R8)
            };

            var gridPresenterChapter =
                new ChapterPagedDataGridPresenter(
                    grid,
                    pager,
                    searchCriteria =>
                    {
                        var chaptersData = GetChapterDatas(grid, DeclarationInvoiceChapterNumber.Chapter8, searchCriteria);
                        if (GetChapterDataCallback != null)
                        {
                            GetChapterDataCallback(grid, DeclarationInvoiceChapterNumber.Chapter8);
                        }
                        return chaptersData;
                    },
                    conditions
                    );

            gridPresenterChapter.Init(SettingsProvider(string.Format("{0}_InitializeGridChapter8", GetType())),
                                      GetDefaultGridOptions(DeclarationPart.R8));
            if(!_gridPresenterChapters.ContainsKey(DeclarationInvoiceChapterNumber.Chapter8))
            {
                _gridPresenterChapters.Add(DeclarationInvoiceChapterNumber.Chapter8, gridPresenterChapter);
            }
        }

        public void InitializeGridChapter9(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort>(),
                Filter = View.GetDefaultFilter(DeclarationPart.R9)
            };

            var gridPresenterChapter =
                new ChapterPagedDataGridPresenter(
                    grid,
                    pager,
                    searchCriteria =>
                    {
                        var chaptersData = GetChapterDatas(grid, DeclarationInvoiceChapterNumber.Chapter9, searchCriteria);
                        if (GetChapterDataCallback != null)
                        {
                            GetChapterDataCallback(grid, DeclarationInvoiceChapterNumber.Chapter9);
                        }
                        return chaptersData;
                    },
                    conditions);



            gridPresenterChapter.Init(SettingsProvider(string.Format("{0}_InitializeGridChapter9", GetType())),
                                      GetDefaultGridOptions(DeclarationPart.R9));
            if (!_gridPresenterChapters.ContainsKey(DeclarationInvoiceChapterNumber.Chapter9))
            {
                _gridPresenterChapters.Add(DeclarationInvoiceChapterNumber.Chapter9, gridPresenterChapter);
            }
        }

        public void InitializeGridChapter10(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort>(),
                Filter = View.GetDefaultFilter(DeclarationPart.R10)
            };

            var gridPresenterChapter =
                new ChapterPagedDataGridPresenter(
                    grid,
                    pager,
                    searchCriteria =>
                    {
                        var chaptersData = GetChapterDatas(grid, DeclarationInvoiceChapterNumber.Chapter10, searchCriteria);
                        if (GetChapterDataCallback != null)
                        {
                            GetChapterDataCallback(grid, DeclarationInvoiceChapterNumber.Chapter10);
                        }
                        return chaptersData;
                    },
                    conditions);

            gridPresenterChapter.Init(SettingsProvider(string.Format("{0}_InitializeGridChapter10", GetType())),
                                      GetDefaultGridOptions(DeclarationPart.R10));
            if (!_gridPresenterChapters.ContainsKey(DeclarationInvoiceChapterNumber.Chapter10))
            {
                _gridPresenterChapters.Add(DeclarationInvoiceChapterNumber.Chapter10, gridPresenterChapter);
            }
        }

        public void InitializeGridChapter11(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort>(),
                Filter = View.GetDefaultFilter(DeclarationPart.R11)

            };

            var gridPresenterChapter =
                new ChapterPagedDataGridPresenter(
                    grid,
                    pager,
                    searchCriteria =>
                    {
                        var chaptersData = GetChapterDatas(grid, DeclarationInvoiceChapterNumber.Chapter11, searchCriteria);
                        if (GetChapterDataCallback != null)
                        {
                            GetChapterDataCallback(grid, DeclarationInvoiceChapterNumber.Chapter11);
                        }
                        return chaptersData;
                    },
                    conditions);

            gridPresenterChapter.Init(SettingsProvider(string.Format("{0}_InitializeGridChapter11", GetType())),
                                      GetDefaultGridOptions(DeclarationPart.R11));
            if (!_gridPresenterChapters.ContainsKey(DeclarationInvoiceChapterNumber.Chapter11))
            {
                _gridPresenterChapters.Add(DeclarationInvoiceChapterNumber.Chapter11, gridPresenterChapter);
            }
        }

        public void InitializeGridChapter12(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort>(),
                Filter = View.GetDefaultFilter(DeclarationPart.R12)

            };

            var gridPresenterChapter =
                new ChapterPagedDataGridPresenter(
                    grid,
                    pager,
                    searchCriteria =>
                    {
                        var chaptersData = GetChapterDatas(grid, DeclarationInvoiceChapterNumber.Chapter12, searchCriteria);
                        if (GetChapterDataCallback != null)
                        {
                            GetChapterDataCallback(grid, DeclarationInvoiceChapterNumber.Chapter12);
                        }
                        return chaptersData;
                    },
                    conditions);

            gridPresenterChapter.Init(SettingsProvider(string.Format("{0}_InitializeGridChapter12", GetType())),
                                      GetDefaultGridOptions(DeclarationPart.R12));
            if (!_gridPresenterChapters.ContainsKey(DeclarationInvoiceChapterNumber.Chapter12))
            {
                _gridPresenterChapters.Add(DeclarationInvoiceChapterNumber.Chapter12, gridPresenterChapter);
            }
        }

        public PageResult<InvoiceModel> GetChapterDatas(IDataGridView grid, DeclarationInvoiceChapterNumber chapter, QueryConditions conditions)
        {
            if (_model.ChapterModel[chapter].IsSkipGetInvoices)
                _model.ChapterModel[chapter].IsSkipGetInvoices = false;
            else
            {
                var resultOperation = GetInvoices(chapter, conditions);
                if (resultOperation.Status != ResultStatus.Success)
                {
                    UIThreadExecutor.CurrentDispatcher.BeginInvoke(
                        new Action(() => View.ShowError(resultOperation.Message)));
                }
                else
                {
                    if (!resultOperation.Result.UsableData)
                    {
                        UIThreadExecutor.CurrentDispatcher.BeginInvoke(
                            new Action(() => View.ShowNotification(ResourceManagerNDS2.DeclarationMessages.InvoiceLoadingFailed)));
                    }
                }

                UIThreadExecutor.CurrentDispatcher.BeginInvoke(
                    new Action(() => View.PartGridsPro[chapter.ToDeclarationPart()].PanelLoadingVisible = false));
            }
            return new PageResult<InvoiceModel>(_model.ChapterModel[chapter].Invoices, _model.ChapterModel[chapter].TotalMatches);
        }

        public void GridChapterUpdate(DeclarationInvoiceChapterNumber chapter)
        {
            if (_gridPresenterChapters.ContainsKey(chapter))
            {
                _gridPresenterChapters[chapter].SetTotalAvalaible(
                    (uint)_model.ChapterModel[chapter].GetTotalInvoiceQuantity());
                _gridPresenterChapters[chapter].Load();
            }
        }

        public bool IsUseDataGridView(DeclarationInvoiceChapterNumber chapter)
        {
            return chapter == DeclarationInvoiceChapterNumber.Chapter8 ||
                   chapter == DeclarationInvoiceChapterNumber.Chapter9 ||
                   chapter == DeclarationInvoiceChapterNumber.Chapter10 ||
                   chapter == DeclarationInvoiceChapterNumber.Chapter11 ||
                   chapter == DeclarationInvoiceChapterNumber.Chapter12 ;
        }

        public bool IsUseDataGridView(DeclarationPart chapter)
        {
            return chapter == DeclarationPart.R8 ||
                   chapter == DeclarationPart.R9 ||
                   chapter == DeclarationPart.R10 ||
                   chapter == DeclarationPart.R11 ||
                   chapter == DeclarationPart.R12 ||
                   chapter == DeclarationPart.DOCKNP;
        }

        private static DeclarationInvoiceChapterNumber ConvertToInvoiceChapterNumber(DeclarationPart part)
        {
            var chapter = DeclarationInvoiceChapterNumber.Chapter8;
            if (part == DeclarationPart.R8)
                chapter = DeclarationInvoiceChapterNumber.Chapter8;
            if (part == DeclarationPart.R9)
                chapter = DeclarationInvoiceChapterNumber.Chapter9;
            if (part == DeclarationPart.R10)
                chapter = DeclarationInvoiceChapterNumber.Chapter10;
            if (part == DeclarationPart.R11)
                chapter = DeclarationInvoiceChapterNumber.Chapter11;
            if (part == DeclarationPart.R12)
                chapter = DeclarationInvoiceChapterNumber.Chapter12;
            return chapter;
        }

        #endregion

        #region Пользовательские настройки

        public bool GetSavedCommonInfoExpandedStateOrDefault()
        {
            ISettingsProvider provider = SettingsProvider(string.Format("{0}_declarationCard", GetType()));
            var state = provider.LoadSettings(ResourceManagerNDS2.UserSettingsKeys.DeclarationCommonInfoExpandState);
            bool val;

            return bool.TryParse(state, out val) && val;
        }

        private void ViewOnOnCommonInfoExpandStateChanged(object sender, EventArgsPrimitiveValue<bool> eventArgsPrimitiveValue)
        {
            ISettingsProvider provider = SettingsProvider(string.Format("{0}_declarationCard", GetType()));
            provider.SaveSettings(eventArgsPrimitiveValue.Value.ToString(), ResourceManagerNDS2.UserSettingsKeys.DeclarationCommonInfoExpandState);
        }

        #endregion

        #region Базовые операции карточки

        private void InitDeclarationChapter(DeclarationInvoiceChapterNumber chapter)
        {
            if ((_model.Type == DeclarationTypeCode.Declaration) && (_model.ChapterModel[chapter].GetTotalInvoiceQuantity() == 0))
            {
                //TODO переделать на событие
                _model.ChapterModel[chapter].ResetInvoices();
                GridChapterUpdate(chapter);

                View.ShowNotification(ResourceManagerNDS2.DeclarationMessages.NoInvoiceRecords);
                return;
            }

            if (PermitLoadingInvoices(chapter))
            {
                //aip показ прогресса
                View.PartGridsPro[chapter.ToDeclarationPart()].PanelLoadingVisible = true;

                LogActivity(new ActivityLogEntry { LogType = ActivityLogType.Chapter812OpenCount, Count = (int)chapter });

                GridChapterUpdate(chapter);

                return;
            }

            // отказ от загрузки
            _cancellationTokenSources[chapter.ToDeclarationPart()] = null;
            _model.ChapterModel[chapter].IsInitialized = false;

            //TODO
            _model.ChapterModel[chapter].ResetInvoices();
            _model.ChapterModel[chapter].IsSkipGetInvoices = true;
            GridChapterUpdate(chapter);
        }

        public void InitChapter(DeclarationPart part)
        {
            if (IsOnlyChapters8_12(part))
            {
                InitDeclarationChapter(part.ToDeclarationInvoiceChapter());
                return;
            }

            if (_cancellationTokenSources[part] != null)
                return;

            var partDt = _model.PartsData[part];

            if (IsUseDataGridView(part)) { }
            else
            {
                View.PartGrids[part].GridControl.PanelLoadingVisible = true;
                _model.PartsData[part].IsInitialized = false;
                _cancellationTokenSources[part] = new CancellationTokenSource();
            }

            if (part == DeclarationPart.DOCKNP)
            {
                GetKnpDocuments();
                return;
            }

            if (part == DeclarationPart.KC)
            {
                QueryConditions qc = View.PartGrids[part].GridControl.QueryConditions;
                partDt.qc = qc.Clone() as QueryConditions;

                GetDataControlRatio(qc, partDt);

                return;
            }

            if (part == DeclarationPart.Discrepancy)
            {
                QueryConditions qc = View.PartGrids[part].GridControl.QueryConditions;
                partDt.qc = qc.Clone() as QueryConditions;

                GetDiscrepancies(qc);
            }
        }

        private bool IsOnlyChapters8_12(DeclarationPart part)
        {
            return new[]
            {
                DeclarationPart.R8,
                DeclarationPart.R9,
                DeclarationPart.R10,
                DeclarationPart.R11,
                DeclarationPart.R12
            }.Contains(part);
        }

        private bool PermitLoadingInvoices(DeclarationInvoiceChapterNumber chapter)
        {
            if (!_model.ChapterModel[chapter].NeedWarning(_warningThreshold))
                return true;

            var nw = new NumWarning(
                string.Format(ResourceManagerNDS2.DeclarationMessages.HeavyLoad,
                    chapter.ToString().Substring("Chapter".Length),
                    _warningThreshold));

            nw.ShowDialog();

            return nw.DialogResult != DialogResult.Cancel;
        }

        #region Request data for DOCKNP, KC and Discrepancy

        public object GetData(DeclarationPart part, QueryConditions qc, out long totalRowsNumber)
        {
            DeclarationPartData item = UpdateChapter(part, qc);
            totalRowsNumber = item.TotalRowNumbers;
            return item.Result;
        }

        private DeclarationPartData UpdateChapter(DeclarationPart part, QueryConditions qc)
        {
            var ret = _model.PartsData[part];

            if (!ret.IsInitialized || (qc.Equals(ret.qc)))
                return ret;

            ret.qc = qc.Clone() as QueryConditions;

            if (IsUseDataGridView(part)) { }
            else
                View.PartGrids[part].GridControl.PanelLoadingVisible = true;

            var cts = _cancellationTokenSources[part];
            if (cts != null)
                cts.Cancel();

            switch (part)
            {
                case DeclarationPart.DOCKNP:
                    GetKnpDocuments();
                    break;
                case DeclarationPart.KC:
                    GetDataControlRatio(qc, ret);
                    break;
                case DeclarationPart.Discrepancy:
                    GetDiscrepancies(qc);
                    break;
            }

            return ret;
        }

        #endregion

        #region Request data for Invoices

        public object GetInvoiceData(DeclarationInvoiceChapterNumber part, QueryConditions qc, out long totalRowsNumber)
        {
            var item = UpdateInvoiceChapter(part, qc);
            totalRowsNumber = item.TotalMatches;
            return item.Invoices;
        }

        private ChapterDataModel UpdateInvoiceChapter(DeclarationInvoiceChapterNumber chapter, QueryConditions qc)
        {
            var data = _model.ChapterModel[chapter];

            if (!data.IsInitialized || (qc.Equals(data.Conditions)))
                return data;

            data.Conditions = qc.Clone() as QueryConditions;

            if (IsUseDataGridView(chapter))
                GridChapterUpdate(chapter);
            else
            {
                View.PartGrids[chapter.ToDeclarationPart()].GridControl.PanelLoadingVisible = true;

                var cts = _cancellationTokenSources[chapter.ToDeclarationPart()];
                if (cts != null)
                    cts.Cancel();

                GetInvoices(chapter, qc);
            }

            return data;
        }

        #endregion

        public void SetRevision(long rev)
        {
            if (_ctsInit != null)
                _ctsInit.Cancel();

            _ctsInit = ExecuteServiceCallAsync(
                c =>
                {
                    var blService = GetServiceProxy<IDeclarationsDataService>();

                    return blService.GetDeclaration(rev);
                },
                (c, r) =>
                {
                    _model.UpdateModel(r.Result);

                    _comparasionProcessValidator.ComparasionDataVersion = _model.ComparisonDataVersion;

                    if (_actEditPresenter != null)
                    {
                        _actEditPresenter.SetComparisionDataVersion(_model.ComparisonDataVersion);

                        if (_model.ActDocId.HasValue)
                            _actEditPresenter.SetDocumentId(_model.ActDocId.Value);
                    }

                    if (_decisionEditPresenter != null)
                    {
                        _decisionEditPresenter.SetComparisionDataVersion(_model.ComparisonDataVersion);

                        if (_model.DecisionDocId.HasValue)
                            _decisionEditPresenter.SetDocumentId(_model.DecisionDocId.Value);
                    }

                    StopPartWorkers();

                    View.ExecuteInUiThread(x =>
                    {
                        View.PartGrids.ForAll(cd => cd.Value.RequestId = null);
                        _gridPresenterChapters.ForAll(gp => gp.Value.ResetTotalsCache());
                        View.ShowDeclaration();
                        View.ExportExcelSetupAcess();
                        View.UpdateCurrentTab();
                        RefreshEKP();
                    });
                });
        }

        public string GetTemplateRnivc()
        {
            var catalogFactoryService = WorkItem.Services.Get<ICatalogService>(true);

            var address = new CatalogAddress(
                CatalogAddressSchemas.LogicalCatalog,
            "RoleCatalog",
            "NDS2Subsystem",
            "FileSystems",
            string.Format(_model.Type == DeclarationTypeCode.Declaration
                    ? ResourceManagerNDS2.NDSTemplateFilePath
                    : ResourceManagerNDS2.JRNLTemplateFilePath, _model.XmlVersion));

            var inputStream = catalogFactoryService.CreateInstance<Stream>(address);

            using (var reader = new StreamReader(inputStream, Encoding.GetEncoding("windows-1251")))
            {
                return reader.ReadToEnd();
            }
        }

        private void StopPartWorkers()
        {
            var keys = _cancellationTokenSources.Keys.ToArray();
            foreach (var part in keys)
            {
                if (_cancellationTokenSources[part] == null)
                    continue;
                _cancellationTokenSources[part].Cancel();
                _cancellationTokenSources[part] = null;
            }
        }

        private void StopWorkers()
        {
            if (_ctsInit != null)
                _ctsInit.Cancel();

            StopPartWorkers();
        }

        public void ViewPyramidReport()
        {
            ViewPyramidReport(_model.RawData);
        }

        public void OpenKnpDocumentDetailsCard(KnpDocument doc)
        {
            _knpDocumentsPresenter.OpenKnpDocumentCard(doc);
        }

        public void ViewTaxPayerDetails()
        {
            ViewTaxPayerByKppEffective(_model.Inn, _model.KppEffective);
        }

        public bool IsTreeRelationEligible()
        {
            var userAccessOperation = new UserAccessOpearation(GetServiceProxy<ISecurityService>());

            return userAccessOperation.AllowOperationTreeRelation(_model.SonoCode);
        }

        public bool IsStartWorkEligible()
        {
            bool ret = false;

            var blService = GetServiceProxy<IDeclarationsDataService>();
            var r = blService.GetUserStructContexts(Constants.SystemPermissions.Operations.OORTakeInWork);
            if (r.Status == ResultStatus.Success)
            {
                ret = r.Result.Contains(_model.SonoCode);
            }

            return ret;
        }
        #endregion

        #region Асинхронное получение данных МРР

        private void OnDiscrepancyListLoaded(object sender, DataLoadedEventArgs<PageResult<DeclarationDiscrepancy>> args)
        { 
            var data = new OperationResult<object> {
                Result = string.Empty, Status = ResultStatus.Success
            };

            var partDt = _model.PartsData[DeclarationPart.Discrepancy];

            var result = (args.Data == null) ? new List<DeclarationDiscrepancy>() : args.Data.Rows;
            var resultModels = result.Select(p => new DeclarationDiscrepancyModel(p)).ToList();

            partDt.Result = resultModels;
            partDt.TotalRowNumbers = (args.Data == null) ? 0 : args.Data.TotalMatches;

            ResultProcessing(DeclarationPart.Discrepancy, data);
        }

        private void GetDiscrepancies(QueryConditions qc)
        {
            _discrepancyListDataLoader.BeginDataLoad(qc);
        }

        private void GetDataControlRatio(QueryConditions qc, DeclarationPartData partData)
        {
            _cancellationTokenSources[DeclarationPart.KC] = ExecuteServiceCallAsync(
            c =>
            {
                var rez = new OperationResult<object> { Result = String.Empty };
                var blService = GetServiceProxy<IDeclarationsDataService>();

                BreakIfCancel(c);

                OperationResult<PageResult<ControlRatio>> r =
                    blService.GetDeclarationControlRatio(_model.RawData.DECLARATION_VERSION_ID, qc);

                BreakIfCancel(c);

                if (r.Status == ResultStatus.Success)
                {
                    partData.Result = r.Result.Rows;
                    partData.TotalRowNumbers = r.Result.TotalMatches;
                    partData.IsInitialized = true;
                }
                else
                {
                    rez.Status = r.Status;
                    rez.Message = r.Message;
                }

                return rez;
            },
            (c, rez) => ResultProcessing(DeclarationPart.KC, rez));
        }

        public bool IsNeedKnpDocumentsRefresh { get; set; }

        private KnpDocumentsPresenter GetKnpDocumentsPresenter()
        {
            if (_knpDocumentsPresenter == null)
            {
                var explainReplyServise = WorkItem.GetWcfServiceProxy<IExplainReplyDataService>();
                var accessContext = explainReplyServise.GetAccessContext().Result;

                _knpDocumentsPresenter = new KnpDocumentsPresenter(
                    View.KnpDocumentsView(),
                    new DeclarationKnpDocumentsModel(
                        accessContext, _model.RawData.INN,
                        _model.RawData.KPP_EFFECTIVE,
                        int.Parse(_model.RawData.FISCAL_YEAR),
                        _model.RawData.PeriodEffective),
                    new ReclaimDetailsViewer(WorkItem, _model.ChainAccess), 
                    new ClaimDetailsViewer(WorkItem), 
                    new KnpDocumentDetailsViewer(WorkItem, PresentationContext), 
                    new KnpDocumentsLoader(GetServiceProxy<IDeclarationsDataService>(),
                    WorkItem.Services.Get<INotifier>(true),
                    WorkItem.Services.Get<IClientLogger>(true))
                   );
            }

            return _knpDocumentsPresenter;
        }

        private void GetKnpDocuments()
        {
            var knpPresenter = GetKnpDocumentsPresenter();
            if (IsNeedKnpDocumentsRefresh)
                knpPresenter.DropCache();
            knpPresenter.BeginLoad(_model.RawData.INN_CONTRACTOR);
            IsNeedKnpDocumentsRefresh = false;
        }

        private void ResultProcessing(DeclarationPart part, IOperationResult resultOperation)
        {
            UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() =>
            {
                DeclarationPartData partData = _model.PartsData[part];
                partData.IsInitialized = true;
                if (!IsUseDataGridView(part))
                    View.PartGrids[part].GridControl.PanelLoadingVisible = false;

                if (resultOperation.Status == ResultStatus.Success)
                {
                    if (IsUseDataGridView(part)) { }
                    else
                        View.PartGrids[part].GridControl.UpdateData();
                }
                else
                {
                    View.ShowError(resultOperation.Message);
                }
            }));
        }

        private int GetSovPartNum(DeclarationPart part)
        {
            if (_model.RawData.DECL_TYPE_CODE != 1)
                return (int)part;

            switch (part)
            {
                case DeclarationPart.R10:
                    return (int)DeclarationPart.J1;
                case DeclarationPart.R11:
                    return (int)DeclarationPart.J2;
                default:
                    return (int)part;
            }
        }

        #endregion

        #region Переход в раздел другой декларации

        public bool IsActual()
        {
            return _model.RawData.IS_ACTUAL;
        }

        private List<DictionaryNalogPeriods> _nalogPeriods;
        private List<DictionaryNalogPeriods> GetDictionaryNalogPeriods()
        {
            List<DictionaryNalogPeriods> surs = null;
            if (ExecuteServiceCall(() => GetServiceProxy<IDataService>().GetNalogPeriods(),
                res =>
                {
                    surs = res.Result;
                }))
            {
                return surs;
            }
            return new List<DictionaryNalogPeriods>();
        }
        public List<DictionaryNalogPeriods> NalogPeriods
        {
            get
            {
                return _nalogPeriods ?? (_nalogPeriods = GetDictionaryNalogPeriods());
            }
        }

        private Direction GetNavigationDirection(InvoiceModel inv)
        {
            var part = (DeclarationPart)inv.CHAPTER;
            switch (part)
            {
                case DeclarationPart.R9:
                case DeclarationPart.R10:
                case DeclarationPart.R12:
                case DeclarationPart.R11:
                    return Direction.Sales;

                case DeclarationPart.R8:
                    return Direction.Purchase;
            }
            return Direction.Invariant;
        }

        private CardOpenResult ViewDeclarationDetailsFromDiscrepancyByZip(long? zip)
        {
            return zip.HasValue
                ? ViewDeclarationDetails(zip.Value)
                : new CardOpenError(
                    string.Format(
                        ResourceManagerNDS2.DeclarationMessages.DECLARATION_NOT_FOUND_PATTERN,
                        _model.RawData.FULL_TAX_PERIOD));
        }

        public void ViewDeclarationDetailsFromDiscrepancy(DeclarationDiscrepancy dis)
        {
            if (string.IsNullOrWhiteSpace(dis.ContractorInvoiceKey)
                && dis.ContractorZip.HasValue)
            {
                View.ShowWarning(ResourceManagerNDS2.DeclarationMessages.RecordNotCompared);
                return;
            }

            var result = !string.IsNullOrEmpty(dis.ContractorInvoiceKey)
                ? _declarationCardOpener.Open(dis.ContractorInvoiceKey, Direction.Invariant, _model.Inn)
                : ViewDeclarationDetailsFromDiscrepancyByZip(dis.ContractorZip);

            if (!result.IsSuccess)
            {
                View.ShowWarning(result.Message);
            }
        }

        public void ViewDeclarationDetailsFromInvoice(Invoice inv)
        {
            ViewDeclarationDetailsFromInvoice(new InvoiceModel(inv));
        }

        public void ViewDeclarationDetailsFromInvoice(InvoiceModel inv)
        {
            if (!IsActual())
                return;

            var direction = GetNavigationDirection(inv);

            if (!string.IsNullOrEmpty(inv.COMPARE_ROW_KEY)) // "С" Есть прямое сопоставление записи
            {
                var invoiceCompareKey = new InvoiceKey(inv.COMPARE_ROW_KEY);
                long zip = invoiceCompareKey.DeclarationZip;

                if (invoiceCompareKey.Chapter.HasValue)
                {
                    zip = GetDeclarationLastZipInheritChapter(invoiceCompareKey.DeclarationZip, (InvoiceRowKeyPartitionNumber)invoiceCompareKey.Chapter);
                }

                var result = _declarationCardOpener.Open(zip, inv.COMPARE_ROW_KEY, direction, _model.Inn);
                if (!result.IsSuccess)
                {
                    View.ShowWarning(result.Message);
                }

                return;
            }


            if (inv.CONTRACTOR_DECL_IN_MC) // "К" В МС есть декларация контрагента
            {
                DeclarationSummary contractorDeclaration = null;
                long? contractorDeclarationZip = null;
                ExecuteServiceCallAsync(
                    r =>
                    {
                        contractorDeclarationZip = GetContractorDeclarationZip(inv);
                        if (contractorDeclarationZip.HasValue)
                            contractorDeclaration = GetDeclaration(contractorDeclarationZip.Value);
                        return new OperationResult();
                    },
                    (c, r) => UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() =>
                    {
                        if (contractorDeclarationZip == null)
                        {
                            View.ShowWarning(ResourceManagerNDS2.DeclarationMessages.DECLARATION_NOT_LOADED_IN_MS);
                            return;
                        }

                        if (contractorDeclaration == null)
                        {
                            View.ShowWarning(ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD);
                            return;
                        }

                        View.ShowWarning(
                            ResourceManagerNDS2.DeclarationMessages
                                .CONTRACTOR_DECLARATION_RELOCATION_WARNING);

                        _declarationCardOpener.Open(
                            contractorDeclaration,
                            null,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            -1,
                            null,
                            direction);
                    })));
                return;
            }

            if (inv.CONTRACTOR_KEY.HasValue)
            {
                View.ShowWarning(
                    ResourceManagerNDS2.DeclarationMessages.DECLARATION_NOT_LOADED_IN_MS);

                return;
            }

            View.ShowWarning(
                string.Format(
                    ResourceManagerNDS2.DeclarationMessages.DECLARATION_NOT_FOUND_PATTERN,
                    ExpectedContractorPeriod(inv)));
        }

        #region Month to period map
        private readonly Dictionary<int, string> _monthToPeriodMap = new Dictionary<int, string>
        {
            {1,  "1 кв. "},
            {2,  "1 кв. "},
            {3,  "1 кв. "},
            {4,  "2 кв. "},
            {5,  "2 кв. "},
            {6,  "2 кв. "},
            {7,  "3 кв. "},
            {8,  "3 кв. "},
            {9,  "3 кв. "},
            {10, "4 кв. "},
            {11, "4 кв. "},
            {12, "4 кв. "}
        };
        #endregion

        private string ExpectedContractorPeriod(InvoiceModel inv)
        {
            if (!inv.CREATE_DATE.HasValue)
                return _model.FulTaxPeriod;
            return _monthToPeriodMap[inv.CREATE_DATE.Value.Month] + inv.CREATE_DATE.Value.Year;
        }

        private long? GetContractorDeclarationZip(InvoiceModel inv)
        {
            if (!inv.CONTRACTOR_KEY.HasValue)
                return 0;
            var helper = new ContractorDeclarationKey(inv.CONTRACTOR_KEY.Value);
            long? zip = 0;
            ExecuteServiceCall(
                () =>
                    GetServiceProxy<IDeclarationsDataService>()
                        .GetContractorDeclarationZip(helper.GetInn(), helper.GetYear(), helper.GetMonth()),
                res =>
                {
                    if (res.Result != null)
                        zip = res.Result.ZIP;
                    else
                        zip = null;
                });
            return zip;
        }

        private long GetDeclarationLastZipInheritChapter(long zip, InvoiceRowKeyPartitionNumber chapter)
        {
            long zipInherit = 0;
            ExecuteServiceCall(
                () =>
                    GetServiceProxy<IDeclarationsDataService>()
                        .GetDeclarationLastZipInheritChapter(zip, chapter),
                res =>
                {
                    zipInherit = res.Result;
                });
            return zipInherit;
        }

        public Dictionary<int, int> GetChaptersSellers(long declarationId, string inn)
        {
            Dictionary<int, int> chapters = null;
            if (ExecuteServiceCall(() => GetServiceProxy<IDeclarationsDataService>().GetChaptersSellers(declarationId, inn),
                res =>
                {
                    chapters = res.Result;
                }))
            {
                return chapters;
            }
            return new Dictionary<int, int>();
        }

        public Dictionary<int, int> GetChaptersBuyers(long declarationId, string inn)
        {
            Dictionary<int, int> chapters = null;
            if (ExecuteServiceCall(() => GetServiceProxy<IDeclarationsDataService>().GetChaptersBuyers(declarationId, inn),
                res =>
                {
                    chapters = res.Result;
                }))
            {
                return chapters;
            }
            return new Dictionary<int, int>();
        }

        public int GetChapterNumberNeed_R8_R11(string inn)
        {
            var chapterNeed = 8;
            var chapters = GetChaptersBuyers(_model.RawData.ID, inn);
            if (chapters.Count(p => p.Value > 0) > 0)
            {
                chapterNeed = chapters.FirstOrDefault(p => p.Value > 0).Key;
            }
            return chapterNeed;
        }
        public int GetChapterNumberNeed_R9_R10_R12(string inn)
        {
            var chapterNeed = 9;
            var chapters = GetChaptersSellers(_model.RawData.ID, inn);
            if (chapters.Count(p => p.Value > 0) > 0)
            {
                chapterNeed = chapters.FirstOrDefault(p => p.Value > 0).Key;
            }
            return chapterNeed;
        }

        #endregion

        #region Трансформация QueryCondition перед отправкой в BlService

        private QueryConditions TransformQueryConditions(QueryConditions queryConditionIn)
        {
            QueryConditions queryConditionsTransform = null;
            if (queryConditionIn != null)
            {
                queryConditionsTransform = (QueryConditions)queryConditionIn.Clone();

                TransformFilteringDigitEmpty(queryConditionsTransform);
            }

            return queryConditionsTransform;
        }

        private void TransformFilteringDigitEmpty(QueryConditions queryConditionsTransform)
        {
            var fieldNamesOfDigitEmpty = new List<string>
            {
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_BUY_NDS_AMOUNT),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_SELL),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_SELL),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_SELL_10),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_SELL_0),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_NDS_18),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_NDS_10),
                    TypeHelper<Invoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_INCREASE),
                    TypeHelper<Invoice>.GetMemberName(t => t.DIFF_CORRECT_DECREASE),
                    TypeHelper<Invoice>.GetMemberName(t => t.DIFF_CORRECT_INCREASE),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_TOTAL),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_NDS_TOTAL),
                    TypeHelper<Invoice>.GetMemberName(t => t.PRICE_TAX_FREE)
                };

            TransformQueryConditionCreator
                .CreateTransformDigitEmpty(queryConditionsTransform, fieldNamesOfDigitEmpty)
                .TransformFiltering();
        }

        #endregion

        #region Экпорт в Эксель разделов 8-12

        public Dictionary<DeclarationPart, ExportExcelParam> ExportExcelParams = new Dictionary<DeclarationPart, ExportExcelParam>();
        public ReportGeneralHelper ReportGeneralhelper = new ReportGeneralHelper();

        public bool AllowExportToExcel()
        {
           return _userAccessOpearation.AllowOpearationDeclarationChapterExportExcel();
        }

        public void ExportExcelStart(DeclarationPart part, string fileName)
        {
            var exportParams = CreateExportExcelParam(part, fileName);

            View.SetExportExcelState(exportParams.DeclarationPart, "Расчет общего количества записей ...");
            View.SetButtonExportExcelVisible(exportParams.DeclarationPart, false, true);
            View.SetExportExcelStateVisible(exportParams.DeclarationPart, true);
            exportParams.TimerUpdateState.Enabled = true;

            var reportToExcelWorker = ExportExcel(exportParams);

            exportParams.ResetEvent.Set();
            reportToExcelWorker.Start();
        }

        private ExportExcelParam CreateExportExcelParam(DeclarationPart part, string filePath)
        {
            var p = new ExportExcelParam();
            p.DeclarationPart = part;
            p.InvoiceCount = -1;
            p.ExcelManager = ExcelManagerCreator.CreateExcelManagerInvoice();
            p.ExcelManager.ExportExcelParam = p;
            p.PageFirstExcel = 1;
            p.PageCountExcel = 1;
            p.InvoiceExcelName = Path.GetFileName(filePath);
            p.TimerOpenReport = new ExportExcelTimer { ExcportExcelParam = p };
            p.TimerOpenReport.Tick += TimerOpenExportExcel_Tick;
            p.TimerUpdateState = new ExportExcelTimer { ExcportExcelParam = p };
            p.TimerUpdateState.Tick += TimerUpdateStateExcel_Tick;
            p.InvoiceExcelFilePathTemp = GenerateInvoiceExcelFileNameTemp(part);
            p.InvoiceExcelFilePath = filePath;
            p.CountReadRows = 0;
            p.ResetEvent = new ManualResetEventSlim();
            p.SheetNameTemplate = string.Format("Раздел_{0}", GetSovPartNum(p.DeclarationPart));
            p.SheetSizeRow = 1000000;

            if (ExportExcelParams.ContainsKey(part))
            {
                ExportExcelParams[part].TimerOpenReport.Enabled = false;
                ExportExcelParams[part].TimerUpdateState.Enabled = false;
                ExportExcelParams[part] = p;
            }
            else
                ExportExcelParams.Add(part, p);
            return p;
        }

        private AsyncWorker<OperationResult<bool>> ExportExcel(ExportExcelParam p)
        {
            var reportToExcelWorker = new AsyncWorker<OperationResult<bool>>();

            reportToExcelWorker.DoWork += (sender, args) =>
            {
                p.InvoiceCount = _model.ChapterModel[p.DeclarationPart.ToDeclarationInvoiceChapter()].TotalMatches;
                p.ExcelManager.ExcelGenerateEventExt += SetCountGenerateRows;

                ConfigExportInvoice cfgExportInvoice = GetConfigExportInvoice();
                p.PageSize = cfgExportInvoice.PageSize;
                p.DelayBetweenPage = cfgExportInvoice.DelayBetweenPage;
                if (p.InvoiceCount > cfgExportInvoice.InvoiceMaxCount)
                {
                    UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() => View.ShowError(
                        string.Format(ResourceManagerNDS2.DeclarationMessages.HaveToDecreaseNumber,
                            cfgExportInvoice.InvoiceMaxCount))));
                    args.Result = new OperationResult<bool> { Status = ResultStatus.Error };
                    return;
                }

                p.Caclulate();
                UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() => ExportExcelUpdateState(p)));

                FileHelper.FileDeleteIfExists(p.InvoiceExcelFilePathTemp);

                var columnsWidth = new List<double>();
                var columns = GetColumnsR8_12(p.DeclarationPart).ToReadOnly();
                var dtRows = CreateDataTableExcel(columns, columnsWidth);
                var sheetName = p.ExcelManager.ExportExcelParam.GetSheetName(0);
                p.ExcelManager.CreateExcel(p.InvoiceExcelFilePathTemp, sheetName, columnsWidth);
                ReportGeneralhelper.AddHeaderForExcel(dtRows, columns, false);

                var isNeedTitleInHeader = ExportExcelIsNeedTitleInHeader(p.DeclarationPart);
                var numRowHeaderTable = 1;
                if (isNeedTitleInHeader)
                {
                    numRowHeaderTable = 3;
                    ExcelSetupWithTitle(p, new List<long> { 2, 3 });
                }
                else { ExcelSetup(p); }
                for (var numSheet = -1; ++numSheet < p.SheetCount; )
                {
                    if (isNeedTitleInHeader)
                    {
                        p.ExcelManager.MergeTwoCells(p.InvoiceExcelFilePathTemp, p.GetSheetName(numSheet), "A1", "Z1");
                        p.ExcelManager.MergeTwoCells(p.InvoiceExcelFilePathTemp, p.GetSheetName(numSheet), "A2", "Z2");
                    }
                    p.ExcelManager.MergeMergeCells(p.InvoiceExcelFilePathTemp, p.GetSheetName(numSheet),
                                                    ReportGeneralhelper.GetExcelMergeCells(columns, numRowHeaderTable));
                }

                long rowTotalCount = 0;
                for (var i = -1; ++i < p.SheetCount; )
                {
                    rowTotalCount = p.ExcelManager.FillWorkbook(p.InvoiceExcelFilePathTemp, i, rowTotalCount, GetPageDataExcel);
                }
                if (p.ResetEvent.IsSet)
                {
                    File.Copy(p.InvoiceExcelFilePathTemp, p.InvoiceExcelFilePath, true);
                    args.Result = new OperationResult<bool> { Status = ResultStatus.Success };
                }
                else
                {
                    FileHelper.FileDeleteIfExists(p.InvoiceExcelFilePathTemp);
                    args.Result = new OperationResult<bool> { Status = ResultStatus.Error };
                }
            };

            reportToExcelWorker.Complete += (sender, args) =>
            {
                p.ResetEvent.Reset();
                if (args.Result.Status == ResultStatus.Success)
                {
                    p.TimerOpenReport.Enabled = true;
                }
                p.TimerUpdateState.Enabled = false;
                UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() =>
                 {
                     View.SetButtonExportExcelVisible(p.DeclarationPart, true, false);
                     ExportExcelUpdateState(p);
                     View.SetExportExcelState(p.DeclarationPart, String.Empty);
                 }));
            };

            reportToExcelWorker.Failed += (sender, args) =>
            {
                var errMessage = String.Empty;
                Exception errException = null;
                if (args != null && args.Exception != null)
                {
                    errMessage = args.Exception.Message;
                    errException = args.Exception;
                }
                LogError(errMessage, "DeclarationCardPresenter::ExportExcel", errException);

                p.ResetEvent.Reset();
                p.TimerOpenReport.Enabled = false;
                p.TimerUpdateState.Enabled = false;
                UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() =>
                {
                    View.SetButtonExportExcelVisible(p.DeclarationPart, true, false);
                    View.ShowError(string.Format(ResourceManagerNDS2.DeclarationMessages.ExportingFailed, p.InvoiceExcelName, errMessage));
                    ExportExcelUpdateState(p);
                    View.SetExportExcelState(p.DeclarationPart, String.Empty);
                }));
            };

            return reportToExcelWorker;
        }

        private DataTable GetPageDataExcel(uint pageFrom, uint page, ExportExcelParam p, out long rowsTotalMatches, out long rowCountHeader)
        {
            rowCountHeader = 0;
            List<ColumnBase> columns = GetColumnsR8_12(p.DeclarationPart);
            DataTable dtRows = CreateDataTableExcel(columns);
            if (pageFrom == page)
            {
                string title = View.GetTitleChapterR8_12(p.DeclarationPart);
                if (!string.IsNullOrWhiteSpace(title))
                {
                    dtRows.Rows.Add(title);
                    dtRows.Rows.Add(String.Empty);
                }
                ReportGeneralhelper.AddHeaderForExcel(dtRows, columns.ToReadOnly(), false);
                rowCountHeader = dtRows.Rows.Count;
            }
            List<Dictionary<string, object>> rows = GetRowsExcelByPage(page, p, out rowsTotalMatches);
            FillDataTableExcel(dtRows, rows, columns);
            return dtRows;
        }

        private void FillDataTableExcel(DataTable dtRows, List<Dictionary<string, object>> rows, List<ColumnBase> columns)
        {
            foreach (Dictionary<string, object> dicts in rows)
            {
                var objs = new List<object>();
                foreach (ColumnBase itemColumn in columns)
                {
                    var columnDef = itemColumn as ColumnDefinition;
                    if (columnDef != null)
                    {
                        ReportGeneralhelper.AddRowItem(columnDef, objs, dicts);
                    }
                    else
                    {
                        var columnGroupDef = itemColumn as ColumnGroupDefinition;
                        if (columnGroupDef == null)
                            continue;

                        foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                        {
                            ReportGeneralhelper.AddRowItem(itemColumnDef, objs, dicts);
                        }
                    }
                }
                dtRows.Rows.Add(objs.ToArray());
            }
        }

        private List<Dictionary<string, object>> GetRowsExcelByPage(uint page, ExportExcelParam p, out long rowsTotalMatches)
        {
            rowsTotalMatches = p.InvoiceCount;

            var blService = GetServiceProxy<IDeclarationsDataService>();
            ConfigExportInvoice cfgExportInvoice;
            OperationResult<ConfigExportInvoice> rezCfgExportExcel = blService.GetConfigExportInvoice();
            if (rezCfgExportExcel.Status == ResultStatus.Success)
                cfgExportInvoice = rezCfgExportExcel.Result;
            else
                throw new Exception(rezCfgExportExcel.Message);

            if (page > p.PageFirstExcel)
                Thread.Sleep(cfgExportInvoice.DelayBetweenPage);

            QueryConditions qc = GetQueryCondition(p.DeclarationPart);
            QueryConditions qcClone = TransformQueryConditions((QueryConditions)qc.Clone());
            qcClone.PaginationDetails.SkipTotalMatches = true;
            qcClone.PageIndex = (uint)page;
            qcClone.PageSize = (ushort)cfgExportInvoice.PageSize;
            qcClone.PaginationDetails.RowsToSkip = qcClone.PageSize * (qcClone.PageIndex - 1);
            qcClone.PaginationDetails.RowsToTake = qcClone.PageSize;

            var chapter = GetSovPartNum(p.DeclarationPart);
            var request = new DeclarationChapterDataRequest
            {
                DeclarationId = _model.DeclarationVersionId,
                Chapter = chapter,
                SearchContext = qcClone,
                ClientCache = _model.ChapterModel[p.DeclarationPart.ToDeclarationInvoiceChapter()].Cache.ToList(),
                ClientCacheSummaries = _model.ChapterModel[p.DeclarationPart.ToDeclarationInvoiceChapter()].CacheSummaries
            };

            var retVal = new List<Dictionary<string, object>>();
            ExecuteServiceCall(
                () => _invoiceService.SearchInDeclarationChapter(request),
                response =>
                {
                    var invoices = response.Result.Invoices != null ? response.Result.Invoices.Select(m => new InvoiceModel(m)).ToList() : null;
                    retVal = ReportGeneralhelper.ConvertToDictionary(invoices);
                    retVal = ReportGeneralhelper.ConvertToColumnFormat(retVal);
                });

            return retVal;
        }

        private ConfigExportInvoice GetConfigExportInvoice()
        {
            ConfigExportInvoice result = null;
            ExecuteServiceCall(
                () => GetServiceProxy<IDeclarationsDataService>().GetConfigExportInvoice(),
                opResult => result = opResult.Result,
                opResult => UIThreadExecutor.CurrentDispatcher.BeginInvoke(
                    new Action(
                        () =>
                            View.ShowError(
                                string.Format(ResourceManagerNDS2.DeclarationMessages.ExportConfigurationFailed,
                                    opResult.Message))))
                );
            return result;
        }

        private DataTable CreateDataTableExcel(IEnumerable<ColumnBase> columns, List<double> columnsWidth = null)
        {
            var dtRows = new DataTable("dtRows");
            foreach (ColumnBase elem in columns)
            {
                var columnDef = elem as ColumnDefinition;
                if (columnDef != null)
                {
                    DataColumn dataColumn = dtRows.Columns.Add(columnDef.Key, typeof(object));
                    dataColumn.ReadOnly = true;
                    dataColumn.Caption = columnDef.Caption;
                    AddColumnsWidth(columnDef, columnsWidth);
                }
                else
                {
                    var columnGroupDef = elem as ColumnGroupDefinition;
                    if (columnGroupDef != null)
                    {
                        foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                        {
                            DataColumn dataColumn = dtRows.Columns.Add(itemColumnDef.Key, typeof(object));
                            dataColumn.ReadOnly = true;
                            dataColumn.Caption = itemColumnDef.Caption;
                            AddColumnsWidth(itemColumnDef, columnsWidth);
                        }
                    }
                }
            }
            return dtRows;
        }

        private void AddColumnsWidth(ColumnDefinition columnDef, List<double> columnsWidth)
        {
            if (columnsWidth != null)
            {
                if (columnDef != null && columnDef.WidthColumnExcel != null)
                {
                    columnsWidth.Add((double)columnDef.WidthColumnExcel);
                }
                else
                {
                    columnsWidth.Add(18);
                }
            }
        }

        private List<ColumnBase> GetColumnsR8_12(DeclarationPart part)
        {
            var columns = new List<ColumnBase>();
            if (part == DeclarationPart.R8)
                columns = View.GetColumnsChapter8();
            if (part == DeclarationPart.R9)
                columns = View.GetColumnsChapter9();
            if (part == DeclarationPart.R10)
                columns = View.GetColumnsChapter10();
            if (part == DeclarationPart.R11)
                columns = View.GetColumnsChapter11();
            if (part == DeclarationPart.R12)
                columns = View.GetColumnsChapter12();
            return columns;
        }

        private bool ExportExcelIsNeedTitleInHeader(DeclarationPart part)
        {
            bool ret = false;
            string title = View.GetTitleChapterR8_12(part);
            if (!string.IsNullOrWhiteSpace(title))
            {
                ret = true;
            }
            return ret;
        }

        private QueryConditions GetQueryCondition(DeclarationPart part)
        {
            QueryConditions searchCriteria = null;
            if (IsUseDataGridView(part))
            {
                var chapter = ConvertToInvoiceChapterNumber(part);
                if (_gridPresenterChapters.ContainsKey(chapter))
                    searchCriteria = _gridPresenterChapters[chapter].GetSearchCriteria();
            }

            if (View.PartGrids.ContainsKey(part))
            {
                searchCriteria = View.PartGrids[part].GridControl.QueryConditions;
            }
            return searchCriteria;
        }

        private void TimerOpenExportExcel_Tick(object sender, EventArgs e)
        {
            var timer = (ExportExcelTimer)sender;
            timer.Enabled = false;
            View.OpenExcelFile(timer.ExcportExcelParam.InvoiceExcelFilePath, timer.ExcportExcelParam.InvoiceExcelName);
            timer.ExcportExcelParam.TimerUpdateState.Enabled = false;
        }

        public void SetCountGenerateRows(ExportExcelParam p, long countRows)
        {
            p.CountReadRows = countRows;
        }

        private void TimerUpdateStateExcel_Tick(object sender, EventArgs e)
        {
            var timer = (ExportExcelTimer)sender;
            ExportExcelParam p = timer.ExcportExcelParam;
            if (p.InvoiceCount > -1)
            {
                ExportExcelUpdateState(p);
            }
        }

        private void ExportExcelUpdateState(ExportExcelParam p)
        {
            View.SetExportExcelState(p.DeclarationPart, string.Format("Экпорт в Excel {0:N0} из {1:N0}", p.CountReadRows, p.InvoiceCount));
        }

        public void ExportExcelCancel(DeclarationPart part)
        {
            if (ExportExcelParams.ContainsKey(part))
            {
                ExportExcelParams[part].ResetEvent.Reset();
            }
        }

        public void ExportExcelCancelAll()
        {
            foreach (KeyValuePair<DeclarationPart, ExportExcelParam> item in ExportExcelParams)
            {
                item.Value.ResetEvent.Reset();
            }
        }

        public void OpenInvoiceExcelFile(string reportExcelFilePath)
        {
            ReportGeneralhelper.OpenReportExcelFile(reportExcelFilePath);
        }

        public string GenerateInvoiceExcelFileNameTemp(DeclarationPart part)
        {
            string basePath = Path.GetTempPath();
            return string.Format("{0}\\temp_{1}_{2}_{3}.{4}", basePath, _model.Inn, GetSovPartNum(part), Guid.NewGuid(), GetInvoiceExcelFileExtention());
        }

        public string GenerateInvoiceExcelFileName(DeclarationPart part)
        {
            DateTime dtNow = DateTime.Now;
            return string.Format("{0}_раздел_{1}_{2:0000}{3:00}{4:00}_{5:00}-{6:00}.{7}", _model.Inn, GetSovPartNum(part), dtNow.Year, dtNow.Month, dtNow.Day, dtNow.Hour, dtNow.Minute, GetInvoiceExcelFileExtention());
        }

        public string GetInvoiceExcelFileExtention()
        {
            return "xlsx";
        }

        private void ExcelSetup(ExportExcelParam p)
        {
            p.ExcelManager.SetupHeaderRowCount(2);

            var rowsStyle = new Dictionary<long, ExcelRowStyle>
            {
                {0, new ExcelRowStyle {IsHorizontalAlignmentCenter = true}},
                {1, new ExcelRowStyle {IsVerticalAlignmentCenter = true}}
            };
            p.ExcelManager.SetupRowStyle(rowsStyle);
        }

        private void ExcelSetupWithTitle(ExportExcelParam p, List<long> headerRows)
        {
            p.ExcelManager.SetupHeaderRows(headerRows);

            long firstRowHeader = headerRows.First();
            long secondRowHeader = firstRowHeader + 1;
            if (headerRows.Count >= 2)
            {
                secondRowHeader = headerRows[1];
            }

            var rowsStyle = new Dictionary<long, ExcelRowStyle>
            {
                {firstRowHeader, new ExcelRowStyle {IsHorizontalAlignmentCenter = true}},
                {secondRowHeader, new ExcelRowStyle {IsVerticalAlignmentCenter = true}}
            };
            p.ExcelManager.SetupRowStyle(rowsStyle);
        }

        #endregion

        # region Загрузка разделов деклараций

        private readonly IInvoiceDataService _invoiceService;

        private readonly int _warningThreshold;

        private OperationResult<DeclarationChapterData> GetInvoices(
            DeclarationInvoiceChapterNumber chapter,
            QueryConditions searchContext)
        {
            var partNumber = GetSovPartNum(chapter.ToDeclarationPart());

            do
            {
                try
                {
                    var response = _invoiceService.SearchInDeclarationChapter(
                        new DeclarationChapterDataRequest
                        {
                            DeclarationId = _model.DeclarationVersionId,
                            Chapter = partNumber,
                            ClientCache = _model.ChapterModel[chapter].Cache.ToList(),
                            ClientCacheSummaries = _model.ChapterModel[chapter].CacheSummaries,
                            SearchContext = TransformQueryConditions(searchContext)
                        });

                    if (response.Status == ResultStatus.Success)
                    {
                        if (!response.Result.UsableData)
                        {
                            _model.ChapterModel[chapter].Update(response.Result);

                            if (!searchContext.PaginationDetails.SkipTotalMatches)
                                _model.ChapterModel[chapter].TotalMatches = response.Result.MatchesQuantity;

                            return response;
                        }

                        if (searchContext.PaginationDetails.SkipTotalMatches)
                            response.Result.MatchesQuantity = _model.ChapterModel[chapter].TotalMatches;

                        _model.ChapterModel[chapter].Update(response.Result);

                        if (response.Result.PartDataList.All(x => x.DataReady))
                        {
                            if (!searchContext.PaginationDetails.SkipTotalMatches)
                            {
                                _model.ChapterModel[chapter].TotalMatches = response.Result.MatchesQuantity;
                            }
                            return response;
                        }
                        Thread.Sleep(2000);
                    }
                    else
                    {
                        return response;
                    }
                }
                catch (Exception error)
                {
                    LogError(error.Message, "GetInvoices", error);

                    return new OperationResult<DeclarationChapterData>
                    {
                        Status = ResultStatus.Error,
                        Message = ResourceManagerNDS2.DeclarationMessages.DataLoadingFailed
                    };
                }
            }
            while (true);
        }

        # endregion

        #region Акты и решения

        public bool IsActSelectionCardAccessible()
        {
            return _model.KnpActState.SelectionAvailable() && (_model.InspectorSid == UserSid);
        }

        public bool IsDecisionSelectionCardAccessible()
        {
            return !_model.KnpActState.SelectionAvailable()
                && _model.KnpDecisionState.SelectionAvailable()
                && (_model.InspectorSid == UserSid);
        }

        public bool IsActEditFullAccess()
        {
            return _model.KnpActState.EditAvailable() && (_model.InspectorSid == UserSid);
        }

        public bool IsDecisionEditFullAccess()
        {
            return _model.KnpDecisionState.EditAvailable() && (_model.InspectorSid == UserSid);
        }

        public void ViewActSelectionCard()
        {
            var validationResult = _comparasionProcessValidator.Validate();
            if (!validationResult.IsValid)
            {
                View.ShowInfo(
                    ResourceManagerNDS2.Attention,
                    validationResult.Message);

                return;
            }
            
            var managementService = GetServiceProxy<IAppendActDiscrepancyService>();

            View.LockViewActSelectionCard(true);
            RefreshEKP();

            // асинхронное создание акта если надо
            if (!_model.ActDocId.HasValue)
            {
                ExecuteServiceCallAsync(
                c => managementService.CreateDocument(
                    _model.Inn,
                    _model.InnContractor,
                    _model.Kpp,
                    _model.KppEffective, 
                    _model.Year,
                    _model.TaxPeriodCode),
                (c, rez) =>
                {
                    UIThreadExecutor.CurrentExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _model.ActDocId = rez.Result;

                            View.LockViewActSelectionCard(false);
                            RefreshEKP();

                            var result = ViewActSelectionCard(
                                rez.Result, 
                                managementService,
                                this.View,
                                _model.ComparisonDataVersion);

                            if (result && !cancelToken.IsCancellationRequested)
                            {
                                _model.HasActData = true;

                                if (_actEditPresenter != null)
                                    _actEditPresenter.ResetTotalsCache();

                                View.UpdateActEditTab();
                            }
                        }
                    }, 
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    UIThreadExecutor.CurrentExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            ServiceCallErrorShow(r, e);

                            View.LockViewActSelectionCard(false);
                            RefreshEKP();
                        }
                    }, 
                    DispatcherPriority.Normal, c);
                });
            }
            else
            {
                var result = ViewActSelectionCard(
                    _model.ActDocId.Value, 
                    managementService,
                    this.View,
                    _model.ComparisonDataVersion);

                if (result)
                {
                    _model.HasActData = true;

                    if (_actEditPresenter != null)
                        _actEditPresenter.ResetTotalsCache();

                    View.UpdateActEditTab();
                }

                View.LockViewActSelectionCard(false);
                RefreshEKP();
            }
        }

        public void ViewDecisionSelectionCard()
        {
            var validationResult = _comparasionProcessValidator.Validate();
            if (!validationResult.IsValid)
            {
                View.ShowInfo(
                    ResourceManagerNDS2.Attention,
                    validationResult.Message);

                return;
            }
            
            var managementService = GetServiceProxy<IAppendDecisionDiscrepancyService>();

            View.LockViewDecisionSelectionCard(true);
            RefreshEKP();

            // асинхронное создание решения если надо
            if (!_model.DecisionDocId.HasValue)
            {
                ExecuteServiceCallAsync(
                c => managementService.CreateDocument(
                    _model.Inn,
                    _model.InnContractor,
                    _model.Kpp,
                    _model.KppEffective,
                    _model.Year,
                    _model.TaxPeriodCode),
                (c, rez) =>
                {
                    UIThreadExecutor.CurrentExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _model.DecisionDocId = rez.Result;

                            View.LockViewDecisionSelectionCard(false);
                            RefreshEKP();

                            var result = ViewDecisionSelectionCard(
                                rez.Result,
                                managementService,
                                this.View,
                                _model.ComparisonDataVersion);

                            if (result && !cancelToken.IsCancellationRequested)
                            {
                                _model.HasDecisionData = true;

                                if (_decisionEditPresenter != null)
                                    _decisionEditPresenter.ResetTotalsCache();

                                View.UpdateDecisionEditTab();
                            }
                        }
                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    UIThreadExecutor.CurrentExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            ServiceCallErrorShow(r, e);
                            
                            View.LockViewDecisionSelectionCard(false);
                            RefreshEKP();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });
            }
            else
            {
                var result = ViewDecisionSelectionCard(
                    _model.DecisionDocId.Value,
                    managementService,
                    this.View,
                    _model.ComparisonDataVersion);

                if (result)
                {
                    _model.HasDecisionData = true;

                    if (_decisionEditPresenter != null)
                        _decisionEditPresenter.ResetTotalsCache();

                    View.UpdateDecisionEditTab();
                }

                View.LockViewDecisionSelectionCard(false);
                RefreshEKP();
            }
        }

        public void InitAct()
        {
            if (_actEditPresenter == null)
            {
                if (_model.ActDocId.HasValue)
                {
                    var srv = base.GetServiceProxy<IEditActDiscrepancyService>();
                    IServiceCallErrorHandler errorHandler = new CommonServiceRequestErrorHandler(View);
                    ISettingsProvider settingsProvider = SettingsProvider(string.Format("{0}_InitializeAct", GetType()));

                    _actEditPresenter = new ActDiscrepancyEditPresenter(_model.ActDocId.Value,
                                                                        new DeclarationModelActDataExtractor(_model),
                                                                        View.ActEditView,
                                                                        _requestExecutor,
                                                                        srv,
                                                                        UIThreadExecutor.CurrentExecutor,
                                                                        errorHandler,
                                                                        this.View,
                                                                        settingsProvider,
                                                                        GetServiceProxy<IDeclarationsDataService>(),
                                                                        _model.ComparisonDataVersion);

                    _actEditPresenter.ClosedDocument += ActDiscrepancyClosedDocument;

                    _actEditPresenter.Init(IsActEditFullAccess());
                }
                else
                    throw new Exception("Error in card initialization workflow");
            }
            else
                _actEditPresenter.Activate();
        }

        private void ActDiscrepancyClosedDocument(object sender, EventArgs e)
        {
            _model.ActIsClosed = true;

            View.UpdateOpenActSelectionButton();
            View.UpdateOpenDecisionSelectionButton();
            RefreshEKP();

            _actEditPresenter.RefreshData();
        }

        public void InitDecision()
        {
            if (_decisionEditPresenter == null)
            {
                if (_model.DecisionDocId.HasValue)
                {
                    var srv = base.GetServiceProxy<IEditDecisionDiscrepancyService>();
                    IServiceCallErrorHandler errorHandler = new CommonServiceRequestErrorHandler(View);
                    ISettingsProvider settingsProvider = SettingsProvider(string.Format("{0}_InitializeDecision", GetType()));
                    
                    _decisionEditPresenter = new DecisionDiscrepancyEditPresenter(_model.DecisionDocId.Value,
                                                                                  new DeclarationModelDecisionDataExtractor(_model),
                                                                                  View.DecisionEditView,
                                                                                  _requestExecutor,
                                                                                  srv,
                                                                                  UIThreadExecutor.CurrentExecutor,
                                                                                  errorHandler,
                                                                                  this.View,
                                                                                  settingsProvider,
                                                                                  GetServiceProxy<IDeclarationsDataService>(),
                                                                                  _model.ComparisonDataVersion);

                    _decisionEditPresenter.ClosedDocument += DecisionDiscrepancyClosedDocument;

                    _decisionEditPresenter.Init(IsDecisionEditFullAccess());
                }
                else
                    throw new Exception("Error in card initialization workflow");
            }
            else
                _decisionEditPresenter.Activate();
        }

        private void DecisionDiscrepancyClosedDocument(object sender, EventArgs e)
        {
            _model.DecisionIsClosed = true;

            View.UpdateOpenActSelectionButton();
            View.UpdateOpenDecisionSelectionButton();
            RefreshEKP();

            _decisionEditPresenter.RefreshData();
        }

        public void RefreshTabActData()
        {
            if (_actEditPresenter != null)
                _actEditPresenter.RefreshData();
        }

        public void RefreshTabDecisionData()
        {
            if (_decisionEditPresenter != null)
                _decisionEditPresenter.RefreshData();
        }


        #endregion

        public override void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            StopWorkers();
        }


    }
}