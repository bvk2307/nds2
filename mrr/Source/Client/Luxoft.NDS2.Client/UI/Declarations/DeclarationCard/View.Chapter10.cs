﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System.Drawing;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinCalcManager;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public partial class View
    {
        #region Пока используются старые колонки для выгрузки в эксель

        public List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> GetColumnsChapter10()
        {
            GridSetupHelper<Invoice> helper = new GridSetupHelper<Invoice>();

            ColumnDefinition c00 = helper.CreateColumnDefinition(o => o.IS_CHANGED, GetDrawPenAction());
            c00.RowSpan = 2;
            c00.AllowRowFiltering = false;
            c00.WidthColumnExcel = 5;

            var c00a = helper.CreateColumnDefinition(o => o.CONTRACTOR_DECL_IN_MC, d => { d.Caption = "К"; d.ToolTip = "Котрагент подал декларацию / журнал учета"; d.RowSpan = 2; d.WidthColumnExcel = 8; });
            var c00b = helper.CreateColumnDefinition(o => o.IS_MATCHING_ROW, d => { d.Caption = "С"; d.ToolTip = "Запись сопоставлена"; d.RowSpan = 2; d.WidthColumnExcel = 8; });

            ColumnDefinition c01 = helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => d.RowSpan = 2);
            ColumnDefinition c02 = helper.CreateColumnDefinition(o => o.CREATE_DATE, d => d.RowSpan = 2);
            ColumnDefinition c03 = helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => d.RowSpan = 2);

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер счета-фактуры"; }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WidthDate; d.ToolTip = "Дата счета-фактуры"; }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер исправления счета-фактуры"; }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = WidthDate; d.ToolTip = "Дата исправления счета-фактуры"; }));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер корректировочного счета-фактуры"; }));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = WidthDate; d.ToolTip = "Дата корректировочного счета-фактуры"; }));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер исправления корректировочного счета-фактуры"; }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = WidthDate; d.Caption = "ИКСФ: Дата\n\r(стр. 100)"; d.ToolTip = "Дата исправления корректировочного счета-фактуры"; }));

            ColumnGroupDefinition g5 = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Width = WidthInn; d.Caption = "ИНН\n\r(стр. 110)"; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Width = WidthKpp; d.Caption = "КПП\n\r(стр. 110)"; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_NAME, d => { d.Width = WidthName; 
                                                                                    d.SortIndicator = false;
                                                                                    d.AllowRowFiltering = false; d.WidthColumnExcel = 20;
            }));


            ColumnGroupDefinition gSellerAgencyInfo = new ColumnGroupDefinition() { Caption = "Сведения о посреднической деятельности продавца" };
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_INN, d => { d.Caption = "ИНН\n\r(стр. 120)"; }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_KPP, d => { d.Caption = "КПП\n\r(стр. 120)"; }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_NAME, d => { d.Caption = "Наименование"; }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_NUM, d => { d.Caption = "№ СФ\n\r(стр. 130)"; }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_DATE, d => { d.Caption = "Дата СФ\n\r(стр. 140)"; }));

            ColumnDefinition c70 = helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.RowSpan = 2; d.Caption = "Код валюты\n\r(стр. 150)"; });
            ColumnDefinition c71 = helper.CreateColumnDefinition(o => o.PRICE_TOTAL, d => { d.Caption = "Стоимость товаров\n\r(стр. 160)"; d.RowSpan = 2; d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; });
            ColumnDefinition c72 = helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, d => { d.Caption = "Сумма НДС\n\r(стр. 170)"; d.RowSpan = 2; d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; });

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Разница стоимости с НДС по КСФ" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, d => { d.Caption = "уменьшение\n\r(стр. 180)"; d.Width = WidthMoney; d.FormatInfo = formatPriceShowZero; d.Align = ColumnAlign.Right; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, d => { d.Caption = "увеличение\n\r(стр. 190)"; d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            ColumnGroupDefinition g9 = new ColumnGroupDefinition() { Caption = "Разница НДС по КСФ" };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, d => { d.Caption = "уменьшение\n\r(стр. 200)"; d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, d => { d.Caption = "увеличение\n\r(стр. 210)"; d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            if (_model.Type == DeclarationTypeCode.Declaration)
            {
                ColumnGroupDefinition g6 = new ColumnGroupDefinition() { Caption = "Сведения о продавце" };
                g6.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_INN, d => d.Width = WidthInn));
                g6.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_KPP, d => d.Width = WidthKpp));
                g6.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_NAME, d => { d.Width = WidthName; 
                                                                                    d.SortIndicator = false;
                                                                                    d.AllowRowFiltering = false; d.WidthColumnExcel = 20;
                }));

                return new List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> { c00, c00a, c00b, c01, c02, c03, g1, g2, g3, g4, g5, g6, gSellerAgencyInfo, 
                                          c70, c71, c72, 
                                          g8, g9 };
            }   
            else
                return new List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> { c00, c00a, c00b, c01, c02, c03, g1, g2, g3, g4, g5, gSellerAgencyInfo, 
                                          c70, c71, c72, 
                                          g8, g9 };
        }

        # endregion

        private void RefreshGridChapter10()
        {
            cmiR10OpenDecl.Enabled = _model.IsOpenDeclAccessible();

            //TODO: Необходимо найти альтернативный способ скрывать/показывать отдельные колонки в уже проинициализированном гриде. Так же необходимо учесть сохранение пользовательских настроек грида.
            //dataGridR10.ClearColumns();
            //dataGridR10.InitColumns(SetupGridChapter10());
            //_presenter.InitializeGridChapter10(dataGridR10, _pagerChapter10);
        }

        private GridColumnSetup SetupGridChapter10()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InvoiceModel>(dataGridR10);
            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            var priznakIcons = new Dictionary<int?, Bitmap>
            {
                { INVOICE_IS_PROCESSED, Properties.Resources.invoice_confirmed_16 },
                { INVOICE_IS_CHANGED, Properties.Resources.invoice_modified_16 },
            };

            setup.Columns.Add(helper.CreateImageColumn(o => o.IS_CHANGED, this, priznakIcons, 4)
                .Configure(d =>
                {
                    d.Caption = String.Empty;
                    d.HeaderToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
                    d.DisableFilter = true; 
                    d.ToolTipViewer = _explainToolTipViewer;
                }));

            #region Флаг К и С

            if (_model.IsChaptersGridIsMatchingColumnVisible())
            {
                var dictionaryBooleanIsMatching = new DictionaryBoolean();
                var optionsBuilderIsMatching = new BooleanAutoCompleteOptionsBuilder(dictionaryBooleanIsMatching);

                setup.Columns.Add(helper.CreateBoolColumn(o => o.CONTRACTOR_DECL_IN_MC, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "К";
                    d.HeaderToolTip = "Котрагент подал декларацию / журнал учета";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.DisableEdit = true;
                    d.MinWidth = WIDTH_CONTRACTOR_DECL_IN_MC;
                    d.Width = WIDTH_CONTRACTOR_DECL_IN_MC;
                }));
                setup.Columns.Add(helper.CreateBoolColumn(o => o.IS_MATCHING_ROW, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "С";
                    d.HeaderToolTip = "Запись сопоставлена";
                    d.MinWidth = WIDTH_IS_MATCHING_ROW;
                    d.Width = WIDTH_IS_MATCHING_ROW;
                }));
            }

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.ORDINAL_NUMBER, 4)
                .Configure(d => { d.Caption = "№\n\r(стр. 005)"; d.HeaderToolTip = "Порядковый номер"; })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(helper.CreateTextColumn(o => o.CREATE_DATE, 4)
                .Configure(d => { d.Caption = "Дата выставления"; d.HeaderToolTip = "Дата выставления"; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(helper.CreateTextColumn(o => o.OPERATION_CODE, 4)
                .Configure(d => { d.Caption = "Код вида операции\n\r(стр. 010)"; d.HeaderToolTip = "Код вида операции"; })
                .SetFilterOperatorForOpearationCode());

            #region СФ

            var group = helper.CreateGroup("СФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 020)"; d.HeaderToolTip = "Номер счета-фактуры"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 030)"; d.HeaderToolTip = "Дата счета-фактуры"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region ИСФ

            group = helper.CreateGroup("ИСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 040)"; d.HeaderToolTip = "Номер исправления счета-фактуры"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 050)"; d.HeaderToolTip = "Дата исправления счета-фактуры"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region КСФ

            group = helper.CreateGroup("КСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 060)"; d.HeaderToolTip = "Номер корректировочного счета-фактуры"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 070)"; d.HeaderToolTip = "Дата корректировочного счета-фактуры"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region ИКСФ

            group = helper.CreateGroup("ИКСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 080)"; d.HeaderToolTip = "Номер исправления корректировочного счета-фактуры"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "ИКСФ: Дата\n\r(стр. 100)"; d.HeaderToolTip = "Дата исправления корректировочного счета-фактуры"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region Сведения о покупателе

            group = helper.CreateGroup("Сведения о покупателе");
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 110)"; d.HeaderToolTip = "ИНН покупателя"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 110)"; d.HeaderToolTip = "КПП покупателя"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование покупателя";
                    d.MinWidth = WidthName;
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            setup.Columns.Add(group);

            #endregion

            if (_model.Type == DeclarationTypeCode.Declaration)
            {
                #region Сведения о продавце

                var groupSeller = helper.CreateGroup("Сведения о продавце");
                groupSeller.Columns.Add(helper.CreateTextColumn(o => o.SELLER_INN, 3)
                    .Configure(d => { d.Caption = "ИНН\n\r(стр. 130)"; d.HeaderToolTip = "ИНН продавца"; d.MinWidth = WidthInn; }));
                groupSeller.Columns.Add(helper.CreateTextColumn(o => o.SELLER_KPP, 3)
                    .Configure(d => { d.Caption = "КПП\n\r(стр. 130)"; d.HeaderToolTip = "КПП продавца"; d.MinWidth = WidthKpp; }));
                groupSeller.Columns.Add(helper.CreateTextColumn(o => o.SELLER_NAME, 3)
                    .Configure(d =>
                    {
                        d.Caption = "Наименование";
                        d.HeaderToolTip = "Наименование продавца";
                        d.MinWidth = WidthName;
                        d.DisableFilter = true;
                        d.DisableSort = true;
                    }));
                setup.Columns.Add(groupSeller);

                #endregion
            }

            #region Сведения о посреднической деятельности продавца

            group = helper.CreateGroup("Сведения о посреднической деятельности продавца");
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_AGENCY_INFO_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 120)"; d.HeaderToolTip = "ИНН посредника (комиссионера, агента)"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_AGENCY_INFO_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 120)"; d.HeaderToolTip = "КПП посредника (комиссионера, агента)"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_AGENCY_INFO_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование"; d.HeaderToolTip = "КПП посредника (комиссионера, агента)"; d.MinWidth = WidthKpp; 
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_AGENCY_INFO_NUM, 3)
                .Configure(d => { d.Caption = "№ СФ\n\r(стр. 130)"; d.HeaderToolTip = "КПП посредника (комиссионера, агента)"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_AGENCY_INFO_DATE, 3)
                .Configure(d => { d.Caption = "Дата СФ\n\r(стр. 140)"; d.HeaderToolTip = "КПП посредника (комиссионера, агента)"; d.MinWidth = WidthKpp; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.OKV_CODE, 4)
                .Configure(d =>
                {
                    d.Caption = "Код валюты\n\r(стр. 150)";
                    d.HeaderToolTip = "Код валюты по ОКВ";
                    d.MinWidth = 100;
                })
                .SetFilterOperatorForDigit());

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_TOTAL, 4)
                .Configure(d =>
                {
                    d.Caption = "Стоимость товаров\n\r(стр. 160)";
                    d.HeaderToolTip = "Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре - всего";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_NDS_TOTAL, 4)
                .Configure(d =>
                {
                    d.Caption = "Сумма НДС\n\r(стр. 170)";
                    d.HeaderToolTip = "В том числе сумма НДС по счету-фактуре";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            #region Разница стоимости с НДС по КСФ

            group = helper.CreateGroup("Разница стоимости с НДС по КСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.DIFF_CORRECT_NDS_DECREASE, 3)
                .Configure(d =>
                {
                    d.Caption = "уменьшение\n\r(стр. 180)";
                    d.HeaderToolTip = "Разница стоимости с учетом налога по корректировочному счету-фактуре - уменьшение";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPriceShowZero;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.DIFF_CORRECT_NDS_INCREASE, 3)
                .Configure(d =>
                {
                    d.Caption = "увеличение\n\r(стр. 190)";
                    d.HeaderToolTip = "Разница стоимости с учетом налога по корректировочному счету-фактуре - увеличение";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(group);

            #endregion

            #region Разница стоимости с НДС по КСФ

            group = helper.CreateGroup("Разница стоимости с НДС по КСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.DIFF_CORRECT_DECREASE, 3)
                .Configure(d =>
                {
                    d.Caption = "уменьшение\n\r(стр. 200)";
                    d.HeaderToolTip = "Разница налога по корректировочному счету-фактуре – уменьшение";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPriceShowZero;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.DIFF_CORRECT_INCREASE, 3)
                .Configure(d =>
                {
                    d.Caption = "увеличение\n\r(стр. 210)";
                    d.HeaderToolTip = "Разница налога по корректировочному счету-фактуре - увеличение";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.ROW_KEY, 4)
                .Configure(d =>
                {
                    d.Caption = "ROW_KEY";
                    d.Hidden = true;
                    d.AllowToggleVisibility = false;
                }));

            return setup;
        }

        private void InitializeGridChapter10()
        {
            dataGridR10
                 .WithPager(new ExtendedDataGridPageNavigator(_pagerChapter10))
                 .InitColumns(SetupGridChapter10());
            dataGridR10.Grid.CalcManager = new UltraCalcManager(dataGridR10.Grid.Container); ;
            dataGridR10.RegisterTableAddin(new ExpansionIndicatorRemover());
            dataGridR10.SelectRow += (sender, args) =>
            {
                var citm = dataGridR10.GetCurrentItem<InvoiceModel>();
                _presenter.RefreshEKP();
            };
            dataGridR10.AfterDataSourceChanged += dataGridR10_AfterDataSourceChanged;
            SetupSummaryRow(dataGridR10);
            _presenter.InitializeGridChapter10(dataGridR10, _pagerChapter10);
        }

        void dataGridR10_AfterDataSourceChanged()
        {
            if (dataGridR10.Grid.DisplayLayout.UIElement.GetDescendant(typeof(RowScrollbarUIElement)) != null)
            {
                dataGridR10.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Default;
            }
            else
            {
                dataGridR10.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Bottom;
            }            
        }

        private readonly PagerStateViewModel _pagerChapter10 = new PagerStateViewModel();

        private void R10ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            var currentItem = dataGridR10.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
            {
                cmiR10OpenBuyer.Visible = !string.IsNullOrEmpty(currentItem.BUYER_INN) &&
                                          !string.IsNullOrEmpty(currentItem.BUYER_KPP);

                cmiR10OpenBroker.Visible = !string.IsNullOrEmpty(currentItem.SELLER_AGENCY_INFO_INN) &&
                                          !string.IsNullOrEmpty(currentItem.SELLER_AGENCY_INFO_KPP);

                cmiR10OpenDecl.Visible = true;
            }
            else
            {
                cmiR10OpenBuyer.Visible = false;
                cmiR10OpenBroker.Visible = false;
                cmiR10OpenDecl.Visible = false;
            }

        }

        private void cmiR10OpenBuyer_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR10.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppOriginal(currentItem.BUYER_INN, currentItem.BUYER_KPP, Direction.Sales);
        }

        private void cmiR10OpenBroker_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR10.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppOriginal(currentItem.SELLER_AGENCY_INFO_INN, currentItem.SELLER_AGENCY_INFO_KPP, Direction.Sales);
        }

        private void cmiR10OpenDecl_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR10.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewDeclarationDetailsFromInvoice(currentItem);
        }
    }
}
