﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators
{
    public struct ValidationResult
    {
        public bool IsValid;
        public string Message;
    }
}
