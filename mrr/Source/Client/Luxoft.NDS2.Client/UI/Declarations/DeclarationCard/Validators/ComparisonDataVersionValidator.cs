﻿using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators
{
    /// <summary>
    /// Проверка актуальности версии данных сопоставления
    /// </summary>
    public class ComparisonDataVersionValidator
    {
        private const int _timeOut = 10;
        private readonly IDeclarationsDataService _declarationsDataService;
        private readonly IServiceRequestWrapper _serviceRequester;

        private DateTime _timer;
        private bool _lastValidateValue;
        private long _lastComparasionDataVersion;
        private bool _lastComparasionDataVersionChanged;

        public ComparisonDataVersionValidator(
            IDeclarationsDataService declarationsDataService,
            IServiceRequestWrapper serviceRequester,
            long comparasionDataVersion)
        {
            _timer = DateTime.Now;
            _lastValidateValue = true;
            _lastComparasionDataVersion = comparasionDataVersion;
            _lastComparasionDataVersionChanged = false;

            _declarationsDataService = declarationsDataService;
            _serviceRequester = serviceRequester;
        }
        
        public long ComparasionDataVersion
        {
            get { return _lastComparasionDataVersion; }
            set
            {
                if (_lastComparasionDataVersion != value)
                {
                    _lastComparasionDataVersion = value;
                    _lastComparasionDataVersionChanged = true;
                }
            }
        }


        /// <summary>
        /// Вызов проверки с кэшированием
        /// </summary>
        /// <returns>
        /// IsValid:
        /// true - данные сопоставления актуальны
        /// false - данные сопоставления устарели
        /// 
        /// Message: сообщение для пользователей
        /// </returns>
        public ValidationResult Validate()
        {
            var result = _lastValidateValue;

            var currentTime = DateTime.Now;
            var diffInSeconds = currentTime.Subtract(_timer).TotalSeconds;

            if ((diffInSeconds >= _timeOut) || _lastComparasionDataVersionChanged)
            {
                _serviceRequester.Execute(
                    () => _declarationsDataService.VerifyComparasionDataVersionActuality(_lastComparasionDataVersion),
                    (r) => { result = r.Result; });

                _timer = currentTime;
                _lastValidateValue = result;
                _lastComparasionDataVersionChanged = false;
            }

            return new ValidationResult()
            {
                IsValid = result,
                Message = result ? String.Empty : ResourceManagerNDS2.ComparasionProcessMessages.Changed
            };
        }
    }
}
