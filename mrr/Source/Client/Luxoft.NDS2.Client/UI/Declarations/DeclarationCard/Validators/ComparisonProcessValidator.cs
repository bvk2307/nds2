﻿using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators
{
    /// <summary>
    /// Проверка блокировки операций с расхождениями
    /// по причине работы процесса сопоставления
    /// </summary>
    public class ComparisonProcessValidator
    {
        private readonly SystemSupportInProcessValidator _supportInProcessValidator;
        private readonly ComparisonDataVersionValidator _comparisonDataVersionValidator;

        public ComparisonProcessValidator(
            IDeclarationsDataService declarationsDataService,
            IServiceRequestWrapper serviceRequester,
            long comparasionDataVersion)
        {
            _supportInProcessValidator = new SystemSupportInProcessValidator(
                declarationsDataService, 
                serviceRequester);

            _comparisonDataVersionValidator = new ComparisonDataVersionValidator(
                declarationsDataService, 
                serviceRequester, 
                comparasionDataVersion);
        }
        
        public long ComparasionDataVersion
        {
            get { return _comparisonDataVersionValidator.ComparasionDataVersion; }
            set { _comparisonDataVersionValidator.ComparasionDataVersion = value; }
        }

        /// <summary>
        /// Вызов проверки
        /// </summary>
        /// <returns>
        /// IsValid:
        /// true - проверка пройдена успешно, с расхождениями можно работать
        /// false - проверка непройдена, текст причины в сообщении для пользователей
        /// 
        /// Message: сообщение для пользователей
        /// </returns>
        public ValidationResult Validate()
        {
            var ret = _supportInProcessValidator.Validate();

            if (ret.IsValid)
                ret = _comparisonDataVersionValidator.Validate();

            return ret;
        }
    }
}
