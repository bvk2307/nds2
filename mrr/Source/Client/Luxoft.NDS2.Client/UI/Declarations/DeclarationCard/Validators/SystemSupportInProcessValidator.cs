﻿using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators
{
    /// <summary>
    /// Проверка проведения регламентных работ в системе
    /// (например идет выгрузка результатов сопоставления данных)
    /// </summary>
    public class SystemSupportInProcessValidator
    {
        private const int _timeOut = 10;
        private readonly IDeclarationsDataService _declarationsDataService;
        private readonly IServiceRequestWrapper _serviceRequester;

        private DateTime _timer;
        private bool? _lastValidateValue;

        public SystemSupportInProcessValidator(
            IDeclarationsDataService declarationsDataService,
            IServiceRequestWrapper serviceRequester)
        {
            _timer = DateTime.Now;
            _lastValidateValue = null;

            _declarationsDataService = declarationsDataService;
            _serviceRequester = serviceRequester;
        }

        /// <summary>
        /// Вызов проверки с кэшированием
        /// </summary>
        /// <returns>
        /// IsValid:
        /// true - регламентные работы не проводяться
        /// false - проводятся регламентные работы
        /// 
        /// Message: сообщение для пользователей
        /// </returns>
        public ValidationResult Validate()
        {
            var result = true;

            var currentTime = DateTime.Now;
            var diffInSeconds = currentTime.Subtract(_timer).TotalSeconds;

            if (diffInSeconds >= _timeOut || !_lastValidateValue.HasValue)
            {
                _serviceRequester.Execute(
                    () => _declarationsDataService.VerifySystemSupportInProcess(),
                    (r) => { result = r.Result; });

                _timer = currentTime;
                _lastValidateValue = result;
            }
            else
                result = _lastValidateValue.Value;

            return new ValidationResult()
            {
                IsValid = result,
                Message = result ? String.Empty : ResourceManagerNDS2.ComparasionProcessMessages.InProcess
            };
        }
    }
}
