﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    /// <summary>
    ///     Класс реализует загрузку доступных пользователю операций
    /// </summary>
    class UserOperationListDataLoader : ServiceProxyBase<List<string>>
    {
        private readonly IDiscrepancyForDeclarationCardService _service;

        public UserOperationListDataLoader(
            IDiscrepancyForDeclarationCardService service,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }
        
        public List<string> LoadUserOperations()
        {
            List<string> result;
            
            if (!Invoke(() => _service.GetUserOperations(), out result))
            {
                _notifier.ShowError(ResourceManagerNDS2.DeclarationMessages.UserOperationsLoadFailed);
            }

            return result ?? new List<string>();
        }
            
    }
}
