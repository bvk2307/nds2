﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Popups
{
    public partial class NumWarning : Form
    {
        public NumWarning(string message)
        {
            InitializeComponent();

            LabelMessage.Text = message;
        }

        private void BtnSubmitClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
