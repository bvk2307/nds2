﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public class UserAccessOpearation
    {
        public UserAccessOpearation(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        private readonly ISecurityService _securityService;

        private Dictionary<string, UserAccessPolicy> _cache = new Dictionary<string, UserAccessPolicy>();

        private UserAccessPolicy GetUserAccess(string operation)
        {
            if (_cache.ContainsKey(operation))
            {
                return _cache[operation];
            }
            else
            {
                var result = _securityService.GetUserAccess(operation);
                return result.Result;
            }
        }

        private IEnumerable<string> _operationsAllowed = new string[0];
        private IEnumerable<string> _operationsAsked = new string[0];

        private bool MrrOperationAllowed(params string[] operation)
        {
            if (!_operationsAsked.AsEnumerable().ArrayEquals(operation))
            {
                _operationsAsked = operation;
                _operationsAllowed = LoadOperations();
            }
            return _operationsAllowed.Intersect(_operationsAsked).Any();
        }

        private IEnumerable<string> LoadOperations()
        {
            try
            {
                var result =
                    _securityService.ValidateAccess(_operationsAsked.ToArray());
                return result.Status == ResultStatus.Success
                    ? result.Result
                    : new string[0];
            }
            catch
            {
                return new string[0];
            }
        }

        private bool AllowOperationWithoutRestriction(string operation)
        {
            var userAccess = GetUserAccess(operation);

            bool ret = false;
            if (userAccess != null)
            {
                if (userAccess.FullAccess)
                    ret = true;
                else
                {
                    ret = userAccess.AllowedInspections.Any();
                }
            }
            return ret;
        }

        private bool AllowOperation(string operation, List<string> sonoCodes)
        {
            var userAccess = GetUserAccess(operation);

            bool ret = false;
            if (userAccess != null)
            {
                if (userAccess.FullAccess)
                    ret = true;
                else
                {
                    foreach (var item in userAccess.AllowedInspections)
                    {
                        if (sonoCodes.Contains(item))
                        {
                            ret = true;
                            break;
                        }
                    }
                }
            }
            return ret;
        }

        public bool AllowOpearationDeclarationChapterExportExcel()
        {
            return MrrOperationAllowed(MrrOperations.Declaration.DeclarationReport);
        }

        public bool AllowOpearationExplainEdit(string operation, string sonoCode)
        {
            return AllowOperation(operation, new List<string>() { { sonoCode } });
        }

        public bool AllowOpearationExplainView()
        {
            return MrrOperationAllowed(MrrOperations.ExplainReply.ExplainView);
        }

        public bool AllowOpearationExplainEdit()
        {
            return MrrOperationAllowed(MrrOperations.ExplainReply.ExplainEdit);
        }

        public bool AllowOperationTreeRelation(string sonoCode)
        {
            var userAccess = GetUserAccess(Constants.SystemPermissions.Operations.DeclarationTreeRelation);

            if (userAccess == null)
            {
                return false;
            }

            return userAccess.FullAccess
                || userAccess.AllowedInspections.Contains(sonoCode);
        }
    }
}
