﻿using System.Linq;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.GridColumns
{
    class CustomDictionaryFilterManager : IColumnFilterManager
    {
        private readonly IDictionary<object, string> _filterValues;

        public CustomDictionaryFilterManager(IDictionary<object, string> filterValues)
        {
            _filterValues = filterValues;
        }

        public void AutoComplete(ValueList initialList)
        {
            initialList.Skip(1);

            var itemsForappend = _filterValues.Select(item => new ValueListItem(item.Key, item.Value)).ToArray();

            initialList.ValueListItems.AddRange(itemsForappend);
        }


        public void OnFilterChanged(ColumnFilter gridFilter)
        {
        }
    }
}
