﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Helpers.EventArguments;
using Luxoft.NDS2.Client.Model.Bargain;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using ColumnFilter = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public partial class View : BaseView
    {
        private DeclarationCardPresenter _presenter;

        private int WidthInvoice = 40;

        private int WidthDate = 40;

        private int WidthName = 300;

        private int WidthInn = 30;

        private int WidthKpp = 30;

        private int WidthMoney = 100;

        private const int WIDTH_IS_MATCHING_ROW = 40;

        private const int WIDTH_CONTRACTOR_DECL_IN_MC = 25;

        private const int INVOICE_IS_PROCESSED = (int)InvoiceExplainState.Processed;

        private const int INVOICE_IS_CHANGED = (int)InvoiceExplainState.Changing;

        public readonly Dictionary<DeclarationPart, ChapterData> PartGrids;
        public readonly Dictionary<DeclarationPart, Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView> PartGridsPro;

        public readonly Dictionary<int, DeclarationPart> CardTabs;

        private Dictionary<int, QueryConditions> chaptersGridQueryConditions =
            new Dictionary<int, QueryConditions>();

        private Dictionary<int, Dictionary<string, FilterCondition>> chaptersGridFilter =
            new Dictionary<int, Dictionary<string, FilterCondition>>();

        private UcRibbonButtonToolContext btnOpen;
        private UcRibbonButtonToolContext btnActSelectionCard;
        private UcRibbonButtonToolContext btnDecisionSelectionCard;

        IntValueZeroFormatter formatPrices = new IntValueZeroFormatter();
        PriceShowZeroFormatter formatPriceShowZero = new PriceShowZeroFormatter();

        private readonly DeclarationModel _model;

        private readonly IToolTipViewer _explainToolTipViewer = new ExplainToolTipViewer();

        public event EventHandler<EventArgsPrimitiveValue<bool>> OnCommonInfoExpandStateChanged;

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext context, WorkItem wi, DeclarationModel model)
            : base(context, wi)
        {
            _model = model;
            InitializeComponent();

            CardTabs = new Dictionary<int, DeclarationPart>
            {
                {tabR8.Tab.VisibleIndex, DeclarationPart.R8}, 
                {tabR9.Tab.VisibleIndex, DeclarationPart.R9}, 
                {tabR10.Tab.VisibleIndex, DeclarationPart.R10}, 
                {tabR11.Tab.VisibleIndex, DeclarationPart.R11}, 
                {tabR12.Tab.VisibleIndex, DeclarationPart.R12},
                {tabDiscrepancies.Tab.VisibleIndex, DeclarationPart.Discrepancy},
                {tabControlRatio.Tab.VisibleIndex, DeclarationPart.KC},
                {tabDOC.Tab.VisibleIndex, DeclarationPart.DOCKNP}
            };
            //TODO. vtsyk

            PartGrids = new Dictionary<DeclarationPart, ChapterData>
            {
                {DeclarationPart.Discrepancy, new ChapterData() { GridControl = gridDiscrepancies }},
                {DeclarationPart.KC, new ChapterData() { GridControl = gridControlRatio }},
                {DeclarationPart.DOCKNP, new ChapterData() { GridControl = null }}
            };

            PartGridsPro = new Dictionary<DeclarationPart, Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView>
            {
                {DeclarationPart.R8, dataGridR8 },
                {DeclarationPart.R9, dataGridR9 },
                {DeclarationPart.R10, dataGridR10 },
                {DeclarationPart.R11, dataGridR11 },
                {DeclarationPart.R12, dataGridR12 }
            };

            DeclarationParts.SelectedTabChanged += new SelectedTabChangedEventHandler(this.ActTabSelected);
            DeclarationParts.SelectedTabChanged += new SelectedTabChangedEventHandler(this.DecisionTabSelected);

            actDiscrepancyEditView.MessageView = this;
            decisionDiscrepancyEditView.MessageView = this;
        }

        public IKnpDocumentListView KnpDocumentsView()
        {
            return _knpDocumentGrid;
        }

        public void AcceptPresenter(DeclarationCardPresenter pr)
        {
            _presenter = pr;
            Init();
            ShowDeclaration();
            if (_model.InitialChapter.IsSet)
            {
                var tabIndex =
                    CardTabs.FirstOrDefault(
                        tab => tab.Value == _model.InitialChapter.KeyDeclarationPart).Key;
                DeclarationParts.SelectedTab = DeclarationParts.Tabs[tabIndex];
            }
        }

        private void Init()
        {
            CreateControlRNIVC();
            InitializeRibbon();
            tabControlRatio.Tab.Visible = _presenter.IsUserEligibleForOperation(Constants.SystemPermissions.Operations.DeclarationCardViewClaims);

            if (_model.InitialChapter.IsSet)
            {
                Dictionary<string, FilterCondition> filterConditions = new Dictionary<string, FilterCondition>();

                QueryConditions qc = new QueryConditions();

                if (!String.IsNullOrEmpty(_model.InitialChapter.InvoiceKey))
                {
                    SearchCriteriaHelper.AddFilterOfAny(qc,
                        TypeHelper<Invoice>.GetMemberName(t => t.ROW_KEY),
                        _model.InitialChapter.InvoiceKey,
                        ColumnFilter.FilterComparisionOperator.Equals);

                    filterConditions.Add(
                        TypeHelper<Invoice>.GetMemberName(t => t.ROW_KEY),
                        new FilterCondition
                        {
                            CompareValue = _model.InitialChapter.InvoiceKey,
                            ComparisionOperator = FilterComparisionOperator.Equals
                        });
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(_model.InitialChapter.InvoiceNumber))
                    {
                        SearchCriteriaHelper.AddFilterOfAny(qc,
                            TypeHelper<Invoice>.GetMemberName(t => t.INVOICE_NUM),
                            _model.InitialChapter.InvoiceNumber,
                            ColumnFilter.FilterComparisionOperator.Equals);

                        filterConditions.Add(
                            TypeHelper<Invoice>.GetMemberName(t => t.INVOICE_NUM),
                            new FilterCondition
                            {
                                CompareValue = _model.InitialChapter.InvoiceNumber,
                                ComparisionOperator = FilterComparisionOperator.Equals
                            });
                    }

                    if (!string.IsNullOrWhiteSpace(_model.InitialChapter.ContractorInn))
                    {
                        var chaptersFiltrFieldName =
                            new Dictionary<DeclarationPart, string>
                        {
                            { DeclarationPart.R8,  TypeHelper<Invoice>.GetMemberName(t=>t.SELLER_INN)},
                            { DeclarationPart.R9,  TypeHelper<Invoice>.GetMemberName(t=>t.BUYER_INN)},
                            { DeclarationPart.R10, TypeHelper<Invoice>.GetMemberName(t=>t.BUYER_INN)},
                            { DeclarationPart.R11, TypeHelper<Invoice>.GetMemberName(t=>t.SELLER_INN)},
                            { DeclarationPart.R12, TypeHelper<Invoice>.GetMemberName(t=>t.BUYER_INN)}
                        };

                        SearchCriteriaHelper.AddFilterOfAny(qc,
                            chaptersFiltrFieldName[_model.InitialChapter.KeyDeclarationPart],
                            _model.InitialChapter.ContractorInn,
                            ColumnFilter.FilterComparisionOperator.Equals);

                        filterConditions.Add(
                           chaptersFiltrFieldName[_model.InitialChapter.KeyDeclarationPart],
                           new FilterCondition
                           {
                               CompareValue = _model.InitialChapter.ContractorInn,
                               ComparisionOperator = FilterComparisionOperator.Equals

                           });
                    }
                }

                int typeFilter = 1;

                if (typeFilter == 1)
                {
                    if (qc.Filter.Count > 0)
                        chaptersGridQueryConditions.Add((int)_model.InitialChapter.KeyDeclarationPart, qc);
                }
                else if (typeFilter == 2)
                {
                    if (filterConditions.Count() > 0)
                        chaptersGridFilter.Add((int)_model.InitialChapter.KeyDeclarationPart, filterConditions);
                }
            }
            else
            {
                DeclarationParts.Tabs[0].Selected = true;
            }
            _presenter.GetChapterDataCallback = CreateSummaryRow;
            InitLabelDictionary();
            InitializeGridChapter8();
            InitializeGridChapter9();
            InitializeGridChapter10();
            InitializeGridChapter11();
            InitializeGridChapter12();
            InitializeGridDiscrepancies();
            InitializeGridClaims();
            InitializeGridControlRatio();
            ExportExcelAddEvents();
            ExportExcelSetupAcess();
        }

        # region CellToolTip

        DictionaryOper dicOper = new DictionaryOper();
        DictionaryCurr dicCurr = new DictionaryCurr();
        DictionaryBargain dicBargain = new DictionaryBargain();
        void InitDict()
        {
            dicOper = _presenter.Oper;
            dicCurr = _presenter.Curr;
            dicBargain = _presenter.Barg;
        }
        private string InvoiceConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OPERATION_CODE;
            return dicOper.NAME(opcode);
        }

        private string InvoiceCurrencyConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OKV_CODE;
            return dicCurr.NAME(opcode);
        }

        private string InvoiceBargainConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = ((int)temp.DEAL_KIND_CODE).ToString();
            return dicBargain.NAME(opcode);
        }

        private void SetCellToolTips(GridSetup setup)
        {
            InitDict();
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();
            Func<object, string> funcOper = InvoiceConvert;
            Func<object, string> funcCurr = InvoiceCurrencyConvert;
            Func<object, string> funcBarg = InvoiceBargainConvert;
            cellToolTips.Add("OPERATION_CODE", funcOper);
            cellToolTips.Add("OKV_CODE", funcCurr);
            cellToolTips.Add("DEAL_KIND_CODE", funcBarg);
            setup.CellToolTips = cellToolTips;
        }

        #endregion

        #region Базовые операции карточки

        public void ShowDeclaration()
        {
            string title = string.Empty;


            if (_model.Type == DeclarationTypeCode.Declaration)
            {
                if (!string.IsNullOrWhiteSpace(_model.RawData.KPP))
                {
                    title = string.Format("ИНН {0} / КПП {1} (НД по НДС)", _model.RawData.INN, _model.RawData.KPP);
                }
                else
                {
                    title = string.Format("ИНН {0} (НД по НДС)", _model.RawData.INN);
                }

                //AnalizeChapterData();

                tabMain.Tab.Text = "Раздел 1-7";

                tabR8.Tab.Visible = true;
                tabR8.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter8].IsDataAvailable;
                tabR8.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter8].DataStateDescription;

                tabR9.Tab.Visible = true;
                tabR9.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter9].IsDataAvailable;
                tabR9.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter9].DataStateDescription;

                tabR10.Tab.Text = "Раздел 10";
                tabR10.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter10].IsDataAvailable;
                tabR10.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter10].DataStateDescription;

                tabR11.Tab.Text = "Раздел 11";
                tabR11.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter11].IsDataAvailable;
                tabR11.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter11].DataStateDescription;

                tabR12.Tab.Visible = true;
                tabR12.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter12].IsDataAvailable;
                tabR12.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter12].DataStateDescription;

                tabControlRatio.Tab.Visible = true;
                tabDOC.Tab.Visible = true;
                captionR10.Text = "Сведения из журнала учета выставленных счетов-фактур в отношении операций, осуществляемых в интересах другого лица на основе договоров комиссии, агентских договоров или на основе договоров транспортной экспедиции, отражаемых за истекший налоговый период";
                captionR11.Text = "Сведения из журнала учета полученных счетов-фактур в отношении операций, осуществляемых в интересах другого лица на основе договоров комиссии, агентских договоров или на основе договоров транспортной экспедиции, отражаемых за истекший налоговый период";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(_model.RawData.KPP))
                {
                    title = string.Format("ИНН {0} / КПП {1} (Журнал учета)", _model.RawData.INN, _model.RawData.KPP);
                }
                else
                {
                    title = string.Format("ИНН {0} (Журнал учета)", _model.RawData.INN);
                }

                tabMain.Tab.Text = "Титульный лист";

                tabR8.Tab.Visible = false;

                tabR9.Tab.Visible = false;

                tabR10.Tab.Text = "Часть 1";
                tabR10.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter10].IsDataAvailable;
                tabR10.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter10].DataStateDescription;

                tabR11.Tab.Text = "Часть 2";
                tabR11.Tab.Enabled = _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter11].IsDataAvailable;
                tabR11.Tab.ToolTipText =
                    _model.ChapterModel[DeclarationInvoiceChapterNumber.Chapter11].DataStateDescription;

                tabR12.Tab.Visible = false;

                tabControlRatio.Tab.Visible = false;
                tabDOC.Tab.Visible = false;
                captionR10.Text = "Сведения части 1 Выставленные счета-фактуры";
                captionR11.Text = "Сведения части 2 Полученные счета-фактуры";
            }

            _presentationContext.WindowTitle = title;
            _presentationContext.WindowDescription = title;

            SetDeclarationRevisions(_model.RawData.Revisions, _model.RawData.CORRECTION_NUMBER, _model.RawData.DECLARATION_VERSION_ID);
            SetCommonInformation();
            SetRnivcData(_model.RawData.DetailData);
            ChaptersShowInfo();
            ShowDeclarationDircrepancyInformation();

            DeclarationParts.SelectedTab = DeclarationParts.Tabs[0];

            progressBar.Visible = false;

            tabDiscrepancies.Enabled = _model.IsDiscrepanciesEditAccessible();
            UpdateOpenActSelectionButton();
            UpdateOpenDecisionSelectionButton();
            UpdateActEditTab();
            UpdateDecisionEditTab();

            RefreshGridChapter8();
            RefreshGridChapter9();
            RefreshGridChapter10();
            RefreshGridChapter11();
            RefreshGridChapter12();
        }

        private void grpCommonInfo_ExpandedStateChanged(object sender, EventArgs e)
        {
            var commonInfoExpandedStateListners = OnCommonInfoExpandStateChanged;
            if (commonInfoExpandedStateListners != null)
            {
                commonInfoExpandedStateListners.Invoke(this, new EventArgsPrimitiveValue<bool>(grpCommonInfo.Expanded));
            }
        }

        public void UpdateCurrentTab()
        {
            int tabIndex = DeclarationParts.SelectedTab.Index;

            if (CardTabs.Keys.Contains(tabIndex))
            {
                _presenter.InitChapter(CardTabs[tabIndex]);
            }
        }

        public override void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            _presenter.OnBeforeClosing(closeContext, eventArgs);
        }

        public override void OnUnload()
        {
            Dispose();
            base.OnUnload();
        }

        public List<FilterQuery> GetDefaultFilter(DeclarationPart chapterGridNumber)
        {
            QueryConditions qc;
            return chaptersGridQueryConditions.TryGetValue((int)chapterGridNumber, out qc) ? qc.Filter : new List<FilterQuery>();
        }

        private void DeclarationParts_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            UpdateCurrentTab();
            UpdateOpenButton();
        }

        private void UpdateOpenButton()
        {
            if (DeclarationParts.SelectedTab.Index == tabDOC.Tab.VisibleIndex)
            {
                UpdateOpenButtonStatus();
            }
            else
            {
                btnOpen.Enabled = false;
                _presenter.RefreshEKP();
            }
        }

        #region Акты и решения

        public void UpdateOpenActSelectionButton()
        {
            btnActSelectionCard.Enabled = _presenter.IsActSelectionCardAccessible();
            btnActSelectionCard.Visible = _model.IsActSelectionCardButtonVisible();

            if (btnActSelectionCard.Visible)
            {
                if (!_presentationContext.UiVisualizationCollection.Contains(btnActSelectionCard))
                    _presentationContext.UiVisualizationCollection.Add(btnActSelectionCard);
            }
            else
            {
                if (_presentationContext.UiVisualizationCollection.Contains(btnActSelectionCard))
                    _presentationContext.UiVisualizationCollection.Remove(btnActSelectionCard);
            }
        }

        public void UpdateOpenDecisionSelectionButton()
        {
            btnDecisionSelectionCard.Enabled = _presenter.IsDecisionSelectionCardAccessible();
            btnDecisionSelectionCard.Visible = _model.IsDecisionSelectionCardButtonVisible();

            if (btnDecisionSelectionCard.Visible)
            {
                if (!_presentationContext.UiVisualizationCollection.Contains(btnDecisionSelectionCard))
                    _presentationContext.UiVisualizationCollection.Add(btnDecisionSelectionCard);
            }
            else
            {
                if (_presentationContext.UiVisualizationCollection.Contains(btnDecisionSelectionCard))
                    _presentationContext.UiVisualizationCollection.Remove(btnDecisionSelectionCard);
            }
        }

        public void LockViewActSelectionCard(bool val)
        {
            btnActSelectionCard.Enabled = !val;
        }

        public void LockViewDecisionSelectionCard(bool val)
        {
            btnDecisionSelectionCard.Enabled = !val;
        }

        public void UpdateActEditTab()
        {
            tabAct.Enabled = _model.IsActEditAccessible();

            if (tabAct.Enabled)
                _presenter.RefreshTabActData();
        }

        public void UpdateDecisionEditTab()
        {
            tabDecision.Enabled = _model.IsDecisionEditAccessible();

            if (tabDecision.Enabled)
                _presenter.RefreshTabDecisionData();
        }

        public IActDiscrepancyEditView ActEditView { get { return actDiscrepancyEditView; } }

        public IDecisionDiscrepancyEditView DecisionEditView { get { return decisionDiscrepancyEditView; } }

        private void ActTabSelected(object sender, SelectedTabChangedEventArgs e)
        {
            if (DeclarationParts.SelectedTab.TabPage.Name == tabAct.Name)
                _presenter.InitAct();
        }

        private void DecisionTabSelected(object sender, SelectedTabChangedEventArgs e)
        {
            if (DeclarationParts.SelectedTab.TabPage.Name == tabDecision.Name)
                _presenter.InitDecision();
        }

        #endregion

        private Action<ColumnDefinition> GetDrawPenAction()
        {
            Action<ColumnDefinition> drawPen = column =>
            {
                DictionaryInvoiceStateRecord drw = new DictionaryInvoiceStateRecord()
                {
                    InvoiceState =
                        ExplainInvoiceState.
                        ChangingInThisExplain,
                    ColorARGB = 0,
                    Description = ""
                };

                column.Caption = "";
                column.ToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
                column.CustomConditionValueAppearance = () =>
                {
                    var result = new ConditionValueAppearance();
                    var condition = new OperatorCondition(ConditionOperator.Equals, 1);
                    var appearance = new Infragistics.Win.Appearance
                    {
                        Image = drw.GetIcon(this),
                        ImageHAlign = HAlign.Center,
                        ImageVAlign = VAlign.Middle,
                        ForegroundAlpha = Alpha.Transparent
                    };

                    result.Add(condition, appearance);

                    return result;
                };
            };
            return drawPen;
        }

        #endregion

        #region Риббон

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2Result")
            {
                Text = "Функции",
                ToolTipText = "Функции",
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnReportTreeRelations = new UcRibbonButtonToolContext(_presentationContext, "btn" + "ReportTreeRelations", "cmdDeclarationCard" + "ReportTreeRelations")
            {
                Text = "Дерево связей",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "tree",
                SmallImageName = "tree",
                Enabled = _presenter.IsTreeRelationEligible()
            };

            btnOpen = new UcRibbonButtonToolContext(_presentationContext, "btn" + "Open", "cmdDeclarationCard" + "Open")
            {
                Text = "Открыть",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            var btnUpdate = new UcRibbonButtonToolContext(_presentationContext, "btn" + "Update", "cmdDeclarationCard" + "Update")
            {
                Text = "Обновить",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            var btnTaxPayerCard = new UcRibbonButtonToolContext(_presentationContext, "btn" + "TaxPayerCard", "cmdDeclarationCard" + "TaxPayerCard")
            {
                Text = "Карточка НП",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = _presenter.IsUserEligibleForOperation(Constants.SystemPermissions.Operations.TaxPayerViewCard)
            };

            btnActSelectionCard = new UcRibbonButtonToolContext(_presentationContext, "btn" + "ActSelectionCard", "cmdDeclarationCard" + "ActSelectionCard")
            {
                Text = "Учет расхождений в акте",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "selector",
                SmallImageName = "selector",
                Enabled = _presenter.IsActSelectionCardAccessible(),
                Visible = _model.IsActSelectionCardButtonVisible()
            };

            btnDecisionSelectionCard = new UcRibbonButtonToolContext(_presentationContext, "btn" + "DecisionSelectionCard", "cmdDeclarationCard" + "DecisionSelectionCard")
            {
                Text = "Учет расхождений в решении",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "selector",
                SmallImageName = "selector",
                Enabled = _presenter.IsDecisionSelectionCardAccessible(),
                Visible = _model.IsDecisionSelectionCardButtonVisible()
            };

            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnReportTreeRelations.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnOpen.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnTaxPayerCard.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnActSelectionCard.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnDecisionSelectionCard.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnOpen, 
                btnReportTreeRelations,
                btnUpdate,
                btnTaxPayerCard,
                tabNavigator
            });

            if (btnActSelectionCard.Visible)
                _presentationContext.UiVisualizationCollection.Add(btnActSelectionCard);

            if (btnDecisionSelectionCard.Visible)
                _presentationContext.UiVisualizationCollection.Add(btnDecisionSelectionCard);

            
            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;

        }

        [CommandHandler("cmdDeclarationCardOpen")]
        public virtual void ViewBtnClick(object sender, EventArgs e)
        {
            _presenter.OpenKnpDocumentDetailsCard((KnpDocument)KnpDocumentsView().SelectedItem);
        }

        [CommandHandler("cmdDeclarationCard" + "ReportTreeRelations")]
        public virtual void ReportTreeRelationsBtnClick(object sender, EventArgs e)
        {
            _presenter.ViewPyramidReport();
        }

        [CommandHandler("cmdDeclarationCardUpdate")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            _presenter.IsNeedKnpDocumentsRefresh = true;
            UpdateCurrentRevision();
        }

        [CommandHandler("cmdDeclarationCardTaxPayerCard")]
        public virtual void TaxPayerCardBtnClick(object sender, EventArgs e)
        {
            _presenter.ViewTaxPayerDetails();
        }

        [CommandHandler("cmdDeclarationCardActSelectionCard")]
        public virtual void ActSelectionCardBtnClick(object sender, EventArgs e)
        {
            _presenter.ViewActSelectionCard();
        }

        [CommandHandler("cmdDeclarationCardDecisionSelectionCard")]
        public virtual void DecisionSelectionCardBtnClick(object sender, EventArgs e)
        {
            _presenter.ViewDecisionSelectionCard();
        }

        #endregion

        #region Переход в раздел другой декларации

        private int GetChapterTabIndex(int destChapterGridNumber)
        {
            int ret = 0;
            if (CardTabs.ContainsKey(destChapterGridNumber))
            {
                ret = (int)CardTabs[destChapterGridNumber];
            }
            return ret;
        }

        private Dictionary<int, Func<ChapterDestinationParameters>> GetChapterDestinationParameters(string Inn)
        {
            Dictionary<int, Func<ChapterDestinationParameters>> chapterDests = new Dictionary<int, Func<ChapterDestinationParameters>>();
            chapterDests.Add(8, () =>
            {
                int chapterNum = _presenter.GetChapterNumberNeed_R9_R10_R12(Inn);
                return new ChapterDestinationParameters()
                {
                    ChapterGridNumber = chapterNum,
                    SelectedTabIndex = GetChapterTabIndex(chapterNum),
                    ColumnNameINN = "BUYER_INN"
                };
            });
            chapterDests.Add(9, () =>
            {
                int chapterNum = _presenter.GetChapterNumberNeed_R8_R11(Inn);
                return new ChapterDestinationParameters()
                {
                    ChapterGridNumber = chapterNum,
                    SelectedTabIndex = GetChapterTabIndex(chapterNum),
                    ColumnNameINN = "SELLER_INN"
                };
            });
            chapterDests.Add(10, () =>
            {
                int chapterNum = _presenter.GetChapterNumberNeed_R8_R11(Inn);
                return new ChapterDestinationParameters()
                {
                    ChapterGridNumber = chapterNum,
                    SelectedTabIndex = GetChapterTabIndex(chapterNum),
                    ColumnNameINN = "SELLER_INN"
                };
            });
            chapterDests.Add(11, () =>
            {
                int chapterNum = _presenter.GetChapterNumberNeed_R9_R10_R12(Inn);
                return new ChapterDestinationParameters()
                {
                    ChapterGridNumber = chapterNum,
                    SelectedTabIndex = GetChapterTabIndex(chapterNum),
                    ColumnNameINN = "BUYER_INN"
                };
            });
            chapterDests.Add(12, () =>
            {
                int chapterNum = _presenter.GetChapterNumberNeed_R8_R11(Inn);
                return new ChapterDestinationParameters()
                {
                    ChapterGridNumber = chapterNum,
                    SelectedTabIndex = GetChapterTabIndex(chapterNum),
                    ColumnNameINN = "SELLER_INN"
                };
            });

            return chapterDests;
        }

        public void InitFromDeclarationSource(DeclarationInfoSource declarationInfoSource)
        {
            if (declarationInfoSource != null)
            {
                if (declarationInfoSource.IsDestination)
                {

                    DeclarationParts.SelectedTab = DeclarationParts.Tabs[GetChapterTabIndex(declarationInfoSource.ChapterGridNumberDestination)];
                    Dictionary<string, FilterCondition> dictFilters = new Dictionary<string, FilterCondition>();
                    dictFilters.Add("INVOICE_NUM", new FilterCondition()
                    {
                        CompareValue = declarationInfoSource.InvoiceNum,
                        ComparisionOperator = FilterComparisionOperator.Equals
                    });
                    chaptersGridFilter.Clear();
                    chaptersGridFilter.Add(declarationInfoSource.ChapterGridNumberDestination, dictFilters);
                }
                else
                {
                    Dictionary<int, Func<ChapterDestinationParameters>> chapterDests = GetChapterDestinationParameters(declarationInfoSource.declarationSource.INN);
                    ChapterDestinationParameters chapterDestination = null;
                    if (chapterDests.ContainsKey(declarationInfoSource.ChapterGridNumber))
                    {
                        chapterDestination = chapterDests[declarationInfoSource.ChapterGridNumber].Invoke();
                    }
                    if (chapterDestination != null)
                    {
                        DeclarationParts.SelectedTab = DeclarationParts.Tabs[chapterDestination.SelectedTabIndex];
                        Dictionary<string, FilterCondition> dictFilters = new Dictionary<string, FilterCondition>();
                        dictFilters.Add(chapterDestination.ColumnNameINN, new FilterCondition()
                        {
                            CompareValue = declarationInfoSource.declarationSource.INN,
                            ComparisionOperator = FilterComparisionOperator.Equals
                        });
                        chaptersGridFilter.Clear();
                        chaptersGridFilter.Add(chapterDestination.ChapterGridNumber, dictFilters);
                    }
                }
            }
        }

        public int GetSelectedTabIndex()
        {
            return DeclarationParts.SelectedTab.Index;
        }


        #endregion

        #region Экспорт в Эксель

        public void ExportExcelSetupAcess()
        {
            ExportExcelSetButtonAccessAll(_presenter.AllowExportToExcel());
        }

        private void ExportExcel(DeclarationPart part)
        {
            try
            {
                saveFileDialogReport.FileName = _presenter.GenerateInvoiceExcelFileName(part);
                saveFileDialogReport.DefaultExt = _presenter.GetInvoiceExcelFileExtention();
                var dlgRez = saveFileDialogReport.ShowDialog();
                if (dlgRez == System.Windows.Forms.DialogResult.OK || dlgRez == System.Windows.Forms.DialogResult.Yes)
                {
                    _presenter.ExportExcelStart(part, saveFileDialogReport.FileName);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void ExportExcelCancel(DeclarationPart part)
        {
            try
            {
                var ret = ShowQuestion("Экспорт в эксель", "Прекратить выгрузку записей о СФ?");
                if (ret == System.Windows.Forms.DialogResult.Yes)
                {
                    _presenter.ExportExcelCancel(part);
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        public void OpenExcelFile(string reportExcelFilePath, string invoiceExcelName)
        {
            var ret = ShowQuestion("Экспорт в эксель", string.Format("файл экспорта в эксель \"{0}\" сформирован. Открыть файл?", invoiceExcelName));
            if (ret == System.Windows.Forms.DialogResult.Yes)
            {
                _presenter.OpenInvoiceExcelFile(reportExcelFilePath);
            }
        }

        public void SetButtonExportExcelVisible(DeclarationPart part, bool exportExcelVisible, bool exportExcelCancelVisible)
        {
            if (_presenter.IsUseDataGridView(part) && PartGridsPro.ContainsKey(part))
            {
                if (exportExcelCancelVisible)
                {
                    PartGridsPro[part].ExportExcelVisible = exportExcelVisible;
                    PartGridsPro[part].ExportExcelCancelVisible = exportExcelCancelVisible;
                }
                else
                {
                    PartGridsPro[part].ExportExcelCancelVisible = exportExcelCancelVisible;
                    PartGridsPro[part].ExportExcelVisible = exportExcelVisible;
                }
            }

            if (PartGrids.ContainsKey(part))
            {
                GridControl gridCurrent = PartGrids[part].GridControl;
                if (exportExcelCancelVisible)
                {
                    gridCurrent.ExportExcelVisible = exportExcelVisible;
                    gridCurrent.ExportExcelCancelVisible = exportExcelCancelVisible;
                }
                else
                {
                    gridCurrent.ExportExcelCancelVisible = exportExcelCancelVisible;
                    gridCurrent.ExportExcelVisible = exportExcelVisible;
                }
            }
        }

        public void SetExportExcelStateVisible(DeclarationPart part, bool isVisible)
        {
            if (_presenter.IsUseDataGridView(part) && PartGridsPro.ContainsKey(part))
            {
                PartGridsPro[part].PanelExportExcelStateVisible = isVisible;
                PartGridsPro[part].SetPanelExportExcelStateWidth(230);
            }

            if (PartGrids.ContainsKey(part))
            {
                PartGrids[part].GridControl.PanelExportExcelStateVisible = isVisible;
                PartGrids[part].GridControl.SetPanelExportExcelStateWidth(230);
            }
        }

        public void SetExportExcelState(DeclarationPart part, string stateText)
        {
            if (_presenter.IsUseDataGridView(part) && PartGridsPro.ContainsKey(part))
                PartGridsPro[part].SetExportExcelState(stateText);

            if (PartGrids.ContainsKey(part))
                PartGrids[part].GridControl.SetExportExcelState(stateText);
        }

        private List<DeclarationPart> _exportExcelChapters = new List<DeclarationPart>() { DeclarationPart.R8, DeclarationPart.R9, DeclarationPart.R10, DeclarationPart.R11, DeclarationPart.R12, DeclarationPart.Discrepancy, DeclarationPart.KC };

        private void ExportExcelSetButtonAccessAll(bool isAccess)
        {
            foreach (var item in PartGridsPro.Where(p => _exportExcelChapters.Contains(p.Key)))
            {
                //откровенно задрало разбираться с этим гомункулом
                //идея была неплохая, но как говорил Рэймонд,- KISS
                if (item.Key == DeclarationPart.Discrepancy)
                {
                    PartGridsPro[DeclarationPart.R10].ExportExcelVisible = isAccess;
                    PartGridsPro[DeclarationPart.R10].SetExportExcelEnabled(_model.ChapterModel[item.Key.ToDeclarationInvoiceChapter()].GetTotalInvoiceQuantity() > 0);
                    PartGridsPro[DeclarationPart.R10].ExportExcelCancelVisible = false;
                }
                else if (item.Key == DeclarationPart.KC)
                {
                    PartGridsPro[DeclarationPart.R11].ExportExcelVisible = isAccess;
                    PartGridsPro[DeclarationPart.R11].SetExportExcelEnabled(_model.ChapterModel[item.Key.ToDeclarationInvoiceChapter()].GetTotalInvoiceQuantity() > 0);
                    PartGridsPro[DeclarationPart.R11].ExportExcelCancelVisible = false;
                }


                if (_presenter.IsUseDataGridView(item.Key) && PartGridsPro.ContainsKey(item.Key))
                {
                    PartGridsPro[item.Key].ExportExcelVisible = isAccess;
                    PartGridsPro[item.Key].SetExportExcelEnabled(_model.ChapterModel[item.Key.ToDeclarationInvoiceChapter()].GetTotalInvoiceQuantity() > 0);
                    PartGridsPro[item.Key].ExportExcelCancelVisible = false;
                }

                if (PartGrids.ContainsKey(item.Key))
                {
                    GridControl gridCurrent = PartGrids[item.Key].GridControl;
                    gridCurrent.ExportExcelVisible = isAccess;
                    gridCurrent.SetExportExcelEnabled(_model.ChapterModel[item.Key.ToDeclarationInvoiceChapter()].GetTotalInvoiceQuantity() > 0);
                    gridCurrent.ExportExcelCancelVisible = false;
                }
            }
        }

        private void ExportExcelAddEvents()
        {
            this.dataGridR8.OnExportExcel += new System.EventHandler(this.gridR8_OnExportExcel);
            this.dataGridR9.OnExportExcel += new System.EventHandler(this.gridR9_OnExportExcel);
            this.dataGridR10.OnExportExcel += new System.EventHandler(this.gridR10_OnExportExcel);
            this.dataGridR11.OnExportExcel += new System.EventHandler(this.gridR11_OnExportExcel);
            this.dataGridR12.OnExportExcel += new System.EventHandler(this.gridR12_OnExportExcel);
            this.dataGridR8.OnExportExcelCancel += new System.EventHandler(this.gridR8_OnExportExcelCancel);
            this.dataGridR9.OnExportExcelCancel += new System.EventHandler(this.gridR9_OnExportExcelCancel);
            this.dataGridR10.OnExportExcelCancel += new System.EventHandler(this.gridR10_OnExportExcelCancel);
            this.dataGridR11.OnExportExcelCancel += new System.EventHandler(this.gridR11_OnExportExcelCancel);
            this.dataGridR12.OnExportExcelCancel += new System.EventHandler(this.gridR12_OnExportExcelCancel);
        }

        private void gridR8_OnExportExcel(object sender, EventArgs e)
        {
            ExportExcel(DeclarationPart.R8);
        }

        private void gridR9_OnExportExcel(object sender, EventArgs e)
        {
            ExportExcel(DeclarationPart.R9);
        }

        private void gridR10_OnExportExcel(object sender, EventArgs e)
        {
            ExportExcel(DeclarationPart.R10);
        }

        private void gridR11_OnExportExcel(object sender, EventArgs e)
        {
            ExportExcel(DeclarationPart.R11);
        }

        private void gridR12_OnExportExcel(object sender, EventArgs e)
        {
            ExportExcel(DeclarationPart.R12);
        }

        private void gridR8_OnExportExcelCancel(object sender, EventArgs e)
        {
            ExportExcelCancel(DeclarationPart.R8);
        }

        private void gridR9_OnExportExcelCancel(object sender, EventArgs e)
        {
            ExportExcelCancel(DeclarationPart.R9);
        }

        private void gridR10_OnExportExcelCancel(object sender, EventArgs e)
        {
            ExportExcelCancel(DeclarationPart.R10);
        }

        private void gridR11_OnExportExcelCancel(object sender, EventArgs e)
        {
            ExportExcelCancel(DeclarationPart.R11);
        }

        private void gridR12_OnExportExcelCancel(object sender, EventArgs e)
        {
            ExportExcelCancel(DeclarationPart.R12);
        }

        private void View_OnFormClosing(object sender, EventArgs e)
        {
            _presenter.ExportExcelCancelAll();
        }

        public string GetTitleChapterR8_12(DeclarationPart part)
        {
            string title = String.Empty;
            List<DeclarationPart> listGridTitle = new List<DeclarationPart>() { DeclarationPart.R8, DeclarationPart.R9, DeclarationPart.R12 };
            if (listGridTitle.Contains(part))
            {
                if (_presenter.IsUseDataGridView(part) && PartGridsPro.ContainsKey(part))
                    title = PartGridsPro[part].Title;

                if (PartGrids.ContainsKey(part))
                    title = PartGrids[part].GridControl.Title;
            }
            else if (part == DeclarationPart.R10)
            {
                title = captionR10.Text;
            }
            else if (part == DeclarationPart.R11)
            {
                title = captionR11.Text;
            }
            return title;
        }

        #endregion

        #region Анализ данных в разделах

        private Dictionary<DeclarationPart, UltraLabel> _chapterEmptyLabels;

        private Dictionary<DeclarationPart, List<System.Windows.Forms.Control>> _hideableControls;

        private void InitLabelDictionary()
        {
            _chapterEmptyLabels = new Dictionary<DeclarationPart, UltraLabel>
            {
                {DeclarationPart.R8, ulRedWords8},
                {DeclarationPart.R9, ulRedWords9},
                {DeclarationPart.R10, ulRedWords10},
                {DeclarationPart.R11, ulRedWords11},
                {DeclarationPart.R12, ulRedWords12}
            };
            _hideableControls = new Dictionary<DeclarationPart, List<Control>>
            {
                {DeclarationPart.R8, new List<Control>{dataGridR8, ultraExpandableGroupBox_R8_Header}},
                {DeclarationPart.R9, new List<Control>{dataGridR9, ultraExpandableGroupBox_R9_Header}},
                {DeclarationPart.R10, new List<Control>{dataGridR10, captionR10}},
                {DeclarationPart.R11, new List<Control>{dataGridR11, captionR11}},
                {DeclarationPart.R12, new List<Control>{dataGridR12}}
            };
        }

        private void AnalizeChapterData()
        {
            /*
            foreach (var infoPair in _model.ChapterInvoicesInfos)
            {
                if (infoPair.Value.InvoiceCount > 0)
                    HideLabel(infoPair.Key);

                switch (infoPair.Key)
                {
                    case DeclarationPart.R8:
                    case DeclarationPart.R9:
                        var info = (ChapterInfoEx)infoPair.Value;
                        if (info.DataExists && info.DataExtExists &&
                            info.InvoiceCountBase == 0 && info.InvoiceCountPart == 0)
                            ShowUpLabel(infoPair.Key,
                                string.Format("В присланных xml файлах ({0} и {1}) отсутствуют данные о СФ.", GetXmlName(infoPair.Key), GetXmlName(infoPair.Key, true)));
                        else if (info.DataExists && info.InvoiceCountBase == 0)
                            ShowNotification(string.Format("В присланном xml файле ({0}) отсутствуют данные о СФ.", GetXmlName(infoPair.Key)));
                        else if (info.DataExtExists && info.InvoiceCountPart == 0)
                            ShowNotification(string.Format("В присланном xml файле ({0}) отсутствуют данные о СФ.", GetXmlName(infoPair.Key, true)));
                        else if (!info.DataExists && !info.DataExtExists)
                            ShowUpLabel(infoPair.Key, "Данные о СФ отсутствуют.");
                        break;
                    default:
                        if (infoPair.Value.DataExists && infoPair.Value.InvoiceCount == 0)
                            ShowUpLabel(infoPair.Key, string.Format("В присланном xml файле ({0}) отсутствуют данные о СФ.", GetXmlName(infoPair.Key)));
                        else if (!infoPair.Value.DataExists)
                            ShowUpLabel(infoPair.Key, "Данные о СФ отсутствуют.");
                        break;
                }
            }
             * */
        }

        private string GetXmlName(DeclarationPart part, bool isExt = false)
        {
            switch (part)
            {
                case DeclarationPart.R8:
                    return isExt ? _model.RawData.Chapter81xml : _model.RawData.Chapter8xml;
                case DeclarationPart.R9:
                    return isExt ? _model.RawData.Chapter91xml : _model.RawData.Chapter9xml;
                case DeclarationPart.R10:
                    return _model.RawData.Chapter10xml;
                case DeclarationPart.R11:
                    return _model.RawData.Chapter11xml;
                case DeclarationPart.R12:
                    return _model.RawData.Chapter12xml;
                default:
                    return string.Empty;
            }
        }

        private void ShowUpLabel(DeclarationPart part, string message)
        {
            UltraLabel label;

            if ((_chapterEmptyLabels == null) || (_hideableControls == null))
                return;

            if (!_chapterEmptyLabels.TryGetValue(part, out label))
                return;
            List<Control> list;
            if (_hideableControls.TryGetValue(part, out list))
            {
                foreach (var control in list)
                {
                    control.Visible = false;
                }
            }
            label.Text = message;
            label.Visible = true;
        }

        private void HideLabel(DeclarationPart part)
        {
            UltraLabel label;

            if ((_chapterEmptyLabels == null) || (_hideableControls == null))
                return;

            if (!_chapterEmptyLabels.TryGetValue(part, out label))
                return;

            label.Visible = false;
            List<Control> list;
            if (!_hideableControls.TryGetValue(part, out list))
                return;
            foreach (var control in list)
            {
                control.Visible = true;
            }
        }

        #endregion

        #region Настройка суммарной строки

        private void SetupSummaryRow(Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView grid)
        {
            Color color = Color.FromArgb(198, 218, 243);
            grid.StartExecuteInUiThread(() =>
            {
                var band = grid.Grid.DisplayLayout.Bands[0];
                band.SummaryFooterCaption = "Всего (с учетом фильтра):";
                band.Override.BorderStyleSummaryFooterCaption = UIElementBorderStyle.None;
                band.Override.BorderStyleSummaryFooter = UIElementBorderStyle.None;
                band.Override.BorderStyleSummaryValue = UIElementBorderStyle.None;
                band.Override.SummaryFooterAppearance.BackColor = color;
                band.Override.SummaryValueAppearance.BackColor = color;
                band.Override.SummaryFooterCaptionAppearance.BackColor = color;
            });
        }

        private void CreateSummaryRow(Luxoft.NDS2.Client.UI.Controls.Grid.V2.IDataGridView grid, DeclarationInvoiceChapterNumber chapter)
        {
            var dataGrid = (Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView)grid;
            if (_model.ChapterModel[chapter].Summaries != null && _model.ChapterModel[chapter].Summaries.Count > 0)
            {
                dataGrid.StartExecuteInUiThread(() =>
                {
                    var band = dataGrid.Grid.DisplayLayout.Bands[0];

                    foreach (var sum in _model.ChapterModel[chapter].Summaries)
                    {
                        var column = band.Columns[sum.Key];
                        if (column != null)
                        {
                            SummarySettings summary;
                            var valueWithFormat = FormatFieldDecimalSummary(sum.Value);
                            if (band.Summaries.Exists(sum.Key))
                            {
                                summary = band.Summaries[sum.Key];
                                summary.Formula = valueWithFormat;
                            }
                            else
                            {
                                summary = band.Summaries.Add(sum.Key, valueWithFormat,
                                    Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn, column);
                            }
                            summary.DisplayFormat = column.GetDecimalFormatString();
                            summary.Appearance.TextHAlign = HAlign.Right;
                            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                        }
                    }
                });
            }
            else
            {
                dataGrid.Grid.DisplayLayout.Bands[0].Summaries.Clear();
            }
        }

        private string FormatFieldDecimalSummary(decimal value)
        {
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ".";

            return value.ToString(numberFormatInfo);
        }

        #endregion

    }

    public class ExplainToolTipViewer : ToolTipViewerBase
    {

        public override string GetToolTip(object listObject)
        {
            var invoice = listObject as InvoiceModel;
            if (invoice == null)
                return null;

            return invoice.ExplainToolTip;
        }
    }
}