﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.GridColumns;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Discrepancy;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {
        private CustomDictionaryFilterManager _knpDocColumnFilter;


        private void InitializeGridDiscrepancies()
        {
            IntValueSignFormatter formatNDSSum = new IntValueSignFormatter();
            
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridDiscrepancies", GetType()));
            setup.GetData = this.GetDataDiscrepanciesData;

            var helper = new GridSetupHelper<DeclarationDiscrepancyModel>();


            var columnTaxpayerSource 
                = helper
                    .CreateColumnDefinition(
                        data => data.TaxPayerInvoiceSource, 
                        settings => 
                        { 
                            settings.Width = 120; 
                            settings.RowSpan = 2; 
                        });

            var groupDiscrepancy = new ColumnGroupDefinition() { Caption = "Расхождение", RowSpan = 1 };
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancyId));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.CreatedByInvoiceNumber,
                        settings => settings.Width = 120));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.CreatedByInvoiceDate,
                        settings => settings.Width = 80));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.FoundDate));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.TypeName));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.Amount, d => d.Width = WidthMoney + 10));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.AmountPvp));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.StatusName));
            groupDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.ActInn, d => d.Width = 180));

            var groupActDecision = new ColumnGroupDefinition() { Caption = "Учет в актах и решениях", RowSpan = 1 };
            groupActDecision.Columns.Add(helper.CreateColumnDefinition(o => o.KnpDocType));
            groupActDecision.Columns.Add(helper.CreateColumnDefinition(o => o.KnpDocAmount, 
                        settings => { settings.FormatInfo = formatPriceShowZero; settings.Width = WidthMoney; }));
            groupActDecision.Columns.Add(helper.CreateColumnDefinition(o => o.DecisionAmountDifference,
                        settings => { settings.FormatInfo = formatPriceShowZero; settings.Width = WidthMoney; }));
            
            var groupContragent = new ColumnGroupDefinition() { Caption = "Контрагент", RowSpan = 1 };
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorInvoiceSource, d => d.Width = 120));
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorInn, d => d.ToolTip = "ИНН контрагента"));
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorKpp, d => d.ToolTip = "КПП контрагента"));
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorName, d => d.ToolTip = "Наименование контрагента"));
            groupContragent.Columns.Add(
                helper.CreateColumnDefinition(
                    dataItem => dataItem.ContractorInnReorganized,
                    column => column.Width = 180));
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorDocType));
            groupContragent.Columns.Add(helper.CreateColumnDefinition(o => o.ContractorDeclSign));

            var c30 = helper.CreateColumnDefinition(o => o.ContractorNdsAmount, d => { d.RowSpan = 2; d.FormatInfo = formatNDSSum; });
            var c40 = helper.CreateColumnDefinition(o => o.KnpStatus, d => d.RowSpan = 2);
            groupContragent.Columns.Add(c30);
            groupContragent.Columns.Add(c40);

            setup.Columns.AddRange(new List<ColumnBase>() { columnTaxpayerSource, groupDiscrepancy, groupActDecision, groupContragent });


            setup.QueryCondition = new QueryConditions()
            {
                Sorting = new List<ColumnSort>() { new ColumnSort() { ColumnKey = helper.GetMemberName(o => o.Amount), Order = ColumnSort.SortOrder.Desc } }
            };

            setup.OrderColumns();

            gridDiscrepancies.AllowSaveFilterSettings = false;
            gridDiscrepancies.Setup = setup;
            gridDiscrepancies.RowDoubleClicked += delegate(object i)
            {
                OpenDiscrepancyCard(i as DeclarationDiscrepancyModel);
            };

            gridDiscrepancies.GetColumn(TypeHelper<DeclarationDiscrepancyModel>.GetMemberName(x => x.DiscrepancyId)).Hidden = true;

            gridDiscrepancies.AppendOptionsBuilder(
                TypeHelper<DeclarationDiscrepancyModel>.GetMemberName(x => x.KnpDocType),
                new CustomDictionaryFilterManager(new Dictionary<object, string>()
                                                      {
                                                          {"Акт", "Акт"},
                                                          {"Решение", "Решение"}
                                                      }));
        }

        private object GetDataDiscrepanciesData(QueryConditions qc, out long totalRowsNumber)
        {
            return _presenter.GetData(DeclarationPart.Discrepancy, qc, out totalRowsNumber);
        }

        private void OpenDiscrepancyCard(DeclarationDiscrepancyModel dis)
        {
            if (dis == null)
            {
                base.ShowError("Ошибка открытия карточки расхождения. Ошибка получения объекта.");
                return;
            }

            var discrepancyCardOpener = new DiscrepancyCardOpener(WorkItem);
            
            discrepancyCardOpener.Open(dis.DeclarationDiscrepancyRaw);
        }

        #region ContextMenu

        private void cmiOpenDiscrep_Click(object sender, EventArgs e)
        {
            OpenDiscrepancyCard(gridDiscrepancies.GetCurrentItem<DeclarationDiscrepancyModel>());
        }

        private void cmiOpenTaxPayer_Click(object sender, EventArgs e)
        {
            var d = gridDiscrepancies.GetCurrentItem<DeclarationDiscrepancyModel>();

            if (d != null)
                _presenter.ViewTaxPayerByKppOriginal(d.ContractorInn, d.ContractorKpp, Direction.Sales);
        }

        private void cmiOpenDecl_Click(object sender, EventArgs e)
        {
            var d = gridDiscrepancies.GetCurrentItem<DeclarationDiscrepancyModel>();

            if (d != null)
            {
                _presenter.ViewDeclarationDetailsFromDiscrepancy(d.DeclarationDiscrepancyRaw);
            }
        }

        #endregion ContextMenu
    }
}
