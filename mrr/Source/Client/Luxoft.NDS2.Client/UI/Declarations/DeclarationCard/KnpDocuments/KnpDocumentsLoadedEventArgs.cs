﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public class KnpDocumentsLoadedEventArgs : EventArgs
    {
        public List<KnpDocumentDeclaration> Declarations
        {
            get;
            set;
        }

        public List<KnpDocumentClaim> Claims
        {
            get;
            set;
        }

        public List<ClaimExplain> Explains
        {
            get;
            set;
        }

        public List<SeodKnpDocument> Acts
        {
            get;
            set;
        }

        public List<SeodKnpDocument> Decisions
        {
            get;
            set;
        }
    }
}
