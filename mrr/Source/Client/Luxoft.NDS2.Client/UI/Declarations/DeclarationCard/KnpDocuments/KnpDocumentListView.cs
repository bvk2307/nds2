﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public partial class KnpDocumentListView : UserControl, IKnpDocumentListView
    {
        public KnpDocumentListView()
        {
            InitializeComponent();

            Columns =
              new GridColumnsCollection(
                  null,
                  _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                  _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(
                      new[]
                      {
                          TypeHelper<MidLevelKnpDocument>.GetMemberName(x => x.StatusDate),
                          TypeHelper<MidLevelKnpDocument>.GetMemberName(x => x.ListItems)
                      }),
                  new GridMultiBandColumnCreator(new List<ColumnsCollection>
                  {
                      _grid.DisplayLayout.Bands[1].Columns,
                      _grid.DisplayLayout.Bands[2].Columns
                  }));

            ApplyColumnFormatter();
        }
       
        public event EventHandler<ListItemEventArgs> ListItemSelected;

        public event EventHandler<KnpDocumentInitEventArgs> ListItemInitializing;

        public event EventHandler RowSelected;

        public IGridColumnsCollection Columns
        {
            get;
            private set;
        }

        private void ApplyColumnFormatter()
        {
            Columns
                 .FindByKey(TypeHelper<KnpDocument>.GetMemberName(x => x.IsHighlighted))
                 .ApplyFormatter(
                     new ImageColumnFormatter<bool>(KnpDocumentFlagDictionary.FlagIcons, this));
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                {
                    return null;
                }

                return _grid.ActiveRow.ListObject;
            }
        }
        
        private void InitializeRow(object sender, InitializeRowsCollectionEventArgs e)
        {
            foreach (UltraGridRow row in e.Rows)
            {
                var args = new KnpDocumentInitEventArgs(row.ListObject);
                if (ListItemInitializing != null)
                    ListItemInitializing(row.ListObject, args);

                if (args.Selectable)
                {
                    row.MakeHyperlink(TypeHelper<KnpDocument>.GetMemberName(x => x.Name));
                }
            }

            foreach (UltraGridRow row in _grid.DisplayLayout.Bands[0].GetRowEnumerator(GridRowType.DataRow))
            {
                if (row.HasChild())
                {
                    row.ExpandAll();
                    break;
                }
            }
        }

        private void ClickCell(object sender, ClickCellEventArgs e)
        {
            if (RowSelected != null)
            {
                RowSelected(sender, e);
            }
            if (ListItemSelected != null && e.Cell.Column.Key == TypeHelper<KnpDocument>.GetMemberName(x => x.Name) && e.Cell.Style == ColumnStyle.URL)
                ListItemSelected(e.Cell.Row.ListObject, new ListItemEventArgs(e.Cell.Row.ListObject));
        }

        private void BeforeSortChange(object sender, BeforeSortChangeEventArgs e)
        {
            if(Columns != null)
            foreach (UltraGridColumn sortColumn in e.SortedColumns)
            {
                Columns.FindByKey(sortColumn.Key).Sort(sortColumn.SortIndicator);
            }
        }
    }
}
