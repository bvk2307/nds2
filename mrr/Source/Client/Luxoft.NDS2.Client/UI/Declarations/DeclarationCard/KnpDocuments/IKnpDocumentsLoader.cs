﻿using System;
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public interface IKnpDocumentsLoader
    {
        void StartLoading(string inn, string innContractor, string kpp, int fiscalYear, int periodCode);

        event EventHandler<KnpDocumentsLoadedEventArgs> DataLoaded;
    }
}
