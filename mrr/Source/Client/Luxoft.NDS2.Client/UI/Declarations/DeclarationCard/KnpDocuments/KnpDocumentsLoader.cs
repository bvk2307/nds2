﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public class KnpDocumentsLoader : ServiceProxyBase<KnpDocumentsData>, IKnpDocumentsLoader
    {
        private readonly IDeclarationsDataService _service;

        public KnpDocumentsLoader(
            IDeclarationsDataService service,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public void StartLoading(string inn, string innContractor, string kppEffective, int fiscalYear, int periodEffective)
        {
            BeginInvoke(() => _service.KnpDocuments(inn, innContractor, kppEffective, fiscalYear, periodEffective));
        }

        protected override void CallBack(KnpDocumentsData result)
        {
            if (DataLoaded != null)
            {
                DataLoaded(this, new KnpDocumentsLoadedEventArgs
                {
                    Declarations = result.Declarations,
                    Claims = result.Claims,
                    Explains = result.Explains,
                    Acts = result.Acts,
                    Decisions = result.Decisions
                });
            }
        }

        public event EventHandler<KnpDocumentsLoadedEventArgs> DataLoaded;
    }
}
