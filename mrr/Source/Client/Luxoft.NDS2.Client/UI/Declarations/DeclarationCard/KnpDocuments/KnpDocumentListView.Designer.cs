﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    partial class KnpDocumentListView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ListItems");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsHighlighted");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Number");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Date", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Descending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusDate");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ListItems");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsHighlighted");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Number");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Date", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Descending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusDate");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsHighlighted");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Number");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Date", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Descending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusDate");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance21;
            this._grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridColumn61.Header.VisiblePosition = 6;
            ultraGridColumn62.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn62.Header.Caption = "";
            ultraGridColumn62.Header.Enabled = false;
            ultraGridColumn62.Header.VisiblePosition = 0;
            ultraGridColumn62.MaxWidth = 40;
            ultraGridColumn62.MinWidth = 16;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn62.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn62.Width = 54;
            ultraGridColumn63.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn63.Header.Caption = "Тип документа";
            ultraGridColumn63.Header.VisiblePosition = 1;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn63.Width = 102;
            ultraGridColumn64.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn64.Header.Caption = "№ документа";
            ultraGridColumn64.Header.VisiblePosition = 2;
            ultraGridColumn64.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn64.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn64.SupportDataErrorInfo = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn64.Width = 103;
            ultraGridColumn65.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn65.Header.Caption = "Дата документа";
            ultraGridColumn65.Header.VisiblePosition = 3;
            ultraGridColumn65.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn65.Width = 109;
            ultraGridColumn66.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn66.Header.Caption = "Статус";
            ultraGridColumn66.Header.VisiblePosition = 4;
            ultraGridColumn66.RowLayoutColumnInfo.OriginX = 5;
            ultraGridColumn66.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn66.Width = 156;
            ultraGridColumn67.Header.VisiblePosition = 5;
            ultraGridColumn67.Hidden = true;
            ultraGridColumn67.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn67.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67});
            ultraGridBand1.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn68.Header.VisiblePosition = 6;
            ultraGridColumn69.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn69.Header.VisiblePosition = 0;
            ultraGridColumn69.MaxWidth = 32;
            ultraGridColumn69.MinWidth = 16;
            ultraGridColumn69.RowLayoutColumnInfo.OriginX = 7;
            ultraGridColumn69.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn69.Width = 35;
            ultraGridColumn70.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn70.Header.Caption = "Тип документа";
            ultraGridColumn70.Header.VisiblePosition = 1;
            ultraGridColumn70.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn70.Width = 102;
            ultraGridColumn71.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn71.Header.Caption = "№ документа";
            ultraGridColumn71.Header.VisiblePosition = 2;
            ultraGridColumn71.RowLayoutColumnInfo.OriginX = 9;
            ultraGridColumn71.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn71.Width = 103;
            ultraGridColumn72.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn72.Header.Caption = "Дата документа";
            ultraGridColumn72.Header.VisiblePosition = 3;
            ultraGridColumn72.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn72.Width = 109;
            ultraGridColumn73.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn73.Header.Caption = "Статус";
            ultraGridColumn73.Header.VisiblePosition = 4;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 11;
            ultraGridColumn73.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn73.Width = 156;
            ultraGridColumn74.Header.VisiblePosition = 5;
            ultraGridColumn74.Hidden = true;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 12;
            ultraGridColumn74.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn74.Width = 79;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand3.ColHeadersVisible = false;
            ultraGridColumn75.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn75.Header.Caption = "";
            ultraGridColumn75.Header.VisiblePosition = 0;
            ultraGridColumn75.MaxWidth = 24;
            ultraGridColumn75.MinWidth = 16;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 13;
            ultraGridColumn75.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn75.Width = 16;
            ultraGridColumn76.Header.Caption = "Тип документа";
            ultraGridColumn76.Header.VisiblePosition = 1;
            ultraGridColumn76.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn76.Width = 102;
            ultraGridColumn77.Header.Caption = "№ документа";
            ultraGridColumn77.Header.VisiblePosition = 2;
            ultraGridColumn77.RowLayoutColumnInfo.OriginX = 15;
            ultraGridColumn77.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn77.Width = 103;
            ultraGridColumn78.Header.Caption = "Дата документа";
            ultraGridColumn78.Header.VisiblePosition = 3;
            ultraGridColumn78.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn78.Width = 109;
            ultraGridColumn79.Header.Caption = "Статус";
            ultraGridColumn79.Header.VisiblePosition = 4;
            ultraGridColumn79.RowLayoutColumnInfo.OriginX = 17;
            ultraGridColumn79.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn79.Width = 156;
            ultraGridColumn80.Header.VisiblePosition = 5;
            ultraGridColumn80.Hidden = true;
            ultraGridColumn80.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn80.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn80.Width = 82;
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn75,
            ultraGridColumn76,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80});
            ultraGridBand3.GroupHeadersVisible = false;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this._grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this._grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.GroupByBox.Hidden = true;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this._grid.DisplayLayout.MaxColScrollRegions = 1;
            this._grid.DisplayLayout.MaxRowScrollRegions = 1;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Synchronized;
            this._grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.CellPadding = 0;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            appearance18.TextHAlignAsString = "Left";
            this._grid.DisplayLayout.Override.HeaderAppearance = appearance18;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle;
            this._grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this._grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(531, 337);
            this._grid.TabIndex = 0;
            this._grid.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.ClickCell);
            this._grid.BeforeSortChange += new Infragistics.Win.UltraWinGrid.BeforeSortChangeEventHandler(this.BeforeSortChange);
            this._grid.InitializeRowsCollection += new Infragistics.Win.UltraWinGrid.InitializeRowsCollectionEventHandler(this.InitializeRow);
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Common.Models.KnpDocuments.DeclarationKnpDocumentsModel);
            // 
            // KnpDocumentListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "KnpDocumentListView";
            this.Size = new System.Drawing.Size(531, 337);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
    }
}
