﻿using System.Drawing;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public static class KnpDocumentFlagDictionary
    {
      public static readonly Dictionary<bool, Bitmap> FlagIcons =
            new Dictionary<bool, Bitmap>
            {
                { false, Properties.Resources.empty },
                { true, Properties.Resources.flag_red_icon_16 }
            };
    }
}
