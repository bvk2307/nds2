﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public class KnpDocumentInitEventArgs : ListItemEventArgs
    {
        public KnpDocumentInitEventArgs(object knpDocument)
            : base(knpDocument)
        {
        }

        public bool Selectable
        {
            get;
            set;
        }
    }
}
