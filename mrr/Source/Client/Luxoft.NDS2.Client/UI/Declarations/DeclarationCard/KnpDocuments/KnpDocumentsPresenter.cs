﻿using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public class KnpDocumentsPresenter 
    {
        private readonly IKnpDocumentListView _view;

        private readonly DeclarationKnpDocumentsModel _model;

        private readonly IKnpDocumentsLoader _dataProxy;

        private readonly IKnpDocumentDetailsViewer _reclaimViewer;

        private readonly IKnpDocumentDetailsViewer _claimViewer;

        private readonly KnpDocumentDetailsViewer _knpDocumentDetailsViewer;

        public KnpDocumentsPresenter(IKnpDocumentListView view,
            DeclarationKnpDocumentsModel model,
            IKnpDocumentDetailsViewer reclaimViewer,
            IKnpDocumentDetailsViewer claimViewer,
            KnpDocumentDetailsViewer knpDocumentDetailsViewer,
            IKnpDocumentsLoader dataLoader
          )
        {
            _view = view;
            _reclaimViewer = reclaimViewer;
            _claimViewer = claimViewer;
            _knpDocumentDetailsViewer = knpDocumentDetailsViewer;
            _dataProxy = dataLoader;

            _model = model;
            
            _dataProxy.DataLoaded += DataLoaded;
            _view.ListItemInitializing += ItemInitializing;
            _view.ListItemSelected += ItemSelected;
        }

        private void ItemInitializing(object sender, KnpDocumentInitEventArgs e)
        {
            var knpDoc = (KnpDocument) sender;
            if (knpDoc != null)
            {
                var selectable = knpDoc.AllowView() || knpDoc.AllowEdit();
                e.Selectable = selectable;
            }
        }
        
        private void ItemSelected(object sender, ListItemEventArgs e)
        {
            OpenKnpDocumentCard((KnpDocument)e.ListItem);
        }

        public void DropCache()
        {
            _model.InnContractor = string.Empty;
        }
        
        public void BeginLoad(string innContractor)
        {
            if (_model.InnContractor != innContractor)
            {
                _dataProxy.StartLoading(
                    _model.GetInn(),
                    innContractor,
                    _model.GetKppEffective(),
                    _model.GetFiscalYear(),
                    _model.GetPeriodEffective());
                _model.InnContractor = innContractor;
            }
        }

        private void DataLoaded(object sender, KnpDocumentsLoadedEventArgs arg)
        {
            _model.BuildTree(arg.Declarations, arg.Claims, arg.Explains, arg.Acts, arg.Decisions);
            _view.SetDataSource(_model);
        }

        public void OpenKnpDocumentCard(KnpDocument doc)
        {
            if (doc is ClaimInvoiceViewModel || doc is ClaimControlRatioViewModel)
            {
                _claimViewer.View(doc);
                return;
            }
            if (doc is ReclaimInvoiceViewModel)
            {
                _reclaimViewer.View(doc);
                return;
            }
            if (doc is ManualExplainModel && doc.AllowEdit())
            {
                _knpDocumentDetailsViewer.Edit(doc);
                return;
            }

            _knpDocumentDetailsViewer.View(doc);
        }

    }
}
