﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments
{
    public interface IKnpDocumentListView : IGridView
    {
        event EventHandler<ListItemEventArgs> ListItemSelected;

        event EventHandler<KnpDocumentInitEventArgs> ListItemInitializing;

        event EventHandler RowSelected;

        object SelectedItem
        {
            get;
        }
    }
}
