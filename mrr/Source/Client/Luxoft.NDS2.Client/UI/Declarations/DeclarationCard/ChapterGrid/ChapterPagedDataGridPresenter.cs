﻿using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ChapterGrid
{
    public class ChapterPagedDataGridPresenter : PagedDataGridPresenter<InvoiceModel>
    {
        private Func<QueryConditions, PageResult<InvoiceModel>> _originalDataLoader;
        private int _lastTotalMatches = 0;
        private int _lastTotalAvailable = 0;
        private uint _lastPageSize = 0;

        private int _cachedPages = 10;

        public ChapterPagedDataGridPresenter
            (
                IDataGridView gridView, 
                IPager pager, 
                Func<QueryConditions, PageResult<InvoiceModel>> dataLoader, 
                QueryConditions searchCriteria = null
            ) : base(gridView, pager, null, searchCriteria)
        {
            _originalDataLoader = dataLoader;
            DataLoader = LoadData;
        }


        Dictionary<uint, List<InvoiceModel>> _cachedDatat = new Dictionary<uint, List<InvoiceModel>>();

        protected override void PageIndexChanged()
        {
            SetPaginationCriteria();
            SearchCriteria.PaginationDetails.SkipTotalMatches = true;
            Load();
        }

        protected override void AfterSorting()
        {
            _cachedDatat.Clear();
            base.AfterSorting();
        }

        protected override void AfterFiltering()
        {
            _cachedDatat.Clear();
            base.AfterFiltering();
        }

        protected override void AfterResetSettings()
        {
            base.AfterResetSettings();
            _cachedDatat.Clear();
        }

        protected override void AfterFilterResetSettings()
        {
            base.AfterFilterResetSettings();
            _cachedDatat.Clear();
        }

        public override void ResetTotalsCache()
        {
            SearchCriteria.PaginationDetails.SkipTotalAvailable = false;
            SearchCriteria.PaginationDetails.SkipTotalMatches = false;
            _cachedDatat.Clear();
        }

        protected PageResult<InvoiceModel> LoadData(QueryConditions queryConditions)
        {
            PageResult<InvoiceModel> result = new PageResult<InvoiceModel>(){TotalMatches = _lastTotalMatches, TotalAvailable = _lastTotalAvailable};

            if (_lastPageSize != _pager.PageSize)
            {
                _lastPageSize = _pager.PageSize;
                _cachedDatat.Clear();
            }

            queryConditions.PaginationDetails.RowsToTake = (uint)(_pager.PageSize * _cachedPages);

            if (_cachedDatat.ContainsKey(_pager.PageIndex))
            {
                result.Rows = _cachedDatat[_pager.PageIndex];
            }
            else
            {
                result = _originalDataLoader(queryConditions);

                _lastTotalAvailable = result.TotalAvailable;
                _lastTotalMatches = result.TotalMatches;

                List<InvoiceModel> resultData = result.Rows;

                if (resultData.Count <= _pager.PageSize)
                {
                    if (!_cachedDatat.ContainsKey(_pager.PageIndex))
                    {
                        _cachedDatat.Add(_pager.PageIndex, new List<InvoiceModel>());
                    }
                    _cachedDatat[_pager.PageIndex] = resultData;

                }
                else
                {
                    var pagesReturned = resultData.Count/_pager.PageSize;
                    var lastPageRowsCount = resultData.Count % _pager.PageSize;

                    uint i = 1;
                    uint keyIdx = 0;
                    for ( ;i <= pagesReturned; i++)
                    {
                        keyIdx = i-1 + _pager.PageIndex;

                        if (!_cachedDatat.ContainsKey(keyIdx))
                        {
                            _cachedDatat.Add(keyIdx, new List<InvoiceModel>());
                            _cachedDatat[keyIdx].AddRange(resultData.GetRange((int)((i - 1) * _pager.PageSize), _pager.PageSize));
                        }
                    }

                    if (lastPageRowsCount > 0)
                    {
                        keyIdx = i - 1 + _pager.PageIndex;
                        if (!_cachedDatat.ContainsKey(keyIdx))
                        {
                            _cachedDatat.Add(keyIdx, new List<InvoiceModel>());
                            _cachedDatat[keyIdx].AddRange(resultData.GetRange((int)((i - 1) * _pager.PageSize), lastPageRowsCount));
                        }
                    }

                    result.Rows = _cachedDatat[_pager.PageIndex];
                }
            }

            return result;
        }
    }
}
