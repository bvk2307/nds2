﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    public static class CorrectionNumberParser
    {
        public static int? ParseAsCorrectionNumber(this string correctionNumber)
        {
            int temp;
            return int.TryParse(correctionNumber, out temp) ? (int?)temp : null;
        }
    }
}
