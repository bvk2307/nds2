﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует абстрактный строитель модели разделов сф НД
    /// </summary>
    public abstract class ChapterModelBuilderBase : IChapterModelBuilder
    {
        # region Конструктор

        protected DeclarationSummary Data
        {
            get;
            private set;
        }

        protected ChapterModelBuilderBase(
            DeclarationInvoiceChapterNumber number,
            DeclarationSummary data)
        {
            Data = data;
            Number = number;
        }

        # endregion

        # region Реализация интерфейса IChapterModelBuilder

        public DeclarationInvoiceChapterNumber Number
        {
            get;
            private set;
        }

        public abstract ChapterTitleModel BuildTitle();

        public abstract List<DeclarationInvoiceChapterInfo> BuildCache();

        # endregion        
    }
}
