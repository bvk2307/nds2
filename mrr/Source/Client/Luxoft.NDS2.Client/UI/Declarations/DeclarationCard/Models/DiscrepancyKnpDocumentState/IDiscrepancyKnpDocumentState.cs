﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public interface IDiscrepancyKnpDocumentState
    {
        bool SelectionAvailable();

        bool EditAvailable();

        bool ViewAvailable();
    }
}
