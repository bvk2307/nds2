﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует строителя модели 12 раздела НД
    /// </summary>
    public class ChapterTwelveModelBuilder : ChapterModelBuilderBase
    {
        public ChapterTwelveModelBuilder(DeclarationSummary declarationData)
            : base(DeclarationInvoiceChapterNumber.Chapter12, declarationData)
        {
        }

        public override ChapterTitleModel BuildTitle()
        {
            return SinglePartitionChapterTitleFactory.Factory(
                (int)Number,
                Data.CORRECTION_NUMBER_EFFECTIVE.ParseAsCorrectionNumber(),
                Data.AKT_NOMKORR_12);
        }

        public override List<DeclarationInvoiceChapterInfo> BuildCache()
        {
            var ret = new List<DeclarationInvoiceChapterInfo>();

            if (Data.ACTUAL_ZIP12.HasValue)
            {
                var cache =
                    new DeclarationInvoiceChapterInfo
                    {
                        Part = AskInvoiceChapterNumber.Chapter12,
                        Priority = Data.PRIORITY_12,
                        TotalQuantity = Data.INVOICE_COUNT12 ?? 0,
                        Zip = Data.ACTUAL_ZIP12.Value
                    };

                ret.Add(cache);
            }

            return ret;
        }
    }
}
