﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public abstract class KnpDocumentState : IDiscrepancyKnpDocumentState
    {
        protected readonly bool _initialState;

        protected KnpDocumentState(bool initialState)
        {
            _initialState = initialState;
        }
        
        #region Implementation of IDiscrepancyKnpDocumentState

        public virtual bool SelectionAvailable()
        {
            return _initialState;
        }

        public virtual bool EditAvailable()
        {
            return _initialState;
        }

        public virtual bool ViewAvailable()
        {
            return _initialState;
        }

        #endregion

        protected bool Blocked()
        {
            return false;
        }

    }
}
