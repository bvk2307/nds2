﻿using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    public class DeclarationModelActDataExtractor
    {
        private readonly DeclarationModel _model;

        public DeclarationModelActDataExtractor(DeclarationModel model)
        {
            _model = model;
        }

        public bool IsExistEodData()
        {
            return _model.HasActEodData;
        }

        public string ExtractNum()
        {
            return _model.ActDocNum;
        }

        public DateTime? ExtractDate()
        {
            return _model.ActDocDate;
        }

        public bool ExtractIsClosed()
        {
            return _model.ActIsClosed;
        }
    }
}
