﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс представляем модель данных разделов 8-12 (книги) НД
    /// </summary>
    public class DeclarationBooksModel
    {
        private readonly Dictionary<DeclarationInvoiceChapterNumber, ChapterDataModel> _data;

        /// <summary>
        /// Создает экземпляр класса DeclarationBookModel
        /// </summary>
        /// <param name="declaration">Данные НД</param>
        public DeclarationBooksModel(DeclarationSummary data)
        {
            _data = 
                new Dictionary<DeclarationInvoiceChapterNumber, ChapterDataModel>
                {
                    { DeclarationInvoiceChapterNumber.Chapter8, data.Chapter8() },
                    { DeclarationInvoiceChapterNumber.Chapter9, data.Chapter9() },
                    { DeclarationInvoiceChapterNumber.Chapter10, data.Chapter10() },
                    { DeclarationInvoiceChapterNumber.Chapter11, data.Chapter11() },
                    { DeclarationInvoiceChapterNumber.Chapter12, data.Chapter12() }
                };
        }

        /// <summary>
        /// Возвращает данные книги НД по номеру раздела
        /// </summary>
        /// <param name="chapter">Номер раздела (книги)</param>
        /// <returns>Модель данных книги НД</returns>
        public ChapterDataModel this[DeclarationInvoiceChapterNumber chapter]
        {
            get
            {
                return _data[chapter];
            }
        }

        /// <summary>
        /// Возвращает общее к-во счетов фактур в разделах 8-12 (книгах) НД
        /// </summary>
        /// <returns></returns>
        public int GetTotalInvoiceQuantity()
        {
            return _data.Values.Sum(chapter => chapter.GetTotalInvoiceQuantity());
        }
    }
}
