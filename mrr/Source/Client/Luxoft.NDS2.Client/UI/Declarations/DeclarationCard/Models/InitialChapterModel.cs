﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    public interface IInitialChapterModel
    {
        bool IsSet { get; }

        int Chapter { get; }

        string InvoiceNumber { get; }

        string ContractorInn { get; }

        string InvoiceKey { get; }

        long OrdinalNumber { get; }

        DeclarationPart KeyDeclarationPart { get; }
    }

    public class NotDefinedInitialChapter : IInitialChapterModel
    {
        private const string NotSupportedExceptionMessage = "Стартовый раздел не задан";

        public bool IsSet
        {
            get 
            { 
                return false; 
            }
        }

        public int Chapter
        {
            get 
            { 
                throw new NotSupportedException(NotSupportedExceptionMessage); 
            }
        }

        public string InvoiceNumber
        {
            get 
            {
                throw new NotSupportedException(NotSupportedExceptionMessage);
            }
        }

        public string ContractorInn
        {
            get
            {
                throw new NotSupportedException(NotSupportedExceptionMessage);
            }
        }

        public DeclarationPart KeyDeclarationPart
        {
            get 
            { 
                throw new NotSupportedException(); 
            }
        }

        public string InvoiceKey
        {
            get { throw new NotImplementedException(); }
        }

        public long OrdinalNumber
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class InitialChapterModel : IInitialChapterModel
    {
        private static Dictionary<int, DeclarationPart> DeclarationPartCodes =
            new Dictionary<int, DeclarationPart>
            {
                { 0, DeclarationPart.R8 },
                { 1, DeclarationPart.R9 },
                { 2, DeclarationPart.R8 },
                { 3, DeclarationPart.R9 },
                { 4, DeclarationPart.R10 },
                { 5, DeclarationPart.R11 },
                { 6, DeclarationPart.R12 },
                { 7, DeclarationPart.J1 },
                { 8, DeclarationPart.J2 },
            };

        public InitialChapterModel(uint chapter, string invoiceNumber, string inn, string invoiceKey, long ordinalNumber)
        {
            Chapter = (int)chapter;
            InvoiceNumber = invoiceNumber;
            ContractorInn = inn;
            InvoiceKey = invoiceKey;
            OrdinalNumber = ordinalNumber;
        }

        public bool IsSet
        {
            get
            {
                return true;
            }
        }

        public int Chapter
        {
            get;
            private set;
        }

        public string InvoiceNumber
        {
            get;
            private set;
        }

        public string ContractorInn
        {
            get;
            private set;
        }

        public string InvoiceKey
        {
            get;
            private set;
        }

        public long OrdinalNumber
        {
            get;
            private set;
        }

        public DeclarationPart KeyDeclarationPart
        {
            get
            {
                return (DeclarationPart)DeclarationPartCodes[Chapter];
            }
        }
    }
}
