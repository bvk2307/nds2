﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    public class DeclarationModelChapterDataExtractor
    {
        private DeclarationModel model;
        private DeclarationInvoiceChapterNumber declarationInvoiceChapterNumber;

        public DeclarationModelChapterDataExtractor(DeclarationModel model, DeclarationInvoiceChapterNumber declarationInvoiceChapterNumber)
        {
            this.model = model;
            this.declarationInvoiceChapterNumber = declarationInvoiceChapterNumber;
        }

        public ChapterDataModel Extract()
        {
            return model.ChapterModel[declarationInvoiceChapterNumber];
        }
    }
}
