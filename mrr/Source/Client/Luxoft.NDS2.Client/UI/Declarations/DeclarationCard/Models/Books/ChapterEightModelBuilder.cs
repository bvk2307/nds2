﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует строителя модели 8 раздела НД
    /// </summary>
    public class ChapterEightModelBuilder : ChapterModelBuilderBase
    {
        public ChapterEightModelBuilder(DeclarationSummary declarationData)
            : base(DeclarationInvoiceChapterNumber.Chapter8, declarationData)
        {
        }

        public override ChapterTitleModel BuildTitle()
        {
            return MultiPartitionChapterTitleFactory.Factory(
                (int)Number,
                Data.CORRECTION_NUMBER_EFFECTIVE.ParseAsCorrectionNumber(),
                Data.AKT_NOMKORR_8,
                Data.AKT_NOMKORR_81);
        }

        public override List<DeclarationInvoiceChapterInfo> BuildCache()
        {
            var ret = new List<DeclarationInvoiceChapterInfo>();
            
            if (Data.ACTUAL_ZIP8.HasValue)
            {
                var mainCache =
                    new DeclarationInvoiceChapterInfo
                    {
                        Part = AskInvoiceChapterNumber.Chapter8,
                        Priority = Data.PRIORITY_08,
                        TotalQuantity = Data.INVOICE_COUNT8 ?? 0,
                        Zip = Data.ACTUAL_ZIP8.Value
                    };

                ret.Add(mainCache);
            }


            if (Data.ACTUAL_ZIP81.HasValue)
            {
                var extensionCache =
                    new DeclarationInvoiceChapterInfo
                        {
                            Part = AskInvoiceChapterNumber.Chapter81,
                            Priority = Data.PRIORITY_81,
                            TotalQuantity = Data.INVOICE_COUNT81 ?? 0,
                            Zip = Data.ACTUAL_ZIP81.Value
                        };

                ret.Add(extensionCache);
            }

            return ret;  
        }
    }
}
