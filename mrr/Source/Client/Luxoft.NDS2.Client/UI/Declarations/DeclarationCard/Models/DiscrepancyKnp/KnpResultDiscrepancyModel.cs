﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp
{
    public abstract class KnpResultDiscrepancyModel : INotifyPropertyChanged
    {
        protected const string _nullAmountText = "Удалено";
        protected const string _openStateText = "Открыто";
        protected const string _closeStateText = "Закрыто";

        public bool Included
        {
            get 
            { 
                return _dto.Status == DiscrepancyStatus.Opened 
                    && _dto.Included; 
            }
            set
            {
                if (_dto.Status == DiscrepancyStatus.Opened && _dto.Included != value)
                {
                    _dto.Included = value;
                    NotifyPropertyChanged("Included");
                }
            }
        }

        public bool CanBeIncluded 
        { 
            get 
            {
                return _dto.Status == DiscrepancyStatus.Opened; 
            } 
        }

        public long Id { get { return _dto.Id; } }

        public string Source
        {
            get
            {
                string ret = _dto.InvoiceChapter.HasValue ? _dto.InvoiceChapter.Value.ToString("d") : String.Empty;
                if (!String.IsNullOrEmpty(_dto.ContractorReorganizedInn))
                {
                    ret = String.Concat(ret, " (", _dto.ContractorReorganizedInn, ")");
                }
                return ret;
            }
        }

        public string ContractorInn { get { return _dto.ContractorInn; } }

        public string ContractorKpp { get { return _dto.ContractorKpp; } }

        public string ContractorName { get { return _dto.ContractorName; } }

        public string ContractorReorganizedInn { get { return _dto.ContractorReorganizedInn; } }

        public string InvoiceNumber { get { return _dto.InvoiceNumber; } }

        public DateTime? InvoiceDate { get { return _dto.InvoiceDate; } }

        public decimal? InvoiceAmount { get { return _dto.InvoiceAmount; } }

        public decimal? InvoiceAmountNds { get { return _dto.InvoiceAmountNds; } }

        public string Type { get { return _dto.Type; } }

        public decimal Amount { get { return _dto.Amount; } }

        public string Status 
        { 
            get 
            { 
                return _dto.Status == DiscrepancyStatus.Closed 
                    ? _closeStateText 
                    : _openStateText ; 
            } 
        }

        public virtual decimal? AmountByActNum
        {
            get { return _dto.AmountByAct; }
        }

        public virtual decimal? AmountByDecisionNum
        {
            get { return _dto.AmountByDecision; }
        }

        public bool IsReadOnly { get; set; }

        public bool IsInvalid { get { return InvalidCheck(); } }

        public decimal? PreviousEditableAmountValue { get; protected set; }

        public decimal EditableAmountDelta
        {
            get
            {
                return GetEditableAmount() - PreviousEditableAmountValue.GetValueOrDefault();
            }
        }

        protected readonly KnpResultDiscrepancy _dto;

        public KnpResultDiscrepancyModel(KnpResultDiscrepancy dto)
        {
            _dto = dto;

            IsReadOnly = true;
            PreviousEditableAmountValue = null;
        }

        #region  INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        protected string GetEditableAmountString(decimal? sum)
        {
            if (!IsReadOnly)
            {
                if (_dto.Status == DiscrepancyStatus.Closed)
                    return String.Empty;
                else if (!_dto.Included)
                    return _nullAmountText;
            }

            return sum.HasValue ? sum.Value.ToString("n2") : String.Empty;
        }

        public abstract decimal GetEditableAmount();

        public abstract void RollbackEditableAmount();

        protected abstract bool InvalidCheck();

        public bool PreviousInvalidState { get; protected set; }

        public event EventHandler DiscrepancyAmountChanged;

        protected void NotifyDiscrepancyAmountChanged(decimal sum)
        {
            if (DiscrepancyAmountChanged != null)
            {
                DiscrepancyAmountChanged(this, EditAmountEventArgs.Create(sum));
            }
        }

    }
}
