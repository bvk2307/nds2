﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public class KnpDocumentStateEmpty : KnpDocumentState
    {
        public KnpDocumentStateEmpty(bool initialState) 
            : base(initialState)
        {
        }
        
        #region overrided IDiscrepancyKnpDocumentState

        public override bool EditAvailable()
        {
            return Blocked();
        }

        public override bool ViewAvailable()
        {
            return Blocked();
        }

        #endregion
    }
}
