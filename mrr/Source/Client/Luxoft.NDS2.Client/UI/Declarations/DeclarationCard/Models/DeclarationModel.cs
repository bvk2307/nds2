﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState;
using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Discrepancy;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    public class DeclarationModel
    {
        private readonly CreatorKnpDocumentState _knpDocumentStateCreator;
        
        # region Конструкторы

        public DeclarationModel(DeclarationSummary data, ChainAccessModel chain, string[] roles)
            : this(data, chain, roles, new NotDefinedInitialChapter())
        {
        }

        public DeclarationModel(
            DeclarationSummary data,
            ChainAccessModel chain,
            string[] roles,
            uint chapter,
            string invoiceNumber,
            string contractorInn,
            string invoiceKey,
            long ordinalNumber)
            : this(data, chain, roles, new InitialChapterModel(chapter, invoiceNumber, contractorInn, invoiceKey, ordinalNumber))
        {
        }

        private DeclarationModel(DeclarationSummary data, ChainAccessModel chain, string[] roles, IInitialChapterModel initialChapter)
        {
            _roles = roles;
            _knpDocumentStateCreator = new CreatorKnpDocumentState();
            InitialChapter = initialChapter;
            ChainAccess = chain;
            PartsData =
                new Dictionary<DeclarationPart, DeclarationPartData>
                {
                    {DeclarationPart.Discrepancy, new DeclarationPartData() {Result = new List<DeclarationDiscrepancyModel>()}},
                    {DeclarationPart.KC, new DeclarationPartData() {Result = new List<ControlRatio>()}},
                    {DeclarationPart.DOCKNP, new DeclarationPartData() {Result = new List<DocumentKNP>()}},
                };
            UpdateModel(data);
        }

        # endregion

        # region Загрузка данных в модель

        public void UpdateModel(DeclarationSummary data)
        {
            ChapterModel = new DeclarationBooksModel(data);
            RawData = data;

            UpdateActState();
            UpdateDecisionState();
        }

        private void UpdateActState()
        {
            KnpActState = _knpDocumentStateCreator.Create(
                RawData.ActId, 
                RawData.HasActDiscrepancy, 
                RawData.ActIsClosed, 
                RawData.HasKnpDiscrepancy && (RawData.ACTKNP != null));
        }

        private void UpdateDecisionState()
        {
            KnpDecisionState = _knpDocumentStateCreator.Create(
                RawData.DecisionId, 
                RawData.HasDecisionDiscrepancy, 
                RawData.DecisionIsClosed, 
                true);
        }

        # endregion

        # region Начальное состояние

        public IInitialChapterModel InitialChapter
        {
            get;
            private set;
        }

        # endregion

        # region Данные

        public long DeclarationVersionId
        {
            get
            {
                return RawData.DECLARATION_VERSION_ID;
            }
        }

        public bool ExistsDiscrepancyForKnp
        {
            get
            {
                return RawData.HasKnpDiscrepancy;
            }
        }

        public long? ActDocId
        {
            get
            {
                return RawData.ActId;
            }
            set
            {
                if (RawData.ActId != value)
                {
                    RawData.ActId = value;
                    UpdateActState();
                }
            }
        }

        public bool HasActData
        {
            get
            {
                return RawData.HasActDiscrepancy;
            }
            set
            {
                if (RawData.HasActDiscrepancy != value)
                {
                    RawData.HasActDiscrepancy = value;
                    UpdateActState();
                }
            }
        }

        public bool ActIsClosed
        {
            get
            {
                return RawData.ActIsClosed;
            }
            set
            {
                if (RawData.ActIsClosed != value)
                {
                    RawData.ActIsClosed = value;
                    UpdateActState();
                }
            }
        }

        public long? DecisionDocId
        {
            get
            {
                return RawData.DecisionId;
            }
            set
            {
                if (RawData.DecisionId != value)
                {
                    RawData.DecisionId = value;
                    UpdateDecisionState();
                }
            }
        }

        public bool HasDecisionData
        {
            get
            {
                return RawData.HasDecisionDiscrepancy;
            }
            set
            {
                if (RawData.HasDecisionDiscrepancy != value)
                {
                    RawData.HasDecisionDiscrepancy = value;
                    UpdateDecisionState();
                }
            }
        }

        public bool DecisionIsClosed
        {
            get
            {
                return RawData.DecisionIsClosed;
            }
            set
            {
                if (RawData.DecisionIsClosed != value)
                {
                    RawData.DecisionIsClosed = value;
                    UpdateDecisionState();
                }
            }
        }

        public string TaxPeriodCode
        {
            get
            {
                return RawData.TAX_PERIOD;
            }
        }

        public int Year
        {
            get
            {
                return Convert.ToInt32(RawData.FISCAL_YEAR);
            }
        }

        public IDiscrepancyKnpDocumentState KnpActState { get; private set; }

        public IDiscrepancyKnpDocumentState KnpDecisionState { get; private set; }

        public bool HasActEodData
        {
            get 
            { 
                return RawData.ACTKNP != null;
            }
        }

        public string ActDocNum
        {
            get 
            { 
                return RawData.ACTKNP != null ? RawData.ACTKNP.Number : null; 
            }
        }

        public DateTime? ActDocDate
        {
            get { return RawData.ACTKNP != null ? RawData.ACTKNP.Date : null; }
        }

        public KnpDecisionType[] SeodDecisionDocuments
        {
            get { return RawData.SeodDecisionDocuments; }
        }

        public long ComparisonDataVersion
        {
            get { return RawData.ComparisonDataVersion; }
        }

        # endregion

        # region Сводные Данные

        public string Inn
        {
            get
            {
                return RawData.INN;
            }
        }

        public string InnContractor
        {
            get
            {
                return RawData.INN_CONTRACTOR;
            }
        }
        public string InnReorganized
        {
            get 
            { 
                return RawData.INN_CONTRACTOR.Equals(RawData.INN) ? String.Empty : RawData.INN_CONTRACTOR;
            }
        }
        public string Kpp
        {
            get
            {
                return RawData.KPP;
            }
        }

        public string KppEffective
        {
            get
            {
                return RawData.KPP_EFFECTIVE;
            }
        }
        
        public int? SurCode
        {
            get
            {
                return RawData.SUR_CODE;
            }
        }

        public string Name
        {
            get
            {
                return RawData.NAME;
            }
        }

        public long? SeodId
        {
            get
            {
                return RawData.SEOD_DECL_ID;
            }
        }

        public string FulTaxPeriod
        {
            get
            {
                return RawData.FULL_TAX_PERIOD;
            }
        }

        public string Inspector
        {
            get
            {
                return RawData.INSPECTOR;
            }
            set { RawData.INSPECTOR = value; }
        }

        public string InspectorSid
        {
            get
            {
                return RawData.INSPECTORSID;
            }
            set { RawData.INSPECTORSID = value; }
        }

        public string TotalNdsTitle
        {
            get
            {
                if (null == RawData.COMPENSATION_AMNT)
                    return "отсутствует";
                else
                {
                    string ret = String.Empty;
                    decimal decValue = (decimal)RawData.COMPENSATION_AMNT;
                    if (decValue != 0)
                    {
                        ret = decValue.ToString("N");
                    }
                    return ret;
                }
            }
        }

        public string TotalNdsValue
        {
            get
            {
                string ret = null;
                DeclarationType? signType = SignType;
                if (!signType.HasValue)
                {
                    ret = "НДС отсутствует";
                }
                else
                {
                    if (signType.Value == DeclarationType.Zero)
                         ret = string.Format("НДС {0}", RawData.DECL_SIGN);
                    else ret = string.Format("НДС  {0}: ", RawData.DECL_SIGN);
                }
                return ret;
            }
        }

        public string DeclarationDate
        {
            get
            {
                return RawData.SUBMISSION_DATE.HasValue
                    ? RawData.SUBMISSION_DATE.Value.ToShortDateString()
                    : string.Empty;
            }
        }

        public string CorrectionStatus
        {
            get
            {
                return RawData.IS_ACTUAL ? "(актуальная)" : "(неактуальная)";
            }
        }

        public string CorrectionNumber
        {
            get
            {
                return RawData.CORRECTION_NUMBER;
            }
        }

        public DeclarationTypeCode Type
        {
            get
            {
                return (DeclarationTypeCode)RawData.DECL_TYPE_CODE;
            }
        }

        /// <summary> A sign type of declaration or 'null' ('Признак декларации') (see <see cref="DeclarationSummary.DECL_SIGN"/>). </summary>
        public DeclarationType? SignType
        {
            get { return RawData.DECL_SIGN == string.Empty	//apopov 14.12.2015	//TODO!!! Is it the real value 'string.Empty' here?
                    ? (DeclarationType?)null : RawData.SignType(); }
        }

        public bool IsProcessedByMS
        {
            get
            {
                return RawData.ProcessingStage == DeclarationProcessignStage.ProcessedByMs;
            }
        }

        public DeclarationProcessignStage ProcessignStage { get { return RawData.ProcessingStage;  } }

        /// <summary>
        /// "К Возмещению"
        /// </summary>
        public bool IsCompensationAmount
        {
            get
            {
                bool ret = false;
                DeclarationType? signType = SignType;
                if (signType.HasValue && signType.Value == DeclarationType.ToCharge)
                    ret = true;

                return ret;
            }
        }

        public string SonoCode
        {
            get
            {
                string ret = String.Empty;
                if (RawData.SOUN_ENTRY != null && !string.IsNullOrEmpty(RawData.SOUN_ENTRY.EntryId))
                {
                    ret = RawData.SOUN_ENTRY.EntryId;
                }
                return ret;
            }
        }

        #endregion

        #region Annulment

        private DeclarationVersion GetCurrentVersion()
        {
            return RawData.Revisions.FirstOrDefault(v => v.DeclarationVersionId == RawData.DECLARATION_VERSION_ID);
        }

        private List<DeclarationVersion> GetVersions()
        {
            return RawData.Revisions;
        }

        public bool IsAnnulmentSeod
        {
            get
            {
                var version = GetCurrentVersion();
                return version != null && version.IsAnnulmentSeod;
            }
        }

        public bool IsAnnulment
        {
            get
            {
                var version = GetCurrentVersion();
                return version != null && version.IsAnnulment;
            }
        }

        public bool IsAnnulmentAll
        {
            get { return GetVersions().All(v => v.IsAnnulment); }
        }

        # endregion

        # region Данные книг

        public Dictionary<DeclarationPart, DeclarationPartData> PartsData
        {
            get;
            private set;
        }

        public DeclarationBooksModel ChapterModel 
        { 
            get; 
            private set; 
        }

        # endregion

        # region Заголовки

        private const string IpTitleFormat = "ИНН {0} ({0})";

        private const string LegalTitleFormat = "ИНН {0} / КПП {1} ({1})";

        private const string DeclarationTitle = "НД по НДС";

        private const string JournalTitle = "Журнал учета";

        public string Title
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Kpp))
                {
                    return string.Format(IpTitleFormat, Inn, TypeName);
                }

                return string.Format(LegalTitleFormat, Inn, Kpp, TypeName);
            }
        }

        public string RegNumber
        {
            get
            {
                if (Type == DeclarationTypeCode.Declaration)
                {
                    if (SeodId.HasValue)
                    {
                        return SeodId.ToString();
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
                else
                {
                    return "-";
                }
            }
        }

        protected string TypeName
        {
            get
            {
                return Type == DeclarationTypeCode.Declaration ? DeclarationTitle : JournalTitle;
            }
        }

        # endregion

        # region Ограничение доступа

        private readonly string[] _roles;

        public ChainAccessModel ChainAccess
        {
            get;
            private set;
        }

        public bool CommonInfoIsAvailable
        {
            get
            {
                return (RawData.SummaryState != DeclarationSummaryState.Unavailable ||
                       !RawData.IS_ACTUAL) &&
                    AllowedByRole();
            }
        }

        public DeclarationSummaryState SummaryState
        {
            get
            {
                return RawData.SummaryState;
            }
        }

        private bool AllowedByRole()
        {
            var list = new[]
            {
                Constants.SystemPermissions.Operations.RoleAnalyst,
                Constants.SystemPermissions.Operations.RoleApprover,
                Constants.SystemPermissions.Operations.RoleMedodologist
            };

            return _roles.Any(list.Contains);
        }

        /// <summary>
        /// Метод проверяет, есть ли у текущего пользователя роли, кроме той, что передана в параметре
        /// </summary>
        public bool IsOneUserRole(string roleName)
        {
            List<string> otherRoleWithotInspector = new List<string> { 
                    Constants.SystemPermissions.Operations.RoleDeveloper, 
                    Constants.SystemPermissions.Operations.RoleAnalyst,
                    Constants.SystemPermissions.Operations.RoleApprover,
                    Constants.SystemPermissions.Operations.RoleManager,
                    Constants.SystemPermissions.Operations.RoleSender,
                    Constants.SystemPermissions.Operations.RoleMedodologist,
                    Constants.SystemPermissions.Operations.RoleInspector};

            var list = otherRoleWithotInspector.Where(p => p != roleName).ToList();
            foreach (var r in _roles)
            {
                foreach (var or in list)
                {
                    if (or == r)
                        return false;
                }
            }
            return true;
        }

        private TwoLeveledVersion _formatVersion;

        public TwoLeveledVersion FormatVersion
        {
            get
            {
                if (_formatVersion == null)
                {
                    _formatVersion = new TwoLeveledVersion(RawData.FormatVersion);
                }
                return _formatVersion;
            }
        }

        public bool AllowShowCustomDeclarationNumbersView
        {
            get
            {
                return FormatVersion.NewerThan(new TwoLeveledVersion(DeclarationFormatVersionConst.FIVE_POINT_ZERO_FOUR));
            }
        }

        # endregion

        public bool IsDiscrepanciesEditAccessible()
        {
            return RawData.ProcessingStage == DeclarationProcessignStage.ProcessedByMs && !IsAnnulmentAll;
        }

        public bool IsActSelectionCardButtonVisible()
        {
            return KnpActState != null && KnpActState.SelectionAvailable();
        }

        public bool IsDecisionSelectionCardButtonVisible()
        {
            return KnpActState.ViewAvailable() && !KnpDecisionState.ViewAvailable();
        }

        public bool IsActEditAccessible()
        {
            return (KnpActState.ViewAvailable() || KnpActState.EditAvailable()) && !IsAnnulmentSeod;
        }

        public bool IsDecisionEditAccessible()
        {
            return (KnpDecisionState.ViewAvailable() || KnpDecisionState.EditAvailable()) && !IsAnnulmentSeod;
        }

        public string CorrectionNumCaption
        {
            get { return Type == DeclarationTypeCode.Declaration ? "№ корректировки" : "№ версии"; }
        }
        
        public bool IsAnnulmentFlagVisible()
        {
            return IsAnnulmentSeod;
        }

        public bool IsDateTakeVisible()
        {
            return Type != DeclarationTypeCode.Declaration || !IsAnnulment;
        }

        public bool IsSignVisible()
        {
            return Type == DeclarationTypeCode.Declaration && !IsAnnulment;
        }

        public bool IsCommonInfoAccessible()
        {
            return !IsAnnulment;
        }

        public bool IsChaptersGridIsMatchingColumnVisible()
        {
            return RawData.IS_ACTUAL || !IsAnnulmentSeod;
        }

        public bool IsOpenDeclAccessible()
        {
            return RawData.IS_ACTUAL || !IsAnnulmentSeod;
        }

        # region Прямой доступ к DTO (и прочие вещи которые необходимо в дальнейшем убрать)

        /// <summary>
        /// Наличие прямого доступа к данным из DTO позволяет View и Presenter-у реализовывать логику модели данных самостоятельно
        /// </summary>
        public DeclarationSummary RawData
        {
            get;
            private set;
        }

        /// <summary>
        /// Частично дублирует логику, связанную с передачей 
        /// </summary>
        public DeclarationInfoSource InfoSource
        {
            get;
            set;
        }

        private string _xmlVersion;

        public const string OldVersion = "5.04";

        public const string NewVersion = "5.05";

        public string XmlVersion
        {
            get
            {
                if (string.IsNullOrEmpty(_xmlVersion))
                    _xmlVersion = new TwoLeveledVersion(RawData.XmlVersion).OlderThan(new TwoLeveledVersion(NewVersion))
                        ? OldVersion
                        : RawData.XmlVersion;
                return _xmlVersion;
            }
        }

        # endregion

    }
}
