﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует построение модели разделов 8-12 НД
    /// </summary>
    public static class ChapterModelBuildDirector
    {
        /// <summary>
        /// Строит модель данных раздела СФ НД (разделы 8-12)
        /// </summary>
        /// <param name="builder">Строитель объектов модели</param>
        /// <returns>Экземпляр модели</returns>
        public static ChapterDataModel Build(this IChapterModelBuilder builder)
        {
            return new ChapterDataModel(
                builder.Number, 
                builder.BuildTitle(), 
                builder.BuildCache());
        }

        /// <summary>
        /// Создает модель данных раздела 8 НД
        /// </summary>
        /// <param name="declarationData">Данные НД</param>
        /// <returns>Модель данных</returns>
        public static ChapterDataModel Chapter8(this DeclarationSummary declarationData)
        {
            return new ChapterEightModelBuilder(declarationData).Build();
        }

        /// <summary>
        /// Создает модель данных раздела 9 НД
        /// </summary>
        /// <param name="declarationData">Данные НД</param>
        /// <returns>Модель данных</returns>
        public static ChapterDataModel Chapter9(this DeclarationSummary declarationData)
        {
            return new ChapterNineModelBuilder(declarationData).Build();
        }

        /// <summary>
        /// Создает модель данных раздела 10 НД
        /// </summary>
        /// <param name="declarationData">Данные НД</param>
        /// <returns>Модель данных</returns>
        public static ChapterDataModel Chapter10(this DeclarationSummary declarationData)
        {
            return new ChapterTenModelBuilder(declarationData).Build();
        }

        /// <summary>
        /// Создает модель данных раздела 11 НД
        /// </summary>
        /// <param name="declarationData">Данные НД</param>
        /// <returns>Модель данных</returns>
        public static ChapterDataModel Chapter11(this DeclarationSummary declarationData)
        {
            return new ChapterElevenModelBuilder(declarationData).Build();
        }

        /// <summary>
        /// Создает модель данных раздела 12 НД
        /// </summary>
        /// <param name="declarationData">Данные НД</param>
        /// <returns>Модель данных</returns>
        public static ChapterDataModel Chapter12(this DeclarationSummary declarationData)
        {
            return new ChapterTwelveModelBuilder(declarationData).Build();
        }
    }
}
