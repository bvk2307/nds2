﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот интерфейс описывает функции построителей модели разделов 8-12 НД
    /// </summary>
    public interface IChapterModelBuilder
    {
        /// <summary>
        /// Возвращает номер раздела декларации
        /// </summary>
        DeclarationInvoiceChapterNumber Number
        {
            get;
        }

        /// <summary>
        /// Строит модель заголовка раздела
        /// </summary>
        /// <returns>Новый экземпляр модели заголовка</returns>
        ChapterTitleModel BuildTitle();

        /// <summary>
        /// Строит кэш сводных данных раздела 
        /// </summary>
        /// <returns>Кэшируемые данные</returns>
        List<DeclarationInvoiceChapterInfo> BuildCache();
    }
}
