﻿using Luxoft.NDS2.Client.Helpers.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    public class InvoiceModel
    {
        private readonly Invoice _invoiceData;

        # region Конструктор

        public InvoiceModel(Invoice invoiceData)
        {
            _invoiceData = invoiceData;
        }

        # endregion

        # region Данные СФ

        /// <summary>
        /// Признак редактирования записи в ходе полученного пояснения/ответа от НП
        /// </summary>
        public int? IS_CHANGED { get { return _invoiceData.IS_CHANGED; } }

        private string _explainToolTip = null;

        public string ExplainToolTip
        {
            get
            {
                if (_explainToolTip == null)
                    _explainToolTip = InvoiceExplainHelper.CreateExplainToolTip(_invoiceData);
                return _explainToolTip;
            }
        }

        /// <summary>
        /// Запись сопоставлена"
        /// </summary>
        public bool IS_MATCHING_ROW { get { return _invoiceData.IS_MATCHING_ROW; } }

        /// <summary>
        /// Котрагент подал декларацию / журнал учета
        /// </summary>
        public long? CONTRACTOR_KEY { get { return _invoiceData.CONTRACTOR_KEY; } }

        /// <summary>
        /// Порядковый номер
        /// </summary>
        public bool CONTRACTOR_DECL_IN_MC { get { return _invoiceData.CONTRACTOR_DECL_IN_MC; } }

        /// <summary>
        /// Порядковый номер
        /// </summary>
        public long? ORDINAL_NUMBER { get { return _invoiceData.ORDINAL_NUMBER; } }

        /// <summary>
        /// Код вида операции
        /// </summary>
        public string OPERATION_CODE { get { return _invoiceData.OPERATION_CODE; } }

        /// <summary>
        /// Номер счета-фактуры
        /// </summary>
        public string INVOICE_NUM { get { return _invoiceData.INVOICE_NUM; } }

        /// <summary>
        /// Дата счета-фактуры
        /// </summary>
        public DateTime? INVOICE_DATE { get { return _invoiceData.INVOICE_DATE; } }

        /// <summary>
        /// Номер исправления счета-фактуры 
        /// </summary>
        public string CHANGE_NUM { get { return _invoiceData.CHANGE_NUM; } }

        /// <summary>
        /// Дата исправления счета-фактуры
        /// </summary>
        public DateTime? CHANGE_DATE { get { return _invoiceData.CHANGE_DATE; } }

        /// <summary>
        /// Номер корректировочного счета-фактуры
        /// </summary>
        public string CORRECTION_NUM { get { return _invoiceData.CORRECTION_NUM; } }

        /// <summary>
        /// Дата корректировочного счета-фактуры
        /// </summary>
        public DateTime? CORRECTION_DATE { get { return _invoiceData.CORRECTION_DATE; } }

        /// <summary>
        /// Номер исправления корректировочного счета-фактуры
        /// </summary>
        public string CHANGE_CORRECTION_NUM { get { return _invoiceData.CHANGE_CORRECTION_NUM; } }

        /// <summary>
        /// Дата исправления корректировочного счета-фактуры
        /// </summary>
        public DateTime? CHANGE_CORRECTION_DATE { get { return _invoiceData.CHANGE_CORRECTION_DATE; } }

        /// <summary>
        /// Номер и дата документа, подтверждающего уплату налога
        /// </summary>
        public string RECEIPT_DOC_DATE_STR { get { return _invoiceData.RECEIPT_DOC_DATE_STR.ToString(); } }

        /// <summary>
        /// Дата принятия на учет товаров (работ, услуг), имущественных прав
        /// </summary>
        public DocDates BUY_ACCEPT_DATE { get { return _invoiceData.BUY_ACCEPT_DATE; } }

        /// <summary>
        /// ИНН продавца
        /// </summary>
        public string SELLER_INN { get { return _invoiceData.SELLER_INN; } }

        /// <summary>
        /// КПП продавца
        /// </summary>
        public string SELLER_KPP { get { return _invoiceData.SELLER_KPP; } }

        /// <summary>
        /// Наименование продавца
        /// </summary>
        public string SELLER_NAME { get { return _invoiceData.SELLER_NAME; } }

        /// <summary>
        /// ИНН посредника(комиссионера, агента, экспедитора или застройщика)
        /// </summary>
        public string BROKER_INN { get { return _invoiceData.BROKER_INN; } }

        /// <summary>
        /// КПП посредника (комиссионера, агента, экспедитора или застройщика)
        /// </summary>
        public string BROKER_KPP { get { return _invoiceData.BROKER_KPP; } }

        /// <summary>
        /// Наименование посредника(комиссионера, агента, экспедитора или застройщика)
        /// </summary>
        public string BROKER_NAME { get { return _invoiceData.BROKER_NAME; } }

        /// <summary>
        /// Дата выставления
        /// </summary>
        public DateTime? CREATE_DATE { get { return _invoiceData.CREATE_DATE; } }

        /// <summary>
        /// Дата получения
        /// </summary>
        public DateTime? RECEIVE_DATE { get { return _invoiceData.RECEIVE_DATE; } }

        /// <summary>
        /// ИНН покупателя
        /// </summary>
        public string BUYER_INN { get { return _invoiceData.BUYER_INN; } }

        /// <summary>
        /// КПП покупателя
        /// </summary>
        public string BUYER_KPP { get { return _invoiceData.BUYER_KPP; } }

        /// <summary>
        /// Наименование покупателя
        /// </summary>
        public string BUYER_NAME { get { return _invoiceData.BUYER_NAME; } }

        /// <summary>
        /// Номер таможенной декларации
        /// </summary>
        public string CUSTOMS_DECLARATION_NUM { get { return _invoiceData.CUSTOMS_DECLARATION_NUM; } }

        public string[] CUSTOMS_DECLARATION_NUM_FULL
        {
            get
            {
                return _invoiceData.CUSTOMS_DECLARATION_NUMBERS;
            }
            set
            {
                _invoiceData.CUSTOMS_DECLARATION_NUMBERS = value;
            }
        }

        /// <summary>
        /// Код валюты по ОКВ
        /// </summary>
        public string OKV_CODE { get { return _invoiceData.OKV_CODE; } }

        /// <summary>
        /// Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры
        /// </summary>
        public decimal? PRICE_BUY_AMOUNT { get { return _invoiceData.PRICE_BUY_AMOUNT; } }

        /// <summary>
        /// Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре принимаемая к вычету в рублях и копейках
        /// </summary>
        public decimal? PRICE_BUY_NDS_AMOUNT { get { return _invoiceData.PRICE_BUY_NDS_AMOUNT; } }

        /// <summary>
        /// Номер счета-фактуры продавца
        /// </summary>
        public string SELLER_INVOICE_NUM { get { return _invoiceData.SELLER_INVOICE_NUM; } }

        /// <summary>
        /// Дата счета-фактуры продавца
        /// </summary>
        public DateTime? SELLER_INVOICE_DATE { get { return _invoiceData.SELLER_INVOICE_DATE; } }

        /// <summary>
        /// Код вида сделки
        /// </summary>
        public int? DEAL_KIND_CODE { get { return _invoiceData.DEAL_KIND_CODE; } }

        /// <summary>
        /// Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог) в рублях и копейках
        /// </summary>
        public decimal? PRICE_SELL { get { return _invoiceData.PRICE_SELL; } }

        /// <summary>
        /// Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры
        /// </summary>
        public decimal? PRICE_SELL_IN_CURR { get { return _invoiceData.PRICE_SELL_IN_CURR; } }

        /// <summary>
        /// Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 18 %
        /// </summary>
        public decimal? PRICE_SELL_18 { get { return _invoiceData.PRICE_SELL_18; } }

        /// <summary>
        /// Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 10 %
        /// </summary>
        public decimal? PRICE_SELL_10 { get { return _invoiceData.PRICE_SELL_10; } }

        /// <summary>
        /// Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 0 %
        /// </summary>
        public decimal? PRICE_SELL_0 { get { return _invoiceData.PRICE_SELL_0; } }

        /// <summary>
        /// Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 18 %
        /// </summary>
        public decimal? PRICE_NDS_18 { get { return _invoiceData.PRICE_NDS_18; } }

        /// <summary>
        /// Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 10 %
        /// </summary>
        public decimal? PRICE_NDS_10 { get { return _invoiceData.PRICE_NDS_10; } }

        /// <summary>
        /// Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в рублях и копейках
        /// </summary>
        public decimal? PRICE_TAX_FREE { get { return _invoiceData.PRICE_TAX_FREE; } }

        /// <summary>
        /// Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре - всего
        /// </summary>
        public decimal? PRICE_TOTAL { get { return _invoiceData.PRICE_TOTAL; } }

        /// <summary>
        /// В том числе сумма НДС по счету-фактуре
        /// </summary>
        public decimal? PRICE_NDS_TOTAL { get { return _invoiceData.PRICE_NDS_TOTAL; } }

        /// <summary>
        /// Разница налога по корректировочному счету-фактуре – уменьшение
        /// </summary>
        public decimal? DIFF_CORRECT_DECREASE { get { return _invoiceData.DIFF_CORRECT_DECREASE; } }

        /// <summary>
        /// Разница налога по корректировочному счету-фактуре - увеличение
        /// </summary>
        public decimal? DIFF_CORRECT_INCREASE { get { return _invoiceData.DIFF_CORRECT_INCREASE; } }

        /// <summary>
        /// Разница стоимости с учетом налога по корректировочному счету-фактуре - уменьшение
        /// </summary>
        public decimal? DIFF_CORRECT_NDS_DECREASE { get { return _invoiceData.DIFF_CORRECT_NDS_DECREASE; } }

        /// <summary>
        /// Разница стоимости с учетом налога по корректировочному счету-фактуре - увеличение
        /// </summary>
        public decimal? DIFF_CORRECT_NDS_INCREASE { get { return _invoiceData.DIFF_CORRECT_NDS_INCREASE; } }

        /// <summary>
        /// Номер корректировки декларации
        /// </summary>
        public string DECL_CORRECTION_NUM { get { return _invoiceData.DECL_CORRECTION_NUM; } }

        /// <summary>
        /// Сумма налога, предъявляемая покупателю, в руб. и коп.
        /// </summary>
        public decimal? PRICE_NDS_BUYER { get { return _invoiceData.PRICE_NDS_BUYER; } }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: ИНН
        /// </summary>
        public string SELLER_AGENCY_INFO_INN { get { return _invoiceData.SELLER_AGENCY_INFO_INN; } }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: КПП
        /// </summary>
        public string SELLER_AGENCY_INFO_KPP { get { return _invoiceData.SELLER_AGENCY_INFO_KPP; } }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: Наименование
        /// </summary>
        public string SELLER_AGENCY_INFO_NAME { get { return _invoiceData.SELLER_AGENCY_INFO_NAME; } }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: № СФ
        /// </summary>
        public string SELLER_AGENCY_INFO_NUM { get { return _invoiceData.SELLER_AGENCY_INFO_NUM; } }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: дата СФ
        /// </summary>
        public DateTime? SELLER_AGENCY_INFO_DATE { get { return _invoiceData.SELLER_AGENCY_INFO_DATE; } }

        /// <summary>
        /// Идентификатор записи в Hbase
        /// </summary>
        public string ROW_KEY { get { return _invoiceData.ROW_KEY; } }

        /// <summary>
        /// Идентификатор записи в Hbase с которой было произведено сопоставление
        /// </summary>
        public string COMPARE_ROW_KEY { get { return _invoiceData.COMPARE_ROW_KEY; } }

        /// <summary>
        /// Номер раздела
        /// </summary>
        public int CHAPTER { get; set; }

        /// <summary>
        /// Сведения из дополнительного листа
        /// </summary>
        public bool IsAdditionalSheet 
        { 
            get 
            {
                return  (_invoiceData.CHAPTER == (int)DeclarationInvoicePartitionNumber.Chapter81 ||
                         _invoiceData.CHAPTER == (int)DeclarationInvoicePartitionNumber.Chapter91);
            } 
        }

        # endregion

        #region Conditions

        public bool AllowCustomDeclarationNumbersView
        {
            get
            {
                return !string.IsNullOrEmpty(CUSTOMS_DECLARATION_NUM) &&
                CUSTOMS_DECLARATION_NUM.Contains("...");
            }
        }

        #endregion
    }
}
