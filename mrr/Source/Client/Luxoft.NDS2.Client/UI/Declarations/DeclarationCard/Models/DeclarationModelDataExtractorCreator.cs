﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    class DeclarationModelDataExtractorCreator
    {
        public static DeclarationModelChapterDataExtractor CreateChapterDataExtractor(DeclarationModel model, DeclarationInvoiceChapterNumber declarationInvoiceChapterNumber)
        {
            return new DeclarationModelChapterDataExtractor(model, declarationInvoiceChapterNumber);
        }

        public static DeclarationModelActDataExtractor CreateDeclarationModelActDataExtractor(DeclarationModel model)
        {
            return new DeclarationModelActDataExtractor(model);
        }

        public static DeclarationModelDecisionDataExtractor CreateDeclarationModelDecisionDataExtractor(DeclarationModel model)
        {
            return new DeclarationModelDecisionDataExtractor(model);
        }

    }
}
