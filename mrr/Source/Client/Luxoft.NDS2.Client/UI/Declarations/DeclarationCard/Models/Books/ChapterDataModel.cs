﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс представляет модель данных раздела СФ НД (разделы 8 - 12)
    /// </summary>
    public class ChapterDataModel
    {
        # region Конструктор

        private readonly DeclarationInvoiceChapterNumber _chapter;

        /// <summary>
        /// Создает экземпляр класса ChapterDataModel
        /// </summary>
        /// <param name="chapter">Номер раздела НД</param>
        /// <param name="cache">Кэш сводных данных раздела</param>
        /// <param name="title">Модель заголовка раздела</param>
        public ChapterDataModel(
            DeclarationInvoiceChapterNumber chapter,
            ChapterTitleModel title,
            List<DeclarationInvoiceChapterInfo> cache)
        {
            if (cache == null)
            {
                throw new ArgumentNullException("cache");
            }

            if (title == null)
            {
                throw new ArgumentNullException("title");
            }

            _chapter = chapter;
            Cache = cache;
            _title = title;
            InitChapterTotalMatches();
            CacheSummaries = new Dictionary<string, decimal>();
        }

        # endregion

        # region Инициализация общего количества СФ

        private void InitChapterTotalMatches()
        {
            int totalQuantity = 0;
            if (_chapter == DeclarationInvoiceChapterNumber.Chapter8)
            {
                totalQuantity = GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter8);
                totalQuantity += GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter81);
            }
            if (_chapter == DeclarationInvoiceChapterNumber.Chapter9)
            {
                totalQuantity = GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter9);
                totalQuantity += GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter91);
            }
            if (_chapter == DeclarationInvoiceChapterNumber.Chapter10)
                totalQuantity = GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter10);
            if (_chapter == DeclarationInvoiceChapterNumber.Chapter11)
                totalQuantity = GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter11);
            if (_chapter == DeclarationInvoiceChapterNumber.Chapter12)
                totalQuantity = GetTotalQuantityFromCache(AskInvoiceChapterNumber.Chapter12);

            TotalMatches = totalQuantity;
        }

        private int GetTotalQuantityFromCache(AskInvoiceChapterNumber askChapter)
        {
            int totalQuantity = 0;
            var item = Cache.Where(p => p.Part == askChapter).SingleOrDefault();
            if (item != null)
                totalQuantity = item.TotalQuantity;
            return totalQuantity;
        }

        # endregion

        # region Обновление данных модели

        public void Update(DeclarationChapterData data)
        {

            Invoices = 
                data.Invoices != null 
                ? data.Invoices.Select(p => new InvoiceModel(p)).ToList() 
                : null;

            TotalMatches = data.MatchesQuantity;

            Summaries = data.Summaries;

            CacheSummaries.Clear();
            if (data.Summaries != null)
            {
                foreach (var item in data.Summaries)
                {
                    CacheSummaries.Add(item.Key, item.Value);
                }
            }

            foreach (var cachedData in Cache)
            {
                var updatedData = data.PartDataList.Find(cachedData.Part);

                if (updatedData != null)
                {
                    cachedData.DataReady = updatedData.DataReady;
                }
            }
        }

        # endregion

        # region Список счетов-фактур

        private List<InvoiceModel> _invoices = new List<InvoiceModel>();

        /// <summary>
        /// Возвращает или задает список 1 страницы счетов-фактур раздела НД
        /// </summary>
        public List<InvoiceModel> Invoices
        {
            get
            {
                return _invoices;
            }
            set
            {
                if (Equals(value, _invoices))
                {
                    return;
                }

                _invoices = value;

                var changeEventSubscribers = AfterInvoicesUpdated;

                if (changeEventSubscribers != null)
                {
                    changeEventSubscribers(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// Возвращает справочников итоговых сумм (ключ - название поля, значение - сумма)
        /// </summary>
        public Dictionary<string, decimal> Summaries
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает общее к-во счетов-фактур в разделе, удовлетворяющих фильтру
        /// TODO: Можно заполнить при создании из данных декларации если на раздел не наложен фильтр
        /// </summary>
        public int TotalMatches 
        {
            get
            {
                return _totalMatches;
            }
            set
            {
                _totalMatches = value;
            }
        }

        private int _totalMatches;

        /// <summary>
        /// Возвращает или задает пользовательский фильтр счетов-фактур в разделе НД
        /// </summary>
        public QueryConditions Conditions 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Очищает список сф в разделе НД
        /// </summary>
        public void ResetInvoices()
        {
            Invoices = new List<InvoiceModel>();
        }

        /// <summary>
        /// Срабатывает после изменения списка счетов-фактур НД
        /// </summary>
        public EventHandler AfterInvoicesUpdated;


        /// <summary>
        /// Пропустить запрос СФ, взять из кэша
        /// </summary>
        public bool IsSkipGetInvoices { get; set; }

        # endregion

        # region Сводные данные раздела

        public IEnumerable<DeclarationInvoiceChapterInfo> Cache
        {
            get;
            private set;
        }

        /// <summary>
        /// Кэш итоговых сумм (ключ - название поля, значение - сумма)
        /// </summary>
        public Dictionary<string, decimal> CacheSummaries
        {
            get;
            set;
        }

        /// <summary>
        /// Определяет необходимость подтверждения пользователем загрузки раздела НД
        /// </summary>
        /// <param name="warningThreshold">Пороговое количество счетов-фактур</param>
        /// <returns>Истина, если предупреждение необходимо</returns>
        public bool NeedWarning(int warningThreshold)
        {
            var notCachedData = Cache.Sum(data => data.DataReady ? 0 : data.TotalQuantity);

            return notCachedData >= warningThreshold;
        }

        /// <summary>
        /// Определяет общее к-во счетов фактур в разделе
        /// </summary>
        /// <returns>К-во сф</returns>
        public int GetTotalInvoiceQuantity()
        {
            return Cache.Sum(x => x.TotalQuantity);
        }

        #endregion

        #region Состояние данных раздела раздела

        public bool IsDataAvailable
        {
            get { return GetTotalInvoiceQuantity() > 0; }
        }

        public string DataStateDescription
        {
            get { return IsDataAvailable? 
                    string.Format("Данные загружены. Всего: {0} записей",GetTotalInvoiceQuantity())
                    : "Данные отсутствуют";
            }
        }

        #endregion

        #region Заголовок раздела

        protected ChapterTitleModel _title;

        /// <summary>
        /// Возвращает или задает сообщение об актуальности данных раздела
        /// </summary>
        public override string ToString()
        {
            return _title.GetTitle();
        }

        # endregion

        # region Мусор

        public bool IsInitialized 
        { 
            get; 
            set; 
        }

        # endregion
    }

}
