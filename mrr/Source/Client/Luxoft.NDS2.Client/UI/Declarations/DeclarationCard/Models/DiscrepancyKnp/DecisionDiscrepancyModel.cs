﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp
{
    public class DecisionDiscrepancyModel : KnpResultDiscrepancyModel
    {
        public DecisionDiscrepancyModel(KnpResultDiscrepancy dto) : base(dto) { }

        public decimal? AmountByAct
        {
            get
            {
                return _dto.AmountByAct;
            }
        }

        public string AmountByDecision
        {
            get
            {
                return GetEditableAmountString(_dto.AmountByDecision);
            }
            set
            {
                var normalizeValue = String.IsNullOrEmpty(value) ? "0" : value;
                var convertedValue = Convert.ToDecimal(normalizeValue);
                
                if (_dto.AmountByDecision != convertedValue)
                {
                    PreviousInvalidState = IsInvalid;
                    PreviousEditableAmountValue = _dto.AmountByDecision;
                    
                    _dto.AmountByDecision = convertedValue;

                    NotifyPropertyChanged("AmountByDecision");
                    NotifyDiscrepancyAmountChanged(convertedValue);
                }
            }
        }

        public override decimal GetEditableAmount()
        {
            return _dto.AmountByDecision.GetValueOrDefault();
        }

        public override void RollbackEditableAmount()
        {
            _dto.AmountByDecision = PreviousEditableAmountValue;

            NotifyPropertyChanged("AmountByDecision");
        }

        protected override bool InvalidCheck()
        {
            return (_dto.Status == DiscrepancyStatus.Opened)
                   && ((_dto.AmountByAct < _dto.AmountByDecision)
                        || (_dto.AmountByDecision <= 0));
        }
    }
}
