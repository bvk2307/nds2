﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует строителя модели 10 раздела НД
    /// </summary>
    public class ChapterTenModelBuilder: ChapterModelBuilderBase
    {
        public ChapterTenModelBuilder(DeclarationSummary declarationData)
            : base(DeclarationInvoiceChapterNumber.Chapter10, declarationData)
        {
        }

        public override ChapterTitleModel BuildTitle()
        {
            return SinglePartitionChapterTitleFactory.Factory(
                (int)Number,
                Data.CORRECTION_NUMBER_EFFECTIVE.ParseAsCorrectionNumber(),
                Data.AKT_NOMKORR_10);
        }

        public override List<DeclarationInvoiceChapterInfo> BuildCache()
        {
            var ret = new List<DeclarationInvoiceChapterInfo>();

            if (Data.ACTUAL_ZIP10.HasValue)
            {
                var cache =
                    new DeclarationInvoiceChapterInfo
                    {
                        Part = AskInvoiceChapterNumber.Chapter10,
                        Priority = Data.PRIORITY_10,
                        TotalQuantity = Data.INVOICE_COUNT10 ?? 0,
                        Zip = Data.ACTUAL_ZIP10.Value
                    };

                ret.Add(cache);
            }

            return ret;
        }
    }
}
