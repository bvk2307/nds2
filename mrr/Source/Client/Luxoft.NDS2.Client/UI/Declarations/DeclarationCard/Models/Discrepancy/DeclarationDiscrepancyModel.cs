﻿using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Discrepancy
{
    public class DeclarationDiscrepancyModel
    {
        private DeclarationDiscrepancy _declarationDiscrepancy;
        private const string _innKppSeparator = " / ";

        public DeclarationDiscrepancyModel()
        {
        }

        public DeclarationDiscrepancyModel(DeclarationDiscrepancy declarationDiscrepancy)
        {
            _declarationDiscrepancy = declarationDiscrepancy;
        }

        public DeclarationDiscrepancy DeclarationDiscrepancyRaw
        {
            get
            {
                return _declarationDiscrepancy;
            }
        }

        # region Атрибуты Расхождения

        [DisplayName("Номер")]
        [Description("Номер расхождения")]
        public long DiscrepancyId
        {
            get
            {
                return _declarationDiscrepancy.DiscrepancyId;
            }
        }

        [DisplayName("Сумма расхождения (руб.)")]
        [Description("Сумма расхождения в руб.")]
        [CurrencyFormat]
        public decimal Amount 
        { 
            get
            {
                return _declarationDiscrepancy.Amount;
            }
        }

        [DisplayName("ПВП расхождения (руб.)")]
        [Description("ПВП расхождения в руб.")]
        [CurrencyFormat]
        public decimal? AmountPvp 
        { 
            get
            {
                return _declarationDiscrepancy.AmountPvp;
            }
        }

        public string StatusCode
        {
            get
            {
                return _declarationDiscrepancy.StatusCode;
            }
        }

        [DisplayName("Статус")]
        [Description("Статус расхождения")]
        public string StatusName 
        { 
            get
            {
                return _declarationDiscrepancy.StatusName;
            }
        }

        public int TypeCode
        {
            get
            {
                return _declarationDiscrepancy.TypeCode;
            }
        }

        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public string TypeName
        {
            get
            {
                return _declarationDiscrepancy.TypeName;
            }
        }

        [DisplayName("Дата выявления")]
        [Description("Дата выявления расхождения")]
        public DateTime? FoundDate
        {
            get
            {
                return _declarationDiscrepancy.FoundDate;
            }
        }

        [DisplayName("Статус КНП")]
        [Description("Статус декларации")]
        public string KnpStatus
        { 
            get
            {
                return _declarationDiscrepancy.KnpStatus;
            }
        }

        # endregion

        # region Атрибуты НД\Журнала НП

        public long TaxPayerZip
        {
            get
            {
                return _declarationDiscrepancy.TaxPayerZip;
            }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string TaxPayerDeclSign 
        {
            get
            {
                return _declarationDiscrepancy.TaxPayerDeclSign;
            }
        
        }

        # endregion

        # region Атрибут счета-фактуры НП

        public string TaxPayerInvoiceKey
        {
            get
            {
                return _declarationDiscrepancy.TaxPayerInvoiceKey;
            }
        }

        [DisplayName("Источник")]
        [Description("Номер раздела/части")]
        public string TaxPayerInvoiceSource
        {
            get
            {
                return _declarationDiscrepancy.TaxPayerInvoiceSource;
            }
        }

        # endregion

        # region Атрибуты контрагента

        [DisplayName("ИНН")]
        [Description("ИНН НП с которым связано расхождение первичной отработки")]
        public string ContractorInn 
        {
            get
            {
                return _declarationDiscrepancy.ContractorInn;
            }
        }

        [DisplayName("КПП")]
        [Description("КПП НП с которым связано расхождение первичной отработки")]
        public string ContractorKpp 
        {
            get
            {
                return _declarationDiscrepancy.ContractorKpp;
            }
        }

        [DisplayName("Реорганизованный")]
        [Description("ИНН реорганизованного НП")]
        public string ContractorInnReorganized
        {
            get
            {
                return _declarationDiscrepancy.ContractorInnReorganized;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование НП с которым связано расхождение первичной отработки")]
        public string ContractorName 
        {
            get
            {
                return _declarationDiscrepancy.ContractorName;
            }
        }

        # endregion

        # region Атрибуты НД\Журнала контрагента 

        public long? ContractorZip
        {
            get
            {
                return _declarationDiscrepancy.ContractorZip;
            }
        }

        [DisplayName("Тип документа")]
        [Description("НД по НДС или Журнал учета")]
        public string ContractorDocType 
        {
            get
            {
                return _declarationDiscrepancy.ContractorDocType;
            }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string ContractorDeclSign 
        {
            get
            {
                return _declarationDiscrepancy.ContractorDeclSign;
            }
        }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public decimal? ContractorNdsAmount 
        {
            get
            {
                return _declarationDiscrepancy.ContractorNdsAmount;
            }
        }

        # endregion

        # region Атрибуты счета-фактуры контрагента

        public string ContractorInvoiceKey
        {
            get
            {
                return _declarationDiscrepancy.ContractorInvoiceKey;
            }
        }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ")]
        public string ContractorInvoiceSource 
        {
            get
            {
                return _declarationDiscrepancy.ContractorInvoiceSource;
            }
        }

        [DisplayName("Номер СФ")]
        [Description("Номер СФ")]
        public string CreatedByInvoiceNumber
        {
            get
            {
                return _declarationDiscrepancy.CreatedByInvoiceNumber;
            }
        }


        [DisplayName("Дата СФ")]
        [Description("Дата СФ")]
        public DateTime? CreatedByInvoiceDate
        {
            get
            {
                return _declarationDiscrepancy.CreatedByInvoiceDate;
            }
        }


        # endregion

        #region Атрибуты Актов/Решений

        [DisplayName("Включено в акт НП")]
        [Description("Расхождение включено в акт налогоплательщика")]
        public string ActInn
        {
            get
            {
                return String.IsNullOrEmpty(_declarationDiscrepancy.ActKpp)
                        ? _declarationDiscrepancy.ActInn
                        : String.Concat(
                                    _declarationDiscrepancy.ActInn,
                                    _innKppSeparator,
                                    _declarationDiscrepancy.ActKpp);
            }
        }

        [DisplayName("Тип документа")]
        [Description("Тип документа")]
        public string KnpDocType
        {
            get
            {
                return _declarationDiscrepancy.KnpDocType;
            }
        }

        [DisplayName("Сумма НДС по документу (руб.)")]
        [Description("Сумма НДС по документу в рублях")]
        public decimal? KnpDocAmount
        {
            get
            {
                return _declarationDiscrepancy.KnpDocAmount;
            }
        }

        [DisplayName("Сумма неустраненных расхождений (руб.)")]
        [Description("Сумма неустраненных расхождений в рублях")]
        public decimal? DecisionAmountDifference
        {
            get
            {
                return _declarationDiscrepancy.DecisionAmountDifference;
            }
        }

        # endregion    
    }
}
