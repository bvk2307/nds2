﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    public class TitleChapterParameters
    {
        public int Chapter { get; set; }

        public int? CorrectionCurrent { get; set; }

        public int? CorrectionNumberActual { get; set; }

        public int? CorrectionNumberWithDopList { get; set; }
    }

    /// <summary>
    /// Этот класс реализует фабричный метод создания модели заголовка разделов 10-12 НД
    /// </summary>
    public static class SinglePartitionChapterTitleFactory
    {
        private static string MESSAGE_CORRECT_PREVIOUS = "Данные по предыдущей корректировке {0}";

        private static string MESSAGE_CORRECT_CURRENT = "Данные по <b>текущей</b> корректировке {0}";

        public static ChapterTitleModel Factory(
            int chapter, 
            int? corrNumberCurrent, 
            int? corrNumberActual)
        {
            var parameters = new TitleChapterParameters()
            {
                Chapter = chapter,
                CorrectionCurrent = corrNumberCurrent,
                CorrectionNumberActual = corrNumberActual,
                CorrectionNumberWithDopList = null,
            };

            return new ChapterTitleModel(
                new List<ChapterTitleBuilder>
                {
                    new TitleChapterBuilderMessageCorrectPrevious(parameters, MESSAGE_CORRECT_PREVIOUS),
                    new TitleChapterBuilderMessageCorrectCurrent(parameters, MESSAGE_CORRECT_CURRENT)
                });
        }
    }

    /// <summary>
    /// Этот класс реализует фабричный метод создания модели заголовка разделов 8-9 НД
    /// </summary>
    public static class MultiPartitionChapterTitleFactory
    {
        private static string MESSAGE_MAIN_CHAPTER_ACTUAL = "по <b>текущей</b> корректировке {0} (раздел {1})";

        private static string MESSAGE_MAIN_CHAPTER_OTHER = "Данные по корректировке {0} (раздел {1})";

        private static string MESSAGE_WITHDOPLIST_CHAPTER_ACTUAL = "Данные по <b>текущей</b> корректировке {0} (приложение 1 к разделу {1})";

        private static string MESSAGE_WITHDOPLIST_CHAPTER_OTHER = "Данные по корректировке {0} (приложение 1 к разделу {1})";

        private static string MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_ACTUAL_EQUALLY = "Данные по <b>текущей</b> корректировке {0}";

        private static string MESSAGE_MAIN_CHAPTER_ACTUAL_AND_WITHDOPLIST_OTHER = "Данные по <b>текущей</b> корректировке {0} (раздел {1}) и по корректировке {2} (приложение 1 к разделу {1})";

        private static string MESSAGE_MAIN_CHAPTER_OTHER_AND_WITHDOPLIST_ACTUAL = "Данные по корректировке {0} (раздел {1}) и по <b>текущей</b> корректировке {2} (приложение 1 к разделу {1})";

        private static string MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_OTHER_EQUALLY = "Данные по корректировке {0} (раздел {1} и приложение 1 к разделу {1})";

        private static string MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_OTHER_DIFFRENT = "Данные по корректировке {0} (раздел {1}) и по корректировке {2} (приложение 1 к разделу {1})";

        public static ChapterTitleModel Factory(
            int chapter, 
            int? corrNumberCurrent, 
            int? corrNumberActual, 
            int? corrNumberActualWithDopList)
        {
            var parameters = new TitleChapterParameters()
            {
                Chapter = chapter,
                CorrectionCurrent = corrNumberCurrent,
                CorrectionNumberActual = corrNumberActual,
                CorrectionNumberWithDopList = corrNumberActualWithDopList,
            };

            return new ChapterTitleModel(
                new List<ChapterTitleBuilder> 
                { 
                    new TitleChapterBuilderMainChapterActual(parameters, MESSAGE_MAIN_CHAPTER_ACTUAL),
                    new TitleChapterBuilderMainChapterOther(parameters, MESSAGE_MAIN_CHAPTER_OTHER),
                    new TitleChapterBuilderWithDopListChapterActual(parameters, MESSAGE_WITHDOPLIST_CHAPTER_ACTUAL),
                    new TitleChapterBuilderWithDopListChapterOther(parameters, MESSAGE_WITHDOPLIST_CHAPTER_OTHER),
                    new TitleChapterBuilderMainChapterAndWithDopListActualEqually(parameters, MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_ACTUAL_EQUALLY),
                    new TitleChapterBuilderMainChapterActualAndWithDopListOther(parameters, MESSAGE_MAIN_CHAPTER_ACTUAL_AND_WITHDOPLIST_OTHER),
                    new TitleChapterBuilderMainChapterOtherAndWithDopListActual(parameters, MESSAGE_MAIN_CHAPTER_OTHER_AND_WITHDOPLIST_ACTUAL),
                    new TitleChapterBuilderMainChapterAndWithDopListOtherEqually(parameters, MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_OTHER_EQUALLY), 
                    new TitleChapterBuilderMainChapterAndWithDopListOtherDiffrent(parameters, MESSAGE_MAIN_CHAPTER_AND_WITHDOPLIST_OTHER_DIFFRENT)
                });
        }
    }
}
