﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models
{
    public class DeclarationModelDecisionDataExtractor
    {
        private readonly DeclarationModel _model;

        public DeclarationModelDecisionDataExtractor(DeclarationModel model)
        {
            _model = model;
        }

        public bool HasKnpDecision()
        {
            bool ret = false;

            if (_model.SeodDecisionDocuments != null)
            {
                ret = _model.SeodDecisionDocuments.Contains(KnpDecisionType.BringToJustice)
                       || _model.SeodDecisionDocuments.Contains(KnpDecisionType.RefuseToJutice);
            }
            
            return ret;
        }
    }
}
