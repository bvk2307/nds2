﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public class KnpDocumentStateClosed : KnpDocumentState
    {
        private const bool _active = true;
        
        public KnpDocumentStateClosed()
            : base(_active)
        {
        }

        #region overrided IDiscrepancyKnpDocumentState

        public override bool SelectionAvailable()
        {
            return Blocked();
        }

        public override bool EditAvailable()
        {
            return Blocked();
        }

        #endregion

    }
}
