﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует строителя модели 11 раздела НД
    /// </summary>
    public class ChapterElevenModelBuilder : ChapterModelBuilderBase
    {
        public ChapterElevenModelBuilder(DeclarationSummary declarationData)
            : base(DeclarationInvoiceChapterNumber.Chapter11, declarationData)
        {
        }

        public override ChapterTitleModel BuildTitle()
        {
            return SinglePartitionChapterTitleFactory.Factory(
                (int)Number,
                Data.CORRECTION_NUMBER_EFFECTIVE.ParseAsCorrectionNumber(),
                Data.AKT_NOMKORR_11);
        }

        public override List<DeclarationInvoiceChapterInfo> BuildCache()
        {
            var ret = new List<DeclarationInvoiceChapterInfo>();

            if (Data.ACTUAL_ZIP11.HasValue)
            {
                var cache =
                    new DeclarationInvoiceChapterInfo
                        {
                            Part = AskInvoiceChapterNumber.Chapter11,
                            Priority = Data.PRIORITY_11,
                            TotalQuantity = Data.INVOICE_COUNT11 ?? 0,
                            Zip = Data.ACTUAL_ZIP11.Value
                        };

                ret.Add(cache);
            }

            return ret;
        }
    }
}