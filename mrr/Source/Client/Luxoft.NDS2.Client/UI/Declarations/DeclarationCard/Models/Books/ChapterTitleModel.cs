﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует абстрактную модель заголовка раздела СФ НД
    /// </summary>
    public class ChapterTitleModel
    {
        private readonly List<ChapterTitleBuilder> _titleChapterBuilders;

        /// <summary>
        /// Создает экземпляр класса ChapterTitleModel
        /// </summary>
        /// <param name="titleChapterBuilders"></param>
        public ChapterTitleModel(List<ChapterTitleBuilder> titleChapterBuilders)
        {
            if (titleChapterBuilders == null)
            {
                throw new ArgumentNullException("titleChapterBuilders");
            }

            _titleChapterBuilders = titleChapterBuilders;
        }

        /// <summary>
        /// Строит заголовок раздела НД
        /// </summary>
        /// <returns>Текст заголовка</returns>
        public string GetTitle()
        {
            return _titleChapterBuilders.First(builder => builder.IsApplicable()).Build();
        }
    }
}
