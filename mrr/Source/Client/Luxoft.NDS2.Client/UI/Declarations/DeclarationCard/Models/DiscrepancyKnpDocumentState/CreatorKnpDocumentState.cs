﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public class CreatorKnpDocumentState
    {
        public IDiscrepancyKnpDocumentState Create(long? documentId, bool documentHasData, bool documentIsClosed, bool initialState)
        {
            if (!documentId.HasValue)
                return new KnpDocumentStateEmpty(initialState);

            if (documentIsClosed)
                return new KnpDocumentStateClosed();

            if (!documentHasData)
                return new KnpDocumentStateEmpty(initialState);

            return new KnpDocumentStateEdit(initialState);
        }
    }
}
