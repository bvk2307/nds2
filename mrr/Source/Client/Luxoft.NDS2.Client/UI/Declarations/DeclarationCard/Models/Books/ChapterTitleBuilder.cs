﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    public abstract class ChapterTitleBuilder
    {
        protected readonly int? corrNumberCurrent;

        protected readonly int? corrNumberActual;

        protected readonly int? corrNumberActualWithDopList;

        protected readonly int chapter;

        protected readonly string messagePattern;

        public ChapterTitleBuilder(TitleChapterParameters parameters, string messagePattern)
        {
            this.corrNumberCurrent = parameters.CorrectionCurrent;
            this.corrNumberActual = parameters.CorrectionNumberActual;
            this.corrNumberActualWithDopList = parameters.CorrectionNumberWithDopList;
            this.chapter = parameters.Chapter;
            this.messagePattern = messagePattern;
        }

        public virtual bool IsApplicable()
        {
            return true;
        }

        public virtual int? GetCorrectionNumberFirst()
        {
            return this.corrNumberActual;
        }

        public virtual int? GetCorrectionNumberSecond()
        {
            return this.corrNumberActualWithDopList;
        }

        public virtual string Build()
        {
            return string.Format(messagePattern, GetCorrectionNumberFirst(), this.chapter, GetCorrectionNumberSecond());
        }
    }

    public class TitleChapterBuilderMainChapterActual : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterActual(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual != null && corrNumberActualWithDopList == null && corrNumberActual == corrNumberCurrent);
        }
    }

    public class TitleChapterBuilderMainChapterOther : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterOther(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual != null && corrNumberActualWithDopList == null && corrNumberActual != corrNumberCurrent);
        }
    }

    public class TitleChapterBuilderWithDopListChapterActual : ChapterTitleBuilder
    {
        public TitleChapterBuilderWithDopListChapterActual(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual == null && corrNumberActualWithDopList != null && corrNumberActualWithDopList == corrNumberCurrent);
        }

        public override int? GetCorrectionNumberFirst()
        {
            return this.corrNumberActualWithDopList;
        }

        public override int? GetCorrectionNumberSecond()
        {
            return null;
        }
    }

    public class TitleChapterBuilderWithDopListChapterOther : ChapterTitleBuilder
    {
        public TitleChapterBuilderWithDopListChapterOther(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual == null && corrNumberActualWithDopList != null && corrNumberActualWithDopList != corrNumberCurrent);
        }

        public override int? GetCorrectionNumberFirst()
        {
            return this.corrNumberActualWithDopList;
        }

        public override int? GetCorrectionNumberSecond()
        {
            return null;
        }
    }

    public class TitleChapterBuilderMainChapterAndWithDopListActualEqually : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterAndWithDopListActualEqually(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return ((corrNumberActual != null && corrNumberActualWithDopList != null) &&
                    (corrNumberActual == corrNumberCurrent && corrNumberActualWithDopList == corrNumberCurrent));
        }
    }

    public class TitleChapterBuilderMainChapterActualAndWithDopListOther : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterActualAndWithDopListOther(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return ((corrNumberActual != null && corrNumberActualWithDopList != null) &&
                    (corrNumberActual == corrNumberCurrent && corrNumberActualWithDopList != corrNumberCurrent));
        }
    }

    public class TitleChapterBuilderMainChapterOtherAndWithDopListActual : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterOtherAndWithDopListActual(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return ((corrNumberActual != null && corrNumberActualWithDopList != null) &&
                    (corrNumberActual != corrNumberCurrent && corrNumberActualWithDopList == corrNumberCurrent));
        }
    }

    public class TitleChapterBuilderMainChapterAndWithDopListOtherEqually : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterAndWithDopListOtherEqually(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return ((corrNumberActual != null && corrNumberActualWithDopList != null) &&
                    (corrNumberActual != corrNumberCurrent && corrNumberActualWithDopList != corrNumberCurrent && corrNumberActual == corrNumberActualWithDopList));
        }
    }

    public class TitleChapterBuilderMainChapterAndWithDopListOtherDiffrent : ChapterTitleBuilder
    {
        public TitleChapterBuilderMainChapterAndWithDopListOtherDiffrent(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return ((corrNumberActual != null && corrNumberActualWithDopList != null) &&
                    (corrNumberActual != corrNumberCurrent && corrNumberActualWithDopList != corrNumberCurrent && corrNumberActual != corrNumberActualWithDopList));
        }
    }

    public class TitleChapterBuilderMessageCorrectPrevious : ChapterTitleBuilder
    {
        public TitleChapterBuilderMessageCorrectPrevious(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual != null && corrNumberCurrent != corrNumberActual);
        }
    }

    public class TitleChapterBuilderMessageCorrectCurrent : ChapterTitleBuilder
    {
        public TitleChapterBuilderMessageCorrectCurrent(TitleChapterParameters parameters, string messagePattern)
            : base(parameters, messagePattern) { }

        public override bool IsApplicable()
        {
            return (corrNumberActual != null && corrNumberCurrent == corrNumberActual);
        }
    }
}
