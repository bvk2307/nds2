﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books
{
    /// <summary>
    /// Этот класс реализует строителя модели 9 раздела НД
    /// </summary>
    public class ChapterNineModelBuilder : ChapterModelBuilderBase
    {
        public ChapterNineModelBuilder(DeclarationSummary declarationData)
            : base(DeclarationInvoiceChapterNumber.Chapter9, declarationData)
        {
        }

        public override ChapterTitleModel BuildTitle()
        {
            return MultiPartitionChapterTitleFactory.Factory(
                (int)Number,
                Data.CORRECTION_NUMBER_EFFECTIVE.ParseAsCorrectionNumber(),
                Data.AKT_NOMKORR_9,
                Data.AKT_NOMKORR_91);
        }

        public override List<DeclarationInvoiceChapterInfo> BuildCache()
        {
            var ret = new List<DeclarationInvoiceChapterInfo>();

            if (Data.ACTUAL_ZIP9.HasValue)
            {
                var mainCache =
                    new DeclarationInvoiceChapterInfo
                    {
                        Part = AskInvoiceChapterNumber.Chapter9,
                        Priority = Data.PRIORITY_09,
                        TotalQuantity = Data.INVOICE_COUNT9 ?? 0,
                        Zip = Data.ACTUAL_ZIP9.Value
                    };

                ret.Add(mainCache);
            }


            if (Data.ACTUAL_ZIP91.HasValue)
            {
                var extensionCache =
                    new DeclarationInvoiceChapterInfo
                    {
                        Part = AskInvoiceChapterNumber.Chapter91,
                        Priority = Data.PRIORITY_91,
                        TotalQuantity = Data.INVOICE_COUNT91 ?? 0,
                        Zip = Data.ACTUAL_ZIP91.Value
                    };

                ret.Add(extensionCache);
            }


            return ret; 
        }
    }
}
