﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp
{
    public class ActDiscrepancyModel : KnpResultDiscrepancyModel
    {
        public ActDiscrepancyModel(KnpResultDiscrepancy dto) : base(dto) { }

        public string AmountByAct
        {
            get 
            {
                return GetEditableAmountString(_dto.AmountByAct);
            }
            set
            {
                var normalizeValue = String.IsNullOrEmpty(value) ? "0" : value;
                var convertedValue = Convert.ToDecimal(normalizeValue);

                if (_dto.AmountByAct != convertedValue)
                {
                    PreviousInvalidState = IsInvalid;
                    PreviousEditableAmountValue = _dto.AmountByAct;

                    _dto.AmountByAct = convertedValue;

                    NotifyPropertyChanged("AmountByAct");
                    NotifyDiscrepancyAmountChanged(convertedValue);
                }
            }
        }

        public override decimal GetEditableAmount()
        {
            return _dto.AmountByAct.GetValueOrDefault();
        }

        public override void RollbackEditableAmount()
        {
            _dto.AmountByAct = PreviousEditableAmountValue;

            NotifyPropertyChanged("AmountByAct");
        }

        protected override bool InvalidCheck()
        {
            return (_dto.Status == DiscrepancyStatus.Opened)
                   && ((_dto.Amount < _dto.AmountByAct)
                        || (_dto.AmountByAct <= 0));
        }

    }
}
