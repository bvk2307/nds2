﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnpDocumentState
{
    public class KnpDocumentStateEdit : KnpDocumentState
    {
        public KnpDocumentStateEdit(bool initialState)
            :base(initialState)
        {
        }

        #region overrided IDiscrepancyKnpDocumentState

        public override bool ViewAvailable()
        {
            return Blocked();
        }

        #endregion

    }
}
