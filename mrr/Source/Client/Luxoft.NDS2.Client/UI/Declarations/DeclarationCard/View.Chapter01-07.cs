﻿using System;
using System.Windows.Forms;
using Rnivc.CoreComponents.UniDocument;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public partial class View
    {
        /// <summary>
        /// ACHTUNG !
        /// Контролл UniDocumentControl вынесен из контструктора в Onload из-за сбоя
        /// генерируемого им в Design-mode при инициализации свойства Document
        /// возникает каждый раз при корректировке и сохранинии формы содержащей данный контрол
        /// что здорово мешает верстке формы
        /// </summary>

        UniDocumentControl declarationRnivcData = new UniDocumentControl()
        {
            Dock = System.Windows.Forms.DockStyle.Fill,
            Location = new System.Drawing.Point(0, 29),
            MinimumSize = new System.Drawing.Size(200, 175),
            Name = "declarationRnivcData",
            Size = new System.Drawing.Size(797, 317),
            TabIndex = 0,
            Visible = false
        };

        private void CreateControlRNIVC()
        {
            rnivcWrapperPanel.ClientArea.Controls.Add(declarationRnivcData);

            var xmlTemlate = _presenter.GetTemplateRnivc();

            var wrp = new LocalRepositoryWrapper(string.Empty, xmlTemlate);

            var serviceProvider = new ServiceCollection(wrp);

            var mode =
                new UniDocumentMode(
                    new UniEnvironment(
                        serviceProvider,
                        new UniDocumentInfo("0"),
                        DocumentStage.Stage1_Register,
                        0),
                    true
                    );

            declarationRnivcData.NewDocument(mode, LocalRepositoryWrapper.DEFAULT_TEMPLATE_ID);

            declarationRnivcData.Activate();
            declarationRnivcData.Refresh();
            declarationRnivcData.SetReadOnly();

            progressBar.Visible = true;       
        }


        public void SetRnivcData(string xmlData)
        {
            var gvnCode = xmlData.Replace("Файл505", "Файл");
            var xmlTemlate = _presenter.GetTemplateRnivc();

            if (!string.IsNullOrEmpty(xmlTemlate) && !string.IsNullOrEmpty(gvnCode))
            {
                var uniTemplate = UniTemplateEngine.Load(xmlTemlate);

                var uniDocument = UniDocumentEngine.TransformToUni(uniTemplate, gvnCode, UniDocumentTransformType.FromFNSXml);

                var rezStr = UniDocumentEngine.SaveXml(uniTemplate, uniDocument);

                var wrp = new LocalRepositoryWrapper(rezStr, xmlTemlate);

                var serviceProvider = new ServiceCollection(wrp);

                var mode =
                    new UniDocumentMode(
                        new UniEnvironment(
                            serviceProvider,
                            new UniDocumentInfo("0"),
                            DocumentStage.Stage1_Register,
                            0),
                        true
                        );

                
                declarationRnivcData.DocumentLoad(mode);
                declarationRnivcData.Activate();
                declarationRnivcData.Refresh();
                declarationRnivcData.SetReadOnly();
            }

            progressBar.Visible = false;
        }


        public override void OnActivate()
        {
            declarationRnivcData.Visible = true;
        }

        public override void OnDeactivate()
        {
            declarationRnivcData.Visible = false;
        }


    }
}
