﻿using System;

using Rnivc.CoreComponents.UniDocument;


namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    /// <summary>
    /// Класс сервисной коллекции
    /// </summary>
    internal class ServiceCollection : IServiceProvider
    {
        private readonly IUniDocumentResolver _resolver;
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// Создает новый экземпляр класса <see cref="ServiceCollection"/>.
        /// </summary>
        /// <param name="repository">Провайдер для хранилища документов и библиотеки шаблонов.</param>
        public ServiceCollection(IUniDocumentRepository repository)
        {
            // Создаем коллекцию сервисов для UniDocument.
            // Содержит внутренний сервис инструментирования.
            _serviceProvider = new UniDocumentServiceProvider();

            // Создаем Резолвер
            _resolver = UniDocumentResolverManager.Create(repository, false);
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType">An object that specifies the type of service object to get.</param>
        /// <returns>
        /// A service object of type <paramref name="serviceType" />.-or- null if there is no service object of type <paramref name="serviceType" />.
        /// </returns>
        public object GetService(Type serviceType)
        {
            //return serviceType == typeof(IUniDocumentResolver) ? _resolver : _serviceProvider.GetService(serviceType);

            if (serviceType == typeof(IUniDocumentResolver))
                return _resolver;

            object ret = null;

            try
            {
                ret = _serviceProvider.GetService(serviceType);
            }
            catch (Exception ex)
            {
            }
            
            return ret;
        }
    }
}
