﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;



namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    /// <summary>
    ///     Класс реализует загрузку списка расхождений
    /// </summary>
    class DiscrepancyListDataLoader : ServiceProxyBase<PageResult<DeclarationDiscrepancy>>, IServerGridDataProvider<PageResult<DeclarationDiscrepancy>>
    { 
        private readonly IDiscrepancyForDeclarationCardService _service;
        private readonly HiveRequestDiscrepanciesList _hiveRequestData; 
        private readonly List<string> _userOperations; 
        
        public DiscrepancyListDataLoader(
            IDiscrepancyForDeclarationCardService service,
            INotifier notifier,
            IClientLogger logger, 
            HiveRequestDiscrepanciesList hiveRequestData, 
            List<string> userOperations 
            ) : base(notifier, logger)
        {
            _service = service;
            _hiveRequestData = hiveRequestData;
            _userOperations = userOperations;
        }

        public event EventHandler<DataLoadedEventArgs<PageResult<DeclarationDiscrepancy>>> DataLoaded;
        
        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(
                () => _userOperations.Contains(Constants.SystemPermissions.Operations.DiscrepancyAllView)
                    ? _service.GetAllDiscrepancyList(
                        queryConditions,
                        _hiveRequestData)
                    : _service.GetKnpDiscrepancyList(
                        queryConditions,
                        _hiveRequestData.Inn,
                        _hiveRequestData.KppEffective,
                        _hiveRequestData.FiscalYear,
                        _hiveRequestData.PeriodEffective));
        }

        protected override void CallBack(PageResult<DeclarationDiscrepancy> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<PageResult<DeclarationDiscrepancy>>(result));
        }
    }
}
