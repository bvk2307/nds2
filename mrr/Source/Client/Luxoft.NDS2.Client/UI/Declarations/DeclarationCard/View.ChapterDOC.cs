﻿using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {
        private void InitializeGridClaims()
        {
            KnpDocumentsView().RowSelected += (sender, arg) => { UpdateOpenButtonStatus(); };
        }

        private void UpdateOpenButtonStatus()
        {
            KnpDocument doc = (KnpDocument)KnpDocumentsView().SelectedItem;
            if (doc != null && doc.GetId() != default(long))
            {
                btnOpen.Enabled = doc.AllowView();
            }
            else
            {
                btnOpen.Enabled = false;
            }
            _presenter.RefreshEKP();
        }
    }
}
