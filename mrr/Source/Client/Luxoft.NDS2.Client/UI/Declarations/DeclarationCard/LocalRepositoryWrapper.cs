﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CommonComponents.Catalog;
using Rnivc.CoreComponents.UniDocument;
using Rnivc.CoreComponents.UniDocument.Instrumentation;


namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    /// <summary>
    /// Провайдер для локального файлового хранилища документов и библиотеки шаблонов.
    /// </summary>
    public sealed class LocalRepositoryWrapper : UniDocumentRepositoryBase
    {
        public const string DEFAULT_TEMPLATE_ID = "1151001_NDS_5_04_Template";
        private string _file;
        private string _template;
        
        public LocalRepositoryWrapper(string f, string t)
        {
            _file = f;
            _template = t;
        }

 
        /// <summary>
        /// Внутренний метод.
        /// Проверяет наличие документа с указанным идентификатором в хранилище.
        /// </summary>
        /// <param name="entry">Событие профилирования.</param>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>
        ///   <c>true</c> если документ есть в хранилище;в противном случае, <c>false</c>.
        /// </returns>
        protected override bool DocumentWithIdExistsInternal(IMonitoringEntry entry, string id)
        {
            return true;
        }

        /// <summary>
        /// Внутренний метод.
        /// Возвращает Xml-содержимое документа по указанному идентификатору.
        /// </summary>
        /// <param name="entry">Событие профилирования.</param>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>
        /// Документ в виде строки.
        /// </returns>
        protected override string GetDocumentXmlByIdInternal(IMonitoringEntry entry, string id)
        {
            return _file;
        }

        /// <summary>
        /// Внутренний метод.
        /// Возвращает Xml-содержимое шаблона по указанному идентификатору.
        /// </summary>
        /// <param name="entry">Событие профилирования.</param>
        /// <param name="id">Идентификатор шаблона.</param>
        /// <returns>
        /// Шаблон в виде строки.
        /// </returns>
        protected override string GetTemplateXmlByIdInternal(IMonitoringEntry entry, string id)
        {
            return _template;
        }

        /// <summary>
        /// Возвращает ID шаблона на основе которого создан документ с указанным идентификатором.
        /// </summary>
        /// <param name="entry">Событие профилирования.</param>
        /// <param name="documentId">Идентификатор документа.</param>
        /// <returns>
        /// Идентификатор шаблона.
        /// </returns>
        protected override string GetDocumentTemplateIdInternal(IMonitoringEntry entry, string documentId)
        {
            return DEFAULT_TEMPLATE_ID;
        }

        /// <summary>
        /// Внутренний метод.
        /// Проверяет наличие шаблона с указанным идентификатором в хранилище.
        /// </summary>
        /// <param name="entry">Событие профилирования.</param>
        /// <param name="id">Идентификатор шаблона.</param>
        /// <returns>
        ///   <c>true</c> если шаблон есть в хранилище;в противном случае, <c>false</c>.
        /// </returns>
        protected override bool TemplateWithIdExistsInternal(IMonitoringEntry entry, string id)
        {
            return id == DEFAULT_TEMPLATE_ID;
        }
    }
}
