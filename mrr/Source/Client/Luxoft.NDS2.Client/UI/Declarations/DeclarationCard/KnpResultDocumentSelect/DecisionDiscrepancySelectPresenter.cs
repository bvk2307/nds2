﻿using FLS.CommonComponents.Lib.Execution;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    public class DecisionDiscrepancySelectPresenter : KnpResultDocumentSelectPresenter<DecisionDiscrepancyModel>
    {
        public DecisionDiscrepancySelectPresenter(
            long knpResultDiscrepancyDocumentId, 
            IKnpResultDocumentSelectView view, 
            IServiceRequestWrapper serviceRequester,
            IAppendDocumentDiscrepancyService service,
            IThreadInvoker uiThreadExecutor,
            IServiceCallErrorHandler serviceErrorHandler,
            ISettingsProvider settingsProvider,
            IDeclarationsDataService validatorService,
            long comparasionDataVersion)
        : base(
            knpResultDiscrepancyDocumentId, 
            view, 
            serviceRequester, 
            service, 
            uiThreadExecutor, 
            serviceErrorHandler, 
            settingsProvider,
            validatorService,
            comparasionDataVersion)
        {
        }

        protected override DecisionDiscrepancyModel ActDiscrepanciesListItemFabricMethod(KnpResultDiscrepancy dto)
        {
            return new DecisionDiscrepancyModel(dto);
        }


    }
}
