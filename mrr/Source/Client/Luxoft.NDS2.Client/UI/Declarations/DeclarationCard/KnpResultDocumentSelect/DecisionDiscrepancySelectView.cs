﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    public partial class DecisionDiscrepancySelectView : KnpResultDocumentSelectView
    {
        public DecisionDiscrepancySelectView()
        {
            InitializeComponent();
        }

        protected override GridColumnSetup SetupGridColumn()
        {
            var setup = base.SetupGridColumn();
            var helper = new ColumnHelper<DecisionDiscrepancyModel>(_actDiscrepancyGrid);

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.Source))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 8;
                    d.Width -= 8;
                });

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.ContractorKpp))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 15;
                    d.Width -= 15;
                });

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.ContractorName))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 20;
                    d.Width -= 20;
                });

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.InvoiceNumber))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 20;
                    d.Width -= 20;
                });

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.InvoiceAmountNds))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 10;
                    d.Width -= 10;
                });

            setup.Columns.FindSingle(TypeHelper<DecisionDiscrepancyModel>.GetMemberName(o => o.Amount))
                .Configure(d =>
                {
                    d.DisableMoving = false;
                    d.MinWidth -= 10;
                    d.Width -= 10;
                });

            setup.Columns.Add(helper.CreateNumberColumn(o => o.AmountByAct, 2, 100).Configure(d =>
            {
                d.Caption = "Сумма по акту";
                d.HeaderToolTip = "Сумма по акту";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));

            return setup;
        }

    }
}
