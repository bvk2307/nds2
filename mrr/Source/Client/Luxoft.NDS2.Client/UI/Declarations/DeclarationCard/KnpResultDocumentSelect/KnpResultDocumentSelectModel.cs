﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    /// <summary>
    /// Класс-модель для окна выбора расхождений в Акты/Решения
    /// </summary>
    /// <typeparam name="TActDiscrepanciesListItemModel">Тип отображаемых данных в списке</typeparam>
    public class KnpResultDocumentSelectModel<TActDiscrepanciesListItemModel> 
        where TActDiscrepanciesListItemModel : KnpResultDiscrepancyModel
    {
        private const int _defaultPageSize = 50;

        /// <summary>
        /// Конструктор - централизованная инициализация класса модели
        /// </summary>
        /// <param name="knpResultDiscrepancyDocumentId">Идентификатор документа</param>
        public KnpResultDocumentSelectModel(long knpResultDiscrepancyDocumentId)
        {
            Id = knpResultDiscrepancyDocumentId;

            ActDiscrepancyListPager = new PagerStateViewModel()
            {
                PageSize = _defaultPageSize
            };

            ActDiscrepancies = new List<TActDiscrepanciesListItemModel>();
        }


        /// <summary>
        /// Идентификатор документа
        /// </summary>
        public long Id { get; private set; }

        /// <summary>
        /// Список актированных расхождений
        /// </summary>
        public List<TActDiscrepanciesListItemModel> ActDiscrepancies { get; private set; }

        /// <summary>
        /// Модель пейджера страниц списка
        /// </summary>
        public IPager ActDiscrepancyListPager { get; private set; }

        /// <summary>
        /// Общее количество актированных расхождений
        /// </summary>
        public int TotalAialibleActDiscrepancies { get; set; }

        /// <summary>
        /// Общее количество отобранных по фильтру актированных расхождений
        /// </summary>
        public int TotalMatchesActDiscrepancies { get; set; }

        /// <summary>
        /// Общее количество отобранных пользователем актированных расхождений
        /// </summary>
        public int TotalSelectedActDiscrepancies { get; set; }

        /// <summary>
        /// Критерии последнего запроса списка актированных расхождений
        /// </summary>
        public QueryConditions LastQueryConditions { get; set; }

    }
}
