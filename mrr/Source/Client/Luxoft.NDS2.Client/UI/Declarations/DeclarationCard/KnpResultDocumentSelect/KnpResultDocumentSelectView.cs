﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    public partial class KnpResultDocumentSelectView : Form, IKnpResultDocumentSelectView
    {
        private ExtendedDataGridPageNavigator _pageNavigator;
        private bool _directClosing = false;
        private bool _isLock = false;
        private CheckColumn _chkColumn;


        public KnpResultDocumentSelectView()
        {
            InitializeComponent();
        }

        #region IEditKnpResultDocumentView

        public event EventHandler SavingChanges;

        public event CancelEventHandler ResetingChanges;

        public event EventHandler ViewOpened;

        public event Action<bool> CheckAllActDiscrepancyChanged;

        public event Action<object, bool> CheckActDiscrepancyChanged;

        public int TotalSelectedRowCount
        {
            set { _pageNavigator.SetSelectedCount(value); }
        }

        public bool AllowSavingChanges
        {
            set { _saveButton.Enabled = value; }
        }

        public IDataGridView ActDiscrepancyList
        {
            get { return _actDiscrepancyGrid; }
        }

        public void InitActDiscrepancyList(IPager pager)
        {
            _pageNavigator = new ExtendedDataGridPageNavigator(pager);

            var setupColumns = SetupGridColumn();

            _actDiscrepancyGrid
                .WithPager(_pageNavigator)
                .InitColumns(setupColumns);

            setupColumns.Columns.ForEachSingle(x => x.DisableMoving = true);

            _actDiscrepancyGrid.AfterDataSourceChanged += ActDiscrepancyGridAfterDataSourceChanged;

        }

        public void Lock()
        {
            _isLock = true;
            _backgroundPanel.Enabled = false;
        }

        public void UnLock()
        {
            _isLock = false;
            _backgroundPanel.Enabled = true;
        }

        public bool ShowModal()
        {
            DialogResult rez = this.ShowDialog();

            return rez == DialogResult.OK;
        }

        public void CloseWithTrue()
        {
            _directClosing = true;

            DialogResult = DialogResult.OK;

            Close();
        }

        public void CloseWithFalse()
        {
            _directClosing = true;

            DialogResult = DialogResult.Cancel;

            Close();
        }

        public IMessageView MessageView { get; set; }

        #endregion

        #region protected virtual

        protected virtual GridColumnSetup SetupGridColumn()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<KnpResultDiscrepancyModel>(_actDiscrepancyGrid);
            var helperOld = new GridSetupHelper<KnpResultDiscrepancyModel>();
            ColumnGroup group;

            _chkColumn = helper.CreateCheckColumn(o => o.Included, 2).Configure(d =>
            {
                d.HeaderToolTip = "Флаг выбора записи";
                d.DisableFilter = true;
                d.DisableSort = true;
                d.CheckEditableCriteria += o =>
                {
                    return ((KnpResultDiscrepancyModel)o).CanBeIncluded;
                };
                d.HeaderCheckedChange += state =>
                {
                    if (CheckAllActDiscrepancyChanged != null)
                        CheckAllActDiscrepancyChanged(state == CheckState.Checked);
                };
                d.RowCheckedChange += (o, b) =>
                {
                    if (o != null)
                    {
                        if (CheckActDiscrepancyChanged != null)
                            CheckActDiscrepancyChanged(o, b);
                    }
                };
            });
            setup.Columns.Add(_chkColumn);
            setup.Columns.Add(helper.CreateTextColumn(o => o.Source, 2, 80).Configure(d =>
            {
                d.Caption = "Источник";
                d.HeaderToolTip = "Номер раздела в НД НП";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));

            group = helper.CreateGroup("Контрагент");
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorInn, 1, 80).Configure(d =>
            {
                d.Caption = "ИНН";
                d.HeaderToolTip = "ИНН контрагента";
                d.DisableMoving = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorKpp, 1, 80).Configure(d =>
            {
                d.Caption = "КПП";
                d.HeaderToolTip = "КПП контрагента";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorName, 1, 134).Configure(d =>
            {
                d.Caption = "Наименование";
                d.HeaderToolTip = "Наименование контрагента";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorReorganizedInn, 1, 134).Configure(d =>
            {
                d.Caption = "Реорганизованный НП";
                d.HeaderToolTip = "ИНН реорганизованного НП";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Счет-фактура");
            group.Columns.Add(helper.CreateTextColumn(o => o.InvoiceNumber, 1, 130).Configure(d =>
            {
                d.Caption = "Номер";
                d.HeaderToolTip = "Номер счета-фактуры";
                d.DisableMoving = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.InvoiceDate, 1, 70).Configure(d =>
            {
                d.Caption = "Дата";
                d.HeaderToolTip = "Дата счета-фактуры";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.InvoiceAmount, 1, 110).Configure(d =>
            {
                d.Caption = "Стоимость по СФ";
                d.HeaderToolTip = "Стоимость по счету-фактуре";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.InvoiceAmountNds, 1, 110).Configure(d =>
            {
                d.Caption = "Сумма НДС";
                d.HeaderToolTip = "Сумма НДС по счету-фактуре";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Расхождение");
            group.Columns.Add(helper.CreateTextColumn(o => o.Type, 1, 110).Configure(d =>
            {
                d.Caption = "Вид расхождения";
                d.HeaderToolTip = "Вид расхождения";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.Amount, 1, 110).Configure(d =>
            {
                d.Caption = "Сумма";
                d.HeaderToolTip = "Сумма расхождения в руб.";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.Status, 1, 60).Configure(d =>
            {
                d.Caption = "Статус";
                d.HeaderToolTip = "Статус  расхождения";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            setup.Columns.Add(group);

            return setup;
        }

        protected virtual void SaveBtnClick(object sender, EventArgs e)
        {
            if (SavingChanges != null)
                SavingChanges(this, EventArgs.Empty);
        }

        protected virtual void EditActDiscrepancyViewLoad(object sender, EventArgs e)
        {
            if (ViewOpened != null)
                ViewOpened(this, EventArgs.Empty);
        }

        protected void EditKnpResultDocumentViewFormClosing(object sender, FormClosingEventArgs e)
        {
            if (_directClosing)
            {
                e.Cancel = false;
            }
            else
            {
                if ((ResetingChanges != null) && (!_isLock))
                {
                    ResetingChanges(this, e);

                    if (!e.Cancel)
                        DialogResult = DialogResult.Cancel;
                }
                else
                    e.Cancel = true;
            }
        }

        #endregion

        private void ActDiscrepancyGridAfterDataSourceChanged()
        {
            _chkColumn.RefreshHeaderState();
        }

    }
}
