﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using FLS.CommonComponents.Lib.Execution;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    public abstract class KnpResultDocumentSelectPresenter<TActDiscrepanciesListItemModel>
        where TActDiscrepanciesListItemModel : KnpResultDiscrepancyModel
    {
        private readonly IKnpResultDocumentSelectView _view;
        private readonly IServiceRequestWrapper _serviceRequester;
        private readonly IAppendDocumentDiscrepancyService _service;
        private readonly IThreadInvoker _uiThreadExecutor;
        private readonly KnpResultDocumentSelectModel<TActDiscrepanciesListItemModel> _model;
        private DataGridPresenter<TActDiscrepanciesListItemModel> _gridPresenter;
        private CancellationTokenSource _lastOperationCancelTokenSource;
        private readonly IServiceCallErrorHandler _serviceErrorHandler;
        private readonly ISettingsProvider _settingsProvider;
        private readonly System.Timers.Timer _sessionTimer;
        private readonly ComparisonProcessValidator _comparasionProcessValidator;

        public KnpResultDocumentSelectPresenter(
            long knpResultDiscrepancyDocumentId, 
            IKnpResultDocumentSelectView view, 
            IServiceRequestWrapper serviceRequester,
            IAppendDocumentDiscrepancyService service,
            IThreadInvoker uiThreadExecutor,
            IServiceCallErrorHandler serviceErrorHandler,
            ISettingsProvider settingsProvider,
            IDeclarationsDataService validatorService,
            long comparasionDataVersion)
        {
            _lastOperationCancelTokenSource = null;
            _model = new KnpResultDocumentSelectModel<TActDiscrepanciesListItemModel>(knpResultDiscrepancyDocumentId);

            _serviceRequester = serviceRequester;
            _service = service;
            _uiThreadExecutor = uiThreadExecutor;
            _serviceErrorHandler = serviceErrorHandler;
            _settingsProvider = settingsProvider;

            _view = view;
            _view.SavingChanges += SaveChages;
            _view.ResetingChanges += RollbackChages;
            _view.ViewOpened += InitOnOpenView;
            _view.CheckAllActDiscrepancyChanged += UpdateAllDiscrepanciesState;
            _view.CheckActDiscrepancyChanged += ActDiscrepancyCheckChanged;

            _comparasionProcessValidator = new ComparisonProcessValidator(
                validatorService,
                serviceRequester,
                comparasionDataVersion);

            _sessionTimer = new System.Timers.Timer()
            {
                Enabled = false,
                AutoReset = false
            };
            _sessionTimer.Elapsed += OnSessionTimer;

            InitGrid();
        }

        private void InitGrid()
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> { new ColumnSort
                {
                    ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(t => t.Amount), 
                    Order = ColumnSort.SortOrder.Desc
                } }
            };

            // порядок важен чтобы грид не обновлялся много раз при создании

            // 1
            _view.InitActDiscrepancyList(_model.ActDiscrepancyListPager);

            // 2
            _gridPresenter =
                new PagedDataGridPresenter<TActDiscrepanciesListItemModel>(
                    _view.ActDiscrepancyList,
                    _model.ActDiscrepancyListPager,
                    SearchActDiscrepancies,
                    conditions);

            // 3
            _gridPresenter.Init(_settingsProvider);
            // TODO: заменить на _gridPresenter.Init(); с реализацией работы метлы и отображении дефолтной сортировки

        }


        /// <summary>
        /// Обновить данные формы
        /// </summary>
        protected virtual void InitOnOpenView(object sender, EventArgs ev)
        {
            CancelLastAsyncOperaion();

            _view.Lock();

            _lastOperationCancelTokenSource = _serviceRequester.ExecuteAsync(
                (c) => _service.StartSession(_model.Id),
                (c, r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _view.UnLock();

                            _gridPresenter.Load();

                            var nextTimeInMilliSeconds = CorrectNextUpdateTime(r.Result);

                            if (nextTimeInMilliSeconds > 0)
                            {
                                _sessionTimer.Interval = nextTimeInMilliSeconds;
                                _sessionTimer.Start();
                            }
                        }
                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _serviceErrorHandler.ProcessingErrors(r, e);

                            _view.UnLock();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });
        }


        /// <summary>
        /// Изменение выбора отдельного расхождения
        /// </summary>
        protected virtual void ActDiscrepancyCheckChanged(object sender, bool isIncluded)
        {
            var actDiscrepancy = (KnpResultDiscrepancyModel) sender;

            CancelLastAsyncOperaion();

            _view.Lock();

            _lastOperationCancelTokenSource = _serviceRequester.ExecuteAsync(
                (c) =>
                {
                    if (isIncluded)
                        return _service.Include(_model.Id, actDiscrepancy.Id);
                    else
                        return _service.Exclude(_model.Id, actDiscrepancy.Id);
                },
                (c, r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            actDiscrepancy.Included = isIncluded;

                            if (isIncluded)
                                _model.TotalSelectedActDiscrepancies++;
                            else
                                _model.TotalSelectedActDiscrepancies--;

                            UpdateViewSelectedCount(_model.TotalSelectedActDiscrepancies);

                            _view.UnLock();
                        }
                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _serviceErrorHandler.ProcessingErrors(r, e);

                            _view.UnLock();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });
        }

        /// <summary>
        /// Выбор/Отмена всех расхождений
        /// </summary>
        protected virtual void UpdateAllDiscrepanciesState(bool isIncluded)
        {
            CancelLastAsyncOperaion();

            _view.Lock();

            _lastOperationCancelTokenSource = _serviceRequester.ExecuteAsync(
                (c) =>
                {
                    if (isIncluded)
                        return _service.IncludeAll(
                                            _model.Id,
                                            _model.LastQueryConditions.Filter != null ? _model.LastQueryConditions.Filter.ToArray() : null);
                    else
                        return _service.ExcludeAll(
                                            _model.Id,
                                            _model.LastQueryConditions.Filter != null ? _model.LastQueryConditions.Filter.ToArray() : null);
                },
                (c, r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _model.ActDiscrepancies.ForEach(x => x.Included = isIncluded);

                            _model.TotalSelectedActDiscrepancies = r.Result.IncludedQuantity;

                            UpdateViewSelectedCount(_model.TotalSelectedActDiscrepancies);

                            _view.UnLock();
                        }

                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _serviceErrorHandler.ProcessingErrors(r, e);

                            _view.UnLock();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });
        }

        /// <summary>
        /// Применить изменения
        /// </summary>
        protected virtual void SaveChages(object sender, EventArgs args)
        {
            CancelLastAsyncOperaion();

            var validationResult = _comparasionProcessValidator.Validate();
            if (!validationResult.IsValid)
            {
                _view.MessageView.ShowInfo(ResourceManagerNDS2.Attention, validationResult.Message);
                _view.CloseWithFalse();
                return;
            }

            _view.Lock();

            _lastOperationCancelTokenSource = _serviceRequester.ExecuteAsync(
                (c) => _service.Commit(_model.Id),
                (c, r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _sessionTimer.Stop();

                            _view.CloseWithTrue();
                        }
                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _serviceErrorHandler.ProcessingErrors(r, e);

                            _view.UnLock();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });
        }

        /// <summary>
        /// Отменить изменения
        /// </summary>
        protected virtual void RollbackChages(object sender, CancelEventArgs eventArg)
        {
            CancelLastAsyncOperaion();

            _view.Lock();

            _lastOperationCancelTokenSource = _serviceRequester.ExecuteAsync(
                (c) => _service.Rollback(_model.Id),
                (c, r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _sessionTimer.Stop();

                            _view.CloseWithFalse();
                        }
                    },
                    DispatcherPriority.Normal, c);
                },
                (c, r, e) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        if (!cancelToken.IsCancellationRequested)
                        {
                            _serviceErrorHandler.ProcessingErrors(r, e);

                            _view.CloseWithFalse();
                        }
                    },
                    DispatcherPriority.Normal, c);
                });

            eventArg.Cancel = true;
        }

        /// <summary>
        /// Создание моделей актированных расхождений - должно быть переопределено в потомках !
        /// </summary>
        /// <param name="dto">данные модели</param>
        /// <returns></returns>
        protected virtual TActDiscrepanciesListItemModel ActDiscrepanciesListItemFabricMethod(KnpResultDiscrepancy dto)
        {
            throw new NotImplementedException("You must override this method !");
        }


        protected virtual PageResult<TActDiscrepanciesListItemModel> SearchActDiscrepancies(QueryConditions conditions)
        {
            _uiThreadExecutor.BeginExecute((cancelToken) =>
            {
                _view.AllowSavingChanges = false;
            },
            DispatcherPriority.Normal);
            
            var ret = new PageResult<TActDiscrepanciesListItemModel>();
            
            var rez = _serviceRequester.Execute(
                () => _service.SearchUncommited(_model.Id, conditions),
                (r) =>
                {
                    var modelList = r.Result.Discrepancies.Select(x => ActDiscrepanciesListItemFabricMethod(x)).ToList();

                    _model.LastQueryConditions = conditions;

                    _model.TotalAialibleActDiscrepancies =
                        _model.LastQueryConditions.PaginationDetails.SkipTotalAvailable
                            ? _model.TotalAialibleActDiscrepancies
                            : r.Result.TotalSummary.Quantity;

                    _model.TotalMatchesActDiscrepancies =
                        _model.LastQueryConditions.PaginationDetails.SkipTotalMatches
                            ? _model.TotalMatchesActDiscrepancies
                            : r.Result.MatchesSummary.Quantity;

                    _model.TotalSelectedActDiscrepancies =
                        _model.LastQueryConditions.PaginationDetails.SkipTotalAvailable
                            ? _model.TotalSelectedActDiscrepancies
                            : r.Result.TotalSummary.IncludedQuantity;

                    _model.ActDiscrepancies.Clear();
                    _model.ActDiscrepancies.AddRange(modelList);

                    ret = new PageResult<TActDiscrepanciesListItemModel>()
                    {
                        Rows = _model.ActDiscrepancies.ToList(),
                        TotalAvailable = _model.TotalAialibleActDiscrepancies,
                        TotalMatches = _model.TotalMatchesActDiscrepancies
                    };

                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        UpdateViewSelectedCount(_model.TotalSelectedActDiscrepancies);
                    },
                    DispatcherPriority.Normal);

                },
                (r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        _serviceErrorHandler.ProcessingErrors(r);

                        UpdateViewSelectedCount(_model.TotalSelectedActDiscrepancies);
                    },
                    DispatcherPriority.Normal);
                });

            return ret;
        }

        private void OnSessionTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            var rez = _serviceRequester.Execute(
                () => _service.ExtendSession(_model.Id),
                (r) =>
                {
                    var nextTimeInMilliSeconds = CorrectNextUpdateTime(r.Result);

                    if (nextTimeInMilliSeconds > 0)
                    {
                        _sessionTimer.Interval = nextTimeInMilliSeconds;
                        _sessionTimer.Start();
                    }
                },
                (r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        _serviceErrorHandler.ProcessingErrors(r);
                    },
                    DispatcherPriority.Normal);
                });

        }

        private double CorrectNextUpdateTime(int nextTimeInSeconds)
        {
            double nextTimeInMilliSeconds = nextTimeInSeconds * 1000 - 100;

            return nextTimeInMilliSeconds;
        }


        protected virtual void UpdateViewSelectedCount(int count)
        {
            _view.TotalSelectedRowCount = count;
            _view.AllowSavingChanges = count > 0;
        }

        protected void CancelLastAsyncOperaion()
        {
            if (_lastOperationCancelTokenSource != null)
                _lastOperationCancelTokenSource.Cancel();
        }

    }
}
