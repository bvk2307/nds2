﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    partial class KnpResultDocumentSelectView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
            this._actDiscrepancyGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this._saveButton = new Infragistics.Win.Misc.UltraButton();
            this._backgroundPanel = new Infragistics.Win.Misc.UltraPanel();
            tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanel.SuspendLayout();
            this._backgroundPanel.ClientArea.SuspendLayout();
            this._backgroundPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            tableLayoutPanel.ColumnCount = 3;
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel.Controls.Add(this._actDiscrepancyGrid, 0, 0);
            tableLayoutPanel.Controls.Add(this._saveButton, 1, 2);
            tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel.Name = "tableLayoutPanel";
            tableLayoutPanel.RowCount = 4;
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            tableLayoutPanel.Size = new System.Drawing.Size(1264, 482);
            tableLayoutPanel.TabIndex = 0;
            // 
            // _actDiscrepancyGrid
            // 
            this._actDiscrepancyGrid.AggregatePanelVisible = true;
            this._actDiscrepancyGrid.AllowFilterReset = false;
            this._actDiscrepancyGrid.AllowMultiGrouping = true;
            this._actDiscrepancyGrid.AllowResetSettings = true;
            this._actDiscrepancyGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._actDiscrepancyGrid.BackColor = System.Drawing.Color.Transparent;
            this._actDiscrepancyGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            tableLayoutPanel.SetColumnSpan(this._actDiscrepancyGrid, 3);
            this._actDiscrepancyGrid.ColumnVisibilitySetupButtonVisible = false;
            this._actDiscrepancyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._actDiscrepancyGrid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._actDiscrepancyGrid.ExportExcelCancelVisible = false;
            this._actDiscrepancyGrid.ExportExcelHint = "Экспорт в MS Excel";
            this._actDiscrepancyGrid.ExportExcelVisible = false;
            this._actDiscrepancyGrid.FilterResetVisible = false;
            this._actDiscrepancyGrid.FooterVisible = true;
            this._actDiscrepancyGrid.GridContextMenuStrip = null;
            this._actDiscrepancyGrid.Location = new System.Drawing.Point(3, 3);
            this._actDiscrepancyGrid.Name = "_actDiscrepancyGrid";
            this._actDiscrepancyGrid.PanelExportExcelStateVisible = false;
            this._actDiscrepancyGrid.PanelLoadingVisible = true;
            this._actDiscrepancyGrid.PanelPagesVisible = true;
            this._actDiscrepancyGrid.RowDoubleClicked = null;
            this._actDiscrepancyGrid.Size = new System.Drawing.Size(1258, 436);
            this._actDiscrepancyGrid.TabIndex = 0;
            this._actDiscrepancyGrid.Title = "";
            // 
            // _saveButton
            // 
            this._saveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveButton.Enabled = false;
            this._saveButton.Location = new System.Drawing.Point(535, 450);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(194, 24);
            this._saveButton.TabIndex = 1;
            this._saveButton.Text = "Включить";
            this._saveButton.Click += new System.EventHandler(this.SaveBtnClick);
            // 
            // _backgroundPanel
            // 
            // 
            // _backgroundPanel.ClientArea
            // 
            this._backgroundPanel.ClientArea.Controls.Add(tableLayoutPanel);
            this._backgroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._backgroundPanel.Location = new System.Drawing.Point(0, 0);
            this._backgroundPanel.Name = "_backgroundPanel";
            this._backgroundPanel.Size = new System.Drawing.Size(1264, 482);
            this._backgroundPanel.TabIndex = 1;
            // 
            // KnpResultDocumentSelectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 482);
            this.Controls.Add(this._backgroundPanel);
            this.MinimizeBox = false;
            this.Name = "KnpResultDocumentSelectView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Учет расхождений по итогам КНП";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditKnpResultDocumentViewFormClosing);
            this.Load += new System.EventHandler(this.EditActDiscrepancyViewLoad);
            tableLayoutPanel.ResumeLayout(false);
            this._backgroundPanel.ClientArea.ResumeLayout(false);
            this._backgroundPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected Infragistics.Win.Misc.UltraButton _saveButton;
        protected Controls.Grid.V2.DataGridView _actDiscrepancyGrid;
        protected Infragistics.Win.Misc.UltraPanel _backgroundPanel;

    }
}