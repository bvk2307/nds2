﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect
{
    public interface IKnpResultDocumentSelectView
    {
        #region Работа с принятием изменений

        bool AllowSavingChanges { set; }

        event EventHandler SavingChanges;

        event CancelEventHandler ResetingChanges;

        #endregion

        #region Работа со списком расхождений

        event Action<bool> CheckAllActDiscrepancyChanged;

        event Action<object, bool> CheckActDiscrepancyChanged;

        IDataGridView ActDiscrepancyList { get; }

        int TotalSelectedRowCount { set; }

        void InitActDiscrepancyList(IPager pager);

        #endregion

        #region Работа со всей View

        bool ShowModal();

        void Lock();

        void UnLock();

        event EventHandler ViewOpened;

        void CloseWithTrue();

        void CloseWithFalse();

        IMessageView MessageView { get; set; }

        #endregion
    }
}
