﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinCalcManager;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public partial class View
    {
        #region Пока используются старые колонки для выгрузки в эксель

        public List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> GetColumnsChapter9()
        {
            GridSetupHelper<Invoice> helper = new GridSetupHelper<Invoice>();

            IntValueZeroFormatter formatInfoZeroEmpty = new IntValueZeroFormatter();

            ColumnDefinition c00 = helper.CreateColumnDefinition(o => o.IS_CHANGED, GetDrawPenAction());
            c00.RowSpan = 2;
            c00.AllowRowFiltering = false;
            c00.WidthColumnExcel = 5;

            var c00a = helper.CreateColumnDefinition(o => o.CONTRACTOR_DECL_IN_MC, d => { d.Caption = "К"; d.ToolTip = "Котрагент подал декларацию / журнал учета"; d.RowSpan = 2; d.WidthColumnExcel = 8; });
            var c00b = helper.CreateColumnDefinition(o => o.IS_MATCHING_ROW, d => { d.Caption = "С"; d.ToolTip = "Запись сопоставлена"; d.RowSpan = 2; d.WidthColumnExcel = 8; });

            ColumnDefinition c01 = helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => d.RowSpan = 2);
            ColumnDefinition c02 = helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => d.RowSpan = 2);

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер счета-фактуры продавца"; }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WidthDate; d.ToolTip = "Дата счета-фактуры продавца"; }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => d.Width = WidthInvoice));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WidthDate));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => d.Width = WidthInvoice));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WidthDate));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => d.Width = WidthInvoice));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WidthDate));

            var c5 = helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE_STR, d => { d.Width = WidthInvoice + WidthDate + 25; d.Caption = "Документ, подтверждающий оплату\n\r(стр. 120, 130)"; d.ToolTip = "Номер и дата документа, подтверждающего оплату"; d.RowSpan = 2; d.WidthColumnExcel = 30; });

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => d.Width = WidthInn));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => d.Width = WidthKpp));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_NAME, d =>
            {
                d.Width = WidthName;
                d.SortIndicator = false;
                d.AllowRowFiltering = false; d.WidthColumnExcel = 20;
            }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Сведения о посреднике" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = WidthInn; d.Caption = "ИНН\n\r(стр. 110)"; d.ToolTip = "ИНН посредника (комиссионера, агента)"; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = WidthKpp; d.Caption = "КПП\n\r(стр. 110)"; d.ToolTip = "КПП посредника (комиссионера, агента)"; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_NAME, d =>
            {
                d.Width = WidthName;
                d.SortIndicator = false;
                d.AllowRowFiltering = false;
                d.ToolTip = "Наименование посредника (комиссионера, агента)"; d.WidthColumnExcel = 20;
            }));

            var c91 = helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM, d => { d.Width = WidthInvoice; d.RowSpan = 2; d.WidthColumnExcel = 50; });
            ColumnDefinition c90 = helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.RowSpan = 2; d.Caption = "Код валюты\n\r(стр. 140)"; });

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = "Стоимость продаж с НДС" };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_IN_CURR, d => { d.Width = WidthMoney; d.FormatInfo = formatPriceShowZero; d.Align = ColumnAlign.Right; d.Caption = "в валюте\n\r(стр. 150)"; }));
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL, d => { d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; d.Caption = "в руб. и коп.\n\r(стр. 160)"; }));

            ColumnGroupDefinition g11 = new ColumnGroupDefinition() { Caption = "Стоимость продаж облагаемых налогом (в руб.) без НДС" };
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_18, d => { d.Width = WidthMoney + 10; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_10, d => { d.Width = WidthMoney + 10; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_0, d => { d.Width = WidthMoney + 10; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            ColumnGroupDefinition g12 = new ColumnGroupDefinition() { Caption = "Сумма НДС" };
            g12.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_18, d => { d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g12.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_10, d => { d.Width = WidthMoney; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            var c131 = helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d => { d.Width = WidthMoney; d.FormatInfo = formatInfoZeroEmpty; d.Align = ColumnAlign.Right; d.RowSpan = 2; });
            var c132 = helper.CreateColumnDefinition(o => o.IsAdditionalSheet, d => { d.Caption = "Доп.лист"; d.ToolTip = "Сведения из дополнительного листа"; d.RowSpan = 2; });

            return new List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> { c00, c00a, c00b, c01, c02,
                                          g1, g2, g3, g4, c5, g7, g8, 
                                          c91, c90, 
                                          g10, g11, g12, c131, c132 };
        }

        # endregion

        private void RefreshGridChapter9()
        {
            cmiR9OpenDecl.Enabled = _model.IsOpenDeclAccessible();

            //TODO: Необходимо найти альтернативный способ скрывать/показывать отдельные колонки в уже проинициализированном гриде. Так же необходимо учесть сохранение пользовательских настроек грида.
            //dataGridR9.ClearColumns();
            //dataGridR9.InitColumns(SetupGridChapter9());
            //_presenter.InitializeGridChapter9(dataGridR9, _pagerChapter9);
        }

        private GridColumnSetup SetupGridChapter9()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InvoiceModel>(dataGridR9);
            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            var priznakIcons = new Dictionary<int?, Bitmap>
            {
                { INVOICE_IS_PROCESSED, Properties.Resources.invoice_confirmed_16 },
                { INVOICE_IS_CHANGED, Properties.Resources.invoice_modified_16 },
            };

            setup.Columns.Add(helper.CreateImageColumn(o => o.IS_CHANGED, this, priznakIcons, 4)
                .Configure(d =>
                {
                    d.Caption = String.Empty;
                    d.HeaderToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
                    d.DisableFilter = true; 
                    d.ToolTipViewer = _explainToolTipViewer;
                }));

            #region Флаг К и С

            if (_model.IsChaptersGridIsMatchingColumnVisible())
            {
                var dictionaryBooleanIsMatching = new DictionaryBoolean();
                var optionsBuilderIsMatching = new BooleanAutoCompleteOptionsBuilder(dictionaryBooleanIsMatching);

                setup.Columns.Add(helper.CreateBoolColumn(o => o.CONTRACTOR_DECL_IN_MC, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "К";
                    d.HeaderToolTip = "Котрагент подал декларацию / журнал учета";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.DisableEdit = true;
                    d.MinWidth = WIDTH_CONTRACTOR_DECL_IN_MC;
                    d.Width = WIDTH_CONTRACTOR_DECL_IN_MC;
                }));
                setup.Columns.Add(helper.CreateBoolColumn(o => o.IS_MATCHING_ROW, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "С";
                    d.HeaderToolTip = "Запись сопоставлена";
                    d.MinWidth = WIDTH_IS_MATCHING_ROW;
                    d.Width = WIDTH_IS_MATCHING_ROW;
                }));
            }

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.ORDINAL_NUMBER, 4)
                .Configure(d => { d.Caption = "№\n\r(стр. 005)"; d.HeaderToolTip = "Порядковый номер"; })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(helper.CreateTextColumn(o => o.OPERATION_CODE, 4)
                .Configure(d => { d.Caption = "Код вида операции\n\r(стр. 010)"; d.HeaderToolTip = "Код вида операции"; })
                .SetFilterOperatorForOpearationCode());

            #region СФ

            var group = helper.CreateGroup("СФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 020)"; d.HeaderToolTip = "Номер счета-фактуры продавца"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 030)"; d.HeaderToolTip = "Дата счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.CUSTOMS_DECLARATION_NUM, 4, 200)
                .Configure(d =>
                {
                    d.Caption = "Регистрационный № ТД\n\r(стр. 035)";
                    d.HeaderToolTip = "Регистрационный номер таможенной декларации";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.MinWidth = WidthMoney;
                }));
            
            #region ИСФ

            group = helper.CreateGroup("ИСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 040)"; d.HeaderToolTip = "Номер исправления счета-фактуры продавца"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 050)"; d.HeaderToolTip = "Дата исправления счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region КСФ

            group = helper.CreateGroup("КСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 060)"; d.HeaderToolTip = "Номер корректировочного счета-фактуры продавца"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 070)"; d.HeaderToolTip = "Дата корректировочного счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region ИКСФ

            group = helper.CreateGroup("ИКСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 080)"; d.HeaderToolTip = "Номер исправления корректировочного счета-фактуры продавца"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 090)"; d.HeaderToolTip = "Дата исправления корректировочного счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.RECEIPT_DOC_DATE_STR, 4)
                .Configure(d =>
                {
                    d.Caption = "Документ, подтверждающий оплату\n\r(стр. 120, 130)";
                    d.HeaderToolTip = "Номер и дата документа, подтверждающего оплату";
                    d.MinWidth = WidthInvoice + WidthDate + 25;
                })
                .SetFilterOperatorForPaymentDoc());

            #region Сведения о покупателе

            group = helper.CreateGroup("Сведения о покупателе");
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 100)"; d.HeaderToolTip = "ИНН покупателя"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 100)"; d.HeaderToolTip = "КПП покупателя"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BUYER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование покупателя";
                    d.MinWidth = WidthName;
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            setup.Columns.Add(group);

            #endregion

            #region Сведения о посреднике

            group = helper.CreateGroup("Сведения о посреднике");
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 110)"; d.HeaderToolTip = "ИНН посредника (комиссионера, агента)"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 110)"; d.HeaderToolTip = "КПП посредника (комиссионера, агента)"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование посредника (комиссионера, агента)";
                    d.MinWidth = WidthName;
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.OKV_CODE, 4)
                .Configure(d =>
                {
                    d.Caption = "Код валюты\n\r(стр. 140)";
                    d.HeaderToolTip = "Код валюты по ОКВ";
                    d.MinWidth = 100;
                }));

            #region Стоимость продаж с НДС

            group = helper.CreateGroup("Стоимость продаж с НДС");
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_SELL_IN_CURR, 3)
                .Configure(d => 
                {
                    d.Caption = "в валюте\n\r(стр. 150)";
                    d.HeaderToolTip = "Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo  = formatPriceShowZero;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_SELL, 3)
                .Configure(d => 
                {
                    d.Caption = "в руб. и коп.\n\r(стр. 160)";
                    d.HeaderToolTip = "Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог) в рублях и копейках";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(group);

            #endregion

            #region Стоимость продаж облагаемых налогом (в руб.) без НДС

            group = helper.CreateGroup("Стоимость продаж облагаемых налогом (в руб.) без НДС");
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_SELL_18, 3)
                .Configure(d =>
                {
                    d.Caption = "18%\n\r(стр. 170)";
                    d.HeaderToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 18 %";
                    d.MinWidth = WidthMoney + 10;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_SELL_10, 3)
                .Configure(d =>
                {
                    d.Caption = "10%\n\r(стр. 180)";
                    d.HeaderToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 10 %";
                    d.MinWidth = WidthMoney + 10;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_SELL_0, 3)
                .Configure(d =>
                {
                    d.Caption = "0%\n\r(стр. 190)";
                    d.HeaderToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 0 %";
                    d.MinWidth = WidthMoney + 10;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(group);

            #endregion

            #region Сумма НДС

            group = helper.CreateGroup("Сумма НДС");
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_NDS_18, 3)
                .Configure(d =>
                {
                    d.Caption = "18%\n\r(стр. 200)";
                    d.HeaderToolTip = "Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 18 %";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.PRICE_NDS_10, 3)
                .Configure(d =>
                {
                    d.Caption = "10%\n\r(стр. 210)";
                    d.HeaderToolTip = "Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 10 %";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_TAX_FREE, 4)
                .Configure(d =>
                {
                    d.Caption = "Стоимость продаж, освобождаемых он налога\n\r(стр. 220)";
                    d.HeaderToolTip = "Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в рублях и копейках";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatInfoZeroEmpty;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            var dictionaryBoolean = new DictionaryBoolean();
            var optionsBuilder = new BooleanAutoCompleteOptionsBuilder(dictionaryBoolean);

            setup.Columns.Add(helper.CreateBoolColumn(o => o.IsAdditionalSheet,
                ColumnExpressionCreator.CreateColumnExpressionIsAdditionalSheet(
                DeclarationModelDataExtractorCreator.CreateChapterDataExtractor(_model, DeclarationInvoiceChapterNumber.Chapter9)),
                dictionaryBoolean, optionsBuilder, 4)
                .Configure(d =>
                {
                    d.Caption = "Доп.лист";
                    d.HeaderToolTip = "Сведения из дополнительного листа";
                    d.MinWidth = 100;
                    d.Width = 100;
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.ROW_KEY, 4)
                .Configure(d =>
                {
                    d.Caption = "ROW_KEY";
                    d.Hidden = true;
                    d.AllowToggleVisibility = false;
                }));

            return setup;
        }

        private void InitializeGridChapter9()
        {
            dataGridR9
                 .WithPager(new ExtendedDataGridPageNavigator(_pagerChapter9))
                 .InitColumns(SetupGridChapter9());
            dataGridR9.Grid.CalcManager = new UltraCalcManager(dataGridR9.Grid.Container); ;
            dataGridR9.RegisterTableAddin(new ExpansionIndicatorRemover());
            dataGridR9.SelectRow += (sender, args) =>
            {
                var citm = dataGridR9.GetCurrentItem<InvoiceModel>();
                _presenter.RefreshEKP();
            };
            dataGridR9.AfterDataSourceChanged += dataGridR9_AfterDataSourceChanged;
            SetupSummaryRow(dataGridR9);
            _presenter.InitializeGridChapter9(dataGridR9, _pagerChapter9);
        }

        void dataGridR9_AfterDataSourceChanged()
        {
            if (dataGridR9.Grid.DisplayLayout.UIElement.GetDescendant(typeof(RowScrollbarUIElement)) != null)
            {
                dataGridR9.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Default;
            }
            else
            {
                dataGridR9.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Bottom;
            }           
        }

        private readonly PagerStateViewModel _pagerChapter9 = new PagerStateViewModel();

        private readonly List<ToolStripMenuItem> _temporaryR9MenuItems = new List<ToolStripMenuItem>();

        private void R9ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            _temporaryR9MenuItems.ForEach(x => R9ContextMenu.Items.Remove(x));
            _temporaryR9MenuItems.Clear();

            var currentItem = dataGridR9.GetCurrentItem<InvoiceModel>();

            if (currentItem == null)
            {
                cmiR9OpenBroker.Visible = false;
                cmiR9OpenDecl.Visible = false;
                return;
            }

            if (!string.IsNullOrEmpty(currentItem.BUYER_INN))
            {
                var innList = currentItem.BUYER_INN.Split(',');
                var kppList = currentItem.BUYER_KPP.Split(',');
                var k = Math.Min(innList.Length, kppList.Length);
                for (var i = 0; i < k; ++i)
                {
                    var item = new ToolStripMenuItem
                    {
                        Name = string.Format("{0},{1}", innList[i], kppList[i]),
                        Size = new Size(242, 22),
                        Text = string.Format("Открыть карточку покупателя (ИНН {0})", innList[i])
                    };
                    item.Click += ContextOpenBuyerClick;
                    R9ContextMenu.Items.Insert(0, item);
                    _temporaryR9MenuItems.Add(item);
                }
            }

            cmiR9OpenBroker.Visible = !string.IsNullOrEmpty(currentItem.BROKER_INN);

            cmiR9OpenDecl.Visible = true;

            cmiR9ShowCustomNums.Enabled = currentItem.AllowCustomDeclarationNumbersView;
        }

        private void ContextOpenBuyerClick(object sender, EventArgs e)
        {
            var cmi = sender as ToolStripMenuItem;
            if (cmi == null)
                return;
            var invoice = dataGridR9.GetCurrentItem<InvoiceModel>();
            if (invoice == null)
                return;
            var args = cmi.Name.Split(',');
            if (args.Length != 2)
                return;
            _presenter.ViewTaxPayerByKppOriginal(args[0], args[1], Direction.Sales);
        }

        private void cmiR9OpenBroker_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR9.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppOriginal(currentItem.BROKER_INN, currentItem.BROKER_KPP, Direction.Sales);
        }

        private void cmiR9OpenDecl_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR9.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewDeclarationDetailsFromInvoice(currentItem);
        }


        private void cmiR9ShowCustomNumsClick(object sender, EventArgs e)
        {
            var handler = CustomDeclarationNumbersRequested;
            if (handler != null)
            {
                var currentItem = dataGridR9.GetCurrentItem<InvoiceModel>();

                if (currentItem == null)
                    return;

                handler(currentItem);
            }
        }
    }
}
