﻿using System;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            this.tabMain = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.rnivcWrapperPanel = new Infragistics.Win.Misc.UltraPanel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.tabR8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dataGridR8 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.R8ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiR8OpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR8OpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR8ShowCustomNums = new System.Windows.Forms.ToolStripMenuItem();
            this.ulRedWords8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraExpandableGroupBox_R8_Header = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel3 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.chapter08InfoPanel = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayoutPanelSummaryChapter8 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabelR8P190 = new Infragistics.Win.Misc.UltraLabel();
            this.labelVsegoBookBuyCaption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.labelItogoBookBuyCaption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR8_1P005 = new Infragistics.Win.Misc.UltraLabel();
            this.labelVsegoByPrilogenieChapter8Caption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR8_1P190 = new Infragistics.Win.Misc.UltraLabel();
            this.panelChapter8_TitleActualData = new Infragistics.Win.Misc.UltraPanel();
            this.panelChapter8_TitleActualData_01 = new Infragistics.Win.Misc.UltraPanel();
            this.labelChapter8_TitleActualData = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabR9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dataGridR9 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.R9ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiR9OpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR9OpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR9ShowCustomNums = new System.Windows.Forms.ToolStripMenuItem();
            this.ulRedWords9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraExpandableGroupBox_R9_Header = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel4 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.chapter09InfoPanel = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayoutPanelSummaryChapter9 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P270 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P250 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P240 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P230 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P260 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9P280 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P020 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P030 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P040 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P050 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P060 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P070 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P310 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P320 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P330 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P340 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P350 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelR9_1P360 = new Infragistics.Win.Misc.UltraLabel();
            this.panelChapter9_TitleActualData = new Infragistics.Win.Misc.UltraPanel();
            this.panelChapter9_TitleActualData_01 = new Infragistics.Win.Misc.UltraPanel();
            this.labelChapter9_TitleActualData = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabR10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dataGridR10 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.R10ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiR10OpenBuyer = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR10OpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR10OpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.ulRedWords10 = new Infragistics.Win.Misc.UltraLabel();
            this.panelChapterInfo10 = new Infragistics.Win.Misc.UltraPanel();
            this.captionR10 = new Infragistics.Win.Misc.UltraLabel();
            this.labelChapter10_TitleActualData = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabR11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dataGridR11 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.R11ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiR11OpenSeller = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR11OpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR11OpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.ulRedWords11 = new Infragistics.Win.Misc.UltraLabel();
            this.panelChapterInfo11 = new Infragistics.Win.Misc.UltraPanel();
            this.captionR11 = new Infragistics.Win.Misc.UltraLabel();
            this.labelChapter11_TitleActualData = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabR12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.dataGridR12 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.R12ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiR12OpenBuyer = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiR12OpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.ulRedWords12 = new Infragistics.Win.Misc.UltraLabel();
            this.panelChapter12_TitleActualData = new Infragistics.Win.Misc.UltraPanel();
            this.labelChapter12_TitleActualData = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabControlRatio = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridControlRatio = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabDiscrepancies = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridDiscrepancies = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.discrepContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenDiscrep = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenTaxPayer = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenDecl = new System.Windows.Forms.ToolStripMenuItem();
            this.tabDOC = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.knpDocLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this._knpDocumentGrid = new Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpDocuments.KnpDocumentListView();
            this.tabAct = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.actDiscrepancyEditView = new Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ActDiscrepancyEditView();
            this.tabDecision = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.decisionDiscrepancyEditView = new Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.DecisionDiscrepancyEditView();
            this.saveFileDialogReport = new System.Windows.Forms.SaveFileDialog();
            this.DeclarationParts = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.tabShared = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.grpCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.grpCommonInfoPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.autosizePanel = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.ulNotLoadedInMS = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesCommonPVPSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesCommonSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesCommonSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesCommonPVPSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSFCountCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSFCountValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCommonSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelBuyBookCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesBuyBookSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesBuyBookSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesBuyBookPVPSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesBuyBookPVPSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSellBookCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSellBookSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSellBookPVPSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSellBookSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesSellBookPVPSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPMinSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPMinSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesMinSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesMinSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelMinSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelMaxSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesMaxSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPMaxSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelAvgSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesAvgSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPAvgSumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesMaxSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPMaxSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesAvgSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesPVPAvgSumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesKSCountCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepanciesKSCountValue = new Infragistics.Win.Misc.UltraLabel();
            this.grpHeader = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.commonInfoPanel = new Infragistics.Win.Misc.UltraPanel();
            this.labelAnnulmentFlag = new Infragistics.Win.Misc.UltraLabel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.labelInspectorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelPeriodValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelInspectorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelPeriodCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateTakeValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateTakeCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSignValue = new Infragistics.Win.Misc.UltraLabel();
            this.comboCorrectionNumValue = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelSignCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionNumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionStatus = new Infragistics.Win.Misc.UltraLabel();
            this.labelRegNumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelRegNumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelNameValue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabMain.SuspendLayout();
            this.rnivcWrapperPanel.SuspendLayout();
            this.tabR8.SuspendLayout();
            this.R8ContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox_R8_Header)).BeginInit();
            this.ultraExpandableGroupBox_R8_Header.SuspendLayout();
            this.ultraExpandableGroupBoxPanel3.SuspendLayout();
            this.chapter08InfoPanel.ClientArea.SuspendLayout();
            this.chapter08InfoPanel.SuspendLayout();
            this.tableLayoutPanelSummaryChapter8.SuspendLayout();
            this.panelChapter8_TitleActualData.ClientArea.SuspendLayout();
            this.panelChapter8_TitleActualData.SuspendLayout();
            this.panelChapter8_TitleActualData_01.ClientArea.SuspendLayout();
            this.panelChapter8_TitleActualData_01.SuspendLayout();
            this.tabR9.SuspendLayout();
            this.R9ContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox_R9_Header)).BeginInit();
            this.ultraExpandableGroupBox_R9_Header.SuspendLayout();
            this.ultraExpandableGroupBoxPanel4.SuspendLayout();
            this.chapter09InfoPanel.ClientArea.SuspendLayout();
            this.chapter09InfoPanel.SuspendLayout();
            this.tableLayoutPanelSummaryChapter9.SuspendLayout();
            this.panelChapter9_TitleActualData.ClientArea.SuspendLayout();
            this.panelChapter9_TitleActualData.SuspendLayout();
            this.panelChapter9_TitleActualData_01.ClientArea.SuspendLayout();
            this.panelChapter9_TitleActualData_01.SuspendLayout();
            this.tabR10.SuspendLayout();
            this.R10ContextMenu.SuspendLayout();
            this.panelChapterInfo10.ClientArea.SuspendLayout();
            this.panelChapterInfo10.SuspendLayout();
            this.tabR11.SuspendLayout();
            this.R11ContextMenu.SuspendLayout();
            this.panelChapterInfo11.ClientArea.SuspendLayout();
            this.panelChapterInfo11.SuspendLayout();
            this.tabR12.SuspendLayout();
            this.R12ContextMenu.SuspendLayout();
            this.panelChapter12_TitleActualData.ClientArea.SuspendLayout();
            this.panelChapter12_TitleActualData.SuspendLayout();
            this.tabControlRatio.SuspendLayout();
            this.tabDiscrepancies.SuspendLayout();
            this.discrepContextMenu.SuspendLayout();
            this.tabDOC.SuspendLayout();
            this.knpDocLayoutPanel.SuspendLayout();
            this.tabAct.SuspendLayout();
            this.tabDecision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeclarationParts)).BeginInit();
            this.DeclarationParts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpCommonInfo)).BeginInit();
            this.grpCommonInfo.SuspendLayout();
            this.grpCommonInfoPanel.SuspendLayout();
            this.autosizePanel.ClientArea.SuspendLayout();
            this.autosizePanel.SuspendLayout();
            this.tableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpHeader)).BeginInit();
            this.grpHeader.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.commonInfoPanel.ClientArea.SuspendLayout();
            this.commonInfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboCorrectionNumValue)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.rnivcWrapperPanel);
            this.tabMain.Controls.Add(this.progressBar);
            this.tabMain.Location = new System.Drawing.Point(1, 23);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(1152, 424);
            // 
            // rnivcWrapperPanel
            // 
            this.rnivcWrapperPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rnivcWrapperPanel.Location = new System.Drawing.Point(0, 0);
            this.rnivcWrapperPanel.Name = "rnivcWrapperPanel";
            this.rnivcWrapperPanel.Size = new System.Drawing.Size(1152, 401);
            this.rnivcWrapperPanel.TabIndex = 6;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar.Location = new System.Drawing.Point(0, 401);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1152, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 4;
            // 
            // tabR8
            // 
            this.tabR8.Controls.Add(this.dataGridR8);
            this.tabR8.Controls.Add(this.ulRedWords8);
            this.tabR8.Controls.Add(this.ultraExpandableGroupBox_R8_Header);
            this.tabR8.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR8.Name = "tabR8";
            this.tabR8.Size = new System.Drawing.Size(1152, 424);
            // 
            // dataGridR8
            // 
            this.dataGridR8.AggregatePanelVisible = true;
            this.dataGridR8.AllowFilterReset = false;
            this.dataGridR8.AllowMultiGrouping = true;
            this.dataGridR8.AllowResetSettings = false;
            this.dataGridR8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.dataGridR8.BackColor = System.Drawing.Color.Transparent;
            this.dataGridR8.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.dataGridR8.ColumnVisibilitySetupButtonVisible = true;
            this.dataGridR8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridR8.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.dataGridR8.ExportExcelCancelVisible = false;
            this.dataGridR8.ExportExcelHint = "Экспорт в MS Excel";
            this.dataGridR8.ExportExcelVisible = true;
            this.dataGridR8.FilterResetVisible = true;
            this.dataGridR8.FooterVisible = true;
            this.dataGridR8.GridContextMenuStrip = this.R8ContextMenu;
            this.dataGridR8.Location = new System.Drawing.Point(0, 102);
            this.dataGridR8.Name = "dataGridR8";
            this.dataGridR8.PanelExportExcelStateVisible = true;
            this.dataGridR8.PanelLoadingVisible = false;
            this.dataGridR8.PanelPagesVisible = true;
            this.dataGridR8.RowDoubleClicked = null;
            this.dataGridR8.Size = new System.Drawing.Size(1152, 322);
            this.dataGridR8.TabIndex = 6;
            this.dataGridR8.Title = "Сведения из книги покупок об операциях, отражаемых за истекший налоговый период";
            // 
            // R8ContextMenu
            // 
            this.R8ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiR8OpenBroker,
            this.cmiR8OpenDecl,
            this.cmiR8ShowCustomNums});
            this.R8ContextMenu.Name = "R8ContextMenu";
            this.R8ContextMenu.Size = new System.Drawing.Size(274, 70);
            this.R8ContextMenu.Opened += new System.EventHandler(this.R8ContextMenu_Opened);
            // 
            // cmiR8OpenBroker
            // 
            this.cmiR8OpenBroker.Name = "cmiR8OpenBroker";
            this.cmiR8OpenBroker.Size = new System.Drawing.Size(273, 22);
            this.cmiR8OpenBroker.Text = "Открыть карточку посредника";
            this.cmiR8OpenBroker.Click += new System.EventHandler(this.cmiR8OpenBroker_Click);
            // 
            // cmiR8OpenDecl
            // 
            this.cmiR8OpenDecl.Name = "cmiR8OpenDecl";
            this.cmiR8OpenDecl.Size = new System.Drawing.Size(273, 22);
            this.cmiR8OpenDecl.Text = "Перейти на сопоставленную запись";
            this.cmiR8OpenDecl.Click += new System.EventHandler(this.cmiR8OpenDecl_Click);
            // 
            // cmiR8ShowCustomNums
            // 
            this.cmiR8ShowCustomNums.Name = "cmiR8ShowCustomNums";
            this.cmiR8ShowCustomNums.Size = new System.Drawing.Size(273, 22);
            this.cmiR8ShowCustomNums.Text = "Просмотреть список ТД";
            this.cmiR8ShowCustomNums.Click += new System.EventHandler(this.cmiR8ShowCustomNumsClick);
            // 
            // ulRedWords8
            // 
            appearance59.FontData.BoldAsString = "True";
            appearance59.ForeColor = System.Drawing.Color.Red;
            appearance59.TextHAlignAsString = "Center";
            appearance59.TextVAlignAsString = "Middle";
            this.ulRedWords8.Appearance = appearance59;
            this.ulRedWords8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords8.Location = new System.Drawing.Point(0, 102);
            this.ulRedWords8.Name = "ulRedWords8";
            this.ulRedWords8.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords8.Size = new System.Drawing.Size(1152, 322);
            this.ulRedWords8.TabIndex = 5;
            this.ulRedWords8.Text = "Сделки с контрагентами отсутствуют";
            this.ulRedWords8.Visible = false;
            // 
            // ultraExpandableGroupBox_R8_Header
            // 
            this.ultraExpandableGroupBox_R8_Header.Controls.Add(this.ultraExpandableGroupBoxPanel3);
            this.ultraExpandableGroupBox_R8_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraExpandableGroupBox_R8_Header.ExpandedSize = new System.Drawing.Size(1152, 102);
            this.ultraExpandableGroupBox_R8_Header.Location = new System.Drawing.Point(0, 0);
            this.ultraExpandableGroupBox_R8_Header.Name = "ultraExpandableGroupBox_R8_Header";
            this.ultraExpandableGroupBox_R8_Header.Size = new System.Drawing.Size(1152, 102);
            this.ultraExpandableGroupBox_R8_Header.TabIndex = 3;
            this.ultraExpandableGroupBox_R8_Header.Text = "Итоговые суммы по разделу";
            // 
            // ultraExpandableGroupBoxPanel3
            // 
            this.ultraExpandableGroupBoxPanel3.Controls.Add(this.chapter08InfoPanel);
            this.ultraExpandableGroupBoxPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel3.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel3.Name = "ultraExpandableGroupBoxPanel3";
            this.ultraExpandableGroupBoxPanel3.Size = new System.Drawing.Size(1146, 80);
            this.ultraExpandableGroupBoxPanel3.TabIndex = 0;
            // 
            // chapter08InfoPanel
            // 
            // 
            // chapter08InfoPanel.ClientArea
            // 
            this.chapter08InfoPanel.ClientArea.Controls.Add(this.tableLayoutPanelSummaryChapter8);
            this.chapter08InfoPanel.ClientArea.Controls.Add(this.panelChapter8_TitleActualData);
            this.chapter08InfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.chapter08InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.chapter08InfoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.chapter08InfoPanel.Name = "chapter08InfoPanel";
            this.chapter08InfoPanel.Size = new System.Drawing.Size(1146, 80);
            this.chapter08InfoPanel.TabIndex = 2;
            // 
            // tableLayoutPanelSummaryChapter8
            // 
            this.tableLayoutPanelSummaryChapter8.AutoSize = true;
            this.tableLayoutPanelSummaryChapter8.ColumnCount = 7;
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.46977F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.040792F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.4691F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.040792F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.4691F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.040792F));
            this.tableLayoutPanelSummaryChapter8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.46965F));
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.ultraLabelR8P190, 2, 1);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.labelVsegoBookBuyCaption, 2, 0);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.ultraLabel17, 0, 0);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.ultraLabel20, 0, 1);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.labelItogoBookBuyCaption, 4, 0);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.ultraLabelR8_1P005, 4, 1);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.labelVsegoByPrilogenieChapter8Caption, 6, 0);
            this.tableLayoutPanelSummaryChapter8.Controls.Add(this.ultraLabelR8_1P190, 6, 1);
            this.tableLayoutPanelSummaryChapter8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelSummaryChapter8.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanelSummaryChapter8.Location = new System.Drawing.Point(0, 21);
            this.tableLayoutPanelSummaryChapter8.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanelSummaryChapter8.Name = "tableLayoutPanelSummaryChapter8";
            this.tableLayoutPanelSummaryChapter8.RowCount = 2;
            this.tableLayoutPanelSummaryChapter8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter8.Size = new System.Drawing.Size(1146, 40);
            this.tableLayoutPanelSummaryChapter8.TabIndex = 3;
            // 
            // ultraLabelR8P190
            // 
            this.ultraLabelR8P190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR8P190.Location = new System.Drawing.Point(235, 21);
            this.ultraLabelR8P190.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR8P190.Name = "ultraLabelR8P190";
            this.ultraLabelR8P190.Size = new System.Drawing.Size(213, 18);
            this.ultraLabelR8P190.TabIndex = 22;
            // 
            // labelVsegoBookBuyCaption
            // 
            this.labelVsegoBookBuyCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVsegoBookBuyCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVsegoBookBuyCaption.Location = new System.Drawing.Point(235, 1);
            this.labelVsegoBookBuyCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelVsegoBookBuyCaption.Name = "labelVsegoBookBuyCaption";
            this.labelVsegoBookBuyCaption.Size = new System.Drawing.Size(266, 18);
            this.labelVsegoBookBuyCaption.TabIndex = 17;
            this.labelVsegoBookBuyCaption.Text = "     Всего по книге покупок";
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel17.Location = new System.Drawing.Point(1, 1);
            this.ultraLabel17.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(209, 18);
            this.ultraLabel17.TabIndex = 0;
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel20.Location = new System.Drawing.Point(1, 21);
            this.ultraLabel20.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(209, 18);
            this.ultraLabel20.TabIndex = 1;
            this.ultraLabel20.Text = "Сумма налога, руб.";
            // 
            // labelItogoBookBuyCaption
            // 
            this.labelItogoBookBuyCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelItogoBookBuyCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelItogoBookBuyCaption.Location = new System.Drawing.Point(526, 1);
            this.labelItogoBookBuyCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelItogoBookBuyCaption.Name = "labelItogoBookBuyCaption";
            this.labelItogoBookBuyCaption.Size = new System.Drawing.Size(266, 18);
            this.labelItogoBookBuyCaption.TabIndex = 20;
            this.labelItogoBookBuyCaption.Text = "Итого по книге покупок";
            // 
            // ultraLabelR8_1P005
            // 
            this.ultraLabelR8_1P005.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR8_1P005.Location = new System.Drawing.Point(526, 21);
            this.ultraLabelR8_1P005.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR8_1P005.Name = "ultraLabelR8_1P005";
            this.ultraLabelR8_1P005.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR8_1P005.TabIndex = 26;
            // 
            // labelVsegoByPrilogenieChapter8Caption
            // 
            this.labelVsegoByPrilogenieChapter8Caption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVsegoByPrilogenieChapter8Caption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVsegoByPrilogenieChapter8Caption.Location = new System.Drawing.Point(817, 1);
            this.labelVsegoByPrilogenieChapter8Caption.Margin = new System.Windows.Forms.Padding(1);
            this.labelVsegoByPrilogenieChapter8Caption.Name = "labelVsegoByPrilogenieChapter8Caption";
            this.labelVsegoByPrilogenieChapter8Caption.Size = new System.Drawing.Size(328, 18);
            this.labelVsegoByPrilogenieChapter8Caption.TabIndex = 27;
            this.labelVsegoByPrilogenieChapter8Caption.Text = "Всего по приложению 1 к разделу 8";
            // 
            // ultraLabelR8_1P190
            // 
            this.ultraLabelR8_1P190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR8_1P190.Location = new System.Drawing.Point(817, 21);
            this.ultraLabelR8_1P190.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR8_1P190.Name = "ultraLabelR8_1P190";
            this.ultraLabelR8_1P190.Size = new System.Drawing.Size(230, 18);
            this.ultraLabelR8_1P190.TabIndex = 28;
            // 
            // panelChapter8_TitleActualData
            // 
            // 
            // panelChapter8_TitleActualData.ClientArea
            // 
            this.panelChapter8_TitleActualData.ClientArea.Controls.Add(this.panelChapter8_TitleActualData_01);
            this.panelChapter8_TitleActualData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapter8_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.panelChapter8_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapter8_TitleActualData.Name = "panelChapter8_TitleActualData";
            this.panelChapter8_TitleActualData.Size = new System.Drawing.Size(1146, 21);
            this.panelChapter8_TitleActualData.TabIndex = 6;
            // 
            // panelChapter8_TitleActualData_01
            // 
            // 
            // panelChapter8_TitleActualData_01.ClientArea
            // 
            this.panelChapter8_TitleActualData_01.ClientArea.Controls.Add(this.labelChapter8_TitleActualData);
            this.panelChapter8_TitleActualData_01.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapter8_TitleActualData_01.Location = new System.Drawing.Point(0, 0);
            this.panelChapter8_TitleActualData_01.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapter8_TitleActualData_01.Name = "panelChapter8_TitleActualData_01";
            this.panelChapter8_TitleActualData_01.Size = new System.Drawing.Size(1146, 18);
            this.panelChapter8_TitleActualData_01.TabIndex = 4;
            // 
            // labelChapter8_TitleActualData
            // 
            appearance78.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelChapter8_TitleActualData.Appearance = appearance78;
            this.labelChapter8_TitleActualData.AutoSize = true;
            this.labelChapter8_TitleActualData.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelChapter8_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.labelChapter8_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.labelChapter8_TitleActualData.Name = "labelChapter8_TitleActualData";
            this.labelChapter8_TitleActualData.Size = new System.Drawing.Size(509, 18);
            this.labelChapter8_TitleActualData.TabIndex = 3;
            this.labelChapter8_TitleActualData.TabStop = true;
            this.labelChapter8_TitleActualData.Value = "Данные по <b>текущей</b> корректировке Т (раздел 8) и по корректировке N (приложе" +
    "ние 1 к разделу 8)";
            // 
            // tabR9
            // 
            this.tabR9.Controls.Add(this.dataGridR9);
            this.tabR9.Controls.Add(this.ulRedWords9);
            this.tabR9.Controls.Add(this.ultraExpandableGroupBox_R9_Header);
            this.tabR9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR9.Name = "tabR9";
            this.tabR9.Size = new System.Drawing.Size(1152, 424);
            // 
            // dataGridR9
            // 
            this.dataGridR9.AggregatePanelVisible = true;
            this.dataGridR9.AllowFilterReset = false;
            this.dataGridR9.AllowMultiGrouping = true;
            this.dataGridR9.AllowResetSettings = false;
            this.dataGridR9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.dataGridR9.BackColor = System.Drawing.Color.Transparent;
            this.dataGridR9.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.dataGridR9.ColumnVisibilitySetupButtonVisible = true;
            this.dataGridR9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridR9.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.dataGridR9.ExportExcelCancelVisible = false;
            this.dataGridR9.ExportExcelHint = "Экспорт в MS Excel";
            this.dataGridR9.ExportExcelVisible = true;
            this.dataGridR9.FilterResetVisible = true;
            this.dataGridR9.FooterVisible = true;
            this.dataGridR9.GridContextMenuStrip = this.R9ContextMenu;
            this.dataGridR9.Location = new System.Drawing.Point(0, 240);
            this.dataGridR9.Name = "dataGridR9";
            this.dataGridR9.PanelExportExcelStateVisible = true;
            this.dataGridR9.PanelLoadingVisible = false;
            this.dataGridR9.PanelPagesVisible = true;
            this.dataGridR9.RowDoubleClicked = null;
            this.dataGridR9.Size = new System.Drawing.Size(1152, 184);
            this.dataGridR9.TabIndex = 6;
            this.dataGridR9.Title = "Сведения из книги продаж об операциях, отражаемых за истекший налоговый период";
            // 
            // R9ContextMenu
            // 
            this.R9ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiR9OpenBroker,
            this.cmiR9OpenDecl,
            this.cmiR9ShowCustomNums});
            this.R9ContextMenu.Name = "R9ContextMenu";
            this.R9ContextMenu.Size = new System.Drawing.Size(274, 70);
            this.R9ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.R9ContextMenu_Opening);
            // 
            // cmiR9OpenBroker
            // 
            this.cmiR9OpenBroker.Name = "cmiR9OpenBroker";
            this.cmiR9OpenBroker.Size = new System.Drawing.Size(273, 22);
            this.cmiR9OpenBroker.Text = "Открыть карточку посредника";
            this.cmiR9OpenBroker.Click += new System.EventHandler(this.cmiR9OpenBroker_Click);
            // 
            // cmiR9OpenDecl
            // 
            this.cmiR9OpenDecl.Name = "cmiR9OpenDecl";
            this.cmiR9OpenDecl.Size = new System.Drawing.Size(273, 22);
            this.cmiR9OpenDecl.Text = "Перейти на сопоставленную запись";
            this.cmiR9OpenDecl.Click += new System.EventHandler(this.cmiR9OpenDecl_Click);
            // 
            // cmiR9ShowCustomNums
            // 
            this.cmiR9ShowCustomNums.Name = "cmiR9ShowCustomNums";
            this.cmiR9ShowCustomNums.Size = new System.Drawing.Size(273, 22);
            this.cmiR9ShowCustomNums.Text = "Просмотреть список ТД";
            this.cmiR9ShowCustomNums.Click += new System.EventHandler(this.cmiR9ShowCustomNumsClick);
            // 
            // ulRedWords9
            // 
            appearance61.FontData.BoldAsString = "True";
            appearance61.ForeColor = System.Drawing.Color.Red;
            appearance61.TextHAlignAsString = "Center";
            appearance61.TextVAlignAsString = "Middle";
            this.ulRedWords9.Appearance = appearance61;
            this.ulRedWords9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords9.Location = new System.Drawing.Point(0, 240);
            this.ulRedWords9.Name = "ulRedWords9";
            this.ulRedWords9.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords9.Size = new System.Drawing.Size(1152, 184);
            this.ulRedWords9.TabIndex = 5;
            this.ulRedWords9.Text = "Сделки с контрагентами отсутствуют";
            this.ulRedWords9.Visible = false;
            // 
            // ultraExpandableGroupBox_R9_Header
            // 
            this.ultraExpandableGroupBox_R9_Header.Controls.Add(this.ultraExpandableGroupBoxPanel4);
            this.ultraExpandableGroupBox_R9_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraExpandableGroupBox_R9_Header.ExpandedSize = new System.Drawing.Size(1152, 240);
            this.ultraExpandableGroupBox_R9_Header.Location = new System.Drawing.Point(0, 0);
            this.ultraExpandableGroupBox_R9_Header.Name = "ultraExpandableGroupBox_R9_Header";
            this.ultraExpandableGroupBox_R9_Header.Size = new System.Drawing.Size(1152, 240);
            this.ultraExpandableGroupBox_R9_Header.TabIndex = 3;
            this.ultraExpandableGroupBox_R9_Header.Text = "Итоговые суммы по разделу";
            // 
            // ultraExpandableGroupBoxPanel4
            // 
            this.ultraExpandableGroupBoxPanel4.Controls.Add(this.chapter09InfoPanel);
            this.ultraExpandableGroupBoxPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel4.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel4.Name = "ultraExpandableGroupBoxPanel4";
            this.ultraExpandableGroupBoxPanel4.Size = new System.Drawing.Size(1146, 218);
            this.ultraExpandableGroupBoxPanel4.TabIndex = 0;
            // 
            // chapter09InfoPanel
            // 
            // 
            // chapter09InfoPanel.ClientArea
            // 
            this.chapter09InfoPanel.ClientArea.Controls.Add(this.tableLayoutPanelSummaryChapter9);
            this.chapter09InfoPanel.ClientArea.Controls.Add(this.panelChapter9_TitleActualData);
            this.chapter09InfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.chapter09InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.chapter09InfoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.chapter09InfoPanel.Name = "chapter09InfoPanel";
            this.chapter09InfoPanel.Size = new System.Drawing.Size(1146, 243);
            this.chapter09InfoPanel.TabIndex = 2;
            // 
            // tableLayoutPanelSummaryChapter9
            // 
            this.tableLayoutPanelSummaryChapter9.AutoSize = true;
            this.tableLayoutPanelSummaryChapter9.ColumnCount = 4;
            this.tableLayoutPanelSummaryChapter9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanelSummaryChapter9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanelSummaryChapter9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanelSummaryChapter9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel13, 0, 7);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P270, 1, 7);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel24, 0, 5);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P250, 1, 4);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel18, 0, 4);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel9, 1, 1);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel2, 1, 0);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel3, 0, 2);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel4, 0, 3);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel7, 0, 1);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel11, 0, 6);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel14, 0, 8);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P240, 1, 3);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P230, 1, 2);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel29, 1, 5);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P260, 1, 6);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9P280, 1, 8);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel6, 2, 0);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel10, 2, 1);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P020, 2, 2);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P030, 2, 3);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P040, 2, 4);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel26, 2, 5);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P050, 2, 6);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P060, 2, 7);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P070, 2, 8);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel5, 3, 0);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel8, 3, 1);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P310, 3, 2);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P320, 3, 3);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P330, 3, 4);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabel25, 3, 5);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P340, 3, 6);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P350, 3, 7);
            this.tableLayoutPanelSummaryChapter9.Controls.Add(this.ultraLabelR9_1P360, 3, 8);
            this.tableLayoutPanelSummaryChapter9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelSummaryChapter9.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanelSummaryChapter9.Location = new System.Drawing.Point(0, 21);
            this.tableLayoutPanelSummaryChapter9.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanelSummaryChapter9.Name = "tableLayoutPanelSummaryChapter9";
            this.tableLayoutPanelSummaryChapter9.RowCount = 9;
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSummaryChapter9.Size = new System.Drawing.Size(1146, 180);
            this.tableLayoutPanelSummaryChapter9.TabIndex = 4;
            // 
            // ultraLabel13
            // 
            appearance73.TextHAlignAsString = "Left";
            this.ultraLabel13.Appearance = appearance73;
            this.ultraLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel13.Location = new System.Drawing.Point(1, 141);
            this.ultraLabel13.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel13.TabIndex = 51;
            this.ultraLabel13.Text = "10%";
            // 
            // ultraLabelR9P270
            // 
            appearance16.TextHAlignAsString = "Left";
            this.ultraLabelR9P270.Appearance = appearance16;
            this.ultraLabelR9P270.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P270.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P270.Location = new System.Drawing.Point(390, 141);
            this.ultraLabelR9P270.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P270.Name = "ultraLabelR9P270";
            this.ultraLabelR9P270.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P270.TabIndex = 46;
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel24.Location = new System.Drawing.Point(1, 101);
            this.ultraLabel24.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel24.TabIndex = 41;
            this.ultraLabel24.Text = "Сумма налога, по ставке, руб.";
            // 
            // ultraLabelR9P250
            // 
            appearance36.TextHAlignAsString = "Left";
            this.ultraLabelR9P250.Appearance = appearance36;
            this.ultraLabelR9P250.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P250.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P250.Location = new System.Drawing.Point(390, 81);
            this.ultraLabelR9P250.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P250.Name = "ultraLabelR9P250";
            this.ultraLabelR9P250.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P250.TabIndex = 38;
            // 
            // ultraLabel18
            // 
            appearance38.TextHAlignAsString = "Left";
            this.ultraLabel18.Appearance = appearance38;
            this.ultraLabel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel18.Location = new System.Drawing.Point(1, 81);
            this.ultraLabel18.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel18.TabIndex = 37;
            this.ultraLabel18.Text = "0%";
            // 
            // ultraLabel9
            // 
            appearance37.TextHAlignAsString = "Right";
            this.ultraLabel9.Appearance = appearance37;
            this.ultraLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel9.Location = new System.Drawing.Point(390, 21);
            this.ultraLabel9.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel9.TabIndex = 30;
            // 
            // ultraLabel2
            // 
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel2.Location = new System.Drawing.Point(390, 1);
            this.ultraLabel2.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel2.TabIndex = 25;
            this.ultraLabel2.Text = "Всего по книге продаж";
            // 
            // ultraLabel3
            // 
            appearance1.TextHAlignAsString = "Left";
            this.ultraLabel3.Appearance = appearance1;
            this.ultraLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel3.Location = new System.Drawing.Point(1, 41);
            this.ultraLabel3.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "18%";
            // 
            // ultraLabel4
            // 
            appearance71.TextHAlignAsString = "Left";
            this.ultraLabel4.Appearance = appearance71;
            this.ultraLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel4.Location = new System.Drawing.Point(1, 61);
            this.ultraLabel4.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "10%";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel7.Location = new System.Drawing.Point(1, 21);
            this.ultraLabel7.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel7.TabIndex = 1;
            this.ultraLabel7.Text = "Стоимость (без налога), по ставке, руб.";
            // 
            // ultraLabel11
            // 
            appearance7.TextHAlignAsString = "Left";
            this.ultraLabel11.Appearance = appearance7;
            this.ultraLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel11.Location = new System.Drawing.Point(1, 121);
            this.ultraLabel11.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel11.TabIndex = 5;
            this.ultraLabel11.Text = "18%";
            // 
            // ultraLabel14
            // 
            appearance8.TextHAlignAsString = "Left";
            this.ultraLabel14.Appearance = appearance8;
            this.ultraLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel14.Location = new System.Drawing.Point(1, 161);
            this.ultraLabel14.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(387, 18);
            this.ultraLabel14.TabIndex = 8;
            this.ultraLabel14.Text = "Стоимость продаж, освобождаемых от налога, руб.";
            // 
            // ultraLabelR9P240
            // 
            appearance23.TextHAlignAsString = "Left";
            this.ultraLabelR9P240.Appearance = appearance23;
            this.ultraLabelR9P240.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P240.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P240.Location = new System.Drawing.Point(390, 61);
            this.ultraLabelR9P240.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P240.Name = "ultraLabelR9P240";
            this.ultraLabelR9P240.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P240.TabIndex = 0;
            this.ultraLabelR9P240.Text = "0";
            // 
            // ultraLabelR9P230
            // 
            appearance72.TextHAlignAsString = "Left";
            this.ultraLabelR9P230.Appearance = appearance72;
            this.ultraLabelR9P230.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P230.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P230.Location = new System.Drawing.Point(390, 41);
            this.ultraLabelR9P230.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P230.Name = "ultraLabelR9P230";
            this.ultraLabelR9P230.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P230.TabIndex = 0;
            // 
            // ultraLabel29
            // 
            appearance22.TextHAlignAsString = "Left";
            this.ultraLabel29.Appearance = appearance22;
            this.ultraLabel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel29.Location = new System.Drawing.Point(390, 101);
            this.ultraLabel29.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel29.TabIndex = 21;
            // 
            // ultraLabelR9P260
            // 
            appearance21.TextHAlignAsString = "Left";
            this.ultraLabelR9P260.Appearance = appearance21;
            this.ultraLabelR9P260.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P260.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P260.Location = new System.Drawing.Point(390, 121);
            this.ultraLabelR9P260.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P260.Name = "ultraLabelR9P260";
            this.ultraLabelR9P260.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P260.TabIndex = 22;
            // 
            // ultraLabelR9P280
            // 
            appearance20.TextHAlignAsString = "Left";
            this.ultraLabelR9P280.Appearance = appearance20;
            this.ultraLabelR9P280.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9P280.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9P280.Location = new System.Drawing.Point(390, 161);
            this.ultraLabelR9P280.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9P280.Name = "ultraLabelR9P280";
            this.ultraLabelR9P280.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9P280.TabIndex = 23;
            // 
            // ultraLabel6
            // 
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance10;
            this.ultraLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel6.Location = new System.Drawing.Point(642, 1);
            this.ultraLabel6.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel6.TabIndex = 28;
            this.ultraLabel6.Text = "Итого по книге продаж";
            // 
            // ultraLabel10
            // 
            appearance68.TextHAlignAsString = "Left";
            this.ultraLabel10.Appearance = appearance68;
            this.ultraLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel10.Location = new System.Drawing.Point(642, 21);
            this.ultraLabel10.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel10.TabIndex = 32;
            // 
            // ultraLabelR9_1P020
            // 
            appearance66.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P020.Appearance = appearance66;
            this.ultraLabelR9_1P020.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P020.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P020.Location = new System.Drawing.Point(642, 41);
            this.ultraLabelR9_1P020.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P020.Name = "ultraLabelR9_1P020";
            this.ultraLabelR9_1P020.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P020.TabIndex = 34;
            // 
            // ultraLabelR9_1P030
            // 
            appearance39.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P030.Appearance = appearance39;
            this.ultraLabelR9_1P030.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P030.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P030.Location = new System.Drawing.Point(642, 61);
            this.ultraLabelR9_1P030.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P030.Name = "ultraLabelR9_1P030";
            this.ultraLabelR9_1P030.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P030.TabIndex = 36;
            // 
            // ultraLabelR9_1P040
            // 
            appearance24.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P040.Appearance = appearance24;
            this.ultraLabelR9_1P040.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P040.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P040.Location = new System.Drawing.Point(642, 81);
            this.ultraLabelR9_1P040.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P040.Name = "ultraLabelR9_1P040";
            this.ultraLabelR9_1P040.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P040.TabIndex = 40;
            // 
            // ultraLabel26
            // 
            appearance19.TextHAlignAsString = "Left";
            this.ultraLabel26.Appearance = appearance19;
            this.ultraLabel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel26.Location = new System.Drawing.Point(642, 101);
            this.ultraLabel26.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(250, 18);
            this.ultraLabel26.TabIndex = 43;
            // 
            // ultraLabelR9_1P050
            // 
            appearance17.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P050.Appearance = appearance17;
            this.ultraLabelR9_1P050.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P050.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P050.Location = new System.Drawing.Point(642, 121);
            this.ultraLabelR9_1P050.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P050.Name = "ultraLabelR9_1P050";
            this.ultraLabelR9_1P050.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P050.TabIndex = 45;
            // 
            // ultraLabelR9_1P060
            // 
            appearance14.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P060.Appearance = appearance14;
            this.ultraLabelR9_1P060.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P060.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P060.Location = new System.Drawing.Point(642, 141);
            this.ultraLabelR9_1P060.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P060.Name = "ultraLabelR9_1P060";
            this.ultraLabelR9_1P060.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P060.TabIndex = 48;
            // 
            // ultraLabelR9_1P070
            // 
            appearance12.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P070.Appearance = appearance12;
            this.ultraLabelR9_1P070.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P070.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P070.Location = new System.Drawing.Point(642, 161);
            this.ultraLabelR9_1P070.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P070.Name = "ultraLabelR9_1P070";
            this.ultraLabelR9_1P070.Size = new System.Drawing.Size(250, 18);
            this.ultraLabelR9_1P070.TabIndex = 50;
            // 
            // ultraLabel5
            // 
            appearance70.TextHAlignAsString = "Left";
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance70;
            this.ultraLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel5.Location = new System.Drawing.Point(894, 1);
            this.ultraLabel5.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(251, 18);
            this.ultraLabel5.TabIndex = 52;
            this.ultraLabel5.Text = "Всего по Приложению 1 к разделу 9";
            // 
            // ultraLabel8
            // 
            appearance69.TextHAlignAsString = "Left";
            this.ultraLabel8.Appearance = appearance69;
            this.ultraLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel8.Location = new System.Drawing.Point(894, 21);
            this.ultraLabel8.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(251, 18);
            this.ultraLabel8.TabIndex = 53;
            // 
            // ultraLabelR9_1P310
            // 
            appearance67.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P310.Appearance = appearance67;
            this.ultraLabelR9_1P310.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P310.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P310.Location = new System.Drawing.Point(894, 41);
            this.ultraLabelR9_1P310.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P310.Name = "ultraLabelR9_1P310";
            this.ultraLabelR9_1P310.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P310.TabIndex = 54;
            // 
            // ultraLabelR9_1P320
            // 
            appearance65.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P320.Appearance = appearance65;
            this.ultraLabelR9_1P320.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P320.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P320.Location = new System.Drawing.Point(894, 61);
            this.ultraLabelR9_1P320.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P320.Name = "ultraLabelR9_1P320";
            this.ultraLabelR9_1P320.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P320.TabIndex = 55;
            // 
            // ultraLabelR9_1P330
            // 
            appearance35.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P330.Appearance = appearance35;
            this.ultraLabelR9_1P330.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P330.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P330.Location = new System.Drawing.Point(894, 81);
            this.ultraLabelR9_1P330.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P330.Name = "ultraLabelR9_1P330";
            this.ultraLabelR9_1P330.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P330.TabIndex = 56;
            // 
            // ultraLabel25
            // 
            appearance25.TextHAlignAsString = "Left";
            this.ultraLabel25.Appearance = appearance25;
            this.ultraLabel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel25.Location = new System.Drawing.Point(894, 101);
            this.ultraLabel25.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(251, 18);
            this.ultraLabel25.TabIndex = 57;
            // 
            // ultraLabelR9_1P340
            // 
            appearance18.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P340.Appearance = appearance18;
            this.ultraLabelR9_1P340.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P340.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P340.Location = new System.Drawing.Point(894, 121);
            this.ultraLabelR9_1P340.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P340.Name = "ultraLabelR9_1P340";
            this.ultraLabelR9_1P340.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P340.TabIndex = 58;
            // 
            // ultraLabelR9_1P350
            // 
            appearance15.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P350.Appearance = appearance15;
            this.ultraLabelR9_1P350.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P350.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P350.Location = new System.Drawing.Point(894, 141);
            this.ultraLabelR9_1P350.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P350.Name = "ultraLabelR9_1P350";
            this.ultraLabelR9_1P350.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P350.TabIndex = 59;
            // 
            // ultraLabelR9_1P360
            // 
            appearance60.TextHAlignAsString = "Left";
            this.ultraLabelR9_1P360.Appearance = appearance60;
            this.ultraLabelR9_1P360.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelR9_1P360.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabelR9_1P360.Location = new System.Drawing.Point(894, 161);
            this.ultraLabelR9_1P360.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelR9_1P360.Name = "ultraLabelR9_1P360";
            this.ultraLabelR9_1P360.Size = new System.Drawing.Size(251, 18);
            this.ultraLabelR9_1P360.TabIndex = 60;
            // 
            // panelChapter9_TitleActualData
            // 
            // 
            // panelChapter9_TitleActualData.ClientArea
            // 
            this.panelChapter9_TitleActualData.ClientArea.Controls.Add(this.panelChapter9_TitleActualData_01);
            this.panelChapter9_TitleActualData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapter9_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.panelChapter9_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapter9_TitleActualData.Name = "panelChapter9_TitleActualData";
            this.panelChapter9_TitleActualData.Size = new System.Drawing.Size(1146, 21);
            this.panelChapter9_TitleActualData.TabIndex = 5;
            // 
            // panelChapter9_TitleActualData_01
            // 
            // 
            // panelChapter9_TitleActualData_01.ClientArea
            // 
            this.panelChapter9_TitleActualData_01.ClientArea.Controls.Add(this.labelChapter9_TitleActualData);
            this.panelChapter9_TitleActualData_01.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapter9_TitleActualData_01.Location = new System.Drawing.Point(0, 0);
            this.panelChapter9_TitleActualData_01.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapter9_TitleActualData_01.Name = "panelChapter9_TitleActualData_01";
            this.panelChapter9_TitleActualData_01.Size = new System.Drawing.Size(1146, 18);
            this.panelChapter9_TitleActualData_01.TabIndex = 4;
            // 
            // labelChapter9_TitleActualData
            // 
            appearance75.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelChapter9_TitleActualData.Appearance = appearance75;
            this.labelChapter9_TitleActualData.AutoSize = true;
            this.labelChapter9_TitleActualData.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelChapter9_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.labelChapter9_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.labelChapter9_TitleActualData.Name = "labelChapter9_TitleActualData";
            this.labelChapter9_TitleActualData.Size = new System.Drawing.Size(509, 18);
            this.labelChapter9_TitleActualData.TabIndex = 3;
            this.labelChapter9_TitleActualData.TabStop = true;
            this.labelChapter9_TitleActualData.Value = "Данные по <b>текущей</b> корректировке Т (раздел 9) и по корректировке N (приложе" +
    "ние 1 к разделу 9)";
            // 
            // tabR10
            // 
            this.tabR10.Controls.Add(this.dataGridR10);
            this.tabR10.Controls.Add(this.ulRedWords10);
            this.tabR10.Controls.Add(this.panelChapterInfo10);
            this.tabR10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR10.Name = "tabR10";
            this.tabR10.Size = new System.Drawing.Size(1152, 424);
            // 
            // dataGridR10
            // 
            this.dataGridR10.AggregatePanelVisible = true;
            this.dataGridR10.AllowFilterReset = false;
            this.dataGridR10.AllowMultiGrouping = true;
            this.dataGridR10.AllowResetSettings = false;
            this.dataGridR10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.dataGridR10.BackColor = System.Drawing.Color.Transparent;
            this.dataGridR10.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.dataGridR10.ColumnVisibilitySetupButtonVisible = true;
            this.dataGridR10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridR10.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.dataGridR10.ExportExcelCancelVisible = false;
            this.dataGridR10.ExportExcelHint = "Экспорт в MS Excel";
            this.dataGridR10.ExportExcelVisible = true;
            this.dataGridR10.FilterResetVisible = true;
            this.dataGridR10.FooterVisible = true;
            this.dataGridR10.GridContextMenuStrip = this.R10ContextMenu;
            this.dataGridR10.Location = new System.Drawing.Point(0, 54);
            this.dataGridR10.Name = "dataGridR10";
            this.dataGridR10.PanelExportExcelStateVisible = true;
            this.dataGridR10.PanelLoadingVisible = false;
            this.dataGridR10.PanelPagesVisible = true;
            this.dataGridR10.RowDoubleClicked = null;
            this.dataGridR10.Size = new System.Drawing.Size(1152, 370);
            this.dataGridR10.TabIndex = 7;
            this.dataGridR10.Title = "";
            // 
            // R10ContextMenu
            // 
            this.R10ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiR10OpenBuyer,
            this.cmiR10OpenBroker,
            this.cmiR10OpenDecl});
            this.R10ContextMenu.Name = "R10ContextMenu";
            this.R10ContextMenu.Size = new System.Drawing.Size(274, 70);
            this.R10ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.R10ContextMenu_Opening);
            // 
            // cmiR10OpenBuyer
            // 
            this.cmiR10OpenBuyer.Name = "cmiR10OpenBuyer";
            this.cmiR10OpenBuyer.Size = new System.Drawing.Size(273, 22);
            this.cmiR10OpenBuyer.Text = "Открыть карточку покупателя";
            this.cmiR10OpenBuyer.Click += new System.EventHandler(this.cmiR10OpenBuyer_Click);
            // 
            // cmiR10OpenBroker
            // 
            this.cmiR10OpenBroker.Name = "cmiR10OpenBroker";
            this.cmiR10OpenBroker.Size = new System.Drawing.Size(273, 22);
            this.cmiR10OpenBroker.Text = "Открыть карточку посредника";
            this.cmiR10OpenBroker.Click += new System.EventHandler(this.cmiR10OpenBroker_Click);
            // 
            // cmiR10OpenDecl
            // 
            this.cmiR10OpenDecl.Name = "cmiR10OpenDecl";
            this.cmiR10OpenDecl.Size = new System.Drawing.Size(273, 22);
            this.cmiR10OpenDecl.Text = "Перейти на сопоставленную запись";
            this.cmiR10OpenDecl.Click += new System.EventHandler(this.cmiR10OpenDecl_Click);
            // 
            // ulRedWords10
            // 
            appearance62.FontData.BoldAsString = "True";
            appearance62.ForeColor = System.Drawing.Color.Red;
            appearance62.TextHAlignAsString = "Center";
            appearance62.TextVAlignAsString = "Middle";
            this.ulRedWords10.Appearance = appearance62;
            this.ulRedWords10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords10.Location = new System.Drawing.Point(0, 54);
            this.ulRedWords10.Name = "ulRedWords10";
            this.ulRedWords10.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords10.Size = new System.Drawing.Size(1152, 370);
            this.ulRedWords10.TabIndex = 5;
            this.ulRedWords10.Text = "Сделки с контрагентами отсутствуют";
            this.ulRedWords10.Visible = false;
            // 
            // panelChapterInfo10
            // 
            // 
            // panelChapterInfo10.ClientArea
            // 
            this.panelChapterInfo10.ClientArea.Controls.Add(this.captionR10);
            this.panelChapterInfo10.ClientArea.Controls.Add(this.labelChapter10_TitleActualData);
            this.panelChapterInfo10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapterInfo10.Location = new System.Drawing.Point(0, 0);
            this.panelChapterInfo10.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapterInfo10.Name = "panelChapterInfo10";
            this.panelChapterInfo10.Size = new System.Drawing.Size(1152, 54);
            this.panelChapterInfo10.TabIndex = 6;
            // 
            // captionR10
            // 
            this.captionR10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.captionR10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionR10.Location = new System.Drawing.Point(0, 18);
            this.captionR10.Name = "captionR10";
            this.captionR10.Padding = new System.Drawing.Size(2, 2);
            this.captionR10.Size = new System.Drawing.Size(1152, 36);
            this.captionR10.TabIndex = 2;
            this.captionR10.Text = resources.GetString("captionR10.Text");
            // 
            // labelChapter10_TitleActualData
            // 
            appearance76.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelChapter10_TitleActualData.Appearance = appearance76;
            this.labelChapter10_TitleActualData.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelChapter10_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.labelChapter10_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.labelChapter10_TitleActualData.Name = "labelChapter10_TitleActualData";
            this.labelChapter10_TitleActualData.Size = new System.Drawing.Size(1152, 18);
            this.labelChapter10_TitleActualData.TabIndex = 4;
            this.labelChapter10_TitleActualData.TabStop = true;
            this.labelChapter10_TitleActualData.Value = "Данные по <b>текущей</b> корректировке";
            // 
            // tabR11
            // 
            this.tabR11.Controls.Add(this.dataGridR11);
            this.tabR11.Controls.Add(this.ulRedWords11);
            this.tabR11.Controls.Add(this.panelChapterInfo11);
            this.tabR11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR11.Name = "tabR11";
            this.tabR11.Size = new System.Drawing.Size(1152, 424);
            // 
            // dataGridR11
            // 
            this.dataGridR11.AggregatePanelVisible = true;
            this.dataGridR11.AllowFilterReset = false;
            this.dataGridR11.AllowMultiGrouping = true;
            this.dataGridR11.AllowResetSettings = false;
            this.dataGridR11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.dataGridR11.BackColor = System.Drawing.Color.Transparent;
            this.dataGridR11.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.dataGridR11.ColumnVisibilitySetupButtonVisible = true;
            this.dataGridR11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridR11.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.dataGridR11.ExportExcelCancelVisible = false;
            this.dataGridR11.ExportExcelHint = "Экспорт в MS Excel";
            this.dataGridR11.ExportExcelVisible = true;
            this.dataGridR11.FilterResetVisible = true;
            this.dataGridR11.FooterVisible = true;
            this.dataGridR11.GridContextMenuStrip = this.R11ContextMenu;
            this.dataGridR11.Location = new System.Drawing.Point(0, 54);
            this.dataGridR11.Name = "dataGridR11";
            this.dataGridR11.PanelExportExcelStateVisible = true;
            this.dataGridR11.PanelLoadingVisible = false;
            this.dataGridR11.PanelPagesVisible = true;
            this.dataGridR11.RowDoubleClicked = null;
            this.dataGridR11.Size = new System.Drawing.Size(1152, 370);
            this.dataGridR11.TabIndex = 7;
            this.dataGridR11.Title = "";
            // 
            // R11ContextMenu
            // 
            this.R11ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiR11OpenSeller,
            this.cmiR11OpenBroker,
            this.cmiR11OpenDecl});
            this.R11ContextMenu.Name = "R11ContextMenu";
            this.R11ContextMenu.Size = new System.Drawing.Size(278, 70);
            this.R11ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.R11ContextMenu_Opening);
            // 
            // cmiR11OpenSeller
            // 
            this.cmiR11OpenSeller.Name = "cmiR11OpenSeller";
            this.cmiR11OpenSeller.Size = new System.Drawing.Size(277, 22);
            this.cmiR11OpenSeller.Text = "Открыть карточку продавца";
            this.cmiR11OpenSeller.Click += new System.EventHandler(this.cmiR11OpenSeller_Click);
            // 
            // cmiR11OpenBroker
            // 
            this.cmiR11OpenBroker.Name = "cmiR11OpenBroker";
            this.cmiR11OpenBroker.Size = new System.Drawing.Size(277, 22);
            this.cmiR11OpenBroker.Text = "Открыть карточку субкомиссионера";
            this.cmiR11OpenBroker.Click += new System.EventHandler(this.cmiR11OpenBroker_Click);
            // 
            // cmiR11OpenDecl
            // 
            this.cmiR11OpenDecl.Name = "cmiR11OpenDecl";
            this.cmiR11OpenDecl.Size = new System.Drawing.Size(277, 22);
            this.cmiR11OpenDecl.Text = "Перейти на сопоставленную запись";
            this.cmiR11OpenDecl.Click += new System.EventHandler(this.cmiR11OpenDecl_Click);
            // 
            // ulRedWords11
            // 
            appearance63.FontData.BoldAsString = "True";
            appearance63.ForeColor = System.Drawing.Color.Red;
            appearance63.TextHAlignAsString = "Center";
            appearance63.TextVAlignAsString = "Middle";
            this.ulRedWords11.Appearance = appearance63;
            this.ulRedWords11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords11.Location = new System.Drawing.Point(0, 54);
            this.ulRedWords11.Name = "ulRedWords11";
            this.ulRedWords11.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords11.Size = new System.Drawing.Size(1152, 370);
            this.ulRedWords11.TabIndex = 5;
            this.ulRedWords11.Text = "Сделки с контрагентами отсутствуют";
            this.ulRedWords11.Visible = false;
            // 
            // panelChapterInfo11
            // 
            // 
            // panelChapterInfo11.ClientArea
            // 
            this.panelChapterInfo11.ClientArea.Controls.Add(this.captionR11);
            this.panelChapterInfo11.ClientArea.Controls.Add(this.labelChapter11_TitleActualData);
            this.panelChapterInfo11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapterInfo11.Location = new System.Drawing.Point(0, 0);
            this.panelChapterInfo11.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapterInfo11.Name = "panelChapterInfo11";
            this.panelChapterInfo11.Size = new System.Drawing.Size(1152, 54);
            this.panelChapterInfo11.TabIndex = 6;
            // 
            // captionR11
            // 
            this.captionR11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.captionR11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionR11.Location = new System.Drawing.Point(0, 18);
            this.captionR11.Name = "captionR11";
            this.captionR11.Padding = new System.Drawing.Size(2, 2);
            this.captionR11.Size = new System.Drawing.Size(1152, 36);
            this.captionR11.TabIndex = 3;
            this.captionR11.Text = resources.GetString("captionR11.Text");
            // 
            // labelChapter11_TitleActualData
            // 
            appearance74.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelChapter11_TitleActualData.Appearance = appearance74;
            this.labelChapter11_TitleActualData.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelChapter11_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.labelChapter11_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.labelChapter11_TitleActualData.Name = "labelChapter11_TitleActualData";
            this.labelChapter11_TitleActualData.Size = new System.Drawing.Size(1152, 18);
            this.labelChapter11_TitleActualData.TabIndex = 5;
            this.labelChapter11_TitleActualData.TabStop = true;
            this.labelChapter11_TitleActualData.Value = "Данные по <b>текущей</b> корректировке";
            // 
            // tabR12
            // 
            this.tabR12.Controls.Add(this.dataGridR12);
            this.tabR12.Controls.Add(this.ulRedWords12);
            this.tabR12.Controls.Add(this.panelChapter12_TitleActualData);
            this.tabR12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR12.Name = "tabR12";
            this.tabR12.Size = new System.Drawing.Size(1152, 424);
            // 
            // dataGridR12
            // 
            this.dataGridR12.AggregatePanelVisible = true;
            this.dataGridR12.AllowFilterReset = false;
            this.dataGridR12.AllowMultiGrouping = true;
            this.dataGridR12.AllowResetSettings = false;
            this.dataGridR12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.dataGridR12.BackColor = System.Drawing.Color.Transparent;
            this.dataGridR12.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.dataGridR12.ColumnVisibilitySetupButtonVisible = true;
            this.dataGridR12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridR12.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.dataGridR12.ExportExcelCancelVisible = false;
            this.dataGridR12.ExportExcelHint = "Экспорт в MS Excel";
            this.dataGridR12.ExportExcelVisible = true;
            this.dataGridR12.FilterResetVisible = true;
            this.dataGridR12.FooterVisible = true;
            this.dataGridR12.GridContextMenuStrip = this.R12ContextMenu;
            this.dataGridR12.Location = new System.Drawing.Point(0, 18);
            this.dataGridR12.Name = "dataGridR12";
            this.dataGridR12.PanelExportExcelStateVisible = true;
            this.dataGridR12.PanelLoadingVisible = false;
            this.dataGridR12.PanelPagesVisible = true;
            this.dataGridR12.RowDoubleClicked = null;
            this.dataGridR12.Size = new System.Drawing.Size(1152, 406);
            this.dataGridR12.TabIndex = 7;
            this.dataGridR12.Title = "Сведения из счетов-фактур, выставленных лицами, указанными в п. 5 ст. 173 НК РФ";
            // 
            // R12ContextMenu
            // 
            this.R12ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiR12OpenBuyer,
            this.cmiR12OpenDecl});
            this.R12ContextMenu.Name = "R12ContextMenu";
            this.R12ContextMenu.Size = new System.Drawing.Size(274, 48);
            this.R12ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.R12ContextMenu_Opening);
            // 
            // cmiR12OpenBuyer
            // 
            this.cmiR12OpenBuyer.Name = "cmiR12OpenBuyer";
            this.cmiR12OpenBuyer.Size = new System.Drawing.Size(273, 22);
            this.cmiR12OpenBuyer.Text = "Открыть карточку покупателя";
            this.cmiR12OpenBuyer.Click += new System.EventHandler(this.cmiR12OpenBuyer_Click);
            // 
            // cmiR12OpenDecl
            // 
            this.cmiR12OpenDecl.Name = "cmiR12OpenDecl";
            this.cmiR12OpenDecl.Size = new System.Drawing.Size(273, 22);
            this.cmiR12OpenDecl.Text = "Перейти на сопоставленную запись";
            this.cmiR12OpenDecl.Click += new System.EventHandler(this.cmiR12OpenDecl_Click);
            // 
            // ulRedWords12
            // 
            appearance13.FontData.BoldAsString = "True";
            appearance13.ForeColor = System.Drawing.Color.Red;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.ulRedWords12.Appearance = appearance13;
            this.ulRedWords12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords12.Location = new System.Drawing.Point(0, 18);
            this.ulRedWords12.Name = "ulRedWords12";
            this.ulRedWords12.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords12.Size = new System.Drawing.Size(1152, 406);
            this.ulRedWords12.TabIndex = 5;
            this.ulRedWords12.Text = "Сделки с контрагентами отсутствуют";
            this.ulRedWords12.Visible = false;
            // 
            // panelChapter12_TitleActualData
            // 
            // 
            // panelChapter12_TitleActualData.ClientArea
            // 
            this.panelChapter12_TitleActualData.ClientArea.Controls.Add(this.labelChapter12_TitleActualData);
            this.panelChapter12_TitleActualData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChapter12_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.panelChapter12_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.panelChapter12_TitleActualData.Name = "panelChapter12_TitleActualData";
            this.panelChapter12_TitleActualData.Size = new System.Drawing.Size(1152, 18);
            this.panelChapter12_TitleActualData.TabIndex = 6;
            // 
            // labelChapter12_TitleActualData
            // 
            appearance64.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelChapter12_TitleActualData.Appearance = appearance64;
            this.labelChapter12_TitleActualData.AutoSize = true;
            this.labelChapter12_TitleActualData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelChapter12_TitleActualData.Location = new System.Drawing.Point(0, 0);
            this.labelChapter12_TitleActualData.Margin = new System.Windows.Forms.Padding(0);
            this.labelChapter12_TitleActualData.Name = "labelChapter12_TitleActualData";
            this.labelChapter12_TitleActualData.Size = new System.Drawing.Size(1152, 18);
            this.labelChapter12_TitleActualData.TabIndex = 3;
            this.labelChapter12_TitleActualData.TabStop = true;
            this.labelChapter12_TitleActualData.Value = "Данные по <b>текущей</b> корректировке";
            // 
            // tabControlRatio
            // 
            this.tabControlRatio.Controls.Add(this.gridControlRatio);
            this.tabControlRatio.Location = new System.Drawing.Point(-10000, -10000);
            this.tabControlRatio.Name = "tabControlRatio";
            this.tabControlRatio.Size = new System.Drawing.Size(1152, 424);
            // 
            // gridControlRatio
            // 
            this.gridControlRatio.AddVirtualCheckColumn = false;
            this.gridControlRatio.AggregatePanelVisible = true;
            this.gridControlRatio.AllowMultiGrouping = true;
            this.gridControlRatio.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridControlRatio.AllowSaveFilterSettings = false;
            this.gridControlRatio.BackColor = System.Drawing.Color.Transparent;
            this.gridControlRatio.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridControlRatio.DefaultPageSize = "";
            this.gridControlRatio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRatio.ExportExcelCancelVisible = false;
            this.gridControlRatio.ExportExcelVisible = false;
            this.gridControlRatio.FilterResetVisible = false;
            this.gridControlRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridControlRatio.FooterVisible = true;
            this.gridControlRatio.GridContextMenuStrip = null;
            this.gridControlRatio.Location = new System.Drawing.Point(0, 0);
            this.gridControlRatio.Name = "gridControlRatio";
            this.gridControlRatio.PanelExportExcelStateVisible = false;
            this.gridControlRatio.PanelLoadingVisible = false;
            this.gridControlRatio.PanelPagesVisible = true;
            this.gridControlRatio.RowDoubleClicked = null;
            this.gridControlRatio.Setup = null;
            this.gridControlRatio.Size = new System.Drawing.Size(1152, 424);
            this.gridControlRatio.TabIndex = 3;
            this.gridControlRatio.Title = "Контрольные соотношения";
            // 
            // tabDiscrepancies
            // 
            this.tabDiscrepancies.Controls.Add(this.gridDiscrepancies);
            this.tabDiscrepancies.Location = new System.Drawing.Point(-10000, -10000);
            this.tabDiscrepancies.Name = "tabDiscrepancies";
            this.tabDiscrepancies.Size = new System.Drawing.Size(1152, 424);
            // 
            // gridDiscrepancies
            // 
            this.gridDiscrepancies.AddVirtualCheckColumn = false;
            this.gridDiscrepancies.AggregatePanelVisible = true;
            this.gridDiscrepancies.AllowMultiGrouping = true;
            this.gridDiscrepancies.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridDiscrepancies.AllowSaveFilterSettings = false;
            this.gridDiscrepancies.BackColor = System.Drawing.Color.Transparent;
            this.gridDiscrepancies.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridDiscrepancies.DefaultPageSize = "";
            this.gridDiscrepancies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDiscrepancies.ExportExcelCancelVisible = false;
            this.gridDiscrepancies.ExportExcelVisible = false;
            this.gridDiscrepancies.FilterResetVisible = false;
            this.gridDiscrepancies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridDiscrepancies.FooterVisible = true;
            this.gridDiscrepancies.GridContextMenuStrip = this.discrepContextMenu;
            this.gridDiscrepancies.Location = new System.Drawing.Point(0, 0);
            this.gridDiscrepancies.Name = "gridDiscrepancies";
            this.gridDiscrepancies.PanelExportExcelStateVisible = false;
            this.gridDiscrepancies.PanelLoadingVisible = true;
            this.gridDiscrepancies.PanelPagesVisible = true;
            this.gridDiscrepancies.RowDoubleClicked = null;
            this.gridDiscrepancies.Setup = null;
            this.gridDiscrepancies.Size = new System.Drawing.Size(1152, 424);
            this.gridDiscrepancies.TabIndex = 2;
            this.gridDiscrepancies.Title = "Список расхождений";
            // 
            // discrepContextMenu
            // 
            this.discrepContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenDiscrep,
            this.cmiOpenTaxPayer,
            this.cmiOpenDecl});
            this.discrepContextMenu.Name = "discrepContextMenu";
            this.discrepContextMenu.Size = new System.Drawing.Size(310, 70);
            // 
            // cmiOpenDiscrep
            // 
            this.cmiOpenDiscrep.Name = "cmiOpenDiscrep";
            this.cmiOpenDiscrep.Size = new System.Drawing.Size(309, 22);
            this.cmiOpenDiscrep.Text = "Открыть карточку расхождения по СФ";
            this.cmiOpenDiscrep.Click += new System.EventHandler(this.cmiOpenDiscrep_Click);
            // 
            // cmiOpenTaxPayer
            // 
            this.cmiOpenTaxPayer.Name = "cmiOpenTaxPayer";
            this.cmiOpenTaxPayer.Size = new System.Drawing.Size(309, 22);
            this.cmiOpenTaxPayer.Text = "Открыть карточку контрагента";
            this.cmiOpenTaxPayer.Click += new System.EventHandler(this.cmiOpenTaxPayer_Click);
            // 
            // cmiOpenDecl
            // 
            this.cmiOpenDecl.Name = "cmiOpenDecl";
            this.cmiOpenDecl.Size = new System.Drawing.Size(309, 22);
            this.cmiOpenDecl.Text = "Открыть декларацию/журнал контрагента";
            this.cmiOpenDecl.Click += new System.EventHandler(this.cmiOpenDecl_Click);
            // 
            // tabDOC
            // 
            this.tabDOC.Controls.Add(this.knpDocLayoutPanel);
            this.tabDOC.Location = new System.Drawing.Point(-10000, -10000);
            this.tabDOC.Name = "tabDOC";
            this.tabDOC.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.tabDOC.Size = new System.Drawing.Size(1152, 424);
            // 
            // knpDocLayoutPanel
            // 
            this.knpDocLayoutPanel.ColumnCount = 1;
            this.knpDocLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.knpDocLayoutPanel.Controls.Add(this.ultraLabel1, 0, 0);
            this.knpDocLayoutPanel.Controls.Add(this._knpDocumentGrid, 0, 1);
            this.knpDocLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.knpDocLayoutPanel.Location = new System.Drawing.Point(0, 5);
            this.knpDocLayoutPanel.Name = "knpDocLayoutPanel";
            this.knpDocLayoutPanel.RowCount = 2;
            this.knpDocLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.knpDocLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.knpDocLayoutPanel.Size = new System.Drawing.Size(1147, 414);
            this.knpDocLayoutPanel.TabIndex = 0;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel1.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(1141, 14);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Документы камеральной налоговой проверки";
            // 
            // _knpDocumentGrid
            // 
            this._knpDocumentGrid.AutoSize = true;
            this._knpDocumentGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._knpDocumentGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._knpDocumentGrid.Location = new System.Drawing.Point(3, 23);
            this._knpDocumentGrid.Name = "_knpDocumentGrid";
            this._knpDocumentGrid.Size = new System.Drawing.Size(1141, 388);
            this._knpDocumentGrid.TabIndex = 1;
            // 
            // tabAct
            // 
            this.tabAct.Controls.Add(this.actDiscrepancyEditView);
            this.tabAct.Enabled = false;
            this.tabAct.Location = new System.Drawing.Point(-10000, -10000);
            this.tabAct.Name = "tabAct";
            this.tabAct.Size = new System.Drawing.Size(1152, 424);
            // 
            // actDiscrepancyEditView
            // 
            this.actDiscrepancyEditView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actDiscrepancyEditView.Location = new System.Drawing.Point(0, 0);
            this.actDiscrepancyEditView.MessageView = null;
            this.actDiscrepancyEditView.Name = "actDiscrepancyEditView";
            this.actDiscrepancyEditView.Size = new System.Drawing.Size(1152, 424);
            this.actDiscrepancyEditView.TabIndex = 0;
            // 
            // tabDecision
            // 
            this.tabDecision.Controls.Add(this.decisionDiscrepancyEditView);
            this.tabDecision.Enabled = false;
            this.tabDecision.Location = new System.Drawing.Point(-10000, -10000);
            this.tabDecision.Name = "tabDecision";
            this.tabDecision.Size = new System.Drawing.Size(1152, 424);
            // 
            // decisionDiscrepancyEditView
            // 
            this.decisionDiscrepancyEditView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.decisionDiscrepancyEditView.Location = new System.Drawing.Point(0, 0);
            this.decisionDiscrepancyEditView.MessageView = null;
            this.decisionDiscrepancyEditView.Name = "decisionDiscrepancyEditView";
            this.decisionDiscrepancyEditView.Size = new System.Drawing.Size(1152, 424);
            this.decisionDiscrepancyEditView.TabIndex = 0;
            // 
            // DeclarationParts
            // 
            this.DeclarationParts.Controls.Add(this.tabShared);
            this.DeclarationParts.Controls.Add(this.tabMain);
            this.DeclarationParts.Controls.Add(this.tabR8);
            this.DeclarationParts.Controls.Add(this.tabR9);
            this.DeclarationParts.Controls.Add(this.tabR10);
            this.DeclarationParts.Controls.Add(this.tabR11);
            this.DeclarationParts.Controls.Add(this.tabDiscrepancies);
            this.DeclarationParts.Controls.Add(this.tabDOC);
            this.DeclarationParts.Controls.Add(this.tabR12);
            this.DeclarationParts.Controls.Add(this.tabControlRatio);
            this.DeclarationParts.Controls.Add(this.tabAct);
            this.DeclarationParts.Controls.Add(this.tabDecision);
            this.DeclarationParts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeclarationParts.Location = new System.Drawing.Point(0, 317);
            this.DeclarationParts.Name = "DeclarationParts";
            this.DeclarationParts.SharedControlsPage = this.tabShared;
            this.DeclarationParts.Size = new System.Drawing.Size(1156, 450);
            this.DeclarationParts.TabIndex = 3;
            ultraTab1.TabPage = this.tabMain;
            ultraTab1.Text = "Раздел 1-7";
            ultraTab2.TabPage = this.tabR8;
            ultraTab2.Text = "Раздел 8";
            ultraTab3.TabPage = this.tabR9;
            ultraTab3.Text = "Раздел 9";
            ultraTab4.TabPage = this.tabR10;
            ultraTab4.Text = "Раздел 10";
            ultraTab5.TabPage = this.tabR11;
            ultraTab5.Text = "Раздел 11";
            ultraTab8.TabPage = this.tabR12;
            ultraTab8.Text = "Раздел 12";
            ultraTab9.TabPage = this.tabControlRatio;
            ultraTab9.Text = "Контрольные соотношения";
            ultraTab6.TabPage = this.tabDiscrepancies;
            ultraTab6.Text = "Список расхождений";
            ultraTab7.TabPage = this.tabDOC;
            ultraTab7.Text = "Документы КНП";
            ultraTab10.TabPage = this.tabAct;
            ultraTab10.Text = "Акт";
            ultraTab11.TabPage = this.tabDecision;
            ultraTab11.Text = "Решение";
            this.DeclarationParts.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4,
            ultraTab5,
            ultraTab8,
            ultraTab9,
            ultraTab6,
            ultraTab7,
            ultraTab10,
            ultraTab11});
            this.DeclarationParts.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.DeclarationParts_SelectedTabChanged);
            // 
            // tabShared
            // 
            this.tabShared.Location = new System.Drawing.Point(-10000, -10000);
            this.tabShared.Name = "tabShared";
            this.tabShared.Size = new System.Drawing.Size(1152, 424);
            // 
            // grpCommonInfo
            // 
            this.grpCommonInfo.Controls.Add(this.grpCommonInfoPanel);
            this.grpCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpCommonInfo.ExpandedSize = new System.Drawing.Size(1156, 224);
            this.grpCommonInfo.Location = new System.Drawing.Point(0, 93);
            this.grpCommonInfo.Name = "grpCommonInfo";
            this.grpCommonInfo.Size = new System.Drawing.Size(1156, 224);
            this.grpCommonInfo.TabIndex = 0;
            this.grpCommonInfo.Text = "Общие сведения";
            this.grpCommonInfo.ExpandedStateChanged += new System.EventHandler(this.grpCommonInfo_ExpandedStateChanged);
            // 
            // grpCommonInfoPanel
            // 
            this.grpCommonInfoPanel.AutoSize = true;
            this.grpCommonInfoPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpCommonInfoPanel.Controls.Add(this.autosizePanel);
            this.grpCommonInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCommonInfoPanel.Location = new System.Drawing.Point(3, 19);
            this.grpCommonInfoPanel.Name = "grpCommonInfoPanel";
            this.grpCommonInfoPanel.Size = new System.Drawing.Size(1150, 202);
            this.grpCommonInfoPanel.TabIndex = 0;
            // 
            // autosizePanel
            // 
            this.autosizePanel.AutoSize = true;
            this.autosizePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            // 
            // autosizePanel.ClientArea
            // 
            this.autosizePanel.ClientArea.Controls.Add(this.tableLayout);
            this.autosizePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.autosizePanel.Location = new System.Drawing.Point(0, 0);
            this.autosizePanel.Name = "autosizePanel";
            this.autosizePanel.Size = new System.Drawing.Size(1150, 242);
            this.autosizePanel.TabIndex = 3;
            // 
            // tableLayout
            // 
            this.tableLayout.AutoSize = true;
            this.tableLayout.ColumnCount = 7;
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.Controls.Add(this.ulNotLoadedInMS, 0, 0);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesCommonPVPSumValue, 1, 4);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesCommonSumValue, 1, 3);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesCommonSumCaption, 0, 3);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesCommonPVPSumCaption, 0, 4);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSFCountCaption, 0, 1);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSFCountValue, 1, 1);
            this.tableLayout.Controls.Add(this.labelCommonSumCaption, 0, 2);
            this.tableLayout.Controls.Add(this.labelBuyBookCaption, 0, 5);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesBuyBookSumCaption, 0, 6);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesBuyBookSumValue, 1, 6);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesBuyBookPVPSumCaption, 0, 7);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesBuyBookPVPSumValue, 1, 7);
            this.tableLayout.Controls.Add(this.labelSellBookCaption, 0, 8);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSellBookSumCaption, 0, 9);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSellBookPVPSumCaption, 0, 10);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSellBookSumValue, 1, 9);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesSellBookPVPSumValue, 1, 10);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPMinSumCaption, 2, 4);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPMinSumValue, 3, 4);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesMinSumCaption, 2, 3);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesMinSumValue, 3, 3);
            this.tableLayout.Controls.Add(this.labelMinSumCaption, 2, 2);
            this.tableLayout.Controls.Add(this.labelMaxSumCaption, 2, 5);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesMaxSumCaption, 2, 6);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPMaxSumCaption, 2, 7);
            this.tableLayout.Controls.Add(this.labelAvgSumCaption, 2, 8);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesAvgSumCaption, 2, 9);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPAvgSumCaption, 2, 10);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesMaxSumValue, 3, 6);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPMaxSumValue, 3, 7);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesAvgSumValue, 3, 9);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesPVPAvgSumValue, 3, 10);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesKSCountCaption, 4, 1);
            this.tableLayout.Controls.Add(this.labelDiscrepanciesKSCountValue, 5, 1);
            this.tableLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayout.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayout.Location = new System.Drawing.Point(0, 0);
            this.tableLayout.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayout.Name = "tableLayout";
            this.tableLayout.RowCount = 12;
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayout.Size = new System.Drawing.Size(1150, 242);
            this.tableLayout.TabIndex = 2;
            // 
            // ulNotLoadedInMS
            // 
            appearance33.FontData.BoldAsString = "True";
            appearance33.ForeColor = System.Drawing.Color.Red;
            appearance33.TextHAlignAsString = "Left";
            appearance33.TextVAlignAsString = "Middle";
            this.ulNotLoadedInMS.Appearance = appearance33;
            this.tableLayout.SetColumnSpan(this.ulNotLoadedInMS, 7);
            this.ulNotLoadedInMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulNotLoadedInMS.Location = new System.Drawing.Point(1, 1);
            this.ulNotLoadedInMS.Margin = new System.Windows.Forms.Padding(1);
            this.ulNotLoadedInMS.Name = "ulNotLoadedInMS";
            this.ulNotLoadedInMS.Size = new System.Drawing.Size(1148, 18);
            this.ulNotLoadedInMS.TabIndex = 25;
            // 
            // labelDiscrepanciesCommonPVPSumValue
            // 
            appearance2.TextHAlignAsString = "Left";
            this.labelDiscrepanciesCommonPVPSumValue.Appearance = appearance2;
            this.labelDiscrepanciesCommonPVPSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesCommonPVPSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesCommonPVPSumValue.Location = new System.Drawing.Point(191, 81);
            this.labelDiscrepanciesCommonPVPSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesCommonPVPSumValue.Name = "labelDiscrepanciesCommonPVPSumValue";
            this.labelDiscrepanciesCommonPVPSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesCommonPVPSumValue.TabIndex = 0;
            this.labelDiscrepanciesCommonPVPSumValue.Text = "0";
            // 
            // labelDiscrepanciesCommonSumValue
            // 
            appearance3.TextHAlignAsString = "Left";
            this.labelDiscrepanciesCommonSumValue.Appearance = appearance3;
            this.labelDiscrepanciesCommonSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesCommonSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesCommonSumValue.Location = new System.Drawing.Point(191, 61);
            this.labelDiscrepanciesCommonSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesCommonSumValue.Name = "labelDiscrepanciesCommonSumValue";
            this.labelDiscrepanciesCommonSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesCommonSumValue.TabIndex = 0;
            this.labelDiscrepanciesCommonSumValue.Text = "2 939 044 205, 99";
            // 
            // labelDiscrepanciesCommonSumCaption
            // 
            appearance4.TextHAlignAsString = "Left";
            this.labelDiscrepanciesCommonSumCaption.Appearance = appearance4;
            this.labelDiscrepanciesCommonSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesCommonSumCaption.Location = new System.Drawing.Point(1, 61);
            this.labelDiscrepanciesCommonSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesCommonSumCaption.Name = "labelDiscrepanciesCommonSumCaption";
            this.labelDiscrepanciesCommonSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesCommonSumCaption.TabIndex = 0;
            this.labelDiscrepanciesCommonSumCaption.Text = "                Расхождений, руб.:";
            // 
            // labelDiscrepanciesCommonPVPSumCaption
            // 
            appearance5.TextHAlignAsString = "Left";
            this.labelDiscrepanciesCommonPVPSumCaption.Appearance = appearance5;
            this.labelDiscrepanciesCommonPVPSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesCommonPVPSumCaption.Location = new System.Drawing.Point(1, 81);
            this.labelDiscrepanciesCommonPVPSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesCommonPVPSumCaption.Name = "labelDiscrepanciesCommonPVPSumCaption";
            this.labelDiscrepanciesCommonPVPSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesCommonPVPSumCaption.TabIndex = 0;
            this.labelDiscrepanciesCommonPVPSumCaption.Text = "                               ПВП, руб.:";
            // 
            // labelDiscrepanciesSFCountCaption
            // 
            this.labelDiscrepanciesSFCountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSFCountCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesSFCountCaption.Location = new System.Drawing.Point(1, 21);
            this.labelDiscrepanciesSFCountCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSFCountCaption.Name = "labelDiscrepanciesSFCountCaption";
            this.labelDiscrepanciesSFCountCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesSFCountCaption.TabIndex = 0;
            this.labelDiscrepanciesSFCountCaption.Text = "Количество расхождений по СФ:";
            // 
            // labelDiscrepanciesSFCountValue
            // 
            appearance6.TextHAlignAsString = "Left";
            this.labelDiscrepanciesSFCountValue.Appearance = appearance6;
            this.labelDiscrepanciesSFCountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSFCountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesSFCountValue.Location = new System.Drawing.Point(191, 21);
            this.labelDiscrepanciesSFCountValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSFCountValue.Name = "labelDiscrepanciesSFCountValue";
            this.labelDiscrepanciesSFCountValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesSFCountValue.TabIndex = 0;
            this.labelDiscrepanciesSFCountValue.Text = "1000000";
            // 
            // labelCommonSumCaption
            // 
            this.labelCommonSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCommonSumCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCommonSumCaption.Location = new System.Drawing.Point(1, 41);
            this.labelCommonSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCommonSumCaption.Name = "labelCommonSumCaption";
            this.labelCommonSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelCommonSumCaption.TabIndex = 1;
            this.labelCommonSumCaption.Text = "Общая сумма";
            // 
            // labelBuyBookCaption
            // 
            this.labelBuyBookCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBuyBookCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBuyBookCaption.Location = new System.Drawing.Point(1, 101);
            this.labelBuyBookCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelBuyBookCaption.Name = "labelBuyBookCaption";
            this.labelBuyBookCaption.Size = new System.Drawing.Size(188, 18);
            this.labelBuyBookCaption.TabIndex = 2;
            this.labelBuyBookCaption.Text = "Книга покупок";
            // 
            // labelDiscrepanciesBuyBookSumCaption
            // 
            appearance11.TextHAlignAsString = "Right";
            this.labelDiscrepanciesBuyBookSumCaption.Appearance = appearance11;
            this.labelDiscrepanciesBuyBookSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesBuyBookSumCaption.Location = new System.Drawing.Point(1, 121);
            this.labelDiscrepanciesBuyBookSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesBuyBookSumCaption.Name = "labelDiscrepanciesBuyBookSumCaption";
            this.labelDiscrepanciesBuyBookSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesBuyBookSumCaption.TabIndex = 3;
            this.labelDiscrepanciesBuyBookSumCaption.Text = "Сумма расхождений, руб.:";
            // 
            // labelDiscrepanciesBuyBookSumValue
            // 
            appearance40.TextHAlignAsString = "Left";
            this.labelDiscrepanciesBuyBookSumValue.Appearance = appearance40;
            this.labelDiscrepanciesBuyBookSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesBuyBookSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesBuyBookSumValue.Location = new System.Drawing.Point(191, 121);
            this.labelDiscrepanciesBuyBookSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesBuyBookSumValue.Name = "labelDiscrepanciesBuyBookSumValue";
            this.labelDiscrepanciesBuyBookSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesBuyBookSumValue.TabIndex = 4;
            this.labelDiscrepanciesBuyBookSumValue.Text = "0";
            // 
            // labelDiscrepanciesBuyBookPVPSumCaption
            // 
            appearance41.TextHAlignAsString = "Right";
            this.labelDiscrepanciesBuyBookPVPSumCaption.Appearance = appearance41;
            this.labelDiscrepanciesBuyBookPVPSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesBuyBookPVPSumCaption.Location = new System.Drawing.Point(1, 141);
            this.labelDiscrepanciesBuyBookPVPSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesBuyBookPVPSumCaption.Name = "labelDiscrepanciesBuyBookPVPSumCaption";
            this.labelDiscrepanciesBuyBookPVPSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesBuyBookPVPSumCaption.TabIndex = 5;
            this.labelDiscrepanciesBuyBookPVPSumCaption.Text = "Сумма ПВП, руб.:";
            // 
            // labelDiscrepanciesBuyBookPVPSumValue
            // 
            appearance42.TextHAlignAsString = "Left";
            this.labelDiscrepanciesBuyBookPVPSumValue.Appearance = appearance42;
            this.labelDiscrepanciesBuyBookPVPSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesBuyBookPVPSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesBuyBookPVPSumValue.Location = new System.Drawing.Point(191, 141);
            this.labelDiscrepanciesBuyBookPVPSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesBuyBookPVPSumValue.Name = "labelDiscrepanciesBuyBookPVPSumValue";
            this.labelDiscrepanciesBuyBookPVPSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesBuyBookPVPSumValue.TabIndex = 6;
            this.labelDiscrepanciesBuyBookPVPSumValue.Text = "0";
            // 
            // labelSellBookCaption
            // 
            this.labelSellBookCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSellBookCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSellBookCaption.Location = new System.Drawing.Point(1, 161);
            this.labelSellBookCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSellBookCaption.Name = "labelSellBookCaption";
            this.labelSellBookCaption.Size = new System.Drawing.Size(188, 18);
            this.labelSellBookCaption.TabIndex = 7;
            this.labelSellBookCaption.Text = "Книга продаж";
            // 
            // labelDiscrepanciesSellBookSumCaption
            // 
            appearance43.TextHAlignAsString = "Right";
            this.labelDiscrepanciesSellBookSumCaption.Appearance = appearance43;
            this.labelDiscrepanciesSellBookSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSellBookSumCaption.Location = new System.Drawing.Point(1, 181);
            this.labelDiscrepanciesSellBookSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSellBookSumCaption.Name = "labelDiscrepanciesSellBookSumCaption";
            this.labelDiscrepanciesSellBookSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesSellBookSumCaption.TabIndex = 8;
            this.labelDiscrepanciesSellBookSumCaption.Text = "Сумма расхождений, руб.:";
            // 
            // labelDiscrepanciesSellBookPVPSumCaption
            // 
            appearance44.TextHAlignAsString = "Right";
            this.labelDiscrepanciesSellBookPVPSumCaption.Appearance = appearance44;
            this.labelDiscrepanciesSellBookPVPSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSellBookPVPSumCaption.Location = new System.Drawing.Point(1, 201);
            this.labelDiscrepanciesSellBookPVPSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSellBookPVPSumCaption.Name = "labelDiscrepanciesSellBookPVPSumCaption";
            this.labelDiscrepanciesSellBookPVPSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesSellBookPVPSumCaption.TabIndex = 9;
            this.labelDiscrepanciesSellBookPVPSumCaption.Text = "Сумма ПВП, руб.:";
            // 
            // labelDiscrepanciesSellBookSumValue
            // 
            appearance45.TextHAlignAsString = "Left";
            this.labelDiscrepanciesSellBookSumValue.Appearance = appearance45;
            this.labelDiscrepanciesSellBookSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSellBookSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesSellBookSumValue.Location = new System.Drawing.Point(191, 181);
            this.labelDiscrepanciesSellBookSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSellBookSumValue.Name = "labelDiscrepanciesSellBookSumValue";
            this.labelDiscrepanciesSellBookSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesSellBookSumValue.TabIndex = 10;
            this.labelDiscrepanciesSellBookSumValue.Text = "0";
            // 
            // labelDiscrepanciesSellBookPVPSumValue
            // 
            appearance46.TextHAlignAsString = "Left";
            this.labelDiscrepanciesSellBookPVPSumValue.Appearance = appearance46;
            this.labelDiscrepanciesSellBookPVPSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesSellBookPVPSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesSellBookPVPSumValue.Location = new System.Drawing.Point(191, 201);
            this.labelDiscrepanciesSellBookPVPSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesSellBookPVPSumValue.Name = "labelDiscrepanciesSellBookPVPSumValue";
            this.labelDiscrepanciesSellBookPVPSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesSellBookPVPSumValue.TabIndex = 11;
            this.labelDiscrepanciesSellBookPVPSumValue.Text = "0";
            // 
            // labelDiscrepanciesPVPMinSumCaption
            // 
            appearance47.TextHAlignAsString = "Right";
            this.labelDiscrepanciesPVPMinSumCaption.Appearance = appearance47;
            this.labelDiscrepanciesPVPMinSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPMinSumCaption.Location = new System.Drawing.Point(291, 81);
            this.labelDiscrepanciesPVPMinSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPMinSumCaption.Name = "labelDiscrepanciesPVPMinSumCaption";
            this.labelDiscrepanciesPVPMinSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesPVPMinSumCaption.TabIndex = 0;
            this.labelDiscrepanciesPVPMinSumCaption.Text = "ПВП расхождения, руб.:";
            // 
            // labelDiscrepanciesPVPMinSumValue
            // 
            appearance48.TextHAlignAsString = "Left";
            this.labelDiscrepanciesPVPMinSumValue.Appearance = appearance48;
            this.labelDiscrepanciesPVPMinSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPMinSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesPVPMinSumValue.Location = new System.Drawing.Point(481, 81);
            this.labelDiscrepanciesPVPMinSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPMinSumValue.Name = "labelDiscrepanciesPVPMinSumValue";
            this.labelDiscrepanciesPVPMinSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesPVPMinSumValue.TabIndex = 0;
            this.labelDiscrepanciesPVPMinSumValue.Text = "0";
            // 
            // labelDiscrepanciesMinSumCaption
            // 
            appearance49.TextHAlignAsString = "Right";
            this.labelDiscrepanciesMinSumCaption.Appearance = appearance49;
            this.labelDiscrepanciesMinSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesMinSumCaption.Location = new System.Drawing.Point(291, 61);
            this.labelDiscrepanciesMinSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesMinSumCaption.Name = "labelDiscrepanciesMinSumCaption";
            this.labelDiscrepanciesMinSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesMinSumCaption.TabIndex = 0;
            this.labelDiscrepanciesMinSumCaption.Text = "Расхождения, руб.:";
            // 
            // labelDiscrepanciesMinSumValue
            // 
            appearance50.TextHAlignAsString = "Left";
            this.labelDiscrepanciesMinSumValue.Appearance = appearance50;
            this.labelDiscrepanciesMinSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesMinSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesMinSumValue.Location = new System.Drawing.Point(481, 61);
            this.labelDiscrepanciesMinSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesMinSumValue.Name = "labelDiscrepanciesMinSumValue";
            this.labelDiscrepanciesMinSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesMinSumValue.TabIndex = 0;
            this.labelDiscrepanciesMinSumValue.Text = "0";
            // 
            // labelMinSumCaption
            // 
            this.labelMinSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMinSumCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMinSumCaption.Location = new System.Drawing.Point(291, 41);
            this.labelMinSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelMinSumCaption.Name = "labelMinSumCaption";
            this.labelMinSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelMinSumCaption.TabIndex = 14;
            this.labelMinSumCaption.Text = "Минимальная сумма";
            // 
            // labelMaxSumCaption
            // 
            this.labelMaxSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxSumCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxSumCaption.Location = new System.Drawing.Point(291, 101);
            this.labelMaxSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelMaxSumCaption.Name = "labelMaxSumCaption";
            this.labelMaxSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelMaxSumCaption.TabIndex = 15;
            this.labelMaxSumCaption.Text = "Максимальная сумма";
            // 
            // labelDiscrepanciesMaxSumCaption
            // 
            appearance51.TextHAlignAsString = "Right";
            this.labelDiscrepanciesMaxSumCaption.Appearance = appearance51;
            this.labelDiscrepanciesMaxSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesMaxSumCaption.Location = new System.Drawing.Point(291, 121);
            this.labelDiscrepanciesMaxSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesMaxSumCaption.Name = "labelDiscrepanciesMaxSumCaption";
            this.labelDiscrepanciesMaxSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesMaxSumCaption.TabIndex = 16;
            this.labelDiscrepanciesMaxSumCaption.Text = "Расхождения, руб.:";
            // 
            // labelDiscrepanciesPVPMaxSumCaption
            // 
            appearance52.TextHAlignAsString = "Right";
            this.labelDiscrepanciesPVPMaxSumCaption.Appearance = appearance52;
            this.labelDiscrepanciesPVPMaxSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPMaxSumCaption.Location = new System.Drawing.Point(291, 141);
            this.labelDiscrepanciesPVPMaxSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPMaxSumCaption.Name = "labelDiscrepanciesPVPMaxSumCaption";
            this.labelDiscrepanciesPVPMaxSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesPVPMaxSumCaption.TabIndex = 17;
            this.labelDiscrepanciesPVPMaxSumCaption.Text = "ПВП расхождения, руб.:";
            // 
            // labelAvgSumCaption
            // 
            this.labelAvgSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAvgSumCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAvgSumCaption.Location = new System.Drawing.Point(291, 161);
            this.labelAvgSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelAvgSumCaption.Name = "labelAvgSumCaption";
            this.labelAvgSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelAvgSumCaption.TabIndex = 18;
            this.labelAvgSumCaption.Text = "Средняя сумма";
            // 
            // labelDiscrepanciesAvgSumCaption
            // 
            appearance53.TextHAlignAsString = "Right";
            this.labelDiscrepanciesAvgSumCaption.Appearance = appearance53;
            this.labelDiscrepanciesAvgSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesAvgSumCaption.Location = new System.Drawing.Point(291, 181);
            this.labelDiscrepanciesAvgSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesAvgSumCaption.Name = "labelDiscrepanciesAvgSumCaption";
            this.labelDiscrepanciesAvgSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesAvgSumCaption.TabIndex = 19;
            this.labelDiscrepanciesAvgSumCaption.Text = "Расхождения, руб.:";
            // 
            // labelDiscrepanciesPVPAvgSumCaption
            // 
            appearance54.TextHAlignAsString = "Right";
            this.labelDiscrepanciesPVPAvgSumCaption.Appearance = appearance54;
            this.labelDiscrepanciesPVPAvgSumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPAvgSumCaption.Location = new System.Drawing.Point(291, 201);
            this.labelDiscrepanciesPVPAvgSumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPAvgSumCaption.Name = "labelDiscrepanciesPVPAvgSumCaption";
            this.labelDiscrepanciesPVPAvgSumCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesPVPAvgSumCaption.TabIndex = 20;
            this.labelDiscrepanciesPVPAvgSumCaption.Text = "ПВП расхождения, руб.:";
            // 
            // labelDiscrepanciesMaxSumValue
            // 
            appearance55.TextHAlignAsString = "Left";
            this.labelDiscrepanciesMaxSumValue.Appearance = appearance55;
            this.labelDiscrepanciesMaxSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesMaxSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesMaxSumValue.Location = new System.Drawing.Point(481, 121);
            this.labelDiscrepanciesMaxSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesMaxSumValue.Name = "labelDiscrepanciesMaxSumValue";
            this.labelDiscrepanciesMaxSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesMaxSumValue.TabIndex = 21;
            this.labelDiscrepanciesMaxSumValue.Text = "0";
            // 
            // labelDiscrepanciesPVPMaxSumValue
            // 
            appearance56.TextHAlignAsString = "Left";
            this.labelDiscrepanciesPVPMaxSumValue.Appearance = appearance56;
            this.labelDiscrepanciesPVPMaxSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPMaxSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesPVPMaxSumValue.Location = new System.Drawing.Point(481, 141);
            this.labelDiscrepanciesPVPMaxSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPMaxSumValue.Name = "labelDiscrepanciesPVPMaxSumValue";
            this.labelDiscrepanciesPVPMaxSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesPVPMaxSumValue.TabIndex = 22;
            this.labelDiscrepanciesPVPMaxSumValue.Text = "0";
            // 
            // labelDiscrepanciesAvgSumValue
            // 
            appearance57.TextHAlignAsString = "Left";
            this.labelDiscrepanciesAvgSumValue.Appearance = appearance57;
            this.labelDiscrepanciesAvgSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesAvgSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesAvgSumValue.Location = new System.Drawing.Point(481, 181);
            this.labelDiscrepanciesAvgSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesAvgSumValue.Name = "labelDiscrepanciesAvgSumValue";
            this.labelDiscrepanciesAvgSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesAvgSumValue.TabIndex = 23;
            this.labelDiscrepanciesAvgSumValue.Text = "0";
            // 
            // labelDiscrepanciesPVPAvgSumValue
            // 
            appearance58.TextHAlignAsString = "Left";
            this.labelDiscrepanciesPVPAvgSumValue.Appearance = appearance58;
            this.labelDiscrepanciesPVPAvgSumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepanciesPVPAvgSumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesPVPAvgSumValue.Location = new System.Drawing.Point(481, 201);
            this.labelDiscrepanciesPVPAvgSumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesPVPAvgSumValue.Name = "labelDiscrepanciesPVPAvgSumValue";
            this.labelDiscrepanciesPVPAvgSumValue.Size = new System.Drawing.Size(98, 18);
            this.labelDiscrepanciesPVPAvgSumValue.TabIndex = 24;
            this.labelDiscrepanciesPVPAvgSumValue.Text = "0";
            // 
            // labelDiscrepanciesKSCountCaption
            // 
            this.labelDiscrepanciesKSCountCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesKSCountCaption.Location = new System.Drawing.Point(581, 21);
            this.labelDiscrepanciesKSCountCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesKSCountCaption.Name = "labelDiscrepanciesKSCountCaption";
            this.labelDiscrepanciesKSCountCaption.Size = new System.Drawing.Size(188, 18);
            this.labelDiscrepanciesKSCountCaption.TabIndex = 12;
            this.labelDiscrepanciesKSCountCaption.Text = "Количество расхождений по КС:";
            // 
            // labelDiscrepanciesKSCountValue
            // 
            this.labelDiscrepanciesKSCountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiscrepanciesKSCountValue.Location = new System.Drawing.Point(771, 21);
            this.labelDiscrepanciesKSCountValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepanciesKSCountValue.Name = "labelDiscrepanciesKSCountValue";
            this.labelDiscrepanciesKSCountValue.Size = new System.Drawing.Size(78, 18);
            this.labelDiscrepanciesKSCountValue.TabIndex = 13;
            this.labelDiscrepanciesKSCountValue.Text = "0";
            // 
            // grpHeader
            // 
            this.grpHeader.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.grpHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpHeader.ExpandedSize = new System.Drawing.Size(1156, 93);
            this.grpHeader.Location = new System.Drawing.Point(0, 0);
            this.grpHeader.Margin = new System.Windows.Forms.Padding(0);
            this.grpHeader.Name = "grpHeader";
            this.grpHeader.Size = new System.Drawing.Size(1156, 93);
            this.grpHeader.TabIndex = 7;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.commonInfoPanel);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1150, 73);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // commonInfoPanel
            // 
            // 
            // commonInfoPanel.ClientArea
            // 
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelAnnulmentFlag);
            this.commonInfoPanel.ClientArea.Controls.Add(this._surIndicator);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelInspectorValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelPeriodValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelInspectorCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelPeriodCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelDateTakeValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelDateTakeCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelSignValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.comboCorrectionNumValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelSignCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelCorrectionNumCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelCorrectionStatus);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelRegNumValue);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelRegNumCaption);
            this.commonInfoPanel.ClientArea.Controls.Add(this.labelNameValue);
            this.commonInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commonInfoPanel.Location = new System.Drawing.Point(0, 0);
            this.commonInfoPanel.Margin = new System.Windows.Forms.Padding(2);
            this.commonInfoPanel.Name = "commonInfoPanel";
            this.commonInfoPanel.Size = new System.Drawing.Size(1150, 73);
            this.commonInfoPanel.TabIndex = 6;
            // 
            // labelAnnulmentFlag
            // 
            appearance26.ForeColor = System.Drawing.Color.Red;
            this.labelAnnulmentFlag.Appearance = appearance26;
            this.labelAnnulmentFlag.AutoSize = true;
            this.labelAnnulmentFlag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelAnnulmentFlag.Location = new System.Drawing.Point(745, 49);
            this.labelAnnulmentFlag.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelAnnulmentFlag.Name = "labelAnnulmentFlag";
            this.labelAnnulmentFlag.Size = new System.Drawing.Size(83, 14);
            this.labelAnnulmentFlag.TabIndex = 33;
            this.labelAnnulmentFlag.Text = "Аннулирована";
            this.labelAnnulmentFlag.Visible = false;
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(4, 4);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 32;
            // 
            // labelInspectorValue
            // 
            this.labelInspectorValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelInspectorValue.Location = new System.Drawing.Point(89, 51);
            this.labelInspectorValue.Name = "labelInspectorValue";
            this.labelInspectorValue.Size = new System.Drawing.Size(201, 21);
            this.labelInspectorValue.TabIndex = 28;
            this.labelInspectorValue.Text = "Кельманзон Анна Климентьевна";
            // 
            // labelPeriodValue
            // 
            this.labelPeriodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPeriodValue.Location = new System.Drawing.Point(399, 27);
            this.labelPeriodValue.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelPeriodValue.Name = "labelPeriodValue";
            this.labelPeriodValue.Size = new System.Drawing.Size(89, 18);
            this.labelPeriodValue.TabIndex = 22;
            this.labelPeriodValue.Text = "сентябрь 2015";
            // 
            // labelInspectorCaption
            // 
            appearance27.TextHAlignAsString = "Right";
            this.labelInspectorCaption.Appearance = appearance27;
            this.labelInspectorCaption.Location = new System.Drawing.Point(23, 51);
            this.labelInspectorCaption.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelInspectorCaption.Name = "labelInspectorCaption";
            this.labelInspectorCaption.Size = new System.Drawing.Size(64, 21);
            this.labelInspectorCaption.TabIndex = 27;
            this.labelInspectorCaption.Text = "Инспектор:";
            // 
            // labelPeriodCaption
            // 
            appearance28.TextHAlignAsString = "Right";
            this.labelPeriodCaption.Appearance = appearance28;
            this.labelPeriodCaption.Location = new System.Drawing.Point(296, 27);
            this.labelPeriodCaption.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelPeriodCaption.Name = "labelPeriodCaption";
            this.labelPeriodCaption.Size = new System.Drawing.Size(101, 21);
            this.labelPeriodCaption.TabIndex = 21;
            this.labelPeriodCaption.Text = "Отчетный период:";
            // 
            // labelDateTakeValue
            // 
            this.labelDateTakeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelDateTakeValue.Location = new System.Drawing.Point(413, 51);
            this.labelDateTakeValue.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelDateTakeValue.Name = "labelDateTakeValue";
            this.labelDateTakeValue.Size = new System.Drawing.Size(66, 21);
            this.labelDateTakeValue.TabIndex = 31;
            this.labelDateTakeValue.Text = "20.10.2014";
            // 
            // labelDateTakeCaption
            // 
            appearance29.TextHAlignAsString = "Right";
            this.labelDateTakeCaption.Appearance = appearance29;
            this.labelDateTakeCaption.Location = new System.Drawing.Point(285, 51);
            this.labelDateTakeCaption.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelDateTakeCaption.Name = "labelDateTakeCaption";
            this.labelDateTakeCaption.Size = new System.Drawing.Size(126, 21);
            this.labelDateTakeCaption.TabIndex = 30;
            this.labelDateTakeCaption.Text = "Дата представления:";
            // 
            // labelSignValue
            // 
            this.labelSignValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSignValue.Location = new System.Drawing.Point(970, 29);
            this.labelSignValue.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelSignValue.Name = "labelSignValue";
            this.labelSignValue.Size = new System.Drawing.Size(169, 21);
            this.labelSignValue.TabIndex = 24;
            this.labelSignValue.Text = "-10 000 000 000";
            this.labelSignValue.WrapText = false;
            // 
            // comboCorrectionNumValue
            // 
            this.comboCorrectionNumValue.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "ValueListItem0";
            valueListItem1.DisplayText = "3/1";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "3/2";
            valueListItem3.DataValue = "ValueListItem2";
            valueListItem3.DisplayText = "3/3";
            this.comboCorrectionNumValue.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.comboCorrectionNumValue.Location = new System.Drawing.Point(617, 25);
            this.comboCorrectionNumValue.Name = "comboCorrectionNumValue";
            this.comboCorrectionNumValue.Size = new System.Drawing.Size(120, 21);
            this.comboCorrectionNumValue.TabIndex = 26;
            this.comboCorrectionNumValue.ValueChanged += new System.EventHandler(this.comboCorrectionNumValue_ValueChanged);
            // 
            // labelSignCaption
            // 
            appearance30.TextHAlignAsString = "Right";
            this.labelSignCaption.Appearance = appearance30;
            this.labelSignCaption.Location = new System.Drawing.Point(850, 29);
            this.labelSignCaption.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelSignCaption.Name = "labelSignCaption";
            this.labelSignCaption.Size = new System.Drawing.Size(121, 21);
            this.labelSignCaption.TabIndex = 23;
            this.labelSignCaption.Text = "НДС К возмещению:";
            this.labelSignCaption.WrapText = false;
            // 
            // labelCorrectionNumCaption
            // 
            appearance31.TextHAlignAsString = "Right";
            this.labelCorrectionNumCaption.Appearance = appearance31;
            this.labelCorrectionNumCaption.Location = new System.Drawing.Point(512, 27);
            this.labelCorrectionNumCaption.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelCorrectionNumCaption.Name = "labelCorrectionNumCaption";
            this.labelCorrectionNumCaption.Size = new System.Drawing.Size(101, 21);
            this.labelCorrectionNumCaption.TabIndex = 25;
            this.labelCorrectionNumCaption.Text = "№ корректировки";
            // 
            // labelCorrectionStatus
            // 
            this.labelCorrectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelCorrectionStatus.Location = new System.Drawing.Point(745, 27);
            this.labelCorrectionStatus.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelCorrectionStatus.Name = "labelCorrectionStatus";
            this.labelCorrectionStatus.Size = new System.Drawing.Size(104, 21);
            this.labelCorrectionStatus.TabIndex = 29;
            this.labelCorrectionStatus.Text = "(неактуальная)";
            // 
            // labelRegNumValue
            // 
            this.labelRegNumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRegNumValue.Location = new System.Drawing.Point(135, 27);
            this.labelRegNumValue.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelRegNumValue.Name = "labelRegNumValue";
            this.labelRegNumValue.Size = new System.Drawing.Size(96, 21);
            this.labelRegNumValue.TabIndex = 20;
            this.labelRegNumValue.Text = "1234567890123";
            // 
            // labelRegNumCaption
            // 
            appearance32.TextHAlignAsString = "Right";
            this.labelRegNumCaption.Appearance = appearance32;
            this.labelRegNumCaption.Location = new System.Drawing.Point(21, 27);
            this.labelRegNumCaption.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelRegNumCaption.Name = "labelRegNumCaption";
            this.labelRegNumCaption.Size = new System.Drawing.Size(116, 21);
            this.labelRegNumCaption.TabIndex = 19;
            this.labelRegNumCaption.Text = "Регистрационный №:";
            // 
            // labelNameValue
            // 
            appearance34.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelNameValue.Appearance = appearance34;
            this.labelNameValue.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.labelNameValue.Location = new System.Drawing.Point(25, 4);
            this.labelNameValue.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelNameValue.Name = "labelNameValue";
            this.labelNameValue.Size = new System.Drawing.Size(986, 19);
            this.labelNameValue.TabIndex = 18;
            this.labelNameValue.TabStop = true;
            this.labelNameValue.Value = "<a href=\"#\">Китайская вечнозеленая аглоонема и нефритовое дерево</a>";
            this.labelNameValue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.labelInnValue_LinkClicked);
            // 
            // View
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.DeclarationParts);
            this.Controls.Add(this.grpCommonInfo);
            this.Controls.Add(this.grpHeader);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1156, 767);
            this.OnFormClosing += new System.EventHandler(this.View_OnFormClosing);
            this.tabMain.ResumeLayout(false);
            this.rnivcWrapperPanel.ResumeLayout(false);
            this.tabR8.ResumeLayout(false);
            this.R8ContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox_R8_Header)).EndInit();
            this.ultraExpandableGroupBox_R8_Header.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel3.ResumeLayout(false);
            this.chapter08InfoPanel.ClientArea.ResumeLayout(false);
            this.chapter08InfoPanel.ClientArea.PerformLayout();
            this.chapter08InfoPanel.ResumeLayout(false);
            this.tableLayoutPanelSummaryChapter8.ResumeLayout(false);
            this.panelChapter8_TitleActualData.ClientArea.ResumeLayout(false);
            this.panelChapter8_TitleActualData.ResumeLayout(false);
            this.panelChapter8_TitleActualData_01.ClientArea.ResumeLayout(false);
            this.panelChapter8_TitleActualData_01.ClientArea.PerformLayout();
            this.panelChapter8_TitleActualData_01.ResumeLayout(false);
            this.tabR9.ResumeLayout(false);
            this.R9ContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox_R9_Header)).EndInit();
            this.ultraExpandableGroupBox_R9_Header.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel4.ResumeLayout(false);
            this.chapter09InfoPanel.ClientArea.ResumeLayout(false);
            this.chapter09InfoPanel.ClientArea.PerformLayout();
            this.chapter09InfoPanel.ResumeLayout(false);
            this.tableLayoutPanelSummaryChapter9.ResumeLayout(false);
            this.panelChapter9_TitleActualData.ClientArea.ResumeLayout(false);
            this.panelChapter9_TitleActualData.ResumeLayout(false);
            this.panelChapter9_TitleActualData_01.ClientArea.ResumeLayout(false);
            this.panelChapter9_TitleActualData_01.ClientArea.PerformLayout();
            this.panelChapter9_TitleActualData_01.ResumeLayout(false);
            this.tabR10.ResumeLayout(false);
            this.R10ContextMenu.ResumeLayout(false);
            this.panelChapterInfo10.ClientArea.ResumeLayout(false);
            this.panelChapterInfo10.ResumeLayout(false);
            this.tabR11.ResumeLayout(false);
            this.R11ContextMenu.ResumeLayout(false);
            this.panelChapterInfo11.ClientArea.ResumeLayout(false);
            this.panelChapterInfo11.ResumeLayout(false);
            this.tabR12.ResumeLayout(false);
            this.R12ContextMenu.ResumeLayout(false);
            this.panelChapter12_TitleActualData.ClientArea.ResumeLayout(false);
            this.panelChapter12_TitleActualData.ClientArea.PerformLayout();
            this.panelChapter12_TitleActualData.ResumeLayout(false);
            this.tabControlRatio.ResumeLayout(false);
            this.tabDiscrepancies.ResumeLayout(false);
            this.discrepContextMenu.ResumeLayout(false);
            this.tabDOC.ResumeLayout(false);
            this.knpDocLayoutPanel.ResumeLayout(false);
            this.knpDocLayoutPanel.PerformLayout();
            this.tabAct.ResumeLayout(false);
            this.tabDecision.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeclarationParts)).EndInit();
            this.DeclarationParts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpCommonInfo)).EndInit();
            this.grpCommonInfo.ResumeLayout(false);
            this.grpCommonInfo.PerformLayout();
            this.grpCommonInfoPanel.ResumeLayout(false);
            this.grpCommonInfoPanel.PerformLayout();
            this.autosizePanel.ClientArea.ResumeLayout(false);
            this.autosizePanel.ClientArea.PerformLayout();
            this.autosizePanel.ResumeLayout(false);
            this.autosizePanel.PerformLayout();
            this.tableLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpHeader)).EndInit();
            this.grpHeader.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.commonInfoPanel.ClientArea.ResumeLayout(false);
            this.commonInfoPanel.ClientArea.PerformLayout();
            this.commonInfoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboCorrectionNumValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl DeclarationParts;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage tabShared;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR9;
        private Infragistics.Win.Misc.UltraExpandableGroupBox grpCommonInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel grpCommonInfoPanel;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabDiscrepancies;
        private Controls.Grid.V1.GridControl gridDiscrepancies;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabDOC;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR12;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabControlRatio;
        private Controls.Grid.V1.GridControl gridControlRatio;
        private Infragistics.Win.Misc.UltraLabel captionR10;
        private Infragistics.Win.Misc.UltraLabel captionR11;
        private Infragistics.Win.Misc.UltraPanel commonInfoPanel;
        private Infragistics.Win.Misc.UltraLabel labelInspectorValue;
        private Infragistics.Win.Misc.UltraLabel labelPeriodValue;
        private Infragistics.Win.Misc.UltraLabel labelInspectorCaption;
        private Infragistics.Win.Misc.UltraLabel labelPeriodCaption;
        private Infragistics.Win.Misc.UltraLabel labelDateTakeValue;
        private Infragistics.Win.Misc.UltraLabel labelDateTakeCaption;
        private Infragistics.Win.Misc.UltraLabel labelSignValue;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboCorrectionNumValue;
        private Infragistics.Win.Misc.UltraLabel labelSignCaption;
        private Infragistics.Win.Misc.UltraLabel labelCorrectionNumCaption;
        private Infragistics.Win.Misc.UltraLabel labelCorrectionStatus;
        private Infragistics.Win.Misc.UltraLabel labelRegNumValue;
        private Infragistics.Win.Misc.UltraLabel labelRegNumCaption;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelNameValue;
        private System.Windows.Forms.TableLayoutPanel tableLayout;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesCommonPVPSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesCommonSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesCommonSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesCommonPVPSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSFCountCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSFCountValue;
        private Infragistics.Win.Misc.UltraLabel labelCommonSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelBuyBookCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesBuyBookSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesBuyBookSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesBuyBookPVPSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesBuyBookPVPSumValue;
        private Infragistics.Win.Misc.UltraLabel labelSellBookCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSellBookSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSellBookSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPMinSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPMinSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesMinSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesMinSumValue;
        private Infragistics.Win.Misc.UltraLabel labelMinSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelMaxSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesMaxSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPMaxSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelAvgSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesAvgSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesMaxSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPMaxSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesAvgSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesKSCountCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesKSCountValue;
        private Infragistics.Win.Misc.UltraPanel chapter08InfoPanel;
        private Infragistics.Win.Misc.UltraPanel chapter09InfoPanel;
        private Infragistics.Win.Misc.UltraPanel rnivcWrapperPanel;
        private System.Windows.Forms.ProgressBar progressBar;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSellBookPVPSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesSellBookPVPSumValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPAvgSumCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepanciesPVPAvgSumValue;
        private Infragistics.Win.Misc.UltraPanel autosizePanel;
        private System.Windows.Forms.ContextMenuStrip discrepContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDiscrep;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTaxPayer;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDecl;
        private System.Windows.Forms.ContextMenuStrip R8ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiR8OpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiR8OpenDecl;
        private System.Windows.Forms.ContextMenuStrip R9ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiR9OpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiR9OpenDecl;
        private System.Windows.Forms.ContextMenuStrip R11ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiR11OpenSeller;
        private System.Windows.Forms.ToolStripMenuItem cmiR11OpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiR11OpenDecl;
        private System.Windows.Forms.ContextMenuStrip R10ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiR10OpenBuyer;
        private System.Windows.Forms.ToolStripMenuItem cmiR10OpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiR10OpenDecl;
        private System.Windows.Forms.ContextMenuStrip R12ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiR12OpenBuyer;
        private System.Windows.Forms.ToolStripMenuItem cmiR12OpenDecl;
        private Infragistics.Win.Misc.UltraExpandableGroupBox grpHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Controls.Sur.SurIcon _surIndicator;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ultraExpandableGroupBox_R8_Header;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel3;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ultraExpandableGroupBox_R9_Header;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSummaryChapter9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P070;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P060;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P270;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P050;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P040;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P250;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P030;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P020;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P240;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P230;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P260;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9P280;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSummaryChapter8;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR8_1P005;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR8P190;
        private Infragistics.Win.Misc.UltraLabel labelItogoBookBuyCaption;
        private Infragistics.Win.Misc.UltraLabel labelVsegoBookBuyCaption;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private System.Windows.Forms.SaveFileDialog saveFileDialogReport;
        private Infragistics.Win.Misc.UltraLabel ulRedWords8;
        private Infragistics.Win.Misc.UltraLabel ulRedWords9;
        private Infragistics.Win.Misc.UltraLabel ulRedWords10;
        private Infragistics.Win.Misc.UltraLabel ulRedWords11;
        private Infragistics.Win.Misc.UltraLabel ulRedWords12;
        private Infragistics.Win.Misc.UltraLabel labelVsegoByPrilogenieChapter8Caption;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR8_1P190;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P310;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P320;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P330;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P340;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P350;
        private Infragistics.Win.Misc.UltraLabel ultraLabelR9_1P360;
        private Infragistics.Win.Misc.UltraPanel panelChapter9_TitleActualData;
        private Infragistics.Win.Misc.UltraPanel panelChapter9_TitleActualData_01;
        private Infragistics.Win.Misc.UltraPanel panelChapter8_TitleActualData;
        private Infragistics.Win.Misc.UltraPanel panelChapter8_TitleActualData_01;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelChapter8_TitleActualData;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelChapter9_TitleActualData;
        private Infragistics.Win.Misc.UltraPanel panelChapterInfo10;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelChapter10_TitleActualData;
        private Infragistics.Win.Misc.UltraPanel panelChapterInfo11;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelChapter11_TitleActualData;
        private Infragistics.Win.Misc.UltraPanel panelChapter12_TitleActualData;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelChapter12_TitleActualData;
        private Controls.Grid.V2.DataGridView dataGridR8;
        private Controls.Grid.V2.DataGridView dataGridR9;
        private Controls.Grid.V2.DataGridView dataGridR10;
        private Controls.Grid.V2.DataGridView dataGridR11;
        private Controls.Grid.V2.DataGridView dataGridR12;
        private Infragistics.Win.Misc.UltraLabel ulNotLoadedInMS;
        private System.Windows.Forms.ToolStripMenuItem cmiR8ShowCustomNums;
        private System.Windows.Forms.ToolStripMenuItem cmiR9ShowCustomNums;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabAct;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabDecision;
        private KnpResultDocumentEdit.ActDiscrepancyEditView actDiscrepancyEditView;
        private KnpResultDocumentEdit.DecisionDiscrepancyEditView decisionDiscrepancyEditView;
        private System.Windows.Forms.TableLayoutPanel knpDocLayoutPanel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private KnpDocuments.KnpDocumentListView _knpDocumentGrid;
        private Infragistics.Win.Misc.UltraLabel labelAnnulmentFlag;
    }
}
