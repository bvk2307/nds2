﻿using Infragistics.Win.UltraWinCalcManager;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public partial class View
    {
        private void RefreshGridChapter8()
        {
            cmiR8OpenDecl.Enabled = _model.IsOpenDeclAccessible();

            //TODO: Необходимо найти альтернативный способ скрывать/показывать отдельные колонки в уже проинициализированном гриде. Так же необходимо учесть сохранение пользовательских настроек грида.
            //dataGridR8.ClearColumns();
            //dataGridR8.InitColumns(SetupGridChapter8());
            //_presenter.InitializeGridChapter8(dataGridR8, _pagerChapter8);
        }


        #region Пока используются старые колонки для выгрузки в эксель

        public List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> GetColumnsChapter8()
        {
            GridSetupHelper<Invoice> helper = new GridSetupHelper<Invoice>();

            ColumnDefinition c00 = helper.CreateColumnDefinition(o => o.IS_CHANGED, GetDrawPenAction());
            c00.RowSpan = 2;
            c00.AllowRowFiltering = false;
            c00.WidthColumnExcel = 5;

            var c00a = helper.CreateColumnDefinition(o => o.CONTRACTOR_DECL_IN_MC, d => { d.Caption = "К"; d.ToolTip = "Котрагент подал декларацию / журнал учета"; d.RowSpan = 2; d.WidthColumnExcel = 8; });
            var c00b = helper.CreateColumnDefinition(o => o.IS_MATCHING_ROW, d => { d.Caption = "С"; d.ToolTip = "Запись сопоставлена"; d.RowSpan = 2; d.WidthColumnExcel = 8; });

            ColumnDefinition c01 = helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => { d.RowSpan = 2; });
            ColumnDefinition c02 = helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => d.RowSpan = 2);

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ " };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WidthInvoice; d.ToolTip = "Номер счета-фактуры продавца"; }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WidthDate; d.ToolTip = "Дата счета-фактуры продавца"; }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => d.Width = WidthInvoice));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WidthDate));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => d.Width = WidthInvoice));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WidthDate));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => d.Width = WidthInvoice));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WidthDate));

            var c5 = helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE_STR, d => { d.Width = WidthInvoice + WidthDate + 25; d.RowSpan = 2; d.WidthColumnExcel = 20; });

            ColumnDefinition c60 = helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE, d => { d.Width = WidthDate; d.RowSpan = 2; });

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = "Сведения о продавце" };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_INN, d => d.Width = WidthInn));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_KPP, d => d.Width = WidthKpp));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_NAME, d =>
            {
                d.Width = WidthName;
                d.SortIndicator = false;
                d.AllowRowFiltering = false; d.WidthColumnExcel = 40;
            }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Сведения о посреднике" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_INN, d => d.Width = WidthInn));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_KPP, d => d.Width = WidthKpp));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_NAME, d =>
            {
                d.Width = WidthName;
                d.SortIndicator = false;
                d.AllowRowFiltering = false; d.WidthColumnExcel = 40;
            }));

            var c91 = helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM, d => { d.Width = WidthInvoice; d.RowSpan = 2; d.WidthColumnExcel = 50; });
            var c92 = helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.RowSpan = 2; });
            var c93 = helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d =>
            {
                d.Width = WidthMoney; d.RowSpan = 2; d.FormatInfo = formatPrices;
                d.Align = ColumnAlign.Right;
            });
            var c94 = helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.Width = WidthMoney; d.RowSpan = 2; d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; });
            var c95 = helper.CreateColumnDefinition(o => o.IsAdditionalSheet, d =>
            {
                d.Caption = "Доп.лист";
                d.ToolTip = "Сведения из дополнительного листа";
                d.RowSpan = 2;
            });

            return new List<Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnBase> { c00, c00a, c00b, c01, c02, 
                                          g1, g2, g3, g4, c5, 
                                          c60, 
                                          g7, g8, c91, c92, c93, c94, c95 };
        }

        # endregion

        private GridColumnSetup SetupGridChapter8()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InvoiceModel>(dataGridR8);

            var priznakIcons = new Dictionary<int?, Bitmap>
            {
                { INVOICE_IS_PROCESSED, Properties.Resources.invoice_confirmed_16 },
                { INVOICE_IS_CHANGED, Properties.Resources.invoice_modified_16 },
            };

            setup.Columns.Add(helper.CreateImageColumn(o => o.IS_CHANGED, this, priznakIcons, 4)
                .Configure(d =>
                {
                    d.Caption = String.Empty;
                    d.HeaderToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
                    d.DisableFilter = true; 
                    d.ToolTipViewer = _explainToolTipViewer;
                }));

            #region Флаг К и С

            if (_model.IsChaptersGridIsMatchingColumnVisible())
            {
                var dictionaryBooleanIsMatching = new DictionaryBoolean();
                var optionsBuilderIsMatching = new BooleanAutoCompleteOptionsBuilder(dictionaryBooleanIsMatching);

                setup.Columns.Add(helper.CreateBoolColumn(o => o.CONTRACTOR_DECL_IN_MC, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "К";
                    d.HeaderToolTip = "Котрагент подал декларацию / журнал учета";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.DisableEdit = true;
                    d.MinWidth = WIDTH_CONTRACTOR_DECL_IN_MC;
                    d.Width = WIDTH_CONTRACTOR_DECL_IN_MC;
                }));
                setup.Columns.Add(helper.CreateBoolColumn(o => o.IS_MATCHING_ROW, ColumnExpressionCreator.CreateColumnExpressionDefault(), dictionaryBooleanIsMatching, optionsBuilderIsMatching, 4).Configure(d =>
                {
                    d.Caption = "С";
                    d.HeaderToolTip = "Запись сопоставлена";
                    d.MinWidth = WIDTH_IS_MATCHING_ROW;
                    d.Width = WIDTH_IS_MATCHING_ROW;
                }));
            }

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.ORDINAL_NUMBER, 4)
                .Configure(d => { d.Caption = "№\n\r(стр. 005)"; d.HeaderToolTip = "Порядковый номер"; })
                .SetFilterOperatorForDigit());
            setup.Columns.Add(helper.CreateTextColumn(o => o.OPERATION_CODE, 4)
                .Configure(d => { d.Caption = "Код вида операции\n\r(стр. 010)"; d.HeaderToolTip = "Код вида операции"; })
               .SetFilterOperatorForOpearationCode());

            #region СФ

            var group = helper.CreateGroup("СФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 020)"; d.HeaderToolTip = "Номер счета-фактуры"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 030)"; d.HeaderToolTip = "Дата счета-фактуры"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region ИСФ

            group = helper.CreateGroup("ИСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 040)"; d.HeaderToolTip = "Номер исправления счета-фактуры продавца"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 050)"; d.HeaderToolTip = "Дата исправления счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region КСФ

            group = helper.CreateGroup("КСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 060)"; d.HeaderToolTip = "Номер корректировочного счета-фактуры продавца"; d.MinWidth = WidthInvoice; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 070)"; d.HeaderToolTip = "Дата корректировочного счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            #region ИКСФ

            group = helper.CreateGroup("ИКСФ");
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_NUM, 3)
                .Configure(d => { d.Caption = "№\n\r(стр. 080)"; d.HeaderToolTip = "Номер исправления корректировочного счета-фактуры продавца"; d.MinWidth = WidthInvoice; })
                .SetFilterOperatorForDigit());
            group.Columns.Add(helper.CreateTextColumn(o => o.CHANGE_CORRECTION_DATE, 3)
                .Configure(d => { d.Caption = "Дата\n\r(стр. 090)"; d.HeaderToolTip = "Дата исправления корректировочного счета-фактуры продавца"; d.MinWidth = WidthDate; })
                .SetFilterOperatorForDateTime());
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.RECEIPT_DOC_DATE_STR, 4)
                .Configure(d => 
                    { 
                        d.Caption = "Документ, подтверждающий уплату налога\n\r(стр. 100, 110)"; 
                        d.HeaderToolTip = "Номер и дата документа, подтверждающего уплату налога";
                        d.MinWidth = WidthInvoice + WidthDate + 25;
                    })
                    .SetFilterOperatorForPaymentDoc());

            setup.Columns.Add(helper.CreateTextColumn(o => o.BUY_ACCEPT_DATE, 4)
                .Configure(d =>
                {
                    d.Caption = "Дата принятия на учет\n\r(стр. 120)";
                    d.HeaderToolTip = "Дата принятия на учет товаров (работ, услуг), имущественных прав";
                    d.MinWidth = WidthDate;
                })
                .SetFilterOperatorForDateTime());

            #region Сведения о продавце

            group = helper.CreateGroup("Сведения о продавце");
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 130)"; d.HeaderToolTip = "ИНН продавца"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 130)"; d.HeaderToolTip = "КПП продавца"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SELLER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование продавца";
                    d.MinWidth = WidthName;
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            setup.Columns.Add(group);

            #endregion

            #region Сведения о посреднике

            group = helper.CreateGroup("Сведения о посреднике");
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_INN, 3)
                .Configure(d => { d.Caption = "ИНН\n\r(стр. 140)"; d.HeaderToolTip = "ИНН посредника(комиссионера, агента, экспедитора или застройщика)"; d.MinWidth = WidthInn; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_KPP, 3)
                .Configure(d => { d.Caption = "КПП\n\r(стр. 130)"; d.HeaderToolTip = "КПП продавца"; d.MinWidth = WidthKpp; }));
            group.Columns.Add(helper.CreateTextColumn(o => o.BROKER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование посредника(комиссионера, агента, экспедитора или застройщика)";
                    d.MinWidth = WidthName;
                    d.DisableFilter = true;
                    d.DisableSort = true;
                }));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.CUSTOMS_DECLARATION_NUM, 4, 200)
                .Configure(d =>
                {
                    d.Caption = "Регистрационный № ТД\n\r(стр. 150)";
                    d.HeaderToolTip = "Регистрационный номер таможенной декларации";
                    d.DisableFilter = true;
                    d.DisableSort = true;
                    d.MinWidth = WidthMoney;
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.OKV_CODE, 4)
                .Configure(d =>
                {
                    d.Caption = "Код валюты\n\r(стр. 160)";
                    d.HeaderToolTip = "Код валюты по ОКВ";
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_BUY_AMOUNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Стоимость покупок с НДС\n\r(стр. 170)";
                    d.HeaderToolTip = "Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            setup.Columns.Add(helper.CreateTextColumn(o => o.PRICE_BUY_NDS_AMOUNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Сумма НДС\n\r(стр. 180)";
                    d.HeaderToolTip = "Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре принимаемая к вычету в рублях и копейках";
                    d.MinWidth = WidthMoney;
                    d.DataFormatInfo = formatPrices;
                    d.CellTextAlign = HorizontalAlign.Right;
                })
                .SetFilterOperatorForDigit());

            var dictionaryBoolean = new DictionaryBoolean();
            var optionsBuilder = new BooleanAutoCompleteOptionsBuilder(dictionaryBoolean);

            setup.Columns.Add(helper.CreateBoolColumn(o => o.IsAdditionalSheet, 
                ColumnExpressionCreator.CreateColumnExpressionIsAdditionalSheet(
                DeclarationModelDataExtractorCreator.CreateChapterDataExtractor(_model, DeclarationInvoiceChapterNumber.Chapter8)),
                dictionaryBoolean, optionsBuilder, 4)
                .Configure(d =>
                {
                    d.Caption = "Доп.лист";
                    d.HeaderToolTip = "Сведения из дополнительного листа";
                    d.MinWidth = 100;
                    d.Width = 100;
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.ROW_KEY, 4)
                .Configure(d =>
                {
                    d.Caption = "ROW_KEY";
                    d.Hidden = true;
                    d.AllowToggleVisibility = false;
                }));

            return setup;
        }
       
        private void InitializeGridChapter8()
        {
            dataGridR8
                 .WithPager(new ExtendedDataGridPageNavigator(_pagerChapter8))
                 .InitColumns(SetupGridChapter8());
            dataGridR8.Grid.CalcManager = new UltraCalcManager(dataGridR8.Grid.Container); ;
            dataGridR8.RegisterTableAddin(new ExpansionIndicatorRemover());
            dataGridR8.SelectRow += (sender, args) => _presenter.RefreshEKP();
            dataGridR8.AfterDataSourceChanged += dataGridR8_AfterDataSourceChanged;
            SetupSummaryRow(dataGridR8);
            _presenter.InitializeGridChapter8(dataGridR8, _pagerChapter8);
        }

        void dataGridR8_AfterDataSourceChanged()
        {
            if (dataGridR8.Grid.DisplayLayout.UIElement.GetDescendant(typeof(RowScrollbarUIElement)) != null)
            {
                dataGridR8.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Default;
            }
            else
            {
                dataGridR8.Grid.DisplayLayout.Bands[0].Override.SummaryDisplayArea = SummaryDisplayAreas.Bottom;
            }          
        }

        private readonly PagerStateViewModel _pagerChapter8 = new PagerStateViewModel();

        private readonly List<ToolStripMenuItem> _temporaryR8MenuItems = new List<ToolStripMenuItem>();

        private void R8ContextMenu_Opened(object sender, EventArgs e)
        {
            _temporaryR8MenuItems.ForEach(x => R8ContextMenu.Items.Remove(x));
            _temporaryR8MenuItems.Clear();

            var currentItem = dataGridR8.GetCurrentItem<InvoiceModel>();

            if (currentItem == null)
                return;

            if (!string.IsNullOrEmpty(currentItem.SELLER_INN))
            {
                var innList = currentItem.SELLER_INN.Split(',');
                var kppList = currentItem.SELLER_KPP.Split(',');
                var k = Math.Min(innList.Length, kppList.Length);
                for (var i = 0; i < k; ++i)
                {
                    var item = new ToolStripMenuItem
                    {
                        Name = string.Format("{0},{1}", innList[i], kppList[i]),
                        Size = new Size(242, 22),
                        Text = string.Format("Открыть карточку продавца (ИНН {0})", innList[i])
                    };
                    item.Click += ContextOpenSellerClick;
                    R8ContextMenu.Items.Insert(0, item);
                    _temporaryR8MenuItems.Add(item);
                }
            }

            cmiR8OpenBroker.Visible = !string.IsNullOrEmpty(currentItem.BROKER_INN) &&
                                      !string.IsNullOrEmpty(currentItem.BROKER_KPP);

            cmiR8ShowCustomNums.Visible = _model.AllowShowCustomDeclarationNumbersView;
            cmiR8ShowCustomNums.Enabled = currentItem.AllowCustomDeclarationNumbersView;
        }

        private void ContextOpenSellerClick(object sender, EventArgs e)
        {
            var cmi = sender as ToolStripMenuItem;
            if (cmi == null)
                return;
            var invoice = dataGridR8.GetCurrentItem<InvoiceModel>();
            if (invoice == null) // ДН никогда не говори никогда!
                return;
            var args = cmi.Name.Split(',');
            if (args.Length != 2)
                return;
            _presenter.ViewTaxPayerByKppOriginal(args[0], args[1], Direction.Purchase);
        }

        private void cmiR8OpenBroker_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR8.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppOriginal(
                    currentItem.BROKER_INN, 
                    currentItem.BROKER_KPP, 
                    Direction.Purchase);
        }

        private void cmiR8OpenDecl_Click(object sender, EventArgs e)
        {
            var currentItem = dataGridR8.GetCurrentItem<InvoiceModel>();

            if (currentItem != null)
                _presenter.ViewDeclarationDetailsFromInvoice(currentItem);
        }

        private void cmiR8ShowCustomNumsClick(object sender, EventArgs e)
        {
            var handler = CustomDeclarationNumbersRequested;
            if (handler != null)
            {
                var currentItem = dataGridR8.GetCurrentItem<InvoiceModel>();

                if (currentItem == null)
                    return;

                handler(currentItem);
            }
        }

        public event Action<InvoiceModel> CustomDeclarationNumbersRequested;

        public void ViewCustomDeclarationNumbers(string text)
        {
            TextViewer.Show(ParentForm, ResourceManagerNDS2.InvoiceMessages.CustomDeclarationNumbersDialogTitle, text);
        }
    }
}
