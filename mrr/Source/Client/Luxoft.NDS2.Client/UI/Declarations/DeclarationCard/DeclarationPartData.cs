﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public class DeclarationPartData
    {
        public long TotalRowNumbers
        {
            get;
            set;
        }

        public object Result
        {
            get;
            set;
        }

        public bool IsInitialized
        {
            get;
            set;
        }

        public QueryConditions qc
        {
            get;
            set;
        }
    }
}
