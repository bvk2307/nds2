﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {

        private void InitializeGridControlRatio()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridControlRatio", GetType()));
            setup.GetData = this.GetDataControlRatio;
            var helper = new GridSetupHelper<ControlRatio>();
            Action<ColumnDefinition> disable = column => { column.Visibility = ColumnVisibility.Disabled; column.RowSpan = 2; };
            Action<ColumnDefinition> multiline = column => { column.CellMultiLine = true; column.RowSpan = 2; };
            Action<ColumnDefinition> multiline1 = column => { column.CellMultiLine = true; column.RowSpan = 1; };

            setup.Columns.AddRange(new ColumnBase[]
            {
                helper.CreateHyperLinkColumn(
                    cr => cr.Id,
                    column => { column.Width = 75; column.RowSpan = 2; },
                    (sender, args) =>
                        _presenter.ViewControlRatioDetails((ControlRatio) ((ClickCellEventArgs) args).Cell.Row.ListObject)),
                helper.CreateColumnDefinition(cr => cr.TypeCode, column => { column.Width = 75; column.RowSpan = 2; }),
                helper.CreateColumnDefinition(cr => cr.HasDiscrepancyString, d => d.RowSpan = 2),
                helper.CreateColumnDefinition(cr => cr.TypeFormulation, multiline),
                helper.CreateColumnDefinition(cr => cr.TypeCalculationCondition, d => { d.CellMultiLine = true; d.RowSpan = 2; }),

                new ColumnGroupDefinition
                {
                    Caption = "Рассчитанная часть, руб.",
                    Columns =
                    {
                        helper.CreateColumnDefinition(cr => cr.DisparityLeft, multiline1),
                        helper.CreateColumnDefinition(cr => cr.DisparityRight, multiline1)
                    }
                },

                helper.CreateColumnDefinition(cr => cr.HasDiscrepancy, disable),
                helper.CreateColumnDefinition(cr => cr.UserComment, disable),
                helper.CreateColumnDefinition(cr => cr.Decl_Version_Id, disable),
                helper.CreateColumnDefinition(cr => cr.Type, disable)
            });

            setup.RowActions = new List<Action<UltraGridRow>>
            {
                row =>
                {
                    var controlRatio = row.ListObject as ControlRatio;
                    if (controlRatio != null && controlRatio.HasDiscrepancy)
                    {
                        row.Appearance.BackColor = Color.LightPink;
                    }
                    else
                    {
                        row.Appearance.ResetBackColor();
                    }
                }
            };

            setup.OrderColumns();
            setup.TypeStyleCell = TipStyle.Hide;

            gridControlRatio.AllowSaveFilterSettings = false;
            gridControlRatio.Setup = setup;
            gridControlRatio.RowDoubleClicked = obj => _presenter.ViewControlRatioDetails((ControlRatio)obj);
            gridControlRatio.ResetShowSelectedItem();
        }

        private object GetDataControlRatio(QueryConditions conditions, out long totalRowsNumber)
        {
            return _presenter.GetData(DeclarationPart.KC, conditions, out totalRowsNumber);
        }
    }
}
