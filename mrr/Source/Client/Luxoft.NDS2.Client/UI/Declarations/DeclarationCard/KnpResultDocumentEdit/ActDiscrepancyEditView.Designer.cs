﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    partial class ActDiscrepancyEditView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraExpandableGroupBoxPanel grpActDeskTaxAuditInfoPanel;
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel labelDocNumberCaption;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel labelDocDateFromCatption;
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            this._flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this._labelDocSum = new Infragistics.Win.Misc.UltraLabel();
            this._labelDocDateFromValue = new Infragistics.Win.Misc.UltraLabel();
            this._labelDocNumberValue = new Infragistics.Win.Misc.UltraLabel();
            this._grpActDeskTaxAuditInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            grpActDeskTaxAuditInfoPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            labelDocNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            labelDocDateFromCatption = new Infragistics.Win.Misc.UltraLabel();
            grpActDeskTaxAuditInfoPanel.SuspendLayout();
            this._flowLayoutPanel.SuspendLayout();
            tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._grpActDeskTaxAuditInfo)).BeginInit();
            this._grpActDeskTaxAuditInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // _actDiscrepancyGrid
            // 
            this._actDiscrepancyGrid.Location = new System.Drawing.Point(0, 21);
            this._actDiscrepancyGrid.Size = new System.Drawing.Size(1261, 403);
            // 
            // _saveButton
            // 
            this._saveButton.Text = "Сохранить данные по акту";
            // 
            // grpActDeskTaxAuditInfoPanel
            // 
            grpActDeskTaxAuditInfoPanel.Controls.Add(this._flowLayoutPanel);
            grpActDeskTaxAuditInfoPanel.Controls.Add(tableLayoutPanel);
            grpActDeskTaxAuditInfoPanel.Location = new System.Drawing.Point(-10000, -10000);
            grpActDeskTaxAuditInfoPanel.Name = "grpActDeskTaxAuditInfoPanel";
            grpActDeskTaxAuditInfoPanel.Size = new System.Drawing.Size(1255, 48);
            grpActDeskTaxAuditInfoPanel.TabIndex = 0;
            grpActDeskTaxAuditInfoPanel.Visible = false;
            // 
            // _flowLayoutPanel
            // 
            this._flowLayoutPanel.Controls.Add(ultraLabel1);
            this._flowLayoutPanel.Controls.Add(this._labelDocSum);
            this._flowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._flowLayoutPanel.Location = new System.Drawing.Point(0, 27);
            this._flowLayoutPanel.Name = "_flowLayoutPanel";
            this._flowLayoutPanel.Size = new System.Drawing.Size(1255, 21);
            this._flowLayoutPanel.TabIndex = 1;
            this._flowLayoutPanel.Visible = false;
            // 
            // ultraLabel1
            // 
            appearance4.TextVAlignAsString = "Top";
            ultraLabel1.Appearance = appearance4;
            ultraLabel1.AutoSize = true;
            ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel1.Location = new System.Drawing.Point(1, 1);
            ultraLabel1.Margin = new System.Windows.Forms.Padding(1, 1, 1, 6);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(190, 14);
            ultraLabel1.TabIndex = 1;
            ultraLabel1.Text = "Сумма расхождений по акту (руб.):";
            // 
            // _labelDocSum
            // 
            appearance2.TextVAlignAsString = "Top";
            this._labelDocSum.Appearance = appearance2;
            this._labelDocSum.AutoSize = true;
            this._labelDocSum.Dock = System.Windows.Forms.DockStyle.Fill;
            this._labelDocSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelDocSum.Location = new System.Drawing.Point(193, 1);
            this._labelDocSum.Margin = new System.Windows.Forms.Padding(1);
            this._labelDocSum.Name = "_labelDocSum";
            this._labelDocSum.Size = new System.Drawing.Size(56, 19);
            this._labelDocSum.TabIndex = 2;
            this._labelDocSum.Text = "12345678";
            // 
            // tableLayoutPanel
            // 
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.ColumnCount = 5;
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            tableLayoutPanel.Controls.Add(this._labelDocDateFromValue, 3, 1);
            tableLayoutPanel.Controls.Add(this._labelDocNumberValue, 1, 1);
            tableLayoutPanel.Controls.Add(labelDocNumberCaption, 0, 1);
            tableLayoutPanel.Controls.Add(labelDocDateFromCatption, 2, 1);
            tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            tableLayoutPanel.Name = "tableLayoutPanel";
            tableLayoutPanel.RowCount = 2;
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tableLayoutPanel.Size = new System.Drawing.Size(1255, 48);
            tableLayoutPanel.TabIndex = 0;
            // 
            // _labelDocDateFromValue
            // 
            appearance1.TextVAlignAsString = "Top";
            this._labelDocDateFromValue.Appearance = appearance1;
            this._labelDocDateFromValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this._labelDocDateFromValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelDocDateFromValue.Location = new System.Drawing.Point(154, 5);
            this._labelDocDateFromValue.Margin = new System.Windows.Forms.Padding(5, 1, 1, 1);
            this._labelDocDateFromValue.Name = "_labelDocDateFromValue";
            this._labelDocDateFromValue.Size = new System.Drawing.Size(74, 42);
            this._labelDocDateFromValue.TabIndex = 3;
            this._labelDocDateFromValue.Text = "20.10.2014";
            // 
            // _labelDocNumberValue
            // 
            appearance3.TextVAlignAsString = "Top";
            this._labelDocNumberValue.Appearance = appearance3;
            this._labelDocNumberValue.AutoSize = true;
            this._labelDocNumberValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this._labelDocNumberValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelDocNumberValue.Location = new System.Drawing.Point(74, 5);
            this._labelDocNumberValue.Margin = new System.Windows.Forms.Padding(1);
            this._labelDocNumberValue.Name = "_labelDocNumberValue";
            this._labelDocNumberValue.Size = new System.Drawing.Size(56, 42);
            this._labelDocNumberValue.TabIndex = 1;
            this._labelDocNumberValue.Text = "12345678";
            // 
            // labelDocNumberCaption
            // 
            appearance5.TextVAlignAsString = "Top";
            labelDocNumberCaption.Appearance = appearance5;
            labelDocNumberCaption.AutoSize = true;
            labelDocNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelDocNumberCaption.Location = new System.Drawing.Point(1, 5);
            labelDocNumberCaption.Margin = new System.Windows.Forms.Padding(1, 1, 1, 6);
            labelDocNumberCaption.Name = "labelDocNumberCaption";
            labelDocNumberCaption.Size = new System.Drawing.Size(71, 37);
            labelDocNumberCaption.TabIndex = 0;
            labelDocNumberCaption.Text = "Документ №    ";
            // 
            // labelDocDateFromCatption
            // 
            appearance26.TextHAlignAsString = "Right";
            appearance26.TextVAlignAsString = "Top";
            labelDocDateFromCatption.Appearance = appearance26;
            labelDocDateFromCatption.AutoSize = true;
            labelDocDateFromCatption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelDocDateFromCatption.Location = new System.Drawing.Point(132, 5);
            labelDocDateFromCatption.Margin = new System.Windows.Forms.Padding(1, 1, 1, 6);
            labelDocDateFromCatption.Name = "labelDocDateFromCatption";
            labelDocDateFromCatption.Size = new System.Drawing.Size(16, 37);
            labelDocDateFromCatption.TabIndex = 2;
            labelDocDateFromCatption.Text = "от";
            // 
            // _grpActDeskTaxAuditInfo
            // 
            this._grpActDeskTaxAuditInfo.Controls.Add(grpActDeskTaxAuditInfoPanel);
            this._grpActDeskTaxAuditInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this._grpActDeskTaxAuditInfo.Expanded = false;
            this._grpActDeskTaxAuditInfo.ExpandedSize = new System.Drawing.Size(1261, 70);
            this._grpActDeskTaxAuditInfo.Location = new System.Drawing.Point(0, 0);
            this._grpActDeskTaxAuditInfo.Name = "_grpActDeskTaxAuditInfo";
            this._grpActDeskTaxAuditInfo.Size = new System.Drawing.Size(1261, 21);
            this._grpActDeskTaxAuditInfo.TabIndex = 6;
            this._grpActDeskTaxAuditInfo.Text = "Акт налоговой проверки";
            // 
            // ActDiscrepancyEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grpActDeskTaxAuditInfo);
            this.Name = "ActDiscrepancyEditView";
            this.Controls.SetChildIndex(this._grpActDeskTaxAuditInfo, 0);
            this.Controls.SetChildIndex(this._actDiscrepancyGrid, 0);
            grpActDeskTaxAuditInfoPanel.ResumeLayout(false);
            grpActDeskTaxAuditInfoPanel.PerformLayout();
            this._flowLayoutPanel.ResumeLayout(false);
            this._flowLayoutPanel.PerformLayout();
            tableLayoutPanel.ResumeLayout(false);
            tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._grpActDeskTaxAuditInfo)).EndInit();
            this._grpActDeskTaxAuditInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox _grpActDeskTaxAuditInfo;
        private Infragistics.Win.Misc.UltraLabel _labelDocDateFromValue;
        private Infragistics.Win.Misc.UltraLabel _labelDocNumberValue;
        private System.Windows.Forms.FlowLayoutPanel _flowLayoutPanel;
        private Infragistics.Win.Misc.UltraLabel _labelDocSum;

    }
}
