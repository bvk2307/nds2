﻿using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using FLS.CommonComponents.Lib.Execution;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public class KnpResultDocumentEditPresenter<TActDiscrepanciesListItemModel>
        where TActDiscrepanciesListItemModel : KnpResultDiscrepancyModel
    {
        protected readonly IKnpResultDocumentEditView _view;
        private readonly IServiceRequestWrapper _serviceRequester;
        private readonly IEditDocumentDiscrepancyService _discrepancyService;
        private readonly IThreadInvoker _uiThreadExecutor;
        protected readonly KnpResultDocumentEditModel<TActDiscrepanciesListItemModel> _model;
        private KnpResultDocumentEditGridPresenter<TActDiscrepanciesListItemModel> _gridPresenter;
        private readonly IServiceCallErrorHandler _serviceErrorHandler;
        private readonly INotifier _serviceNotification;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ComparisonProcessValidator _comparasionProcessValidator;

        public event EventHandler ClosedDocument;

        public KnpResultDocumentEditPresenter(
            long knpResultDiscrepancyDocumentId, 
            IKnpResultDocumentEditView view, 
            IServiceRequestWrapper serviceRequester,
            IEditDocumentDiscrepancyService service,
            IThreadInvoker uiThreadExecutor,
            IServiceCallErrorHandler serviceErrorHandler,
            INotifier serviceNotification,
            ISettingsProvider settingsProvider,
            IDeclarationsDataService validatorService,
            long comparasionDataVersion)
        {
            _model = new KnpResultDocumentEditModel<TActDiscrepanciesListItemModel>(knpResultDiscrepancyDocumentId);
            _model.TotalSelectedActDiscrepanciesChanges += TotalSelectedActDiscrepanciesChanged;
            _model.TotalInvalidActDiscrepanciesChanges += TotalInvalidActDiscrepanciesChanged;
            _model.TotalAialibleActDiscrepanciesChanges += TotalAialibleActDiscrepanciesChanged;
            _model.TotalMatchesActDiscrepanciesChanges += TotalMatchesActDiscrepanciesChanged;
            _model.DiscrepancyAmountInvalid += DiscrepancyAmountStayInvalid;
            _model.DiscrepancyAmountValid += DiscrepancyAmountStayValid;
            _model.DiscrepancyAmountChange += DiscrepancyAmountChange;

            _serviceRequester = serviceRequester;
            _discrepancyService = service;

            _uiThreadExecutor = uiThreadExecutor;
            _serviceErrorHandler = serviceErrorHandler;
            _serviceNotification = serviceNotification;
            _settingsProvider = settingsProvider;

            _view = view;
            _view.SavingChanges += CloseDocument;
            _view.ActDiscrepancyExcluding += ActDiscrepancyExcludingUnchecked;
            _view.OnDataUpdated += OnDataUpdated;

            _comparasionProcessValidator = new ComparisonProcessValidator(
                validatorService,
                serviceRequester,
                comparasionDataVersion);

            InitGrid();
        }

        /// <summary>
        /// Выставление идентификатора документа (при переходе с реорганизованного на правоприемника)
        /// </summary>
        /// <param name="id">идентификатора документа</param>
        public void SetDocumentId(long id)
        {
            _model.Id = id;
        }

        /// <summary>
        /// Выставление номера версии сопоставления для валидации
        /// </summary>
        /// <param name="version">номер версии сопоставления</param>
        public void SetComparisionDataVersion(long version)
        {
            _comparasionProcessValidator.ComparasionDataVersion = version;

            _model.CurrentDataVersion = version;
        }

        /// <summary>
        /// Начальная инициализация
        /// </summary>
        public void Init(bool isFullAccess)
        {
            if (isFullAccess)
                SetFullAccess();
            else
                SetReadOnly();

            RefreshData();

            UpdateTotalInvalidRowCount();
        }

        /// <summary>
        /// Обновить данные формы
        /// </summary>
        public virtual void RefreshData()
        {
            _view.ExitAmountControlEdit();

            _gridPresenter.Load();
        }

        /// <summary>
        /// Активация контролла
        /// </summary>
        public virtual void Activate()
        {
            if (_model.CurrentDataVersion != _model.DataVersion)
            {
                _gridPresenter.ResetTotalsCache();

                RefreshData();

                UpdateTotalInvalidRowCount();

                _model.DataVersion = _model.CurrentDataVersion;
            }
        }

        public void SetFullAccess()
        {
            _model.IsReadOnly = false;

            _view.SetFullAccess();

            if (_model.TotalSelectedActDiscrepancies > 0)
                _view.AllowSavingChanges = true;
        }

        public void SetReadOnly()
        {
            _model.IsReadOnly = true;

            _view.SetReadOnly();

            _view.AllowSavingChanges = false;
        }

        private void InitGrid()
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> { new ColumnSort
                {
                    ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(t => t.Amount), 
                    Order = ColumnSort.SortOrder.Desc
                } }
            };

            // порядок важен чтобы грид не обновлялся много раз при создании

            // 1
            _view.InitActDiscrepancyList(_model.ActDiscrepancyListPager);

            // 2
            _gridPresenter =
                new KnpResultDocumentEditGridPresenter<TActDiscrepanciesListItemModel>(
                    _view.ActDiscrepancyList,
                    _model.ActDiscrepancyListPager,
                    SearchActDiscrepancies,
                    conditions);

            // 3
            _gridPresenter.Init(_settingsProvider);

        }

        public virtual void ResetTotalsCache()
        {
            _gridPresenter.ResetTotalsCache();
        }

        /// <summary>
        /// Изменение выбора отдельного расхождения
        /// </summary>
        protected virtual void ActDiscrepancyExcludingUnchecked(object sender, EventArgs args)
        {
            var eventArgs = args as GridItemEventArgs;

            if (eventArgs != null)
            {
                _view.Lock();

                var actDiscrepancy = (TActDiscrepanciesListItemModel)eventArgs.ChangedItem;

                _serviceRequester.ExecuteAsync(
                    (c) => _discrepancyService.Exclude(_model.Id, actDiscrepancy.Id),
                    (c, r) =>
                    {
                        actDiscrepancy.Included = false;

                        _model.Remove(actDiscrepancy);

                        _view.DeleteSelectedRows();

                        UpdateViewEditableAmountControls(_model);

                        UpdateViewSummaryData(_model);

                        _view.UnLock();
                    },
                    (c, r, e) =>
                    {
                        _serviceErrorHandler.ProcessingErrors(r, e);

                        _view.UnLock();
                    });
            }
        }

        private void DiscrepancyAmountChange(object sender, EventArgs args)
        {
            var actDiscrepancy = sender as TActDiscrepanciesListItemModel;
            var eventArgs = args as EditAmountEventArgs;

            if ((actDiscrepancy != null) && (eventArgs != null))
            {
                var validationResult = _comparasionProcessValidator.Validate();

                if (!validationResult.IsValid)
                {
                    _view.MessageView.ShowInfo(ResourceManagerNDS2.Attention, validationResult.Message);

                    actDiscrepancy.RollbackEditableAmount();

                    return;
                }
                
                _view.Lock();

                _serviceRequester.ExecuteAsync(
                    (c) => _discrepancyService.Update(actDiscrepancy.Id, eventArgs.Amount),
                    (c, r) =>
                    {
                        ApplyActDiscrepancyAmountChange(actDiscrepancy);
                            
                        _view.UnLock();
                    },
                    (c, r, e) =>
                    {
                        actDiscrepancy.RollbackEditableAmount();

                        _serviceErrorHandler.ProcessingErrors(r, e);

                        _view.UnLock();
                    });
            }
        }

        /// <summary>
        /// Применить изменения
        /// </summary>
        protected virtual void CloseDocument(object sender, EventArgs args)
        {
            var validationResult = _comparasionProcessValidator.Validate();
            if (!validationResult.IsValid)
            {
                _view.MessageView.ShowInfo(ResourceManagerNDS2.Attention, validationResult.Message);
                return;
            }

            _view.Lock();

            _serviceRequester.ExecuteAsync(
                (c) => _discrepancyService.CloseDocument(_model.Id),
                (c, r) =>
                {
                    _view.UnLock();

                    SetReadOnly();

                    if (ClosedDocument != null)
                        ClosedDocument(this, EventArgs.Empty);
                },
                (c, r, e) =>
                {
                    _serviceErrorHandler.ProcessingErrors(r, e);
                         
                    _view.UnLock();
                });
        }

        public void UpdateTotalInvalidRowCount()
        {
            _serviceRequester.ExecuteAsync(
                (c) => _discrepancyService.CountAllInvalid(_model.Id),
                (c, r) =>
                {
                    _model.TotalInvalidActDiscrepancies = r.Result;
                },
                (c, r, e) =>
                {
                    _serviceErrorHandler.ProcessingErrors(r, e);

                    _view.AllowSavingChanges = false;
                });
        }

        #region abstract methods

        private string abstractMethodExecutionError = "You must override this base method !";

        protected virtual TActDiscrepanciesListItemModel ActDiscrepanciesListItemFabricMethod(KnpResultDiscrepancy dto)
        {
            throw new NotImplementedException(abstractMethodExecutionError);
        }

        protected virtual string GetAmountEditFieldName()
        {
            throw new NotImplementedException(abstractMethodExecutionError);
        }

        protected virtual void CorrectSummaryData(decimal diffValue)
        {
            throw new NotImplementedException(abstractMethodExecutionError);
        }

        protected virtual string GetAmountEditErrorMessage()
        {
            throw new NotImplementedException(abstractMethodExecutionError);
        }

        #endregion

        private void DiscrepancyAmountStayValid(object sender, EventArgs args)
        {
            var actDiscrepancy = sender as TActDiscrepanciesListItemModel;
            var eventArgs = args as EditAmountEventArgs;

            if ((actDiscrepancy != null) && (eventArgs != null))
            {
                var obj = _view.GetAmountControlByEventObject(actDiscrepancy);

                if (obj != null)
                    _view.SetAmountControlError(obj, false);
            }
        }

        private void DiscrepancyAmountStayInvalid(object sender, EventArgs args)
        {
            var actDiscrepancy = sender as TActDiscrepanciesListItemModel;
            var eventArgs = args as EditAmountEventArgs;

            if ((actDiscrepancy != null) && (eventArgs != null))
            {
                var obj = _view.GetAmountControlByEventObject(actDiscrepancy);

                if (obj != null)
                    _view.SetAmountControlError(obj, true);

                var sumErrorMessage = ResourceManagerNDS2.KnpResultDocumentStrings.AmountZeroErrorMessageText;

                if (eventArgs.Amount > 0)
                {
                    sumErrorMessage = String.Concat(
                        eventArgs.Amount,
                        Environment.NewLine,
                        GetAmountEditErrorMessage());
                }

                _serviceNotification.ShowError(sumErrorMessage);
            }
        }

        private PageResult<TActDiscrepanciesListItemModel> SearchActDiscrepancies(QueryConditions conditions)
        {
            _uiThreadExecutor.BeginExecute((cancelToken) =>
            {
                _view.AllowSavingChanges = false;
            },
            DispatcherPriority.Normal);

            CorectQueryConditions(conditions);

            var ret = new PageResult<TActDiscrepanciesListItemModel>();

            var rez = _serviceRequester.Execute(
                () =>
                {
                    if (_model.IsReadOnly)
                        return _discrepancyService.Search(_model.Id, conditions);
                    else
                        return _discrepancyService.SearchUncommited(_model.Id, conditions);
                },
                (r) =>
                {
                    var modelList = r.Result.Discrepancies.Select(x => ActDiscrepanciesListItemFabricMethod(x)).ToList();

                    _model.LastQueryConditions = conditions;

                    if (!_model.LastQueryConditions.PaginationDetails.SkipTotalAvailable)
                    {
                        _model.TotalAialibleActDiscrepancies = r.Result.TotalSummary.Quantity;
                        _model.TotalSelectedActDiscrepancies = r.Result.TotalSummary.IncludedQuantity;
                        _model.SummaryDiscrepancyAmount = r.Result.TotalSummary.Amount;
                        _model.SummaryActAmount = r.Result.TotalSummary.ActAmount;
                        _model.SummaryDecisionAmount = r.Result.TotalSummary.DecisionAmount;
                    }

                    if (!_model.LastQueryConditions.PaginationDetails.SkipTotalMatches)
                    {
                        _model.TotalMatchesActDiscrepancies = r.Result.MatchesSummary.Quantity;
                    }

                    _model.ReloadActDiscrepancies(modelList);

                    ret = new PageResult<TActDiscrepanciesListItemModel>()
                    {
                        Rows = _model.ActDiscrepancies.ToList(),
                        TotalAvailable = _model.TotalAialibleActDiscrepancies,
                        TotalMatches = _model.TotalMatchesActDiscrepancies
                    };

                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        UpdateViewSummaryData(_model);
                    },
                    DispatcherPriority.Normal);
                },
                (r) =>
                {
                    _uiThreadExecutor.BeginExecute((cancelToken) =>
                    {
                        _serviceErrorHandler.ProcessingErrors(r);

                        _view.AllowSavingChanges = false;
                    },
                    DispatcherPriority.Normal);
                });

            return ret;
        }

        private void OnDataUpdated(object sender, EventArgs e)
        {
            UpdateViewEditableAmountControls(_model);
        }

        private void UpdateViewEditableAmountControls(KnpResultDocumentEditModel<TActDiscrepanciesListItemModel> model)
        {
            if (!_model.IsReadOnly)
            {
                bool existIncluded = model.ActDiscrepancies.Exists(x => x.Included);

                _view.SetAllAmountControlEditAccess(existIncluded);

                if (existIncluded)
                {
                    model.ActDiscrepancies.ForEach(x =>
                    {
                        var obj = _view.GetAmountControlByEventObject(x);

                        if (obj != null)
                        {
                            _view.SetAmountControlEditAccess(obj, x.Included);

                            _view.SetAmountControlError(obj, x.IsInvalid);
                        }
                    });
                }
            }
            else
                _view.SetAllAmountControlEditAccess(false);
        }

        private void ApplyActDiscrepancyAmountChange(TActDiscrepanciesListItemModel actDiscrepancy)
        {
            _model.ApplyActDiscrepancyAmountChange(actDiscrepancy);

            CorrectSummaryData(actDiscrepancy.EditableAmountDelta);

            UpdateViewSummaryData(_model);
        }

        protected virtual void UpdateViewSummaryData(KnpResultDocumentEditModel<TActDiscrepanciesListItemModel> model)
        {
            _view.SummaryDiscrepancyAmount = _model.SummaryDiscrepancyAmountView;
        }

        private void TotalAialibleActDiscrepanciesChanged(object sender, EventArgs e)
        {
            _uiThreadExecutor.BeginExecute((cancelToken) =>
            {
                _gridPresenter.SetTotalAvalaible((uint)_model.TotalAialibleActDiscrepancies);
            },
            DispatcherPriority.Normal);

            ViewSetSaveButtonState();
        }

        private void TotalMatchesActDiscrepanciesChanged(object sender, EventArgs e)
        {
            _uiThreadExecutor.BeginExecute((cancelToken) =>
            {
                _gridPresenter.SetTotalMatches((uint)_model.TotalMatchesActDiscrepancies);
            },
            DispatcherPriority.Normal);

            ViewSetSaveButtonState();
        }

        private void TotalInvalidActDiscrepanciesChanged(object sender, EventArgs eventArgs)
        {
            if (!_model.IsReadOnly)
            {
                _uiThreadExecutor.BeginExecute((cancelToken) =>
                {
                    _view.TotalInvalidRowCount = _model.TotalInvalidActDiscrepancies;
                },
                DispatcherPriority.Normal);
            }

            ViewSetSaveButtonState();
        }

        private void TotalSelectedActDiscrepanciesChanged(object sender, EventArgs eventArgs)
        {
            ViewSetSaveButtonState();
        }

        private void ViewSetSaveButtonState()
        {
            if (!_model.IsReadOnly)
            {
                _uiThreadExecutor.BeginExecute(
                    (cancelToken) => { _view.AllowSavingChanges = _model.AllowSaveChanges && AllowSaveChanges(); },
                    DispatcherPriority.Normal);
            }
        }

        protected virtual bool AllowSaveChanges() { return true; }

        private void CorectQueryConditions(QueryConditions conditions)
        {
            var columnSort = conditions.Sorting.FirstOrDefault(x => x.ColumnKey == GetAmountEditFieldName());
            
            if (columnSort != null)
            {
                if (columnSort.Order == ColumnSort.SortOrder.Asc)
                {
                    conditions.Sorting.Insert(0, new ColumnSort()
                    {
                        ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(x => x.Included),
                        Order = ColumnSort.SortOrder.Asc
                    });
                    conditions.Sorting.Insert(0, new ColumnSort()
                    {
                        ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(x => x.Status),
                        Order = ColumnSort.SortOrder.Desc
                    });
                }
                else if (columnSort.Order == ColumnSort.SortOrder.Desc)
                {
                    conditions.Sorting.Insert(0, new ColumnSort()
                    {
                        ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(x => x.Included),
                        Order = ColumnSort.SortOrder.Desc
                    });
                    conditions.Sorting.Insert(0, new ColumnSort()
                    {
                        ColumnKey = TypeHelper<TActDiscrepanciesListItemModel>.GetMemberName(x => x.Status),
                        Order = ColumnSort.SortOrder.Asc
                    });
                }
            }
        }
    }
}
