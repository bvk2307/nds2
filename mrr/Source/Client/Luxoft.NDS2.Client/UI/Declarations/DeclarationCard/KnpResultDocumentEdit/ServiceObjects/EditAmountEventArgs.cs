﻿using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects
{
    public class EditAmountEventArgs : EventArgs
    {
        public decimal Amount { get; private set; }

        public EditAmountEventArgs(decimal sum)
        {
            Amount = sum;
        }

        public static EditAmountEventArgs Create(decimal sum)
        {
            return new EditAmountEventArgs(sum);
        }
    }
}
