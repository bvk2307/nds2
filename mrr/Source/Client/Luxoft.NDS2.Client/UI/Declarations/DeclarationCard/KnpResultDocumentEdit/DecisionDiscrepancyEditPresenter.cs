﻿using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using System;
using System.Windows.Forms;
using FLS.CommonComponents.Lib.Execution;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public class DecisionDiscrepancyEditPresenter : KnpResultDocumentEditPresenter<DecisionDiscrepancyModel>
    {
        private IDecisionDiscrepancyEditView _decisionView;
        private DeclarationModelDecisionDataExtractor _decisionData;
        
        public DecisionDiscrepancyEditPresenter(
            long knpResultDiscrepancyDocumentId,
            DeclarationModelDecisionDataExtractor decisionData,
            IDecisionDiscrepancyEditView view,
            IServiceRequestWrapper serviceRequester,
            IEditDocumentDiscrepancyService service,
            IThreadInvoker uiThreadExecutor,
            IServiceCallErrorHandler serviceErrorHandler,
            INotifier serviceNotification,
            ISettingsProvider settingsProvider,
            IDeclarationsDataService validatorService,
            long comparasionDataVersion)
        : base(
            knpResultDiscrepancyDocumentId, 
            view, 
            serviceRequester, 
            service, 
            uiThreadExecutor, 
            serviceErrorHandler, 
            serviceNotification, 
            settingsProvider,
            validatorService,
            comparasionDataVersion)
        {
            _decisionView = view;
            _decisionData = decisionData;
        }

        protected override bool AllowSaveChanges()
        {
            return _decisionData.HasKnpDecision();
        }

        protected override void CloseDocument(object sender, EventArgs args)
        {
            if (_view.MessageView.ShowQuestion(
                    ResourceManagerNDS2.KnpResultDocumentStrings.SaveDecisionMsgCaption,
                    ResourceManagerNDS2.KnpResultDocumentStrings.SaveDecisionMsgText) == DialogResult.Yes)
                base.CloseDocument(sender, args);
        }

        protected override string GetAmountEditFieldName()
        {
            return TypeHelper<DecisionDiscrepancyModel>.GetMemberName(x => x.AmountByDecision);
        }

        protected override DecisionDiscrepancyModel ActDiscrepanciesListItemFabricMethod(KnpResultDiscrepancy dto)
        {
            return new DecisionDiscrepancyModel(dto);
        }

        protected override void CorrectSummaryData(decimal diffValue)
        {
            _model.SummaryDecisionAmount += diffValue;
        }

        protected override string GetAmountEditErrorMessage()
        {
            return ResourceManagerNDS2.KnpResultDocumentStrings.DecisionAmountEditErrorMessage;
        }

        protected override void UpdateViewSummaryData(KnpResultDocumentEditModel<DecisionDiscrepancyModel> _model)
        {
            base.UpdateViewSummaryData(_model);

            _decisionView.SummaryActAmount = _model.SummaryActAmountView;

            _decisionView.SummaryDecisionAmount = _model.SummaryDecisionAmountView;
        }

    }
}
