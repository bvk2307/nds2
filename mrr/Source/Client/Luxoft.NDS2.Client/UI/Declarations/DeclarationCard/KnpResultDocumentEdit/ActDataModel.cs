﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Discrepancy;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public class ActDataModel
    {
        public ActDataModel(string docNum, DateTime? docDate)
        {
            this.DocNum = docNum;
            this.DocDate = docDate;
        }

        /// <summary>
        /// Номер Акта (из ЭОД-4)
        /// </summary>
        public string DocNum { get; private set; }

        /// <summary>
        /// Дата акта (из ЭОД-4)
        /// </summary>
        public DateTime? DocDate { get; private set; }


        /// <summary>
        /// признак наличия содержательной информации
        /// </summary>
        public bool ValidInformation
        {
            get { return !String.IsNullOrEmpty(DocNum); }
        }
    }
}
