﻿using Infragistics.Win;
using Infragistics.Win.UltraWinCalcManager;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public partial class KnpResultDocumentEditView : UserControl, IKnpResultDocumentEditView
    {
        private ExtendedDataGridPageNavigator _pageNavigator;
        private bool _isLock = false;
        private SummarySettings _discrepancySummaryAmount;
        private bool _prevResetSettingsState;
        private bool _prevSaveButtonState;
        protected SingleColumn _sumColumn;

        public KnpResultDocumentEditView()
        {
            InitializeComponent();
        }

        #region Implementation of IKnpResultDocumentEditView

        public event EventHandler SavingChanges;

        public event EventHandler ResetingChanges;

        public event EventHandler OnDataUpdated;

        public event EventHandler ActDiscrepancyExcluding;

        public IDataGridView ActDiscrepancyList
        {
            get { return _actDiscrepancyGrid; }
        }

        public int TotalInvalidRowCount
        {
            set
            {
                _discrepancyInvalidCountLabel.Text = String.Format("Некорректно заполненных записей: {0}", value);
                _discrepancyInvalidCountLabel.Visible = value > 0;
            }
        }

        public bool AllowSavingChanges
        {
            set { _saveButton.Enabled = value; }
        }

        public string SummaryDiscrepancyAmount
        {
            set
            {
                if (_discrepancySummaryAmount != null)
                    _discrepancySummaryAmount.DisplayFormat = value;
            }
        }

        public void InitActDiscrepancyList(IPager pager)
        {
            _pageNavigator = new ExtendedDataGridPageNavigator(pager);

            GridColumnSetup setupColumns = SetupGridColumn(); 

            _actDiscrepancyGrid
                .WithPager(_pageNavigator)
                .InitColumns(setupColumns);

            setupColumns.Columns.ForEachSingle(x => x.DisableMoving = true);

            InitSummaryRow(_actDiscrepancyGrid);
            SetupSummaryRow(_actDiscrepancyGrid);

            _actDiscrepancyGrid.AfterDataSourceChanged += OnDataChage;
            _actDiscrepancyGrid.Grid.AfterEnterEditMode += OnEditCellEnter;
            _actDiscrepancyGrid.Grid.AfterExitEditMode += OnEditCellExit;
        }

        public void Lock()
        {
            _isLock = true;
            this.Enabled = false;
        }

        public void UnLock()
        {
            _isLock = false;
            this.Enabled = true;
        }

        public virtual void SetFullAccess()
        {
            SetAllAmountControlEditAccess(true);

            _actDiscrepancyGrid.GridContextMenuStrip = _contextMenuStrip;
        }

        public virtual void SetReadOnly()
        {
            _discrepancyInvalidCountLabel.Visible = false;

            SetAllAmountControlEditAccess(false);

            _buttonPanel.Visible = false;

            HideGridUserContentMenu();
        }

        public object GetAmountControlByEventObject(object eventObject)
        {
            object ret = null;

            UltraGridRow row = _actDiscrepancyGrid.Grid.Rows.FirstOrDefault(x => x.ListObject == eventObject);

            if ((row != null) && (row.Cells != null))
            {
                ret = row.Cells.Cast<UltraGridCell>().FirstOrDefault(item => item.Column.Key == GetEditAmountColumnName());
            }

            return ret;
        }
        
        public void SetAmountControlValue(object sumControl, string value)
        {
            var cell = (UltraGridCell) sumControl;

            cell.SetValue(value, false);

        }

        public void SetAllAmountControlError(bool isError)
        {
            if (isError)
                _actDiscrepancyGrid.Grid.DisplayLayout.Override.EditCellAppearance.BackColor = Color.LightPink;
            else
                _actDiscrepancyGrid.Grid.DisplayLayout.Override.EditCellAppearance.BackColor = 
                    _actDiscrepancyGrid.Grid.DisplayLayout.Override.CellAppearance.BackColor;
        }

        public void SetAmountControlError(object sumControl, bool isError)
        {
            var cell = (UltraGridCell)sumControl;

            if (isError)
                cell.Appearance.BackColor = Color.LightPink;
            else
                cell.Appearance.BackColor = _actDiscrepancyGrid.Grid.DisplayLayout.Override.CellAppearance.BackColor;
        }

        public void SetAmountControlEditAccess(object sumControl, bool isEnable)
        {
            var cell = (UltraGridCell)sumControl;

            cell.Activation = isEnable ? Activation.AllowEdit : Activation.NoEdit;
        }

        public virtual void SetAllAmountControlEditAccess(bool isEnable)
        {
            if (_sumColumn != null)
            {
                _sumColumn.ToggleEdit(!isEnable);
            }
        }

        public virtual void ExitAmountControlEdit()
        {
            _actDiscrepancyGrid.Grid.PerformAction(UltraGridAction.ExitEditMode);
        }

        public IMessageView MessageView { get; set; }

        public void DeleteSelectedRows()
        {
            _actDiscrepancyGrid.Grid.Selected.Rows.Cast<UltraGridRow>().All(x => x.Delete(false));
        }

        #endregion

        #region protected virtual

        protected virtual GridColumnSetup SetupGridColumn()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<KnpResultDiscrepancyModel>(_actDiscrepancyGrid);
            var helperOld = new GridSetupHelper<KnpResultDiscrepancyModel>();
            ColumnGroup group;

            setup.Columns.Add(helper.CreateTextColumn(o => o.Source, 2, 90).Configure(d =>
            {
                d.Caption = "Источник";
                d.HeaderToolTip = "Номер раздела в НД НП";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));

            group = helper.CreateGroup("Контрагент");
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorInn, 1, 80).Configure(d =>
            {
                d.Caption = "ИНН";
                d.HeaderToolTip = "ИНН контрагента";
                d.DisableMoving = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorKpp, 1, 65).Configure(d =>
            {
                d.Caption = "КПП";
                d.HeaderToolTip = "КПП контрагента";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorName, 1, 134).Configure(d =>
            {
                d.Caption = "Наименование";
                d.HeaderToolTip = "Наименование контрагента";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.ContractorReorganizedInn, 1, 134).Configure(d =>
            {
                d.Caption = "Реорганизованный НП";
                d.HeaderToolTip = "ИНН реорганизованного НП";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Счет-фактура");
            group.Columns.Add(helper.CreateTextColumn(o => o.InvoiceNumber, 1, 134).Configure(d =>
            {
                d.Caption = "Номер";
                d.HeaderToolTip = "Номер счета-фактуры";
                d.DisableMoving = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.InvoiceDate, 1, 70).Configure(d =>
            {
                d.Caption = "Дата";
                d.HeaderToolTip = "Дата счета-фактуры";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.InvoiceAmount, 1, 110).Configure(d =>
            {
                d.Caption = "Стоимость по СФ";
                d.HeaderToolTip = "Стоимость по счету-фактуре";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.InvoiceAmountNds, 1, 110).Configure(d =>
            {
                d.Caption = "Сумма НДС";
                d.HeaderToolTip = "Сумма НДС по счету-фактуре";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Расхождение");
            group.Columns.Add(helper.CreateTextColumn(o => o.Type, 1, 110).Configure(d =>
            {
                d.Caption = "Вид расхождения";
                d.HeaderToolTip = "Вид расхождения";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableSort = true;
            }));
            group.Columns.Add(helper.CreateNumberColumn(o => o.Amount, 1, 110).Configure(d =>
            {
                d.Caption = "Сумма";
                d.HeaderToolTip = "Сумма расхождения в руб.";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            group.Columns.Add(helper.CreateTextColumn(o => o.Status, 1, 60).Configure(d =>
            {
                d.Caption = "Статус";
                d.HeaderToolTip = "Статус  расхождения";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));
            setup.Columns.Add(group);

            return setup;
        }

        private void InitSummaryRow(Controls.Grid.V2.DataGridView grid)
        {
            grid.Grid.CalcManager = new UltraCalcManager(_actDiscrepancyGrid.Grid.Container);

            Color color = Color.FromArgb(198, 218, 243);

            var band = grid.Grid.DisplayLayout.Bands[0];

            band.SummaryFooterCaption = "Итого:";
            band.Override.BorderStyleSummaryFooterCaption = UIElementBorderStyle.None;
            band.Override.BorderStyleSummaryFooter = UIElementBorderStyle.None;
            band.Override.BorderStyleSummaryValue = UIElementBorderStyle.None;
            band.Override.SummaryFooterAppearance.BackColor = color;
            band.Override.SummaryFooterCaptionAppearance.BackColor = color;
            band.Override.SummaryValueAppearance.BackColor = color;
        }

        protected virtual void SetupSummaryRow(Controls.Grid.V2.DataGridView grid)
        {
            string amountColumnName = TypeHelper<KnpResultDiscrepancyModel>.GetMemberName(t => t.Amount);
            var band = grid.Grid.DisplayLayout.Bands[0];

            SummarySettings summary;
            summary = band.Summaries.Add(amountColumnName, String.Empty, SummaryPosition.UseSummaryPositionColumn, band.Columns[amountColumnName]);
            summary.DisplayFormat = " ";
            summary.Appearance.TextHAlign = HAlign.Right;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            _discrepancySummaryAmount = summary;
        }

        protected virtual void SaveBtnClick(object sender, EventArgs e)
        {
            if (SavingChanges != null)
                SavingChanges(this, EventArgs.Empty);
        }

        protected virtual string GetEditAmountColumnName()
        {
            throw new NotImplementedException();
        }

        protected EditAmountColumn CreateEditAmountColumn(int rowSpan, int width)
        {
            return new EditAmountColumn(GetEditAmountColumnName(), _actDiscrepancyGrid, ColumnExpressionCreator.CreateColumnExpressionDefault())
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
            };
        }

        #endregion

        private void OnDataChage()
        {
            if (OnDataUpdated != null)
                OnDataUpdated(this, EventArgs.Empty);
        }

        private void OnEditCellEnter(object sender, EventArgs e)
        {
            if (_actDiscrepancyGrid.Grid.ActiveCell.Column.Key != GetEditAmountColumnName())
                return;

            _pageNavigator.Enabled = false;

            _prevSaveButtonState = _saveButton.Enabled;
            _saveButton.Enabled = false;

            _prevResetSettingsState = _actDiscrepancyGrid.AllowResetSettings;
            _actDiscrepancyGrid.AllowResetSettings = false;
        }


        private void OnEditCellExit(object sender, EventArgs e)
        {
            if (_actDiscrepancyGrid.Grid.ActiveCell.Column.Key != GetEditAmountColumnName())
                return;

            _pageNavigator.Enabled = true;

            _saveButton.Enabled = _prevSaveButtonState;

            _actDiscrepancyGrid.AllowResetSettings = _prevResetSettingsState;
        }

        private void OnDeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (ActDiscrepancyExcluding != null)
                ActDiscrepancyExcluding(this, GridItemEventArgs.Create(_actDiscrepancyGrid.Grid.ActiveRow.ListObject));
        }

        private void HideGridUserContentMenu()
        {
            _actDiscrepancyGrid.GridContextMenuStrip = null;

            _deleteToolStripMenuItem.Visible = false;

            if (_actDiscrepancyGrid.Grid.ContextMenuStrip != null)
            {
                foreach (ToolStripItem item in _actDiscrepancyGrid.Grid.ContextMenuStrip.Items)
                {
                    if (item is ToolStripSeparator)
                        item.Visible = false;
                }
            }
        }

    }
}
