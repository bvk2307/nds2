﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public interface IActDiscrepancyEditView : IKnpResultDocumentEditView
    {
        void ShowActData(string num, string date);

        void ShowActAmount(bool isShow);

        string SummaryActAmount { set; }
    }
}
