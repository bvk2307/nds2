﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public interface IKnpResultDocumentEditView
    {
        #region Работа с принятием изменений

        bool AllowSavingChanges { set; }

        event EventHandler SavingChanges;

        #endregion

        #region Работа со списком расхождений

        event EventHandler ActDiscrepancyExcluding;

        event EventHandler OnDataUpdated;
        
        IDataGridView ActDiscrepancyList { get; }

        int TotalInvalidRowCount { set; }

        void InitActDiscrepancyList(IPager pager);

        string SummaryDiscrepancyAmount { set; }

        object GetAmountControlByEventObject(object eventObject);

        void SetAmountControlValue(object sumControl, string value);

        void SetAmountControlError(object sumControl, bool isError);

        void SetAmountControlEditAccess(object sumControl, bool isEnable);

        void SetAllAmountControlEditAccess(bool isEnable);

        void SetAllAmountControlError(bool isError);

        void ExitAmountControlEdit();

        void DeleteSelectedRows();


        #endregion

        #region Работа со всей View

        void Lock();

        void UnLock();

        void SetFullAccess();

        void SetReadOnly();

        IMessageView MessageView { get; set; }

        #endregion

    }
}
