﻿using System;
using FLS.CommonComponents.Lib.Execution;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public class ActDiscrepancyEditPresenter : KnpResultDocumentEditPresenter<ActDiscrepancyModel>
    {
        private DeclarationModelActDataExtractor _actData;

        private IActDiscrepancyEditView _actView;

        public ActDiscrepancyEditPresenter(
            long knpResultDiscrepancyDocumentId,
            DeclarationModelActDataExtractor actData,
            IActDiscrepancyEditView view,
            IServiceRequestWrapper serviceRequester,
            IEditDocumentDiscrepancyService service,
            IThreadInvoker uiThreadExecutor,
            IServiceCallErrorHandler serviceErrorHandler,
            INotifier serviceNotification,
            ISettingsProvider settingsProvider,
            IDeclarationsDataService validatorService,
            long comparasionDataVersion)
        : base(
            knpResultDiscrepancyDocumentId, 
            view, 
            serviceRequester, 
            service, 
            uiThreadExecutor, 
            serviceErrorHandler, 
            serviceNotification, 
            settingsProvider, 
            validatorService, 
            comparasionDataVersion)
        {
            _actView = view;
            _actData = actData;
        }

        public override void RefreshData()
        {
            base.RefreshData();

            if (_actData.IsExistEodData())
            {
                var actDate = _actData.ExtractDate();

                _actView.ShowActData(
                                _actData.ExtractNum(), 
                                actDate.HasValue ? actDate.Value.ToShortDateString() : String.Empty);

                _actView.ShowActAmount(_model.IsReadOnly && _actData.ExtractIsClosed());
            }
        }

        protected override string GetAmountEditFieldName()
        {
            return TypeHelper<ActDiscrepancyModel>.GetMemberName(x => x.AmountByAct);
        }

        protected override ActDiscrepancyModel ActDiscrepanciesListItemFabricMethod(KnpResultDiscrepancy dto)
        {
            return new ActDiscrepancyModel(dto);
        }

        protected override void CorrectSummaryData(decimal diffValue)
        {
            _model.SummaryActAmount += diffValue;
        }

        protected override string GetAmountEditErrorMessage()
        {
            return ResourceManagerNDS2.KnpResultDocumentStrings.ActAmountEditErrorMessage;
        }

        protected override void UpdateViewSummaryData(KnpResultDocumentEditModel<ActDiscrepancyModel> _model)
        {
            base.UpdateViewSummaryData(_model);

            _actView.SummaryActAmount = _model.SummaryActAmountView;

        }
    }
}
