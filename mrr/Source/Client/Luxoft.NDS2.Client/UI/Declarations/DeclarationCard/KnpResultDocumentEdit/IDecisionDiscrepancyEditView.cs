﻿
namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public interface IDecisionDiscrepancyEditView : IKnpResultDocumentEditView
    {
        string SummaryActAmount { set; }

        string SummaryDecisionAmount { set; }

    }
}
