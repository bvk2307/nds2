﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public class KnpResultDocumentEditGridPresenter<TRowData> : PagedDataGridPresenter<TRowData>
    {
        public KnpResultDocumentEditGridPresenter(
            IDataGridView gridView,
            IPager pager,
            Func<QueryConditions, PageResult<TRowData>> dataLoader,
            QueryConditions searchCriteria = null
            )
            : base(gridView, pager, dataLoader, searchCriteria)
        {
        }

        public override void SetTotalAvalaible(uint totalAvalable)
        {
            _pager.TotalDataRows = totalAvalable;
        }

        public virtual void SetTotalMatches(uint totalMatches)
        {
            _pager.TotalMatches = totalMatches;
        }
    }
}
