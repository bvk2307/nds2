﻿using System;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects
{
    public class GridItemEventArgs : EventArgs
    {
        public object ChangedItem { get; private set; }

        public GridItemEventArgs(object item)
        {
            ChangedItem = item;
        }

        public static GridItemEventArgs Create(object item)
        {
            return new GridItemEventArgs(item);
        }
    }
}
