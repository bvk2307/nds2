﻿using System;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public partial class DecisionDiscrepancyEditView : KnpResultDocumentEditView, IDecisionDiscrepancyEditView
    {
        private SummarySettings _actSummaryAmount;
        private SummarySettings _decisionSummaryAmount;

        public DecisionDiscrepancyEditView()
        {
            InitializeComponent();

            _deleteToolStripMenuItem.Text = ResourceManagerNDS2
                                                .KnpResultDocumentStrings
                                                    .DecisionDiscrepancyExcludeMenuText;
        }

        #region Implementation of IDecisionDiscrepancyEditView

        public string SummaryActAmount
        {
            set
            {
                if (_actSummaryAmount != null)
                    _actSummaryAmount.DisplayFormat = value;
            }
        }

        public string SummaryDecisionAmount
        {
            set
            {
                if (_decisionSummaryAmount != null)
                    _decisionSummaryAmount.DisplayFormat = value;
            }
        }

        #endregion

        protected override GridColumnSetup SetupGridColumn()
        {
            var setup = base.SetupGridColumn();
            var helper = new ColumnHelper<DecisionDiscrepancyModel>(_actDiscrepancyGrid);

            setup.Columns.Add(helper.CreateNumberColumn(o => o.AmountByAct, 2, 110).Configure(d =>
            {
                d.Caption = "Сумма по акту";
                d.HeaderToolTip = "Сумма по акту";
                d.DisableMoving = true;
                d.DisableFilter = true;
            }));

            _sumColumn = CreateEditAmountColumn(2, 110).Configure(d =>
            {
                d.Caption = "Сумма по решению";
                d.HeaderToolTip = "Сумма по решению";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.DisableEdit = false;
            });

            setup.Columns.Add(_sumColumn);


            return setup;
        }

        protected override string GetEditAmountColumnName()
        {
            return TypeHelper<DecisionDiscrepancyModel>.GetMemberName(t => t.AmountByDecision);
        }

        protected override void SetupSummaryRow(Controls.Grid.V2.DataGridView grid)
        {
            base.SetupSummaryRow(grid);

            string amountColumnName;
            SummarySettings summary;

            var band = grid.Grid.DisplayLayout.Bands[0];

            amountColumnName = TypeHelper<DecisionDiscrepancyModel>.GetMemberName(t => t.AmountByAct);

            summary = band.Summaries.Add(amountColumnName, String.Empty, SummaryPosition.UseSummaryPositionColumn, band.Columns[amountColumnName]);
            summary.DisplayFormat = " ";
            summary.Appearance.TextHAlign = HAlign.Left;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            _actSummaryAmount = summary;

            amountColumnName = TypeHelper<DecisionDiscrepancyModel>.GetMemberName(t => t.AmountByDecision);

            summary = band.Summaries.Add(amountColumnName, String.Empty, SummaryPosition.UseSummaryPositionColumn, band.Columns[amountColumnName]);
            summary.DisplayFormat = " ";
            summary.Appearance.TextHAlign = HAlign.Left;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            _decisionSummaryAmount = summary;
        }
    }
}
