﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects
{
    public class EditAmountColumn : SingleColumn
    {
        public EditAmountColumn(string key, IGrid grid, IColumnExpression columnExpression) 
            : base(key, grid, columnExpression)
        {
        }

        protected override void SetupColumn()
        {
            base.SetupColumn();

            var column = Column();

            column.Nullable = Nullable.EmptyString;
            column.Style = ColumnStyle.DoubleNonNegative;
        }
    }
}
