﻿using System;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    public partial class ActDiscrepancyEditView : KnpResultDocumentEditView, IActDiscrepancyEditView
    {
        private SummarySettings _actSummaryAmount;
        
        public ActDiscrepancyEditView()
        {
            InitializeComponent();

            _deleteToolStripMenuItem.Text = ResourceManagerNDS2
                                                .KnpResultDocumentStrings
                                                    .ActDiscrepancyExcludeMenuText;

            _labelDocNumberValue.Text = String.Empty;
            _labelDocDateFromValue.Text = String.Empty;
            _labelDocSum.Text = String.Empty;
        }

        #region Implementation of IActDiscrepancyEditView

        public void ShowActData(string num, string date)
        {
            _labelDocNumberValue.Text = num;
            _labelDocDateFromValue.Text = date;
        }

        public void ShowActAmount(bool isShow)
        {
            _flowLayoutPanel.Visible = isShow;
        }


        public override void SetFullAccess()
        {
            _flowLayoutPanel.Visible = false;

            base.SetFullAccess();
        }

        public override void SetReadOnly()
        {
            _flowLayoutPanel.Visible = true;

            base.SetReadOnly();
        }

        public string SummaryActAmount
        {
            set
            {
                if (_actSummaryAmount != null)
                    _actSummaryAmount.DisplayFormat = value;

                _labelDocSum.Text = value;
            }
        }

        #endregion

        protected override GridColumnSetup SetupGridColumn()
        {
            var setup = base.SetupGridColumn();

            _sumColumn = CreateEditAmountColumn(2, 110)
                .Configure(d =>
                {
                    d.Caption = "Сумма по акту";
                    d.HeaderToolTip = "Сумма по акту";
                    d.DisableMoving = true;
                    d.DisableFilter = true;
                    d.DisableEdit = false;
                });

            setup.Columns.Add(_sumColumn);

            return setup;
        }

        protected override string GetEditAmountColumnName()
        {
            return TypeHelper<ActDiscrepancyModel>.GetMemberName(t => t.AmountByAct);
        }

        protected override void SetupSummaryRow(Controls.Grid.V2.DataGridView grid)
        {
            base.SetupSummaryRow(grid);
            
            string amountColumnName = TypeHelper<ActDiscrepancyModel>.GetMemberName(t => t.AmountByAct);
            var band = grid.Grid.DisplayLayout.Bands[0];

            SummarySettings summary;
            summary = band.Summaries.Add(amountColumnName, String.Empty, SummaryPosition.UseSummaryPositionColumn, band.Columns[amountColumnName]);
            summary.DisplayFormat = " ";
            summary.Appearance.TextHAlign = HAlign.Left;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            _actSummaryAmount = summary;
        }
    }
}
