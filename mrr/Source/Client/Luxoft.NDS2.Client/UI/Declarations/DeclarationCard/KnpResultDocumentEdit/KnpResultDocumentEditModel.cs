﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit.ServiceObjects;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.DiscrepancyKnp;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Validators;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    
    /// <summary>
    /// Класс-модель для окна редактирования расхождений в Акты/Решения
    /// </summary>
    /// <typeparam name="TActDiscrepanciesListItemModel">Тип отображаемых данных в списке</typeparam>   
    public class KnpResultDocumentEditModel<TActDiscrepanciesListItemModel> 
        where TActDiscrepanciesListItemModel : KnpResultDiscrepancyModel
    {
        private const int _defaultPageSize = 50;

        /// <summary>
        /// Идентификатор документа
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Список актированных расхождений
        /// </summary>
        public List<TActDiscrepanciesListItemModel> ActDiscrepancies { get; private set; }

        /// <summary>
        /// Модель пейджера страниц списка
        /// </summary>
        public IPager ActDiscrepancyListPager { get; private set; }

        /// <summary>
        /// Общее количество актированных расхождений
        /// </summary>
        public int TotalAialibleActDiscrepancies
        {
            get { return _totalAialibleActDiscrepancies; }
            set
            {
                _totalAialibleActDiscrepancies = value;

                if (TotalAialibleActDiscrepanciesChanges != null)
                    TotalAialibleActDiscrepanciesChanges(this, EventArgs.Empty);
            }
        }

        private int _totalAialibleActDiscrepancies;

        public event EventHandler TotalAialibleActDiscrepanciesChanges;

        /// <summary>
        /// Общее количество отобранных по фильтру актированных расхождений
        /// </summary>
        public int TotalMatchesActDiscrepancies
        {
            get { return _totalMatchesActDiscrepancies; }
            set
            {
                _totalMatchesActDiscrepancies = value;

                if (TotalMatchesActDiscrepanciesChanges != null)
                    TotalMatchesActDiscrepanciesChanges(this, EventArgs.Empty);
            }
        }

        private int _totalMatchesActDiscrepancies;

        public event EventHandler TotalMatchesActDiscrepanciesChanges;

        /// <summary>
        /// Общее количество отобранных пользователем актированных расхождений
        /// </summary>
        public int TotalSelectedActDiscrepancies
        {
            get { return _totalSelectedActDiscrepancies; }
            set
            {
                _totalSelectedActDiscrepancies = value;

                if (TotalSelectedActDiscrepanciesChanges != null)
                    TotalSelectedActDiscrepanciesChanges(this, EventArgs.Empty);
            }
        }

        private int _totalSelectedActDiscrepancies;

        public event EventHandler TotalSelectedActDiscrepanciesChanges;

        /// <summary>
        /// Общее количество некорректных актированных расхождений
        /// </summary>
        public int TotalInvalidActDiscrepancies
        {
            get { return _totalInvalidActDiscrepancies; }
            set
            {
                _totalInvalidActDiscrepancies = value;

                if (TotalInvalidActDiscrepanciesChanges != null)
                    TotalInvalidActDiscrepanciesChanges(this, EventArgs.Empty);
            }
        }

        private int _totalInvalidActDiscrepancies;

        public event EventHandler TotalInvalidActDiscrepanciesChanges;

        /// <summary>
        /// Критерии последнего запроса списка актированных расхождений
        /// </summary>
        public QueryConditions LastQueryConditions { get; set; }

        /// <summary>
        /// Признак выставления режима только на чтение
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; } 
            set
            {
                _isReadOnly = value;

                ActDiscrepancies.ForEach(x => x.IsReadOnly = value);
            }
        }

        private bool _isReadOnly;

        /// <summary>
        /// Признак того, что данные формы можно сохранять
        /// </summary>
        public bool AllowSaveChanges
        {
            get { return (TotalSelectedActDiscrepancies > 0) && (TotalInvalidActDiscrepancies == 0); }
        }

        /// <summary>
        /// Общая сумма по включенным расхождениям
        /// </summary>
        public decimal SummaryDiscrepancyAmount { get; set; }
        public string SummaryDiscrepancyAmountView { get { return SummaryDiscrepancyAmount.ToString("n2"); } }

        /// <summary>
        /// Общая сумма акта по включенным расхождениям
        /// </summary>
        public decimal SummaryActAmount { get; set; }
        public string SummaryActAmountView { get { return SummaryActAmount.ToString("n2"); } }

        /// <summary>
        /// Общая сумма решения по включенным расхождениям
        /// </summary>
        public decimal SummaryDecisionAmount { get; set; }
        public string SummaryDecisionAmountView { get { return SummaryDecisionAmount.ToString("n2"); } }

        public event EventHandler DiscrepancyAmountInvalid;
        public event EventHandler DiscrepancyAmountValid;
        public event EventHandler DiscrepancyAmountChange;

        /// <summary>
        /// Текущая версия данных
        /// </summary>
        public long CurrentDataVersion { get; set; }

        /// <summary>
        /// Версия загруженных данных
        /// </summary>
        public long DataVersion { get; set; }


        /// <summary>
        /// Конструктор - централизованная инициализация класса модели
        /// </summary>
        /// <param name="knpResultDiscrepancyDocumentId">Идентификатор документа</param>
        public KnpResultDocumentEditModel(long knpResultDiscrepancyDocumentId)
        {
            Id = knpResultDiscrepancyDocumentId;

            ActDiscrepancyListPager = new PagerStateViewModel()
            {
                PageSize = _defaultPageSize
            };

            ActDiscrepancies = new List<TActDiscrepanciesListItemModel>();
        }

        public void ReloadActDiscrepancies(List<TActDiscrepanciesListItemModel> newActDiscrepancies)
        {
            ActDiscrepancies.Clear();
            ActDiscrepancies.AddRange(newActDiscrepancies);
            ActDiscrepancies.ForEach(x =>
            {
                x.IsReadOnly = IsReadOnly;
                x.DiscrepancyAmountChanged += DiscrepancyAmountChanged;
            });
        }

        /// <summary>
        /// Удалить расхождение
        /// </summary>
        /// <param name="actDiscrepancy">расхождение</param>
        public void Remove(TActDiscrepanciesListItemModel actDiscrepancy)
        {
            TotalAialibleActDiscrepancies--;
            TotalSelectedActDiscrepancies--;
            TotalMatchesActDiscrepancies--;

            if (actDiscrepancy.IsInvalid)
                TotalInvalidActDiscrepancies--;

            ActDiscrepancies.Remove(actDiscrepancy);

            SummaryDiscrepancyAmount -= actDiscrepancy.Amount;
            SummaryActAmount -= actDiscrepancy.AmountByActNum.HasValue ? actDiscrepancy.AmountByActNum.Value : 0;
            SummaryDecisionAmount -= actDiscrepancy.AmountByDecisionNum.HasValue ? actDiscrepancy.AmountByDecisionNum.Value : 0;
        }

        private void DiscrepancyAmountChanged(object sender, EventArgs args)
        {
            if (DiscrepancyAmountChange != null)
                DiscrepancyAmountChange(sender, args);
        }

        public void ApplyActDiscrepancyAmountChange(TActDiscrepanciesListItemModel actDiscrepancy)
        {
            if (actDiscrepancy.IsInvalid)
            {
                if (DiscrepancyAmountInvalid != null)
                    DiscrepancyAmountInvalid(actDiscrepancy, EditAmountEventArgs.Create(actDiscrepancy.GetEditableAmount()));

                if (!actDiscrepancy.PreviousInvalidState)
                    TotalInvalidActDiscrepancies++;
            }
            else
            {
                if (DiscrepancyAmountValid != null)
                    DiscrepancyAmountValid(actDiscrepancy, EditAmountEventArgs.Create(actDiscrepancy.GetEditableAmount()));

                if (actDiscrepancy.PreviousInvalidState)
                    TotalInvalidActDiscrepancies--;
            }
        }


    }
}
