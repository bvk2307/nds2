﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentEdit
{
    partial class KnpResultDocumentEditView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._saveButton = new Infragistics.Win.Misc.UltraButton();
            this._contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._buttonPanel = new Infragistics.Win.Misc.UltraPanel();
            this._discrepancyInvalidCountLabel = new Infragistics.Win.Misc.UltraLabel();
            this._actDiscrepancyGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanel.SuspendLayout();
            this._contextMenuStrip.SuspendLayout();
            this._buttonPanel.ClientArea.SuspendLayout();
            this._buttonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            tableLayoutPanel.ColumnCount = 3;
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel.Controls.Add(this._saveButton, 1, 0);
            tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel.Name = "tableLayoutPanel";
            tableLayoutPanel.RowCount = 1;
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tableLayoutPanel.Size = new System.Drawing.Size(1261, 35);
            tableLayoutPanel.TabIndex = 0;
            // 
            // _saveButton
            // 
            this._saveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveButton.Enabled = false;
            this._saveButton.Location = new System.Drawing.Point(533, 3);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(194, 29);
            this._saveButton.TabIndex = 1;
            this._saveButton.Text = "Сохранить";
            this._saveButton.Click += new System.EventHandler(this.SaveBtnClick);
            // 
            // _contextMenuStrip
            // 
            this._contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._deleteToolStripMenuItem});
            this._contextMenuStrip.Name = "contextMenuStrip";
            this._contextMenuStrip.Size = new System.Drawing.Size(153, 48);
            // 
            // _deleteToolStripMenuItem
            // 
            this._deleteToolStripMenuItem.Name = "_deleteToolStripMenuItem";
            this._deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this._deleteToolStripMenuItem.Text = "Удалить";
            this._deleteToolStripMenuItem.Click += new System.EventHandler(this.OnDeleteToolStripMenuItemClick);
            // 
            // _buttonPanel
            // 
            // 
            // _buttonPanel.ClientArea
            // 
            this._buttonPanel.ClientArea.Controls.Add(tableLayoutPanel);
            this._buttonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonPanel.Location = new System.Drawing.Point(0, 447);
            this._buttonPanel.Name = "_buttonPanel";
            this._buttonPanel.Size = new System.Drawing.Size(1261, 35);
            this._buttonPanel.TabIndex = 2;
            // 
            // _discrepancyInvalidCountLabel
            // 
            appearance1.ForeColor = System.Drawing.Color.Red;
            appearance1.TextVAlignAsString = "Middle";
            this._discrepancyInvalidCountLabel.Appearance = appearance1;
            this._discrepancyInvalidCountLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._discrepancyInvalidCountLabel.Location = new System.Drawing.Point(0, 424);
            this._discrepancyInvalidCountLabel.Name = "_discrepancyInvalidCountLabel";
            this._discrepancyInvalidCountLabel.Size = new System.Drawing.Size(1261, 23);
            this._discrepancyInvalidCountLabel.TabIndex = 1;
            this._discrepancyInvalidCountLabel.Text = "Некорректно заполненных записей:";
            // 
            // _actDiscrepancyGrid
            // 
            this._actDiscrepancyGrid.AggregatePanelVisible = true;
            this._actDiscrepancyGrid.AllowFilterReset = false;
            this._actDiscrepancyGrid.AllowMultiGrouping = true;
            this._actDiscrepancyGrid.AllowResetSettings = true;
            this._actDiscrepancyGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._actDiscrepancyGrid.BackColor = System.Drawing.Color.Transparent;
            this._actDiscrepancyGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._actDiscrepancyGrid.ColumnVisibilitySetupButtonVisible = false;
            this._actDiscrepancyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._actDiscrepancyGrid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._actDiscrepancyGrid.ExportExcelCancelVisible = false;
            this._actDiscrepancyGrid.ExportExcelHint = "Экспорт в MS Excel";
            this._actDiscrepancyGrid.ExportExcelVisible = false;
            this._actDiscrepancyGrid.FilterResetVisible = false;
            this._actDiscrepancyGrid.FooterVisible = true;
            this._actDiscrepancyGrid.GridContextMenuStrip = this._contextMenuStrip;
            this._actDiscrepancyGrid.Location = new System.Drawing.Point(0, 0);
            this._actDiscrepancyGrid.Name = "_actDiscrepancyGrid";
            this._actDiscrepancyGrid.PanelExportExcelStateVisible = false;
            this._actDiscrepancyGrid.PanelLoadingVisible = true;
            this._actDiscrepancyGrid.PanelPagesVisible = true;
            this._actDiscrepancyGrid.RowDoubleClicked = null;
            this._actDiscrepancyGrid.Size = new System.Drawing.Size(1261, 424);
            this._actDiscrepancyGrid.TabIndex = 0;
            this._actDiscrepancyGrid.Title = "";
            // 
            // KnpResultDocumentEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._actDiscrepancyGrid);
            this.Controls.Add(this._discrepancyInvalidCountLabel);
            this.Controls.Add(this._buttonPanel);
            this.Name = "KnpResultDocumentEditView";
            this.Size = new System.Drawing.Size(1261, 482);
            tableLayoutPanel.ResumeLayout(false);
            this._contextMenuStrip.ResumeLayout(false);
            this._buttonPanel.ClientArea.ResumeLayout(false);
            this._buttonPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected Controls.Grid.V2.DataGridView _actDiscrepancyGrid;
        protected Infragistics.Win.Misc.UltraButton _saveButton;
        private Infragistics.Win.Misc.UltraLabel _discrepancyInvalidCountLabel;
        private Infragistics.Win.Misc.UltraPanel _buttonPanel;
        protected System.Windows.Forms.ToolStripMenuItem _deleteToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip _contextMenuStrip;
    }
}
