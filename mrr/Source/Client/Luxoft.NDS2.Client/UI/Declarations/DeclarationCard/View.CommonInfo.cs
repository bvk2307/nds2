﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Infragistics.Win;
using Infragistics.Win.FormattedLinkLabel;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    partial class View
    {

        #region Шапка

        public void SetDeclarationRevisions(List<DeclarationVersion> declarationRevisions, string correctionNumber = null, long declarationVersionId = 0)
        {
            ShowRevisions(declarationRevisions, correctionNumber, declarationVersionId);
        }

        private System.Windows.Forms.ToolTip toolTipSur = new System.Windows.Forms.ToolTip();
        private bool _initializingRevisions = false;

        private void ShowRevisions(List<DeclarationVersion> declarationRevisions, string correctionNumber = null, long declarationVersionId = 0)
        {
            _initializingRevisions = true;

            foreach (DeclarationVersion version in declarationRevisions)
                version.BuildCaption();

            if (declarationRevisions.Where(p => p.CorrectionNumberRank > 1).Count() > 0)
                comboCorrectionNumValue.DropDownListWidth = 340;
            else
                comboCorrectionNumValue.DropDownListWidth = 0;

            comboCorrectionNumValue.Clear();
            comboCorrectionNumValue.Items.Clear();

            ValueListItem selectedItem = null;
            foreach (var item in declarationRevisions)
            {
                var listItem = new ValueListItem()
                {
                    DisplayText = item.CorrectionNumberCaption,
                    DataValue = item
                };
                comboCorrectionNumValue.Items.Add(listItem);

                if (correctionNumber != null && correctionNumber.Equals(item.CorrectionNumber, StringComparison.InvariantCultureIgnoreCase) &&
                    declarationVersionId.Equals(item.DeclarationVersionId))
                {
                    selectedItem = listItem;
                }
            }

            if (comboCorrectionNumValue.Items.Count > 0)
            {
                comboCorrectionNumValue.SelectedItem = selectedItem == null ? comboCorrectionNumValue.Items[0] : selectedItem;
            }

            _initializingRevisions = false;
        }

        private void SetCommonInformation()
        {
            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = _model.SurCode;

            grpHeader.Text =
                _model.Type == DeclarationTypeCode.Declaration
                    ? "Сведения о поданной декларации"
                    : "Сведения о поданном журнале";

            labelNameValue.Value = string.Concat("<a href='#'>", _model.Name, "</a>");
            if (_model.Type == DeclarationTypeCode.Declaration)
                labelRegNumValue.Text = _model.RegNumber;
            else if (_model.Type == DeclarationTypeCode.Journal)
                labelRegNumValue.Text = _model.SeodId.ToString();

            labelCorrectionNumCaption.Text = _model.CorrectionNumCaption;
            labelCorrectionStatus.Text = _model.CorrectionStatus;

            labelPeriodValue.Text = _model.FulTaxPeriod;
            labelInspectorValue.Text = _model.Inspector;
            labelSignValue.Text = _model.TotalNdsTitle;
            labelSignCaption.Text = _model.TotalNdsValue;
            labelDateTakeValue.Text = _model.DeclarationDate;

            labelAnnulmentFlag.Visible = _model.IsAnnulmentFlagVisible();

            labelDateTakeCaption.Visible = _model.IsDateTakeVisible();
            labelDateTakeValue.Visible = _model.IsDateTakeVisible();

            labelSignCaption.Visible = _model.IsSignVisible();
            labelSignValue.Visible = _model.IsSignVisible();
            
            grpCommonInfo.Expanded = _model.IsCommonInfoAccessible() && grpCommonInfo.Expanded;
            grpCommonInfo.Enabled = _model.IsCommonInfoAccessible();
        }

        private void labelInnValue_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.ViewTaxPayerDetails();
        }

        private void comboCorrectionNumValue_ValueChanged(object sender, EventArgs e)
        {
            UpdateCurrentRevision();
        }

        private void UpdateCurrentRevision()
        {
            long _curRev = -1;

            if (comboCorrectionNumValue.SelectedItem != null)
            {
                var declarationVersion = (DeclarationVersion)comboCorrectionNumValue.SelectedItem.DataValue;
                _curRev = declarationVersion.DeclarationVersionId;
            }

            if (!_initializingRevisions)
            {
                progressBar.Visible = true;
                _presenter.SetRevision(_curRev);
            }
        }

        #endregion

        #region Общие сведения

        private string FormatAggregateValue(decimal? value)
        {
            return value == null || value.Value == 0 ? "" : string.Format(new IntValueZeroFormatter(), "{0:N2}", value);
        }

        private string FormatCombinePriceShowZero(string label, decimal? value)
        {
            if (value == null) return string.Empty;
            var ret = new StringBuilder();
            ret.Append(label);
            ret.Append(new String(' ', 15));
            ret.Append(value.Value.ToString("N2"));
            return ret.ToString();
        }

        private void ShowDeclarationDircrepancyInformation()
        {
            grpCommonInfo.Visible = _model.CommonInfoIsAvailable;
            grpCommonInfo.Expanded = _presenter.GetSavedCommonInfoExpandedStateOrDefault();
            if (_model.SummaryState == DeclarationSummaryState.Actual)
            {
                ulNotLoadedInMS.Text = string.Empty;
            }
            else if (_model.SummaryState == DeclarationSummaryState.Nonactual && _model.RawData.UPDATE_DATE.HasValue)
            {
                ulNotLoadedInMS.Text = string.Format(_model.Type == DeclarationTypeCode.Declaration
                    ? ResourceManagerNDS2.DeclarationMessages.DECLARATION_PROCESSED_PREVIOUS
                    : ResourceManagerNDS2.DeclarationMessages.JOURNAL_PROCESSED_PREVIOUS, _model.RawData.UPDATE_DATE.Value.ToString("dd.MM.yyyy"));
            }
            else
            {
                ulNotLoadedInMS.Text = _model.Type == DeclarationTypeCode.Declaration
                    ? ResourceManagerNDS2.DeclarationMessages.DECLARATION_NOT_PROCESSED
                    : ResourceManagerNDS2.DeclarationMessages.JOURNAL_NOT_PROCESSED;
            }

            if (_model.CommonInfoIsAvailable)
            {
                labelDiscrepanciesSFCountValue.Text = _model.RawData.TOTAL_DISCREP_COUNT.ToString();
                labelDiscrepanciesCommonSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_TOTAL_AMNT);
                labelDiscrepanciesCommonPVPSumValue.Text = FormatAggregateValue(_model.RawData.PVP_TOTAL_AMNT);
                labelDiscrepanciesBuyBookSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_BUY_BOOK_AMNT);
                labelDiscrepanciesBuyBookPVPSumValue.Text = FormatAggregateValue(_model.RawData.PVP_BUY_BOOK_AMNT);
                labelDiscrepanciesSellBookSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_SELL_BOOK_AMNT);
                labelDiscrepanciesSellBookPVPSumValue.Text = FormatAggregateValue(_model.RawData.PVP_SELL_BOOK_AMNT);

                labelDiscrepanciesMinSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_MIN_AMNT);
                labelDiscrepanciesPVPMinSumValue.Text = FormatAggregateValue(_model.RawData.PVP_DISCREP_MIN_AMNT);
                labelDiscrepanciesMaxSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_MAX_AMNT);
                labelDiscrepanciesPVPMaxSumValue.Text = FormatAggregateValue(_model.RawData.PVP_DISCREP_MAX_AMNT);
                labelDiscrepanciesAvgSumValue.Text = FormatAggregateValue(_model.RawData.DISCREP_AVG_AMNT);
                labelDiscrepanciesPVPAvgSumValue.Text = FormatAggregateValue(_model.RawData.PVP_DISCREP_AVG_AMNT);

                if (_model.RawData.DECL_TYPE_CODE == 0)
                {
                    labelDiscrepanciesKSCountCaption.Visible = true;
                    labelDiscrepanciesKSCountValue.Visible = true;
                    if (_model.RawData.CONTROL_RATIO_COUNT != null)
                    {
                        long cr = (long)_model.RawData.CONTROL_RATIO_COUNT;
                        labelDiscrepanciesKSCountValue.Text = (cr.ToString(CultureInfo.InvariantCulture));
                    }
                    labelBuyBookCaption.Text = "Книга покупок";
                    labelSellBookCaption.Text = "Книга продаж";
                    tableLayout.RowStyles[3].Height = 20;
                    tableLayout.RowStyles[6].Height = 20;
                    tableLayout.RowStyles[9].Height = 20;
                }
                else
                {
                    labelDiscrepanciesKSCountCaption.Visible = false;
                    labelDiscrepanciesKSCountValue.Visible = false;

                    labelBuyBookCaption.Text = "Журнал выставленных СФ";
                    labelSellBookCaption.Text = "Журнал полученных СФ";
                    tableLayout.RowStyles[3].Height = 0;
                    tableLayout.RowStyles[6].Height = 0;
                    tableLayout.RowStyles[9].Height = 0;
                }

                grpCommonInfo.Height = autosizePanel.Height;
            }
        }

        #endregion

        private void ChaptersShowInfo()
        {
            ultraExpandableGroupBox_R8_Header.Expanded = false;
            ultraExpandableGroupBox_R9_Header.Expanded = false;

            GetChapterTitleActualData(DeclarationInvoiceChapterNumber.Chapter8, labelChapter8_TitleActualData);
            GetChapterTitleActualData(DeclarationInvoiceChapterNumber.Chapter9, labelChapter9_TitleActualData);
            GetChapterTitleActualData(DeclarationInvoiceChapterNumber.Chapter10, labelChapter10_TitleActualData);
            GetChapterTitleActualData(DeclarationInvoiceChapterNumber.Chapter11, labelChapter11_TitleActualData);
            GetChapterTitleActualData(DeclarationInvoiceChapterNumber.Chapter12, labelChapter12_TitleActualData);

            Chapter8ShowInfo();
            Chapter9ShowInfo();
        }

        private void GetChapterTitleActualData(DeclarationInvoiceChapterNumber chapterNumber, UltraFormattedLinkLabel label)
        {
            label.Value = String.Empty;

            if (_model.Type == DeclarationTypeCode.Declaration)
            {
                try
                {
                    if (_model.ChapterModel[chapterNumber].Cache.Any())
                        label.Value = _model.ChapterModel[chapterNumber].ToString();
                }
                catch (Exception ex)
                {
                    string messageError = string.Format("Ошибка формирования сообщения об актуальности данных в разделе {0}", (int)chapterNumber);
                    _presenter.LogError(messageError, String.Empty, ex);
                    this.ShowError(messageError);
                }
            }
        }

        private void Chapter8ShowInfo()
        {
            ultraLabelR8P190.Text = FormatCombinePriceShowZero("р.8 стр.190",_model.RawData.SumNDSPok_8);
            ultraLabelR8_1P190.Text = FormatCombinePriceShowZero("р.8.1 стр.190", _model.RawData.SumNDSPokDL_81);
            ultraLabelR8_1P005.Text = FormatCombinePriceShowZero("р.8.1 стр.005", _model.RawData.SumNDSPok_81);
        }

        private void Chapter9ShowInfo()
        {
            ultraLabelR9P230.Text = FormatCombinePriceShowZero("р.9 стр.230",_model.RawData.StProd18_9);
            ultraLabelR9P240.Text = FormatCombinePriceShowZero("р.9 стр.240",_model.RawData.StProd10_9);
            ultraLabelR9P250.Text = FormatCombinePriceShowZero("р.9 стр.250",_model.RawData.StProd0_9);
            ultraLabelR9P260.Text = FormatCombinePriceShowZero("р.9 стр.260",_model.RawData.SumNDSProd18_9);
            ultraLabelR9P270.Text = FormatCombinePriceShowZero("р.9 стр.270",_model.RawData.SumNDSProd10_9);
            ultraLabelR9P280.Text = FormatCombinePriceShowZero("р.9 стр.280",_model.RawData.StProdOsv_9);

            ultraLabelR9_1P020.Text = FormatCombinePriceShowZero("р.9.1 стр.020",_model.RawData.StProd18_91);
            ultraLabelR9_1P030.Text = FormatCombinePriceShowZero("р.9.1 стр.030",_model.RawData.StProd10_91);
            ultraLabelR9_1P040.Text = FormatCombinePriceShowZero("р.9.1 стр.040",_model.RawData.StProd0_91);
            ultraLabelR9_1P050.Text = FormatCombinePriceShowZero("р.9.1 стр.050",_model.RawData.SumNDSProd18_91);
            ultraLabelR9_1P060.Text = FormatCombinePriceShowZero("р.9.1 стр.060",_model.RawData.SumNDSProd10_91);
            ultraLabelR9_1P070.Text = FormatCombinePriceShowZero("р.9.1 стр.070",_model.RawData.StProdOsv_91);

            ultraLabelR9_1P310.Text = FormatCombinePriceShowZero("р.9.1 стр.310",_model.RawData.StProd18DL_91);
            ultraLabelR9_1P320.Text = FormatCombinePriceShowZero("р.9.1 стр.320",_model.RawData.StProd10DL_91);
            ultraLabelR9_1P330.Text = FormatCombinePriceShowZero("р.9.1 стр.330",_model.RawData.StProd0DL_91);
            ultraLabelR9_1P340.Text = FormatCombinePriceShowZero("р.9.1 стр.340",_model.RawData.SumNDSProd18DL_91);
            ultraLabelR9_1P350.Text = FormatCombinePriceShowZero("р.9.1 стр.350",_model.RawData.SumNDSProd10DL_91);
            ultraLabelR9_1P360.Text = FormatCombinePriceShowZero("р.9.1 стр.360",_model.RawData.StProdOsvDL_91);
        }
    }
}
