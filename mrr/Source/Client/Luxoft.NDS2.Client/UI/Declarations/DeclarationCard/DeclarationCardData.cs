﻿using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard
{
    public class DeclarationCardData
    {
        private long _id = -1;

        public readonly string inn = string.Empty;
        public readonly string year = string.Empty;
        public readonly string period = string.Empty;
        public readonly string key = string.Empty;

        public readonly string filtrInn = string.Empty;

        public readonly string _invoiceNum = string.Empty;
        public readonly int _chapter = -1;
        public int _step = 0;

        public readonly DeclarationSummary _decl;
        public readonly DeclarationInfoSource _declSource;

        public DeclarationCardData(long id) 
        { _id = id; }
       
        public DeclarationCardData(string innDecl, string fiscalYear, string fiscalPeriod) 
        { inn = innDecl; year = fiscalYear; period = fiscalPeriod; }

        public DeclarationCardData(string declKey, string innDecl, int step)
        { key = declKey; filtrInn = innDecl;
            _step = step;
        }

        public DeclarationCardData(long id, int chapter, string invoiceNum)
        { _id = id; _chapter = chapter; _invoiceNum = invoiceNum; }

        public DeclarationCardData(DeclarationSummary declaration)
        { _decl = declaration; inn = declaration.INN; year = declaration.FISCAL_YEAR; period = declaration.TAX_PERIOD; }

        public DeclarationCardData(DeclarationSummary declaration, DeclarationInfoSource declarationInfoSource)
        { _decl = declaration; inn = declaration.INN; year = declaration.FISCAL_YEAR; period = declaration.TAX_PERIOD; _declSource = declarationInfoSource; }


        public long Id 
        {
            get
            {
                long ret = -1;

                if (_id != -1)
                    ret = _id;
                else if (_decl != null)
                    ret = _decl.DECLARATION_VERSION_ID;
                else if (!string.IsNullOrEmpty(key))
                    ret = GetKeyID(key);

                return ret;
            }
        }

        public DeclarationPart KeyDeclarationPart
        {
            get 
            {
                if (_chapter > -1)
                {
                    return (DeclarationPart)_chapter;
                }
                else
                {
                    return GetKeyDeclPart(key);
                }
            }
        }

        private long GetKeyID(string k)
        {
            long ret;
            int pos = k.IndexOf("/");

            if (pos > 0)
                ret = Convert.ToInt64(k.Substring(0, pos));
            else
                ret = Convert.ToInt64(k);

            return ret;
        }

        private DeclarationPart GetKeyDeclPart(string k)
        {
            Dictionary<int, DeclarationPart> partsRecode = new Dictionary<int, DeclarationPart>()
            {
                { 0, DeclarationPart.R8 },
                { 1, DeclarationPart.R9 },
                { 2, DeclarationPart.R8 },
                { 3, DeclarationPart.R9 },
                { 4, DeclarationPart.R10 },
                { 5, DeclarationPart.R11 },
                { 6, DeclarationPart.R12 },
                { 7, DeclarationPart.J1 },
                { 8, DeclarationPart.J2 },
            };

            string[] parts = k.Split('/');

            int num = Convert.ToInt32(parts[1]);

            return partsRecode[num];
        }
    }
}
