﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel
{
    public class ExportExcelParam
    {
        public DeclarationPart DeclarationPart { get; set; }
        public ExcelManagerInvoice ExcelManager { get; set; }
        public string InvoiceExcelFilePathTemp { get; set; }
        public string InvoiceExcelFilePath { get; set; }
        public uint PageFirstExcel { get; set; }
        public uint PageCountExcel { get; set; }
        public string InvoiceExcelName { get; set; }
        public ExportExcelTimer TimerOpenReport { get; set; }
        public ExportExcelTimer TimerUpdateState { get; set; }
        public long InvoiceCount { get; set; }
        public int PageSize { get; set; }
        public long CountReadRows { get; set; }
        public long DelayBetweenPage { get; set; }
        public ManualResetEventSlim ResetEvent { get; set; }
        public string SheetNameTemplate { get; set; }
        public long SheetSizeRow { get; set; }
        public int SheetCount { get; set; }
        public int SheetSizePage { get; set; }
        public Dictionary<int, string> SheetNames { get; set; }

        public ExportExcelParam()
        {
            SheetNames = new Dictionary<int, string>();
        }

        public string GetSheetName(int numSheet)
        {
            string ret = String.Empty; 
            if (SheetNames != null && SheetNames.ContainsKey(numSheet))
            {
                ret = SheetNames[numSheet];
            }
            return ret;
        }

        public void Caclulate()
        {
            CalculatePageCount();
            CalculateSheetCount();
            CalculateSheetSizePage();
            FillSheetNames();
        }

        public void FillSheetNames()
        {
            SheetNames.Clear();
            for (int i = -1; ++i < SheetCount; )
            {
                SheetNames.Add(i, string.Format("{0}{1}{2}{3}", SheetNameTemplate, "(", i + 1, ")"));
            }
        }

        public void CalculatePageCount()
        {
            long ostatok;
            long celoe = Math.DivRem(InvoiceCount, (long)PageSize, out ostatok);
            if (ostatok > 0)
            {
                celoe++;
            }
            PageCountExcel = (uint)celoe;
        }

        public void CalculateSheetCount()
        {
            long ostatok;
            long celoe = Math.DivRem(InvoiceCount, (long)SheetSizeRow, out ostatok);
            if (ostatok > 0)
            {
                celoe++;
            }
            if (celoe == 0)
                celoe = 1;
            SheetCount = (int)celoe;
        }

        public void CalculateSheetSizePage()
        {
            if (InvoiceCount <= SheetSizeRow)
            {
                SheetSizePage = (int)PageCountExcel;
            }
            else
            {
                long ostatok;
                SheetSizePage = (int)Math.DivRem(SheetSizeRow, (long)PageSize, out ostatok);
            }
        }
    }
}
