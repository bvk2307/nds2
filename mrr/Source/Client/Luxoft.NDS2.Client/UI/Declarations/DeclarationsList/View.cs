﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Drawing;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Extensions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationsList
{
    public partial class View : BaseView
    {
        private DeclarationsListPresenter _presenter;


        [CreateNew]
        public DeclarationsListPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        public View(PresentationContext context)
            : base(context)
        {
            _presentationContext = context;
            _presentationContext.WindowTitle = "Список деклараций";
            _presentationContext.WindowDescription = "Список деклараций";
            InitializeComponent();
        }

        public override void OnUnload()
        {
            Dispose();
            base.OnUnload();
        }

        private void View_Load(object sender, EventArgs e)
        {
            InitializePreFilter();
            InitializeGrid();
            InitializeRibbon();

            _presenter.RefreshEKP();
        }

        private GridColumnSetup SetupGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<DeclarationSummary>(grid);

            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            var booleanCodes = new List<BooleanCode>
            {
                new BooleanCode() {Index = 1, Code = 1, Description = "Установлен"},
                new BooleanCode() {Index = 0, Code = 0, Description = "Не установлен"}
            };

            var cancelledTips = new Dictionary<string, string>
            {
                {false.ToString(), null},
                {true.ToString(), "Есть аннулированная корректировка за отчетный период"}
            };

            var columnExpressionDefault = ColumnExpressionCreator.CreateColumnExpressionDefault();
            var dictionaryBoolean = new DictionaryBoolean(booleanCodes);
            var optionsBuilder = new BooleanAutoCompleteOptionsBuilder(dictionaryBoolean);

            setup.Columns.Add(helper.CreateTextColumn(o => o.DECL_TYPE, 4)
                .Configure(d => { d.Caption = "Тип"; d.HeaderToolTip = "НД по НДС или Журнал учета"; }));

            setup.Columns.Add(
                helper.CreateBoolColumn(o => o.HAS_CANCELLED_CORRECTION, columnExpressionDefault, dictionaryBoolean, optionsBuilder, 3, 60, true).Configure(
            c =>
            {
                c.DisableEdit = true;
                c.ToolTipViewer = new DictionaryToolTipViewer(cancelledTips);
            })
            );

            #region Налогоплательщик

            var group = helper.CreateGroup("Налогоплательщик");
            group.Columns.Add(helper.CreateSurColumn(o => o.SUR_CODE, _presenter.Sur, 3));
            group.Columns.Add(helper.CreateTextColumn(o => o.INN, 3));
            group.Columns.Add(helper.CreateTextColumn(o => o.KPP, 3).Configure(c => c.BlankValue = '-'));
            group.Columns.Add(helper.CreateTextColumn(o => o.NAME, 3));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.SEOD_DECL_ID, 4));
            setup.Columns.Add(helper.CreateTextColumn(o => o.DECL_SIGN, 4));
            setup.Columns.Add(helper.CreateNumberColumn(o => o.COMPENSATION_AMNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Сумма НДС";
                    d.DataFormatInfo = new SumNDSDeclarationFormatter();
                    d.HeaderToolTip = "Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет";
                }));
            setup.Columns.Add(helper.CreateNumberColumn(o => o.NDS_WEIGHT, 4)
                .Configure(column =>
                {
                    column.DataFormatInfo = new IntFormatValueNullFormatter();
                }));
            setup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.TOTAL_DISCREP_COUNT, 4)
                .Configure(d =>
                {
                    d.Caption = "Кол-во расхождений по СФ";
                    d.HeaderToolTip = "Кол-во расхождений по СФ";
                }));

            setup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CONTROL_RATIO_COUNT, 4));

            setup.Columns.Add(helper.CreateTextColumn(o => o.CH8_PRESENT_DESCRIPTION, 4)
                .Configure(d =>
                {
                    d.Caption = "Признак подачи: Книга покупок";
                    d.HeaderToolTip = "Признак подачи книги покупок (раздел 8)";
                }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.CH9_PRESENT_DESCRIPTION, 4)
                .Configure(d =>
                {
                    d.Caption = "Признак подачи: Книга продаж";
                    d.HeaderToolTip = "Признак подачи книги продаж (раздел 9)";
                }));
           

            #region Сумма расхождений по книгам

            group = helper.CreateGroup("Сумма расхождений по книгам");
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_BUY_BOOK_AMNT, 3, 90));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_SELL_BOOK_AMNT, 3, 90));
            setup.Columns.Add(group);

            #endregion

            #region Сумма расхождений в НД

            group = helper.CreateGroup("Сумма расхождений в НД");
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_TOTAL_AMNT, 3)
                .Configure(d =>
                {
                    d.HeaderToolTip = "Общая сумма расхождений по СФ";
                    d.DataFormatInfo = formatInfoZeroEmpty;
                }));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_MIN_AMNT, 3)
                .Configure(d =>
                {
                    d.HeaderToolTip = "Минимальная сумма расхождения по СФ";
                    d.DataFormatInfo = formatInfoZeroEmpty;
                }));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_MAX_AMNT, 3)
                .Configure(d => { d.HeaderToolTip = "Максимальная сумма расхождения по СФ"; d.DataFormatInfo = formatInfoZeroEmpty; }));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.DISCREP_AVG_AMNT, 3)
                .Configure(d => { d.DataFormatInfo = formatInfoZeroEmpty; }));
            setup.Columns.Add(group);

            #endregion

            #region Сумма ПВП по книгам

            group = helper.CreateGroup("Сумма ПВП по книгам");
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_BUY_BOOK_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_SELL_BOOK_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            setup.Columns.Add(group);

            #endregion

            #region Сумма ПВП

            group = helper.CreateGroup("Сумма ПВП");
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_TOTAL_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_DISCREP_MIN_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_DISCREP_MAX_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            group.Columns.Add(helper.CreateNumberColumn(o => o.PVP_DISCREP_AVG_AMNT, 3)
                .Configure(d => d.DataFormatInfo = formatInfoZeroEmpty));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.FULL_TAX_PERIOD, ColumnExpressionCreator.ColumnExpressionTaxPeriod(), 4)
                .Configure(d => { d.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateNumberColumn(o => o.CORRECTION_NUMBER, 4)
                .Configure(d =>
                {
                    d.Caption = "№ корр./ версии";
                    d.HeaderToolTip = "Номер корректировки декларации/версии журнала";
                }));
            setup.Columns.Add(helper.CreateTextColumn(o => o.SUBMISSION_DATE, 4)
                .Configure(d =>
                {
                    d.Caption = "Дата представления";
                    d.HeaderToolTip = "Дата представления в НО";
                }));

            #region Подписант

            group = helper.CreateGroup("Лицо, подписавшее декларацию");
            group.Columns.Add(helper.CreateTextColumn(o => o.SUBSCRIBER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "ФИО";
                    d.Width = 120;
                }));
            group.Columns.Add(helper.CreateTextColumn(o => o.PRPODP, 3)
                .Configure(d =>
                {
                    d.Caption = "Статус";
                    d.Width = 120;
                }));
            setup.Columns.Add(group);

            #endregion

            setup.Columns.Add(helper.CreateTextColumn(o => o.STATUS, 4)
                .Configure(d => d.Caption = "Статус КНП"));

            setup.Columns.Add(helper.CreateTextColumn(o => o.CATEGORY_RU, 4)
                .Configure(d =>
                {
                    d.Caption = "Крупнейший";
                    d.HeaderToolTip = "НП является крупнейшим";
                }).MakeHidden());

            var groupSono = helper.CreateGroup("Инспекция");
            groupSono.Columns.Add(helper.CreateTextColumn(o => o.SOUN_CODE, 2)
                .Configure(d => { d.Caption = "Код"; d.HeaderToolTip = "Код инспекции"; d.MinWidth = 70; d.Width = 70; }));
            groupSono.Columns.Add(helper.CreateTextColumn(o => o.SOUN_NAME, 2)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование инспекции";
                }));
            setup.Columns.Add(groupSono);

            var groupRegion = helper.CreateGroup("Регион");
            groupRegion.Columns.Add(helper.CreateTextColumn(o => o.REGION_CODE, 2)
                .Configure(d => { d.Caption = "Код"; d.HeaderToolTip = "Код региона"; d.MinWidth = 70; d.Width = 70; }));
            groupRegion.Columns.Add(helper.CreateTextColumn(o => o.REGION_NAME, 2)
                .Configure(d =>
                {
                    d.Caption = "Наименование";
                    d.HeaderToolTip = "Наименование региона";
                }));
            setup.Columns.Add(groupRegion);

            setup.Columns.Add(helper.CreateTextColumn(o => o.INSPECTOR, 4));

            return setup;
        }

        private void InitializeGrid()
        {
            grid
                 .WithPager(new DataGridPageNavigator(_pager))
                 .InitColumns(SetupGrid());
            grid.RegisterTableAddin(new ExpansionIndicatorRemover());
            grid.RowDoubleClicked += i => _presenter.OpenDeclarationCard((DeclarationSummary)i);
            grid.SelectRow += (sender, args) =>
                {
                    var citm = ((DataGridView)sender).GetCurrentItem<DeclarationSummary>();
                    _reportTreeButton.Enabled = _presenter.IsTreeRelationEligible(citm);

                    _presenter.RefreshEKP();
                };

            //aip Статусбар
            grid.PanelLoadingVisible = true;

            _presenter.InitializeGrid(grid, _pager);
            //grid.Visible = false;
        }

        private readonly PagerStateViewModel _pager = new PagerStateViewModel();

        
        #region Пред-фильтр

        private void InitializePreFilter()
        {
            if (_presenter.TaxPeriodModel != null)
            {
                comboTaxPeriod.DataSource = _presenter.TaxPeriodModel.TaxPeriods;
                SetTaxPeriodInitialValue();
            }
            else
                comboTaxPeriod.DataSource = null;
            comboTaxPeriod.Refresh();
            comboTaxPeriod.ValueChanged += ComboTaxPeriod_ValueChanged;
        }
        
        private void SetTaxPeriodInitialValue()
        {
            if (_presenter.TaxPeriodModel.TaxPeriods.Count > 0)
            {
                string taxPeriodValueSettings = _presenter.SettingsProviderPredFilter.LoadSettings(comboTaxPeriod.Name);
                bool isFromSettingsValue = !string.IsNullOrWhiteSpace(taxPeriodValueSettings);
                comboTaxPeriod.SelectedIndex = -1;
                _selectedTaxPeriod = null;
                int index = 0;
                foreach (var item in _presenter.TaxPeriodModel.TaxPeriods)
                {
                    if ((isFromSettingsValue && item.Id.Equals(taxPeriodValueSettings)) ||
                        (!isFromSettingsValue && item.Id.Equals(_presenter.TaxPeriodModel.TaxPeriodDefault.Id)))
                    {
                        _selectedTaxPeriod = item;
                        comboTaxPeriod.SelectedIndex = index;
                        break;
                    }
                    index++;
                }
            }
            else
                comboTaxPeriod.SelectedIndex = -1;
        }

        private TaxPeriodBase _selectedTaxPeriod;

        public TaxPeriodBase SelectedTaxPeriod
        {
            get
            {
                return _selectedTaxPeriod;
            }
        }

        private void ComboTaxPeriod_ValueChanged(object sender, EventArgs e)
        {
            TaxPeriodBase itemCurrent = null;
            if (comboTaxPeriod.SelectedIndex > -1)
            {
                itemCurrent = (TaxPeriodBase)comboTaxPeriod.SelectedItem.ListObject;
                _presenter.SettingsProviderPredFilter.SaveSettings(itemCurrent.Id, comboTaxPeriod.Name);
                _pager.FirstPage();
            }
            _selectedTaxPeriod = itemCurrent;
            _presenter.GridUpdate();
        }

        #endregion
        

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = "Функции",
                ToolTipText = "Функции",
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnUpdate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Update", "cmdDeclarationList" + "Update")
            {
                Text = "Обновить",
                ToolTipText = "Обновить список выборок",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            var btnOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Open", "cmdDeclarationList" + "Open")
            {
                Text = "Открыть",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = true
            };

            var btnReportTreeRelations = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "ReportTreeRelations", "cmdDeclarationList" + "ReportTreeRelations")
            {
                Text = "Дерево связей",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "tree",
                SmallImageName = "tree",
                Enabled = false
            };

            _reportTreeButton = btnReportTreeRelations;

            var btnTaxPayerCard = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "TaxPayerCard", "cmdDeclarationList" + "TaxPayerCard")
            {
                Text = "Карточка НП",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = _presenter.IsUserEligibleForOperation(Constants.SystemPermissions.Operations.TaxPayerViewCard)
            };



            groupNavigation1.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(btnOpen.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnReportTreeRelations.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnTaxPayerCard.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnOpen, 
                btnReportTreeRelations,
                btnUpdate,
                btnTaxPayerCard,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;

        }

        private UcRibbonButtonToolContext _reportTreeButton;

        [CommandHandler("cmdDeclarationListUpdate")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            //grid.Visible = true;

            //gbFilter.Expanded = false;
            //aip Статусбар
            grid.PanelLoadingVisible = true;
            _presenter.GridUpdate();
        }

        [CommandHandler("cmdDeclarationListOpen")]
        public virtual void ViewBtnClick(object sender, EventArgs e)
        {
            var curItem = grid.GetCurrentItem<DeclarationSummary>();

            if (curItem != null)
                _presenter.OpenDeclarationCard(curItem);
        }


        [CommandHandler("cmdDeclarationList" + "ReportTreeRelations")]
        public virtual void ReportTreeRelationsBtnClick(object sender, EventArgs e)
        {
            var curItem = grid.GetCurrentItem<DeclarationSummary>();

            if (curItem != null)
                _presenter.ViewPyramidReport(curItem);
        }

        [CommandHandler("cmdDeclarationListTaxPayerCard")]
        public virtual void TaxPayerCardBtnClick(object sender, EventArgs e)
        {
            var currentItem = grid.GetCurrentItem<DeclarationSummary>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppEffective(currentItem.INN, currentItem.KPP_EFFECTIVE);
        }

        private void mainContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var currentItem = grid.GetCurrentItem<DeclarationSummary>();

            if (currentItem != null)
            {
                cmiOpenDeclCard.Visible = true;
                cmiOpenDeclCard.Text = currentItem.TypeCode() == DeclarationTypeCode.Declaration ? "Открыть декларацию" : "Открыть журнал";

                cmiOpenTaxPayerCard.Visible = true;
                cmiOpenReportTree.Visible = _presenter.IsTreeRelationEligible(currentItem);
            }
            else
            {
                cmiOpenDeclCard.Visible = false;
                cmiOpenTaxPayerCard.Visible = false;
                cmiOpenReportTree.Visible = false;
            }
        }

        private void cmiOpenDeclCard_Click(object sender, EventArgs e)
        {
            var currentItem = grid.GetCurrentItem<DeclarationSummary>();

            if (currentItem != null)
                _presenter.OpenDeclarationCard(currentItem);
        }

        private void cmiOpenTaxPayerCard_Click(object sender, EventArgs e)
        {
            var currentItem = grid.GetCurrentItem<DeclarationSummary>();

            if (currentItem != null)
                _presenter.ViewTaxPayerByKppEffective(currentItem.INN, currentItem.KPP_EFFECTIVE);
        }

        private void cmiOpenReportTree_Click(object sender, EventArgs e)
        {
            var currentItem = grid.GetCurrentItem<DeclarationSummary>();

            if (currentItem != null)
                _presenter.ViewPyramidReport(currentItem);
        }
    }
}
