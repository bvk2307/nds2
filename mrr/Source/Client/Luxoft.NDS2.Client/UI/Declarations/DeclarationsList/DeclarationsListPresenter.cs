﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.TransformQueryCondition;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationsList
{
    public class DeclarationsListPresenter : BasePresenter<View>
    {
        private IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy = null;

        private DataGridPresenter<DeclarationSummary> _gridPresenter;

        private IDeclarationCardOpener _declarationCardOpener;

        private ISettingsProvider _settingsProviderPredFilter;

        public void InitializeGrid(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> { new ColumnSort
                {
                    ColumnKey = TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBMISSION_DATE), 
                    Order = ColumnSort.SortOrder.Asc
                } }
            };

            _gridPresenter =
                new PagedDataGridPresenter<DeclarationSummary>(
                    grid,
                    pager,
                    GetDatas,
                    conditions);

            _gridPresenter.Init(SettingsProvider(string.Format("{0}_InitializeGrid", GetType())));

            GridUpdate();
        }

        public ISettingsProvider SettingsProviderPredFilter
        {
            get {
                return _settingsProviderPredFilter ??
                       (_settingsProviderPredFilter =
                           SettingsProvider(string.Format("{0}_SettingsProviderPredFilter", GetType())));
            }
        }

        public void GridUpdate()
        {
            _gridPresenter.Load();
        }
        
        public void OpenDeclarationCard(DeclarationSummary declaration)
        {
            _declarationCardOpener = _declarationCardOpener ?? GetDeclarationCardOpener();
            var result = _declarationCardOpener.Open(declaration);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public PageResult<DeclarationSummary> GetDatas(QueryConditions conditions)
        {
            var blService = GetServiceProxy<IDeclarationsDataService>();

            var transformerQueryConditions = TransformQueryConditionCreator.CreateTransformQueryCondition(conditions);
            transformerQueryConditions.AddFilterTaxPeriod(
                    View.SelectedTaxPeriod,
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FullTaxPeriod),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            var conditionsTransform = transformerQueryConditions.GetResultQueryConditions();

            var ret = new PageResult<DeclarationSummary>();
            if (conditions.FilterValid())
            {
                ExecuteServiceCall(
                    () => blService.SelectDeclarations(conditionsTransform),
                    result =>
                    {
                        ret = result.Result;
                    });
            }
            return ret;
        }

        public bool IsTreeRelationEligible( DeclarationSummary declaration )
        {
            if ( _declarationUserPermissionsPolicy == null )
            {
                ISecurityService securityService = GetServiceProxy<ISecurityService>();
                _declarationUserPermissionsPolicy = new DeclarationUserPermissionsPolicy<View>( this, securityService ) ;
            }
            return _declarationUserPermissionsPolicy.IsTreeRelationEligible( declaration );
        }
    }
}
