﻿namespace Luxoft.NDS2.Client.UI.Declarations.DeclarationsList
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenDeclCard = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenTaxPayerCard = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenReportTree = new System.Windows.Forms.ToolStripMenuItem();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.panelTaxPeriod = new Infragistics.Win.Misc.UltraPanel();
            this.labelTaxPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.comboTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.mainContextMenu.SuspendLayout();
            this.panelTaxPeriod.ClientArea.SuspendLayout();
            this.panelTaxPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).BeginInit();
            this.SuspendLayout();
            // 
            // mainContextMenu
            // 
            this.mainContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenDeclCard,
            this.cmiOpenTaxPayerCard,
            this.cmiOpenReportTree});
            this.mainContextMenu.Name = "mainContextMenu";
            this.mainContextMenu.Size = new System.Drawing.Size(291, 70);
            this.mainContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.mainContextMenu_Opening);
            // 
            // cmiOpenDeclCard
            // 
            this.cmiOpenDeclCard.Name = "cmiOpenDeclCard";
            this.cmiOpenDeclCard.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenDeclCard.Text = "Открыть декларацию/журнал";
            this.cmiOpenDeclCard.Click += new System.EventHandler(this.cmiOpenDeclCard_Click);
            // 
            // cmiOpenTaxPayerCard
            // 
            this.cmiOpenTaxPayerCard.Name = "cmiOpenTaxPayerCard";
            this.cmiOpenTaxPayerCard.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenTaxPayerCard.Text = "Открыть карточку налогоплательщика";
            this.cmiOpenTaxPayerCard.Click += new System.EventHandler(this.cmiOpenTaxPayerCard_Click);
            // 
            // cmiOpenReportTree
            // 
            this.cmiOpenReportTree.Name = "cmiOpenReportTree";
            this.cmiOpenReportTree.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenReportTree.Text = "Открыть дерево связей по декларации";
            this.cmiOpenReportTree.Click += new System.EventHandler(this.cmiOpenReportTree_Click);
            // 
            // grid
            // 
            this.grid.AggregatePanelVisible = true;
            this.grid.AllowMultiGrouping = true;
            this.grid.AllowResetSettings = false;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.FooterVisible = true;
            this.grid.GridContextMenuStrip = this.mainContextMenu;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = true;
            this.grid.RowDoubleClicked = null;
            this.grid.Size = new System.Drawing.Size(655, 469);
            this.grid.TabIndex = 1;
            this.grid.Title = "";
            // 
            // panelTaxPeriod
            // 
            // 
            // panelTaxPeriod.ClientArea
            // 
            this.panelTaxPeriod.ClientArea.Controls.Add(this.labelTaxPeriod);
            this.panelTaxPeriod.ClientArea.Controls.Add(this.comboTaxPeriod);
            this.panelTaxPeriod.Location = new System.Drawing.Point(1, 0);
            this.panelTaxPeriod.Name = "panelTaxPeriod";
            this.panelTaxPeriod.Size = new System.Drawing.Size(242, 30);
            this.panelTaxPeriod.TabIndex = 2;
            // 
            // labelTaxPeriod
            // 
            this.labelTaxPeriod.Location = new System.Drawing.Point(7, 6);
            this.labelTaxPeriod.Name = "labelTaxPeriod";
            this.labelTaxPeriod.Size = new System.Drawing.Size(99, 17);
            this.labelTaxPeriod.TabIndex = 26;
            this.labelTaxPeriod.Text = "отчетный период:";
            // 
            // comboTaxPeriod
            // 
            this.comboTaxPeriod.DisplayMember = "DESCRIPTION";
            this.comboTaxPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTaxPeriod.Location = new System.Drawing.Point(110, 3);
            this.comboTaxPeriod.Name = "comboTaxPeriod";
            this.comboTaxPeriod.Size = new System.Drawing.Size(120, 21);
            this.comboTaxPeriod.TabIndex = 25;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panelTaxPeriod);
            this.Controls.Add(this.grid);
            this.Name = "View";
            this.Size = new System.Drawing.Size(655, 469);
            this.Load += new System.EventHandler(this.View_Load);
            this.mainContextMenu.ResumeLayout(false);
            this.panelTaxPeriod.ClientArea.ResumeLayout(false);
            this.panelTaxPeriod.ClientArea.PerformLayout();
            this.panelTaxPeriod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mainContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclCard;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTaxPayerCard;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenReportTree;
        private Controls.Grid.V2.DataGridView grid;
        private Infragistics.Win.Misc.UltraPanel panelTaxPeriod;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTaxPeriod;
    }
}
