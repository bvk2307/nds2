﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Extensions;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Declarations
{
    /// <summary> User perimissions policy for a declaration. </summary>
    /// <typeparam name="TV"> A view type. It isn't used actually here only a presenter is used. </typeparam>
    public sealed class DeclarationUserPermissionsPolicy<TV> : IDeclarationUserPermissionsPolicy where TV : IViewBase
    {
        private const string DeclarationRelationTreeBuild =
            Constants.SystemPermissions.Operations.DeclarationTreeRelation;

        private readonly BasePresenter<TV> _presenter;
        private readonly IUserPermissionRequestService _userPermissionRequestService;

        public DeclarationUserPermissionsPolicy( BasePresenter<TV> presenter, IUserPermissionRequestService userPermissionRequestService )
        {
            Contract.Requires( presenter != null );
            Contract.Requires( userPermissionRequestService != null );

            _presenter = presenter;
            _userPermissionRequestService = userPermissionRequestService;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationBrief"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( DeclarationBrief declarationBrief )
        {
            Contract.Requires( declarationBrief != null );

            OperationResult<UserPermissionRequestResult> result = 
                _userPermissionRequestService.IsOperationEligibleByUserAccess( DeclarationRelationTreeBuild, declarationBrief );

            return result.Status == ResultStatus.Success && result.Result == UserPermissionRequestResult.Granted;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationSummary"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( DeclarationSummary declarationSummary )
        {
            Contract.Requires( declarationSummary != null );

            OperationResult<UserPermissionRequestResult> result = 
                _userPermissionRequestService.IsOperationEligibleByUserAccess( DeclarationRelationTreeBuild, declarationSummary );

            return result.Status == ResultStatus.Success && result.Result == UserPermissionRequestResult.Granted;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( string sounCode )
        {
            Contract.Requires( sounCode != null );

            OperationResult<UserPermissionRequestResult> result = 
                _userPermissionRequestService.IsOperationEligibleByUserAccess( DeclarationRelationTreeBuild, sounCode );

            return result.Status == ResultStatus.Success && result.Result == UserPermissionRequestResult.Granted;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="taxPayerId"></param>
        /// <param name="period"> Optional. Checks for all periods if it is 'null' (by default). </param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( TaxPayerId taxPayerId, Period period = null )
        {
            Contract.Requires( taxPayerId != null );

            OperationResult<UserPermissionRequestResult> result = 
                _userPermissionRequestService.IsOperationEligibleByUserAccess( DeclarationRelationTreeBuild, taxPayerId, period );

            return result.Status == ResultStatus.Success && result.Result == UserPermissionRequestResult.Granted;
        }

        /// <summary> Is All tree node report of Dependency Tree allowable for the current user? </summary>
        /// <returns></returns>
        public bool IsTreeRelationAllTreeReportEligible()
        {
            string[] roles =
            {
                Constants.SystemPermissions.Operations.RoleAnalyst, 
                Constants.SystemPermissions.Operations.RoleMedodologist, 
                Constants.SystemPermissions.Operations.RoleManager, 
                Constants.SystemPermissions.Operations.RoleObserver
            };
            string[] structContexts =
            {
                Constants.SystemPermissions.CentralDepartmentCode, 
                Constants.SystemPermissions.MIKKCode
            };
            string operation = DeclarationRelationTreeBuild;

            return _presenter.IsUserEligibleForRole(roles) 
                && _presenter.IsUserEligibleForOperation(operation) 
                && _presenter.UserOperations
                .Where(v => string.Equals(v.Name, operation, Constants.KeysComparison))
                .Any(v => structContexts.Any(vv => string.Equals(v.StructContext, vv, Constants.KeysComparison)));
        }

        public int? GetTreeRelationMaxLayersLimit( bool isByPurchase )
        {
            int? maxLayersLimit = null;
            ContractorDataPolicy restrictions = _presenter.UserRestrictionConfig;
            if ( !restrictions.NotRestricted )
                maxLayersLimit = isByPurchase ? restrictions.MaxLevelPurchase : restrictions.MaxLevelSales;
            return maxLayersLimit;
        }

        private bool IsRoleAndOrganizationEligible( ICollection<string> operationPermissions, string operationRole, ICollection<string> organizationCodes )
        {
            bool isEligible = IsRole( operationRole ) 
                            && organizationCodes.Any( orgCode => IsUserEligibleForOrganization( orgCode, operationPermissions ) );

            return isEligible;
        }

        private bool IsUserEligibleForDeclarationTreeRelation( 
            DeclarationType? declarationSignType, string organizationCode, string kppCode )
        {
            Contract.Requires( organizationCode != null );  //empty code defines that declaration is not provided yet

            bool isEligible = false;
            if ( organizationCode != string.Empty ) //"not provided" declaration is not allowed
            {
                string[] operationPermissions =
                ( from right in _presenter.UserOperations
                  where string.Equals( right.Name, DeclarationRelationTreeBuild, Constants.KeysComparison )
                  select right.StructContext ).ToArray();
                if ( operationPermissions.Length > 0 )
                {
                    isEligible = IsRole( Constants.SystemPermissions.Operations.RoleMedodologist );
                    if ( !isEligible )
                    {
                        isEligible = IsRoleAndOrganizationEligible( operationPermissions, Constants.SystemPermissions.Operations.RoleAnalyst,
                                        new []{ Constants.SystemPermissions.CentralDepartmentCode, Constants.SystemPermissions.MIKKCode } );
                        if ( !isEligible )
                        {
                            isEligible = IsRoleAndOrganizationEligible( operationPermissions, Constants.SystemPermissions.Operations.RoleApprover,
                                        new []{ Constants.SystemPermissions.MIKKCode } );
                            if ( !isEligible )
                            {
                                if ( IsRole( Constants.SystemPermissions.Operations.RoleInspector ) )
                                     isEligible = IsInspectorEligibleForOrganization( declarationSignType, organizationCode, operationPermissions, kppCode );
                            }
                        }
                    }
                }
            }
            return isEligible;
        }

        private static bool IsInspectorEligibleForOrganization( 
            DeclarationType? declarationSignType, string organizationCode, ICollection<string> operationOrgCodes, string kppCode )
        {
            bool isEligible = IsUserEligibleForOrganization( organizationCode, operationOrgCodes );
            if ( isEligible )
            {
                InspectionGroup? inspectionGroup = InspectionGroupHelper.GetInspectionGroupByOrganizationCode( organizationCode );

                //changed by request from Natalya Volkova on 14.1.2016
                isEligible = inspectionGroup.HasValue &&                //IFNS doesn't have permission for any declarations now including owned itself
                    inspectionGroup.Value != InspectionGroup.CA         //CA doesn't have permission for any declarations now including owned itself
                    && inspectionGroup.Value != InspectionGroup.MIKK;   //MIKK doesn't have permission for any declarations including owned itself
                //isEligible = !inspectionGroup.HasValue                          //IFNS
                //             || inspectionGroup.Value != InspectionGroup.MIKK;  //MIKK doesn't have permission for any declarations including owned itself
            }
            if ( !isEligible )
            {
                foreach ( string orgCode in operationOrgCodes.Where( 
                    orgCode => !string.Equals( organizationCode, orgCode, Constants.KeysComparison ) ) )
                {
                    InspectionGroup? inspectionGroup = InspectionGroupHelper.GetInspectionGroupByOrganizationCode( orgCode );
                    if ( inspectionGroup.HasValue )
                        isEligible = IsUserEligibleForInspectionGroup(
                            organizationCode, inspectionGroup.Value, inspectionGroupCode: orgCode, kppCode: kppCode );
                    //changed by request from Natalya Volkova on 14.1.2016
                    //IFNS doesn't have permission for any declarations now including owned itself
                    //else //IFNS
                    //    isEligible = declarationSignType.HasValue && declarationSignType.Value == DeclarationType.ToCharge;
                    if ( isEligible )
                        break;
                }
            }
            return isEligible;
        }

        private static bool IsUserEligibleForInspectionGroup( 
            string organizationCode, InspectionGroup inspectionGroup, string inspectionGroupCode, string kppCode )
        {
            bool isEligible = false;

            //changed by request from Natalya Volkova on 14.1.2016
            //CA doesn't have permission for any declarations now including owned itself
            //if ( inspectionGroup == InspectionGroup.CA )
            //{   
            //    isEligible = true;
            //}
            //else 
            if ( inspectionGroup == InspectionGroup.MILarge )
            {
                isEligible = string.Equals( inspectionGroupCode, organizationCode, Constants.KeysComparison )
                             && KppCodeHelper.IsLargeTaxPayerByKpp( kppCode );  
                //apopov 15.12.2015	//TODO!!! no requirements yet
            }
            else if ( inspectionGroup == InspectionGroup.UFNS )
            {
                isEligible = InspectionGroupHelper.IsInspectionGroupIncludingOrganizationCode( organizationCode, inspectionGroup, inspectionGroupCode );
                //apopov 15.12.2015	//TODO!!! no requirements yet
            }
            //IFNS is allowed above in IsInspectorEligibleForOrganization()
            //others do not have acccess

            return isEligible;
        }

        private bool IsSingleInspectorRole()
        {
            var nonLimitedRoles = new[]
            {
               Constants.SystemPermissions.Operations.RoleAnalyst,
               Constants.SystemPermissions.Operations.RoleApprover,
               Constants.SystemPermissions.Operations.RoleMedodologist
            };
            bool isSingleInspectorRole = !_presenter.IsUserEligibleForRole( nonLimitedRoles );
            if ( isSingleInspectorRole )
                isSingleInspectorRole = IsRole( Constants.SystemPermissions.Operations.RoleInspector );

            return isSingleInspectorRole;
        }

        private static bool IsUserEligibleForOrganization( string organizationCode, ICollection<string> operationOrgCodes )
        {
            bool isEligible = operationOrgCodes.Any(
                orgCode => string.Equals( organizationCode, orgCode, Constants.KeysComparison ) ); //does the user have the right in the organization?

            return isEligible;
        }

        private bool IsRole( string role )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( role ) );

            bool isRole = _presenter.IsUserEligibleForRole( new[] { role } );

            return isRole;
        }
    }
}