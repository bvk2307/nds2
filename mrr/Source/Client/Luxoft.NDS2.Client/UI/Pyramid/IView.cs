﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    /// <summary>
    /// Этот интерфейс описывает представление отчета "Пирамида"
    /// </summary>
    public interface IView
    {
        # region Дочерние представления

        string PeriodKey { set; }
        decimal? MinSum { get; }
        decimal? MinNdsPercent { get; }
        event ParameterlessEventHandler NewParamsAccepted;
        event GenericEventHandler<object> ContractorRelationsDoubleClicked;


        Controls.TreeViewer.IView ContractorTree { get; }
        Controls.Grid.V1.GridControl ContractorGrid { get; }

        # endregion
    }
}
