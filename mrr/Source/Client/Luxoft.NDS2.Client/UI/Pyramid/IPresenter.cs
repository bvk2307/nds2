﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public interface IPresenter
    {
        DictionarySur Sur
        {
            get;
        }

        GridSetup CommonSetup(string key);

        object GetDatas(QueryConditions conditions, out long totalRowsNumber);

        ContractorTreeElementModel GetCurrentContractor();
        long? GetLoadingKey();
    }
}
