﻿using Infragistics.Win.UltraWinTree;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.TreeViewer;
using Luxoft.NDS2.Client.UI.Pyramid.ContractorTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public class ContractorTreeDataStorage : 
        IKeyValueStorage<string, UltraTreeNode>,
        ITreeNodeStorage,
        IKeyValueStorage<long, ContractorTreeElementModel>
    {
        private readonly ConcurrentDictionary<string, UltraTreeNode> _viewStorage = 
            new ConcurrentDictionary<string, UltraTreeNode>();

        private readonly ConcurrentDictionary<string, TreeNodeBase> _modelStorage =
            new ConcurrentDictionary<string, TreeNodeBase>();

        private readonly ConcurrentDictionary<string, ContractorTreeElementModel> _dataStorage =
            new ConcurrentDictionary<string, ContractorTreeElementModel>();

        public void Push(string key, UltraTreeNode obj)
        {
            _viewStorage.TryAdd(key, obj);
        }

        public void Push(string key, TreeNodeBase obj)
        {
            _modelStorage.TryAdd(key, obj);
        }

        public void Push(long key, ContractorTreeElementModel obj)
        {
            _dataStorage.TryAdd(key.ToString(), obj);
        }

        public bool Pull(string key, out UltraTreeNode obj)
        {
            return _viewStorage.TryGetValue(key, out obj);
        }

        public bool Pull(string key, out TreeNodeBase obj)
        {
            return _modelStorage.TryGetValue(key, out obj);
        }

        public bool Pull(long key, out ContractorTreeElementModel obj)
        {
            return _dataStorage.TryGetValue(key.ToString(), out obj);
        }

        public TreeNodeBase[] PullChildren(string parentKey)
        {
            var result = new List<TreeNodeBase>();
            UltraTreeNode viewNode;

            if (_viewStorage.TryGetValue(parentKey, out viewNode))
            {
                foreach (var childViewNode in viewNode.Nodes)
                {
                    TreeNodeBase childModelNode;

                    if (_modelStorage.TryGetValue(childViewNode.Key, out childModelNode))
                    {
                        result.Add(childModelNode);
                    }
                }
            }

            return result.ToArray();
        }

        public void Release(string key)
        {
            UltraTreeNode viewNode;
            TreeNodeBase modelNode;
            ContractorTreeElementModel dataNode;

            if (_viewStorage.TryRemove(key, out viewNode))
            {
                foreach (var childViewNode in viewNode.Nodes)
                {
                    Release(childViewNode.Key);
                }

                _modelStorage.TryRemove(key, out modelNode);
                _dataStorage.TryRemove(key, out dataNode);
            }
        }

        public void Release(long key)
        {
            Release(key.ToString());
        }

        public void Clear()
        {
            _viewStorage.Clear();
            _modelStorage.Clear();
            _dataStorage.Clear();
        }
    }
}
