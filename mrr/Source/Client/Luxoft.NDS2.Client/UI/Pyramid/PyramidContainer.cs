﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Controls.TreeViewer;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public partial class PyramidContainer : BaseView, IView
    {
        public IPresenter presenter = null;

        private IntValueZeroFormatter formatPrices = new IntValueZeroFormatter();
        private SumNDSDeclarationFormatter formatNDSSum = new SumNDSDeclarationFormatter();

        public PyramidContainer()
        {
            InitializeComponent();
        }

        public PyramidContainer(PresentationContext context, WorkItem workItem, PyramidContainerData dt)
            : base(context, workItem)
        {
            InitializeComponent();

            var title = String.Format("{0} (Дерево связей)", dt.Inn);

            context.WindowTitle = title;
            context.WindowDescription = title;
            
            contractorTree.AddFormatter<PendingNode>(
            (node) =>
            {               
                node.LeftImages.Add(Properties.Resources.hourglass);
                node.LeftImagesAlignment = Infragistics.Win.VAlign.Middle;
            });

        }

        #region Иннициализация

        public void AcceptPresenter(IPresenter p)
        {
            presenter = p;

            InitializeContractorsGrid();
        }

        private void InitializeContractorsGrid()
        {
            Contract.Assert(presenter != null);

            var setup = presenter.CommonSetup(string.Format("{0}", GetType()));
            var helper = new GridSetupHelper<ContractorRelationshipsDetail>();

            setup.AllowSaveSettings = false;

            var rowIndex = new ColumnDefinition("index") {Caption = "№ п/п", IsAutoIncrement = true, RowSpan = 2, Align = ColumnAlign.Center};

            var g1 = new ColumnGroupDefinition() { Caption = "Сведения о контрагентах" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.BuyerName, d => d.Width = 250));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.SellerName, d => d.Width = 250));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.Level, d => d.Align = ColumnAlign.Center));

            var g2 = new ColumnGroupDefinition() { Caption = "По данным покупателя" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.DECL_NDS_8, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.DECL_NDS_8_WT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            
            var g3 = new ColumnGroupDefinition() { Caption = "Сведения о продавце" };
            g3.Columns.Add(
                helper.CreateColumnDefinition(
                    o => o.SYR, 
                    (column) => column.CustomConditionValueAppearance = () => this.DrawSurColumn(presenter.Sur)));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.DECL_PROVIDED));

            var g4 = new ColumnGroupDefinition() { Caption = "Сведения о расхождениях (по сумме НДС)" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_TOTAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_TOTAL_DEAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_TOTAL_DEAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            var g5 = new ColumnGroupDefinition() { Caption = "В том числе сведения о разрывах" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_GAP_TOTAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_GAP_TOTAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_GAP_DEAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_GAP_DEAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            var g6 = new ColumnGroupDefinition() { Caption = "В том числе сведения о неточных сопоставлениях" };
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_WEAK_TOTAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_WEAK_TOTAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_WEAK_DEAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_WEAK_DEAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            var g7 = new ColumnGroupDefinition() { Caption = "В том числе сведения о нарушениях НДС" };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_NDS_TOTAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_NDS_TOTAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_NDS_DEAL_COUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.DISCR_NDS_DEAL_SUM, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));

            var g8 = new ColumnGroupDefinition() { Caption = "Сведения о декларации продавца" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DECL_NDS_AMOUNT, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DECL_NDS_9_FOR_PAY, d => { d.FormatInfo = formatNDSSum; d.Align = ColumnAlign.Right; }));

            var g9 = new ColumnGroupDefinition() { Caption = "Сумма операций (включая НДС), руб." };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.SUM_RUB_MATCHED, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.SUM_RUB, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.SUM_RUB_MATCHED_2, d => { d.FormatInfo = formatPrices; d.Align = ColumnAlign.Right; }));
            
            setup.Columns.AddRange(new List<ColumnBase>() { rowIndex, g1, g2, g3, g4, g5, g6, g7, g8, g9 });
            setup.OrderColumns();

            setup.CellToolTips = new Dictionary<string, Func<object, string>>
                {
                    {
                        TypeHelper<ContractorRelationshipsDetail>.GetMemberName(t => t.SYR),
                        (dataObject) => presenter.Sur.Description(((ContractorRelationshipsDetail)dataObject).SYR)
                    }
                };
            setup.GetData = presenter.GetDatas;

            contractorGrid.Setup = setup;

            contractorGrid.AppendOptionsBuilder(
                TypeHelper<ContractorRelationshipsDetail>.GetMemberName(t => t.SYR), 
                new SurAutoCompleteOptionsBuilder(presenter.Sur));

            contractorGrid.RowDoubleClicked = item =>
            {
                if (ContractorRelationsDoubleClicked != null)
                {
                    ContractorRelationsDoubleClicked(item);
                }
            };

            contractorGrid.BeforeLoadData += (s, e) =>
            {
                contractorGrid.HideMessageInWorkplace();
            };

            contractorGrid.AfterLoadData += (s, e) =>
            {
                int size = contractorGrid.GetRows().Count;

                if ((size == 0) && (presenter != null))
                {
                    ContractorTreeElementModel currContractor = presenter.GetCurrentContractor();
                    long? loadingContractorKey = presenter.GetLoadingKey();

                    if ((currContractor != null) && currContractor.SelfRequestId.HasValue && (!(currContractor.Key == loadingContractorKey)))
                        contractorGrid.ShowMessageInWorkplace(ResourceManagerNDS2.RelationTreeTaxPayerNoData);                   
                }
            };
        }

        public void SetCacheStorage(ContractorTreeDataStorage storage)
        {
            contractorTree.SetNodeStorage(storage);
        }

        #endregion

        #region Реализация IView

        public string PeriodKey { set { periodView.Text = value; } }

        public decimal? MinSum
        {
            get
            {
                return
                    minSumEdit.Value == null
                        ? (decimal?)null
                        : decimal.Parse(
                            minSumEdit.Value.ToString(), CultureInfo.InvariantCulture);
            }
        }

        public decimal? MinNdsPercent
        {
            get
            {
                return
                    minNdsPercentEdit.Value == null
                        ? (decimal?)null
                        : decimal.Parse(
                            minNdsPercentEdit.Value.ToString(), CultureInfo.InvariantCulture);
            }
        }

        public event GenericEventHandler<object> ContractorRelationsDoubleClicked;

        public event ParameterlessEventHandler NewParamsAccepted;

        public Controls.TreeViewer.IView ContractorTree
        {
            get { return contractorTree; }
        }

        public GridControl ContractorGrid
        {
            get { return contractorGrid; }
        }

        #endregion

        #region Вспомогательные методы

        private void OpenTaxPayerMenuItem_Click(object sender, EventArgs e)
        {
            ContractorRelationsDoubleClicked(contractorGrid.GetCurrentItem<ContractorRelationshipsDetail>());
        }
        
        private void CheckNumericOnKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar);

            if (!e.Handled && sender is UltraTextEditor)
            {
                var s = sender as UltraTextEditor;

                if ((s.Name == "minNdsPercentEdit") && (char.IsDigit(e.KeyChar)))
                {
                    string rez = s.Text + e.KeyChar;
                    int r = int.Parse(rez);
                    e.Handled = !((r >= 1) && (r <= 100));
                }
            }
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            if (NewParamsAccepted != null)
                NewParamsAccepted();
        }

        #endregion

    }
}
