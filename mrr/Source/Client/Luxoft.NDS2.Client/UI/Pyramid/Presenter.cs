﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.TreeViewer;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
	//apopov 16.6.2016	//TODO!!!   //remove as old unsupported code
    public class Presenter : BasePresenter<PyramidContainer>, IDataLoader<long, ContractorTreeElementModel>, IPresenter
    {
        private readonly PyramidContainerData cardData;
        private ContractorTreeElementModel root;
        private readonly IPyramidDataServiceOld service;
        private readonly ContractorTree.Presenter treePresenter;
        private readonly ContractorTreeDataStorage cacheStorage;

        public Presenter(
            PyramidContainerData dt,
            PresentationContext context,
            WorkItem workItem,
            PyramidContainer view)
            : base(context, workItem, view)
        {
            cardData = dt;

            cacheStorage = new ContractorTreeDataStorage();

            service = GetServiceProxy<IPyramidDataServiceOld>();

            treePresenter = new ContractorTree.Presenter(
                View.ContractorTree,
                this,
                cacheStorage,
                ContractorTree.DepthRestrictionCreator.Restriction(UserRestrictionConfig, dt.IsPurchase),
                SelectedContractorChanged,
                ContractorLoadStarted,
                ContractorLoadCompleted,
                ContractorLoadFailed);

            View.AcceptPresenter(this);
            View.PeriodKey = dt.FullTaxPeriod;
            View.SetCacheStorage(cacheStorage);

            View.Load += (sender, args) => Init();
            View.NewParamsAccepted += AfterParamsAccepted;
            View.ContractorRelationsDoubleClicked += ContractorRelationsDoubleClick;
        }

        private void AfterParamsAccepted()
        {
            _currentContractorId = null;
            View.ContractorGrid.UpdateData();
            treePresenter.StartBuilding();             
        }

        private void ContractorRelationsDoubleClick(object item)
        {
            var details = item as ContractorRelationshipsDetail;

            if (details == null)
            {
                return;
            }

            ViewTaxPayerByKppOriginal(details.Inn, details.Kpp);
        }

        # region Инициализация

        private void Init()
        {
            if (!ExecuteServiceCall<OperationResult<Contractor>>(
                () => service.LoadRoot(cardData.Id.Value),
                (response) =>
                {
                    root = new ContractorTreeElementModel(response.Result);
                }))
            {
                return;
            }
        }

        # endregion

        # region Работа с таблицей связей

        private long? _currentContractorId;
        private int _currentLevel;
        private ContractorTreeElementModel _currentContractor = null;

        public void SelectedContractorChanged(long key, int lvl)
        {
            var contractor = Load(key);
            _currentContractor = contractor;

            View.ContractorGrid.UpdateData();            

            if (contractor != null)
            {
                _currentContractorId = key;
                _currentLevel = lvl;

                var asyncWorker = new AsyncWorker<bool>();

                asyncWorker.DoWork += (sender, args) =>
                {
                    var status = Status(contractor.Key);

                    View.ExecuteInUiThread(c =>
                    {
                        View.ContractorGrid.UpdateData();

                        if ((status != DataLoadingStatus.InProcess) && (!(key == _loadingKey)))
                        {
                            View.ContractorGrid.PanelLoadingVisible = false;
                        }
                    });
                };

                View.ContractorGrid.PanelLoadingVisible = true;
                asyncWorker.Start();
            }
        }

        public ContractorTreeElementModel GetCurrentContractor()
        {
            return _currentContractor;
        }


        private long? _loadingKey = null;

        public long? GetLoadingKey()
        {
            return _loadingKey;
        }

        public void ContractorLoadStarted(long key)
        {
            _loadingKey = key;
            
            if (_currentContractorId.HasValue
                && _currentContractorId.Value == key)
            {
                View.ContractorGrid.PanelLoadingVisible = true;
            }
        }

        public void ContractorLoadCompleted(long key)
        {
            _loadingKey = null;

            if (_currentContractorId.HasValue
                && _currentContractorId.Value == key)
            {
                View.ExecuteInUiThread(c =>
                {
                    View.ContractorGrid.UpdateData();
                    View.ContractorGrid.PanelLoadingVisible = false;
                });
            }
        }

        public void ContractorLoadFailed(long key, TypeViewMessage typeViewMessage, string message)
        {
            _loadingKey = null;
            
            if (_currentContractorId.HasValue
                && _currentContractorId.Value == key)
            {
                View.ExecuteInUiThread(c =>
                {
                    View.ContractorGrid.PanelLoadingVisible = false;
                    if (typeViewMessage == TypeViewMessage.ErrorMessage)
                    {
                        View.ShowError(message);
                    }
                    else if (typeViewMessage == TypeViewMessage.NotifyMessage)
                    {
                        View.ShowNotification(message);
                    }
                });
            }
        }

        public object GetDatas(QueryConditions conditions, out long totalRowsNumber)
        {
            var contractorId = _currentContractorId;
            totalRowsNumber = 0;
            var ret = new List<ContractorRelationshipsDetail>();

            if (!contractorId.HasValue)
            {
                return ret;
            }

            var contractor = Load(contractorId.Value);           
            var requestId = contractor == null ? (long?)null : contractor.SelfRequestId;

            if (!requestId.HasValue)
            {
                return ret;
            }
           

            var ndsKey = TypeHelper<ContractorRelationshipsDetail>.GetMemberName(x => x.DECL_NDS_8_WT);
            var sumKey = TypeHelper<ContractorRelationshipsDetail>.GetMemberName(x => x.SUM_RUB_MATCHED_2);

            foreach (var fq in conditions.Filter)
            {
                if ((fq.ColumnName == ndsKey) || (fq.ColumnName == sumKey))
                {
                    fq.Filtering.RemoveAll(filter => filter.ComparisonOperator == ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo);
                }
            }
            
            conditions.Filter.RemoveAll(x => x.Filtering.Count == 0);

            if (View.MinNdsPercent.HasValue)
            {
                conditions.Filter.Add(
                    new FilterQuery
                    {
                        ColumnName = ndsKey,
                        Filtering = 
                        {
                            new ColumnFilter 
                            { 
                                ComparisonOperator = 
                                    ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo, 
                                Value = View.MinNdsPercent.Value
                            }
                        }
                    });
            }

            if (View.MinSum.HasValue)
            {
                conditions.Filter.Add(
                    new FilterQuery 
                    {
                        ColumnName = sumKey, 
                        Filtering =
                        {
                            new ColumnFilter 
                            { 
                                ComparisonOperator = 
                                    ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo, 
                                    Value = View.MinSum
                            }
                        }
                    });
            }

            var tempRowNumber = 0;
            ExecuteServiceCall<OperationResult<PageResult<ContractorRelationshipsDetail>>>(
                () => service.LoadRelationships(requestId.Value, conditions),
                (result) =>
                {
                    tempRowNumber = result.Result.TotalMatches;
                    ret = result.Result.Rows;
                });

            totalRowsNumber = tempRowNumber;
            ret.ForEach(x =>
                {
                    x.BuyerName =
                        String.Format(
                            "{0} ({1}/{2})",
                            cardData.Name,
                            cardData.Inn,
                            cardData.Kpp);
                    x.Level = (_currentLevel + 1).ToString();
                });

            return ret;
        }

        # endregion

        # region Реализация интерфейса IDataLoader<long, ContractorTreeElementModel>

        public void StartLoading(long parentKey)
        {
            var contractor = Load(parentKey);

            if (contractor != null)
            {
                ExecuteServiceCall<OperationResult<long>>(
                    () => service.SubmitRequest(Request(contractor)),
                    (response) => contractor.SelfRequestId = response.Result);   
            }
        }

        public DataLoadingStatus Status(long parentKey)
        {
            var model = Load(parentKey);

            if (model == null)
            {
                return DataLoadingStatus.Error;
            }

            var status = DataLoadingStatus.NotStarted;

            if (model.SelfRequestId.HasValue)
            {
                ExecuteServiceCall<OperationResult<RequestStatusType>>(
                    () => service.RequestStatus(model.SelfRequestId.Value),
                    (response) => status = Convert(response.Result));
            }          

            return status;
        }

        public PageResult<ContractorTreeElementModel> Load(long parentKey, int offset, int pageSize)
        {
            var minSum = (int) (View.MinSum ?? -1);
            var minNdsPercent = (int) (View.MinNdsPercent ?? -1);
            
            var model = Load(parentKey);
            var result = 
                new PageResult<ContractorTreeElementModel>(new List<ContractorTreeElementModel>(), 0);

            if (model != null && model.SelfRequestId.HasValue)
            {
                ExecuteServiceCall<OperationResult<PageResult<Contractor>>>(
                    () => service.LoadContractorsPage(model.SelfRequestId.Value, offset, pageSize, minSum, minNdsPercent),
                    (response) =>
                    {
                        result = 
                            new PageResult<ContractorTreeElementModel>(
                                response.Result.Rows.Select(
                                    x => new ContractorTreeElementModel(x)).ToList(),
                                response.Result.TotalMatches);
                        foreach (var item in result.Rows)
                        {
                            cacheStorage.Push(item.Key, item);
                        }
                    });
            }

            return result;
        }

        public PageResult<ContractorTreeElementModel> Load(int offset, int pageSize)
        {
            ExecuteServiceCall<OperationResult<Contractor>>(
                () => service.LoadRoot(cardData.Id.Value),
                (response) =>
                {
                    root = new ContractorTreeElementModel(response.Result);
                });

            return root == null
                ? new PageResult<ContractorTreeElementModel>(
                    new List<ContractorTreeElementModel>(),
                    0)
                : new PageResult<ContractorTreeElementModel>(
                    new List<ContractorTreeElementModel>() 
                    { 
                        root
                    },
                    1);
        }

        # endregion

        # region Вспомогательные методы

        private Request Request(ContractorTreeElementModel contractor)
        {
            return new Request
            {
                Inn = contractor.Inn,
                Kpp = contractor.Kpp,
                Year = cardData.FiscalYear,
                Period = cardData.TaxPeriod,
                RequestType = 0
            };
        }

        private ContractorTreeElementModel Load(long key)
        {
            if (root.Key == key)
            {
                return root;
            }

            ContractorTreeElementModel model;
            if (cacheStorage.Pull(key, out model))
            {
                return model;
            }

            return null;
        }

        private DataLoadingStatus Convert(RequestStatusType status)
        {
            if (status == RequestStatusType.Ok)
            {
                return DataLoadingStatus.InProcess;
            }

            return (DataLoadingStatus)((int)status);
        }

        # endregion
    }
}
