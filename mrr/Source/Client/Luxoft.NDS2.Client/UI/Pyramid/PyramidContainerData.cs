﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public class PyramidContainerData
    {
        public long? Id { get; private set; }
        public string Inn { get; private set; }
        public string Kpp { get; private set; }
        public string Name { get; private set; }
        public string FullTaxPeriod { get; private set; }
        public string TaxPeriod { get; private set; }
        public string FiscalYear { get; private set; }
        public bool IsPurchase { get; private set; }
        public string ErrorMessage { get; private set; }
        public string WarningMessage { get; private set; }

        public PyramidContainerData(DeclarationSummary dt)
        {
            Id = dt.ID;
            Inn = dt.INN;
            Kpp = dt.KPP;
            Name = dt.NAME;
            FullTaxPeriod = dt.FULL_TAX_PERIOD;
            TaxPeriod = dt.TAX_PERIOD;
            FiscalYear = dt.FISCAL_YEAR;
            IsPurchase = dt.COMPENSATION_AMNT < 0;

            CreateMessges(dt.LOAD_MARK, dt.ProcessingStage);
        }

        public PyramidContainerData(DeclarationBrief dt)
        {
            Id = dt.ID;
            Inn = dt.INN;
            Kpp = dt.KPP;
            Name = dt.NAME;
            FullTaxPeriod = dt.FULL_TAX_PERIOD;
            TaxPeriod = dt.TAX_PERIOD.ToString();
            FiscalYear = dt.FISCAL_YEAR.ToString();
            IsPurchase = dt.COMPENSATION_AMNT.HasValue 
                && dt.COMPENSATION_AMNT.Value < 0;

            CreateMessges(dt.LOAD_MARK, dt.ProcessingStage);
        }

        private void CreateMessges(DeclarationProcessignStage allRevisionStage, DeclarationProcessignStage currentRevisionStage)
        {
            string valueBuffer;

            ErrorMessage = String.Empty;
            WarningMessage = String.Empty;

            var item = Tuple.Create(allRevisionStage, currentRevisionStage);
            
            var errorMessagesDictionary =
            new Dictionary<Tuple<DeclarationProcessignStage, DeclarationProcessignStage>, string>()
                {
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY }
                };

            if (errorMessagesDictionary.TryGetValue(item, out valueBuffer))
                ErrorMessage = valueBuffer;
               


            var warningMessagesDictionary =
            new Dictionary<Tuple<DeclarationProcessignStage, DeclarationProcessignStage>, string>()
                {
                    { Tuple.Create(DeclarationProcessignStage.ProcessedByMs, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.OPEN_RELATION_TREE_PARTIAL_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.ProcessedByMs, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.OPEN_RELATION_TREE_PARTIAL_NO_READY }
                };


            if (warningMessagesDictionary.TryGetValue(item, out valueBuffer))
                WarningMessage = valueBuffer;
        }


    }
}
