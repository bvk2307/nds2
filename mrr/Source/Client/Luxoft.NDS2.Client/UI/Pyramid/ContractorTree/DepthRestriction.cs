﻿namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    public class DepthRestriction : IDepthRestriction
    {
        private readonly int _maxLevel;

        public DepthRestriction(int maxLevel)
        {
            _maxLevel = maxLevel;
        }

        public bool IsRestricted(int level)
        {
            return _maxLevel <= level;
        }
    }
}
