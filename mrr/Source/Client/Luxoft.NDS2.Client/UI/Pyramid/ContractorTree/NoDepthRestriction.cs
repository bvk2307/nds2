﻿namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    public class NoDepthRestriction : IDepthRestriction
    {
        public bool IsRestricted(int level)
        {
            return false;
        }
    }
}
