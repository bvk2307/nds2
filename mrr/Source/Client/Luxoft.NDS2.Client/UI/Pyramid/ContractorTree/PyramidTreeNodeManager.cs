﻿using Luxoft.NDS2.Client.UI.Controls.TreeViewer;

namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    public class PyramidTreeNodeManager : TreeNodeManager<long, ContractorTreeElementModel>
    {
        public PyramidTreeNodeManager(
            ITreeNodeStorage storage,
            int pageSize)
            : base(storage, pageSize)
        {
        }

        protected override string PendingNodeTitle()
        {
            return "Загрузка контрагентов...";
        }
    }
}
