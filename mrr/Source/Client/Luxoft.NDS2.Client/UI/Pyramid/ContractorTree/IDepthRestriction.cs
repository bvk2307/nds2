﻿namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    public interface IDepthRestriction
    {
        bool IsRestricted(int level);
    }
}
