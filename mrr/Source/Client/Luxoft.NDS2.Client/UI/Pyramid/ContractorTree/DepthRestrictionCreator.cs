﻿using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    public static class DepthRestrictionCreator
    {
        public static IDepthRestriction Restriction(this ContractorDataPolicy config, bool isPurchase)
        {
            return config.NotRestricted 
                ? (IDepthRestriction)new NoDepthRestriction() 
                : new DepthRestriction(isPurchase ? config.MaxLevelPurchase : config.MaxLevelSales);
        }
    }
}
