﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Controls.TreeViewer;
using System;

namespace Luxoft.NDS2.Client.UI.Pyramid.ContractorTree
{
    /// <summary>
    /// Этот класс описывает реализацию презентера дерева контрагентов отчета "Пирамида"
    /// </summary>
    public class Presenter : Presenter<long, ContractorTreeElementModel>
    {
        # region Конструкторы

        private readonly Action<long> _waitStarted;

        private readonly Action<long> _waitCompleted;

        private readonly Action<long, TypeViewMessage, string> _waitFailed;

        private readonly IDepthRestriction _depth;

        public Presenter(
            Controls.TreeViewer.IView view,
            IDataLoader<long, ContractorTreeElementModel> dataLoader,
            ITreeNodeStorage nodeStorage,
            IDepthRestriction depth,
            Action<long, int> selectCallBack,
            Action<long> waitStarted,
            Action<long> waitCompleted,
            Action<long, TypeViewMessage, string> waitFailed)
            : base(
                view, 
                dataLoader, 
                new PyramidTreeNodeManager(nodeStorage, view.PageSize), 
                (str) => long.Parse(str), 
                AutoStartMode.None)
        {
            _waitStarted = waitStarted;
            _waitCompleted = waitCompleted;
            _waitFailed = waitFailed;
            _depth = depth;
            view.SelectedNodeChanged +=
                (key, level) =>
                {
                    var contractorNode = NodeManager.GetNode<DataNode>(key);

                    if (contractorNode != null)
                    {
                        selectCallBack(long.Parse(contractorNode.DataItemKey), level);
                    }
                };
        }

        # endregion

        # region Перегрузки базовых методов

        protected override void NodeVisible(TreeNodeBase node)
        {
            var dataNode = node as DataNode;

            if (dataNode != null && !_depth.IsRestricted(dataNode.Level))
            {
                LoadDataAsync((DataNode)node);
            }
        }

        protected override void NodeAdded(TreeNodeBase node)
        {
            var dataNode = node as DataNode;

            if (dataNode == null)
            {
                base.NodeAdded(node);
                return;
            }

            if (!_depth.IsRestricted(dataNode.Level))
            {
                base.NodeAdded(node);

                if (dataNode.IsRoot)
                {
                    StartLoadingContent(dataNode);
                    View.SelectNode(dataNode.Key);
                }                
            }
        }

        protected override void WaitStarted(DataNode node)
        {
            if (_waitStarted != null)
                _waitStarted(long.Parse(node.DataItemKey));
        }

        protected override void WaitCompleted(DataNode node)
        {
            if (node.LoadingStatus == DataLoadingStatus.Completed)
            {
                _waitCompleted(long.Parse(node.DataItemKey));
            }
            if (node.LoadingStatus == DataLoadingStatus.NotSuccessInNightMode)
            {
                _waitFailed(long.Parse(node.DataItemKey), TypeViewMessage.NotifyMessage, ResourceManagerNDS2.SovNotSuccessInNightModeMessage);
            }
            if (node.LoadingStatus == DataLoadingStatus.Error ||
                node.LoadingStatus == DataLoadingStatus.NotRegistered)
            {
                string message = ResourceManagerNDS2.SovError;
                if (node.LoadingStatus == DataLoadingStatus.NotRegistered)
                {
                    message = ResourceManagerNDS2.SovNotRegistered;
                }
                _waitFailed(long.Parse(node.DataItemKey), TypeViewMessage.ErrorMessage, message);
            }
        }

        # endregion       
    }
}
