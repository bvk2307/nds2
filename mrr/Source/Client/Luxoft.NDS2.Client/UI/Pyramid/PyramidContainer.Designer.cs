﻿namespace Luxoft.NDS2.Client.UI.Pyramid
{
    partial class PyramidContainer
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraExpandableGroupBox _parametersGroupBox;
            Infragistics.Win.Misc.UltraExpandableGroupBoxPanel _parametersPanel;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _mappedAmountLabel;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel2;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _notMatchedAmountLabel;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _periodLabel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.minNdsPercentEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.minSumEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.periodView = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.acceptButton = new Infragistics.Win.Misc.UltraButton();
            this.contractorTree = new Luxoft.NDS2.Client.UI.Controls.TreeViewer.AsyncTreeViewer();
            this.SP = new System.Windows.Forms.Splitter();
            this.contractorGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.gridContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenTaxPayerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _parametersGroupBox = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            _parametersPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            _mappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            _notMatchedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _periodLabel = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(_parametersGroupBox)).BeginInit();
            _parametersGroupBox.SuspendLayout();
            _parametersPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minNdsPercentEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minSumEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodView)).BeginInit();
            this.gridContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _parametersGroupBox
            // 
            _parametersGroupBox.Controls.Add(_parametersPanel);
            _parametersGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            _parametersGroupBox.ExpandedSize = new System.Drawing.Size(898, 96);
            _parametersGroupBox.Location = new System.Drawing.Point(0, 0);
            _parametersGroupBox.Name = "_parametersGroupBox";
            _parametersGroupBox.Size = new System.Drawing.Size(898, 96);
            _parametersGroupBox.TabIndex = 0;
            _parametersGroupBox.Text = "Параметры";
            // 
            // _parametersPanel
            // 
            _parametersPanel.AutoSize = true;
            _parametersPanel.Controls.Add(this.minNdsPercentEdit);
            _parametersPanel.Controls.Add(this.minSumEdit);
            _parametersPanel.Controls.Add(this.periodView);
            _parametersPanel.Controls.Add(this.acceptButton);
            _parametersPanel.Controls.Add(_mappedAmountLabel);
            _parametersPanel.Controls.Add(ultraLabel2);
            _parametersPanel.Controls.Add(ultraLabel1);
            _parametersPanel.Controls.Add(_notMatchedAmountLabel);
            _parametersPanel.Controls.Add(_periodLabel);
            _parametersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _parametersPanel.Location = new System.Drawing.Point(3, 19);
            _parametersPanel.Name = "_parametersPanel";
            _parametersPanel.Padding = new System.Windows.Forms.Padding(5);
            _parametersPanel.Size = new System.Drawing.Size(892, 74);
            _parametersPanel.TabIndex = 0;
            // 
            // minNdsPercentEdit
            // 
            this.minNdsPercentEdit.Location = new System.Drawing.Point(625, 39);
            this.minNdsPercentEdit.Name = "minNdsPercentEdit";
            this.minNdsPercentEdit.Size = new System.Drawing.Size(113, 21);
            this.minNdsPercentEdit.TabIndex = 6;
            this.minNdsPercentEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumericOnKeyPress);
            // 
            // minSumEdit
            // 
            this.minSumEdit.Location = new System.Drawing.Point(625, 9);
            this.minSumEdit.Name = "minSumEdit";
            this.minSumEdit.Size = new System.Drawing.Size(113, 21);
            this.minSumEdit.TabIndex = 3;
            this.minSumEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumericOnKeyPress);
            // 
            // periodView
            // 
            this.periodView.Location = new System.Drawing.Point(123, 8);
            this.periodView.Multiline = true;
            this.periodView.Name = "periodView";
            this.periodView.ReadOnly = true;
            this.periodView.Size = new System.Drawing.Size(150, 34);
            this.periodView.TabIndex = 1;
            this.periodView.Text = "B";
            // 
            // acceptButton
            // 
            appearance7.FontData.BoldAsString = "True";
            this.acceptButton.Appearance = appearance7;
            this.acceptButton.Location = new System.Drawing.Point(786, 40);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(103, 23);
            this.acceptButton.TabIndex = 8;
            this.acceptButton.Text = "Применить";
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // _mappedAmountLabel
            // 
            appearance2.FontData.BoldAsString = "True";
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            _mappedAmountLabel.Appearance = appearance2;
            _mappedAmountLabel.Location = new System.Drawing.Point(308, 32);
            _mappedAmountLabel.Name = "_mappedAmountLabel";
            _mappedAmountLabel.Size = new System.Drawing.Size(324, 33);
            _mappedAmountLabel.TabIndex = 5;
            _mappedAmountLabel.Text = "Минимальный удельный вес суммы НДС, подлежащей вычету у покупателя, в общей сумме" +
    " НДС";
            // 
            // ultraLabel2
            // 
            appearance6.FontData.BoldAsString = "True";
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            ultraLabel2.Appearance = appearance6;
            ultraLabel2.Location = new System.Drawing.Point(744, 40);
            ultraLabel2.Name = "ultraLabel2";
            ultraLabel2.Size = new System.Drawing.Size(36, 21);
            ultraLabel2.TabIndex = 7;
            ultraLabel2.Text = "%";
            // 
            // ultraLabel1
            // 
            appearance5.FontData.BoldAsString = "True";
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            ultraLabel1.Appearance = appearance5;
            ultraLabel1.Location = new System.Drawing.Point(744, 9);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(36, 21);
            ultraLabel1.TabIndex = 4;
            ultraLabel1.Text = "руб.";
            // 
            // _notMatchedAmountLabel
            // 
            appearance4.FontData.BoldAsString = "True";
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            _notMatchedAmountLabel.Appearance = appearance4;
            _notMatchedAmountLabel.Location = new System.Drawing.Point(308, 7);
            _notMatchedAmountLabel.Name = "_notMatchedAmountLabel";
            _notMatchedAmountLabel.Size = new System.Drawing.Size(324, 21);
            _notMatchedAmountLabel.TabIndex = 2;
            _notMatchedAmountLabel.Text = "Минимальная сумма операций по сведениям продавца";
            // 
            // _periodLabel
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            _periodLabel.Appearance = appearance1;
            _periodLabel.Location = new System.Drawing.Point(7, 8);
            _periodLabel.Name = "_periodLabel";
            _periodLabel.Size = new System.Drawing.Size(125, 21);
            _periodLabel.TabIndex = 0;
            _periodLabel.Text = "Отчетный период";
            // 
            // contractorTree
            // 
            this.contractorTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.contractorTree.Location = new System.Drawing.Point(0, 96);
            this.contractorTree.Margin = new System.Windows.Forms.Padding(5);
            this.contractorTree.Name = "contractorTree";
            this.contractorTree.PageSize = 10;
            this.contractorTree.Size = new System.Drawing.Size(242, 353);
            this.contractorTree.TabIndex = 1;
            this.contractorTree.Title = "Дерево связей";
            // 
            // SP
            // 
            this.SP.Location = new System.Drawing.Point(242, 96);
            this.SP.Name = "SP";
            this.SP.Size = new System.Drawing.Size(3, 353);
            this.SP.TabIndex = 0;
            this.SP.TabStop = false;
            // 
            // contractorGrid
            // 
            this.contractorGrid.AddVirtualCheckColumn = false;
            this.contractorGrid.AggregatePanelVisible = false;
            this.contractorGrid.AllowMultiGrouping = true;
            this.contractorGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.contractorGrid.BackColor = System.Drawing.Color.Transparent;
            this.contractorGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.contractorGrid.ContextMenuStrip = this.gridContextMenu;
            this.contractorGrid.DefaultPageSize = "";
            this.contractorGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contractorGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.contractorGrid.FooterVisible = true;
            this.contractorGrid.GridContextMenuStrip = null;
            this.contractorGrid.Location = new System.Drawing.Point(245, 96);
            this.contractorGrid.Name = "contractorGrid";
            this.contractorGrid.PanelLoadingVisible = false;
            this.contractorGrid.PanelPagesVisible = false;
            this.contractorGrid.RowDoubleClicked = null;
            this.contractorGrid.Setup = null;
            this.contractorGrid.Size = new System.Drawing.Size(653, 353);
            this.contractorGrid.TabIndex = 2;
            this.contractorGrid.Title = "Информация по связям с контрагентами";
            // 
            // gridContextMenu
            // 
            this.gridContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenTaxPayerMenuItem});
            this.gridContextMenu.Name = "gridContextMenu";
            this.gridContextMenu.Size = new System.Drawing.Size(291, 26);
            // 
            // OpenTaxPayerMenuItem
            // 
            this.OpenTaxPayerMenuItem.Name = "OpenTaxPayerMenuItem";
            this.OpenTaxPayerMenuItem.Size = new System.Drawing.Size(290, 22);
            this.OpenTaxPayerMenuItem.Text = "Открыть карточку налогоплательщика";
            this.OpenTaxPayerMenuItem.Click += new System.EventHandler(this.OpenTaxPayerMenuItem_Click);
            // 
            // PyramidContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.contractorGrid);
            this.Controls.Add(this.SP);
            this.Controls.Add(this.contractorTree);
            this.Controls.Add(_parametersGroupBox);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "PyramidContainer";
            this.Size = new System.Drawing.Size(898, 449);
            ((System.ComponentModel.ISupportInitialize)(_parametersGroupBox)).EndInit();
            _parametersGroupBox.ResumeLayout(false);
            _parametersGroupBox.PerformLayout();
            _parametersPanel.ResumeLayout(false);
            _parametersPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minNdsPercentEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minSumEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodView)).EndInit();
            this.gridContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.TreeViewer.AsyncTreeViewer contractorTree;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor periodView;
        private Infragistics.Win.Misc.UltraButton acceptButton;
        private System.Windows.Forms.Splitter SP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor minSumEdit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor minNdsPercentEdit;
        private Controls.Grid.V1.GridControl contractorGrid;
        private System.Windows.Forms.ContextMenuStrip gridContextMenu;
        private System.Windows.Forms.ToolStripMenuItem OpenTaxPayerMenuItem;
    }
}
