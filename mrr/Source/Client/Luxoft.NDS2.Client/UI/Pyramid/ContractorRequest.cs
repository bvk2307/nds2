﻿using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public class ContractorRequest
    {
        public Contractor Contractor
        {
            get;
            set;
        }

        public Request Request
        {
            get;
            set;
        }
    }
}
