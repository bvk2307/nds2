﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.Pyramid
{
    public class ContractorTreeElementModel : IIdentifiableEntity<long>
    {
        public ContractorTreeElementModel(Contractor dto)
        {
            Key = dto.Id;
            Name = dto.Name;
            Inn = dto.Inn;
            Kpp = dto.Kpp;
            RequestId = dto.RequestId;
            SelfRequestId = dto.SelfRequestId;
        }

        public long Key
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Inn
        {
            get;
            set;
        }

        public string Kpp
        {
            get;
            set;
        }

        public long? RequestId
        {
            get;
            set;
        }

        public long? SelfRequestId
        {
            get;
            set;
        }

        public override string ToString()
        {
            return Name ?? string.Format("НП (ИНН {0}, КПП {1})", Inn, Kpp);
        }
    }
}
