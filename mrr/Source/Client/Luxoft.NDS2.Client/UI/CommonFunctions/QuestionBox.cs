﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.CommonFunctions
{
    public class QuestionBox : IQuestionBox
    {
        private readonly IUcMessageService _service;

        public QuestionBox(IUcMessageService service)
        {
            _service = service;
        }

        public bool Ask(string caption, string message)
        {
            return QuestionDialog(caption, message) == DialogResult.Yes;
        }

        public DialogResult QuestionDialog(string caption, string message)
        {
            var context =
                new MessageInfoContext(caption, message, MessageCategory.Question)
                {
                    Buttons = MessageBoxButtons.YesNo
                };
            return _service.ShowQuestion(context);
        }
    }
}
