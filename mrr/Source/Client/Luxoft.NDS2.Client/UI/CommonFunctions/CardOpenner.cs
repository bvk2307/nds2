﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CommonComponents.Communication;
//using CommonComponents.Uc.Infrastructure.Interface.Contexts;
//using CommonComponents.Uc.Infrastructure.Interface.Services;
//using Luxoft.NDS2.Client.UI.Base;
//using Luxoft.NDS2.Common.Contracts;
//using Luxoft.NDS2.Common.Contracts.DTO;
//using Luxoft.NDS2.Common.Contracts.DTO.Business.discrepancyDocument;
//using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
//using Luxoft.NDS2.Common.Contracts.Services;
//using Microsoft.Practices.CompositeUI;
//using tp = Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
//
//namespace Luxoft.NDS2.Client.UI.CommonFunctions
//{
//    public static class CardOpenner
//    {
//        public static void OpenDiscrepancyCard(WorkItem wi, long entityId)
//        {
//            var managementService = wi.Services.Get<IWindowsManagerService>();
//
//            Guid viewId = Guid.NewGuid();
//            Guid featureId = Guid.NewGuid();
//
//            var newWi = wi.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
//
//            var fc = new FeatureContextBase(featureId);
//            var pc = new PresentationContext(viewId, null, fc);
//
//            if (!managementService.Contains(viewId))
//            {
//                var title = string.Format("Просмотр расхождения {0}", string.Empty);
//
//                pc.WindowTitle = title;
//                pc.WindowDescription = title;
//
//                var searchView = managementService.AddNewSmartPart<Discrepancies.DiscrepancyCard.View>(newWi,
//                    viewId.ToString(), pc, newWi, entityId);
//                managementService.Show(pc, newWi, searchView);
//            }
//        }
//    }
//}