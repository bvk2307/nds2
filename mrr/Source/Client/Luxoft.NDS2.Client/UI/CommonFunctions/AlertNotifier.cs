﻿using CommonComponents.Instrumentation;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Luxoft.NDS2.Common.Contracts;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Drawing;
using FLS.CommonComponents.Lib.Execution;
using FLS.CommonComponents.Lib.Execution.Extensions;

namespace Luxoft.NDS2.Client.UI.CommonFunctions
{
    /// <summary> Shows common alerts and messages. </summary>
    public sealed class AlertNotifier : INotifier, IMessageInfoNotifier
    {
        private readonly IThreadInvoker _uiThreadInvoker;
        private readonly IInstrumentationService _instrumentationService;

        public AlertNotifier( IThreadInvoker uiThreadInvoker, IInstrumentationService instrumentationService )
        {
            if ( uiThreadInvoker == null )
                throw new ArgumentNullException( "uiThreadInvoker" );
            if ( instrumentationService == null )
                throw new ArgumentNullException( "instrumentationService" );
            Contract.EndContractBlock();

            _uiThreadInvoker        = uiThreadInvoker;
            _instrumentationService = instrumentationService;
        }

        public void ShowError( string message )
        {
            ShowNotificationDelayed( message, "Ошибка", MessageCategory.Error );

            LogEntry le = _instrumentationService.CreateLogEntry(
                new LogEntryTemplate( Constants.SubsystemId, 223, TraceEventType.Error, InstrumentationLevel.Normal ) );
            le.Message = message;
            le.Title = "NDS2 Standalone";
            le.MethodName = null;

            _instrumentationService.Write( le );
        }

        public void ShowWarning( string message )
        {
            ShowNotificationDelayed( message, "Предупреждение", MessageCategory.Exclamation );
        }

        public void ShowNotification( string message )
        {
            ShowNotificationDelayed( message, "Информация", MessageCategory.Info );
        }

        private void ShowNotificationDelayed( string message, string caption, MessageCategory category )
        {
            _uiThreadInvoker.BeginExecute(
                () => Show( new MessageInfoContext( caption, message, category ) ) );
        }

        public void Show( MessageInfoContext message )
        {
            System.Drawing.Icon defaultIcon = null;
            string defaultCaption = null;

            #region deafult icon and caption

            switch ( message.Category )
            {
                case MessageCategory.Info:
                    defaultIcon = SystemIcons.Information;
                    defaultCaption = ResourceManagerNDS2.Information;
                    break;
                case MessageCategory.Exclamation:
                    defaultIcon = SystemIcons.Exclamation;
                    defaultCaption = ResourceManagerNDS2.Warning;
                    break;
                case MessageCategory.Error:
                    defaultIcon = SystemIcons.Error;
                    defaultCaption = ResourceManagerNDS2.Error;
                    break;
                case MessageCategory.Question:
                    defaultIcon = SystemIcons.Question;
                    defaultCaption = ResourceManagerNDS2.Information;
                    break;
            }

            #endregion

            var showWindowInfo = new UltraDesktopAlertShowWindowInfo
            {
                Caption = !string.IsNullOrEmpty(message.Caption) ? message.Caption : defaultCaption,
                Sound = message.Sound,
                Image = message.Image ?? (defaultIcon != null ? defaultIcon.ToBitmap() : null),
                Key = Guid.NewGuid().ToString(),
                Text = string.IsNullOrWhiteSpace( message.Message ) ? message.Message : message.Message.Replace( "\n", "<br/>" )
            };

            var alert = CreateAlert();

            if ( message.AutoCloseDelay != TimeSpan.Zero )
            {
                var autoCloseDelay = (int)message.AutoCloseDelay.TotalMilliseconds;

                if ( autoCloseDelay < 0 )
                    autoCloseDelay = 0;

                if ( autoCloseDelay > 3600000 )
                    autoCloseDelay = 3600000;

                alert.AutoCloseDelay = autoCloseDelay;
            }
            else
            {
                alert.AutoCloseDelay = 8000;
            }

            //var messageContextHelper = new DataContextHelper(message);
            //Guid smartPartId;
            //messageContextHelper.TryGetContextItemValue( "smartPartId", out smartPartId );

            //var handler = new AlertLinkClickHandler(this.workItem, smartPartId, alert);
            //handler.BindEventHandlers();

            alert.Show( showWindowInfo );
        }

        private static UltraDesktopAlert CreateAlert()
        {
            var desktopAlert = new UltraDesktopAlert(null)
            {
                //CaptionAreaAppearance = { Image = Properties.Resources.gear_time },
                AnimationStyleShow = AnimationStyle.Fade,
                AnimationStyleAutoClose = AnimationStyle.Fade,
                AnimationSpeed = AnimationSpeed.Slow,
                AllowMove = DefaultableBoolean.False,
                ImageSize = new Size(32, 32),
                ButtonImageSize = new Size(16, 16),
                MultipleWindowDisplayStyle = MultipleWindowDisplayStyle.Tiled,
                CloseButtonVisible = DefaultableBoolean.True,
                TreatTextAsLink = DefaultableBoolean.True,
                TreatCaptionAsLink = DefaultableBoolean.False,
                TreatFooterTextAsLink = DefaultableBoolean.False,
                AutoCloseDelay = 2000,
                AutoClose = DefaultableBoolean.True
            };
            return desktopAlert;
        }

        #region private class AlertLinkClickHandler
        /*
                private class AlertLinkClickHandler
                {
                    private readonly WorkItem _workItem;
                    private readonly Guid _smartPartId;
                    private readonly UltraDesktopAlert _desktopAlert;

                    public AlertLinkClickHandler(WorkItem pWorkItem, Guid pSmartPartId, UltraDesktopAlert pDesktopAlert)
                    {
                        _workItem = pWorkItem;
                        _smartPartId = pSmartPartId;
                        _desktopAlert = pDesktopAlert;
                    }

                    public void BindEventHandlers()
                    {
                        _desktopAlert.DesktopAlertLinkClicked += AlertLinkClicked;
                        _desktopAlert.DesktopAlertClosed += AlertClosed;
                    }

                    private void AlertLinkClicked(object sender, DesktopAlertLinkClickedEventArgs e)
                    {
                        if (_smartPartId != null)
                        {
                            var wts = this._workItem.Services.Get<IWindowsTreeService>(true);
                            IWindowInfo wi;
                            if (wts.TryGetWindow(this._smartPartId, out wi))
                                wts.ActivateWindow(wi);
                        }

                        string msg = e.WindowInfo.Text;
                        if (!string.IsNullOrEmpty(msg))
                        {
                            Clipboard.Clear();
                            msg = msg.Replace("<br/>", Environment.NewLine);
                            Clipboard.SetText(msg);
                        }
                    }

                    private void AlertClosed(object sender, DesktopAlertClosedEventArgs e)
                    {
                        this._desktopAlert.DesktopAlertLinkClicked -= this.AlertLinkClicked;
                        this._desktopAlert.DesktopAlertClosed -= this.AlertClosed;
                    }
                }
         */
        #endregion
    }
}