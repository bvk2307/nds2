﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

namespace Luxoft.NDS2.Client.UI.Discrepancies
{
    public static class GridSetupHelper
    {
        private static void Reset(this BandDefinition band)
        {
            band.Columns.Clear();
            band.Groups.Clear();
            band.BindWidth = false;
            band.Caption = null;
            band.Name = null;
            band.ColumnsHeadersVisible = true;
        }

        public static void SetupBandDiscrepancySideCurrency(this BandDefinition band)
        {
            band.Reset();
            band.Name = "DiscrepancySide";
            band.ColumnsHeadersVisible = false;
            Action<ColumnDefinition> isHyperlink = column => column.IsHyperLink = true;
            var helper = new GridSetupHelper<DiscrepancySide>();
            //var group1 = new ColumnGroupDefinition { Caption = "Сторона отработки", RowSpan = 2 };
            var group2 = new ColumnGroupDefinition { Caption = "Продавец" };
            var group4 = new ColumnGroupDefinition { Caption = "Сведения стороны отработки" };
            band.Groups.AddRange(new[] { /*group1,*/ group2, group4 });

            var group2_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН НП" };
            var group2_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование НП" };
            group2.SubGroups.AddRange(new[] { group2_1, group2_3 });

            var group4_1 = new ColumnGroupDefinition { Caption = "Номер СФ", ToolTip = "Номер СФ декларации НП, содержащее расхождение" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата СФ", ToolTip = "Дата СФ декларации НП, содержащее расхождение" };
            var group4_3 = new ColumnGroupDefinition { Caption = "Сумма по СФ", ToolTip = "Сумма покупки/продажи по СФ с учетом НДС" };
            var group4_4 = new ColumnGroupDefinition { Caption = "Сумма НДС", ToolTip = "Сумма НДС по СФ" };
            var group4_5 = new ColumnGroupDefinition { Caption = "Инспекция", ToolTip = "Инспекция НП подавшего декларацию" };
            var group4_6 = new ColumnGroupDefinition { Caption = "Регион", ToolTip = "Регион НП подавшего декларацию" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2, group4_3, group4_4, group4_5, group4_6 });

            //group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideName));

            group2_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1INN, isHyperlink));
            group2_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1Name));

            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceNumber, isHyperlink));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceDate));
            group4_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceAmount));
            group4_4.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoTaxAmount));
            group4_5.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInspection));
            group4_6.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoRegion));
        }

        public static void SetupBandDiscrepancySideBreak(this BandDefinition band)
        {
            band.Reset();
            band.Name = "DiscrepancySide";
            band.ColumnsHeadersVisible = false;
            Action<ColumnDefinition> isHyperlink = column => column.IsHyperLink = true;
            var helper = new GridSetupHelper<DiscrepancySide>();
            var group1 = new ColumnGroupDefinition { Caption = "Сторона отработки", RowSpan = 2 };
            var group2 = new ColumnGroupDefinition { Caption = "Покупатель" };
            var group3 = new ColumnGroupDefinition { Caption = "Продавец" };
            var group4 = new ColumnGroupDefinition { Caption = "Сведения стороны расхождения" };
            band.Groups.AddRange(new[] { group1, group2, group3, group4 });

            var group2_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН НП" };
            var group2_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП НП" };
            var group2_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование НП" };
            group2.SubGroups.AddRange(new[] { group2_1, group2_2, group2_3 });

            var group3_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН НП" };
            var group3_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП НП" };
            var group3_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование НП" };
            group3.SubGroups.AddRange(new[] { group3_1, group3_2, group3_3 });

            var group4_1 = new ColumnGroupDefinition { Caption = "Номер СФ", ToolTip = "Номер СФ декларации НП, содержащее расхождение" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата СФ", ToolTip = "Дата СФ декларации НП, содержащее расхождение" };
            var group4_3 = new ColumnGroupDefinition { Caption = "Сумма по СФ", ToolTip = "Сумма покупки/продажи по СФ с учетом НДС" };
            var group4_4 = new ColumnGroupDefinition { Caption = "Сумма НДС", ToolTip = "Сумма НДС по СФ" };
            var group4_5 = new ColumnGroupDefinition { Caption = "Инспекция", ToolTip = "Инспекция НП подавшего декларацию" };
            var group4_6 = new ColumnGroupDefinition { Caption = "Регион", ToolTip = "Регион НП подавшего декларацию" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2, group4_3, group4_4, group4_5, group4_6 });

            group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideName));

            group2_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1INN, isHyperlink));
            group2_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1KPP));
            group2_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1Name));

            group3_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2INN, isHyperlink));
            group3_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2KPP));
            group3_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2Name));

            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceNumber, isHyperlink));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceDate));
            group4_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceAmount));
            group4_4.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoTaxAmount));
            group4_5.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInspection));
            group4_6.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoRegion));

        }

        public static void SetupBandDiscrepancySide(this BandDefinition band, DiscrepancySummaryInfo discrepancy)
        {

            
            string side1Name = null;
            string side2Name = null;

            if ((new[] {8,11,12}).Contains(discrepancy.InvoiceChapter.Value))
            {
                side1Name = "Покупатель";
                side2Name = "Продавец";
            }
            else 
            {
                side1Name = "Продавец";
                side2Name = "Покупатель";
            }

            band.Reset();
            band.Name = "DiscrepancySide";
            band.ColumnsHeadersVisible = false;
            Action<ColumnDefinition> isHyperlink = column => column.IsHyperLink = true;
            var helper = new GridSetupHelper<DiscrepancySide>();
            var group1 = new ColumnGroupDefinition { Caption = "Сторона отработки", RowSpan = 2, Height = 100};
            var group2 = new ColumnGroupDefinition { Caption = side1Name };
            var group3 = new ColumnGroupDefinition { Caption = side2Name };
            var group4 = new ColumnGroupDefinition { Caption = "Сведения стороны расхождения" };
            band.Groups.AddRange(new[] { group1, group2, group3, group4 });

            var group2_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН НП" };
            var group2_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП НП" };
            var group2_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование НП" };
            group2.SubGroups.AddRange(new[] { group2_1, group2_2, group2_3 });

            var group3_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН НП" };
            var group3_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП НП" };
            var group3_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование НП" };
            group3.SubGroups.AddRange(new[] { group3_1, group3_2, group3_3 });

            var group4_1 = new ColumnGroupDefinition { Caption = "Номер СФ", ToolTip = "Номер СФ декларации НП, содержащее расхождение" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата СФ", ToolTip = "Дата СФ декларации НП, содержащее расхождение", Width = 100};
            var group4_3 = new ColumnGroupDefinition { Caption = "Сумма по СФ", ToolTip = "Сумма покупки/продажи по СФ с учетом НДС", Width = 100 };
            var group4_4 = new ColumnGroupDefinition { Caption = "Сумма НДС", ToolTip = "Сумма НДС по СФ", Width = 100 };
            var group4_5 = new ColumnGroupDefinition { Caption = "Инспекция", ToolTip = "Инспекция НП подавшего декларацию", Width = 100 };
            var group4_6 = new ColumnGroupDefinition { Caption = "Регион", ToolTip = "Регион НП подавшего декларацию", Width = 100 };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2, group4_3, group4_4, group4_5, group4_6 });

            group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideName));

            group2_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1INN, isHyperlink));
            group2_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1KPP));
            group2_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side1Name));

            group3_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2INN, isHyperlink));
            group3_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2KPP));
            group3_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.Side2Name));

            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceNumber, isHyperlink));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceDate));
            group4_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInvoiceAmount));
            group4_4.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoTaxAmount));
            group4_5.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoInspection));
            group4_6.Columns.Add(helper.CreateColumnDefinition(obj => obj.SideInfoRegion));
        }


        private static IEnumerable<ColumnDefinition> ToColumns(this IEnumerable<ColumnGroupDefinition> groups)
        {
            var result = new List<ColumnDefinition>();
            foreach (var group in groups)
            {
                result.AddRange(group.Columns);
                result.AddRange(group.SubGroups.ToColumns());
            }
            return result;
        }

        public static void SetupBandAggregateBuyBook(this BandDefinition band, bool isAggregate = false)
        {
            band.Reset();
            band.Name = "BuyBooks";
            band.ColumnsHeadersVisible = false;
            band.Caption = isAggregate ? "Агрегированные сведения из книги покупок" : "Сведения из книги покупок";
            band.Groups.AddRange(GetGroupsBuyBook());
        }

        private static ColumnGroupDefinition[] GetGroupsBuyBook()
        {
            var helper = new GridSetupHelper<AggregateBuyBook>();
            var group0 = new ColumnGroupDefinition { Caption = "№", RowSpan = 2, ToolTip = "Порядковый номер" };
            //var group1 = new ColumnGroupDefinition { Caption = "Отчетный период", RowSpan = 2, ToolTip = "Отчетный период" };
            var group2 = new ColumnGroupDefinition { Caption = "Код вида операции", RowSpan = 2, ToolTip = "Код вида операции" };
            var group3 = new ColumnGroupDefinition { Caption = "СФ" };
            var group4 = new ColumnGroupDefinition { Caption = "КСФ" };
            var group5 = new ColumnGroupDefinition { Caption = "ИСФ" };
            var group6 = new ColumnGroupDefinition { Caption = "ИКСФ" };
            var group7 = new ColumnGroupDefinition { Caption = "Документ, подтверждающий оплату" };

            var group8 = new ColumnGroupDefinition { Caption = "Дата принятия на учет", RowSpan = 2 };
            var group9 = new ColumnGroupDefinition { Caption = "Сведения о продавце" };
            var group10 = new ColumnGroupDefinition { Caption = "Сведения о посреднике" };
            var group11 = new ColumnGroupDefinition { Caption = "Регистрационный № ТД", RowSpan = 2 };
            var group12 = new ColumnGroupDefinition { Caption = "Код валюты", RowSpan = 2 };
            var group13 = new ColumnGroupDefinition { Caption = "Стоимость покупок с НДС", RowSpan = 2 };
            var group14 = new ColumnGroupDefinition { Caption = "Сумма НДС", RowSpan = 2 };
            var group15 = new ColumnGroupDefinition { Caption = "Доп.лист", RowSpan = 2, ToolTip = "Сведения из дополнительного листа" };

            var group3_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер счета-фактуры продавца" };
            var group3_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата счета-фактуры продавца" };
            group3.SubGroups.AddRange(new[] { group3_1, group3_2 });

            var group4_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер корректировочного счета-фактуры продавца" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата корректировочного счета-фактуры продавца" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2 });

            var group5_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления счета-фактуры продавца" };
            var group5_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления счета-фактуры продавца" };
            group5.SubGroups.AddRange(new[] { group5_1, group5_2 });

            var group6_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления корректировочного счета-фактуры продавца" };
            var group6_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления корректировочного счета-фактуры продавца" };
            group6.SubGroups.AddRange(new[] { group6_1, group6_2 });

            var group7_1 = new ColumnGroupDefinition { Caption = "Номер", ToolTip = "Номер документа, подтверждающего оплату" };
            var group7_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата документа, подтверждающего оплату" };
            group7.SubGroups.AddRange(new[] { group7_1, group7_2 });

            var group9_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН продавца" };
            var group9_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП продавца" };
            var group9_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование продавца" };
            group9.SubGroups.AddRange(new[] { group9_1, group9_2, group9_3 });

            var group10_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН посредника(комиссионера, агента, застройщика или заказчика)" };
            var group10_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП посредника (комиссионера, агента, застройщика или заказчика)" };
            var group10_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование посредника(комиссионера, агента, застройщика или заказчика)" };
            group10.SubGroups.AddRange(new[] { group10_1, group10_2, group10_3 });

            group0.Columns.Add(helper.CreateColumnDefinition(obj => obj.OrdinalNumber));
            //group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.ReportPeriod));
            group2.Columns.Add(helper.CreateColumnDefinition(obj => obj.OperationCode));
            group3_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceNumber));
            group3_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceDate));
            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceNumber));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceDate));
            group5_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceNumber));
            group5_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceDate));
            group6_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceNumber));
            group6_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceDate));
            group7_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.PaymentDocNumber));
            group7_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.PaymentDocDate));
            group8.Columns.Add(helper.CreateColumnDefinition(obj => obj.AccountingDate));
            group9_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoINN));
            group9_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoKPP));
            group9_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoName));
            group10_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoINN));
            group10_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoKPP));
            group10_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoName));
            group11.Columns.Add(helper.CreateColumnDefinition(obj => obj.NumberTD));
            group12.Columns.Add(helper.CreateColumnDefinition(obj => obj.CurrencyCode));
            group13.Columns.Add(helper.CreateColumnDefinition(obj => obj.TotalAmount));
            group14.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxAmount));
            group15.Columns.Add(helper.CreateColumnDefinition(obj => obj.PriznakDopList));

            return new[] { group0/*, group1*/, group2, group3, group4, group5, group6, group7, group8, group9, group10, group11, group12, group13, group14, group15 };
        }

        public static void SetupBandAggregateSellBook(this BandDefinition band, bool isAggregate = false)
        {
            band.Reset();
            band.Name = "SellBooks";
            band.ColumnsHeadersVisible = false;
            band.Caption = isAggregate? "Агрегированные сведения из книги продаж" : "Сведения из книги продаж";
            band.Groups.AddRange(GetGroupsSellBook());
        }

        private static ColumnGroupDefinition[] GetGroupsSellBook()
        {
            var helper = new GridSetupHelper<AggregateSellBook>();
            var group0 = new ColumnGroupDefinition { Caption = "№", RowSpan = 2, ToolTip = "Порядковый номер" };
            //var group1 = new ColumnGroupDefinition { Caption = "Отчетный период", RowSpan = 2, ToolTip = "Отчетный период" };
            var group2 = new ColumnGroupDefinition { Caption = "Код вида операции", RowSpan = 2, ToolTip = "Код вида операции" };
            var group3 = new ColumnGroupDefinition { Caption = "СФ" };
            var group4 = new ColumnGroupDefinition { Caption = "КСФ" };
            var group5 = new ColumnGroupDefinition { Caption = "ИСФ" };
            var group6 = new ColumnGroupDefinition { Caption = "ИКСФ" };
            var group7 = new ColumnGroupDefinition { Caption = "Сведения о покупателе" };
            var group8 = new ColumnGroupDefinition { Caption = "Сведения о посреднике" };
            var group9 = new ColumnGroupDefinition { Caption = "Документ подтверждающий оплату", Height = 30};
            var group10 = new ColumnGroupDefinition { Caption = "Код валюты", RowSpan = 2, ToolTip = "Код валюты по ОКВ" };
            var group11 = new ColumnGroupDefinition { Caption = "Стоимость продаж с НДС" };
            var group12 = new ColumnGroupDefinition { Caption = "Стоимость продаж облагаемых налогом (в руб.) без НДС" };
            var group13 = new ColumnGroupDefinition { Caption = "Сумма НДС" };
            var group14 = new ColumnGroupDefinition { Caption = "Стоимость продаж, освобождаемых он налога", RowSpan = 2, ToolTip = "Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп." };
            var group15 = new ColumnGroupDefinition { Caption = "Доп.лист", RowSpan = 2, ToolTip = "Сведения из дополнительного листа" };

            var group3_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер счета-фактуры продавца" };
            var group3_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата счета-фактуры продавца" };
            group3.SubGroups.AddRange(new[] { group3_1, group3_2 });

            var group3_3 = new ColumnGroupDefinition { Caption = "Регистрационный № ТД", RowSpan = 2 };

            var group4_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер корректировочного счета-фактуры продавца" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата корректировочного счета-фактуры продавца" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2 });

            var group5_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления счета-фактуры продавца" };
            var group5_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления счета-фактуры продавца" };
            group5.SubGroups.AddRange(new[] { group5_1, group5_2 });

            var group6_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления корректировочного счета-фактуры продавца" };
            var group6_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления корректировочного счета-фактуры продавца" };
            group6.SubGroups.AddRange(new[] { group6_1, group6_2 });

            var group7_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН покупателя" };
            var group7_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП покупателя" };
            var group7_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование покупателя" };
            group7.SubGroups.AddRange(new[] { group7_1, group7_2, group7_3 });

            var group8_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН посредника (комиссионера, агента)" };
            var group8_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП посредника (комиссионера, агента)" };
            var group8_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование посредника (комиссионера, агента)" };
            group8.SubGroups.AddRange(new[] { group8_1, group8_2, group8_3 });

            var group9_1 = new ColumnGroupDefinition { Caption = "Номер", ToolTip = "Номер документа, подтверждающего оплату" };
            var group9_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата документа, подтверждающего оплату" };
            group9.SubGroups.AddRange(new[] { group9_1, group9_2 });

            var group11_1 = new ColumnGroupDefinition { Caption = "в валюте", ToolTip = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры" };
            var group11_2 = new ColumnGroupDefinition { Caption = "в руб. и коп.", ToolTip = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в руб. и коп." };
            group11.SubGroups.AddRange(new[] { group11_1, group11_2 });

            var group12_1 = new ColumnGroupDefinition { Caption = "18%", ToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 18 %" };
            var group12_2 = new ColumnGroupDefinition { Caption = "10%", ToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 10" };
            var group12_3 = new ColumnGroupDefinition { Caption = "0%", ToolTip = "Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 0" };
            group12.SubGroups.AddRange(new[] { group12_1, group12_2, group12_3 });

            var group13_1 = new ColumnGroupDefinition { Caption = "18%", ToolTip = "Сумма НДС по счету-фактуре, разница суммы НДС по корректировочному счету-фактуре, в руб. и коп., по ставке 18" };
            var group13_2 = new ColumnGroupDefinition { Caption = "10%", ToolTip = "Сумма НДС по счету-фактуре, разница суммы НДС по корректировочному счету-фактуре, в руб. и коп., по ставке 10" };
            group13.SubGroups.AddRange(new[] { group13_2, group13_1 });

            group0.Columns.Add(helper.CreateColumnDefinition(obj => obj.OrdinalNumber));
            //group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.ReportPeriod));
            group2.Columns.Add(helper.CreateColumnDefinition(obj => obj.OperationCode));
            group3_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceNumber));
            group3_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceDate));
            group3_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.NumberTD));
            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceNumber));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceDate));
            group5_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceNumber));
            group5_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceDate));
            group6_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceNumber));
            group6_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceDate));
            group7_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoINN));
            group7_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoKPP));
            group7_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoName));
            group8_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoINN));
            group8_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoKPP));
            group8_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.IntermediaryInfoName));
            group9_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.PaymentDocNumber));
            group9_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.PaymentDocDate));
            group10.Columns.Add(helper.CreateColumnDefinition(obj => obj.CurrencyCode));
            group11_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.TotalAmountCurrency));
            group11_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.TotalAmountRouble));
            group12_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxableAmount18));
            group12_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxableAmount10));
            group12_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxableAmount0));
            group13_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxAmount18));
            group13_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxAmount10));
            group14.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxFreeAmount));
            group15.Columns.Add(helper.CreateColumnDefinition(obj => obj.PriznakDopList));

            return new[] { group0, /*group1,*/ group2, group3, group3_3, group4, group5, group6, group7, group8, group9, group10, group11, group12, group13, group14, group15 };
        }

        public static void SetupBandAggregateSentInvoiceJournal(this BandDefinition band, bool isAggregate = false)
        {
            band.Reset();
            band.Name = "SentInvoiceJournals";
            band.ColumnsHeadersVisible = false;
            band.Caption = isAggregate ? "Агрегированные сведения из журнала выставленных СФ" : "Сведения из журнала выставленных СФ";
            band.Groups.AddRange(GetGroupsSentInvoiceJournal());
        }

        private static ColumnGroupDefinition[] GetGroupsSentInvoiceJournal()
        {
            var helper = new GridSetupHelper<AggregateSentInvoiceJournal>();
            var group0 = new ColumnGroupDefinition { Caption = "№", RowSpan = 2, ToolTip = "Порядковый номер" };
            //var group1 = new ColumnGroupDefinition { Caption = "Отчетный период", RowSpan = 2, ToolTip = "Отчетный период" };
            var group2 = new ColumnGroupDefinition { Caption = "Дата выставления", RowSpan = 2, ToolTip = "Дата выставления" };
            var group3 = new ColumnGroupDefinition { Caption = "Код вида операции", RowSpan = 2, ToolTip = "Код вида операции" };
            var group4 = new ColumnGroupDefinition { Caption = "СФ" };
            var group5 = new ColumnGroupDefinition { Caption = "КСФ" };
            var group6 = new ColumnGroupDefinition { Caption = "ИСФ" };
            var group7 = new ColumnGroupDefinition { Caption = "ИКСФ" };
            var group8 = new ColumnGroupDefinition { Caption = "Сведения о покупателе" };
            var group9 = new ColumnGroupDefinition { Caption = "Сведения о продавце" };
            var group10 = new ColumnGroupDefinition { Caption = "Код валюты", RowSpan = 2, ToolTip = "Код валюты по ОКВ" };
            var group11 = new ColumnGroupDefinition { Caption = "Стоимость товаров", RowSpan = 2, ToolTip = "Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре , в руб. икоп. - всего" };
            var group12 = new ColumnGroupDefinition { Caption = "Сумма НДС", RowSpan = 2, ToolTip = "В том числе сумма НДС по счету-фактуре , в руб. и коп." };
            var group13 = new ColumnGroupDefinition { Caption = "Разница стоимости с НДС по КСФ" };
            var group14 = new ColumnGroupDefinition { Caption = "Разница НДС по КСФ" };
            var group15 = new ColumnGroupDefinition { Caption = "№ коррект.", RowSpan = 2, ToolTip = "Номер корректировки декларации" };

            var group4_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер счета-фактуры" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата счета-фактуры" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2 });

            var group5_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер корректировочного счета-фактуры" };
            var group5_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата корректировочного счета-фактуры" };
            group5.SubGroups.AddRange(new[] { group5_1, group5_2 });

            var group6_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления счета-фактуры" };
            var group6_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления счета-фактуры" };
            group6.SubGroups.AddRange(new[] { group6_1, group6_2 });

            var group7_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления корректировочного счета-фактуры" };
            var group7_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления корректировочного счета-фактуры" };
            group7.SubGroups.AddRange(new[] { group7_1, group7_2 });

            var group8_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН покупателя" };
            var group8_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП покупателя" };
            var group8_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование покупателя" };
            group8.SubGroups.AddRange(new[] { group8_1, group8_2, group8_3 });

            var group9_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН продавца" };
            var group9_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП продавца" };
            var group9_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование продавца" };
            group9.SubGroups.AddRange(new[] { group9_1, group9_2, group9_3 });

            var group13_1 = new ColumnGroupDefinition { Caption = "уменьшение", ToolTip = "Разница стоимости с учетом НДС по корректировочному счету-фактуре, в руб. и коп. - уменьшение" };
            var group13_2 = new ColumnGroupDefinition { Caption = "увеличение", ToolTip = "Разница стоимости с учетом НДС по корректировочному счету-фактуре, в руб. и коп. - увеличение" };
            group13.SubGroups.AddRange(new[] { group13_1, group13_2 });

            var group14_1 = new ColumnGroupDefinition { Caption = "уменьшение", ToolTip = "Разница НДС по корректировочному счету-фактуре, в руб. и коп. – уменьшение" };
            var group14_2 = new ColumnGroupDefinition { Caption = "увеличение", ToolTip = "Разница НДС по корректировочному счету-фактуре, в руб. и коп. - увеличение" };
            group14.SubGroups.AddRange(new[] { group14_1, group14_2 });

            group0.Columns.Add(helper.CreateColumnDefinition(obj => obj.OrdinalNumber));
            //group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.ReportPeriod));
            group2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SendDate));
            group3.Columns.Add(helper.CreateColumnDefinition(obj => obj.OperationCode));
            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceNumber));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceDate));
            group5_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceNumber));
            group5_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceDate));
            group6_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceNumber));
            group6_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceDate));
            group7_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceNumber));
            group7_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceDate));
            group8_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoINN));
            group8_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoKPP));
            group8_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.BuyerInfoName));
            group9_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoINN));
            group9_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoKPP));
            group9_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoName));
            group10.Columns.Add(helper.CreateColumnDefinition(obj => obj.CurrencyCode));
            group11.Columns.Add(helper.CreateColumnDefinition(obj => obj.Amount));
            group12.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxAmount));
            group13_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CostDifferenceIncrease));
            group13_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CostDifferenceDecrease));
            group14_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxDifferenceIncrease));
            group14_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxDifferenceDecrease));

            return new[] { group0, /*group1,*/ group2, group3, group4, group5, group6, group7, group8, group9, group10, group11, group12, group13, group14 };
        }

        public static void SetupBandAggregateRecievedInvoiceJournal(this BandDefinition band, bool isAggregate = false)
        {
            band.Reset();
            band.Name = "RecievedInvoiceJournals";
            band.ColumnsHeadersVisible = false;
            band.Caption = isAggregate ? "Агрегированные сведения из журнала полученных СФ" :"Сведения из журнала полученных СФ";
            band.Groups.AddRange(GetGroupsRecievedInvoiceJournal());
        }

        private static ColumnGroupDefinition[] GetGroupsRecievedInvoiceJournal()
        {
            var helper = new GridSetupHelper<AggregateRecievedInvoiceJournal>();
            var group0 = new ColumnGroupDefinition { Caption = "№", RowSpan = 2, ToolTip = "Порядковый номер" };
            //var group1 = new ColumnGroupDefinition { Caption = "Отчетный период", RowSpan = 2, ToolTip = "Отчетный период" };
            var group2 = new ColumnGroupDefinition { Caption = "Дата получения", RowSpan = 2, ToolTip = "Дата получения" };
            var group3 = new ColumnGroupDefinition { Caption = "Код вида операции", RowSpan = 2, ToolTip = "Код вида операции" };
            var group4 = new ColumnGroupDefinition { Caption = "СФ" };
            var group5 = new ColumnGroupDefinition { Caption = "КСФ" };
            var group6 = new ColumnGroupDefinition { Caption = "ИСФ" };
            var group7 = new ColumnGroupDefinition { Caption = "ИКСФ" };
            var group8 = new ColumnGroupDefinition { Caption = "Сведения о продавце" };
            var group9 = new ColumnGroupDefinition { Caption = "Сведения о субкомиссионере" };
            var group10 = new ColumnGroupDefinition { Caption = "Код вида сделки", RowSpan = 2, ToolTip = "Код вида сделки" };
            var group11 = new ColumnGroupDefinition { Caption = "Код валюты", RowSpan = 2, ToolTip = "Код валюты по ОКВ" };
            var group12 = new ColumnGroupDefinition { Caption = "Стоимость товаров", RowSpan = 2, ToolTip = "Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре , в руб. икоп. - всего" };
            var group13 = new ColumnGroupDefinition { Caption = "Сумма НДС", RowSpan = 2, ToolTip = "В том числе сумма НДС по счету-фактуре , в руб. и коп." };
            var group14 = new ColumnGroupDefinition { Caption = "Разница стоимости с НДС по КСФ" };
            var group15 = new ColumnGroupDefinition { Caption = "Разница НДС по КСФ" };
            var group16 = new ColumnGroupDefinition { Caption = "№ коррект.", RowSpan = 2, ToolTip = "Номер корректировки декларации" };

            var group4_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер счета-фактуры" };
            var group4_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата счета-фактуры" };
            group4.SubGroups.AddRange(new[] { group4_1, group4_2 });

            var group5_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер корректировочного счета-фактуры" };
            var group5_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата корректировочного счета-фактуры" };
            group5.SubGroups.AddRange(new[] { group5_1, group5_2 });

            var group6_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления счета-фактуры" };
            var group6_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления счета-фактуры" };
            group6.SubGroups.AddRange(new[] { group6_1, group6_2 });

            var group7_1 = new ColumnGroupDefinition { Caption = "№", ToolTip = "Номер исправления корректировочного счета-фактуры" };
            var group7_2 = new ColumnGroupDefinition { Caption = "Дата", ToolTip = "Дата исправления корректировочного счета-фактуры" };
            group7.SubGroups.AddRange(new[] { group7_1, group7_2 });

            var group8_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН продавца" };
            var group8_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП продавца" };
            var group8_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование продавца" };
            group8.SubGroups.AddRange(new[] { group8_1, group8_2, group8_3 });

            var group9_1 = new ColumnGroupDefinition { Caption = "ИНН", ToolTip = "ИНН субкомиссионера (агента)" };
            var group9_2 = new ColumnGroupDefinition { Caption = "КПП", ToolTip = "КПП субкомиссионера (агента)" };
            var group9_3 = new ColumnGroupDefinition { Caption = "Наименование", ToolTip = "Наименование субкомиссионера (агента)" };
            group9.SubGroups.AddRange(new[] { group9_1, group9_2, group9_3 });

            var group14_1 = new ColumnGroupDefinition { Caption = "уменьшение", ToolTip = "Разница стоимости с учетом НДС по корректировочному счету-фактуре, в руб. и коп. - уменьшение" };
            var group14_2 = new ColumnGroupDefinition { Caption = "увеличение", ToolTip = "Разница стоимости с учетом НДС по корректировочному счету-фактуре, в руб. и коп. - увеличение" };
            group14.SubGroups.AddRange(new[] { group14_1, group14_2 });

            var group15_1 = new ColumnGroupDefinition { Caption = "уменьшение", ToolTip = "Разница НДС по корректировочному счету-фактуре, в руб. и коп. - уменьшение" };
            var group15_2 = new ColumnGroupDefinition { Caption = "увеличение", ToolTip = "Разница НДС по корректировочному счету-фактуре, в руб. и коп. - увеличение" };
            group15.SubGroups.AddRange(new[] { group15_1, group15_2 });

            group0.Columns.Add(helper.CreateColumnDefinition(obj => obj.OrdinalNumber));
            //group1.Columns.Add(helper.CreateColumnDefinition(obj => obj.ReportPeriod));
            group2.Columns.Add(helper.CreateColumnDefinition(obj => obj.RecieveDate));
            group3.Columns.Add(helper.CreateColumnDefinition(obj => obj.OperationCode));
            group4_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceNumber));
            group4_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.InvoiceDate));
            group5_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceNumber));
            group5_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CorrectedInvoiceDate));
            group6_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceNumber));
            group6_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedInvoiceDate));
            group7_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceNumber));
            group7_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.FixedCorrectedInvoiceDate));
            group8_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoINN));
            group8_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoKPP));
            group8_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SellerInfoName));
            group9_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.SubcommissionAgentINN));
            group9_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.SubcommissionAgentKPP));
            group9_3.Columns.Add(helper.CreateColumnDefinition(obj => obj.SubcommissionAgentName));
            group10.Columns.Add(helper.CreateColumnDefinition(obj => obj.DealTypeCode));
            group11.Columns.Add(helper.CreateColumnDefinition(obj => obj.CurrencyCode));
            group12.Columns.Add(helper.CreateColumnDefinition(obj => obj.Amount));
            group13.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxAmount));
            group14_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.CostDifferenceIncrease));
            group14_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.CostDifferenceDecrease));
            group15_1.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxDifferenceIncrease));
            group15_2.Columns.Add(helper.CreateColumnDefinition(obj => obj.TaxDifferenceDecrease));

            return new[] { group0, /*group1,*/ group2, group3, group4, group5, group6, group7, group8, group9, group10, group11, group12, group13, group14, group15 };
        }

        public static void SetupBandDetailsBuyBook(this BandDefinition band)
        {
            band.Reset();
            band.Name = "BuyBookDetails";
            band.Caption = "Cведения из книги покупок";
            band.BindWidth = true;
            band.ColumnsHeadersVisible = false;
            band.Columns.AddRange(GetGroupsBuyBook().ToColumns());
        }

        public static void SetupBandDetailsSellBook(this BandDefinition band)
        {
            band.Reset();
            band.Name = "SellBookDetails";
            band.Caption = "Агрегированные сведения из книги продаж";
            band.BindWidth = true;
            band.ColumnsHeadersVisible = false;
            band.Columns.AddRange(GetGroupsSellBook().ToColumns());
        }

        public static void SetupBandDetailsSentInvoiceJournal(this BandDefinition band)
        {
            band.Reset();
            band.Name = "SentInvoiceJournalDetails";
            band.Caption = "Агрегированные сведения из журнала выставленных СФ";
            band.BindWidth = true;
            band.ColumnsHeadersVisible = false;
            band.Columns.AddRange(GetGroupsSentInvoiceJournal().ToColumns());
        }

        public static void SetupBandDetailsRecievedInvoiceJournal(this BandDefinition band)
        {
            band.Reset();
            band.Name = "RecievedInvoiceJournalDetails";
            band.Caption = "Агрегированные сведения из журнала полученных СФ";
            band.BindWidth = true;
            band.ColumnsHeadersVisible = false;
            band.Columns.AddRange(GetGroupsRecievedInvoiceJournal().ToColumns());
        }
    }
}