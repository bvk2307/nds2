﻿namespace Luxoft.NDS2.Client.UI.Discrepancies.Dialog
{
    partial class CommentsAdding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel upFooter;
            this.ubSave = new Infragistics.Win.Misc.UltraButton();
            this.upGrid = new Infragistics.Win.Misc.UltraPanel();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.DiscrepancyGridControl();
            upFooter = new Infragistics.Win.Misc.UltraPanel();
            upFooter.ClientArea.SuspendLayout();
            upFooter.SuspendLayout();
            this.upGrid.ClientArea.SuspendLayout();
            this.upGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // upFooter
            // 
            // 
            // upFooter.ClientArea
            // 
            upFooter.ClientArea.Controls.Add(this.ubSave);
            upFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            upFooter.Location = new System.Drawing.Point(0, 347);
            upFooter.Name = "upFooter";
            upFooter.Size = new System.Drawing.Size(786, 47);
            upFooter.TabIndex = 0;
            // 
            // ubSave
            // 
            this.ubSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ubSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ubSave.Location = new System.Drawing.Point(12, 12);
            this.ubSave.Name = "ubSave";
            this.ubSave.Size = new System.Drawing.Size(75, 23);
            this.ubSave.TabIndex = 1;
            this.ubSave.Text = "Сохранить";
            // 
            // upGrid
            // 
            // 
            // upGrid.ClientArea
            // 
            this.upGrid.ClientArea.Controls.Add(this.grid);
            this.upGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upGrid.Location = new System.Drawing.Point(0, 0);
            this.upGrid.Name = "upGrid";
            this.upGrid.Size = new System.Drawing.Size(786, 347);
            this.upGrid.TabIndex = 1;
            // 
            // grid
            // 
            this.grid.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.grid.DataSource = null;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.HeaderVisible = false;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.RowDoubleClicked = null;
            this.grid.Setup = null;
            this.grid.Size = new System.Drawing.Size(786, 347);
            this.grid.TabIndex = 0;
            this.grid.ViewMode = Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup.DiscrepancyGridViewMode.Edit;
            // 
            // CommentsAdding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 394);
            this.Controls.Add(this.upGrid);
            this.Controls.Add(upFooter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CommentsAdding";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ввод пояснений к СФ";
            upFooter.ClientArea.ResumeLayout(false);
            upFooter.ResumeLayout(false);
            this.upGrid.ClientArea.ResumeLayout(false);
            this.upGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ubSave;
        private Infragistics.Win.Misc.UltraPanel upGrid;
        private Controls.Grid.V1.DiscrepancyGridControl grid;
    }
}