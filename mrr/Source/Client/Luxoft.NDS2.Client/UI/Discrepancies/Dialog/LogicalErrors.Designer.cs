﻿namespace Luxoft.NDS2.Client.UI.Discrepancies.Dialog
{
    partial class LogicalErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.upGrid = new Infragistics.Win.Misc.UltraPanel();
            this.gridError = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.DiscrepancyGridControl();
            this.gridRecord = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.DiscrepancyGridControl();
            this.upGrid.ClientArea.SuspendLayout();
            this.upGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // upGrid
            // 
            // 
            // upGrid.ClientArea
            // 
            this.upGrid.ClientArea.Controls.Add(this.gridError);
            this.upGrid.ClientArea.Controls.Add(this.gridRecord);
            this.upGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upGrid.Location = new System.Drawing.Point(0, 0);
            this.upGrid.Name = "upGrid";
            this.upGrid.Size = new System.Drawing.Size(786, 455);
            this.upGrid.TabIndex = 1;
            // 
            // gridError
            // 
            this.gridError.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridError.DataSource = null;
            this.gridError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridError.HeaderVisible = false;
            this.gridError.Location = new System.Drawing.Point(0, 174);
            this.gridError.Name = "gridError";
            this.gridError.RowDoubleClicked = null;
            this.gridError.Setup = null;
            this.gridError.Size = new System.Drawing.Size(786, 281);
            this.gridError.TabIndex = 1;
            this.gridError.ViewMode = Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup.DiscrepancyGridViewMode.Normal;
            // 
            // gridRecord
            // 
            this.gridRecord.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridRecord.DataSource = null;
            this.gridRecord.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridRecord.HeaderVisible = false;
            this.gridRecord.Location = new System.Drawing.Point(0, 0);
            this.gridRecord.Name = "gridRecord";
            this.gridRecord.RowDoubleClicked = null;
            this.gridRecord.Setup = null;
            this.gridRecord.Size = new System.Drawing.Size(786, 174);
            this.gridRecord.TabIndex = 0;
            this.gridRecord.ViewMode = Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup.DiscrepancyGridViewMode.Normal;
            // 
            // LogicalErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 455);
            this.Controls.Add(this.upGrid);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogicalErrors";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ошибки ЛК";
            this.upGrid.ClientArea.ResumeLayout(false);
            this.upGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upGrid;
        private Controls.Grid.V1.DiscrepancyGridControl gridError;
        private Controls.Grid.V1.DiscrepancyGridControl gridRecord;
    }
}