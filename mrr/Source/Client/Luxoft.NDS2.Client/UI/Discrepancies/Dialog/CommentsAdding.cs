﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Discrepancies.Dialog
{
    public partial class CommentsAdding : Form
    {
        private readonly DetailsBase _source;
        private readonly Type _type;

        public DetailsBase Source
        {
            get { return _source; }
        }

        public Type Type
        {
            get { return _type; }
        } 

        public CommentsAdding(IAggreagate source)
        {
            InitializeComponent();
            _source = source.ToDetails();
            _type = _source.GetType();
            InitGrid();
        }

        private void InitGrid()
        {
            var setup = new DiscrepancyGridSetup();
            _source.CorrectionNumber = "пояснение";
            var grayColumnsName = new List<string>
            {
                _source.GetPropertyName(o => o.ReportPeriod),
                //_source.GetPropertyName(o => o.CorrectionNumber)
            };

            if (_type == typeof(DetailsBuyBook))
            {
                GridSetupHelper.SetupBandAggregateBuyBook(setup.Band);

                var obj = (DetailsBuyBook)_source;
                grid.DataSource = new List<DetailsBuyBook> { obj };
                grayColumnsName.AddRange(new[]
                {
                    obj.GetPropertyName(o => o.SellerInfoName),
                    obj.GetPropertyName(o => o.IntermediaryInfoName),
                    obj.GetPropertyName(o => o.TotalAmount),
                    obj.GetPropertyName(o => o.TaxAmount)
                });
            }
            else if (_type == typeof(DetailsSellBook))
            {
                GridSetupHelper.SetupBandAggregateSellBook(setup.Band);

                var obj = (DetailsSellBook)_source;
                grid.DataSource = new List<DetailsSellBook> { obj };
                grayColumnsName.AddRange(new[]
                {
                    obj.GetPropertyName(o => o.BuyerInfoName),
                    obj.GetPropertyName(o => o.IntermediaryInfoName),
                    obj.GetPropertyName(o => o.TotalAmountCurrency),
                    obj.GetPropertyName(o => o.TotalAmountRouble),
                    obj.GetPropertyName(o => o.TaxableAmount0),
                    obj.GetPropertyName(o => o.TaxableAmount10),
                    obj.GetPropertyName(o => o.TaxableAmount18),
                    obj.GetPropertyName(o => o.TaxAmount10),
                    obj.GetPropertyName(o => o.TaxAmount18),
                    obj.GetPropertyName(o => o.TaxFreeAmount)
                });
            }
            else if (_type == typeof(DetailsSentInvoiceJournal))
            {
                GridSetupHelper.SetupBandAggregateSentInvoiceJournal(setup.Band);

                var obj = (DetailsSentInvoiceJournal)_source;
                grid.DataSource = new List<DetailsSentInvoiceJournal> { obj };
                grayColumnsName.AddRange(new[]
                {
                    obj.GetPropertyName(o => o.BuyerInfoName),
                    obj.GetPropertyName(o => o.SellerInfoName),
                    obj.GetPropertyName(o => o.Amount),
                    obj.GetPropertyName(o => o.TaxAmount),
                    obj.GetPropertyName(o => o.CostDifferenceDecrease),
                    obj.GetPropertyName(o => o.CostDifferenceIncrease),
                    obj.GetPropertyName(o => o.TaxDifferenceDecrease),
                    obj.GetPropertyName(o => o.TaxDifferenceIncrease)
                });
            }
            else if (_type == typeof(DetailsRecievedInvoiceJournal))
            {
                GridSetupHelper.SetupBandAggregateRecievedInvoiceJournal(setup.Band);

                var obj = (DetailsRecievedInvoiceJournal)_source;
                grid.DataSource = new List<DetailsRecievedInvoiceJournal> { obj };
                grayColumnsName.AddRange(new[]
                {
                    obj.GetPropertyName(o => o.SellerInfoName),
                    obj.GetPropertyName(o => o.SubcommissionAgentName),
                    obj.GetPropertyName(o => o.Amount),
                    obj.GetPropertyName(o => o.TaxAmount),
                    obj.GetPropertyName(o => o.CostDifferenceDecrease),
                    obj.GetPropertyName(o => o.CostDifferenceIncrease),
                    obj.GetPropertyName(o => o.TaxDifferenceDecrease),
                    obj.GetPropertyName(o => o.TaxDifferenceIncrease)
                });
            }

            setup.Band.Caption = null;
            setup.Band.Name = _type.Name;
            var sColumns =
                from column in setup.Band.GetAllColumns()
                where !grayColumnsName.Contains(column.Key)
                select column;
            foreach (var column in sColumns)
            {
                column.EditableDefinition.Editable = true;
            }
            grid.Setup = setup;
        }
    }
}