﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Discrepancies.Dialog
{
    public partial class LogicalErrors : Form
    {
        private readonly DetailsBase _source;
        private readonly Type _type;
        private readonly List<ErrorMapping> _errors;

        public DetailsBase Source
        {
            get { return _source; }
        }

        public Type Type
        {
            get { return _type; }
        }

        public LogicalErrors(IAggreagate source, List<ErrorMapping> errors)
        {
            _source = source.ToDetails();
            _type = _source.GetType();
            _errors = errors;

            InitializeComponent();
            InitRecordGrid();
            InitErrorsGrid();
            //Shown += (sender, e) => gridError.SelectRow += GridError_SelectRow;
        }

        private void InitErrorsGrid()
        {
            var setup = new DiscrepancyGridSetup();
            setup.Band.Caption = "Ошибки ЛК";
            setup.Band.Name = "ErrorMapping";
            var helper = new GridSetupHelper<ErrorMapping>();
            setup.Band.Columns.AddRange(new[]
            {
                helper.CreateColumnDefinition(obj => obj.ErrorCode, "Код ошибки"),
                helper.CreateColumnDefinition(obj => obj.Description, "Описание ошибки")
            });
            gridError.DataSource = _errors;
            gridError.Setup = setup;
            gridError.SelectRow += GridError_SelectRow;
        }

        private void GridError_SelectRow(object sender, SelectedRowEventArgs e)
        {
            var properties =
                from property in _type.GetProperties()
                select property.Name;

            gridRecord.ResetCellBackColor(_source, properties);
            var error = e.DataObject as ErrorMapping;
            if (error != null)
            {
                gridRecord.SetCellBackColor(_source, properties.Where(p => error.DataMembersArray.Contains(p)), Color.LightGoldenrodYellow, error.Description);
            }
        }

        private void InitRecordGrid()
        {
            var setup = new DiscrepancyGridSetup();
            if (_type == typeof(DetailsBuyBook))
            {
                GridSetupHelper.SetupBandAggregateBuyBook(setup.Band);
                var obj = (DetailsBuyBook)_source;
                gridRecord.DataSource = new List<DetailsBuyBook> { obj };
            }
            else if (_type == typeof(DetailsSellBook))
            {
                GridSetupHelper.SetupBandAggregateSellBook(setup.Band);
                var obj = (DetailsSellBook)_source;
                gridRecord.DataSource = new List<DetailsSellBook> { obj };
            }
            else if (_type == typeof(DetailsSentInvoiceJournal))
            {
                GridSetupHelper.SetupBandAggregateSentInvoiceJournal(setup.Band);
                var obj = (DetailsSentInvoiceJournal)_source;
                gridRecord.DataSource = new List<DetailsSentInvoiceJournal> { obj };
            }
            else if (_type == typeof(DetailsRecievedInvoiceJournal))
            {
                GridSetupHelper.SetupBandAggregateRecievedInvoiceJournal(setup.Band);
                var obj = (DetailsRecievedInvoiceJournal)_source;
                gridRecord.DataSource = new List<DetailsRecievedInvoiceJournal> { obj };
            }
            setup.Band.Name = _type.Name;
            gridRecord.Setup = setup;
        }
    }
}