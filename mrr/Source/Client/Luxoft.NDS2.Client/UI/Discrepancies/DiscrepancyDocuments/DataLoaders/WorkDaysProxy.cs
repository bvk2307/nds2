﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.DataLoaders
{
    public class WorkDaysProxy : ServiceProxyBase<DateTime>
    {
        private readonly ICalendarDataService _service;

        public WorkDaysProxy(ICalendarDataService service, INotifier notifier, IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public DateTime CalcDate(DateTime date, int daysToShift)
        {
            DateTime result;

            if (!Invoke(() => _service.AddWorkingDays(date, daysToShift), out result))
            {
                _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.InvoiceClaimList.ServiceError,
                        ResourceManagerNDS2.ClaimCard.CalcDateFault
                        ));
            }

            return result;
        }
    }
}
