﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Settings.Nds2Parameters;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card
{
    // ReSharper disable InconsistentNaming

    public class DocBasePresenter : BasePresenter<DocBaseView>
    {
        protected DiscrepancyDocumentInfo _docInfo;

        protected long _declarationId;

        private AsyncWorker<int> _invoiceWorker = null;

        protected AsyncWorker<int> _mainDataWorker = null;

        private readonly List<int> _updatableParts = new List<int>();

        private readonly RequestStateAsync _requestStateAsyncInvoice = new RequestStateAsync();

        protected bool _stopWorkers = false;

        protected IDeclarationsDataService _declarationService;

        protected IDiscrepancyDocumentService _discrepancyDocumentService;

        private List<int> _chaptersList = new List<int>() { 8, 9, 10, 11, 12 };

        protected DeclarationSummary _declaration;

        protected Dictionary<string, DateTime> _dates = new Dictionary<string, DateTime>();

        protected DocumentCalculateInfo _docCalculateInfo;

        private readonly IDeclarationCardOpener _declarationCardOpener;

        protected readonly UserAccessOpearation _userAccessOpearation;

        public DocBasePresenter(DocBaseView view, PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo docInfo)
            : base(ctx, wi, view)
        {
            _declarationCardOpener = GetDeclarationCardOpener();
            _docInfo = docInfo;
            _declarationId = _docInfo.DeclarationVersionId;
            _declarationService = base.GetServiceProxy<IDeclarationsDataService>();
            _discrepancyDocumentService = base.GetServiceProxy<IDiscrepancyDocumentService>();
            _userAccessOpearation = new UserAccessOpearation(SecurityService);
            _explainReplyViewer = new KnpDocumentDetailsViewer(wi, ctx);
        }

        public void ViewDeclarationDetails(long id, DeclarationInfoSource info)
        {
            var result = _declarationCardOpener.Open(id, Direction.Invariant, info);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public void Reload()
        {
            LoadMainData();
            _requestStateAsyncInvoice.State = StateAsync.Start;
            LoadChaptersData();
        }

        public virtual void LoadChaptersData()
        {
            View.LoadCalculateInfo();
            View.GridInvoices.UpdateData();
        }

        public virtual void LoadMainData()
        {
            _mainDataWorker = new AsyncWorker<int>();
            _mainDataWorker.DoWork += (sender, args) =>
            {
                FillDocument(_docInfo.Id);
                FillDeclaration(_declarationId);
                FillDocumentStatusDates(_docInfo.Id);
                FillDocumentCalculateInfo(_docInfo.Id);
            };
            _mainDataWorker.Complete += (sender, args) => View.ExecuteInUiThread(c =>
            {
                View.LoadData();
                View.LoadCalculateInfo();
            });
            _mainDataWorker.Failed += (sender, args) =>
            {
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };
            _mainDataWorker.Start();
        }

        #region Загрузка СФ

        private bool CheckUpdateAllChapter()
        {
            bool ret = true;
            foreach (int itemChapter in _chaptersList)
            {
                if (!_updatableParts.Contains(itemChapter))
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        #region Неотраженные операции

        private RequestStateAsync _notReflectedRequestState = new RequestStateAsync();
        private AsyncWorker<int> _notReflectedWorker;

        public PageResult<NotReflectedInvoice> GetInvoicesNotReflected(QueryConditions qc)
        {
            switch (_notReflectedRequestState.State)
            {
                case StateAsync.Start:
                case StateAsync.None:
                    GetNotReflectedInvoicesAsync(qc);
                    break;
                case StateAsync.Complete:
                    if (!QueryConditions.IsEqualsConditions(qc, _notReflectedRequestState.QueryConditionsSend))
                        GetNotReflectedInvoicesAsync(qc);
                    else
                        _notReflectedRequestState.QueryConditionsSend = (QueryConditions)qc.Clone();
                    break;
            }

            return new PageResult<NotReflectedInvoice>
            {
                Rows = (List<NotReflectedInvoice>)_notReflectedRequestState.ResultData,
                TotalMatches = (int)_notReflectedRequestState.TotalRowsNumber
            };
        }

        private void GetNotReflectedInvoicesAsync(QueryConditions qc)
        {
            SetRuqestStateAsyncInvoceEmpty<NotReflectedInvoice>(_notReflectedRequestState);
            _notReflectedRequestState.QueryConditionsSend = (QueryConditions)qc.Clone();

            _notReflectedWorker = new AsyncWorker<int>();
            _notReflectedWorker.DoWork += (sender, args) =>
            {
                _notReflectedRequestState.State = StateAsync.Send;
                long totalRowsNumber = 0;
                _notReflectedRequestState.ResultData = GetNotReflectedInvoices(_notReflectedRequestState.QueryConditionsSend, out totalRowsNumber);
                _notReflectedRequestState.TotalRowsNumber = totalRowsNumber;
            };
            _notReflectedWorker.Complete += (sender, args) =>
            {
                _notReflectedRequestState.State = StateAsync.Complete;
                View.ExecuteInUiThread(c => View.NotReflectedInvoiceLoaded(true));
            };
            _notReflectedWorker.Failed += (sender, args) =>
            {
                SetRuqestStateAsyncInvoceEmpty<NotReflectedInvoice>(_notReflectedRequestState);
                _notReflectedRequestState.State = StateAsync.Complete;
                if (!this._stopWorkers)
                {
                    View.ExecuteInUiThread(c => View.NotReflectedInvoiceLoaded(false));
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
                }
            };

            _notReflectedWorker.Start();
        }

        #endregion

        public object GetInvocesCached(QueryConditions qc, out long totalRowsNumber)
        {
            switch (_requestStateAsyncInvoice.State)
            {
                case StateAsync.Start:
                case StateAsync.None:
                    GetInvoicesAsync(qc);
                    break;
                case StateAsync.Complete:
                    if (qc != null &&
                        _requestStateAsyncInvoice.QueryConditionsSend != null &&
                        (!qc.EqualsBase(_requestStateAsyncInvoice.QueryConditionsSend) ||
                         !QueryConditions.EqualsSorting(qc, _requestStateAsyncInvoice.QueryConditionsSend)))
                    {
                        GetInvoicesAsync(qc);
                    }
                    else
                    {
                        _requestStateAsyncInvoice.QueryConditionsSend = (QueryConditions)qc.Clone();
                        View.GridInvoices.PanelLoadingVisible = false;
                    }
                    break;
            }
            totalRowsNumber = _requestStateAsyncInvoice.TotalRowsNumber;
            return _requestStateAsyncInvoice.ResultData;
        }

        #region Chapters

        public bool IsLoadingCompleted(int chapter)
        {
            return _chapterStatesAsync.Value[chapter].State == StateAsync.Complete;
        }

        private readonly Lazy<Dictionary<int, RequestStateAsync>> _chapterStatesAsync =
            new Lazy<Dictionary<int, RequestStateAsync>>(ChapterStatesAsyncFactory);

        private static Dictionary<int, RequestStateAsync> ChapterStatesAsyncFactory()
        {
            var res = new Dictionary<int, RequestStateAsync>();
            for (var i = 8; i < 13; ++i)
            {
                res[i] = new RequestStateAsync();
            }
            return res;
        }

        #region Загрузка базовых СФ

        public object GetChapterInvoces(int chapter, QueryConditions qc, out long totalRowsNumber)
        {
            var stateData = _chapterStatesAsync.Value[chapter];
            switch (stateData.State)
            {
                case StateAsync.Start:
                case StateAsync.None:
                    GetChapterInvoicesAsync(chapter, qc);
                    break;
                case StateAsync.Complete:
                    if (!QueryConditions.IsEqualsConditions(qc, stateData.QueryConditionsSend))
                    {
                        GetChapterInvoicesAsync(chapter, qc);
                    }
                    else
                    {
                        stateData.QueryConditionsSend = (QueryConditions)qc.Clone();
                        View.GridInvoices.PanelLoadingVisible = false;
                    }
                    break;
            }

            totalRowsNumber = stateData.TotalRowsNumber;
            return stateData.ResultData;
        }

        private void GetChapterInvoicesAsync(int chapter, QueryConditions qc)
        {
            var stateData = _chapterStatesAsync.Value[chapter];
            stateData.Reset<List<InvoiceRelated>>();
            stateData.QueryConditionsSend = (QueryConditions)qc.Clone();

            var worker = new AsyncWorker<int>();
            worker.DoWork += (sender, args) =>
            {
                View.StartExecuteInUiThread(() =>
                {
                    View.StartProgressBar(View.GetChapterGrid(chapter));
                });
                stateData.State = StateAsync.Send;
                long totalRowsNumber;
                stateData.ResultData = GetInvoicesRelated(stateData.QueryConditionsSend, out totalRowsNumber);
                stateData.TotalRowsNumber = totalRowsNumber;
            };
            worker.Complete += (sender, args) =>
            {
                stateData.State = StateAsync.Complete;
                View.StartExecuteInUiThread(() =>
                {
                    View.UpdateData(chapter);
                    View.StopProgressBar(View.GetChapterGrid(chapter));
                });
            };
            worker.Failed += (sender, args) =>
            {
                stateData.State = StateAsync.Complete;
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };

            worker.Start();
        }

        #endregion

        #region Загрузка поясненных СФ
        /*
        public object GetInvocesCached(int chapter, QueryConditions qc, out long totalRowsNumber)
        {
            var stateData = _chapterStatesAsync.Value[chapter];
            switch (stateData.State)
            {
                case StateAsync.Start:
                case StateAsync.None:
                    GetInvoicesAsync(chapter, qc);
                    break;
                case StateAsync.Complete:
                    if (!IsEqualsConditions(qc, stateData.QueryConditionsSend))
                    {
                        GetInvoicesAsync(chapter, qc);
                    }
                    else
                    {
                        requestStateAsyncInvoice.QueryConditionsSend = (QueryConditions)qc.Clone();
                    }
                    break;
            }
            totalRowsNumber = stateData.TotalRowsNumber;
            return requestStateAsyncInvoice.ResultData;
        }

        public void SetStateWorkerStart(int chapter)
        {
            RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(chapter);
            SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
            requestStateAsyncInvoice.State = StateAsync.Start;
        }

        private void SetRuqestStateAsyncInvoceEmpty(RequestStateAsync requestStateAsyncInvoice)
        {
            requestStateAsyncInvoice.ResultData = new List<ExplainInvoice>();
            requestStateAsyncInvoice.TotalRowsNumber = 0;
        }

        private RequestStateAsync GetRequestStateAsyncInvoices(int chapter)
        {
            RequestStateAsync ret;
            if (_chapterStatesAsync.Value.TryGetValue(chapter, out ret))
                return ret;
            ret = new RequestStateAsync();
            SetRuqestStateAsyncInvoceEmpty(ret);
            _chapterStatesAsync.Value.Add(chapter, ret);
            return ret;
        }

        private void GetInvoicesAsync(int chapter, QueryConditions qc)
        {
            var state = GetRequestStateAsyncInvoices(chapter);
            SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
            state.QueryConditionsSend = (QueryConditions)qc.Clone();

            invoiceWorker = new AsyncWorker<int>();
            invoiceWorker.DoWork += (sender, args) =>
            {
                state.State = StateAsync.Send;
                long totalRowsNumber = 0;
                state.ResultData = GetExplainInvoicesDB(_exlpainReplyCardData, chapter, requestStateAsyncInvoice.QueryConditionsSend, out totalRowsNumber);
                state.TotalRowsNumber = totalRowsNumber;
            };
            invoiceWorker.Complete += (sender, args) =>
            {
                state.State = StateAsync.Complete;
                View.ExecuteInUiThread(c => View.UpdateData(chapter));
            };
            invoiceWorker.Failed += (sender, args) =>
            {
                SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
                requestStateAsyncInvoice.State = StateAsync.Complete;
                if (!this.stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };

            invoiceWorker.Start();
        }

        private List<ExplainInvoice> GetExplainInvoicesDB(ExplainReplyCardData exlpainReplyCardData, int chapter, QueryConditions qc, out long totalRowsNumber)
        {
            List<ExplainInvoice> ret = null;
            totalRowsNumber = 0;
            var explainReplyService = GetServiceProxy<IExplainReplyDataService>();
            PageResult<ExplainInvoice> serviceResult = null;
            if (ExecuteServiceCall<OperationResult<PageResult<ExplainInvoice>>>(
                    () => explainReplyService.GetExplainInvoices(_exlpainReplyCardData.ExplainReplyInfo, exlpainReplyCardData.Regim, chapter, qc),
                    (result) => serviceResult = result.Result))
            {
                ret = serviceResult.Rows;
                totalRowsNumber = serviceResult.TotalMatches;
            }
            if (null == ret)
            {
                ret = new List<ExplainInvoice>();
            }
            return ret;
        }
        */
        #endregion

        #endregion

        private void SetRuqestStateAsyncInvoceEmpty<T>(RequestStateAsync state)
        {
            state.ResultData = new List<T>();
            state.TotalRowsNumber = 0;
        }

        private void GetInvoicesAsync(QueryConditions qc)
        {
            SetRuqestStateAsyncInvoceEmpty<DiscrepancyDocumentInvoice>(_requestStateAsyncInvoice);
            _requestStateAsyncInvoice.QueryConditionsSend = (QueryConditions)qc.Clone();

            _invoiceWorker = new AsyncWorker<int>();
            _invoiceWorker.DoWork += (sender, args) =>
            {
                _requestStateAsyncInvoice.State = StateAsync.Send;
                long totalRowsNumber = 0;
                _requestStateAsyncInvoice.ResultData = GetInvoices(_requestStateAsyncInvoice.QueryConditionsSend, out totalRowsNumber);
                _requestStateAsyncInvoice.TotalRowsNumber = totalRowsNumber;
            };
            _invoiceWorker.Complete += (sender, args) =>
            {
                _requestStateAsyncInvoice.State = StateAsync.Complete;
                View.StartExecuteInUiThread(() =>
                {
                    View.GridInvoices.PanelLoadingVisible = false;
                    View.GridInvoices.UpdateData();
                });
            };
            _invoiceWorker.Failed += (sender, args) =>
            {
                SetRuqestStateAsyncInvoceEmpty<DiscrepancyDocumentInvoice>(_requestStateAsyncInvoice);
                _requestStateAsyncInvoice.State = StateAsync.Complete;
                View.StartExecuteInUiThread(() =>
                {
                    if (!this._stopWorkers)
                        View.GridInvoices.PanelLoadingVisible = false;
                });
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };

            _invoiceWorker.Start();
        }

        #endregion

        #region Заполнение данных

        protected void FillDocument(long docId)
        {
            _docInfo = GetDiscrepancyDocumentInfo(docId);
        }

        protected void FillDeclaration(long declarationId)
        {
            _declaration = GetDeclarationDB(_declarationId);
        }

        protected void FillDocumentCalculateInfo(long docId)
        {
            _docCalculateInfo = GetDocumentCalculateInfoDB(docId);
        }

        protected void FillDocumentStatusDates(long docId)
        {
            _dates.Clear();

            int daysWaitReceivedNP = 0;
            int daysWaitAnswer = 0;
            SystemSettings config = GetConfiguration();
            if (config != null)
            {
                daysWaitReceivedNP = config.ClaimDeliveryTimeout.SecondsToDays();
                daysWaitAnswer = config.ReclaimReplyTimeout.SecondsToDays();
            }

            int docType = _docInfo.DocType.EntryId;

            List<DocumentStatusHistory> statusHistories = GetDocumentStatusHistories(docId);
            AddStatusDate(statusHistories, DocStatusCode.Create(docType), _dates, DocStatusDate.Create);
            AddStatusDate(statusHistories, DocStatusCode.SendToSEOD(docType), _dates, DocStatusDate.SendToSEOD);

            AddStatusDate(statusHistories, DocStatusCode.SendToNOExecutor(docType), _dates, DocStatusDate.SendToNOExecutor);
            AddStatusDate(statusHistories, DocStatusCode.ReceivedNOExecutor(docType), _dates, DocStatusDate.ReceivedNOExecutor);

            DateTime? dtSendToNP = AddStatusDate(statusHistories, DocStatusCode.SendToNP(docType), _dates, DocStatusDate.SendToNP);
            if (dtSendToNP != null)
            {
                DateTime dtCalcReceived = dtSendToNP.Value.AddDays(daysWaitReceivedNP);
                _dates.Add(DocStatusDate.CalculateReceived, dtCalcReceived);
            }

            DateTime? dtReceivedNP = AddStatusDate(statusHistories, DocStatusCode.ReceivedNP(docType), _dates, DocStatusDate.ReceivedNP);
            if (dtReceivedNP != null && dtSendToNP.HasValue)
            {
                DateTime dtCalcWaitAnswer = dtSendToNP.Value.AddDays(daysWaitAnswer);
                _dates.Add(DocStatusDate.CalculateWaitAnswer, dtCalcWaitAnswer);
            }

            if (_docInfo.ProlongAnswerDate != null)
            {
                _dates.Add(DocStatusDate.CalculateProlong, _docInfo.ProlongAnswerDate.Value);
            }

            AddStatusDate(statusHistories, DocStatusCode.Close(docType), _dates, DocStatusDate.Close);
        }

        #endregion

        #region Данные

        public DeclarationSummary Declaration
        {
            get
            {
                if (null == _declaration)
                {
                    FillDeclaration(_declarationId);
                }
                return _declaration;
            }
        }

        public DiscrepancyDocumentInfo DocInfo
        {
            get
            {
                return _docInfo;
            }
        }

        public DocumentCalculateInfo DocumentCalculateInfo
        {
            get
            {
                return _docCalculateInfo;
            }
        }

        public Dictionary<string, DateTime> GetDocumentStatusDates()
        {
            return _dates;
        }

        #endregion

        #region Методы работы с сервисами (БД)

        private DeclarationSummary GetDeclarationDB(long declarationId)
        {
            DeclarationSummary ret = null;
            if (ExecuteServiceCall(
                    () => _declarationService.GetDeclaration(declarationId),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        private DocumentCalculateInfo GetDocumentCalculateInfoDB(long docId)
        {
            DocumentCalculateInfo ret = null;
            if (ExecuteServiceCall(
                    () => _discrepancyDocumentService.GetDocumentCalculateInfo(docId),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        private DateTime? AddStatusDate(List<DocumentStatusHistory> statusHistories, int statusCode, Dictionary<string, DateTime> dates, string nameDate)
        {
            DateTime? ret = null;
            DocumentStatusHistory status = statusHistories.Where(p => p.StatusCode == statusCode).FirstOrDefault();
            if (status != null)
            {
                ret = status.StatusDate;
                dates.Add(nameDate, status.StatusDate);
            }
            return ret;
        }

        private List<DocumentStatusHistory> GetDocumentStatusHistories(long docId)
        {
            List<DocumentStatusHistory> docStatusHistories = null;
            if (ExecuteServiceCall(() => _discrepancyDocumentService.GetStatusHistories(docId),
                res =>
                {
                    docStatusHistories = res.Result;
                }))
            {
                return docStatusHistories;
            }
            return new List<DocumentStatusHistory>();
        }

        public SystemSettings GetConfiguration()
        {
            SystemSettings config = null;
            if (ExecuteServiceCall<OperationResult<SystemSettings>>(
                () => base.GetServiceProxy<ISystemSettingsService>().Load(),
                (result) =>
                {
                    config = result.Result;
                }))
            { }
            return config;
        }

        public long? GetDiscrepancyId(string invoiceId, int typeCode)
        {
            long? ret = null;
            if (ExecuteServiceCall(
                    () => _discrepancyDocumentService.GetDiscrepancyId(invoiceId, typeCode, _docInfo.Id),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        public void UpdateUserComment(string userComment)
        {
            if (_docInfo != null && _docInfo.UserComment != userComment)
            {
                OperationResult operRes = null;
                if (ExecuteServiceCall(
                        () => _discrepancyDocumentService.UpdateUserComment(_docInfo.Id, userComment),
                        (result) => operRes = result))
                { }
            }
        }

        private List<DiscrepancyDocumentInvoice> GetInvoices(QueryConditions qc, out long totalRowsNumber)
        {
            List<DiscrepancyDocumentInvoice> ret = null;
            totalRowsNumber = 0;

            PageResult<DiscrepancyDocumentInvoice> serviceResult = null;
            if (ExecuteServiceCall<OperationResult<PageResult<DiscrepancyDocumentInvoice>>>(
                    () => _discrepancyDocumentService.GetInvoices(_docInfo.Id, qc),
                    (result) => serviceResult = result.Result))
            {
                ret = serviceResult.Rows;
                totalRowsNumber = serviceResult.TotalMatches;
            }
            if (null == ret)
            {
                ret = new List<DiscrepancyDocumentInvoice>();
            }
            return ret;
        }

        private List<NotReflectedInvoice> GetNotReflectedInvoices(QueryConditions qc, out long totalRowsNumber)
        {
            List<NotReflectedInvoice> ret = null;
            totalRowsNumber = 0;

            PageResult<NotReflectedInvoice> serviceResult = null;
            if (ExecuteServiceCall(
                    () => _discrepancyDocumentService.GetNotReflectedInvoices(_docInfo.Id, qc),
                    result => serviceResult = result.Result))
            {
                ret = serviceResult.Rows;
                totalRowsNumber = serviceResult.TotalMatches;
            }
            return ret ?? (ret = new List<NotReflectedInvoice>());
        }

        private List<InvoiceRelated> GetInvoicesRelated(QueryConditions qc, out long totalRowsNumber)
        {
            List<InvoiceRelated> ret = null;
            totalRowsNumber = 0;

            PageResult<InvoiceRelated> serviceResult = null;
            if (!ExecuteServiceCall(() => _discrepancyDocumentService.GetInvoicesRelated(_docInfo.Id, qc), result => serviceResult = result.Result))
                return new List<InvoiceRelated>();

            ret = serviceResult.Rows;
            totalRowsNumber = serviceResult.TotalMatches;
            return ret;
        }

        public DiscrepancyDocumentInfo GetDiscrepancyDocumentInfo(long docId)
        {
            DiscrepancyDocumentInfo ddi = null;
            if (ExecuteServiceCall(() => base.GetServiceProxy<IDiscrepancyDocumentService>().GetDocument(docId),
                res =>
                {
                    ddi = res.Result;
                }))
            { }
            return ddi;
        }

        #endregion

        #region Explain card opening

        private new DeclarationSummary GetDeclaration(long declarationId)
        {
            DeclarationSummary ret = null;
            if (ExecuteServiceCall(
                    () => base.GetServiceProxy<IExplainReplyDataService>().GetDeclaration(declarationId),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        private readonly KnpDocumentDetailsViewer _explainReplyViewer;

        public bool ViewExplainOrReply(object explainReply)
        {
            var knpDocument = explainReply as KnpDocument;

            if (knpDocument == null || !knpDocument.AllowView())
            {
                return false;
            }

            _explainReplyViewer.View(knpDocument);
            return true;
        }

        public bool EditExplainOrReply(object explainReply)
        {
            var knpDocument = explainReply as KnpDocument;

            if (knpDocument == null || !knpDocument.AllowEdit())
            {
                return false;
            }

            _explainReplyViewer.Edit(knpDocument);
            return true;
        }

        public void OpenExplainCard(long id)
        {
            var rcd = GetExplainCardRcd(id, _userAccessOpearation);
            ViewExplainReplyDetails(rcd);
        }

        protected void SetAdditionalSortingForExplainReplies(QueryConditions conditions)
        {
            var columnKeyIncomingDate = TypeHelper<ExplainReplyInfo>.GetMemberName(x => x.INCOMING_DATE);
            var columnInComingDate = conditions.Sorting.Where(p => p.ColumnKey == columnKeyIncomingDate).SingleOrDefault();
            if (columnInComingDate != null)
            {
                var columnKeyExplainId = TypeHelper<ExplainReplyInfo>.GetMemberName(x => x.EXPLAIN_ID);
                if (!conditions.Sorting.Any(p => p.ColumnKey == columnKeyExplainId))
                {
                    conditions.Sorting.Add(new ColumnSort() { ColumnKey = columnKeyExplainId, Order = columnInComingDate.Order });
                }
            }
        }

        #endregion
    }

}
