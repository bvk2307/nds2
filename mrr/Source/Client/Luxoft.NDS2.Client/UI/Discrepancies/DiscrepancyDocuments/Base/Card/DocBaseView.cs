﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.CompositeUI;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.Helpers.Explain;
using Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

namespace Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card
{
    public partial class DocBaseView : BaseView
    {
        private readonly DocBasePresenter _presenter;

        public DocBaseView()
        {
            InitializeComponent();
        }

        public DocBaseView(PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo docInfo)
            : base(ctx, wi)
        {
            InitializeComponent();
            _presenter = new DocBasePresenter(this, ctx, wi, docInfo);
            this.Load += Loaded;
        }

        private void Loaded(object sender, EventArgs e)
        {
            _presenter.RefreshEKP();
        }

        public virtual GridControl GridInvoices
        {
            get { return null; }
        }
        public virtual ExplainListView GridExplain
        {
            get { return null; }
        }
        public virtual void GridControlRatioLoad() {}

        public virtual void GridControlRatioSetLoadingVisibility(bool isVisible) {}

        public virtual void LoadData()
        {
        }

        public virtual void LoadHistoryStatusDates()
        {
        }

        public virtual void LoadCalculateInfo()
        {
        }

        public virtual GridControl GetChapterGrid(int chapter)
        {
            return null;
        }

        protected GridSetup GetGridInvoiceSetup(string key)
        {
            var setup = _presenter.CommonSetup(key);
            var helper = new GridSetupHelper<DiscrepancyDocumentInvoice>();

            setup.Columns.AddRange(GetGroupsInvoice(helper));

            setup.OrderColumns();

            setup.QueryCondition.Sorting.Add(new ColumnSort { ColumnKey = TypeHelper<Invoice>.GetMemberName(x => x.CHAPTER), Order = ColumnSort.SortOrder.Asc });
            setup.QueryCondition.Sorting.Add(new ColumnSort { ColumnKey = TypeHelper<Invoice>.GetMemberName(x => x.INVOICE_DATE), Order = ColumnSort.SortOrder.Asc });

            setup.CellToolTips = GetCellToolTipsBase();

            List<Action<Infragistics.Win.UltraWinGrid.UltraGridRow>> rowActions = new List<Action<Infragistics.Win.UltraWinGrid.UltraGridRow>>();
            rowActions.Add((row) =>
            {
                if (row.ListObject is DiscrepancyDocumentInvoice)
                {
                    DiscrepancyDocumentInvoice inv = (DiscrepancyDocumentInvoice)row.ListObject;
                    if (inv != null)
                    {
                        SetCellByDiscrepancyStatus(row.Cells[TypeHelper<DiscrepancyDocumentInvoice>.GetMemberName(t => t.GAP_AMOUNT)], inv.GAP_STATUS);
                        SetCellByDiscrepancyStatus(row.Cells[TypeHelper<DiscrepancyDocumentInvoice>.GetMemberName(t => t.NDS_AMOUNT)], inv.NDS_STATUS);
                    }
                }
            });
            setup.RowActions = rowActions;

            return setup;
        }

        private string InvoiceExplainTitleConvert(object inv)
        {
            return InvoiceExplainHelper.CreateExplainToolTip((Invoice)inv);
        }

        private Dictionary<string, Func<object, string>> GetCellToolTipsBase()
        {
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();

            Func<object, string> funcExplainTitle = InvoiceExplainTitleConvert;
            cellToolTips.Add(TypeHelper<Invoice>.GetMemberName(t => t.IS_CHANGED), funcExplainTitle);

            return cellToolTips;
        }

        private void SetCellByDiscrepancyStatus(Infragistics.Win.UltraWinGrid.UltraGridCell cell, int? status)
        {
            if (status != null && status == 2)
            {
                cell.Appearance.FontData.Strikeout = DefaultableBoolean.True;
            }
            else 
            {
                cell.Appearance.FontData.Strikeout = DefaultableBoolean.False;
            }
        }

        protected void DrawPen(ColumnDefinition column)
        {
            var drw = new DictionaryInvoiceStateRecord
            {
                InvoiceState = ExplainInvoiceState.ChangingInThisExplain,
                ColorARGB = 0,
                Description = ""
            };

            var drw_conf = new DictionaryInvoiceStateRecord
            {
                InvoiceState = ExplainInvoiceState.ProcessedInThisExplain,
                ColorARGB = 0,
                Description = ""
            };

            column.Caption = "";
            column.ToolTip = "Признак редактирования записи в ходе полученного пояснения от НП";
            column.DontHide = true;

            column.CustomConditionValueAppearance = () =>
            {
                var result = new ConditionValueAppearance();
                var condition = new OperatorCondition(ConditionOperator.Equals, 1);
                var appearance = new Appearance
                {
                    Image = drw.GetIcon(this),
                    ImageHAlign = HAlign.Center,
                    ImageVAlign = VAlign.Middle,
                    ForegroundAlpha = Alpha.Transparent
                };
                result.Add(condition, appearance);

                var condition_conf = new OperatorCondition(ConditionOperator.Equals, 0);
                var appearance_conf = new Appearance
                {
                    Image = drw_conf.GetIcon(this),
                    ImageHAlign = HAlign.Center,
                    ImageVAlign = VAlign.Middle,
                    ForegroundAlpha = Alpha.Transparent
                };
                result.Add(condition_conf, appearance_conf);

                return result;
            };
        }

        protected List<ColumnBase> GetGroupsInvoice(GridSetupHelper<DiscrepancyDocumentInvoice> helper)
        {
            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            var cChanged = helper.CreateColumnDefinition(data => data.IS_CHANGED, x =>
            {
                x.RowSpan = 2;
                DrawPen(x);
            });

            var cSourceSF = helper.CreateColumnDefinition(o => o.CHAPTER, d => { d.Caption = "Источник"; d.ToolTip = "Источник записи о СФ"; d.RowSpan = 2; });

            var cSFNumber = helper.CreateColumnDefinition(data => data.INVOICE_NUM, d => { d.Caption = "Номер СФ"; d.ToolTip = "Номер СФ"; d.Width = 40; d.RowSpan = 2; });

            var cSFDate = helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Caption = "Дата СФ"; d.ToolTip = "Дата СФ"; d.Width = 40; d.RowSpan = 2; });

            var cAmountSF = helper.CreateColumnDefinition(o => o.CALC_PRICE_WITH_NDS, d => { d.Caption = "Сумма по СФ, руб."; d.ToolTip = "Сумма по СФ с учетом НДС"; d.FormatInfo = formatInfoZeroEmpty; d.Align = ColumnAlign.Right; d.RowSpan = 2; });

            var cAmountNDS = helper.CreateColumnDefinition(o => o.CALC_PRICE_NDS, d => { d.Caption = "Сумма НДС, руб."; d.ToolTip = "Сумма НДС по СФ"; d.FormatInfo = formatInfoZeroEmpty; d.Align = ColumnAlign.Right; d.RowSpan = 2; });

            var gPVPDiscrepancy = new ColumnGroupDefinition { Caption = "Сумма расхождения"};

            var col = helper.CreateColumnDefinition(data => data.GAP_AMOUNT, d =>
            {
                d.Caption = "Разрыв";
                d.ToolTip = "Сумма расхождения";
                d.FormatInfo = formatInfoZeroEmpty;
                d.Align = ColumnAlign.Right;
            });
            gPVPDiscrepancy.Columns.Add(col);
            col = helper.CreateColumnDefinition(data => data.NDS_AMOUNT, d =>
            {
                d.Caption = "Проверка НДС";
                d.ToolTip = "Сумма расхождения";
                d.FormatInfo = formatInfoZeroEmpty;
                d.Align = ColumnAlign.Right;
            });
            gPVPDiscrepancy.Columns.Add(col);
            col = helper.CreateColumnDefinition(data => data.DISPLAY_FULL_TAX_PERIOD, d =>
            {
                d.Caption = "Период отображения записи";
                d.ToolTip = "Отчетный период НД, в которой данная запись была отражена";
            });
            gPVPDiscrepancy.Columns.Add(col);

            return new List<ColumnBase> { cChanged, cSourceSF, cSFNumber, cSFDate, cAmountSF, cAmountNDS, gPVPDiscrepancy };
        }

        protected void OpenDeclaration(DiscrepancyDocumentInvoice value)
        {
            if (value != null)
            {
                var declarationInfoSource = new DeclarationInfoSource();
                DeclarationSummary decl = _presenter.Declaration;

                declarationInfoSource.IsDestination = true;
                declarationInfoSource.ChapterGridNumberDestination = value.CHAPTER;
                declarationInfoSource.InvoiceRowKey = value.ROW_KEY;
                declarationInfoSource.InvoiceNum = value.INVOICE_NUM;

                _presenter.ViewDeclarationDetails(decl.DECLARATION_VERSION_ID, declarationInfoSource);
            }
        }

        protected void OpenDiscrepancy(Invoice value, DiscrepancyType typeCode)
        {
            if (value != null)
            {
                long? discrepancyId = _presenter.GetDiscrepancyId(value.ROW_KEY, (int)typeCode);
                if (discrepancyId != null)
                {
                    _presenter.ViewDiscrepancyDetails(discrepancyId.Value);
                }
            }
        }

        public virtual void UpdateData(int chapter) { }

        public virtual void UpdateGridsData()
        {
            GridInvoices.UpdateData();
            GridControlRatioLoad();
        }

        public virtual void NotReflectedInvoiceLoaded(bool isSuccessful) { }

        public void StartProgressBar(GridControl grid)
        {
            grid.PanelLoadingVisible = true;
        }

        public void StopProgressBar(GridControl grid)
        {
            grid.PanelLoadingVisible = false;
        }

    }
}