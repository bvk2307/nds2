﻿using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    partial class View
    {
        private DataGridPresenter<NotReflectedInvoice> _notReflectedPresenter;

        private void InitializeGridNotReflected()
        {
            //return; // см https://sentinel2.luxoft.com/sen/issues/browse/GNIVC_NDS2-1248

            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<NotReflectedInvoice>(gridNotReflected);

            var group = helper.CreateGroup("Контрагент");
            group.Columns.Add(helper.CreateTextColumn(x => x.INN));
            group.Columns.Add(helper.CreateTextColumn(x => x.KPP));
            group.Columns.Add(helper.CreateTextColumn(x => x.NAME).Configure(c => { c.DisableFilter = true; c.DisableSort = true;}));
            setup.Columns.Add(group);

            setup.Columns.Add(helper.CreateTextColumn(x => x.INVOICE_NUM, 2).Configure(d =>
            {
                d.Caption = "Номер СФ";
                d.HeaderToolTip = "Номер счета-фактуры";
            }));
            setup.Columns.Add(helper.CreateTextColumn(x => x.INVOICE_DATE, 2).Configure(d =>
            {
                d.Caption = "Дата СФ";
                d.HeaderToolTip = "Дата счета-фактуры";
            }));
            setup.Columns.Add(helper.CreateTextColumn(x => x.DISCREPANCY_STATUS, 2));

            var qc = new QueryConditions();
            qc.Sorting.Add(new ColumnSort { ColumnKey = TypeHelper<NotReflectedInvoice>.GetMemberName(x => x.INN), Order = ColumnSort.SortOrder.Asc });
            qc.Sorting.Add(new ColumnSort { ColumnKey = TypeHelper<NotReflectedInvoice>.GetMemberName(x => x.INVOICE_DATE), Order = ColumnSort.SortOrder.Asc });

            var pager = new PagerStateViewModel();
            gridNotReflected.WithPager(new DataGridPageNavigator(pager)).InitColumns(setup);
            gridNotReflected.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;

            gridNotReflected.RowDoubleClicked += i => ViewDeclarationDetailsFromInvoice((NotReflectedInvoice)i);

            _notReflectedPresenter = 
                new PagedDataGridPresenter<NotReflectedInvoice>(
                    gridNotReflected, 
                    pager, 
                    _presenter.GetInvoicesNotReflected,
                    qc);

            _notReflectedPresenter.Init(_presenter.SettingsProvider(string.Format("{0}_notReflected", GetType())));
            _notReflectedPresenter.Load();
        }

        public override void NotReflectedInvoiceLoaded(bool isSuccessful)
        {
            if (isSuccessful)
                _notReflectedPresenter.Load();
            gridNotReflected.PanelLoadingVisible = false;
        }

        private void NotReflectedContragentClick(object sender, EventArgs e)
        {
            var invoice = gridNotReflected.GetCurrentItem<NotReflectedInvoice>();
            if (invoice == null)
                return;
            _presenter.ViewTaxPayerByKppOriginal(invoice.INN, invoice.KPP);
        }

        private void NotReflectedComparedRecordClick(object sender, EventArgs e)
        {
            var invoice = gridNotReflected.GetCurrentItem<NotReflectedInvoice>();
            if (invoice == null)
                return;
            ViewDeclarationDetailsFromInvoice(invoice);
        }


        public void ViewDeclarationDetailsFromInvoice(NotReflectedInvoice inv)
        {
            var opener = _presenter.GetDeclarationCardOpener();
            if (!string.IsNullOrEmpty(inv.COMPARE_ROW_KEY))
            {
                opener.Open(inv.COMPARE_ROW_KEY, GetNavigationDirection(inv.CHAPTER), inv.INN);
            }
            else
            {
                opener.Open(inv.INN, inv.KPP, _presenter.Declaration.FISCAL_YEAR, _presenter.Declaration.TAX_PERIOD);
            }
        }

        private Direction GetNavigationDirection(int chapter)
        {
            var part = (DeclarationPart)chapter;
            switch (part)
            {
                case DeclarationPart.R8:
                case DeclarationPart.R11:
                case DeclarationPart.R12:
                    return Direction.Purchase;

                case DeclarationPart.R9:
                case DeclarationPart.R10:
                    return Direction.Sales;
            }
            return Direction.Invariant;
        }
    }
}
