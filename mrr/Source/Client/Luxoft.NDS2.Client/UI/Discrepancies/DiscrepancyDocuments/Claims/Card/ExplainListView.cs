﻿using Infragistics.Win.UltraWinGrid;
using ColumnHeader = Infragistics.Win.UltraWinGrid.ColumnHeader;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public partial class ExplainListView : UserControl, IExplainListView
    {
        # region .ctor
        public ExplainListView()
        {
            InitializeComponent();
            InitColumnsCollection();
            GridEvents = new NullGridEventsHandler();
            _grid.SetRowContextMenu(_gridMenu);
        }

        private void InitColumnsCollection()
        {
            var columns = new GridColumnsCollection();
          
            foreach (var ultraColumn in _grid.DisplayLayout.Bands[0].Columns)
            {
                if (ultraColumn.Key 
                    != TypeHelper<KnpDocument>.GetMemberName(x => x.IsHighlighted))
                {
                    columns.Add(
                        new SingleGridColumnView(
                            ultraColumn, 
                            new GridColumnFilterView(ultraColumn)));
                    
                }
            }
            Columns = columns;
        }

        # endregion

        # region IExplainListView

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public object SelectedItem
        {
            get 
            {
                if (_grid.ActiveRow == null)
                {
                    return null;
                }

                return _grid.ActiveRow.ListObject; 
            }
        }

        public void SelectFirstRow()
        {
            if (_grid.Rows.Any())
                _grid.Rows[0].Activate();
        }

        public event EventHandler<ExplainListItemEventArgs> ItemSelected;

        public event EventHandler ItemViewing;

        public event EventHandler ItemEditing;

        public event EventHandler ItemDoubleClick;

        public IGridEventsHandler GridEvents
        {
            get;
            private set;
        }

        public IGridColumnsCollection Columns
        {
            get;
            private set;
        }

        public RowsCollection Rows
        {
            get { return _grid.Rows; }
        }

        #endregion
        private void ExplainListDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (ItemDoubleClick != null)
                ItemDoubleClick(sender, e);
        }

        private void ExplainListCtxViewClick(object sender, EventArgs e)
        {
            if (ItemViewing != null)
                ItemViewing(sender, e);
        }

        private void ExplainListCtxEditClick(object sender, EventArgs e)
        {
            if (ItemEditing != null)
                ItemEditing(sender, e);
        }

        private void ExplainListClickCell(object sender, ClickCellEventArgs e)
        {
            _editExplain.Visible = true;
            _viewExplain.Visible = true;

            if (ItemSelected != null)
            {
                var newArg= new ExplainListItemEventArgs();  
                ItemSelected(sender, newArg);
                _editExplain.Visible = newArg.AllowEdit;
                _viewExplain.Visible = newArg.AllowView;
            }
        }
    }
}
