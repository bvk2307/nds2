﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using ColumnFilter = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Bargain;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    partial class View
    {
        #region Width constants

        private const int WIDTH_INVOICE = 40;
        private const int WIDTH_DATE = 40;
        private const int WIDTH_NAME = 300;
        private const int WIDTH_INN = 30;
        private const int WIDTH_KPP = 30;
        private const int WIDTH_MONEY = 100;

        #endregion

        private readonly Dictionary<int, QueryConditions> _chaptersGridQueryConditions = new Dictionary<int, QueryConditions>();
        private readonly Dictionary<int, FilterQuery> _chaptersGridFilter = new Dictionary<int, FilterQuery>();
        private Dictionary<int, int> chapterMapTabControls = new Dictionary<int, int>();

        // Entry point
        private void InitializeChapters()
        {
            #region FIlter by chapter

            for (var i = 8; i < 13; ++i)
            {
                _chaptersGridQueryConditions[i] = new QueryConditions
                {
                    Filter =
                    {
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<InvoiceRelated>.GetMemberName(x => x.CHAPTER),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = new List<ColumnFilter> { new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = i} }
                        }
                    }
                };
            }

            #endregion

            InitChapterMapTabControls();
            InitializeChapter8();
            InitializeChapter9();
            InitializeChapter10();
            InitializeChapter11();
            InitializeChapter12();
        }

        private void DrawNumber(ColumnDefinition column)
        {
            column.Align = ColumnAlign.Right;
            column.Width = WIDTH_INVOICE;
        }

        private void DrawMoney(ColumnDefinition column)
        {
            column.Align = ColumnAlign.Right;
            column.FormatInfo = _zeroFormat;
            column.Width = WIDTH_MONEY;
        }

        private void SetChaptersQueryConditions(GridSetup setup, int chapter)
        {
            QueryConditions conditions;
            if (_chaptersGridQueryConditions.TryGetValue(chapter, out conditions))
            {
                setup.QueryCondition = conditions;
            }

            setup.QueryCondition.Sorting.Add(new ColumnSort
            {
                ColumnKey = chapter == 12
                    ? TypeHelper<InvoiceRelated>.GetMemberName(x => x.SELLER_INVOICE_DATE)
                    : TypeHelper<InvoiceRelated>.GetMemberName(x => x.INVOICE_NUM),
                Order = ColumnSort.SortOrder.Asc
            });

            setup.AllowSaveSettings = false;
        }

        private void InitChapterMapTabControls()
        {
            chapterMapTabControls.Add(tabInvoiceChapter8.Tab.VisibleIndex, 8); 
            chapterMapTabControls.Add(tabInvoiceChapter9.Tab.VisibleIndex, 9);
            chapterMapTabControls.Add(tabInvoiceChapter10.Tab.VisibleIndex, 10);
            chapterMapTabControls.Add(tabInvoiceChapter11.Tab.VisibleIndex, 11);
            chapterMapTabControls.Add(tabInvoiceChapter12.Tab.VisibleIndex, 12);
        }

        public override GridControl GetChapterGrid(int chapter)
        {
            switch (chapter)
            {
                case 8: return gridChapter8;
                case 9: return gridChapter9;
                case 10: return gridChapter10;
                case 11: return gridChapter11;
                case 12: return gridChapter12;
                default: return null;
            }
        }

        #region Row action

        public Dictionary<string, object> ToDictionary<T>(T entity)
        {
            var properties = entity.GetType().GetProperties();
            return properties.ToDictionary(itemProp => itemProp.Name, itemProp => itemProp.GetValue(entity, null));
        }

        private object GetFieldEntity(Dictionary<string, object> filedsEntity, string fieldKey)
        {
            object fieldValue;
            filedsEntity.TryGetValue(fieldKey, out fieldValue);
            return fieldValue;
        }

        private void GridRowAction(UltraGridRow row)
        {
            var invoice = row.ListObject as InvoiceRelated;
            if (invoice == null || !invoice.IsChildren)
                return;

            var appearanceActiveRow = row.Cells[0].Band.Override.ActiveRowAppearance;
            var appearanceActiveCell = row.Cells[0].Band.Override.ActiveCellAppearance;
            appearanceActiveRow.Reset();
            appearanceActiveCell.Reset();
            var filedsEntityOriginal = ToDictionary(invoice.Parent);

            foreach (var cell in row.Cells)
            {
                if (!cell.Column.IsVisibleInLayout)
                    continue;
                var columnKey = cell.Column.Key;
                var fieldValue = cell.Value;
                var fieldValueOriginal = GetFieldEntity(filedsEntityOriginal, columnKey);

                var isEqals = fieldValueOriginal != null && fieldValueOriginal.Equals(fieldValue);

                if (!isEqals)
                {
                    cell.Appearance.ForeColor = System.Drawing.Color.Blue;
                    cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                }
                else
                {
                    cell.Appearance.ResetForeColor();
                    cell.Appearance.ResetFontData();
                }
            }
        }

        #endregion

        #region CellTooltip

        DictionaryOper dicOper = new DictionaryOper();
        DictionaryCurr dicCurr = new DictionaryCurr();
        DictionaryBargain dicBargain = new DictionaryBargain();
        void InitDict()
        {
            dicOper = _presenter.Oper;
            dicCurr = _presenter.Curr;
            dicBargain = _presenter.Barg;
        }
        private string InvoiceConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OPERATION_CODE;
            return dicOper.NAME(opcode);
        }

        private string InvoiceCurrencyConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OKV_CODE;
            return dicCurr.NAME(opcode);
        }

        private string InvoiceBargainConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = ((int)temp.DEAL_KIND_CODE).ToString();
            return dicBargain.NAME(opcode);
        }

        private Dictionary<string, Func<object, string>> GetCellToolTipsBase()
        {
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();

            InitDict();
            Func<object, string> funcOper = InvoiceConvert;
            Func<object, string> funcCurr = InvoiceCurrencyConvert;
            Func<object, string> funcBarg = InvoiceBargainConvert;
            cellToolTips.Add("OPERATION_CODE", funcOper);
            cellToolTips.Add("OKV_CODE", funcCurr);
            cellToolTips.Add("DEAL_KIND_CODE", funcBarg);

            return cellToolTips;
        }

        #endregion

        #region Chapter 8

        private void InitializeChapter8()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGrid8", GetType()));
            setup.GetData = GetDataC8;

            var helper = new GridSetupHelper<InvoiceRelated>();

            setup.Columns.AddRange(GetGroupsC8(helper));

            var bandGroups = new List<ColumnBase>(GetGroupsC8(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Columns = bandGroups.Select(x => x as ColumnDefinition).Where(x => x!= null).ToList(),
                Groups = bandGroups.Select(x => x as ColumnGroupDefinition).Where(x => x!= null).ToList()
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 8);

            setup.OrderColumns();

            gridChapter8.Setup = setup;
            gridChapter8.Title = "Раздел 8";
            gridChapter8.Band_EditMode = true;
            gridChapter8.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnBase> GetGroupsC8(GridSetupHelper<InvoiceRelated> helper)
        {
            var rets = new List<ColumnBase>();

            ColumnGroupDefinition groupIsChange = new ColumnGroupDefinition() { Caption = string.Empty };
            groupIsChange.Columns.Add(helper.CreateColumnDefinition(o => o.IS_CHANGED, d => { d.RowSpan = 2; DrawPen(d); }));
            rets.Add(groupIsChange);

            ColumnGroupDefinition groupOrdinalNumber = new ColumnGroupDefinition() { Caption = string.Empty };
            groupOrdinalNumber.Columns.Add(helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => d.RowSpan = 2));
            rets.Add(groupOrdinalNumber);

            ColumnGroupDefinition groupOperationCode = new ColumnGroupDefinition() { Caption = string.Empty };
            groupOperationCode.Columns.Add(helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }));
            rets.Add(groupOperationCode);

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "СФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                    helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_NUM, DrawNumber),
                    helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WIDTH_DATE)
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CORRECTION_NUM, DrawNumber),
                    helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИКСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, DrawNumber),
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Документ, подтверждающий уплату налога",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM_VIEW, d => { DrawNumber(d); 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE_VIEW, d => { d.Width = WIDTH_DATE; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });

            ColumnGroupDefinition buyAcceptDate = new ColumnGroupDefinition() { Caption = string.Empty };
            buyAcceptDate.Columns.Add(helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE_VIEW, d => { d.Width = WIDTH_DATE;
                d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }));
            rets.Add(buyAcceptDate);

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о продавце",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.SELLER_INN, d => { d.Width = WIDTH_INN; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.SELLER_KPP, d => { d.Width = WIDTH_KPP; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.SELLER_NAME, d => { d.Width = WIDTH_NAME; d.Visibility = ColumnVisibility.Disabled;})
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о посреднике",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = WIDTH_INN; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = WIDTH_KPP; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BROKER_NAME, d => { d.Width = WIDTH_NAME; d.Visibility = ColumnVisibility.Disabled;})
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = string.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM,
                    d =>
                    {
                        d.Caption = "Регистрационный № ТД\r\n(стр. 150)";
                        d.ToolTip = "Регистрационный номер таможенной декларации";
                        d.AllowRowFiltering = false;
                        d.SortIndicator = false;
                        d.Width = WIDTH_INVOICE;
                        d.RowSpan = 2;
                    })
                }
            });

            ColumnGroupDefinition groupOkvCode = new ColumnGroupDefinition() { Caption = string.Empty };
            groupOkvCode.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => d.RowSpan = 2));
            rets.Add(groupOkvCode);

            ColumnGroupDefinition groupPriceBuyAmount = new ColumnGroupDefinition() { Caption = string.Empty };
            groupPriceBuyAmount.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d => {d.RowSpan=2; DrawMoney(d);}));
            rets.Add(groupPriceBuyAmount);

            ColumnGroupDefinition groupPriceBuyNdsAmount = new ColumnGroupDefinition() { Caption = string.Empty };
            groupPriceBuyNdsAmount.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.RowSpan = 2; DrawMoney(d); }));
            rets.Add(groupPriceBuyNdsAmount);

            ColumnGroupDefinition groupTaxPeriod = new ColumnGroupDefinition() { Caption = string.Empty };
            groupTaxPeriod.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.RowSpan = 2; }));
            rets.Add(groupTaxPeriod);

            return rets.ToArray();
        }

        #endregion

        #region Chapter 9

        private void InitializeChapter9()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR9", GetType()));
            setup.GetData = GetDataC9;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC9(helper));

            var bandGroups = new List<ColumnBase>(GetGroupsC9(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Columns = bandGroups.Select(x => x as ColumnDefinition).Where(x => x != null).ToList(),
                Groups = bandGroups.Select(x => x as ColumnGroupDefinition).Where(x => x != null).ToList()
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 9);

            setup.OrderColumns();

            gridChapter9.Setup = setup;
            gridChapter9.Title = "Раздел 9";
            gridChapter9.Band_EditMode = true;
            gridChapter9.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnBase> GetGroupsC9(GridSetupHelper<InvoiceRelated> helper)
        {
            var rets = new List<ColumnBase>();

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.IS_CHANGED, d => { d.RowSpan=2; DrawPen(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => { d.RowSpan=2; DrawNumber(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "СФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = string.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM,
                    d =>
                    {
                        d.Caption = "Регистрационный № ТД\r\n(стр. 035)";
                        d.ToolTip = "Регистрационный номер таможенной декларации";
                        d.AllowRowFiltering = false;
                        d.SortIndicator = false;
                        d.Width = WIDTH_INVOICE;
                        d.RowSpan = 2;
                    })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d=>{ DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИКСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Документ, подтверждающий уплату налога",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM_VIEW, d=> { DrawNumber(d); d.RowSpan = 2; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE_VIEW, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о покупателе",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Width = WIDTH_INN; d.RowSpan = 2; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Width = WIDTH_KPP; d.RowSpan = 2; 
                        d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    //helper.CreateColumnDefinition(o => o.BUYER_NAME, d => d.Width = WIDTH_NAME)
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о посреднике",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.ToolTip = "ИНН посредника (комиссионера, агента)"; 
                        d.Width = WIDTH_INN; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.ToolTip = "КПП посредника (комиссионера, агента)";
                        d.Width = WIDTH_KPP; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false;}),
                    //helper.CreateColumnDefinition(o => o.BROKER_NAME, d => { d.ToolTip = "Наименование посредника (комиссионера, агента)"; d.Width = WIDTH_NAME; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.OKV_CODE, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Стоимость продаж с НДС",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_SELL_IN_CURR, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.PRICE_SELL, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_SELL_18, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_SELL_10, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_SELL_0, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сумма НДС",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_NDS_18, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.PRICE_NDS_10, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => {d.RowSpan = 2; })
                }
            });

            return rets;
        }

        #endregion

        #region Chapter 10

        private void InitializeChapter10()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR10", GetType()));
            setup.GetData = GetDataC10;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC10(helper));

            var bandGroups = new List<ColumnBase>(GetGroupsC10(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Columns = bandGroups.Select(x => x as ColumnDefinition).Where(x => x != null).ToList(),
                Groups = bandGroups.Select(x => x as ColumnGroupDefinition).Where(x => x != null).ToList()
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 10);

            setup.OrderColumns();

            gridChapter10.Setup = setup;
            gridChapter10.Title = "Раздел 10";
            gridChapter10.Band_EditMode = true;
            gridChapter10.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnBase> GetGroupsC10(GridSetupHelper<InvoiceRelated> helper)
        {
            var rets = new List<ColumnBase>();

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.IS_CHANGED, d => { d.RowSpan=2; DrawPen(d); })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => { d.RowSpan=2; DrawNumber(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.CREATE_DATE, d => { d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "СФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИКСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false;})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о покупателе",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Width = WIDTH_INN; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Width = WIDTH_KPP; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    //helper.CreateColumnDefinition(o => o.BUYER_NAME, definition => definition.Width = WIDTH_NAME)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о посреднической деятельности продавца",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_INN, d => {d.Caption = "ИНН"; d.Width = WIDTH_INN; d.RowSpan = 2;}),
                    helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_KPP, d => {d.Caption = "КПП"; d.Width = WIDTH_KPP; d.RowSpan = 2;}),
                    helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_NUM, d => { d.Caption = "№ СФ"; d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_DATE, d => { d.Caption = "Дата СФ";d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.OKV_CODE, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_TOTAL, d => { d.RowSpan = 2; DrawMoney(d); })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, d => { d.RowSpan = 2; DrawMoney(d);})
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Разница стоимости с НДС по КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            }); 
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Разница НДС по КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.RowSpan = 2; })
                }
            });
            return rets;
        }

        #endregion

        #region Chapter 11

        private void InitializeChapter11()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR11", GetType()));
            setup.GetData = GetDataC11;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC11(helper));

            var bandGroups = new List<ColumnBase>(GetGroupsC11(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Columns = bandGroups.Select(x => x as ColumnDefinition).Where(x => x != null).ToList(),
                Groups = bandGroups.Select(x => x as ColumnGroupDefinition).Where(x => x != null).ToList()
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 11);

            setup.OrderColumns();

            gridChapter11.Setup = setup;
            gridChapter11.Title = "Раздел 11";
            gridChapter11.Band_EditMode = true;
            gridChapter11.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnBase> GetGroupsC11(GridSetupHelper<InvoiceRelated> helper)
        {
            var rets = new List<ColumnBase>();

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.IS_CHANGED, d => {d.RowSpan = 2; DrawPen(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.RECEIVE_DATE, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "СФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d); d.RowSpan = 2;}),
                    helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "ИКСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { DrawNumber(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = WIDTH_DATE; d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о продавце",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.SELLER_INN, d => { d.Width = WIDTH_INN; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.SELLER_KPP, d => { d.Width = WIDTH_KPP; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    //helper.CreateColumnDefinition(o => o.SELLER_NAME, definition => definition.Width = WIDTH_NAME)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о субкомиссионере",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = WIDTH_INN; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = WIDTH_KPP; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    //helper.CreateColumnDefinition(o => o.BROKER_NAME, definition => definition.Width = WIDTH_NAME)
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.DEAL_KIND_CODE, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.OKV_CODE, d => d.RowSpan = 2)
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_TOTAL, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, d => {d.RowSpan = 2; DrawMoney(d);})
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Разница стоимости с НДС по КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Разница НДС по КСФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, d => { DrawMoney(d); d.RowSpan = 2; }),
                    helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, d => { DrawMoney(d); d.RowSpan = 2; })
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.RowSpan = 2; })
                }
            });

            return rets.ToArray();
        }

        #endregion

        #region Chapter 12

        private void InitializeChapter12()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR12", GetType()));
            setup.GetData = GetDataC12;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC12(helper));

            var bandGroups = new List<ColumnBase>(GetGroupsC12(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Columns = bandGroups.Select(x => x as ColumnDefinition).Where(x => x != null).ToList(),
                Groups = bandGroups.Select(x => x as ColumnGroupDefinition).Where(x => x != null).ToList()
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            setup.OrderColumns();

            SetChaptersQueryConditions(setup, 12);

            gridChapter12.Setup = setup;
            gridChapter12.Title = "Раздел 12";
            gridChapter12.Band_EditMode = true;
            gridChapter12.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnBase> GetGroupsC12(GridSetupHelper<InvoiceRelated> helper)
        {
            var rets = new List<ColumnBase>();

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.IS_CHANGED, d => {d.RowSpan = 2; DrawPen(d);})
                }
            });
            
            rets.Add(new ColumnGroupDefinition
            {
                Caption = "СФ",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Caption = "№"; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d); d.RowSpan = 2;}),
                    helper.CreateColumnDefinition(o => o.SELLER_INVOICE_DATE, d => { d.Caption = "Дата"; d.ToolTip = "Дата счета-фактуры продавца"; d.RowSpan = 2; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = "Сведения о покупателе",
                Columns =
                {
                    helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Caption = "ИНН"; d.ToolTip = "ИНН покупателя"; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; }),
                    helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Caption = "КПП"; d.ToolTip = "КПП покупателя"; d.RowSpan = 2; d.AllowRowFiltering = false; d.SortIndicator = false; })
                }
            });

            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.Caption = "Код валюты"; d.ToolTip = "Код валюты по ОКВ"; d.RowSpan = 2;})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d => { d.Caption = "Стоимость без НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав без налога - всего"; d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.Caption = "Сумма НДС"; d.ToolTip = "Сумма налога, предъявляемая покупателю"; d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d => { d.Caption = "Стоимость с НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав с налогом - всего"; d.RowSpan = 2; DrawMoney(d);})
                }
            });
            rets.Add(new ColumnGroupDefinition
            {
                Caption = String.Empty,
                Columns = 
                {
                    helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.RowSpan = 2; })
                }
            });
            return rets.ToArray();
        }

        #endregion

        #region Data requests

        private object GetChaptersData(int chapter, QueryConditions conditions, out long totalRows)
        {
            var grid = GetChapterGrid(chapter);

            if (ultraTabControlMain.SelectedTab != null
                && chapterMapTabControls.Keys.Contains(ultraTabControlMain.SelectedTab.Index)
                && chapterMapTabControls[ultraTabControlMain.SelectedTab.Index] == chapter)
            {
                FilterQuery filter;
                if (_chaptersGridFilter.TryGetValue(chapter, out filter))
                {
                    filter.UserDefined = true;
                    conditions.Filter.RemoveAll(x => x.ColumnName == filter.ColumnName);
                    conditions.Filter.Add(filter);
                    _chaptersGridFilter.Remove(chapter);
                }
                var result = _presenter.GetChapterInvoces(chapter, conditions, out totalRows);
                return result;
            }

            grid.PanelLoadingVisible = false;
            totalRows = 0;
            List<InvoiceRelated> listResult = null;
            return listResult;
        }

        public object GetDataC8(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(8, conditions, out totalRows);
        }

        public object GetDataC9(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(9, conditions, out totalRows);
        }

        public object GetDataC10(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(10, conditions, out totalRows);
        }

        public object GetDataC11(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(11, conditions, out totalRows);
        }

        public object GetDataC12(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(12, conditions, out totalRows);
        }

        public override void UpdateData(int chapter)
        {
            var grid = GetChapterGrid(chapter);
            grid.UpdateData();
            grid.PanelLoadingVisible = false;
        }

        #endregion

        #region Context menu handlers

        public GridControl GetCurrentGridInvoices()
        {
            GridControl grid = null;
            int tabIndex = ultraTabControlMain.SelectedTab.Index;
            if (chapterMapTabControls.Keys.Contains(tabIndex))
            {
                int chapter = chapterMapTabControls[tabIndex];
                grid = GetChapterGrid(chapter);
            }
            return grid;
        }

        private void cmiOpenBuyer_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(ultraTabControlMain.Tabs.IndexOf(ultraTabControlMain.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewTaxPayerByKppOriginal(x.BUYER_INN, x.BUYER_KPP);
        }

        private void cmiOpenSeller_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(ultraTabControlMain.Tabs.IndexOf(ultraTabControlMain.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewTaxPayerByKppOriginal(x.SELLER_INN, x.SELLER_KPP);
        }
        private void cmiOpenBroker_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(ultraTabControlMain.Tabs.IndexOf(ultraTabControlMain.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewTaxPayerByKppOriginal(x.BROKER_INN, x.BROKER_KPP);
        }

        private void cmiOpenDeclaration_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(ultraTabControlMain.Tabs.IndexOf(ultraTabControlMain.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewDeclarationDetails(x.DeclarationId);
        }

        #endregion

    }
}
