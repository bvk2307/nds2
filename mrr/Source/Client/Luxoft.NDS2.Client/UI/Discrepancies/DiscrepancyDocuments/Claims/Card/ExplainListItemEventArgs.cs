﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public class ExplainListItemEventArgs : EventArgs
    {
        public ExplainListItemEventArgs()
        {
        }

        public bool AllowEdit
        {
            get;
            set;
        }

        public bool AllowView
        {
            get;
            set;
        }
    }
}
