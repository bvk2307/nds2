﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Claims.Card
{
    public class Presenter : BasePresenter<View>
    {
        private readonly DiscrepancyDocumentInfo _discrepancyDocument;
        private readonly IDiscrepancyDocumentService _blService;

        public Presenter(View view, PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo discrepancyDocument)
            : base(ctx, wi, view)
        {
            _discrepancyDocument = discrepancyDocument;
            _blService = base.GetServiceProxy<IDiscrepancyDocumentService>();
            InitializeView();
        }

        private void InitializeView()
        {
            View.BindCommonInfo(_discrepancyDocument);
        }

        public void OpenDiscrepancyCard(Discrepancy data)
        {
            base.ViewDiscrepancyDetails(data);
        }

        public object GetDatas(QueryConditions conditions, out long totalRowsNumber)
        {
            OperationResult<PageResult<Discrepancy>> or = null;
            if (ExecuteServiceCall(() => _blService.GetDiscrepancies(_discrepancyDocument.Id, conditions),
                    (result) =>
                    {
                        or = result;
                    }))
            {
                totalRowsNumber = or.Result.TotalMatches;
                return or.Result.Rows;
            }

            totalRowsNumber = 0;
            return new List<Discrepancy>();
        }

        public List<ControlRatio> GetDataControlRatio(QueryConditions conditions, out long totalRowsNumber)
        {
            PageResult<ControlRatio> result = null;
            var success = ExecuteServiceCall<OperationResult<PageResult<ControlRatio>>>(
                () => _blService.GetClaimControlRatio(_discrepancyDocument.Id, conditions),
                opResult => result = opResult.Result,
                opResult =>
                {
                    result = new PageResult<ControlRatio>();
                    View.ExecuteInUiThread(v => v.ShowError("Ошибка при загрузке контрольных соотношений: " + opResult.Message));
                });
            totalRowsNumber = success ? result.TotalMatches : 0;
            return result.Rows;
        }
    }
}