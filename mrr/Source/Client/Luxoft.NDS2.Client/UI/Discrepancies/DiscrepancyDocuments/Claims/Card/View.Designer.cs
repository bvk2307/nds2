﻿using Infragistics.Win.UltraWinTabControl;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControlInvoice = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridInvoice = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmInvoice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiGap = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiNds = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter8 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter8 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter9 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter9_10 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenBuyer = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenDeclaration = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter10 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabInvoiceChapter11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter11 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter11 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter12 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter12 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControlKS = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridControlRatio = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.ultraTabPageExplainList = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridExplain = new Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card.ExplainListView();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridNotReflected = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.cmNotReflected = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControlHistory = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxHistoryChangeStatus = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutHistoryStatusMain = new System.Windows.Forms.TableLayoutPanel();
            this.cmiViewDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiEditDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabControlMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ueGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.reclaimTaxPayerInfoCtrl = new Luxoft.NDS2.Client.UI.Controls.Reclaim.ReclaimTaxPayerInfo();
            this.reclaimCommonInfoCtrl = new Luxoft.NDS2.Client.UI.Controls.Reclaim.ReclaimCommonInfo();
            this.ultraTabPageControlInvoice.SuspendLayout();
            this.cmInvoice.SuspendLayout();
            this.tabInvoiceChapter8.SuspendLayout();
            this.cmChapter8.SuspendLayout();
            this.tabInvoiceChapter9.SuspendLayout();
            this.cmChapter9_10.SuspendLayout();
            this.tabInvoiceChapter10.SuspendLayout();
            this.tabInvoiceChapter11.SuspendLayout();
            this.cmChapter11.SuspendLayout();
            this.tabInvoiceChapter12.SuspendLayout();
            this.cmChapter12.SuspendLayout();
            this.ultraTabPageControlKS.SuspendLayout();
            this.ultraTabPageExplainList.SuspendLayout();
            this.ultraTabPageControl1.SuspendLayout();
            this.cmNotReflected.SuspendLayout();
            this.ultraTabPageControlHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHistoryChangeStatus)).BeginInit();
            this.ultraGroupBoxHistoryChangeStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).BeginInit();
            this.ultraTabControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).BeginInit();
            this.ueGroupBoxTaxPayerInfo.SuspendLayout();
            this.ultraExGroupBoxTaxPayerInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControlInvoice
            // 
            this.ultraTabPageControlInvoice.Controls.Add(this.gridInvoice);
            this.ultraTabPageControlInvoice.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControlInvoice.Name = "ultraTabPageControlInvoice";
            this.ultraTabPageControlInvoice.Size = new System.Drawing.Size(934, 244);
            // 
            // gridInvoice
            // 
            this.gridInvoice.AddVirtualCheckColumn = false;
            this.gridInvoice.AggregatePanelVisible = true;
            this.gridInvoice.AllowMultiGrouping = true;
            this.gridInvoice.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridInvoice.AllowSaveFilterSettings = false;
            this.gridInvoice.BackColor = System.Drawing.Color.Transparent;
            this.gridInvoice.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridInvoice.DefaultPageSize = "200";
            this.gridInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInvoice.ExportExcelCancelVisible = false;
            this.gridInvoice.ExportExcelVisible = false;
            this.gridInvoice.FilterResetVisible = false;
            this.gridInvoice.FooterVisible = true;
            this.gridInvoice.GridContextMenuStrip = this.cmInvoice;
            this.gridInvoice.Location = new System.Drawing.Point(0, 0);
            this.gridInvoice.Name = "gridInvoice";
            this.gridInvoice.PanelExportExcelStateVisible = false;
            this.gridInvoice.PanelLoadingVisible = true;
            this.gridInvoice.PanelPagesVisible = true;
            this.gridInvoice.RowDoubleClicked = null;
            this.gridInvoice.Setup = null;
            this.gridInvoice.Size = new System.Drawing.Size(934, 244);
            this.gridInvoice.TabIndex = 0;
            this.gridInvoice.Title = "";
            // 
            // cmInvoice
            // 
            this.cmInvoice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.cmiGap,
            this.cmiNds});
            this.cmInvoice.Name = "cmRowMenu";
            this.cmInvoice.Size = new System.Drawing.Size(343, 92);
            this.cmInvoice.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuInvoiceOpening);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.toolStripMenuItem5.Text = "Перейти к данным по СФ";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.InvoiceDataClick);
            // 
            // cmiGap
            // 
            this.cmiGap.Name = "cmiGap";
            this.cmiGap.Size = new System.Drawing.Size(342, 22);
            this.cmiGap.Text = "Открыть карточку расхождения (Разрыв)";
            this.cmiGap.Click += new System.EventHandler(this.DiscrepancyClick);
            // 
            // cmiNds
            // 
            this.cmiNds.Name = "cmiNds";
            this.cmiNds.Size = new System.Drawing.Size(342, 22);
            this.cmiNds.Text = "Открыть карточку расхождения (Проверка НДС)";
            this.cmiNds.Click += new System.EventHandler(this.DiscrepancyClick);
            // 
            // tabInvoiceChapter8
            // 
            this.tabInvoiceChapter8.Controls.Add(this.gridChapter8);
            this.tabInvoiceChapter8.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter8.Name = "tabInvoiceChapter8";
            this.tabInvoiceChapter8.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter8
            // 
            this.gridChapter8.AddVirtualCheckColumn = false;
            this.gridChapter8.AggregatePanelVisible = true;
            this.gridChapter8.AllowMultiGrouping = true;
            this.gridChapter8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter8.AllowSaveFilterSettings = false;
            this.gridChapter8.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter8.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter8.DefaultPageSize = "";
            this.gridChapter8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter8.ExportExcelCancelVisible = false;
            this.gridChapter8.ExportExcelVisible = false;
            this.gridChapter8.FilterResetVisible = false;
            this.gridChapter8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter8.FooterVisible = true;
            this.gridChapter8.GridContextMenuStrip = this.cmChapter8;
            this.gridChapter8.Location = new System.Drawing.Point(0, 0);
            this.gridChapter8.Name = "gridChapter8";
            this.gridChapter8.PanelExportExcelStateVisible = false;
            this.gridChapter8.PanelLoadingVisible = true;
            this.gridChapter8.PanelPagesVisible = true;
            this.gridChapter8.RowDoubleClicked = null;
            this.gridChapter8.Setup = null;
            this.gridChapter8.Size = new System.Drawing.Size(934, 244);
            this.gridChapter8.TabIndex = 6;
            this.gridChapter8.Title = "";
            // 
            // cmChapter8
            // 
            this.cmChapter8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripMenuItem9});
            this.cmChapter8.Name = "cmRowMenu";
            this.cmChapter8.Size = new System.Drawing.Size(243, 48);
            this.cmChapter8.Opening += new System.ComponentModel.CancelEventHandler(this.cmChapter8_Opening);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(242, 22);
            this.toolStripMenuItem8.Text = "Открыть карточку посредника";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.cmiOpenBroker_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(242, 22);
            this.toolStripMenuItem9.Text = "Открыть декларацию/журнал";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // tabInvoiceChapter9
            // 
            this.tabInvoiceChapter9.Controls.Add(this.gridChapter9);
            this.tabInvoiceChapter9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter9.Name = "tabInvoiceChapter9";
            this.tabInvoiceChapter9.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter9
            // 
            this.gridChapter9.AddVirtualCheckColumn = false;
            this.gridChapter9.AggregatePanelVisible = true;
            this.gridChapter9.AllowMultiGrouping = true;
            this.gridChapter9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter9.AllowSaveFilterSettings = false;
            this.gridChapter9.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter9.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter9.DefaultPageSize = "";
            this.gridChapter9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter9.ExportExcelCancelVisible = false;
            this.gridChapter9.ExportExcelVisible = false;
            this.gridChapter9.FilterResetVisible = false;
            this.gridChapter9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter9.FooterVisible = true;
            this.gridChapter9.GridContextMenuStrip = this.cmChapter9_10;
            this.gridChapter9.Location = new System.Drawing.Point(0, 0);
            this.gridChapter9.Name = "gridChapter9";
            this.gridChapter9.PanelExportExcelStateVisible = false;
            this.gridChapter9.PanelLoadingVisible = true;
            this.gridChapter9.PanelPagesVisible = true;
            this.gridChapter9.RowDoubleClicked = null;
            this.gridChapter9.Setup = null;
            this.gridChapter9.Size = new System.Drawing.Size(934, 244);
            this.gridChapter9.TabIndex = 3;
            this.gridChapter9.Title = "";
            // 
            // cmChapter9_10
            // 
            this.cmChapter9_10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenBuyer,
            this.cmiOpenBroker,
            this.cmiOpenDeclaration});
            this.cmChapter9_10.Name = "cmRowMenu";
            this.cmChapter9_10.Size = new System.Drawing.Size(243, 70);
            // 
            // cmiOpenBuyer
            // 
            this.cmiOpenBuyer.Name = "cmiOpenBuyer";
            this.cmiOpenBuyer.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenBuyer.Text = "Открыть карточку покупателя";
            this.cmiOpenBuyer.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // cmiOpenBroker
            // 
            this.cmiOpenBroker.Name = "cmiOpenBroker";
            this.cmiOpenBroker.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenBroker.Text = "Открыть карточку посредника";
            this.cmiOpenBroker.Click += new System.EventHandler(this.cmiOpenBroker_Click);
            // 
            // cmiOpenDeclaration
            // 
            this.cmiOpenDeclaration.Name = "cmiOpenDeclaration";
            this.cmiOpenDeclaration.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenDeclaration.Text = "Открыть декларацию/журнал";
            this.cmiOpenDeclaration.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // tabInvoiceChapter10
            // 
            this.tabInvoiceChapter10.Controls.Add(this.gridChapter10);
            this.tabInvoiceChapter10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter10.Name = "tabInvoiceChapter10";
            this.tabInvoiceChapter10.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter10
            // 
            this.gridChapter10.AddVirtualCheckColumn = false;
            this.gridChapter10.AggregatePanelVisible = true;
            this.gridChapter10.AllowMultiGrouping = true;
            this.gridChapter10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter10.AllowSaveFilterSettings = false;
            this.gridChapter10.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter10.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter10.DefaultPageSize = "";
            this.gridChapter10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter10.ExportExcelCancelVisible = false;
            this.gridChapter10.ExportExcelVisible = false;
            this.gridChapter10.FilterResetVisible = false;
            this.gridChapter10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter10.FooterVisible = true;
            this.gridChapter10.GridContextMenuStrip = this.cmChapter9_10;
            this.gridChapter10.Location = new System.Drawing.Point(0, 0);
            this.gridChapter10.Name = "gridChapter10";
            this.gridChapter10.PanelExportExcelStateVisible = false;
            this.gridChapter10.PanelLoadingVisible = true;
            this.gridChapter10.PanelPagesVisible = true;
            this.gridChapter10.RowDoubleClicked = null;
            this.gridChapter10.Setup = null;
            this.gridChapter10.Size = new System.Drawing.Size(934, 244);
            this.gridChapter10.TabIndex = 3;
            this.gridChapter10.Title = "";
            // 
            // tabInvoiceChapter11
            // 
            this.tabInvoiceChapter11.Controls.Add(this.gridChapter11);
            this.tabInvoiceChapter11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter11.Name = "tabInvoiceChapter11";
            this.tabInvoiceChapter11.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter11
            // 
            this.gridChapter11.AddVirtualCheckColumn = false;
            this.gridChapter11.AggregatePanelVisible = true;
            this.gridChapter11.AllowMultiGrouping = true;
            this.gridChapter11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter11.AllowSaveFilterSettings = false;
            this.gridChapter11.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter11.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter11.DefaultPageSize = "";
            this.gridChapter11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter11.ExportExcelCancelVisible = false;
            this.gridChapter11.ExportExcelVisible = false;
            this.gridChapter11.FilterResetVisible = false;
            this.gridChapter11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter11.FooterVisible = true;
            this.gridChapter11.GridContextMenuStrip = this.cmChapter11;
            this.gridChapter11.Location = new System.Drawing.Point(0, 0);
            this.gridChapter11.Name = "gridChapter11";
            this.gridChapter11.PanelExportExcelStateVisible = false;
            this.gridChapter11.PanelLoadingVisible = true;
            this.gridChapter11.PanelPagesVisible = true;
            this.gridChapter11.RowDoubleClicked = null;
            this.gridChapter11.Setup = null;
            this.gridChapter11.Size = new System.Drawing.Size(934, 244);
            this.gridChapter11.TabIndex = 4;
            this.gridChapter11.Title = "";
            // 
            // cmChapter11
            // 
            this.cmChapter11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.cmChapter11.Name = "cmRowMenu";
            this.cmChapter11.Size = new System.Drawing.Size(278, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem1.Text = "Открыть карточку покупателя";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem2.Text = "Открыть карточку субкомиссионера";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.cmiOpenBroker_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem3.Text = "Открыть декларацию/журнал";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // tabInvoiceChapter12
            // 
            this.tabInvoiceChapter12.Controls.Add(this.gridChapter12);
            this.tabInvoiceChapter12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter12.Name = "tabInvoiceChapter12";
            this.tabInvoiceChapter12.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter12
            // 
            this.gridChapter12.AddVirtualCheckColumn = false;
            this.gridChapter12.AggregatePanelVisible = true;
            this.gridChapter12.AllowMultiGrouping = true;
            this.gridChapter12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter12.AllowSaveFilterSettings = false;
            this.gridChapter12.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter12.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter12.DefaultPageSize = "";
            this.gridChapter12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter12.ExportExcelCancelVisible = false;
            this.gridChapter12.ExportExcelVisible = false;
            this.gridChapter12.FilterResetVisible = false;
            this.gridChapter12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter12.FooterVisible = true;
            this.gridChapter12.GridContextMenuStrip = this.cmChapter12;
            this.gridChapter12.Location = new System.Drawing.Point(0, 0);
            this.gridChapter12.Name = "gridChapter12";
            this.gridChapter12.PanelExportExcelStateVisible = false;
            this.gridChapter12.PanelLoadingVisible = true;
            this.gridChapter12.PanelPagesVisible = true;
            this.gridChapter12.RowDoubleClicked = null;
            this.gridChapter12.Setup = null;
            this.gridChapter12.Size = new System.Drawing.Size(934, 244);
            this.gridChapter12.TabIndex = 3;
            this.gridChapter12.Title = "";
            // 
            // cmChapter12
            // 
            this.cmChapter12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem6});
            this.cmChapter12.Name = "cmRowMenu";
            this.cmChapter12.Size = new System.Drawing.Size(241, 48);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(240, 22);
            this.toolStripMenuItem4.Text = "Открыть карточку покупателя";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(240, 22);
            this.toolStripMenuItem6.Text = "Открыть декларацию/журнал";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // ultraTabPageControlKS
            // 
            this.ultraTabPageControlKS.Controls.Add(this.gridControlRatio);
            this.ultraTabPageControlKS.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControlKS.Name = "ultraTabPageControlKS";
            this.ultraTabPageControlKS.Size = new System.Drawing.Size(934, 244);
            // 
            // gridControlRatio
            // 
            this.gridControlRatio.AggregatePanelVisible = true;
            this.gridControlRatio.AllowFilterReset = false;
            this.gridControlRatio.AllowMultiGrouping = true;
            this.gridControlRatio.AllowResetSettings = false;
            this.gridControlRatio.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridControlRatio.BackColor = System.Drawing.Color.Transparent;
            this.gridControlRatio.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridControlRatio.ColumnVisibilitySetupButtonVisible = true;
            this.gridControlRatio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRatio.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.gridControlRatio.ExportExcelCancelVisible = false;
            this.gridControlRatio.ExportExcelHint = "Экспорт в MS Excel";
            this.gridControlRatio.ExportExcelVisible = false;
            this.gridControlRatio.FilterResetVisible = false;
            this.gridControlRatio.FooterVisible = true;
            this.gridControlRatio.GridContextMenuStrip = null;
            this.gridControlRatio.Location = new System.Drawing.Point(0, 0);
            this.gridControlRatio.Name = "gridControlRatio";
            this.gridControlRatio.PanelExportExcelStateVisible = false;
            this.gridControlRatio.PanelLoadingVisible = false;
            this.gridControlRatio.PanelPagesVisible = true;
            this.gridControlRatio.RowDoubleClicked = null;
            this.gridControlRatio.Size = new System.Drawing.Size(934, 244);
            this.gridControlRatio.TabIndex = 4;
            this.gridControlRatio.Title = "";
            // 
            // ultraTabPageExplainList
            // 
            this.ultraTabPageExplainList.Controls.Add(this.gridExplain);
            this.ultraTabPageExplainList.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageExplainList.Name = "ultraTabPageExplainList";
            this.ultraTabPageExplainList.Size = new System.Drawing.Size(934, 244);
            // 
            // gridExplain
            // 
            this.gridExplain.BackColor = System.Drawing.Color.Transparent;
            this.gridExplain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridExplain.Location = new System.Drawing.Point(0, 0);
            this.gridExplain.Name = "gridExplain";
            this.gridExplain.Size = new System.Drawing.Size(934, 244);
            this.gridExplain.TabIndex = 5;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.gridNotReflected);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(934, 244);
            // 
            // gridNotReflected
            // 
            this.gridNotReflected.AggregatePanelVisible = true;
            this.gridNotReflected.AllowFilterReset = false;
            this.gridNotReflected.AllowMultiGrouping = true;
            this.gridNotReflected.AllowResetSettings = false;
            this.gridNotReflected.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridNotReflected.BackColor = System.Drawing.Color.Transparent;
            this.gridNotReflected.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridNotReflected.ColumnVisibilitySetupButtonVisible = true;
            this.gridNotReflected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridNotReflected.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.gridNotReflected.ExportExcelCancelVisible = false;
            this.gridNotReflected.ExportExcelHint = "Экспорт в MS Excel";
            this.gridNotReflected.ExportExcelVisible = false;
            this.gridNotReflected.FilterResetVisible = false;
            this.gridNotReflected.FooterVisible = true;
            this.gridNotReflected.GridContextMenuStrip = this.cmNotReflected;
            this.gridNotReflected.Location = new System.Drawing.Point(0, 0);
            this.gridNotReflected.Name = "gridNotReflected";
            this.gridNotReflected.PanelExportExcelStateVisible = false;
            this.gridNotReflected.PanelLoadingVisible = false;
            this.gridNotReflected.PanelPagesVisible = true;
            this.gridNotReflected.RowDoubleClicked = null;
            this.gridNotReflected.Size = new System.Drawing.Size(934, 244);
            this.gridNotReflected.TabIndex = 5;
            this.gridNotReflected.Title = "";
            // 
            // cmNotReflected
            // 
            this.cmNotReflected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem7,
            this.toolStripMenuItem10});
            this.cmNotReflected.Name = "cmRowMenu";
            this.cmNotReflected.Size = new System.Drawing.Size(245, 48);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(244, 22);
            this.toolStripMenuItem7.Text = "Открыть карточку контрагента";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.NotReflectedContragentClick);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(244, 22);
            this.toolStripMenuItem10.Text = "Перейти на запись о СФ";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.NotReflectedComparedRecordClick);
            // 
            // ultraTabPageControlHistory
            // 
            this.ultraTabPageControlHistory.Controls.Add(this.ultraGroupBoxHistoryChangeStatus);
            this.ultraTabPageControlHistory.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControlHistory.Name = "ultraTabPageControlHistory";
            this.ultraTabPageControlHistory.Size = new System.Drawing.Size(934, 244);
            // 
            // ultraGroupBoxHistoryChangeStatus
            // 
            this.ultraGroupBoxHistoryChangeStatus.Controls.Add(this.tableLayoutHistoryStatusMain);
            this.ultraGroupBoxHistoryChangeStatus.Location = new System.Drawing.Point(0, 4);
            this.ultraGroupBoxHistoryChangeStatus.Name = "ultraGroupBoxHistoryChangeStatus";
            this.ultraGroupBoxHistoryChangeStatus.Size = new System.Drawing.Size(897, 131);
            this.ultraGroupBoxHistoryChangeStatus.TabIndex = 1;
            // 
            // tableLayoutHistoryStatusMain
            // 
            this.tableLayoutHistoryStatusMain.ColumnCount = 7;
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutHistoryStatusMain.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutHistoryStatusMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutHistoryStatusMain.Name = "tableLayoutHistoryStatusMain";
            this.tableLayoutHistoryStatusMain.RowCount = 6;
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutHistoryStatusMain.Size = new System.Drawing.Size(891, 126);
            this.tableLayoutHistoryStatusMain.TabIndex = 1;
            // 
            // cmiViewDescription
            // 
            this.cmiViewDescription.Name = "cmiViewDescription";
            this.cmiViewDescription.Size = new System.Drawing.Size(321, 22);
            this.cmiViewDescription.Text = "Открыть карточку пояснения";
            this.cmiViewDescription.Click += new System.EventHandler(this.OpenExplainCardViewClick);
            // 
            // cmiEditDescription
            // 
            this.cmiEditDescription.Name = "cmiEditDescription";
            this.cmiEditDescription.Size = new System.Drawing.Size(321, 22);
            this.cmiEditDescription.Text = "Открыть карточку пояснения для изменения";
            this.cmiEditDescription.Click += new System.EventHandler(this.OpenExplainCardEditClick);
            // 
            // ultraTabControlMain
            // 
            this.ultraTabControlMain.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControlMain.Controls.Add(this.ultraTabPageControlInvoice);
            this.ultraTabControlMain.Controls.Add(this.ultraTabPageControlHistory);
            this.ultraTabControlMain.Controls.Add(this.ultraTabPageControlKS);
            this.ultraTabControlMain.Controls.Add(this.tabInvoiceChapter8);
            this.ultraTabControlMain.Controls.Add(this.tabInvoiceChapter9);
            this.ultraTabControlMain.Controls.Add(this.tabInvoiceChapter10);
            this.ultraTabControlMain.Controls.Add(this.tabInvoiceChapter11);
            this.ultraTabControlMain.Controls.Add(this.tabInvoiceChapter12);
            this.ultraTabControlMain.Controls.Add(this.ultraTabPageExplainList);
            this.ultraTabControlMain.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControlMain.Location = new System.Drawing.Point(0, 170);
            this.ultraTabControlMain.Name = "ultraTabControlMain";
            this.ultraTabControlMain.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControlMain.Size = new System.Drawing.Size(938, 270);
            this.ultraTabControlMain.TabIndex = 3;
            ultraTab1.Key = "tabInvoice";
            ultraTab1.TabPage = this.ultraTabPageControlInvoice;
            ultraTab1.Text = "Записи о СФ";
            ultraTab4.Key = "tabChapter8";
            ultraTab4.TabPage = this.tabInvoiceChapter8;
            ultraTab4.Text = "Раздел 8";
            ultraTab5.Key = "tabChapter9";
            ultraTab5.TabPage = this.tabInvoiceChapter9;
            ultraTab5.Text = "Раздел 9";
            ultraTab6.Key = "tabChapter10";
            ultraTab6.TabPage = this.tabInvoiceChapter10;
            ultraTab6.Text = "Раздел 10";
            ultraTab7.Key = "tabChapter11";
            ultraTab7.TabPage = this.tabInvoiceChapter11;
            ultraTab7.Text = "Раздел 11";
            ultraTab8.Key = "tabChapter12";
            ultraTab8.TabPage = this.tabInvoiceChapter12;
            ultraTab8.Text = "Раздел 12";
            ultraTab3.Key = "tabControlRation";
            ultraTab3.TabPage = this.ultraTabPageControlKS;
            ultraTab3.Text = "Контрольные соотношения";
            ultraTab9.Key = "tabExplainList";
            ultraTab9.TabPage = this.ultraTabPageExplainList;
            ultraTab9.Text = "Список пояснений";
            ultraTab10.Key = "tabUnshownOperations";
            ultraTab10.TabPage = this.ultraTabPageControl1;
            ultraTab10.Text = "Неотраженные операции";
            ultraTab2.Key = "tabHistoryChangeStatus";
            ultraTab2.TabPage = this.ultraTabPageControlHistory;
            ultraTab2.Text = "История изменения статуса";
            this.ultraTabControlMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab8,
            ultraTab3,
            ultraTab9,
            ultraTab10,
            ultraTab2});
            this.ultraTabControlMain.ActiveTabChanged += new Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventHandler(this.ultraTabControlMain_TabIndexChanged);
            this.ultraTabControlMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControlMain_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(934, 244);
            // 
            // ueGroupBoxTaxPayerInfo
            // 
            this.ueGroupBoxTaxPayerInfo.Controls.Add(this.ultraExGroupBoxTaxPayerInfo);
            this.ueGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.ueGroupBoxTaxPayerInfo.ExpandedSize = new System.Drawing.Size(938, 147);
            this.ueGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(0, 23);
            this.ueGroupBoxTaxPayerInfo.Name = "ueGroupBoxTaxPayerInfo";
            this.ueGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(938, 147);
            this.ueGroupBoxTaxPayerInfo.TabIndex = 2;
            this.ueGroupBoxTaxPayerInfo.Text = "Общие сведения";
            // 
            // ultraExGroupBoxTaxPayerInfo
            // 
            this.ultraExGroupBoxTaxPayerInfo.Controls.Add(this.reclaimTaxPayerInfoCtrl);
            this.ultraExGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(3, 19);
            this.ultraExGroupBoxTaxPayerInfo.Name = "ultraExGroupBoxTaxPayerInfo";
            this.ultraExGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(932, 125);
            this.ultraExGroupBoxTaxPayerInfo.TabIndex = 0;
            // 
            // reclaimTaxPayerInfoCtrl
            // 
            this.reclaimTaxPayerInfoCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.reclaimTaxPayerInfoCtrl.Location = new System.Drawing.Point(0, 0);
            this.reclaimTaxPayerInfoCtrl.Name = "reclaimTaxPayerInfoCtrl";
            this.reclaimTaxPayerInfoCtrl.Size = new System.Drawing.Size(932, 125);
            this.reclaimTaxPayerInfoCtrl.TabIndex = 0;
            this.reclaimTaxPayerInfoCtrl.OnInnClick += new System.EventHandler(this.reclaimTaxPayerInfoCtrl_OnInnClick);
            // 
            // reclaimCommonInfoCtrl
            // 
            this.reclaimCommonInfoCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.reclaimCommonInfoCtrl.Location = new System.Drawing.Point(0, 0);
            this.reclaimCommonInfoCtrl.Name = "reclaimCommonInfoCtrl";
            this.reclaimCommonInfoCtrl.Size = new System.Drawing.Size(938, 23);
            this.reclaimCommonInfoCtrl.TabIndex = 1;
            this.reclaimCommonInfoCtrl.CorrectionNumberLinkClicked += new System.EventHandler(this.ReclaimCommonInfoCtrlCorrectionNumberLinkClicked);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.ultraTabControlMain);
            this.Controls.Add(this.ueGroupBoxTaxPayerInfo);
            this.Controls.Add(this.reclaimCommonInfoCtrl);
            this.Name = "View";
            this.Size = new System.Drawing.Size(938, 440);
            this.OnFormClosing += new System.EventHandler(this.View_OnFormClosing);
            this.ultraTabPageControlInvoice.ResumeLayout(false);
            this.cmInvoice.ResumeLayout(false);
            this.tabInvoiceChapter8.ResumeLayout(false);
            this.cmChapter8.ResumeLayout(false);
            this.tabInvoiceChapter9.ResumeLayout(false);
            this.cmChapter9_10.ResumeLayout(false);
            this.tabInvoiceChapter10.ResumeLayout(false);
            this.tabInvoiceChapter11.ResumeLayout(false);
            this.cmChapter11.ResumeLayout(false);
            this.tabInvoiceChapter12.ResumeLayout(false);
            this.cmChapter12.ResumeLayout(false);
            this.ultraTabPageControlKS.ResumeLayout(false);
            this.ultraTabPageExplainList.ResumeLayout(false);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.cmNotReflected.ResumeLayout(false);
            this.ultraTabPageControlHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHistoryChangeStatus)).EndInit();
            this.ultraGroupBoxHistoryChangeStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).EndInit();
            this.ultraTabControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).EndInit();
            this.ueGroupBoxTaxPayerInfo.ResumeLayout(false);
            this.ultraExGroupBoxTaxPayerInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Reclaim.ReclaimCommonInfo reclaimCommonInfoCtrl;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ueGroupBoxTaxPayerInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExGroupBoxTaxPayerInfo;
        private Controls.Reclaim.ReclaimTaxPayerInfo reclaimTaxPayerInfoCtrl;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControlInvoice;
        private Controls.Grid.V1.GridControl gridInvoice;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControlHistory;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControlKS;
        private DataGridView gridControlRatio;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHistoryChangeStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutHistoryStatusMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter12;
        private Controls.Grid.V1.GridControl gridChapter8;
        private Controls.Grid.V1.GridControl gridChapter9;
        private Controls.Grid.V1.GridControl gridChapter10;
        private Controls.Grid.V1.GridControl gridChapter11;
        private System.Windows.Forms.ContextMenuStrip cmChapter9_10;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenBuyer;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclaration;
        private System.Windows.Forms.ContextMenuStrip cmChapter11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip cmChapter12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ContextMenuStrip cmInvoice;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cmiGap;
        private System.Windows.Forms.ToolStripMenuItem cmiNds;
        private System.Windows.Forms.ToolStripMenuItem cmiViewDescription;
        private System.Windows.Forms.ToolStripMenuItem cmiEditDescription;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageExplainList;
        private ExplainListView gridExplain;
        private System.Windows.Forms.ContextMenuStrip cmChapter8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private UltraTabPageControl ultraTabPageControl1;
        private DataGridView gridNotReflected;
        private System.Windows.Forms.ContextMenuStrip cmNotReflected;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private Controls.Grid.V1.GridControl gridChapter12;
    }
}
