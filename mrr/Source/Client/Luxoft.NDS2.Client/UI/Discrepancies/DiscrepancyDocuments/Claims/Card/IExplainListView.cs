﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public interface IExplainListView : IExtendedGridView
    {
        object SelectedItem
        {
            get;
        }

        void SelectFirstRow();

        event EventHandler<ExplainListItemEventArgs> ItemSelected;

        event EventHandler ItemViewing;

        event EventHandler ItemEditing;
    }
}
