﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public interface IInvoiceClaimView
    {
        IInvoiceClaimView WithStatus(
            string caption,
            string value,
            string additionalCaption = null,
            string additionalValue = null);

        void SetHistoryStatusDataEmpty();

        DiscrepancyDocumentInvoice getCurrentDiscrepancyInvoice();

        bool ContextMenuItemGapEnable { set; }
        bool ContextMenuItemNdsEnable { set; }

        event System.ComponentModel.CancelEventHandler ContextMenuItemOpening;
    }
}
