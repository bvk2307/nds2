﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.DataLoaders;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card;
using Luxoft.NDS2.Client.UI.Settings.Nds2Parameters;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public class ClaimCardPresenter : DocBasePresenter
    {
        private AsyncWorker<int> _controlRatioWorker;
        private bool stopWorkersControlRatio = false;
        private RequestStateAsync requestStateAsyncControlRation = new RequestStateAsync();
        protected AsyncWorker<int> _workerStatusDates = null;

        private readonly IDeclarationCardOpener _declarationCardOpener;
        private readonly IGridViewPresenter _explainListPresenter;

        private IInvoiceClaimView _view;
        private readonly WorkDaysProxy _workDaysProxy;

        public ClaimCardPresenter(View view, 
            PresentationContext ctx, 
            WorkItem wi, 
            WorkDaysProxy workDaysProxy,
            DiscrepancyDocumentInfo docInfo)
            : base(view, ctx, wi, docInfo)
        {
            _view = view;
            _workDaysProxy = workDaysProxy;

            _declarationCardOpener = GetDeclarationCardOpener();
            var explainReplyServise = wi.GetWcfServiceProxy<IExplainReplyDataService>();
            var accessContext = explainReplyServise.GetAccessContext().Result;
            _explainListPresenter =
                new ClientGridViewPresenter<KnpDocument, ClaimExplain, List<ClaimExplain>>(
                    view.GridExplain,
                    new ExplainListModel(docInfo, accessContext),
                    new ExplainListLoader(explainReplyServise, docInfo.Id, Notifier, ClientLogger));
            _view.ContextMenuItemOpening += OnContextMenuItemOpening;
        }

        public void ViewDeclarationDetails(long id)
        {
            var result = _declarationCardOpener.Open(id);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        #region Заполнение данных

        public override void LoadMainData()
        {
            LoadCommonInfo();
            LoadStatusDates();
        }

        public void LoadCommonInfo()
        {
            _mainDataWorker = new AsyncWorker<int>();
            _mainDataWorker.DoWork += (sender, args) =>
            {
                FillDeclarationLight(_declarationId);
                if (_docInfo.DocType.EntryId == DocType.ClaimSF)
                    FillDocumentCalculateInfoClaimSF(_docInfo.Id);
                else
                    base.FillDocumentCalculateInfo(_docInfo.Id);
            };
            _mainDataWorker.Complete += (sender, args) => View.ExecuteInUiThread(c =>
            {
                if (_docInfo.DocType.EntryId == DocType.ClaimSF && _docInfo.KnpSyncStatus != (int)KnpSyncStatus.Synchronized)
                    Notifier.ShowWarning(ResourceManagerNDS2.ClaimCard.NoneKnpDiscrepancies);
                View.LoadData();
                View.LoadCalculateInfo();
            });
            _mainDataWorker.Failed += (sender, args) =>
            {
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };
            _mainDataWorker.Start();
        }

        public void LoadStatusDates()
        {
            _workerStatusDates = new AsyncWorker<int>();
            _workerStatusDates.DoWork += (sender, args) => FillDocumentStatusDates(_docInfo.Id);
            _workerStatusDates.Complete += OnStatusDatesLoaded;
            _workerStatusDates.Failed += (sender, args) =>
            {
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };
            _workerStatusDates.Start();
        }

        private void OnStatusDatesLoaded(object sender, AsyncOperationCompletedEventArgs<int> e)
        {
            _view.SetHistoryStatusDataEmpty();
            
            // TODO Когда-нибудь мы сделаем рефакторинг этих карточек и вынесем условия и данные в модель
            if (_docInfo.DocType.EntryId == 2) // АТ по КС
                _view.WithStatus(ResourceManagerNDS2.ClaimCard.SendToSeod, FormatDate(_docInfo.SeodSentDate));
            if (_docInfo.DocType.EntryId == 1) // АТ по СФ
                _view.WithStatus(ResourceManagerNDS2.ClaimCard.ReceivedBySeod, FormatDate(_docInfo.SeodAcceptDate));

            
            var config = GetConfiguration();

            if (_docInfo.SendDate.HasValue && !_docInfo.DeliveryDate.HasValue)
            {
                var calcDate = _workDaysProxy.CalcDate(_docInfo.SendDate.Value, config.ClaimDeliveryTimeout.SecondsToDays());
                _view.WithStatus(
                    ResourceManagerNDS2.ClaimCard.SentToTaxpayer, FormatDate(_docInfo.SendDate),
                    ResourceManagerNDS2.ClaimCard.CalcDeliveryDate, FormatDate(calcDate));
            }
            else
            {
                _view.WithStatus(ResourceManagerNDS2.ClaimCard.SentToTaxpayer, FormatDate(_docInfo.SendDate));
            }

            if (_docInfo.DeliveryDate.HasValue && !_docInfo.CloseDate.HasValue)
            {
                var calcDate = _workDaysProxy.CalcDate(_docInfo.DeliveryDate.Value, config.ClaimExplainTimeout.SecondsToDays());
                _view.WithStatus(ResourceManagerNDS2.ClaimCard.DeliveryDate, FormatDate(_docInfo.DeliveryDate),
                    ResourceManagerNDS2.ClaimCard.CalcAnswerDate, FormatDate(calcDate));
            }
            else
            {
                _view.WithStatus(ResourceManagerNDS2.ClaimCard.DeliveryDate, FormatDate(_docInfo.DeliveryDate));
            }

            _view.WithStatus(ResourceManagerNDS2.ClaimCard.Closed, FormatDate(_docInfo.CloseDate));
        }

        private void OnContextMenuItemOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var invoice = _view.getCurrentDiscrepancyInvoice();
            if (invoice == null)
            {
                e.Cancel = true;
                return;
            }
            
            _view.ContextMenuItemGapEnable = (invoice.GAP_AMOUNT > 0 && DocInfo.KnpSyncStatus == (int)KnpSyncStatus.Synchronized);
            _view.ContextMenuItemNdsEnable = (invoice.NDS_AMOUNT > 0 && DocInfo.KnpSyncStatus == (int)KnpSyncStatus.Synchronized);
        }

        private string FormatDate(DateTime? dt)
        {
            return dt.HasValue ? dt.Value.ToString("dd.MM.yyyy") : string.Empty;
        }
        
        private void FillDeclarationLight(long declarationId)
        {
           _declaration =_discrepancyDocumentService.GetDeclaration(declarationId).Result;
        }

        private void FillDocumentCalculateInfoClaimSF(long docId)
        {
            _docCalculateInfo = _discrepancyDocumentService.GetDocumentCalculateInfoClaimSF(docId).Result;
        }

        #endregion

        public object GetControlRatioCached(QueryConditions qc, out long totalRowsNumber)
        {
            if (requestStateAsyncControlRation.State == StateAsync.None ||
                requestStateAsyncControlRation.State == StateAsync.Start)
            {
                GetControlRatioAsync(qc);
            }
            else if (requestStateAsyncControlRation.State == StateAsync.Complete)
            {
                if (qc != null &&
                    requestStateAsyncControlRation.QueryConditionsSend != null &&
                    !qc.EqualsBase(requestStateAsyncControlRation.QueryConditionsSend))
                {
                    GetControlRatioAsync(qc);
                }
                else
                {
                    requestStateAsyncControlRation.QueryConditionsSend = (QueryConditions)qc.Clone();
                }
            }
            totalRowsNumber = requestStateAsyncControlRation.TotalRowsNumber;
            return requestStateAsyncControlRation.ResultData;
        }

        private void SetRuqestStateAsyncKSEmpty()
        {
            requestStateAsyncControlRation.ResultData = new List<ControlRatio>();
            requestStateAsyncControlRation.TotalRowsNumber = 0;
        }

        private void GetControlRatioAsync(QueryConditions qc)
        {
            SetRuqestStateAsyncKSEmpty();
            requestStateAsyncControlRation.QueryConditionsSend = (QueryConditions)qc.Clone();

            _controlRatioWorker = new AsyncWorker<int>();
            _controlRatioWorker.DoWork += (sender, args) =>
            {
                requestStateAsyncControlRation.State = StateAsync.Send;
                long totalRowsNumber = 0;
                requestStateAsyncControlRation.ResultData = GetDataControlRatio(requestStateAsyncControlRation.QueryConditionsSend, out totalRowsNumber);
                requestStateAsyncControlRation.TotalRowsNumber = totalRowsNumber;
            };
            _controlRatioWorker.Complete += (sender, args) =>
            {
                requestStateAsyncControlRation.State = StateAsync.Complete;
                View.ExecuteInUiThread(c =>
                {
                    View.GridControlRatioSetLoadingVisibility(false);
                    View.GridControlRatioLoad();
                });
            };
            _controlRatioWorker.Failed += (sender, args) =>
            {
                SetRuqestStateAsyncKSEmpty();
                requestStateAsyncControlRation.State = StateAsync.Complete;
                View.ExecuteInUiThread(c =>
                {
                    if (!this.stopWorkersControlRatio)
                        View.GridControlRatioSetLoadingVisibility(false);
                });
                if (!this.stopWorkersControlRatio)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };

            _controlRatioWorker.Start();
        }

        private List<ControlRatio> GetDataControlRatio(QueryConditions conditions, out long totalRowsNumber)
        {
            PageResult<ControlRatio> result = null;
            var success = ExecuteServiceCall<OperationResult<PageResult<ControlRatio>>>(
                () => base.GetServiceProxy<IDiscrepancyDocumentService>().GetClaimControlRatio(_docInfo.Id, conditions),
                opResult => result = opResult.Result,
                opResult =>
                {
                    result = new PageResult<ControlRatio>();
                    View.ExecuteInUiThread(v => v.ShowError("Ошибка при загрузке контрольных соотношений: " + opResult.Message));
                });
            totalRowsNumber = success ? result.TotalMatches : 0;
            return result.Rows;
        }

        public void LoadExplainList()
        {
            _explainListPresenter.BeginLoad();
        }

        public override void LoadChaptersData()
        {
            View.LoadCalculateInfo();
            View.UpdateGridsData();
        }

        public void GetAccessToExplain(object explainObject, out bool allowEdit, out bool allowView)
        {
            allowEdit = true;
            allowView = true;
            var selectedItem = (KnpDocument)explainObject;
            if (selectedItem != null)
            {
                allowView = selectedItem.AllowView();
                allowEdit = selectedItem.AllowEdit();
            }
        }

    }
}