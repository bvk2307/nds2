﻿using Infragistics.Win.Misc;
using System;
using System.Windows.Threading;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public interface ILabelPair
    {
        ILabelPair WithDescription(bool visible, string description = null);
        ILabelPair WithValue(bool visible, string value = null);
    }

    public class LabelPair : ILabelPair
    {
        private readonly Dispatcher _dispatcher;
        private readonly UltraLabel _lblDescription;
        private readonly UltraLabel _lblValue;

        public LabelPair(Dispatcher dispatcher, UltraLabel lblDescription, UltraLabel lblValue)
        {
            _dispatcher = dispatcher;
            _lblDescription = lblDescription;
            _lblValue = lblValue;
        }

        public ILabelPair WithDescription(bool visible, string description = null)
        {
            _dispatcher.BeginInvoke(new Action(() =>
            {
                _lblDescription.Text = description;
                _lblDescription.Visible = visible;
            }));
            return this;
        }

        public ILabelPair WithValue(bool visible, string value = null)
        {
            _dispatcher.BeginInvoke(new Action(() =>
            {
                _lblValue.Text = value;
                _lblValue.Visible = visible;
            }));
            return this;
        }

    }
}
