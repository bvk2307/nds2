﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public class ExplainListLoader : ServiceProxyBase<List<ClaimExplain>>, IClientGridDataProvider<List<ClaimExplain>>
    {
        private readonly IExplainReplyDataService _service;

        private readonly long _claimId;

        public ExplainListLoader(
            IExplainReplyDataService service,
            long claimId,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
            _claimId = claimId;
        }

        public void StartLoading()
        {
            BeginInvoke(() => _service.SearchByClaim(_claimId));
        }

        protected override void CallBack(List<ClaimExplain> result)
        {
            if (DataLoaded != null)
            {
                DataLoaded(this, new DataLoadedEventArgs<List<ClaimExplain>>(result));
            }
        }

      
        public event EventHandler<DataLoadedEventArgs<List<ClaimExplain>>> DataLoaded;
    }
}
