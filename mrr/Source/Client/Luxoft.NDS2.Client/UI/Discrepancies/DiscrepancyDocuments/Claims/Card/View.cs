﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using FLS.CommonComponents.App.Execution;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.DataLoaders;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public partial class View : DocBaseView, IInvoiceClaimView
    {
        private readonly ClaimCardPresenter _presenter;

        private UcRibbonButtonToolContext _btnExplainOpen;

        private readonly Timer _timerLoadChapter = new Timer { Interval = 200, Enabled = false };

        private readonly ControlValueZeroFormatter _zeroFormat = new ControlValueZeroFormatter();

        public event System.ComponentModel.CancelEventHandler ContextMenuItemOpening;

        public bool ContextMenuItemGapEnable
        {
            set
            {
                cmiGap.Enabled = value;
            }
        }

        public bool ContextMenuItemNdsEnable
        {
            set
            {
                cmiNds.Enabled = value;
            }
        }
        
        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo docInfo)
            : base(ctx, wi, docInfo)
        {
            InitializeComponent();

            var logger = WorkItem.Services.Get<IClientLogger>();
            var calendarService = WorkItem.Services.GetServiceProxy<ICalendarDataService>();
            var wdProxy = new WorkDaysProxy(calendarService, Notifier, logger);

            _presenter = new ClaimCardPresenter(this, ctx, wi, wdProxy, docInfo);

            _timerLoadChapter.Tick += TimerLoadChapter_Tick;

            reclaimCommonInfoCtrl.SetDataEmpty();
            reclaimTaxPayerInfoCtrl.SetDataEmpty(docInfo);

            SetHistoryStatusDataEmpty();

            if (docInfo.DocType != null && docInfo.DocType.EntryId == DocType.ClaimKS)
            {
                /*Для контрольный соотношений показываем только закладку с кривыми соотношениями*/
                for (int i = 0; i < 6; ++i)
                {
                    ultraTabControlMain.Tabs[i].Visible = false;
                }
                /*TODO: магические числа обернуть в наименования*/
                ultraTabControlMain.Tabs[7].Visible = false;
                ultraTabControlMain.Tabs[8].Visible = false;
                gridControlRatio.PanelLoadingVisible = true;
                InitializeGridControlRatio();
            }
            else
            {
                ultraTabControlMain.Tabs[6].Visible = false;
                InitializeGridInvoice();
                InitializeChapters();
                InitializeGridNotReflected();
            }

            InitializeGridExplain();
            InitializeRibbon();

            _presenter.LoadMainData();
            _presenter.LoadChaptersData();

        }

        public override void LoadCalculateInfo()
        {
            reclaimTaxPayerInfoCtrl.SetDataDocumentCalculateInfo(_presenter.DocumentCalculateInfo, _presenter.DocInfo);
        }

        public override void LoadData()
        {
            reclaimCommonInfoCtrl.SetDataDocumentInfo(_presenter.DocInfo, _presenter.GetDocumentStatusDates());
            reclaimCommonInfoCtrl.SetDataDeclaration(_presenter.Declaration);
            reclaimTaxPayerInfoCtrl.SURCodes = _presenter.Sur;
            reclaimTaxPayerInfoCtrl.SetDataDeclaration(_presenter.Declaration, _presenter.DocumentCalculateInfo);
            reclaimTaxPayerInfoCtrl.SetDataDocumentInfo(_presenter.DocInfo);
        }

        public override void LoadHistoryStatusDates()
        {
        }

        #region Invoices main

        private void InitializeGridInvoice()
        {
            GridSetup setup = GetGridInvoiceSetup(string.Format("{0}_InitializeGridInvoice", GetType()));
            setup.GetData = _presenter.GetInvocesCached;

            gridInvoice.RowDoubleClicked += GoToInvoiceData;
            setup.AllowSaveSettings = false;
            gridInvoice.Setup = setup;
        }

        public override GridControl GridInvoices
        {
            get { return gridInvoice; }
        }
        public override ExplainListView GridExplain
        {
            get { return gridExplain; }
        }
        private void GoToInvoiceData(object o)
        {
            var invoice = o as Invoice;
            if (invoice == null)
                return;

            ultraTabControlMain.Tabs[invoice.CHAPTER - 7].Selected = true;

            var grid = GetChapterGrid(invoice.CHAPTER);
            var cn = TypeHelper<Invoice>.GetMemberName(x => x.INVOICE_NUM);
            var col = grid.GetColumn(cn);
            grid.FilterDatas(new ColumnFilter(col, FilterLogicalOperator.And)
            {
                FilterConditions = { new FilterCondition(col, FilterComparisionOperator.Equals, invoice.INVOICE_NUM) }
            });
        }

        private void InvoiceDataClick(object sender, EventArgs e)
        {
            GoToInvoiceData(gridInvoice.GetCurrentItem<Invoice>());
        }

        private void DiscrepancyClick(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            if (item == null)
                return;
            var invoice = gridInvoice.GetCurrentItem<Invoice>();
            if (invoice == null)
                return;

            if (item.Equals(cmiGap))
                OpenDiscrepancy(invoice, DiscrepancyType.Break);
            else if (item.Equals(cmiNds))
                OpenDiscrepancy(invoice, DiscrepancyType.NDS);
        }

        private void ContextMenuInvoiceOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.ComponentModel.CancelEventHandler ceHandler = ContextMenuItemOpening;
            if (ceHandler != null)
            {
                ceHandler(sender, e);
            }
        }

        #endregion

        private void reclaimTaxPayerInfoCtrl_OnInnClick(object sender, EventArgs e)
        {
            _presenter.ViewTaxPayerByKppEffective(_presenter.Declaration.INN, _presenter.Declaration.KPP_EFFECTIVE);
        }

        private void View_OnFormClosing(object sender, EventArgs e)
        {
            _presenter.UpdateUserComment(reclaimTaxPayerInfoCtrl.GetUserComment());
        }

        #region Control ratios

        public override void GridControlRatioLoad()
        {
            _controlRatioGridPresenter.Load();
        }

        public override void GridControlRatioSetLoadingVisibility(bool isVisible)
        {
            gridControlRatio.PanelLoadingVisible = isVisible;
        }

        private void CellClickControlRatio(object sender)
        {
            var cell = sender as UltraGridCell;
            if (cell == null)
                return;
            var cr = cell.Row.ListObject as ControlRatio;
            if (cr == null)
                return;
            _presenter.ViewControlRatioDetails(cr);
        }

        private DataGridPresenter<ControlRatio> _controlRatioGridPresenter;

        private readonly PagerStateViewModel _pager = new PagerStateViewModel();

        public IPager Pager
        {
            get
            {
                return _pager;
            }
        }

        public void InitializeGridControlRatio()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<ControlRatio>(gridControlRatio);

            setup.Columns.Add(helper.CreateHyperlinkColumn(cr => cr.Id, CellClickControlRatio, 2));
            setup.Columns.Add(helper.CreateNumberColumn(cr => cr.TypeCode, 2, 75));
            //           setup.Columns.Add(helper.CreateTextColumn(cr => cr.TypeDescription, 2).Configure(c => c.WrapText = true));
            setup.Columns.Add(helper.CreateTextColumn(cr => cr.TypeFormulation, 2).Configure(c => c.WrapText = true));
            setup.Columns.Add(helper.CreateTextColumn(cr => cr.TypeCalculationCondition, 2).Configure(c => c.WrapText = true));
            var group = helper.CreateGroup("Рассчетные части, руб.");
            group.Columns.Add(helper.CreateNumberColumn(cr => cr.DisparityLeft, 1, 75));
            group.Columns.Add(helper.CreateNumberColumn(cr => cr.DisparityRight, 1, 75));
            setup.Columns.Add(group);

            gridControlRatio
                .WithPager(new DataGridPageNavigator(_pager))
                .InitColumns(setup);
            gridControlRatio.RowDoubleClicked = obj => _presenter.ViewControlRatioDetails((ControlRatio)obj);
            gridControlRatio.AutoSizeMode = AutoSizeMode.GrowOnly;

            _controlRatioGridPresenter = new PagedDataGridPresenter<ControlRatio>(
                gridControlRatio,
                _pager,
                conditions =>
                {
                    long rows;
                    var list = (List<ControlRatio>)_presenter.GetControlRatioCached(conditions, out rows);
                    return new PageResult<ControlRatio>(list, (int)rows);
                });
            _controlRatioGridPresenter.Init(_presenter.SettingsProvider(string.Format("{0}_controlRatios", GetType())));
            _controlRatioGridPresenter.Load();
        }

        #endregion

        #region Explains

        public void InitializeGridExplain()
        {
            _presenter.LoadExplainList();
            gridExplain.ItemSelected += ExplainListItemSelected;
            gridExplain.ItemDoubleClick += GridExplainOnItemDoubleClick;
            gridExplain.ItemEditing += OpenExplainCardEditClick;
            gridExplain.ItemViewing += OpenExplainCardViewClick;
        }

        private void GridExplainOnItemDoubleClick(object sender, EventArgs eventArgs)
        {
            if (!_presenter.EditExplainOrReply(gridExplain.SelectedItem))
                _presenter.ViewExplainOrReply(gridExplain.SelectedItem);
        }

        private void ExplainListItemSelected(object sender, ExplainListItemEventArgs e)
        {
            bool allowView;
            bool allowEdit;
            _presenter.GetAccessToExplain(gridExplain.SelectedItem, out allowEdit, out allowView);
            e.AllowView = allowView;
            e.AllowEdit = allowEdit;
            SetRibbonButtonState(true, allowView, allowEdit);
        }

        private void OpenExplainCardViewClick(object sender, EventArgs e)
        {
            _presenter.ViewExplainOrReply(gridExplain.SelectedItem);
        }

        private void OpenExplainCardEditClick(object sender, EventArgs e)
        {
            _presenter.EditExplainOrReply(gridExplain.SelectedItem);
        }
        #endregion

        #region Ribbon

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2ClaimManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnClaimReload = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Reload", "cmdClaim" + "Reload")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnExplainOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Explain", "cmdExplain" + "Open")
            {
                Text = "Открыть пояснение",
                ToolTipText = "Открыть пояснение",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            groupNavigation1.ToolList.AddRange(new[]
            {
                new UcRibbonToolInstanceSettings(btnClaimReload.ItemName, UcRibbonToolSize.Large),
                new UcRibbonToolInstanceSettings(_btnExplainOpen.ItemName, UcRibbonToolSize.Large)
            });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnClaimReload,
                _btnExplainOpen,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
            _presenter.RefreshEKP();
        }

        [CommandHandler("cmdClaimReload")]
        public virtual void ClaimReloadBtnClick(object sender, EventArgs e)
        {
            _presenter.Reload();
        }

        [CommandHandler("cmdExplainOpen")]
        public virtual void ExplainOpenBtnClick(object sender, EventArgs e)
        {
            if (_viewMode)
                OpenExplainCardViewClick(sender, e);
            else
                OpenExplainCardEditClick(sender, e);
        }

        #endregion

        #region Status history

        private int _statusHistoryLineNumber;

        public IInvoiceClaimView WithStatus(string caption, string value, string additionalCaption = null, string additionalValue = null)
        {
            UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() =>
            {
                tableLayoutHistoryStatusMain.Controls.Add(new UltraLabel
                {
                    Text = caption,
                    Appearance = {TextHAlign = HAlign.Right},
                    Dock = DockStyle.Fill
                }, 0, _statusHistoryLineNumber);

                tableLayoutHistoryStatusMain.Controls.Add(new UltraLabel
                {
                    Text = value,
                    Dock = DockStyle.Fill
                }, 1, _statusHistoryLineNumber);

                if (!string.IsNullOrEmpty(additionalCaption))
                    tableLayoutHistoryStatusMain.Controls.Add(new UltraLabel
                    {
                        Text = additionalCaption,
                        Appearance = { TextHAlign = HAlign.Right },
                        Dock = DockStyle.Fill
                    }, 2, _statusHistoryLineNumber);

                if (!string.IsNullOrEmpty(additionalValue))
                    tableLayoutHistoryStatusMain.Controls.Add(new UltraLabel
                    {
                        Text = additionalValue,
                        Dock = DockStyle.Fill
                    }, 3, _statusHistoryLineNumber);

                ++_statusHistoryLineNumber;
            }));

            return this;
        }

        public void SetHistoryStatusDataEmpty()
        {
            UIThreadExecutor.CurrentDispatcher.Invoke(new Action(() =>
            {
                tableLayoutHistoryStatusMain.Controls.Clear();
                _statusHistoryLineNumber = 0;
            }));
        }

        #endregion

        public override void UpdateGridsData()
        {
            switch (ultraTabControlMain.Tabs.IndexOf(ultraTabControlMain.SelectedTab))
            {
                case 0: gridInvoice.UpdateData(); break;
                case 1: gridChapter8.UpdateData(); break;
                case 2: gridChapter9.UpdateData(); break;
                case 3: gridChapter10.UpdateData(); break;
                case 4: gridChapter11.UpdateData(); break;
                case 5: gridChapter12.UpdateData(); break;
                case 6: _controlRatioGridPresenter.Load(); break;
                case 7: _presenter.LoadExplainList(); break;
                case 8: _notReflectedPresenter.Load(); break;
            }
        }

        private void ultraTabControlMain_TabIndexChanged(object sender, ActiveTabChangedEventArgs e)
        {
            bool isExplainTabActive = ultraTabControlMain.Tabs.IndexOf(e.Tab) == 7;
            var selectedItem = (ExplainListItemViewModel)gridExplain.SelectedItem;
            if (isExplainTabActive && selectedItem == null)
                gridExplain.SelectFirstRow();

            selectedItem = (ExplainListItemViewModel)gridExplain.SelectedItem;
            if (selectedItem != null)
                SetRibbonButtonState(isExplainTabActive, selectedItem.AllowView(), selectedItem.AllowEdit());
        }

        private bool _viewMode = true;
        private void SetRibbonButtonState(bool isExplainTabActive, bool allowView, bool allowEdit)
        {
            if (_btnExplainOpen == null)
                return;

            _btnExplainOpen.Enabled = isExplainTabActive && (allowView || allowEdit);
            _viewMode = allowView;
            _presenter.RefreshEKP();
        }

        public DiscrepancyDocumentInvoice getCurrentDiscrepancyInvoice()
        {
            return gridInvoice.GetCurrentItem<DiscrepancyDocumentInvoice>();
        }

        private readonly List<ToolStripMenuItem> _temporaryMenuItems = new List<ToolStripMenuItem>();

        private void cmChapter8_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _temporaryMenuItems.ForEach(x => cmChapter8.Items.Remove(x));
            _temporaryMenuItems.Clear();

            var grid = GetChapterGrid(8);
            if (grid == null)
                return;

            var currentItem = grid.GetCurrentItem<Invoice>();

            if (currentItem == null)
                return;

            if (!string.IsNullOrEmpty(currentItem.SELLER_INN))
            {
                var innList = currentItem.SELLER_INN.Split(',');
                var kppList = currentItem.SELLER_KPP.Split(',');
                var k = Math.Min(innList.Length, kppList.Length);
                for (var i = 0; i < k; ++i)
                {
                    var item = new ToolStripMenuItem
                    {
                        Name = string.Format("{0},{1}", innList[i], kppList[i]),
                        Size = new Size(242, 22),
                        Text = string.Format("Открыть карточку продавца (ИНН {0})", innList[i])
                    };
                    item.Click += ContextOpenSellerClick;
                    cmChapter8.Items.Insert(0, item);
                    _temporaryMenuItems.Add(item);
                }
            }
        }

        private void ContextOpenSellerClick(object sender, EventArgs e)
        {
            var cmi = sender as ToolStripMenuItem;
            if (cmi == null)
                return;
            var grid = GetChapterGrid(8);
            if (grid == null)
                return;
            var invoice = grid.GetCurrentItem<Invoice>();
            if (invoice == null) // ДН никогда не говори никогда!
                return;
            var args = cmi.Name.Split(',');
            if (args.Length != 2)
                return;
            _presenter.ViewTaxPayerByKppOriginal(args[0], args[1]);
        }

        private void ultraTabControlMain_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            StartUpdateCurrentChapterInvoice();
        }

        private void StartUpdateCurrentChapterInvoice()
        {
            _timerLoadChapter.Enabled = true;
        }

        private void TimerLoadChapter_Tick(object sender, EventArgs e)
        {
            _timerLoadChapter.Enabled = false;
            GridControl currentgrid = GetCurrentGridInvoices();
            if (currentgrid != null)
            {
                currentgrid.UpdateData();
            }
        }

        private void ReclaimCommonInfoCtrlCorrectionNumberLinkClicked(object sender, EventArgs e)
        {
            _presenter.ViewDeclarationDetails(_presenter.Declaration.DECLARATION_VERSION_ID);
        }

    }
}
