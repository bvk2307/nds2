﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card
{
    public class ExplainListModel : ClaimViewModel
    {
        public ExplainListModel(
            DiscrepancyDocumentInfo docInfo,
            Common.Contracts.DTO.AccessRestriction.AccessContext accessContext)
            : base(docInfo.Soun.EntryId, new KnpDocumentClaim() { CloseReason = docInfo.CloseReason }, accessContext)
        {
        }

        public override string Name
        {
            get { return string.Empty; }
        }

    }
}
