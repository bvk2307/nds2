﻿using System.Collections.Generic;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Base;
using CommonComponents.Utils.Async;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Reclaims.Card
{
    public class ReclaimCardPresenter : DocBasePresenter
    {
        private readonly IDeclarationCardOpener _declarationCardOpener;

        public ReclaimCardPresenter(View view, PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo docInfo, ChainAccessModel chainDataModel)
            : base(view, ctx, wi, docInfo)
        {
            _declarationCardOpener = chainDataModel != null ? GetDeclarationCardOpener(chainDataModel) : GetDeclarationCardOpener();
        }

        public IDeclarationCardOpener DeclarationCardOpener { get { return _declarationCardOpener; } }

        public object GetExplainData(QueryConditions conditions, out long totalRowsNumber)
        {
            List<ExplainReplyInfo> result = null;

            SetAdditionalSortingForExplainReplies(conditions);

            var success = ExecuteServiceCall(
                () => GetServiceProxy<IExplainReplyDataService>().GetExplainReplyList(_docInfo.Id, conditions),
                opResult => result = opResult.Result,
                opResult =>
                {
                    result = new List<ExplainReplyInfo>();
                    View.ExecuteInUiThread(v => v.ShowError("Ошибка при загрузке пояснений: " + opResult.Message));
                });
            totalRowsNumber = success ? result.Count : 0;
            return result;
        }

        public override void LoadChaptersData()
        {
            View.LoadCalculateInfo();
            View.UpdateGridsData();
        }

        public override void LoadMainData()
        {
            _mainDataWorker = new AsyncWorker<int>();
            _mainDataWorker.DoWork += (sender, args) =>
            {
                FillDocument(_docInfo.Id);
                FillDeclaration(_declarationId);
                FillDocumentStatusDates(_docInfo.Id);
                FillDocumentCalculateInfoReclaim(_docInfo.Id);
            };
            _mainDataWorker.Complete += (sender, args) => View.StartExecuteInUiThread(() =>
            {
                View.LoadData();
                View.LoadCalculateInfo();
            });
            _mainDataWorker.Failed += (sender, args) =>
            {
                if (!this._stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };
            _mainDataWorker.Start();
        }

        private void FillDocumentCalculateInfoReclaim(long docId)
        {
            _docCalculateInfo = _discrepancyDocumentService.GetDocumentCalculateInfoReclaim(docId).Result;
        }
    }
}
