﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Base.Card;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Reclaims.Card
{
    public partial class View : DocBaseView
    {
        private readonly ReclaimCardPresenter _presenter;
        private UcRibbonButtonToolContext _btnAnswerOpen;
        private readonly IntValueZeroFormatter _zeroFormat = new IntValueZeroFormatter();

        private bool _answerAllowView;
        private bool _answerAllowEdit;

        #region .ctors

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx, WorkItem wi, DiscrepancyDocumentInfo docInfo, ChainAccessModel chainDataModel)
            : base(ctx, wi, docInfo)
        {
            InitializeComponent();

            _presenter = new ReclaimCardPresenter(this, ctx, wi, docInfo, chainDataModel);

            reclaimCommonInfoCtrl.SetDataEmpty();
            reclaimTaxPayerInfoCtrl.SetDataEmpty(docInfo);
            SetHistoryStatusDataEmpty();

            InitializeGridInvoice();
            InitializeChapters();
            InitializeGridAnswer();

            InitializeRibbon();

            _presenter.LoadMainData();
            _presenter.LoadChaptersData();
        }

        #endregion

        #region Load basic data
        
        public override void LoadData()
        {
            reclaimCommonInfoCtrl.SetDataDocumentInfo(_presenter.DocInfo, _presenter.GetDocumentStatusDates());
            reclaimCommonInfoCtrl.SetDataDeclaration(_presenter.Declaration);
            reclaimTaxPayerInfoCtrl.SURCodes = _presenter.Sur;
            reclaimTaxPayerInfoCtrl.SetDataDeclaration(_presenter.Declaration, _presenter.DocumentCalculateInfo);
            reclaimTaxPayerInfoCtrl.SetDataDocumentInfo(_presenter.DocInfo);
            SetHistoryStatusDates(_presenter.DocInfo, _presenter.GetDocumentStatusDates());
        }

        public override void LoadCalculateInfo()
        {
            reclaimTaxPayerInfoCtrl.SetDataDocumentCalculateInfo(_presenter.DocumentCalculateInfo, _presenter.DocInfo);
        }

        #endregion

        #region Grid invoice total
        
        private void InitializeGridInvoice()
        {
            GridSetup setup = GetGridInvoiceSetup(string.Format("{0}_InitializeGridInvoice", GetType()));
            setup.GetData = _presenter.GetInvocesCached;

            gridInvoice.RowDoubleClicked += delegate(object rowData)
            {
                var invoice = (Invoice)rowData;
                GoToInvoiceData(invoice);
            };
            setup.AllowSaveSettings = false;
            gridInvoice.Setup = setup;
        }

        public override GridControl GridInvoices
        {
            get { return gridInvoice; }
        }

        private void GoToInvoiceData(Invoice invoice)
        {
            if (invoice == null)
                return;

            mainTabControl.Tabs[invoice.CHAPTER - 7].Selected = true;

            _chaptersGridFilter[invoice.CHAPTER] = new FilterQuery
            {
                ColumnName = TypeHelper<Invoice>.GetMemberName(x => x.INVOICE_NUM),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>
                {
                    new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = invoice.INVOICE_NUM}
                }
            };
            GetChapterGrid(invoice.CHAPTER).UpdateData();
        }

        private void InvoiceDataClick(object sender, EventArgs e)
        {
            GoToInvoiceData(gridInvoice.GetCurrentItem<Invoice>());
        }

        private void DiscrepancyClick(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            if (item == null)
                return;
            var invoice = gridInvoice.GetCurrentItem<Invoice>();
            if (invoice == null)
                return;

            if (item.Equals(cmiGap))
                OpenDiscrepancy(invoice, DiscrepancyType.Break);
            else if (item.Equals(cmiNds))
                OpenDiscrepancy(invoice, DiscrepancyType.NDS);
        }

        private void ContextMenuInvoiceOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var invoice = gridInvoice.GetCurrentItem<DiscrepancyDocumentInvoice>();
            cmiGap.Enabled = invoice.GAP_AMOUNT > 0;
            cmiNds.Enabled = invoice.NDS_AMOUNT > 0;
        }

        #endregion

        #region Answers (based on explains)

        public void InitializeGridAnswer()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridAnswer", GetType()));
            setup.GetData = (QueryConditions qc, out long cnt) =>
            {
                var result = _presenter.GetExplainData(qc, out cnt);
                gridAnswer.PanelLoadingVisible = false;
                return result;
            };
            var helper = new GridSetupHelper<ExplainReplyInfo>();
            setup.Columns.AddRange(new[]
            {
                new ColumnGroupDefinition {Caption = "", Columns = {helper.CreateColumnDefinition(x => x.TYPE_NAME)}},
                //new ColumnGroupDefinition {Caption = "", Columns = {helper.CreateColumnDefinition(x => x.INCOMING_NUM, d => {d.Caption = "Номер"; d.ToolTip = "Номер регистрации ответа в СЭОД";})}},
                new ColumnGroupDefinition {Caption = "", Columns = {helper.CreateColumnDefinition(x => x.INCOMING_DATE, d => {d.Caption = "Дата"; d.ToolTip = "Дата регистрации ответа в СЭОД";})}},
                new ColumnGroupDefinition {
                    Caption = "Статус",
                    Columns = {
                        helper.CreateColumnDefinition(x => x.status_name, d => {d.Caption = "Наименование"; d.ToolTip = "Текущий статус ответа";}),
                        helper.CreateColumnDefinition(x => x.status_set_date, d => {d.Caption = "Дата"; d.ToolTip = "Дата и время присвоения текущего статуса ответа";})
                    }
                }
            });
            setup.QueryCondition.Sorting.Add(new ColumnSort
            {
                ColumnKey = helper.GetMemberName(x => x.INCOMING_DATE),
                Order = ColumnSort.SortOrder.Asc
            });
            gridAnswer.Setup = setup;
            gridAnswer.RowDoubleClicked = row =>
            {
                var doc = row as ExplainReplyInfo;
                if (doc == null) return;

                if (_answerAllowView || _answerAllowEdit)
                {
                    _presenter.OpenExplainCard(doc.EXPLAIN_ID);
                }
            };
            gridAnswer.SelectRow += (sender, args) => SetAnswerAbilities();
        }

        private void cmExplainList_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_answerAllowView && !_answerAllowEdit)
            {
                e.Cancel = true;
                return;
            }
            cmiViewDescription.Visible = _answerAllowView;
            cmiEditDescription.Visible = _answerAllowEdit;
        }

        private void OpenDescriptionCardClick(object sender, EventArgs e)
        {
            var doc = gridAnswer.GetCurrentItem<ExplainReplyInfo>();
            if (doc == null)
                return;

            _presenter.OpenExplainCard(doc.EXPLAIN_ID);
        }

        private void SetRibbonButtonsState()
        {
            if (_btnAnswerOpen == null)
                return;
            _btnAnswerOpen.Enabled = (mainTabControl.Tabs.IndexOf(mainTabControl.SelectedTab) == 6) && (_answerAllowEdit || _answerAllowView);
            _presenter.RefreshEKP();
        }

        private void SetAnswerAbilities()
        {
            _answerAllowEdit = false;
            _answerAllowView = false;

            var answer = gridAnswer.GetCurrentItem<ExplainReplyInfo>();
            if (answer != null)
            {
                _answerAllowView = answer.status_id == ExplainReplyStatus.SendToMC || 
                                   answer.status_id == ExplainReplyStatus.ProcessedInMC;

                _answerAllowEdit = answer.status_id == ExplainReplyStatus.NotProcessed && 
                                   !gridAnswer.Rows.Cast<ExplainReplyInfo>()
                                        .Any(ans => ((ans.INCOMING_DATE < answer.INCOMING_DATE) ||
                                                     (ans.INCOMING_DATE == answer.INCOMING_DATE && 
                                                      ans.EXPLAIN_ID < answer.EXPLAIN_ID)) && 
                                                    ans.status_id != ExplainReplyStatus.SendToMC && 
                                                    ans.status_id != ExplainReplyStatus.ProcessedInMC) &&
                                   !Utils.IsKnpClosedByAnnulment(_presenter.Declaration.KnpClosedReasonId);
            }

            SetRibbonButtonsState();
        }

        #endregion

        #region Ribbon

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2ReclaimManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnReclaimReload = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Reload", "cmdReclaim" + "Reload")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnAnswerOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Answer", "cmdAnswer" + "Open")
            {
                Text = "Открыть ответ",
                ToolTipText = "Открыть ответ",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            groupNavigation1.ToolList.AddRange(new[]
            {
                new UcRibbonToolInstanceSettings(btnReclaimReload.ItemName, UcRibbonToolSize.Large),
                new UcRibbonToolInstanceSettings(_btnAnswerOpen.ItemName, UcRibbonToolSize.Large)
            });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnReclaimReload,
                _btnAnswerOpen,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
            _presenter.RefreshEKP();
        }

        [CommandHandler("cmdReclaimReload")]
        public virtual void ReclaimReloadBtnClick(object sender, EventArgs e)
        {
            _presenter.Reload();
        }

        [CommandHandler("cmdAnswerOpen")]
        public virtual void AnswerOpenBtnClick(object sender, EventArgs e)
        {
            OpenDescriptionCardClick(sender, e);
        }

        #endregion

        #region History status

        public void SetHistoryStatusDataEmpty()
        {
            labelSendToSEODValue.Text = String.Empty;
            labelSendToNOExecutorValue.Text = String.Empty;
            labelReceivedNOExecutorValue.Text = String.Empty;
            labelSendToTaxPayerValue.Text = String.Empty;
            labelHandedToTaxpayerValue.Text = String.Empty;
            labelCloseValue.Text = String.Empty;
            labelProlongAnswerDateValue.Text = String.Empty;
        }

        public void SetHistoryStatusDates(DiscrepancyDocumentInfo docInfo, Dictionary<string, DateTime> dates)
        {
            if (dates != null)
            {
                labelSendToSEODValue.Text = FormatDate(GetStatusDate(dates, DocStatusDate.SendToSEOD));
                labelSendToTaxPayerValue.Text = FormatDate(GetStatusDate(dates, DocStatusDate.SendToNP));
                labelHandedToTaxpayerValue.Text = FormatDate(GetStatusDate(dates, DocStatusDate.ReceivedNP));

                if (docInfo.DocType != null && docInfo.DocType.EntryId == DocType.Reclaim93_1)
                {
                    labelSendToNOExecutorCaption.Visible = true;
                    labelSendToNOExecutorValue.Visible = true;
                    labelReceivedNOExecutorCaption.Visible = true;
                    labelReceivedNOExecutorValue.Visible = true;
                    labelSendToNOExecutorValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.SendToNOExecutor));
                    labelReceivedNOExecutorValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.ReceivedNOExecutor));
                }
                else
                {
                    labelSendToNOExecutorCaption.Visible = false;
                    labelSendToNOExecutorValue.Visible = false;
                    labelReceivedNOExecutorCaption.Visible = false;
                    labelReceivedNOExecutorValue.Visible = false;
                    tableLayoutHistoryStatusMain.RowStyles[2].Height = 0;
                    tableLayoutHistoryStatusMain.RowStyles[3].Height = 0;
                    if (tableLayoutHistoryStatusMain.RowStyles[2].Height > 0)
                    {
                        const int heightSummaryRemove = 50;
                        tableLayoutHistoryStatusMain.Height -= heightSummaryRemove;
                    }
                }

                if (dates.ContainsKey(DocStatusDate.CalculateProlong))
                {
                    var visible = GetStatusDate(dates, DocStatusDate.CalculateProlong).HasValue &&
                                  !GetStatusDate(dates, DocStatusDate.Close).HasValue;
                    labelProlongAnswerDateCaption.Visible = visible;
                    labelProlongAnswerDateValue.Visible = visible;
                    labelProlongAnswerDateValue.Text = FormatDate(GetStatusDate(dates, DocStatusDate.CalculateProlong));
                }
                else
                {
                    labelProlongAnswerDateCaption.Visible = false;
                    labelProlongAnswerDateValue.Visible = false;
                }

                labelCloseValue.Text = FormatDate(GetStatusDate(dates, DocStatusDate.Close));
            }
            else
            {
                SetHistoryStatusDataEmpty();
            }
        }

        private DateTime? GetStatusDate(Dictionary<string, DateTime> dates, string key)
        {
            DateTime? dt = null;
            if (dates.ContainsKey(key))
            {
                dt = dates[key];
            }
            return dt;
        }

        private string FormatDate(DateTime? dt)
        {
            return dt == null ? string.Empty : dt.Value.ToString("dd.MM.yyyy");
        }

        private string FormatDateTime(DateTime? dt)
        {
            return dt.HasValue ? dt.Value.ToString("dd.MM.yyyy hh:mm") : String.Empty;
        }

        #endregion

        public override void UpdateGridsData()
        {
            switch (mainTabControl.Tabs.IndexOf(mainTabControl.SelectedTab))
            {
                case 0: gridInvoice.UpdateData(); break;
                case 1: gridChapter8.UpdateData(); break;
                case 2: gridChapter9.UpdateData(); break;
                case 3: gridChapter10.UpdateData(); break;
                case 4: gridChapter11.UpdateData(); break;
                case 5: gridChapter12.UpdateData(); break;
                case 6: gridAnswer.UpdateData(); break;
            }
        }

        private void mainTabControl_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            SetAnswerAbilities();
            UpdateChapterGridsData();
        }

        private void reclaimTaxPayerInfoCtrl_OnInnClick(object sender, EventArgs e)
        {
            _presenter.ViewTaxPayerByKppEffective(_presenter.Declaration.INN, _presenter.Declaration.KPP_EFFECTIVE);
        }

        private void View_OnFormClosing(object sender, EventArgs e)
        {
            _presenter.UpdateUserComment(reclaimTaxPayerInfoCtrl.GetUserComment());
        }

        private void ReclaimCommonInfoCtrlCorrectionNumberLinkClicked(object sender, EventArgs e)
        {
            _presenter.DeclarationCardOpener.Open(_presenter.Declaration.DECLARATION_VERSION_ID);
        }

    }
}
