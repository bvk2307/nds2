﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using ColumnFilter = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Bargain;
using Luxoft.NDS2.Client.Helpers.Explain;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Reclaims.Card
{
    partial class View
    {
        #region Width constants

        private const int WIDTH_INVOICE = 40;
        private const int WIDTH_DATE = 40;
        private const int WIDTH_NAME = 300;
        private const int WIDTH_INN = 30;
        private const int WIDTH_KPP = 30;
        private const int WIDTH_MONEY = 100;

        #endregion

        private readonly Dictionary<int, QueryConditions> _chaptersGridQueryConditions = new Dictionary<int, QueryConditions>();
        private readonly Dictionary<int, FilterQuery> _chaptersGridFilter = new Dictionary<int, FilterQuery>();
        private readonly Dictionary<int, int> _chapterMapTabControls = new Dictionary<int, int>();

        // Entry point
        private void InitializeChapters()
        {
            #region FIlter by chapter

            for (var i = 8; i < 13; ++i)
            {
                _chaptersGridQueryConditions[i] = new QueryConditions
                {
                    Filter =
                    {
                        new FilterQuery
                        {
                            ColumnName = TypeHelper<InvoiceRelated>.GetMemberName(x => x.CHAPTER),
                            FilterOperator = FilterQuery.FilterLogicalOperator.And,
                            Filtering = new List<ColumnFilter> { new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = i} }
                        }
                    }
                };
            }

            #endregion

            InitChapterMapTabControls();

            InitializeChapter8();
            InitializeChapter9();
            InitializeChapter10();
            InitializeChapter11();
            InitializeChapter12();
        }

        private void InitChapterMapTabControls()
        {
            _chapterMapTabControls.Clear();
            _chapterMapTabControls.Add(tabInvoiceChapter8.Tab.VisibleIndex, (int)DeclarationInvoiceChapterNumber.Chapter8);
            _chapterMapTabControls.Add(tabInvoiceChapter9.Tab.VisibleIndex, (int)DeclarationInvoiceChapterNumber.Chapter9);
            _chapterMapTabControls.Add(tabInvoiceChapter10.Tab.VisibleIndex, (int)DeclarationInvoiceChapterNumber.Chapter10);
            _chapterMapTabControls.Add(tabInvoiceChapter11.Tab.VisibleIndex, (int)DeclarationInvoiceChapterNumber.Chapter11);
            _chapterMapTabControls.Add(tabInvoiceChapter12.Tab.VisibleIndex, (int)DeclarationInvoiceChapterNumber.Chapter12);
        }

        public override GridControl GetChapterGrid(int chapter)
        {
            switch (chapter)
            {
                case 8: return gridChapter8;
                case 9: return gridChapter9;
                case 10: return gridChapter10;
                case 11: return gridChapter11;
                case 12: return gridChapter12;
                default: return null;
            }
        }

        private void DrawPen(ColumnDefinition column)
        {
            var drw = new DictionaryInvoiceStateRecord
            {
                InvoiceState = ExplainInvoiceState.ChangingInThisExplain,
                ColorARGB = 0,
                Description = ""
            };

            column.Caption = "";
            column.ToolTip = "Признак редактирования записи в ходе полученного пояснения от НП";
            column.DontHide = true;

            column.CustomConditionValueAppearance = () =>
            {
                var result = new ConditionValueAppearance();
                var condition = new OperatorCondition(ConditionOperator.Equals, 1);
                var appearance = new Appearance
                {
                    Image = drw.GetIcon(this),
                    ImageHAlign = HAlign.Center,
                    ImageVAlign = VAlign.Middle,
                    ForegroundAlpha = Alpha.Transparent
                };
                result.Add(condition, appearance);

                return result;
            };
        }

        private void DrawNumber(ColumnDefinition column)
        {
            column.Align = ColumnAlign.Right;
            column.Width = WIDTH_INVOICE;
        }

        private void DrawMoney(ColumnDefinition column)
        {
            column.Align = ColumnAlign.Right;
            column.FormatInfo = _zeroFormat;
            column.Width = WIDTH_MONEY;
        }

        private void SetChaptersQueryConditions(GridSetup setup, int chapter)
        {
            QueryConditions conditions;
            if (_chaptersGridQueryConditions.TryGetValue(chapter, out conditions))
            {
                setup.QueryCondition = conditions;
            }

            setup.QueryCondition.Sorting.Add(new ColumnSort
            {
                ColumnKey = chapter == 12
                    ? TypeHelper<InvoiceRelated>.GetMemberName(x => x.INVOICE_DATE)
                    : TypeHelper<InvoiceRelated>.GetMemberName(x => x.INVOICE_NUM),
                Order = ColumnSort.SortOrder.Asc
            });

            setup.AllowSaveSettings = false;
        }

        #region Row action

        public Dictionary<string, object> ToDictionary<T>(T entity)
        {
            var properties = entity.GetType().GetProperties();
            return properties.ToDictionary(itemProp => itemProp.Name, itemProp => itemProp.GetValue(entity, null));
        }

        private object GetFieldEntity(Dictionary<string, object> filedsEntity, string fieldKey)
        {
            object fieldValue;
            filedsEntity.TryGetValue(fieldKey, out fieldValue);
            return fieldValue;
        }

        private void GridRowAction(UltraGridRow row)
        {
            var invoice = row.ListObject as InvoiceRelated;
            if (invoice == null || !invoice.IsChildren)
                return;

            var appearanceActiveRow = row.Cells[0].Band.Override.ActiveRowAppearance;
            var appearanceActiveCell = row.Cells[0].Band.Override.ActiveCellAppearance;
            appearanceActiveRow.Reset();
            appearanceActiveCell.Reset();
            var filedsEntityOriginal = ToDictionary(invoice.Parent);

            foreach (var cell in row.Cells)
            {
                if (!cell.Column.IsVisibleInLayout)
                    continue;
                var columnKey = cell.Column.Key;
                var fieldValue = cell.Value;
                var fieldValueOriginal = GetFieldEntity(filedsEntityOriginal, columnKey);

                var isEqals = fieldValueOriginal != null && fieldValueOriginal.Equals(fieldValue);

                if (!isEqals)
                {
                    cell.Appearance.ForeColor = System.Drawing.Color.Blue;
                    cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                }
                else
                {
                    cell.Appearance.ResetForeColor();
                    cell.Appearance.ResetFontData();
                }
            }
        }

        #endregion

        #region CellTooltip

        DictionaryOper dicOper = new DictionaryOper();
        DictionaryCurr dicCurr = new DictionaryCurr();
        DictionaryBargain dicBargain = new DictionaryBargain();
        void InitDict()
        {
            dicOper = _presenter.Oper;
            dicCurr = _presenter.Curr;
            dicBargain = _presenter.Barg;
        }
        private string InvoiceConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OPERATION_CODE;
            return dicOper.NAME(opcode);
        }

        private string InvoiceCurrencyConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = temp.OKV_CODE;
            return dicCurr.NAME(opcode);
        }

        private string InvoiceBargainConvert(object inv)
        {
            Invoice temp = (Invoice)inv;
            string opcode = "";
            opcode = ((int)temp.DEAL_KIND_CODE).ToString();
            return dicBargain.NAME(opcode);
        }

        private string InvoiceExplainTitleConvert(object inv)
        {
            return InvoiceExplainHelper.CreateExplainToolTip((Invoice)inv);
        }

        private Dictionary<string, Func<object, string>> GetCellToolTipsBase()
        {
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();

            InitDict();
            Func<object, string> funcOper = InvoiceConvert;
            Func<object, string> funcCurr = InvoiceCurrencyConvert;
            Func<object, string> funcBarg = InvoiceBargainConvert;
            Func<object, string> funcExplainTitle = InvoiceExplainTitleConvert;
            cellToolTips.Add(TypeHelper<Invoice>.GetMemberName(t => t.OPERATION_CODE), funcOper);
            cellToolTips.Add(TypeHelper<Invoice>.GetMemberName(t => t.OKV_CODE), funcCurr);
            cellToolTips.Add(TypeHelper<Invoice>.GetMemberName(t => t.DEAL_KIND_CODE), funcBarg);
            cellToolTips.Add(TypeHelper<Invoice>.GetMemberName(t => t.IS_CHANGED), funcExplainTitle);

            return cellToolTips;
        }

        #endregion

        #region Chapter 8

        private void InitializeChapter8()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGrid8", GetType()));
            setup.GetData = GetDataC8;

            var helper = new GridSetupHelper<InvoiceRelated>();

            setup.Columns.AddRange(GetGroupsC8(helper));

            var bandGroups = new List<ColumnGroupDefinition>(GetGroupsC8(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Groups = bandGroups
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 8);


            gridChapter8.Setup = setup;
            gridChapter8.Title = "Раздел 8";
            gridChapter8.Band_EditMode = true;
            gridChapter8.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnGroupDefinition> GetGroupsC8(GridSetupHelper<InvoiceRelated> helper)
        {
            return new[]
            {
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.IS_CHANGED, DrawPen)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OPERATION_CODE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                        helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИКСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Документ, подтверждающий уплату налога",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE, d => d.Width = WIDTH_DATE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о продавце",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.SELLER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.SELLER_KPP, definition => definition.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.SELLER_NAME, definition => definition.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о посреднике",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BROKER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.BROKER_KPP, definition => definition.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.BROKER_NAME, definition => definition.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM,
                        d =>
                        {
                            d.Caption = "Регистрационный № ТД\r\n(стр. 150)";
                            d.ToolTip = "Регистрационный номер таможенной декларации";
                            d.AllowRowFiltering = false;
                            d.SortIndicator = false;
                            d.Width = WIDTH_INVOICE;
                            d.RowSpan = 2;
                        }),
                        helper.CreateColumnDefinition(o => o.OKV_CODE),
                        helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT,DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, DrawMoney),
                        helper.CreateColumnDefinition(o => o.IsAdditionalSheet, d => { d.Caption = "Доп.лист"; d.ToolTip = "Сведения из дополнительного листа"; })
                    }
                }
            };
        }

        #endregion

        #region Chapter 9

        private void InitializeChapter9()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR9", GetType()));
            setup.GetData = GetDataC9;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC9(helper));

            var bandGroups = new List<ColumnGroupDefinition>(GetGroupsC9(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Groups = bandGroups
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 9);

            gridChapter9.Setup = setup;
            gridChapter9.Title = "Раздел 9";
            gridChapter9.Band_EditMode = true;
            gridChapter9.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnGroupDefinition> GetGroupsC9(GridSetupHelper<InvoiceRelated> helper)
        {
            return new[]
            {
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.IS_CHANGED, DrawPen)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, DrawNumber)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OPERATION_CODE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                        helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; })
                    }
                },

                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM,
                        d =>
                        {
                            d.Caption = "Регистрационный № ТД\r\n(стр. 035)";
                            d.ToolTip = "Регистрационный номер таможенной декларации";
                            d.AllowRowFiltering = false;
                            d.SortIndicator = false;
                            d.Width = WIDTH_INVOICE;
                            d.RowSpan = 2;
                        })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИКСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM,  DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Документ, подтверждающий уплату налога",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о покупателе",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BUYER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.BUYER_KPP, d => d.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.BUYER_NAME, d => d.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о посреднике",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.ToolTip = "ИНН посредника (комиссионера, агента)"; d.Width = WIDTH_INN; }),
                        helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.ToolTip = "КПП посредника (комиссионера, агента)"; d.Width = WIDTH_KPP; }),
                        //helper.CreateColumnDefinition(o => o.BROKER_NAME, d => { d.ToolTip = "Наименование посредника (комиссионера, агента)"; d.Width = WIDTH_NAME; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OKV_CODE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "Стоимость продаж с НДС",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.PRICE_SELL_IN_CURR, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_SELL, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Стоимость продаж облагаемых налогом (в руб.) без НДС",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.PRICE_SELL_18, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_SELL_10, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_SELL_0, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сумма НДС",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.PRICE_NDS_18, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_NDS_10, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, DrawMoney),
                        helper.CreateColumnDefinition(o => o.IsAdditionalSheet, d => { d.Caption = "Доп.лист"; d.ToolTip = "Сведения из дополнительного листа"; })
                    }
                }
            };
        }

        #endregion

        #region Chapter 10

        private void InitializeChapter10()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR10", GetType()));
            setup.GetData = GetDataC10;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC10(helper));

            var bandGroups = new List<ColumnGroupDefinition>(GetGroupsC10(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Groups = bandGroups
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 10);

            gridChapter10.Setup = setup;
            gridChapter10.Title = "Раздел 10";
            gridChapter10.Band_EditMode = true;
            gridChapter10.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnGroupDefinition> GetGroupsC10(GridSetupHelper<InvoiceRelated> helper)
        {
            return new[]
            {
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.IS_CHANGED, DrawPen)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OPERATION_CODE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                        helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИКСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE, definition => definition.Width = WIDTH_DATE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о покупателе",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BUYER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.BUYER_KPP, definition => definition.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.BUYER_NAME, definition => definition.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о посреднической деятельности продавца",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_KPP, definition => definition.Width = WIDTH_KPP),
                        helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_NUM, d => { d.Caption = "№ СФ"; }),
                        helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_DATE, d => { d.Caption = "Дата СФ"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.OKV_CODE),
                        helper.CreateColumnDefinition(o => o.PRICE_TOTAL, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Разница стоимости с НДС по КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, DrawMoney),
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, DrawMoney)
                    }
                }, 
                new ColumnGroupDefinition
                {
                    Caption = "Разница НДС по КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, DrawMoney),
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, DrawMoney)
                    }
                } 
            };
        }

        #endregion

        #region Chapter 11

        private void InitializeChapter11()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR11", GetType()));
            setup.GetData = GetDataC11;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC11(helper));

            var bandGroups = new List<ColumnGroupDefinition>(GetGroupsC11(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Groups = bandGroups
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 11);

            gridChapter11.Setup = setup;
            gridChapter11.Title = "Раздел 11";
            gridChapter11.Band_EditMode = true;
            gridChapter11.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnGroupDefinition> GetGroupsC11(GridSetupHelper<InvoiceRelated> helper)
        {
            return new[]
            {
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.IS_CHANGED, DrawPen)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.RECEIVE_DATE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OPERATION_CODE)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = WIDTH_INVOICE; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                        helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = WIDTH_DATE; d.ToolTip = "Дата счета-фактуры продавца"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "ИКСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, DrawNumber),
                        helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => d.Width = WIDTH_DATE)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о продавце",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.SELLER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.SELLER_KPP, definition => definition.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.SELLER_NAME, definition => definition.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о субкомиссионере",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BROKER_INN, d => d.Width = WIDTH_INN),
                        helper.CreateColumnDefinition(o => o.BROKER_KPP, definition => definition.Width = WIDTH_KPP),
                        //helper.CreateColumnDefinition(o => o.BROKER_NAME, definition => definition.Width = WIDTH_NAME)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.DEAL_KIND_CODE),
                        helper.CreateColumnDefinition(o => o.OKV_CODE),
                        helper.CreateColumnDefinition(o => o.PRICE_TOTAL, DrawMoney),
                        helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Разница стоимости с НДС по КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, DrawMoney),
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, DrawMoney)
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Разница НДС по КСФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, DrawMoney),
                        helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, DrawMoney)
                    }
                }
            };
        }

        #endregion

        #region Chapter 12

        private void InitializeChapter12()
        {
            var setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridR12", GetType()));
            setup.GetData = GetDataC12;

            var helper = new GridSetupHelper<InvoiceRelated>();
            setup.Columns.AddRange(GetGroupsC12(helper));

            var bandGroups = new List<ColumnGroupDefinition>(GetGroupsC12(helper));
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.Children),
                Groups = bandGroups
            };
            setup.CellToolTips = GetCellToolTipsBase();
            setup.Bands.Add(band);
            setup.RowActions = new List<Action<UltraGridRow>> { GridRowAction };

            SetChaptersQueryConditions(setup, 12);

            gridChapter12.Setup = setup;
            gridChapter12.Title = "Раздел 12";
            gridChapter12.Band_EditMode = true;
            gridChapter12.Band_GridRegimEdit = GridRegimEdit.View;
        }

        private IEnumerable<ColumnGroupDefinition> GetGroupsC12(GridSetupHelper<InvoiceRelated> helper)
        {
            return new[]
            {
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.IS_CHANGED, DrawPen)}
                },
                new ColumnGroupDefinition
                {
                    Caption = "СФ",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Caption = "№"; d.ToolTip = "Номер счета-фактуры продавца"; DrawNumber(d);}),
                        helper.CreateColumnDefinition(o => o.SELLER_INVOICE_DATE, d => { d.Caption = "Дата"; d.ToolTip = "Дата счета-фактуры продавца"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = "Сведения о покупателе",
                    Columns =
                    {
                        helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Caption = "ИНН"; d.ToolTip = "ИНН покупателя"; }),
                        helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Caption = "КПП"; d.ToolTip = "КПП покупателя"; })
                    }
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.Caption = "Код валюты"; d.ToolTip = "Код валюты по ОКВ"; })}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d => { d.Caption = "Стоимость без НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав без налога - всего"; DrawMoney(d);})}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.Caption = "Сумма НДС"; d.ToolTip = "Сумма налога, предъявляемая покупателю"; DrawMoney(d);})}
                },
                new ColumnGroupDefinition
                {
                    Caption = string.Empty,
                    Columns = {helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d => { d.Caption = "Стоимость с НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав с налогом - всего"; DrawMoney(d);})}
                }
            };
        }

        #endregion

        #region Data requests

        private object GetChaptersData(int chapter, QueryConditions conditions, out long totalRows)
        {
            if (CheckSelectedCurrentChapterTab(chapter))
            {
                FilterQuery filter;
                if (_chaptersGridFilter.TryGetValue(chapter, out filter))
                {
                    filter.UserDefined = true;
                    conditions.Filter.RemoveAll(x => x.ColumnName == filter.ColumnName);
                    conditions.Filter.Add(filter);
                    _chaptersGridFilter.Remove(chapter);
                }
                var result = _presenter.GetChapterInvoces(chapter, conditions, out totalRows);
                return result;
            }
            return GetChaptersDataEmpty(chapter, out totalRows);
        }

        private bool CheckSelectedCurrentChapterTab(int chapter)
        {
            return mainTabControl.SelectedTab != null
                && _chapterMapTabControls.Keys.Contains(mainTabControl.SelectedTab.Index)
                && _chapterMapTabControls[mainTabControl.SelectedTab.Index] == chapter;
        }

        private object GetChaptersDataEmpty(int chapter, out long totalRows)
        {
            GetChapterGrid(chapter).PanelLoadingVisible = false;
            totalRows = 0;
            List<InvoiceRelated> listResult = null;
            return listResult;
        }

        public object GetDataC8(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(8, conditions, out totalRows);
        }

        public object GetDataC9(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(9, conditions, out totalRows);
        }

        public object GetDataC10(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(10, conditions, out totalRows);
        }

        public object GetDataC11(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(11, conditions, out totalRows);
        }

        public object GetDataC12(QueryConditions conditions, out long totalRows)
        {
            return GetChaptersData(12, conditions, out totalRows);
        }

        public override void UpdateData(int chapter)
        {
            var grid = GetChapterGrid(chapter);
            grid.UpdateData();
        }

        private void UpdateChapterGridsData()
        {
            int chapter;
            if (_chapterMapTabControls.TryGetValue(mainTabControl.SelectedTab.VisibleIndex, out chapter))
            {
                UpdateData(chapter);
            }
        }

        #endregion

        #region Context menu handlers

        private void cmiOpenBuyer_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(mainTabControl.Tabs.IndexOf(mainTabControl.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewTaxPayerByKppOriginal(x.BUYER_INN, x.BUYER_KPP);
        }


        private void cmiOpenBroker_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(mainTabControl.Tabs.IndexOf(mainTabControl.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.ViewTaxPayerByKppOriginal(x.BROKER_INN, x.BROKER_KPP);
        }

        private void cmiOpenDeclaration_Click(object sender, EventArgs e)
        {
            var grid = GetChapterGrid(mainTabControl.Tabs.IndexOf(mainTabControl.SelectedTab) + 7);
            if (grid == null)
                return;

            var x = grid.GetCurrentItem<InvoiceRelated>();
            if (x == null)
                return;

            _presenter.DeclarationCardOpener.Open(x.DeclarationId);
        }

        #endregion
    }
}
