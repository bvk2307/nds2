﻿namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Reclaims.Card
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControlInvoice = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridInvoice = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmInvoice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiGap = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiNds = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter8 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter8_10 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenBuyer = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenBroker = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenDeclaration = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter9 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabInvoiceChapter10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter10 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabInvoiceChapter11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter11 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter11 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabInvoiceChapter12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridChapter12 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmChapter12 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageExplainList = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridAnswer = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.cmExplainList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiViewDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiEditDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControlHistory = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutHistoryStatusMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelSendToSEODCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToSEODValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToNOExecutorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToNOExecutorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelReceivedNOExecutorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelReceivedNOExecutorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToTaxPayerCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToTaxPayerValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelHandedToTaxpayerCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelHandedToTaxpayerValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCloseCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCloseValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelProlongAnswerDateCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelProlongAnswerDateValue = new Infragistics.Win.Misc.UltraLabel();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.reclaimCommonInfoCtrl = new Luxoft.NDS2.Client.UI.Controls.Reclaim.ReclaimCommonInfo();
            this.ueGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.reclaimTaxPayerInfoCtrl = new Luxoft.NDS2.Client.UI.Controls.Reclaim.ReclaimTaxPayerInfo();
            this.mainTabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControlInvoice.SuspendLayout();
            this.cmInvoice.SuspendLayout();
            this.tabInvoiceChapter8.SuspendLayout();
            this.cmChapter8_10.SuspendLayout();
            this.tabInvoiceChapter9.SuspendLayout();
            this.tabInvoiceChapter10.SuspendLayout();
            this.tabInvoiceChapter11.SuspendLayout();
            this.cmChapter11.SuspendLayout();
            this.tabInvoiceChapter12.SuspendLayout();
            this.cmChapter12.SuspendLayout();
            this.ultraTabPageExplainList.SuspendLayout();
            this.cmExplainList.SuspendLayout();
            this.ultraTabPageControlHistory.SuspendLayout();
            this.tableLayoutHistoryStatusMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).BeginInit();
            this.ueGroupBoxTaxPayerInfo.SuspendLayout();
            this.ultraExGroupBoxTaxPayerInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).BeginInit();
            this.mainTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControlInvoice
            // 
            this.ultraTabPageControlInvoice.Controls.Add(this.gridInvoice);
            this.ultraTabPageControlInvoice.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControlInvoice.Name = "ultraTabPageControlInvoice";
            this.ultraTabPageControlInvoice.Size = new System.Drawing.Size(934, 244);
            // 
            // gridInvoice
            // 
            this.gridInvoice.AddVirtualCheckColumn = false;
            this.gridInvoice.AggregatePanelVisible = true;
            this.gridInvoice.AllowMultiGrouping = true;
            this.gridInvoice.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridInvoice.AllowSaveFilterSettings = false;
            this.gridInvoice.BackColor = System.Drawing.Color.Transparent;
            this.gridInvoice.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridInvoice.ContextMenuStrip = this.cmInvoice;
            this.gridInvoice.DefaultPageSize = "200";
            this.gridInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInvoice.ExportExcelCancelVisible = false;
            this.gridInvoice.ExportExcelVisible = false;
            this.gridInvoice.FilterResetVisible = false;
            this.gridInvoice.FooterVisible = true;
            this.gridInvoice.GridContextMenuStrip = null;
            this.gridInvoice.Location = new System.Drawing.Point(0, 0);
            this.gridInvoice.Name = "gridInvoice";
            this.gridInvoice.PanelExportExcelStateVisible = false;
            this.gridInvoice.PanelLoadingVisible = true;
            this.gridInvoice.PanelPagesVisible = true;
            this.gridInvoice.RowDoubleClicked = null;
            this.gridInvoice.Setup = null;
            this.gridInvoice.Size = new System.Drawing.Size(934, 244);
            this.gridInvoice.TabIndex = 0;
            this.gridInvoice.Title = "";
            // 
            // cmInvoice
            // 
            this.cmInvoice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.cmiGap,
            this.cmiNds});
            this.cmInvoice.Name = "cmRowMenu";
            this.cmInvoice.Size = new System.Drawing.Size(301, 70);
            this.cmInvoice.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuInvoiceOpening);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(300, 22);
            this.toolStripMenuItem5.Text = "Перейти к данным по СФ";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.InvoiceDataClick);
            // 
            // cmiGap
            // 
            this.cmiGap.Name = "cmiGap";
            this.cmiGap.Size = new System.Drawing.Size(300, 22);
            this.cmiGap.Text = "Открыть карточку расхождения (Разрыв)";
            this.cmiGap.Click += new System.EventHandler(this.DiscrepancyClick);
            // 
            // cmiNds
            // 
            this.cmiNds.Name = "cmiNds";
            this.cmiNds.Size = new System.Drawing.Size(300, 22);
            this.cmiNds.Text = "Открыть карточку расхождения (НДС)";
            this.cmiNds.Click += new System.EventHandler(this.DiscrepancyClick);
            // 
            // tabInvoiceChapter8
            // 
            this.tabInvoiceChapter8.Controls.Add(this.gridChapter8);
            this.tabInvoiceChapter8.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter8.Name = "tabInvoiceChapter8";
            this.tabInvoiceChapter8.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter8
            // 
            this.gridChapter8.AddVirtualCheckColumn = false;
            this.gridChapter8.AggregatePanelVisible = true;
            this.gridChapter8.AllowMultiGrouping = true;
            this.gridChapter8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter8.AllowSaveFilterSettings = false;
            this.gridChapter8.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter8.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter8.ContextMenuStrip = this.cmChapter8_10;
            this.gridChapter8.DefaultPageSize = "200";
            this.gridChapter8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter8.ExportExcelCancelVisible = false;
            this.gridChapter8.ExportExcelVisible = false;
            this.gridChapter8.FilterResetVisible = false;
            this.gridChapter8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter8.FooterVisible = true;
            this.gridChapter8.GridContextMenuStrip = null;
            this.gridChapter8.Location = new System.Drawing.Point(0, 0);
            this.gridChapter8.Name = "gridChapter8";
            this.gridChapter8.PanelExportExcelStateVisible = false;
            this.gridChapter8.PanelLoadingVisible = true;
            this.gridChapter8.PanelPagesVisible = true;
            this.gridChapter8.RowDoubleClicked = null;
            this.gridChapter8.Setup = null;
            this.gridChapter8.Size = new System.Drawing.Size(934, 244);
            this.gridChapter8.TabIndex = 6;
            this.gridChapter8.Title = "";
            // 
            // cmChapter8_10
            // 
            this.cmChapter8_10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenBuyer,
            this.cmiOpenBroker,
            this.cmiOpenDeclaration});
            this.cmChapter8_10.Name = "cmRowMenu";
            this.cmChapter8_10.Size = new System.Drawing.Size(243, 70);
            // 
            // cmiOpenBuyer
            // 
            this.cmiOpenBuyer.Name = "cmiOpenBuyer";
            this.cmiOpenBuyer.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenBuyer.Text = "Открыть карточку покупателя";
            this.cmiOpenBuyer.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // cmiOpenBroker
            // 
            this.cmiOpenBroker.Name = "cmiOpenBroker";
            this.cmiOpenBroker.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenBroker.Text = "Открыть карточку посредника";
            this.cmiOpenBroker.Click += new System.EventHandler(this.cmiOpenBroker_Click);
            // 
            // cmiOpenDeclaration
            // 
            this.cmiOpenDeclaration.Name = "cmiOpenDeclaration";
            this.cmiOpenDeclaration.Size = new System.Drawing.Size(242, 22);
            this.cmiOpenDeclaration.Text = "Открыть декларацию/журнал";
            this.cmiOpenDeclaration.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // tabInvoiceChapter9
            // 
            this.tabInvoiceChapter9.Controls.Add(this.gridChapter9);
            this.tabInvoiceChapter9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter9.Name = "tabInvoiceChapter9";
            this.tabInvoiceChapter9.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter9
            // 
            this.gridChapter9.AddVirtualCheckColumn = false;
            this.gridChapter9.AggregatePanelVisible = true;
            this.gridChapter9.AllowMultiGrouping = true;
            this.gridChapter9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter9.AllowSaveFilterSettings = false;
            this.gridChapter9.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter9.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter9.ContextMenuStrip = this.cmChapter8_10;
            this.gridChapter9.DefaultPageSize = "";
            this.gridChapter9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter9.ExportExcelCancelVisible = false;
            this.gridChapter9.ExportExcelVisible = false;
            this.gridChapter9.FilterResetVisible = false;
            this.gridChapter9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter9.FooterVisible = true;
            this.gridChapter9.GridContextMenuStrip = null;
            this.gridChapter9.Location = new System.Drawing.Point(0, 0);
            this.gridChapter9.Name = "gridChapter9";
            this.gridChapter9.PanelExportExcelStateVisible = false;
            this.gridChapter9.PanelLoadingVisible = true;
            this.gridChapter9.PanelPagesVisible = true;
            this.gridChapter9.RowDoubleClicked = null;
            this.gridChapter9.Setup = null;
            this.gridChapter9.Size = new System.Drawing.Size(934, 244);
            this.gridChapter9.TabIndex = 3;
            this.gridChapter9.Title = "";
            // 
            // tabInvoiceChapter10
            // 
            this.tabInvoiceChapter10.Controls.Add(this.gridChapter10);
            this.tabInvoiceChapter10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter10.Name = "tabInvoiceChapter10";
            this.tabInvoiceChapter10.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter10
            // 
            this.gridChapter10.AddVirtualCheckColumn = false;
            this.gridChapter10.AggregatePanelVisible = true;
            this.gridChapter10.AllowMultiGrouping = true;
            this.gridChapter10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter10.AllowSaveFilterSettings = false;
            this.gridChapter10.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter10.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter10.ContextMenuStrip = this.cmChapter8_10;
            this.gridChapter10.DefaultPageSize = "";
            this.gridChapter10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter10.ExportExcelCancelVisible = false;
            this.gridChapter10.ExportExcelVisible = false;
            this.gridChapter10.FilterResetVisible = false;
            this.gridChapter10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter10.FooterVisible = true;
            this.gridChapter10.GridContextMenuStrip = null;
            this.gridChapter10.Location = new System.Drawing.Point(0, 0);
            this.gridChapter10.Name = "gridChapter10";
            this.gridChapter10.PanelExportExcelStateVisible = false;
            this.gridChapter10.PanelLoadingVisible = true;
            this.gridChapter10.PanelPagesVisible = true;
            this.gridChapter10.RowDoubleClicked = null;
            this.gridChapter10.Setup = null;
            this.gridChapter10.Size = new System.Drawing.Size(934, 244);
            this.gridChapter10.TabIndex = 3;
            this.gridChapter10.Title = "";
            // 
            // tabInvoiceChapter11
            // 
            this.tabInvoiceChapter11.Controls.Add(this.gridChapter11);
            this.tabInvoiceChapter11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter11.Name = "tabInvoiceChapter11";
            this.tabInvoiceChapter11.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter11
            // 
            this.gridChapter11.AddVirtualCheckColumn = false;
            this.gridChapter11.AggregatePanelVisible = true;
            this.gridChapter11.AllowMultiGrouping = true;
            this.gridChapter11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter11.AllowSaveFilterSettings = false;
            this.gridChapter11.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter11.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter11.ContextMenuStrip = this.cmChapter11;
            this.gridChapter11.DefaultPageSize = "";
            this.gridChapter11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter11.ExportExcelCancelVisible = false;
            this.gridChapter11.ExportExcelVisible = false;
            this.gridChapter11.FilterResetVisible = false;
            this.gridChapter11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter11.FooterVisible = true;
            this.gridChapter11.GridContextMenuStrip = null;
            this.gridChapter11.Location = new System.Drawing.Point(0, 0);
            this.gridChapter11.Name = "gridChapter11";
            this.gridChapter11.PanelExportExcelStateVisible = false;
            this.gridChapter11.PanelLoadingVisible = true;
            this.gridChapter11.PanelPagesVisible = true;
            this.gridChapter11.RowDoubleClicked = null;
            this.gridChapter11.Setup = null;
            this.gridChapter11.Size = new System.Drawing.Size(934, 244);
            this.gridChapter11.TabIndex = 4;
            this.gridChapter11.Title = "";
            // 
            // cmChapter11
            // 
            this.cmChapter11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.cmChapter11.Name = "cmRowMenu";
            this.cmChapter11.Size = new System.Drawing.Size(278, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem1.Text = "Открыть карточку покупателя";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem2.Text = "Открыть карточку субкомиссионера";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.cmiOpenBroker_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(277, 22);
            this.toolStripMenuItem3.Text = "Открыть декларацию/журнал";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // tabInvoiceChapter12
            // 
            this.tabInvoiceChapter12.Controls.Add(this.gridChapter12);
            this.tabInvoiceChapter12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabInvoiceChapter12.Name = "tabInvoiceChapter12";
            this.tabInvoiceChapter12.Size = new System.Drawing.Size(934, 244);
            // 
            // gridChapter12
            // 
            this.gridChapter12.AddVirtualCheckColumn = false;
            this.gridChapter12.AggregatePanelVisible = true;
            this.gridChapter12.AllowMultiGrouping = true;
            this.gridChapter12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridChapter12.AllowSaveFilterSettings = false;
            this.gridChapter12.BackColor = System.Drawing.Color.Transparent;
            this.gridChapter12.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridChapter12.ContextMenuStrip = this.cmChapter12;
            this.gridChapter12.DefaultPageSize = "";
            this.gridChapter12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChapter12.ExportExcelCancelVisible = false;
            this.gridChapter12.ExportExcelVisible = false;
            this.gridChapter12.FilterResetVisible = false;
            this.gridChapter12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridChapter12.FooterVisible = true;
            this.gridChapter12.GridContextMenuStrip = null;
            this.gridChapter12.Location = new System.Drawing.Point(0, 0);
            this.gridChapter12.Name = "gridChapter12";
            this.gridChapter12.PanelExportExcelStateVisible = false;
            this.gridChapter12.PanelLoadingVisible = true;
            this.gridChapter12.PanelPagesVisible = true;
            this.gridChapter12.RowDoubleClicked = null;
            this.gridChapter12.Setup = null;
            this.gridChapter12.Size = new System.Drawing.Size(934, 244);
            this.gridChapter12.TabIndex = 3;
            this.gridChapter12.Title = "";
            // 
            // cmChapter12
            // 
            this.cmChapter12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem6});
            this.cmChapter12.Name = "cmRowMenu";
            this.cmChapter12.Size = new System.Drawing.Size(241, 48);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(240, 22);
            this.toolStripMenuItem4.Text = "Открыть карточку покупателя";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.cmiOpenBuyer_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(240, 22);
            this.toolStripMenuItem6.Text = "Открыть декларацию/журнал";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // ultraTabPageExplainList
            // 
            this.ultraTabPageExplainList.Controls.Add(this.gridAnswer);
            this.ultraTabPageExplainList.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageExplainList.Name = "ultraTabPageExplainList";
            this.ultraTabPageExplainList.Size = new System.Drawing.Size(934, 244);
            // 
            // gridAnswer
            // 
            this.gridAnswer.AddVirtualCheckColumn = false;
            this.gridAnswer.AggregatePanelVisible = true;
            this.gridAnswer.AllowMultiGrouping = true;
            this.gridAnswer.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridAnswer.AllowSaveFilterSettings = false;
            this.gridAnswer.BackColor = System.Drawing.Color.Transparent;
            this.gridAnswer.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.gridAnswer.ContextMenuStrip = this.cmExplainList;
            this.gridAnswer.DefaultPageSize = "";
            this.gridAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAnswer.ExportExcelCancelVisible = false;
            this.gridAnswer.ExportExcelVisible = false;
            this.gridAnswer.FilterResetVisible = false;
            this.gridAnswer.FooterVisible = true;
            this.gridAnswer.GridContextMenuStrip = null;
            this.gridAnswer.Location = new System.Drawing.Point(0, 0);
            this.gridAnswer.Name = "gridAnswer";
            this.gridAnswer.PanelExportExcelStateVisible = false;
            this.gridAnswer.PanelLoadingVisible = false;
            this.gridAnswer.PanelPagesVisible = true;
            this.gridAnswer.RowDoubleClicked = null;
            this.gridAnswer.Setup = null;
            this.gridAnswer.Size = new System.Drawing.Size(934, 244);
            this.gridAnswer.TabIndex = 5;
            this.gridAnswer.Title = "";
            // 
            // cmExplainList
            // 
            this.cmExplainList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiViewDescription,
            this.cmiEditDescription});
            this.cmExplainList.Name = "cmRowMenu";
            this.cmExplainList.Size = new System.Drawing.Size(298, 48);
            this.cmExplainList.Opening += new System.ComponentModel.CancelEventHandler(this.cmExplainList_Opening);
            // 
            // cmiViewDescription
            // 
            this.cmiViewDescription.Name = "cmiViewDescription";
            this.cmiViewDescription.Size = new System.Drawing.Size(297, 22);
            this.cmiViewDescription.Text = "Открыть карточку ответа";
            this.cmiViewDescription.Click += new System.EventHandler(this.OpenDescriptionCardClick);
            // 
            // cmiEditDescription
            // 
            this.cmiEditDescription.Name = "cmiEditDescription";
            this.cmiEditDescription.Size = new System.Drawing.Size(297, 22);
            this.cmiEditDescription.Text = "Открыть карточку ответа для изменения";
            this.cmiEditDescription.Click += new System.EventHandler(this.OpenDescriptionCardClick);
            // 
            // ultraTabPageControlHistory
            // 
            this.ultraTabPageControlHistory.Controls.Add(this.tableLayoutHistoryStatusMain);
            this.ultraTabPageControlHistory.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControlHistory.Name = "ultraTabPageControlHistory";
            this.ultraTabPageControlHistory.Size = new System.Drawing.Size(934, 244);
            // 
            // tableLayoutHistoryStatusMain
            // 
            this.tableLayoutHistoryStatusMain.ColumnCount = 7;
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToSEODCaption, 0, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToSEODValue, 1, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToNOExecutorCaption, 0, 1);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToNOExecutorValue, 1, 1);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelReceivedNOExecutorCaption, 0, 2);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelReceivedNOExecutorValue, 1, 2);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToTaxPayerCaption, 0, 3);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelSendToTaxPayerValue, 1, 3);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelHandedToTaxpayerCaption, 0, 4);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelHandedToTaxpayerValue, 1, 4);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelCloseCaption, 0, 5);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelCloseValue, 1, 5);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelProlongAnswerDateCaption, 2, 4);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelProlongAnswerDateValue, 3, 4);
            this.tableLayoutHistoryStatusMain.Location = new System.Drawing.Point(3, 6);
            this.tableLayoutHistoryStatusMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutHistoryStatusMain.Name = "tableLayoutHistoryStatusMain";
            this.tableLayoutHistoryStatusMain.RowCount = 7;
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutHistoryStatusMain.Size = new System.Drawing.Size(892, 150);
            this.tableLayoutHistoryStatusMain.TabIndex = 1;
            // 
            // labelSendToSEODCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            this.labelSendToSEODCaption.Appearance = appearance2;
            this.labelSendToSEODCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToSEODCaption.Location = new System.Drawing.Point(1, 1);
            this.labelSendToSEODCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToSEODCaption.Name = "labelSendToSEODCaption";
            this.labelSendToSEODCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToSEODCaption.TabIndex = 2;
            this.labelSendToSEODCaption.Text = "Отправлено в СЭОД:";
            // 
            // labelSendToSEODValue
            // 
            this.labelSendToSEODValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToSEODValue.Location = new System.Drawing.Point(201, 1);
            this.labelSendToSEODValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToSEODValue.Name = "labelSendToSEODValue";
            this.labelSendToSEODValue.Size = new System.Drawing.Size(98, 23);
            this.labelSendToSEODValue.TabIndex = 3;
            this.labelSendToSEODValue.Text = "10.05.2015";
            // 
            // labelSendToNOExecutorCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            this.labelSendToNOExecutorCaption.Appearance = appearance3;
            this.labelSendToNOExecutorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToNOExecutorCaption.Location = new System.Drawing.Point(1, 26);
            this.labelSendToNOExecutorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToNOExecutorCaption.Name = "labelSendToNOExecutorCaption";
            this.labelSendToNOExecutorCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToNOExecutorCaption.TabIndex = 4;
            this.labelSendToNOExecutorCaption.Text = "Отправлено в НО-испольнитель:";
            // 
            // labelSendToNOExecutorValue
            // 
            this.labelSendToNOExecutorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToNOExecutorValue.Location = new System.Drawing.Point(201, 26);
            this.labelSendToNOExecutorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToNOExecutorValue.Name = "labelSendToNOExecutorValue";
            this.labelSendToNOExecutorValue.Size = new System.Drawing.Size(98, 23);
            this.labelSendToNOExecutorValue.TabIndex = 5;
            this.labelSendToNOExecutorValue.Text = "10.05.2015";
            // 
            // labelReceivedNOExecutorCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            this.labelReceivedNOExecutorCaption.Appearance = appearance4;
            this.labelReceivedNOExecutorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReceivedNOExecutorCaption.Location = new System.Drawing.Point(1, 51);
            this.labelReceivedNOExecutorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelReceivedNOExecutorCaption.Name = "labelReceivedNOExecutorCaption";
            this.labelReceivedNOExecutorCaption.Size = new System.Drawing.Size(198, 23);
            this.labelReceivedNOExecutorCaption.TabIndex = 6;
            this.labelReceivedNOExecutorCaption.Text = "Получено НО-исполнителем:";
            // 
            // labelReceivedNOExecutorValue
            // 
            this.labelReceivedNOExecutorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReceivedNOExecutorValue.Location = new System.Drawing.Point(201, 51);
            this.labelReceivedNOExecutorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelReceivedNOExecutorValue.Name = "labelReceivedNOExecutorValue";
            this.labelReceivedNOExecutorValue.Size = new System.Drawing.Size(98, 23);
            this.labelReceivedNOExecutorValue.TabIndex = 7;
            this.labelReceivedNOExecutorValue.Text = "11.05.2015";
            // 
            // labelSendToTaxPayerCaption
            // 
            appearance5.TextHAlignAsString = "Right";
            this.labelSendToTaxPayerCaption.Appearance = appearance5;
            this.labelSendToTaxPayerCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToTaxPayerCaption.Location = new System.Drawing.Point(1, 76);
            this.labelSendToTaxPayerCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToTaxPayerCaption.Name = "labelSendToTaxPayerCaption";
            this.labelSendToTaxPayerCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToTaxPayerCaption.TabIndex = 8;
            this.labelSendToTaxPayerCaption.Text = "Отправлено налогоплательщику:";
            // 
            // labelSendToTaxPayerValue
            // 
            this.labelSendToTaxPayerValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToTaxPayerValue.Location = new System.Drawing.Point(201, 76);
            this.labelSendToTaxPayerValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToTaxPayerValue.Name = "labelSendToTaxPayerValue";
            this.labelSendToTaxPayerValue.Size = new System.Drawing.Size(98, 23);
            this.labelSendToTaxPayerValue.TabIndex = 9;
            this.labelSendToTaxPayerValue.Text = "12.05.2015";
            // 
            // labelHandedToTaxpayerCaption
            // 
            appearance6.TextHAlignAsString = "Right";
            this.labelHandedToTaxpayerCaption.Appearance = appearance6;
            this.labelHandedToTaxpayerCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHandedToTaxpayerCaption.Location = new System.Drawing.Point(1, 101);
            this.labelHandedToTaxpayerCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelHandedToTaxpayerCaption.Name = "labelHandedToTaxpayerCaption";
            this.labelHandedToTaxpayerCaption.Size = new System.Drawing.Size(198, 23);
            this.labelHandedToTaxpayerCaption.TabIndex = 10;
            this.labelHandedToTaxpayerCaption.Text = "Вручено налогоплательщику:";
            // 
            // labelHandedToTaxpayerValue
            // 
            this.labelHandedToTaxpayerValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHandedToTaxpayerValue.Location = new System.Drawing.Point(201, 101);
            this.labelHandedToTaxpayerValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelHandedToTaxpayerValue.Name = "labelHandedToTaxpayerValue";
            this.labelHandedToTaxpayerValue.Size = new System.Drawing.Size(98, 23);
            this.labelHandedToTaxpayerValue.TabIndex = 11;
            this.labelHandedToTaxpayerValue.Text = "14.05.2015";
            // 
            // labelCloseCaption
            // 
            appearance7.TextHAlignAsString = "Right";
            this.labelCloseCaption.Appearance = appearance7;
            this.labelCloseCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCloseCaption.Location = new System.Drawing.Point(1, 126);
            this.labelCloseCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCloseCaption.Name = "labelCloseCaption";
            this.labelCloseCaption.Size = new System.Drawing.Size(198, 23);
            this.labelCloseCaption.TabIndex = 12;
            this.labelCloseCaption.Text = "Закрыто:";
            // 
            // labelCloseValue
            // 
            this.labelCloseValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCloseValue.Location = new System.Drawing.Point(201, 126);
            this.labelCloseValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCloseValue.Name = "labelCloseValue";
            this.labelCloseValue.Size = new System.Drawing.Size(98, 23);
            this.labelCloseValue.TabIndex = 13;
            this.labelCloseValue.Text = "15.05.2015";
            // 
            // labelProlongAnswerDateCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            this.labelProlongAnswerDateCaption.Appearance = appearance9;
            this.labelProlongAnswerDateCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProlongAnswerDateCaption.Location = new System.Drawing.Point(301, 101);
            this.labelProlongAnswerDateCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelProlongAnswerDateCaption.Name = "labelProlongAnswerDateCaption";
            this.labelProlongAnswerDateCaption.Size = new System.Drawing.Size(198, 23);
            this.labelProlongAnswerDateCaption.TabIndex = 16;
            this.labelProlongAnswerDateCaption.Text = "Дата продления ответа:";
            // 
            // labelProlongAnswerDateValue
            // 
            this.labelProlongAnswerDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProlongAnswerDateValue.Location = new System.Drawing.Point(501, 101);
            this.labelProlongAnswerDateValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelProlongAnswerDateValue.Name = "labelProlongAnswerDateValue";
            this.labelProlongAnswerDateValue.Size = new System.Drawing.Size(78, 23);
            this.labelProlongAnswerDateValue.TabIndex = 17;
            this.labelProlongAnswerDateValue.Text = "28.05.2015";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(32, 19);
            // 
            // reclaimCommonInfoCtrl
            // 
            this.reclaimCommonInfoCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.reclaimCommonInfoCtrl.Location = new System.Drawing.Point(0, 0);
            this.reclaimCommonInfoCtrl.Name = "reclaimCommonInfoCtrl";
            this.reclaimCommonInfoCtrl.Size = new System.Drawing.Size(938, 23);
            this.reclaimCommonInfoCtrl.TabIndex = 0;
            this.reclaimCommonInfoCtrl.CorrectionNumberLinkClicked += new System.EventHandler(this.ReclaimCommonInfoCtrlCorrectionNumberLinkClicked);
            // 
            // ueGroupBoxTaxPayerInfo
            // 
            this.ueGroupBoxTaxPayerInfo.Controls.Add(this.ultraExGroupBoxTaxPayerInfo);
            this.ueGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.ueGroupBoxTaxPayerInfo.ExpandedSize = new System.Drawing.Size(938, 147);
            this.ueGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(0, 23);
            this.ueGroupBoxTaxPayerInfo.Name = "ueGroupBoxTaxPayerInfo";
            this.ueGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(938, 147);
            this.ueGroupBoxTaxPayerInfo.TabIndex = 1;
            this.ueGroupBoxTaxPayerInfo.Text = "Общие сведения";
            // 
            // ultraExGroupBoxTaxPayerInfo
            // 
            this.ultraExGroupBoxTaxPayerInfo.Controls.Add(this.reclaimTaxPayerInfoCtrl);
            this.ultraExGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(3, 19);
            this.ultraExGroupBoxTaxPayerInfo.Name = "ultraExGroupBoxTaxPayerInfo";
            this.ultraExGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(932, 125);
            this.ultraExGroupBoxTaxPayerInfo.TabIndex = 0;
            // 
            // reclaimTaxPayerInfoCtrl
            // 
            this.reclaimTaxPayerInfoCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.reclaimTaxPayerInfoCtrl.Location = new System.Drawing.Point(0, 0);
            this.reclaimTaxPayerInfoCtrl.Name = "reclaimTaxPayerInfoCtrl";
            this.reclaimTaxPayerInfoCtrl.Size = new System.Drawing.Size(932, 125);
            this.reclaimTaxPayerInfoCtrl.TabIndex = 0;
            this.reclaimTaxPayerInfoCtrl.OnInnClick += new System.EventHandler(this.reclaimTaxPayerInfoCtrl_OnInnClick);
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.ultraTabSharedControlsPage1);
            this.mainTabControl.Controls.Add(this.ultraTabPageControlInvoice);
            this.mainTabControl.Controls.Add(this.ultraTabPageControlHistory);
            this.mainTabControl.Controls.Add(this.tabInvoiceChapter8);
            this.mainTabControl.Controls.Add(this.tabInvoiceChapter9);
            this.mainTabControl.Controls.Add(this.tabInvoiceChapter10);
            this.mainTabControl.Controls.Add(this.tabInvoiceChapter11);
            this.mainTabControl.Controls.Add(this.tabInvoiceChapter12);
            this.mainTabControl.Controls.Add(this.ultraTabPageExplainList);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 170);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.mainTabControl.Size = new System.Drawing.Size(938, 270);
            this.mainTabControl.TabIndex = 4;
            ultraTab1.Key = "tabInvoice";
            ultraTab1.TabPage = this.ultraTabPageControlInvoice;
            ultraTab1.Text = "Записи о СФ";
            ultraTab4.Key = "tabChapter8";
            ultraTab4.TabPage = this.tabInvoiceChapter8;
            ultraTab4.Text = "Раздел 8";
            ultraTab5.Key = "tabChapter9";
            ultraTab5.TabPage = this.tabInvoiceChapter9;
            ultraTab5.Text = "Раздел 9";
            ultraTab6.Key = "tabChapter10";
            ultraTab6.TabPage = this.tabInvoiceChapter10;
            ultraTab6.Text = "Раздел 10";
            ultraTab7.Key = "tabChapter11";
            ultraTab7.TabPage = this.tabInvoiceChapter11;
            ultraTab7.Text = "Раздел 11";
            ultraTab8.Key = "tabChapter12";
            ultraTab8.TabPage = this.tabInvoiceChapter12;
            ultraTab8.Text = "Раздел 12";
            ultraTab9.Key = "tabExplainList";
            ultraTab9.TabPage = this.ultraTabPageExplainList;
            ultraTab9.Text = "Список ответов";
            ultraTab2.Key = "tabHistoryChangeStatus";
            ultraTab2.TabPage = this.ultraTabPageControlHistory;
            ultraTab2.Text = "История изменения статуса";
            this.mainTabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab8,
            ultraTab9,
            ultraTab2});
            this.mainTabControl.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.mainTabControl_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(934, 244);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.ueGroupBoxTaxPayerInfo);
            this.Controls.Add(this.reclaimCommonInfoCtrl);
            this.Name = "View";
            this.Size = new System.Drawing.Size(938, 440);
            this.OnFormClosing += new System.EventHandler(this.View_OnFormClosing);
            this.ultraTabPageControlInvoice.ResumeLayout(false);
            this.cmInvoice.ResumeLayout(false);
            this.tabInvoiceChapter8.ResumeLayout(false);
            this.cmChapter8_10.ResumeLayout(false);
            this.tabInvoiceChapter9.ResumeLayout(false);
            this.tabInvoiceChapter10.ResumeLayout(false);
            this.tabInvoiceChapter11.ResumeLayout(false);
            this.cmChapter11.ResumeLayout(false);
            this.tabInvoiceChapter12.ResumeLayout(false);
            this.cmChapter12.ResumeLayout(false);
            this.ultraTabPageExplainList.ResumeLayout(false);
            this.cmExplainList.ResumeLayout(false);
            this.ultraTabPageControlHistory.ResumeLayout(false);
            this.tableLayoutHistoryStatusMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).EndInit();
            this.ueGroupBoxTaxPayerInfo.ResumeLayout(false);
            this.ultraExGroupBoxTaxPayerInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).EndInit();
            this.mainTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Reclaim.ReclaimCommonInfo reclaimCommonInfoCtrl;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ueGroupBoxTaxPayerInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExGroupBoxTaxPayerInfo;
        private Controls.Reclaim.ReclaimTaxPayerInfo reclaimTaxPayerInfoCtrl;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl mainTabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControlInvoice;
        private Controls.Grid.V1.GridControl gridInvoice;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControlHistory;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter8;
        private Controls.Grid.V1.GridControl gridChapter8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter9;
        private Controls.Grid.V1.GridControl gridChapter9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter10;
        private Controls.Grid.V1.GridControl gridChapter10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter11;
        private Controls.Grid.V1.GridControl gridChapter11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabInvoiceChapter12;
        private Controls.Grid.V1.GridControl gridChapter12;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageExplainList;
        private Controls.Grid.V1.GridControl gridAnswer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutHistoryStatusMain;
        private Infragistics.Win.Misc.UltraLabel labelSendToSEODCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToSEODValue;
        private Infragistics.Win.Misc.UltraLabel labelSendToNOExecutorCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToNOExecutorValue;
        private Infragistics.Win.Misc.UltraLabel labelReceivedNOExecutorCaption;
        private Infragistics.Win.Misc.UltraLabel labelReceivedNOExecutorValue;
        private Infragistics.Win.Misc.UltraLabel labelSendToTaxPayerCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToTaxPayerValue;
        private Infragistics.Win.Misc.UltraLabel labelHandedToTaxpayerCaption;
        private Infragistics.Win.Misc.UltraLabel labelHandedToTaxpayerValue;
        private Infragistics.Win.Misc.UltraLabel labelCloseCaption;
        private Infragistics.Win.Misc.UltraLabel labelCloseValue;
        private Infragistics.Win.Misc.UltraLabel labelProlongAnswerDateCaption;
        private Infragistics.Win.Misc.UltraLabel labelProlongAnswerDateValue;
        private System.Windows.Forms.ContextMenuStrip cmChapter8_10;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenBuyer;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenBroker;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclaration;
        private System.Windows.Forms.ContextMenuStrip cmChapter11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip cmChapter12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ContextMenuStrip cmInvoice;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cmiGap;
        private System.Windows.Forms.ToolStripMenuItem cmiNds;
        private System.Windows.Forms.ContextMenuStrip cmExplainList;
        private System.Windows.Forms.ToolStripMenuItem cmiViewDescription;
        private System.Windows.Forms.ToolStripMenuItem cmiEditDescription;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
    }
}
