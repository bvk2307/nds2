﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models
{
    public class DocStatusDate
    {
        public static readonly string Create = "Create";
        public static readonly string SendToSEOD = "SendToSEOD";
        public static readonly string SendToNOExecutor = "SendToNOExecutor";
        public static readonly string ReceivedNOExecutor = "ReceivedNOExecutor";
        public static readonly string SendToNP = "SendToNP";
        public static readonly string CalculateReceived = "CalculateReceived";
        public static readonly string ReceivedNP = "ReceivedNP";
        public static readonly string CalculateWaitAnswer = "CalculateWaitAnswer";
        public static readonly string CalculateProlong = "CalculateProlong";
        public static readonly string Close = "Close";
    }

    public class DocType
    {
        public static readonly int Reclaim93 = 2;
        public static readonly int Reclaim93_1 = 3;
        public static readonly int ClaimSF = 1;
        public static readonly int ClaimKS = 5;
    }

    public class DocStatusCode
    {

        public static int Create(int docType)
        {
            return 1;
        }

        public static int SendToSEOD(int docType)
        {
            return 2;
        }

        public static int SendToNP(int docType)
        {
            return 4;
        }

        public static int ReceivedNP(int docType)
        {
            return 5;
        }

        public static int SendToNOExecutor(int docType)
        {
            return 6;
        }

        public static int ReceivedNOExecutor(int docType)
        {
            return 7;
        }

        public static int Close(int docType)
        {
            if (docType == DocType.ClaimKS || docType == DocType.ClaimSF ||
                docType == DocType.Reclaim93)
            {
                return 6;
            }
            if (docType == DocType.Reclaim93_1)
            {
                return 8;
            }
            return 8;
        }
    }
}
