﻿namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel ulAmountCaption;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCloseDataCaption;
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulAmountPVPCaption;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulIssueDateCaption;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulDiscrepancyNumberCaption;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulDiscrepancyTypeCaption;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulDiscrepancyStatusCaption;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCurInfoCodeCurrency;
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCurInfoSalesCostInCurrency;
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCurInfoSalesCostInRuble;
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCurInfoRate;
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulCurInfoSalesCostRate;
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraPanel upSides;
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel labelCommentCaption;
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulReorgINN;
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.ugbCurrencyInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.tbCurInfoSalesCostRate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCurInfoRate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCurInfoSalesCostInRuble = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCurInfoSalesCostInCurrency = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCurInfoCodeCurrency = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ugbDiscrepancyInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.discrepancyGridControl = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.DiscrepancyGridControl();
            this.upMain = new Infragistics.Win.Misc.UltraPanel();
            this.uegbCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tableLayoutCommonMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutCommonLeft = new System.Windows.Forms.TableLayoutPanel();
            this.tbIssueDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbAmountPVP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tableLayoutCommonRight = new System.Windows.Forms.TableLayoutPanel();
            this.tbReorgINN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCloseDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.upHeader = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayoutPanelHeader = new System.Windows.Forms.TableLayoutPanel();
            this.ulDiscrepancyStatus = new Infragistics.Win.Misc.UltraLabel();
            this.ulDiscrepancyNumber = new Infragistics.Win.Misc.UltraLabel();
            this.ulDiscrepancyType = new Infragistics.Win.Misc.UltraLabel();
            this.labelSideInfoCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStageCaption = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutSideInfo = new System.Windows.Forms.TableLayoutPanel();
            this.linkLabelSideInfoInnVaalue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.labelStageValue = new Infragistics.Win.Misc.UltraLabel();
            ulAmountCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCloseDataCaption = new Infragistics.Win.Misc.UltraLabel();
            ulAmountPVPCaption = new Infragistics.Win.Misc.UltraLabel();
            ulIssueDateCaption = new Infragistics.Win.Misc.UltraLabel();
            ulDiscrepancyNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            ulDiscrepancyTypeCaption = new Infragistics.Win.Misc.UltraLabel();
            ulDiscrepancyStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            ulCurInfoCodeCurrency = new Infragistics.Win.Misc.UltraLabel();
            ulCurInfoSalesCostInCurrency = new Infragistics.Win.Misc.UltraLabel();
            ulCurInfoSalesCostInRuble = new Infragistics.Win.Misc.UltraLabel();
            ulCurInfoRate = new Infragistics.Win.Misc.UltraLabel();
            ulCurInfoSalesCostRate = new Infragistics.Win.Misc.UltraLabel();
            upSides = new Infragistics.Win.Misc.UltraPanel();
            labelCommentCaption = new Infragistics.Win.Misc.UltraLabel();
            ulReorgINN = new Infragistics.Win.Misc.UltraLabel();
            upSides.ClientArea.SuspendLayout();
            upSides.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugbCurrencyInfo)).BeginInit();
            this.ugbCurrencyInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostInRuble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostInCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoCodeCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbDiscrepancyInfo)).BeginInit();
            this.ugbDiscrepancyInfo.SuspendLayout();
            this.upMain.ClientArea.SuspendLayout();
            this.upMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).BeginInit();
            this.uegbCommonInfo.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.tableLayoutCommonMain.SuspendLayout();
            this.tableLayoutCommonLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmountPVP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).BeginInit();
            this.tableLayoutCommonRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbReorgINN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCloseDate)).BeginInit();
            this.upHeader.ClientArea.SuspendLayout();
            this.upHeader.SuspendLayout();
            this.tableLayoutPanelHeader.SuspendLayout();
            this.tableLayoutSideInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // ulAmountCaption
            // 
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            ulAmountCaption.Appearance = appearance8;
            ulAmountCaption.Location = new System.Drawing.Point(3, 24);
            ulAmountCaption.Name = "ulAmountCaption";
            ulAmountCaption.Size = new System.Drawing.Size(187, 15);
            ulAmountCaption.TabIndex = 2;
            ulAmountCaption.Text = "Сумма расхождения (руб.):";
            ulAmountCaption.WrapText = false;
            // 
            // ulCloseDataCaption
            // 
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            ulCloseDataCaption.Appearance = appearance12;
            ulCloseDataCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            ulCloseDataCaption.Location = new System.Drawing.Point(3, 3);
            ulCloseDataCaption.Name = "ulCloseDataCaption";
            ulCloseDataCaption.Size = new System.Drawing.Size(104, 15);
            ulCloseDataCaption.TabIndex = 0;
            ulCloseDataCaption.Text = "Дата закрытия:";
            ulCloseDataCaption.WrapText = false;
            // 
            // ulAmountPVPCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            ulAmountPVPCaption.Appearance = appearance9;
            ulAmountPVPCaption.Location = new System.Drawing.Point(3, 45);
            ulAmountPVPCaption.Name = "ulAmountPVPCaption";
            ulAmountPVPCaption.Size = new System.Drawing.Size(187, 15);
            ulAmountPVPCaption.TabIndex = 4;
            ulAmountPVPCaption.Text = "ПВП расхождения (руб.):";
            ulAmountPVPCaption.WrapText = false;
            // 
            // ulIssueDateCaption
            // 
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            ulIssueDateCaption.Appearance = appearance10;
            ulIssueDateCaption.Location = new System.Drawing.Point(3, 3);
            ulIssueDateCaption.Name = "ulIssueDateCaption";
            ulIssueDateCaption.Size = new System.Drawing.Size(187, 15);
            ulIssueDateCaption.TabIndex = 0;
            ulIssueDateCaption.Text = "Дата выявления расхождения:";
            ulIssueDateCaption.WrapText = false;
            // 
            // ulDiscrepancyNumberCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            ulDiscrepancyNumberCaption.Appearance = appearance4;
            ulDiscrepancyNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            ulDiscrepancyNumberCaption.Location = new System.Drawing.Point(1, 1);
            ulDiscrepancyNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            ulDiscrepancyNumberCaption.Name = "ulDiscrepancyNumberCaption";
            ulDiscrepancyNumberCaption.Size = new System.Drawing.Size(148, 18);
            ulDiscrepancyNumberCaption.TabIndex = 1;
            ulDiscrepancyNumberCaption.Text = "Расхождение №";
            // 
            // ulDiscrepancyTypeCaption
            // 
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            ulDiscrepancyTypeCaption.Appearance = appearance5;
            ulDiscrepancyTypeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            ulDiscrepancyTypeCaption.Location = new System.Drawing.Point(1, 21);
            ulDiscrepancyTypeCaption.Margin = new System.Windows.Forms.Padding(1);
            ulDiscrepancyTypeCaption.Name = "ulDiscrepancyTypeCaption";
            ulDiscrepancyTypeCaption.Size = new System.Drawing.Size(148, 18);
            ulDiscrepancyTypeCaption.TabIndex = 6;
            ulDiscrepancyTypeCaption.Text = "Вид расхождения:";
            // 
            // ulDiscrepancyStatusCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            ulDiscrepancyStatusCaption.Appearance = appearance2;
            ulDiscrepancyStatusCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            ulDiscrepancyStatusCaption.Location = new System.Drawing.Point(1, 41);
            ulDiscrepancyStatusCaption.Margin = new System.Windows.Forms.Padding(1);
            ulDiscrepancyStatusCaption.Name = "ulDiscrepancyStatusCaption";
            ulDiscrepancyStatusCaption.Size = new System.Drawing.Size(148, 18);
            ulDiscrepancyStatusCaption.TabIndex = 2;
            ulDiscrepancyStatusCaption.Text = "Статус расхождения:";
            // 
            // ulCurInfoCodeCurrency
            // 
            appearance20.TextHAlignAsString = "Right";
            appearance20.TextVAlignAsString = "Middle";
            ulCurInfoCodeCurrency.Appearance = appearance20;
            ulCurInfoCodeCurrency.Location = new System.Drawing.Point(6, 25);
            ulCurInfoCodeCurrency.Margin = new System.Windows.Forms.Padding(0);
            ulCurInfoCodeCurrency.Name = "ulCurInfoCodeCurrency";
            ulCurInfoCodeCurrency.Size = new System.Drawing.Size(193, 21);
            ulCurInfoCodeCurrency.TabIndex = 0;
            ulCurInfoCodeCurrency.Text = "Код валюты:";
            ulCurInfoCodeCurrency.WrapText = false;
            // 
            // ulCurInfoSalesCostInCurrency
            // 
            appearance21.TextHAlignAsString = "Right";
            appearance21.TextVAlignAsString = "Middle";
            ulCurInfoSalesCostInCurrency.Appearance = appearance21;
            ulCurInfoSalesCostInCurrency.Location = new System.Drawing.Point(6, 44);
            ulCurInfoSalesCostInCurrency.Margin = new System.Windows.Forms.Padding(0);
            ulCurInfoSalesCostInCurrency.Name = "ulCurInfoSalesCostInCurrency";
            ulCurInfoSalesCostInCurrency.Size = new System.Drawing.Size(193, 21);
            ulCurInfoSalesCostInCurrency.TabIndex = 6;
            ulCurInfoSalesCostInCurrency.Text = "Стоимость продаж в валюте:";
            ulCurInfoSalesCostInCurrency.WrapText = false;
            // 
            // ulCurInfoSalesCostInRuble
            // 
            appearance30.TextHAlignAsString = "Right";
            appearance30.TextVAlignAsString = "Middle";
            ulCurInfoSalesCostInRuble.Appearance = appearance30;
            ulCurInfoSalesCostInRuble.Location = new System.Drawing.Point(6, 63);
            ulCurInfoSalesCostInRuble.Margin = new System.Windows.Forms.Padding(0);
            ulCurInfoSalesCostInRuble.Name = "ulCurInfoSalesCostInRuble";
            ulCurInfoSalesCostInRuble.Size = new System.Drawing.Size(193, 21);
            ulCurInfoSalesCostInRuble.TabIndex = 8;
            ulCurInfoSalesCostInRuble.Text = "Стоимость продаж в рублях:";
            ulCurInfoSalesCostInRuble.WrapText = false;
            // 
            // ulCurInfoRate
            // 
            appearance34.TextHAlignAsString = "Right";
            appearance34.TextVAlignAsString = "Middle";
            ulCurInfoRate.Appearance = appearance34;
            ulCurInfoRate.Location = new System.Drawing.Point(394, 25);
            ulCurInfoRate.Margin = new System.Windows.Forms.Padding(0);
            ulCurInfoRate.Name = "ulCurInfoRate";
            ulCurInfoRate.Size = new System.Drawing.Size(73, 21);
            ulCurInfoRate.TabIndex = 2;
            ulCurInfoRate.Text = "Курс:";
            ulCurInfoRate.WrapText = false;
            // 
            // ulCurInfoSalesCostRate
            // 
            appearance35.TextHAlignAsString = "Right";
            appearance35.TextVAlignAsString = "Middle";
            ulCurInfoSalesCostRate.Appearance = appearance35;
            ulCurInfoSalesCostRate.Location = new System.Drawing.Point(637, 25);
            ulCurInfoSalesCostRate.Margin = new System.Windows.Forms.Padding(0);
            ulCurInfoSalesCostRate.Name = "ulCurInfoSalesCostRate";
            ulCurInfoSalesCostRate.Size = new System.Drawing.Size(192, 21);
            ulCurInfoSalesCostRate.TabIndex = 4;
            ulCurInfoSalesCostRate.Text = "Стоимость продаж по курсу:";
            ulCurInfoSalesCostRate.WrapText = false;
            // 
            // upSides
            // 
            // 
            // upSides.ClientArea
            // 
            upSides.ClientArea.Controls.Add(this.tableLayoutPanelMain);
            upSides.Dock = System.Windows.Forms.DockStyle.Fill;
            upSides.Location = new System.Drawing.Point(0, 171);
            upSides.Name = "upSides";
            upSides.Size = new System.Drawing.Size(1190, 412);
            upSides.TabIndex = 5;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.ugbCurrencyInfo, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.ugbDiscrepancyInfo, 0, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1190, 412);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // ugbCurrencyInfo
            // 
            this.ugbCurrencyInfo.Controls.Add(this.tbCurInfoSalesCostRate);
            this.ugbCurrencyInfo.Controls.Add(ulCurInfoSalesCostRate);
            this.ugbCurrencyInfo.Controls.Add(this.tbCurInfoRate);
            this.ugbCurrencyInfo.Controls.Add(ulCurInfoRate);
            this.ugbCurrencyInfo.Controls.Add(this.tbCurInfoSalesCostInRuble);
            this.ugbCurrencyInfo.Controls.Add(this.tbCurInfoSalesCostInCurrency);
            this.ugbCurrencyInfo.Controls.Add(this.tbCurInfoCodeCurrency);
            this.ugbCurrencyInfo.Controls.Add(ulCurInfoCodeCurrency);
            this.ugbCurrencyInfo.Controls.Add(ulCurInfoSalesCostInCurrency);
            this.ugbCurrencyInfo.Controls.Add(ulCurInfoSalesCostInRuble);
            this.ugbCurrencyInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugbCurrencyInfo.Location = new System.Drawing.Point(3, 315);
            this.ugbCurrencyInfo.Name = "ugbCurrencyInfo";
            this.ugbCurrencyInfo.Size = new System.Drawing.Size(1184, 94);
            this.ugbCurrencyInfo.TabIndex = 1;
            this.ugbCurrencyInfo.Text = "Сведения по валюте";
            // 
            // tbCurInfoSalesCostRate
            // 
            appearance36.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance36.TextHAlignAsString = "Left";
            appearance36.TextVAlignAsString = "Middle";
            this.tbCurInfoSalesCostRate.Appearance = appearance36;
            this.tbCurInfoSalesCostRate.Location = new System.Drawing.Point(834, 25);
            this.tbCurInfoSalesCostRate.Margin = new System.Windows.Forms.Padding(0);
            this.tbCurInfoSalesCostRate.Name = "tbCurInfoSalesCostRate";
            this.tbCurInfoSalesCostRate.ReadOnly = true;
            this.tbCurInfoSalesCostRate.Size = new System.Drawing.Size(185, 21);
            this.tbCurInfoSalesCostRate.TabIndex = 5;
            // 
            // tbCurInfoRate
            // 
            appearance37.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance37.TextHAlignAsString = "Left";
            appearance37.TextVAlignAsString = "Middle";
            this.tbCurInfoRate.Appearance = appearance37;
            this.tbCurInfoRate.Location = new System.Drawing.Point(472, 25);
            this.tbCurInfoRate.Margin = new System.Windows.Forms.Padding(0);
            this.tbCurInfoRate.Name = "tbCurInfoRate";
            this.tbCurInfoRate.ReadOnly = true;
            this.tbCurInfoRate.Size = new System.Drawing.Size(159, 21);
            this.tbCurInfoRate.TabIndex = 3;
            // 
            // tbCurInfoSalesCostInRuble
            // 
            appearance38.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Middle";
            this.tbCurInfoSalesCostInRuble.Appearance = appearance38;
            this.tbCurInfoSalesCostInRuble.Location = new System.Drawing.Point(204, 63);
            this.tbCurInfoSalesCostInRuble.Margin = new System.Windows.Forms.Padding(0);
            this.tbCurInfoSalesCostInRuble.Name = "tbCurInfoSalesCostInRuble";
            this.tbCurInfoSalesCostInRuble.ReadOnly = true;
            this.tbCurInfoSalesCostInRuble.Size = new System.Drawing.Size(185, 21);
            this.tbCurInfoSalesCostInRuble.TabIndex = 9;
            // 
            // tbCurInfoSalesCostInCurrency
            // 
            appearance39.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Middle";
            this.tbCurInfoSalesCostInCurrency.Appearance = appearance39;
            this.tbCurInfoSalesCostInCurrency.Location = new System.Drawing.Point(204, 44);
            this.tbCurInfoSalesCostInCurrency.Margin = new System.Windows.Forms.Padding(0);
            this.tbCurInfoSalesCostInCurrency.Name = "tbCurInfoSalesCostInCurrency";
            this.tbCurInfoSalesCostInCurrency.ReadOnly = true;
            this.tbCurInfoSalesCostInCurrency.Size = new System.Drawing.Size(185, 21);
            this.tbCurInfoSalesCostInCurrency.TabIndex = 7;
            // 
            // tbCurInfoCodeCurrency
            // 
            appearance40.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Middle";
            this.tbCurInfoCodeCurrency.Appearance = appearance40;
            this.tbCurInfoCodeCurrency.Location = new System.Drawing.Point(204, 25);
            this.tbCurInfoCodeCurrency.Margin = new System.Windows.Forms.Padding(0);
            this.tbCurInfoCodeCurrency.Name = "tbCurInfoCodeCurrency";
            this.tbCurInfoCodeCurrency.ReadOnly = true;
            this.tbCurInfoCodeCurrency.Size = new System.Drawing.Size(185, 21);
            this.tbCurInfoCodeCurrency.TabIndex = 1;
            // 
            // ugbDiscrepancyInfo
            // 
            this.ugbDiscrepancyInfo.Controls.Add(this.discrepancyGridControl);
            this.ugbDiscrepancyInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugbDiscrepancyInfo.Location = new System.Drawing.Point(3, 3);
            this.ugbDiscrepancyInfo.Name = "ugbDiscrepancyInfo";
            this.ugbDiscrepancyInfo.Size = new System.Drawing.Size(1184, 306);
            this.ugbDiscrepancyInfo.TabIndex = 2;
            this.ugbDiscrepancyInfo.Text = "Стороны отработки";
            // 
            // discrepancyGridControl
            // 
            this.discrepancyGridControl.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.discrepancyGridControl.DataSource = null;
            this.discrepancyGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.discrepancyGridControl.HeaderVisible = false;
            this.discrepancyGridControl.Location = new System.Drawing.Point(3, 16);
            this.discrepancyGridControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.discrepancyGridControl.Name = "discrepancyGridControl";
            this.discrepancyGridControl.RowDoubleClicked = null;
            this.discrepancyGridControl.Setup = null;
            this.discrepancyGridControl.Size = new System.Drawing.Size(1178, 287);
            this.discrepancyGridControl.TabIndex = 1;
            this.discrepancyGridControl.ViewMode = Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup.DiscrepancyGridViewMode.Normal;
            // 
            // labelCommentCaption
            // 
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            labelCommentCaption.Appearance = appearance11;
            labelCommentCaption.Dock = System.Windows.Forms.DockStyle.Top;
            labelCommentCaption.Location = new System.Drawing.Point(3, 24);
            labelCommentCaption.Name = "labelCommentCaption";
            labelCommentCaption.Size = new System.Drawing.Size(104, 20);
            labelCommentCaption.TabIndex = 4;
            labelCommentCaption.Text = "Комментарий:";
            labelCommentCaption.WrapText = false;
            // 
            // ulReorgINN
            // 
            appearance19.TextHAlignAsString = "Right";
            appearance19.TextVAlignAsString = "Middle";
            ulReorgINN.Appearance = appearance19;
            ulReorgINN.Dock = System.Windows.Forms.DockStyle.Fill;
            ulReorgINN.Location = new System.Drawing.Point(333, 3);
            ulReorgINN.Name = "ulReorgINN";
            ulReorgINN.Size = new System.Drawing.Size(258, 15);
            ulReorgINN.TabIndex = 2;
            ulReorgINN.Text = "ИНН реорганизованного лица:";
            ulReorgINN.WrapText = false;
            // 
            // upMain
            // 
            // 
            // upMain.ClientArea
            // 
            this.upMain.ClientArea.Controls.Add(upSides);
            this.upMain.ClientArea.Controls.Add(this.uegbCommonInfo);
            this.upMain.ClientArea.Controls.Add(this.upHeader);
            this.upMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upMain.Location = new System.Drawing.Point(0, 0);
            this.upMain.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.upMain.Name = "upMain";
            this.upMain.Size = new System.Drawing.Size(1190, 583);
            this.upMain.TabIndex = 1;
            // 
            // uegbCommonInfo
            // 
            this.uegbCommonInfo.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uegbCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.uegbCommonInfo.ExpandedSize = new System.Drawing.Size(1190, 108);
            this.uegbCommonInfo.Location = new System.Drawing.Point(0, 63);
            this.uegbCommonInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.uegbCommonInfo.Name = "uegbCommonInfo";
            this.uegbCommonInfo.Size = new System.Drawing.Size(1190, 108);
            this.uegbCommonInfo.TabIndex = 2;
            this.uegbCommonInfo.Text = "Общие сведения";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.tableLayoutCommonMain);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1184, 86);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // tableLayoutCommonMain
            // 
            this.tableLayoutCommonMain.ColumnCount = 2;
            this.tableLayoutCommonMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 480F));
            this.tableLayoutCommonMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommonMain.Controls.Add(this.tableLayoutCommonLeft, 0, 0);
            this.tableLayoutCommonMain.Controls.Add(this.tableLayoutCommonRight, 1, 0);
            this.tableLayoutCommonMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommonMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCommonMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommonMain.Name = "tableLayoutCommonMain";
            this.tableLayoutCommonMain.RowCount = 1;
            this.tableLayoutCommonMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommonMain.Size = new System.Drawing.Size(1184, 86);
            this.tableLayoutCommonMain.TabIndex = 27;
            // 
            // tableLayoutCommonLeft
            // 
            this.tableLayoutCommonLeft.ColumnCount = 3;
            this.tableLayoutCommonLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 193F));
            this.tableLayoutCommonLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270F));
            this.tableLayoutCommonLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutCommonLeft.Controls.Add(ulIssueDateCaption, 0, 0);
            this.tableLayoutCommonLeft.Controls.Add(ulAmountCaption, 0, 1);
            this.tableLayoutCommonLeft.Controls.Add(ulAmountPVPCaption, 0, 2);
            this.tableLayoutCommonLeft.Controls.Add(this.tbIssueDate, 1, 0);
            this.tableLayoutCommonLeft.Controls.Add(this.tbAmountPVP, 1, 2);
            this.tableLayoutCommonLeft.Controls.Add(this.tbAmount, 1, 1);
            this.tableLayoutCommonLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommonLeft.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCommonLeft.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommonLeft.Name = "tableLayoutCommonLeft";
            this.tableLayoutCommonLeft.RowCount = 4;
            this.tableLayoutCommonLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutCommonLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutCommonLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutCommonLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutCommonLeft.Size = new System.Drawing.Size(480, 86);
            this.tableLayoutCommonLeft.TabIndex = 0;
            // 
            // tbIssueDate
            // 
            appearance26.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Middle";
            this.tbIssueDate.Appearance = appearance26;
            this.tbIssueDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbIssueDate.Location = new System.Drawing.Point(193, 0);
            this.tbIssueDate.Margin = new System.Windows.Forms.Padding(0);
            this.tbIssueDate.Name = "tbIssueDate";
            this.tbIssueDate.ReadOnly = true;
            this.tbIssueDate.Size = new System.Drawing.Size(270, 21);
            this.tbIssueDate.TabIndex = 1;
            // 
            // tbAmountPVP
            // 
            appearance27.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Middle";
            this.tbAmountPVP.Appearance = appearance27;
            this.tbAmountPVP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAmountPVP.Location = new System.Drawing.Point(193, 42);
            this.tbAmountPVP.Margin = new System.Windows.Forms.Padding(0);
            this.tbAmountPVP.Name = "tbAmountPVP";
            this.tbAmountPVP.ReadOnly = true;
            this.tbAmountPVP.Size = new System.Drawing.Size(270, 21);
            this.tbAmountPVP.TabIndex = 5;
            // 
            // tbAmount
            // 
            appearance28.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance28.TextHAlignAsString = "Left";
            appearance28.TextVAlignAsString = "Middle";
            this.tbAmount.Appearance = appearance28;
            this.tbAmount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAmount.Location = new System.Drawing.Point(193, 21);
            this.tbAmount.Margin = new System.Windows.Forms.Padding(0);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.ReadOnly = true;
            this.tbAmount.Size = new System.Drawing.Size(270, 21);
            this.tbAmount.TabIndex = 3;
            // 
            // tableLayoutCommonRight
            // 
            this.tableLayoutCommonRight.ColumnCount = 4;
            this.tableLayoutCommonRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutCommonRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutCommonRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommonRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutCommonRight.Controls.Add(this.tbReorgINN, 3, 0);
            this.tableLayoutCommonRight.Controls.Add(ulReorgINN, 2, 0);
            this.tableLayoutCommonRight.Controls.Add(ulCloseDataCaption, 0, 0);
            this.tableLayoutCommonRight.Controls.Add(this.tbComment, 1, 1);
            this.tableLayoutCommonRight.Controls.Add(labelCommentCaption, 0, 1);
            this.tableLayoutCommonRight.Controls.Add(this.tbCloseDate, 1, 0);
            this.tableLayoutCommonRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommonRight.Location = new System.Drawing.Point(480, 0);
            this.tableLayoutCommonRight.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommonRight.Name = "tableLayoutCommonRight";
            this.tableLayoutCommonRight.RowCount = 2;
            this.tableLayoutCommonRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutCommonRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommonRight.Size = new System.Drawing.Size(704, 86);
            this.tableLayoutCommonRight.TabIndex = 0;
            // 
            // tbReorgINN
            // 
            appearance29.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance29.TextHAlignAsString = "Left";
            appearance29.TextVAlignAsString = "Middle";
            this.tbReorgINN.Appearance = appearance29;
            this.tbReorgINN.Location = new System.Drawing.Point(594, 0);
            this.tbReorgINN.Margin = new System.Windows.Forms.Padding(0);
            this.tbReorgINN.Name = "tbReorgINN";
            this.tbReorgINN.ReadOnly = true;
            this.tbReorgINN.Size = new System.Drawing.Size(110, 21);
            this.tbReorgINN.TabIndex = 3;
            // 
            // tbComment
            // 
            this.tbComment.AlwaysInEditMode = true;
            this.tableLayoutCommonRight.SetColumnSpan(this.tbComment, 3);
            this.tbComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbComment.Location = new System.Drawing.Point(111, 25);
            this.tbComment.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.tbComment.Multiline = true;
            this.tbComment.Name = "tbComment";
            this.tbComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbComment.Size = new System.Drawing.Size(592, 60);
            this.tbComment.TabIndex = 5;
            // 
            // tbCloseDate
            // 
            appearance17.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.tbCloseDate.Appearance = appearance17;
            this.tbCloseDate.Location = new System.Drawing.Point(110, 0);
            this.tbCloseDate.Margin = new System.Windows.Forms.Padding(0);
            this.tbCloseDate.Name = "tbCloseDate";
            this.tbCloseDate.ReadOnly = true;
            this.tbCloseDate.Size = new System.Drawing.Size(220, 21);
            this.tbCloseDate.TabIndex = 1;
            // 
            // upHeader
            // 
            // 
            // upHeader.ClientArea
            // 
            this.upHeader.ClientArea.Controls.Add(this.tableLayoutPanelHeader);
            this.upHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.upHeader.Location = new System.Drawing.Point(0, 0);
            this.upHeader.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.upHeader.Name = "upHeader";
            this.upHeader.Size = new System.Drawing.Size(1190, 63);
            this.upHeader.TabIndex = 1;
            // 
            // tableLayoutPanelHeader
            // 
            this.tableLayoutPanelHeader.ColumnCount = 5;
            this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelHeader.Controls.Add(ulDiscrepancyNumberCaption, 0, 0);
            this.tableLayoutPanelHeader.Controls.Add(this.ulDiscrepancyStatus, 1, 2);
            this.tableLayoutPanelHeader.Controls.Add(ulDiscrepancyStatusCaption, 0, 2);
            this.tableLayoutPanelHeader.Controls.Add(this.ulDiscrepancyNumber, 1, 0);
            this.tableLayoutPanelHeader.Controls.Add(ulDiscrepancyTypeCaption, 0, 1);
            this.tableLayoutPanelHeader.Controls.Add(this.ulDiscrepancyType, 1, 1);
            this.tableLayoutPanelHeader.Controls.Add(this.labelSideInfoCaption, 2, 0);
            this.tableLayoutPanelHeader.Controls.Add(this.labelStageCaption, 2, 1);
            this.tableLayoutPanelHeader.Controls.Add(this.tableLayoutSideInfo, 3, 0);
            this.tableLayoutPanelHeader.Controls.Add(this.labelStageValue, 3, 1);
            this.tableLayoutPanelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelHeader.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelHeader.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelHeader.Name = "tableLayoutPanelHeader";
            this.tableLayoutPanelHeader.RowCount = 4;
            this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanelHeader.Size = new System.Drawing.Size(1190, 63);
            this.tableLayoutPanelHeader.TabIndex = 6;
            // 
            // ulDiscrepancyStatus
            // 
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.ulDiscrepancyStatus.Appearance = appearance1;
            this.ulDiscrepancyStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulDiscrepancyStatus.Location = new System.Drawing.Point(151, 41);
            this.ulDiscrepancyStatus.Margin = new System.Windows.Forms.Padding(1);
            this.ulDiscrepancyStatus.Name = "ulDiscrepancyStatus";
            this.ulDiscrepancyStatus.Size = new System.Drawing.Size(228, 18);
            this.ulDiscrepancyStatus.TabIndex = 1;
            this.ulDiscrepancyStatus.Text = "В работе";
            // 
            // ulDiscrepancyNumber
            // 
            appearance3.FontData.BoldAsString = "True";
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.ulDiscrepancyNumber.Appearance = appearance3;
            this.ulDiscrepancyNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulDiscrepancyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ulDiscrepancyNumber.Location = new System.Drawing.Point(151, 1);
            this.ulDiscrepancyNumber.Margin = new System.Windows.Forms.Padding(1);
            this.ulDiscrepancyNumber.Name = "ulDiscrepancyNumber";
            this.ulDiscrepancyNumber.Size = new System.Drawing.Size(228, 18);
            this.ulDiscrepancyNumber.TabIndex = 2;
            this.ulDiscrepancyNumber.Text = "123";
            // 
            // ulDiscrepancyType
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ulDiscrepancyType.Appearance = appearance6;
            this.ulDiscrepancyType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulDiscrepancyType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ulDiscrepancyType.Location = new System.Drawing.Point(151, 21);
            this.ulDiscrepancyType.Margin = new System.Windows.Forms.Padding(1);
            this.ulDiscrepancyType.Name = "ulDiscrepancyType";
            this.ulDiscrepancyType.Size = new System.Drawing.Size(228, 18);
            this.ulDiscrepancyType.TabIndex = 7;
            this.ulDiscrepancyType.Text = "НДС";
            // 
            // labelSideInfoCaption
            // 
            appearance31.TextHAlignAsString = "Right";
            this.labelSideInfoCaption.Appearance = appearance31;
            this.labelSideInfoCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSideInfoCaption.Location = new System.Drawing.Point(381, 1);
            this.labelSideInfoCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSideInfoCaption.Name = "labelSideInfoCaption";
            this.labelSideInfoCaption.Size = new System.Drawing.Size(128, 18);
            this.labelSideInfoCaption.TabIndex = 4;
            this.labelSideInfoCaption.Text = "Сторона отработки:";
            // 
            // labelStageCaption
            // 
            appearance32.TextHAlignAsString = "Right";
            this.labelStageCaption.Appearance = appearance32;
            this.labelStageCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStageCaption.Location = new System.Drawing.Point(381, 21);
            this.labelStageCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStageCaption.Name = "labelStageCaption";
            this.labelStageCaption.Size = new System.Drawing.Size(128, 18);
            this.labelStageCaption.TabIndex = 8;
            // 
            // tableLayoutSideInfo
            // 
            this.tableLayoutSideInfo.ColumnCount = 2;
            this.tableLayoutSideInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutSideInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 375F));
            this.tableLayoutSideInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutSideInfo.Controls.Add(this.linkLabelSideInfoInnVaalue, 1, 0);
            this.tableLayoutSideInfo.Controls.Add(this._surIndicator, 0, 0);
            this.tableLayoutSideInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutSideInfo.Location = new System.Drawing.Point(510, 0);
            this.tableLayoutSideInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutSideInfo.Name = "tableLayoutSideInfo";
            this.tableLayoutSideInfo.RowCount = 1;
            this.tableLayoutSideInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutSideInfo.Size = new System.Drawing.Size(400, 20);
            this.tableLayoutSideInfo.TabIndex = 3;
            // 
            // linkLabelSideInfoInnVaalue
            // 
            appearance33.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.linkLabelSideInfoInnVaalue.Appearance = appearance33;
            this.linkLabelSideInfoInnVaalue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.linkLabelSideInfoInnVaalue.Location = new System.Drawing.Point(26, 1);
            this.linkLabelSideInfoInnVaalue.Margin = new System.Windows.Forms.Padding(1);
            this.linkLabelSideInfoInnVaalue.Name = "linkLabelSideInfoInnVaalue";
            this.linkLabelSideInfoInnVaalue.Size = new System.Drawing.Size(373, 18);
            this.linkLabelSideInfoInnVaalue.TabIndex = 1;
            this.linkLabelSideInfoInnVaalue.TabStop = true;
            this.linkLabelSideInfoInnVaalue.Value = "<a href=\"#\">1234567890</a>";
            this.linkLabelSideInfoInnVaalue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.linkLabelSideInfoInnVaalue_LinkClicked);
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(3, 0);
            this._surIndicator.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 0;
            // 
            // labelStageValue
            // 
            this.labelStageValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStageValue.Location = new System.Drawing.Point(511, 21);
            this.labelStageValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStageValue.Name = "labelStageValue";
            this.labelStageValue.Size = new System.Drawing.Size(398, 18);
            this.labelStageValue.TabIndex = 9;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.upMain);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1190, 583);
            this.OnFormClosing += new System.EventHandler(this.View_OnFormClosing);
            this.Load += new System.EventHandler(this.View_Load);
            upSides.ClientArea.ResumeLayout(false);
            upSides.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugbCurrencyInfo)).EndInit();
            this.ugbCurrencyInfo.ResumeLayout(false);
            this.ugbCurrencyInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostInRuble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoSalesCostInCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurInfoCodeCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbDiscrepancyInfo)).EndInit();
            this.ugbDiscrepancyInfo.ResumeLayout(false);
            this.upMain.ClientArea.ResumeLayout(false);
            this.upMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uegbCommonInfo)).EndInit();
            this.uegbCommonInfo.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.tableLayoutCommonMain.ResumeLayout(false);
            this.tableLayoutCommonLeft.ResumeLayout(false);
            this.tableLayoutCommonLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmountPVP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).EndInit();
            this.tableLayoutCommonRight.ResumeLayout(false);
            this.tableLayoutCommonRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbReorgINN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCloseDate)).EndInit();
            this.upHeader.ClientArea.ResumeLayout(false);
            this.upHeader.ResumeLayout(false);
            this.tableLayoutPanelHeader.ResumeLayout(false);
            this.tableLayoutSideInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upMain;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uegbCommonInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraPanel upHeader;
        private Infragistics.Win.Misc.UltraLabel ulDiscrepancyNumber;
        private Infragistics.Win.Misc.UltraLabel ulDiscrepancyStatus;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCloseDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbAmountPVP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbIssueDate;
        private Infragistics.Win.Misc.UltraLabel ulDiscrepancyType;
        private Infragistics.Win.Misc.UltraGroupBox ugbCurrencyInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCurInfoSalesCostInRuble;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCurInfoSalesCostInCurrency;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCurInfoCodeCurrency;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCurInfoRate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCurInfoSalesCostRate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelHeader;
        private Infragistics.Win.Misc.UltraLabel labelSideInfoCaption;
        private Infragistics.Win.Misc.UltraLabel labelStageCaption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSideInfo;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel linkLabelSideInfoInnVaalue;
        private Infragistics.Win.Misc.UltraLabel labelStageValue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbComment;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommonMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommonLeft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommonRight;
        private Controls.Sur.SurIcon _surIndicator;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbReorgINN;
        private Infragistics.Win.Misc.UltraGroupBox ugbDiscrepancyInfo;
        private Controls.Grid.V1.DiscrepancyGridControl discrepancyGridControl;
    }
}