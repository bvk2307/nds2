﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard
{
    /// <summary>
    ///     Класс реализует загрузку данных о СФ сторон расхождения
    /// </summary>
    class DiscrepancySidesDataLoader : ServiceProxyBase<List<DiscrepancySide>>, IClientGridDataProvider<List<DiscrepancySide>>
    {
        private readonly IDiscrepancyDataService _service;
        private readonly HiveRequestDiscrepancyCard _hiveRequestData;

        public DiscrepancySidesDataLoader(
            IDiscrepancyDataService service,
            INotifier notifier,
            IClientLogger logger, 
            HiveRequestDiscrepancyCard hiveRequestData 
            ) : base(notifier, logger)
        {
            _service = service;
            _hiveRequestData = hiveRequestData;
        }

        public event EventHandler<DataLoadedEventArgs<List<DiscrepancySide>>> DataLoaded;

        public void StartLoading()
        {
            BeginInvoke(
                () =>
                {
                    return _service.GetDiscrepancySides(_hiveRequestData);
                }
                );
        }

        protected override void CallBack(List<DiscrepancySide> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<DiscrepancySide>>(result));
        }
    }
}
