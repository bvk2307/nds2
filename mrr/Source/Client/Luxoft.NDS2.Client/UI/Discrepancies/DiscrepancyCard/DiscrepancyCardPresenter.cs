﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using Infragistics.Win.UltraWinGrid;
using System.Linq.Expressions;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Bargain;


namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard
{
    public sealed class DiscrepancyCardPresenter : BasePresenter<View>
    {
        private readonly IDiscrepancyDataService _dataService;
        private readonly IUcVisualStateService _visualStateService;
        private readonly IUcResourceManagersService _resourceManagersService;

        private DiscrepancySummaryInfo _discrepancy;
        private long _discrepancyId;

        private AsyncWorker<bool> _tableLoader = new AsyncWorker<bool>();
        private AsyncWorker<bool> _discrepancyInvoiceLoader = new AsyncWorker<bool>();
        private AsyncWorker<bool> _mainDataWorker = new AsyncWorker<bool>();

        public IUcResourceManagersService ResourceManagersService
        {
            get { return _resourceManagersService; }
        }

        public DiscrepancySummaryInfo Discrepancy
        {
            get { return _discrepancy; }
        }

        public IUcVisualStateService VisualStateService
        {
            get { return _visualStateService; }
        }

        private readonly IDeclarationCardOpener _declarationCardOpener;
        private readonly DiscrepancySidesDataLoader _discrepancySidesDataLoader;
        private readonly HiveRequestDiscrepancyCard _hiveRequest;


        public DiscrepancyCardPresenter(View v, PresentationContext pc, WorkItem wi, long id)
            : base(pc, wi, v)
        {
            _dataService = base.GetServiceProxy<IDiscrepancyDataService>();
            _visualStateService = wi.Services.Get<IUcVisualStateService>();
            
            _resourceManagersService = wi.Services.Get<IUcResourceManagersService>();
            _declarationCardOpener = GetDeclarationCardOpener();
            _discrepancyId = id;
            
            _hiveRequest = new HiveRequestDiscrepancyCard(_discrepancyId);
            _discrepancySidesDataLoader = new DiscrepancySidesDataLoader(_dataService, Notifier, ClientLogger, _hiveRequest);

            _discrepancySidesDataLoader.DataLoaded += OnDiscrepancySidesLoaded;
        }

        public DiscrepancyCardPresenter(View v, PresentationContext pc, WorkItem wi, DiscrepancySummaryInfo discrepancy)
            : base(pc, wi, v)
        {
            _dataService = base.GetServiceProxy<IDiscrepancyDataService>();
            _visualStateService = wi.Services.Get<IUcVisualStateService>();
            _resourceManagersService = wi.Services.Get<IUcResourceManagersService>();
            _declarationCardOpener = GetDeclarationCardOpener();

            _discrepancy = discrepancy;
            _discrepancyId = _discrepancy.Id;

            _hiveRequest = new HiveRequestDiscrepancyCard(_discrepancy.Id, _discrepancy.InvoiceId, _discrepancy.ContractorInvoiceId);
            _discrepancySidesDataLoader = new DiscrepancySidesDataLoader(_dataService, Notifier, ClientLogger, _hiveRequest);

            _discrepancySidesDataLoader.DataLoaded += OnDiscrepancySidesLoaded;
        }

        #region Загрузка данных

        public void InitLoadData()
        {
            LoadMainDataAsync();
            //LoadGridData();
        }

        private void LoadMainDataAsync()
        {
            _mainDataWorker.DoWork += (sender, args) =>
            {
                if (_discrepancy == null)
                {
                    FillDiscrepancy(_discrepancyId);
                    FillHiveRequest();
                }
                    
            };
            _mainDataWorker.Complete += (sender, args) =>
            {
                View.ExecuteInUiThread(c =>
                {
                    View.LoadMainData();
                    View.ShowSideLoadingProgress();
                    _discrepancySidesDataLoader.StartLoading();
                });

            };
            _mainDataWorker.Failed += (sender, args) =>
            {
                View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };

            _mainDataWorker.Start();
        }
        private void OnDiscrepancySidesLoaded(object sender, DataLoadedEventArgs<List<DiscrepancySide>> args)
        {
            if (args.Data != null)
            {
                _discrepancy.Sides = args.Data;
                View.ExecuteInUiThread((v) => { SetOneSideByDiscrepancyType(); });
                LoadDiscrepancyInvoiceData();
                View.ExecuteInUiThread(c =>
                {
                    c.DiscrepancyGridControl.DataSource = _discrepancy.Sides;
                    LoadGridData();
                    SetErrorsColor();
                    CorrectLinks(View);
                });
            }
            View.HideSideLoadingProgress();
            
        }

        public void LoadGridData()
        {
            View.DiscrepancyGridControl.Setup = GetDiscrepancyGridSetup(_discrepancy);
        }
        
        private void SetErrorsColor()
        {
            foreach (var side in _discrepancy.Sides)
            {
                foreach (var bb in side.BuyBooks)
                {
                    SetCellBackClolor(bb);
                    bb.BuyBookDetails.ForEach(d => SetCellBackClolor(d));
                }
                foreach (var sb in side.SellBooks)
                {
                    SetCellBackClolor(sb);
                    sb.SellBookDetails.ForEach(d => SetCellBackClolor(d));
                }
                foreach (var sj in side.SentInvoiceJournals)
                {
                    SetCellBackClolor(sj);
                    sj.SentInvoiceJournalDetails.ForEach(d => SetCellBackClolor(d));
                }
                foreach (var rj in side.RecievedInvoiceJournals)
                {
                    SetCellBackClolor(rj);
                    rj.RecievedInvoiceJournalDetails.ForEach(d => SetCellBackClolor(d));
                }
            }
            SetSideErrorsColor();
        }

        private void CorrectLinks(DiscrepancyCard.View view)
        {
            if (_discrepancy.Sides == null || _discrepancy.Sides.Count == 0)
            {
                return;
            }
            view.DiscrepancyGridControl.RemoveEmptyLinks();

            var helper = new GridSetupHelper<DiscrepancySide>();
            view.DiscrepancyGridControl.RemoveHyperlink(_discrepancy.Sides[0], helper.GetMemberName(side => side.Side2INN));
            if (_discrepancy.Sides.Count > 1)
            {
                view.DiscrepancyGridControl.RemoveHyperlink(_discrepancy.Sides[1], helper.GetMemberName(side => side.Side1INN));
            }
        }

        #endregion 

        private void FillDiscrepancy(long id)
        {
            _discrepancy = GetDiscrepancySummaryInfo(id);
        }

        private DiscrepancySummaryInfo GetDiscrepancySummaryInfo(long id)
        {
            DiscrepancySummaryInfo d = null;
            if (base.ExecuteServiceCall(() => _dataService.GetSummaryInfo(id),
                res => { d = res.Result; }))
            {}
            return d; 
        }

        private void FillHiveRequest()
        {
            _hiveRequest.InvoiceRowKey = _discrepancy.InvoiceId;
            _hiveRequest.ContractorInvoiceRowKey = _discrepancy.ContractorInvoiceId;
        }

        private void LoadDiscrepancyInvoiceData()
        {
            if (_discrepancy.Type != DiscrepancyType.Currency)
            {
                View.LoadCurrencyInfo();
            }
        }

        private void SetOneSideByDiscrepancyType()
        {
            if (_discrepancy.Type == DiscrepancyType.Currency ||
                _discrepancy.Type == DiscrepancyType.Break)
            {
                if (_discrepancy.Sides.Count() > 1)
                {
                    _discrepancy.Sides = _discrepancy.Sides.Where(p => p.InvoiceId == _discrepancy.InvoiceId).ToList();
                }
            }
        }

        public void OpenDiscrepancyCard(long id)
        {
            base.ViewDiscrepancyDetails(id);
        }

        public void OpenDeclarationCard(long id)
        {
            var result = _declarationCardOpener.Open(id);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public void OpenDeclarationCard(string invoiceId)
        {
            var result = _declarationCardOpener.OpenActualDeclaration(invoiceId);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        #region GridSetups


        public void OpenDisrepancyDocumentInfo(int stageId)
        {
            DiscrepancyDocumentInfo docInfo = GetDiscrepancyDocumentInfo(_discrepancyId, stageId);
            if (docInfo != null)
            {
                if (docInfo.DocType.EntryId == DocType.ClaimKS || docInfo.DocType.EntryId == DocType.ClaimSF)
                {
                    ViewClaimDetails(docInfo);
                }
                else if (docInfo.DocType.EntryId == DocType.Reclaim93 || docInfo.DocType.EntryId == DocType.Reclaim93_1)
                {
                    ViewReclaimDetails(docInfo);
                }
            }
        }

        # region CellToolTip

        DictionaryOper dicOper = new DictionaryOper();
        DictionaryCurr dicCurr = new DictionaryCurr();
        DictionaryBargain dicBargain = new DictionaryBargain();

        void InitDict()
        {
            dicOper = this.Oper;
            dicCurr = this.Curr;
            dicBargain = this.Barg;
        }
        private string InvoiceConvert(object inv)
        {
            DetailsBase temp = (DetailsBase)inv;
            string opcode = "";
            opcode = temp.OperationCode;
            return dicOper.NAME(opcode);
        }

        private string InvoiceCurrencyConvert(object inv)
        {
            DetailsBase temp = (DetailsBase)inv;
            string opcode = "";
            opcode = temp.CurrencyCode;
            return dicCurr.NAME(opcode);
        }

        private string InvoiceBargainConvert(object inv)
        {
            DetailsRecievedInvoiceJournal temp = (DetailsRecievedInvoiceJournal)inv;
            string opcode = "";
            opcode = ((int)temp.DealTypeCode).ToString();
            return dicBargain.NAME(opcode);
        }

        private Dictionary<string, Func<object, string>> GetCellToolTipsBase()
        {
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();

            InitDict();
            Func<object, string> funcOper = InvoiceConvert;
            Func<object, string> funcCurr = InvoiceCurrencyConvert;
            Func<object, string> funcBarg = InvoiceBargainConvert;
            cellToolTips.Add("OperationCode", funcOper);
            cellToolTips.Add("CurrencyCode", funcCurr);
            cellToolTips.Add("DealTypeCode", funcBarg);

            return cellToolTips;
        }

        #endregion

        private DiscrepancyGridSetup GetDiscrepancyGridSetup(DiscrepancySummaryInfo discrepancy)
        {
            var result = new DiscrepancyGridSetup();
            var band0 = result.Band;
            var band1 = new BandDefinition();
            var band2 = new BandDefinition();
            var band3 = new BandDefinition();
            var band4 = new BandDefinition();
            band0.Bands.AddRange(new[] { band1, band2, band3, band4 });

            var band11 = new BandDefinition();
            band1.Bands.Add(band11);
            var band21 = new BandDefinition();
            band2.Bands.Add(band21);
            var band31 = new BandDefinition();
            band3.Bands.Add(band31);
            var band41 = new BandDefinition();
            band4.Bands.Add(band41);

            if (discrepancy.Type == DiscrepancyType.Currency)
            {
                GridSetupHelper.SetupBandDiscrepancySideCurrency(band0);
            }
            else if (discrepancy.Type == DiscrepancyType.Break)
            {
                GridSetupHelper.SetupBandDiscrepancySideBreak(band0);
            }
            else
            {
                GridSetupHelper.SetupBandDiscrepancySide(band0, discrepancy);
            }



            GridSetupHelper.SetupBandAggregateBuyBook(band1, discrepancy.Sides.Any(s=>s.chapter == 8 && s.BuyBooks.Any() && s.BuyBooks.First().BuyBookDetails.Any()));
            GridSetupHelper.SetupBandAggregateSellBook(band2, discrepancy.Sides.Any(s => s.chapter == 9 && s.SellBooks.Any() && s.SellBooks.First().SellBookDetails.Any()));
            GridSetupHelper.SetupBandAggregateSentInvoiceJournal(band3, discrepancy.Sides.Any(s => s.chapter == 10 && s.SentInvoiceJournals.Any() && s.SentInvoiceJournals.First().SentInvoiceJournalDetails.Any()));
            GridSetupHelper.SetupBandAggregateRecievedInvoiceJournal(band4, discrepancy.Sides.Any(s => s.chapter == 11 && s.RecievedInvoiceJournals.Any() && s.RecievedInvoiceJournals.First().RecievedInvoiceJournalDetails.Any()));

            GridSetupHelper.SetupBandDetailsBuyBook(band11);
            GridSetupHelper.SetupBandDetailsSellBook(band21);
            GridSetupHelper.SetupBandDetailsSentInvoiceJournal(band31);
            GridSetupHelper.SetupBandDetailsRecievedInvoiceJournal(band41);
            result.CellToolTips = GetCellToolTipsBase();
            return result;
        }

        #endregion

        private bool Compare<T>(Nullable<T> v1, Nullable<T> v2) where T : struct
        {
            return v1.HasValue && v2.HasValue ? v1.Value.Equals(v2.Value) : v1.HasValue == v2.HasValue;
        }

        private bool Compare<T>(T v1, T v2) where T : class
        {
            return !ReferenceEquals(v1, null) && !ReferenceEquals(v2, null) ? v1.Equals(v2) : ReferenceEquals(v1, null) == ReferenceEquals(v2, null);
        }

        #region Покраска атрибутов Расхождения

        private void SetSideErrorsColor()
        {
            if (_discrepancy.Sides.Count != 2)
            {
                return;
            }

            var side1 = _discrepancy.Sides[0];
            var side2 = _discrepancy.Sides[1];
            if (side1 == null || side2 == null)
            {
                return;
            }

            var side1Errors = new List<string>();
            var side2Errors = new List<string>();
            if (!Compare(side1.Side1INN, side2.Side1INN))
            {
                side1Errors.Add("Side1INN");
                side2Errors.Add("Side1INN");
            }

            if (!Compare(side1.Side2INN, side2.Side2INN))
            {
                side1Errors.Add("Side2INN");
                side2Errors.Add("Side2INN");
            }
            if (!Compare(side1.SideInfoInvoiceNumber, side2.SideInfoInvoiceNumber))
            {
                side1Errors.Add("SideInfoInvoiceNumber");
                side2Errors.Add("SideInfoInvoiceNumber");
            }
            if (!Compare(side1.SideInfoInvoiceDate, side2.SideInfoInvoiceDate))
            {
                side1Errors.Add("SideInfoInvoiceDate");
                side2Errors.Add("SideInfoInvoiceDate");
            }
            if (!Compare(side1.SideInfoInvoiceAmount, side2.SideInfoInvoiceAmount))
            {
                side1Errors.Add("SideInfoInvoiceAmount");
                side2Errors.Add("SideInfoInvoiceAmount");
            }
            if (!Compare(side1.SideInfoTaxAmount, side2.SideInfoTaxAmount))
            {
                side1Errors.Add("SideInfoTaxAmount");
                side2Errors.Add("SideInfoTaxAmount");
            }

            View.DiscrepancyGridControl.SetCellBackColor(side1, side1Errors, Color.LightPink);
            View.DiscrepancyGridControl.SetCellBackColor(side2, side2Errors, Color.LightPink);
        }

        #endregion Покраска атрибутов Расхождения

        #region Покраска атрибутов СФ

        private void SetCellBackClolor(DetailsBase instance)
        {
            instance.ErrorCodes.ForEach(code => SetCellBackClolor(instance, instance.Chapter, code));
        }

        #endregion Покраска атрибутов СФ

        private void SetCellBackClolor(object instance, int chapter, int code)
        {
            var errors =
                from mapping in _discrepancy.ErrorsDictionary
                where mapping.Chapter == chapter
                   && mapping.ErrorCode == code
                from member in mapping.DataMembersArray
                select new { ErrorCode = mapping.ErrorCode, DataMember = member, Description = mapping.Description };

            var properties =
                from property in instance.GetType().GetProperties()
                select property.Name;

            foreach (var error in errors)
            {
                if (properties.Contains(error.DataMember))
                {
                    string toolTip = string.Format("{0} - {1}", error.ErrorCode, error.Description);
                    View.ExecuteInUiThread(c => c.DiscrepancyGridControl.SetCellBackColor(instance, error.DataMember, Color.LightGoldenrodYellow, toolTip));
                }
            }
        }

        public bool SaveComment(DetailsBase details)
        {
            var opResult = _dataService.SaveComment(_discrepancy.Id, details);
            var result = opResult.Status == ResultStatus.Success;
            var operation = new LogObject
            {
                IssueDate = DateTime.Now,
                ObjectId = _discrepancy.Id,
                Operation = LogOperation.DiscrepancyAddCorrection,
                UserName = UserName
            };
            SecurityLogger.Write(operation, result);
            if (!result)
            {
                View.ExecuteInUiThread(view => view.ShowError(opResult.Message));
            }
            return result;
        }

        private bool IsCanSaveUserComment(string userComment)
        {
            bool isCanSave = false;
            if (_discrepancy != null)
            {
                isCanSave = true;
                if (string.IsNullOrEmpty(_discrepancy.UserComment) && string.IsNullOrEmpty(userComment))
                {
                    isCanSave = false;
                }
                else if (_discrepancy.UserComment != userComment)
                {
                    isCanSave = true;
                }
                else
                {
                    isCanSave = false;
                }
            }
            return isCanSave;
        }

        public void SaveDiscrepancyUserComment(string userComment)
        {
            if (IsCanSaveUserComment(userComment))
            {
                OperationResult operRes = null;
                if (ExecuteServiceCall(
                        () => _dataService.SaveDiscrepancyUserComment(_discrepancyId, userComment),
                        (result) => operRes = result))
                { }
            }
        }

        public DiscrepancyDocumentInfo GetDiscrepancyDocumentInfo(long discrepancyId, int stageId)
        {
            DiscrepancyDocumentInfo ddi = null;
            if (ExecuteServiceCall(() => _dataService.GetDiscrepancyDocumentInfo(discrepancyId, stageId),
                res =>
                {
                    ddi = res.Result;
                }))
            { }
            return ddi;
        }
    }
}