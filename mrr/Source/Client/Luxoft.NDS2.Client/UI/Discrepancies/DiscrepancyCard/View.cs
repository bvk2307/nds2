﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Luxoft.NDS2.Client.UI.Discrepancies.Dialog;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;

namespace Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard
{
    public partial class View : BaseView
    {
        private readonly DiscrepancyCardPresenter _presenter;
        private ProgressIndicator _gridInvoicesProgress;
        private object _activeObject;

        private UcRibbonButtonToolContext _urbtcShowErrorsLK;
        private System.Windows.Forms.ToolTip toolTipSur = new System.Windows.Forms.ToolTip();
        private string formatCurrency = "{0:N2}";

        public void ShowSideLoadingProgress()
        {
            _gridInvoicesProgress.ShowProgress();
        }

        public void HideSideLoadingProgress()
        {
            _gridInvoicesProgress.HideProgress();
        }

        public DiscrepancyGridControl DiscrepancyGridControl
        {
            get { return discrepancyGridControl; }
        }

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx, WorkItem wi, long entityId)
            : base(ctx, wi)
        {
            InitializeComponent();
            _presenter = new DiscrepancyCardPresenter(this, ctx, wi, entityId);
            _presentationContext = ctx;
            _gridInvoicesProgress = new ProgressIndicator(ugbDiscrepancyInfo, discrepancyGridControl);
            
            discrepancyGridControl.CellClick += DiscrepancyGridControl_CellClick;
        }

        public View(PresentationContext ctx, WorkItem wi, DiscrepancySummaryInfo discrepancy)
            : base(ctx, wi)
        {
            InitializeComponent();
            _presenter = new DiscrepancyCardPresenter(this, ctx, wi, discrepancy);
            _presentationContext = ctx;
            _gridInvoicesProgress = new ProgressIndicator(ugbDiscrepancyInfo, discrepancyGridControl);

            discrepancyGridControl.CellClick += DiscrepancyGridControl_CellClick;
        }

        private void View_Load(object sender, EventArgs e)
        {
            SetHeaderDataEmpty();
            SetCommonDataEmpty();
            SetCurrencyInfoEmpty();

            _presenter.InitLoadData();
        }

        private void DiscrepancyGridControl_CellClick(object sender, ClickedCellEventArgs e)
        {
            var side = e.Instance as DiscrepancySide;
            if (side == null)
            {
                return;
            }

            if (e.DataMember == side.GetPropertyName(s => s.Side1INN))
            {
                _presenter.ViewTaxPayerByKppOriginal(side.Side1INN, side.Side1KPP);
            }
            else if (e.DataMember == side.GetPropertyName(s => s.Side2INN))
            {
                _presenter.ViewTaxPayerByKppOriginal(side.Side2INN, side.Side2KPP);
            }
            else if (e.DataMember == side.GetPropertyName(s => s.SideInfoInvoiceNumber))
            {
                _presenter.OpenDeclarationCard(side.InvoiceId);
            }
        }

        public void LoadMainData()
        {
            DiscrepancySummaryInfo discrepancy = _presenter.Discrepancy;
            LoadCaptionData(discrepancy);
            LoadHeaderData(discrepancy);
            LoadCommonData(discrepancy);
        }

        private void SetHeaderDataEmpty()
        {
            ulDiscrepancyNumber.Text = String.Empty;
            ulDiscrepancyStatus.Text = String.Empty;
            ulDiscrepancyType.Text = String.Empty;
            linkLabelSideInfoInnVaalue.Value = String.Empty;
        }

        private void LoadCaptionData(DiscrepancySummaryInfo discrepancy)
        {
            string title;
            if (discrepancy.Type != DiscrepancyType.Currency && discrepancy.OtherSideINN != null)
            {
                title = string.Format("{0}-{1} ({2})", discrepancy.WorkSideINN, discrepancy.OtherSideINN, discrepancy.TypeNameForCaption);
            }
            else
            {
                title = string.Format("{0} ({1})", discrepancy.WorkSideINN, discrepancy.TypeNameForCaption);                
            }

            _presentationContext.WindowTitle = title;
            _presentationContext.WindowDescription = title;
        }

        private void LoadHeaderData(DiscrepancySummaryInfo discrepancy)
        {
            ulDiscrepancyNumber.Text = discrepancy.Id.ToString();
            ulDiscrepancyStatus.Text = discrepancy.StatusName;
            ulDiscrepancyType.Text = discrepancy.TypeName;
            LoadSURInfo(discrepancy);

            linkLabelSideInfoInnVaalue.Value = string.Concat(discrepancy.WorkSideINN, "/", discrepancy.WorkSideKpp, " (покупатель)");
        }

        private void SetCommonDataEmpty()
        {
            tbIssueDate.Text = String.Empty;
            tbAmountPVP.Text = String.Empty;
            tbAmount.Text = String.Empty;
            tbCloseDate.Text = String.Empty;
            tbComment.Text = String.Empty;
            tbComment.Text = String.Empty;
        }

        private void LoadCommonData(DiscrepancySummaryInfo discrepancy)
        {
            tbIssueDate.Text = discrepancy.IssueDate.HasValue ? discrepancy.IssueDate.Value.ToString("dd.MM.yyyy") : string.Empty;
            tbAmountPVP.Text = string.Format(formatCurrency, discrepancy.AmountPVP);
            tbAmount.Text = string.Format(formatCurrency, discrepancy.Amount);
            tbCloseDate.Text = discrepancy.CloseDate.HasValue ? discrepancy.CloseDate.Value.ToString("dd.MM.yyyy") : string.Empty;
            tbReorgINN.Text = discrepancy.WorkSideReorgINN;
            tbComment.Text = discrepancy.UserComment;
        }

        private void LoadSURInfo(DiscrepancySummaryInfo discrepancy)
        {
            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = discrepancy.WorkSideCodeSUR;
        }

        private void SetCurrencyInfoEmpty()
        {
            TableLayoutRowStyleCollection rows = tableLayoutPanelMain.RowStyles;
            rows[1].Height = 0;
        }

        public void LoadCurrencyInfo()
        {
            DiscrepancySummaryInfo discrepancy = _presenter.Discrepancy;
            TableLayoutRowStyleCollection rows = tableLayoutPanelMain.RowStyles;
            if (discrepancy.Type == DiscrepancyType.Currency)
            {
                rows[1].Height = 100;
                if (discrepancy.DiscrepancyInvoice != null)
                {
                    tbCurInfoCodeCurrency.Text = discrepancy.DiscrepancyInvoice.CodeCurrency;
                    tbCurInfoSalesCostInCurrency.Text = string.Format(formatCurrency, discrepancy.DiscrepancyInvoice.SalesCostInCurrency);
                    tbCurInfoSalesCostInRuble.Text = string.Format(formatCurrency, discrepancy.DiscrepancyInvoice.SalesCostInRuble);
                }
                tbCurInfoRate.Text = string.Format(formatCurrency, discrepancy.Course);
                tbCurInfoSalesCostRate.Text = string.Format(formatCurrency, discrepancy.CourseCost); 
            }
            else
            {
                rows[1].Height = 0;
            }
        }

        private void LinkedDiscreptions_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.OpenDiscrepancyCard(int.Parse(e.LinkText));
            e.AddToVisitedLinks = false;
        }

        public override void OnLoad()
        {
            base.OnLoad();
            InitializeRibonMenu();
            discrepancyGridControl.SelectRow += DiscrepancyGridControl_SelectRow;
            discrepancyGridControl.CellClick += (sender, e) =>
            {
                var type = e.Instance.GetType();
                var data = type.GetProperty(e.DataMember).GetValue(e.Instance, null);
                _presenter.LogWarning(string.Format("{0}.{1} = '{2}'", type.Name, e.DataMember, data), "CellClick");
            };
        }

        #region Ribbon
        #region Creation
        private const string COMMAND_NAME_SHOW_ERRORS = "CmdShowErrorsLK";
        private const string COMMAND_NAME_ADD_COMMENTS = "CmdAddComments";
        private void InitializeRibonMenu()
        {
            EnsureResourceManagerCreated(ResourceManagerNDS2.NDS2ClientResources);
            var tab = new UcRibbonTabContext(_presentationContext, "DiscrepancyManagementTab")
            {
                Text = "Управление",
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };
            _presentationContext.UiVisualizationCollection.Add(tab);

            var group = tab.AddGroup("DiscrepancyManagementGroup");
            group.Text = "Функции расхождения";
            group.Visible = true;
            group.PreferredToolSize = UcRibbonToolSize.Large;
            _presentationContext.UiVisualizationCollection.Add(group);

            _urbtcShowErrorsLK = AddRibbonButton(group, _presentationContext, "urbtcShowErrorsLK", "Ошибки ЛК", "Показать ошибки ЛК", COMMAND_NAME_SHOW_ERRORS, "alert_32");
            _presentationContext.ActiveMenuTab = tab.ItemName;
            RefreshRibbon();
        }

        private void EnsureResourceManagerCreated(string resourcesName)
        {
            if (!_presenter.ResourceManagersService.Contains(resourcesName))
            {
                _presenter.ResourceManagersService.Add(resourcesName, Properties.Resources.ResourceManager);
            }
        }

        public void RefreshRibbon()
        {
            _presenter.VisualStateService.DoRefreshVisualEnvironment(_presentationContext.Id);
        }

        private UcRibbonButtonToolContext AddRibbonButton(UcRibbonGroupContext group, PresentationContext context, string name, string text, string toolTip, string command, string resource)
        {
            var button = new UcRibbonButtonToolContext(context, name, command)
            {
                Enabled = false,
                Visible = true,
                Text = text,
                ToolTipText = toolTip,
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = resource,
                SmallImageName = resource
            };

            group.ToolList.Add(new UcRibbonToolInstanceSettings(button.ItemName, UcRibbonToolSize.Large));
            context.UiVisualizationCollection.Add(button);
            return button;
        }
        #endregion
        #region Handlers
        [CommandHandler(COMMAND_NAME_SHOW_ERRORS)]
        public void CmdShowErrorsLK(object sender, EventArgs e)
        {
            var source = (IAggreagate)_activeObject;
            var codes = source.ToDetails().ErrorCodes;
            var errors = _presenter.Discrepancy.ErrorsDictionary.Where(error => codes.Contains(error.ErrorCode)).ToList();
            using (var dialog = new LogicalErrors(source, errors))
            {
                dialog.ShowDialog();
            }
        }

        #endregion
        #region Behavior
        void DiscrepancyGridControl_SelectRow(object sender, SelectedRowEventArgs e)
        {
            _activeObject = e.DataObject;
            if (e.DataObject == null)
            {
                _urbtcShowErrorsLK.Enabled = false;
            }
            else
            {
                _urbtcShowErrorsLK.Enabled = e.DataObject is IAggreagate;
                var firstSide = ((List<DiscrepancySide>)discrepancyGridControl.DataSource).FirstOrDefault();
                if (firstSide != null)
                {
                    IEnumerable<IAggreagate> seq1 = firstSide.RecievedInvoiceJournals ?? new List<AggregateRecievedInvoiceJournal>();
                    IEnumerable<IAggreagate> seq2 = firstSide.SentInvoiceJournals ?? new List<AggregateSentInvoiceJournal>();
                    IEnumerable<IAggreagate> seq3 = firstSide.BuyBooks ?? new List<AggregateBuyBook>();
                    IEnumerable<IAggreagate> seq4 = firstSide.SellBooks ?? new List<AggregateSellBook>();

                    var sequence = seq1.Union(seq2).Union(seq3).Union(seq4).Where(item => !item.Commented);
                }
            }
            RefreshRibbon();
        }
        #endregion

        #endregion

        private void View_OnFormClosing(object sender, EventArgs e)
        {
            try
            {
                _presenter.SaveDiscrepancyUserComment(tbComment.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка сохранения комментария пользователя");
                _presenter.LogError(string.Format("Ошибка сохранения комментария пользователя: {0}", ex));
            }
        }

        private void linkLabelSideInfoInnVaalue_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.ViewTaxPayerByKppOriginal(_presenter.Discrepancy.WorkSideINN, _presenter.Discrepancy.WorkSideKpp);
        }


    }
};