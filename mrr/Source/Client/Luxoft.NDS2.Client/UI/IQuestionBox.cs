﻿namespace Luxoft.NDS2.Client.UI
{
    public interface IQuestionBox
    {
        bool Ask(string caption, string message);
    }
}
