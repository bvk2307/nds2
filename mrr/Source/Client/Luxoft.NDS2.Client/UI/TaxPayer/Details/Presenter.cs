﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.Execution;
using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Threading;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public interface IBankAccountServiceProvider
    {
        PageResult<BankAccountBrief> BankAccountsForPeriod(QueryConditions criteria, string inn, string kpp, DateTime periodBegin, DateTime periodEnd);

        bool FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear);

        List<string> GetRequestedAccounts(string bik, DateTime begin, DateTime end);

        bool PushRequest(BankAccountManualRequest request);

    }

    public sealed class Presenter : BasePresenter<View>, IDeclarationColumnVisibility, IBankAccountServiceProvider
    {
        private readonly IDeclarationsDataService _service;

        private readonly TaxPayerModel _model;

        private readonly ITaxPayerDetailView _view;

        private readonly TaxPayerPresentationModel _presentationModel = new TaxPayerPresentationModel();

        private readonly IDeclarationCardOpener _declarationCardOpener;

        private readonly CancellationTokenSource _ctsLoadData = null;

        private readonly DateTime _firstAllowedDate = new DateTime(2015, 1, 1);
        
        private readonly AccessContext _accessContext;

        //.ctor
        public Presenter(PresentationContext presentationContext, WorkItem wi, View view, TaxPayerModel model)
            : base(presentationContext, wi, view)
        {
            _model = model;
            _view = view;

            _accessContext = ClientSecurityService.Create(wi).GetAccessContext(
                new string[]
                {
                    MrrOperations.BankAccountRequestCreate,
                    MrrOperations.TaxPayerDiscrepanciesAll,
                    MrrOperations.TaxPayerDiscrepanciesKnp
                }
                ); 

            InitGeneralData();
            _view.InitRibbon();

            _service = GetServiceProxy<IDeclarationsDataService>();
            _declarationCardOpener = GetDeclarationCardOpener(model.Chain);

            FillStatusArea();

            PresentationContext.WindowTitle = model.Title;
            PresentationContext.WindowDescription = model.Title;

            _view.Stop += StopWorkers;
            _view.FocusedDeclarationChanged +=
                (sender, arg) => SetRequestBankAccountsAvailability(arg.Model);
            _view.DeclarationRequested += d => ViewDeclarationDetails(d.GetZip());
            _view.SuccessorRequested += () => ViewTaxPayerByKppEffective(_model.InnSuccessor, _model.KppEffectiveSuccessor);
            _view.RefreshRequested += OnRefreshRequested;
            _view.BankAccountsRequested += ViewBankAccountsRequestDialog;
            _view.BankOperationsViewRequested += OnBankOperationsViewRequested;

            _view.InitDeclarationsGrid(this, _model.Sur, _model.AnnulmentReason);
            InitDeclarationGridPresenter();

            _view.InitBanksAccountsGrid(_presentationModel.BankAccountsPager);
            InitAccountGridPresenter();

            _view.InitRequestsGrid(_presentationModel.RequestPager);
            InitRequestsPresenter();

            RefreshEKP();
        }

        private void SetRequestBankAccountsAvailability(DeclarationModel model)
        {
            var periodBegin = model != null
                ? PeriodHelper.GetBeginDate(model.PeriodCode, model.FiscalYear)
                : DateTime.Now;

            _view.IsRequestBankAccountsEnabled =
                _view.IsDeclarationListFocused &&
                model != null &&
                model.IsActive &&
                model.IsDeclaration() &&
                model.IsKnpOpened &&
                periodBegin <= DateTime.Today &&
                periodBegin >= _firstAllowedDate;
        }

        private void OnRefreshRequested()
        {
            _presentationModel.DeclarationGridPresenter.Load();
            _presentationModel.BankAccountGridPresenter.Load();
            _presentationModel.RequestGridPresenter.Load();
        }

        #region Configure view data

        private void InitGeneralData()
        {
            _view.IsRequestBankAccountsPosible
                = _accessContext.AvailableInspections(MrrOperations.BankAccountRequestCreate).Any(i => i == _model.Sono);

            _view.SurView.ARGB = _model.SurARGB;
            _view.SurView.Description = _model.SurDescription;
            _view.TaxPayerName = _model.Name;

            if (_model.IsUL)
            {
                _view.ULInspection = _model.InspectionFullName;
                _view.ULInn = _model.Inn;
                _view.ULKpp = _model.Kpp;
                _view.ULCapital = _model.Capital;
                _view.ULRegistrationDate = _model.RegistrationData;
                _view.ULAccountDate = _model.AccountData;
                _view.ULDeregistrationDate = _model.DeregisterData;
                _view.ULOkved = _model.OkvedDescription;
                _view.ULAddress = _model.Address;
                _view.ULSize = _model.IsLarge;
            }
            else
            {
                _view.FLInspection = _model.InspectionFullName;
                _view.FLInn = _model.Inn;
                _view.FLRegistrationDate = _model.AccountData;
                _view.FLDeregistrationDate = _model.DeregisterData;
                _view.FLOkved = _model.OkvedDescription;
                _view.FLAddress = _model.Address;
            }
            _view.TaxPayerType = _model.TaxPayerType;

            _view.IsReorganized = _model.IsReorganized;
            if (_model.IsReorganized)
            {
                _view.SuccessorName = _model.NameSuccessor;
                _view.SuccessorInn = _model.InnSuccessor;
                _view.SuccessorKpp = _model.KppSuccessor;
                _view.SuccessorInspection = _model.InspectionFullName;
            }
        }

        private void FillStatusArea()
        {
            ExecuteServiceCallAsync(
                c => _service.GetTaxPayerSolvencyStatus(_model.Inn, _model.KppEffective),
                (c, result) => UIThreadExecutor.CurrentExecutor.BeginExecute(
                    ct =>
                    {
                        var status = result.Result;
                        if (status != null)
                        {
                            _view.IsStatusVisible = true;
                            _view.Status = status.Status;
                            _view.StatusDate = status.StatusDate.HasValue
                                ? status.StatusDate.Value.ToString("dd.MM.yyyy")
                                : string.Empty;
                            _view.StatusComment = status.StatusComment;
                        }
                        else
                        {
                            _view.IsStatusVisible = false;
                        }
                    },
                    DispatcherPriority.Normal,
                    c));
        }

        #endregion

        class SettingsProvider : CommonSettingsProvider
        {
            public SettingsProvider(WorkItem workItem, string key)
                : base(workItem, key)
            {
            }

            public override void SaveSettings(string valueSetup, string settingsKey = "settings")
            {

            }

            public override string LoadSettings(string settingsKey = "settings")
            {
                return null;
            }
        }

        #region Bank accounts

        private void InitAccountGridPresenter()
        {
            var qc = new QueryConditions
            {
                Sorting =
                    new List<ColumnSort>
                    {
                        new ColumnSort
                        {
                            ColumnKey = TypeHelper<BankAccount>.GetMemberName(x => x.BankName),
                            Order = ColumnSort.SortOrder.Asc
                        }
                    }
            };

            _presentationModel.BankAccountGridPresenter =
                new PagedDataGridPresenter<BankAccount>(
                    _view.BankAccountGridView,
                    _presentationModel.BankAccountsPager,
                    BankAccounts, qc);

            _presentationModel.BankAccountGridPresenter.Init(
                new SettingsProvider(WorkItem,
                string.Format("{0}_BankAccountGrid", GetType())));

            _presentationModel.BankAccountGridPresenter.Load();
        }

        public PageResult<BankAccount> BankAccounts(QueryConditions criteria)
        {
            var data = new PageResult<BankAccount>(new List<BankAccount>(), 0);

            if (!criteria.FilterValid()) return data;
            var criteriaSearh = criteria
                .WithInn(_model.Inn);

            ExecuteServiceCall(
                () => _model.IsUL ? _service.SelectULBankAccounts(criteriaSearh) : _service.SelectFLBankAccounts(criteriaSearh),
                result =>
                {
                    data = result.Result;
                });

            _model.BankAccounts = data.Rows;

            return data;
        }

        #endregion

        #region Declarations

        public bool IsTotalDiscrepancyQuantityVisible
        {
            get
            {
                return _accessContext.Operations.Any(x => x.Name == MrrOperations.TaxPayerDiscrepanciesAll);
            }
        }

        public bool IsKnpDiscrepancyQuantityVisible
        {
            get
            {
                return _accessContext.Operations.Any(x=>x.Name == MrrOperations.TaxPayerDiscrepanciesKnp) ;
            }
        }

        private void InitDeclarationGridPresenter()
        {
            var qc = new QueryConditions
            {
                Sorting =
                    new List<ColumnSort>
                    {
                        new ColumnSort
                        {
                            ColumnKey = TypeHelper<DeclarationModel>.GetMemberName(x => x.TaxPeriod),
                            Order = ColumnSort.SortOrder.Desc
                        }
                    }
            };

            _presentationModel.DeclarationGridPresenter =
                new DataGridPresenter<DeclarationModel>(
                    _view.DeclarationGridView,
                    Declarations, qc);

            _presentationModel.DeclarationGridPresenter.Init(
                new SettingsProvider(WorkItem,
                string.Format("{0}_DeclarationGrid", GetType())));

            _presentationModel.DeclarationGridPresenter.Load();
        }

        private PageResult<DeclarationModel> Declarations(QueryConditions criteria)
        {
            var data = new PageResult<DeclarationModel>(new List<DeclarationModel>(), 0);

            if (criteria.FilterValid())
            {
                var criteriaSearh =
                    criteria
                        .WithSort()
                        .WithInn(_model.Inn, "InnDeclarant")
                        .WithKppEffective(_model.KppEffective);

                ExecuteServiceCall(
                    () => _service.SelectTaxPayerDeclarations(criteriaSearh),
                    result =>
                    {
                        var rows = result.Result.Rows
                            .Select(dto => new DeclarationModel(dto))
                            .ToList();
                        data = new PageResult<DeclarationModel>(
                            rows,
                            result.Result.TotalMatches,
                            result.Result.TotalAvailable);
                    });
            }

            //aip костыль чтобы решить что делать с ОКВЕДом в журнале
            _model.CalcOkvedDescription = data.Rows.Any(x => x.IsDeclaration());
            //aip костыль чтобы решить что делать с Крупнейшим
            _model.IsLarge = data.Rows.Any(x => x.IsLarge());

            _view.AllowOpenDeclaration = data.Rows.Any();

            _view.IsRequestBankAccountsEnabled = false;

            _view.IsBankOperationsEnabled = data.Rows.Count > 0;

            _model.Declarations = data.Rows;

            return data;
        }

        #endregion

        #region Requests

        private void InitRequestsPresenter()
        {
            _presentationModel.RequestGridPresenter =
                new PagedDataGridPresenter<BankAccountRequestSummary>(
                    _view.GridRequests,
                    _presentationModel.RequestPager,
                    Requests);

            _presentationModel.RequestGridPresenter.Init(
                new SettingsProvider(WorkItem,
                    string.Format("{0}_RequestsGrid", GetType())));

            _presentationModel.RequestGridPresenter.Load();
        }

        private PageResult<BankAccountRequestSummary> Requests(QueryConditions criteria)
        {
            var data = new PageResult<BankAccountRequestSummary>
            {
                Rows = new List<BankAccountRequestSummary>()
            };

            if (criteria.FilterValid())
            {
                var criteriaSearch =
                    criteria
                    .WithInn(_model.Inn)
                    .WithKppEffective(_model.KppEffective, TypeHelper<BankAccountRequestSummary>.GetMemberName(x => x.KppEffective))
                    .OrderBy<BankAccountRequestSummary, DateTime?>(x => x.CreateDate, ColumnSort.SortOrder.Desc);

                ExecuteServiceCall(
                    () => _service.SelectBankAccountRequests(criteriaSearch, _model.Inn, _model.KppEffective),
                    result =>
                    {
                        data = new PageResult<BankAccountRequestSummary>(result.Result.Rows, result.Result.TotalMatches, result.Result.TotalAvailable);
                    });
            }

            return data;
        }

        #endregion

        public CardOpenResult ViewDeclarationDetails(long id)
        {
            var result = _declarationCardOpener.Open(id);
            if (!result.IsSuccess)
                _view.ShowError(result.Message);
            return result;
        }

        private const int AvailablePeriodsQuantity = 13;

        private const int MonthPerPeriod = 3;

        private IEnumerable<TaxPeriod> GetKnpPeriods(DateTime date)
        {
            var result = new List<TaxPeriod>();

            for (var i = 0; i < AvailablePeriodsQuantity; ++i)
            {
                var q = (int)Math.Ceiling(date.Month / (double)MonthPerPeriod);

                result.Add(new TaxPeriod
                {
                    Year = date.Year.ToString(),
                    Period = new TaxPeriodSummary
                    {
                        Code = q.ToString(),
                        Description = string.Format("{0} кв. {1}", q, date.Year),
                        MonthOfBegin = (q - 1)*MonthPerPeriod + 1,
                        MonthOfEnd = (q - 1)*MonthPerPeriod + MonthPerPeriod
                    }
                });

                date = date.AddMonths(-MonthPerPeriod);
            }
            return result;
        }

        private IEnumerable<TaxPeriod> GetPeriods()
        {
            return GetPeriods(_model.Declarations);
        }

        private IEnumerable<TaxPeriod> GetPeriods(IEnumerable<DeclarationModel> source)
        {
            var result =
                source
                    .Select(
                        d => new TaxPeriod
                        {
                            Period = _model.TaxPeriodList[d.PeriodCode],
                            Year = d.FiscalYear
                        })
                    .Distinct(new TaxPeriodComparer())
                    .OrderByDescending(x => x.Year)
                    .ThenByDescending(x => x.End)
                    .ThenByDescending(x => x.Begin);
            return result;
        }

        private void ViewBankAccountsRequestDialog(object sender, DeclarationModelEventArg arg)
        {
            if (_model.Declarations == null || arg.Model == null)
                return;

            var model = new BankAccountModel
            {
                Inn = arg.Model.InnContractor,
                Kpp = arg.Model.KppContractor,
                KppEffective = arg.Model.KppEffectiveContractor,
                IsReorganized = arg.Model.IsReorganized,
                UserName = UserDisplayName,
                UserSid = UserSid,
                IsJuridical = string.IsNullOrEmpty(arg.Model.KppContractor),
                SonoCode = _model.Sono,
                PeriodList = GetKnpPeriods(PeriodHelper.GetBeginDate(arg.Model.PeriodCode, arg.Model.FiscalYear)),
                SelectedYear = arg.Model.FiscalYear,
                SelectedPeriod = arg.Model.PeriodCode
            };

            var view = _view.GetBankAccountsRequestDialog();
            var presenter = new BankAccountsRequestDialog.Presenter(
                view,
                this,
                new CommonSettingsProvider(WorkItem, "RequestBankAccounts"),
                model);
            presenter.TryPushRequest();
            _presentationModel.RequestGridPresenter.ResetTotalsCache();
            _presentationModel.RequestGridPresenter.Load();
        }

        #region IBankAccountServiceProvider

        public PageResult<BankAccountBrief> BankAccountsForPeriod(QueryConditions criteria, string inn, string kpp, DateTime periodBegin, DateTime periodEnd)
        {
            var data = new PageResult<BankAccountBrief>(new List<BankAccountBrief>(), 0);

            if (!criteria.FilterValid()) return data;

            ExecuteServiceCall(
                () => _model.IsUL
                    ? _service.SelectULBankAccountBriefs(inn, kpp, periodBegin, periodEnd, criteria)
                    : _service.SelectFLBankAccountBriefs(inn, periodBegin, periodEnd, criteria),
                result =>
                {
                    data = result.Result;
                });

            return data;
        }

        public bool FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear)
        {
            var result = false;
            ExecuteServiceCall(
                () => _service.FindSentBankAccountRequest(inn, kppEffective, periodCode, fiscalYear),
                res =>
                {
                    result = res.Result.Count > 0;
                });
            return result;
        }

        public List<string> GetRequestedAccounts(string bik, DateTime begin, DateTime end)
        {
            var result = new List<string>();
            ExecuteServiceCall(
                () => _service.GetRequestedAccounts(bik, begin, end),
                res =>
                {
                    result = res.Result;
                });
            return result;
        }

        public bool PushRequest(BankAccountManualRequest request)
        {
            return ExecuteServiceCall(() => _service.PushBankAccountRequest(request)).Result;
        }

        #endregion

        private void OnBankOperationsViewRequested()
        {
            var period = _view.SelectTaxPeriod(GetPeriods());
            if (period == null)
                return;

            try
            {
                ShowOperationReportView(_model.Inn, period.Begin, period.End);
            }
            catch (Exception e)
            {
                _view.ShowError(string.Format(ResourceManagerNDS2.TaxPayerCard.OperationWindowFailed, e.Message));
            }
        }

        private void ShowOperationReportView(string taxpayerInn, DateTime beginDate, DateTime endDate)
        {

            var integrationService = WorkItem.Services.Get<IUcSubsystemIntegrationService>();
            const string remoteSubsystemName = "INIAS.TS";
            const string remoteActionName = "logicalCatalog://RoleCatalog/Abd/Models/AbdListServiceGenerated/AbdOperationReportOperation";


            if (!integrationService.HasSubsystem(remoteSubsystemName))
            {
                string msgText = string.Format(ResourceManagerNDS2.Integration.SubsystemNotFound, remoteSubsystemName);
                LogError(msgText, "ShowOperationReportView");
                _view.ShowError(msgText);

                return;
            }

            if (!integrationService.HasPublishedNavigationNode(remoteSubsystemName, remoteActionName))
            {
                string msgText =
                    string.Format(
                        ResourceManagerNDS2.Integration.ExternalSystemError,
                        remoteSubsystemName, remoteActionName);
                LogError(msgText, "ShowOperationReportView");
                _view.ShowError(msgText);

                return;
            }

            var parameters = new DataContext
            {
                new TypedDataContextItem<string>("TaxpayerInn", taxpayerInn),
                new TypedDataContextItem<DateTime>("BeginDate", beginDate),
                new TypedDataContextItem<DateTime>("EndDate", endDate)
            };

            var pack = new NavigationNodeExecutingPack
            {
                ParentSmartPartId = PresentationContext.Id,
                SubsystemIntegrationName = remoteSubsystemName,
                NodePublicName = remoteActionName,
                ExecutionContext = parameters,
                ResultContext = new DataContext(),
            };


            if (!integrationService.TryExecuteNavigationNode(pack))
            {
                string msgText = string.Format(ResourceManagerNDS2.Integration.ExternalCallFailed,
                    remoteSubsystemName, remoteActionName);

                LogError(msgText, "ShowOperationReportView", pack.ExecutionException);
                _view.ShowError(msgText + (pack.ExecutionException == null ? String.Empty : pack.ExecutionException.Message));
            }
        }

        private void StopWorkers()
        {
            if (_ctsLoadData != null)
                _ctsLoadData.Cancel();
        }

    }
}
