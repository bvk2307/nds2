﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public class DeclarationModel
    {
        # region Constructor

        private readonly TaxPayerDeclarationVersion _data;

        public DeclarationModel(TaxPayerDeclarationVersion data)
        {
            _data = data;
        }

        # endregion

        # region Grid Columns

        [DisplayName("Отчетный период")]
        [Description("Период за который подается декларация/журнал")]
        public string TaxPeriod
        {
            get
            {
                return _data.TaxPeriod;
            }
        }

        [DisplayName("Код отчетного периода")]
        [Description("Период за который подается декларация/журнал")]
        public string PeriodCode
        {
            get
            {
                return _data.PeriodCode;
            }
        }

        [DisplayName("Отчетный год")]
        [Description("Год подачи декларации/журнала")]
        public string FiscalYear
        {
            get
            {
                return _data.FiscalYear;
            }
        }
        
        
        [DisplayName("СУР НП")]
        [Description("СУР НП")]
        public int? SurCode
        {
            get
            {
                return _data.SurCode;
            }
        }

        [DisplayName("Тип")]
        [Description("НД по НДС или Журнал учета")]
        public string TypeDescription
        {
            get
            {
                return _data.TypeDescription;
            }
        }


        [DisplayName("Регистрационный №")]
        [Description("Регистрационный № СЭОД")]
        public string RegNumber
        {
            get
            {
                return _data.RegNumber;
            }
        }

        [DisplayName("Статус КНП")]
        [Description("Статус декларации")]
        public string KnpStatus
        {
            get
            {
                return _data.KnpStatus;
            }
        }

        [DisplayName("№ корр./версии")]
        [Description("Номер корректировки декларации / версии журнала")]
        public string CorrectionNumber
        {
            get { return _data.CorrectionNumber; }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string Sign
        {
            get
            {
                return _data.Sign;
            }
        }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public decimal? NdsTotal
        {
            get { return _data.NdsTotal; }
        }


        [DisplayName("Кол-во расхождений СФ")]
        [Description("Количество расхождений")]
        public long? DiscrepancyQuantity
        {
            get { return _data.DiscrepancyQuantity; }
        }

        [DisplayName("Кол-во расхождений СФ")]
        [Description("Количество расхождений")]
        public long? KnpDiscrepancyQuantity
        {
            get { return _data.KnpDiscrepancyQuantity; }
        }

        [DisplayName("Кол-во расхождений по КС")]
        [Description("Количество расхождений по КС в НД")]
        public int? ControlRatioQuantity
        {
            get
            {
                return _data.ControlRatioQuantity;
            }
        }

        public bool IsKnpOpened
        {
            get
            {
                return KnpStatus == "Открыта";
            }
        }

        [DisplayName("Дата")]
        [Description("Дата Аннулирования")]
        public DateTime? AnnulmentDate
        {
            get
            {
                return _data.AnnulmentDate;
            }
        }

        [DisplayName("Код причины")]
        [Description("Код причины аннулирования")]
        public int? AnnulmentReasonId
        {
            get
            {
                return _data.AnnulmentReasonId;
            }
        }

        # endregion

        public bool IsReorganized {
            get
            {
                return !string.IsNullOrEmpty(_data.InnReorganized);
            }
        }

        public string InnContractor
        {
            get
            {
                return string.IsNullOrEmpty(_data.InnReorganized) ? _data.InnDeclarant : _data.InnReorganized;
            }
        }

        public string KppContractor
        {
            get
            {
                return string.IsNullOrEmpty(_data.InnReorganized) ? _data.KppDeclarant : _data.KppReorganized;
            }
        }

        public string KppEffectiveContractor
        {
            get
            {
                return _data.KppEffectiveDeclarant;
            }
        }

        # region Hidden Data

        public long GetZip()
        {
            return _data.Zip;
        }

        public bool IsDeclaration()
        {
            return _data.TypeCode == 0;
        }

        public bool IsLarge()
        {
            return _data.IsLarge.HasValue
                && _data.IsLarge.Value;
        }

        public bool IsActive
        {
            get
            {
                return _data.IsActive;
            }
        }

        # endregion
    }
}
