﻿using System;
using System.Linq.Expressions;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public static class DeclarationsQueryHelper
    {
        public static QueryConditions WithInn(this QueryConditions criteria, string inn, string fieldName = "INN")
        {

            criteria.Filter.Add(
                new FilterQuery
                {
                    ColumnName = fieldName,
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                        new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                     ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                     Value = inn
                                }
                            }
                });

            return criteria;
        }

        public static QueryConditions WithSort(this QueryConditions criteria)
        {
            if (!criteria.Sorting.Any())
            {
                criteria.Sorting = 
                    new List<ColumnSort>
                    {
                        { new ColumnSort { ColumnKey = "FiscalYear", Order = ColumnSort.SortOrder.Desc }},
                        { new ColumnSort { ColumnKey = "PeriodWeight", Order = ColumnSort.SortOrder.Desc}},
                        { new ColumnSort { ColumnKey = "CorrectionNumberShort", Order = ColumnSort.SortOrder.Desc}},
                        { new ColumnSort { ColumnKey = "InnReorganized", Order = ColumnSort.SortOrder.Asc }}
                    };
            }

            return criteria;
        }

        public static QueryConditions WithKppEffective(this QueryConditions criteria, string kppEffective, string fieldName = "KppEffectiveDeclarant")
        {
            if (criteria.Filter.All(flt => flt.ColumnName != fieldName))
            {
                criteria.Filter.Add(
                    new FilterQuery()
                    {
                        ColumnName = fieldName,
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering =
                            new List<ColumnFilter>()
                            {
                                new ColumnFilter() 
                                {
                                     ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                     Value = kppEffective
                                }
                            }
                    });
            }

            return criteria;
        }

        public static QueryConditions WithCondition<T1, T2>(this QueryConditions criteria, Expression<Func<T1, T2>> key,
            object value) where T1 : class
        {
            criteria.Filter.Add(
                new FilterQuery
                {
                    ColumnName = Common.Contracts.Helpers.TypeHelper<T1>.GetMemberName(key),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                        new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                     ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                     Value = value
                                }
                            }
                });

            return criteria;
        }

        public static QueryConditions WithCondition<T1, T2>(this QueryConditions criteria, Expression<Func<T1, T2>> key,
           ColumnFilter.FilterComparisionOperator comparisonOperator, object value) where T1 : class
        {
            criteria.Filter.Add(
                new FilterQuery
                {
                    ColumnName = Common.Contracts.Helpers.TypeHelper<T1>.GetMemberName(key),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                        new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                     ComparisonOperator = comparisonOperator,
                                     Value = value
                                }
                            }
                });

            return criteria;
        }

        public static QueryConditions OrderBy<T1, T2>(this QueryConditions criteria, Expression<Func<T1, T2>> key, ColumnSort.SortOrder sortOrder = ColumnSort.SortOrder.Asc) where T1 : class
        {
            if (!criteria.Sorting.Any())
            {
                criteria.Sorting =
                    new List<ColumnSort>
                    {
                        new ColumnSort { ColumnKey = Common.Contracts.Helpers.TypeHelper<T1>.GetMemberName(key), Order = sortOrder }
                    };
            }

            return criteria;
        }
    }
}
