﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public class TaxPayerPresentationModel
    {
        #region Declarations

        public DataGridPresenter<DeclarationModel> DeclarationGridPresenter { get; set; }

        #endregion

        #region Bank accounts

        private readonly PagerStateViewModel _bankAccountsPager = new PagerStateViewModel();

        public IPager BankAccountsPager { get { return _bankAccountsPager; } }

        public PagedDataGridPresenter<BankAccount> BankAccountGridPresenter { get; set; }

        #endregion

        #region Requests

        private readonly PagerStateViewModel _requestPager = new PagerStateViewModel();

        public IPager RequestPager { get { return _requestPager; } }

        public PagedDataGridPresenter<BankAccountRequestSummary> RequestGridPresenter { get; set; }

        #endregion
    }
}
