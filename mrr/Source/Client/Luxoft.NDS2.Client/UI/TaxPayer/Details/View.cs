﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.GridColumns;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public partial class View : BaseView, ITaxPayerDetailView, IMessageService
    {
        private UcRibbonButtonToolContext _btnOpen;
        private UcRibbonButtonToolContext _btnTaxPayerCard;
        private UcRibbonButtonToolContext _btnRequestBankAccounts;
        private UcRibbonButtonToolContext _btnBankOperationsView;

        #region .ctors

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext context, WorkItem wi)
            : base(context, wi)
        {
            InitializeComponent();
            ReorgInnKpp.LinkClicked += (sender, args) => RaiseSuccessorRequested();
        }

        #endregion

        public void RefreshEKP()
        {
            var ucVisualStateService = WorkItem.Services.Get<IUcVisualStateService>(true);
            ucVisualStateService.DoRefreshVisualEnvironment(_presentationContext.Id);
        }

        #region Ribbon menu

        public void InitRibbon()
        {
            var resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2Result")
            {
                Text = "Функции",
                ToolTipText = "Функции",
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            _btnOpen = new UcRibbonButtonToolContext(_presentationContext, "btn" + "Open", "cmdTaxPayerCard" + "Open")
            {
                Text = "Открыть",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = true
            };

            var btnRefresh = new UcRibbonButtonToolContext(_presentationContext, "btnRefresh", "cmdTaxPayerCardRefresh")
            {
                Text = "Обновить",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnTaxPayerCard = new UcRibbonButtonToolContext(_presentationContext, "btnTaxPayerCard", "cmdTaxPayerCardTaxPayerCard")
            {
                Text = "Карточка правопреемника",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = IsReorganized
            };

            _btnRequestBankAccounts = new UcRibbonButtonToolContext(
                _presentationContext,
                "btnRequestBankAccounts",
                "cmdTaxPayerCardRequestBankAccounts")
            {
                Text = "Сформировать запрос",
                ToolTipText = ResourceManagerNDS2.TaxPayerCard.RequestBankAccountsUnavailable,
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "request",
                SmallImageName = "request",
                Enabled = false
            };

            _btnBankOperationsView = new UcRibbonButtonToolContext(
                _presentationContext,
                "btnBankOperationsView",
                "cmdTaxPayerCardBankOperationsView")
            {
                Text = "Просмотреть операции",
                ToolTipText = ResourceManagerNDS2.TaxPayerCard.BankOperationsUnavailable,
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "accounts",
                SmallImageName = "accounts",
                Enabled = false
            };

            groupNavigation1.ToolList.Add(new UcRibbonToolInstanceSettings(_btnOpen.ItemName, UcRibbonToolSize.Large));
            groupNavigation1.ToolList.Add(new UcRibbonToolInstanceSettings(btnRefresh.ItemName, UcRibbonToolSize.Large));
            //groupNavigation1.ToolList.Add(new UcRibbonToolInstanceSettings(_btnBankOperationsView.ItemName, UcRibbonToolSize.Large));
            if (_isRequestBankAccountsPosible)
                groupNavigation1.ToolList.Add(new UcRibbonToolInstanceSettings(_btnRequestBankAccounts.ItemName, UcRibbonToolSize.Large));
            if (IsReorganized)
                groupNavigation1.ToolList.Add(new UcRibbonToolInstanceSettings(_btnTaxPayerCard.ItemName,
                    UcRibbonToolSize.Large));

            //общее
            _presentationContext.UiVisualizationCollection.Add(_btnOpen);
            _presentationContext.UiVisualizationCollection.Add(btnRefresh);
            //_presentationContext.UiVisualizationCollection.Add(_btnBankOperationsView);
            if (IsReorganized)
                _presentationContext.UiVisualizationCollection.Add(_btnTaxPayerCard);
            if (_isRequestBankAccountsPosible)
                _presentationContext.UiVisualizationCollection.Add(_btnRequestBankAccounts);
            _presentationContext.UiVisualizationCollection.Add(tabNavigator);

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;

            RefreshEKP();
        }

        [CommandHandler("cmdTaxPayerCardOpen")]
        public virtual void ViewBtnClick(object sender, EventArgs e)
        {
            OnOpenDeclarationClick(sender, e);
        }

        [CommandHandler("cmdTaxPayerCardRefresh")]
        public virtual void RefreshClick(object sender, EventArgs e)
        {
            var handler = RefreshRequested;
            if (handler != null)
                handler();
        }

        [CommandHandler("cmdTaxPayerCardTaxPayerCard")]
        public virtual void TaxPayerCardBtnClick(object sender, EventArgs e)
        {
            RaiseSuccessorRequested();
        }

        [CommandHandler("cmdTaxPayerCardRequestBankAccounts")]
        public virtual void RequestBankAccountsClick(object sender, EventArgs e)
        {
            RaiseBankAccontsRequested();
        }

        [CommandHandler("cmdTaxPayerCardBankOperationsView")]
        public virtual void BankOperationsViewClick(object sender, EventArgs e)
        {
            var handler = BankOperationsViewRequested;
            if (handler != null)
                handler();
        }

        public override void OnUnload()
        {
            Dispose();
            base.OnUnload();
        }
        public bool IsRequestBankAccountsEnabled
        {
            set
            {
                _btnRequestBankAccounts.Enabled = value;
                _btnRequestBankAccounts.ToolTipText = value
                    ? ResourceManagerNDS2.TaxPayerCard.RequestBankAccountsAvailable
                    : ResourceManagerNDS2.TaxPayerCard.RequestBankAccountsUnavailable;
                RefreshEKP();
            }
        }

        public bool IsBankOperationsEnabled
        {
            set
            {
                _btnBankOperationsView.Enabled = value;
                _btnBankOperationsView.ToolTipText = value
                    ? ResourceManagerNDS2.TaxPayerCard.BankOperationsAvailable
                    : ResourceManagerNDS2.TaxPayerCard.BankOperationsUnavailable;
                RefreshEKP();
            }
        }

        public bool IsRequestBankAccountsPosible
        {
            set
            {
                _isRequestBankAccountsPosible = value;
                if (_btnRequestBankAccounts != null)
                {
                    _btnRequestBankAccounts.Visible = value;
                    RefreshEKP();
                }
            }
        }

        private bool _isRequestBankAccountsPosible;

        #endregion

        #region General data

        public ISurView SurView
        {
            get
            {
                return _surIndicator;
            }
        }

        public string TaxPayerName
        {
            get { return labelTaxPayerName.Text; }
            set { labelTaxPayerName.Text = value; }
        }

        public TaxPayerType TaxPayerType
        {
            get
            {
                return (TaxPayerTypeTab.SelectedTab == ULTab.Tab)
                    ? TaxPayerType.Juridical
                    : TaxPayerType.Individual;
            }
            set
            {
                TaxPayerTypeTab.SelectedTab = (value == TaxPayerType.Juridical)
                    ? ULTab.Tab
                    : FLTab.Tab;
            }
        }

        #region UL

        public string ULInspection
        {
            get
            {
                return ULInspectionValueLabel.Text;
            }
            set
            {
                ULInspectionValueLabel.Text = value;
            }
        }

        public string ULInn
        {
            get
            {
                return ULINNValueLabel.Text;
            }
            set
            {
                ULINNValueLabel.Text = value;
            }
        }

        public string ULKpp
        {
            get
            {
                return ULKPPValueLabel.Text;
            }
            set
            {
                ULKPPValueLabel.Text = value;
            }
        }

        public string ULCapital
        {
            get { return ULCapitalValueLabel.Text; }
            set
            {
                ULCapitalValueLabel.Text = value;
            }
        }

        public string ULRegistrationDate
        {
            get
            {
                return ULRegistrationDataValueLabel.Text;
            }
            set
            {
                ULRegistrationDataValueLabel.Text = value;
            }
        }

        public string ULAccountDate
        {
            get
            {
                return ULAccountDataValueLabel.Text;
            }
            set
            {
                ULAccountDataValueLabel.Text = value;
            }
        }

        public string ULDeregistrationDate
        {
            get
            {
                return ULDeregisterDataValueLabel.Text;
            }
            set
            {
                ULDeregisterDataValueLabel.Text = value;
            }
        }

        public string ULOkved
        {
            get { return ULOkvedValueEdit.Text; }
            set { ULOkvedValueEdit.Text = value; }
        }

        public string ULAddress
        {
            get { return ULAddressValueEdit.Text; }
            set { ULAddressValueEdit.Text = value; }
        }

        public bool ULSize
        {
            get { return ULSizeLabel.Text.Length > 0; }
            set { ULSizeLabel.Text = value ? "Крупнейший" : string.Empty; }
        }

        #endregion

        #region FL

        public string FLInn
        {
            get
            {
                return FLINNValueLabel.Text;
            }
            set
            {
                FLINNValueLabel.Text = value;
            }
        }

        public string FLInspection
        {
            get
            {
                return FLInspectionValueLabel.Text;
            }
            set
            {
                FLInspectionValueLabel.Text = value;
            }
        }

        public string FLRegistrationDate
        {
            get
            {
                return FLRegistrationDateValueLabel.Text;
            }
            set
            {
                FLRegistrationDateValueLabel.Text = value;
            }
        }

        public string FLDeregistrationDate
        {
            get
            {
                return FLDeregistrationDateValueLabel.Text;
            }
            set
            {
                FLDeregistrationDateValueLabel.Text = value;
            }
        }

        public string FLOkved
        {
            get { return FLOkvedValueEdit.Text; }
            set { FLOkvedValueEdit.Text = value; }
        }

        public string FLAddress
        {
            get { return FLAddressValueEdit.Text; }
            set { FLAddressValueEdit.Text = value; }
        }

        #endregion

        #region ReorgData

        public bool IsReorganized
        {
            get { return ReorgBox.Visible; }
            set
            {
                ReorgBox.Visible = value;
                RefreshEKP();
            }
        }

        public string SuccessorName
        {
            get { return ReorgName.Text; }
            set { ReorgName.Text = value; }
        }

        public string SuccessorInn
        {
            get { return _successorInn; }
            set
            {
                _successorInn = value;
                ReorgInnKpp.Value = string.Format("<a href='#'>{0}</a> / {1}", _successorInn, _successorKpp);
            }
        }
        private string _successorInn;

        public string SuccessorKpp
        {
            get { return _successorKpp; }
            set
            {
                _successorKpp = value;
                ReorgInnKpp.Value = string.Format("<a href='#'>{0}</a> / {1}", _successorInn, _successorKpp);
            }
        }
        private string _successorKpp;

        public string SuccessorInspection
        {
            get { return ReorgSono.Text; }
            set { ReorgSono.Text = value; }
        }

        #endregion

        #region Status area

        public bool IsStatusVisible
        {
            set
            {
                _groupStatus.Visible = value;
            }
        }

        public string Status
        {
            set
            {
                _labelStatusValue.Text = value;
            }
        }

        public string StatusDate
        {
            set
            {
                _labelDateValue.Text = value;
            }
        }

        public string StatusComment
        {
            set
            {
                _textComment.Text = value;
            }
        }

        #endregion

        #endregion

        #region BankAccount grid

        public IDataGridView BankAccountGridView { get { return _bankAccountsGrid; } }

        public void InitBanksAccountsGrid(IPager pager)
        {
            _bankAccountsGrid
                .WithPager(new ExtendedDataGridPageNavigator(pager))
                .InitColumns(GetBankAccountGridSetup());
        }

        private GridColumnSetup GetBankAccountGridSetup()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<BankAccount>(_bankAccountsGrid);

            setup
                .WithColumn(helper.CreateTextColumn(o => o.BankInn))
                .WithColumn(helper.CreateTextColumn(o => o.BankKpp))
                .WithColumn(helper.CreateTextColumn(o => o.BIK))
                .WithColumn(helper.CreateTextColumn(o => o.RegistrationBankNumber))
                .WithColumn(helper.CreateTextColumn(o => o.BankBrunhNumber))
                .WithColumn(helper.CreateTextColumn(o => o.BankName).Configure(d => d.Width = 400))
                .WithColumn(helper.CreateTextColumn(o => o.AccountNumber).Configure(d => d.Width = 200))
                .WithColumn(helper.CreateTextColumn(o => o.AccountValute))
                .WithColumn(helper.CreateTextColumn(o => o.OpenDate))
                .WithColumn(helper.CreateTextColumn(o => o.CloseDate))
                .WithColumn(helper.CreateTextColumn(o => o.Inn))
                .WithColumn(helper.CreateTextColumn(o => o.Kpp))
                .WithColumn(helper.CreateTextColumn(o => o.AccountType));

            return setup;
        }

        #endregion

        #region Declaration grid

        public bool IsDeclarationListFocused
        {
            get
            {
                return _tabControl.SelectedTab != null && _tabControl.SelectedTab.Index == 0;
            }
        }

        public bool AllowOpenDeclaration
        {
            set
            {
                if (value == _btnOpen.Enabled)
                    return;
                _btnOpen.Enabled = value;
                RefreshEKP();
            }
        }

        public IDataGridView DeclarationGridView { get { return _declarationGrid; } }

        public void InitDeclarationsGrid(IDeclarationColumnVisibility visibility, DictionarySur surDic, DictionaryAnnulmentReason annulmentReasonDict)
        {
            _declarationGrid
                .InitColumns(GetDeclarationsGridSetup(visibility, surDic, annulmentReasonDict));

            _declarationGrid.SelectRow += RaiseFocusedDeclarationChanged;
            _declarationGrid.AfterDataSourceChanged += () => RaiseFocusedDeclarationChanged(this, new EventArgs());
            _tabControl.SelectedTabChanged += RaiseFocusedDeclarationChanged;

            _declarationGrid.RowDoubleClicked +=
                i => RaiseDeclarationRequested((DeclarationModel)i);
        }

        private void RaiseFocusedDeclarationChanged(object sender, EventArgs e)
        {
            var handler = FocusedDeclarationChanged;
            if (handler == null)
                return;

            var decl = _declarationGrid.GetCurrentItem<DeclarationModel>();
            handler(this, new DeclarationModelEventArg(decl));
        }

        public event EventHandler<DeclarationModelEventArg> FocusedDeclarationChanged;

        private GridColumnSetup GetDeclarationsGridSetup(IDeclarationColumnVisibility visibility, DictionarySur surDic, DictionaryAnnulmentReason annulmentReasonDict)
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<DeclarationModel>(_declarationGrid);

            var formatNDSSum = new SumNDSDeclarationFormatter();

            var annulmentGroup = helper.CreateGroup("Аннулирована");
            annulmentGroup.Columns.Add(helper.CreateTextColumn(col => col.AnnulmentDate));
            annulmentGroup.Columns.Add(helper.CreateTextColumn(col => col.AnnulmentReasonId, new CustomDictionaryFilterManager(annulmentReasonDict.GetAutoFilterDictionary()))
                .Configure(col => col.CellTextAlign=HorizontalAlign.Right)
                .Configure(col => col.ToolTipViewer = new AnnulmentCauseToolTipViewer(annulmentReasonDict)));

            setup
                .WithColumn(helper.CreateTextColumn(col => col.TaxPeriod, 2))
                .WithColumn(helper.CreateSurColumn(o => o.SurCode, surDic, 2))
                .WithColumn(helper.CreateTextColumn(col => col.TypeDescription, 2))
                .WithColumn(helper.CreateTextColumn(col => col.RegNumber, 2))
                .WithColumn(helper.CreateTextColumn(col => col.KnpStatus, 2))
                .WithColumn(helper.CreateTextColumn(col => col.CorrectionNumber, 2))
                .WithColumn(helper.CreateTextColumn(col => col.Sign,2))
                .WithColumn(helper.CreateNumberColumn(col => col.NdsTotal, 2)
                    .Configure(d => { d.DataFormatInfo = formatNDSSum; }))
                .WithColumn(helper.CreateNumberColumn(col => col.DiscrepancyQuantity, 2)
                    .Configure(d => d.Hidden = !visibility.IsTotalDiscrepancyQuantityVisible))
                .WithColumn(helper.CreateNumberColumn(col => col.KnpDiscrepancyQuantity, 2)
                    .Configure(d => d.Hidden = !visibility.IsKnpDiscrepancyQuantityVisible))
                .WithColumn(helper.CreateTextColumn(col => col.ControlRatioQuantity, 2)
                    .Configure(col => col.CellTextAlign = HorizontalAlign.Right))
                .WithGroup(annulmentGroup);

            return setup;
        }

        private class AnnulmentCauseToolTipViewer : ToolTipViewerBase
        {
            private readonly DictionaryAnnulmentReason _annulmentReasonDict;

            public AnnulmentCauseToolTipViewer(DictionaryAnnulmentReason annulmentReasonDict)
            {
                _annulmentReasonDict = annulmentReasonDict;
            }

            public override string GetToolTip(object listObject)
            {
                var model = (DeclarationModel) listObject;
                return model.AnnulmentReasonId == null ? null : _annulmentReasonDict.GetName(model.AnnulmentReasonId.Value);
            }
        }

        #endregion

        #region Requests grid

        public IDataGridView GridRequests { get { return _gridRequests; } }

        public void InitRequestsGrid(IPager pager)
        {
            _gridRequests
                .WithPager(new ExtendedDataGridPageNavigator(pager))
                .InitColumns(GetGridRequestsSetup());
        }

        private GridColumnSetup GetGridRequestsSetup()
        {
            var helper = new ColumnHelper<BankAccountRequestSummary>(_gridRequests);

            var setup = new GridColumnSetup()
                .WithColumn(helper.CreateTextColumn(o => o.CreateDate))
                .WithColumn(helper.CreateTextColumn(o => o.BankInn))
                .WithColumn(helper.CreateTextColumn(o => o.BankKpp))
                .WithColumn(helper.CreateTextColumn(o => o.Bik))
                .WithColumn(helper.CreateTextColumn(o => o.BankNumber).MakeHidden())
                .WithColumn(helper.CreateTextColumn(o => o.BankFilial).MakeHidden())
                .WithColumn(helper.CreateTextColumn(o => o.BankName))
                .WithColumn(helper.CreateTextColumn(o => o.AccountsList, width:200).Configure(c =>
                {
                    c.WrapText = true;
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.FullTaxPeriod))
                .WithColumn(helper.CreateTextColumn(o => o.RequestBeginDate))
                .WithColumn(helper.CreateTextColumn(o => o.RequestEndDate))
                .WithColumn(helper.CreateTextColumn(o => o.RequestNumber))
                .WithColumn(helper.CreateTextColumn(o => o.RequestDate))
                .WithColumn(helper.CreateTextColumn(o => o.RequestStatus))
                .WithColumn(helper.CreateTextColumn(o => o.InnContractor))
                .WithColumn(helper.CreateTextColumn(o => o.KppContractor))
                .WithColumn(helper.CreateTextColumn(o => o.Requester));

            return setup;
        }

        #endregion

        #region Events

        public event ParameterlessEventHandler RefreshRequested;

        public event Action<DeclarationModel> DeclarationRequested;

        private void RaiseDeclarationRequested(DeclarationModel d)
        {
            var handler = DeclarationRequested;
            if (handler != null)
                handler(d);
        }

        public event ParameterlessEventHandler SuccessorRequested;

        private void RaiseSuccessorRequested()
        {
            var handler = SuccessorRequested;
            if (handler != null)
                handler();
        }

        public event EventHandler<DeclarationModelEventArg> BankAccountsRequested;

        public event ParameterlessEventHandler BankOperationsViewRequested;

        public IDialogView GetBankAccountsRequestDialog()
        {
            return new DialogView(this) { Owner = ParentForm };
        }

        public TaxPeriod SelectTaxPeriod(IEnumerable<TaxPeriod> periods)
        {
            return SelectPeriodDialog.DialogView.TrySelectPeriod(ParentForm, periods);
        }

        private void RaiseBankAccontsRequested()
        {
            var handler = BankAccountsRequested;
            if (handler == null)
                return;

            var decl = _declarationGrid.GetCurrentItem<DeclarationModel>();
            handler(this, new DeclarationModelEventArg(decl));
        }

        public event ParameterlessEventHandler Stop;

        public override void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            var handler = Stop;
            if (handler != null)
                handler();
        }

        private void OnViewLayout(object sender, System.Windows.Forms.LayoutEventArgs e)
        {
            var displayHeight = DisplayRectangle.Height;

            var h1 = HeadPanel.Height;
            var h2 = DetailsBox.Height;
            var h3 = ReorgBox.Visible ? ReorgBox.Height : 0;
            var h4 = DataLoadProgress.Height;
            var h5 = _groupStatus.Visible ? _groupStatus.Height : 0;

            var delta = displayHeight - h1 - h2 - h3 - h4 - h5;

            _tabControl.Height = delta < 200 ? 200 : delta;
        }

        private void OnDeclarationContextMenuOpening(object sender, CancelEventArgs e)
        {
            var currentItem = _declarationGrid.GetCurrentItem<DeclarationModel>();

            if (currentItem != null)
            {
                cmiOpenDeclCard.Visible = true;
                cmiOpenDeclCard.Text = currentItem.IsDeclaration() ? "Открыть декларацию" : "Открыть журнал";
            }
            else
            {
                cmiOpenDeclCard.Visible = false;
            }

        }

        private void OnOpenDeclarationClick(object sender, EventArgs e)
        {
            var d = _declarationGrid.GetCurrentItem<DeclarationModel>();

            if (d != null)
                RaiseDeclarationRequested(d);
        }

        #endregion

    }
}
