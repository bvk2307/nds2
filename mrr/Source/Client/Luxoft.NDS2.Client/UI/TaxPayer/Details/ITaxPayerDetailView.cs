﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public enum TaxPayerType
    {
        Individual = 0,
        Juridical = 1
    }

    public class DeclarationModelEventArg : EventArgs
    {
        public DeclarationModel Model { get; private set; }

        public DeclarationModelEventArg(DeclarationModel model)
        {
            Model = model;
        }
    }

    public interface IDeclarationColumnVisibility
    {
        bool IsTotalDiscrepancyQuantityVisible { get; }
        bool IsKnpDiscrepancyQuantityVisible { get; }
    }

    public interface ITaxPayerDetailView: IViewBase
    {

        IDialogView GetBankAccountsRequestDialog();

        TaxPeriod SelectTaxPeriod(IEnumerable<TaxPeriod> periods);

        Controls.Grid.V2.IDataGridView DeclarationGridView { get; }

        Controls.Grid.V2.IDataGridView BankAccountGridView { get; }

        Controls.Grid.V2.IDataGridView GridRequests { get; }

        #region General data

        ISurView SurView { get; }

        bool AllowOpenDeclaration { set; }

        string TaxPayerName { get; set; }

        TaxPayerType TaxPayerType { get; set; }

        #region UL

        string ULInspection { get; set; }
        
        string ULKpp { get; set; }
        
        string ULInn { get; set; }
        
        string ULCapital { get; set; }
        
        string ULRegistrationDate { get; set; }
        
        string ULDeregistrationDate { get; set; }
        
        string ULAccountDate { get; set; }
        
        string ULOkved { get; set; }
        
        string ULAddress { get; set; }
        
        bool ULSize { get; set; }

        #endregion

        #region FL

        string FLInspection { get; set; }
        
        string FLInn { get; set; }
        
        string FLRegistrationDate { get; set; }
        
        string FLDeregistrationDate { get; set; }
        
        string FLAddress { get; set; }
        
        string FLOkved { get; set; }

        #endregion

        #region Successor

        bool IsReorganized { get; set; }
        
        string SuccessorInspection { get; set; }
        
        string SuccessorKpp { get; set; }
        
        string SuccessorInn { get; set; }
        
        string SuccessorName { get; set; }

        #endregion

        #region Status

        bool IsStatusVisible { set; }

        string Status { set; }

        string StatusDate { set; }

        string StatusComment { set; }

        #endregion

        #endregion

        #region Ribbon

        void InitRibbon();

        bool IsRequestBankAccountsEnabled { set; }

        bool IsRequestBankAccountsPosible { set; }

        bool IsBankOperationsEnabled { set; }

        #endregion

        bool IsDeclarationListFocused { get; }

        event EventHandler<DeclarationModelEventArg> FocusedDeclarationChanged;

        void InitDeclarationsGrid(IDeclarationColumnVisibility visibility, DictionarySur surDic, DictionaryAnnulmentReason annulmentReasonDict);

        void InitBanksAccountsGrid(IPager pager);

        event Base.ParameterlessEventHandler Stop;

        event Base.ParameterlessEventHandler RefreshRequested;

        event Action<DeclarationModel> DeclarationRequested;

        event Base.ParameterlessEventHandler SuccessorRequested;

        event EventHandler<DeclarationModelEventArg> BankAccountsRequested;

        event Base.ParameterlessEventHandler BankOperationsViewRequested;
        
        void InitRequestsGrid(IPager pager);
    }
}