﻿using System.Collections.Generic;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using System;
using System.ComponentModel;
using Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using dto = Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    public class TaxPayerModel : INotifyPropertyChanged
    {
        private const string DateViewFormat = "dd.MM.yyyy";

        public TaxPayerModel(dto.TaxPayer data, ChainAccessModel chain, DictionarySur sur, DictionaryAnnulmentReason annulmentReason, Dictionary<string, TaxPeriodSummary> periods)
        {
            RawData = data;
            Chain = chain;
            Sur = sur;
            AnnulmentReason = annulmentReason;
            TaxPeriodList = periods;
        }

        /// <summary>
        /// В дальнейшем убрать прямой доступ к DTO
        /// </summary>
        private dto.TaxPayer RawData
        {
            get;
            set;
        }

        public ChainAccessModel Chain
        {
            get;
            private set;
        }

        public DictionarySur Sur
        {
            get;
            private set;
        }

        public DictionaryAnnulmentReason AnnulmentReason { get; private set; }

        public string Title
        {
            get
            {
                return string.Format(
                    "{0} (НП {1}) {2}", 
                    RawData.Inn, 
                    !RawData.IsUL ? "ИП" : "ЮЛ",
                    IsReorganized ? " — реорганизованный" : "");
            }
        }

        public bool IsReorganized
        {
            get
            {
                return !string.IsNullOrEmpty(RawData.InnSuccessor) && RawData.Inn != RawData.InnSuccessor;
            }
        }

        /// <summary>
        /// флаг показывает  необходимость рассчета полного наименования окведа.
        /// </summary>
        private bool _calcOkvedDescription = true;

        public bool CalcOkvedDescription
        {
            get
            {
                return _calcOkvedDescription;
            }
            set
            {
                if (value == _calcOkvedDescription) return;
                _calcOkvedDescription = value;
                NotifyPropertyChanged("CalcOkvedDescription");
            }
        }
        
        /// <summary>
        /// Строка отображения полного наименования ОКВЕДа
        /// </summary>
        public string OkvedDescription
        {
            get
            {
                if (RawData == null || !_calcOkvedDescription)
                    return string.Empty;
                return string.Format("{0} - {1}", RawData.OkvedCode, !string.IsNullOrEmpty(RawData.OkvedName) ? RawData.OkvedName : "неверно указан ОКВЭД");
            }
        }

        #region Данные

        public int? SurCode
        {
            get
            {
                return RawData != null ? RawData.SurCode : null;
            }
        }

        public string Name
        {
            get
            {
                return RawData != null ? RawData.Name : null;
            }
        }

        public bool IsUL
        {
            get
            {
                return RawData == null || RawData.IsUL;
            }
        }

        public string Sono
        {
            get
            {
                return RawData != null ? RawData.Soun : null;
            }
        }

        public string SonoName
        {
            get
            {
                return RawData != null ? RawData.SounName : null;
            }
        }

        public string Inn
        {
            get
            {
                return RawData != null ? RawData.Inn : null;
            }
        }

        public string Kpp
        {
            get
            {
                return RawData != null ? RawData.Kpp : null;
            }
        }

        public string KppEffective
        {
            get
            {
                return RawData != null ? RawData.KppEffective : null;
            }
        }

        public string InnSuccessor
        {
            get
            {
                return RawData != null ? RawData.InnSuccessor : null;
            }
        }

        public string KppSuccessor
        {
            get
            {
                return RawData != null ? RawData.KppSuccessor : null;
            }
        }

        public string KppEffectiveSuccessor
        {
            get
            {
                return RawData != null ? RawData.KppEffectiveSuccessor : null;
            }
        }

        public string NameSuccessor
        {
            get
            {
                return RawData != null ? RawData.NameSuccessor : null;
            }
        }

        public DateTime? RecordedAt
        {
            get
            {
                return RawData != null ? RawData.RecordedAt : null;
            }
        }

        public decimal RegulationFund
        {
            get
            {
                return RawData != null ? RawData.RegulationFund : decimal.Zero;
            }
        }

        public DateTime? RegistryAt
        {
            get
            {
                return RawData != null ? RawData.RegistryAt : null;
            }
        }

        public DateTime? UnregistryAt
        {
            get
            {
                return RawData != null ? RawData.UnregistryAt : null;
            }
        }

        public string Address
        {
            get
            {
                return RawData != null ? RawData.Address : null;
            }
        }


        bool? _isLarge;

        public bool IsLarge
        {
            set
            {
                if (value == _isLarge) return;
                _isLarge = value;
                NotifyPropertyChanged("IsLarge");
            }
            get
            {
                if (_isLarge.HasValue)
                    return _isLarge.Value;

                _isLarge = RawData != null && RawData.IsLArge;
                return _isLarge.Value;
            }
        }

        public string OkvedCode
        {
            get
            {
                return RawData != null ? RawData.OkvedCode : null;
            }
        }

        public string OkvedName
        {
            get
            {
                return RawData != null ? RawData.OkvedName : null;
            }
        }

        public int SurARGB
        {
            get
            {
                return Sur.GetARGB(SurCode);
            }
        }

        public string SurDescription
        {
            get
            {
                return Sur.Description(SurCode);
            }
        }

        public string InspectionFullName
        {
            get
            {
                return string.Format("{0} - {1}", Sono, SonoName);
            }
        }

        public string Capital { get { return string.Format(new IntValueZeroFormatter(), "{0:N2}", RegulationFund); } }

        public string RegistrationData { get { return RecordedAt.HasValue ? RecordedAt.Value.ToString(DateViewFormat) : string.Empty; } }

        public string AccountData { get { return RegistryAt.HasValue ? RegistryAt.Value.ToString(DateViewFormat) : string.Empty; } }
        
        public string DeregisterData { get { return UnregistryAt.HasValue ? UnregistryAt.Value.ToString(DateViewFormat) : string.Empty; } }

        public TaxPayerType TaxPayerType { get { return IsUL ? TaxPayerType.Juridical : TaxPayerType.Individual; }}

        #endregion

        # region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        /// </summary>
        /// <param name="propertyName">Name of changed property</param>
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        # endregion

        #region Декларации

        public List<DeclarationModel> Declarations { get; set; }
        
        public List<BankAccount> BankAccounts { get; set; }

        #endregion

        #region Справочники

        public Dictionary<string, TaxPeriodSummary> TaxPeriodList { get; private set; }

        #endregion
    }
}
