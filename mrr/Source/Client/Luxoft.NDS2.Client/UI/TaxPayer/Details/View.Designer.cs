﻿using Infragistics.Win.Misc;
using Infragistics.Win.SupportDialogs.ConditionalFormatting;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Details
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraExpandableGroupBoxPanel _detailsBoxArea;
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Misc.UltraLabel ULlabelOKVED;
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULAddressCaptionLabel;
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULDeregisterDataCaptionLabel;
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULAccountDataCaptionLabel;
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULRegistrationDataCaptionLabel;
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULCapitalCaptionLabel;
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULKPPCaptionLabel;
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULINNCaptionLabel;
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ULInspectionCaptionLabel;
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLlabelOKVED;
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLAddressCaptionLabel;
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLDeregistrationDateCaptionLabel;
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLRegistrationDateCaptionLabel;
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLINNCaptionLabel;
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel FLInspectionCaptionLabel;
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel2;
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel4;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel6;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel8;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel10;
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel12;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel14;
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel17;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ReorgSonoLabel;
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ReorgInnKppLabel;
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ReorgNameLabel;
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _labelStatusTitle;
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _labelDateTitle;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.TaxPayerTypeTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this._pageDeclarations = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ULTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ULOkvedValueEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ULAddressValueEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ULDeregisterDataValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULAccountDataValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULRegistrationDataValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULCapitalValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULKPPValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULINNValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULSizeLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ULInspectionValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.FLTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.FLOkvedValueEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.FLAddressValueEdit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.FLDeregistrationDateValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.FLRegistrationDateValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.FLINNValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.FLInspectionValueLabel = new Infragistics.Win.Misc.UltraLabel();
            this.gridControl1 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this._tabControlDeclarations = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._declarationGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.declContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenDeclCard = new System.Windows.Forms.ToolStripMenuItem();
            this._tabControlAccounts = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._bankAccountsGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this._tabControlRequests = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._gridRequests = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.DetailsBox = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.HeadPanel = new Infragistics.Win.Misc.UltraPanel();
            this.labelTaxPayerName = new Infragistics.Win.Misc.UltraLabel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurView();
            this.marginPanel = new Infragistics.Win.Misc.UltraPanel();
            this.DataLoadProgress = new System.Windows.Forms.ProgressBar();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraExpandableGroupBox1 = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel3 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel4 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel5 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel6 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel7 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel8 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel9 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._tabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this._gridTabSharedControl = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraExpandableGroupBoxPanel10 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ReorgBox = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel2 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ReorgSono = new Infragistics.Win.Misc.UltraLabel();
            this.ReorgInnKpp = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ReorgName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraExpandableGroupBoxPanel11 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraExpandableGroupBoxPanel12 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._groupStatus = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this._panelStatus = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._textComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._labelDateValue = new Infragistics.Win.Misc.UltraLabel();
            this._labelStatusValue = new Infragistics.Win.Misc.UltraLabel();
            _detailsBoxArea = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ULlabelOKVED = new Infragistics.Win.Misc.UltraLabel();
            ULAddressCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULDeregisterDataCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULAccountDataCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULRegistrationDataCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULCapitalCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULKPPCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULINNCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ULInspectionCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            FLlabelOKVED = new Infragistics.Win.Misc.UltraLabel();
            FLAddressCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            FLDeregistrationDateCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            FLRegistrationDateCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            FLINNCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            FLInspectionCaptionLabel = new Infragistics.Win.Misc.UltraLabel();
            ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            ReorgSonoLabel = new Infragistics.Win.Misc.UltraLabel();
            ReorgInnKppLabel = new Infragistics.Win.Misc.UltraLabel();
            ReorgNameLabel = new Infragistics.Win.Misc.UltraLabel();
            _labelStatusTitle = new Infragistics.Win.Misc.UltraLabel();
            _labelDateTitle = new Infragistics.Win.Misc.UltraLabel();
            _detailsBoxArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxPayerTypeTab)).BeginInit();
            this.TaxPayerTypeTab.SuspendLayout();
            this.ULTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ULOkvedValueEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULAddressValueEdit)).BeginInit();
            this.FLTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FLOkvedValueEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FLAddressValueEdit)).BeginInit();
            ultraExpandableGroupBoxPanel1.SuspendLayout();
            this._tabControlDeclarations.SuspendLayout();
            this.declContextMenu.SuspendLayout();
            this._tabControlAccounts.SuspendLayout();
            this._tabControlRequests.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsBox)).BeginInit();
            this.DetailsBox.SuspendLayout();
            this.HeadPanel.ClientArea.SuspendLayout();
            this.HeadPanel.SuspendLayout();
            this.marginPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).BeginInit();
            this._tabControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReorgBox)).BeginInit();
            this.ReorgBox.SuspendLayout();
            this.ultraExpandableGroupBoxPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._groupStatus)).BeginInit();
            this._groupStatus.SuspendLayout();
            this._panelStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._textComment)).BeginInit();
            this.SuspendLayout();
            // 
            // _detailsBoxArea
            // 
            _detailsBoxArea.Controls.Add(this.TaxPayerTypeTab);
            _detailsBoxArea.Dock = System.Windows.Forms.DockStyle.Fill;
            _detailsBoxArea.Location = new System.Drawing.Point(3, 19);
            _detailsBoxArea.Name = "_detailsBoxArea";
            _detailsBoxArea.Size = new System.Drawing.Size(607, 221);
            _detailsBoxArea.TabIndex = 0;
            // 
            // TaxPayerTypeTab
            // 
            this.TaxPayerTypeTab.Controls.Add(this._pageDeclarations);
            this.TaxPayerTypeTab.Controls.Add(this.ULTab);
            this.TaxPayerTypeTab.Controls.Add(this.FLTab);
            this.TaxPayerTypeTab.Dock = System.Windows.Forms.DockStyle.Top;
            this.TaxPayerTypeTab.Location = new System.Drawing.Point(0, 0);
            this.TaxPayerTypeTab.Name = "TaxPayerTypeTab";
            this.TaxPayerTypeTab.SharedControlsPage = this._pageDeclarations;
            this.TaxPayerTypeTab.Size = new System.Drawing.Size(607, 226);
            this.TaxPayerTypeTab.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.TaxPayerTypeTab.TabIndex = 0;
            ultraTab9.TabPage = this.ULTab;
            ultraTab9.Text = "ULTab";
            ultraTab10.TabPage = this.FLTab;
            ultraTab10.Text = "FLTab";
            this.TaxPayerTypeTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab9,
            ultraTab10});
            // 
            // _pageDeclarations
            // 
            this._pageDeclarations.Location = new System.Drawing.Point(-10000, -10000);
            this._pageDeclarations.Name = "_pageDeclarations";
            this._pageDeclarations.Size = new System.Drawing.Size(607, 226);
            // 
            // ULTab
            // 
            this.ULTab.Controls.Add(ULlabelOKVED);
            this.ULTab.Controls.Add(this.ULOkvedValueEdit);
            this.ULTab.Controls.Add(this.ULAddressValueEdit);
            this.ULTab.Controls.Add(ULAddressCaptionLabel);
            this.ULTab.Controls.Add(this.ULDeregisterDataValueLabel);
            this.ULTab.Controls.Add(ULDeregisterDataCaptionLabel);
            this.ULTab.Controls.Add(this.ULAccountDataValueLabel);
            this.ULTab.Controls.Add(ULAccountDataCaptionLabel);
            this.ULTab.Controls.Add(this.ULRegistrationDataValueLabel);
            this.ULTab.Controls.Add(ULRegistrationDataCaptionLabel);
            this.ULTab.Controls.Add(this.ULCapitalValueLabel);
            this.ULTab.Controls.Add(ULCapitalCaptionLabel);
            this.ULTab.Controls.Add(this.ULKPPValueLabel);
            this.ULTab.Controls.Add(ULKPPCaptionLabel);
            this.ULTab.Controls.Add(this.ULINNValueLabel);
            this.ULTab.Controls.Add(ULINNCaptionLabel);
            this.ULTab.Controls.Add(this.ULSizeLabel);
            this.ULTab.Controls.Add(this.ULInspectionValueLabel);
            this.ULTab.Controls.Add(ULInspectionCaptionLabel);
            this.ULTab.Location = new System.Drawing.Point(0, 0);
            this.ULTab.Name = "ULTab";
            this.ULTab.Size = new System.Drawing.Size(607, 226);
            // 
            // ULlabelOKVED
            // 
            appearance29.TextHAlignAsString = "Left";
            appearance29.TextVAlignAsString = "Middle";
            ULlabelOKVED.Appearance = appearance29;
            ULlabelOKVED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULlabelOKVED.Location = new System.Drawing.Point(14, 108);
            ULlabelOKVED.Name = "ULlabelOKVED";
            ULlabelOKVED.Padding = new System.Drawing.Size(2, 0);
            ULlabelOKVED.Size = new System.Drawing.Size(70, 12);
            ULlabelOKVED.TabIndex = 18;
            ULlabelOKVED.Text = "ОКВЭД:";
            // 
            // ULOkvedValueEdit
            // 
            this.ULOkvedValueEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ULOkvedValueEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULOkvedValueEdit.Location = new System.Drawing.Point(14, 124);
            this.ULOkvedValueEdit.Multiline = true;
            this.ULOkvedValueEdit.Name = "ULOkvedValueEdit";
            this.ULOkvedValueEdit.ReadOnly = true;
            this.ULOkvedValueEdit.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.ULOkvedValueEdit.Size = new System.Drawing.Size(582, 32);
            this.ULOkvedValueEdit.TabIndex = 17;
            // 
            // ULAddressValueEdit
            // 
            this.ULAddressValueEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ULAddressValueEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULAddressValueEdit.Location = new System.Drawing.Point(14, 179);
            this.ULAddressValueEdit.Multiline = true;
            this.ULAddressValueEdit.Name = "ULAddressValueEdit";
            this.ULAddressValueEdit.ReadOnly = true;
            this.ULAddressValueEdit.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.ULAddressValueEdit.Size = new System.Drawing.Size(582, 37);
            this.ULAddressValueEdit.TabIndex = 16;
            // 
            // ULAddressCaptionLabel
            // 
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Middle";
            ULAddressCaptionLabel.Appearance = appearance30;
            ULAddressCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULAddressCaptionLabel.Location = new System.Drawing.Point(14, 162);
            ULAddressCaptionLabel.Name = "ULAddressCaptionLabel";
            ULAddressCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULAddressCaptionLabel.Size = new System.Drawing.Size(126, 12);
            ULAddressCaptionLabel.TabIndex = 15;
            ULAddressCaptionLabel.Text = "Юридический Адрес:";
            // 
            // ULDeregisterDataValueLabel
            // 
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Middle";
            this.ULDeregisterDataValueLabel.Appearance = appearance31;
            this.ULDeregisterDataValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULDeregisterDataValueLabel.Location = new System.Drawing.Point(507, 85);
            this.ULDeregisterDataValueLabel.Name = "ULDeregisterDataValueLabel";
            this.ULDeregisterDataValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULDeregisterDataValueLabel.Size = new System.Drawing.Size(81, 12);
            this.ULDeregisterDataValueLabel.TabIndex = 14;
            this.ULDeregisterDataValueLabel.Text = "01.02.2010";
            // 
            // ULDeregisterDataCaptionLabel
            // 
            appearance32.TextHAlignAsString = "Right";
            appearance32.TextVAlignAsString = "Middle";
            ULDeregisterDataCaptionLabel.Appearance = appearance32;
            ULDeregisterDataCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULDeregisterDataCaptionLabel.Location = new System.Drawing.Point(368, 81);
            ULDeregisterDataCaptionLabel.Name = "ULDeregisterDataCaptionLabel";
            ULDeregisterDataCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULDeregisterDataCaptionLabel.Size = new System.Drawing.Size(122, 20);
            ULDeregisterDataCaptionLabel.TabIndex = 13;
            ULDeregisterDataCaptionLabel.Text = "Дата снятия с учета:";
            // 
            // ULAccountDataValueLabel
            // 
            appearance33.TextHAlignAsString = "Left";
            appearance33.TextVAlignAsString = "Middle";
            this.ULAccountDataValueLabel.Appearance = appearance33;
            this.ULAccountDataValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULAccountDataValueLabel.Location = new System.Drawing.Point(507, 60);
            this.ULAccountDataValueLabel.Name = "ULAccountDataValueLabel";
            this.ULAccountDataValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULAccountDataValueLabel.Size = new System.Drawing.Size(81, 12);
            this.ULAccountDataValueLabel.TabIndex = 10;
            this.ULAccountDataValueLabel.Text = "12.02.2010";
            // 
            // ULAccountDataCaptionLabel
            // 
            appearance34.TextHAlignAsString = "Right";
            appearance34.TextVAlignAsString = "Middle";
            ULAccountDataCaptionLabel.Appearance = appearance34;
            ULAccountDataCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULAccountDataCaptionLabel.Location = new System.Drawing.Point(334, 60);
            ULAccountDataCaptionLabel.Name = "ULAccountDataCaptionLabel";
            ULAccountDataCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULAccountDataCaptionLabel.Size = new System.Drawing.Size(156, 12);
            ULAccountDataCaptionLabel.TabIndex = 9;
            ULAccountDataCaptionLabel.Text = "Дата постановки на учет:";
            // 
            // ULRegistrationDataValueLabel
            // 
            appearance35.TextHAlignAsString = "Left";
            appearance35.TextVAlignAsString = "Middle";
            this.ULRegistrationDataValueLabel.Appearance = appearance35;
            this.ULRegistrationDataValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULRegistrationDataValueLabel.Location = new System.Drawing.Point(507, 35);
            this.ULRegistrationDataValueLabel.Name = "ULRegistrationDataValueLabel";
            this.ULRegistrationDataValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULRegistrationDataValueLabel.Size = new System.Drawing.Size(81, 12);
            this.ULRegistrationDataValueLabel.TabIndex = 6;
            this.ULRegistrationDataValueLabel.Text = "01.02.2010";
            // 
            // ULRegistrationDataCaptionLabel
            // 
            appearance36.TextHAlignAsString = "Right";
            appearance36.TextVAlignAsString = "Middle";
            ULRegistrationDataCaptionLabel.Appearance = appearance36;
            ULRegistrationDataCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULRegistrationDataCaptionLabel.Location = new System.Drawing.Point(378, 35);
            ULRegistrationDataCaptionLabel.Name = "ULRegistrationDataCaptionLabel";
            ULRegistrationDataCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULRegistrationDataCaptionLabel.Size = new System.Drawing.Size(112, 12);
            ULRegistrationDataCaptionLabel.TabIndex = 5;
            ULRegistrationDataCaptionLabel.Text = "Дата регистрации:";
            // 
            // ULCapitalValueLabel
            // 
            appearance37.TextHAlignAsString = "Left";
            appearance37.TextVAlignAsString = "Middle";
            this.ULCapitalValueLabel.Appearance = appearance37;
            this.ULCapitalValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULCapitalValueLabel.Location = new System.Drawing.Point(158, 85);
            this.ULCapitalValueLabel.Name = "ULCapitalValueLabel";
            this.ULCapitalValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULCapitalValueLabel.Size = new System.Drawing.Size(132, 12);
            this.ULCapitalValueLabel.TabIndex = 12;
            this.ULCapitalValueLabel.Text = "1 000 000 000,00";
            // 
            // ULCapitalCaptionLabel
            // 
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Middle";
            ULCapitalCaptionLabel.Appearance = appearance38;
            ULCapitalCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULCapitalCaptionLabel.Location = new System.Drawing.Point(14, 85);
            ULCapitalCaptionLabel.Name = "ULCapitalCaptionLabel";
            ULCapitalCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULCapitalCaptionLabel.Size = new System.Drawing.Size(147, 12);
            ULCapitalCaptionLabel.TabIndex = 11;
            ULCapitalCaptionLabel.Text = "Уставный капитал (в руб.):";
            // 
            // ULKPPValueLabel
            // 
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Middle";
            this.ULKPPValueLabel.Appearance = appearance39;
            this.ULKPPValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULKPPValueLabel.Location = new System.Drawing.Point(53, 60);
            this.ULKPPValueLabel.Name = "ULKPPValueLabel";
            this.ULKPPValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULKPPValueLabel.Size = new System.Drawing.Size(114, 12);
            this.ULKPPValueLabel.TabIndex = 8;
            this.ULKPPValueLabel.Text = "123456789";
            // 
            // ULKPPCaptionLabel
            // 
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Middle";
            ULKPPCaptionLabel.Appearance = appearance40;
            ULKPPCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULKPPCaptionLabel.Location = new System.Drawing.Point(14, 60);
            ULKPPCaptionLabel.Name = "ULKPPCaptionLabel";
            ULKPPCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULKPPCaptionLabel.Size = new System.Drawing.Size(39, 12);
            ULKPPCaptionLabel.TabIndex = 7;
            ULKPPCaptionLabel.Text = "КПП:";
            // 
            // ULINNValueLabel
            // 
            appearance41.TextHAlignAsString = "Left";
            appearance41.TextVAlignAsString = "Middle";
            this.ULINNValueLabel.Appearance = appearance41;
            this.ULINNValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULINNValueLabel.Location = new System.Drawing.Point(53, 35);
            this.ULINNValueLabel.Name = "ULINNValueLabel";
            this.ULINNValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULINNValueLabel.Size = new System.Drawing.Size(114, 12);
            this.ULINNValueLabel.TabIndex = 4;
            this.ULINNValueLabel.Text = "1234567890";
            // 
            // ULINNCaptionLabel
            // 
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            ULINNCaptionLabel.Appearance = appearance16;
            ULINNCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULINNCaptionLabel.Location = new System.Drawing.Point(14, 35);
            ULINNCaptionLabel.Name = "ULINNCaptionLabel";
            ULINNCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULINNCaptionLabel.Size = new System.Drawing.Size(39, 12);
            ULINNCaptionLabel.TabIndex = 3;
            ULINNCaptionLabel.Text = "ИНН:";
            // 
            // ULSizeLabel
            // 
            this.ULSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.TextHAlignAsString = "Right";
            appearance43.TextVAlignAsString = "Middle";
            this.ULSizeLabel.Appearance = appearance43;
            this.ULSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULSizeLabel.Location = new System.Drawing.Point(779, 10);
            this.ULSizeLabel.Name = "ULSizeLabel";
            this.ULSizeLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULSizeLabel.Size = new System.Drawing.Size(17, 12);
            this.ULSizeLabel.TabIndex = 2;
            this.ULSizeLabel.Text = "Крупнейший";
            // 
            // ULInspectionValueLabel
            // 
            appearance44.TextHAlignAsString = "Left";
            appearance44.TextVAlignAsString = "Middle";
            this.ULInspectionValueLabel.Appearance = appearance44;
            this.ULInspectionValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULInspectionValueLabel.Location = new System.Drawing.Point(81, 10);
            this.ULInspectionValueLabel.Name = "ULInspectionValueLabel";
            this.ULInspectionValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.ULInspectionValueLabel.Size = new System.Drawing.Size(707, 12);
            this.ULInspectionValueLabel.TabIndex = 1;
            this.ULInspectionValueLabel.Text = "7703 - Инспекция Федеральной налогоывой службы № 3 по г.Москва";
            this.ULInspectionValueLabel.WrapText = false;
            // 
            // ULInspectionCaptionLabel
            // 
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Middle";
            ULInspectionCaptionLabel.Appearance = appearance45;
            ULInspectionCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ULInspectionCaptionLabel.Location = new System.Drawing.Point(14, 10);
            ULInspectionCaptionLabel.Name = "ULInspectionCaptionLabel";
            ULInspectionCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            ULInspectionCaptionLabel.Size = new System.Drawing.Size(70, 12);
            ULInspectionCaptionLabel.TabIndex = 0;
            ULInspectionCaptionLabel.Text = "Инспекция:";
            // 
            // FLTab
            // 
            this.FLTab.Controls.Add(FLlabelOKVED);
            this.FLTab.Controls.Add(this.FLOkvedValueEdit);
            this.FLTab.Controls.Add(this.FLAddressValueEdit);
            this.FLTab.Controls.Add(FLAddressCaptionLabel);
            this.FLTab.Controls.Add(this.FLDeregistrationDateValueLabel);
            this.FLTab.Controls.Add(FLDeregistrationDateCaptionLabel);
            this.FLTab.Controls.Add(this.FLRegistrationDateValueLabel);
            this.FLTab.Controls.Add(FLRegistrationDateCaptionLabel);
            this.FLTab.Controls.Add(this.FLINNValueLabel);
            this.FLTab.Controls.Add(FLINNCaptionLabel);
            this.FLTab.Controls.Add(this.FLInspectionValueLabel);
            this.FLTab.Controls.Add(FLInspectionCaptionLabel);
            this.FLTab.Location = new System.Drawing.Point(-10000, -10000);
            this.FLTab.Name = "FLTab";
            this.FLTab.Size = new System.Drawing.Size(607, 226);
            // 
            // FLlabelOKVED
            // 
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            FLlabelOKVED.Appearance = appearance46;
            FLlabelOKVED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLlabelOKVED.Location = new System.Drawing.Point(13, 73);
            FLlabelOKVED.Name = "FLlabelOKVED";
            FLlabelOKVED.Padding = new System.Drawing.Size(2, 0);
            FLlabelOKVED.Size = new System.Drawing.Size(166, 17);
            FLlabelOKVED.TabIndex = 11;
            FLlabelOKVED.Text = "ОКВЭД:";
            // 
            // FLOkvedValueEdit
            // 
            this.FLOkvedValueEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FLOkvedValueEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLOkvedValueEdit.Location = new System.Drawing.Point(13, 96);
            this.FLOkvedValueEdit.Multiline = true;
            this.FLOkvedValueEdit.Name = "FLOkvedValueEdit";
            this.FLOkvedValueEdit.ReadOnly = true;
            this.FLOkvedValueEdit.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.FLOkvedValueEdit.Size = new System.Drawing.Size(599, 31);
            this.FLOkvedValueEdit.TabIndex = 10;
            // 
            // FLAddressValueEdit
            // 
            this.FLAddressValueEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FLAddressValueEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLAddressValueEdit.Location = new System.Drawing.Point(12, 154);
            this.FLAddressValueEdit.Multiline = true;
            this.FLAddressValueEdit.Name = "FLAddressValueEdit";
            this.FLAddressValueEdit.ReadOnly = true;
            this.FLAddressValueEdit.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.FLAddressValueEdit.Size = new System.Drawing.Size(599, 66);
            this.FLAddressValueEdit.TabIndex = 9;
            // 
            // FLAddressCaptionLabel
            // 
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            FLAddressCaptionLabel.Appearance = appearance47;
            FLAddressCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLAddressCaptionLabel.Location = new System.Drawing.Point(13, 132);
            FLAddressCaptionLabel.Name = "FLAddressCaptionLabel";
            FLAddressCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            FLAddressCaptionLabel.Size = new System.Drawing.Size(166, 17);
            FLAddressCaptionLabel.TabIndex = 8;
            FLAddressCaptionLabel.Text = "Адрес места жительства ИП:";
            // 
            // FLDeregistrationDateValueLabel
            // 
            appearance48.TextHAlignAsString = "Left";
            appearance48.TextVAlignAsString = "Middle";
            this.FLDeregistrationDateValueLabel.Appearance = appearance48;
            this.FLDeregistrationDateValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLDeregistrationDateValueLabel.Location = new System.Drawing.Point(482, 76);
            this.FLDeregistrationDateValueLabel.Name = "FLDeregistrationDateValueLabel";
            this.FLDeregistrationDateValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.FLDeregistrationDateValueLabel.Size = new System.Drawing.Size(81, 12);
            this.FLDeregistrationDateValueLabel.TabIndex = 7;
            this.FLDeregistrationDateValueLabel.Text = "12.02.2010";
            // 
            // FLDeregistrationDateCaptionLabel
            // 
            appearance49.TextHAlignAsString = "Right";
            appearance49.TextVAlignAsString = "Middle";
            FLDeregistrationDateCaptionLabel.Appearance = appearance49;
            FLDeregistrationDateCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLDeregistrationDateCaptionLabel.Location = new System.Drawing.Point(351, 73);
            FLDeregistrationDateCaptionLabel.Name = "FLDeregistrationDateCaptionLabel";
            FLDeregistrationDateCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            FLDeregistrationDateCaptionLabel.Size = new System.Drawing.Size(125, 17);
            FLDeregistrationDateCaptionLabel.TabIndex = 6;
            FLDeregistrationDateCaptionLabel.Text = "Дата снятия с учета:";
            // 
            // FLRegistrationDateValueLabel
            // 
            appearance50.TextHAlignAsString = "Left";
            appearance50.TextVAlignAsString = "Middle";
            this.FLRegistrationDateValueLabel.Appearance = appearance50;
            this.FLRegistrationDateValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLRegistrationDateValueLabel.Location = new System.Drawing.Point(481, 48);
            this.FLRegistrationDateValueLabel.Name = "FLRegistrationDateValueLabel";
            this.FLRegistrationDateValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.FLRegistrationDateValueLabel.Size = new System.Drawing.Size(81, 12);
            this.FLRegistrationDateValueLabel.TabIndex = 5;
            this.FLRegistrationDateValueLabel.Text = "12.02.2010";
            // 
            // FLRegistrationDateCaptionLabel
            // 
            appearance51.TextHAlignAsString = "Right";
            appearance51.TextVAlignAsString = "Middle";
            FLRegistrationDateCaptionLabel.Appearance = appearance51;
            FLRegistrationDateCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLRegistrationDateCaptionLabel.Location = new System.Drawing.Point(242, 45);
            FLRegistrationDateCaptionLabel.Name = "FLRegistrationDateCaptionLabel";
            FLRegistrationDateCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            FLRegistrationDateCaptionLabel.Size = new System.Drawing.Size(234, 17);
            FLRegistrationDateCaptionLabel.TabIndex = 4;
            FLRegistrationDateCaptionLabel.Text = "Дата постановки на учет в качестве ИП:";
            // 
            // FLINNValueLabel
            // 
            appearance52.TextHAlignAsString = "Left";
            appearance52.TextVAlignAsString = "Middle";
            this.FLINNValueLabel.Appearance = appearance52;
            this.FLINNValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLINNValueLabel.Location = new System.Drawing.Point(55, 47);
            this.FLINNValueLabel.Name = "FLINNValueLabel";
            this.FLINNValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.FLINNValueLabel.Size = new System.Drawing.Size(114, 12);
            this.FLINNValueLabel.TabIndex = 3;
            this.FLINNValueLabel.Text = "123456789012";
            // 
            // FLINNCaptionLabel
            // 
            appearance53.TextHAlignAsString = "Left";
            appearance53.TextVAlignAsString = "Middle";
            FLINNCaptionLabel.Appearance = appearance53;
            FLINNCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLINNCaptionLabel.Location = new System.Drawing.Point(13, 45);
            FLINNCaptionLabel.Name = "FLINNCaptionLabel";
            FLINNCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            FLINNCaptionLabel.Size = new System.Drawing.Size(48, 17);
            FLINNCaptionLabel.TabIndex = 2;
            FLINNCaptionLabel.Text = "ИНН:";
            // 
            // FLInspectionValueLabel
            // 
            appearance54.TextHAlignAsString = "Left";
            appearance54.TextVAlignAsString = "Middle";
            this.FLInspectionValueLabel.Appearance = appearance54;
            this.FLInspectionValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLInspectionValueLabel.Location = new System.Drawing.Point(91, 16);
            this.FLInspectionValueLabel.Name = "FLInspectionValueLabel";
            this.FLInspectionValueLabel.Padding = new System.Drawing.Size(2, 0);
            this.FLInspectionValueLabel.Size = new System.Drawing.Size(718, 12);
            this.FLInspectionValueLabel.TabIndex = 1;
            this.FLInspectionValueLabel.Text = "7703 - Инспекция Федеральной налогоывой службы № 3 по г.Москва";
            this.FLInspectionValueLabel.WrapText = false;
            // 
            // FLInspectionCaptionLabel
            // 
            appearance55.TextHAlignAsString = "Left";
            appearance55.TextVAlignAsString = "Middle";
            FLInspectionCaptionLabel.Appearance = appearance55;
            FLInspectionCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            FLInspectionCaptionLabel.Location = new System.Drawing.Point(13, 10);
            FLInspectionCaptionLabel.Name = "FLInspectionCaptionLabel";
            FLInspectionCaptionLabel.Padding = new System.Drawing.Size(2, 0);
            FLInspectionCaptionLabel.Size = new System.Drawing.Size(72, 21);
            FLInspectionCaptionLabel.TabIndex = 0;
            FLInspectionCaptionLabel.Text = "Инспекция:";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            ultraExpandableGroupBoxPanel1.Controls.Add(this.gridControl1);
            ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(0, 0);
            ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(200, 100);
            ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AddVirtualCheckColumn = false;
            this.gridControl1.AggregatePanelVisible = true;
            this.gridControl1.AllowMultiGrouping = true;
            this.gridControl1.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridControl1.AllowSaveFilterSettings = false;
            this.gridControl1.BackColor = System.Drawing.Color.Transparent;
            this.gridControl1.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridControl1.DefaultPageSize = "";
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.ExportExcelCancelVisible = false;
            this.gridControl1.ExportExcelVisible = false;
            this.gridControl1.FilterResetVisible = false;
            this.gridControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControl1.FooterVisible = true;
            this.gridControl1.GridContextMenuStrip = null;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.PanelExportExcelStateVisible = false;
            this.gridControl1.PanelLoadingVisible = false;
            this.gridControl1.PanelPagesVisible = true;
            this.gridControl1.RowDoubleClicked = null;
            this.gridControl1.Setup = null;
            this.gridControl1.Size = new System.Drawing.Size(200, 100);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.Title = "Банковские счета";
            // 
            // ultraLabel1
            // 
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            ultraLabel1.Appearance = appearance12;
            ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel1.Location = new System.Drawing.Point(14, 108);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Padding = new System.Drawing.Size(2, 0);
            ultraLabel1.Size = new System.Drawing.Size(70, 12);
            ultraLabel1.TabIndex = 18;
            ultraLabel1.Text = "ОКВЭД:";
            // 
            // ultraLabel2
            // 
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Middle";
            ultraLabel2.Appearance = appearance27;
            ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel2.Location = new System.Drawing.Point(14, 162);
            ultraLabel2.Name = "ultraLabel2";
            ultraLabel2.Padding = new System.Drawing.Size(2, 0);
            ultraLabel2.Size = new System.Drawing.Size(126, 12);
            ultraLabel2.TabIndex = 15;
            ultraLabel2.Text = "Юридический Адрес:";
            // 
            // ultraLabel4
            // 
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            ultraLabel4.Appearance = appearance9;
            ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel4.Location = new System.Drawing.Point(368, 81);
            ultraLabel4.Name = "ultraLabel4";
            ultraLabel4.Padding = new System.Drawing.Size(2, 0);
            ultraLabel4.Size = new System.Drawing.Size(122, 20);
            ultraLabel4.TabIndex = 13;
            ultraLabel4.Text = "Дата снятия с учета:";
            // 
            // ultraLabel6
            // 
            appearance1.TextHAlignAsString = "Right";
            appearance1.TextVAlignAsString = "Middle";
            ultraLabel6.Appearance = appearance1;
            ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel6.Location = new System.Drawing.Point(334, 60);
            ultraLabel6.Name = "ultraLabel6";
            ultraLabel6.Padding = new System.Drawing.Size(2, 0);
            ultraLabel6.Size = new System.Drawing.Size(156, 12);
            ultraLabel6.TabIndex = 9;
            ultraLabel6.Text = "Дата постановки на учет:";
            // 
            // ultraLabel8
            // 
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            ultraLabel8.Appearance = appearance7;
            ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel8.Location = new System.Drawing.Point(378, 35);
            ultraLabel8.Name = "ultraLabel8";
            ultraLabel8.Padding = new System.Drawing.Size(2, 0);
            ultraLabel8.Size = new System.Drawing.Size(112, 12);
            ultraLabel8.TabIndex = 5;
            ultraLabel8.Text = "Дата регистрации:";
            // 
            // ultraLabel10
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            ultraLabel10.Appearance = appearance15;
            ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel10.Location = new System.Drawing.Point(14, 85);
            ultraLabel10.Name = "ultraLabel10";
            ultraLabel10.Padding = new System.Drawing.Size(2, 0);
            ultraLabel10.Size = new System.Drawing.Size(147, 12);
            ultraLabel10.TabIndex = 11;
            ultraLabel10.Text = "Уставный капитал (в руб.):";
            // 
            // ultraLabel12
            // 
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            ultraLabel12.Appearance = appearance10;
            ultraLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel12.Location = new System.Drawing.Point(14, 60);
            ultraLabel12.Name = "ultraLabel12";
            ultraLabel12.Padding = new System.Drawing.Size(2, 0);
            ultraLabel12.Size = new System.Drawing.Size(39, 12);
            ultraLabel12.TabIndex = 7;
            ultraLabel12.Text = "КПП:";
            // 
            // ultraLabel14
            // 
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            ultraLabel14.Appearance = appearance13;
            ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel14.Location = new System.Drawing.Point(14, 35);
            ultraLabel14.Name = "ultraLabel14";
            ultraLabel14.Padding = new System.Drawing.Size(2, 0);
            ultraLabel14.Size = new System.Drawing.Size(39, 12);
            ultraLabel14.TabIndex = 3;
            ultraLabel14.Text = "ИНН:";
            // 
            // ultraLabel17
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            ultraLabel17.Appearance = appearance5;
            ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ultraLabel17.Location = new System.Drawing.Point(14, 10);
            ultraLabel17.Name = "ultraLabel17";
            ultraLabel17.Padding = new System.Drawing.Size(2, 0);
            ultraLabel17.Size = new System.Drawing.Size(70, 12);
            ultraLabel17.TabIndex = 0;
            ultraLabel17.Text = "Инспекция:";
            // 
            // ReorgSonoLabel
            // 
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Middle";
            ReorgSonoLabel.Appearance = appearance21;
            ReorgSonoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ReorgSonoLabel.Location = new System.Drawing.Point(14, 39);
            ReorgSonoLabel.Name = "ReorgSonoLabel";
            ReorgSonoLabel.Padding = new System.Drawing.Size(2, 0);
            ReorgSonoLabel.Size = new System.Drawing.Size(91, 12);
            ReorgSonoLabel.TabIndex = 8;
            ReorgSonoLabel.Text = "Инспекция:";
            // 
            // ReorgInnKppLabel
            // 
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Middle";
            ReorgInnKppLabel.Appearance = appearance25;
            ReorgInnKppLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ReorgInnKppLabel.Location = new System.Drawing.Point(14, 21);
            ReorgInnKppLabel.Name = "ReorgInnKppLabel";
            ReorgInnKppLabel.Padding = new System.Drawing.Size(2, 0);
            ReorgInnKppLabel.Size = new System.Drawing.Size(91, 12);
            ReorgInnKppLabel.TabIndex = 6;
            ReorgInnKppLabel.Text = "ИНН / КПП:";
            // 
            // ReorgNameLabel
            // 
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Middle";
            ReorgNameLabel.Appearance = appearance26;
            ReorgNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ReorgNameLabel.Location = new System.Drawing.Point(14, 3);
            ReorgNameLabel.Name = "ReorgNameLabel";
            ReorgNameLabel.Padding = new System.Drawing.Size(2, 0);
            ReorgNameLabel.Size = new System.Drawing.Size(91, 12);
            ReorgNameLabel.TabIndex = 4;
            ReorgNameLabel.Text = "Наименование:";
            // 
            // _tabControlDeclarations
            // 
            this._tabControlDeclarations.Controls.Add(this._declarationGrid);
            this._tabControlDeclarations.Location = new System.Drawing.Point(1, 23);
            this._tabControlDeclarations.Name = "_tabControlDeclarations";
            this._tabControlDeclarations.Size = new System.Drawing.Size(609, 165);
            // 
            // _declarationGrid
            // 
            this._declarationGrid.AggregatePanelVisible = false;
            this._declarationGrid.AllowFilterReset = false;
            this._declarationGrid.AllowMultiGrouping = true;
            this._declarationGrid.AllowResetSettings = false;
            this._declarationGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._declarationGrid.Grid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            this._declarationGrid.BackColor = System.Drawing.Color.Transparent;
            this._declarationGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._declarationGrid.ColumnVisibilitySetupButtonVisible = false;
            this._declarationGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declarationGrid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._declarationGrid.ExportExcelCancelVisible = false;
            this._declarationGrid.ExportExcelHint = "Экспорт в MS Excel";
            this._declarationGrid.ExportExcelVisible = false;
            this._declarationGrid.FilterResetVisible = false;
            this._declarationGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._declarationGrid.FooterVisible = true;
            this._declarationGrid.GridContextMenuStrip = this.declContextMenu;
            this._declarationGrid.Location = new System.Drawing.Point(0, 0);
            this._declarationGrid.Name = "_declarationGrid";
            this._declarationGrid.PanelExportExcelStateVisible = false;
            this._declarationGrid.PanelLoadingVisible = false;
            this._declarationGrid.PanelPagesVisible = true;
            this._declarationGrid.RowDoubleClicked = null;
            this._declarationGrid.Size = new System.Drawing.Size(609, 165);
            this._declarationGrid.TabIndex = 3;
            this._declarationGrid.Title = "";
            // 
            // declContextMenu
            // 
            this.declContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenDeclCard});
            this.declContextMenu.Name = "declContextMenu";
            this.declContextMenu.Size = new System.Drawing.Size(240, 26);
            this.declContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.OnDeclarationContextMenuOpening);
            // 
            // cmiOpenDeclCard
            // 
            this.cmiOpenDeclCard.Name = "cmiOpenDeclCard";
            this.cmiOpenDeclCard.Size = new System.Drawing.Size(239, 22);
            this.cmiOpenDeclCard.Text = "Открыть декларацию/журнал";
            this.cmiOpenDeclCard.Click += new System.EventHandler(this.OnOpenDeclarationClick);
            // 
            // _tabControlAccounts
            // 
            this._tabControlAccounts.Controls.Add(this._bankAccountsGrid);
            this._tabControlAccounts.Location = new System.Drawing.Point(-10000, -10000);
            this._tabControlAccounts.Name = "_tabControlAccounts";
            this._tabControlAccounts.Size = new System.Drawing.Size(609, 165);
            // 
            // _bankAccountsGrid
            // 
            this._bankAccountsGrid.AggregatePanelVisible = false;
            this._bankAccountsGrid.AllowFilterReset = false;
            this._bankAccountsGrid.AllowMultiGrouping = true;
            this._bankAccountsGrid.AllowResetSettings = false;
            this._bankAccountsGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._bankAccountsGrid.BackColor = System.Drawing.Color.Transparent;
            this._bankAccountsGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._bankAccountsGrid.ColumnVisibilitySetupButtonVisible = false;
            this._bankAccountsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._bankAccountsGrid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._bankAccountsGrid.ExportExcelCancelVisible = false;
            this._bankAccountsGrid.ExportExcelHint = "Экспорт в MS Excel";
            this._bankAccountsGrid.ExportExcelVisible = false;
            this._bankAccountsGrid.FilterResetVisible = false;
            this._bankAccountsGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bankAccountsGrid.FooterVisible = true;
            this._bankAccountsGrid.GridContextMenuStrip = null;
            this._bankAccountsGrid.Location = new System.Drawing.Point(0, 0);
            this._bankAccountsGrid.Name = "_bankAccountsGrid";
            this._bankAccountsGrid.PanelExportExcelStateVisible = false;
            this._bankAccountsGrid.PanelLoadingVisible = false;
            this._bankAccountsGrid.PanelPagesVisible = true;
            this._bankAccountsGrid.RowDoubleClicked = null;
            this._bankAccountsGrid.Size = new System.Drawing.Size(609, 165);
            this._bankAccountsGrid.TabIndex = 3;
            this._bankAccountsGrid.Title = "";
            // 
            // _tabControlRequests
            // 
            this._tabControlRequests.Controls.Add(this._gridRequests);
            this._tabControlRequests.Location = new System.Drawing.Point(-10000, -10000);
            this._tabControlRequests.Name = "_tabControlRequests";
            this._tabControlRequests.Size = new System.Drawing.Size(609, 165);
            // 
            // _gridRequests
            // 
            this._gridRequests.AggregatePanelVisible = true;
            this._gridRequests.AllowFilterReset = false;
            this._gridRequests.AllowMultiGrouping = true;
            this._gridRequests.AllowResetSettings = false;
            this._gridRequests.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._gridRequests.BackColor = System.Drawing.Color.Transparent;
            this._gridRequests.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._gridRequests.ColumnVisibilitySetupButtonVisible = true;
            this._gridRequests.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridRequests.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._gridRequests.ExportExcelCancelVisible = false;
            this._gridRequests.ExportExcelHint = "Экспорт в MS Excel";
            this._gridRequests.ExportExcelVisible = false;
            this._gridRequests.FilterResetVisible = false;
            this._gridRequests.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gridRequests.FooterVisible = true;
            this._gridRequests.GridContextMenuStrip = null;
            this._gridRequests.Location = new System.Drawing.Point(0, 0);
            this._gridRequests.Name = "_gridRequests";
            this._gridRequests.PanelExportExcelStateVisible = false;
            this._gridRequests.PanelLoadingVisible = false;
            this._gridRequests.PanelPagesVisible = true;
            this._gridRequests.RowDoubleClicked = null;
            this._gridRequests.Size = new System.Drawing.Size(609, 165);
            this._gridRequests.TabIndex = 4;
            this._gridRequests.Title = "";
            // 
            // _labelStatusTitle
            // 
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            _labelStatusTitle.Appearance = appearance14;
            _labelStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            _labelStatusTitle.Location = new System.Drawing.Point(14, 3);
            _labelStatusTitle.Name = "_labelStatusTitle";
            _labelStatusTitle.Padding = new System.Drawing.Size(2, 0);
            _labelStatusTitle.Size = new System.Drawing.Size(163, 12);
            _labelStatusTitle.TabIndex = 4;
            _labelStatusTitle.Text = "Статус налогоплательщика:";
            // 
            // _labelDateTitle
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            _labelDateTitle.Appearance = appearance8;
            _labelDateTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            _labelDateTitle.Location = new System.Drawing.Point(14, 21);
            _labelDateTitle.Name = "_labelDateTitle";
            _labelDateTitle.Padding = new System.Drawing.Size(2, 0);
            _labelDateTitle.Size = new System.Drawing.Size(163, 12);
            _labelDateTitle.TabIndex = 6;
            _labelDateTitle.Text = "Дата установки статуса:";
            // 
            // DetailsBox
            // 
            this.DetailsBox.Controls.Add(_detailsBoxArea);
            this.DetailsBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.DetailsBox.ExpandedSize = new System.Drawing.Size(613, 243);
            this.DetailsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailsBox.Location = new System.Drawing.Point(0, 26);
            this.DetailsBox.Name = "DetailsBox";
            this.DetailsBox.Size = new System.Drawing.Size(613, 243);
            this.DetailsBox.TabIndex = 0;
            this.DetailsBox.TabStop = false;
            this.DetailsBox.Text = "Основные сведения";
            // 
            // HeadPanel
            // 
            // 
            // HeadPanel.ClientArea
            // 
            this.HeadPanel.ClientArea.Controls.Add(this.labelTaxPayerName);
            this.HeadPanel.ClientArea.Controls.Add(this._surIndicator);
            this.HeadPanel.ClientArea.Controls.Add(this.marginPanel);
            this.HeadPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadPanel.Location = new System.Drawing.Point(0, 0);
            this.HeadPanel.Name = "HeadPanel";
            this.HeadPanel.Size = new System.Drawing.Size(613, 26);
            this.HeadPanel.TabIndex = 2;
            // 
            // labelTaxPayerName
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.labelTaxPayerName.Appearance = appearance3;
            this.labelTaxPayerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTaxPayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTaxPayerName.Location = new System.Drawing.Point(25, 0);
            this.labelTaxPayerName.Name = "labelTaxPayerName";
            this.labelTaxPayerName.Size = new System.Drawing.Size(588, 26);
            this.labelTaxPayerName.TabIndex = 1;
            this.labelTaxPayerName.Text = "Василек и Ромашка";
            this.labelTaxPayerName.WrapText = false;
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Dock = System.Windows.Forms.DockStyle.Left;
            this._surIndicator.Location = new System.Drawing.Point(5, 0);
            this._surIndicator.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 0;
            // 
            // marginPanel
            // 
            this.marginPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.marginPanel.Location = new System.Drawing.Point(0, 0);
            this.marginPanel.Name = "marginPanel";
            this.marginPanel.Size = new System.Drawing.Size(5, 26);
            this.marginPanel.TabIndex = 2;
            // 
            // DataLoadProgress
            // 
            this.DataLoadProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DataLoadProgress.Location = new System.Drawing.Point(0, 622);
            this.DataLoadProgress.Name = "DataLoadProgress";
            this.DataLoadProgress.Size = new System.Drawing.Size(613, 23);
            this.DataLoadProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.DataLoadProgress.TabIndex = 3;
            this.DataLoadProgress.Visible = false;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControl1.Size = new System.Drawing.Size(200, 100);
            this.ultraTabControl1.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(196, 77);
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(ultraLabel1);
            this.ultraTabPageControl1.Controls.Add(this.ultraTextEditor1);
            this.ultraTabPageControl1.Controls.Add(this.ultraTextEditor2);
            this.ultraTabPageControl1.Controls.Add(ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl1.Controls.Add(ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel11);
            this.ultraTabPageControl1.Controls.Add(ultraLabel12);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(ultraLabel17);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(862, 226);
            // 
            // ultraTextEditor1
            // 
            this.ultraTextEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTextEditor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor1.Location = new System.Drawing.Point(14, 124);
            this.ultraTextEditor1.Multiline = true;
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.ultraTextEditor1.Size = new System.Drawing.Size(837, 32);
            this.ultraTextEditor1.TabIndex = 17;
            // 
            // ultraTextEditor2
            // 
            this.ultraTextEditor2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTextEditor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor2.Location = new System.Drawing.Point(14, 179);
            this.ultraTextEditor2.Multiline = true;
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.ultraTextEditor2.Size = new System.Drawing.Size(837, 37);
            this.ultraTextEditor2.TabIndex = 16;
            // 
            // ultraLabel3
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance6;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(507, 85);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel3.Size = new System.Drawing.Size(81, 12);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "01.02.2010";
            // 
            // ultraLabel5
            // 
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance22;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(507, 60);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel5.Size = new System.Drawing.Size(81, 12);
            this.ultraLabel5.TabIndex = 10;
            this.ultraLabel5.Text = "12.02.2010";
            // 
            // ultraLabel7
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance19;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(507, 35);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel7.Size = new System.Drawing.Size(81, 12);
            this.ultraLabel7.TabIndex = 6;
            this.ultraLabel7.Text = "01.02.2010";
            // 
            // ultraLabel9
            // 
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance11;
            this.ultraLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel9.Location = new System.Drawing.Point(158, 85);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel9.Size = new System.Drawing.Size(132, 12);
            this.ultraLabel9.TabIndex = 12;
            this.ultraLabel9.Text = "1 000 000 000,00";
            // 
            // ultraLabel11
            // 
            appearance23.TextHAlignAsString = "Left";
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance23;
            this.ultraLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel11.Location = new System.Drawing.Point(53, 60);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel11.Size = new System.Drawing.Size(114, 12);
            this.ultraLabel11.TabIndex = 8;
            this.ultraLabel11.Text = "123456789";
            // 
            // ultraLabel13
            // 
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance24;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(53, 35);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel13.Size = new System.Drawing.Size(114, 12);
            this.ultraLabel13.TabIndex = 4;
            this.ultraLabel13.Text = "1234567890";
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance4;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(779, 10);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel15.Size = new System.Drawing.Size(70, 12);
            this.ultraLabel15.TabIndex = 2;
            this.ultraLabel15.Text = "Крупнейший";
            // 
            // ultraLabel16
            // 
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance20;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(81, 10);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Padding = new System.Drawing.Size(2, 0);
            this.ultraLabel16.Size = new System.Drawing.Size(707, 12);
            this.ultraLabel16.TabIndex = 1;
            this.ultraLabel16.Text = "7703 - Инспекция Федеральной налогоывой службы № 3 по г.Москва";
            this.ultraLabel16.WrapText = false;
            // 
            // ultraExpandableGroupBox1
            // 
            this.ultraExpandableGroupBox1.ExpandedSize = new System.Drawing.Size(0, 0);
            this.ultraExpandableGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraExpandableGroupBox1.Name = "ultraExpandableGroupBox1";
            this.ultraExpandableGroupBox1.Size = new System.Drawing.Size(200, 185);
            this.ultraExpandableGroupBox1.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel3
            // 
            this.ultraExpandableGroupBoxPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel3.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel3.Name = "ultraExpandableGroupBoxPanel3";
            this.ultraExpandableGroupBoxPanel3.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel3.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel4
            // 
            this.ultraExpandableGroupBoxPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel4.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel4.Name = "ultraExpandableGroupBoxPanel4";
            this.ultraExpandableGroupBoxPanel4.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel4.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel5
            // 
            this.ultraExpandableGroupBoxPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel5.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel5.Name = "ultraExpandableGroupBoxPanel5";
            this.ultraExpandableGroupBoxPanel5.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel5.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel6
            // 
            this.ultraExpandableGroupBoxPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel6.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel6.Name = "ultraExpandableGroupBoxPanel6";
            this.ultraExpandableGroupBoxPanel6.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel6.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel7
            // 
            this.ultraExpandableGroupBoxPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel7.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel7.Name = "ultraExpandableGroupBoxPanel7";
            this.ultraExpandableGroupBoxPanel7.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel7.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel8
            // 
            this.ultraExpandableGroupBoxPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel8.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel8.Name = "ultraExpandableGroupBoxPanel8";
            this.ultraExpandableGroupBoxPanel8.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel8.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel9
            // 
            this.ultraExpandableGroupBoxPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel9.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel9.Name = "ultraExpandableGroupBoxPanel9";
            this.ultraExpandableGroupBoxPanel9.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel9.TabIndex = 0;
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._gridTabSharedControl);
            this._tabControl.Controls.Add(this._tabControlDeclarations);
            this._tabControl.Controls.Add(this._tabControlAccounts);
            this._tabControl.Controls.Add(this._tabControlRequests);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._tabControl.Location = new System.Drawing.Point(0, 431);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SharedControlsPage = this._gridTabSharedControl;
            this._tabControl.Size = new System.Drawing.Size(613, 191);
            this._tabControl.TabIndex = 6;
            ultraTab1.Key = "_tabDeclarations";
            ultraTab1.TabPage = this._tabControlDeclarations;
            ultraTab1.Text = "Список деклараций/журналов";
            ultraTab3.Key = "_tabAccounts";
            ultraTab3.TabPage = this._tabControlAccounts;
            ultraTab3.Text = "Банковские счета";
            ultraTab6.Key = "_tabRequests";
            ultraTab6.TabPage = this._tabControlRequests;
            ultraTab6.Text = "История запросов";
            this._tabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3,
            ultraTab6});
            // 
            // _gridTabSharedControl
            // 
            this._gridTabSharedControl.Location = new System.Drawing.Point(-10000, -10000);
            this._gridTabSharedControl.Name = "_gridTabSharedControl";
            this._gridTabSharedControl.Size = new System.Drawing.Size(609, 165);
            // 
            // ultraExpandableGroupBoxPanel10
            // 
            this.ultraExpandableGroupBoxPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel10.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel10.Name = "ultraExpandableGroupBoxPanel10";
            this.ultraExpandableGroupBoxPanel10.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel10.TabIndex = 0;
            // 
            // ReorgBox
            // 
            this.ReorgBox.Controls.Add(this.ultraExpandableGroupBoxPanel2);
            this.ReorgBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReorgBox.Expanded = false;
            this.ReorgBox.ExpandedSize = new System.Drawing.Size(613, 83);
            this.ReorgBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReorgBox.Location = new System.Drawing.Point(0, 269);
            this.ReorgBox.Name = "ReorgBox";
            this.ReorgBox.Size = new System.Drawing.Size(613, 21);
            this.ReorgBox.TabIndex = 7;
            this.ReorgBox.TabStop = false;
            this.ReorgBox.Text = "Сведения о правоприемнике";
            // 
            // ultraExpandableGroupBoxPanel2
            // 
            this.ultraExpandableGroupBoxPanel2.Controls.Add(this.ReorgSono);
            this.ultraExpandableGroupBoxPanel2.Controls.Add(ReorgSonoLabel);
            this.ultraExpandableGroupBoxPanel2.Controls.Add(this.ReorgInnKpp);
            this.ultraExpandableGroupBoxPanel2.Controls.Add(ReorgInnKppLabel);
            this.ultraExpandableGroupBoxPanel2.Controls.Add(this.ReorgName);
            this.ultraExpandableGroupBoxPanel2.Controls.Add(ReorgNameLabel);
            this.ultraExpandableGroupBoxPanel2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraExpandableGroupBoxPanel2.Name = "ultraExpandableGroupBoxPanel2";
            this.ultraExpandableGroupBoxPanel2.Size = new System.Drawing.Size(607, 61);
            this.ultraExpandableGroupBoxPanel2.TabIndex = 0;
            this.ultraExpandableGroupBoxPanel2.Visible = false;
            // 
            // ReorgSono
            // 
            appearance28.TextHAlignAsString = "Left";
            appearance28.TextVAlignAsString = "Middle";
            this.ReorgSono.Appearance = appearance28;
            this.ReorgSono.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReorgSono.Location = new System.Drawing.Point(111, 39);
            this.ReorgSono.Name = "ReorgSono";
            this.ReorgSono.Padding = new System.Drawing.Size(2, 0);
            this.ReorgSono.Size = new System.Drawing.Size(441, 12);
            this.ReorgSono.TabIndex = 9;
            this.ReorgSono.Text = "7705 - ИФНС №5 по г.Москва";
            // 
            // ReorgInnKpp
            // 
            appearance56.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance56.BorderColor = System.Drawing.Color.Transparent;
            appearance56.BorderColor2 = System.Drawing.Color.Transparent;
            appearance56.TextHAlignAsString = "Left";
            appearance56.TextVAlignAsString = "Middle";
            this.ReorgInnKpp.Appearance = appearance56;
            this.ReorgInnKpp.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ReorgInnKpp.Location = new System.Drawing.Point(108, 19);
            this.ReorgInnKpp.Margin = new System.Windows.Forms.Padding(0);
            this.ReorgInnKpp.Name = "ReorgInnKpp";
            this.ReorgInnKpp.Size = new System.Drawing.Size(179, 14);
            this.ReorgInnKpp.TabIndex = 7;
            this.ReorgInnKpp.TabStop = true;
            this.ReorgInnKpp.Value = "<a href=\'#\'>1234567890 / 123456789</a>";
            // 
            // ReorgName
            // 
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.ReorgName.Appearance = appearance57;
            this.ReorgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReorgName.Location = new System.Drawing.Point(111, 3);
            this.ReorgName.Name = "ReorgName";
            this.ReorgName.Padding = new System.Drawing.Size(2, 0);
            this.ReorgName.Size = new System.Drawing.Size(544, 12);
            this.ReorgName.TabIndex = 5;
            this.ReorgName.Text = "Цветочная поляна";
            // 
            // ultraExpandableGroupBoxPanel11
            // 
            this.ultraExpandableGroupBoxPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel11.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel11.Name = "ultraExpandableGroupBoxPanel11";
            this.ultraExpandableGroupBoxPanel11.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel11.TabIndex = 0;
            // 
            // ultraExpandableGroupBoxPanel12
            // 
            this.ultraExpandableGroupBoxPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel12.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel12.Name = "ultraExpandableGroupBoxPanel12";
            this.ultraExpandableGroupBoxPanel12.Size = new System.Drawing.Size(194, 165);
            this.ultraExpandableGroupBoxPanel12.TabIndex = 0;
            // 
            // _groupStatus
            // 
            this._groupStatus.Controls.Add(this._panelStatus);
            this._groupStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupStatus.ExpandedSize = new System.Drawing.Size(613, 135);
            this._groupStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._groupStatus.Location = new System.Drawing.Point(0, 290);
            this._groupStatus.Name = "_groupStatus";
            this._groupStatus.Size = new System.Drawing.Size(613, 135);
            this._groupStatus.TabIndex = 8;
            this._groupStatus.TabStop = false;
            this._groupStatus.Text = "Статус налогоплательщика";
            this._groupStatus.Visible = false;
            // 
            // _panelStatus
            // 
            this._panelStatus.Controls.Add(this._textComment);
            this._panelStatus.Controls.Add(this._labelDateValue);
            this._panelStatus.Controls.Add(_labelDateTitle);
            this._panelStatus.Controls.Add(this._labelStatusValue);
            this._panelStatus.Controls.Add(_labelStatusTitle);
            this._panelStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelStatus.Location = new System.Drawing.Point(3, 19);
            this._panelStatus.Name = "_panelStatus";
            this._panelStatus.Size = new System.Drawing.Size(607, 113);
            this._panelStatus.TabIndex = 0;
            // 
            // _textComment
            // 
            this._textComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._textComment.Location = new System.Drawing.Point(12, 38);
            this._textComment.Multiline = true;
            this._textComment.Name = "_textComment";
            this._textComment.ReadOnly = true;
            this._textComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this._textComment.Size = new System.Drawing.Size(582, 72);
            this._textComment.TabIndex = 17;
            // 
            // _labelDateValue
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this._labelDateValue.Appearance = appearance2;
            this._labelDateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelDateValue.Location = new System.Drawing.Point(167, 21);
            this._labelDateValue.Name = "_labelDateValue";
            this._labelDateValue.Padding = new System.Drawing.Size(2, 0);
            this._labelDateValue.Size = new System.Drawing.Size(163, 12);
            this._labelDateValue.TabIndex = 7;
            this._labelDateValue.Text = "31.12.2016";
            // 
            // _labelStatusValue
            // 
            appearance17.ForeColor = System.Drawing.Color.Red;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this._labelStatusValue.Appearance = appearance17;
            this._labelStatusValue.AutoSize = true;
            this._labelStatusValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelStatusValue.Location = new System.Drawing.Point(167, 3);
            this._labelStatusValue.Name = "_labelStatusValue";
            this._labelStatusValue.Padding = new System.Drawing.Size(2, 0);
            this._labelStatusValue.Size = new System.Drawing.Size(112, 14);
            this._labelStatusValue.TabIndex = 5;
            this._labelStatusValue.Text = "Платёжеспособный";
            // 
            // View
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._groupStatus);
            this.Controls.Add(this.ReorgBox);
            this.Controls.Add(this._tabControl);
            this.Controls.Add(this.DataLoadProgress);
            this.Controls.Add(this.DetailsBox);
            this.Controls.Add(this.HeadPanel);
            this.Name = "View";
            this.Size = new System.Drawing.Size(613, 645);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.OnViewLayout);
            _detailsBoxArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TaxPayerTypeTab)).EndInit();
            this.TaxPayerTypeTab.ResumeLayout(false);
            this.ULTab.ResumeLayout(false);
            this.ULTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ULOkvedValueEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULAddressValueEdit)).EndInit();
            this.FLTab.ResumeLayout(false);
            this.FLTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FLOkvedValueEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FLAddressValueEdit)).EndInit();
            ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this._tabControlDeclarations.ResumeLayout(false);
            this.declContextMenu.ResumeLayout(false);
            this._tabControlAccounts.ResumeLayout(false);
            this._tabControlRequests.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DetailsBox)).EndInit();
            this.DetailsBox.ResumeLayout(false);
            this.HeadPanel.ClientArea.ResumeLayout(false);
            this.HeadPanel.ResumeLayout(false);
            this.marginPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraExpandableGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).EndInit();
            this._tabControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReorgBox)).EndInit();
            this.ReorgBox.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._groupStatus)).EndInit();
            this._groupStatus.ResumeLayout(false);
            this._panelStatus.ResumeLayout(false);
            this._panelStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._textComment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl TaxPayerTypeTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage _pageDeclarations;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ULTab;
        private UltraTextEditor ULAddressValueEdit;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl FLTab;
        private Infragistics.Win.Misc.UltraPanel HeadPanel;
        private Infragistics.Win.Misc.UltraLabel labelTaxPayerName;
        private UltraTextEditor FLAddressValueEdit;
        private System.Windows.Forms.ProgressBar DataLoadProgress;
        private Infragistics.Win.Misc.UltraLabel ULDeregisterDataValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULAccountDataValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULRegistrationDataValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULCapitalValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULKPPValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULINNValueLabel;
        private Infragistics.Win.Misc.UltraLabel ULSizeLabel;
        private Infragistics.Win.Misc.UltraLabel ULInspectionValueLabel;
        private Infragistics.Win.Misc.UltraLabel FLDeregistrationDateValueLabel;
        private Infragistics.Win.Misc.UltraLabel FLRegistrationDateValueLabel;
        private Infragistics.Win.Misc.UltraLabel FLINNValueLabel;
        private Infragistics.Win.Misc.UltraLabel FLInspectionValueLabel;
        private Infragistics.Win.Misc.UltraPanel marginPanel;
        private Infragistics.Win.Misc.UltraExpandableGroupBox DetailsBox;
        private System.Windows.Forms.ContextMenuStrip declContextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclCard;
        private Luxoft.NDS2.Client.UI.Controls.Sur.SurView _surIndicator;
        private UltraTextEditor ULOkvedValueEdit;
        private UltraTextEditor FLOkvedValueEdit;
        private Controls.Grid.V1.GridControl gridControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private UltraTextEditor ultraTextEditor1;
        private UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ultraExpandableGroupBox1;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel3;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel4;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel5;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel6;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel7;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel8;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel9;
        private UltraTabControl _tabControl;
        private UltraTabSharedControlsPage _gridTabSharedControl;
        private UltraTabPageControl _tabControlDeclarations;
        private DataGridView _declarationGrid;
        private UltraTabPageControl _tabControlAccounts;
        private DataGridView _bankAccountsGrid;
        private UltraTabPageControl _tabControlRequests;
        private DataGridView _gridRequests;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel10;
        private UltraExpandableGroupBox ReorgBox;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel2;
        private UltraLabel ReorgSono;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ReorgInnKpp;
        private UltraLabel ReorgName;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel11;
        private UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel12;
        private UltraExpandableGroupBox _groupStatus;
        private UltraExpandableGroupBoxPanel _panelStatus;
        private UltraLabel _labelStatusValue;
        private UltraTextEditor _textComment;
        private UltraLabel _labelDateValue;
    }
}
