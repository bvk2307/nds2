﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog
{
    public class TaxPeriodSummary
    {
        public string Code { get; set; }

        public int MonthOfBegin { get; set; }

        public int MonthOfEnd { get; set; }

        public string Description { get; set; }
    }

    public class TaxPeriod
    {
        public TaxPeriodSummary Period { get; set; }

        public string Year { get; set; }

        public string Description
        {
            get
            {
                return string.Format("{0} {1}", Period.Description, Year);
            }
        }

        public DateTime Begin
        {
            get
            {
                return new DateTime(int.Parse(Year), Period.MonthOfBegin, 1);
            }
        }

        public DateTime End
        {
            get
            {
                return new DateTime(int.Parse(Year), Period.MonthOfEnd, 1).AddMonths(1).AddDays(-1);
            }
        }
    }

    public class TaxPeriodComparer : IEqualityComparer<TaxPeriod>
    {
        public bool Equals(TaxPeriod x, TaxPeriod y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return (x.Year == y.Year && x.Period.MonthOfBegin == y.Period.MonthOfBegin && x.Period.MonthOfEnd == y.Period.MonthOfEnd);
        }

        public int GetHashCode(TaxPeriod period)
        {
            var hCode = int.Parse(period.Year) ^ period.Period.MonthOfBegin ^ period.Period.MonthOfEnd;
            return hCode.GetHashCode();
        }
    }


    public class BankAccountBriefModel
    {
        private readonly BankAccountBrief _source;

        private bool _isChecked;

        public BankAccountBriefModel(BankAccountBrief source)
        {
            _source = source;
        }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                if (_isChecked == value)
                    return;

                _isChecked = value;
                RaiseCheckedChanged();
            }
        }

        [DisplayName(@"Номер счета")]
        [Description(@"Номер счета")]
        public string Account
        {
            get
            {
                return _source.Account;
            }
        }

        [DisplayName(@"Вид счета")]
        [Description(@"Вид счета")]
        public string AccountType
        {
            get
            {
                return _source.AccountType;
            }
        }

        [DisplayName(@"Валюта счета")]
        [Description(@"Валюта счета")]
        public string Currency
        {
            get
            {
                return _source.Currency;
            }
        }

        [DisplayName(@"БИК банка")]
        [Description(@"БИК банка")]
        public string Bik
        {
            get
            {
                return _source.Bik;
            }
        }

        [DisplayName(@"Наименование банка")]
        [Description(@"Наименование банка")]
        public string NameBank
        {
            get
            {
                return _source.NameBank;
            }
        }

        [DisplayName(@"ИНН НП")]
        [Description(@"ИНН налогоплательщика")]
        public string Inn
        {
            get
            {
                return _source.Inn;
            }
        }
        
        [DisplayName(@"КПП НП")]
        [Description(@"КПП налогоплательщика")]
        public string Kpp
        {
            get
            {
                return _source.Kpp;
            }
        }
        
        public DateTime? CloseDate
        {
            get
            {
                return _source.CloseDate;
            }
        }

        private void RaiseCheckedChanged()
        {
            var handler = CheckedChanged;
            if (handler != null)
                handler();
        }

        public event ParameterlessEventHandler CheckedChanged;

        public BankAccountBrief GetBaseData()
        {
            return _source;
        }
    }

    public class BankAccountModel
    {
        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string KppEffective { get; set; }

        public bool IsReorganized { get; set; }

        public bool IsJuridical { get; set; }

        public IEnumerable<TaxPeriod> PeriodList { get; set; }

        private List<BankAccountBriefModel> _accounts;

        public List<BankAccountBriefModel> Accounts
        {
            get
            {
                return _accounts;
            }
            set
            {
                if (Equals(value, _accounts))
                    return;
                if (_accounts != null)
                {
                    foreach (var a in _accounts)
                    {
                        a.CheckedChanged -= RaiseSelectionChanged;
                    }
                }
                _accounts = value;
                foreach (var a in _accounts)
                {
                    a.CheckedChanged += RaiseSelectionChanged;
                }
            }
        }

        public event ParameterlessEventHandler SelectionChanged;

        private void RaiseSelectionChanged()
        {
            var handler = SelectionChanged;
            if (handler != null)
                handler();
        }

        public string UserSid { get; set; }

        public string UserName { get; set; }

        public string SonoCode { get; set; }

        public string SelectedYear { get; set; }

        public string SelectedPeriod { get; set; }
    }

}
