﻿using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog
{
    partial class DialogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layout = new System.Windows.Forms.TableLayoutPanel();
            this._gridAccounts = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this._panelFooter = new System.Windows.Forms.Panel();
            this._btnCancel = new Infragistics.Win.Misc.UltraButton();
            this._btnExecute = new Infragistics.Win.Misc.UltraButton();
            this._panelHeader = new System.Windows.Forms.Panel();
            this._comboPeriodList = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._comboYearList = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._btnSearch = new Infragistics.Win.Misc.UltraButton();
            this.layout.SuspendLayout();
            this._panelFooter.SuspendLayout();
            this._panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboPeriodList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._comboYearList)).BeginInit();
            this.SuspendLayout();
            // 
            // layout
            // 
            this.layout.ColumnCount = 1;
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.layout.Controls.Add(this._gridAccounts, 0, 1);
            this.layout.Controls.Add(this._panelFooter, 0, 2);
            this.layout.Controls.Add(this._panelHeader, 0, 0);
            this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout.Location = new System.Drawing.Point(0, 0);
            this.layout.Name = "layout";
            this.layout.RowCount = 3;
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layout.Size = new System.Drawing.Size(664, 354);
            this.layout.TabIndex = 0;
            // 
            // _gridAccounts
            // 
            this._gridAccounts.AggregatePanelVisible = false;
            this._gridAccounts.AllowFilterReset = false;
            this._gridAccounts.AllowMultiGrouping = false;
            this._gridAccounts.AllowResetSettings = false;
            this._gridAccounts.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._gridAccounts.BackColor = System.Drawing.Color.Transparent;
            this._gridAccounts.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this._gridAccounts.ColumnVisibilitySetupButtonVisible = false;
            this._gridAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridAccounts.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._gridAccounts.ExportExcelCancelVisible = false;
            this._gridAccounts.ExportExcelHint = "Экспорт в MS Excel";
            this._gridAccounts.ExportExcelVisible = false;
            this._gridAccounts.FilterResetVisible = false;
            this._gridAccounts.FooterVisible = true;
            this._gridAccounts.GridContextMenuStrip = null;
            this._gridAccounts.Location = new System.Drawing.Point(3, 53);
            this._gridAccounts.Name = "_gridAccounts";
            this._gridAccounts.PanelExportExcelStateVisible = false;
            this._gridAccounts.PanelLoadingVisible = false;
            this._gridAccounts.PanelPagesVisible = false;
            this._gridAccounts.RowDoubleClicked = null;
            this._gridAccounts.Size = new System.Drawing.Size(658, 248);
            this._gridAccounts.TabIndex = 1;
            this._gridAccounts.Title = "";
            // 
            // _panelFooter
            // 
            this._panelFooter.Controls.Add(this._btnCancel);
            this._panelFooter.Controls.Add(this._btnExecute);
            this._panelFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelFooter.Location = new System.Drawing.Point(3, 307);
            this._panelFooter.Name = "_panelFooter";
            this._panelFooter.Size = new System.Drawing.Size(658, 44);
            this._panelFooter.TabIndex = 1;
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._btnCancel.Location = new System.Drawing.Point(335, 12);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(120, 23);
            this._btnCancel.TabIndex = 1;
            this._btnCancel.Text = "Отменить";
            // 
            // _btnExecute
            // 
            this._btnExecute.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._btnExecute.Location = new System.Drawing.Point(206, 12);
            this._btnExecute.Name = "_btnExecute";
            this._btnExecute.Size = new System.Drawing.Size(120, 23);
            this._btnExecute.TabIndex = 0;
            this._btnExecute.Text = "Сформировать";
            // 
            // _panelHeader
            // 
            this._panelHeader.Controls.Add(this._btnSearch);
            this._panelHeader.Controls.Add(this.label2);
            this._panelHeader.Controls.Add(this._comboYearList);
            this._panelHeader.Controls.Add(this.label1);
            this._panelHeader.Controls.Add(this._comboPeriodList);
            this._panelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelHeader.Location = new System.Drawing.Point(3, 3);
            this._panelHeader.Name = "_panelHeader";
            this._panelHeader.Size = new System.Drawing.Size(658, 44);
            this._panelHeader.TabIndex = 0;
            // 
            // _comboPeriodList
            // 
            this._comboPeriodList.Location = new System.Drawing.Point(206, 12);
            this._comboPeriodList.Name = "_comboPeriodList";
            this._comboPeriodList.Size = new System.Drawing.Size(101, 21);
            this._comboPeriodList.TabIndex = 1;
            this._comboPeriodList.DropDownStyle = DropDownStyle.DropDownList;
            // 
            // _comboYearList
            // 
            this._comboYearList.Location = new System.Drawing.Point(40, 12);
            this._comboYearList.Name = "_comboYearList";
            this._comboYearList.Size = new System.Drawing.Size(101, 21);
            this._comboYearList.TabIndex = 3;
            this._comboYearList.DropDownStyle = DropDownStyle.DropDownList;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Год";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Период";
            // 
            // _btnSearch
            // 
            this._btnSearch.Location = new System.Drawing.Point(325, 11);
            this._btnSearch.Name = "_btnSearch";
            this._btnSearch.Size = new System.Drawing.Size(99, 23);
            this._btnSearch.TabIndex = 5;
            this._btnSearch.Text = "Поиск";
            // 
            // DialogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 354);
            this.Controls.Add(this.layout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(680, 300);
            this.Name = "DialogView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Формирование запроса";
            this.layout.ResumeLayout(false);
            this._panelFooter.ResumeLayout(false);
            this._panelHeader.ResumeLayout(false);
            this._panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboPeriodList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._comboYearList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layout;
        private System.Windows.Forms.Panel _panelFooter;
        private Infragistics.Win.Misc.UltraButton _btnCancel;
        private Infragistics.Win.Misc.UltraButton _btnExecute;
        private System.Windows.Forms.Panel _panelHeader;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboPeriodList;
        private Controls.Grid.V2.DataGridView _gridAccounts;
        private Infragistics.Win.Misc.UltraButton _btnSearch;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboYearList;
        private System.Windows.Forms.Label label1;
    }
}