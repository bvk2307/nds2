﻿using System;
using System.Windows.Forms;
using CommonComponents.Utils;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.TaxPayer.Details;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog
{
    public class Presenter
    {
        private readonly IDialogView _view;

        private readonly IBankAccountServiceProvider _service;

        private readonly ISettingsProvider _settingsProvider;

        private readonly BankAccountModel _model;

        public Presenter(IDialogView view, IBankAccountServiceProvider service, ISettingsProvider settingsProvider, BankAccountModel model)
        {
            _view = view;
            _service = service;
            _settingsProvider = settingsProvider;
            _model = model;

            _view.Periods = _model.PeriodList;
            _view.SelectPeriod(model.SelectedYear, model.SelectedPeriod);
            _view.InitAccountGrid();
            InitAccountsPresenter();
            _model.SelectionChanged += OnSelectedAccountsChanged;
            _view.ApplyRequested += OnApplyRequested;
            _view.AccountHeaderChecked +=
                state =>
                {
                    _gridAccountsPresenter.Rows.ForAll(r => r.IsChecked = (state == CheckState.Checked));
                    _view.GridRefresh();
                };
            _view.ApplyEnabled = false;
        }

        #region Accounts

        private DataGridPresenter<BankAccountBriefModel> _gridAccountsPresenter;

        class GridPresenter : DataGridPresenter<BankAccountBriefModel>
        {
            public GridPresenter(
                IDataGridView gridView,
                Func<QueryConditions, PageResult<BankAccountBriefModel>> dataLoader,
                QueryConditions searchCriteria = null)
                : base(gridView, dataLoader, searchCriteria)
            {
            }

            protected override void AfterFiltering()
            {
                base.AfterFiltering();
                Load();
            }

            protected override void AfterSorting()
            {
                base.AfterSorting();
                Load();
            }
        }

        private void InitAccountsPresenter()
        {
            var qc = new QueryConditions
            {
                Sorting = new List<ColumnSort>
                {
                    new ColumnSort
                    {
                      ColumnKey  = "BIK",
                      Order = ColumnSort.SortOrder.Asc
                    },
                    new ColumnSort
                    {
                        ColumnKey = "ACCOUNT",
                        Order = ColumnSort.SortOrder.Asc
                    }
                }
            };
            
            _gridAccountsPresenter =
                new GridPresenter(
                    _view.GridAccounts, GetAccounts, qc);

            _gridAccountsPresenter.Init(_settingsProvider);

            _view.SearchAccountsRequested += _gridAccountsPresenter.Load;
        }

        private void OnSelectedAccountsChanged()
        {
            _view.ApplyEnabled = _model.Accounts.Any(x => x.IsChecked);
        }

        private PageResult<BankAccountBriefModel> GetAccounts(QueryConditions arg)
        {
            if (_view.SelectedPeriod == null)
                return new PageResult<BankAccountBriefModel> { Rows = new List<BankAccountBriefModel>() };

            var result = _service.BankAccountsForPeriod(arg, _model.Inn, _model.Kpp, _view.SelectedPeriod.Begin, _view.SelectedPeriod.End);

            _model.Accounts = result.Rows.Select(a => new BankAccountBriefModel(a)).ToList();

            _view.ApplyEnabled = false;

            return new PageResult<BankAccountBriefModel> { Rows = _model.Accounts };
        }

        #endregion

        private void OnApplyRequested()
        {
            var accounts = _model.Accounts.Where(x => x.IsChecked).Select(a => a.GetBaseData()).ToArray();
            if (accounts.Length == 0)
                return;

            foreach (var group in accounts.GroupBy(x => x.Bik))
            {
                var requested = _service.GetRequestedAccounts(group.Key, _view.SelectedPeriod.Begin, _view.SelectedPeriod.End);
                if (requested.Any(acc => group.Any(a => a.Account == acc)))
                {
                    _view.ShowRequestRejectedMessage();
                    _gridAccountsPresenter.Load();
                    return;
                }
            }

            var request = new BankAccountManualRequest(
                            _model.Inn,
                            _model.Kpp,
                            _model.KppEffective,
                            _model.IsReorganized,
                            0,
                            string.Format("2{0}", _view.SelectedPeriod.Period.Code),
                            _view.SelectedPeriod.Year,
                            _model.SonoCode,
                            accounts,
                            _model.UserSid,
                            _model.UserName);

            if (_service.PushRequest(request))
            {
                _view.Close();
            }
            else
            {
                _view.ShowRequestRejectedMessage();
                _gridAccountsPresenter.Load();
            }
        }

        public void TryPushRequest()
        {
            _view.ShowDialog();
        }

    }

}
