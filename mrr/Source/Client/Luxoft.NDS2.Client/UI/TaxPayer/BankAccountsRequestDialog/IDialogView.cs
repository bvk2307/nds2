﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog
{
    public interface IDialogView
    {
        DialogResult ShowDialog();

        TaxPeriod SelectedPeriod { get; }

        IEnumerable<TaxPeriod> Periods { set; }

        event ParameterlessEventHandler SearchAccountsRequested;

        event ParameterlessEventHandler ApplyRequested;

        event Action<CheckState> AccountHeaderChecked;

        IDataGridView GridAccounts { get; }

        bool ApplyEnabled { set; }

        void InitAccountGrid();

        void Close();

        bool ConfirmRequest();

        void ShowRequestRejectedMessage();

        void GridRefresh();

        void SelectPeriod(string year, string period);
    }
}
