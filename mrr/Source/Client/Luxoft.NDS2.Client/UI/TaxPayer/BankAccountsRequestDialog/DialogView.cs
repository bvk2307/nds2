﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
//using Luxoft.NDS2.Client.UI.Navigator.Models;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;

namespace Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog
{
    public interface IMessageService
    {
        DialogResult ShowQuestion(string caption, string message);

        void ShowInfo(string caption, string message);
    }

    partial class DialogView : Form, IDialogView
    {
        private readonly IMessageService _messageService;

        public DialogView(IMessageService messageService)
        {
            _messageService = messageService;
            InitializeComponent();
            _btnCancel.Click += (sender, args) => Close();
            _btnExecute.Click += (sender, args) =>
            {
                var handler = ApplyRequested;
                if (handler != null)
                    handler();
            };
            _btnSearch.Click += RaiseSearchAccounts;
            _comboYearList.SelectionChanged += (sender, args) =>
            {
                PeriodList = _periods.Where(x => x.Year == SelectedYear).Select(x => x.Period);
            };
        }

        #region Periods

        public event ParameterlessEventHandler SearchAccountsRequested;

        private void RaiseSearchAccounts(object sender, EventArgs e)
        {
            var handler = SearchAccountsRequested;
            if (handler != null)
                handler();
        }

        public string SelectedYear
        {
            get
            {
                return _comboYearList.SelectedItem != null
                    ? (string)_comboYearList.SelectedItem.DataValue
                    : null;
            }
        }
        
        public IEnumerable<string> Years
        {
            set
            {
                _comboYearList.Clear();
                _comboYearList.Items.Clear();

                if (!value.Any())
                {
                    _comboYearList.DropDownListWidth = 0;
                    return;
                }

                _comboYearList.DropDownListWidth = _comboYearList.Width;

                foreach (var period in value.OrderByDescending(int.Parse))
                {
                    var item = new ValueListItem
                    {
                        DataValue = period,
                        DisplayText = period
                    };
                    _comboYearList.Items.Add(item);
                }

                _comboYearList.SelectedItem = _comboYearList.Items[0];
            }
        }

        public TaxPeriodSummary SelectedTaxPeriod
        {
            get
            {
                return _comboPeriodList.SelectedItem != null
                    ? (TaxPeriodSummary)_comboPeriodList.SelectedItem.DataValue
                    : null;
            }
        }

        public TaxPeriod SelectedPeriod
        {
            get
            {
                return
                    _periods.FirstOrDefault(x => x.Year == SelectedYear && x.Period.Code == SelectedTaxPeriod.Code);
            }
        }

        public void SelectPeriod(string year, string period)
        {
            foreach (var item in _comboYearList.Items)
            {
                var y = (string)item.DataValue;

                if (y == year)
                {
                    _comboYearList.SelectedItem = item;
                    break;
                }
            }

            var pq = ((int)Math.Ceiling(PeriodHelper.GetBeginDate(period, year).Month/3.0)).ToString();

            foreach (var item in _comboPeriodList.Items)
            {
                var p = (TaxPeriodSummary)item.DataValue;

                if (p.Code == pq)
                {
                    _comboPeriodList.SelectedItem = item;
                    return;
                }
            }
        }

        private TaxPeriod[] _periods;

        public IEnumerable<TaxPeriod> Periods
        {
            set
            {
                _periods = value.ToArray();
                Years = _periods.Select(x => x.Year).Distinct();
            }
        }

        public IEnumerable<TaxPeriodSummary> PeriodList
        {
            set
            {
                _comboPeriodList.Clear();
                _comboPeriodList.Items.Clear();

                if (!value.Any())
                {
                    _comboPeriodList.DropDownListWidth = 0;
                    return;
                }

                _comboPeriodList.DropDownListWidth = _comboPeriodList.Width;

                foreach (var period in value.OrderBy(p => p.MonthOfBegin))
                {
                    var item = new ValueListItem
                    {
                        DataValue = period,
                        DisplayText = period.Description
                    };
                    _comboPeriodList.Items.Add(item);
                }

                _comboPeriodList.SelectedItem = _comboPeriodList.Items[0];
            }
        }

        #endregion

        #region Accounts

        public IDataGridView GridAccounts
        {
            get
            {
                return _gridAccounts;
            }
        }

        public bool ApplyEnabled
        {
            set
            {
                _btnExecute.Enabled = value;
            }
        }

        public void InitAccountGrid()
        {
            _gridAccounts
                .InitColumns(GetBankAccountGridSetup());
        }

        private GridColumnSetup GetBankAccountGridSetup()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<BankAccountBriefModel>(_gridAccounts);

            setup
                .WithColumn(helper.CreateCheckColumnThreeWay(o => o.IsChecked).Configure(c =>
                {
                    c.DisableFilter = true;
                    c.DisableSort = true;
                    c.HeaderCheckedChange += state =>
                    {
                        var handler = AccountHeaderChecked;
                        if (handler != null)
                            handler(state);
                    };
                }))
                .WithColumn(helper.CreateTextColumn(o => o.Account).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.AccountType).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.Currency).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.Bik).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.NameBank).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                    c.DisableSort = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.Inn).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                .WithColumn(helper.CreateTextColumn(o => o.Kpp).Configure(c =>
                {
                    c.Width = 300;
                    c.DisableFilter = true;
                }))
                ;
            return setup;
        }

        public void GridRefresh()
        {
            _gridAccounts.Refresh();
        }

        public event Action<CheckState> AccountHeaderChecked;

        #endregion

        public bool ConfirmRequest()
        {
            return _messageService.ShowQuestion(
                ResourceManagerNDS2.Attention,
                ResourceManagerNDS2.TaxPayerCard.RequestAlreadyExists)
                   == DialogResult.Yes;
        }

        public void ShowRequestRejectedMessage()
        {
            _messageService.ShowInfo(
                ResourceManagerNDS2.Attention,
                ResourceManagerNDS2.TaxPayerCard.AccountsAlreadyRequested);
        }

        public event ParameterlessEventHandler ApplyRequested;

    }
}
