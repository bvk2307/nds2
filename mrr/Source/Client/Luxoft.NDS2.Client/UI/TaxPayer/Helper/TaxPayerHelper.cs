﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using taxPayerDto = Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;

namespace Luxoft.NDS2.Client.UI.TaxPayer.Helper
{
    public class TaxPayerHelper
    {
        public static taxPayerDto.TaxPayer CreateTaxPayer(DeclarationSummary decl)
        {
            taxPayerDto.TaxPayer taxPayer = new taxPayerDto.TaxPayer();

            taxPayer.Id = -1;
            taxPayer.Inn = decl.INN;
            taxPayer.Kpp = decl.KPP;
            taxPayer.Name = String.Empty;
            if (decl.SOUN_ENTRY != null)
            {
                taxPayer.Soun = decl.SOUN_ENTRY.EntryId;
            }
            taxPayer.SounName = decl.SOUN;
            taxPayer.SurCode = decl.SUR_CODE;

            return taxPayer;
        }
    }
}
