﻿namespace Luxoft.NDS2.Client.UI.TaxPayer.SelectPeriodDialog
{
    public partial class DialogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layout = new System.Windows.Forms.TableLayoutPanel();
            this._panelFooter = new System.Windows.Forms.Panel();
            this._btnCancel = new Infragistics.Win.Misc.UltraButton();
            this._btnExecute = new Infragistics.Win.Misc.UltraButton();
            this._panelHeader = new System.Windows.Forms.Panel();
            this._comboPeriodList = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._labelPeriodList = new System.Windows.Forms.Label();
            this.layout.SuspendLayout();
            this._panelFooter.SuspendLayout();
            this._panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboPeriodList)).BeginInit();
            this.SuspendLayout();
            // 
            // layout
            // 
            this.layout.ColumnCount = 1;
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout.Controls.Add(this._panelFooter, 0, 2);
            this.layout.Controls.Add(this._panelHeader, 0, 0);
            this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout.Location = new System.Drawing.Point(0, 0);
            this.layout.Name = "layout";
            this.layout.RowCount = 3;
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layout.Size = new System.Drawing.Size(464, 166);
            this.layout.TabIndex = 0;
            // 
            // _panelFooter
            // 
            this._panelFooter.Controls.Add(this._btnCancel);
            this._panelFooter.Controls.Add(this._btnExecute);
            this._panelFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelFooter.Location = new System.Drawing.Point(3, 119);
            this._panelFooter.Name = "_panelFooter";
            this._panelFooter.Size = new System.Drawing.Size(458, 44);
            this._panelFooter.TabIndex = 1;
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._btnCancel.Location = new System.Drawing.Point(233, 12);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(120, 23);
            this._btnCancel.TabIndex = 1;
            this._btnCancel.Text = "Отменить";
            // 
            // _btnExecute
            // 
            this._btnExecute.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._btnExecute.Location = new System.Drawing.Point(104, 12);
            this._btnExecute.Name = "_btnExecute";
            this._btnExecute.Size = new System.Drawing.Size(120, 23);
            this._btnExecute.TabIndex = 0;
            this._btnExecute.Text = "Сформировать";
            // 
            // _panelHeader
            // 
            this._panelHeader.Controls.Add(this._comboPeriodList);
            this._panelHeader.Controls.Add(this._labelPeriodList);
            this._panelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelHeader.Location = new System.Drawing.Point(3, 3);
            this._panelHeader.Name = "_panelHeader";
            this._panelHeader.Size = new System.Drawing.Size(458, 94);
            this._panelHeader.TabIndex = 0;
            // 
            // _comboPeriodList
            // 
            this._comboPeriodList.Location = new System.Drawing.Point(155, 24);
            this._comboPeriodList.Name = "_comboPeriodList";
            this._comboPeriodList.Size = new System.Drawing.Size(249, 21);
            this._comboPeriodList.TabIndex = 1;
            this._comboPeriodList.Text = "ultraComboEditor1";
            // 
            // _labelPeriodList
            // 
            this._labelPeriodList.AutoSize = true;
            this._labelPeriodList.Location = new System.Drawing.Point(54, 28);
            this._labelPeriodList.Name = "_labelPeriodList";
            this._labelPeriodList.Size = new System.Drawing.Size(95, 13);
            this._labelPeriodList.TabIndex = 0;
            this._labelPeriodList.Text = "Отчетный период";
            // 
            // DialogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 166);
            this.Controls.Add(this.layout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(480, 200);
            this.Name = "DialogView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Формирование запроса";
            this.layout.ResumeLayout(false);
            this._panelFooter.ResumeLayout(false);
            this._panelHeader.ResumeLayout(false);
            this._panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboPeriodList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layout;
        private System.Windows.Forms.Panel _panelFooter;
        private Infragistics.Win.Misc.UltraButton _btnCancel;
        private Infragistics.Win.Misc.UltraButton _btnExecute;
        private System.Windows.Forms.Panel _panelHeader;
        private System.Windows.Forms.Label _labelPeriodList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboPeriodList;
    }
}