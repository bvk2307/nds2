﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.TaxPayer.SelectPeriodDialog
{
    public partial class DialogView : Form
    {
        public static TaxPeriod TrySelectPeriod(Form owner, IEnumerable<TaxPeriod> periods)
        {
            var dialog = new DialogView { PeriodList = periods.ToArray() };
            var result = dialog.ShowDialog(owner);
            return result == DialogResult.OK ? dialog.SelectedPeriod : null;
        }
        
        private DialogView()
        {
            InitializeComponent();

            _comboPeriodList.SelectionChanged += (sender, args) =>
            {
                _btnExecute.Enabled = _comboPeriodList.SelectedItem != null;
            };

            _btnCancel.Click += (sender, args) =>
            {
                DialogResult = DialogResult.Cancel;
                Close();
            };
            _btnExecute.Click += (sender, args) =>
            {
                DialogResult = DialogResult.OK;
                Close();
            };
        }

        #region Periods

        public TaxPeriod SelectedPeriod
        {
            get
            {
                return _comboPeriodList.SelectedItem != null
                    ? (TaxPeriod) _comboPeriodList.SelectedItem.DataValue
                    : null;
            }
        }

        public TaxPeriod[] PeriodList
        {
            set
            {
                _comboPeriodList.Clear();
                _comboPeriodList.Items.Clear();

                if (value.Length == 0)
                {
                    _comboPeriodList.DropDownListWidth = 0;
                    return;
                }

                _comboPeriodList.DropDownListWidth = _comboPeriodList.Width;

                foreach (var period in value.OrderByDescending(p => p.Year).ThenByDescending(p => p.Period.MonthOfBegin))
                {
                    var item = new ValueListItem
                    {
                        DataValue = period,
                        DisplayText = period.Description
                    };
                    _comboPeriodList.Items.Add(item);
                }

                _comboPeriodList.SelectedItem = _comboPeriodList.Items[0];
            }
        }

        #endregion
    }
}
