﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    public class Model
    {
        private const string PendingText = "Получение информации о наличии обновлений";

        private const string FailedText = "Не удалось получить информацию о наличии обновлений";

        private const string UpToDateText = "Нет доступных обновлений";

        private const string UpdateAvailableText = "Доступно обновление для установки";

        private VersionVerificationState _state = VersionVerificationState.Pending;

        public Model(string version)
        {
            CurrentVersion = version;
        }

        public string CurrentVersion
        {
            get;
            private set;
        }

        public string UpdateInfoStatus
        {
            get
            {
                switch (_state)
                {
                    case VersionVerificationState.Pending: return PendingText;
                    case VersionVerificationState.Failed: return FailedText;
                    case VersionVerificationState.UpdateAvailable: return UpdateAvailableText;
                    case VersionVerificationState.UpToDate: return UpToDateText;
                }

                return string.Empty;
            }
        }

        public bool UpdateInfoAvailable
        {
            get
            {
                return _state == VersionVerificationState.UpdateAvailable;
            }
        }

        public string NextVersion
        {
            get;
            private set;
        }

        public string ReleaseDate
        {
            get;
            private set;
        }

        public string Path
        {
            get;
            private set;
        }

        public long Size
        {
            get;
            private set;
        }

        public bool ReindexRequired
        {
            get;
            private set;
        }

        public event ParameterlessEventHandler UpdateInfoStatusChanged;

        public event GenericEventHandler<int> OnDownloadProgress;

        public void SetUpdateInfo(UpdateInfo info)
        {
            _state =
                info.Version.Undefined || info.Version.VersionNumber == CurrentVersion
                ? VersionVerificationState.UpToDate
                : VersionVerificationState.UpdateAvailable;

            if (UpdateInfoAvailable)
            {
                UpdateBaseParam(info);
            }

            RaiseUpdateInfoStatusChanged();
        }

        public void SetUpdateInfoFailed(UpdateInfo info)
        {
            _state = VersionVerificationState.Failed;
            UpdateBaseParam(info);
            RaiseUpdateInfoStatusChanged();
        }

        public void SetUpdateInfoFailed()
        {
            _state = VersionVerificationState.Failed;
            RaiseUpdateInfoStatusChanged();
        }

        private void UpdateBaseParam(UpdateInfo info)
        {
            NextVersion = info.Version.VersionNumber;
            ReleaseDate = info.Version.ReleaseDate.ToString();
            Path = info.Path;
            Size = info.Length;
            ReindexRequired = info.Version.CatalogReindexRequired;
        }

        private void RaiseUpdateInfoStatusChanged()
        {
            if (UpdateInfoStatusChanged != null)
            {
                UpdateInfoStatusChanged();
            }
        }

        public void SetDownloadProgress(long bytesDownloaded)
        {
            if (OnDownloadProgress != null)
            {
                OnDownloadProgress((int)(100 * bytesDownloaded / Size));
            }
        }
    }
}
