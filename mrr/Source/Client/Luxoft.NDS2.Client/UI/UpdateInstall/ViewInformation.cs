﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    public partial class ViewInformation : BaseView
    {
        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
                _presenter.Model.UpdateInfoStatusChanged += SetViewDataThreadSafe;
            }
        }
        
        
        public ViewInformation()
        {
            InitializeComponent();
        }

        public ViewInformation(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
        }

        private void ViewInformation_Load(object sender, EventArgs e)
        {
            _presenter.StartLoading();
        }

        private void SetViewDataThreadSafe()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new VoidDelegate(SetViewData));
            }
            else
            {
                SetViewData();
            }
        }

        private void SetViewData()
        {
            string footerTextTemplate = "Текущая версия: {0}, следующая версия: {1}";

            footerFormattedText.Text = string.Format(footerTextTemplate,
                                                        _presenter.Model.CurrentVersion,
                                                        _presenter.Model.NextVersion);
        }
    }
}
