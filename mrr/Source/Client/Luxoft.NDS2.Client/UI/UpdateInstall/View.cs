﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    public delegate void VoidDelegate();

    public partial class View : BaseView
    {
        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presentationContext.WindowTitle = "Обновление версии";
                SetViewData();
                _presenter.Model.UpdateInfoStatusChanged += SetViewDataThreadSafe;
            }
        }

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
        }

        private void SetViewDataThreadSafe()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new VoidDelegate(SetViewData));
            }
            else
            {
                SetViewData();
            }
        }

        private void SetViewData()
        {
            _currentVersion.Text = _presenter.Model.CurrentVersion;
            _updateInfoStatus.Text = _presenter.Model.UpdateInfoStatus;
            _updateInfo.Visible = _presenter.Model.UpdateInfoAvailable;
            _updateVersion.Text = _presenter.Model.NextVersion;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            _presenter.StartLoading();
        }

        private const string QuestionReindexRequired =
            "Для выполнения обновления Вам необходимо закрыть АИС Налог-3. После того как обновление будет завершено, потребуется повторная индексация катологов АСК НДС-2. Выполнить обновление сейчас?";

        private const string QuestionNoReindex =
            "Для выполнения обновления Вам необходимо закрыть АИС Налог-3. После обновления АИС Налог-3 будет запущен автоматически. Выполнить обновление сейчас?";

        private void Install(object sender, EventArgs e)
        {
            if (ShowQuestion(
                "Обновление АСК НДС-2",
                _presenter.Model.ReindexRequired
                    ? QuestionReindexRequired
                    : QuestionNoReindex) == DialogResult.Yes)
            {
                _presenter.StartDownloading();
            }
        }
    }
}
