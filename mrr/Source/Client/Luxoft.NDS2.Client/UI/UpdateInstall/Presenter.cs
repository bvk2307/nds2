﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    public class Presenter : BasePresenter<View>
    {
        private const string UpdaterPath = @"RoleCatalog\Abs4.0\NDS2Subsystem\Bin\Updater\Luxoft.NDS2.Client.Updater.exe";

        private const string LocalPathToArchive = @"{0}nds2_mrr_client\";

        private const string LocalArchiveName = "client.zip";

        private const int MaxChunkDownloadAttempt = 3;

        public Presenter()
        {
            Model = new Model(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        public Model Model
        {
            get;
            private set;
        }

        public void StartLoading()
        {
            ExecuteServiceCall(
                () => GetServiceProxy<IVersionInfoService>().GetUpdateInfo(),
                (response) => 
                    {
                        if (!response.Result.Version.Undefined && response.Result.Length > 0)
                        {
                            Model.SetUpdateInfo(response.Result);
                        }
                        else
                        {
                            Model.SetUpdateInfoFailed(response.Result);
                        }                        
                    },
                (response) => Model.SetUpdateInfoFailed());
        }

        public void StartDownloading()
        {
            var asyncWorker = new AsyncWorker<object>();      
     

            asyncWorker.DoWork +=
                (sender, args) =>
                {
                    var localFolder = string.Format(LocalPathToArchive, Path.GetTempPath());

                    if (TryDownload(localFolder, LocalArchiveName))
                    {
                        Process.Start(
                            AppDomain.CurrentDomain.BaseDirectory + UpdaterPath,
                            localFolder 
                                + LocalArchiveName 
                                + " " 
                                + (Model.ReindexRequired ? "1" : "0"));
                    }
                };

            asyncWorker.Start();
        }

        private bool TryDownload(string destinationFolder, string fileName)
        {
            var chunkSize = 131072;
            var bytesReceived = 0;
            var chunkFailures = 0;
            var localFilePath = destinationFolder + fileName;
            var proxy = GetServiceProxy<IVersionInfoService>();

            destinationFolder.CreateOrClearDirectory();

            using (var stream = new FileStream(localFilePath, FileMode.Create))
            {
                do
                {
                    try
                    {
                        var data =
                            proxy.GetClientChunk(Model.Path, bytesReceived, chunkSize);

                        if (data.Length < chunkSize
                            && bytesReceived + data.Length < Model.Size)
                        {
                            chunkFailures++;
                        }
                        else
                        {
                            chunkFailures = 0;
                            stream.Write(data, 0, data.Length);
                            bytesReceived += data.Length;
                            Model.SetDownloadProgress(bytesReceived);
                        }
                    }
                    catch
                    {
                        chunkFailures++;
                    }

                    if (chunkFailures >= MaxChunkDownloadAttempt)
                    {
                        View.ShowError("Ошибка загрузки обновления");
                        stream.Close();
                        return false;
                    }
                }
                while (bytesReceived < Model.Size);

                stream.Close();
            }

            return true;
        }
    }

    public static class DirectryHelper
    {
        public static void CreateOrClearDirectory(this string path)
        {
            if (Directory.Exists(path))
            {
                ClearDirectory(path);
            }
            else
            {
                Directory.CreateDirectory(path);
            }
        }

        private static void ClearDirectory(this string source)
        {
            foreach (var fileName in Directory.GetFiles(source))
            {
                File.Delete(fileName);
            }

            foreach (var directory in Directory.GetDirectories(source))
            {
                ClearDirectory(directory);
                Directory.Delete(directory);
            }
        }
    }
}
