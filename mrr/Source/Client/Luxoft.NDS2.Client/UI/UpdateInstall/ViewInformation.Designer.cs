﻿namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    partial class ViewInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewInformation));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.infoFormattedText = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.footerFormattedText = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.SuspendLayout();
            // 
            // infoFormattedText
            // 
            appearance1.FontData.BoldAsString = "False";
            appearance1.FontData.ItalicAsString = "False";
            appearance1.FontData.Name = "Microsoft Sans Serif";
            appearance1.FontData.SizeInPoints = 8.25F;
            appearance1.FontData.StrikeoutAsString = "False";
            appearance1.FontData.UnderlineAsString = "False";
            appearance1.TextVAlignAsString = "Middle";
            this.infoFormattedText.Appearance = appearance1;
            this.infoFormattedText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoFormattedText.Location = new System.Drawing.Point(0, 0);
            this.infoFormattedText.Name = "infoFormattedText";
            this.infoFormattedText.ReadOnly = true;
            this.infoFormattedText.Size = new System.Drawing.Size(698, 399);
            this.infoFormattedText.TabIndex = 2;
            this.infoFormattedText.TabStop = false;
            this.infoFormattedText.Value = resources.GetString("infoFormattedText.Value");
            // 
            // footerFormattedText
            // 
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            this.footerFormattedText.Appearance = appearance2;
            this.footerFormattedText.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerFormattedText.Location = new System.Drawing.Point(0, 399);
            this.footerFormattedText.Name = "footerFormattedText";
            this.footerFormattedText.ReadOnly = true;
            this.footerFormattedText.Size = new System.Drawing.Size(698, 23);
            this.footerFormattedText.TabIndex = 3;
            this.footerFormattedText.TabStop = false;
            this.footerFormattedText.Value = "";
            // 
            // ViewInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoFormattedText);
            this.Controls.Add(this.footerFormattedText);
            this.Name = "ViewInformation";
            this.Size = new System.Drawing.Size(698, 422);
            this.Load += new System.EventHandler(this.ViewInformation_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor infoFormattedText;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor footerFormattedText;
    }
}
