﻿namespace Luxoft.NDS2.Client.UI.UpdateInstall
{
    partial class View
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _updateVersionLabel;
            Infragistics.Win.Misc.UltraPanel _formArea;
            Infragistics.Win.Misc.UltraLabel _versionLabel;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this._currentVersion = new Infragistics.Win.Misc.UltraLabel();
            this._updateInfoStatus = new Infragistics.Win.Misc.UltraLabel();
            this._updateInfo = new Infragistics.Win.Misc.UltraPanel();
            this._install = new Infragistics.Win.Misc.UltraButton();
            this._updateVersion = new Infragistics.Win.Misc.UltraLabel();
            _updateVersionLabel = new Infragistics.Win.Misc.UltraLabel();
            _formArea = new Infragistics.Win.Misc.UltraPanel();
            _versionLabel = new Infragistics.Win.Misc.UltraLabel();
            _formArea.ClientArea.SuspendLayout();
            _formArea.SuspendLayout();
            this._updateInfo.ClientArea.SuspendLayout();
            this._updateInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // _updateVersionLabel
            // 
            _updateVersionLabel.Location = new System.Drawing.Point(4, 4);
            _updateVersionLabel.Name = "_updateVersionLabel";
            _updateVersionLabel.Size = new System.Drawing.Size(203, 23);
            _updateVersionLabel.TabIndex = 0;
            _updateVersionLabel.Text = "Версия, доступная для обновления:";
            // 
            // _formArea
            // 
            // 
            // _formArea.ClientArea
            // 
            _formArea.ClientArea.Controls.Add(this._currentVersion);
            _formArea.ClientArea.Controls.Add(_versionLabel);
            _formArea.ClientArea.Controls.Add(this._updateInfoStatus);
            _formArea.ClientArea.Controls.Add(this._updateInfo);
            _formArea.Dock = System.Windows.Forms.DockStyle.Fill;
            _formArea.Location = new System.Drawing.Point(0, 0);
            _formArea.Name = "_formArea";
            _formArea.Size = new System.Drawing.Size(444, 136);
            _formArea.TabIndex = 9;
            // 
            // _currentVersion
            // 
            this._currentVersion.Location = new System.Drawing.Point(171, 6);
            this._currentVersion.Name = "_currentVersion";
            this._currentVersion.Size = new System.Drawing.Size(250, 23);
            this._currentVersion.TabIndex = 10;
            // 
            // _versionLabel
            // 
            _versionLabel.Location = new System.Drawing.Point(8, 6);
            _versionLabel.Name = "_versionLabel";
            _versionLabel.Size = new System.Drawing.Size(166, 23);
            _versionLabel.TabIndex = 9;
            _versionLabel.Text = "Текущая версия АСК НДС-2:";
            // 
            // _updateInfoStatus
            // 
            appearance2.ForeColor = System.Drawing.Color.Red;
            this._updateInfoStatus.Appearance = appearance2;
            this._updateInfoStatus.Location = new System.Drawing.Point(8, 34);
            this._updateInfoStatus.Name = "_updateInfoStatus";
            this._updateInfoStatus.Size = new System.Drawing.Size(427, 23);
            this._updateInfoStatus.TabIndex = 5;
            // 
            // _updateInfo
            // 
            // 
            // _updateInfo.ClientArea
            // 
            this._updateInfo.ClientArea.Controls.Add(this._install);
            this._updateInfo.ClientArea.Controls.Add(this._updateVersion);
            this._updateInfo.ClientArea.Controls.Add(_updateVersionLabel);
            this._updateInfo.Location = new System.Drawing.Point(4, 62);
            this._updateInfo.Name = "_updateInfo";
            this._updateInfo.Size = new System.Drawing.Size(435, 65);
            this._updateInfo.TabIndex = 6;
            this._updateInfo.Visible = false;
            // 
            // _install
            // 
            this._install.Location = new System.Drawing.Point(4, 33);
            this._install.Name = "_install";
            this._install.Size = new System.Drawing.Size(75, 23);
            this._install.TabIndex = 4;
            this._install.Text = "Обновить";
            this._install.Click += new System.EventHandler(this.Install);
            // 
            // _updateVersion
            // 
            this._updateVersion.Location = new System.Drawing.Point(213, 3);
            this._updateVersion.Name = "_updateVersion";
            this._updateVersion.Size = new System.Drawing.Size(219, 23);
            this._updateVersion.TabIndex = 1;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(_formArea);
            this.Name = "View";
            this.Size = new System.Drawing.Size(444, 136);
            this.Load += new System.EventHandler(this.OnLoad);
            _formArea.ClientArea.ResumeLayout(false);
            _formArea.ResumeLayout(false);
            this._updateInfo.ClientArea.ResumeLayout(false);
            this._updateInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel _updateInfoStatus;
        private Infragistics.Win.Misc.UltraButton _install;
        private Infragistics.Win.Misc.UltraLabel _updateVersion;
        private Infragistics.Win.Misc.UltraPanel _updateInfo;
        private Infragistics.Win.Misc.UltraLabel _currentVersion;
    }
}
