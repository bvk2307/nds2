﻿using System.Windows.Media;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapPath
    {
        public string Name { get; set; }

        public PathFigureCollection Path { set; get; }

        public MapRegionElement Parent { get; set; }

    }
}
