﻿using System;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapStateModel : BaseViewModel
    {
        private string _regionId;
        private MacroReportType _reportType;
        private MapType _mapType;
        private NdsType _ndsType;
        private int _year;
        private int _quarter;
        private double _dimensionLegend;

        public MapStateModel()
        {
            ReportType = MacroReportType.Nds;
            MapType = MapType.Districts; //вся Россия
            RegionId = "0";
            NdsType = NdsType.ShareDeductionsInComputingNds;
            Year = DateTime.Now.Year;
            var year = Year;
            Quarter = DateTime.Now.ToQuarter(ref year);
            Year = year;
        }

        public string RegionId
        {
            get { return _regionId; }
            set
            {
                _regionId = value;
                OnPropertyChanged("RegionId");
            }
        }

        public MacroReportType ReportType
        {
            get { return _reportType; }
            set
            {
                _reportType = value;
                OnPropertyChanged("ReportType");
            }
        }

        public MapType MapType
        {
            get { return _mapType; }
            set
            {
                _mapType = value;
                OnPropertyChanged("MapType");
            }
        }

        public NdsType NdsType
        {
            get { return _ndsType; }
            set
            {
                _ndsType = value;
                OnPropertyChanged("NdsType");
            }
        }

        public int Year
        {
            get { return _year; }
            set
            {
                _year = value;
                OnPropertyChanged("Year");
            }
        }

        public int Quarter
        {
            get { return _quarter; }
            set
            {
                _quarter = value;
                OnPropertyChanged("Quarter");
            }
        }
       
    }
}