﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class TimeSequenceViewModel
    {
        private readonly List<String> _firstPeriodList = new List<String> { "1", "01", "02", "03", "21", "51", "71", "72", "73" };
        private readonly List<String> _secondListPeriodList = new List<String> { "2", "04", "05", "06", "22", "54", "74", "75", "76" };
        private readonly List<String> _thirdPeriodList = new List<String> { "3", "07", "08", "09", "23", "55", "77", "78", "79" };
        private readonly List<String> _fourthPeriodList = new List<String> { "4", "10", "11", "12", "24", "56", "80", "81", "82" };

        public int FiscalYear { get; set; }

        public int TaxPeriod { get; set; }
        
        public decimal CountTotal { get; set; }

        public String GetTitle( )
        {
            return String.Format( "{0} кв. {1}", FormatTaxPeriod( ), FiscalYear );
        }

        private int FormatTaxPeriod( )
        {
            if ( _firstPeriodList.Contains( TaxPeriod.ToString( ) ) )
            {
                return 1;
            }
            if ( _secondListPeriodList.Contains( TaxPeriod.ToString( ) ) )
            {
                return 2;
            }
            if ( _thirdPeriodList.Contains( TaxPeriod.ToString( ) ) )
            {
                return 3;
            }
            return _fourthPeriodList.Contains( TaxPeriod.ToString( ) ) ? 4 : 0;
        }
    }
}