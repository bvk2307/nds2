﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class NpRequestData
    {
        public string IfnsId { get; set; }

        public int Year { get; set; }

        public int Quarter { get; set; }

        public int Skip { get; set; }

        public int Take { get; set; }

        public IEnumerable<ColumnSortModel> Order { get; set; }

        public IEnumerable<FilterSetting> Filter { get; set; }

        public bool WithTotal { get; set; }
    }
}
