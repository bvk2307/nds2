﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class TnoViewModel
    {
        public string TnoId { get; set; }
        public string TnoName { get; set; }
        public decimal CountTotal { get; set; }
    }
}
