﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class BreadcrumbsViewModel : BaseViewModel
    {
        private string _id;
        private string _header;
        private string _previewHeader;
        private ImageSource _image;
        private bool _isCurrent;
        private MapType _mapTypeLevel;
        private readonly ObservableCollection<BreadcrumbsViewModel> _children;

        public BreadcrumbsViewModel()
        {
            this._children = new ObservableCollection<BreadcrumbsViewModel>();
        }


        public string Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        public string Header
        {
            get { return _header; }
            set
            {
                if (_header != value)
                {
                    _header = value;
                    PreviewHeader = value;
                    OnPropertyChanged("Header");
                }
            }
        }

        public string PreviewHeader
        {
            get { return _previewHeader; }
            set
            {
                if (_previewHeader != value)
                {
                    _previewHeader = value;
                    OnPropertyChanged("PreviewHeader");
                }
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                if (_image != value)
                {
                    _image = value;
                    OnPropertyChanged("Image");
                }
            }
        }

        public bool IsCurrent
        {
            get { return _isCurrent; }
            set
            {
                if (_isCurrent != value)
                {
                    _isCurrent = value;
                    OnPropertyChanged("IsCurrent");
                }
            }
        }

        public MapType MapTypeLevel
        {
            get { return _mapTypeLevel; }
            set
            {
                if (_mapTypeLevel != value)
                {
                    _mapTypeLevel = value;
                    OnPropertyChanged("MapTypeLevel");
                }
            }
        }

        public ObservableCollection<BreadcrumbsViewModel> Children
        {
            get { return _children; }
        }

        public BreadcrumbsViewModel Parent { get; set; }
    }
}
