﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Linq;
using Luxoft.NDS2.Client.UI.MapWpf.Controls;
using Luxoft.NDS2.Client.UI.MapWpf.Exceptions;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapViewModel : BaseViewModel
    {

        public MapViewModel(ReportControlViewModel parent)
        {
            _parent = parent;
            Init();
        }

        public static double CircleSize = 45;

        private ObservableCollection<MapRootElement> _elements;
        private double _mapScaleX = 1;
        private double _mapScaleY = 1;
        private bool _showRoolLabels = true;
        private Point _scalePoint;
        private double _rootCircleSize = CircleSize;
        private MapLevel _mapLevel = MapLevel.Country;
        private readonly ReportControlViewModel _parent;
        private double _regionStrokeThickness = 1;

        private void Init()
        {
            var asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream("Luxoft.NDS2.Client.UI.MapWpf.Data.MapData.xml"))
            {
                XDocument doc = XDocument.Load(stream);
                Elements = new ObservableCollection<MapRootElement>(doc.Root.Elements().Select(s => new MapRootElement
                {
                    Parent = this,
                    Id = s.XAttribute<string>("Id"),
                    Name = s.XAttribute<string>("Name"),
                    ScaleCenterPoint = s.XAttribute<Point>("Center"),
                    Scale = s.XAttribute<double>("Scale"),
                    Title = s.XAttribute<string>("Title"),
                    RootBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x6b, 0xa6, 0xdc)),
                    CenterPosition = s.XAttribute<Point>("CenterPosition"),
                    FullPathFigures = s.XAttribute<PathFigureCollection>("Figures"),
                    Children = new ObservableCollection<MapRegionElement>(s.Elements("RegionCanvas").Select(r=> new MapRegionElement
                    {
                        Id = r.XAttribute<string>("Id"),
                        Name = r.XAttribute<string>("Name"),
                        Title = r.XAttribute<string>("Title"),
                        CenterPosition = r.XAttribute<Point>("CenterPosition"),
                        RenderTransform = r.XAttribute<Transform>("RenderTransform"),
                        CirclePosition = r.XAttribute<Point>("CirclePosition"),
                        Figures = new ObservableCollection<MapPath>(r.Elements("Path").Select(p => new MapPath{ Name = p.XAttribute<string>("Name"), Path = p.Element("Path.Data").Element("PathGeometry").XAttribute<PathFigureCollection>("Figures")})),
                    }))
                }));
            }
        }

        public void Submit()
        {
            var root = new XElement("MapData");
            foreach (var mapRootElement in Elements)
            {
                var el = new XElement("RootCanvas", 
                    new XAttribute("Id", mapRootElement.Id),
                    new XAttribute("Name", mapRootElement.Name),
                    new XAttribute("Scale", mapRootElement.Scale),
                    new XAttribute("Center", mapRootElement.ScaleCenterPoint.ToString(CultureInfo.InvariantCulture)),
                    new XAttribute("Title", mapRootElement.Title),
                    new XAttribute("CenterPosition", mapRootElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));

                foreach (var mapRegionElement in mapRootElement.Children)
                {
                    var rc = new XElement("RegionCanvas", new XAttribute("Id", mapRegionElement.Id),
                        new XAttribute("Name", mapRegionElement.Name),
                        new XAttribute("Title", mapRegionElement.Title),
                        new XAttribute("CenterPosition", mapRegionElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));
                    if (!mapRegionElement.CirclePosition.IsEmpty())
                    {
                        rc.Add(new XAttribute("CirclePosition", mapRegionElement.CirclePosition.ToString(CultureInfo.InvariantCulture)));
                    }
                    foreach (var mapPath in mapRegionElement.Figures)
                    {
                        var path = new XElement("Path");
                        if (!string.IsNullOrEmpty(mapPath.Name))
                        {
                            path.Add(new XAttribute("Name", mapPath.Name));
                        }

                        var p = new Path();
                        p.Data = new PathGeometry(mapPath.Path);
                        var geometry = Geometry.Combine(p.Data, p.Data, GeometryCombineMode.Intersect, mapRegionElement.RenderTransform);
                        var geometryString = geometry.ToString(CultureInfo.InvariantCulture);
                        path.Add(new XElement("Path.Data", new XElement("PathGeometry", 
                                            new XAttribute("FillRule", "Nonzero"),
                                            new XAttribute("Figures", geometryString))));

                        rc.Add(path);
                    }
                    el.Add(rc);
                }


                root.Add(el);
            }
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null), root);
            doc.Save(@"c:\Projects\GNIVC_NDS2\mrr\Source\Client\Luxoft.NDS2.Client\UI\MapWpf\Data\MapData_new.xml");
        }

        public void Submit1()
        {
            var root = new XElement("MapData");
            foreach (var mapRootElement in Elements)
            {
                var el = new XElement("RootCanvas",
                    new XAttribute("Id", mapRootElement.Id),
                    new XAttribute("Name", mapRootElement.Name),
                    new XAttribute("Scale", mapRootElement.Scale),
                    new XAttribute("Center", mapRootElement.ScaleCenterPoint.ToString(CultureInfo.InvariantCulture)),
                    new XAttribute("Title", mapRootElement.Title),
                    new XAttribute("CenterPosition", mapRootElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));

                if (mapRootElement.FullPath != null)
                {
                    var geometryRootString = mapRootElement.FullPath.Data.GetFlattenedPathGeometry().ToString(CultureInfo.InvariantCulture);
                    el.Add(new XAttribute("Figures", geometryRootString));
                }

                foreach (var mapRegionElement in mapRootElement.Children)
                {
                    var rc = new XElement("RegionCanvas", new XAttribute("Id", mapRegionElement.Id),
                        new XAttribute("Name", mapRegionElement.Name),
                        new XAttribute("Title", mapRegionElement.Title),
                        new XAttribute("CenterPosition", mapRegionElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));
                    if (!mapRegionElement.CirclePosition.IsEmpty())
                    {
                        rc.Add(new XAttribute("CirclePosition", mapRegionElement.CirclePosition.ToString(CultureInfo.InvariantCulture)));
                    }
                    foreach (var mapPath in mapRegionElement.Figures)
                    {
                        var path = new XElement("Path");
                        if (!string.IsNullOrEmpty(mapPath.Name))
                        {
                            path.Add(new XAttribute("Name", mapPath.Name));
                        }
                        var geometryString = mapPath.Path.ToString(CultureInfo.InvariantCulture);
                        path.Add(new XElement("Path.Data", new XElement("PathGeometry",
                                            new XAttribute("FillRule", "Nonzero"),
                                            new XAttribute("Figures", geometryString))));

                        rc.Add(path);
                    }
                    el.Add(rc);
                }


                root.Add(el);
            }
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null), root);
            doc.Save(@"c:\Projects\GNIVC_NDS2\mrr\Source\Client\Luxoft.NDS2.Client\UI\MapWpf\Data\MapData_new.xml");
        }


        /// <summary>
        /// Сохранение текущих настроек
        /// </summary>
        public void Submit2()
        {
            var root = new XElement("MapData");
            foreach (var mapRootElement in Elements)
            {
                var el = new XElement("RootCanvas",
                    new XAttribute("Id", mapRootElement.Id),
                    new XAttribute("Name", mapRootElement.Name),
                    new XAttribute("Scale", mapRootElement.Scale),
                    new XAttribute("Center", mapRootElement.ScaleCenterPoint.ToString(CultureInfo.InvariantCulture)),
                    new XAttribute("Title", mapRootElement.Title),
                    new XAttribute("CenterPosition", mapRootElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));

                if (mapRootElement.FullPathFigures != null)
                {
                    var geometryRootString = mapRootElement.FullPathFigures.ToString(CultureInfo.InvariantCulture);
                    el.Add(new XAttribute("Figures", geometryRootString));
                }

                foreach (var mapRegionElement in mapRootElement.Children)
                {
                    var rc = new XElement("RegionCanvas", new XAttribute("Id", mapRegionElement.Id),
                        new XAttribute("Name", mapRegionElement.Name),
                        new XAttribute("Title", mapRegionElement.Title),
                        new XAttribute("CenterPosition", mapRegionElement.CenterPosition.ToString(CultureInfo.InvariantCulture)));
                    if (!mapRegionElement.CirclePosition.IsEmpty())
                    {
                        rc.Add(new XAttribute("CirclePosition", mapRegionElement.CirclePosition.ToString(CultureInfo.InvariantCulture)));
                    }
                    foreach (var mapPath in mapRegionElement.Figures)
                    {
                        var path = new XElement("Path");
                        if (!string.IsNullOrEmpty(mapPath.Name))
                        {
                            path.Add(new XAttribute("Name", mapPath.Name));
                        }
                        var geometryString = mapPath.Path.ToString(CultureInfo.InvariantCulture);
                        path.Add(new XElement("Path.Data", new XElement("PathGeometry",
                                            new XAttribute("FillRule", "Nonzero"),
                                            new XAttribute("Figures", geometryString))));

                        rc.Add(path);
                    }
                    el.Add(rc);
                }


                root.Add(el);
            }
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null), root);
            doc.Save(@"c:\Projects\GNIVC_NDS2\mrr\Source\Client\Luxoft.NDS2.Client\UI\MapWpf\Data\MapData_new.xml");
        }

        public void SetRegionCounts(IEnumerable<MapDataRegionViewModel> rcounts)
        {
            if (MapLevel == MapLevel.Country)
            {
                foreach (var element in Elements)
                {
                    var mdrvm = rcounts.SingleOrDefault(s => s.RegionId == element.Id);
                    if (mdrvm != null)
                    {
                        element.MapValue = mdrvm.CountTotal.ToFormat(_parent.Section.SubSection,
                            _parent.MapStateModel.MapType);
                    }
                    else
                    {
                        element.MapValue = "Н/Д";
                    }
                }
            }
            else if (MapLevel == MapLevel.Region)
            {
                var selected = SelectedRegion;
                if (selected != null)
                {
                    foreach (var element in selected.Children)
                    {
                        var mdrvm = rcounts.SingleOrDefault(s => s.RegionId  == element.Id);
                        if (mdrvm != null)
                        {
                            element.MapValue = mdrvm.CountTotal.ToFormat(_parent.Section.SubSection,
                                _parent.MapStateModel.MapType);
                        }
                        else
                        {
                            element.MapValue = "Н/Д";
                        }
                        //todo видимо сюда идет chart
                        //total.CountTotal += mdrvm.CountTotal;
                    }
                }
            }
        }

        public ReportControlViewModel Parent
        {
            get { return _parent; }
        }

        public ObservableCollection<MapRootElement> Elements
        {
            get { return _elements; }
            set { _elements = value; }
        }

        public MapRootElement SelectedRegion
        {
            get
            {
                return Elements.FirstOrDefault(s => s.IsSelected);
            }
        }

        public double MapScaleX
        {
            get { return _mapScaleX; }
            set
            {
                if (_mapScaleX != value)
                {
                    _mapScaleX = value;
                    OnPropertyChanged("MapScaleX");
                }
            }
        }

        public double MapScaleY
        {
            get { return _mapScaleY; }
            set
            {
                if (_mapScaleY != value)
                {
                    _mapScaleY = value;
                    OnPropertyChanged("MapScaleY");
                }
            }
        }

        public Point ScalePoint
        {
            get { return _scalePoint; }
            set
            {
                if (_scalePoint != value)
                {
                    _scalePoint = value;
                    OnPropertyChanged("ScalePoint");
                }
            }
        }

        public bool ShowRootLabels
        {
            get { return _showRoolLabels; }
            set
            {
                if (_showRoolLabels != value)
                {
                    _showRoolLabels = value;
                    OnPropertyChanged("ShowRootLabels");
                }
            }
        }


        public MapLevel MapLevel
        {
            get { return _mapLevel; }
            set
            {
                if (_mapLevel != value)
                {
                    _mapLevel = value;
                    OnPropertyChanged("MapLevel");
                    switch (_mapLevel)
                    {
                        case MapLevel.Country:
                        {
                            ShowRootLabels = true;
                            _parent.MapStateModel.MapType = MapType.Districts;
                            break;
                        }
                        case MapLevel.Region:
                        {
                            ShowRootLabels = false;
                            _parent.MapStateModel.MapType = MapType.Regions;
                            break;
                        }
                    }
                }
            }
        }

        public double RootCircleSize
        {
            get { return _rootCircleSize; }
            set
            {
                if (_rootCircleSize != value)
                {
                    _rootCircleSize = value;
                    OnPropertyChanged("RootCircleSize");
                }
            }
        }

        public double RegionStrokeThickness
        {
            get { return _regionStrokeThickness; }
            set
            {
                if (_regionStrokeThickness != value)
                {
                    _regionStrokeThickness = value;
                    OnPropertyChanged("RegionStrokeThickness");
                }
            }
        }

        public void SwithToCountryView()
        {
            MapLevel = MapLevel.Country;
            foreach (var mapRootElement in Elements)
            {
                mapRootElement.IsSelected = false;
            }
            _parent.MapStateModel.RegionId = "0";
            this.MapScaleX = 1;
            this.MapScaleY = 1;
            this.RegionStrokeThickness = 1;
            this.ScalePoint = new Point();
            _parent.InvokeLoadDataCommand();
        }

        public void SwithToDistrictView(MapRootElement element)
        {
            MapLevel = MapLevel.Region;
            foreach (var mapRootElement in Elements)
            {
                mapRootElement.IsSelected = false;
            }
            element.IsSelected = true;
            _parent.MapStateModel.RegionId = element.Id;
            this.MapScaleX = element.Scale;
            this.MapScaleY = element.Scale;
            this.ScalePoint = element.ScaleCenterPoint;
            this.RegionStrokeThickness = 1 / element.Scale;
            try
            {
                _parent.InvokeLoadDataCommand();    
            }
            catch (BreadcrumbsItemNotFoundException ex)
            {
                // todo - куда то пихнуть эксепшен
                SwithToCountryView();
            }
        }

        public void SwithToSonoView(MapRegionElement element)
        {
            _parent.MapStateModel.MapType = MapType.Sono;
            _parent.MapStateModel.RegionId = element.Id;
            try
            {
                _parent.InvokeLoadDataCommand();
            }
            catch (BreadcrumbsItemNotFoundException ex)
            {
                // todo - куда то пихнуть эксепшен
                SwithToDistrictView(element.Parent);
            }
        }
    }
}
