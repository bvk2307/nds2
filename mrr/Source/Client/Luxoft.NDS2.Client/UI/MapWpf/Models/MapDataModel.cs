﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapDataModel
    {
        public MapDataModel()
        {
            ResponseDataNdses = new List<MapResponseDataNds>();
            ResponseDataDiscrepancies = new List<MapResponseDataDiscrepancy>();
        }

        public IEnumerable<MapResponseDataNds> ResponseDataNdses { get; set; }
        public IEnumerable<MapResponseDataDiscrepancy> ResponseDataDiscrepancies { get; set; }
    }
}