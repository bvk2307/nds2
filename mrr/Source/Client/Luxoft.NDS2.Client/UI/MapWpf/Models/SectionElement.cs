﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using CommonComponents.Utils;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class SectionElement : BaseViewModel
    {

        private ReportControlViewModel _parent;
        private MacroReportType _reportType;
        private string _name;
        private ObservableCollection<SubSectionElement> _subSections;
        private SubSectionElement _subSection;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public MacroReportType ReportType
        {
            get { return _reportType; }
            set
            {
                _reportType = value;
                OnPropertyChanged("ReportType");
            }
        }

        public ObservableCollection<SubSectionElement> SubSections
        {
            get { return _subSections; }
            set
            {
                _subSections = value;
                OnPropertyChanged("SubSections");
                if (_subSections != null)
                {
                    _subSections.ForAll(s => s.Section = this);
                    SubSection = _subSections.FirstOrDefault();
                }
            }
        }


        public SubSectionElement SubSection
        {
            get { return _subSection; }
            set
            {
                if (_subSection != value)
                {
                    _subSection = value;
                    OnPropertyChanged("SubSection");
                    if (_subSection != null)
                    {
                        SubSectionChanged(this, _subSection);
                    }
                }
            }
        }
        private void SubSectionChanged(SectionElement sectionElement, SubSectionElement subSection)
        {
            if (_parent.Section == sectionElement)
            {
                _parent.MapStateModel.NdsType = subSection.NdsType;
                _parent.InvokeLoadDataCommand();
            }
        }

        public ReportControlViewModel Parent
        {
            get { return _parent; }
            set
            {
                _parent = value;
                OnPropertyChanged("Parent");
            }
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
