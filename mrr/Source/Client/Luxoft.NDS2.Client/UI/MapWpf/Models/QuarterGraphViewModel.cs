﻿namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class QuarterGraphViewModel
    {
        public string Title { get; set; }
        public decimal Value { get; set; }
        public string ShowValue { get; set; }
        public int TaxPeriod { get; set; }
        public int FiscalYear { get; set; }
        public bool Selected { get; set; }
        public bool IsEmpty { get; set; }
    }
}