﻿

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapDataRegionViewModel
    {
        public string RegionId { get; set; }
        public string RegionName { get; set; }
        public int FiscalYear { get; set; }
        public int Quarter { get; set; }
        public decimal CountTotal { get; set; }
    }
}