﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapDataViewModel : MapStateModel
    {
        public IEnumerable<MapDataRegionViewModel> DataRegionModels { get; set; }
        public IEnumerable<TimeSequenceViewModel> TimeSequenceViewModels { get; set; }
    }
}
