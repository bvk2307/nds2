﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapRootElement: BaseViewModel
    {
        private string _id;
        private string _name;
        private double _scale;
        private string _mapValue;
        private Point _scaleCenterPoint;
        private string _title;
        private Point _centerPosition;
        private bool _isSelected;
        private double _regionFontSize = 16;
        private double _regionCircleFontSize = 12;
        private double _regionCircleSize;
        
        private double _regionHalfCircleSize;
        private MapViewModel _parent;
        private ObservableCollection<MapRegionElement> _children;
        private Path _fullPath;
        private PathFigureCollection _fullPathFigures;
        private SolidColorBrush _rootBrush;

        public ObservableCollection<MapRegionElement> Children
        {
            get { return _children; }
            set
            {
                _children = value;
                if (_children != null)
                {
                    foreach (var mapRegionElement in _children)
                    {
                        mapRegionElement.Parent = this;
                    }
                    //InitFullPath();
                }
            }
        }

        private void InitFullPath()
        {
            CombinedGeometry c1 = null;
            Geometry prev = null;
            foreach (var mapRegionElement in Children)
            {
                var path = new Path();
                path.Data = new PathGeometry(mapRegionElement.Figures.SelectMany(s => s.Path));
                path.RenderTransform = mapRegionElement.RenderTransform;
                path.Stroke = path.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x6b, 0xa6, 0xdc));
                var geometry = path.Data;
                if (c1 == null)
                {
                    c1 = new CombinedGeometry(GeometryCombineMode.Union, geometry, prev);
                }
                else
                {
                    c1 = new CombinedGeometry(GeometryCombineMode.Union, c1, geometry);
                }
                prev = geometry;
            }
            FullPath = new Path {Stroke = Brushes.White, StrokeThickness = 1, Data = c1};
        }

        public string Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public double Scale
        {
            get { return _scale; }
            set
            {
                if (_scale != value)
                {
                    _scale = value;
                    OnPropertyChanged("Scale");
                    RegionFontSize = 16 / _scale;
                    RegionCircleSize = MapViewModel.CircleSize / _scale;
                    RegionHalfCircleSize = MapViewModel.CircleSize / (_scale * 2.5);
                    RegionCircleFontSize = 12/_scale;
                }
            }
        }

        public Point ScaleCenterPoint
        {
            get { return _scaleCenterPoint; }
            set
            {
                if (_scaleCenterPoint != value)
                {
                    _scaleCenterPoint = value;
                    OnPropertyChanged("ScaleCenterPoint");
                }
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    OnPropertyChanged("Title");
                }
            }
        }

        public Point CenterPosition
        {
            get { return _centerPosition; }
            set
            {
                if (_centerPosition != value)
                {
                    _centerPosition = value;
                    OnPropertyChanged("CenterPosition");
                }
            }
        }

        public MapViewModel Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != value)
                {
                    _parent = value;
                    OnPropertyChanged("Parent");
                }
            }
        }
        

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
                foreach (var mapRegionElement in Children)
                {
                    mapRegionElement.ShowLabels = _isSelected;
                    mapRegionElement.PathBrush = this.Parent.ShowRootLabels ? new SolidColorBrush(Color.FromArgb(0xFF, 0x6b, 0xa6, 0xdc)) : (_isSelected ? new SolidColorBrush(Color.FromArgb(0xFF, 0x6b, 0xa6, 0xdc)) : new SolidColorBrush(Colors.Gray));
                }
            }
        }

        public SolidColorBrush RootBrush
        {
            get { return _rootBrush; }
            set
            {
                if (_rootBrush != value)
                {
                    _rootBrush = value;
                    OnPropertyChanged("RootBrush");
                }
            }
        }

        public double RegionFontSize
        {
            get { return _regionFontSize; }
            set
            {
                if (_regionFontSize != value)
                {
                    _regionFontSize = value;
                    OnPropertyChanged("RegionFontSize");
                }
            }
        }

        public double RegionCircleFontSize
        {
            get { return _regionCircleFontSize; }
            set
            {
                if (_regionCircleFontSize != value)
                {
                    _regionCircleFontSize = value;
                    OnPropertyChanged("RegionCircleFontSize");
                }
            }
        }

        public double RegionCircleSize
        {
            get { return _regionCircleSize; }
            set
            {
                if (_regionCircleSize != value)
                {
                    _regionCircleSize = value;
                    OnPropertyChanged("RegionCircleSize");
                }
            }
        }


        public double RegionHalfCircleSize
        {
            get { return _regionHalfCircleSize; }
            set
            {
                if (_regionHalfCircleSize != value)
                {
                    _regionHalfCircleSize = value;
                    OnPropertyChanged("RegionHalfCircleSize");
                }
            }
        }

        public string MapValue
        {
            get { return _mapValue; }
            set
            {
                if (_mapValue != value)
                {
                    _mapValue = value;
                    OnPropertyChanged("MapValue");
                }
            }
        }

        public Path FullPath
        {
            get { return _fullPath; }
            set
            {
                if (_fullPath != value)
                {
                    _fullPath = value;
                    OnPropertyChanged("FullPath");
                }
            }
        }

        public PathFigureCollection FullPathFigures
        {
            get { return _fullPathFigures; }
            set
            {
                if (_fullPathFigures != value)
                {
                    _fullPathFigures = value;
                    OnPropertyChanged("FullPathFigures");
                }
            }
        }
      
    }
}
