﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class NpCollectionGridViewModel : BaseViewModel
    {
        private IEnumerable<FilterSetting> _filterState;
        private IEnumerable<ColumnSortModel> _sortState;
        private MapPresenter _presenter;
        private readonly TableViewModel _viewModel;
        private bool _isBusy;


        public NpCollectionGridViewModel(TableViewModel viewModel)
        {
            _viewModel = viewModel;
            GridDataSource = new QueriableDataSource<NpResponseData>(new List<NpResponseData>(50));
            ExtensionViewModel = new RadGridServerSideViewModel<NpResponseData>(DataProvider);
            TotalItemCount = 0;
            RequestForTotal = true;
        }

        public bool RequestForTotal { get; set; }

        public bool IsBusy
        {
            get { return this._isBusy; }
            set
            {
                if (this._isBusy != value)
                {
                    this._isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        public MapPresenter Presenter
        {
            get { return _presenter; }
            set
            {
                _presenter = value;
                if (_presenter != null)
                {
                    ReloadData();
                }
            }
        }

        #region ExportExcel

        public RadGridServerSideViewModel<NpResponseData> ExtensionViewModel { get; private set; }

        public PageResult<NpResponseData> ReadFromServer(int skip, int take, IEnumerable<FilterSetting> filters = null)
        {
            if (_presenter == null)
                return null;

            return _presenter.GetNpDataFromServer(new NpRequestData
            {
                IfnsId = _viewModel.Parent.MapStateModel.RegionId,
                Year = _viewModel.Parent.MapStateModel.Year,
                Quarter = _viewModel.Parent.MapStateModel.Quarter,
                Skip = skip,
                Take = take,
                Order = SortState,
                Filter = filters,
                WithTotal = RequestForTotal
            });
        }

        #endregion ExportExcel

        #region получение/обновление данных

        private QueriableDataSource<NpResponseData> _data;

        public QueriableDataSource<NpResponseData> GridDataSource
        {
            get { return _data; }
            set
            {
                _data = value;
                base.OnPropertyChanged("GridDataSource");
            }
        }

        public void ReloadData()
        {
            RequestForTotal = true;
            ExtensionViewModel.RefreshData();
        }

        private void DataProvider(PageRequestModel pageRequest)
        {
            if (_presenter == null)
                return;

            var asyncWorker = new AsyncWorker<PageResult<NpResponseData>>();
            asyncWorker.DoWork +=
                (sender, args) =>
                {
                    FilterState = pageRequest.Filters;
                    SortState = pageRequest.Sorting;
                    args.Result = ReadFromServer(pageRequest.PageIndex * pageRequest.PageSize, pageRequest.PageSize, FilterState);
                };

            asyncWorker.Complete +=
                (sender, args) =>
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        if (RequestForTotal)
                        {
                            TotalItemCount = args.Result.TotalAvailable;
                            RequestForTotal = false;
                        }
                        ItemsCount = args.Result.TotalMatches;
                        PageIndex = pageRequest.PageIndex;
                        PageSize = pageRequest.PageSize;
                        PageCount = ItemsCount / PageSize; 
                        GridDataSource.Data = args.Result.Rows;
                    }));
                    IsBusy = false;
                };

            asyncWorker.Failed += (sender, args) =>
            {
                IsBusy = false;
            };
            IsBusy = true;
            asyncWorker.Start();
        }

        #endregion получение/обновление данных

        #region Паджинация

        private int _pageSize = 50;
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                if (_pageSize != value)
                {
                    _pageSize = value;
                    OnPropertyChanged("PageSize");
                    ExtensionViewModel.PageSize = _pageSize;
                }
            }
        }

        private int _itemsCount;
        public int ItemsCount
        {
            get { return _itemsCount; }
            set
            {
                _itemsCount = value;
                OnPropertyChanged("ItemsCount");
            }
        }

        private int _totalCount;
        public int TotalItemCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                OnPropertyChanged("TotalItemCount");
            }
        }

        private int _pageIndex;

        public int PageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("PageIndex");
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged("PageCount");
            }
        }


        #endregion

        #region сортировка

        public IEnumerable<ColumnSortModel> SortState
        {
            get { return _sortState; }
            set
            {
                _sortState = value;
                OnPropertyChanged("SortState");
            }
        }

        #endregion

        #region фильтрация
        public IEnumerable<FilterSetting> FilterState
        {
            get { return _filterState; }
            set
            {
                _filterState = value;
                OnPropertyChanged("FilterState");
            }
        }

        #endregion


        public void RestoreSortState(ColumnSortDescriptor descriptor)
        {
            ExtensionViewModel.InitSortState(new ColumnSortModel { Name = descriptor.Column.UniqueName, SortDirection = descriptor.SortDirection });
            ReloadData();
        }
    }
}
