﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public enum TabDimensionType
    {
        [Display(Name = "")]
        None,
        [Display(Name = "млрд. руб.")]
        Mlrd,
        [Display(Name = "млн. руб.")]
        Mln,
        [Display(Name = "тыс. руб.")]
        Ts,
        [Display(Name = "руб.")]
        R,
        [Display(Name = "тыс. шт.")]
        TsS,
        [Display(Name = "шт.")]
        S,
        [Display(Name = "%")]
        Prc

    }
}
