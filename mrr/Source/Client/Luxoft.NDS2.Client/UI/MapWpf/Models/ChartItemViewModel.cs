﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class ChartItemViewModel : BaseViewModel
    {
        
        private string _name;
        private decimal _value;
        private string _stringValue;
        private bool _isSelected;
        private int _fiscalYear;
        private int _fiscalQuarter;
        private bool _showLabel = true;

        public bool IsSelected
        {
            get
            {
                return this._isSelected;
            }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        public bool ShowLabel
        {
            get
            {
                return this._showLabel;
            }
            set
            {
                if (_showLabel != value)
                {
                    _showLabel = value;
                    OnPropertyChanged("ShowLabel");
                }
            }
        }

        public int FiscalYear
        {
            get { return _fiscalYear; }
            set
            {
                if (_fiscalYear != value)
                {
                    _fiscalYear = value;
                    OnPropertyChanged("FiscalYear");
                }
            }
        }

        public int FiscalQuarter
        {
            get { return _fiscalQuarter; }
            set
            {
                if (_fiscalQuarter != value)
                {
                    _fiscalQuarter = value;
                    OnPropertyChanged("FiscalQuarter");
                }
            }
        }

        public string RepresentativeName
        {
            get
            {
                return this._name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("RepresentativeName");
                }
            }
        }

        public decimal Value
        {
            get
            {
                return this._value;
            }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        public string StringValue
        {
            get
            {
                return this._stringValue;
            }
            set
            {
                if (_stringValue != value)
                {
                    _stringValue = value;
                    OnPropertyChanged("StringValue");
                }
            }
        }
    }
}
