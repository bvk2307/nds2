﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class SubSectionElement : BaseViewModel
    {
        private NdsType _ndsType;
        private string _name;
        private SectionElement _section;
        private string _unit;


        public SectionElement Section
        {
            get { return _section; }
            set
            {
                _section = value;
                OnPropertyChanged("Section");
            }
        }

        public NdsType NdsType
        {
            get { return _ndsType; }
            set
            {
                _ndsType = value;
                OnPropertyChanged("NdsType");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public IList<UnitItem> Units { get; set; }

        public string FormatUnit(MapType mapType)
        {
            return Units.SingleOrDefault(s => s.MapType == mapType).Type.GetDisplayName();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class UnitItem
    {
        public TabDimensionType Type { get; set; }

        public MapType MapType { get; set; }

        public bool WithDecimal { get; set; }
    }
}
