﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using Luxoft.NDS2.Client.UI.MapWpf.Controls;
using Luxoft.NDS2.Client.UI.MapWpf.Exceptions;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class ChartViewModel : BaseViewModel
    {

        public ChartViewModel(ReportControlViewModel parent)
        {
            _parent = parent;
        }

        private readonly ObservableCollection<ChartItemViewModel> _items =
            new ObservableCollection<ChartItemViewModel>();

        private readonly ReportControlViewModel _parent;
        private decimal _maximum;


        public ObservableCollection<ChartItemViewModel> Items
        {
            get { return _items; }
        }

        public decimal Maximum
        {
            get { return _maximum; }
            set
            {
                if (_maximum != value)
                {
                    _maximum = value;
                    OnPropertyChanged("Maximum");
                }
            }
        }

        public void SetTimeSequence(IEnumerable<TimeSequenceViewModel> timeSequenceViewModels,
            MapStateModel mapStateModel)
        {
            this.Items.Clear();
            if (timeSequenceViewModels.Any())
            {
                var max = timeSequenceViewModels.Max(s => Math.Abs(s.CountTotal));
                this.Items.AddRange(timeSequenceViewModels.Select(s => new ChartItemViewModel
                {
                    RepresentativeName = s.GetTitle(),
                    FiscalYear = s.FiscalYear,
                    FiscalQuarter = s.TaxPeriod,
                    Value = max == 0 ? 0 : Math.Abs(100 * s.CountTotal/max) < 10 ? 10 : Math.Abs(100 * s.CountTotal/max),
                    StringValue = s.CountTotal.ToFormat(_parent.Section.SubSection,
                            _parent.MapStateModel.MapType),
                    IsSelected = mapStateModel.Quarter == s.TaxPeriod && mapStateModel.Year == s.FiscalYear
                }));
                this.Maximum = this.Items.Max(s => s.Value) == 0 ? 100 : this.Items.Max(s => s.Value) * 100 / 80;
            }
            else
            {
                this.Maximum = 100;
            }
        }
    }
}
