﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using CommonComponents.Utils;
using DocumentFormat.OpenXml.Office2010.Excel;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Commands;
using Luxoft.NDS2.Client.UI.MapWpf.Converters;
using Luxoft.NDS2.Client.UI.MapWpf.Exceptions;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class ReportControlViewModel : BaseViewModel
    {
        public ReportControlViewModel()
        {
            _map = new MapViewModel(this);
            _chart = new ChartViewModel(this);
            _table = new TableViewModel(this);

            MapStateModel = new MapStateModel();
            MapDataModel = new MapDataModel();
            LoadDataCommand = new LoadDataCommand(this);

            Init();
        }

        private void Init()
        {
            #region InitSections

            Sections = new ObservableCollection<SectionElement>
            {
                new SectionElement
                {
                    Parent = this,
                    ReportType = MacroReportType.Nds,
                    Name = "НДС",
                    SubSections = new ObservableCollection<SubSectionElement>
                    {
                        new SubSectionElement
                        {
                            NdsType = NdsType.ShareDeductionsInComputingNds,
                            Name = "Доля вычетов в исчисленном НДС",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.AmountsOfComputingNds,
                            Name = "Сумма исчисленного НДС",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.AmountOfDeductionsNds,
                            Name = "Сумма вычетов НДС",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.AmountNdsToBeRecovered,
                            Name = "Сумма НДС к возмещению",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.AmountNdsToPayment,
                            Name = "Сумма НДС к уплате",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.NdsSaldo, 
                            Name = "НДС сальдо", 
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mlrd, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.SubmitDeclarationsForCompensation,
                            Name = "Подано деклараций к возмещению",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.TsS, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.S},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.S},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.S}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.SubmitDeclarationsToPayment,
                            Name = "Подано деклараций к уплате",
                             Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.TsS, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.TsS, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.TsS},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                    }
                },
                new SectionElement
                {
                    Parent = this,
                    ReportType = MacroReportType.Discrepancy,
                    Name = "Расхождения",
                    SubSections = new ObservableCollection<SubSectionElement>
                    {
                        new SubSectionElement
                        {
                            NdsType = NdsType.SumOfAllDiscrepancy,
                            Name = "Сумма расхождений всего",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.SumOfDiscrepancyInTears,
                            Name = "Сумма расхождений по разрывам",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.SumOfDiscrepancyInTearsAtInflatedDeduction,
                            Name = "Сумма расхождений по завышению вычета",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.ShareOfTransactionsWithDiscrepancy,
                            Name = "Удельный вес операций с расхождениями в общем количестве операций",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.Prc, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                        new SubSectionElement
                        {
                            NdsType = NdsType.Tno,
                            Name = "Сумма расхождений, отправленных в составе требований и истребований в ТНО",
                            Units =
                                new List<UnitItem>
                                {
                                    new UnitItem {MapType = MapType.Districts, Type = TabDimensionType.Mln, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Regions, Type = TabDimensionType.Ts, WithDecimal = true},
                                    new UnitItem {MapType = MapType.Sono, Type = TabDimensionType.R},
                                    new UnitItem {MapType = MapType.Np, Type = TabDimensionType.None}
                                }
                        },
                    }
                }
            };

            #endregion

            Section = Sections.FirstOrDefault();
        }

        private readonly MapViewModel _map;
        private readonly ChartViewModel _chart;
        private SectionElement _section;
        private BreadcrumbsViewModel _selectedPathItem;
        private readonly TableViewModel _table;
        private bool _forceSelectedPathItemChanged;
        private bool _forceSelectedChartItem;
        private string _bottomText;
        private string _topText;


        private readonly BreadcrumbsViewModel _rootBreadcrumb = new BreadcrumbsViewModel
        {
            Header = "РФ в целом",
            IsCurrent = true,
            MapTypeLevel = MapType.Districts
        };

        private bool _isBusy;
        private MapPresenter _presenter;


        public MapViewModel Map
        {
            get { return _map; }
        }

        public ChartViewModel Chart
        {
            get { return _chart; }
        }

        public TableViewModel Table
        {
            get { return _table; }
        }

        public BreadcrumbsViewModel SelectedPathItem
        {
            get { return _selectedPathItem; }
            set
            {
                if (_selectedPathItem != value)
                {
                    _selectedPathItem = value;
                    OnPropertyChanged("SelectedPathItem");
                    LocateFromBreadcrumbs();
                }
            }
        }

        public BreadcrumbsViewModel RootBreadcrumb
        {
            get { return _rootBreadcrumb; }
        }

        public MapStateModel MapStateModel { get; private set; }

        public MapDataModel MapDataModel { get; private set; }

        public MapDataViewModel MapDataViewModel { get; private set; }

        public LoadDataCommand LoadDataCommand { get; private set; }

        public ObservableCollection<SectionElement> Sections { get; private set; }

        public SectionElement Section
        {
            get { return _section; }
            set
            {
                if (_section != value)
                {
                    _section = value;
                    OnPropertyChanged("Section");
                    if (_section != null)
                    {
                        MapStateModel.ReportType = _section.ReportType;
                        if (_section.SubSection != null)
                        {
                            MapStateModel.NdsType = _section.SubSection.NdsType;
                        }
                        InvokeLoadDataCommand();
                    }
                }
            }
        }

        public string BottomText
        {
            get { return _bottomText; }
            set
            {
                if (_bottomText != value)
                {
                    _bottomText = value;
                    OnPropertyChanged("BottomText");
                }
            }
        }

        public string TopText
        {
            get { return _topText; }
            set
            {
                if (_topText != value)
                {
                    _topText = value;
                    OnPropertyChanged("TopText");
                }
            }
        }

        public RadGridView SecondGrid { get; set; }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy != value)
                {
                    _isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        public MapPresenter Presenter
        {
            get { return _presenter; }
            set
            {
                _presenter = value;
                LoadDataCommand.Presenter = value;
                Table.RowsNp.Presenter = value;
            }
        }

        public void InvokeLoadDataCommand(bool changeSortOrder = true)
        {
            if (LoadDataCommand != null)
            {
                LoadDataCommand.Execute(MapStateModel);
                ChangeViewData();
                ChangeTexts();
                if (changeSortOrder)
                    Table.ChangeSecondSortOdrer(SecondGrid);
            }
        }

        public void LocateFromBreadcrumbs()
        {
            if (_forceSelectedPathItemChanged)
                return;
    
            MapStateModel.MapType = SelectedPathItem.MapTypeLevel;
            if (SelectedPathItem.MapTypeLevel == MapType.Districts)
            {
                Map.SwithToCountryView();
            }
            else if (SelectedPathItem.MapTypeLevel == MapType.Regions)
            {
                var element = Map.Elements.SingleOrDefault(s => s.Id == SelectedPathItem.Id);
                Map.SwithToDistrictView(element);
            }
            else if (SelectedPathItem.MapTypeLevel == MapType.Sono || SelectedPathItem.MapTypeLevel == MapType.Np)
            {
                MapStateModel.RegionId = SelectedPathItem.Id;
                InvokeLoadDataCommand();
            }
        }

        private void ChangeTexts()
        {
            this.BottomText = string.Format("Динамика ({0}) {1}", this.Section.SubSection.Name,
                this.SelectedPathItem.Header);

            var str = this.Section.SubSection.FormatUnit(this.MapStateModel.MapType);
            this.TopText = string.IsNullOrEmpty(str) ? this.Section.SubSection.Name : string.Format("{0} ({1})", this.Section.SubSection.Name, str);
        }

        private void ChangeBreadcrumbs(string currentId, IEnumerable<KeyValuePair<string, string>> children)
        {
            _forceSelectedPathItemChanged = true;
            try
            {
                if (MapStateModel.MapType == MapType.Districts)
                {
                    var items = children.Select(
                        s => new BreadcrumbsViewModel
                        {
                            Id = s.Key,
                            Header = s.Value,
                            PreviewHeader = s.Value,
                            MapTypeLevel = s.Key == "99" ? MapType.Sono : MapType.Regions
                        }).OrderBy(s => s.Header).ToList();

                    // 99 в конец
                    var mi = items.SingleOrDefault(s => s.Id == "99");
                    if (mi != null)
                    {
                        items.Remove(mi);
                        items.Add(mi);
                    }
                    RootBreadcrumb.AddRange(items);
                    SelectedPathItem = RootBreadcrumb;
                }
                else if (MapStateModel.MapType == MapType.Regions)
                {
                    var selected = RootBreadcrumb.Select(currentId);
                    if (selected == null)
                        throw new BreadcrumbsItemNotFoundException(currentId);
                    selected.AddRange(
                        children.Select(
                            s => new BreadcrumbsViewModel
                            {
                                Id = s.Key, 
                                Header = s.Value, 
                                PreviewHeader = s.Value, 
                                MapTypeLevel = MapType.Sono 
                            }));
                    SelectedPathItem = selected;
                }
                else if (MapStateModel.MapType == MapType.Sono)
                {
                    if (MapStateModel.RegionId == "99")
                    {
                        var selected = RootBreadcrumb.Select(currentId);
                        if (selected == null)
                            throw new BreadcrumbsItemNotFoundException(currentId);
                        selected.AddRange(
                            children.Select(
                                s => new BreadcrumbsViewModel
                                {
                                    Id = s.Key, 
                                    Header = string.Format("{0} - {1}", s.Key, s.Value), 
                                    PreviewHeader = s.Value, 
                                    MapTypeLevel = MapType.Np
                                }));
                        SelectedPathItem = selected;
                    }
                    else
                    {
                        var regionSelected = RootBreadcrumb.Selected();
                        if (regionSelected == null)
                            throw new BreadcrumbsItemNotFoundException("?");
                        var selected = regionSelected.Select(currentId);
                        if (selected == null)
                            throw new BreadcrumbsItemNotFoundException(currentId);
                        selected.AddRange(
                            children.Select(
                                s => new BreadcrumbsViewModel
                                {
                                    Id = s.Key, 
                                    Header = string.Format("{0} - {1}", s.Key, s.Value), 
                                    PreviewHeader = s.Value, 
                                    MapTypeLevel = MapType.Np
                                }));
                        SelectedPathItem = selected;    
                    }
                }
                else if (MapStateModel.MapType == MapType.Np)
                {
                    var regionSelected = RootBreadcrumb.Selected();
                    if (regionSelected == null)
                        throw new BreadcrumbsItemNotFoundException("?");

                    BreadcrumbsViewModel sonoSelected = null;
                    if (regionSelected.MapTypeLevel == MapType.Sono)
                    {
                        sonoSelected = regionSelected;
                    }
                    else
                    {
                        sonoSelected = regionSelected.Selected();
                        if (sonoSelected == null)
                            throw new BreadcrumbsItemNotFoundException("?");
                    }

                    var selected = sonoSelected.Select(currentId);
                    if (selected == null)
                        throw new BreadcrumbsItemNotFoundException(currentId);
                    SelectedPathItem = selected;
                }
            }
            finally
            {
                _forceSelectedPathItemChanged = false;
            }
        }

        private void FillChartData(MapDataViewModel mapDataViewModel)
        {
            if (mapDataViewModel == null)
                return;
            _forceSelectedChartItem = true;
            try
            {
                Chart.SetTimeSequence(mapDataViewModel.TimeSequenceViewModels, MapStateModel);
            }
            finally
            {
                _forceSelectedChartItem = false;
            }
        }

        private void ChangeViewData()
        {
            if (MapStateModel.MapType != MapType.Np && MapStateModel.MapType != MapType.Graph)
            {
                MapDataViewModel mapDataViewModel;
                switch (Section.ReportType)
                {
                    case MacroReportType.Nds:
                        {
                            var mapDataRegionViewModel = MapDataModel.ResponseDataNdses.ToMapDataRegionViewModel(MapStateModel);
                            var existingRegions = mapDataRegionViewModel.GroupBy(m => m.RegionId)
                                .Select(group => group.First()).Select(
                                q => new KeyValuePair<string, string>(q.RegionId, q.RegionName)).OrderBy(s => int.Parse(s.Key));

                            ChangeBreadcrumbs(MapStateModel.RegionId, existingRegions);
                            mapDataViewModel = MapStateModel.ToMapDataViewModel(mapDataRegionViewModel, MapDataModel.ResponseDataNdses.ToSequenceViewModel(MapStateModel));
                        }
                        break;
                    case MacroReportType.Discrepancy:
                        {
                            var mapDataRegionViewModel = MapDataModel.ResponseDataDiscrepancies.ToMapDataRegionViewModel(MapStateModel);
                            var existingRegions = mapDataRegionViewModel.GroupBy(m => m.RegionId)
                                .Select(group => group.First()).Select(
                                    q => new KeyValuePair<string, string>(q.RegionId, q.RegionName))
                                .OrderBy(s => int.Parse(s.Key));

                            ChangeBreadcrumbs(MapStateModel.RegionId, existingRegions);
                            mapDataViewModel = MapStateModel.ToMapDataViewModel(mapDataRegionViewModel, MapDataModel.ResponseDataDiscrepancies.ToSequenceViewModel(MapStateModel));
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (mapDataViewModel.DataRegionModels.Any())
                {
                    if (MapStateModel.MapType == MapType.Districts || MapStateModel.MapType == MapType.Regions)
                    {
                        Map.SetRegionCounts(mapDataViewModel.DataRegionModels);
                    }
                    else
                    {
                        Table.SetTableModel(mapDataViewModel.DataRegionModels);
                    }
                    
                    FillChartData(mapDataViewModel);
                }
                else
                {
                    if (MapStateModel.MapType != MapType.Districts)
                    {
                        // нет данных в указанном разделе - уйти на шаг назад
                        var parent = SelectedPathItem.Parent;
                        SelectedPathItem.Children.Clear();
                        if (MapStateModel.MapType == MapType.Sono)
                        {
                            MapStateModel.MapType = MapType.Regions;
                        }
                        else if (MapStateModel.MapType == MapType.Regions)
                        {
                            MapStateModel.MapType = MapType.Districts;
                        }
                        Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => ChangeSelectedPathItem(parent)));
                        DialogHelper.Inform("Данные за выбранный квартал отсутствуют. Выберите другой период");
                    }
                    else
                    {
                        Map.SetRegionCounts(mapDataViewModel.DataRegionModels);
                        FillChartData(mapDataViewModel);
                    }
                }
                MapDataViewModel = mapDataViewModel;
            }
            else
            {
                if (MapStateModel.MapType == MapType.Np)
                {
                    Table.RowsNp.ReloadData();
                    Table.ChangeSelectedTabText();
                    FillChartData(MapDataViewModel);
                }
                else
                {
                    MapStateModel.MapType = MapType.Np;
                }
                ChangeBreadcrumbs(MapStateModel.RegionId, null);
            }
        }

        private void ChangeSelectedPathItem(BreadcrumbsViewModel selected)
        {
            Thread.Sleep(200);
            SelectedPathItem = selected;
        }

        public void ChangeViewPeriod(int fiscalYear, int fiscalQuarter)
        {
            if (_forceSelectedChartItem)
                return;

            MapStateModel.Year = fiscalYear;
            MapStateModel.Quarter = fiscalQuarter;
            InvokeLoadDataCommand(false);
        }
    }
}
