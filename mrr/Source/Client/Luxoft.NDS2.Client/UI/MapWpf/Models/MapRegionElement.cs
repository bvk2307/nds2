﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;

namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class MapRegionElement: BaseViewModel
    {
        private string _id;
        private string _name;
        private string _title;
        private Point _centerPosition;
        private bool _showLabels;
        private MapRootElement _parent;
        private string _mapValue = "Н/Д";
        private SolidColorBrush _pathBrush = new SolidColorBrush(Colors.LightSteelBlue);
        private ObservableCollection<MapPath> _figures;
        private bool _showSmallCircle;
        private Point _circlePosition;

        public string Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }


        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }
        public Point CenterPosition
        {
            get { return _centerPosition; }
            set
            {
                if (_centerPosition != value)
                {
                    _centerPosition = value;
                    OnPropertyChanged("CenterPosition");
                }
            }
        }

        /// <summary>
        /// Маленький кружечкек
        /// </summary>
        public Point CirclePosition
        {
            get { return _circlePosition; }
            set
            {
                if (_circlePosition != value)
                {
                    _circlePosition = value;
                    OnPropertyChanged("CirclePosition");
                }
            }
        }

        public MapRootElement Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != value)
                {
                    _parent = value;
                    OnPropertyChanged("Parent");
                }
            }
        }

        public System.Windows.Media.Transform RenderTransform { get; set; }

        public SolidColorBrush PathBrush
        {
            get { return _pathBrush; }
            set
            {
                if (_pathBrush != value)
                {
                    _pathBrush = value;
                    OnPropertyChanged("PathBrush");
                }
            }
        }

        public ObservableCollection<MapPath> Figures
        {
            get { return _figures; }
            set
            {
                _figures = value;
                if (_figures != null)
                {
                    foreach (var path in _figures)
                    {
                        path.Parent = this;
                    }
                }
            }
        }

        public string MapValue
        {
            get { return _mapValue; }
            set
            {
                if (_mapValue != value)
                {
                    _mapValue = value;
                    OnPropertyChanged("MapValue");
                }
            }
        }

        public bool ShowLabels
        {
            get { return _showLabels; }
            set
            {
                if (_showLabels != value)
                {
                    _showLabels = value;
                    OnPropertyChanged("ShowLabels");
                    ShowSmallCircle = _showLabels && !CirclePosition.IsEmpty();
                }
            }
        }

        public bool ShowSmallCircle
        {
            get { return _showSmallCircle; }
            set
            {
                if (_showSmallCircle != value)
                {
                    _showSmallCircle = value;
                    OnPropertyChanged("ShowSmallCircle");
                }
            }
        }
    }
}
