﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Controls;
using Luxoft.NDS2.Client.UI.MapWpf.Exceptions;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;


namespace Luxoft.NDS2.Client.UI.MapWpf.Models
{
    public class TableViewModel : BaseViewModel, IClerableModel, IPagedModel
    {

        public TableViewModel(ReportControlViewModel parent)
        {
            _parent = parent;
            _npCollection = new NpCollectionGridViewModel(this);
        }

        private readonly ObservableCollection<TnoViewModel> _rows = new ObservableCollection<TnoViewModel>();
        private readonly NpCollectionGridViewModel _npCollection;
        private readonly ReportControlViewModel _parent;
        private string _selectedTabText;

        public Func<bool> SecondGridNextPageAction { get; set; }

        public Action SecondGridFirstPageAction { get; set; }

        public Func<int, bool> GoPageAction { get; set; }

        public ReportControlViewModel Parent
        {
            get { return _parent; }
        }


        public ObservableCollection<TnoViewModel> Rows
        {
            get { return _rows; }
        }


        public NpCollectionGridViewModel RowsNp
        {
            get { return _npCollection; }
        }

        public string SelectedTabText
        {
            get { return _selectedTabText; }
            set
            {
                if (_selectedTabText != value)
                {
                    _selectedTabText = value;
                    OnPropertyChanged("SelectedTabText");
                }
            }
        }

        public void SetTableModel(IEnumerable<MapDataRegionViewModel> rows)
        {
            _rows.Clear();
            _rows.AddRange(rows.Select(s=> new TnoViewModel
                {
                    TnoId = s.RegionId.ToString(),
                    TnoName = s.RegionName,
                    CountTotal = s.CountTotal
                }));
            ChangeSelectedTabText();
        }

        public void ChangeSelectedTabText()
        {
            this.SelectedTabText = _parent.Section.SubSection.Name;
        }

        public bool NextPage()
        {
            if (SecondGridNextPageAction != null)
                return SecondGridNextPageAction();
            return false;
        }

        public void FirstPage()
        {
            if (SecondGridFirstPageAction != null)
                SecondGridFirstPageAction();
        }

        public bool GoPage(int pageIndex)
        {
            if (GoPageAction != null)
                return GoPageAction(pageIndex);
            return false;
        }

        public int PageIndex { get { return this._npCollection.PageIndex; } }
        public int PageCount { get { return this._npCollection.PageCount; } }

        public ICollection PageData(int pageIndex)
        {
            return this._npCollection.ReadFromServer(this._npCollection.PageSize * pageIndex,
               this._npCollection.PageSize).Rows;
        }


        public void RestoreState(GridViewDataControl gridView)
        {
            if (gridView.Name == "FirstGrid")
            {
                // какая сортировка?
            }
            else if (gridView.Name == "SecondGrid")
            {
                gridView.SortDescriptors.Clear();
                var csd = new ColumnSortDescriptor()
                {
                    Column = gridView.Columns[GridViewDataColumnSortExtension.GetColumnName(this.Parent.MapStateModel.NdsType)],
                    SortDirection = GridViewDataColumnSortExtension.GetDirection(this.Parent.MapStateModel.NdsType)
                };
                gridView.SortDescriptors.Add(csd);
                _npCollection.ExtensionViewModel.CustomFilterChanged(gridView, false);
                _npCollection.RestoreSortState(csd);
            }
        }

        public bool CanRestoreState(GridViewDataControl gridView)
        {
            if (gridView.Name == "FirstGrid")
            {
                
            }
            else if (gridView.Name == "SecondGrid")
            {
                return !gridView.IsSorted(GridViewDataColumnSortExtension.GetColumnName(this.Parent.MapStateModel.NdsType), GridViewDataColumnSortExtension.GetDirection(this.Parent.MapStateModel.NdsType));
            }
            return false;
        }

        public void ChangeSecondSortOdrer(GridViewDataControl gridView)
        {
            if (gridView == null)
                return;

            if (CanRestoreState(gridView))
            {
                RestoreState(gridView);
            }
        }
    }
}
