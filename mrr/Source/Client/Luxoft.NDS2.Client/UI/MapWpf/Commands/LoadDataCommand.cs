﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Luxoft.NDS2.Client.UI.MapWpf.Controls;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Commands
{
    public class LoadDataCommand: ICommand
    {

        public LoadDataCommand(ReportControlViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        private ReportControlViewModel _viewModel;
        private MapPresenter _presenter;

        public MapPresenter Presenter
        {
            get { return _presenter; }
            set { _presenter = value; }
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            var request = (MapStateModel) parameter;
            if (_presenter == null)
                return;

            if (request.MapType != MapType.Np)
            {
                if (request.MapType == MapType.Regions && request.RegionId == "99")
                {
                    request.MapType++;
                }
                switch (request.ReportType)
                {
                    case MacroReportType.Nds:
                        _viewModel.MapDataModel.ResponseDataNdses = _presenter.GetNdsMapRegionViewModel(request);
                        break;
                    case MacroReportType.Discrepancy:
                        _viewModel.MapDataModel.ResponseDataDiscrepancies = _presenter.GetDiscrepancyMapRegionViewModel(request);
                        break;
                    default:
                        return;
                }
            }
        }

    }
}
