﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;

namespace Luxoft.NDS2.Client.UI.MapWpf.Extensions
{
    public static class XElementExtension
    {
        public static TValue XAttribute<TValue>(this XElement element, string attributeName)
        {
            var value = element.Attributes().Any(a => a.Name == attributeName)
                ? element.Attribute(attributeName).Value
                : "";

            if (string.IsNullOrEmpty(value))
                return default(TValue);

            if (typeof(TValue) == typeof(Point))
            {
                return (TValue)(object)Point.Parse(value);
            }
            if (typeof (TValue) == typeof (Transform))
            {
                return (TValue)(object)Transform.Parse(value);
            }
            else if (typeof(TValue) == typeof(PathFigureCollection))
            {
                return (TValue)(object)PathFigureCollection.Parse(value);
            }
            return (TValue)Convert.ChangeType(value, typeof(TValue), new CultureInfo("en-US"));
        }
    }
}
