﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using CommonComponents.Utils;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Client.UI.MapWpf.Models;

namespace Luxoft.NDS2.Client.UI.MapWpf.Extensions
{
    public static class ModelsExtensions
    {
        public static IEnumerable<MapDataRegionViewModel> ToMapDataRegionViewModel(
            this IEnumerable<MapResponseDataNds> mapResponseDatal
            , MapStateModel mapStateModel)
        {
            return mapResponseDatal.GroupBy(m => new { AggregateCode = m.AGGREGATE_CODE, AggregateName = m.AGGREGATE_NAME, Year = m.FISCAL_YEAR, Quarter = m.QUARTER }).Select(q => new MapDataRegionViewModel
            {
                RegionId = q.Key.AggregateCode,
                RegionName = q.Key.AggregateName,
                FiscalYear = q.Key.Year,
                Quarter = q.Key.Quarter,
                CountTotal = GetNdsTotalCount(mapStateModel, q.ToList())
            });
        }

        public static IEnumerable<MapDataRegionViewModel> ToMapDataRegionViewModel(
            this IEnumerable<MapResponseDataDiscrepancy> mapResponseDatal
            , MapStateModel mapStateModel)
        {
            return mapResponseDatal.GroupBy(m => new { AggregateCode = m.AGGREGATE_CODE, AggregateName = m.AGGREGATE_NAME, Year = m.FISCAL_YEAR, Quarter = m.QUARTER }).Select(q => new MapDataRegionViewModel
            {
                RegionId = q.Key.AggregateCode,
                RegionName = q.Key.AggregateName,
                FiscalYear = q.Key.Year,
                Quarter = q.Key.Quarter,
                CountTotal = GetDiscrepancyTotalCount(mapStateModel, q)
            });
        }

        public static IEnumerable<TimeSequenceViewModel> ToSequenceViewModel(
            this IEnumerable<MapResponseDataNds> mapResponseDatal
            , MapStateModel mapStateModel)
        {
            return mapResponseDatal.GroupBy(m => new { Year = m.FISCAL_YEAR, Quarter = m.QUARTER }).Select(q => new TimeSequenceViewModel
            {
                FiscalYear = q.Key.Year,
                TaxPeriod = q.Key.Quarter,
                CountTotal = GetNdsTotalCount(mapStateModel, q.ToList())
            });
        }

        public static IEnumerable<TimeSequenceViewModel> ToSequenceViewModel(
            this IEnumerable<MapResponseDataDiscrepancy> mapResponseDatal
            , MapStateModel mapStateModel)
        {
            return mapResponseDatal.GroupBy(m => new { Year = m.FISCAL_YEAR, Quarter = m.QUARTER }).Select(q => new TimeSequenceViewModel
            {
                FiscalYear = q.Key.Year,
                TaxPeriod = q.Key.Quarter,
                CountTotal = GetDiscrepancyTotalCount(mapStateModel, q)
            });
        }

        //public static IEnumerable<NpViewModel> ToNpDataViewModel(this IEnumerable<NpResponseData> mapResponseDatal)
        //{
        //    return mapResponseDatal.Select(q => new NpViewModel
        //    {
        //        Inn = q.INN,
        //        Kpp = q.KPP,
        //        TaxName = q.TAX_NAME,
        //        NdsCalculated = q.NDS_CALCULATED,
        //        NdsDeduction = q.NDS_DEDUCTION,
        //        ShareDeductions = q.NDS_CALCULATED != 0 ? q.NDS_DEDUCTION / q.NDS_CALCULATED * 100 : 0,
        //        NdsPayableOrRefundable = q.NDS_TOTAL,
        //        GapSum = q.GAP_SUM,
        //    });
        //}

        public static MapDataViewModel ToMapDataViewModel(
            this MapStateModel mapStateModel,
            IEnumerable<MapDataRegionViewModel> dataRegionViewModels,
            IEnumerable<TimeSequenceViewModel> timeSequenceViewModels)
        {
            return new MapDataViewModel
            {
                MapType = mapStateModel.MapType,
                NdsType = mapStateModel.NdsType,
                Quarter = mapStateModel.Quarter,
                RegionId = mapStateModel.RegionId,
                DataRegionModels = dataRegionViewModels.Where(r => r.FiscalYear == mapStateModel.Year && r.Quarter == mapStateModel.Quarter).OrderBy(r => r.RegionId),
                TimeSequenceViewModels = timeSequenceViewModels.OrderBy(t => t.FiscalYear).ThenBy(t => t.TaxPeriod)
            };
        }

        private static decimal GetNdsTotalCount(MapStateModel mapStateModel, IList<MapResponseDataNds> mapResponseDatal)
        {
            switch (mapStateModel.NdsType)
            {
                case NdsType.ShareDeductionsInComputingNds:
                    var sum = mapResponseDatal.Sum(m => m.NDS_CALCULATED_SUM);
                    return sum != 0 ? Math.Round(mapResponseDatal.Sum(m => m.NDS_DEDUCTION_SUM) / sum * 100, 2) : 0;
                case NdsType.AmountsOfComputingNds:
                    return mapResponseDatal.Sum(m => m.NDS_CALCULATED_SUM);
                case NdsType.AmountOfDeductionsNds:
                    return mapResponseDatal.Sum(m => m.NDS_DEDUCTION_SUM);
                case NdsType.AmountNdsToBeRecovered:
                    return mapResponseDatal.Sum(m => m.NDS_COMPENSATION_SUM);
                case NdsType.AmountNdsToPayment:
                    return mapResponseDatal.Sum(m => m.NDS_PAYMENT_SUM);
                case NdsType.NdsSaldo:
                    return mapResponseDatal.Sum(m => m.NDS_PAYMENT_SUM) + mapResponseDatal.Sum(m => m.NDS_COMPENSATION_SUM);
                case NdsType.SubmitDeclarationsForCompensation:
                    return mapResponseDatal.Sum(m => m.DECL_COUNT_COMPENSATION);
                case NdsType.SubmitDeclarationsToPayment:
                    return mapResponseDatal.Sum(m => m.DECL_COUNT_PAYMENT);
                default:
                    return 0;
            }
        }

        private static decimal GetDiscrepancyTotalCount(MapStateModel mapStateModel, IEnumerable<MapResponseDataDiscrepancy> mapResponseDatal)
        {
            switch (mapStateModel.NdsType)
            {
                case NdsType.SumOfAllDiscrepancy:
                    return mapStateModel.MapType == MapType.Sono ? Math.Round(mapResponseDatal.Sum(m => m.MISMATCH_TOTAL_SUM), MidpointRounding.AwayFromZero) : mapResponseDatal.Sum(m => m.MISMATCH_TOTAL_SUM);
                case NdsType.SumOfDiscrepancyInTears:
                    return mapStateModel.MapType == MapType.Sono ? Math.Round(mapResponseDatal.Sum(m => m.GAP_AMOUNT), MidpointRounding.AwayFromZero) : mapResponseDatal.Sum(m => m.GAP_AMOUNT);
                case NdsType.SumOfDiscrepancyInTearsAtInflatedDeduction:
                    return mapStateModel.MapType == MapType.Sono ? Math.Round(mapResponseDatal.Sum(m => m.MISMATCH_NDS), MidpointRounding.AwayFromZero) : mapResponseDatal.Sum(m => m.MISMATCH_NDS);
                case NdsType.Tno:
                    return mapResponseDatal.Sum(m => m.MISMATCH_TNO_SUM);
                case NdsType.ShareOfTransactionsWithDiscrepancy:
                    return Math.Round(mapResponseDatal.Sum(m => m.WEIGHT_MISMATCH_IN_TOTAL_OPER) / mapResponseDatal.Count(), 2);
                default:
                    return 0;
            }
        }

        public static BreadcrumbsViewModel Select(this BreadcrumbsViewModel parent, string id)
        {
            foreach (var breadcrumbsViewModel in parent.Children)
            {
                breadcrumbsViewModel.IsCurrent = breadcrumbsViewModel.Id == id;
            }
            return parent.Children.FirstOrDefault(s => s.IsCurrent);
        }

        public static BreadcrumbsViewModel Selected(this BreadcrumbsViewModel parent)
        {
            return parent.Children.FirstOrDefault(s => s.IsCurrent);
        }

        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                collection.Add(item);
            }
        }

        public static void AddRange(this BreadcrumbsViewModel parent, IEnumerable<BreadcrumbsViewModel> items)
        {
            parent.Children.Clear();
            foreach (var item in items)
            {
                parent.Children.Add(item);
                item.Parent = parent;
            }
        }
    }
}
