﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Extensions
{
    public class GridViewDataColumnSortExtension
    {
        public static string GetColumnName(NdsType ndsType)
        {
            switch (ndsType)
            {
                case NdsType.ShareDeductionsInComputingNds:
                {
                    return "SHARE_DEDUCTIONS";
                }
                case NdsType.AmountsOfComputingNds:
                {
                    return "NDS_CALCULATED";
                }
                case NdsType.AmountOfDeductionsNds:
                {
                    return "NDS_DEDUCTION";
                }
                case NdsType.AmountNdsToBeRecovered:
                {
                    return "NDS_TOTAL";
                }
                case NdsType.AmountNdsToPayment:
                {
                    return "NDS_TOTAL";
                }
                case NdsType.NdsSaldo:
                {
                    return "NDS_TOTAL";
                }
                case NdsType.SubmitDeclarationsForCompensation:
                {
                    return "NDS_TOTAL";
                }
                case NdsType.SubmitDeclarationsToPayment:
                {
                    return "NDS_TOTAL";
                }
                case NdsType.SumOfAllDiscrepancy:
                {
                    return "GAP_SUM";
                }
                case NdsType.SumOfDiscrepancyInTears:
                {
                    return "GAP_SUM";
                }
                case NdsType.SumOfDiscrepancyInTearsAtInflatedDeduction:
                {
                    return "GAP_SUM";
                }
                case NdsType.ShareOfTransactionsWithDiscrepancy:
                {
                    return "GAP_SUM";
                }
                case NdsType.Tno:
                {
                    return "GAP_SUM";
                }
            }
            return "";
        }

        public static ListSortDirection GetDirection(NdsType ndsType)
        {
            switch (ndsType)
            {
                case NdsType.ShareDeductionsInComputingNds:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.AmountsOfComputingNds:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.AmountOfDeductionsNds:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.AmountNdsToBeRecovered:
                    {
                        return ListSortDirection.Ascending;
                    }
                case NdsType.AmountNdsToPayment:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.NdsSaldo:
                    {
                        return ListSortDirection.Ascending;
                    }
                case NdsType.SubmitDeclarationsForCompensation:
                    {
                        return ListSortDirection.Ascending;
                    }
                case NdsType.SubmitDeclarationsToPayment:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.SumOfAllDiscrepancy:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.SumOfDiscrepancyInTears:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.SumOfDiscrepancyInTearsAtInflatedDeduction:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.ShareOfTransactionsWithDiscrepancy:
                    {
                        return ListSortDirection.Descending;
                    }
                case NdsType.Tno:
                    {
                        return ListSortDirection.Descending;
                    }
            }
            return ListSortDirection.Ascending;
        }
    }
}
