﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml.Drawing;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Extensions
{
    public static class TypeExtensions
    {
        public static int ToQuarter(this DateTime dateTime, ref int year)
        {
            var quarter = ((DateTime.Now.Month + 2) / 3) - 1;
            var returnValue = 0;
            if (quarter == 0)
            {
                returnValue = 4;
                year = year - 1;
            }
            else
            {
                returnValue = quarter;
            }
            return returnValue;
        }

        public static string ToFormat(this decimal count, SubSectionElement dimension, MapType mapType)
        {
            var type = dimension.Units.SingleOrDefault(s => s.MapType == mapType);
            string decFormat = "#,##0.00";
            string intFormat = "#,##0";

            switch (type.Type)
            {
                case TabDimensionType.Mlrd:
                {
                    return (count / 1000000000).ToString(type.WithDecimal ? decFormat : intFormat);
                }
                case TabDimensionType.Mln:
                {
                    return (count / 1000000).ToString(type.WithDecimal ? decFormat : intFormat);
                }
                case TabDimensionType.Ts:
                {
                    return (count / 1000).ToString(type.WithDecimal ? decFormat : intFormat);
                }
                case TabDimensionType.Prc:
                {
                    return (count).ToString(type.WithDecimal ? decFormat : intFormat) +" %";
                }
                case TabDimensionType.R:
                {
                    return (count).ToString(type.WithDecimal ? decFormat : intFormat);
                }
                case TabDimensionType.TsS:
                {
                    return (count / 1000).ToString(type.WithDecimal ? decFormat : intFormat);
                }
                case TabDimensionType.S:
                {
                    return (count).ToString(type.WithDecimal ? decFormat : intFormat);
                }
            }
            return (count).ToString(type.WithDecimal ? decFormat : intFormat);
        }


        public static bool IsEmpty(this System.Windows.Point point)
        {
            return point.X == 0  && point.Y == 0;
        }
    }
}
