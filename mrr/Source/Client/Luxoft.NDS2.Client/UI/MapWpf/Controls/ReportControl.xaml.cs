﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Luxoft.NDS2.Client.UI.Base.Wpf.Commands;
using Luxoft.NDS2.Client.UI.Base.Wpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Commands;
using Luxoft.NDS2.Client.UI.MapWpf.Exceptions;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.GridView.SearchPanel;

namespace Luxoft.NDS2.Client.UI.MapWpf.Controls
{
    /// <summary>
    /// Interaction logic for ReportControl.xaml
    /// </summary>
    public partial class ReportControl : UserControl
    {

        public ReportControl()
        {
            this.DataContext = new ReportControlViewModel();
            InitializeComponent();

            DataContextModel.SecondGrid = this.SecondGrid;
            DataContextModel.Table.SecondGridNextPageAction = () => SecondGridPager.CanMoveToNextPage && SecondGridPager.MoveToNextPage();
            DataContextModel.Table.SecondGridFirstPageAction = () => SecondGridPager.MoveToFirstPage();
            DataContextModel.Table.RowsNp.PropertyChanged += RowsNp_PropertyChanged;
            this.FindPanelSecondGrid.SearchColumns = new List<string> { "INN", "TAX_NAME" };
        }

        void RowsNp_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "PageSize")
            {
                SecondGridPager.PageSize = DataContextModel.Table.RowsNp.PageSize;
            }
        }

        private bool _searchApplied;

        public ReportControlViewModel DataContextModel
        {
            get { return this.DataContext as ReportControlViewModel; }
        }

        public MapPresenter Presenter
        {
            get { return DataContextModel.Presenter; }
            set
            {
                DataContextModel.Presenter = value;
                DataContextModel.InvokeLoadDataCommand();
            }
        }

        private void Table_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var originalSender = e.OriginalSource as FrameworkElement;
            if (originalSender != null)
            {
                var row = originalSender.ParentOfType<GridViewRow>();
                if (row != null && row.DataContext != null)
                {
                    var newId = ((TnoViewModel)row.DataContext).TnoId;
                    var oldId = DataContextModel.MapStateModel.RegionId;
                    DataContextModel.MapStateModel.MapType = MapType.Np;
                    DataContextModel.MapStateModel.RegionId = newId;
                    try
                    {
                        DataContextModel.InvokeLoadDataCommand();
                    }
                    catch (BreadcrumbsItemNotFoundException ex)
                    {
                        // todo - куда то пихнуть эксепшен
                        DataContextModel.MapStateModel.MapType = MapType.Sono;
                        DataContextModel.MapStateModel.RegionId = oldId;
                        DataContextModel.InvokeLoadDataCommand();
                    }
                }
            }

        }

        private void TableNa_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var originalSender = e.OriginalSource as FrameworkElement;
            if (originalSender != null)
            {
                var row = originalSender.ParentOfType<GridViewRow>();
                if (row != null)
                {
                    Dispatcher.Invoke(new Action(() => Presenter.CreateGreaph(((NpResponseData)row.DataContext).INN, DataContextModel.MapStateModel.Year, DataContextModel.MapStateModel.Quarter)));
                }
            }

        }

        private void ChartSelectionBehavior_OnSelectionChanged(object sender, ChartSelectionChangedEventArgs e)
        {
            var points = e.AddedPoints;
            if (points.Count == 0)
                return;
            var model = (ChartItemViewModel)points[0].DataItem;
            DataContextModel.ChangeViewPeriod(model.FiscalYear, model.FiscalQuarter);
        }

        private void FirstGrid_OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                var clearSearchValue = GridViewFindCommands.RestoreStateCommand;
                clearSearchValue.Execute(null, this.FirstGrid.ChildrenOfType<GridViewFindPanel>().FirstOrDefault());
            }
        }

        private void SecondGrid_OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var clearSearchValue = GridViewFindCommands.RestoreStateCommand as RoutedUICommand;
            clearSearchValue.Execute(null, this.SecondGrid.ChildrenOfType<GridViewFindPanel>().FirstOrDefault());
            if ((bool)e.NewValue)
            {
                SecondGridSortedChanged(false);
            }
        }

        public void SecondGridSortedChanged(bool reload = true)
        {
            if (reload)
                DataContextModel.Table.RowsNp.ReloadData();
        }
    }
}
