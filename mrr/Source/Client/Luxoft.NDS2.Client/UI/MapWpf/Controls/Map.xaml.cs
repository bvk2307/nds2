﻿using System.Globalization;
using System.Windows;
using System.Windows.Input;
using Luxoft.NDS2.Client.UI.MapWpf.Models;


namespace Luxoft.NDS2.Client.UI.MapWpf.Controls
{
    /// <summary>
    /// Interaction logic for Map.xaml
    /// </summary>
    public partial class Map
    {

        public Map()
        {
            InitializeComponent();
        }

        public MapViewModel Model
        {
            get { return (MapViewModel) DataContext; }
        }



        void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 3)
            {
                var rc = sender as RegionCanvas;
                if (Model.MapLevel == MapLevel.Region && rc != null)
                {
                    var p = e.GetPosition(MapCanvas);
                    ((MapRegionElement) rc.DataContext).CenterPosition = p;
                }
            }
            else if (e.ClickCount == 2)
            {
                var c = sender as RootCanvas;
                if (Model.MapLevel == MapLevel.Country && c != null)
                {
                    this.Model.SwithToDistrictView((MapRootElement) c.DataContext);
                }
                else
                {
                    var rc = sender as RegionCanvas;
                    if (Model.MapLevel == MapLevel.Region && rc != null)
                    {
                        this.Model.SwithToSonoView((MapRegionElement) rc.DataContext);
                    }
                }
            }
            
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.Model.Submit2();
        }
    }
}
