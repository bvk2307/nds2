﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Wpf.Extensions;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.MapWpf
{
    public class MapPresenter : BasePresenter<MapView>
    {
        public void CreateGreaph(string inn, int year, int quarter, bool isPurchase = true)
        {
            var cardData = new GraphTreeParameters(new TaxPayerId(inn, null), year, quarter, isPurchase);
            ViewPyramidReport(cardData);
        }

        public IEnumerable<MapResponseDataNds> GetNdsMapRegionViewModel(MapStateModel mapStateModel)
        {
            string errorStr;
            var model = GetMapDataNdsFromServer(new MapRequestData
            {
                MapType = mapStateModel.MapType,
                RegionId = mapStateModel.RegionId,
                NdsType = mapStateModel.NdsType,
                ReportType = mapStateModel.ReportType
            }, out errorStr);

            if (!String.IsNullOrEmpty(errorStr))
            {
                View.ShowError(string.Format("{0}. {1}", ResourceManagerNDS2.MacroReportMessages.ERROR_GET_MAP_DATA, errorStr));
                return new List<MapResponseDataNds>();
            }
            return model;
        }

        public IEnumerable<MapResponseDataDiscrepancy> GetDiscrepancyMapRegionViewModel(MapStateModel mapStateModel)
        {
            string errorStr;
            var model = GetMapDataDiscrepancyFromServer(new MapRequestData
            {
                MapType = mapStateModel.MapType,
                RegionId = mapStateModel.RegionId,
                NdsType = mapStateModel.NdsType,
                ReportType = mapStateModel.ReportType
            }, out errorStr);

            if (!String.IsNullOrEmpty(errorStr))
            {
                View.ShowError(string.Format("{0}. {1}", ResourceManagerNDS2.MacroReportMessages.ERROR_GET_MAP_DATA, errorStr));
                return new List<MapResponseDataDiscrepancy>();
            }
            return model;
        }

        private IEnumerable<MapResponseDataNds> GetMapDataNdsFromServer(MapRequestData model, out string errorMessage)
        {
            var errMes = String.Empty;
            var blService = GetServiceProxy<IMacroReportDataService>();

            var ret = new List<MapResponseDataNds>();
            ExecuteServiceCall(
                () => blService.GetMapDataNds(model),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    ret = null;
                    errMes = resultErr.Message;
                });
            errorMessage = errMes;
            return ret;
        }

        private IEnumerable<MapResponseDataDiscrepancy> GetMapDataDiscrepancyFromServer(MapRequestData model, out string errorMessage)
        {
            var errMes = String.Empty;
            var blService = GetServiceProxy<IMacroReportDataService>();

            var ret = new List<MapResponseDataDiscrepancy>();
            ExecuteServiceCall(
                () => blService.GetMapDataDiscrepancy(model),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    ret = null;
                    errMes = resultErr.Message;
                });
            errorMessage = errMes;
            return ret;
        }

        public PageResult<NpResponseData> GetNpDataFromServer(NpRequestData requestModel)
        {
            var ret = new PageResult<NpResponseData>();

            var request = new QueryConditions();
            /*Условия фильтрации*/
            request.Filter.Add(
                new FilterQuery()
                {
                    ColumnName = "QUARTER",
                    ColumnType = typeof (int),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                        new List<ColumnFilter>()
                        {
                            new ColumnFilter()
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = requestModel.Quarter
                            }
                        }
                });

            request.Filter.Add(
                new FilterQuery()
                {
                    ColumnName = "TAX_YEAR",
                    ColumnType = typeof (int),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter>()
                    {
                        new ColumnFilter()
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = requestModel.Year
                        }
                    }
                });

            request.Filter.Add(
                new FilterQuery()
                {
                    ColumnName = "SONO_CODE",
                    ColumnType = typeof (string),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter>()
                    {
                        new ColumnFilter()
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = requestModel.IfnsId
                            
                        }
                    }
                });

            if (requestModel.Filter != null)
            {
                foreach (var filterSetting in requestModel.Filter)
                {
                    var fq = new FilterQuery
                    {
                        UserDefined = true,
                        ColumnName = filterSetting.ColumnName,
                        ColumnType = filterSetting.ColumnType,
                        FilterOperator =
                            filterSetting.FieldFilterLogicalOperator == FilterCompositionLogicalOperator.And
                                ? FilterQuery.FilterLogicalOperator.And
                                : FilterQuery.FilterLogicalOperator.Or,
                        Filtering = new List<ColumnFilter>()
                    };

                    if (filterSetting.Filter1 != null)
                    {
                        fq.Filtering.Add(filterSetting.Filter1.ToFilterComparision());
                    }
                    if (filterSetting.Filter2 != null)
                    {
                        fq.Filtering.Add(filterSetting.Filter2.ToFilterComparision());
                    }

                    request.Filter.Add(fq);
                }
            }

            /*Условия сортировки*/
            if (requestModel.Order != null)
            {
                foreach (var sortDescriptor in requestModel.Order)
                {
                    request.Sorting.Add(new ColumnSort
                    {
                        ColumnKey = sortDescriptor.Name,
                        Order =
                            sortDescriptor.SortDirection == ListSortDirection.Ascending
                                ? ColumnSort.SortOrder.Asc
                                : ColumnSort.SortOrder.Desc
                    });
                }
            }

            /*Условия паджинации*/
            request.PaginationDetails.RowsToSkip = (uint) requestModel.Skip;
            request.PaginationDetails.RowsToTake = (uint) requestModel.Take;

            var blService = GetServiceProxy<IMacroReportDataService>();

            ExecuteServiceCall(
                () => blService.GetNpData(request, requestModel.WithTotal),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    View.ShowError(resultErr.Message);
                });

            return ret;
        }
    }
}
