﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Wpf;
using Luxoft.NDS2.Client.UI.MapWpf.Controls;
using Microsoft.Practices.ObjectBuilder;
using Telerik.Windows.Controls;
using Application = System.Windows.Forms.Application;

namespace Luxoft.NDS2.Client.UI.MapWpf
{
    public sealed partial class MapView : BaseView
    {

        public MapView(PresentationContext context)
            : base(context)
        {
            _presentationContext = context;
            InitializeComponent();

            //BackColor = System.Drawing.Color.FromArgb(221, 221, 223);
        }

        private MapPresenter _presenter;
        private ElementHost _ctrlHost;
        private ReportControl _reportControl;

        [CreateNew]
        public MapPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();

                if (_reportControl != null)
                {
                    _reportControl.Presenter = _presenter;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                EnsureApplicationResources();
                _ctrlHost = new ElementHost {Visible = false};
                _reportControl = new ReportControl();
                _ctrlHost.Child = _reportControl;
                _reportControl.Presenter = this._presenter;
                this.Controls.Add(_ctrlHost);
            }
        }

        private void EnsureApplicationResources()
        {
            TelerikLoader.Init();

            // merge in your application resources
            System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                System.Windows.Application.LoadComponent(new Uri("/Luxoft.NDS2.Client;component/UI/MapWpf/Styles/Settings.xaml", UriKind.Relative)) as ResourceDictionary);
        }

        public override void OnActivate()
        {
            if (!_ctrlHost.Visible)
            {
                Application.DoEvents();
                _ctrlHost.Size = this.Size;
                _ctrlHost.Dock = DockStyle.Fill;
                _ctrlHost.Visible = true;
            }
        }

    }
}
