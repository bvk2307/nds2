﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.MapWpf.Exceptions
{
    public class BreadcrumbsItemNotFoundException: ApplicationException
    {
        public BreadcrumbsItemNotFoundException(string id)
            : base(string.Format("Не найден указанный элемент с ID = {0}", id))
        {
            
        }

            
    }
}
