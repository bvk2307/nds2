﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.MapWpf.Models;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class TabWidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var element = (SectionElement)values[0];
            var width = (double)values[1] - 60;
            return width/element.SubSections.Count;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
