﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class CircleMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var point = values[0];
            if (point == DependencyProperty.UnsetValue)
            {
                return new Thickness(0);
            }
            var size = 1.0;
            if (values.Count() > 1)
            {
                size = (double)values[1];    
            }
            return new Thickness(((Point)point).X - size / 2, ((Point)point).Y - size / 2, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
