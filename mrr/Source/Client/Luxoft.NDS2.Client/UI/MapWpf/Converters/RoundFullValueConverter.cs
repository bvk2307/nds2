﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class RoundFullValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                var d = (int) value;
                return d.ToString("##,0.###############", culture.NumberFormat);
            }
            else
            {
                var d = (decimal) value;
                return Math.Round(d).ToString("##,0.###############", culture.NumberFormat);
            }
        }

        
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
