﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class TextCenterConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var point = (Point)values[0];
            var size = (double)values[1];
            //var height = (double)values[2];
            return (double)point.Y + (size / 2) + (20 / 2);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
