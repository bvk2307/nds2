﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class TextMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var point = (Point)values[0];
            var size = (double)values[1];
            var width = (double)values[2];
            return new Thickness(point.X - width / 2, point.Y + size / 2, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
