﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class BarItemVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var model = (ChartItemViewModel)values[0];
            var mapType = (MapType)values[1];
            var mouseOver = (bool)values[2];
            model.ShowLabel = mapType == MapType.Np ? (model.IsSelected || mouseOver) : true;
            return mapType == MapType.Np ? (model.IsSelected || mouseOver ? (mouseOver ? 1.0 : 0.9) : 0.0) : (model.IsSelected || mouseOver ? (mouseOver ? 1.0 : 0.9) : 0.5);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
