﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using Luxoft.NDS2.Client.UI.MapWpf.Models;


namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    class BarSelectionToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var item = (ChartItemViewModel)value;
            if (item.IsSelected)
            {
                return new SolidColorBrush(Color.FromArgb(0xFF, 0x55, 0x84, 0xb0));
                
            }
            else
            {
                return new SolidColorBrush(Color.FromArgb(0xFF, 0x6b, 0xa6, 0xdc));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
