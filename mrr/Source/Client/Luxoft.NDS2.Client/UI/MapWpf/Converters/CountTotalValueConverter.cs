﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.MapWpf.Converters
{
    public class CountTotalValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue)
                return string.Empty;
            var value = (decimal)values[0];
            var dimension = (SubSectionElement) values[1];
            var mapType = (MapType) values[2];
            return value.ToFormat(dimension, mapType);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
