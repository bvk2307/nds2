﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public interface ISettingsProviderCreator
    {
        ISettingsProvider FactoryMethod(string key);
    }
}
