﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base
{
    public interface IDataTableSelector
    {
        Dictionary<string, string> GetData(string nameData, DictionaryConditions criteria = null);
    }
}
