﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel.Extensions;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;
using Luxoft.NDS2.Common.Helpers.Tasks.Extensions;

namespace Luxoft.NDS2.Client.UI.Base.ServiceAsync
{
    /// <summary> A manager of separate calls to server services as data flow. </summary>
    public class ServiceCallDataManager : IServiceCallDataManager
    {
        private readonly IServiceCallDispatcherFactory _serviceCallDispatcherFactory;
        private readonly IServiceCallWrapper _serviceCallWrapper;
        private readonly IExecutorWErrorHandling _executorWErrorHandling;
        private readonly IClientLogger _clientLogger;

        public ServiceCallDataManager( IServiceCallDispatcherFactory serviceCallDispatcherFactory, 
            IServiceCallWrapper serviceCallWrapper, IExecutorWErrorHandling executorWErrorHandling, IClientLogger clientLogger )
        {
            Contract.Requires( serviceCallDispatcherFactory != null );
            Contract.Requires( serviceCallWrapper != null );
            Contract.Requires( executorWErrorHandling != null );
            Contract.Requires( clientLogger != null );

            _serviceCallDispatcherFactory   = serviceCallDispatcherFactory;
            _serviceCallWrapper             = serviceCallWrapper;
            _executorWErrorHandling         = executorWErrorHandling;
            _clientLogger                   = clientLogger;
        }

        protected IServiceCallWrapper ServiceCallWrapper
        {
            get { return _serviceCallWrapper; }
        }

        /// <summary> Creates a new service call dispatcher connected to the pool of client executors. </summary>
        /// <typeparam name="TParam"> A type of the parameter of <paramref name="dataLoadHandler"/>. </typeparam>
        /// <typeparam name="TInstanceKey"> A call instance key that will be returned into <see cref="dataLoadedHandler"/> as a parameter. </typeparam>
        /// <typeparam name="TContextKey"> An execution context key that will be returned into <see cref="dataLoadedHandler"/> as a parameter. </typeparam>
        /// <typeparam name="TData"> A type of the non transformed result. </typeparam>
        /// <typeparam name="TResult"> A type of the result of <paramref name="dataLoadHandler"/>. </typeparam>
        /// <param name="dispatcherOptions"> Initialization options including <see cref="TaskScheduler"/> using for data post processing. </param>
        /// <param name="dataTransofrmHandler"> A data tranform hadnler. </param>
        /// <param name="dataLoadHandler"> A data load delegate. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="faultOrCancelHandler"> An error or cancellation handler. Optional. Does not used if it is not defined. </param>
        /// <returns> A new service call dispatcher created. </returns>
        protected IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> 
            InitServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                ServiceCallDispatcherOptions dispatcherOptions, 
                Func<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> 
                    dataTransofrmHandler, 
                Func<TParam, CancellationToken, TData> dataLoadHandler, 
                Action<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>, CancellationToken> dataLoadedHandler, 
                Action<Task> faultOrCancelHandler = null )
        {
            Contract.Requires( dispatcherOptions != null && dispatcherOptions.Scheduler != null );
            Contract.Requires( dataTransofrmHandler != null );
            Contract.Requires( dataLoadHandler != null );
            Contract.Requires( dataLoadedHandler != null );
            Contract.Ensures( Contract.Result<IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>>() != null );

            CancellationToken cancelToken = dispatcherOptions.CancelSource == null ? CancellationToken.None : dispatcherOptions.CancelSource.Token;

            IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> serviceCallDispatcher = 
                _serviceCallDispatcherFactory.Open( dataTransofrmHandler, dataLoadHandler, cancelToken, dispatcherOptions.InputBoundCapacity, 
                                                    dispatcherOptions.LowInputBoundCapacity );

            var dataProcessor = new ActionBlock<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>>(
                callExecContextPar => Execute( dataLoadedHandler, callExecContextPar, cancelToken ),
                new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = 1, CancellationToken = cancelToken, TaskScheduler = dispatcherOptions.Scheduler,
                    NameFormat = dispatcherOptions.PostprocessNameFormat
                } );

            Action<Task> faultOrCancelContinuationHandler =
                taskPrev => Execute(
                    ( taskPrevPar, cancelTokenUselessPar ) =>
                    {
                        if ( faultOrCancelHandler != null )
                            faultOrCancelHandler( taskPrevPar );

                        if ( !taskPrevPar.IsCanceled )  //called after user handler 'faultOrCancelHandler' to allow to final clear or|and to throw an own exception
                            taskPrevPar.Wait(); //to rethrow an exception only if it has been caused. 'taskPrevPar' is already completed here
                    }, 
                    taskPrev );

            dataProcessor.Completion.ContinueWith( faultOrCancelContinuationHandler, CancellationToken.None,   //handles only errors
                TaskContinuationOptions.NotOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext() );

            serviceCallDispatcher.Output.LinkTo( dataProcessor, new DataflowLinkOptions { PropagateCompletion = true } ); //subscribes to receiving nodes

            return serviceCallDispatcher;
        }

        /// <summary> Creates a new batch service call dispatcher connected to the pool of client executors. </summary>
        /// <typeparam name="TParam"> A type of the parameter of <paramref name="dataLoadHandler"/>. </typeparam>
        /// <typeparam name="TInstanceKey"> A call instance key that will be returned into <see cref="dataLoadedHandler"/> as a parameter. </typeparam>
        /// <typeparam name="TContextKey"> An execution context key that will be returned into <see cref="dataLoadedHandler"/> as a parameter. </typeparam>
        /// <typeparam name="TData"> A type of the result of <paramref name="dataLoadHandler"/>. </typeparam>
        /// <typeparam name="TResult"> A type of the result of <paramref name="dataLoadedHandler"/>. </typeparam>
        /// <typeparam name="TFinishResult"> A type of the final result. </typeparam>
        /// <param name="dispatcherOptions"> Initialization options including <see cref="TaskScheduler"/> using for data post processing. </param>
        /// <param name="dataLoadHandler"> A data load delegate. </param>
        /// <param name="dataLoadStrategy"> A batch data strategy. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="finishStepHandler"> A handler called on the dispatcher finish step before completion. </param>
        /// <param name="onFinishHandler"> A final handler called after finish step <paramref name="finishStepHandler"/>. It is called in the main UI thread. </param>
        /// <returns> A new batch service call dispatcher created. </returns>
        protected IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey> 
            InitServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult, TFinishResult>( 
                ServiceCallBatchDispatcherOptions dispatcherOptions, 
                Func<TParam, CancellationToken, TData> dataLoadHandler, 
                IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult> dataLoadStrategy, 
                Action<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>, CancellationToken> dataLoadedHandler, 
                Func<CancellationToken, Exception, TFinishResult> finishStepHandler, 
                Action<TFinishResult, Exception, CancellationToken, CancellationToken> onFinishHandler )
        {
            Contract.Requires( dispatcherOptions != null && dispatcherOptions.Scheduler != null );
            Contract.Requires( dataLoadHandler != null );
            Contract.Requires( dataLoadStrategy != null );
            Contract.Requires( dataLoadedHandler != null );
            Contract.Requires( finishStepHandler != null );
            Contract.Requires( onFinishHandler != null );
            Contract.Ensures( Contract.Result<IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey>>() != null );

            CancellationToken cancelToken = dispatcherOptions.CancelSource == null ? CancellationToken.None : dispatcherOptions.CancelSource.Token;

            IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey> serviceCallDispatcher =
                _serviceCallDispatcherFactory.CreateBatch( 
                    dataLoadHandler, dataLoadStrategy,
                    callExecContextPar => dataLoadedHandler( callExecContextPar, cancelToken ),
                    cancelToken, dispatcherOptions.InputBoundCapacity );

            Task<TFinishResult> finishStep = serviceCallDispatcher.Completion.ContinueWith(
                taskPrev =>
                {
                    TFinishResult fileSpec = finishStepHandler( cancelToken, taskPrev.Exception );

                    if ( !taskPrev.IsCanceled )
                        taskPrev.Wait();    //to rethrow an exception if it has been caused. 'taskPrev' is already completed here

                    return fileSpec;
                },
                TaskContinuationOptions.ExecuteSynchronously );

            // ReSharper disable once PossibleNullReferenceException
            finishStep.ContinueWith( 
                taskFaulted => _clientLogger.LogError( taskFaulted.Exception.Message, taskFaulted.Exception, "finishStep_Fault" ),
                TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously ); //handles only errors

            finishStep.ContinueWith(
                taskFinishStep =>
                {
                    TFinishResult result = default(TFinishResult);
                    if ( taskFinishStep.IsRanToCompletion() )
                        result = taskFinishStep.Result;
                    Execute( onFinishHandler, result, taskFinishStep.Exception, cancelToken, CancellationToken.None );   //the call is not canceled if 'cancelToken.IsCancelationRequested == true'
                },
                TaskScheduler.FromCurrentSynchronizationContext() );

            return serviceCallDispatcher;
        }

        /// <summary> Posts a data request to <paramref name="nodeDataDispatcher"/>. A result will be processed in the data loaded handler initilized in <see cref="InitServiceCallDispatcher{TParam,TInstanceKey,TContextKey,TData,TResult}"/>(). </summary>
        /// <typeparam name="TParam"> A type of request parameter. </typeparam>
        /// <typeparam name="TInstanceKey"> A type of call instance key that will be returned into the post processing. </typeparam>
        /// <typeparam name="TContextKey"> A type of an execution context key that will be returned into the post processing. </typeparam>
        /// <typeparam name="TResult"> A type of a request result. </typeparam>
        /// <param name="nodeDataDispatcher"> A service call dispatcher to request. </param>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        /// <param name="priority"> A request execution priority. </param>
        /// <returns> A request result. </returns>
        protected bool PostTo<TParam, TInstanceKey, TContextKey, TData, TResult>( 
            IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> nodeDataDispatcher, 
            TParam keyParameters, TInstanceKey instanceKey, TContextKey contextKey, CallExecPriority priority = CallExecPriority.Normal )
        {
            ITargetBlock<CallExecContext<TParam, TInstanceKey, TContextKey, TData>> inputBlock =
                priority.IsLoadingCountsOnly() ? nodeDataDispatcher.InputLow : nodeDataDispatcher.Input;

            bool success = inputBlock.Post( new CallExecContext<TParam, TInstanceKey, TContextKey, TData>(
                                            keyParameters, instanceKey, contextKey ) );

            return success;
        }

        private void Execute<TParam1>(
            Action<TParam1, CancellationToken> callDelegate, TParam1 param1, CancellationToken cancelToken = default(CancellationToken) )
        {
            _executorWErrorHandling.Execute( callDelegate, param1, cancelToken );
        }

        private void Execute<TParam1, TParam2>(
            Action<TParam1, TParam2, CancellationToken, CancellationToken> callDelegate, TParam1 param1, TParam2 param2, CancellationToken cancelTokenParam,
            CancellationToken cancelToken = default(CancellationToken) )
        {
            _executorWErrorHandling.Execute( callDelegate, param1, param2, cancelTokenParam, cancelToken );
        }
    }
}