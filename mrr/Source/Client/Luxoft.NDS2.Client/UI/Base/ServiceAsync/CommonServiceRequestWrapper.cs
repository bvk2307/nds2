﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Diagnostics.Contracts;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Base.ServiceAsync
{
    public class CommonServiceRequestWrapper : IServiceRequestWrapper
    {
        private IViewBase _view = null;

        private readonly ILogger _logger;

        public CommonServiceRequestWrapper(IViewBase view, ILogger logger)
        {
            _view = view;
            _logger = logger;
        }

        //apopov 9.3.2016	//TODO!!!   //replace this patch implementation by something that will be better

        /// <summary> DO NOT USE this method. It is temporary patch method only. </summary>
        /// <param name="view"></param>
        [Obsolete("It is temporary patch method only", error: false)]
        public void ReinitializeMessageService(IViewBase view)
        {
            _view = view;
        }

        /// <summary>
        /// Обертка серверного асинхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <param name="doOnCancel">Делегат выполянемый в случае отмены операции</param>
        /// <returns>токен для отмены операции</returns>
        /// 
        public CancellationTokenSource ExecuteAsync<TResult>(
            Func<CancellationToken, TResult> callDelegate,
            Action<CancellationToken, TResult> doOnSuccess,
            Action<CancellationToken, TResult, Exception> doOnError = null,
            Action<TResult> doOnCancel = null) where TResult : IOperationResult
        {
            Contract.Requires(callDelegate != null);
            Contract.Requires(doOnSuccess != null);

            CancellationTokenSource cancelSource = new CancellationTokenSource();
            CancellationToken cancelKey = cancelSource.Token;

            AsyncWorker<TResult> Worker = new AsyncWorker<TResult>();
            Worker.DoWork += (sender, args) =>
            {
                if (CheckIfNotCancel(cancelKey))
                    args.Result = callDelegate(cancelKey);
            };
            Worker.Complete += (sender, args) =>
            {
                if (CheckIfNotCancel(cancelKey))
                {
                    bool isRealyOk = CheckOperationResult(args.Result, doOnError == null);

                    if (CheckIfNotCancel(cancelKey))
                    {
                        if (isRealyOk)
                            doOnSuccess(cancelKey, args.Result);
                        else
                        {
                            if (doOnError != null)
                                doOnError(cancelKey, args.Result, null);
                            else if (_view != null)
                                _view.ShowError(args.Result.Message);
                        }
                    }
                    else
                    {
                        if (doOnCancel != null)
                            doOnCancel(args.Result);
                    }
                }
                else
                {
                    if (doOnCancel != null)
                        doOnCancel(args.Result);
                }
            };
            Worker.Failed += (sender, args) =>
            {
                TResult paramEmpty = default(TResult);

                if (CheckIfNotCancel(cancelKey))
                {
                    if (doOnError != null)
                        doOnError(cancelKey, paramEmpty, args.Exception);
                    else if (_view != null)
                        _view.ShowError(args.Exception.ToString());
                }
                else
                {
                    if (doOnCancel != null)
                        doOnCancel(paramEmpty);
                }

                if (args.Exception is OperationCanceledException)
                    LogError("Отмена операции", null, args.Exception);
                else
                    LogError("Ошибка", null, args.Exception);
            };
            Worker.Start();

            return cancelSource;
        }

        private bool CheckOperationResult(IOperationResult result, bool isNotQuite)
        {
            bool ret = true;

            if (result == null)
            {
                LogError("Сервер вернул пустой результат", "ExecuteServiceCall");
                if (isNotQuite && _view != null) _view.ShowError("Сервер вернул пустой результат");
                ret = false;
            }
            else if (result.Status == ResultStatus.Error)
            {
                LogError("Ошибка выполнения на сервере", "ExecuteServiceCall", new Exception(result.Message));
                if (isNotQuite && _view != null) _view.ShowError(result.Message);
                ret = false;
            }
            else if (result.Status == ResultStatus.Denied)
            {
                LogError("Отказано в доступе", "ExecuteServiceCall", new Exception(result.Message));
                if (isNotQuite && _view != null) _view.ShowError(result.Message);
                ret = false;
            }
            else if (result.Status == ResultStatus.NoDataFound)
            {
                LogError("Данные не найдены", "ExecuteServiceCall", new Exception(result.Message));
                ret = false;
            }

            return ret;
        }        

        private void LogError(string message, string methodName = null, Exception ex = null)
        {
            _logger.LogError(message, methodName, ex);
        }

        private bool CheckIfNotCancel(CancellationToken ct)
        {
            return !ct.IsCancellationRequested;
        }

        private void BreakIfCancel(CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
                throw new OperationCanceledException("operation cancelled", ct);
        }

        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <returns>признак успешности исполнения</returns>
        public bool Execute<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess,
            Action<TResult> doOnError) where TResult : IOperationResult, new()
        {
            TResult result;

            try
            {
                result = callDelegate();
            }
            catch (OperationCanceledException)
            {
                result = new TResult { Status = ResultStatus.Undefined };
            }
            catch (Exception exc)
            {
                result = new TResult { Status = ResultStatus.Error, Message = exc.Message };
            }


            bool success = result.Status == ResultStatus.Success;
            if (success)
            {
                if (doOnSuccess != null)
                    doOnSuccess(result);
            }
            else
            {
                if (doOnError != null)
                    doOnError(result);
            }

            return success;
        }

        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <returns>признак успешности исполнения</returns>
        public bool Execute<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess = null) where TResult : IOperationResult, new()
        {
            return Execute(
                callDelegate,
                doOnSuccess,
                execRes => CheckOperationResult(execRes, true)
                );
        }


    }
}
