﻿using System.Diagnostics.Contracts;
using Luxoft.NDS2.Client.Infrastructure;

namespace Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries
{
    /// <summary> Options for initialization <see cref="IBatchDataLoadStrategy{TParam,TInstanceKey,TContextKey,TData,TResult}"/>. </summary>
    public sealed class BatchDataLoadStrategyOptions
    {
        private const int DefaultMaxInnNumberInRequest = 200;
        private readonly int _maxParallelRequestCount;
        private readonly int _maxInnNumber;
        private readonly int _maxLevel;
        public BatchDataLoadStrategyOptions( int maxLevel, int maxParallelRequestCount, int maxInnNum = 0 )
        {
            Contract.Requires(0 < maxParallelRequestCount && maxParallelRequestCount <= 128); //128 -- too large parallel degree

            _maxParallelRequestCount = maxParallelRequestCount;
            _maxInnNumber = maxInnNum == 0 ? DefaultMaxInnNumberInRequest : maxInnNum;
            _maxLevel = maxLevel;
        }

        /// <summary> The maximum number of parallel requests for data loading returning from <see cref="IBatchDataLoadStrategy{TParam,TInstanceKey,TContextKey,TData,TResult}.GetNextToPostOrFinish()"/>. </summary>
        public int MaxParallelRequestCount
        {
            get { return _maxParallelRequestCount; }
        }

        public int MaxInnNumber
        {
            get { return _maxInnNumber; }
        }

        public int MaxLevel
        {
            get { return _maxLevel; }
        }
    }
}