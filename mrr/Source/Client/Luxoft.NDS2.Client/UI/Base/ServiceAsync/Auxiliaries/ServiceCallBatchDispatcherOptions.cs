﻿using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Infrastructure;

namespace Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries
{
    /// <summary> Options for initialization <see cref="IServiceCallBatchDispatcher{TParam,TInstanceKey,TContextKey,TResult}"/>. </summary>
    public sealed class ServiceCallBatchDispatcherOptions
    {
        private readonly TaskScheduler _taskScheduler;
        private string _postprocessBlockNameFormat = null;
        private CancellationTokenSource _cancelSource = null;
        private int _inputBoundCapacity = 1;

        /// <summary> </summary>
        /// <param name="taskScheduler"> A stask scheduler using for data postprocessing. By default uses <see cref="TaskScheduler.FromCurrentSynchronizationContext()"/>. </param>
        public ServiceCallBatchDispatcherOptions( TaskScheduler taskScheduler = null )
        {
            _taskScheduler = taskScheduler ?? TaskScheduler.FromCurrentSynchronizationContext();
        }

        /// <summary> A stask scheduler using for data postprocessing. By default uses <see cref="TaskScheduler.FromCurrentSynchronizationContext()"/>. </summary>
        public TaskScheduler Scheduler
        {
            get { return _taskScheduler; }
        }

        /// <summary> Gets or sets the format string to use when a data postprocess block is queried for its name. By default has not a name. </summary>
        /// <remarks> The name format may contain up to two format items. {0} will be substituted with the block's name. 
        /// {1} will be substituted with the block's Id, as is returned from the block's Completion.Id property.
        /// </remarks>
        public string PostprocessNameFormat
        {
            get { return _postprocessBlockNameFormat; }
            set { _postprocessBlockNameFormat = value; }
        }

        /// <summary> A cancellation source. By default it is 'null' and cancellation does not used. </summary>
        public CancellationTokenSource CancelSource
        {
            get { return _cancelSource; }
            set { _cancelSource = value; }
        }

        /// <summary> Bound capacity of the dispatcher input. By default is 1. </summary>
        public int InputBoundCapacity
        {
            get { return _inputBoundCapacity; }
            set { _inputBoundCapacity = value; }
        }
    }
}