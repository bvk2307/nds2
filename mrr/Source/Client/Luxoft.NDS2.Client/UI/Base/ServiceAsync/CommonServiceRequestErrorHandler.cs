﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Diagnostics.Contracts;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Base.ServiceAsync
{
    /// <summary>
    /// дефолтный обработчик ошибок серверных операций
    /// </summary>
    public class CommonServiceRequestErrorHandler : IServiceCallErrorHandler
    {
        private IViewBase _messageHandler;

        public CommonServiceRequestErrorHandler(IViewBase messageHandler)
        {
            Contract.Requires(messageHandler != null);

            _messageHandler = messageHandler;
        }

        public static CommonServiceRequestErrorHandler Create(IViewBase messageHandler)
        {
            return new CommonServiceRequestErrorHandler(messageHandler);
        }

        public void ProcessingErrors(IOperationResult result)
        {
            ProcessingErrors(result, null);
        }

        public void ProcessingErrors(IOperationResult result, Exception ex)
        {
            if ((result != null) && (result.Status != ResultStatus.Success))
            {
                _messageHandler.ShowError(result.Message);
            }
            else
            {
                if (ex != null)
                    _messageHandler.ShowError(ex.ToString());
            }
        }


    }
}
