﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Microsoft.Practices.CompositeUI;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base.Ribbon
{
    public class RibbonCreator : IRibbonAdapterCreator
    {
        private readonly PresentationContext _presentationContext;

        private readonly WorkItem _workItem;

        public RibbonCreator(PresentationContext presentationContext, WorkItem workItem)
        {
            _presentationContext = presentationContext;
            _workItem = workItem;
        }

        public IRibbonAdapter WithRibbon(
            string itemName, 
            string title, 
            string tooltip, 
            bool visible = true, 
            int order = 1)
        {
            return new RibbonAdapter(
                new RibbonManager(
                    _presentationContext,
                    _workItem,
                    itemName,
                    title,
                    tooltip,
                    visible,
                    order), _presentationContext);
        }
    }

    public class RibbonAdapter : IRibbonAdapter
    {
        private readonly RibbonManager _manager;

        private readonly PresentationContext _presentationContext;

        public RibbonAdapter(RibbonManager manager, PresentationContext presentationContext)
        {
            _manager = manager;
            _presentationContext = presentationContext;
        }

        public IRibbonGroupAdapter WithGroup(string name, string text)
        {
            return new RibbonGroupAdapter(_manager.WithGroup(name, text), _presentationContext);
        }

        public void ToggleVisibility(IEnumerable<string> buttonKeys, bool visible = true)
        {
            _manager.ToggleVisibility(buttonKeys, visible);
            _manager.Refresh();
        }

        public void ToggleAvailability(IEnumerable<string> buttonKeys, bool available)
        {
            _manager.ToggleAvailability(buttonKeys, available);
            _manager.Refresh();
        }

        public void Init()
        {
            _manager.Init();
            _manager.Refresh();
        }
    }

    public class RibbonGroupAdapter : IRibbonGroupAdapter
    {
        private readonly RibbonGroup _group;

        private readonly PresentationContext _presentationContext;

        public RibbonGroupAdapter(RibbonGroup group, PresentationContext presentationContext)
        {
            _group = group;
            _presentationContext = presentationContext;
        }

        public IRibbonButtonAdapter WithButton(
            UcRibbonToolSize size, 
            string itemName, 
            string commandName = null)
        {
            var button = _presentationContext.NewButton(itemName, commandName);
            _group.WithButton(size, button);

            return new RibbonButtonAdapter(button);
        }
    }

    public class RibbonButtonAdapter : IRibbonButtonAdapter
    {
        private readonly UcRibbonButtonToolContext _button;

        public RibbonButtonAdapter(UcRibbonButtonToolContext button)
        {
            _button = button;
        }

        public IRibbonButtonAdapter WithText(string text)
        {
            _button.WithText(text);

            return this;
        }

        public IRibbonButtonAdapter WithToolTip(string text)
        {
            _button.WithToolTip(text);

            return this;
        }

        public IRibbonButtonAdapter WithResourse(string imageResource)
        {
            _button.WithResource(imageResource);

            return this;
        }

        public IRibbonButtonAdapter WithImage(string imageLarge, string imageSmall = null)
        {
            _button.WithImage(imageLarge, imageSmall);

            return this;
        }
    }
}
