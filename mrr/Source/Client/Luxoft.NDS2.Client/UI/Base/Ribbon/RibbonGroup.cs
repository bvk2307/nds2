﻿using CommonComponents.Uc.Infrastructure.Interface.UI;

namespace Luxoft.NDS2.Client.UI.Base.Ribbon
{
    public class RibbonGroup
    {
        private readonly UcRibbonGroupContext _context;

        private readonly RibbonManager _owner;

        public RibbonGroup(
            UcRibbonGroupContext context,
            RibbonManager owner)
        {
            _context = context;
            _owner = owner;
        }

        public RibbonGroup WithButton(UcRibbonToolSize size, UcRibbonButtonToolContext button)
        {
            _context.ToolList.Add(new UcRibbonToolInstanceSettings(button.ItemName, size));
            _owner.RegisterButton(_context.ItemName, button);

            return this;
        }

        public RibbonManager WithLastButton(UcRibbonToolSize size, UcRibbonButtonToolContext button)
        {
            WithButton(size, button);

            return _owner;
        }
    }
}
