﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;

namespace Luxoft.NDS2.Client.UI.Base.Ribbon
{
    public static class RibbonHelper
    {
        public static UcRibbonGroupContext AddGroup(
            this UcRibbonTabContext tab,
            string name,
            string text,
            bool visible = false)
        {
            var group = tab.AddGroup(name);
            group.Text = text;
            group.Visible = visible;

            return group;
        }

        public static UcRibbonButtonToolContext NewButton(
            this PresentationContext presentationContext,
            string itemName,
            string commandName = null)
        {
            return new UcRibbonButtonToolContext(
                presentationContext,
                itemName,
                string.IsNullOrWhiteSpace(commandName) ? itemName : commandName);
        }

        public static UcRibbonButtonToolContext WithText(
            this UcRibbonButtonToolContext button, 
            string text)
        {
            button.Text = text;

            return button;
        }

        public static UcRibbonButtonToolContext WithToolTip(
            this UcRibbonButtonToolContext button,
            string toolTip)
        {
            button.ToolTipText = toolTip;

            return button;
        }

        public static UcRibbonButtonToolContext WithResource(
            this UcRibbonButtonToolContext button,
            string imageResource)
        {
            button.ResourceManagerName = imageResource;

            return button;
        }

        public static UcRibbonButtonToolContext WithImage(
            this UcRibbonButtonToolContext button,
            string imageLarge,
            string imageSmall = null)
        {
            button.LargeImageName = imageLarge;
            button.SmallImageName = 
                string.IsNullOrWhiteSpace(imageSmall) 
                ? imageLarge 
                : imageSmall;

            return button;
        }

        public static string GetName(this UcRibbonButtonToolContext button)
        {
            return button.ItemName.ExtractText('[',']');
        }

        public static string ExtractText(this string initialString, char start, char end)
        {
            var startIndex = initialString.IndexOf(start);
            var endingIndex = initialString.IndexOf(end);

            return initialString.Substring(startIndex + 1, endingIndex - startIndex - 1);
        }
    }
}
