﻿using CommonComponents.Uc.Infrastructure.Interface.UI;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base.Ribbon
{
    public interface IRibbonAdapterCreator
    {
        IRibbonAdapter WithRibbon(
            string itemName,
            string title,
            string tooltip,
            bool visible = true,
            int order = 1);
    }

    public interface IRibbonAdapter
    {
        IRibbonGroupAdapter WithGroup(string name, string text);

        void ToggleVisibility(IEnumerable<string> buttonKeys, bool visible = true);

        void ToggleAvailability(IEnumerable<string> buttonKeys, bool available);

        void Init();
    }

    public interface IRibbonGroupAdapter
    {
        IRibbonButtonAdapter WithButton(
            UcRibbonToolSize size, 
            string itemName, 
            string commandName = null);
    }

    public interface IRibbonButtonAdapter
    {
        IRibbonButtonAdapter WithText(string text);

        IRibbonButtonAdapter WithToolTip(string text);

        IRibbonButtonAdapter WithResourse(string imageResource);

        IRibbonButtonAdapter WithImage(string imageLarge, string imageSmall = null);
    }
}
