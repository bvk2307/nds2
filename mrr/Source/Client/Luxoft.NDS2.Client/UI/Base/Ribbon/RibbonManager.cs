﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Base.Ribbon
{
    public class RibbonManager
    {
        private const string GroupNotFoundMessagePattern = "Group {0} has to be added to manager first via WithGroup method";

        private readonly PresentationContext _presentationContext;

        private readonly WorkItem _workItem;

        private readonly UcRibbonTabContext _tab;

        private readonly List<UcRibbonGroupContext> _groups =
            new List<UcRibbonGroupContext>();

        private readonly Dictionary<string, string> _buttonGroups =
            new Dictionary<string, string>();

        private readonly Dictionary<string, UcRibbonButtonToolContext> _buttons =
            new Dictionary<string, UcRibbonButtonToolContext>();

        public RibbonManager(
            PresentationContext presentationContext,
            WorkItem workItem,
            string itemName,
            string title,
            string tooltip,
            bool visible = true,
            int order = 1)
        {
            _presentationContext = presentationContext;
            _workItem = workItem;

			if (_workItem != null)
			{
				var resourceService = Service<IUcResourceManagersService>();

				if (!resourceService.Contains(ResourceManagerNDS2.NDS2ClientResources))
				{
					resourceService.Add(
						ResourceManagerNDS2.NDS2ClientResources,
						Properties.Resources.ResourceManager);
				}
			}
            _tab = new UcRibbonTabContext(_presentationContext, itemName)
            {
                Text = title,
                ToolTipText = tooltip,
                Visible = visible,
                Order = order
            };
        }        

        public RibbonGroup WithGroup(string name, string text)
        {
            var newTab = _tab.AddGroup(name, text);
            _groups.Add(newTab);

            return new RibbonGroup(newTab, this);
        }

        public void RegisterButton(string groupId, UcRibbonButtonToolContext context)
        {
            if (_groups.All(group => group.ItemName != groupId))
            {
                throw new ArgumentException(
                    string.Format(
                        GroupNotFoundMessagePattern, 
                        groupId), 
                    "groupId");
            }

            _buttonGroups.Add(context.GetName(), groupId);
            _buttons.Add(context.GetName(), context);
        }

        public void Init()
        {            
            _presentationContext
                .UiVisualizationCollection
                .AddRange(_buttons.Values.Select(x => (VisualizationElementBase)x));

            _presentationContext
                .UiVisualizationCollection
                .Add(_tab);

            _presentationContext.ActiveMenuTab = _tab.ItemName;
        }

        public void HideAll()
        {
            foreach (var button in _buttons)
            {
                button.Value.Visible = false;
                button.Value.Enabled = false;
            }

            foreach (var group in _groups)
            {
                group.Visible = false;
            }
        }

        public void ToggleVisibility(IEnumerable<string> buttonKeys, bool visible = true)
        {
            foreach (var key in buttonKeys)
            {
                //TODO. vtsyk. remove all unnessary operations
                if(_buttons.ContainsKey(key))
                _buttons[key].Visible = visible;
            }

            RefreshGroupVisibility();
        }

        public void ToggleAvailability(IEnumerable<string> buttonKeys, bool available)
        {
            foreach (var key in buttonKeys)
            {
                if (_buttons.ContainsKey(key))
                    _buttons[key].Enabled = available;
            }
        }

        public void Refresh()
        {
			if (_workItem != null)
			{
				Service<IUcVisualStateService>().DoRefreshVisualEnvironment(_presentationContext.Id);
			}
        }

        private TService Service<TService>()
        {
            return _workItem.Services.Get<TService>(true);
        }

        private void RefreshGroupVisibility()
        {
            foreach (var group in _groups)
            {
                group.Visible = _buttonGroups.Any(x => _buttons[x.Key].Visible && x.Value == group.ItemName);
            }
        }
    }
}
