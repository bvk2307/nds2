﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Base
{
    public static class FormHelper
    {
        public static void InvokeIfNeeded(this Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke(new ParameterlessEventHandler(() => action()));
            }
            else
            {
                action();
            }
        }
    }
}
