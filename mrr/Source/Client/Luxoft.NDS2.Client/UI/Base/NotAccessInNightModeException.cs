﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base
{
    public class NotAccessInNightModeException : ApplicationException
    {
        public NotAccessInNightModeException() { }

        public NotAccessInNightModeException(string message) : base(message) { }

        public NotAccessInNightModeException(string message, Exception inner) : base(message, inner) { }
    }
}
