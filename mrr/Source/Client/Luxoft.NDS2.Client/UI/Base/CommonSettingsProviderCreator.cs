﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Base
{
    internal class CommonSettingsProviderCreator : ISettingsProviderCreator
    {
        private readonly WorkItem _workItem;

        public CommonSettingsProviderCreator(WorkItem workItem)
        {
            _workItem = workItem;
        }

        public ISettingsProvider FactoryMethod(string key)
        {
            return new CommonSettingsProvider(_workItem, key);
        }
    }
}
