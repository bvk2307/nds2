﻿namespace Luxoft.NDS2.Client.UI.Base
{
    /// <summary>
    /// Этот интерфейс описывает функции, хранилища объектов, идентифицируемых ключем
    /// </summary>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    /// <typeparam name="TValue">Тип объекта</typeparam>
    public interface IKeyValueStorage<TKey, TValue>
    {
        /// <summary>
        /// Помещает объект в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="obj">Объект</param>
        void Push(TKey key, TValue obj);

        /// <summary>
        /// Возвращает объект из хранилища
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="obj">Объект</param>
        bool Pull(TKey key, out TValue obj);

        /// <summary>
        /// Помечает объект на удаление из хранилища (объект либо удаляется либо помещается в кэш).
        /// После данной операции нахождение объекта в хранилище не гарантируется.
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        void Release(TKey key);

        /// <summary>
        /// Полностью очищает содержимое хранилища, включая кэш, если данные кэшируются
        /// </summary>
        void Clear();
    }
}
