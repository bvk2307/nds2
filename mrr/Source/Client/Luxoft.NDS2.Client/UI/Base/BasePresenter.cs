﻿using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Security.Authorization;
using CommonComponents.Security.SecurityLogger;
using CommonComponents.Uc.Infrastructure.Interface;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils.Async;
using FLS.Common.Lib.Attributes;
using FLS.CommonComponents.App.Services.Exrtensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.Model.Bargain;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Collections;
using Microsoft.Practices.CompositeUI.SmartParts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Base
{
    public partial class BasePresenter<TV> 
        : Presenter<TV>, 
        IWindowEventsSubscriber, 
        ISmartPartInfo,
        IServiceCallWrapper,
        ILogger,
        IClaimCardOpener
        where TV : IViewBase
    {
        #region Private members

        protected readonly IServiceRequestWrapper _requestExecutor;

        private AppCache _cache = null;
        private IClientLogger _clientLogger = null;
        private INotifier _notifier = null;
        private ISecurityService _securityService;
        private ISecurityLoggerService _securityLogger;

        #endregion Private members

        public PresentationContext PresentationContext { get; set; }

        protected ServiceCollection Services { get { return WorkItem.Services; } }

        public AppCache Cache { get { return _cache; } }

        protected ISecurityService SecurityService
        {
            get
            {
                if (_securityService == null)
                {
                    if (WorkItem != null)
                    {
                        _securityService = GetServiceProxy<ISecurityService>();
                    }
                }

                return _securityService;
            }
        }

        protected ISecurityLoggerService SecurityLogger
        {
            get
            {
                if (_securityLogger == null)
                {
                    if (WorkItem != null)
                    {
                        _securityLogger = WorkItem.Services.Get<ISecurityLoggerService>();
                    }
                }
                return _securityLogger;
            }
        }

        protected T GetServiceProxy<T>(string context = null) where T : class
        {
            return this.WorkItem.Services.GetServiceProxy<T>(string.IsNullOrEmpty(context) ? Constants.EndpointName : context);
        }

        protected BasePresenter()
        {
            this.AlwaysInitalize();

            _requestExecutor = new CommonServiceRequestWrapper(View, this);
        }

        protected BasePresenter(PresentationContext presentationContext, WorkItem wi, TV view)
        {
            this.PresentationContext = presentationContext;
            if (wi != null)
            {
                this.Initialize(wi);
            }
            this.AlwaysInitalize();

            _requestExecutor = new CommonServiceRequestWrapper(view, this);

            this.View = view;
        }

        private void AlwaysInitalize()
        {
            Contract.Ensures( _cache != null );

            Func<AppCache> cacheInitiator = () => AppCache.Current;
            _cache = WorkItem == null ? cacheInitiator() : Services.InitOrGetService( cacheInitiator );
        }

        public bool IsUserEligibleForOperation(string operationName)
        {
            return UserOperations.Any(o => o.Name.Equals(operationName));
        }

        public bool IsUserEligibleForRole(params string[] roleNames)
        {
            return GetUserPermissions().IsUserInRole(roleNames);
        }

        protected List<AccessRight> Rigths
        {
            get
            {
                return GetUserPermissions();
            }
        }

        public List<AccessRight> UserOperations
        {
            get
            {
                return GetUserPermissions().Where(r => r.PermType == PermissionType.Operation).ToList();
            }
        }

        protected List<AccessRight> UserRoles
        {
            get
            {
                return GetUserPermissions().Where(r => r.PermType == PermissionType.Operation).ToList();
            }
        }

        protected void ClearPermissionCache()
        {
            Cache.Remove<List<AccessRight>>( ServerOperations.BP_UserPerms );
            Cache.Remove<ContractorDataPolicy>( ServerOperations.BP_UserRestrCfg );
        }

        private List<AccessRight> _userPermissions = null;

        private List<AccessRight> GetUserPermissions()
        {
            return _userPermissions = _userPermissions ?? WorkItem.GetUserPermissions();
        }

        protected string UserName
        {
            get
            {
                var wi = WindowsIdentity.GetCurrent();
                if (wi != null)
                {
                    return wi.Name;
                }

                return Thread.CurrentPrincipal.Identity.Name;
            }
        }

        protected string UserDisplayName
        {
            get
            {
                if(Utils.IsUserInDomain())
                {
                    var uis = WorkItem.Services.Get<IUserInfoService>();
                    var sid = UserSid;
                    if (uis != null && !string.IsNullOrEmpty(sid))
                        return uis.GetUserInfoBySID(sid).DisplayName;
                    return string.Empty;                    
                }
                else
                {
                    return UserName.ToLower();
                }
            }
        }
        
        protected string UserSid
        {
            get
            {
                var wi = WindowsIdentity.GetCurrent();
                if (wi != null)
                {
                    return wi.User.Value;
                }

                return string.Empty;
            }
        }

        #region Implementation of IWindowEventsSubscriber

        public virtual void OnLoad()
        {
        }

        public virtual void OnActivate()
        {
        }

        public virtual void OnDeactivate()
        {
        }

        public virtual void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            
        }

        public virtual void OnChildClosed(WindowCloseContextBase closeContext)
        {
        }

        public virtual void OnClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
        }

        public virtual void OnClosed(WindowCloseContextBase closeContext)
        {
        }

        public virtual void OnUnload()
        {
        }

        #endregion

        /// <summary>
        /// вызывается по необходимости если объект был создан через object builder
        /// </summary>
        public virtual void OnObjectCreated()
        {
            if ( View != null )
                ( (CommonServiceRequestWrapper)_requestExecutor ).ReinitializeMessageService( View );
        }

        #region Implementation of ISmartPartInfo

        public virtual string Description { get; set; }
        public virtual string Title { get; set; }

        #endregion

        #region logging

        public IClientLogger ClientLogger
        {
            get
            {
                Contract.Ensures( Contract.Result<IClientLogger>() != null );

                if (_clientLogger == null)
                {
                    Contract.Assume(WorkItem != null);

                    _clientLogger = Services.Get<IClientLogger>(ensureExists: true);    //throws NullReferenceException if 'WorkItem == null' yet
                }
                return _clientLogger;
            }
        }

        public void LogNotification(string message, string methodName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            ClientLogger.LogInfo(message, methodName);
        }

        public void LogError(Exception ex,
            [CallerMemberName]
            string callerMemberName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            ClientLogger.LogError(ex.Message, ex, callerMemberName);
        }

        public void LogError(string message, string methodName = null, Exception ex = null)
        {
            ClientLogger.LogError(message, ex, methodName);
        }

        public void LogWarning(string message, string methodName = null)
        {
            ClientLogger.LogWarning(message, methodName);
        }

#endregion logging

#region View

        /// <summary> Called after than <see cref="View"/> has been assgined. </summary>
        protected override void OnViewSet()
        {
            if ( View != null )
                ( (CommonServiceRequestWrapper)_requestExecutor ).ReinitializeMessageService( View );

            base.OnViewSet();
        }

#endregion

        public void RefreshEKP()
        {
            var UcVisualStateService = this.WorkItem.Services.Get<IUcVisualStateService>(true);

            UcVisualStateService.DoRefreshVisualEnvironment(PresentationContext.Id);
        }

#region Grid Setup

        public GridSetup CommonSetup(string key)
        {
            return new GridSetup(SettingsProvider(key));
        }

        public virtual ISettingsProvider SettingsProvider(string key)
        {
            return new CommonSettingsProvider(WorkItem, key);
        }

#endregion

#region Обертки серверных вызовов

        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <returns>признак успешности исполнения</returns>
        public bool ExecuteServiceCall<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess,
            Action<TResult> doOnError = null) where TResult : IOperationResult, new()
        {
            TResult result = ExecuteServiceCall( callDelegate );

            bool success = result.Status == ResultStatus.Success;
            if ( success )
            {
                if ( doOnSuccess != null )
                    doOnSuccess( result );
            }
            else
            {
                if ( doOnError != null )
                    doOnError( result );
            }

            return success;
        }

        /// <summary>
        /// Обертка серверного асинхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <param name="doOnCancel">Делегат выполянемый в случае отмены операции</param>
        /// <returns>Источник токенов для отмены операции</returns>
        /// 
        public CancellationTokenSource ExecuteServiceCallAsync<TResult>(
            Func<CancellationToken, TResult> callDelegate,
            Action<CancellationToken, TResult> doOnSuccess,
            Action<CancellationToken, TResult, Exception> doOnError = null,
            Action<TResult> doOnCancel = null) where TResult : IOperationResult
        {
            return _requestExecutor.ExecuteAsync(
                callDelegate,
                doOnSuccess,
                doOnError,
                doOnCancel);
        }


        public void ServiceCallErrorShow(IOperationResult r, Exception e)
        {
            IServiceCallErrorHandler errorHandler = new CommonServiceRequestErrorHandler(View);

            errorHandler.ProcessingErrors(r, e);
        }


        /// <summary>
        /// Обработка асинхронного запроса к МС
        /// </summary>
        /// <typeparam name="T">Тип объекта передаваемого и получаемого от главного делегата</typeparam>
        /// <param name="executeDelegate">Главный исполняющий делегат</param>
        /// <param name="resultSovSuccess">Заглушка результата от СОВ для варианта вызова с указанным RequestId</param>
        /// <param name="requestParams">Объект передаваемый главному делегату как параметр</param>
        /// <param name="timeOutMs">Таймаут ожидания при опросе СОВа</param>
        /// <param name="onCompleteDelegate">Делегат на случай успешного завершения</param>
        /// <param name="cancelToken">Токен для отмены предыдущей операции</param>
        /// <param name="onErrorDelegate">Делегат на случай ошибки</param>
        protected void ExecuteSovRequest<T>(
            Func<T, OperationResult<T>> executeDelegate,
            OperationResult<T> resultSovSuccess,
            T requestParams,
            int timeOutMs,
            string sovErrorMessage,
            Action<T> onCompleteDelegate,
            CancellationToken cancelToken,
            Action<Exception> onErrorDelegate = null,
            Action<Exception> onNightModeDelegate = null
            ) where T : SovOperation, new()
        {
            var worker = new AsyncWorker<T, Func<T, OperationResult<T>>, T>(executeDelegate, requestParams);

            worker.DoWork += (sender, args) =>
            {
                var callDelegate = args.Parameter1;
                var arguments = args.Parameter2;

                BreakIfCancel(cancelToken);

                OperationResult<T> internalResult = null;
                bool IsSendToSOV = true;
                if (arguments.RequestId.HasValue && resultSovSuccess != null)
                {
                    IsSendToSOV = false;
                    internalResult = resultSovSuccess;
                }
                else
                {
                    internalResult = callDelegate(arguments);
                }

                BreakIfCancel(cancelToken);

                switch (internalResult.Status)
                {
                    case ResultStatus.Success:
                        bool isCycling = true;
                        while (isCycling)
                        {
                            if (IsSendToSOV)
                                Thread.Sleep(timeOutMs);

                            BreakIfCancel(cancelToken);

                            internalResult = callDelegate(internalResult.Result);

                            if (internalResult.Status != ResultStatus.Success)
                                isCycling = false;

                            if (internalResult.Result != null && internalResult.Result.ExecutionStatus != RequestStatusType.InProcess)
                                isCycling = false;

                            if (isCycling && !IsSendToSOV)
                                Thread.Sleep(timeOutMs);
                        }

                        switch (internalResult.Status)
                        {
                            case ResultStatus.Success:
                                switch (internalResult.Result.ExecutionStatus)
                                {
                                    case RequestStatusType.Completed:
                                        args.Result = internalResult.Result;
                                        break;
                                    case RequestStatusType.NotSuccessInNightMode:
                                        throw new NotAccessInNightModeException(ResourceManagerNDS2.SovNotSuccessInNightModeMessage);
                                    case RequestStatusType.Error:
                                        throw new ApplicationException(sovErrorMessage);
                                    case RequestStatusType.NotRegistered:
                                        throw new ApplicationException("Ошибка регистрации запроса");
                                    default:
                                        throw new ApplicationException("Ошибочный ответ от сервера");
                                }
                                break;
                            case ResultStatus.Error:
                                throw new ApplicationException(internalResult.Message);
                                break;
                        }
                        break;

                    case ResultStatus.Error:
                        throw new ApplicationException(internalResult.Message);
                        break;
                }

            };

            worker.Complete += (sender, args) =>
            {
                if (CheckIfNotCancel(cancelToken))
                    onCompleteDelegate(args.Result);
            };

            worker.Failed += (sender, args) =>
            {
                if (CheckIfNotCancel(cancelToken))
                {

                    if (onErrorDelegate != null)
                        onErrorDelegate(args.Exception);
                    else
                    {
                        if (!(args.Exception is OperationCanceledException))
                        {
                            if (args.Exception is NotAccessInNightModeException)
                            {
                                if (onNightModeDelegate != null)
                                    onNightModeDelegate(args.Exception);
                                else
                                {
                                    LogNotification(args.Exception.ToString(), "ExecuteSovRequest");
                                    View.ShowNotification(args.Exception.Message);
                                }
                            }
                            else
                            {
                                LogError(args.Exception.ToString(), "ExecuteSovRequest", args.Exception);
                                View.ShowError(args.Exception.Message);
                            }
                        }
                    }
                }

                if (args.Exception is OperationCanceledException)
                    LogError("Отмена операции", null, args.Exception);
                else
                    LogError("Ошибка", null, args.Exception);
            };

            worker.Start();
        }

        /// <summary>
        /// Обертка серверного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult ExecuteServiceCall<TResult>( Func<TResult> callDelegate ) 
            where TResult : IOperationResult, new()
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            try
            {
                result = callDelegate();
            }
            catch ( OperationCanceledException )
            {
                result = new TResult { Status = ResultStatus.Undefined };   //returns 'ResultStatus.Undefined' without result if execution is cancelled
            }
            catch ( Exception exc ) //for example, TransportException	//apopov 16.5.2016	//TODO!!!   //peel AggregationException until OperationCanceledException or some different exception occurs
            {
                result = new TResult { Status = ResultStatus.Error, Message = exc.Message };
            }
            string errorMessage = LogUnsuccessOperationResult( result );
            if ( errorMessage != null )
                ShowOperationResultError( result, errorMessage );

            return result;
        }

        /// <summary>
        /// Обертка серверного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult ExecuteServiceCall<TResult>( 
            Func<CancellationToken, TResult> callDelegate, CancellationToken cancelToken = default(CancellationToken) ) 
            where TResult : IOperationResult, new()
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( cancelToken.IsCancellationRequested )
            {
                result = new TResult { Status = ResultStatus.Undefined };   //returns 'ResultStatus.Undefined' without result if execution is cancelled
            }
            else
            {
                try
                {
                    result = callDelegate( cancelToken );
                }
                catch ( OperationCanceledException )
                {
                    result = new TResult { Status = ResultStatus.Undefined };   //returns 'ResultStatus.Undefined' without result if execution is cancelled
                }
                catch ( Exception exc ) //for example, TransportException	//apopov 16.5.2016	//TODO!!!   //peel AggregationException until OperationCanceledException or some different exception occurs
                {
                    result = new TResult { Status = ResultStatus.Error, Message = exc.Message };
                }
                string errorMessage = LogUnsuccessOperationResult( result );
                if ( errorMessage != null )
                    ShowOperationResultError( result, errorMessage );
            }
            return result;
        }
        
        /// <summary> Returns 'null' if <paramref name="result"/> is successful otherwise an unsuccess operation status. </summary>
        /// <param name="result"></param>
        /// <returns> 'null' if <paramref name="result"/> is successful otherwise an unsuccess operation status. </returns>
        protected ResultStatus? GetUnsuccessOperationResult(IOperationResult result)
        {
            ResultStatus? errorResultStatus = null;
            ResultStatus resultStatus = result == null ? ResultStatus.Undefined : result.Status;

            switch (resultStatus)
            {case ResultStatus.Undefined:
            case ResultStatus.Error:
            case ResultStatus.Denied:
            case ResultStatus.NoDataFound:
                errorResultStatus = resultStatus;
                break;
            }

            return errorResultStatus;
        }        

        protected bool CheckIfNotCancel(CancellationToken ct)
        {
            return !ct.IsCancellationRequested;
        }            

        protected void BreakIfCancel(CancellationToken ct)
        {
            if (ct.IsCancellationRequested) 
                throw new OperationCanceledException("operation cancelled", ct);
        }

        /// <summary> Logs an operation result if it is error one. </summary>
        /// <param name="result"></param>
        /// <param name="callerMemberName"> A caller name. Optional. </param>
        /// <returns> 'null' if the operation result successes otherwise error operation result message. </returns>
        private string LogUnsuccessOperationResult(IOperationResult result,
            [CallerMemberName]
            string callerMemberName = null)
        {
            string message = null;
            ResultStatus? resultStatus = GetUnsuccessOperationResult(result);
            if (resultStatus.HasValue)
            { 
                switch (resultStatus.Value)
                {case ResultStatus.Undefined:
                case ResultStatus.Error:
                case ResultStatus.Denied:
                case ResultStatus.NoDataFound:
                    message = AttributeReadHelper.GetDisplayName(resultStatus.Value);
                    break;
                }
                Exception exception = null;
                switch (resultStatus.Value)
                {case ResultStatus.Error:
                case ResultStatus.Denied:
                case ResultStatus.NoDataFound:
                    exception =new Exception(result.Message);
                    break;
                }
                if (message != null)
                    LogError(message, callerMemberName, exception);
            }

            return message;
        }

        public INotifier Notifier
        {
            get
            {
                Contract.Ensures( Contract.Result<INotifier>() != null );

                if (_notifier == null)
                {
                    Contract.Assume(WorkItem != null);

                    _notifier = Services.Get<INotifier>(ensureExists: true);    //throws NullReferenceException if 'WorkItem == null' yet
                }
                return _notifier;
            }
        }

        public void ShowOperationResultError( IOperationResult result, string errorMessage )
        {
            ResultStatus? resultStatus = GetUnsuccessOperationResult(result);
            if (resultStatus.HasValue)
            {
                string message = null;
                switch (resultStatus.Value)
                {case ResultStatus.Undefined:
                    message = errorMessage;
                    break;
                case ResultStatus.Error:
                case ResultStatus.Denied:
                    message = result.Message ?? errorMessage;
                    break;
                }
                if (message != null)
                    Notifier.ShowError(message);
            }
        }

#endregion


#region Oper

        private DictionaryOper _oper;

        public DictionaryOper Oper
        {
            get
            {
                return _oper = _oper
                       ?? Cache.AddOrGetExisting( () => new DictionaryOper( LoadOper() ), ServerOperations.BP_Opers, TimeSpan.FromHours( 24 ) );
            }
        }

        private IEnumerable<OperCode> LoadOper()
        {
            IEnumerable<OperCode> result = null;

            ExecuteServiceCall(
                () => GetServiceProxy<IDictionaryDataService>().GetOperDictionary(),
                (response) => result = response.Result);

            return result ?? new List<OperCode>();
        }

        private DictionaryCurr _curr;

        public DictionaryCurr Curr
        {
            get
            {
                return _curr = _curr
                    ?? Cache.AddOrGetExisting( () => new DictionaryCurr( LoadCurr() ), ServerOperations.BP_Currs, TimeSpan.FromHours( 24 ) );
            }
        }

        private IEnumerable<OperCode> LoadCurr()
        {
            IEnumerable<OperCode> result = null;

            ExecuteServiceCall(
                () => GetServiceProxy<IDictionaryDataService>().GetCurrencyDictionary(),
                (response) => result = response.Result);

            return result ?? new List<OperCode>();
        }

        private DictionaryBargain _barg;

        public DictionaryBargain Barg
        {
            get
            {
                return _barg = _barg
                ?? Cache.AddOrGetExisting( () => new DictionaryBargain( LoadBarg() ), ServerOperations.BP_Bargains, TimeSpan.FromHours( 24 ) );
            }
        }

        private IEnumerable<OperCode> LoadBarg()
        {
            IEnumerable<OperCode> result = null;

            ExecuteServiceCall(
                () => GetServiceProxy<IDictionaryDataService>().GetBargainDictionary(),
                (response) => result = response.Result);

            return result ?? new List<OperCode>();
        }

#endregion

        #region СУР

        private DictionarySur _sur;

        public DictionarySur Sur
        {
            get
            {
                return _sur = _sur ?? WorkItem.DictionarySur();                    
            }
        }

        #endregion

        #region Справочник причин аннулирования

        private DictionaryAnnulmentReason _annulmentReason;

        public DictionaryAnnulmentReason AnnulmentReason
        {
            get { return _annulmentReason ?? WorkItem.DictionaryAnnulmentReason(); }
        }

        #endregion

        #region Отчетный период

        public TaxPeriodModel _taxPeriodModel;
        public TaxPeriodModel TaxPeriodModel
        {
            get
            {
                return _taxPeriodModel = _taxPeriodModel
                    ?? Cache.AddOrGetExisting( CreateTaxPeriodModel, ServerOperations.BP_TaxPrd, TimeSpan.FromHours( 1 ) );
            }
        }

        private TaxPeriodModel CreateTaxPeriodModel()
        {
            var result = TaxPeriodHelper.CreateExistsTaxPeriod(LoadDictonaryTaxPeriods(), LoadTaxPeriodConfiguration());
            return new TaxPeriodModel(result.TaxPeriods, result.TaxPeriodDefault);
        }

        public List<DictTaxPeriod> LoadDictonaryTaxPeriods()
        {
            List<DictTaxPeriod> result = null;

            ExecuteServiceCall(
                () => GetServiceProxy<IDictionaryDataService>().GetTaxPeriodDictionary(),
                (response) => result = response.Result);

            return result ?? new List<DictTaxPeriod>();
        }

        public TaxPeriodConfiguration LoadTaxPeriodConfiguration()
        {
            TaxPeriodConfiguration result = null;

            ExecuteServiceCall(
                () => GetServiceProxy<IDataService>().GetTaxPeriodConfiguration(),
                (response) => result = response.Result);

            return result ?? new TaxPeriodConfiguration() { FiscalYearBegin = FISCAL_YEAR_BEGIN_DEFAULT, TaxPeriodBegin = TAX_PERIOD_BEGIN_DEFAULT };
        }

        private const int FISCAL_YEAR_BEGIN_DEFAULT = 2015;
        private const string TAX_PERIOD_BEGIN_DEFAULT = "22";

#endregion

        #region Настройки доступа к цепочкам сделок по уровню

        private ContractorDataPolicy _userConfig;

        public ContractorDataPolicy UserRestrictionConfig
        {
            get
            {
                return _userConfig = _userConfig ?? WorkItem.ContractorDataPolicy();
            }
        }

        #endregion

#region Activity log

        public void LogActivity(ActivityLogEntry entry)
        {
            var svc = WorkItem.Services.Get<IClientContextService>().CreateCommunicationProxy<IActivityLogService>(Constants.EndpointName);
            var asyncWorker = new AsyncWorker<OperationResult>();
            asyncWorker.DoWork += (sender, args) => svc.Write(UserSid, entry);
            asyncWorker.Start();
        }

        #endregion

    }
}
