﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Luxoft.NDS2.Client.UI.CommonFunctions;

namespace Luxoft.NDS2.Client.UI.Base
{
    public class BaseView : UserControl, ISmartPartInfoProvider, IWindowEventsSubscriber, IViewBase
    {
        private INotifier _notifier = null;
        private IUcMessageService _ucMessageService = null;
        private IUcVisualStateService _ucVisualStateService = null;

        protected PresentationContext _presentationContext;

        private IClientLogger _logger;

        protected IClientLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    if (WorkItem != null)
                        _logger = WorkItem.Services.Get<IClientLogger>(true);
                }
                return _logger;
            }
        }

        protected WorkItem WorkItem { get; set; }

        protected INotifier Notifier
        {
            get
            {
                if (_notifier == null)
                {
                    if (WorkItem != null)
                        _notifier = WorkItem.Services.Get<INotifier>(ensureExists: true);
                    //else  //does not throw an exception because it requests full testing of all notifications
                }
                return _notifier;
            }
        }

        public event EventHandler OnFormClosing;

        protected IUcMessageService MessageService
        {
            get
            {
                if (_ucMessageService == null)
                { 
                if (WorkItem != null)
                        _ucMessageService = WorkItem.Services.Get<IUcMessageService>(ensureExists: true);
                    //else  //does not throw an exception because it requests full testing of all messages
                }
                return _ucMessageService;
            }
        }

        protected IUcVisualStateService VisualStateService
        {
            get
            {
                if (_ucVisualStateService == null)
                { 
                    if (WorkItem == null)
                        throw new InvalidOperationException("WorkItem have to be defined to use VisualStateService");

                    _ucVisualStateService = WorkItem.Services.Get<IUcVisualStateService>(ensureExists: true);
                }
                return _ucVisualStateService;
            }
        }

        internal BaseView()
        {

        }

        protected BaseView(PresentationContext context)
            : this(context, null)
        {
        }

        protected BaseView(PresentationContext context, WorkItem workItem)
        {
            _presentationContext = context;
            if (workItem != null)
            {
                WorkItem = workItem;
            }
        }

        public void SetWindowTitle(string title)
        {
            if (_presentationContext != null)
            {
                _presentationContext.WindowTitle = title;

                RefreshEkp();
            }
        }

        public string GetWindowTitle()
        {
            if (_presentationContext != null)
            {
                return _presentationContext.WindowTitle;
            }
            return string.Empty;
        }

        public void SetWindowDesciption(string description)
        {
            if (_presentationContext != null)
            {
                _presentationContext.WindowDescription = description;

                RefreshEkp();
            }
        }

        public void ShowError(string message)
        {
            if ( Notifier != null )
                Notifier.ShowError( message );
            //else  //does not throw an exception because it requests full testing of all notifications
        }

        public void ShowWarning(string message)
        {
            if ( Notifier != null )
                Notifier.ShowWarning( message );
            //else  //does not throw an exception because it requests full testing of all notifications
        }

        public void ShowNotification(string message)
        {
            if ( Notifier != null )
                Notifier.ShowNotification( message );
            //else  //does not throw an exception because it requests full testing of all notifications
        }

        public DialogResult ShowQuestion(string caption, string message)
        {
            if (MessageService == null)
            {
                return DialogResult.Abort;
            }

            return new QuestionBox(MessageService).QuestionDialog(caption, message);
        }

        public void ShowInfo(string caption, string message)
        {
            var context = new MessageInfoContext(caption, message, MessageCategory.Info) { Buttons = MessageBoxButtons.OK };
            MessageService.ShowInfo(context);
        }

        public event ParameterlessEventHandler AfterLoad;

        public void RaiseAfterLoad()
        {
            var handler = AfterLoad;
            if (handler != null)
                handler();
        }

        public void RefreshEkp()
        {
            VisualStateService.DoRefreshVisualEnvironment(_presentationContext.Id);
        }

        /// <summary> ATTENTION! Does not pass control to the main UI thread inside and does not delay the call (unlikes <see cref="ShowError"/>, <see cref="ShowWarning"/> and <see cref="ShowNotification"/>. </summary>
        /// <param name="message"></param>
        public void ShowTrayMessage(MessageInfoContext message)
        {
            if (Notifier != null)
            {
                IMessageInfoNotifier messageInfoNotifier = Notifier as IMessageInfoNotifier;
                if (messageInfoNotifier != null)
                    messageInfoNotifier.Show(message);
            }
        }

        #region Implementation of ISmartPartInfoProvider

        public ISmartPartInfo GetSmartPartInfo(Type smartPartInfoType)
        {
            if (smartPartInfoType.IsAssignableFrom(typeof(PresentationContext)))
                return this._presentationContext;

            //if (smartPartInfoType.IsAssignableFrom(typeof(IWindowEventsSubscriber)))
            //    return this._presenter;

            return null;
        }

        #endregion

        #region Implementation of IWindowEventsSubscriber

        public virtual void OnLoad()
        {
        }

        public virtual void OnActivate()
        {
        }

        public virtual void OnDeactivate()
        {
        }

        public virtual void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
        }

        public virtual void OnChildClosed(WindowCloseContextBase closeContext)
        {
        }

        public virtual void OnClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            if (OnFormClosing != null)
            {
                OnFormClosing(this, EventArgs.Empty);
            }

            //if (WorkItem != null && WorkItem.RootWorkItem != null)
            //{
            //    //_workItem.RootWorkItem.WorkItems.Remove(_workItem);
            //}
        }

        public virtual void OnClosed(WindowCloseContextBase closeContext)
        {
        }

        public virtual void OnUnload()
        {
        }

        #endregion

    }
}
