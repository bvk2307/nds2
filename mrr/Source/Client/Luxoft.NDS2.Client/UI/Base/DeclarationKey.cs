﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Client.UI.Base
{
    public interface IDeclarationIdentifier
    {
        DeclarationKey GetKey();
    }

    public class ContractorPeriodDeclarationKey : IDeclarationIdentifier
    {
        private readonly string _inn;

        private readonly string _kpp;

        private readonly string _year;

        private readonly string _period;

        public ContractorPeriodDeclarationKey(string inn, string kpp, string year, string period)
        {
            _inn = inn;
            _kpp = kpp;
            _year = year;
            _period = period;
        }

        public DeclarationKey GetKey()
        {
            return new DeclarationKey
            {
                Inn = _inn,
                Kpp = _kpp,
                Year = _year,
                Period = _period
            };
        }
    }

    public class DeclarationIdentifier : IDeclarationIdentifier
    {
        private readonly long _id;

        public DeclarationIdentifier(long id)
        {
            _id = id;
        }

        public DeclarationKey GetKey()
        {
 	        return new DeclarationKey { Id = _id };
        }
    }
}
