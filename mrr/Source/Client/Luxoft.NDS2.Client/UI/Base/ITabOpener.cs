﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public interface ITabOpener
    {
        void List<TView>();

        void Details<TView>(long requestId, long? id = null);   
    }
}
