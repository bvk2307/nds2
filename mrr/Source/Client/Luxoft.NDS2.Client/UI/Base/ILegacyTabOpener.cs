﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public interface ILegacyTabOpener
    {
        void ViewTaxPayerDetails(string inn, string kpp);
    }
}
