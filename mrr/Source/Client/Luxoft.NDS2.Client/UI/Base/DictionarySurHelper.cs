﻿using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base
{
    public static class DictionarySurHelper
    {
        public static DictionarySur DictionarySur(this WorkItem workItem)
        {
            return 
                AppCache.Current.AddOrGetExisting(
                    () => new DictionarySur(workItem.LoadSur()),
                    ServerOperations.BP_Surs,
                    TimeSpan.FromHours(24));
        }

        private static IEnumerable<SurCode> LoadSur(this WorkItem workItem)
        {
            List<SurCode> result;

            if (workItem
                .GetWcfServiceProxy<IDictionaryDataService>()
                .ExecuteNoHandling(
                    proxy => proxy.GetSurDictionary(),
                    out result))
            {
                return result;
            }

            return new List<SurCode>();
        }
    }
}
