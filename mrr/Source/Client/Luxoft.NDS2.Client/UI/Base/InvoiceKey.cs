﻿using System;
namespace Luxoft.NDS2.Client.UI.Base
{
    public class InvoiceKey
    {
        private const string PartsSplitter = "/";

        private const int ZipPartIndex = 0;

        private const int ChapterPartIndex = 1;

        private const int OrdinalNumberPartIndex = 2;

        private readonly string _rawKey;

        public InvoiceKey(string key)
        {
            _rawKey = key;
        }

        public bool HasChapter
        {
            get
            {
                return _rawKey.Contains(PartsSplitter);
            }
        }

        public long DeclarationZip
        {
            get
            {
                return HasChapter 
                    ? long.Parse(GetPart(ZipPartIndex))
                    : long.Parse(_rawKey);
            }
        }

        public uint? Chapter
        {
            get
            {
                if (!HasChapter)
                {
                    return null;
                }

                return uint.Parse(GetPart(ChapterPartIndex));
            }
        }

        public long OrdinalNumber
        {
            get
            {
                string retString = String.Empty;
                string[] words = _rawKey.Split(new string[] { PartsSplitter }, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length >= OrdinalNumberPartIndex)
                {
                    retString = words[OrdinalNumberPartIndex];
                }
                long ret = -1;
                long temp = 0;
                if (long.TryParse(retString, out temp))
                {
                    ret = temp;
                }
                return ret;
            }
        }

        private string GetPart(int index)
        {
            return _rawKey.Split(
                new string[] { PartsSplitter }, 
                StringSplitOptions.RemoveEmptyEntries)[index];
        }
    }
}
