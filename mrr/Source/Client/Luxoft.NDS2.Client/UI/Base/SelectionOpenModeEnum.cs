﻿using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Base
{
    /// <summary>
    ///     Режимы открыти ЭФ выборки
    /// </summary>
    public enum SelectionOpenModeEnum
    {
        [Description("Создание выборки")] 
        NewSelection = 0,
        [Description("Создание шаблона")] 
        NewTemplate = 1,
        [Description("Копирование шаблона")] 
        CopyTemplate = 2,
        [Description("Редактирование выборки")] 
        EditSelection = 3,
        [Description("Редактирование выборки по шаблону")]
        EditSelectionByTemplate = 4,
        [Description("Редактирование шаблона")]
        EditTemplate = 5,
        [Description("Просмотр выборки")]
        ViewSelection = 6,
        [Description("Просмотр выборки по шаблону")]
        ViewSelectionByTemplate = 7,
        [Description("Просмотр шаблона")]
        ViewTemplate = 8,
        [Description("Согласование")] 
        ApproveSelection = 9,
        [Description("Согласование")]
        ApproveSelectionByTemplate = 10

    }
}