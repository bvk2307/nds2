﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using CommonComponents.Directory;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Base
{
    class CurrentUserInfo
    {
        public CurrentUserInfo(IUserInfoService service)
        {
            var wi = WindowsIdentity.GetCurrent();
            UserName = wi != null
                ? wi.Name
                : Thread.CurrentPrincipal.Identity.Name;

            UserSid = (wi != null && wi.User != null)
                ? wi.User.Value
                : string.Empty;

            if (!Utils.IsUserInDomain())
            {
                UserDisplayName = UserName.ToLower();
                return;
            }

            UserDisplayName = (service != null && !string.IsNullOrEmpty(UserSid))
                    ? service.GetUserInfoBySID(UserSid).DisplayName
                    : string.Empty;
        }

        public string UserName { get; private set; }

        public string UserDisplayName { get; private set; }

        public string UserSid { get; private set; }
    }
}
