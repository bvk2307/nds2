﻿using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason;

namespace Luxoft.NDS2.Client.UI.Base
{
    public static class DictionaryAnnulmentReasonHelper
    {
        public static DictionaryAnnulmentReason DictionaryAnnulmentReason(this WorkItem workItem)
        {
            return 
                AppCache.Current.AddOrGetExisting(
                    () => new DictionaryAnnulmentReason(Load(workItem)),
                    ServerOperations.BP_AnnulReason,
                    TimeSpan.FromHours(24));
        }

        private static List<AnnulmentReason> Load(WorkItem workItem)
        {
            List<AnnulmentReason> result;

            if (workItem
                .GetWcfServiceProxy<IDictionaryDataService>()
                .ExecuteNoHandling(
                    proxy => proxy.GetAnnulmentReasonDictionary(),
                    out result))
            {
                return result;
            }

            return new List<AnnulmentReason>();
        }
    }
}
