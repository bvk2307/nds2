﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Configuration;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.Execution;
using FLS.CommonComponents.App.Services.Exrtensions;
using FLS.CommonComponents.Lib.Execution;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Config;
using Luxoft.NDS2.Client.Config.GraphTree;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.Services.Cache;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Client.UI.Declarations;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.KnpResultDocumentSelect;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentParams;
using Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Base
{
    public partial class BasePresenter<TV>  where TV : IViewBase
    {
        #region Карточка декларации
        
        /// <summary> Returns the common declaration card opener for the default chain access with the direxction defined by <see cref="UserRestrictionConfig"/>. </summary>
        /// <returns> The common declaration card opener for the default chain access with the direxction defined by <see cref="UserRestrictionConfig"/>. </returns>
        public IDeclarationCardOpener GetDeclarationCardOpener()
        {
            IDeclarationCardOpener declarationCardOpenerCommon =
                Services.InitOrGetService<ChainAccessModel, IDeclarationCardOpener, IDeclarationCardOpener>( GetDeclarationCardOpener, 
                                                new ChainAccessModel( new DirectionCreator( UserRestrictionConfig ) ) );

            return declarationCardOpenerCommon;
        }

        /// <summary> Returns a declaration card opener for the defined chain access. </summary>
        /// <returns> The common declaration card opener for the defined chain access. </returns>
        public IDeclarationCardOpener GetDeclarationCardOpener(ChainAccessModel model)
        {
            var opener = 
                new DeclarationCardOpener(
                    PresentationContext, 
                    WorkItem, 
                    UserRoles.Select(x => x.Name).ToArray(), 
                    model);

            opener.AfterOpen += 
                (mSec) => 
                    LogActivity(
                        ActivityLogEntry.New(
                            ActivityLogType.Chapter17OpenTime, 
                            mSec));

            return opener;
        }

        #endregion

        # region Карточка налогоплательщика

        public ITaxPayerDetails GetTaxPayerDetailsViewer()
        {
            return new TaxPayerDetailsViewer(
                    WorkItem,
                    new ChainAccessModel(new DirectionCreator(UserRestrictionConfig)));
        }

        public virtual void ViewTaxPayerByKppEffective(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            var result = 
                new TaxPayerDetailsViewer(
                    WorkItem,
                    new ChainAccessModel(new DirectionCreator(UserRestrictionConfig)))
                    .ViewByKppEffective(inn, kpp, direction);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public virtual void ViewTaxPayerByKppOriginal(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            var result = 
                new TaxPayerDetailsViewer(
                    WorkItem,
                    new ChainAccessModel(new DirectionCreator(UserRestrictionConfig))).ViewByKppOriginal(inn, kpp, direction);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }
        
        # endregion

        #region Карточка расхождения

        public void ViewClaimDetails(long id)
        {
            new ClaimDetailsViewer(WorkItem).ViewClaimDetails(id);
        }

        public void ViewClaimDetails(DiscrepancyDocumentInfo discrepancyDocument)
        {
            new ClaimDetailsViewer(WorkItem).ViewClaimDetails(discrepancyDocument);
        }

        public void ViewDiscrepancyDetails(long id)
        {
            if (IsUserEligibleForRole(Constants.SystemPermissions.Operations.RoleAnalyst)
                || IsUserEligibleForRole(Constants.SystemPermissions.Operations.RoleMedodologist)
                || IsUserEligibleForRole(Constants.SystemPermissions.Operations.RoleInspector)
                || IsUserEligibleForRole(Constants.SystemPermissions.Operations.RoleObserver))
            {
                bool discrepancyIsClosed = false;
                if (ExecuteServiceCall(() => GetServiceProxy<IDiscrepancyDataService>().DiscrepancyIsClosed(id),
                    res => { discrepancyIsClosed = res.Result; })) { }

                if (discrepancyIsClosed)
                {
                    View.ShowError("На текущий момент просмотр карточки расхождения невозможен");
                }
                else
                {
                    var managementService = this.WorkItem.Services.Get<IWindowsManagerService>();

                    Guid viewId = Guid.NewGuid();
                    Guid featureId = Guid.NewGuid();

                    var newWi = this.WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

                    var fc = new FeatureContextBase(featureId);
                    var pc = new PresentationContext(viewId, null, fc);

                    if (!managementService.Contains(viewId))
                    {
                        var searchView = managementService.AddNewSmartPart<Discrepancies.DiscrepancyCard.View>(newWi, viewId.ToString(), pc, newWi, id);
                        managementService.Show(pc, newWi, searchView);
                    }
                }
            }
            else
            {
                View.ShowError("Недостаточно прав на просмотр карточки расхождения");
            }
        }

        public void ViewDiscrepancyDetails(Discrepancy item)
        {
            ViewDiscrepancyDetails(item.DiscrepancyId);
        }

        #endregion

        #region Карточка КС

        public void ViewControlRatioDetails(ControlRatio input)
        {
            ViewControlRatioDetails(input.Id);
        }

        public void ViewControlRatioDetails(long id)
        {
            var roles = new[]
            {
                Constants.SystemPermissions.Operations.RoleAnalyst,
                Constants.SystemPermissions.Operations.RoleApprover,
                Constants.SystemPermissions.Operations.RoleMedodologist,
                Constants.SystemPermissions.Operations.RoleInspector,
                Constants.SystemPermissions.Operations.RoleObserver
            };
            if (!IsUserEligibleForRole(roles))
            {
                return;
            }

            var managementService = this.WorkItem.Services.Get<IWindowsManagerService>();

            Guid viewId = Guid.NewGuid();
            Guid featureId = Guid.NewGuid();

            var newWi = this.WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

            var fc = new FeatureContextBase(featureId);
            var pc = new PresentationContext(viewId, null, fc);
            if (!managementService.Contains(viewId))
            {
                var title = string.Format("Контрольное соотношение");

                pc.WindowTitle = title;
                pc.WindowDescription = title;

                var searchView = managementService.AddNewSmartPart<ControlRatios.Card.View>(newWi, viewId.ToString(), pc, newWi, id);
                managementService.Show(pc, newWi, searchView);
            }
        }

        #endregion

        # region Отчет дерева связей

        public void ViewPyramidReport( DeclarationSummary declarationSummary )
        {
            string errorMessage;
            bool error = CreateMessges( declarationSummary.LOAD_MARK, declarationSummary.ProcessingStage, out errorMessage );
            if ( error )
            {
                     View.ShowError( errorMessage );
            }
	    if ( !error )
                ViewPyramidReport( new GraphTreeParameters( declarationSummary ) );
        }

        public void ViewPyramidReport( DeclarationBrief declarationBrief )
        {
            string errorMessage;
            bool error = CreateMessges( declarationBrief.LOAD_MARK, declarationBrief.ProcessingStage, out errorMessage);
            if ( error )
            {
                     View.ShowError( errorMessage );
            }
            if ( !error )
                ViewPyramidReport( new GraphTreeParameters( declarationBrief ) );
        }

        public void ViewPyramidReport( GraphTreeParameters treeParameters )
        {
            IConfigurationDataService configurationDataService = Services.Get<IConfigurationDataService>( ensureExists: true );
            DependencyTreeSection dependencyTreeSection = configurationDataService.GetSection<DependencyTreeSection>( ProfileInfo.Default );

            ISecurityService securityService = 
                Services.Get<IClientContextService>( ensureExists: true ).CreateCommunicationProxy<ISecurityService>( Constants.EndpointName );

            IPyramidDataCachingService pyramidDataService = 
                Services.InitOrGetService<string, IPyramidDataService, AppCache, GraphTreeDataCacheSettings, IPyramidDataCachingService>( 
                    typeof(PyramidDataCachingService), Services.GetServiceProxy<IPyramidDataService>, Constants.EndpointName, 
                    Cache, dependencyTreeSection.DataCacheSettings );

            IServiceCallExecutorManager callExecutorManager = Services.Get<IServiceCallExecutorManager>( ensureExists: true );
            Contract.Assume( callExecutorManager != null );

            IServiceCallDispatcherFactory serviceCallDispatcherFactory = 
                Services.InitOrGetService<IServiceCallExecutorManager, IServiceCallDispatcherFactory>( typeof(ServiceCallDispatcherFactory), callExecutorManager );

            IThreadInvokerWErrorHandling threadInvokerWErrorHandling = Services.Get<IThreadInvokerWErrorHandling>( ensureExists: true );
            IExecutorWErrorHandling executorWErrorHandling = Services.Get<IExecutorWErrorHandling>( ensureExists: true );
            IGraphTreeDataManager graphTreeDataManager = 
                Services.InitOrGetService<IServiceCallDispatcherFactory, IServiceCallWrapper, IExecutorWErrorHandling, 
                                          IPyramidDataService, IClientLogger, IGraphTreeDataManager>(
                    typeof( GraphTreeDataManager ), serviceCallDispatcherFactory, this, executorWErrorHandling, pyramidDataService, ClientLogger );
            IDeclarationUserPermissionsPolicy declarationUserPermissionsPolicy = 
                Services.InitOrGetService<BasePresenter<TV>, IUserPermissionRequestService, IDeclarationUserPermissionsPolicy>( 
                    typeof(DeclarationUserPermissionsPolicy<TV>), this, securityService );

	        //apopov 21.7.2016	//fix of GNIVC_NDS2-5566
            IInstanceProvider<INodeTableReporter> nodeTableProvider = new NodeTableReporterProvider( ClientLogger );
                //Services.InitOrGetService<IClientLogger, IInstanceProvider<INodeTableReporter>>( 
                //    typeof(NodeTableReporterProvider), ClientLogger );
            IInstanceProvider<INodeAllTreeReporter> nodeAllTreeProvider = 
                new NodeAllTreeReporterProvider( ClientLogger, dependencyTreeSection.ReportSettings );
                //Services.InitOrGetService<IClientLogger, GraphTreeReportSettings, IInstanceProvider<INodeAllTreeReporter>>( 
                //    typeof(NodeAllTreeReporterProvider), ClientLogger, dependencyTreeSection.ReportSettings );
            IInstanceProvider<INodeVisibleTreeReporter> nodeVisibleTreeProvider = new NodeVisibleTreeReporterProvider( ClientLogger );
                //Services.InitOrGetService<IClientLogger, IInstanceProvider<INodeVisibleTreeReporter>>( 
                //    typeof(NodeVisibleTreeReporterProvider), ClientLogger );

            IDeclarationCardOpener declarationCardOpenerCommon = GetDeclarationCardOpener();

            IWindowsManagerService winManager = Services.Get<IWindowsManagerService>( ensureExists: true );
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            WorkItem workItem = WorkItem.WorkItems.AddNew<WorkItem>( Guid.NewGuid().ToString() );
            var featureContext = new FeatureContextBase( featureId );
            var presentationContext = new PresentationContext( viewId, null, featureContext );

            GraphTreeContainer view = winManager.AddNewSmartPart<GraphTreeContainer>( 
                                workItem, viewId.ToString(), presentationContext, workItem, treeParameters, threadInvokerWErrorHandling );

            var presenter = new Presenter( 
                treeParameters, presentationContext, workItem, view, pyramidDataService, graphTreeDataManager, 
                declarationUserPermissionsPolicy, nodeAllTreeProvider, nodeVisibleTreeProvider, nodeTableProvider, 
                declarationCardOpenerCommon, dependencyTreeSection, executorWErrorHandling );

            view.AcceptPresenter( presenter );

            winManager.Show( presentationContext, workItem, view );
        }

        #endregion

        # region Карточка АТ СФ/КС

        public void ViewClaimDetails(DiscrepancyDocumentInfo discrepancyDocument, ChainAccessModel chain = null)
        {
            new ClaimDetailsViewer(WorkItem).ViewClaimDetails(discrepancyDocument);
        }

        #endregion

        #region Карточка АИ 93/93.1

        public void ViewReclaimDetails(DiscrepancyDocumentInfo discrepancyDocument)
        {
           new ReclaimDetailsViewer(WorkItem).ViewReclaimDetails(discrepancyDocument);
        }

        public void ViewReclaimDetails(DiscrepancyDocumentInfo discrepancyDocument, ChainAccessModel chainData)
        {
            new ReclaimDetailsViewer(WorkItem, chainData).ViewReclaimDetails(discrepancyDocument);
        }

        #endregion

        # region

        public void ViewExplainReplyDetails(ExplainReplyCardData exlpainReplyCardData)
        {
            bool allowOperationExplainCardOpen = false;

            if (exlpainReplyCardData.Regim == ExplainRegim.Edit)
            {
                allowOperationExplainCardOpen = exlpainReplyCardData
                    .UserAccessOpearation
                    .AllowOpearationExplainEdit();
            }
            if (exlpainReplyCardData.Regim == ExplainRegim.View)
            {
                allowOperationExplainCardOpen = exlpainReplyCardData
                    .UserAccessOpearation
                    .AllowOpearationExplainView();
            }

            if (allowOperationExplainCardOpen)
            {
                var managementService = this.WorkItem.Services.Get<IWindowsManagerService>();

                Guid viewId = Guid.NewGuid();
                Guid featureId = Guid.NewGuid();

                var newWi = this.WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

                var fc = new FeatureContextBase(featureId);
                var pc = new PresentationContext(viewId, null, fc);

                if (!managementService.Contains(viewId))
                {
                    string title = string.Empty;

                    if (exlpainReplyCardData.ExplainReplyInfo != null)
                    {
                        string inn = String.Empty;
                        string typeName = String.Empty;
                        if (exlpainReplyCardData.DeclarationSummary != null)
                        {
                            inn = exlpainReplyCardData.DeclarationSummary.INN;
                        }
                        if (exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Explain)
                        {
                            typeName = "(Пояснение)";
                            if (exlpainReplyCardData.ExplainReplyInfo.RECEIVE_BY_TKS)
                            {
                                typeName = "(Формализованное пояснение)";
                            }
                            title = string.Format("{0} {1}", inn, typeName);
                        }
                        else if (exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93 ||
                                 exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93_1)
                        {
                            typeName = "(Ответ)";
                        }
                        title = string.Format("{0} {1}", inn, typeName);
                    }

                    pc.WindowTitle = title;
                    pc.WindowDescription = title;

                    var viewExplainReply = managementService.AddNewSmartPart<Explain.Manual.View>(newWi, viewId.ToString(), pc, newWi,
                        exlpainReplyCardData);
                    managementService.Show(pc, newWi, viewExplainReply);
                }
            }
            else
            {
                string typeName = String.Empty;
                if (exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Explain)
                {
                    typeName = "пояснения";
                }
                else if (exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93 ||
                         exlpainReplyCardData.ExplainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93_1)
                {
                    typeName = "ответа";
                }

                var notifier = this.WorkItem.Services.Get<INotifier>();
                notifier.ShowError(string.Format("Недостаточно прав на просмотр карточки {0}", typeName));
            }
        }
 
		 /// <summary>
		  /// источник для заполнения "Карточка Пояснение/Ответ"
		 /// </summary>
		 /// <param name="explainReplayId"></param>
		 /// <param name="userAccessOpearation"></param>
		 /// <param name="explainRegim"></param>
		 /// <returns></returns>
		  public ExplainReplyCardData GetExplainCardRcd(
			  long explainReplayId,
			  UserAccessOpearation userAccessOpearation = null)
		  {
              var rcd = new ExplainReplyCardData(explainReplayId, ExplainRegim.View);
			  rcd.UserAccessOpearation = userAccessOpearation ?? new UserAccessOpearation(SecurityService);
			  rcd.ExplainReplyInfo = GetExplainReplyInfo(rcd.ExplainReplyId);

			  if (rcd.ExplainReplyInfo != null)
			  {
				  rcd.DeclarationSummary = GetDeclaration(rcd.ExplainReplyInfo.DECLARATION_VERSION_ID);
                  SetExplainRegimDependingOnStatuses(rcd);
			  }
			  return rcd;
		  }
        
        public ExplainDetailsData GetExplainDetails(long explainId, ExplainType type)
        {
            ExplainDetailsData ret = null;
            if (ExecuteServiceCall(
                       () => GetServiceProxy<IExplainReplyDataService>().GetExplainDetails(explainId, type),
                       (result) => ret = result.Result))
            { }
            return ret;
        }

        private ExplainReplyInfo GetExplainReplyInfo(long explainReplyInfoId)
		  {
			  ExplainReplyInfo ret = null;
			  if (ExecuteServiceCall(
						 () => GetServiceProxy<IExplainReplyDataService>().GetExplainReplyInfo(explainReplyInfoId),
						 (result) => ret = result.Result))
			  { }
			  return ret;
		  }

		  protected DeclarationSummary GetDeclaration(long declarationId)
		  {
			  DeclarationSummary ret = null;
			  ExecuteServiceCall(
					() => GetServiceProxy<IDeclarationsDataService>().GetDeclaration(declarationId),
					result =>
					{
						ret = result.Result;
						if (result.Status != ResultStatus.Success)
						{
							LogError(string.Format(ResourceManagerNDS2.DeclarationMessages.DECLARATION_OPEN_ERROR_ZIP,
								 declarationId, result.Message));
						}
					});
			  return ret;
		  }

		  public bool IsPreviousExplainsOfDocumentInWorked(ExplainReplyInfo explainReplyInfo)
		  {
			  var ret = false;
			  ExecuteServiceCall(
					() => GetServiceProxy<IExplainReplyDataService>().IsPreviousExplainsOfDocumentInWorked(explainReplyInfo),
					result => ret = result.Result);
			  return ret;
		  }

		  private void SetExplainRegimDependingOnStatuses(ExplainReplyCardData explainReplyCardData)
		  {
			  if (explainReplyCardData.ExplainReplyInfo.status_id == ExplainReplyStatus.SendToMC || explainReplyCardData.ExplainReplyInfo.status_id == ExplainReplyStatus.ProcessedInMC)
			  {
				  explainReplyCardData.Regim = ExplainRegim.View;
			  }
			  else if (explainReplyCardData.ExplainReplyInfo.status_id == ExplainReplyStatus.NotProcessed)
			  {
				  if (IsPreviousExplainsOfDocumentInWorked(explainReplyCardData.ExplainReplyInfo))
				  {
					  explainReplyCardData.Regim = ExplainRegim.Edit;
				  }
			  }
		  }


        # endregion

        # region Отображение настроечных форм
    
        public void OpenLoadingInspection()
        {
            var roles = new[]
            {
                Constants.SystemPermissions.Operations.RoleMedodologist
            };
            if (!IsUserEligibleForRole(roles))
            {
                return;
            }

            var managementService = this.WorkItem.Services.Get<IWindowsManagerService>();

            Guid viewId = Guid.NewGuid();
            Guid featureId = Guid.NewGuid();

            var newWi = this.WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

            var fc = new FeatureContextBase(featureId);
            var pc = new PresentationContext(viewId, null, fc);
            if (!managementService.Contains(viewId))
            {
                pc.WindowTitle = ResourceManagerNDS2.SettingsMRRLoadingInspection;
                pc.WindowDescription = ResourceManagerNDS2.SettingsMRRLoadingInspection;

                var searchView = managementService.AddNewSmartPart<SettingsMRR.LoadingInspection.View>(newWi, viewId.ToString(), pc, newWi);
                managementService.Show(pc, newWi, searchView);
            }
        }


        # endregion

        #region Формы контрагентов

        public void ViewContragents(DeclarationSummary declaration)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var title = string.Format("{0} (Список контрагентов)", declaration.INN);
            var presentationContext =
                new PresentationContext(viewId, null, featureContext)
                {
                    WindowTitle = title,
                    WindowDescription = title
                };
            var view =
                winManager.AddNewSmartPart<ContragentView>(
                    workItem,
                    viewId.ToString(),
                    presentationContext,
                    workItem,
                    declaration);

            winManager.Show(presentationContext, workItem, view);
        }

        public void ViewContragentParams(DeclarationSummary declaration, string contractorInn, int chapter)
        {
            #warning Определиться с правами

            if (IsUserEligibleForOperation(Constants.SystemPermissions.Operations.OOREntrу))
            {
                var winManager = WorkItem.Services.Get<IWindowsManagerService>();
                var viewId = Guid.NewGuid();
                var featureId = Guid.NewGuid();
                var workItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
                var featureContext = new FeatureContextBase(featureId);
                var titles = new Dictionary<int, string>
                {
                    {8, "Книга покупок"},
                    {9, "Книга продаж"},
                    {10, "Журнал выставленных СФ"},
                    {11, "Журнал полученных СФ"},
                    {12, "СФ (п.5 ст.173 НК РФ"},
                };
                var title = string.Format("{0} ({1})", declaration.INN, titles[chapter]);
                var presentationContext =
                    new PresentationContext(viewId, null, featureContext)
                    {
                        WindowTitle = title,
                        WindowDescription = title
                    };
                var view =
                    winManager.AddNewSmartPart<ContragentParamsView>(
                        workItem,
                        viewId.ToString(),
                        presentationContext,
                        workItem,
                        declaration,
                        contractorInn,
                        chapter);

                winManager.Show(presentationContext, workItem, view);
            }
            else
            {
                View.ShowError("Отсутствуют права доступа");
            }
        }

        #endregion

        #region Акты и решения

        public bool ViewActSelectionCard(
            long documentId, 
            IAppendActDiscrepancyService managementService,
            IMessageView msgView,
            long comparisonDataVersion)
        {
            IKnpResultDocumentSelectView selectView = new ActDiscrepancySelectView();
            selectView.MessageView = msgView;

            IServiceCallErrorHandler errorHandler = new CommonServiceRequestErrorHandler(View);
            ISettingsProvider settingsProvider = SettingsProvider(selectView.GetType().ToString());

            var presenter = new ActDiscrepancySelectPresenter(
                documentId, 
                selectView, 
                _requestExecutor, 
                managementService, 
                UIThreadExecutor.CurrentExecutor,
                errorHandler,
                settingsProvider,
                GetServiceProxy<IDeclarationsDataService>(),
                comparisonDataVersion);

            return selectView.ShowModal();
        }

        public bool ViewDecisionSelectionCard(
            long documentId,
            IAppendDecisionDiscrepancyService managementService,
            IMessageView msgView,
            long comparisonDataVersion)
        {
            IKnpResultDocumentSelectView selectView = new DecisionDiscrepancySelectView();
            selectView.MessageView = msgView;

            IServiceCallErrorHandler errorHandler = new CommonServiceRequestErrorHandler(View);
            ISettingsProvider settingsProvider = SettingsProvider(selectView.GetType().ToString());

            var presenter = new DecisionDiscrepancySelectPresenter(
                documentId, 
                selectView, 
                _requestExecutor, 
                managementService, 
                UIThreadExecutor.CurrentExecutor,
                errorHandler,
                settingsProvider,
                GetServiceProxy<IDeclarationsDataService>(),
                comparisonDataVersion);

            return selectView.ShowModal();
        }

        #endregion


        private static bool CreateMessges( DeclarationProcessignStage allRevisionStage, DeclarationProcessignStage currentRevisionStage, 
            out string errorMessage)
        {
            errorMessage = null;

            string valueBuffer;
            var item = Tuple.Create(allRevisionStage, currentRevisionStage);

            var errorMessagesDictionary =
                new Dictionary<Tuple<DeclarationProcessignStage, DeclarationProcessignStage>, string>()
                {
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY }
                };

            if ( errorMessagesDictionary.TryGetValue( item, out valueBuffer ) )
                errorMessage = valueBuffer;

            return errorMessage != null;
        }
    }
}
