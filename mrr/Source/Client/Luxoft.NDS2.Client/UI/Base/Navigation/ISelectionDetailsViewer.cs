﻿using Luxoft.NDS2.Client.UI.Selections;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public interface ISelectionDetailsViewer
    {
        void Open(
            SelectionListItemViewModel selection,
            ISelectionListView selectionListView,
            ISelectionServiceProxy proxy, 
            SelectionOpenModeEnum mode);
    }
}
