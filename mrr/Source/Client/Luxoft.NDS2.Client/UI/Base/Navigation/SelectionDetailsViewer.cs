﻿using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Selections;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public class SelectionDetailsViewer : ISelectionDetailsViewer
    {
        private readonly WorkItem _workItem;

        public SelectionDetailsViewer(WorkItem workItem)
        {
            _workItem = workItem;
        }

        private static readonly Dictionary<SelectionOpenModeEnum, string> TitleDictionary = new Dictionary
            <SelectionOpenModeEnum, string>()
            {
                {SelectionOpenModeEnum.ViewSelection, ResourceManagerNDS2.SelectionCard.ViewSelectionTitle},
                {SelectionOpenModeEnum.ViewTemplate, ResourceManagerNDS2.SelectionCard.ViewTemplateTitle},
                {SelectionOpenModeEnum.ViewSelectionByTemplate, ResourceManagerNDS2.SelectionCard.ViewSelectionByTemplateTitle},
                {SelectionOpenModeEnum.EditSelection, ResourceManagerNDS2.SelectionCard.EditSelectionTitle},
                {SelectionOpenModeEnum.EditTemplate, ResourceManagerNDS2.SelectionCard.EditTemplateTitle},
                {SelectionOpenModeEnum.EditSelectionByTemplate, ResourceManagerNDS2.SelectionCard.EditSelectionByTemplateTitle},
                {SelectionOpenModeEnum.ApproveSelection, ResourceManagerNDS2.SelectionCard.ApproveSelectionTitle},
                {SelectionOpenModeEnum.ApproveSelectionByTemplate, ResourceManagerNDS2.SelectionCard.ApproveSelectionByTemplateTitle},
                {SelectionOpenModeEnum.NewSelection, ResourceManagerNDS2.SelectionCard.NewSelectionTitle},
                {SelectionOpenModeEnum.NewTemplate, ResourceManagerNDS2.SelectionCard.NewTemplateTitle},
                {SelectionOpenModeEnum.CopyTemplate, ResourceManagerNDS2.SelectionCard.CopyTemplateTitle}
            };

        public void Open(
            SelectionListItemViewModel selection, 
            ISelectionListView selectionListView,
            ISelectionServiceProxy proxy,
            SelectionOpenModeEnum mode)
        {
            var sur = _workItem.DictionarySur();
            _workItem.Show<SelectionCardPresenter, View>(
               (px, wi) => new object[] { px, wi, sur},
                (px, wi, vw) => new SelectionCardPresenterCreator(px, wi, vw)
                        .Create(vw, selectionListView, this, proxy, mode, selection == null ? (long?)null : selection.Id), 
                string.Format(TitleDictionary[mode], selection == null ? string.Empty : selection.Name));
        }
     
    }
}
