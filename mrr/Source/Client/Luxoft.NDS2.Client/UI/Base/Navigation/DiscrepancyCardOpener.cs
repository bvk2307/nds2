﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyCard;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public interface IDiscrepancyCardOpener
    {
        CardOpenResult Open(DeclarationDiscrepancy discrepancy);
    }

    class DiscrepancyCardOpener : IDiscrepancyCardOpener 
    {
        private readonly WorkItem _workItem;
        private readonly INotifier _notifier;
        private readonly IClientLogger _logger;
        private readonly IDiscrepancyDataService _service;

        public DiscrepancyCardOpener(
            WorkItem workItem)
        {
            _workItem = workItem;
            _service = _workItem.GetWcfServiceProxy<IDiscrepancyDataService>();
            _notifier = _workItem.Services.Get<INotifier>();
            _logger = _workItem.Services.Get<IClientLogger>(true);
        }
        
        public CardOpenResult Open(DeclarationDiscrepancy discrepancy)
        {
            var managementService = _workItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var discrepancyWorkItem = _workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);
            
            var discrepancySummaryInfo = new DiscrepancySummaryInfo(discrepancy);

            var view =
                managementService.AddNewSmartPart<Discrepancies.DiscrepancyCard.View>(
                    discrepancyWorkItem,
                    viewId.ToString(),
                    presentationContext,
                    discrepancyWorkItem,
                    discrepancySummaryInfo);

            var presenter = new DiscrepancyCardPresenter(view, 
                                presentationContext, 
                                discrepancyWorkItem, 
                                discrepancySummaryInfo);
            
            managementService.Show(presentationContext, discrepancyWorkItem, view);

            return new CardOpenSuccess();
        }
    }
}
