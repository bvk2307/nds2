﻿using CommonComponents.Communication;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.Models;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Diagnostics;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Base
{
    public interface IDeclarationCardOpener
    {
        CardOpenResult Open(IDeclarationIdentity declaration);

        CardOpenResult Open(long id, Direction direction = Direction.Invariant, DeclarationInfoSource info = null);

        CardOpenResult Open(long id, uint chapter, string invoiceNumber, Direction direction = Direction.Invariant);

        CardOpenResult Open(long id, string invoiceKey, Direction direction = Direction.Invariant, string contractorInn = null);

        CardOpenResult Open(string invoiceKey, Direction direction = Direction.Invariant, string contractorInn = null);

        CardOpenResult OpenActualDeclaration(string invoiceKey, Direction direction = Direction.Invariant, string contractorInn = null);

        CardOpenResult Open(string inn, string kpp, string year, string period, Direction direction = Direction.Invariant);

        CardOpenResult Open(
            DeclarationSummary declarationData,
            uint? chapter,
            string invoiceNumber,
            string contractorInn,
            string invoiceKey,
            long ordinalNumber,
            DeclarationInfoSource info,
            Direction direction = Direction.Invariant);
    }

    public class DeclarationCardOpener : IDeclarationCardOpener
    {
        private readonly PresentationContext _presentationContext;

        private readonly WorkItem _workItem;

        private readonly ChainAccessModel _chain;

        private readonly IDeclarationsDataService _service;

        private readonly string[] _roles;

        private Stopwatch _stopWatch;

        public DeclarationCardOpener(
            PresentationContext presentationContext,
            WorkItem workItem,
            string[] roles,
            ChainAccessModel chain)
        {
            _presentationContext = presentationContext;
            _workItem = workItem;
            _chain = chain;
            _roles = roles;
            _service = _workItem.Services.Get<IClientContextService>()
                .CreateCommunicationProxy<IDeclarationsDataService>(Constants.EndpointName);
        }

        public CardOpenResult Open(IDeclarationIdentity declaration)
        {
            return Open(declaration.DECLARATION_VERSION_ID);
        }

        public CardOpenResult Open(long zip, Direction direction = Direction.Invariant, DeclarationInfoSource info = null)
        {
            return Open(
                new DeclarationIdentifier(zip),
                null,
                string.Empty,
                string.Empty,
                string.Empty,
                -1,
                direction,
                info);
        }

        public CardOpenResult Open(long id, uint chapter, string invoiceNumber,
            Direction direction = Direction.Invariant)
        {
            return Open(
                new DeclarationIdentifier(id),
                chapter,
                invoiceNumber,
                string.Empty,
                string.Empty,
                -1,
                direction);
        }

        public CardOpenResult Open(long id, string invoiceKey, Direction direction = Direction.Invariant,
            string contractorInn = null)
        {
            var key = new InvoiceKey(invoiceKey);

            return Open(
                new DeclarationIdentifier(id),
                key.Chapter,
                String.Empty,
                contractorInn ?? string.Empty,
                invoiceKey,
                key.OrdinalNumber,
                direction);
        }

        public CardOpenResult Open(string invoiceKey, Direction direction = Direction.Invariant,
            string contractorInn = null)
        {
            var key = new InvoiceKey(invoiceKey);

            return Open(
                new DeclarationIdentifier(key.DeclarationZip),
                key.Chapter,
                String.Empty,
                contractorInn ?? string.Empty,
                invoiceKey,
                key.OrdinalNumber,
                direction);
        }

        public CardOpenResult OpenActualDeclaration(string invoiceKey, Direction direction = Direction.Invariant,
            string contractorInn = null)
        {
            var key = new InvoiceKey(invoiceKey);

            return OpenActualDeclaration(
                new DeclarationIdentifier(key.DeclarationZip),
                key.Chapter,
                String.Empty,
                contractorInn ?? string.Empty,
                invoiceKey,
                key.OrdinalNumber,
                direction);
        }

        public CardOpenResult Open(string inn, string kpp, string year, string period, Direction direction = Direction.Invariant)
        {
            return Open(
                new ContractorPeriodDeclarationKey(inn, kpp, year, period),
                null,
                string.Empty,
                string.Empty,
                string.Empty,
                -1,
                direction);
        }

        private CardOpenResult Open(
            IDeclarationIdentifier identifier,
            uint? chapter,
            string invoiceNumber,
            string contractorInn,
            string invoiceKey,
            long ordinalNumber,
            Direction direction,
            DeclarationInfoSource info = null)
        {
            return Open(
                        identifier,
                        chapter,
                        invoiceNumber,
                        contractorInn,
                        invoiceKey,
                        ordinalNumber,
                        direction,
                        info,
                        () => { return _service.GetDeclaration(identifier.GetKey()); });

        }

        private CardOpenResult OpenActualDeclaration(
            IDeclarationIdentifier identifier,
            uint? chapter,
            string invoiceNumber,
            string contractorInn,
            string invoiceKey,
            long ordinalNumber,
            Direction direction,
            DeclarationInfoSource info = null)
        {
            return Open(
                        identifier,
                        chapter,
                        invoiceNumber,
                        contractorInn,
                        invoiceKey,
                        ordinalNumber,
                        direction,
                        info,
                        () => { return _service.GetActualDeclaration(identifier.GetKey()); });

        }

        private CardOpenResult Open(
                    IDeclarationIdentifier identifier,
                    uint? chapter,
                    string invoiceNumber,
                    string contractorInn,
                    string invoiceKey,
                    long ordinalNumber,
                    Direction direction,
                    DeclarationInfoSource info,
                    Func<OperationResult<DeclarationSummary>> getDeclaration)
        {
            _stopWatch = Stopwatch.StartNew();

            try
            {
                var response = getDeclaration();

                switch (response.Status)
                {
                    case Common.Contracts.DTO.ResultStatus.Success:

                        DeclarationSummary declaration = (DeclarationSummary)response.Result;
                        return Open(declaration, chapter, invoiceNumber, contractorInn, invoiceKey, ordinalNumber,
                            info,
                            direction);

                    case Common.Contracts.DTO.ResultStatus.NoDataFound:
                        return new CardOpenError(ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD);
                    default:
                        return new CardOpenError(string.Format("{0}. {1}",
                            ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD, response.Message));
                }
            }
            catch (Exception ex)
            {
                return new CardOpenError(
                                string.Format("{0}. {1}",
                                        ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD,
                                        ex.ToString()));
            }
        }

        public CardOpenResult Open(
            DeclarationSummary declarationData,
            uint? chapter,
            string invoiceNumber,
            string contractorInn,
            string invoiceKey,
            long ordinalNumber,
            DeclarationInfoSource info,
            Direction direction = Direction.Invariant)
        {
            if (_stopWatch == null || (_stopWatch != null && !_stopWatch.IsRunning))
                _stopWatch = Stopwatch.StartNew();

            var chain = _chain.Clone();

            if (!chain.IsStarted)
            {
                chain.Start(declarationData);
            }
            else if (!chain.TryMove(declarationData, direction))
            {
                return new CardOpenError(ResourceManagerNDS2.DeclarationMessages.DECLARATION_DETAILS_ACCESS_DENIED);
            }

            var model =
                chapter.HasValue
                    ? new DeclarationModel(declarationData, chain, _roles, chapter.Value, invoiceNumber, contractorInn,
                        invoiceKey, ordinalNumber)
                    : new DeclarationModel(declarationData, chain, _roles);
            model.InfoSource = info;

            var managementService = _workItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var declarationWorkItem = _workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);

            var view =
                managementService.AddNewSmartPart<Declarations.DeclarationCard.View>(
                    declarationWorkItem,
                    viewId.ToString(),
                    presentationContext,
                    declarationWorkItem,
                    model);

            view.AfterLoad += () =>
            {
                _stopWatch.Stop();

                if (AfterOpen != null)
                {
                    AfterOpen(_stopWatch.ElapsedMilliseconds);
                }
            };

            var presenter = new DeclarationCardPresenter(presentationContext, declarationWorkItem, view, model);    //calls inside the constructor View.AcceptPresenter(this)
            managementService.Show(presentationContext, declarationWorkItem, view);

            return new CardOpenSuccess();
        }

        public GenericEventHandler<long> AfterOpen;
    }
}
