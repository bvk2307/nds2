﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Explain.Manual;
using Luxoft.NDS2.Client.UI.Explain.Tks;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public class KnpDocumentDetailsViewer
    {
        # region .ctor

        public KnpDocumentDetailsViewer(WorkItem workItem, PresentationContext presentationContext)
        {
            _workItem = workItem;
            _presentationContext = presentationContext;
            InitViewers();
        }

        # endregion

        # region Инициализация

        private readonly WorkItem _workItem;

        private readonly PresentationContext _presentationContext;

        private readonly List<IKnpDocumentDetailsViewer> _viewers =
            new List<IKnpDocumentDetailsViewer>();

        private void InitViewers()
        {
            var dictionarySur = _workItem.DictionarySur();
            var claimViewer = new ClaimDetailsViewer(_workItem);
            var taxpayerViewer = 
                new TaxPayerDetailsViewer(
                    _workItem, 
                    new ChainAccessModel(new DirectionCreator(_workItem.ContractorDataPolicy())));

            _viewers.Add(claimViewer);
            _viewers.Add(new TksInvoiceExplainViewer(_workItem, dictionarySur, claimViewer, taxpayerViewer));
            _viewers.Add(new ControlRatioExplainViewer(_workItem, dictionarySur, claimViewer, taxpayerViewer));
            _viewers.Add(new OtherReasonExplainViewer(_workItem, dictionarySur, claimViewer, taxpayerViewer));
            _viewers.Add(new ManualExplainDetailsViewer(_workItem));
        }

        # endregion

        # region Открытие карточки документа КНП

        private IKnpDocumentDetailsViewer FindViewer(KnpDocument document)
        {
            var viewer = _viewers.FirstOrDefault(x => x.Supports(document));

            if (viewer == null)
            {
                throw new NotSupportedException(
                    string.Format(
                        "There's no viewer associated with {0}", 
                        document.GetType()));
            }

            return viewer;
        }

        public void View(KnpDocument document)
        {
            FindViewer(document).View(document);
        }

        public void Edit(KnpDocument document)
        {
            FindViewer(document).Edit(document);
        }

        # endregion
    }
}
