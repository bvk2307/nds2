﻿using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public interface IKnpDocumentDetailsViewer
    {
        bool Supports(KnpDocument document);

        void View(KnpDocument document);

        void Edit(KnpDocument document);
    }
}
