﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public class ClaimDetailsViewer : IClaimCardOpener, IKnpDocumentDetailsViewer
    {
        # region .ctor

        private readonly WorkItem _workItem;

        private readonly INotifier _notifier;

        public ClaimDetailsViewer(WorkItem workItem)
        {
            _workItem = workItem;
            _notifier = workItem.Services.Get<INotifier>(true);
        }

        # endregion

        # region IClaimCardOpener

        private bool ValidatePermissions()
        {
            var accessContext = _workItem.GetUserPermissions(new string[] { MrrOperations.Declaration.ClaimReclaimView, MrrOperations.Claim.ClaimView });
            return accessContext.Operations.Any();
        }

        public void ViewClaimDetails(long id)
        {
            var dataService = _workItem.GetWcfServiceProxy<IDiscrepancyDocumentService>();

            if (!ValidatePermissions())
            {
                _notifier.ShowError("Недостаточно прав на просмотр карточки требования");
                return;
            }

            DiscrepancyDocumentInfo data = null;
            if (!dataService.ExecuteNoHandling(proxy => proxy.GetDocument(id), out data))
            {
                _notifier.ShowError("Ошибка загрузки данных");
                return;
            }

            Open(data);
        }

        public void ViewClaimDetails(DiscrepancyDocumentInfo data)
        {
            if (!ValidatePermissions())
            {
                _notifier.ShowError("Недостаточно прав на просмотр карточки требования");
                return;
            }

            Open(data);
        }

        private void Open(DiscrepancyDocumentInfo data)
        {
            var managementService = _workItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var newWi = _workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var pc = new PresentationContext(viewId, null, new FeatureContextBase(Guid.NewGuid()));

            if (!managementService.Contains(viewId))
            {
                var title = string.Format("{0} ({1})", data.TaxPayerInn, data.DocType.EntryValue);

                pc.WindowTitle = title;
                pc.WindowDescription = title;

                var searchView =
                    managementService
                        .AddNewSmartPart<Luxoft.NDS2.Client.UI.Discrepancies.DiscrepancyDocuments.Claims.Card.View>(
                            newWi, viewId.ToString(), pc, newWi, data);
                managementService.Show(pc, newWi, searchView);
            }
        }

        # endregion

        # region IKnpDocumentDetailsViewer

        public bool Supports(KnpDocument document)
        {
            return document is ClaimInvoiceViewModel || document is ClaimControlRatioViewModel;
        }

        public void View(KnpDocument document)
        {
            if (!Supports(document))
            {
                throw new NotSupportedException();
            }

            ViewClaimDetails(document.GetId());
        }

        public void Edit(KnpDocument document)
        {
            throw new NotSupportedException();
        }

        # endregion
    }
}
