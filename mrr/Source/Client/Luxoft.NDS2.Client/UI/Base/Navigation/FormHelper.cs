﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public static class FormHelper
    {
        public static void Show<TView>(
            this WorkItem workItem,
            Action<WorkItem, TView> presenterCreator,
            string title = null)
        {
            workItem.Show<object, TView>(
                (px, wi) => new object[0],
                (px, wi, vw) => { presenterCreator(wi, vw); return null; },
                title);
        }

        public static void Show<TPresenter, TView>(
            this WorkItem workItem,
            Func<PresentationContext, WorkItem, object[]> viewCreator, 
            Func<PresentationContext, WorkItem, TView, TPresenter> presenterCreator,
            string title = null)
        {
            var winManager = workItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var newWorkItem = workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());     
            var presentationContext = new PresentationContext(viewId, null, new FeatureContextBase(Guid.NewGuid()));
            presentationContext.WindowTitle = title ?? string.Empty;
            presentationContext.WindowDescription = title ?? string.Empty;
            var view = winManager.AddNewSmartPart<TView>(newWorkItem, viewId.ToString(), viewCreator(presentationContext, newWorkItem));
            var presenter = presenterCreator(presentationContext, newWorkItem, view);

            winManager.Show(presentationContext, newWorkItem, view);
        }
    }
}
