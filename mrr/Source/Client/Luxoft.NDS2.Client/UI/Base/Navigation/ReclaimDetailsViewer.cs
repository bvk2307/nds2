﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public class ReclaimDetailsViewer : IKnpDocumentDetailsViewer
    {
        # region .ctor

        private readonly WorkItem _workItem;

        private readonly INotifier _notifier;

        private readonly ChainAccessModel _chainAccessModel;

        public ReclaimDetailsViewer(WorkItem workItem, ChainAccessModel chainAccessModel = null)
        {
            _workItem = workItem;
            _chainAccessModel = chainAccessModel;
            _notifier = workItem.Services.Get<INotifier>(true);
        }

        # endregion

        # region IClaimCardOpener

        private bool ValidatePermissions()
        {
            var accessContext = _workItem.GetUserPermissions(new string[] { MrrOperations.Declaration.ClaimReclaimView});
            return accessContext.Operations.Any();
        }

        private void Open(DiscrepancyDocumentInfo data)
        {
            var managementService = _workItem.Services.Get<IWindowsManagerService>();

            Guid viewId = Guid.NewGuid();
            var newWi = _workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var pc = new PresentationContext(viewId, null, new FeatureContextBase(Guid.NewGuid()));

            if (!managementService.Contains(viewId))
            {
                var title = string.Format("{0} ({1})", data.TaxPayerInn, data.DocType.EntryValue);

                pc.WindowTitle = title;
                pc.WindowDescription = title;

                var searchView = managementService.
                    AddNewSmartPart<Discrepancies.DiscrepancyDocuments.Reclaims.Card.View>(
                    newWi, viewId.ToString(), pc, newWi, data, _chainAccessModel);
                managementService.Show(pc, newWi, searchView);
            }
        }

        public void ViewReclaimDetails(long id)
        {

            var dataService = _workItem.GetWcfServiceProxy<IDiscrepancyDocumentService>();

            if (!ValidatePermissions())
            {
                _notifier.ShowError("Недостаточно прав на просмотр карточки истребования");
                return;
            }

            DiscrepancyDocumentInfo data = null;
            if (!dataService.ExecuteNoHandling(proxy => proxy.GetDocument(id), out data))
            {
                _notifier.ShowError("Ошибка загрузки данных");
                return;
            }

            Open(data);
        }

        public void ViewReclaimDetails(DiscrepancyDocumentInfo data)
        {
            if (!ValidatePermissions())
            {
                _notifier.ShowError("Недостаточно прав на просмотр карточки истребования");
                return;
            }

            Open(data);
        }

        # endregion

        # region IKnpDocumentDetailsViewer

        public bool Supports(KnpDocument document)
        {
            return document is ReclaimInvoiceViewModel;
        }

        public void View(KnpDocument document)
        {
            var claim = document as ReclaimInvoiceViewModel;

            if (claim == null)
            {
                throw new NotSupportedException();
            }

            ViewReclaimDetails(claim.GetId());
        }

        public void Edit(KnpDocument document)
        {
            throw new NotSupportedException();
        }

        # endregion

    }
}
