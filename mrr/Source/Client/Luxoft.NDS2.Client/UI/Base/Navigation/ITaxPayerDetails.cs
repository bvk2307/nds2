﻿using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public interface ITaxPayerDetails
    {
        /// <summary>
        /// Открытие карточки НП по ИНН и эффективному КПП
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="kpp">КПП эффективный</param>
        /// <param name="direction">Направление</param>
        /// <returns></returns>
        CardOpenResult ViewByKppEffective(string inn, string kpp, Direction direction = Direction.Invariant);
        
        /// <summary>
        /// Открытие карточки НП по ИНН и оригинальному КПП
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="kpp">КПП</param>
        /// <param name="direction">Направление</param>
        /// <returns></returns>
        CardOpenResult ViewByKppOriginal(string inn, string kpp, Direction direction = Direction.Invariant);
    }
}
