﻿using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.TaxPayer.Helper;
using Luxoft.NDS2.Client.UI.TaxPayer.Details;
using Luxoft.NDS2.Common.Contracts.DTO;
using TaxPayerData = Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxPeriodSummary = Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog.TaxPeriodSummary;

namespace Luxoft.NDS2.Client.UI.Base.Navigation
{
    public class TaxPayerDetailsViewer : ITaxPayerDetails
    {
        private readonly WorkItem _workItem;

        private readonly IDeclarationsDataService _dataService;

        private readonly IDictionaryDataService _dictionaryService;

        private readonly ChainAccessModel _chain;

        public TaxPayerDetailsViewer(WorkItem workItem, ChainAccessModel chain)
        {
            _workItem = workItem;
            _dataService = _workItem.GetWcfServiceProxy<IDeclarationsDataService>();
            _dictionaryService = _workItem.GetWcfServiceProxy<IDictionaryDataService>();
            _chain = chain;
        }

        public CardOpenResult ViewByKppEffective(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            return View(inn, kpp, _dataService.GetTaxPayerByKppEffective, direction);
        }

        public CardOpenResult ViewByKppOriginal(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            return (string.IsNullOrEmpty(kpp))
                ? View(inn, kpp, (i, k) => _dataService.GetTaxPayerByInn(i), direction)
            : View(inn, kpp, _dataService.GetTaxPayerByKppOriginal, direction);
        }
        
        private CardOpenResult View(string inn, string kpp, Func<string, string, OperationResult<TaxPayerData>> getTaxPayer, Direction direction = Direction.Invariant)
        {
            try
            {
                TaxPayerData taxPayer;
                Dictionary<string, TaxPeriodSummary> periods;
                var response = getTaxPayer(inn, kpp);
                if (response.Status == ResultStatus.NoDataFound) 
                {
                    return new CardOpenError("Налогоплательщик не найден");

                    /*NOTE: этот кейс в 88 не отрабатывает корректно из-за серверной ошибки обращения к v$declaration_history*/
                    var responseDecl = _dataService.SearchLastDeclaration(inn, kpp);
                    if (responseDecl.Status == ResultStatus.NoDataFound)
                    {
                        return new CardOpenError(ResourceManagerNDS2.TAXPAYER_NOT_FOUND);
                    }
                    if (responseDecl.Status == ResultStatus.Success)
                    {
                        DeclarationSummary declSummary = responseDecl.Result;
                        taxPayer = TaxPayerHelper.CreateTaxPayer(declSummary);
                    }
                    else
                    {
                        return new CardOpenError(response.Message);
                    }
                    
                }
                else if (response.Status != ResultStatus.Success)
                {
                    return new CardOpenError(response.Message);
                }
                else
                {
                    taxPayer = response.Result;
                }

                var responsePeriods = _dictionaryService.GetTaxPeriodDictionary();
                if (responsePeriods.Status == ResultStatus.Success)
                {
                    periods = responsePeriods.Result.Select(v => new TaxPeriodSummary()
                    {
                        Code = v.Code,
                        Description = v.Description,
                        MonthOfBegin = v.Month == 0 ? (v.Quarter - 1)*3 + 1 : v.Month,
                        MonthOfEnd = v.Month == 0 ? (v.Quarter - 1)*3 + 3 : v.Month
                    }).ToDictionary(v => v.Code);
                }
                else
                {
                    periods = new Dictionary<string, TaxPeriodSummary>();
                }

                var chain = _chain.Clone();
                if (chain.IsStarted && !chain.TryMove(taxPayer, direction))
                {
                    return new CardOpenError("У Вас нет прав для просмотра данных НП");
                }

                return View(new TaxPayerModel(taxPayer, chain, _workItem.DictionarySur(), _workItem.DictionaryAnnulmentReason(), periods));
            }
            catch
            {
                return new CardOpenError("Ошибка при открытии карточки НП");
            }
        }
        
        private CardOpenResult View(TaxPayerModel model)
        {
            _workItem.Show<Presenter, View>(
                (cx, workItem) => new object[] { cx, workItem },
                (cx, workItem, view) => new Presenter(cx, workItem, view, model));

            return new CardOpenSuccess();
        }
    }
}
