﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public delegate void GenericEventHandler<T>(T data);

    public delegate void GenericEventHandler<T,V>(T first, V second);

    public delegate TResult FuncEventHandler<TParam, TResult>(TParam parameter);
}
