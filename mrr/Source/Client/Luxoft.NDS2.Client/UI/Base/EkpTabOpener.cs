﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Common.Contracts;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.UI.Base
{
    public class EkpTabOpener : ITabOpener
    {
        private readonly WorkItem _workItem;

        public EkpTabOpener(WorkItem workItem)
        {
            _workItem = workItem;
        }

        public void List<TView>()
        {
            OpenView<TView>((pc) => new Navigator.ViewMaster<TView>(pc));
        }

        public void Details<TView>(long requestId, long? id = null)
        {
            OpenView<TView>((pc) => new Navigator.ViewMaster<TView>(pc, requestId, id));
        }

        private void OpenView<TView>(Func<PresentationContext, object> viewCreator)
        {
            var managementService = _workItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var newWorkItem = _workItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var presentationContext = 
                new PresentationContext(viewId, null, new FeatureContextBase(Guid.NewGuid()));

            var view = viewCreator(presentationContext);

            newWorkItem.SmartParts.Add(view, viewId.ToString());
            managementService.Show(presentationContext, newWorkItem, view);

            newWorkItem.Services.Get<IUcVisualStateService>(true)
                .DoRefreshVisualEnvironment(presentationContext.Id);
        }
    }
}
