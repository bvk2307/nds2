﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base
{
    public class CommandToOperationMapper
    {
        private readonly Dictionary<string, string> commandToOperation =
            new Dictionary<string, string>();

        private readonly Dictionary<string, List<string>> operationToCommands =
            new Dictionary<string, List<string>>();

        private readonly Dictionary<string, Func<CommandResult>> commandActions =
            new Dictionary<string, Func<CommandResult>>();

        public CommandToOperationMapper WithCommand(
            string command, 
            string operationId,
            Func<CommandResult> action)
        {
            commandToOperation.Add(command, operationId);
            commandActions.Add(command, action);

            if (!operationToCommands.ContainsKey(operationId))
            {
                operationToCommands.Add(operationId, new List<string>());
            }
            operationToCommands[operationId].Add(command);

            return this;
        }

        public CommandResult Execute(string command)
        {
            return commandActions[command]();
        }

        public string OperationId(string command)
        {
            return commandToOperation[command];
        }

        public IEnumerable<string> Commands(string operationId)
        {
            if (!operationToCommands.ContainsKey(operationId))
            {
                return new string[0];
            }

            return operationToCommands[operationId].AsReadOnly();
        }
    }
}
