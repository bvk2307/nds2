﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformFullTaxPeriod : TransformFieldBase
    {
        protected string _fieldNameFullTaxPeriod;
        protected string _fieldNameTaxPeriod;
        protected string _fieldNameFiscalYear;

        public TransformFullTaxPeriod(QueryConditions conditions, string fieldNameFullTaxPeriod, string fieldNameTaxPeriod, string fieldNameFiscalYear)
            : base(conditions)
        {
            _fieldNameFullTaxPeriod = fieldNameFullTaxPeriod;
            _fieldNameTaxPeriod = fieldNameTaxPeriod;
            _fieldNameFiscalYear = fieldNameFiscalYear;
        }

        public override void TransformFiltering()
        {
            var filterFullTaxPeriods =
                _conditions.Filter.Where(p => p.ColumnName == _fieldNameFullTaxPeriod).ToList();
            _conditions.Filter =
                _conditions.Filter.Where(p => p.ColumnName != _fieldNameFullTaxPeriod).ToList();

            foreach (FilterQuery fq in filterFullTaxPeriods)
            {
                foreach (ColumnFilter cf in fq.Filtering)
                {
                    if (cf.Value == null)
                        continue;

                    var fullTaxPeriod = cf.Value as FullTaxPeriodInfo;
                    if (fullTaxPeriod != null)
                    {
                        SearchCriteriaHelper.AddFilter(_conditions, _fieldNameTaxPeriod, fq.FilterOperator, fullTaxPeriod.TaxPeriod, cf.ComparisonOperator);
                        SearchCriteriaHelper.AddFilter(_conditions, _fieldNameFiscalYear, fq.FilterOperator, fullTaxPeriod.FiscalYear, cf.ComparisonOperator);
                    }
                }
            }
        }

        public override void TransformSorting()
        {
            var existsTransformField = _conditions.Sorting.Any(p => p.ColumnKey == _fieldNameTaxPeriod);

            if (!existsTransformField)
                return;

            var newSortList = new List<ColumnSort>();

            foreach (var columnSort in _conditions.Sorting)
            {
                if (columnSort.ColumnKey == _fieldNameFullTaxPeriod)
                {
                    var col = (ColumnSort)columnSort.Clone();
                    col.ColumnKey = _fieldNameTaxPeriod;
                    newSortList.Add(col);
                    col = (ColumnSort)columnSort.Clone();
                    col.ColumnKey = _fieldNameFiscalYear;
                    newSortList.Add(col);
                }
                else
                {
                    newSortList.Add(columnSort);
                }
            }

            _conditions.Sorting = newSortList;
        }
    }
}
