﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public abstract class TransformFieldBase
    {
        protected QueryConditions _conditions;

        public TransformFieldBase(QueryConditions conditions)
        {
            _conditions = conditions;
        }

        public virtual void TransformFiltering()
        {
        }

        public virtual void TransformSorting()
        {
        }
    }
}
