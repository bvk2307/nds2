﻿using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformQueryCondition
    {
        private QueryConditions conditionsTransform = null;

        public TransformQueryCondition(QueryConditions conditions)
        {
            if (conditions != null)
            {
                conditionsTransform = (QueryConditions)conditions.Clone();
            }
        }

        public QueryConditions GetResultQueryConditions()
        {
            return conditionsTransform;
        }

        private bool IsApplicableTransform()
        {
            return (conditionsTransform != null);
        }

        public void AddFilterTaxPeriod(TaxPeriodBase selectedTaxPeriod, string fieldNameFullTaxPeriod, string fieldNameTaxPeriod, string fieldNameFiscalYear)
        {
            if (IsApplicableTransform())
            {
                if (selectedTaxPeriod != null)
                {
                    AddFilterTaxPeriod(selectedTaxPeriod, fieldNameFullTaxPeriod);
                    TransformTaxPeriodInternal(fieldNameFullTaxPeriod, fieldNameTaxPeriod, fieldNameFiscalYear);
                }
            }
        }

        private void AddFilterTaxPeriod(TaxPeriodBase selectedTaxPeriod, string fieldNameFullTaxPeriod)
        {
            foreach (var taxPeriod in selectedTaxPeriod.GetTaxPeriodsNeed())
                SearchCriteriaHelper.AddFilter(conditionsTransform,
                    fieldNameFullTaxPeriod,
                    FilterQuery.FilterLogicalOperator.Or,
                    new FullTaxPeriodInfo()
                    {
                        TaxPeriod = taxPeriod.Code,
                        Description = taxPeriod.Description,
                        FiscalYear = taxPeriod.Year
                    },
                    ColumnFilter.FilterComparisionOperator.Equals);
        }

        private void TransformTaxPeriodInternal(string fieldNameFullTaxPeriod, string fieldNameTaxPeriod, string fieldNameFiscalYear)
        {
            var transformer = TransformQueryConditionCreator.CreateTransformFullTaxPeriod(conditionsTransform, fieldNameFullTaxPeriod, fieldNameTaxPeriod, fieldNameFiscalYear);
            transformer.TransformFiltering();
            transformer.TransformSorting();
        }
    }
}
