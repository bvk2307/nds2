﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformExplainDecimal : TransformFieldBase
    {
        protected List<string> _fieldNames= new List<string>();

        public TransformExplainDecimal(QueryConditions conditions, List<string> fieldNames)
            : base(conditions)
        {
            _fieldNames.AddRange(fieldNames);
        }

        public override void TransformFiltering()
        {
            var filterTransforming =
                _conditions.Filter.Where(p => _fieldNames.Contains(p.ColumnName)).ToList();
            _conditions.Filter =
                _conditions.Filter.Where(p => !_fieldNames.Contains(p.ColumnName)).ToList();

            foreach (FilterQuery fq in filterTransforming)
            {
                foreach (ColumnFilter cf in fq.Filtering)
                {
                    if (cf.Value == null)
                        continue;

                    decimal? decimalValue = ParseDecimal(cf.Value);
                    if (decimalValue != null)
                        SearchCriteriaHelper.AddFilter(_conditions, fq.ColumnName, fq.FilterOperator, decimalValue, cf.ComparisonOperator);
                }
            }
        }

        private decimal? ParseDecimal(object value)
        {
            decimal? ret = null;
            if (value != null)
            {
                string stringValue = (string)value;
                stringValue = stringValue.Trim();
                stringValue = stringValue.Replace(",", ".");
                decimal decimalValue;
                if (decimal.TryParse(stringValue, out decimalValue))
                    ret = decimalValue;
            }
            return ret;
        }
    }
}
