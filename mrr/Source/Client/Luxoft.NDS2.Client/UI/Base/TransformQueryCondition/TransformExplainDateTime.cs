﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformExplainDateTime : TransformFieldBase
    {
        protected List<string> _fieldNames = new List<string>();

        public TransformExplainDateTime(QueryConditions conditions, List<string> fieldNames)
            : base(conditions)
        {
            _fieldNames.AddRange(fieldNames);
        }

        public override void TransformFiltering()
        {
            var filterTransforming =
                _conditions.Filter.Where(p => _fieldNames.Contains(p.ColumnName)).ToList();
            _conditions.Filter =
                _conditions.Filter.Where(p => !_fieldNames.Contains(p.ColumnName)).ToList();

            foreach (FilterQuery fq in filterTransforming)
            {
                foreach (ColumnFilter cf in fq.Filtering)
                {
                    if (cf.Value == null)
                        continue;

                    DateTime? dtValue = ParseDate(cf.Value);
                    if (dtValue != null)
                        SearchCriteriaHelper.AddFilter(_conditions, fq.ColumnName, fq.FilterOperator, dtValue, cf.ComparisonOperator);
                }
            }
        }

        private DateTime? ParseDate(object value)
        {
            DateTime? ret = null;
            if (value != null)
            {
                string stringValue = (string)value;
                stringValue = stringValue.Trim();
                DateTime dtValue;
                string currentFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                if (DateTime.TryParseExact(stringValue, currentFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out dtValue))
                    ret = dtValue;
                else
                {
                    if (DateTime.TryParse(stringValue, out dtValue))
                        ret = dtValue;
                }
            }
            return ret;
        }
    }
}
