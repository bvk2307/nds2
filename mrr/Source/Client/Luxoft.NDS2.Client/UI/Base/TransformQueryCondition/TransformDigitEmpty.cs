﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformDigitEmpty : TransformFieldBase
    {
        protected List<string> _fieldNames= new List<string>();

        public TransformDigitEmpty(QueryConditions conditions, List<string> fieldNames)
            : base(conditions)
        {
            _fieldNames.AddRange(fieldNames);
        }

        public override void TransformFiltering()
        {
            var filterConditionClone = (QueryConditions)_conditions.Clone();

            var filterTransforming =
                filterConditionClone.Filter.Where(p => _fieldNames.Contains(p.ColumnName)).ToList();

            foreach (FilterQuery filterQuery in filterTransforming)
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    if (columnFilter.Value == DBNull.Value || columnFilter.Value == null)
                    {
                        SearchCriteriaHelper.AddFilter(_conditions, filterQuery.ColumnName, filterQuery.FilterOperator, 0, columnFilter.ComparisonOperator);
                    }
                }
            }
        }
    }
}
