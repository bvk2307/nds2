﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base.TransformQueryCondition
{
    public class TransformQueryConditionCreator
    {
        public static TransformQueryCondition CreateTransformQueryCondition(QueryConditions conditions)
        {
            return new TransformQueryCondition(conditions);
        }

        public static TransformFullTaxPeriod CreateTransformFullTaxPeriod(QueryConditions conditions, string fieldNameFullTaxPeriod, string fieldNameTaxPeriod, string fieldNameFiscalYear)
        {
            return new TransformFullTaxPeriod(conditions, fieldNameFullTaxPeriod, fieldNameTaxPeriod, fieldNameFiscalYear);
        }

        public static TransformExplainDateTime CreateTransformExplainDateTime(QueryConditions conditions, List<string> fieldNames)
        {
            return new TransformExplainDateTime(conditions, fieldNames);
        }

        public static TransformExplainDecimal CreateTransformExplainDecimal(QueryConditions conditions, List<string> fieldNames)
        {
            return new TransformExplainDecimal(conditions, fieldNames);
        }

        public static TransformDigitEmpty CreateTransformDigitEmpty(QueryConditions conditions, List<string> fieldNames)
        {
            return new TransformDigitEmpty(conditions, fieldNames);
        }
    }
}
