﻿using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.Base
{
    [Obsolete]
    public static class UserPermissionsHelper
    {
        private static readonly TimeSpan UserPermissionsCacheExpiration = TimeSpan.FromMinutes(60);

        public static List<AccessRight> GetUserPermissions(this WorkItem workItem)
        {
            return
                AppCache
                    .Current
                    .AddOrGetExisting(
                        () => workItem.LoadUserPermissions(),
                        ServerOperations.BP_UserPerms,
                        UserPermissionsCacheExpiration);
        }

        public static AccessContext GetUserPermissions(this WorkItem workItem, string[] operations)
        {
            return workItem.LoadUserPermissions(operations);
        }

        public static bool IsUserInRole(this List<AccessRight> allPermissions, params string[] roleNames)
        {
            return roleNames != null 
                && allPermissions
                    .Where(r => r.PermType == PermissionType.Operation)
                    .Select(o => o.Name)
                    .Intersect(roleNames, EqualityComparer<string>.Default)
                    .Any();
        }
        private static List<AccessRight> LoadUserPermissions(this WorkItem workItem)
        {
            List<AccessRight> result;

            if (workItem
                .GetWcfServiceProxy<ISecurityService>()
                .ExecuteNoHandling(proxy => proxy.GetUserPermissions(), out result))
            {
                return result;
            }

            return new List<AccessRight>();
        }

        private static AccessContext LoadUserPermissions(this WorkItem workItem, string[] operations)
        {
            AccessContext result;

            if (workItem
                .GetWcfServiceProxy<ISecurityService>()
                .ExecuteNoHandling(proxy => proxy.GetAccessContext(operations), out result))
            {
                return result;
            }

            return new AccessContext();
        }

        public static ContractorDataPolicy ContractorDataPolicy(this WorkItem workItem)
        {          
            return 
                AppCache
                    .Current
                    .AddOrGetExisting(
                        () => workItem.LoadUserRestrictionСonfiguration(),
                        ServerOperations.BP_UserRestrCfg, 
                        UserPermissionsCacheExpiration);
        }

        private static ContractorDataPolicy LoadUserRestrictionСonfiguration(this WorkItem workItem)
        {
            ContractorDataPolicy result = null;

            if (workItem
                    .GetWcfServiceProxy<ISecurityService>()
                    .ExecuteNoHandling(proxy => proxy.GetUserRestrictionConfiguration(), out result))
            {
                return result;
            }

            return new ContractorDataPolicy();
        }
    }
}
