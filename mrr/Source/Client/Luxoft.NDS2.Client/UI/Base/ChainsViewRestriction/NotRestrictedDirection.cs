﻿using System;

namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class NotRestrictedDirection : IDirection
    {
        public Direction RawType
        {
            get
            {
                return Direction.Invariant;
            }
        }

        public bool TryMoveDown(IDirection nextDirection)
        {
            return true;
        }

        public object Clone()
        {
            return new NotRestrictedDirection();
        }
    }
}
