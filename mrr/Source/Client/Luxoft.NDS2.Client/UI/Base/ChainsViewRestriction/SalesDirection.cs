﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class SalesDirection : LevelRestrictedDirection
    {
        public SalesDirection(int maxLevel)
            : base(maxLevel)
        {
        }

        private SalesDirection(int maxLevel, int currentLevel)
            : base(maxLevel, currentLevel)
        {
        }

        public override Direction RawType
        {
            get { return Direction.Sales; }
        }

        protected override bool IsRestrictedByType(IDirection nextDirection)
        {
            return false;
        }

        public override object Clone()
        {
            return new SalesDirection(this.MaxLevel, this.CurrentLevel);
        }
    }
}
