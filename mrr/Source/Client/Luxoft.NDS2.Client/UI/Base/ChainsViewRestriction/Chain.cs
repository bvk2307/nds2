﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class Chain
    {
        private readonly Link _startingLink;

        private Link _currentLink;

        public Chain(Link startingLink)
        {
            _startingLink = startingLink;
            _currentLink = startingLink;
        }

        /// <summary>
        /// Попытка осуществить переход по хранилищу деклараций в указанном направлении.
        /// 1. Определяется является ли переход "проваливанием" (т.е. производится переход к данным другого НП/декларации НП), если нет, то переход разрешается
        /// 2. Если
        /// </summary>
        /// <param name="nextLink">Параметры перехода</param>
        /// <returns>true-если переход выполнен, false-если переход не выполнен (запрещен)</returns>
        public bool TryMove(Link nextLink)
        {
            bool ret = false;

            if (nextLink.Inn == _currentLink.Inn)
            {
                ret = true;
            }
            else
            {
                ret = _startingLink.Direction.TryMoveDown(nextLink.Direction);
            }

            if (ret)
            {
                _currentLink = nextLink.Clone();
            }

            return ret;
        }

        public IDirection CurrentDirection
        {
            get
            {
                return _currentLink.Direction;
            }
        }

        public Chain Clone()
        {
            var newStartingLink = _startingLink != null ? _startingLink.Clone() : null;
            var newCurrentLink = _currentLink != null ? _currentLink.Clone() : null;

            var chain = new Chain(newStartingLink);
            chain._currentLink = newCurrentLink;

            return chain;
        }
    }
}
