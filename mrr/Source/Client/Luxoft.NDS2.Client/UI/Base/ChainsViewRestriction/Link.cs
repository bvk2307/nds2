﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class Link
    {
        public Link(string inn, IDirection direction)
        {
            Inn = inn;
            Direction = direction;
        }

        public string Inn
        {
            get;
            private set;
        }

        public IDirection Direction
        {
            get;
            private set;
        }

        public Link Clone()
        {
            IDirection newDirection = Direction != null ? (IDirection)Direction.Clone() : null;
            return new Link(Inn, newDirection);
        }
    }
}
