﻿using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class DirectionCreator
    {
        private readonly ContractorDataPolicy _config;

        public DirectionCreator(ContractorDataPolicy config)
        {
            _config = config;
        }

        public IDirection PurchaseDirection()
        {
            if (_config.NotRestricted)
            {
                return new NotRestrictedDirection();
            }

            if (!_config.AllowPurchase)
            {
                return new TypeRestrictedPurchaseDirection(
                    _config.MaxLevelPurchase,
                    _config.MaxLevelSales);
            }

            return new PurchaseDirection(_config.MaxLevelPurchase);
        }

        public IDirection SalesDirection()        
        {
            if (_config.NotRestricted)
            {
                return new NotRestrictedDirection();
            }

            return new SalesDirection(_config.MaxLevelSales);
        }

    }
}
