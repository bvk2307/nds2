﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public interface IDirection
    {
        Direction RawType
        {
            get;
        }

        bool TryMoveDown(IDirection nextDirection);

        object Clone();
    }
}
