﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class TypeRestrictedPurchaseDirection : PurchaseDirection
    {
        private readonly int _maxLevelOther;

        /// <summary>
        /// Создает экземпляр класса TypeRestrictedPurchaseDirection
        /// </summary>
        /// <param name="maxLevel">Макс. глубина проваливания</param>
        /// <param name="maxLevelOther">Макс. глубина проваливания в другом направлении</param>
        public TypeRestrictedPurchaseDirection(int maxLevel, int maxLevelSales)
            : base(maxLevel)
        {
            _maxLevelOther = maxLevelSales;
        }

        protected override bool IsRestrictedByType(IDirection nextDirection)
        {
            return RawType != nextDirection.RawType // Если движение в другом направлении
                && CurrentLevel >= _maxLevelOther; // Допускаются переходы к покупателям до определенного уровня
        }
    }
}
