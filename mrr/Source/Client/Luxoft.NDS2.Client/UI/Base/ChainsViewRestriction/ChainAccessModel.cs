﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using taxPayerDto = Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using System;

namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public enum Direction { Invariant, Purchase, Sales };

    public class ChainAccessModel
    {
        private readonly DirectionCreator _directionCreator;

        private Chain _chain;

        public ChainAccessModel(DirectionCreator directionCreator)
        {
            _directionCreator = directionCreator;
        }

        public void Start(DeclarationSummary declarationData)
        {
            if (_chain != null)
            {
                throw new InvalidOperationException("Просмотр цепочки уже начался");
            }

            _chain = 
                new Chain(
                    new Link(
                        declarationData.INN, 
                        NewDirection(declarationData.COMPENSATION_AMNT < 0)));
        }

        public bool TryMove(DeclarationSummary declarationData, Direction direction)
        {
            return TryMove(
                new Link(
                    declarationData.INN, 
                    Direction(direction)));
        }

        public bool TryMove(taxPayerDto.TaxPayer taxPayerData, Direction direction)
        {
            return TryMove(
                new Link(
                    taxPayerData.Inn,
                    Direction(direction)));
        }

        private IDirection Direction(Direction direction)
        {
            return direction == ChainsViewRestriction.Direction.Invariant
                ? _chain.CurrentDirection
                : NewDirection(direction == ChainsViewRestriction.Direction.Purchase);
        }

        private IDirection NewDirection(bool isPurchase)
        {
            return isPurchase ? _directionCreator.PurchaseDirection() : _directionCreator.SalesDirection();
        }

        public ChainAccessModel Clone()
        {
            var newChain = new ChainAccessModel(_directionCreator);

            if (_chain != null)
            {
                newChain._chain = _chain.Clone();
            }

            return newChain;
        }

        public bool IsStarted
        {
            get
            {
                return _chain != null;
            }
        }

        private bool TryMove(Link nextLink)
        {
            if (_chain == null)
            {
                throw new InvalidOperationException("Цепочка пуста");
            }

            return _chain.TryMove(nextLink);
        }
    }
}
