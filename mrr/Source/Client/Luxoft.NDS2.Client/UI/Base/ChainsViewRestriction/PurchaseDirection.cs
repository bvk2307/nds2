﻿namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public class PurchaseDirection : LevelRestrictedDirection
    {
        public PurchaseDirection(int maxLevel)
            : base(maxLevel)
        {
        }

        private PurchaseDirection(int maxLevel, int currentLevel)
            : base(maxLevel, currentLevel)
        {
        }

        public override Direction RawType
        {
            get { return Direction.Purchase; }
        }

        protected override bool IsRestrictedByType(IDirection nextDirection)
        {
            return false;
        }

        public override object Clone()
        {
            return new PurchaseDirection(this.MaxLevel, this.CurrentLevel);
        }
    }
}
