﻿using System;

namespace Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction
{
    public abstract class LevelRestrictedDirection : IDirection
    {
        private const int StartingLevel = 0;

        protected LevelRestrictedDirection(int maxLevel)
        {
            MaxLevel = maxLevel;
            CurrentLevel = StartingLevel;
        }

        protected LevelRestrictedDirection(int maxLevel, int currentLevel)
        {
            MaxLevel = maxLevel;
            CurrentLevel = currentLevel;
        }

        protected int CurrentLevel
        {
            get;
            private set;
        }

        public int MaxLevel
        {
            get;
            private set;
        }

        public abstract Direction RawType
        {
            get;           
        }

        public bool IsRestricted(IDirection nextDirection, int nextLevel)
        {
            return nextLevel > MaxLevel || IsRestrictedByType(nextDirection);
        }

        protected abstract bool IsRestrictedByType(IDirection nextDirection);

        public bool TryMoveDown(IDirection nextDirection)
        {
            if (MaxLevel == CurrentLevel
                || IsRestrictedByType(nextDirection))
            {
                return false;
            }

            CurrentLevel++;

            return true;
        }

        public virtual object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
