﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public class CardOpenResult
    {
        public CardOpenResult(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

        public bool IsSuccess
        {
            get;
            private set;
        }

        public string Message
        {
            get;
            private set;
        }
    }

    public class CardOpenSuccess : CardOpenResult
    {
        public CardOpenSuccess()
            : base(true, string.Empty)
        {
        }
    }

    public class CardOpenError : CardOpenResult
    {
        public CardOpenError(string errorText)
            : base(false, errorText)
        {
        }
    }
}
