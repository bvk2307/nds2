﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;

namespace Luxoft.NDS2.Client.UI.Base
{
    public interface IClaimCardOpener
    {
        void ViewClaimDetails(long id);

        void ViewClaimDetails(DiscrepancyDocumentInfo data);
    }
}
