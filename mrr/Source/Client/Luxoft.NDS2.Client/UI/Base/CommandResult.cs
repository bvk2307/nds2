﻿namespace Luxoft.NDS2.Client.UI.Base
{
    public delegate CommandResult ActionRequiredEvent();

    public delegate CommandResult ActionRequiredEvent<TParam>(TParam param);

    public enum CommandResult
    {
        Success,
        Failure,
        Refuse
    }
}
