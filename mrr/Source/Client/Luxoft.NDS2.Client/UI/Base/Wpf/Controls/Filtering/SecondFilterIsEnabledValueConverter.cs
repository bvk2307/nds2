﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    public class SecondFilterIsEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
                throw new NotSupportedException();
            bool operatorEnableValue = System.Convert.ToBoolean(values[0]);
            bool isValidSelectedValue = System.Convert.ToBoolean(values[1]);

            return operatorEnableValue && isValidSelectedValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}