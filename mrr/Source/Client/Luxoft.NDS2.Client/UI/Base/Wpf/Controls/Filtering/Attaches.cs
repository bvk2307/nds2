﻿using System.Windows;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    /// <summary>
    /// Содержит необходимые свойства для контрола фильтрации.
    /// </summary>
    public static class Attaches
    {
        public static string GetWaterMarkText(DependencyObject obj)
        {
            return (string)obj.GetValue(WaterMarkTextProperty);
        }

        public static void SetWaterMarkText(DependencyObject obj, string value)
        {
            obj.SetValue(WaterMarkTextProperty, value);
        }

        /// <summary>
        /// Свойство содержит текст водяного знака.
        /// </summary>
        public static readonly DependencyProperty WaterMarkTextProperty =
            DependencyProperty.RegisterAttached("WaterMarkText", typeof(string), typeof(Attaches), new PropertyMetadata(null));
    }
}
