﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;
using Telerik.Windows.Controls.Filtering.Editors;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
/// <summary>
/// Поведение производящее сброс в начальное состояние визуальных элементов фильтра при изменении свойства IsEnabled у 
/// контент контрола содержащего данные визуальные элементы.
/// Используется во втором фильтре контрола фильтрации, т.к. данный фильтр должен уметь 
/// дизейблиться тем самым показывая пользователю явное отсутсвие выражения. 
/// После дизейбла визуальные компоненты второго фильтра должны сброситься в начальное состояние.
/// Рутина сброса используемых в отчетах визуальных элементов фильтрации находится в данном поведении.
/// </summary>
    public class ClearContentOnDisabledBehavior : Behavior<FilterEditorContentControl>
    {
        protected override void OnAttached()
        {
            AssociatedObject.IsEnabledChanged += (s, e) =>
            {
                var isEnabled = (bool)e.NewValue;
                if (!isEnabled)
                {
                    //Очистка строкового фильтра
                    if (AssociatedObject.Content is StringFilterEditor)
                    {
                        var sf = AssociatedObject.Content as StringFilterEditor;
                        sf.Text = null;
                        sf.IsCaseSensitive = false;
                    }

                    //Очистка дефолтного фильтра
                    if (AssociatedObject.Content is TextBox)
                    {
                        var tb = AssociatedObject.Content as TextBox;
                        tb.Text = null;
                        System.Windows.Data.BindingOperations.GetBindingExpression(tb, TextBox.TextProperty).UpdateSource();
                    }
                }
            };
        }
    }
}
