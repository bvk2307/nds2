﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls.Filtering.Editors;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    /// <summary>
    /// Контрол используется в шаблоне контрола фильтрации. 
    /// Выполняет замену дефолтных стилей объектов в свойстве Content, на стили поддерживающие водяные знаки (Watermark)
    /// </summary>
    public class FilterEditorContentControl : ContentControl
    {
        static FilterEditorContentControl()
        {
            ContentProperty.OverrideMetadata(typeof(FilterEditorContentControl),
                             new FrameworkPropertyMetadata(OnContentPropertyChangedHandler));
        }

        private static void OnContentPropertyChangedHandler(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var fecc = d as FilterEditorContentControl;
                fecc.OnContentPropertyChanged(e);
            }
        }

        private void OnContentPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is TextBox)
            {
                var tb = e.NewValue as TextBox;
                Attaches.SetWaterMarkText(tb, "Введите значение");
                tb.Style = (Style)Application.Current.FindResource("TextBoxWaterMarkStyle");
            }

            if (e.NewValue is StringFilterEditor)
            {
                var sfe = e.NewValue as StringFilterEditor;
                Attaches.SetWaterMarkText(sfe, "Введите значение");
                sfe.Style = (Style)Application.Current.FindResource("StringFilterEditorWaterMarkedStyle");
            }
        }
    }
}
