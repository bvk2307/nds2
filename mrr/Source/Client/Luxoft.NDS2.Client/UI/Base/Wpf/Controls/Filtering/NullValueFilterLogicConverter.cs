﻿using System;
using System.Windows.Data;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    public class NullValueFilterLogicConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return FilterCompositionLogicalOperator.And;
            else
                return value;
        }
    }
}
