﻿using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows;
using Telerik.Windows.Data;
using System.Collections.ObjectModel;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls
{
    /// <summary>
    /// Класс реализует логику автоматического закрытия окна фильтра 
    /// после нажатия на кнопки Применить, убирает операции фильтра на равенство/неравенство null
    /// </summary>
    public class AutoClosedColumnDataFilteringControl : FilteringControl
    {
        private bool _reduced;
        public AutoClosedColumnDataFilteringControl(GridViewColumn column)
            : base(column)
        {
            column.IsFilteringDeferred = true;
            column.ShowDistinctFilters = false;
            this.DistinctFiltersVisibility = Visibility.Collapsed;
        }

        public override void Prepare(GridViewColumn columnToPrepare)
        {
            ReduceAvailableOperations(columnToPrepare);
            base.Prepare(columnToPrepare);
        }

        protected override void OnApplyFilter()
        {
            base.OnApplyFilter();
            CloseHostPopup();
        }

        protected void CloseHostPopup()
        {
            var popup = this.ParentOfType<Popup>();
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        private void ReduceAvailableOperations(GridViewColumn column)
        {
            if (_reduced)
                return;
            column.DataControl.FilterOperatorsLoading +=
                (s, e) =>
                {
                    RemoveOperationIfExists(e.AvailableOperators, FilterOperator.IsNull);
                    RemoveOperationIfExists(e.AvailableOperators, FilterOperator.IsNotNull);
                };
            _reduced = true;
        }

        private void RemoveOperationIfExists(Collection<FilterOperator> opCollection, FilterOperator opToRemove)
        {
            if (opCollection.Contains(opToRemove))
                opCollection.Remove(opToRemove);
        }
    }
}
