﻿using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls
{
    /// <summary>
    /// Класс столбца грида, содержащего численные данные фильтр для которых зависит от текущего масштаба.
    /// При создании объекта создает специальный контрол фильтрации имеющий зависимость от текущего значения  масштаба.
    /// </summary>
    public class GridViewScaledDataColumn : GridViewDataColumn
    {
        /// <summary>
        /// Текущий масштаб величины столбца
        /// </summary>
        public static readonly DependencyProperty ScaleFactorProperty =
                DependencyProperty.Register("ScaleFactor", typeof(int), typeof(GridViewScaledDataColumn), new PropertyMetadata(0));

        public GridViewScaledDataColumn():base()
        {
            var filteringControl = new ScaleFactorColumnDataFilteringControl(this);
            var binding = new Binding();
            binding.Path = new PropertyPath(GridViewScaledDataColumn.ScaleFactorProperty);
            binding.Source = this;
            BindingOperations.SetBinding(filteringControl, ScaleFactorColumnDataFilteringControl.ScaleFactorProperty, binding);

            this.FilteringControl = filteringControl;
        }

        /// <summary>
        /// Текущий масштаб величины столбца
        /// </summary>
        public int ScaleFactor
        {
            get { return (int)GetValue(ScaleFactorProperty); }
            set { SetValue(ScaleFactorProperty, value); }
        }
    }
}
