﻿using System;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using System.Windows;
using Telerik.Windows.Data;
using Luxoft.NDS2.Client.UI.Base.Wpf.Interfaces;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls
{
    /// <summary>
    /// Контрол фильтрации предназначенный для учета текущего масштаба фильтруемой величины. 
    /// </summary>
    public class ScaleFactorColumnDataFilteringControl : AutoClosedColumnDataFilteringControl, IFilteringClearableControl
    {
        /// <summary>
        /// Текущий масштаб фильтруемой величины
        /// </summary>
        public static readonly DependencyProperty ScaleFactorProperty =
            DependencyProperty.Register("ScaleFactor", typeof(int), typeof(AutoClosedColumnDataFilteringControl), new PropertyMetadata(1));

        GridViewColumn _column;
        RadGridView _grid;
        RoundedCompositeFilterDescriptor _appliedFilter;
        bool _prepared;
        FilteringViewModel _vm;

        public ScaleFactorColumnDataFilteringControl(GridViewColumn column)
            : base(column)
        {
            //Устанавливаем ссылку на стиль. Стиль уже должен быть загружен.
            this.SetResourceReference(StyleProperty, "AutoClosedColumnDataFilteringControlStyle");
        }

        public int ScaleFactor
        {
            get { return (int)GetValue(ScaleFactorProperty); }
            set { SetValue(ScaleFactorProperty, value); }
        }

        public override void Prepare(GridViewColumn columnToPrepare)
        {
            if (!_prepared)
            {
                base.Prepare(columnToPrepare);
                _grid = columnToPrepare.Parent as RadGridView;
                _column = columnToPrepare;
                _vm = this.DataContext as FilteringViewModel;
                _prepared = true;
                return;
            }
            if (_appliedFilter != null)
                RestoreFiltersViewModels();
        }

        public void ClearFilters()
        {
            OnClearFilter();
        }

        protected override void OnApplyFilter()
        {
            if (!IsConvertibleToDecimal(_vm.Filter1.Value))
                return;

            _column.DataControl.FilterDescriptors.SuspendNotifications();
            if (_appliedFilter != null)
            {
                if (_column.DataControl.FilterDescriptors.Contains(_appliedFilter))
                    _column.DataControl.FilterDescriptors.Remove(_appliedFilter);
            }

            int roundFactor = CalculateRoundFactor(ScaleFactor);

            var newFilter = new RoundedCompositeFilterDescriptor(_column);
            newFilter.LogicalOperator = _vm.FieldFilterLogicalOperator;

            newFilter.Filter1 = new RoundedDecimalFilterDescriptor(roundFactor, ScaleFactor)
            {
                Member = _column.UniqueName,
                MemberType = typeof(decimal),
                Operator = _vm.Filter1.Operator,
                Value = Convert.ToDecimal(_vm.Filter1.Value) * ScaleFactor,
            };

            if (IsConvertibleToDecimal(_vm.Filter2.Value))
            {
                newFilter.Filter2 = new RoundedDecimalFilterDescriptor(roundFactor, ScaleFactor)
                {
                    Member = _column.UniqueName,
                    MemberType = typeof(decimal),
                    Operator = _vm.Filter2.Operator,
                    Value = Convert.ToDecimal(_vm.Filter2.Value) * ScaleFactor,
                };
            }

            _column.DataControl.FilterDescriptors.Add(newFilter);
            _column.DataControl.FilterDescriptors.ResumeNotifications();
            _grid.RaiseEvent(new GridViewFilteredEventArgs(new[] { newFilter }, new[] { _appliedFilter }));
            _appliedFilter = newFilter;
            IsActive = true;
            CloseHostPopup();
        }

        protected override void OnClearFilter()
        {
            if (!_prepared)
                return;
            if (_column.DataControl.FilterDescriptors.Contains(_appliedFilter))
            {
                _column.DataControl.FilterDescriptors.Remove(_appliedFilter);
                _grid.RaiseEvent(new GridViewFilteredEventArgs(null, new[] { _appliedFilter }));
                _appliedFilter = null;
            }
            _vm.Filter1.Operator = FilterOperator.IsEqualTo;
            _vm.Filter1.Value = String.Empty;
            _vm.FieldFilterLogicalOperator = FilterCompositionLogicalOperator.And;
            _vm.Filter2.Operator = FilterOperator.IsEqualTo;
            _vm.Filter2.Value = String.Empty;
            IsActive = false;
        }

        private bool IsConvertibleToDecimal(object target)
        {
            try
            {
                Convert.ToDecimal(target);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void RestoreFiltersViewModels()
        {
            RestoreFilterViewModel(_appliedFilter.Filter1, _vm.Filter1);
            RestoreFilterViewModel(_appliedFilter.Filter2, _vm.Filter2);
        }

        private void RestoreFilterViewModel(FilterDescriptor AppliedFilter, FilterDescriptorViewModel FilterViewModel)
        {
            if (AppliedFilter != null && FilterViewModel != null)
            {
                int scaleFactor = this.ScaleFactor;
                if (_appliedFilter.Filter1.RoundFactor != CalculateRoundFactor(scaleFactor))
                {
                    FilterViewModel.Value = String.Empty;
                    return;
                }

                decimal? appliedFilterValue = IsConvertibleToDecimal(AppliedFilter.Value) ? Convert.ToDecimal(AppliedFilter.Value) : (decimal?)null;
                decimal? viewModelValue = IsConvertibleToDecimal(FilterViewModel.Value) ? Convert.ToDecimal(FilterViewModel.Value) : (decimal?)null;
                if (appliedFilterValue != null)
                {
                    if (appliedFilterValue != viewModelValue * scaleFactor)
                        FilterViewModel.Value = appliedFilterValue / scaleFactor;
                }
            }
            else if (FilterViewModel != null)
                FilterViewModel.Value = String.Empty;
        }

        private int CalculateRoundFactor(int scaleFactor)
        {
            return -1 * (int)Math.Log10(ScaleFactor);
        }
    }
}
