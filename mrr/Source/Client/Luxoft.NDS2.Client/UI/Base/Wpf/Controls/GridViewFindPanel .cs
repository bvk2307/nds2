﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Commands;
using Luxoft.NDS2.Client.UI.Base.Wpf.Extensions;
using Luxoft.NDS2.Client.UI.Base.Wpf.Interfaces;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using GridViewColumn = Telerik.Windows.Controls.GridViewColumn;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls
{
    [TemplatePart(Name = "PART_PanelFindTextBox", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_PanelFindButton", Type = typeof(Button))]
    public class GridViewFindPanel : Control
    {
        /// <summary>
        /// Identifies the <see cref="GridView"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty GridViewProperty =
            DependencyProperty.Register(
                "GridView",
                typeof(GridViewDataControl),
                typeof(GridViewFindPanel),
                new PropertyMetadata(null, null));

        private TextBox _panelFindTextBox;
        private readonly FindViewModel _findViewModel;
        private readonly FindViewSearchStateModel _searchState;

        static GridViewFindPanel()
        {
            RegisterCommands();
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(GridViewFindPanel), new FrameworkPropertyMetadata(typeof(GridViewFindPanel)));
        }

        public GridViewFindPanel()
        {
            base.DataContext = this._findViewModel = new FindViewModel();
            base.IsTabStop = false;
            this.ReplaceActions = new List<BaseFindFieldReplaceAction>();
            this._searchState = new FindViewSearchStateModel();
        }

        public IList<BaseFindFieldReplaceAction> ReplaceActions { get; private set; }

        public IList<string> SearchColumns { get; set; }

        public GridViewDataControl GridView
        {
            get { return (GridViewDataControl)GetValue(GridViewProperty); }
            set { SetValue(GridViewProperty, value); }
        }

        private TextBox FindPanelFindTextBox()
        {
            return (this.Nds2GetChildByName("PART_PanelFindTextBox") as TextBox);
        }

        internal void FocusSearchTextBox()
        {
            if (this._panelFindTextBox != null)
            {
                Action method = () => FocusTextBox(this._panelFindTextBox);
                base.Dispatcher.BeginInvoke(method, DispatcherPriority.Input, new object[0]);
            }
            else
            {
                Action action2 = delegate
                {
                    this._panelFindTextBox = this.FindPanelFindTextBox();
                    FocusTextBox(this._panelFindTextBox);
                };
                base.Dispatcher.BeginInvoke(action2, DispatcherPriority.Render, new object[0]);
            }
        }

        private static void FocusTextBox(TextBox textBox)
        {
            if (textBox != null)
            {
                textBox.Focus();
                Keyboard.Focus(textBox);
            }
        }

        internal string GetFindText()
        {
            if (this._findViewModel == null)
            {
                return string.Empty;
            }
            return this._findViewModel.FindText;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._panelFindTextBox = FindPanelFindTextBox();
            this.SubscribeToEvents();
        }


        private static void CanFindNextValueExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var panel = sender as GridViewFindPanel;
            if (panel != null)
            {
                var dataContext = panel.DataContext as FindViewModel;
                e.CanExecute = (dataContext != null) ? !string.IsNullOrEmpty(dataContext.FindText) : false;
            }
        }

        private static void OnFindNextValue(object sender, ExecutedRoutedEventArgs e)
        {
            var panel = sender as GridViewFindPanel;
            if (panel != null)
            {
                panel.LocateNext();
            }
        }

        private static void CanRestoreState(object sender, CanExecuteRoutedEventArgs e)
        {
            var panel = sender as GridViewFindPanel;
            if (panel != null)
            {
                var dataContext = panel.DataContext as FindViewModel;
                e.CanExecute = (dataContext != null) && !string.IsNullOrEmpty(dataContext.FindText);

                if (!e.CanExecute)
                {
                    var grid = panel.GridView;
                    if (grid != null)
                    {
                        e.CanExecute = grid.FilterDescriptors.Any();
                        if (!e.CanExecute && grid.DataContext is IClerableModel)
                        {
                            e.CanExecute = ((IClerableModel)grid.DataContext).CanRestoreState(grid);
                        }
                    }
                }
            }
        }


        private static void OnRestoreState(object sender, ExecutedRoutedEventArgs e)
        {
            var panel = sender as GridViewFindPanel;
            if (panel != null)
            {
                panel._searchState.InitSearch(null);

                var dataContext = panel.DataContext as FindViewModel;
                if (dataContext != null)
                {
                    dataContext.ClearFindText();
                }
                var grid = panel.GridView;
                if (grid != null)
                {
                    foreach (var column in grid.Columns)
                    {
                        if (column.FilteringControl != null)
                        {
                            if (column.FilteringControl is IFilteringClearableControl)
                            {
                                ((IFilteringClearableControl)column.FilteringControl).ClearFilters();
                            }
                            column.FilteringControl.IsActive = false;
                        }
                        column.ClearFilters();
                    }

                    grid.SortDescriptors.Clear();
                    if (grid.DataContext is IClerableModel)
                    {
                        ((IClerableModel)grid.DataContext).RestoreState(grid);
                    }
                }
            }
        }

        private void LocateNext()
        {
            var dataContext = this.DataContext as FindViewModel;
            if (dataContext != null)
            {
                LocateNextValue(dataContext.FindText);
            }
        }

        private void LocateNextValue(string searchValue)
        {
            if (String.IsNullOrWhiteSpace(searchValue))
                return;
            if (!String.Equals(searchValue, _searchState.SearchingValue, StringComparison.CurrentCultureIgnoreCase))
                _searchState.InitSearch(searchValue);

            object itemFound = null;
            GridViewColumn columnFound = null;
            if (TryFindItemOnCurrentPage(out itemFound, out columnFound))
            {
                FocusViewOnItem(itemFound, columnFound);
                _searchState.SetLastFound(itemFound, columnFound.DisplayIndex);
                return;
            }
            else
            {
                if (TrySetNextPage())
                {
                    _searchState.ClearLastFound();
                    LocateNextValue(searchValue);
                    return;
                }

                if (_searchState.NoAnyValueFound)
                    ShowNoFoundDialog();
                else
                    ShowEndOfSearchDialog();
            }
        }

        private bool TryFindItemOnCurrentPage(out object itemFound, out GridViewColumn columnFound)
        {
            int startSearchIndex = 0;
            if (_searchState.LastFoundItem != null)
            {
                startSearchIndex = GridView.Items.IndexOf(_searchState.LastFoundItem);
                if (startSearchIndex < 0)
                {
                    //Не получается найти результат прошлого поиска на текущей странице
                    //Бывает когда юзер ушел на новую страницу или сменил масштабирование
                    //Тогда начинаем поиск заново
                    _searchState.ReinitSearch();
                    startSearchIndex = 0;
                }
            }
            for (int i = startSearchIndex; i < GridView.Items.Count; i++)
            {
                var foundColumn = FindColumnString(GridView.Items[i]);
                if (foundColumn != null)
                {
                    itemFound = GridView.Items[i];
                    columnFound = foundColumn;
                    return true;
                }
            }
            itemFound = null;
            columnFound = null;
            return false;
        }

        private void FocusViewOnItem(object itemFound, GridViewColumn columnFound)
        {
            var grid = this.GridView;
            var scroller = grid.ChildrenOfType<GridViewScrollViewer>().First();
            var header = grid.ChildrenOfType<GridViewHeaderRow>().First();
            var frow = grid.ChildrenOfType<GridViewRow>().FirstOrDefault(s => s.ActualHeight > 0);
            double offset = (grid.Items.IndexOf(itemFound) + 1) * (frow == null ? 24 : frow.ActualHeight) + header.ActualHeight + this.ActualHeight - grid.ActualHeight;
            scroller.ScrollToVerticalOffset(offset);
            scroller.UpdateLayout();

            grid.ScrollIntoViewAsync(itemFound, columnFound,null);
            grid.CurrentCellInfo = new GridViewCellInfo(itemFound, columnFound);
        }

        private bool TrySetNextPage(bool supressEndOfSearchDetection = false)
        {
            var grid = this.GridView;
            if (grid.DataContext is IPagedModel)
            {
                var pagedModel = (IPagedModel)grid.DataContext;

                _searchState.UpdateStartIndex(pagedModel.PageIndex);

                var nextPageIndexes = Enumerable.Range(0, pagedModel.PageCount).
                                Except(Enumerable.Range(0, pagedModel.PageIndex + 1)).
                                Union(Enumerable.Range(0, pagedModel.PageIndex + 1));

                foreach (var pageIndex in nextPageIndexes)
                {
                    if (!supressEndOfSearchDetection)
                        if (pageIndex == _searchState.SearchStartPageIndex)
                            return false;

                    var data = pagedModel.PageData(pageIndex);
                    if (ExistsSearchingValue(data))
                    {
                        pagedModel.GoPage(pageIndex);
                        return true;
                    }
                }
            }
            return false;
        }

        private void ShowNoFoundDialog()
        {
            DialogHelper.WarningWithTitle("Результаты поиска", "Совпадений не найдено");
        }

        private void ShowEndOfSearchDialog()
        {
            if (DialogHelper.Confirm("Поиск завершен. Продолжить сначала?"))
            {
                TrySetNextPage(true);
                _searchState.ReinitSearch();
                LocateNextValue(_searchState.SearchingValue);
            }
            else
            {
                _searchState.InitSearch(null);
            }
        }

        private GridViewColumn FindColumnString(object row)
        {
            var text = _searchState.SearchingValue.ToLower();

            var searchColumnsFull = GetColumnsToSearch();
            var columns = row != _searchState.LastFoundItem ? searchColumnsFull :
                    searchColumnsFull.Where(x => x.DisplayIndex > _searchState.LastFoundColumnIndex);

            foreach (var column in columns)
            {
                string columnName = column.UniqueName;
                var replacer = this.ReplaceActions.FirstOrDefault(s => s.FieldName == columnName);
                string val = null;
                if (replacer != null)
                {
                    var pi = replacer.GetType().GetProperties().Where(s => s.Name.StartsWith("ReplaceFunc")).FirstOrDefault();
                    if (pi != null)
                    {
                        var pival = pi.GetValue(replacer, null);
                        val = (string)((Delegate)pival).DynamicInvoke(new[] { row });
                    }
                }
                else
                {
                    var pi = row.GetType().GetProperty(columnName);
                    var v = pi.GetValue(row, null);
                    if (v != null)
                    {
                        val = v.ToString();
                    }
                }
                if (val != null)
                {
                    var ex = val.ToLower().Contains(text);
                    if (ex) return column;
                }
            }
            return null;
        }

        private bool ExistsSearchingValue(ICollection collection)
        {
            return (from object item in collection select this.FindColumnString(item)).Any(column => column != null);
        }

        /// <summary>
        /// Содержит алгоритм определения столбцов по которым производится поиск в гриде
        /// </summary>
        /// <returns></returns>
        private IEnumerable<GridViewColumn> GetColumnsToSearch()
        {
            if (this.SearchColumns == null || this.SearchColumns.Count == 0)
                return this.GridView.Columns;
            else
                return (from gvc in this.GridView.Columns as IEnumerable<GridViewColumn>
                        join sc in this.SearchColumns on gvc.UniqueName equals sc
                        select gvc)
                        .ToArray();
        }

        protected override void OnInitialized(System.EventArgs e)
        {
            base.OnInitialized(e);
            StyleManager.SetDefaultStyleKey(this, typeof(GridViewFindPanel));
        }

        private static void RegisterCommand(ICommand command, ExecutedRoutedEventHandler executed,
            CanExecuteRoutedEventHandler canExecute)
        {
            CommandManager.RegisterClassCommandBinding(typeof(GridViewFindPanel),
                new CommandBinding(command, executed, canExecute));
        }

        private static void RegisterCommands()
        {
            RegisterCommand(GridViewFindCommands.FindNext, OnFindNextValue, CanFindNextValueExecute);
            RegisterCommand(GridViewFindCommands.RestoreStateCommand, OnRestoreState, CanRestoreState);
        }

        private void SubscribeToEvents()
        {
            if (this._panelFindTextBox == null)
                return;
            this._panelFindTextBox.KeyDown -= _panelFindTextBox_KeyDown;
            this._panelFindTextBox.KeyDown += _panelFindTextBox_KeyDown;
        }

        void _panelFindTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                LocateNext();
                this._panelFindTextBox.Focus();
            }
        }
    }
}
