﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Filtering.Editors;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    /// <summary>
    /// Поведение устанавливающее в комбобоксе индекс выбранного айтема на первый элемент коллекции айтемов 
    /// при изменении свойства IsEnabled в false.
    /// </summary>
    public class InitSelectedIndexOnDisabledBehavior : Behavior<RadComboBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.IsEnabledChanged += (s, e) =>
            {
                var isEnabled = (bool)e.NewValue;
                if (!isEnabled)
                {
                    AssociatedObject.SelectedIndex = 0;
                }
            };
        }
    }
}
