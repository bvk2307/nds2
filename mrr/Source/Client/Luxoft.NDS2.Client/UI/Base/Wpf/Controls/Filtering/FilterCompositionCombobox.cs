﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Controls.Filtering
{
    /// <summary>
    /// Комбобокс используется в шаблоне атвозакрывающегося контрола фильтрации. 
    /// </summary>
    public class FilterCompositionCombobox : RadComboBox
    {
        static FilterCompositionCombobox()
        {
            SelectedIndexProperty.OverrideMetadata(typeof(FilterCompositionCombobox),
                    new FrameworkPropertyMetadata(OnSelectedIndexChangedHandler));
        }
        public static readonly DependencyProperty IsValidValueSelectedProperty =
            DependencyProperty.Register("IsValidValueSelected", typeof(bool), typeof(FilterCompositionCombobox), new PropertyMetadata(false));

        public FilterCompositionCombobox()
        {
            ClearFilterCompositionCommand = new DelegateCommand((o) =>
                                                        this.SelectedIndex = -1);
        }

        public DelegateCommand ClearFilterCompositionCommand { get; private set; }

        public bool IsValidValueSelected
        {
            get { return (bool)GetValue(IsValidValueSelectedProperty); }
            set { SetValue(IsValidValueSelectedProperty, value); }
        }

        private static void OnSelectedIndexChangedHandler(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var filterCombo = d as FilterCompositionCombobox;
            if (filterCombo != null)
                filterCombo.OnSelectedIndexChanged((int)e.NewValue);
        }

        private void OnSelectedIndexChanged(int index)
        {
            if (index < 0)
                this.IsValidValueSelected = false;
            else
                this.IsValidValueSelected = true;
        }
    }
}
