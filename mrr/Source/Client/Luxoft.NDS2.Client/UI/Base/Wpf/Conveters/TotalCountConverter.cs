﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Telerik.Windows.Controls.Data.DataPager;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Conveters
{
    public class TotalCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Format("(из {0})", value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
