﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Telerik.Windows.Controls.Data.DataPager;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Conveters
{
    public class CountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = new List<int>();
            result.AddRange(new []{50, 100, 150, 200});
            //if (value != DependencyProperty.UnsetValue)
            //{
            //    var itemCount = (int)value;
            //    for (int i = 1; i <= itemCount; i++)
            //    {
            //        if (i % 50 == 0)
            //        {
            //            result.Add(i);
            //        }
            //    }
            //    var lastCount = result.LastOrDefault();
            //    if (itemCount != 0 && lastCount < itemCount)
            //    {
            //        result.Add(itemCount);
            //    }    
            //}
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
