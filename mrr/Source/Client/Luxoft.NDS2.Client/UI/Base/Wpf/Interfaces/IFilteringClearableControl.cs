﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Interfaces
{
    public interface IFilteringClearableControl : IFilteringControl
    {
        void ClearFilters();
    }
}
