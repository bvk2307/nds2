﻿using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.EventArgs
{
    public class CustomFilterEventArgs : System.EventArgs
    {
        public GridViewDataControl SourceGrid { get; private set; }

        public CustomFilterEventArgs(GridViewDataControl grid)
        {
            SourceGrid = grid;
        }
    }
}
