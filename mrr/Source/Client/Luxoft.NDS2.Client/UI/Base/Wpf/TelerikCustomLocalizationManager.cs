﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf
{
    public class TelerikCustomLocalizationManager : LocalizationManager
    {
        public override string GetStringOverride(string key)
        {
            switch (key)
            {
                case "GridViewGroupPanelText":
                    return "Zum gruppieren ziehen Sie den Spaltenkopf in diesen Bereich.";
                    //---------------------- RadGridView Filter Dropdown items texts:
                case "GridViewClearFilter":
                    return "Очистить";
                case "GridViewFilterShowRowsWithValueThat":
                    return "Отфильтровать по правилу:";
                case "GridViewFilterSelectAll":
                    return "Выделить все";
                case "GridViewFilterContains":
                    return "Содержит";
                case "GridViewFilterEndsWith":
                    return "Оканчивается на";
                case "GridViewFilterIsContainedIn":
                    return "Содержится в";
                case "GridViewFilterIsNotContainedIn":
                    return "Не содержится в";
                case "GridViewFilterDoesNotContain":
                    return "Не содержит";
                case "GridViewFilterIsNull":
                    return "Не указано значение";
                case "GridViewFilterIsNotNull":
                    return "Указано значение";

                case "GridViewFilterDistinctValueNull":
                    return "[null]";
                case "GridViewFilterDistinctValueStringEmpty":
                    return "[empty]";
                case "GridViewFilterIsEmpty":
                    return "Пустое";
                case "GridViewFilterIsNotEmpty":
                    return "Не пустое";

                case "GridViewFilterIsEqualTo":
                    return "Равно";
                case "GridViewFilterIsGreaterThan":
                    return "Больше ";
                case "GridViewFilterIsGreaterThanOrEqualTo":
                    return "Больше или равно";
                case "GridViewFilterIsLessThan":
                    return "Меньше";
                case "GridViewFilterIsLessThanOrEqualTo":
                    return "Меньше или равно";
                case "GridViewFilterIsNotEqualTo":
                    return "Не равно";
                case "GridViewFilterStartsWith":
                    return "Начинается с";
                case "GridViewFilterAnd":
                    return "И";
                case "GridViewFilterOr":
                    return "Или";
                case "GridViewFilter":
                    return "Применить";
                case "GridViewFilterMatchCase":
                    return "С учетом регистра";
                case "GridViewSearchPanelTopText":
                    return "Поиск:";
                case "RadDataPagerOf":
                    return "из";
                case "RadDataPagerPage":
                    return "Страница";
                case "RadDataOnPage":
                    return "на стр.";
                case "RadDataTotal":
                    return "Всего";
                case "GidViewFilterIsTrue":
                    return "Да";
                case "GridViewFilterIsFalse":
                    return "Нет";

                case "Close":
                    return "Закрыть";
                case "EnterDate":
                    return "Введите дату";
                case "Error":
                    return "Ошибка";
                case "Today":
                    return "Сегодня";
            }
            return base.GetStringOverride(key);
        }
    }
}
