﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Extensions
{
    public static class Nds2ChildrenOfTypeExtensions
    {
        internal static FrameworkElement Nds2GetChildByName(this FrameworkElement element, string name)
        {
            return (((FrameworkElement)element.FindName(name)) ?? element.ChildrenOfType<FrameworkElement>().FirstOrDefault<FrameworkElement>(c => (c.Name == name)));
        }
 

    }
}
