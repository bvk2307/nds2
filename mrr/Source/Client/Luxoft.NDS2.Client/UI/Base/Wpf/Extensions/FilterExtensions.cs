﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Extensions
{
    public static class FilterExtensions
    {
        public static ColumnFilter.FilterComparisionOperator ToFilterComparisionOperator(this Telerik.Windows.Data.FilterOperator filterOperator)
        {
            switch (filterOperator)
            {
                case FilterOperator.Contains:
                    return ColumnFilter.FilterComparisionOperator.Contains;
                case FilterOperator.DoesNotContain:
                    return ColumnFilter.FilterComparisionOperator.DoesNotContain;
                case FilterOperator.EndsWith:
                    return ColumnFilter.FilterComparisionOperator.EndsWith;
                case FilterOperator.IsContainedIn:
                    return ColumnFilter.FilterComparisionOperator.NotDefinedOperator;
                case FilterOperator.IsEmpty:
                    return ColumnFilter.FilterComparisionOperator.Equals;
                case FilterOperator.IsEqualTo:
                    return ColumnFilter.FilterComparisionOperator.Equals;
                case FilterOperator.IsGreaterThan:
                    return ColumnFilter.FilterComparisionOperator.GreaterThan;
                case FilterOperator.IsGreaterThanOrEqualTo:
                    return ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo;
                case FilterOperator.IsLessThan:
                    return ColumnFilter.FilterComparisionOperator.LessThan;
                case FilterOperator.IsLessThanOrEqualTo:
                    return ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo;
                case FilterOperator.IsNotContainedIn:
                    return ColumnFilter.FilterComparisionOperator.DoesNotMatch;
                case FilterOperator.IsNotEmpty:
                    return ColumnFilter.FilterComparisionOperator.NotEquals;
                case FilterOperator.IsNotEqualTo:
                    return ColumnFilter.FilterComparisionOperator.NotEquals;
                case FilterOperator.IsNotNull:
                    return ColumnFilter.FilterComparisionOperator.NotEquals;
                case FilterOperator.IsNull:
                    return ColumnFilter.FilterComparisionOperator.Equals;
                case FilterOperator.StartsWith:
                    return ColumnFilter.FilterComparisionOperator.StartsWith;
            }
            return ColumnFilter.FilterComparisionOperator.NotDefinedOperator;
        }

        public static ColumnFilter ToFilterComparision(this FilterDescriptorModel filterDescriptorModel)
        {
            var filter = new ColumnFilter();
            switch (filterDescriptorModel.Operator)
            {
                case FilterOperator.Contains:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Contains;
                    break;
                case FilterOperator.DoesNotContain:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.DoesNotContain;
                    break;
                case FilterOperator.EndsWith:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.EndsWith;
                    break;
                case FilterOperator.IsContainedIn:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotDefinedOperator;
                    break;
                case FilterOperator.IsEmpty:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.IsEqualTo:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.IsGreaterThan:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThan;
                    break;
                case FilterOperator.IsGreaterThanOrEqualTo:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo;
                    break;
                case FilterOperator.IsLessThan:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThan;
                    break;
                case FilterOperator.IsLessThanOrEqualTo:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo;
                    break;
                case FilterOperator.IsNotContainedIn:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.DoesNotMatch;
                    break;
                case FilterOperator.IsNotEmpty:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNotEqualTo:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNotNull:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNull:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.StartsWith:
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.StartsWith;
                    break;
            }

            if (filterDescriptorModel.Operator == FilterOperator.IsEmpty ||
                filterDescriptorModel.Operator == FilterOperator.IsNull ||
                filterDescriptorModel.Operator == FilterOperator.IsNotEmpty ||
                filterDescriptorModel.Operator == FilterOperator.IsNotNull)
            {
                filter.Value = null;
            }
            else
            {
                filter.Value = filterDescriptorModel.Value;
                filter.HasRoundFactor = filterDescriptorModel.HasRoundFactor;
                filter.RoundFactor = filterDescriptorModel.RoundFactor;
            }

            if (filter.Value != null &&
                filterDescriptorModel.Value.GetType() == typeof(string) &&
                !filterDescriptorModel.IsCaseSensitive)
            {
                filter.IsCaseSensitive = false;
                filter.Value = (filter.Value as string).ToUpper();
            }
            return filter;
        }
    }
}
