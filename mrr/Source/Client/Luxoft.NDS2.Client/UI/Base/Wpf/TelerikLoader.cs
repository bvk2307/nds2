﻿using System;
using System.Windows;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf
{
	//apopov 10.1.2017	//TODO!!!   //consider to consolidate and use the one sets of Telerik styles and one loader
    /// <summary> Suggestion: consolidate with <see cref="TelerikLoader"/> and use instead of. </summary>
    public static class TelerikLoader
    {
        private static bool s_isInitializedAlready;

        

        public static void Init()
        {	
            //apopov 3.11.2016	//'Application.Current != null' is allways now
            if (Application.Current == null)
                throw new NullReferenceException("System.Windows.Application.Current is null");

            StyleManager.IsEnabled = false;

            if (!s_isInitializedAlready)
            {
                s_isInitializedAlready = true;

	            //apopov 3.11.2016	//  //moved into GoWpfLicensedApplication's initialization
                //// create the Application object
                //var app = new System.Windows.Application();
                //// for prevent first wpf window dispose application
                //app.ShutdownMode = ShutdownMode.OnExplicitShutdown;


                //var path = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Application.ExecutablePath), ClientConstants.UserSettingsKeys.PATH);
                //var a = Assembly.LoadFrom(System.IO.Path.Combine(path, "Telerik.Windows.Themes.Office2013.dll"));

                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/System.Windows.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Chart.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.Relative)) as ResourceDictionary);
                //System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                //    System.Windows.Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.RibbonView.xaml", UriKind.Relative)) as ResourceDictionary);

                //Office2013Palette.Palette.FontSizeXXS = 8;
                //Office2013Palette.Palette.FontSizeXS = 10;
                //Office2013Palette.Palette.FontSizeS = 12;
                //Office2013Palette.Palette.FontSize = 13;
                //Office2013Palette.Palette.FontSizeL = 14;
                //Office2013Palette.Palette.FontSizeXL = 15;

                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri("/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/System.Windows.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.Data.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.Navigation.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.Input.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.Chart.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.GridView.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.GridViewAdd.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri(
                            "/Luxoft.NDS2.Client;component/UI/Base/Wpf/TelerikStyles/Telerik.Windows.Controls.RibbonView.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                LocalizationManager.Manager = new TelerikCustomLocalizationManager();
            }
        }
    }
}
