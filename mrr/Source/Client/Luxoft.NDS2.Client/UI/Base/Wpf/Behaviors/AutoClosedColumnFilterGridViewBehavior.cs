﻿using Luxoft.NDS2.Client.UI.Base.Wpf.Controls;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Behaviors
{
    /// <summary>
    /// Заменяет в каждом столбце грида стандартный телериковский контрол фильтрации,
    /// на контрол поддерживающий автозакрытие по нажатии кнопки "Применить фильтр".
    /// </summary>
    public class AutoClosedColumnFilterGridViewBehavior : Behavior<RadGridView>
    {
        protected override void OnAttached()
        {
            foreach (var col in AssociatedObject.Columns)
            {
                if (col.IsFilterable == true && col.FilteringControl == null)
                {
                    col.FilteringControl = new AutoClosedColumnDataFilteringControl(col);
                    col.ShowDistinctFilters = false;
                    col.IsFilteringDeferred = true;
                }
            }
        }
    }
}

