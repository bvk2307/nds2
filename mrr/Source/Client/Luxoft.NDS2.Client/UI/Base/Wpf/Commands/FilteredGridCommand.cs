﻿using System;
using System.Windows.Input;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Commands
{
    public class FilteredGridCommand : ICommand
    {
        private Action<GridViewFilteredEventArgs> _filteredAction;

        public FilteredGridCommand(Action<GridViewFilteredEventArgs> filteredAction)
        {
            _filteredAction = filteredAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _filteredAction((GridViewFilteredEventArgs)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
