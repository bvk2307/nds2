﻿using System;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Commands
{
    public class SortingGridCommand : ICommand
    {
        private Action<GridViewSortingEventArgs> _sortingAction;
        public SortingGridCommand(Action<GridViewSortingEventArgs> sortingAction)
        {
            _sortingAction = sortingAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _sortingAction((GridViewSortingEventArgs)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
