﻿using System;
using System.Windows.Input;
using Luxoft.NDS2.Client.UI.Base.Wpf.EventArgs;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Commands
{
    public class CustomFilteringGridCommand : ICommand
    {
        private Action<CustomFilterEventArgs> _filteringAction;

        public CustomFilteringGridCommand(Action<CustomFilterEventArgs> filteringAction)
        {
            _filteringAction = filteringAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _filteringAction((CustomFilterEventArgs)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
