﻿using System;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Commands
{
    /// <summary>
    /// Команда изменения индекса страницы грида
    /// </summary>
    public class PageIndexChangedCommand : ICommand
    {
        private Action<PageIndexChangedEventArgs> _pageIndexChangedAction;
        public PageIndexChangedCommand(Action<PageIndexChangedEventArgs> pageIndexChangedAction)
        {
            _pageIndexChangedAction = pageIndexChangedAction;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _pageIndexChangedAction((PageIndexChangedEventArgs)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
