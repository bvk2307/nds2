﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Commands
{
    public class GridViewFindCommands
    {

        static GridViewFindCommands()
        {
            FindNext = new RoutedUICommand("Find next", "Nds2FindNext", typeof(GridViewFindCommands));
            RestoreStateCommand = new RoutedUICommand("Restore State", "Nds2RestoreState", typeof(GridViewFindCommands));
            ExportExcelCommand = new RoutedUICommand("Export Excel", "Nds2ExportExcel", typeof(GridViewFindCommands));
        }

        private GridViewFindCommands(){}

        public static readonly RoutedUICommand FindNext;
        public static readonly RoutedUICommand RestoreStateCommand;
        public static readonly RoutedUICommand ExportExcelCommand;



    }
}
