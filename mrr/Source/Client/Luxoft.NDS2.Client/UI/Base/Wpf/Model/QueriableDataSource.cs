﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using JetBrains.Annotations;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public class QueriableDataSource<T> : IQueryProvider, IQueryable<T>, INotifyPropertyChanged, INotifyCollectionChanged
    {
        private IEnumerable<T> _data;
        public QueriableDataSource(IEnumerable<T> data)
        {
            _data = data;
        }


        public IEnumerable<T> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
                var subs = CollectionChanged;
                if (subs != null)
                {
                    subs(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }
                
            }
        }

        #region IQueryable Members

        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        Expression IQueryable.Expression
        {
            get { return Expression.Constant(this); }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return this; }
        }

        #endregion

        #region IEnumerable<Person> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return (this as IQueryable).Provider.Execute<IEnumerator<T>>(_expression);
        }


        private Expression _expression = null;

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }


        #endregion

        #region IQueryProvider Members

        IQueryable<S> IQueryProvider.CreateQuery<S>(Expression expression)
        {
            if (typeof(S) != typeof(T))
                throw new Exception("Only " + typeof(T).FullName + " objects are supported.");

            this._expression = expression;

            return (IQueryable<S>)this;
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            return (IQueryable<T>)(this as IQueryProvider).CreateQuery<T>(expression);
        }

        TResult IQueryProvider.Execute<TResult>(Expression expression)
        {
            return (TResult)_data.GetEnumerator();
        }

        object IQueryProvider.Execute(Expression expression)
        {
            if (expression.Type == typeof(int))
            {
                return _data.Count();
            }

            return (this as IQueryProvider).Execute<IEnumerator<T>>(expression);
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }
}
