﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Luxoft.NDS2.Client.UI.Base.Wpf.Commands;
using Luxoft.NDS2.Client.UI.Base.Wpf.Controls;
using Luxoft.NDS2.Client.UI.Base.Wpf.EventArgs;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public class RadGridServerSideViewModel<T> : BaseViewModel where T : class, INotifyPropertyChanged
    {
        private Action<PageRequestModel> _dataProvider;
        private readonly PageRequestModel _request;

        public RadGridServerSideViewModel(Action<PageRequestModel> dataProvider)
        {
            _dataProvider = dataProvider;
            _request = new PageRequestModel { PageIndex = 0, PageSize = 50 };
            FilteredCommand = new FilteredGridCommand(FilteredGridCommandHandler);
            SortingGridCommand = new SortingGridCommand(SortingGridCommandHandler);
            PageIndexChangedCommand = new PageIndexChangedCommand(PageIndexChangedCommandHandler);
        }


        #region Grid commands and handlers

        public FilteredGridCommand FilteredCommand { get; private set; }

        public SortingGridCommand SortingGridCommand { get; private set; }

        public PageIndexChangedCommand PageIndexChangedCommand { get; private set; }

        public ObservableItemCollection<T> Data { get; private set; }

        public void CustomFilterChanged(GridViewDataControl grid, bool withRefresh = true)
        {
            var existingFilters = GetFilterCriterias(grid.FilterDescriptors);

            _request.Filters.Clear();
            _request.Filters.AddRange(existingFilters);

            if (withRefresh)
            {
                _request.PageIndex = 0;
                RefreshData();
            }
        }

        public void InitSortState(ColumnSortModel sortModel)
        {
            _request.Sorting.Clear();
            _request.Sorting.Add(sortModel);
        }

        void SortingGridCommandHandler(GridViewSortingEventArgs e)
        {
            _request.Sorting.Clear();

            ListSortDirection result;
            if (Enum.TryParse(e.NewSortingState.ToString(), true, out result))
            {
                _request.Sorting.Add(new ColumnSortModel() { Name = e.Column.UniqueName, SortDirection = result });
            }
            RefreshData();
        }

        void FilteredGridCommandHandler(GridViewFilteredEventArgs e)
        {
            var existingFilters = GetFilterCriterias(((RadGridView)e.Source).FilterDescriptors);
            _request.Filters.Clear();
            _request.Filters.AddRange(existingFilters);
            _request.PageIndex = 0;

            RefreshData();
        }

        public int PageIndex
        {
            get { return _request.PageIndex; }
            set
            {
                if (_request.PageIndex != value)
                {
                    _request.PageIndex = value;
                    RefreshData();
                }
            }
        }

        public int PageSize
        {
            get { return _request.PageSize; }
            set
            {
                if (_request.PageSize != value)
                {
                    _request.PageSize = value;
                    RefreshData();
                }
            }
        }


        void PageIndexChangedCommandHandler(PageIndexChangedEventArgs e)
        {
            this.PageIndex = e.NewPageIndex;
        }

        #endregion


        private List<FilterSetting> GetFilterCriterias(IEnumerable<IFilterDescriptor> source)
        {
            var filters = new List<FilterSetting>();
            foreach (var filter in source)
            {
                if (filter is FilterDescriptor)
                {
                    var ff = ((FilterDescriptor)filter);
                    var setting = new FilterSetting
                    {
                        ColumnName = ff.Member,
                        ColumnType = ff.MemberType,
                        Filter1 = new FilterDescriptorModel
                        {
                            Operator = ff.Operator,
                            Value = ff.Value,
                        }
                    };
                    filters.Add(setting);
                }
                else if (filter is IColumnFilterDescriptor)
                {
                    var ff = ((IColumnFilterDescriptor)filter);
                    Type pType = typeof(string);
                    if (ff.Column.ItemType != null)
                    {
                        var pi = ff.Column.ItemType.GetProperty(ff.Column.UniqueName);
                        if (pi != null)
                        {
                            pType = pi.PropertyType;
                        }
                    }

                    var setting = new FilterSetting { ColumnName = ff.Column.UniqueName, ColumnType = pType, FieldFilterLogicalOperator = ff.FieldFilter.LogicalOperator };
                    if (ff.FieldFilter.Filter1.IsActive)
                    {
                        setting.Filter1 = new FilterDescriptorModel
                        {
                            Operator = ff.FieldFilter.Filter1.Operator,
                            Value = ff.FieldFilter.Filter1.Value,
                            IsCaseSensitive = ff.FieldFilter.Filter1.IsCaseSensitive
                        };
                    }
                    if (ff.FieldFilter.Filter2.IsActive)
                    {
                        setting.Filter2 = new FilterDescriptorModel
                        {
                            Operator = ff.FieldFilter.Filter2.Operator,
                            Value = ff.FieldFilter.Filter2.Value,
                            IsCaseSensitive = ff.FieldFilter.Filter2.IsCaseSensitive
                        };
                    }
                    filters.Add(setting);
                }
                else if (filter is RoundedCompositeFilterDescriptor)
                {
                    var ff = ((RoundedCompositeFilterDescriptor)filter);
                    var setting = new FilterSetting
                    {
                        ColumnName = ff.ColumnName,
                        ColumnType = ff.ColumnType,
                        FieldFilterLogicalOperator = ff.LogicalOperator
                    };

                    if (ff.Filter1 != null)
                    {
                        setting.Filter1 = new FilterDescriptorModel
                        {
                            Operator = ff.Filter1.Operator,
                            Value = ff.Filter1.Value,
                            HasRoundFactor = true,
                            RoundFactor = ff.Filter1.RoundFactor
                        };
                    }
                    if (ff.Filter2 != null)
                    {
                        setting.Filter2 = new FilterDescriptorModel
                        {
                            Operator = ff.Filter2.Operator,
                            Value = ff.Filter2.Value,
                            HasRoundFactor = true,
                            RoundFactor = ff.Filter2.RoundFactor
                        };
                    }
                    filters.Add(setting);
                }
            }
            return filters;
        }

        /// <summary>
        /// Обновление данных
        /// </summary>
        public void RefreshData()
        {
            _dataProvider(_request);
        }


    }
}
