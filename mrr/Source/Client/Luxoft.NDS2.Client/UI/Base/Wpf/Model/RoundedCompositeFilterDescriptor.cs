﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    /// <summary>
    /// Класс описывает составой фильтр учитывающий свойство округления 
    /// </summary>
    internal class RoundedCompositeFilterDescriptor : IFilterDescriptor
    {
        internal RoundedCompositeFilterDescriptor(GridViewColumn gridViewColumn)
        {
            ColumnName = gridViewColumn.UniqueName;
            ColumnType = gridViewColumn.ItemType;
        }
        public string ColumnName { get; private set; }
        public Type ColumnType { get; private set; }

        public FilterCompositionLogicalOperator LogicalOperator { get; set; }

        public RoundedDecimalFilterDescriptor Filter1 { get; set; }

        public RoundedDecimalFilterDescriptor Filter2 { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Expression CreateFilterExpression(System.Linq.Expressions.Expression instance)
        {
            Expression retVal = null;
            if (Filter1 == null)
                throw new ArgumentException();

            if (Filter2 == null)
            {
                retVal = Filter1.CreateFilterExpression(instance);
                return Filter1.CreateFilterExpression(instance); ;
            }

            var composite = new CompositeFilterDescriptor();
            composite.LogicalOperator = LogicalOperator;
            composite.FilterDescriptors.Add(Filter1);
            composite.FilterDescriptors.Add(Filter2);
            retVal = composite.CreateFilterExpression(instance);
            return composite.CreateFilterExpression(instance);
        }
    }
}
