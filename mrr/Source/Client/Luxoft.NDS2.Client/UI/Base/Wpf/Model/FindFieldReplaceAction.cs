﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    
    public delegate string ReplaceAction<T>(T obj);


    public abstract class BaseFindFieldReplaceAction
    {
        public string FieldName { get; set; }
    }

    public class FindFieldReplaceAction<T> : BaseFindFieldReplaceAction
    {
        public ReplaceAction<T> ReplaceFunc { get; set; }
    }
}
