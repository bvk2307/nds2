﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public class FindViewModel: ViewModelBase
    {
        private string _findText;

        public string FindText
        {
            get
            {
                return this._findText;
            }
            set
            {
                if (this._findText != value)
                {
                    this._findText = value;
                    this.OnPropertyChanged("FindText");
                }
            }
        }

        public void ClearFindText()
        {
            FindText = string.Empty;
        }

    }
}
