﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    /// <summary>
    ///  Класс описывает фильтр учитывающий свойство округления 
    /// </summary>
    internal class RoundedDecimalFilterDescriptor : FilterDescriptor
    {
        internal RoundedDecimalFilterDescriptor(int RoundFactor, int ScaleFactor)
        {
            _roundFactor = RoundFactor;
            _scaleFactor = ScaleFactor;
        }
        private int _scaleFactor;
        private int _roundFactor;

        public int ScaleFactor
        {
            get { return _scaleFactor; }
        }

        public int RoundFactor
        {
            get { return _roundFactor; }
            set { _roundFactor = value; }
        }

        public override Expression CreateFilterExpression(Expression instance)
        {
            var expression = base.CreateFilterExpression(instance);

            var modifier = new RoundDecimalExpressionModifier(_roundFactor, _scaleFactor);

            var modifiedExpression = modifier.Visit(expression);
            return modifiedExpression;
        }
    }
}
