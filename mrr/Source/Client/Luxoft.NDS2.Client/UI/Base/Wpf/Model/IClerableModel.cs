﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Win.Misc;
using JetBrains.Annotations;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public interface IClerableModel
    {
        void RestoreState(GridViewDataControl gridView);

        bool CanRestoreState(GridViewDataControl gridView);
    }
}
