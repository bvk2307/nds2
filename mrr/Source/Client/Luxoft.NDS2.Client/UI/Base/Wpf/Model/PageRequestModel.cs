﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public class PageRequestModel
    {
        public PageRequestModel()
        {
            Filters = new List<FilterSetting>();
            Sorting = new List<ColumnSortModel>();
        }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public List<FilterSetting> Filters { get;  private set; }

        public IList<ColumnSortModel> Sorting { get; private set; }

    }
}
