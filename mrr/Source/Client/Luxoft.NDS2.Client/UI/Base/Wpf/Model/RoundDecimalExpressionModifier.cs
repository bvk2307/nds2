﻿using System;
using System.Linq.Expressions;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    /// <summary>
    /// Внедряет в дерево Linq выражения фильтрации процедуру округлению значения до выбранного масштаба.
    /// Linq выражение фильтрации должно иметь тип параметра - Decimal.
    /// </summary>
    internal class RoundDecimalExpressionModifier : ExpressionVisitor
    {
        internal RoundDecimalExpressionModifier(int RoundFactor, int ScaleFactor)
        {
            _roundFactor = RoundFactor;
            _scaleFactor = ScaleFactor;
        }

        private int _scaleFactor;
        private int _roundFactor;

        public int ScaleFactor
        {
            get { return _scaleFactor; }
        }

        public int RoundFactor
        {
            get { return _roundFactor; }
            set { _roundFactor = value; }
        }

        protected override Expression VisitConditional(ConditionalExpression node)
        {
            if (node.IfTrue is MemberExpression)
            {
                var roundedValue = Expression.Call(
                                                        this.GetType().GetMethod("RoundAsPlSql"),
                                                        node.IfTrue,
                                                        Expression.Constant(_scaleFactor),
                                                        Expression.Constant(_roundFactor));

                return Expression.Condition(node.Test, roundedValue, node.IfFalse);
            }
            return base.VisitConditional(node);
        }

        public static decimal RoundAsPlSql(decimal value, int scaleFactor, int roundFactor)
        {
            return Math.Round(value / scaleFactor, MidpointRounding.AwayFromZero) * scaleFactor;
        }
    }
}
