﻿using GridViewColumn = Telerik.Windows.Controls.GridViewColumn;
using System;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    /// <summary>
    /// Хранит параметры процесса текущего поиска
    /// </summary>
    internal class FindViewSearchStateModel
    {
        private bool _startIndexUpdated;

        /// <summary>
        /// Индекс страницы с которой начался поиск в гриде
        /// </summary>
        public int SearchStartPageIndex { get; private set; }

        /// <summary>
        /// Строка вхождения которой нужно найти в гриде
        /// </summary>
        public string SearchingValue { get; set; }

        /// <summary>
        /// Признак отсутсвия найденных вхождений
        /// </summary>
        public bool NoAnyValueFound { get; private set; }

        /// <summary>
        /// Айтем грида в котором было найдено вхождение искомой строки
        /// </summary>
        public object LastFoundItem { get; private set; }

        /// <summary>
        /// Индекс столбца грида в котором было найдено вхождение искомой строки
        /// </summary>
        public int LastFoundColumnIndex { get; private set; }
        /// <summary>
        /// Задает начальное состояние поиска
        /// </summary>
        /// <param name="SearchValue">Строка для поиска</param>
        public void InitSearch(string SearchValue)
        {
            SearchingValue = SearchValue;
            NoAnyValueFound = true;
            LastFoundItem = null;
            LastFoundColumnIndex = -1;
            SearchStartPageIndex = 0;
            _startIndexUpdated = false;
        }

        /// <summary>
        /// Задает  начальное состояние поиска с сохранением текущей строки.
        /// </summary>
        public void ReinitSearch()
        {
            InitSearch(this.SearchingValue);
        }

        /// <summary>
        /// Сохраняет объект как последний найденный
        /// </summary>
        /// <param name="itemFound">Айтем грида в котором найдено вхождение</param>
        /// <param name="columnFoundIndex">Столбец грида в котором найдено вхождение</param>
        public void SetLastFound(object itemFound, int columnFoundIndex)
        {
            if (NoAnyValueFound && itemFound != null)
                NoAnyValueFound = false;

            this.LastFoundItem = itemFound;
            this.LastFoundColumnIndex = columnFoundIndex;
        }

        /// <summary>
        /// Зануляет значения последнего найденного вхождения
        /// </summary>
        public void ClearLastFound()
        {
            SetLastFound(null, -1);
        }

        /// <summary>
        /// Обновляет свойство начальной страницы поиска. 
        /// Начальной страницей поиска считается та страница с индексом которой был вызыван метод в первый раз с момента инициализации поиска.
        /// </summary>
        /// <param name="index">Индекс старницы</param>
        /// <exception cref="System.ArgumentException">Выбрасывается когда индекс страницы меньше ноля</exception>
        public void UpdateStartIndex(int index)
        {
            if(!_startIndexUpdated)
            {
                if (index < 0)
                    throw new ArgumentException("index", "Индекс начальной страницы поиска не модет быть отрицательным");
                this.SearchStartPageIndex = index;
                _startIndexUpdated = true;
            }
        }
    }
}
