﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public interface IPagedModel
    {
        int PageIndex { get; }

        int PageCount { get; }

        ICollection PageData(int pageIndex);

        bool NextPage();

        void FirstPage();

        bool GoPage(int pageIndex);
    }
}
