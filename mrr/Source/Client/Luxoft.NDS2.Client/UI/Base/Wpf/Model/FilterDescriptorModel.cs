﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Model
{
    public class FilterDescriptorModel
    {
        public Telerik.Windows.Data.FilterOperator Operator { get; set; }
        
        public object Value { get; set; }
        
        public bool IsCaseSensitive { get; set; }

        public int RoundFactor { get; set; }

        public bool HasRoundFactor { get; set; }
    }

    public class FilterSetting
    {
        public string ColumnName { get; set; }

        public List<object> SelectedDistinctValues = new List<object>();

        public FilterDescriptorModel Filter1 { get; set; }
        
        public Telerik.Windows.Data.FilterCompositionLogicalOperator FieldFilterLogicalOperator { get; set; }
        
        public FilterDescriptorModel Filter2 { get; set; }
        
        public Type ColumnType { get; set; }
    }
}
