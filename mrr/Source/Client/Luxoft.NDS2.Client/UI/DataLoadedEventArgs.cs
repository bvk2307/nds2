﻿using System;

namespace Luxoft.NDS2.Client.UI
{
    public class DataLoadedEventArgs<T> : EventArgs
    {
        public DataLoadedEventArgs(T data)
        {
            Data = data;
        }

        public T Data { get; private set; }
    }
}
