﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI
{
    public static class UiHelper
    {
        public static bool IsPaste(this System.Windows.Forms.KeyEventArgs e)
        {
            return e.KeyCode == Keys.V && e.Control;
        }
    }
}
