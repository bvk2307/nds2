using System;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI
{
    public interface IServiceCallErrorHandler
    {
        void ProcessingErrors(IOperationResult result);

        void ProcessingErrors(IOperationResult result, Exception ex);
    }
}