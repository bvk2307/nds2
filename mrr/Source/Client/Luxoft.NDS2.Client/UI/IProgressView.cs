﻿using System;

namespace Luxoft.NDS2.Client.UI
{
    public class ProgressStateChangedEventArgs : EventArgs
    {
        public ProgressStateChangedEventArgs(bool visible)
        {
            Visible = visible;
        }

        public bool Visible
        {
            get;
            private set;
        }
    }

    public interface IProgressView
    {
        void Show();

        void Hide();

        event EventHandler<ProgressStateChangedEventArgs> StateChanged;
    }
}
