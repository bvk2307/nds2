﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI
{
    public interface IInspectorsDataProxy
    {
        IEnumerable<UserInformation> GetUsers(string sonoCode);
    }
}
