﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Threading;

namespace Luxoft.NDS2.Client.UI
{
    public abstract class ServiceProxyBase
    {
        protected readonly INotifier _notifier;

        protected readonly IClientLogger _logger;

        protected ServiceProxyBase(INotifier notifier, IClientLogger logger)
        {
            _notifier = notifier;
            _logger = logger;
        }

        protected TResult Invoke<TResult>(Func<TResult> doWork, Func<TResult> failure)
            where TResult : IOperationResult
        {
            try
            {
                var result = doWork();
                ProcessResult(result);
                return result;
            }
            catch (Exception error)
            {
                OnInternalError(error);
                return failure();
            }
        }

        protected CancellationTokenSource BeginInvoke<T>(
            Func<T> doWork,
            Action<T> callBack)
            where T : IOperationResult
        {
            var source = new CancellationTokenSource();
            BeginInvoke<T>(source, doWork, callBack);

            return source;
        }

        protected void BeginInvoke<T>(
            CancellationTokenSource source,
            Func<T> doWork,
            Action<T> callBack)
            where T : IOperationResult
        {
            var token = source.Token;

            var worker = new AsyncWorker<T>();
            worker.DoWork +=
                (sender, e) =>
                {
                    if (!token.IsCancellationRequested)
                    {
                        e.Result = doWork();
                    }
                };
            worker.Complete +=
                (sender, e) =>
                {
                    if (!token.IsCancellationRequested)
                    {
                        ProcessResult(e.Result);
                        callBack(e.Result);
                    }
                };
            worker.Failed +=
                (sender, e) =>
                {
                    if (!token.IsCancellationRequested)
                    {
                        OnInternalError(e.Exception);
                    }
                };
            worker.Start();
        }

        private void ProcessResult(IOperationResult operationResult)
        {
            if (operationResult == null)
            {
                OnNullResponse();
                return;
            }

            if (operationResult.Status == ResultStatus.Denied)
            {
                OnDenied();
            }

            if (operationResult.Status == ResultStatus.NoDataFound)
            {
                OnNoDataFound();
            }

            if (operationResult.Status == ResultStatus.Error)
            {
                OnServerError(operationResult.Message);
            }            
        }

        protected virtual void OnNoDataFound()
        {
        }

        protected virtual void OnNullResponse()
        {
            _notifier.ShowError("Произошла непредвиденная ошибка. Повторите операцию позже или обратитесь на СТП");
            _logger.LogError(string.Format("Null operation result. Proxy class: {0}", GetType()), new Exception());
        }

        protected virtual void OnDenied()
        {
            _notifier.ShowError("У Вас нет прав на совершение данной операции");
            _logger.LogWarning(string.Format("Access denied. Proxy class: {0}", GetType()));
        }

        protected virtual void OnServerError(string message)
        {
            _notifier.ShowError("Произошла непредвиденная ошибка. Повторите операцию позже или обратитесь на СТП");
            _logger.LogError(string.Format("{0}. Proxy class: {1}", message, GetType()), new Exception());
        }

        protected virtual void OnInternalError(Exception error)
        {
            _notifier.ShowError("Произошла непредвиденная ошибка. Повторите операцию позже или обратитесь на СТП");
            _logger.LogError(string.Format("{0} internal error", GetType()), error, "OnInternalError");
        }
    }

    public abstract class ServiceProxyBase<TResult> : ServiceProxyBase
    {
        protected ServiceProxyBase(INotifier notifier, IClientLogger logger)
            : base(notifier, logger)
        {
        }

        protected CancellationTokenSource BeginInvoke(Func<OperationResult<TResult>> doWork)
        {
            return BeginInvoke(doWork, result => CallBack(ExtractResult(result)));
        }

        protected CancellationTokenSource BeginInvoke(CancellationTokenSource token, Func<OperationResult<TResult>> doWork)
        {
            BeginInvoke<OperationResult<TResult>>(
                token,
                doWork, 
                result => CallBack(ExtractResult(result)));

            return token;
        }

        protected bool Invoke(Func<OperationResult<TResult>> doWork, out TResult result)
        {
            var retVal = false;

            try
            {
                var response = doWork();
                retVal = response.Status == ResultStatus.Success;                
                result = ExtractResult(response);
            }
            catch(Exception error)
            {
                OnInternalError(error);
                result = FailureResult();
                retVal = false;
            }

            return retVal;
        }

        private TResult ExtractResult(OperationResult<TResult> operationResult)
        {
            return operationResult.Status == ResultStatus.Success
                ? operationResult.Result
                : FailureResult();
        }

        protected virtual TResult FailureResult()
        {
            return default(TResult);
        }

        protected virtual void CallBack(TResult result)
        {
            return;
        }
    }
}
