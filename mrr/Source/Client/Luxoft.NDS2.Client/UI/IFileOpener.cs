﻿namespace Luxoft.NDS2.Client.UI
{
    public interface IFileOpener
    {
        void Open(string filename);
    }
}
