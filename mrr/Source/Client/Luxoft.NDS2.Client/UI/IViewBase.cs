﻿namespace Luxoft.NDS2.Client.UI
{
    public interface IViewBase : INotifier, IMessageView { }
}
