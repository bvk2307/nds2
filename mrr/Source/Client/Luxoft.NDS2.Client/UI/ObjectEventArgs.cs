﻿using System;

namespace Luxoft.NDS2.Client.UI
{
    public class ObjectEventArgs<T> : EventArgs
    {
        public ObjectEventArgs(T item)
        {
            Item = item;
        }

        public T Item
        {
            get;
            private set;
        }
    }

    public class ObjectEventArgs : ObjectEventArgs<object>
    {
        public ObjectEventArgs(object item)
            : base(item)
        {
        }
    }
}
