﻿using Luxoft.NDS2.Client.Model.TaxPeriods;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.InternalControls
{
    public partial class InspectorOptions : UserControl
    {
        public event EventHandler OnFilterChanged;

        ISettingsProvider _settingsProvider;

        private DataShowStates _showState;
        public DataShowStates ShowState
        {
            get
            {
                return _showState;
            }
            private set
            {
                if (_showState == value)
                    return;
                _showState = value;
                GenerateEventFilterChanged();
            }
        }

        private void GenerateEventFilterChanged()
        {
            var subscribers = OnFilterChanged;
            if (subscribers != null)
            {
                subscribers(this, EventArgs.Empty);
            }
        }

        public ISettingsProvider SettingsProvider
        {
            get
            {
                return _settingsProvider;
            }
            set
            {
                _settingsProvider = value;
            }
        }

        public void SetTaxPeriodModel(TaxPeriodModel taxPeriodModel)
        {
            comboTaxPeriod.DataSource = taxPeriodModel.TaxPeriods;
            SetTaxPeriodInitialValue(taxPeriodModel);
            comboTaxPeriod.Refresh();
            comboTaxPeriod.ValueChanged += new EventHandler(ComboTaxPeriod_ValueChanged);
        }

        private void SetTaxPeriodInitialValue(TaxPeriodModel taxPeriodModel)
        {
            if (taxPeriodModel.TaxPeriods.Count > 0)
            {
                string taxPeriodValueSettings = _settingsProvider.LoadSettings(comboTaxPeriod.Name);
                bool isFromSettingsValue = !string.IsNullOrWhiteSpace(taxPeriodValueSettings);
                comboTaxPeriod.SelectedIndex = -1;
                _selectedTaxPeriod = null;
                int index = 0;
                foreach (var item in taxPeriodModel.TaxPeriods)
                {
                    if ((isFromSettingsValue && item.Id.Equals(taxPeriodValueSettings)) ||
                        (!isFromSettingsValue && item.Id.Equals(taxPeriodModel.TaxPeriodDefault.Id)))
                    {
                        _selectedTaxPeriod = item;
                        comboTaxPeriod.SelectedIndex = index;
                        break;
                    }
                    index++;
                }
            }
            else
                comboTaxPeriod.SelectedIndex = -1;
        }

        private TaxPeriodBase _selectedTaxPeriod;

        public TaxPeriodBase SelectedTaxPeriod
        {
            get
            {
                return _selectedTaxPeriod;
            }
        }

        public bool AllowAssign
        {
            set
            {
                rbNotOwned.Checked = value;
                rbOnlyMine.Checked = !value;
                rbAll.Checked = false;
                ShowState = GetState();
            }
        }

        public InspectorOptions()
        {
            InitializeComponent();
            AllowAssign = false;
        }

        private void ComboTaxPeriod_ValueChanged(object sender, EventArgs e)
        {
            TaxPeriodBase itemCurrent = null;
            if (comboTaxPeriod.SelectedIndex > -1)
            {
                itemCurrent = (TaxPeriodBase)comboTaxPeriod.SelectedItem.ListObject;
                _settingsProvider.SaveSettings(itemCurrent.Id, comboTaxPeriod.Name);
            }
            _selectedTaxPeriod = itemCurrent;
            GenerateEventFilterChanged();
        }

        private DataShowStates GetState()
        {
            if (rbOnlyMine.Checked)
                return DataShowStates.Mine;
            if (rbNotOwned.Checked)
                return DataShowStates.Unassigned;
            return DataShowStates.All;            
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            ShowState = GetState();
        }
    }
}
