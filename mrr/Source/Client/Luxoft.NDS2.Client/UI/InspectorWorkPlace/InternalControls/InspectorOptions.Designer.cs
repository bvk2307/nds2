﻿namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.InternalControls
{
    partial class InspectorOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.rbOnlyMine = new System.Windows.Forms.RadioButton();
            this.rbNotOwned = new System.Windows.Forms.RadioButton();
            this.labelTaxPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.comboTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).BeginInit();
            this.SuspendLayout();
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.BackColor = System.Drawing.Color.Transparent;
            this.rbAll.Location = new System.Drawing.Point(3, 3);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(44, 17);
            this.rbAll.TabIndex = 0;
            this.rbAll.Text = "Все";
            this.rbAll.UseVisualStyleBackColor = false;
            this.rbAll.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            // 
            // rbOnlyMine
            // 
            this.rbOnlyMine.AutoSize = true;
            this.rbOnlyMine.BackColor = System.Drawing.Color.Transparent;
            this.rbOnlyMine.Checked = true;
            this.rbOnlyMine.Location = new System.Drawing.Point(66, 3);
            this.rbOnlyMine.Name = "rbOnlyMine";
            this.rbOnlyMine.Size = new System.Drawing.Size(46, 17);
            this.rbOnlyMine.TabIndex = 1;
            this.rbOnlyMine.TabStop = true;
            this.rbOnlyMine.Text = "Мои";
            this.rbOnlyMine.UseVisualStyleBackColor = false;
            this.rbOnlyMine.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            // 
            // rbNotOwned
            // 
            this.rbNotOwned.AutoSize = true;
            this.rbNotOwned.BackColor = System.Drawing.Color.Transparent;
            this.rbNotOwned.Location = new System.Drawing.Point(124, 3);
            this.rbNotOwned.Name = "rbNotOwned";
            this.rbNotOwned.Size = new System.Drawing.Size(125, 17);
            this.rbNotOwned.TabIndex = 2;
            this.rbNotOwned.Text = "Не взятые в работу";
            this.rbNotOwned.UseVisualStyleBackColor = false;
            // 
            // labelTaxPeriod
            // 
            this.labelTaxPeriod.Location = new System.Drawing.Point(256, 4);
            this.labelTaxPeriod.Name = "labelTaxPeriod";
            this.labelTaxPeriod.Size = new System.Drawing.Size(99, 17);
            this.labelTaxPeriod.TabIndex = 24;
            this.labelTaxPeriod.Text = "отчетный период:";
            // 
            // comboTaxPeriod
            // 
            this.comboTaxPeriod.DisplayMember = "DESCRIPTION";
            this.comboTaxPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTaxPeriod.Location = new System.Drawing.Point(359, 1);
            this.comboTaxPeriod.Name = "comboTaxPeriod";
            this.comboTaxPeriod.Size = new System.Drawing.Size(120, 21);
            this.comboTaxPeriod.TabIndex = 26;
            // 
            // InspectorOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.comboTaxPeriod);
            this.Controls.Add(this.labelTaxPeriod);
            this.Controls.Add(this.rbNotOwned);
            this.Controls.Add(this.rbOnlyMine);
            this.Controls.Add(this.rbAll);
            this.Name = "InspectorOptions";
            this.Size = new System.Drawing.Size(489, 31);
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.RadioButton rbOnlyMine;
        private System.Windows.Forms.RadioButton rbNotOwned;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTaxPeriod;
    }
}
