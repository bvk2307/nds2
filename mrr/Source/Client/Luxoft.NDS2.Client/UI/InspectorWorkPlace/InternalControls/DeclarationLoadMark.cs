﻿using Luxoft.NDS2.Client.Properties;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.InternalControls
{
    public enum DeclarationLoadState
    {
        NotLoaded = 0,
        NotActual = 1,
        Actual = 2
    }

    public class DeclarationLoadMark
    {
        private readonly Dictionary<int, Image> _icons;

        public DeclarationProcessignStage State { get; set; }

        public string Description { get; set; }

        public int ColorARGB { get; set; }

        public DeclarationLoadMark()
        {
            _icons = new Dictionary<int, Image>();
        }

        public Image GetIcon(Control owner)
        {
            if (!_icons.ContainsKey(ColorARGB))
            {
                lock (_icons)
                {
                    if (!_icons.ContainsKey(ColorARGB))
                    {
                        _icons[ColorARGB] = DrawIcon(owner);
                    }
                }
            }
            return _icons[ColorARGB];
        }

        private Image DrawIcon(Control owner)
        {
            using (var stream = new MemoryStream())
            using (var hdc = owner.CreateGraphics())
            {
                var rectangle = new Rectangle(0, 0, 16, 16);
                var emf = new Metafile(stream, hdc.GetHdc(), rectangle, MetafileFrameUnit.Pixel, EmfType.EmfPlusOnly);
                using (var graphics = Graphics.FromImage(emf))
                {
                    graphics.FillRectangle(Brushes.Transparent, rectangle);
                    switch (State)
                    {
                        case DeclarationProcessignStage.LoadedInMs:
                            graphics.DrawImage(Resources.star_half, rectangle);
                            break;
                        case DeclarationProcessignStage.ProcessedByMs:
                            graphics.DrawImage(Resources.star_full, rectangle);
                            break;
                    }
                }
                return emf;
            }
        }
    }
}
