﻿using System.Threading;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentParams
{
    public class ContragentParamsPresenter : BasePresenter<ContragentParamsView>
    {
        private readonly DeclarationSummary _declaration;

        private readonly string _contractorInn;

        private readonly int _chapter;
        public DeclarationSummary Declaration
        {
            get
            {
                return _declaration;
            }
        }

        public string ContractorInn { get { return _contractorInn; } }

        public DataGridPresenter<ContragentParamsSummary> GridParamsPresenter { get; set; }

        private readonly IDeclarationCardOpener _declarationCardOpener;

        #region .ctors

        public ContragentParamsPresenter(PresentationContext context, WorkItem wi, ContragentParamsView view, DeclarationSummary declaration, string contractorInn, int chapter)
            : base(context, wi, view)
        {
            _declarationCardOpener = GetDeclarationCardOpener();
            _declaration = declaration;
            _contractorInn = contractorInn;
            _chapter = chapter;
        }

        #endregion

        public void ViewDeclarationDetails()
        {
            var result = _declarationCardOpener.Open(_declaration.DECLARATION_VERSION_ID);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        public object GetData(QueryConditions conditions, out long totalRowsNumber)
        {
            totalRowsNumber = 0;

            var blService = GetServiceProxy<IDeclarationsDataService>();

            var ret = new List<ContragentParamsSummary>();
            int rowsNumTmp = 0;
            if (ExecuteServiceCall(
                    () => blService.GetContragentParams(
                        inn: _declaration.INN,
                        innReorganized: Declaration.INN_CONTRACTOR == Declaration.INN ? null : Declaration.INN_CONTRACTOR,
                        kppEffective: Declaration.KPP_EFFECTIVE,
                        year: Declaration.FISCAL_YEAR,
                        period: Declaration.TAX_PERIOD,
                        typeCode: Declaration.DECL_TYPE_CODE,
                        contragentInn: ContractorInn,
                        chapter: _chapter,
                        conditions: conditions),
                    result =>
                    {
                        rowsNumTmp = result.Result.TotalMatches;
                        ret = result.Result.Rows;
                    }))
            {
                totalRowsNumber = rowsNumTmp;
            }

            return ret;
        }

        public void RequestDataFromSOV()
        {
            var cts = new CancellationTokenSource();

            var buf = new ContragentsSovOperation { Declaration = new DeclarationRequestData(_declaration) };

            var blService = GetServiceProxy<IDeclarationsDataService>();
            ExecuteSovRequest(
                blService.GetContragentParamsDataSov,
                null,
                buf,
                ResourceManagerNDS2.RequestsTimeOutMillisec,
                ResourceManagerNDS2.SovError,
                r => View.ExecuteInUiThread(c => View.GridUpdate()),
                cts.Token
                );
        }
    }
}
