﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Infragistics.Win.FormattedLinkLabel;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentParams
{
    public partial class ContragentParamsView : BaseView
    {
        private readonly ContragentParamsPresenter _presenter;
        private readonly IUcVisualStateService _visualStateService;
        private const string CMD_PREFIX = "iwpContragentParams";
        private UcRibbonButtonToolContext _btnOpen;
        private UcRibbonButtonToolContext _btnOpenGapCard;
        private int _chapter;

        public ContragentParamsView()
        {
            _presenter = null;

            InitializeComponent();
        }

        public ContragentParamsView(PresentationContext context, WorkItem wi, DeclarationSummary declaration, string contractorInn, int chapter)
            : base(context, wi)
        {
            _visualStateService = wi.Services.Get<IUcVisualStateService>();
            _presenter = new ContragentParamsPresenter(context, wi, this, declaration, contractorInn, chapter);
            _presenter.OnObjectCreated();

            InitializeComponent();

            if (String.IsNullOrEmpty(declaration.KPP))
            {
                labelInnKpp.Text = "ИНН:";
                labelInnKppValue.Text = declaration.INN;
            }
            else
            {
                labelInnKpp.Text = "ИНН/КПП:";
                labelInnKppValue.Text = string.Format("{0} / {1}", declaration.INN, declaration.KPP);
            }
            labelPeriodValue.Text = declaration.FULL_TAX_PERIOD;
            labelTaxPayerName.Value = string.Format("<a href='#'>{0}</a>", declaration.NAME);
            labelTaxPayerName.LinkClicked +=
                (sender, args) => _presenter.ViewTaxPayerByKppEffective(declaration.INN, declaration.KPP_EFFECTIVE);

            labelDeclarationLink.Value = string.Format("(№ корректировки: <a href='#'>{0}</a>)", declaration.CORRECTION_NUMBER);

            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = declaration.SUR_CODE;
            _chapter = chapter;
        }

        private void View_Load(object sender, EventArgs e)
        {
            InitializeGrid();
            InitializeRibbon();
            SetEnabledRibbonButtons();
        }

        #region Ribbon

        private void SetEnabledRibbonButtons()
        {
        }

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var groupFuncs = tabNavigator.AddGroup("NDS2ContragentParamsManage");
            groupFuncs.Text = "Функции";
            groupFuncs.Visible = true;

            var btnUpdate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Update", CMD_PREFIX + "UpdateView")
            {
                Text = "Обновить",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Open", CMD_PREFIX + "OpenTaxPayer")
            {
                Text = "Карточка контрагента",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = false
            };

            _btnOpenGapCard = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "OpenGapCard", CMD_PREFIX + "DiscrepancyCardOpen")
            {
                Text = "Карточка расхождения",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            groupFuncs.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpen.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpenGapCard.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnUpdate,
                _btnOpen,
                _btnOpenGapCard,
                tabNavigator
            });
            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
            _visualStateService.DoRefreshVisualEnvironment(_presentationContext.Id);
        }

        [CommandHandler(CMD_PREFIX + "UpdateView")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            GridUpdate();
        }

        [CommandHandler(CMD_PREFIX + "OpenTaxPayer")]
        public void TaxPayerOpenClick(object sender, EventArgs e)
        {
            var summary = gridParams.GetCurrentItem<ContragentParamsSummary>();

            if (summary != null)
                _presenter.ViewTaxPayerByKppOriginal(summary.CONTRACTOR_INN, summary.CONTRACTOR_KPP);
        }

        [CommandHandler(CMD_PREFIX + "DiscrepancyCardOpen")]
        public void DiscrepancyCardOpen(object sender, EventArgs e)
        {
            var summary = gridParams.GetCurrentItem<ContragentParamsSummary>();

            if (summary.DIS_ID.HasValue)
                _presenter.ViewDiscrepancyDetails(summary.DIS_ID.Value);

        }

        #endregion
        
        private void InitializeGrid()
        {
            var helper = new ColumnHelper<ContragentParamsSummary>(gridParams);
            var setup = new GridColumnSetup();

            #region Тип документа, СУР, Налогоплательщик ИНН, КПП, Название

            //setup.Columns.Add(helper.CreateTextColumn(o => o.MARK, 2));
            setup.Columns.Add(helper.CreateTextColumn(o => o.DIS_STATUS_NAME, 2));

            var group = helper.CreateGroup("Контрагент");
            group.Columns.Add(helper.CreateSurColumn(o => o.SUR_CODE, _presenter.Sur));
            group.Columns.Add(helper.CreateTextColumn(o => o.CONTRACTOR_INN));
            group.Columns.Add(helper.CreateTextColumn(o => o.CONTRACTOR_KPP));
            group.Columns.Add(helper.CreateTextColumn(o => o.CONTRACTOR_NAME).Configure(c => c.DisableSort = true));
            setup.Columns.Add(group);

            #endregion

            #region Счет-фактура

            group = helper.CreateGroup("Счет-фактура");
            group.Columns.Add(helper.CreateNumberColumn(o => o.INVOICE_NUM));
            group.Columns.Add(helper.CreateTextColumn(o => o.INVOICE_DATE));
            setup.Columns.Add(group);

            #endregion

            #region Сумма покупок, сумма НДС, вес

            setup.Columns.Add(helper.CreateNumberColumn(o => o.AMOUNT_TOTAL, 2));
            setup.Columns.Add(helper.CreateNumberColumn(o => o.AMOUNT_NDS, 2));

            #endregion

            #region Расхождения

            group = helper.CreateGroup("Расхождение");
            group.Columns.Add(helper.CreateTextColumn(o => o.DIS_TYPE_NAME));
            group.Columns.Add(helper.CreateNumberColumn(o => o.DIS_AMOUNT));
            setup.Columns.Add(group);

            #endregion

            var pager = new PagerStateViewModel();
            var pageNavigator = new DataGridPageNavigator(pager);
            gridParams.WithPager(pageNavigator).InitColumns(setup);

            var queryConditions = 
                new QueryConditions 
                { 
                    Sorting = 
                        new List<ColumnSort> 
                        { 
                            new ColumnSort 
                            { 
                                ColumnKey =
                                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.AMOUNT_NDS), 
                                Order = ColumnSort.SortOrder.Asc 
                            } 
                        } 
                };

            gridParams.RowDoubleClicked += item =>
            {
                var summary = (ContragentParamsSummary)item;
                _presenter.ViewTaxPayerByKppOriginal(summary.CONTRACTOR_INN, summary.CONTRACTOR_KPP);
            };
            gridParams.SelectRow += SelectRowHandler;
            gridParams.Title = string.IsNullOrWhiteSpace(_presenter.ContractorInn) ? "Показатели контрагентов" : "Показатели контрагента";

            _presenter.GridParamsPresenter = 
                new PagedDataGridPresenter<ContragentParamsSummary>(
                    gridParams,
                    pager,
                    conditions =>
                    {
                        long rows;
                        var list = (List<ContragentParamsSummary>)_presenter.GetData(conditions, out rows);
                        return new PageResult<ContragentParamsSummary>(list, (int)rows);
                    },
                    queryConditions);
            _presenter.GridParamsPresenter.Init(_presenter.SettingsProvider(string.Format("{0}_contragentParams", GetType())));

            GridUpdate();
        }

        void SelectRowHandler(object sender, EventArgs e)
        {
            var summary = gridParams.GetCurrentItem<ContragentParamsSummary>();
            _btnOpen.Enabled = summary != null;
            _btnOpenGapCard.Enabled = summary != null;
            _presenter.RefreshEKP();
        }

        public void GridUpdate()
        {
            _presenter.GridParamsPresenter.Load();
        }

        private void DeclarationLinkClicked(object sender, LinkClickedEventArgs e)
        {
            _presenter.ViewDeclarationDetails();
        }

        private void RowMenuOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var summary = gridParams.GetCurrentItem<ContragentParamsSummary>();
            cmiGapCard.Enabled = summary.DIS_AMOUNT > 0;
        }

    }
}
