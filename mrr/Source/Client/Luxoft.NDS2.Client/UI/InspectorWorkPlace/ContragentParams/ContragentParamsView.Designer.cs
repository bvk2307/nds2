﻿using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentParams
{
    partial class ContragentParamsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.tabPage = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.labelTaxPayerName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.labelPeriodValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnKppValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnKpp = new Infragistics.Win.Misc.UltraLabel();
            this.labelDeclarationLink = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.detailsBoxArea = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tcTaxPayer = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.groupTaxPayerData = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.gridParams = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.cmRowMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenTaxPayer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmiGapCard = new System.Windows.Forms.ToolStripMenuItem();
            this.progressLoad = new System.Windows.Forms.ProgressBar();
            this.tabPage.SuspendLayout();
            this.detailsBoxArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcTaxPayer)).BeginInit();
            this.tcTaxPayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxPayerData)).BeginInit();
            this.groupTaxPayerData.SuspendLayout();
            this.cmRowMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage
            // 
            this.tabPage.Controls.Add(this._surIndicator);
            this.tabPage.Controls.Add(this.labelTaxPayerName);
            this.tabPage.Controls.Add(this.labelPeriodValue);
            this.tabPage.Controls.Add(this.labelPeriod);
            this.tabPage.Controls.Add(this.labelInnKppValue);
            this.tabPage.Controls.Add(this.labelInnKpp);
            this.tabPage.Controls.Add(this.labelDeclarationLink);
            this.tabPage.Location = new System.Drawing.Point(0, 0);
            this.tabPage.Name = "tabPage";
            this.tabPage.Size = new System.Drawing.Size(879, 175);
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(5, 0);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 19;
            // 
            // labelTaxPayerName
            // 
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelTaxPayerName.ActiveLinkAppearance = appearance3;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.labelTaxPayerName.Appearance = appearance1;
            this.labelTaxPayerName.Location = new System.Drawing.Point(27, 0);
            this.labelTaxPayerName.Name = "labelTaxPayerName";
            this.labelTaxPayerName.Size = new System.Drawing.Size(858, 26);
            this.labelTaxPayerName.TabIndex = 8;
            this.labelTaxPayerName.TabStop = true;
            this.labelTaxPayerName.Value = "<a href=\"#\">Василек и Ромашка</a>";
            this.labelTaxPayerName.WrapText = false;
            // 
            // labelPeriodValue
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.labelPeriodValue.Appearance = appearance19;
            this.labelPeriodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeriodValue.Location = new System.Drawing.Point(501, 32);
            this.labelPeriodValue.Name = "labelPeriodValue";
            this.labelPeriodValue.Padding = new System.Drawing.Size(2, 0);
            this.labelPeriodValue.Size = new System.Drawing.Size(60, 12);
            this.labelPeriodValue.TabIndex = 6;
            this.labelPeriodValue.Text = "1 кв 2010";
            // 
            // labelPeriod
            // 
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this.labelPeriod.Appearance = appearance7;
            this.labelPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeriod.Location = new System.Drawing.Point(372, 32);
            this.labelPeriod.Name = "labelPeriod";
            this.labelPeriod.Padding = new System.Drawing.Size(2, 0);
            this.labelPeriod.Size = new System.Drawing.Size(112, 12);
            this.labelPeriod.TabIndex = 5;
            this.labelPeriod.Text = "Отчетный период:";
            // 
            // labelInnKppValue
            // 
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            this.labelInnKppValue.Appearance = appearance24;
            this.labelInnKppValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInnKppValue.Location = new System.Drawing.Point(74, 32);
            this.labelInnKppValue.Name = "labelInnKppValue";
            this.labelInnKppValue.Padding = new System.Drawing.Size(2, 0);
            this.labelInnKppValue.Size = new System.Drawing.Size(184, 12);
            this.labelInnKppValue.TabIndex = 4;
            this.labelInnKppValue.Text = "1234567890 / 1234567890";
            // 
            // labelInnKpp
            // 
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            this.labelInnKpp.Appearance = appearance13;
            this.labelInnKpp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInnKpp.Location = new System.Drawing.Point(8, 32);
            this.labelInnKpp.Name = "labelInnKpp";
            this.labelInnKpp.Padding = new System.Drawing.Size(2, 0);
            this.labelInnKpp.Size = new System.Drawing.Size(68, 12);
            this.labelInnKpp.TabIndex = 3;
            this.labelInnKpp.Text = "ИНН/КПП:";
            // 
            // labelDeclarationLink
            // 
            appearance34.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance34.FontData.Name = "Microsoft Sans Serif";
            appearance34.FontData.SizeInPoints = 8.25F;
            appearance34.TextHAlignAsString = "Left";
            appearance34.TextVAlignAsString = "Top";
            this.labelDeclarationLink.Appearance = appearance34;
            this.labelDeclarationLink.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.labelDeclarationLink.Location = new System.Drawing.Point(586, 29);
            this.labelDeclarationLink.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelDeclarationLink.Name = "labelDeclarationLink";
            this.labelDeclarationLink.Padding = new System.Drawing.Size(2, 0);
            this.labelDeclarationLink.Size = new System.Drawing.Size(270, 16);
            this.labelDeclarationLink.TabIndex = 18;
            this.labelDeclarationLink.TabStop = true;
            this.labelDeclarationLink.Value = "<a href=\"#\">(Декларация)</a>";
            this.labelDeclarationLink.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.DeclarationLinkClicked);
            // 
            // detailsBoxArea
            // 
            this.detailsBoxArea.Controls.Add(this.tcTaxPayer);
            this.detailsBoxArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsBoxArea.Location = new System.Drawing.Point(3, 19);
            this.detailsBoxArea.Name = "detailsBoxArea";
            this.detailsBoxArea.Size = new System.Drawing.Size(879, 58);
            this.detailsBoxArea.TabIndex = 0;
            // 
            // tcTaxPayer
            // 
            this.tcTaxPayer.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tcTaxPayer.Controls.Add(this.tabPage);
            this.tcTaxPayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcTaxPayer.Location = new System.Drawing.Point(0, 0);
            this.tcTaxPayer.Name = "tcTaxPayer";
            this.tcTaxPayer.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tcTaxPayer.Size = new System.Drawing.Size(879, 175);
            this.tcTaxPayer.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.tcTaxPayer.TabIndex = 0;
            ultraTab1.TabPage = this.tabPage;
            ultraTab1.Text = "ULTab";
            this.tcTaxPayer.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(879, 175);
            // 
            // groupTaxPayerData
            // 
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.Opaque;
            this.groupTaxPayerData.Appearance = appearance4;
            this.groupTaxPayerData.Controls.Add(this.detailsBoxArea);
            this.groupTaxPayerData.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupTaxPayerData.ExpandedSize = new System.Drawing.Size(885, 80);
            this.groupTaxPayerData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTaxPayerData.Location = new System.Drawing.Point(0, 0);
            this.groupTaxPayerData.Name = "groupTaxPayerData";
            this.groupTaxPayerData.Size = new System.Drawing.Size(885, 80);
            this.groupTaxPayerData.TabIndex = 0;
            this.groupTaxPayerData.TabStop = false;
            this.groupTaxPayerData.Text = "Данные налогоплательщика";
            // 
            // gridParams
            // 
            this.gridParams.AggregatePanelVisible = true;
            this.gridParams.AllowMultiGrouping = true;
            this.gridParams.AllowResetSettings = false;
            this.gridParams.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridParams.BackColor = System.Drawing.Color.Transparent;
            this.gridParams.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridParams.FooterVisible = true;
            this.gridParams.GridContextMenuStrip = this.cmRowMenu;
            this.gridParams.Location = new System.Drawing.Point(0, 80);
            this.gridParams.Name = "gridParams";
            this.gridParams.PanelLoadingVisible = false;
            this.gridParams.PanelPagesVisible = true;
            this.gridParams.RowDoubleClicked = null;
            this.gridParams.Size = new System.Drawing.Size(885, 227);
            this.gridParams.TabIndex = 1;
            this.gridParams.Title = "Показатели контрагентов";
            // 
            // cmRowMenu
            // 
            this.cmRowMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenTaxPayer,
            this.toolStripSeparator2,
            this.cmiGapCard});
            this.cmRowMenu.Name = "cmRowMenu";
            this.cmRowMenu.Size = new System.Drawing.Size(287, 76);
            this.cmRowMenu.Opening += new System.ComponentModel.CancelEventHandler(this.RowMenuOpening);
            // 
            // cmiOpenTaxPayer
            // 
            this.cmiOpenTaxPayer.Name = "cmiOpenTaxPayer";
            this.cmiOpenTaxPayer.Size = new System.Drawing.Size(400, 22);
            this.cmiOpenTaxPayer.Text = "Открыть карточку контрагента";
            this.cmiOpenTaxPayer.Click += new System.EventHandler(this.TaxPayerOpenClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(397, 6);
            // 
            // cmiGapCard
            // 
            this.cmiGapCard.Name = "cmiGapCard";
            this.cmiGapCard.Size = new System.Drawing.Size(286, 22);
            this.cmiGapCard.Text = "Открыть карточку расхождения по СФ";
            this.cmiGapCard.Click += new System.EventHandler(this.DiscrepancyCardOpen);
            // 
            // progressLoad
            // 
            this.progressLoad.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressLoad.Location = new System.Drawing.Point(0, 307);
            this.progressLoad.Name = "progressLoad";
            this.progressLoad.Size = new System.Drawing.Size(885, 23);
            this.progressLoad.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressLoad.TabIndex = 3;
            this.progressLoad.Visible = false;
            // 
            // ContragentParamsView
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.gridParams);
            this.Controls.Add(this.progressLoad);
            this.Controls.Add(this.groupTaxPayerData);
            this.Name = "ContragentParamsView";
            this.Size = new System.Drawing.Size(885, 330);
            this.Load += new System.EventHandler(this.View_Load);
            this.tabPage.ResumeLayout(false);
            this.detailsBoxArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcTaxPayer)).EndInit();
            this.tcTaxPayer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxPayerData)).EndInit();
            this.groupTaxPayerData.ResumeLayout(false);
            this.cmRowMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V2.DataGridView gridParams;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tcTaxPayer;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.ProgressBar progressLoad;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groupTaxPayerData;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel detailsBoxArea;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPage;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelTaxPayerName;
        private Infragistics.Win.Misc.UltraLabel labelPeriodValue;
        private Infragistics.Win.Misc.UltraLabel labelPeriod;
        private Infragistics.Win.Misc.UltraLabel labelInnKppValue;
        private Infragistics.Win.Misc.UltraLabel labelInnKpp;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelDeclarationLink;
        private System.Windows.Forms.ContextMenuStrip cmRowMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTaxPayer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem cmiGapCard;
        private Controls.Sur.SurIcon _surIndicator;
    }
}
