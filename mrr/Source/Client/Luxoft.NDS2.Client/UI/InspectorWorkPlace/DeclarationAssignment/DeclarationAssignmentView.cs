﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public partial class DeclarationAssignmentView : Form, IDeclarationAssignmentView
    {
        bool _isDroppingDown = false;
        #region .ctor

        public DeclarationAssignmentView()
        {
            InitializeComponent();
        }

        #endregion

        public event EventHandler Assinging;

        public event EventHandler SearchingForUsers;

        private void TimerSearchTick(object sender, EventArgs e)
        {
            _timerSearch.Stop();
            if (_usersList.TextLength > 1)
            {
                if (_usersList.DroppedDown)
                    _usersList.DroppedDown = false;
                var subscriber = SearchingForUsers;
                if (subscriber != null)
                    subscriber.Invoke(this, e);
            }
            else
                _usersList.CloseUp();
        }

        #region Выбор пользователя

        public string UserSearchText
        {
            get { return _usersList.Text; }
        }

        public UserInformation SelectedUser
        {
            get
            {
                return _usersList.GetValue() as UserInformation;
            }
        }

        public void SetUsers(IEnumerable<UserInformation> users)
        {
            _usersList.Items.Clear();

            foreach (var dataItem in users)
            {
                _usersList.Items.Add(new Infragistics.Win.ValueListItem(dataItem, GetUserText(dataItem)));
            }

            if (_usersList.Items.Count > 0)
            {
                if (!_isDroppingDown)
                    _usersList.DroppedDown = true;
                _usersList.SelectionStart = _usersList.TextLength;
            }
            else
                _usersList.CloseUp();
        }

        public void ResetSelectedUser()
        {
            _usersList.SelectedIndex = -1;
            _usersList.Text = string.Empty;
            _usersList.Items.Clear();
        }

        private void UsersListTextChanged(object sender, EventArgs e)
        {
            _acceptButton.Enabled = false;
            if (_timerSearch.Enabled)
                _timerSearch.Stop();
            _timerSearch.Start();
        }

        private void UsersListSelectionChanged(object sender, EventArgs e)
        {
            if (_timerSearch.Enabled)
                _timerSearch.Stop();
        }

        private void UsersListBeforeDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _isDroppingDown = true;
            var subscriber = SearchingForUsers;
            if (subscriber != null)
                subscriber.Invoke(this, e);
        }

        private void UsersListAfterDropDown(object sender, EventArgs e)
        {
            _isDroppingDown = false;
            if (_usersList.Items.Count == 0)
                _usersList.CloseUp();
        }

        private static string GetUserText(UserInformation user)
        {
            return string.Concat(user.EmployeeNum, " - ", user.Name);
        }

        #endregion

        #region Назначение

        public void ShowProgress(decimal percentsCompleted)
        {
            _progressBar.StartExecuteInUiThread(() =>
            {
                _progressBar.Value = Convert.ToInt32(percentsCompleted);
                Refresh();
            });
        }

        private void UsersListSelectionChangeCommitted(object sender, EventArgs e)
        {
            _acceptButton.Enabled = true;
        }

        private bool _assignmentInProcess = false;

        private bool AssignmentInProcess
        {
            get
            {
                return _assignmentInProcess;
            }
            set
            {
                _assignmentInProcess = value;
                _acceptButton.Enabled = !_assignmentInProcess;
                _cancelButton.Enabled = !_assignmentInProcess;
                _progressBar.Value = 0;
                _progressBar.Visible = _assignmentInProcess;
            }
        }

        private void DeclarationAssignmentViewValidating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = AssignmentInProcess;
        }

        private void DeclarationAssignmentViewFormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = AssignmentInProcess;
        }

        private void AcceptButtonClick(object sender, EventArgs e)
        {
            var subscriber = Assinging;
            if (subscriber != null)
            {
                AssignmentInProcess = true;
                subscriber.Invoke(this, e);
            }
        }

        #endregion

        void IDeclarationAssignmentView.ShowDialog()
        {
            ShowDialog();
        }

        void IDeclarationAssignmentView.Close()
        {
            AssignmentInProcess = false;
            Close();
        }

        private void DeclarationAssignmentViewShown(object sender, EventArgs e)
        {
            _usersList.Focus();
        }

    }
}
