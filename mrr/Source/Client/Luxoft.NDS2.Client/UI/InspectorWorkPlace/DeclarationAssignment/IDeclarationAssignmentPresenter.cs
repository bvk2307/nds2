﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public interface IDeclarationAssignmentPresenter
    {
        void ShowDialog(IEnumerable<DeclarationSummaryKey> declarationsToAssign);

        event EventHandler Assigned;
    }
}
