﻿namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    partial class DeclarationAssignmentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._cancelButton = new Infragistics.Win.Misc.UltraButton();
            this._acceptButton = new Infragistics.Win.Misc.UltraButton();
            this._inspectorSelectArea = new Infragistics.Win.Misc.UltraGroupBox();
            this._usersList = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._progressBar = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this._timerSearch = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._inspectorSelectArea)).BeginInit();
            this._inspectorSelectArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._usersList)).BeginInit();
            this.SuspendLayout();
            // 
            // _cancelButton
            // 
            this._cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point(322, 165);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(106, 25);
            this._cancelButton.TabIndex = 2;
            this._cancelButton.Text = "Отмена";
            // 
            // _acceptButton
            // 
            this._acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._acceptButton.Enabled = false;
            this._acceptButton.Location = new System.Drawing.Point(210, 165);
            this._acceptButton.Name = "_acceptButton";
            this._acceptButton.Size = new System.Drawing.Size(106, 25);
            this._acceptButton.TabIndex = 1;
            this._acceptButton.Text = "Назначить";
            this._acceptButton.Click += new System.EventHandler(this.AcceptButtonClick);
            // 
            // _inspectorSelectArea
            // 
            this._inspectorSelectArea.Controls.Add(this._usersList);
            this._inspectorSelectArea.Dock = System.Windows.Forms.DockStyle.Top;
            this._inspectorSelectArea.Location = new System.Drawing.Point(0, 0);
            this._inspectorSelectArea.Name = "_inspectorSelectArea";
            this._inspectorSelectArea.Size = new System.Drawing.Size(446, 59);
            this._inspectorSelectArea.TabIndex = 0;
            this._inspectorSelectArea.Text = "Инспектор:";
            // 
            // _usersList
            // 
            this._usersList.DisplayMember = "Name";
            this._usersList.Location = new System.Drawing.Point(21, 25);
            this._usersList.MaxLength = 255;
            this._usersList.Name = "_usersList";
            this._usersList.Size = new System.Drawing.Size(407, 21);
            this._usersList.TabIndex = 0;
            this._usersList.ValueMember = "Sid";
            this._usersList.SelectionChangeCommitted += new System.EventHandler(this.UsersListSelectionChangeCommitted);
            this._usersList.SelectionChanged += new System.EventHandler(this.UsersListSelectionChanged);
            this._usersList.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.UsersListBeforeDropDown);
            this._usersList.AfterDropDown += new System.EventHandler(this.UsersListAfterDropDown);
            this._usersList.TextChanged += new System.EventHandler(this.UsersListTextChanged);
            // 
            // _progressBar
            // 
            this._progressBar.Location = new System.Drawing.Point(85, 87);
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(282, 23);
            this._progressBar.TabIndex = 3;
            this._progressBar.Text = "[Formatted]";
            this._progressBar.Visible = false;
            // 
            // _timerSearch
            // 
            this._timerSearch.Interval = 300;
            this._timerSearch.Tick += new System.EventHandler(this.TimerSearchTick);
            // 
            // DeclarationAssignmentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelButton;
            this.ClientSize = new System.Drawing.Size(446, 202);
            this.Controls.Add(this._progressBar);
            this.Controls.Add(this._inspectorSelectArea);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._acceptButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeclarationAssignmentView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Назначение исполнителя";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeclarationAssignmentViewFormClosing);
            this.Shown += new System.EventHandler(this.DeclarationAssignmentViewShown);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.DeclarationAssignmentViewValidating);
            ((System.ComponentModel.ISupportInitialize)(this._inspectorSelectArea)).EndInit();
            this._inspectorSelectArea.ResumeLayout(false);
            this._inspectorSelectArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._usersList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _acceptButton;
        private Infragistics.Win.Misc.UltraGroupBox _inspectorSelectArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _usersList;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar _progressBar;
        private System.Windows.Forms.Timer _timerSearch;
        private Infragistics.Win.Misc.UltraButton _cancelButton;
    }
}