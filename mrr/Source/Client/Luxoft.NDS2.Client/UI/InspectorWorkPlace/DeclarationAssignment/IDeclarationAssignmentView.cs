﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public interface IDeclarationAssignmentView
    {
        /// <summary>
        /// Thread-safe
        /// </summary>
        /// <param name="users"></param>
        void SetUsers(IEnumerable<UserInformation> users);

        UserInformation SelectedUser { get; }

        void ResetSelectedUser();

        string UserSearchText { get; }

        event EventHandler Assinging;

        event EventHandler SearchingForUsers;

        /// <summary>
        /// Thread-safe
        /// </summary>
        /// <param name="percentsCompleted"></param>
        void ShowProgress(decimal percentsCompleted);

        void ShowDialog();

        void Close();
    }
}
