﻿using System;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public class DeclarationAssignmentProxy : ServiceProxyBase, IDeclarationAssignmentProxy
    {
        private readonly IDeclarationsDataService _service;

        public DeclarationAssignmentProxy(
            IDeclarationsDataService service,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public event EventHandler Assigned;

        public event EventHandler Failed;

        public event EventHandler Completed;

        private IEnumerator<DeclarationSummaryKey> _declarations;

        private string _assignToSid;

        public void AssignAll(IEnumerator<DeclarationSummaryKey> declarations, string assignToSid)
        {
            try
            {
                ThrowOnInvalidArgs(declarations, assignToSid);
                _declarations = declarations;
                _assignToSid = assignToSid;
                AssignCurrent();
            }
            catch (Exception Ex)
            {
                _logger.LogError(Ex);
                _notifier.ShowError("При выполнении назначения произошла ошибка.");
            }
        }

        private void ThrowOnInvalidArgs(IEnumerator<DeclarationSummaryKey> declarations, string assignToSid)
        {
            if (declarations == null)
                throw new ArgumentNullException("declarations");
            
            if (!declarations.MoveNext())
                throw new ArgumentException("declarations", "Перечень ключей деклараций пуст.");

            if (String.IsNullOrWhiteSpace(assignToSid))
                throw new ArgumentException("assignToSid", "Пустой SID");
        }

        private void AssignCurrent()
        {
            BeginInvoke<OperationResult>(
                () => _service.AssignDeclaration(_declarations.Current, _assignToSid),
                CallBackAssign);
        }

        private void CallBackAssign(OperationResult result)
        {
            if (result.Status != ResultStatus.Success)
            {
                RaiseFailure();
                return;
            }
            RaiseAssigned();

            if (_declarations.MoveNext())
            {
                AssignCurrent();
            }
            else
            {
                RaiseCompleted();
                _declarations.Dispose();
            }
        }

        protected override void OnInternalError(Exception error)
        {
            base.OnInternalError(error);
            RaiseFailure();
        }

        private void RaiseFailure()
        {
            var handler = Failed;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        private void RaiseAssigned()
        {
            var handler = Assigned;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        private void RaiseCompleted()
        {
            var handler = Completed;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
