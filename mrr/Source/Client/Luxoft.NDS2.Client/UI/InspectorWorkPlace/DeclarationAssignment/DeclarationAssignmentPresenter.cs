﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public sealed class DeclarationAssignmentPresenter : IDeclarationAssignmentPresenter
    {
        # region .ctor

        public DeclarationAssignmentPresenter(
            IInspectorsDataProxy inspectorsProxy,
            IDeclarationAssignmentProxy assignmentProxy,
            IDeclarationAssignmentView view,
            IClientLogger logger,
            string sonoCode)
        {
            _inspectorsProxy = inspectorsProxy;
            _assignmentProxy = assignmentProxy;
            _logger = logger;
            _view = view;
            _view.SearchingForUsers += ViewSearchingForUsers;
            _view.Assinging += ViewAssinging;
            _userslistLazy = 
                new Lazy<IEnumerable<UserInformation>>(() => _inspectorsProxy.GetUsers(sonoCode).Where(n => !string.IsNullOrEmpty(n.Name) && !string.IsNullOrEmpty(n.EmployeeNum)));
            _assignmentProxy.Assigned += AssignmentProxyAssigned;
            _assignmentProxy.Completed += AssignmentProxyCompleted;
            _assignmentProxy.Failed += AssignmentProxyCompleted;
        }

        # endregion

        # region Отображение диалогового окна

        private readonly IDeclarationAssignmentView _view;

        public void ShowDialog(IEnumerable<DeclarationSummaryKey> declarationsToAssign)
        {
            _declarationsToAssign = declarationsToAssign;
            _view.ResetSelectedUser();
            _view.ShowDialog();
        }

        # endregion

        # region Список пользователей

        private Lazy<IEnumerable<UserInformation>> _userslistLazy;

        private readonly IInspectorsDataProxy _inspectorsProxy;

        private void ViewSearchingForUsers(object sender, EventArgs e)
        {
            _view.SetUsers(
                _userslistLazy
                    .Value
                    .Where(
                        x => 
                            string.IsNullOrWhiteSpace(_view.UserSearchText) 
                            || x.EmployeeNum.ToUpper().Contains(_view.UserSearchText.ToUpper())
                            || x.Name.ToUpper().Contains(_view.UserSearchText.ToUpper()))
                    .OrderBy(x=>x.Name)
                    .ToList());
        }

        # endregion

        # region Назначение деклараций на пользователя

        public event EventHandler Assigned;

        private IEnumerable<DeclarationSummaryKey> _declarationsToAssign;

        private int _declarationsTotalCount;

        private int _declarationsAssignedCount = 0;

        private readonly IDeclarationAssignmentProxy _assignmentProxy;

        private readonly IClientLogger _logger;

        private void AssignmentProxyAssigned(object sender, EventArgs e)
        {
            Interlocked.Add(ref _declarationsAssignedCount, 1);
            _view.ShowProgress(_declarationsAssignedCount * 100 / _declarationsTotalCount);
        }

        private void AssignmentProxyCompleted(object sender, EventArgs e)
        {
            _declarationsAssignedCount = 0;
            _view.Close();
            if (Assigned != null)
            {
                Assigned(sender, e);
            }
        }        

        private void ViewAssinging(object sender, EventArgs e)
        {
            if (_view.SelectedUser == null)
            {
                _logger.LogError(null, string.Format("Назначение пользователя. Пользователь не выбран. EventArgs = {0}", e));
                return;
            }
            _declarationsTotalCount = _declarationsToAssign.Count();
            _view.ShowProgress(0);
            _assignmentProxy.AssignAll(_declarationsToAssign.GetEnumerator(), _view.SelectedUser.Sid);
        }

        # endregion
    }
}
