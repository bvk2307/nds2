﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment
{
    public interface IDeclarationAssignmentProxy
    {
        /// <summary>
        /// Выполняет операцию назначения деклараций на пользователя
        /// </summary>
        /// <param name="declarations">Перечисление деклараций, которые необходимо назначить</param>
        /// <param name="assignToSid">СИД пользователя, на которого необходимо назначить декларации</param>
        void AssignAll(IEnumerator<DeclarationSummaryKey> declarations, string assignToSid);

        event EventHandler Assigned;

        event EventHandler Failed;

        event EventHandler Completed;
    }
}
