﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList
{
    partial class ContragentView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.tabPage = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.labelTaxPayerName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.labelPeriodValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnKppValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnKpp = new Infragistics.Win.Misc.UltraLabel();
            this.labelDeclarationLink = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.tabR8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridContragents8 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.tabR9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridContragents9 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.tabR10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridContragents10 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.tabR11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridContragents11 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.tabR12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridContragents12 = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.ulRedWords = new Infragistics.Win.Misc.UltraLabel();
            this.detailsBoxArea = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tcTaxPayer = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.groupTaxPayerData = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.progressLoad = new System.Windows.Forms.ProgressBar();
            this.ultraTabControlMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tabPage.SuspendLayout();
            this.tabR8.SuspendLayout();
            this.tabR9.SuspendLayout();
            this.tabR10.SuspendLayout();
            this.tabR11.SuspendLayout();
            this.tabR12.SuspendLayout();
            this.detailsBoxArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcTaxPayer)).BeginInit();
            this.tcTaxPayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxPayerData)).BeginInit();
            this.groupTaxPayerData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).BeginInit();
            this.ultraTabControlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage
            // 
            this.tabPage.Controls.Add(this._surIndicator);
            this.tabPage.Controls.Add(this.labelTaxPayerName);
            this.tabPage.Controls.Add(this.labelPeriodValue);
            this.tabPage.Controls.Add(this.labelPeriod);
            this.tabPage.Controls.Add(this.labelInnKppValue);
            this.tabPage.Controls.Add(this.labelInnKpp);
            this.tabPage.Controls.Add(this.labelDeclarationLink);
            this.tabPage.Location = new System.Drawing.Point(0, 0);
            this.tabPage.Name = "tabPage";
            this.tabPage.Size = new System.Drawing.Size(879, 175);
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(5, 0);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 19;
            // 
            // labelTaxPayerName
            // 
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelTaxPayerName.ActiveLinkAppearance = appearance3;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.labelTaxPayerName.Appearance = appearance1;
            this.labelTaxPayerName.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.labelTaxPayerName.Location = new System.Drawing.Point(30, -3);
            this.labelTaxPayerName.Name = "labelTaxPayerName";
            this.labelTaxPayerName.Size = new System.Drawing.Size(713, 26);
            this.labelTaxPayerName.TabIndex = 8;
            this.labelTaxPayerName.TabStop = true;
            this.labelTaxPayerName.Value = "<a href=\"#\">Василек и Ромашка</a>";
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelTaxPayerName.VisitedLinkAppearance = appearance2;
            this.labelTaxPayerName.WrapText = false;
            // 
            // labelPeriodValue
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Bottom";
            this.labelPeriodValue.Appearance = appearance7;
            this.labelPeriodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeriodValue.Location = new System.Drawing.Point(493, 32);
            this.labelPeriodValue.Name = "labelPeriodValue";
            this.labelPeriodValue.Padding = new System.Drawing.Size(2, 0);
            this.labelPeriodValue.Size = new System.Drawing.Size(87, 12);
            this.labelPeriodValue.TabIndex = 6;
            this.labelPeriodValue.Text = "сентябрь 2014";
            // 
            // labelPeriod
            // 
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Bottom";
            this.labelPeriod.Appearance = appearance8;
            this.labelPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeriod.Location = new System.Drawing.Point(372, 32);
            this.labelPeriod.Name = "labelPeriod";
            this.labelPeriod.Padding = new System.Drawing.Size(2, 0);
            this.labelPeriod.Size = new System.Drawing.Size(112, 12);
            this.labelPeriod.TabIndex = 5;
            this.labelPeriod.Text = "Отчетный период:";
            // 
            // labelInnKppValue
            // 
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Bottom";
            this.labelInnKppValue.Appearance = appearance24;
            this.labelInnKppValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInnKppValue.Location = new System.Drawing.Point(74, 32);
            this.labelInnKppValue.Name = "labelInnKppValue";
            this.labelInnKppValue.Padding = new System.Drawing.Size(2, 0);
            this.labelInnKppValue.Size = new System.Drawing.Size(184, 12);
            this.labelInnKppValue.TabIndex = 4;
            this.labelInnKppValue.Text = "1234567890 / 1234567890";
            // 
            // labelInnKpp
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Bottom";
            this.labelInnKpp.Appearance = appearance6;
            this.labelInnKpp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInnKpp.Location = new System.Drawing.Point(8, 32);
            this.labelInnKpp.Name = "labelInnKpp";
            this.labelInnKpp.Padding = new System.Drawing.Size(2, 0);
            this.labelInnKpp.Size = new System.Drawing.Size(68, 12);
            this.labelInnKpp.TabIndex = 3;
            this.labelInnKpp.Text = "ИНН/КПП:";
            // 
            // labelDeclarationLink
            // 
            appearance34.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance34.FontData.Name = "Microsoft Sans Serif";
            appearance34.FontData.SizeInPoints = 8.25F;
            appearance34.TextHAlignAsString = "Left";
            appearance34.TextVAlignAsString = "Bottom";
            this.labelDeclarationLink.Appearance = appearance34;
            this.labelDeclarationLink.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.labelDeclarationLink.Location = new System.Drawing.Point(611, 28);
            this.labelDeclarationLink.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelDeclarationLink.Name = "labelDeclarationLink";
            this.labelDeclarationLink.Padding = new System.Drawing.Size(2, 0);
            this.labelDeclarationLink.Size = new System.Drawing.Size(251, 15);
            this.labelDeclarationLink.TabIndex = 18;
            this.labelDeclarationLink.TabStop = true;
            this.labelDeclarationLink.Value = "(№ корректировки: <a href=\'#\'>0</a>)";
            this.tabR8.Controls.Add(this.gridContragents8);
            this.tabR8.Location = new System.Drawing.Point(2, 24);
            this.tabR8.Name = "tabR8";
            this.tabR8.Size = new System.Drawing.Size(881, 201);
            // 
            // gridContragents8
            // 
            this.gridContragents8.AggregatePanelVisible = true;
            this.gridContragents8.AllowFilterReset = false;
            this.gridContragents8.AllowMultiGrouping = true;
            this.gridContragents8.AllowResetSettings = false;
            this.gridContragents8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridContragents8.BackColor = System.Drawing.Color.Transparent;
            this.gridContragents8.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridContragents8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContragents8.ExportExcelCancelVisible = false;
            this.gridContragents8.ExportExcelVisible = false;
            this.gridContragents8.FilterResetVisible = false;
            this.gridContragents8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridContragents8.FooterVisible = true;
            this.gridContragents8.GridContextMenuStrip = null;
            this.gridContragents8.Location = new System.Drawing.Point(0, 0);
            this.gridContragents8.Name = "gridContragents8";
            this.gridContragents8.PanelExportExcelStateVisible = false;
            this.gridContragents8.PanelLoadingVisible = false;
            this.gridContragents8.PanelPagesVisible = true;
            this.gridContragents8.RowDoubleClicked = null;
            this.gridContragents8.Size = new System.Drawing.Size(881, 201);
            this.gridContragents8.TabIndex = 2;
            this.gridContragents8.Title = "Список контрагентов НП";
            // 
            // tabR9
            // 
            this.tabR9.Controls.Add(this.gridContragents9);
            this.tabR9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR9.Name = "tabR9";
            this.tabR9.Size = new System.Drawing.Size(881, 201);
            // 
            // gridContragents9
            // 
            this.gridContragents9.AggregatePanelVisible = true;
            this.gridContragents9.AllowFilterReset = false;
            this.gridContragents9.AllowMultiGrouping = true;
            this.gridContragents9.AllowResetSettings = false;
            this.gridContragents9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridContragents9.BackColor = System.Drawing.Color.Transparent;
            this.gridContragents9.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridContragents9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContragents9.ExportExcelCancelVisible = false;
            this.gridContragents9.ExportExcelVisible = false;
            this.gridContragents9.FilterResetVisible = false;
            this.gridContragents9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridContragents9.FooterVisible = true;
            this.gridContragents9.GridContextMenuStrip = null;
            this.gridContragents9.Location = new System.Drawing.Point(0, 0);
            this.gridContragents9.Name = "gridContragents9";
            this.gridContragents9.PanelExportExcelStateVisible = false;
            this.gridContragents9.PanelLoadingVisible = false;
            this.gridContragents9.PanelPagesVisible = true;
            this.gridContragents9.RowDoubleClicked = null;
            this.gridContragents9.Size = new System.Drawing.Size(881, 201);
            this.gridContragents9.TabIndex = 3;
            this.gridContragents9.Title = "Список контрагентов НП";
            // 
            // tabR10
            // 
            this.tabR10.Controls.Add(this.gridContragents10);
            this.tabR10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR10.Name = "tabR10";
            this.tabR10.Size = new System.Drawing.Size(881, 201);
            // 
            // gridContragents10
            // 
            this.gridContragents10.AggregatePanelVisible = true;
            this.gridContragents10.AllowFilterReset = false;
            this.gridContragents10.AllowMultiGrouping = true;
            this.gridContragents10.AllowResetSettings = false;
            this.gridContragents10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridContragents10.BackColor = System.Drawing.Color.Transparent;
            this.gridContragents10.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridContragents10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContragents10.ExportExcelCancelVisible = false;
            this.gridContragents10.ExportExcelVisible = false;
            this.gridContragents10.FilterResetVisible = false;
            this.gridContragents10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridContragents10.FooterVisible = true;
            this.gridContragents10.GridContextMenuStrip = null;
            this.gridContragents10.Location = new System.Drawing.Point(0, 0);
            this.gridContragents10.Name = "gridContragents10";
            this.gridContragents10.PanelExportExcelStateVisible = false;
            this.gridContragents10.PanelLoadingVisible = false;
            this.gridContragents10.PanelPagesVisible = true;
            this.gridContragents10.RowDoubleClicked = null;
            this.gridContragents10.Size = new System.Drawing.Size(881, 201);
            this.gridContragents10.TabIndex = 4;
            this.gridContragents10.Title = "Список контрагентов НП";
            // 
            // tabR11
            // 
            this.tabR11.Controls.Add(this.gridContragents11);
            this.tabR11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR11.Name = "tabR11";
            this.tabR11.Size = new System.Drawing.Size(881, 201);
            // 
            // gridContragents11
            // 
            this.gridContragents11.AggregatePanelVisible = true;
            this.gridContragents11.AllowFilterReset = false;
            this.gridContragents11.AllowMultiGrouping = true;
            this.gridContragents11.AllowResetSettings = false;
            this.gridContragents11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridContragents11.BackColor = System.Drawing.Color.Transparent;
            this.gridContragents11.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridContragents11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContragents11.ExportExcelCancelVisible = false;
            this.gridContragents11.ExportExcelVisible = false;
            this.gridContragents11.FilterResetVisible = false;
            this.gridContragents11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridContragents11.FooterVisible = true;
            this.gridContragents11.GridContextMenuStrip = null;
            this.gridContragents11.Location = new System.Drawing.Point(0, 0);
            this.gridContragents11.Name = "gridContragents11";
            this.gridContragents11.PanelExportExcelStateVisible = false;
            this.gridContragents11.PanelLoadingVisible = false;
            this.gridContragents11.PanelPagesVisible = true;
            this.gridContragents11.RowDoubleClicked = null;
            this.gridContragents11.Size = new System.Drawing.Size(881, 201);
            this.gridContragents11.TabIndex = 4;
            this.gridContragents11.Title = "Список контрагентов НП";
            // 
            // tabR12
            // 
            this.tabR12.Controls.Add(this.gridContragents12);
            this.tabR12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR12.Name = "tabR12";
            this.tabR12.Size = new System.Drawing.Size(881, 201);
            // 
            // gridContragents12
            // 
            this.gridContragents12.AggregatePanelVisible = true;
            this.gridContragents12.AllowFilterReset = false;
            this.gridContragents12.AllowMultiGrouping = true;
            this.gridContragents12.AllowResetSettings = false;
            this.gridContragents12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridContragents12.BackColor = System.Drawing.Color.Transparent;
            this.gridContragents12.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridContragents12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContragents12.ExportExcelCancelVisible = false;
            this.gridContragents12.ExportExcelVisible = false;
            this.gridContragents12.FilterResetVisible = false;
            this.gridContragents12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridContragents12.FooterVisible = true;
            this.gridContragents12.GridContextMenuStrip = null;
            this.gridContragents12.Location = new System.Drawing.Point(0, 0);
            this.gridContragents12.Name = "gridContragents12";
            this.gridContragents12.PanelExportExcelStateVisible = false;
            this.gridContragents12.PanelLoadingVisible = false;
            this.gridContragents12.PanelPagesVisible = true;
            this.gridContragents12.RowDoubleClicked = null;
            this.gridContragents12.Size = new System.Drawing.Size(881, 201);
            this.gridContragents12.TabIndex = 4;
            this.gridContragents12.Title = "Список контрагентов НП";
            // 
            // ulRedWords
            // 
            appearance13.FontData.BoldAsString = "True";
            appearance13.ForeColor = System.Drawing.Color.Red;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.ulRedWords.Appearance = appearance13;
            this.ulRedWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulRedWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ulRedWords.Location = new System.Drawing.Point(0, 0);
            this.ulRedWords.Name = "ulRedWords";
            this.ulRedWords.Padding = new System.Drawing.Size(2, 0);
            this.ulRedWords.Size = new System.Drawing.Size(885, 330);
            this.ulRedWords.TabIndex = 4;
            this.ulRedWords.Text = "Идет загрузка данных...";
            // 
            // detailsBoxArea
            // 
            this.detailsBoxArea.Controls.Add(this.tcTaxPayer);
            this.detailsBoxArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsBoxArea.Location = new System.Drawing.Point(3, 19);
            this.detailsBoxArea.Name = "detailsBoxArea";
            this.detailsBoxArea.Size = new System.Drawing.Size(879, 58);
            this.detailsBoxArea.TabIndex = 0;
            // 
            // tcTaxPayer
            // 
            this.tcTaxPayer.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tcTaxPayer.Controls.Add(this.tabPage);
            this.tcTaxPayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcTaxPayer.Location = new System.Drawing.Point(0, 0);
            this.tcTaxPayer.Name = "tcTaxPayer";
            this.tcTaxPayer.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tcTaxPayer.Size = new System.Drawing.Size(879, 175);
            this.tcTaxPayer.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.tcTaxPayer.TabIndex = 0;
            ultraTab7.TabPage = this.tabPage;
            ultraTab7.Text = "ULTab";
            this.tcTaxPayer.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab7});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(879, 175);
            // 
            // groupTaxPayerData
            // 
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.Opaque;
            this.groupTaxPayerData.Appearance = appearance4;
            this.groupTaxPayerData.Controls.Add(this.detailsBoxArea);
            this.groupTaxPayerData.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupTaxPayerData.ExpandedSize = new System.Drawing.Size(885, 80);
            this.groupTaxPayerData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTaxPayerData.Location = new System.Drawing.Point(0, 0);
            this.groupTaxPayerData.Name = "groupTaxPayerData";
            this.groupTaxPayerData.Size = new System.Drawing.Size(885, 80);
            this.groupTaxPayerData.TabIndex = 0;
            this.groupTaxPayerData.TabStop = false;
            this.groupTaxPayerData.Text = "Данные налогоплательщика";
            // 
            // progressLoad
            // 
            this.progressLoad.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressLoad.Location = new System.Drawing.Point(0, 307);
            this.progressLoad.Name = "progressLoad";
            this.progressLoad.Size = new System.Drawing.Size(885, 23);
            this.progressLoad.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressLoad.TabIndex = 3;
            this.progressLoad.Visible = false;
            // 
            // ultraTabControlMain
            // 
            this.ultraTabControlMain.Controls.Add(this.ultraTabSharedControlsPage2);
            this.ultraTabControlMain.Controls.Add(this.tabR8);
            this.ultraTabControlMain.Controls.Add(this.tabR9);
            this.ultraTabControlMain.Controls.Add(this.tabR10);
            this.ultraTabControlMain.Controls.Add(this.tabR11);
            this.ultraTabControlMain.Controls.Add(this.tabR12);
            this.ultraTabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControlMain.Location = new System.Drawing.Point(0, 80);
            this.ultraTabControlMain.Name = "ultraTabControlMain";
            this.ultraTabControlMain.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControlMain.Size = new System.Drawing.Size(885, 227);
            this.ultraTabControlMain.TabIndex = 5;
            ultraTab1.Key = "tabChapter8";
            ultraTab1.TabPage = this.tabR8;
            ultraTab1.Text = "Книга покупок";
            ultraTab3.Key = "tabChapter9";
            ultraTab3.TabPage = this.tabR9;
            ultraTab3.Text = "Книга продаж";
            ultraTab4.Key = "tabChapter10";
            ultraTab4.TabPage = this.tabR10;
            ultraTab4.Text = "Журнал выставленных СФ";
            ultraTab5.Key = "tabChapter11";
            ultraTab5.TabPage = this.tabR11;
            ultraTab5.Text = "Журнал полученных СФ";
            ultraTab6.Key = "tabChapter12";
            ultraTab6.TabPage = this.tabR12;
            ultraTab6.Text = "СФ (п.5. ст.173 НК РФ)";
            this.ultraTabControlMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3,
            ultraTab4,
            ultraTab5,
            ultraTab6});
            this.ultraTabControlMain.Visible = false;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(881, 201);
            // 
            // ContragentView
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.ultraTabControlMain);
            this.Controls.Add(this.progressLoad);
            this.Controls.Add(this.groupTaxPayerData);
            this.Controls.Add(this.ulRedWords);
            this.Name = "ContragentView";
            this.Size = new System.Drawing.Size(885, 330);
            this.Load += new System.EventHandler(this.View_Load);
            this.tabPage.ResumeLayout(false);
            this.tabR8.ResumeLayout(false);
            this.tabR9.ResumeLayout(false);
            this.tabR10.ResumeLayout(false);
            this.tabR11.ResumeLayout(false);
            this.tabR12.ResumeLayout(false);
            this.detailsBoxArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcTaxPayer)).EndInit();
            this.tcTaxPayer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxPayerData)).EndInit();
            this.groupTaxPayerData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).EndInit();
            this.ultraTabControlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.ContextMenuStrip GetContextMenu()
        {
            var cmiOpenTaxPayer = new System.Windows.Forms.ToolStripMenuItem();           
            cmiOpenTaxPayer.Name = "cmiOpenTaxPayer";
            cmiOpenTaxPayer.Size = new System.Drawing.Size(256, 22);
            cmiOpenTaxPayer.Text = "Открыть карточку контрагента";
            cmiOpenTaxPayer.Click += new System.EventHandler(this.TaxPayerOpenClick);
            var cmiOpenContragentParams = new System.Windows.Forms.ToolStripMenuItem();
            cmiOpenContragentParams.Name = "cmiOpenContragentParams";
            cmiOpenContragentParams.Size = new System.Drawing.Size(256, 22);
            cmiOpenContragentParams.Text = "Открыть показатели контрагента";
            cmiOpenContragentParams.Click += new System.EventHandler(this.ContragentsParamsOpenClick);
            var cmRowMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            cmRowMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            cmiOpenTaxPayer,
            cmiOpenContragentParams});
            cmRowMenu.Name = "cmRowMenu";
            cmRowMenu.Size = new System.Drawing.Size(257, 48);
            cmRowMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuOpening);
            return cmRowMenu;
        }
        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl tcTaxPayer;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.ProgressBar progressLoad;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groupTaxPayerData;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel detailsBoxArea;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPage;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelTaxPayerName;
        private Infragistics.Win.Misc.UltraLabel labelPeriodValue;
        private Infragistics.Win.Misc.UltraLabel labelPeriod;
        private Infragistics.Win.Misc.UltraLabel labelInnKppValue;
        private Infragistics.Win.Misc.UltraLabel labelInnKpp;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelDeclarationLink;
        private Controls.Sur.SurIcon _surIndicator;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR8;
        private DataGridView gridContragents8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR12;
        private DataGridView gridContragents9;
        private DataGridView gridContragents10;
        private DataGridView gridContragents11;
        private DataGridView gridContragents12;
        private Infragistics.Win.Misc.UltraLabel ulRedWords;
    }
}
