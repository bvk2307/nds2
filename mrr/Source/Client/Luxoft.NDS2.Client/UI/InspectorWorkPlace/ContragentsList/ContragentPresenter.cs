﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList
{
    public class ContragentPresenter : BasePresenter<ContragentView>
    {
        private readonly DeclarationSummary _declaration;

        public DeclarationSummary Declaration { get { return _declaration; } }

        private Dictionary<int, int> chaptersCount = new Dictionary<int, int>()
        {
            { 8, 0 },
            { 9, 0 },
            { 10, 0 },
            { 11, 0 },
            { 12, 0 }
        };

        #region .ctors

        public ContragentPresenter() { }

        public ContragentPresenter(PresentationContext context, WorkItem wi, ContragentView view, DeclarationSummary declaration)
            : base(context, wi, view)
        {
            _declaration = declaration;
        }

        #endregion

        public object GetData(int chapter, QueryConditions conditions, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            var ret = new List<ContragentSummary>();

            var blService = GetServiceProxy<IDeclarationsDataService>();

            conditions.PaginationDetails.SkipTotalAvailable = false;
            int rowsNumTmp = 0;
            if (ExecuteServiceCall(
                    () => blService.GetContragentsList( inn: Declaration.INN,
                                                        innReorganized: GetInnReorganized(Declaration),
                                                        kppEffective: Declaration.KPP_EFFECTIVE,
                                                        period: Declaration.TAX_PERIOD,
                                                        year: Declaration.FISCAL_YEAR,
                                                        typeCode: Declaration.DECL_TYPE_CODE,
                                                        chapter: chapter,
                                                        conditions: conditions,
                                                        needCount: false),
            result =>
            {
                rowsNumTmp = (int)chaptersCount[chapter];
                ret = result.Result.Rows;
            }))
            {
                totalRowsNumber = rowsNumTmp;
            }

            return ret.Select(p => new ContragentSummaryModel(p)).ToList();
        }

        private string GetInnReorganized(DeclarationSummary declaration)
        {
            return declaration.INN_CONTRACTOR == declaration.INN ? null : declaration.INN_CONTRACTOR;
        }

        public int GetChapterTotalCount(int chapter)
        {
            var conditions = new QueryConditions();
            int totalRowsNumber = 0;

            var blService = GetServiceProxy<IDeclarationsDataService>();

            conditions.PaginationDetails.SkipTotalAvailable = true;
            conditions.PaginationDetails.SkipTotalMatches = false;
            int rowsNumTmp = 0;
            if (ExecuteServiceCall(
                    () => blService.GetContragentsList(inn: Declaration.INN,
                                                        innReorganized: GetInnReorganized(Declaration),
                                                        kppEffective: Declaration.KPP_EFFECTIVE,
                                                        period: Declaration.TAX_PERIOD,
                                                        year: Declaration.FISCAL_YEAR,
                                                        typeCode: Declaration.DECL_TYPE_CODE,
                                                        chapter: chapter,
                                                        conditions: conditions,
                                                        needCount: true),
                    result =>
                    {
                        rowsNumTmp = result.Result.TotalMatches;
                    }))
            {
                totalRowsNumber = rowsNumTmp;
            }

            return totalRowsNumber;
        }

        public void StartChaptersTotalCount(int chapter)
        {
            AsyncWorker<bool> worker = new AsyncWorker<bool>();

            var asyncWorker = new AsyncWorker<bool>();
            asyncWorker.DoWork += (sender, args) =>
            {
                chaptersCount[chapter] = GetChapterTotalCount(chapter);
            };

            asyncWorker.Complete += (s, a) =>
            {
                bool isShow = (chaptersCount[chapter] > 0);
                View.ExecuteInUiThread(x =>
                {
                    View.ShowChapter(chapter, isShow);
                    View.SetVisibleTabs();
                });
            };

            asyncWorker.Failed += (s, a) =>
            {
                View.ExecuteInUiThread(x =>
                {
                    View.ShowChapter(chapter, false);
                });
            };

            asyncWorker.Start();
        }

        internal void ViewDeclarationDetails(long p)
        {
            var result = GetDeclarationCardOpener().Open(p);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }
    }
}
