﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList.Models
{
    public class ContragentSummaryModel
    {
        private readonly ContragentSummary _contragentData;

        # region Конструктор

        public ContragentSummaryModel(ContragentSummary contragentData)
        {
            _contragentData = contragentData;
        }

        # endregion

        # region Данные контрагента

        /// <summary>
        /// Признак
        /// </summary>
        public string MARK { get { return _contragentData.MARK; } }

        /// <summary>
        /// Тип документа контрагента
        /// </summary>
        public string DOC_TYPE { get { return _contragentData.DOC_TYPE; } }

        ///// <summary>
        ///// Код типа документа контрагента
        ///// </summary>
        //public string DOC_TYPE_CODE { get { return _contragentData.DOC_TYPE_CODE; } }

        /// <summary>
        /// Показатель СУР контаргента
        /// </summary>
        public int? SUR_CODE { get { return _contragentData.SUR_CODE; } }

        /// <summary>
        /// Признак "Импортная сделка"
        /// </summary>
        private bool IsImport
        {
            get
            {
                return _contragentData.IS_IMPORT.HasValue ? (bool)_contragentData.IS_IMPORT : false;
            }
        }

        /// <summary>
        /// ИНН контрагента
        /// </summary>
        public string CONTRACTOR_INN { get { return IsImport ? String.Empty : _contragentData.CONTRACTOR_INN; } }

        /// <summary>
        /// КПП контрагента
        /// </summary>
        public string CONTRACTOR_KPP { get { return IsImport ? String.Empty : _contragentData.CONTRACTOR_KPP; } }

        /// <summary>
        /// Наименование контрагента
        /// </summary>
        public string CONTRACTOR_NAME { get { return IsImport ? "Импортная сделка" : _contragentData.CONTRACTOR_NAME; } }

        /// <summary>
        /// Стоимость покупок у контрагента, включая НДС, руб.
        /// </summary>
        public decimal? CHAPTER8_AMOUNT { get { return _contragentData.CHAPTER8_AMOUNT; } }

        /// <summary>
        /// Стоимость продаж  контрагенту, включая НДС
        /// </summary>
        public decimal? CHAPTER9_AMOUNT { get { return _contragentData.CHAPTER9_AMOUNT; } }

        /// <summary>
        /// Стоимость товаров (работ, услуг), имущественных прав по выставленным СФ, руб.
        /// </summary>
        public decimal? CHAPTER10_AMOUNT { get { return _contragentData.CHAPTER10_AMOUNT; } }

        /// <summary>
        /// Стоимость товаров (работ, услуг), имущественных прав по полученным СФ, руб.
        /// </summary>
        public decimal? CHAPTER11_AMOUNT { get { return _contragentData.CHAPTER11_AMOUNT; } }

        /// <summary>
        /// Стоимость продаж  контрагенту, включая НДС
        /// </summary>
        public decimal? CHAPTER12_AMOUNT { get { return _contragentData.CHAPTER12_AMOUNT; } }

        /// <summary>
        /// Заявленный к вычету НДС, руб.
        /// Сумма НДС, заявленная налогоплательщиком к вычету по операциям с контрагентом
        /// </summary>
        public decimal? DECLARED_TAX_AMNT { get { return _contragentData.DECLARED_TAX_AMNT; } }

        /// <summary>
        /// Исчисленная сумма НДС, руб.
        /// Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом
        /// </summary>
        public decimal? CALCULATED_TAX_AMNT_R9 { get { return _contragentData.CALCULATED_TAX_AMNT_R9; } }

        /// <summary>
        /// Исчисленная сумма НДС, руб.
        /// Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом
        /// </summary>
        public decimal? CALCULATED_TAX_AMNT_R12 { get { return _contragentData.CALCULATED_TAX_AMNT_R12; } }

        /// <summary>
        /// Сумма НДС, руб. (10 раздел)
        /// </summary>
        public decimal? CHAPTER10_AMOUNT_NDS { get { return _contragentData.CHAPTER10_AMOUNT_NDS; } }

        /// <summary>
        /// Сумма НДС, руб. (11 раздел)
        /// </summary>
        public decimal? CHAPTER11_AMOUNT_NDS { get { return _contragentData.CHAPTER11_AMOUNT_NDS; } }

        /// <summary>
        /// Заявленная к вычету НДС по всему разделу
        /// </summary>
        public decimal? DECLARED_TAX_AMNT_TOTAL { get { return _contragentData.DECLARED_TAX_AMNT_TOTAL; } }

        /// <summary>
        /// Удельный вес в общей сумме НДС, %
        /// </summary>
        public decimal? NDS_WEIGHT 
        { 
            get 
            {
                decimal? ndsWeight = null;
                if (_contragentData.DECLARED_TAX_AMNT != null &&
                    _contragentData.DECLARED_TAX_AMNT_TOTAL != null &&
                    _contragentData.DECLARED_TAX_AMNT_TOTAL > 0)
                    ndsWeight = (_contragentData.DECLARED_TAX_AMNT * 100) / _contragentData.DECLARED_TAX_AMNT_TOTAL;
                return ndsWeight; 
            } 
        }
        /// <summary>
        /// Количество операций с контрагентом-продавцом
        /// </summary>
        public int CHAPTER8_COUNT {
            get { return _contragentData.CHAPTER8_COUNT; }
        }
        /// <summary>
        /// Количество операций с контрагентом-продавцом 
        /// </summary>
        public int CHAPTER9_COUNT
        {
            get { return _contragentData.CHAPTER9_COUNT; }
        }
        /// <summary>
        /// Количество операций с контрагентом-продавцом 
        /// </summary>
        public int CHAPTER10_COUNT
        {
            get { return _contragentData.CHAPTER10_COUNT; }
        }
        /// <summary>
        /// Количество операций с контрагентом-продавцом 
        /// </summary>
        public int CHAPTER11_COUNT
        {
            get { return _contragentData.CHAPTER11_COUNT; }
        }
        /// <summary>
        /// Количество операций с контрагентом-продавцом 
        /// </summary>
        public int CHAPTER12_COUNT
        {
            get { return _contragentData.CHAPTER12_COUNT; }
        }
        /// <summary>
        /// Количество расхождений вида «Разрыв»
        /// </summary>
        public decimal? GAP_QUANTITY_8 { get { return _contragentData.GAP_QUANTITY_8; } }
        /// <summary>
        /// Количество расхождений вида «Разрыв»
        /// </summary>
        public decimal? GAP_QUANTITY_9 { get { return _contragentData.GAP_QUANTITY_9; } }
        /// <summary>
        /// Количество расхождений вида «Разрыв»
        /// </summary>
        public decimal? GAP_QUANTITY_10 { get { return _contragentData.GAP_QUANTITY_10; } }
        /// <summary>
        /// Количество расхождений вида «Разрыв»
        /// </summary>
        public decimal? GAP_QUANTITY_11 { get { return _contragentData.GAP_QUANTITY_11; } }
        /// <summary>
        /// Количество расхождений вида «Разрыв»
        /// </summary>
        public decimal? GAP_QUANTITY_12 { get { return _contragentData.GAP_QUANTITY_12; } }
        /// <summary>
        /// Сумма расхождений с видом «Разрыв»
        /// </summary>
        public decimal? GAP_AMOUNT_8 { get { return _contragentData.GAP_AMOUNT_8; } }
        /// <summary>
        /// Сумма расхождений с видом «Разрыв»
        /// </summary>
        public decimal? GAP_AMOUNT_9 { get { return _contragentData.GAP_AMOUNT_9; } }
        /// <summary>
        /// Сумма расхождений с видом «Разрыв»
        /// </summary>
        public decimal? GAP_AMOUNT_10 { get { return _contragentData.GAP_AMOUNT_10; } }
        /// <summary>
        /// Сумма расхождений с видом «Разрыв»
        /// </summary>
        public decimal? GAP_AMOUNT_11 { get { return _contragentData.GAP_AMOUNT_11; } }
        /// <summary>
        /// Сумма расхождений с видом «Разрыв»
        /// </summary>
        public decimal? GAP_AMOUNT_12 { get { return _contragentData.GAP_AMOUNT_12; } }
        /// <summary>
        /// Количество расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_QUANTITY_8 { get { return _contragentData.OTHER_QUANTITY_8; } }
        /// <summary>
        /// Количество расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_QUANTITY_9 { get { return _contragentData.OTHER_QUANTITY_9; } }
        /// <summary>
        /// Количество расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_QUANTITY_10 { get { return _contragentData.OTHER_QUANTITY_10; } }
        /// <summary>
        /// Количество расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_QUANTITY_11 { get { return _contragentData.OTHER_QUANTITY_11; } }
        /// <summary>
        /// Количество расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_QUANTITY_12 { get { return _contragentData.OTHER_QUANTITY_12; } }
        /// <summary>
        /// Сумма расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_AMOUNT_8 { get { return _contragentData.OTHER_AMOUNT_8; } }
        /// <summary>
        /// Сумма расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_AMOUNT_9 { get { return _contragentData.OTHER_AMOUNT_9; } }
        /// <summary>
        /// Сумма расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_AMOUNT_10 { get { return _contragentData.OTHER_AMOUNT_10; } }
        /// <summary>
        /// Сумма расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_AMOUNT_11 { get { return _contragentData.OTHER_AMOUNT_11; } }
        /// <summary>
        /// Сумма расхождений отличных от расхождений вида «Разрыв»
        /// </summary>
        public decimal? OTHER_AMOUNT_12 { get { return _contragentData.OTHER_AMOUNT_12; } }

        # endregion
    }
}
