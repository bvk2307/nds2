﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.ContragentsList
{
    public partial class ContragentView : BaseView
    {
        private readonly ContragentPresenter _presenter;
        private readonly IUcVisualStateService _visualStateService;
        private const string CmdPrefix = "iwpContragents";
        private UcRibbonButtonToolContext _btnOpen;
        private UcRibbonButtonToolContext _btnOpenParams;
        private const int NumberFirstChapter = 8;
        private const int CountOfChapters = 5;

        #region .ctors

        public ContragentView()
        {
            _presenter = null;

            InitializeComponent();
        }

        public ContragentView(PresentationContext context, WorkItem wi, DeclarationSummary declaration)
            : base(context, wi)
        {
            _visualStateService = wi.Services.Get<IUcVisualStateService>();
            _presenter = new ContragentPresenter(context, wi, this, declaration);
            _presenter.OnObjectCreated();

            InitializeComponent();
        }

        #endregion

        #region After load

        private void View_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(_presenter.Declaration.KPP))
            {
                labelInnKpp.Text = @"ИНН:";
                labelInnKppValue.Text = _presenter.Declaration.INN;
            }
            else
            {
                labelInnKpp.Text = @"ИНН/КПП:";
                labelInnKppValue.Text = string.Format("{0} / {1}", _presenter.Declaration.INN, _presenter.Declaration.KPP);
            }
            labelPeriodValue.Text = _presenter.Declaration.FULL_TAX_PERIOD;
            labelTaxPayerName.Value = string.Format("<a href='#'>{0}</a>", _presenter.Declaration.NAME);
            labelTaxPayerName.LinkClicked +=
                (s, args) => _presenter.ViewTaxPayerByKppEffective(_presenter.Declaration.INN, _presenter.Declaration.KPP_EFFECTIVE);

            if (_presenter.Declaration.DECL_TYPE_CODE == 0)
                labelDeclarationLink.Value = string.Format("(№ корректировки: <a href='#'>{0}</a>)",
                    _presenter.Declaration.CORRECTION_NUMBER_EFFECTIVE);
            else if (_presenter.Declaration.DECL_TYPE_CODE == 1)
                labelDeclarationLink.Value = string.Format("(№ версии: <a href='#'>{0}</a>)",
                    _presenter.Declaration.CORRECTION_NUMBER_EFFECTIVE);

            labelDeclarationLink.LinkClicked +=
                (s, args) => _presenter.ViewDeclarationDetails(_presenter.Declaration.DECLARATION_VERSION_ID);
            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = _presenter.Declaration.SUR_CODE;

            if (_presenter.Declaration.DECL_TYPE_CODE == 1)
                ultraTabControlMain.SelectedTab = ultraTabControlMain.Tabs[2]; 
            
            BindGrids();
            for (var i = 8; i < 13; ++i)
            {
                InitializeGrid(i);
                _presenter.StartChaptersTotalCount(i);
            }
            GridUpdate();
            InitializeRibbon();
            SetEnabledRibbonButtons();
        }

        #endregion

        #region Ribbon

        private void SetEnabledRibbonButtons()
        {
        }

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var groupFuncs = tabNavigator.AddGroup("NDS2ContragentsListManage");
            groupFuncs.Text = "Функции";
            groupFuncs.Visible = true;

            var btnUpdate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Update", CmdPrefix + "UpdateView")
            {
                Text = "Обновить",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Open", CmdPrefix + "OpenTaxPayer")
            {
                Text = "Карточка контрагента",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = false
            };

            _btnOpenParams = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "OpenParams", CmdPrefix + "OpenParams")
            {
                Text = "Показатели контрагента",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            groupFuncs.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpen.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpenParams.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnUpdate,
                _btnOpen,
                _btnOpenParams,
                tabNavigator
            });
            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
            _visualStateService.DoRefreshVisualEnvironment(_presentationContext.Id);
        }

        [CommandHandler(CmdPrefix + "UpdateView")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            GridUpdate();
        }

        [CommandHandler(CmdPrefix + "OpenTaxPayer")]
        public void TaxPayerOpenClick(object sender, EventArgs e)
        {
            var contragent = GetCurrentGrid().GetCurrentItem<ContragentSummaryModel>();

            if (contragent != null)
                _presenter.ViewTaxPayerByKppOriginal(contragent.CONTRACTOR_INN, contragent.CONTRACTOR_KPP);
        }

        [CommandHandler(CmdPrefix + "OpenParams")]
        public void ContragentsParamsOpenClick(object sender, EventArgs e)
        {
            var contragent = GetCurrentGrid().GetCurrentItem<ContragentSummaryModel>();

            if (contragent != null)
                _presenter.ViewContragentParams(_presenter.Declaration, contragent.CONTRACTOR_INN, ultraTabControlMain.SelectedTab.Index + 8);
        }

        #endregion

        #region Grid

        #region Grid tech

        private readonly Dictionary<int, DataGridPresenter<ContragentSummaryModel>> _gridPresenters = new Dictionary<int, DataGridPresenter<ContragentSummaryModel>>();
        private readonly Dictionary<int, DataGridView> _grids = new Dictionary<int, DataGridView>();

        private void BindGrids()
        {
            _grids[8] = gridContragents8;
            _grids[9] = gridContragents9;
            _grids[10] = gridContragents10;
            _grids[11] = gridContragents11;
            _grids[12] = gridContragents12;
        }

        #endregion

        private DataGridView GetCurrentGrid()
        {
            return _grids[ultraTabControlMain.SelectedTab.Index + 8];
        }

        private GridColumnSetup SetupGrid(int chapter)
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<ContragentSummaryModel>(_grids[chapter]);

            setup.Columns.Add(helper.CreateTextColumn(x => x.DOC_TYPE, 3)
                .Configure(d => { d.Caption = "Тип"; d.HeaderToolTip = "Тип документа контрагента"; }));

            var group = helper.CreateGroup("Контрагент");
            group.Columns.Add(helper.CreateSurColumn(x => x.SUR_CODE, _presenter.Sur, 2)
                .Configure(d => { d.Caption = "СУР"; d.HeaderToolTip = "Показатель СУР контаргента"; }));
            group.Columns.Add(helper.CreateTextColumn(x => x.CONTRACTOR_INN, 2)
                .Configure(d => { d.Caption = "ИНН"; d.HeaderToolTip = "ИНН контрагента, с которым произошло сопоставление"; }));
            group.Columns.Add(helper.CreateTextColumn(x => x.CONTRACTOR_KPP, 2)
                .Configure(d => { d.Caption = "КПП"; d.HeaderToolTip = "КПП контрагента"; }));
            group.Columns.Add(helper.CreateTextColumn(x => x.CONTRACTOR_NAME, 2)
                .Configure(d => { d.Caption = "Наименование"; d.HeaderToolTip = "Наименование контрагента"; d.DisableSort = true; }));
            setup.Columns.Add(group);

            if (chapter == 8)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER8_AMOUNT, 3)
                    .Configure(d => { d.Caption = "Стоимость покупок, включая НДС, руб."; d.HeaderToolTip = "Стоимость покупок у контрагента, включая НДС, руб."; }));
            if (chapter == 9)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER9_AMOUNT, 3)
                    .Configure(d => { d.Caption = "Стоимость продаж, включая НДС, руб."; d.HeaderToolTip = "Стоимость продаж  контрагенту, включая НДС"; }));
            if (chapter == 10)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER10_AMOUNT, 3)
                    .Configure(d => { d.Caption = "Стоимость по выставленным СФ, руб."; d.HeaderToolTip = "Стоимость товаров (работ, услуг), имущественных прав по выставленным СФ, руб."; }));
            if (chapter == 11)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER11_AMOUNT, 3)
                    .Configure(d => { d.Caption = "Стоимость по полученным СФ, руб."; d.HeaderToolTip = "Стоимость товаров (работ, услуг), имущественных прав по полученным СФ, руб."; }));
            if (chapter == 12)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER12_AMOUNT, 3)
                    .Configure(d => { d.Caption = "Стоимость продаж, включая НДС, руб."; d.HeaderToolTip = "Стоимость продаж  контрагенту, включая НДС"; }));

            if (chapter == 8)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.DECLARED_TAX_AMNT,3)
                    .Configure(d => { d.Caption = "Заявленный к вычету НДС, руб."; d.HeaderToolTip = "Сумма НДС, заявленная налогоплательщиком к вычету по операциям с контрагентом"; }));
            if (chapter == 9)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CALCULATED_TAX_AMNT_R9, 3)
                    .Configure(d => { d.Caption = "Исчисленная сумма НДС, руб."; d.HeaderToolTip = "Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом"; }));
            if (chapter == 10)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER10_AMOUNT_NDS, 3)
                    .Configure(d => { d.Caption = "Сумма НДС, руб."; d.HeaderToolTip = "Сумма НДС, руб."; }));
            if (chapter == 11)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER11_AMOUNT_NDS, 3)
                    .Configure(d => { d.Caption = "Сумма НДС, руб."; d.HeaderToolTip = "Сумма НДС, руб."; }));
            if (chapter == 12)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CALCULATED_TAX_AMNT_R12, 3)
                    .Configure(d => { d.Caption = "Исчисленная сумма НДС, руб."; d.HeaderToolTip = "Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом"; }));

            if (chapter == 8)
                setup.Columns.Add(helper.CreateNumberColumn(x => x.NDS_WEIGHT, 3)
                    .Configure(d => { d.Caption = "Удельный вес"; d.HeaderToolTip = "Удельный вес в общей сумме НДС, %"; }));

            var zeroValueFormatter = new IntFormatValueZeroFormatter();
            if (chapter == 8)
            {
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER8_COUNT, 3)
                    .Configure(d => { d.Caption = "Кол-во операций"; d.HeaderToolTip = "Количество операций с контрагентом"; }));

            group = helper.CreateGroup("Информация об открытых  расхождениях для КНП");
            var sub = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_QUANTITY_8, zeroValueFormatter)
                .Configure(d => { d.Caption = "Кол-во разрывов"; d.HeaderToolTip = "Количество расхождений вида «Разрыв»"; })
                .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_AMOUNT_8)
                .Configure(d => { d.Caption = "Сумма разрывов, руб."; d.HeaderToolTip = "Сумма расхождений с видом «Разрыв»"; })
                .MakeHidden());
            group.Columns.Add(sub);
            sub = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_QUANTITY_8, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во расхождений"; d.HeaderToolTip = "Количество расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_AMOUNT_8)
                    .Configure(d => { d.Caption = "Сумма расхождений, руб."; d.HeaderToolTip = "Сумма расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
            }
            else if (chapter == 9)
            {
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER9_COUNT, 3)
                        .Configure(d => { d.Caption = "Кол-во операций"; d.HeaderToolTip = "Количество операций с контрагентом-продавцом"; }));

                group = helper.CreateGroup("Информация об открытых  расхождениях для КНП");
                var sub = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_QUANTITY_9, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во разрывов"; d.HeaderToolTip = "Количество расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_AMOUNT_9)
                    .Configure(d => { d.Caption = "Сумма разрывов, руб."; d.HeaderToolTip = "Сумма расхождений с видом «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
                sub = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_QUANTITY_9, zeroValueFormatter)
                .Configure(d => { d.Caption = "Кол-во расхождений"; d.HeaderToolTip = "Количество расхождений отличных от расхождений вида «Разрыв»"; })
                .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_AMOUNT_9)
                    .Configure(d => { d.Caption = "Сумма расхождений, руб."; d.HeaderToolTip = "Сумма расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
            }
            else if (chapter == 10)
            {
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER10_COUNT, 3)
                        .Configure(d => { d.Caption = "Кол-во операций"; d.HeaderToolTip = "Количество операций с контрагентом-продавцом"; }));

                group = helper.CreateGroup("Информация об открытых  расхождениях для КНП");
                var sub = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_QUANTITY_10, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во разрывов"; d.HeaderToolTip = "Количество расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_AMOUNT_10)
                    .Configure(d => { d.Caption = "Сумма разрывов, руб."; d.HeaderToolTip = "Сумма расхождений с видом «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
                sub = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_QUANTITY_10, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во расхождений"; d.HeaderToolTip = "Количество расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_AMOUNT_10)
                    .Configure(d => { d.Caption = "Сумма расхождений, руб."; d.HeaderToolTip = "Сумма расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
            }
            else if (chapter == 11)
            {
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER11_COUNT, 3)
                        .Configure(d => { d.Caption = "Кол-во операций"; d.HeaderToolTip = "Количество операций с контрагентом-продавцом"; }));

                group = helper.CreateGroup("Информация об открытых  расхождениях для КНП");
                var sub = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_QUANTITY_11, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во разрывов"; d.HeaderToolTip = "Количество расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_AMOUNT_11)
                    .Configure(d => { d.Caption = "Сумма разрывов, руб."; d.HeaderToolTip = "Сумма расхождений с видом «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
                sub = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_QUANTITY_11, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во расхождений"; d.HeaderToolTip = "Количество расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_AMOUNT_11)
                .Configure(d => { d.Caption = "Сумма расхождений, руб."; d.HeaderToolTip = "Сумма расхождений отличных от расхождений вида «Разрыв»"; })
                .MakeHidden());
            group.Columns.Add(sub);
            }
            else if (chapter == 12)
            {
                setup.Columns.Add(helper.CreateNumberColumn(x => x.CHAPTER12_COUNT, 3)
                        .Configure(d => { d.Caption = "Кол-во операций"; d.HeaderToolTip = "Количество операций с контрагентом-продавцом"; }));

                group = helper.CreateGroup("Информация об открытых  расхождениях для КНП");
                var sub = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_QUANTITY_12, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во разрывов"; d.HeaderToolTip = "Количество расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.GAP_AMOUNT_12)
                    .Configure(d => { d.Caption = "Сумма разрывов, руб."; d.HeaderToolTip = "Сумма расхождений с видом «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
                sub = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_QUANTITY_12, zeroValueFormatter)
                    .Configure(d => { d.Caption = "Кол-во расхождений"; d.HeaderToolTip = "Количество расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                sub.Columns.Add(helper.CreateNumberColumn(x => x.OTHER_AMOUNT_12)
                    .Configure(d => { d.Caption = "Сумма расхождений, руб."; d.HeaderToolTip = "Сумма расхождений отличных от расхождений вида «Разрыв»"; })
                    .MakeHidden());
                group.Columns.Add(sub);
            }
            
            setup.Columns.Add(group);

            return setup;
        }

        private void InitializeGrid(int chapter)
        {
            var pager = new PagerStateViewModel();
            var grid = _grids[chapter];
            var pageNavigator = new DataGridPageNavigator(pager);
            grid.WithPager(pageNavigator).InitColumns(SetupGrid(chapter));
            grid.RowDoubleClicked += item =>
            {
                var contragent = (ContragentSummaryModel)item;
                if (contragent != null)
                    _presenter.ViewContragentParams(_presenter.Declaration, contragent.CONTRACTOR_INN, chapter);
            };
            // TODO
            grid.SelectRow += (sender, args) => SelectRowHandler(chapter);

            QueryConditions qc;
            if (chapter == 8)
                qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<ContragentSummaryModel>.GetMemberName(x => x.CHAPTER8_AMOUNT), Order = ColumnSort.SortOrder.Desc } } };
            else if (chapter == 9)
                qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<ContragentSummaryModel>.GetMemberName(x => x.CHAPTER9_AMOUNT), Order = ColumnSort.SortOrder.Desc } } };
            else if (chapter == 10)
                qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<ContragentSummaryModel>.GetMemberName(x => x.CHAPTER10_AMOUNT), Order = ColumnSort.SortOrder.Desc } } };
            else if (chapter == 11)
                qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<ContragentSummaryModel>.GetMemberName(x => x.CHAPTER11_AMOUNT), Order = ColumnSort.SortOrder.Desc } } };
            else if (chapter == 12)
                qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<ContragentSummaryModel>.GetMemberName(x => x.CHAPTER12_AMOUNT), Order = ColumnSort.SortOrder.Desc } } };
            else
                qc = new QueryConditions { Sorting = new List<ColumnSort>() };

            var gp =
                new PagedDataGridPresenter<ContragentSummaryModel>(
                    grid,
                    pager,
                    conditions =>
                    {
                        long rows;
                        var list = (List<ContragentSummaryModel>)_presenter.GetData(chapter, conditions, out rows);
                        return new PageResult<ContragentSummaryModel>(list, (int)rows);
                    },
                    qc);
            gp.Init(_presenter.SettingsProvider(string.Format("{0}_contragents_{1}", GetType(), chapter)));
            _gridPresenters[chapter] = gp;
            ShowChapter(chapter, false, true);
        }

        void SelectRowHandler(int chapter)
        {
            var contragent = _grids[chapter].GetCurrentItem<ContragentSummaryModel>();
            #region Кнопка "Показаели контрагента"
            //decimal? gap_cnt = 0;
            //decimal? other_cnt = 0;
            //switch (chapter)
            //{
            //    case 8 :
            //        gap_cnt = contragent.GAP_QUANTITY_8;
            //        other_cnt = contragent.OTHER_QUANTITY_8;
            //        break;
            //    case 9:
            //        gap_cnt = contragent.GAP_QUANTITY_9;
            //        other_cnt = contragent.OTHER_QUANTITY_9;
            //        break;
            //    case 10:
            //        gap_cnt = contragent.GAP_QUANTITY_10;
            //        other_cnt = contragent.OTHER_QUANTITY_10;
            //        break;
            //    case 11:
            //        gap_cnt = contragent.GAP_QUANTITY_11;
            //        other_cnt = contragent.OTHER_QUANTITY_11;
            //        break;
            //    case 12:
            //        gap_cnt = contragent.GAP_QUANTITY_12;
            //        other_cnt = contragent.OTHER_QUANTITY_12;
            //        break;
            //}
            #endregion
            if (contragent != null)
            {
                _btnOpen.Enabled = true;
                _btnOpenParams.Enabled = true;
            }
            else
            {
                _btnOpen.Enabled = false;
                _btnOpenParams.Enabled = false;
            }
            _presenter.RefreshEKP();
        }

        public void ShowChapter(int chapter, bool isShow, bool isLoading = false)
        {
            var grid = _grids[chapter];
            var tab = ultraTabControlMain.Tabs[chapter - NumberFirstChapter];
            if (isShow)
            {
                tab.Visible = true;
                grid.Visible = true;
            }
            else
            {
                tab.Visible = false;
                grid.Visible = false;
            }
        }

        public void GridUpdate()
        {
            foreach (var gp in _gridPresenters.Values)
                gp.Load();
        }

        private void ContextMenuOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var contragent = GetCurrentGrid().GetCurrentItem<ContragentSummaryModel>();
            #region Пункт меню "Открыть показатели контрагента"
            //decimal? gap_cnt = 0;
            //decimal? other_cnt = 0;
            //switch (GetCurrentChapter())
            //{
            //    case 8:
            //        gap_cnt = contragent.GAP_QUANTITY_8;
            //        other_cnt = contragent.OTHER_QUANTITY_8;
            //        break;
            //    case 9:
            //        gap_cnt = contragent.GAP_QUANTITY_9;
            //        other_cnt = contragent.OTHER_QUANTITY_9;
            //        break;
            //    case 10:
            //        gap_cnt = contragent.GAP_QUANTITY_10;
            //        other_cnt = contragent.OTHER_QUANTITY_10;
            //        break;
            //    case 11:
            //        gap_cnt = contragent.GAP_QUANTITY_11;
            //        other_cnt = contragent.OTHER_QUANTITY_11;
            //        break;
            //    case 12:
            //        gap_cnt = contragent.GAP_QUANTITY_12;
            //        other_cnt = contragent.OTHER_QUANTITY_12;
            //        break;
            //}
            //if (contragent == null)
            //    return;
            //cmiOpenContragentParams.Enabled = (gap_cnt == null ? 0 : gap_cnt) + (other_cnt == null ? 0 : other_cnt) > 0;
            #endregion
        }

        private int _counterOfChapers;
        public void SetVisibleTabs()
        {
            _counterOfChapers++;
            if (_counterOfChapers == CountOfChapters)
            {
                foreach (var tab in ultraTabControlMain.Tabs)
                {
                    if (tab.Visible)
                    {
                        ultraTabControlMain.Visible = true;
                        ulRedWords.Visible = false;
                        return;
                    }
                }
                ulRedWords.Text = ResourceManagerNDS2.DeclarationMessages.ContragentChapterNoData;
            }
        }

        #endregion

        public void SetRequestState(bool state)
        {
            gridContragents8.PanelLoadingVisible = state;
        }
    }
}
