﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationsList
{
    partial class InspectorDeclarationsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.cmRowMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiOpenDeclaration = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenJournal = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenTaxPayer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmiOpenTree = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiOpenContragents = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparatorPrevResetMark = new System.Windows.Forms.ToolStripSeparator();
            this.cmiResetMark = new System.Windows.Forms.ToolStripMenuItem();
            this.filterOwnage = new Luxoft.NDS2.Client.UI.InspectorWorkPlace.InternalControls.InspectorOptions();
            this.cmRowMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AggregatePanelVisible = true;
            this.grid.AllowFilterReset = false;
            this.grid.AllowMultiGrouping = true;
            this.grid.AllowResetSettings = false;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.grid.ColumnVisibilitySetupButtonVisible = true;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this.grid.ExportExcelCancelVisible = false;
            this.grid.ExportExcelHint = "Экспорт в MS Excel";
            this.grid.ExportExcelVisible = false;
            this.grid.FilterResetVisible = false;
            this.grid.FooterVisible = true;
            this.grid.GridContextMenuStrip = this.cmRowMenu;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.PanelExportExcelStateVisible = false;
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = true;
            this.grid.RowDoubleClicked = null;
            this.grid.Size = new System.Drawing.Size(655, 469);
            this.grid.TabIndex = 0;
            this.grid.Title = "";
          
            // 
            // cmRowMenu
            // 
            this.cmRowMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiOpenDeclaration,
            this.cmiOpenJournal,
            this.cmiOpenTaxPayer,
            this.toolStripSeparator1,
            this.cmiOpenTree,
            this.cmiOpenContragents,
            this.toolStripSeparatorPrevResetMark,
            this.cmiResetMark});
            this.cmRowMenu.Name = "cmRowMenu";
            this.cmRowMenu.Size = new System.Drawing.Size(291, 148);
            this.cmRowMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cmRowMenu_Opening);
            // 
            // cmiOpenDeclaration
            // 
            this.cmiOpenDeclaration.Name = "cmiOpenDeclaration";
            this.cmiOpenDeclaration.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenDeclaration.Text = "Открыть декларацию";
            this.cmiOpenDeclaration.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // cmiOpenJournal
            // 
            this.cmiOpenJournal.Name = "cmiOpenJournal";
            this.cmiOpenJournal.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenJournal.Text = "Открыть журнал";
            this.cmiOpenJournal.Click += new System.EventHandler(this.cmiOpenDeclaration_Click);
            // 
            // cmiOpenTaxPayer
            // 
            this.cmiOpenTaxPayer.Name = "cmiOpenTaxPayer";
            this.cmiOpenTaxPayer.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenTaxPayer.Text = "Открыть карточку налогоплательщика";
            this.cmiOpenTaxPayer.Click += new System.EventHandler(this.cmiOpenTaxPayer_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(287, 6);
            // 
            // cmiOpenTree
            // 
            this.cmiOpenTree.Name = "cmiOpenTree";
            this.cmiOpenTree.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenTree.Text = "Открыть дерево связей по декларации";
            this.cmiOpenTree.Click += new System.EventHandler(this.cmiOpenTree_Click);
            // 
            // cmiOpenContragents
            // 
            this.cmiOpenContragents.Name = "cmiOpenContragents";
            this.cmiOpenContragents.Size = new System.Drawing.Size(290, 22);
            this.cmiOpenContragents.Text = "Открыть список контрагентов";
            this.cmiOpenContragents.Click += new System.EventHandler(this.cmiOpenContragents_Click);
            // 
            // toolStripSeparatorPrevResetMark
            // 
            this.toolStripSeparatorPrevResetMark.Name = "toolStripSeparatorPrevResetMark";
            this.toolStripSeparatorPrevResetMark.Size = new System.Drawing.Size(287, 6);
            // 
            // cmiResetMark
            // 
            this.cmiResetMark.Name = "cmiResetMark";
            this.cmiResetMark.Size = new System.Drawing.Size(290, 22);
            this.cmiResetMark.Text = "Сбросить признак";
            this.cmiResetMark.Click += new System.EventHandler(this.cmiResetMark_Click);
            // 
            // filterOwnage
            // 
            this.filterOwnage.BackColor = System.Drawing.Color.Transparent;
            this.filterOwnage.Location = new System.Drawing.Point(3, 3);
            this.filterOwnage.Name = "filterOwnage";
            this.filterOwnage.SettingsProvider = null;
            this.filterOwnage.Size = new System.Drawing.Size(536, 28);
            this.filterOwnage.TabIndex = 1;
            // 
            // InspectorDeclarationsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.filterOwnage);
            this.Controls.Add(this.grid);
            this.Name = "InspectorDeclarationsView";
            this.Size = new System.Drawing.Size(655, 469);
            this.Load += new System.EventHandler(this.View_Load);
            this.cmRowMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V2.DataGridView grid;
        private InspectorWorkPlace.InternalControls.InspectorOptions filterOwnage;
   
        #region Context menu
        
        private System.Windows.Forms.ContextMenuStrip cmRowMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenDeclaration;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenJournal;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTaxPayer;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenTree;
        private System.Windows.Forms.ToolStripMenuItem cmiOpenContragents;

        #endregion
		
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparatorPrevResetMark;
        private System.Windows.Forms.ToolStripMenuItem cmiResetMark;
    }
}
