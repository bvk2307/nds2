﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.TransformQueryCondition;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Declarations;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Results;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationAssignment;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationsList
{
    class SignificantChagesToolTipViewer : ToolTipViewerBase
    {
        public override string GetToolTip(object listObject)
        {
            var decl = listObject as DeclarationBrief;
            if (decl == null)
                return null;
            var list = new List<string>();
            if (decl.HAS_AT)
                list.Add(ResourceManagerNDS2.InspectorDeclarations.CHANGES_AT);
            if (decl.SLIDE_AT_COUNT != 0)
                list.Add(ResourceManagerNDS2.InspectorDeclarations.CHANGES_SLIDE_AT);
            if (decl.NO_TKS_COUNT != 0)
                list.Add(ResourceManagerNDS2.InspectorDeclarations.CHANGES_NO_TKS);
            return list.Count > 0 ? string.Join("\\", list) : null;
        }
    }

    public class DeclarationsListPresenter : BasePresenter<InspectorDeclarationsView>
    {
        private IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy = null;

        private ISettingsProvider _settingsProviderPredFilter;

        private UserAccessOperation _userAccessOpearation;

        private IDeclarationAssignmentPresenter _declarationAssignmentPresenter;

        private AccessContext _accessContext;

        public DeclarationsListPresenter()
        {
        }

        public override void OnObjectCreated()
        {
            _userAccessOpearation = new UserAccessOperation(SecurityService);
            _accessContext = AccessContext();

            if (AllowOperationAssign())
            {
                var inspectorsDataProxy =
                    new InspectorsDataProxy(
                        WorkItem.GetWcfServiceProxy<IUserInformationService>(),
                        Notifier,
                        ClientLogger);

                var declarationAssignmentProxy =
                    new DeclarationAssignmentProxy(
                        WorkItem.GetWcfServiceProxy<IDeclarationsDataService>(),
                        Notifier,
                        ClientLogger);

                _declarationAssignmentPresenter =
                    new DeclarationAssignmentPresenter(
                        inspectorsDataProxy,
                        declarationAssignmentProxy,
                        new DeclarationAssignmentView(),
                        ClientLogger,
                        _accessContext[MrrOperations.Declaration.DeclarationAssignment]
                            .AvailableInspections
                            .First());
            }
        }

        private AccessContext AccessContext()
        {
            AccessContext result;

            try
            {
                if (WorkItem
                    .GetWcfServiceProxy<ISecurityService>()
                    .ExecuteNoHandling(
                        x => x.GetAccessContext(new string[] { MrrOperations.Declaration.DeclarationAssignment }),
                        out result))
                {
                    return result;
                }
            }
            catch (Exception error)
            {
                LogError(error);
            }

            return new AccessContext() { Operations = new OperationAccessContext[0] };
        }

        public PagedDataGridPresenter<DeclarationBrief> GridPresenter { get; set; }

        #region Подсказки для Признака записи

        public Dictionary<string, string> RecordMarkToolTips { get; private set; }

        public void FillRecordMarkToolTips()
        {
            var blService = GetServiceProxy<IDeclarationsDataService>();

            ExecuteServiceCall(blService.GetTipsForMarks, result => RecordMarkToolTips = result.Result);
        }

        #endregion

        private FilterQuery FilterColumn(string columnName, object value)
        {
            return new FilterQuery
            {
                ColumnName = columnName,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                Filtering = new List<ColumnFilter>
                {
                    new ColumnFilter
                    {
                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                        Value = value
                    }
                }
            };
        }

        public PageResult<DeclarationBrief> GetData(QueryConditions qc)
        {
            var inspector = TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID);
            var selected = TypeHelper<DeclarationBrief>.GetMemberName(t => t.SELECTED);

            var conditions = (QueryConditions)qc.Clone();
            conditions.Filter.RemoveAll(x => x.ColumnName == inspector || x.ColumnName == selected);
            conditions.Sorting.RemoveAll(x => x.ColumnKey == selected);

            switch (View.DataShowState)
            {
                case DataShowStates.Mine:
                    conditions.Filter.Add(FilterColumn(inspector, UserSid));
                    break;
                case DataShowStates.Unassigned:
                    conditions.Filter.Add(FilterColumn(inspector, null));
                    break;
            }

            var transformerQueryConditions = TransformQueryConditionCreator.CreateTransformQueryCondition(conditions);
            transformerQueryConditions.AddFilterTaxPeriod(
                    View.SelectedTaxPeriod,
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FullTaxPeriod),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            QueryConditions conditionsTransform = transformerQueryConditions.GetResultQueryConditions();

            var blService = GetServiceProxy<IDeclarationsDataService>();

            var ret = new PageResult<DeclarationBrief>();
            ExecuteServiceCall(() => blService.SelectDeclarationsForInspector(conditionsTransform),
                result => { ret = result.Result; });

            return ret;
        }

        public PageResult<DeclarationBrief> GetDataForMonitoringPerfomance(QueryConditions qc)
        {
            var inspector = TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID);
            var selected = TypeHelper<DeclarationBrief>.GetMemberName(t => t.SELECTED);

            var conditions = (QueryConditions)qc.Clone();
            conditions.Filter.RemoveAll(x => x.ColumnName == inspector || x.ColumnName == selected);
            conditions.Sorting.RemoveAll(x => x.ColumnKey == selected);

            switch (View.DataShowState)
            {
                case DataShowStates.Mine:
                    conditions.Filter.Add(FilterColumn(inspector, UserSid));
                    break;
                case DataShowStates.Unassigned:
                    conditions.Filter.Add(FilterColumn(inspector, null));
                    break;
            }

            var transformerQueryConditions = TransformQueryConditionCreator.CreateTransformQueryCondition(conditions);
            transformerQueryConditions.AddFilterTaxPeriod(
                    View.SelectedTaxPeriod,
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FullTaxPeriod),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD),
                    TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            QueryConditions conditionsTransform = transformerQueryConditions.GetResultQueryConditions();

            var blService = GetServiceProxy<IDeclarationsDataService>();

            var ret = new PageResult<DeclarationBrief>();
            ExecuteServiceCall(() => blService.SelectDeclarationsForInspector(conditionsTransform),
                result => { ret = result.Result; });

            return ret;
        }

        private IDeclarationCardOpener _declarationCardOpener;

        public void OpenDeclarationOrJournal(DeclarationBrief decl)
        {
            if (decl == null)
            {
                View.ShowError(ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD);

                return;
            }

            _declarationCardOpener = _declarationCardOpener ?? GetDeclarationCardOpener();
            var result = _declarationCardOpener.Open(decl);

            if (!result.IsSuccess)
            {
                View.ShowError(result.Message);
            }
        }

        private bool LoadAndCheckDeclarationForContragent(DeclarationBrief decl, out DeclarationSummary declSummary)
        {
            declSummary = null;
            if (decl == null)
            {
                View.ShowError(ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD);
                return false;
            }

            string errorMessageServer = String.Empty;
            bool hasErrorInner = false;
            var lastFullLoadedCorrection = SearchLastFullLoadedCorrection(decl.INN_CONTRACTOR, decl.KPP_EFFECTIVE_CONTRACTOR, decl.TAX_PERIOD, decl.FISCAL_YEAR, decl.DECL_TYPE_CODE, out errorMessageServer, out hasErrorInner);
            if (hasErrorInner)
            {
                View.ShowError(string.Format("{0}. {1}", ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD, errorMessageServer));
                return false;
            }
            if (lastFullLoadedCorrection != null)
            {
                declSummary = lastFullLoadedCorrection;
            }
            else
            {
                string errorMessage;
                declSummary = GetDeclarationSummary(decl, out errorMessage);
                if (declSummary != null)
                    return true;

                View.ShowError(string.Format("{0}. {1}", ResourceManagerNDS2.DeclarationMessages.ERROR_OPENING_DECLARATION_CARD, errorMessage));
                return false;
            }

            return true;
        }

        public void OpenContragents(DeclarationBrief decl)
        {
            DeclarationSummary declSummary;
            if (LoadAndCheckDeclarationForContragent(decl, out declSummary) && !decl.CANCELLED)
            {
                ViewContragents(declSummary);
            }
        }

        public DeclarationSummary GetDeclarationSummary(DeclarationBrief d, out string errorMessage)
        {
            var errMes = String.Empty;
            var blService = GetServiceProxy<IDeclarationsDataService>();

            var ret = new DeclarationSummary();
            ExecuteServiceCall(
                () => blService.GetDeclaration(
                    d.INN_CONTRACTOR,
                    d.KPP_EFFECTIVE,
                    d.FISCAL_YEAR.ToString(CultureInfo.InvariantCulture),
                    d.TAX_PERIOD.ToString(CultureInfo.InvariantCulture),
                    d.DECL_TYPE_CODE),
                result => ret = result.Result,
                resultErr =>
                {
                    ret = null;
                    errMes = resultErr.Message;
                });
            errorMessage = errMes;
            return ret;
        }

        private DeclarationSummary SearchLastFullLoadedCorrection(string innContractor, string kppEffective, string period, string year, int typeCode, out string errorMessage, out bool hasError)
        {
            string errorMessageInner = String.Empty;
            bool hasErrorInner = false;
            var blService = GetServiceProxy<IDeclarationsDataService>();

            var ret = new DeclarationSummary();
            ExecuteServiceCall(
                () => blService.SearchLastFullLoadedCorrection(innContractor, kppEffective, period, year, typeCode),
                result => ret = result.Result,
                resultError =>
                {
                    ret = null;
                    hasErrorInner = true;
                    errorMessageInner = resultError.Message;
                });
            errorMessage = errorMessageInner;
            hasError = hasErrorInner;
            return ret;
        }

        public void ResetMark(DeclarationBrief decl)
        {
            ResetMarkResult resetMarkResult = null;
            var blService = GetServiceProxy<IDeclarationsDataService>();
            if (ExecuteServiceCall(() => blService.ResetMark(decl.INN_CONTRACTOR, decl.KPP_EFFECTIVE, decl.DECL_TYPE_CODE, decl.TAX_PERIOD, decl.FISCAL_YEAR),
                result => resetMarkResult = result.Result))
            {
                if (resetMarkResult.Success)
                {
                    decl.HAS_CHANGES = DeclarationHasChangesType.NoChanges;
                    View.RefreshResetMarkButton(false);
                }
                else
                {
                    View.ShowError(string.Format("{0}", ResourceManagerNDS2.HAS_NOT_RIGHTS_TO_THIS_OPERATION));
                    View.RefreshResetMarkButton(false);
                    View.GridUpdate();
                }
            }
        }

        public ISettingsProvider SettingsProviderPredFilter
        {
            get
            {
                return _settingsProviderPredFilter ??
                       (_settingsProviderPredFilter =
                           SettingsProvider(string.Format("{0}_IWP_SettingsProviderPredFilter", GetType())));
            }
        }

        public bool IsAssigned(DeclarationBrief declaration)
        {
            return string.Equals(declaration.INSPECTOR_SID, UserSid, StringComparison.CurrentCultureIgnoreCase);
        }

        public bool IsTreeRelationEligible(DeclarationBrief declaration)
        {
            if (_declarationUserPermissionsPolicy == null)
            {
                ISecurityService securityService = GetServiceProxy<ISecurityService>();
                _declarationUserPermissionsPolicy = new DeclarationUserPermissionsPolicy<InspectorDeclarationsView>(this, securityService);
            }
            return _declarationUserPermissionsPolicy.IsTreeRelationEligible(declaration);
        }

        public bool AllowOpearationOORResetMark(DeclarationBrief declaration)
        {
            return _userAccessOpearation.AllowOpearationOORResetMark(declaration.SOUN_CODE);
        }

        public bool AllowOpearationOORResetMark()
        {
            return _userAccessOpearation.AllowOpearationOORResetMark();
        }

        public bool AllowOperationTreeRelation()
        {
            return _userAccessOpearation.AllowOperationTreeRelation();
        }

        public void OpenDeclarationAssignmentDialog(IEnumerable<DeclarationBrief> list)
        {
            var declarationBriefs = list as IList<DeclarationBrief> ?? list.ToList();

            _declarationAssignmentPresenter.ShowDialog(declarationBriefs.Select(n => new DeclarationSummaryKey()
            {
                FiscalYear = Convert.ToInt32(n.FISCAL_YEAR)
               ,InnDeclarant = n.INN
               ,KppEffective = n.KPP_EFFECTIVE
               ,IsJournal = n.DECL_TYPE_CODE == 1
               ,PeriodEffective = n.PERIOD_EFFECTIVE
            }));
        }


        internal bool AllowOperationAssign()
        {
            return _accessContext.Operations.Any(x => x.Name == MrrOperations.Declaration.DeclarationAssignment);
        }
    }
}
