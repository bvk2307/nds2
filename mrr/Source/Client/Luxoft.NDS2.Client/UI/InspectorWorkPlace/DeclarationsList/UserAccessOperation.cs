﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationsList
{
    public class UserAccessOperation
    {
        public UserAccessOperation(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        private ISecurityService _securityService;

        private Dictionary<string, UserAccessPolicy> _cache = new Dictionary<string, UserAccessPolicy>();

        private UserAccessPolicy GetUserAccess(string operation)
        {
            if (_cache.ContainsKey(operation))
            {
                return _cache[operation];
            }
            else
            {
                var result = _securityService.GetUserAccess(operation);
                return result.Result;
            }
        }

        public bool AllowOperation(string operation, List<string> sonoCodes)
        {
            var userAccess = GetUserAccess(operation);

            bool ret = false;
            if (userAccess != null)
            {
                if (userAccess.FullAccess)
                    ret = true;
                else
                {
                    foreach (var item in userAccess.AllowedInspections)
                    {
                        if (sonoCodes.Contains(item))
                        {
                            ret = true;
                            break;
                        }
                    }
                }
            }
            return ret;
        }

        public bool AllowOpearationOORResetMark(string sonoCode)
        {
            return AllowOperation(Constants.SystemPermissions.Operations.OORResetMark, new List<string>() { { sonoCode } });
        }

        public bool AllowOpearationOORResetMark()
        {
            return AllowOperationWithoutRestriction(Constants.SystemPermissions.Operations.OORResetMark);
        }

        public bool AllowOperationOORTakeInWork()
        {
            return MrrOperationAllowed(MrrOperations.Declaration.DeclarationAssignment);
        }

        public bool AllowOperationTreeRelation()
        {
            return AllowOperationWithoutRestriction(Constants.SystemPermissions.Operations.DeclarationTreeRelation);
        }

        private IEnumerable<string> _operationsAllowed = new string[0];
        private IEnumerable<string> _operationsAsked = new string[0];

        private bool MrrOperationAllowed(params string[] operation)
        {
            if (!_operationsAsked.AsEnumerable().ArrayEquals(operation))
            {
                _operationsAsked = operation;
                _operationsAllowed = LoadOperations();
            }
            return _operationsAllowed.Intersect(_operationsAsked).Any();
        }

        private IEnumerable<string> LoadOperations()
        {
            try
            {
                var result =
                    _securityService.ValidateAccess(_operationsAsked.ToArray());
                return result.Status == ResultStatus.Success
                    ? result.Result
                    : new string[0];
            }
            catch
            {
                return new string[0];
            }
        }

        private bool AllowOperationWithoutRestriction(string operation)
        {
            var userAccess = GetUserAccess(operation);

            bool ret = false;
            if (userAccess != null)
            {
                if (userAccess.FullAccess)
                    ret = true;
                else
                {
                    ret = userAccess.AllowedInspections.Any();
                }
            }
            return ret;
        }
    }
}
