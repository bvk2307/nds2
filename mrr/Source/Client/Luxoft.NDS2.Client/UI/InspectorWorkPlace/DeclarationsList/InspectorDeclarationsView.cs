﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using CommonComponents.Utils;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.InspectorWorkPlace.DeclarationsList
{
    public partial class InspectorDeclarationsView : BaseView
    {
        private DeclarationsListPresenter _presenter;

        private const string CMD_PREFIX = "iwpDeclarations";
        private UcRibbonButtonToolContext _btnReportTreeRelations;
        private UcRibbonButtonToolContext _btnUpdate;
        private UcRibbonButtonToolContext _btnAssign;
        private UcRibbonButtonToolContext _btnOpen;
        private UcRibbonButtonToolContext _btnOpenTaxPayer;
        private UcRibbonButtonToolContext _btnOpenContragents;
        private UcRibbonButtonToolContext _btnOpenContragentsParams;
        private UcRibbonButtonToolContext _btnResetMark;

        public DataShowStates DataShowState
        {
            get { return filterOwnage.ShowState; }
        }

        #region .ctor and init

        [CreateNew]
        public DeclarationsListPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        public InspectorDeclarationsView(PresentationContext context)
            : base(context)
        {
            _presentationContext = context;
            InitializeComponent();
        }

        public override void OnUnload()
        {
            Dispose();
            base.OnUnload();
        }

        private void View_Load(object sender, EventArgs e)
        {
            InitializeRibbon();
            InitializeGrid();
            filterOwnage.SettingsProvider = _presenter.SettingsProviderPredFilter;
            filterOwnage.SetTaxPeriodModel(_presenter.TaxPeriodModel);
            filterOwnage.OnFilterChanged += (s,ea)=>ReloadGridData(true);
            SetVisibleRibbonButtons();
            SetEnabledRibbonButtons();

        }

        private void ReloadGridData(bool resetTotalsCache = false)
        {
            //aip show progress
            grid.PanelLoadingVisible = true;

            _presenter.GridPresenter.Rows.ForAll(row => row.SELECTED = false);
            _pageNavigator.SetSelectedCount(0);

            if(resetTotalsCache)
                _presenter.GridPresenter.ResetTotalsCache();
            GridUpdate();
        }

        public TaxPeriodBase SelectedTaxPeriod
        {
            get
            {
                return filterOwnage.SelectedTaxPeriod;
            }
        }

        #endregion

        #region Grid

        private CheckColumn _checkColumn;

        private GridColumnSetup SetupGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<DeclarationBrief>(grid);

            #region icons and tips and other dictionaries

            var loadIcons = new Dictionary<DeclarationProcessignStage, Bitmap>
            {
                {DeclarationProcessignStage.Seod, Properties.Resources.star_empty},
                {DeclarationProcessignStage.LoadedInMs, Properties.Resources.star_half},
                {DeclarationProcessignStage.ProcessedByMs, Properties.Resources.star_full}
            };
            var loadTips = new Dictionary<string, string>
            {
                {DeclarationProcessignStage.Seod.ToString(), "Данные не загружены"},
                {DeclarationProcessignStage.LoadedInMs.ToString(), "Загружена неактуальная декларация/журнал"},
                {DeclarationProcessignStage.ProcessedByMs.ToString(), "Загружена актуальная декларация/журнал"}
            };
            // Используется для картинок значений колонки "Признак значимых изменений"
            var loadRecordMarkIcons = new Dictionary<DeclarationHasChangesType, Bitmap>
            {
                {DeclarationHasChangesType.NoChanges, null},
                {DeclarationHasChangesType.Changes, Properties.Resources.exclamation}
            };
            // Используется для отображения списка предопределенных фильтров колонки "Признак значимых изменений"
            var booleanCodes = new List<BooleanCode>
            {
                new BooleanCode() {Index = 1, Code = 1, Description = "Установлен"},
                new BooleanCode() {Index = 0, Code = 0, Description = "Не установлен"}
            };
            // Испольуется для всплывающих подсказок значений колонки "Р"
            var tksTips = new Dictionary<string, string>
            {
                {false.ToString(), null},
                {true.ToString(), "Сформировано автотребование по СФ"}
            };

            var cancelledTips = new Dictionary<string, string>
            {
                {false.ToString(), null},
                {true.ToString(), "Есть аннулированная корректировка за отчетный период"}
            };

            var yesNoCodes = new List<BooleanCode>
            {
                new BooleanCode(){Index = 0, Code=1, Description="Да"},
                new BooleanCode() {Index=1, Code=0, Description="Нет"}
            };
            var correctionProcessedTips = new Dictionary<string, string>
            {
                {"Да", "Да - отображается информация об актуальных расхождениях по текущей корректировке"},
                {"Нет", "Нет - до сопоставления отображаются данные по расхождениям по предыдущей корректировке"}
            };

            #endregion

            _checkColumn = helper.CreateCheckColumn(x => x.SELECTED, 3);
            if (!_presenter.AllowOperationAssign())
                _checkColumn.MakeHidden();
            else
                filterOwnage.AllowAssign = true;

            setup.Columns.Add(_checkColumn.Configure(x =>
            {
                x.HeaderToolTip = "Флаг выбора записи";
                x.DisableMoving = true;
                x.DisableFilter = true;
                x.DisableSort = true;
            }));

            setup.Columns.Add(helper.CreateTextColumn(o => o.DECL_TYPE, 4));

            var columnExpressionDefault = ColumnExpressionCreator.CreateColumnExpressionDefault();
            var dictionaryBoolean = new DictionaryBoolean(booleanCodes);
            var optionsBuilder = new BooleanAutoCompleteOptionsBuilder(dictionaryBoolean);

            setup.Columns.Add(
                                helper.CreateBoolColumn(o => o.HAS_CANCELLED_CORRECTION, columnExpressionDefault, dictionaryBoolean, optionsBuilder, 3, 60, true).Configure(
                c =>
                {
                    c.DisableEdit = true;
                    c.ToolTipViewer = new DictionaryToolTipViewer(cancelledTips);
                })
                );

            var group = helper.CreateGroup("Признак");
            var dictionaryYesNo = new DictionaryBoolean(yesNoCodes);
            var filterOptionsYesNo = new BooleanAutoCompleteOptionsBuilder(dictionaryYesNo);
            group.Columns.Add(helper.CreateImageColumn(o => o.HAS_CHANGES, this, loadRecordMarkIcons, 3, optionsBuilder).Configure(c =>
            {
                c.Caption = "";
                c.HeaderToolTip = "Признак значимых изменений";
                c.ToolTipViewer = new SignificantChagesToolTipViewer();
                c.Width = 40;
            }));

            group.Columns.Add(helper.CreateBoolColumn(o => o.HAS_AT, columnExpressionDefault, dictionaryBoolean, optionsBuilder, 3, 60, true).Configure(
                c =>
                {
                    c.DisableEdit = true;
                    c.ToolTipViewer = new DictionaryToolTipViewer(tksTips);
                }));
            group.Columns.Add(helper.CreateTextColumn(o => o.SLIDE_AT_COUNT, 3, 60).Configure(c => c.CellTextAlign = HorizontalAlign.Right));
            group.Columns.Add(helper.CreateTextColumn(o => o.NO_TKS_COUNT, 3, 60).Configure(c => c.CellTextAlign = HorizontalAlign.Right));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Статус");
            group.Columns.Add(helper.CreateTextColumn(o => o.STATUS, 3));
            group.Columns.Add(helper.CreateTextColumn(o => o.STATUS_KNP, 3));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Налогоплательщик");
            group.Columns.Add(helper.CreateSurColumn(o => o.SUR_CODE, _presenter.Sur, 3));
            group.Columns.Add(helper.CreateTextColumn(o => o.INN, 3));
            group.Columns.Add(helper.CreateTextColumn(o => o.KPP, 3).Configure(c => c.BlankValue = "-"));
            group.Columns.Add(helper.CreateTextColumn(o => o.NAME, 3).Configure(c => c.DisableSort = true));
            setup.Columns.Add(group);

            setup.Columns.Add(helper.CreateNumberColumn(data => data.SEOD_DECL_ID, 4));

            setup.Columns.Add(helper.CreateTextColumn(o => o.FULL_TAX_PERIOD, ColumnExpressionCreator.ColumnExpressionTaxPeriod(), 4)
                .Configure(d => { d.Caption = "Отчетный период"; d.HeaderToolTip = "Период за который подается декларация/журнал"; d.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(o => o.SUBMISSION_DATE, 4).Configure(d => { d.Caption = "Дата представления"; d.HeaderToolTip = "Дата представления в НО НД/Журнала"; }));

            group = helper.CreateGroup("Корр./версия");
            group.Columns.Add(helper.CreateNumberColumn(o => o.CORRECTION_NUMBER, 3).Configure(d =>
            {
                d.Caption = "№";
                d.HeaderToolTip = "Номер корректировки декларации / версии журнала";
            }));
            group.Columns.Add(helper.CreateTextColumn(
                o => o.CORRECTION_PROCESSED,
                ColumnExpressionCreator.CreateColumnInvertedExpression(),
                filterOptionsYesNo,
                3).Configure(c =>
            {
                c.Caption = "Обработана";
                c.HeaderToolTip = "Обработана";
                c.ToolTipViewer = new DictionaryToolTipViewer(correctionProcessedTips);
            }));
            setup.Columns.Add(group);

            #region Подписант

            group = helper.CreateGroup("Лицо, подписавшее декларацию");
            group.Columns.Add(helper.CreateTextColumn(o => o.SUBSCRIBER_NAME, 3)
                .Configure(d =>
                {
                    d.Caption = "ФИО";
                    d.Width = 120;
                }));

            group.Columns.Add(helper.CreateTextColumn(o => o.PRPODP, 3)
                .Configure(d =>
                {
                    d.Caption = "Статус";
                    d.Width = 120;
                }));
            setup.Columns.Add(group);

            #endregion
            setup.Columns.Add(helper.CreateTextColumn(o => o.DECL_SIGN, 4));

            group = helper.CreateGroup("Сумма НДС, руб");
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.NDS_CHAPTER8, 3));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.NDS_CHAPTER9, 3));
            group.Columns.Add(helper.CreateNumberColumn(o => o.COMPENSATION_AMNT, 3)
                .Configure(d =>
                {
                    d.DataFormatInfo = new SumNDSDeclarationFormatter();
                    d.SortNullAsZero = true;
                    d.BlankValue = DBNull.Value;
                }));
            setup.Columns.Add(group);

            setup.Columns.Add(helper.CreateNumberColumn(o => o.NDS_WEIGHT, 4)
                .Configure(d =>
                {
                    d.DataFormatInfo = new IntFormatValueNullFormatter();
                    d.SortNullAsZero = true;
                    d.BlankValue = DBNull.Value;
                }));

            #region Книга покупок

            group = helper.CreateGroup("Книга покупок");
            //group.Columns.Add(helper.CreateTextColumn(o => o.CHAPTER8_MARK, 3));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_CONTRAGENT_CNT, 3).MakeHidden());

            var midGroup = helper.CreateGroup("Информация об открытых расхождениях для КНП");
            var subGroup = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_GAP_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_GAP_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_GAP_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);

            subGroup = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_OTHER_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_OTHER_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER8_OTHER_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);
            group.Columns.Add(midGroup);
            setup.Columns.Add(group);

            #endregion

            #region Книга продаж

            group = helper.CreateGroup("Книга продаж");

            //group.Columns.Add(helper.CreateTextColumn(o => o.CHAPTER9_MARK, 3));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_CONTRAGENT_CNT, 3).MakeHidden());

            midGroup = helper.CreateGroup("Информация об открытых расхождениях для КНП");
            subGroup = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_GAP_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_GAP_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_GAP_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);

            subGroup = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_OTHER_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_OTHER_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER9_OTHER_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);
            group.Columns.Add(midGroup);
            setup.Columns.Add(group);

            #endregion
           
            #region Журнал выставленных СФ

            group = helper.CreateGroup("Журнал выставленных СФ");
            //group.Columns.Add(helper.CreateTextColumn(o => o.CHAPTER10_MARK, 3));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_CONTRAGENT_CNT, 3).MakeHidden());

            midGroup = helper.CreateGroup("Информация об открытых расхождениях для КНП");
            subGroup = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_GAP_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_GAP_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_GAP_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);

            subGroup = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_OTHER_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_OTHER_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER10_OTHER_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);
            group.Columns.Add(midGroup);
            setup.Columns.Add(group);

            #endregion

            #region Журнал полученных СФ

            group = helper.CreateGroup("Журнал полученных СФ");
            //group.Columns.Add(helper.CreateTextColumn(o => o.CHAPTER11_MARK, 3));
            group.Columns.Add(helper.CreateNumberColumn(o => o.CHAPTER11_CONTRAGENT_CNT, 3).MakeHidden());

            midGroup = helper.CreateGroup("Информация об открытых расхождениях для КНП");
            subGroup = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_GAP_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_GAP_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_GAP_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);

            subGroup = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_OTHER_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_OTHER_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER11_OTHER_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);
            group.Columns.Add(midGroup);
            setup.Columns.Add(group);

            #endregion

            #region Сведения из СФ (п.5 ст.173 НК РФ)

            group = helper.CreateGroup("СФ (п.5 ст.173 НК РФ)");
            //group.Columns.Add(helper.CreateTextColumn(o => o.CHAPTER12_MARK, 3));
            group.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_CONTRAGENT_CNT, 3).MakeHidden());

            midGroup = helper.CreateGroup("Информация об открытых расхождениях для КНП");
            subGroup = helper.CreateGroup("Вид расхождения - РАЗРЫВ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_GAP_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_GAP_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_GAP_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);

            subGroup = helper.CreateGroup("Вид расхождения - ДРУГИЕ");
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_OTHER_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_OTHER_CONTRAGENT_CNT).MakeHidden());
            subGroup.Columns.Add(helper.CreateNumberColumnWithEmptyZero(o => o.CHAPTER12_OTHER_AMNT).MakeHidden());
            midGroup.Columns.Add(subGroup);
            group.Columns.Add(midGroup);
            setup.Columns.Add(group);

            #endregion

            var groupSono = helper.CreateGroup("Инспекция");
            groupSono.Columns.Add(helper.CreateTextColumn(o => o.SOUN_CODE, 2).Configure(column =>
            {
                column.Caption = "Код";
                column.HeaderToolTip = "Код инспекции";
                column.MinWidth = 70;
                column.Width = 70;
            }).MakeHidden());
            groupSono.Columns.Add(helper.CreateTextColumn(o => o.SOUN_NAME, 2).Configure(column =>
            {
                column.Caption = "Наименование";
                column.HeaderToolTip = "Наименование инспекции";
            }).MakeHidden());
            setup.Columns.Add(groupSono);

            var groupRegion = helper.CreateGroup("Регион");
            groupRegion.Columns.Add(helper.CreateTextColumn(o => o.REGION_CODE, 2).Configure(column =>
            {
                column.Caption = "Код";
                column.HeaderToolTip = "Код региона";
                column.MinWidth = 70;
                column.Width = 70;
            }).MakeHidden());
            groupRegion.Columns.Add(helper.CreateTextColumn(o => o.REGION_NAME, 2).Configure(column =>
            {
                column.Caption = "Наименование";
                column.HeaderToolTip = "Наименование региона";
            }).MakeHidden());
            setup.Columns.Add(groupRegion);

            setup.Columns.Add(helper.CreateTextColumn(o => o.INSPECTOR, 4));

            return setup;
        }

        private ExtendedDataGridPageNavigator _pageNavigator;

        private void BeforeCellActivate(object sender, CancelableCellEventArgs e)
        {
            if (e.Cell.Column.Key == TypeHelper<DeclarationBrief>.GetMemberName(x => x.SELECTED))
            { 
                e.Cell.Activation = grid.GetCurrentItem<DeclarationBrief>().HAS_CANCELLED_CORRECTION ? Activation.Disabled : Activation.AllowEdit;  
            }
        }

        private void InitializeGrid()
        {
            _presenter.FillRecordMarkToolTips();

            var pager = new PagerStateViewModel();
            _pageNavigator = new ExtendedDataGridPageNavigator(pager);
            _pageNavigator.OnError += ShowNotification;
            grid.WithPager(_pageNavigator).InitColumns(SetupGrid());

            grid.RowDoubleClicked += x => _presenter.OpenContragents(x as DeclarationBrief);
            grid.SelectRow += grid_SelectRow;
            grid.Grid.BeforeCellActivate += BeforeCellActivate;


            var qc = new QueryConditions { Sorting = new List<ColumnSort> { new ColumnSort { ColumnKey = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_DATE), Order = ColumnSort.SortOrder.Asc } } };

            _presenter.GridPresenter =
                new PagedDataGridPresenter<DeclarationBrief>(
                    grid,
                    pager,
                    _presenter.GetData,
                    qc);
            _presenter.GridPresenter.Init(_presenter.SettingsProvider(string.Format("{0}_inspector", GetType())));
            _presenter.GridPresenter.OnAfterDataLoad += GridPresenter_OnAfterDataLoad;

            grid.SetColumnVisibility(TypeHelper<DeclarationBrief>.GetMemberName(x => x.SELECTED), _presenter.AllowOperationAssign());

            _checkColumn.RowCheckedChange += (o, b) =>
            {
                var item = o as DeclarationBrief;
                if (item != null)
                    item.SELECTED = b;
                
                 _pageNavigator.SetSelectedCount(_presenter.GridPresenter.Rows.Count(r => r.SELECTED));
                SetEnabledRibbonButtons();            
            };
            _checkColumn.HeaderCheckedChange += state =>
            {
                _presenter.GridPresenter.Rows.ForAll(row => row.SELECTED = (state == CheckState.Checked) && !row.HAS_CANCELLED_CORRECTION );
                _pageNavigator.SetSelectedCount(_presenter.GridPresenter.Rows.Count(r => r.SELECTED));
                SetEnabledRibbonButtons();
                grid.Refresh();
            };

            GridUpdate();
        }

        private void DisableEKPButtons(bool refreshEKP = false)
        {
            _btnOpen.Enabled = false;
            _btnOpenContragents.Enabled = false;
            _btnOpenContragentsParams.Enabled = false;
            if (_btnResetMark.Visible)
                _btnResetMark.Enabled = false;
            _btnOpenTaxPayer.Enabled = false;
            _btnReportTreeRelations.Enabled = false;
            _btnAssign.Enabled = false;
            if (refreshEKP)
                _presenter.RefreshEKP();
        }

        private void GridPresenter_OnAfterDataLoad(object sender, EventArgs e)
        {
            if ((sender as IPager).TotalDataRows == 0)
                DisableEKPButtons(true);
        }

        void grid_SelectRow(object sender = null, EventArgs e = null)
        {
            var declaration = grid.GetCurrentItem<DeclarationBrief>();

            if (declaration != null)
            {
                _btnOpen.Enabled = declaration.ProcessingStage != DeclarationProcessignStage.Seod;
                _btnOpenContragents.Enabled = !declaration.CANCELLED;
                _btnOpenContragentsParams.Enabled = declaration.HasDiscrepancies();
                if (_btnResetMark.Visible)
                {
                    _btnResetMark.Enabled =
                            declaration.HAS_CHANGES == DeclarationHasChangesType.Changes &&
                            _presenter.IsAssigned(declaration) &&
                            _presenter.AllowOpearationOORResetMark(declaration);
                }
                _btnOpenTaxPayer.Enabled = true;
                _btnReportTreeRelations.Enabled = declaration.TypeCode() == DeclarationTypeCode.Declaration;
                _btnAssign.Enabled = _presenter.AllowOperationAssign() && GetGridSelected().Any() && !declaration.CANCELLED;
                cmiOpenContragents.Enabled = !declaration.CANCELLED;
            }
            else
                DisableEKPButtons();
            _presenter.RefreshEKP();
        }

        public void RefreshResetMarkButton(bool isEnabled)
        {
            _btnResetMark.Enabled = isEnabled;
            _presenter.RefreshEKP();
        }

        #endregion

        #region Ribbon

        private void InitializeRibbon()
        {
            var resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            _btnAssign = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Assign", CMD_PREFIX + "Assign")
            {
                Text = "Назначить исполнителя",
                ToolTipText = "Назначить исполнителя",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "approval",
                SmallImageName = "approval",
                Enabled = false
            };

            _btnReportTreeRelations = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "ReportTreeRelations", CMD_PREFIX + "OpenRelationTree")
            {
                Text = "Дерево связей",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "tree",
                SmallImageName = "tree",
                Enabled = false
            };

            _btnUpdate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Update", CMD_PREFIX + "UpdateView")
            {
                Text = "Обновить",
                ToolTipText = "Обновить список деклараций",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Open", CMD_PREFIX + "OpenDoc")
            {
                Text = "Декларация/Журнал",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            _btnOpenTaxPayer = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "OpenTaxPayer", CMD_PREFIX + "OpenTaxPayer")
            {
                Text = "Карточка НП",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = false
            };

            _btnOpenContragents = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "OpenContragents", CMD_PREFIX + "OpenContragents")
            {
                Text = "Список контрагентов",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            _btnOpenContragentsParams = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "OpenContragentsParams", CMD_PREFIX + "OpenContragentParams")
            {
                Text = "Показатели контрагентов",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            _btnResetMark = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "ResetMark", CMD_PREFIX + "ResetMark")
            {
                Text = "Сбросить признак",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "edit_clear",
                SmallImageName = "edit_clear",
                Enabled = false
            };

            groupNavigation1.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(_btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnAssign.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpen.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpenTaxPayer.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnOpenContragents.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnResetMark.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnReportTreeRelations.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                _btnAssign, 
                _btnReportTreeRelations,
                _btnUpdate,
                _btnOpen,
                _btnOpenTaxPayer,
                _btnOpenContragents,
                _btnResetMark,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler(CMD_PREFIX + "UpdateView")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            //grid.Visible = true;

            //gbFilter.Expanded = false;

            GridUpdate();
        }

        [CommandHandler(CMD_PREFIX + "OpenDoc")]
        public virtual void ViewBtnClick(object sender, EventArgs e)
        {
            _presenter.OpenDeclarationOrJournal(grid.GetCurrentItem<DeclarationBrief>());
        }

        [CommandHandler(CMD_PREFIX + "OpenTaxPayer")]
        public void TaxPayerOpenClick(object sender, EventArgs e)
        {
            var d = grid.GetCurrentItem<DeclarationBrief>();

            if (d != null)
                _presenter.ViewTaxPayerByKppEffective(d.INN, d.KPP_EFFECTIVE);
        }

        [CommandHandler(CMD_PREFIX + "OpenContragents")]
        public void ContragentsOpenClick(object sender, EventArgs e)
        {
            var d = grid.GetCurrentItem<DeclarationBrief>();

            if ((d != null) && (!d.CANCELLED))
                _presenter.OpenContragents(d);
        }

        [CommandHandler(CMD_PREFIX + "ResetMark")]
        public void ResetMarkClick(object sender, EventArgs e)
        {
            var d = grid.GetCurrentItem<DeclarationBrief>();

            if (d != null)
            {
                _presenter.ResetMark(d);
                GridUpdate();
            }
        }

        [CommandHandler(CMD_PREFIX + "Assign")]
        public virtual void AssignBtnClick(object sender, EventArgs e)
        {
            var selectedDeclarationRows = GetGridSelected();

            if (selectedDeclarationRows != null)
            {
                _presenter.OpenDeclarationAssignmentDialog(selectedDeclarationRows);
                ReloadGridData();
            }
        }

        [CommandHandler(CMD_PREFIX + "OpenRelationTree")]
        public virtual void ReportTreeRelationsBtnClick(object sender, EventArgs e)
        {
            var curItem = grid.GetCurrentItem<DeclarationBrief>();

            if (curItem != null)
                _presenter.ViewPyramidReport(curItem);
        }

        public void SetEnabledRibbonButtons()
        {
            grid_SelectRow();
        }

        private void SetVisibleRibbonButtons()
        {
            _btnResetMark.Visible = _presenter.AllowOpearationOORResetMark();
            if (!_btnResetMark.Visible)
                _presentationContext.UiVisualizationCollection.Remove(_btnResetMark);

            _btnAssign.Visible = _presenter.AllowOperationAssign();
            if (!_btnAssign.Visible)
                _presentationContext.UiVisualizationCollection.Remove(_btnAssign);

            _btnReportTreeRelations.Visible = _presenter.AllowOperationTreeRelation();
            if (!_btnReportTreeRelations.Visible)
                _presentationContext.UiVisualizationCollection.Remove(_btnReportTreeRelations);

            _presenter.RefreshEKP();
        }

        #endregion

        public void GridUpdate()
        {
            _presenter.GridPresenter.Load();
            _checkColumn.RefreshHeaderState();
        }

        public List<DeclarationBrief> GetGridSelected()
        {
            return _presenter.GridPresenter.Rows.Where(x => x.SELECTED).ToList();
        }

        private void cmRowMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var declaration = grid.GetCurrentItem<DeclarationBrief>();
            if (declaration == null)
            {
                e.Cancel = true;
                return;
            }
            bool isDeclaration = declaration.TypeCode() == DeclarationTypeCode.Declaration;

            cmiOpenDeclaration.Visible = isDeclaration;
            cmiOpenJournal.Visible = !isDeclaration;
            toolStripSeparatorPrevResetMark.Visible = isDeclaration;
            cmiOpenTree.Visible = _presenter.AllowOperationTreeRelation();

            cmiOpenDeclaration.Enabled = declaration.ProcessingStage != DeclarationProcessignStage.Seod;
            cmiOpenJournal.Enabled = declaration.ProcessingStage != DeclarationProcessignStage.Seod;
            cmiOpenTree.Enabled = isDeclaration;

            bool allowResetMark = _presenter.AllowOpearationOORResetMark(declaration);
            cmiResetMark.Visible = allowResetMark;
            toolStripSeparatorPrevResetMark.Visible = allowResetMark;
            cmiResetMark.Enabled = allowResetMark && declaration.HAS_CHANGES == DeclarationHasChangesType.Changes && _presenter.IsAssigned(declaration);
        }

        private void cmiOpenDeclaration_Click(object sender, EventArgs e)
        {
            ViewBtnClick(sender, e);
        }

        private void cmiOpenTaxPayer_Click(object sender, EventArgs e)
        {
            TaxPayerOpenClick(sender, e);
        }

        private void cmiOpenContragents_Click(object sender, EventArgs e)
        {
            ContragentsOpenClick(sender, e);
        }

        private void cmiOpenTree_Click(object sender, EventArgs e)
        {
            ReportTreeRelationsBtnClick(sender, e);
        }

        private void cmiResetMark_Click(object sender, EventArgs e)
        {
            ResetMarkClick(sender, e);
        }
    }
}
