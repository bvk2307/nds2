﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Threading;

namespace Luxoft.NDS2.Client.UI
{
    /// <summary>
    /// Интерфейс обертки вызовов серверных методов
    /// </summary>
    [Obsolete]
    public interface IServiceRequestWrapper
    {
        /// <summary>
        /// Обертка серверного асинхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <param name="doOnCancel">Делегат выполянемый в случае отмены операции</param>
        /// <returns>токен для отмены операции</returns>
        /// 
        CancellationTokenSource ExecuteAsync<TResult>(
            Func<CancellationToken, TResult> callDelegate,
            Action<CancellationToken, TResult> doOnSuccess,
            Action<CancellationToken, TResult, Exception> doOnError = null,
            Action<TResult> doOnCancel = null) where TResult : IOperationResult;

        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <param name="doOnError">Делегат выполянемый в случае получения ошибки сервера</param>
        /// <returns>признак успешности исполнения</returns>
        bool Execute<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess,
            Action<TResult> doOnError) where TResult : IOperationResult, new();

        /// <summary>
        /// Обертка серверного синхронного вызова. Обрабатывает исключения общего характера.
        /// </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="doOnSuccess">Делегат, выполняемый в случае успешного вызова</param>
        /// <returns>признак успешности исполнения</returns>
        bool Execute<TResult>(
            Func<TResult> callDelegate,
            Action<TResult> doOnSuccess = null) where TResult : IOperationResult, new();

    }
}
