﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base.Ribbon;

namespace Luxoft.NDS2.Client.UI.Selections.Ribbon
{
    public static class RibbonBuildHelper
    {
        public static RibbonGroup WithRefreshSelectionList(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("RefreshSelectionList")
                        .WithText(ResourceManagerNDS2.Refresh)
                        .WithToolTip(ResourceManagerNDS2.Refresh)
                        .WithCommonResource()
                        .WithImage("update"));
        }

        public static RibbonGroup WithCreateSelection(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("CreateSelection")
                        .WithText(ResourceManagerNDS2.SelectionFilterCreateTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterCreateDescription)
                        .WithCommonResource()
                        .WithImage("window_new_32_32"));
        }

        public static RibbonGroup WithCreateTemplate(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("CreateSelectionTemplate")
                        .WithText(ResourceManagerNDS2.SelectionTemplateCreateTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionTemplateCreateDescription)
                        .WithCommonResource()
                        .WithImage("view_right_32_32")); 
        }

        public static RibbonGroup WithCopyTemplate(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("CopySelectionTemplate")
                        .WithText(ResourceManagerNDS2.SelectionTemplateCopyTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionTemplateCopyDescription)
                        .WithCommonResource()
                        .WithImage("view_top_bottom_32_32"));
        }

        public static RibbonGroup WithOpenSelection(
          this RibbonGroup group,
          PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("OpenSelection")
                        .WithText(ResourceManagerNDS2.SelectionViewTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionViewTitle)
                        .WithCommonResource()
                        .WithImage("view_text_32_32"));
        }

        public static RibbonGroup WithDeleteSelection(
         this RibbonGroup group,
         PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("DeleteSelection")
                        .WithText(ResourceManagerNDS2.Delete)
                        .WithToolTip(ResourceManagerNDS2.Delete)
                        .WithCommonResource()
                        .WithImage("window_suppressed_32_32"));
        }

        public static RibbonManager WithCreateClaim(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group.WithLastButton(
                UcRibbonToolSize.Large,
                context
                    .NewButton("CreateClaim")
                    .WithText(ResourceManagerNDS2.SelectionCreateClaimTitle)
                    .WithToolTip(ResourceManagerNDS2.SelectionCreateClaimDescription)
                    .WithCommonResource()
                    .WithImage("run_32_32"));
        }

        public static UcRibbonButtonToolContext WithCommonResource(
            this UcRibbonButtonToolContext button)
        {
            return button.WithResource(ResourceManagerNDS2.NDS2ClientResources);
        }
    }
}
