﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class DictionarySelectionStatus
    {
        private readonly List<DictionaryItem> _dataItems = new List<DictionaryItem>();

        public DictionarySelectionStatus(IEnumerable<DictionaryItem> dataItems)
        {
            _dataItems.AddRange(dataItems);
        }

        public IEnumerable<DictionaryItem> SelectionStatusList
        {
            get { return _dataItems; }
        }

        public string Description(int key)
        {
            return Find(key).Description;
        }

        public DictionaryItem Find(int key)
        {
            return _dataItems.First(item => item.Id == key);
        }
    }
}
