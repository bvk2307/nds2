﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections
{
    /// <summary>
    ///     Класс реализует загрузку списка выборок
    /// </summary>
    public class SelectionListDataLoader : ServiceProxyBase<PageResult<SelectionListItemData>>,
        IServerGridDataProvider<PageResult<SelectionListItemData>>
    {
        private readonly ISelectionService _service;

        public SelectionListDataLoader(
            ISelectionService service,
            INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        public event EventHandler<DataLoadedEventArgs<PageResult<SelectionListItemData>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _service.Search(queryConditions));
        }

        protected override void CallBack(PageResult<SelectionListItemData> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<PageResult<SelectionListItemData>>(result));
        }
    }
}