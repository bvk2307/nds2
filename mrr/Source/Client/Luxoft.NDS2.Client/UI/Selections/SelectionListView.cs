﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Controls;
using Luxoft.NDS2.Client.UI.Selections.Controls;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Client.UI.Selections.Ribbon;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public partial class SelectionListView : BaseView, ISelectionListView
    {
        private const int ColumnChooserWidth = 300;

        private const int ColumnChooserHeigth = 350;

        private readonly GridPagerView _listPager =
            new GridPagerView();

        private readonly SelectionListGridView _listsGrid = new SelectionListGridView();

        private RibbonManager _ribbonManager;

        public SelectionListView()
        {
            InitializeComponent();
        }

        public SelectionListView(PresentationContext context) : base(context)
        {
            InitializeComponent();
        }

        [CreateNew]
        public ListPresenterCreator ListPresenterCreator
        {
            set
            {
                var creator = value;
                WorkItem = value.WorkItem;
                creator.Create(this, new SelectionListViewModel());
            }
        }

        public event EventHandler<SelectionListOpenedEventArgs> Opened;
        public event EventHandler RefresSelectionListClicked;
        public event EventHandler CreateSelectionClicked;
        public event EventHandler CreateSelectionTemplateClicked;
        public event EventHandler CopySelectionTemplateClicked;
        public event EventHandler OpenSelectionClicked;
        public event EventHandler DeleteSelectionClicked;
        public event EventHandler CreateClaimClicked;
        public event EventHandler RowSelected;
        public event EventHandler RibbonButtonsInitialized;
        public event EventHandler RowDoubleClicked;


        public SelectionListGridView GetGrid()
        {
            return _listsGrid;
        }

        public IGridPagerView SelectionListPager
        {
            get
            {
                return _listPager;
            }
        }

        /// <summary>
        ///     Инициализирует грид со списко БС
        /// </summary>
        private void InitListComponent(DictionarySelectionStatus dictionarySelectionStatus)
        {
            _listsGrid.InitFilterView(dictionarySelectionStatus);
            _selectionListGridContainer
                .WithGrid(_listsGrid)
                .WithPager(_listPager)
                .WithGridResetter(new GridColumnSortResetter(_listsGrid))
                .WithGridResetter(new GridColumnFilterResetter(_listsGrid))
                .WithGridResetter(new GridColumnsVisibilityResetter(_listsGrid))
                .WithColumnChooser(new GridColumnChooserView(
                    ColumnChooserWidth,
                    ColumnChooserHeigth));

            _listsGrid.RowSelected += OnRowSelected;
            _listsGrid.RowDoubleClicked += OnRowDoubleClicked;
        }

        private void OnRowSelected(object sender, EventArgs eventArgs)
        {
            if (RowSelected != null)
                RowSelected(sender, eventArgs);
        }

        private void OnRowDoubleClicked(object sender, EventArgs eventArgs)
        {
            if (RowDoubleClicked != null)
                RowDoubleClicked(sender, eventArgs);
        }

        private void SelectionListViewLoad(object sender, EventArgs e)
        {
            if (Opened != null)
            {
                var args = new SelectionListOpenedEventArgs();
                Opened(sender, args);

                InitListComponent(args.DictionarySelectionStatus);

                InitializeListRibbon();
            }
        }

        /// <summary>
        ///     Инициализирует панель с кнопками
        /// </summary>
        private void InitializeListRibbon()
        {
            _ribbonManager = new RibbonManager(_presentationContext,
                    WorkItem,
                    "SelectionList",
                    _presentationContext.WindowTitle,
                    _presentationContext.WindowTitle)
                .WithGroup("SelectionListGroup", "Функции")
                .WithRefreshSelectionList(_presentationContext)
                .WithCreateSelection(_presentationContext)
                .WithCreateTemplate(_presentationContext)
                .WithCopyTemplate(_presentationContext)
                .WithOpenSelection(_presentationContext)
                .WithDeleteSelection(_presentationContext)
                .WithCreateClaim(_presentationContext);
            _ribbonManager.Init();
            _ribbonManager.ToggleVisibility(new[]
            {
                "RefreshSelectionList",
                "CreateSelection",
                "CreateSelectionTemplate",
                "CopySelectionTemplate",
                "OpenSelection",
                "DeleteSelection",
                "CreateClaim"
            });

            if (RibbonButtonsInitialized != null)
            {
                RibbonButtonsInitialized(this, EventArgs.Empty);
            }
        }


        /// <summary>
        ///     Нажатие на кнопку на панели "Обновить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("RefreshSelectionList")]
        public void RefreshSelectionList(object sender, EventArgs e)
        {
            if (RefresSelectionListClicked != null)
                RefresSelectionListClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Создать"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("CreateSelection")]
        public void CreateSelection(object sender, EventArgs e)
        {
            if (CreateSelectionClicked != null)
                CreateSelectionClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Создать Шаблон"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("CreateSelectionTemplate")]
        public void CreateSelectionTemplate(object sender, EventArgs e)
        {
            if (CreateSelectionTemplateClicked != null)
                CreateSelectionTemplateClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Копировать Шаблон"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("CopySelectionTemplate")]
        public void CopySelectionTemplate(object sender, EventArgs e)
        {
            if (CopySelectionTemplateClicked != null)
                CopySelectionTemplateClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Открыть"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("OpenSelection")]
        public void OpenSelection(object sender, EventArgs e)
        {
            if (OpenSelectionClicked != null)
                OpenSelectionClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Удалить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("DeleteSelection")]
        public void DeleteSelection(object sender, EventArgs e)
        {
            if (DeleteSelectionClicked != null)
                DeleteSelectionClicked(sender, e);
        }

        /// <summary>
        ///     Нажатие на кнопку на панели "Сформировать АТ"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("CreateClaim")]
        public void CreateClaim(object sender, EventArgs e)
        {
            if (CreateClaimClicked != null)
                CreateClaimClicked(sender, e);
        }


        public void ToggleMenuItemsAvailability(string[] commands, bool available)
        {
            _ribbonManager.ToggleAvailability(commands, available);
        }

        public void RefreshMenu()
        {
            Action action = () => _ribbonManager.Refresh();

            if (InvokeRequired)
                BeginInvoke(action);
            else
                action();
        }

        public void RefreshView()
        {
            RefreshSelectionList(this, EventArgs.Empty);
        }
    }
}