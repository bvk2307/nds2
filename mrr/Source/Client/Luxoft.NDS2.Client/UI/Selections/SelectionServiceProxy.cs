﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Net;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class SelectionServiceProxy : ServiceProxyBase, ISelectionServiceProxy
    {
        private readonly ISelectionService _service;
        private readonly IObjectLocker _objectLocker;
        public SelectionServiceProxy(
            ISelectionService service,
            IObjectLocker objectLocker,
            INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
            _objectLocker = objectLocker;
        }

        /// <summary>
        ///     Загружает список всех возможных статусов Выборки
        /// </summary>
        /// <returns></returns>
        public DictionarySelectionStatus TryLoadStatusDictionary()
        {
            return
                AppCache.Current.AddOrGetExisting(
                    () => new DictionarySelectionStatus(LoadSelectionStatusDictionary()),
                    ServerOperations.BP_SelStatus,
                    TimeSpan.FromHours(24));
        }

        /// <summary>
        ///     Загружает список всех возможных статусов Выборки
        /// </summary>
        /// <returns></returns>
        private IEnumerable<DictionaryItem> LoadSelectionStatusDictionary()
        {
            return Invoke(
                () => _service.StatusDictionary(),
                () => new OperationResult<List<DictionaryItem>>()).Result;
        }

        /// <summary>
        ///     Загружает список всех возможных причин исключения расхождений
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DictionaryItem> TryLoadExcludeReasonDictionary()
        {
            return
                AppCache.Current.AddOrGetExisting(
                    () => LoadExcludeReasonDictionary(),
                    ServerOperations.BP_ExclReason,
                    TimeSpan.FromHours(24));
        }

        /// <summary>
        /// Загружает словарь всех возможных причин исключения
        /// </summary>
        /// <returns></returns>
        private IEnumerable<DictionaryItem> LoadExcludeReasonDictionary()
        {
            return Invoke(
                () => _service.ExcludeReasonDictionary(),
                () => new OperationResult<List<DictionaryItem>>()).Result;
        }

        /// <summary>
        /// Устанавливает статус Удалена выборке
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public bool TryDelete(Selection selection, SelectionCommandParam param)
        {
            var result = Invoke(
               () => _service.Delete(selection, param),
               () => new OperationResult<Selection>
                {
                    Status = ResultStatus.Error
                });

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Определяет может ли данная выборка быть удалена текущим пользователем
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        public bool CanBeDeleted(Selection selection)
        {
            return Invoke(
               () => _service.CanBeDeleted(selection),
               () => new OperationResult<bool>()).Result;
        }

        /// <summary>
        /// Определяет возможность удаления шаблона в зависимости от статусов выборок в нем
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public bool CanDeleteTemplate(long templateId)
        {
            return Invoke(
               () => _service.CanDeleteTemplate(templateId),
               () => new OperationResult<bool>()).Result;
        }

        /// <summary>
        /// Возаращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserDisplayName()
        {
            return _service.GetCurrentUserDisplayName();
        }

        /// <summary>
        /// Возвращает идентификатор текущего пользователя
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserSid()
        {
            return _service.GetCurrentUserSid();
        }

        /// <summary>
        /// Устанавливает статус Удалена всем выборкам из шаблона
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public bool TryDeleteSelectionsByTemplate(long templateId)
        {
             var result =Invoke(
             () => _service.DeleteSelectionsByTemplate(templateId),
             () => new OperationResult {Status =  ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Возвращает полный список расхождений выборки по первой или второй стороне
        /// согласно условиям фильтрации и сортировки
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="firstSide"></param>
        /// <param name="qc"></param>
        /// <returns></returns>
        public List<SelectionDiscrepancy> AllDiscrepancies(long selectionId, bool firstSide, QueryConditions qc)
        {
            return Invoke(() => _service.AllDiscrepancies(selectionId, firstSide, qc), () => new OperationResult<List<SelectionDiscrepancy>>()).Result;
        }

        #region Creating Claim Functionality


        public Selection CreateClaim(Selection selection)
        {
            return Invoke(
                () => _service.CreateClaim(selection),
                () => new OperationResult<Selection>()).Result;
        }

        /// <summary>
        /// Определяет наличие выборок в статусе "Согласована"
        /// </summary>
        public bool ExistApprovedSelections()
        {
            return Invoke(() => _service.ExistApprovedSelections(), () => new OperationResult<bool>()).Result;
        }

        /// <summary>
        /// Подсчитывает количество выборок в статусе "Согласована" и "На согласовании"
        /// </summary>
        public SelectionByStatusesCount CountApprovedSelections()
        {
            return Invoke(() => _service.CountApprovedSelections(), () => new OperationResult<SelectionByStatusesCount>()).Result;
        }
        /// <summary>
        /// Переводит все выборки из статуса "Согласована" в статус "Формирование АТ"
        /// </summary>
        public void SetClaimCreatingStatus()
        {
            Invoke(() => _service.SetClaimCreatingStatus(), () => new OperationResult());
        }

        #endregion

        /// <summary>
        /// Ручное включение декларации
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит включение</param>
        /// <param name="declarationId">идентификатор декларации, кототрую включают</param>
        public void IncludeDeclaration(long selectionId, long declarationId)
        {
            Invoke(() => _service.IncludeDeclaration(selectionId, declarationId), () => new OperationResult());
        }

        /// <summary>
        /// Ручное исключение декларации
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит исключение</param>
        /// <param name="declarationId">идентификатор декларации, кототрую исключают</param>
        public void ExcludeDeclaration(long selectionId, long declarationId)
        {
            Invoke(() => _service.ExcludeDeclaration(selectionId, declarationId), () => new OperationResult());
        }

        /// <summary>
        /// Ручное включение деклараций
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит включение</param>
        /// <param name="zipList"> список идентификатор деклараций, кототрые включают</param>
        public void IncludeDeclarations(long selectionId, long[] zipList)
        {
            Invoke(() => _service.IncludeDeclarations(selectionId, zipList), () => new OperationResult());
        }

        /// <summary>
        /// Ручное исключение деклараций
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит исключение</param>
        /// <param name="zipList">список идентификаторов деклараций, кототрые исключают</param>
        public void ExcludeDeclarations(long selectionId, long[] zipList)
        {
            Invoke(() => _service.ExcludeDeclarations(selectionId, zipList), () => new OperationResult());
        }

        /// <summary>
        /// Создает новый шаблон по команде "Копировать шаблон"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SelectionDetails NewFromTemplate(long id)
        {
            return Invoke(
                () => _service.NewFromTemplate(id),
                () => new OperationResult<SelectionDetails>()).Result;
        }

        /// <summary>
        ///  Загружает выборку
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lockKey"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public SelectionDetails Load(long? id, string lockKey, SelectionType type)
        {
            return Invoke(
                () => _service.Load(id, lockKey, type),
                () => new OperationResult<SelectionDetails>()).Result;
        }

        /// <summary>
        /// Обновляет аггрегатные данные в выбокре:
        /// к-во Нд
        /// Сумма расхождений
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        public void UpdateSelectionAggregate(long selectionId, SelectionType type)
        {
            Invoke(
                () => _service.UpdateSelectionAggregate(selectionId, type),
                () => new OperationResult());
        }

        /// <summary>
        /// Обновляет данные по регионам выборки
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        public void UpdateSelectionRegions(long selectionId, string regions)
        {
            Invoke(
                () => _service.UpdateSelectionRegions(selectionId, regions),
                () => new OperationResult());
        }

        /// <summary>
        /// Возвращает информацию о к-ве расхождений в выборке
        /// Сколько всего расхождений и сколько выбрано в выборку
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<SelectionDiscrepancyStatistic> TryGetDiscrepancyStatistic(long selectionId, SelectionType type)
        {
          var  result = Invoke(
                () => _service.GetDiscrepancyStatistic(selectionId, type),
                () => new OperationResult<List<SelectionDiscrepancyStatistic>> {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success ? result.Result : null;
        }

        /// <summary>
        /// Отправляет выборку на согласовани е
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        public bool TrySendToAproval(Selection selection)
        {
            var result = Invoke(
                () => _service.SendToAproval(selection),
                () => new OperationResult<Selection> {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Согласование 
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="selIds"></param>
        /// <returns></returns>
        public bool TryApprove(Selection selection, List<long> selIds)
        {
            var result = Invoke(
                () => _service.Approve(selection, selIds),
                () => new OperationResult { Status = ResultStatus.Error });

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Отправка выборки на доработку
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="selIds"></param>
        /// <returns></returns>
        public bool TrySendBackForCorrections(Selection selection, List<long> selIds)
        {
            var result = Invoke(
                () => _service.SendBackForCorrections(selection, selIds),
                () => new OperationResult {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }
        
        /// <summary>
        /// Запрос на выборку в HIVE
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool TryRequestSelection(
            Selection selection,
            SelectionType type,
            Filter filter,
            ActionType action)
        {
            var result = Invoke(() => _service.RequestSelection(selection, type, filter, action),
                () => new OperationResult<SelectionRequestResult> {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Заспрос на создание выборок в шаблоне в Hive
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="templateName"></param>
        /// <param name="filter"></param>
        /// <param name="regionalBounds"></param>
        /// <param name="action"></param>
        public bool TryRequestSelections(
            long templateId,
            string templateName,
            Filter filter,
            List<SelectionRegionalBoundsDataItem> regionalBounds,
            ActionType action)
        {
            var result = Invoke(() => _service.RequestSelections(templateId, templateName, filter, regionalBounds, action),
                () => new OperationResult {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Обновляет статус выборки или выборок в шаблоне, после того как получен результат из Hive
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <param name="action"></param>
        public void TryUpdatetSelectionStatusAfterHiveLoad(long selectionId, SelectionType type, ActionType action)
        {
            Invoke(() => _service.UpdatetSelectionStatusAfterHiveLoad(selectionId, type, action),
                    () => new OperationResult());
        }

        /// <summary>
        /// Сохраняет выборку
        /// </summary>
        /// <param name="data"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public Selection TrySave(Selection data, ActionType action)
        {
            var result = Invoke(() => _service.Save(data, action),
                    () => new OperationResult<Selection> {Status =  ResultStatus.Error});

            return result.Status == ResultStatus.Success ? result.Result : null;
        }

        /// <summary>
        /// Сохраняет фильтр
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool TrySaveFilter(long selectionId, Filter filter)
        {
           var  result = Invoke(() => _service.SaveFilter(selectionId, filter),
                () => new OperationResult {Status = ResultStatus.Error});

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        /// Обновляет список границ отбора по регионам
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public bool TryUpdateRegionalBounds(long templateId, List<SelectionRegionalBoundsDataItem> items)
        {
            var result = Invoke(() => _service.UpdateRegionalBounds(templateId, items),
                () => new OperationResult { Status = ResultStatus.Error });

            return result.Status == ResultStatus.Success;
        }

        #region Работа с блокировками

        public bool Lock(long objectId, long objectType, ref string lockData)
        {
            bool ret = false;
            string rez = String.Empty;
            string hostName = Dns.GetHostName();

            var lockObject = Invoke(() => _objectLocker.Lock(objectId, objectType, hostName),
                    () => new OperationResult<object>())
                .Result;
            if (lockObject != null)
                rez = lockObject.ToString();

            if (!String.IsNullOrEmpty(rez))
            {
                lockData = rez;
                ret = true;
            }
            else
            { 
                lockObject = Invoke(() => _objectLocker.CheckLock(objectId, objectType), () => new OperationResult<object>());
            if (lockObject != null)
                rez = lockObject.ToString();
            }

            return ret;
        }

        public void UnLock(string lockKey)
        {
            string hostName = Dns.GetHostName();
            Invoke(() => _objectLocker.Unlock(lockKey, hostName), () => new OperationResult());
        }

        #endregion
    }
}