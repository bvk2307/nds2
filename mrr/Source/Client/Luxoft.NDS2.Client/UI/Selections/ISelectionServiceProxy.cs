﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections
{
    /// <summary>
    ///     Интерфейс реализующий работу с выборками
    /// </summary>
    public interface ISelectionServiceProxy
    {
        /// <summary>
        /// Загружает список всех возможных статусов Выборки
        /// </summary>
        /// <returns></returns>
        DictionarySelectionStatus TryLoadStatusDictionary();

        /// <summary>
        /// Загружает список всех возможных причин исключения расхождений
        /// </summary>
        /// <returns></returns>
        IEnumerable<DictionaryItem> TryLoadExcludeReasonDictionary();

        /// <summary>
        /// Устанавливает статус Удалена
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        bool TryDelete(Selection selection, SelectionCommandParam param);

        /// <summary>
        /// Устанавливает статус Удалена всем выборкам из шаблона
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        bool TryDeleteSelectionsByTemplate(long templateId);

        /// <summary>
        /// Проверяет, еслть ли у польщователя права на удаление 
        /// и не заблокирована ли выборка
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        bool CanBeDeleted(Selection selection);

        /// <summary>
        /// Определяет возможность удаления шаблона в зависимости от статусов выборок в нем
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        bool CanDeleteTemplate(long templateId);

        Selection CreateClaim(Selection selection);

        /// <summary>
        /// Возаращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetCurrentUserDisplayName();

        /// <summary>
        /// Возвращает идентификатор текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetCurrentUserSid();
        
        /// <summary>
        /// Определяет наличие выборок в статусе "Согласована"
        /// </summary>
        bool ExistApprovedSelections();

        /// <summary>
        /// Возвращает полный список расхождений выборки по первой или второй стороне
        /// согласно условиям фильтрации и сортировки
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="firstSide"></param>
        /// <param name="qc"></param>
        /// <returns></returns>
        List<SelectionDiscrepancy> AllDiscrepancies(
            long selectionId, 
            bool firstSide,
            QueryConditions qc);


        /// <summary>
        /// Подсчитывает количество выборок в статусе "Согласована" и "На согласовании"
        /// </summary>
        SelectionByStatusesCount CountApprovedSelections();

        /// <summary>
        /// Переводит все выборки из статуса "Согласована" в статус "Формирование АТ"
        /// </summary>
        void SetClaimCreatingStatus();

        /// <summary>
        /// Ручное включение декларации
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит включение</param>
        /// <param name="declarationId">идентификатор декларации, кототрую включают</param>
        void IncludeDeclaration(long selectionId, long declarationId);

        /// <summary>
        /// Ручное исключение декларации
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит исключение</param>
        /// <param name="declarationId">идентификатор декларации, кототрую исключают</param>
        void ExcludeDeclaration(long selectionId, long declarationId);

        /// <summary>
        /// Ручное включение деклараций
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит включение</param>
        /// <param name="zipList"> список идентификатор деклараций, кототрые включают</param>
        void IncludeDeclarations(long selectionId, long[] zipList);

        /// <summary>
        /// Ручное исключение деклараций
        /// </summary>
        /// <param name="selectionId">идентификатор выборки, в которой происходит исключение</param>
        /// <param name="zipList">список идентификаторов деклараций, кототрые исключают</param>
        void ExcludeDeclarations(long selectionId, long[] zipList);


        /// <summary>
        /// Создает новый шаблон по команде "Копировать шаблон"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SelectionDetails NewFromTemplate(long id);

        /// <summary>
        /// Загружает выборку
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lockKey"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        SelectionDetails Load(long? id, string lockKey, SelectionType type);

        /// <summary>
        /// Обновляет аггрегатные данные в выбокре:
        /// к-во Нд
        /// Сумма расхождений
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        void UpdateSelectionAggregate(long selectionId, SelectionType type);

        /// <summary>
        /// Обновляет данные по регионам выборки
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        void UpdateSelectionRegions(long selectionId, string regions);

        /// <summary>
        /// Возвращает информацию о к-ве расхождений в выборке
        /// Сколько всего расхождений и сколько выбрано в выборку
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
         List<SelectionDiscrepancyStatistic> TryGetDiscrepancyStatistic(long selectionId, SelectionType type);

        /// <summary>
        /// Отправка выборки на согласование
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        bool TrySendToAproval(Selection selection);

        /// <summary>
        /// Согласование 
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="selIds"></param>
        /// <returns></returns>
        bool TryApprove(Selection selection, List<long> selIds);


        /// <summary>
        /// Отправка выборки на доработку
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="selIds"></param>
        /// <returns></returns>
        bool TrySendBackForCorrections(Selection selection, List<long> selIds);

        /// <summary>
        /// Запрос на создание выборки в HIVE
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        bool TryRequestSelection(
            Selection selection,
            SelectionType type,
            Filter filter,
            ActionType action);

        /// <summary>
        /// Заспрос на создание выборок в шаблоне в Hive
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="templateName"></param>
        /// <param name="filter"></param>
        /// <param name="regionalBounds"></param>
        /// <param name="action"></param>
        bool TryRequestSelections(
            long templateId,
            string templateName,
            Filter filter,
            List<SelectionRegionalBoundsDataItem> regionalBounds,
            ActionType action);

        /// <summary>
        /// Обновляет статус выборки или выборок в шаблоне, после того как получен результат из Hive
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <param name="action"></param>
        void TryUpdatetSelectionStatusAfterHiveLoad(long selectionId, SelectionType type, ActionType action);

        /// <summary>
        /// Сохраняет выборку
        /// </summary>
        /// <param name="data"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        Selection TrySave(Selection data, ActionType action);

        /// <summary>
        /// Сохраняет фильтр
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        bool TrySaveFilter(long selectionId, Filter filter);

        /// <summary>
        /// Обновляет список границ отбора по регионам
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        bool TryUpdateRegionalBounds(long templateId, List<SelectionRegionalBoundsDataItem> items);


        bool Lock(long objectId, long objectType, ref string lockData);

        void UnLock(string lockKey);




    }
}