﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.Models
{
    public class SelectionListViewModel : ListViewModel<SelectionListItemViewModel, SelectionListItemData>
    {
        public bool ExistApprovedSelections { get; set; }

        public SelectionListViewModel()
        {
            AddSortBuilder(
                TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.CreatedAt),
                new DefaultColumnSortBuilder(
                    TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.CreatedAt)));
        }

        protected override SelectionListItemViewModel ConvertToModel(SelectionListItemData dataItem)
        {
            return new SelectionListItemViewModel(dataItem);
        }
    }
}