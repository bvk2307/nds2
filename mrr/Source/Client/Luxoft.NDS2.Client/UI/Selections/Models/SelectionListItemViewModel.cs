﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.Models
{
    public class SelectionListItemViewModel : INotifyPropertyChanged
    {
        private readonly SelectionListItemData _data;

        /// <summary>
        ///     View модель элемента выборки в таблице
        /// </summary>
        public SelectionListItemViewModel(SelectionListItemData data)
        {
            _data = data;
        }

        private bool _isSelected = true;

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                NotifyPropertyChanged(TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.IsSelected));
            }
        }

        /// <summary>
        ///     Номер выборки
        /// </summary>
        public long Id
        {
            get { return _data.Id; }
        }

        /// <summary>
        ///     Идентификатор шаблона выборки
        /// </summary>
        public long? TemplateId
        {
            get { return _data.TemplateId; }
        }

        /// <summary>
        ///     Название выборки
        /// </summary>
        public string Name
        {
            get { return _data.Name; }
        }

        /// <summary>
        ///     Тип выборки
        /// </summary>
        public string TypeName
        {
            get { return _data.TypeName; }
        }

        public SelectionType TypeCode
        {
            get { return _data.TypeCode; }
        }

        /// <summary>
        ///     Дата создания выборки
        /// </summary>
        public DateTime? CreatedAt
        {
            get { return _data.CreatedAt; }
        }

        /// <summary>
        ///     Дата изменения статуса
        /// </summary>
        public DateTime? LastStatusChangedAt
        {
            get { return _data.LastStatusChangedAt; }
        }

        /// <summary>
        ///     Статус
        /// </summary>
        public SelectionStatus? StatusId
        {
            get { return _data.StatusId; }
        }

        /// <summary>
        ///     К-во НД по выборке
        /// </summary>
        public int? InitialDeclarationQuantity
        {
            get { return _data.InitialDeclarationQuantity; }
        }

        /// <summary>
        ///     Количество отобранных расхождений по СФ
        /// </summary>
        public int? InitialDiscrepancyQuantity
        {
            get { return _data.InitialDiscrepancyQuantity; }
        }

        /// <summary>
        ///     Общая сумма расхождений, включенных в выборку
        /// </summary>
        public decimal? InitialDiscrepancyAmount
        {
            get { return _data.InitialDiscrepancyAmount; }
        }

        /// <summary>
        ///     ФИО Аналитика
        /// </summary>
        public string LastEditedBy
        {
            get { return _data.LastEditedBy; }
        }

        public string LastEditedBySid
        {
            get { return _data.LastEditedBySid; }
        }
        /// <summary>
        ///     ФИО согласующего
        /// </summary>
        public string ApprovedBy
        {
            get { return _data.ApprovedBy; }
        }

        public string ApprovedBySid
        {
            get { return _data.ApprovedBySid; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}