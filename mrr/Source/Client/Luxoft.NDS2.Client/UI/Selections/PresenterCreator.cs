﻿using CommonComponents.Communication;
using CommonComponents.Security.Authorization;
using CommonComponents.Uc.Infrastructure.Interface;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class ListPresenterCreator : Presenter<ISelectionListView>
    {
        public object Create(ISelectionListView view, SelectionListViewModel model)
        {
            var service = WorkItem.GetWcfServiceProxy<ISelectionService>();
            var notifier = WorkItem.Services.Get<INotifier>(true);
            var logger = WorkItem.Services.Get<IClientLogger>(true);
            var securityService = WorkItem.Services.Get<IClientContextService>().CreateCommunicationProxy<ISecurityService>(Constants.EndpointName);
            var objectLocker = WorkItem.Services.GetServiceProxy<IObjectLocker>();

            var r = securityService.ValidateAccess(
                new[]
                {
                    MrrOperations.Selection.Create,
                    MrrOperations.Selection.CreateTemplate,
                    MrrOperations.Selection.Edit,
                    MrrOperations.Selection.EditTemplate,
                    MrrOperations.Selection.EditTemplateBasedSelection,
                    MrrOperations.Selection.CopyTemplate,
                    MrrOperations.Selection.DeleteSelectionDraft,
                    MrrOperations.Selection.DeleteSelectionApproved,
                    MrrOperations.Selection.DeleteTemplate,
                    MrrOperations.Selection.SelectionToApprove,
                    MrrOperations.Selection.SelectionToRework,
                    MrrOperations.Selection.SelectionApprove,
                    MrrOperations.Selection.ViewSelection,
                    MrrOperations.Selection.ViewTemplate,
                    MrrOperations.Selection.SelectionCreateClaim
                });

            var operations = r.Status == ResultStatus.Success
                ? r.Result
                : new string[] {};


            var p = new SelectionListPresenter(
                view,
                model,
                new SelectionServiceProxy(service, objectLocker, notifier, logger),
                new SelectionDetailsViewer(WorkItem),
                new SelectionListDataLoader(service, notifier, logger),
                operations.Select(x => new AccessRight{Name = x, PermType = PermissionType.Operation}).ToList()
            );
            p.WithLogger(logger);
            p.WithNotifier(notifier);
            return p;
        }
    }
}