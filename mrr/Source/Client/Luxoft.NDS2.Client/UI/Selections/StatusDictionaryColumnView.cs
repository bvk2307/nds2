﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public static class StatusDictionaryColumnView
    {
        public static void Apply(UltraGridColumn column, DictionarySelectionStatus dictionarySelectionStatus)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems(dictionarySelectionStatus); 
        }

        private static ValueList FillColumnItems(DictionarySelectionStatus dictionarySelectionStatus)
        {
            var valueList = new ValueList();
            foreach (var dictionaryItem in dictionarySelectionStatus.SelectionStatusList)
            {
                valueList.ValueListItems.Add(new ValueListItem((SelectionStatus)dictionaryItem.Id, dictionaryItem.Description));
            }
            return valueList;
        }
    }
}