﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Selections
{
    /// <summary>
    ///     Интерфейс реализующий работу с избранными фильтрами
    /// </summary>
    public interface IManageFavoritesServiceProxy
    {
        /// <summary>
        /// Сохраняет филтр в список избранного
        /// </summary>
        /// <param name="name"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        OperationResult<KeyValuePair<long, string>> CreateFavorite(string name, Filter filter);

        /// <summary>
        /// Перписывает фильтр
        /// </summary>
        /// <param name="name"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        bool TryOverwriteFavorite(string name, Filter filter);

        /// <summary>
        /// Удаляет фильтр из избранного
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool TryDeleteFavorite(long id);

        /// <summary>
        /// Получает список избранных фильтрров
        /// </summary>
        /// <returns></returns>
        OperationResult<List<KeyValuePair<long, string>>> TryGetFavoriteFilters();

        OperationResult<Filter> TryGetFavoriteSelectionFilterVersionTwo(long favoriteId);




    }
}