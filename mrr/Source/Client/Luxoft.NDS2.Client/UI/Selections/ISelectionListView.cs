﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.Controls;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public interface ISelectionListView
    {
        IGridPagerView SelectionListPager { get; }

        event EventHandler<SelectionListOpenedEventArgs> Opened;
        event EventHandler RefresSelectionListClicked;
        event EventHandler CreateSelectionClicked;
        event EventHandler CreateSelectionTemplateClicked;
        event EventHandler CopySelectionTemplateClicked;
        event EventHandler OpenSelectionClicked;
        event EventHandler DeleteSelectionClicked;
        event EventHandler CreateClaimClicked;
        event EventHandler RowSelected;
        event EventHandler RibbonButtonsInitialized;
        event EventHandler RowDoubleClicked;

        /// <summary>
        ///     Возвращает View
        /// </summary>
        /// <returns></returns>
        SelectionListGridView GetGrid();

        DialogResult ShowQuestion(string caption, string message);

        void ShowInfo(string caption, string message);

        void ShowWarning(string message);

        void ToggleMenuItemsAvailability(string[] commands, bool available);

        void RefreshMenu();

        void RefreshView();
    }
}