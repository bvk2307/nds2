﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class SelectionListOpenedEventArgs : EventArgs
    {
        public DictionarySelectionStatus DictionarySelectionStatus { get; set; }
    }
}
