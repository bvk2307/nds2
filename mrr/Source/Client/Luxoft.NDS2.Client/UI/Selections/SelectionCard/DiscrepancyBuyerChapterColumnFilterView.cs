﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancyBuyerChapterColumnFilterView : GridColumnFilterView
    {
        public DiscrepancyBuyerChapterColumnFilterView(UltraGridColumn column)
            : base(column)
        {
       }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }
            if (DiscrepancyBuyerChapterColumnView.BuyerChapterDictionary.ContainsKey(BuyerChapter.Unknown))
                DiscrepancyBuyerChapterColumnView.BuyerChapterDictionary.Remove(BuyerChapter.Unknown);

            foreach (var dictionaryItem in DiscrepancyBuyerChapterColumnView.BuyerChapterDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }
    public static class DiscrepancyBuyerChapterColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<BuyerChapter, string> BuyerChapterDictionary =
            new Dictionary<BuyerChapter, string>
            {
                {BuyerChapter.PurchaseBook, "Книга покупок"},
                {BuyerChapter.InvoiceJournalOut, "Журнал выставленных СФ"},
                {BuyerChapter.InvoiceJournalIn, "Журнал полученных СФ"},
                {BuyerChapter.Unknown, "Неизвестен" }
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();

            if (!BuyerChapterDictionary.ContainsKey(BuyerChapter.Unknown))
                BuyerChapterDictionary.Add(BuyerChapter.Unknown, "Неизвестен");

            foreach (var dictionaryItem in BuyerChapterDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
