﻿using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageHistory;
            Infragistics.Win.Misc.UltraLabel ulSelectionTypeCaption;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ulSelectionNumberCaption;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.selectionTabs = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.filterTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._tablePanel = new System.Windows.Forms.TableLayoutPanel();
            this._manualFilterEditArea = new System.Windows.Forms.Panel();
            this._filterAddGroup = new Infragistics.Win.Misc.UltraButton();
            this.declarationTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.discrepancyTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.discrepancySecondPartTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._regionalBounds = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._selectionListTab = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._expandableCommonInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.upHeader = new Infragistics.Win.Misc.UltraPanel();
            this.ulSelectionType = new Infragistics.Win.Misc.UltraLabel();
            this._ulSelectionStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            this._ulSelectionStatus = new Infragistics.Win.Misc.UltraLabel();
            this.ulSelectionNumber = new Infragistics.Win.Misc.UltraLabel();
            this._declarationListContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._discrepancyListContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._historyContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._discrepancySecondPartContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._regionalBoundsContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._selectionListGridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._templateCommonInformationl = new Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls.TemplateCommonInformation();
            this._selectionMainParams = new Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls.SelectionCommonInformation();
            pageHistory = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            ulSelectionTypeCaption = new Infragistics.Win.Misc.UltraLabel();
            ulSelectionNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectionTabs)).BeginInit();
            this.selectionTabs.SuspendLayout();
            this.filterTab.SuspendLayout();
            this._tablePanel.SuspendLayout();
            this.declarationTab.SuspendLayout();
            this.discrepancyTab.SuspendLayout();
            pageHistory.SuspendLayout();
            this.discrepancySecondPartTab.SuspendLayout();
            this._regionalBounds.SuspendLayout();
            this._selectionListTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._expandableCommonInfo)).BeginInit();
            this._expandableCommonInfo.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.upHeader.ClientArea.SuspendLayout();
            this.upHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(200, 100);
            this.ultraTabControl1.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(196, 77);
            // 
            // selectionTabs
            // 
            this.selectionTabs.Controls.Add(this.ultraTabSharedControlsPage2);
            this.selectionTabs.Controls.Add(this.filterTab);
            this.selectionTabs.Controls.Add(this.declarationTab);
            this.selectionTabs.Controls.Add(this.discrepancyTab);
            this.selectionTabs.Controls.Add(pageHistory);
            this.selectionTabs.Controls.Add(this.discrepancySecondPartTab);
            this.selectionTabs.Controls.Add(this._regionalBounds);
            this.selectionTabs.Controls.Add(this._selectionListTab);
            this.selectionTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectionTabs.Location = new System.Drawing.Point(0, 176);
            this.selectionTabs.Name = "selectionTabs";
            this.selectionTabs.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.selectionTabs.Size = new System.Drawing.Size(1199, 609);
            this.selectionTabs.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.selectionTabs.TabIndex = 3;
            this.selectionTabs.TabLayoutStyle = Infragistics.Win.UltraWinTabs.TabLayoutStyle.MultiRowAutoSize;
            ultraTab2.FixedWidth = 150;
            ultraTab2.TabPage = this.filterTab;
            ultraTab2.Text = "Фильтр";
            ultraTab1.Key = "_regionalBoundsContainer";
            ultraTab1.TabPage = this._regionalBounds;
            ultraTab1.Text = "Границы отбора по регионам";
            ultraTab1.Visible = false;
            ultraTab7.Key = "_selectionListGridContainer";
            ultraTab7.TabPage = this._selectionListTab;
            ultraTab7.Text = "Список выборок";
            ultraTab7.Visible = false;
            ultraTab3.FixedWidth = 150;
            ultraTab3.Key = "_declarationListContainer";
            ultraTab3.TabPage = this.declarationTab;
            ultraTab3.Text = "Список деклараций";
            ultraTab4.FixedWidth = 150;
            ultraTab4.Key = "_discrepancyListContainer";
            ultraTab4.TabPage = this.discrepancyTab;
            ultraTab4.Text = "Список расхождений";
            ultraTab6.Key = "_discrepancySecondPartContainer";
            ultraTab6.TabPage = this.discrepancySecondPartTab;
            ultraTab6.Text = "Расхождения 2 сторона";
            ultraTab5.FixedWidth = 150;
            ultraTab5.Key = "_historyContainer";
            ultraTab5.TabPage = pageHistory;
            ultraTab5.Text = "История";
            this.selectionTabs.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab1,
            ultraTab7,
            ultraTab3,
            ultraTab4,
            ultraTab6,
            ultraTab5});
            this.selectionTabs.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(1197, 588);
            // 
            // filterTab
            // 
            this.filterTab.Controls.Add(this._tablePanel);
            this.filterTab.Location = new System.Drawing.Point(-10000, -10000);
            this.filterTab.Name = "filterTab";
            this.filterTab.Size = new System.Drawing.Size(1197, 588);
            // 
            // _tablePanel
            // 
            this._tablePanel.ColumnCount = 1;
            this._tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tablePanel.Controls.Add(this._manualFilterEditArea, 0, 0);
            this._tablePanel.Controls.Add(this._filterAddGroup, 0, 1);
            this._tablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tablePanel.Location = new System.Drawing.Point(0, 0);
            this._tablePanel.Margin = new System.Windows.Forms.Padding(0);
            this._tablePanel.Name = "_tablePanel";
            this._tablePanel.RowCount = 2;
            this._tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this._tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this._tablePanel.Size = new System.Drawing.Size(1197, 588);
            this._tablePanel.TabIndex = 0;
            // 
            // _manualFilterEditArea
            // 
            this._manualFilterEditArea.AutoScroll = true;
            this._manualFilterEditArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this._manualFilterEditArea.Location = new System.Drawing.Point(0, 0);
            this._manualFilterEditArea.Margin = new System.Windows.Forms.Padding(0);
            this._manualFilterEditArea.Name = "_manualFilterEditArea";
            this._manualFilterEditArea.Size = new System.Drawing.Size(1197, 552);
            this._manualFilterEditArea.TabIndex = 2;
            // 
            // _filterAddGroup
            // 
            this._filterAddGroup.Location = new System.Drawing.Point(30, 555);
            this._filterAddGroup.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this._filterAddGroup.Name = "_filterAddGroup";
            this._filterAddGroup.Size = new System.Drawing.Size(185, 30);
            this._filterAddGroup.TabIndex = 3;
            this._filterAddGroup.Text = "Добавить группу";
            this._filterAddGroup.Click += new System.EventHandler(this.AddNewGroup);
            // 
            // declarationTab
            // 
            this.declarationTab.Controls.Add(this._declarationListContainer);
            this.declarationTab.Location = new System.Drawing.Point(-10000, -10000);
            this.declarationTab.Name = "declarationTab";
            this.declarationTab.Size = new System.Drawing.Size(1197, 588);
            // 
            // discrepancyTab
            // 
            this.discrepancyTab.Controls.Add(this._discrepancyListContainer);
            this.discrepancyTab.Location = new System.Drawing.Point(1, 20);
            this.discrepancyTab.Name = "discrepancyTab";
            this.discrepancyTab.Size = new System.Drawing.Size(1197, 588);
            // 
            // pageHistory
            // 
            pageHistory.AutoScroll = true;
            pageHistory.Controls.Add(this._historyContainer);
            pageHistory.Location = new System.Drawing.Point(-10000, -10000);
            pageHistory.Name = "pageHistory";
            pageHistory.Size = new System.Drawing.Size(1197, 588);
            // 
            // discrepancySecondPartTab
            // 
            this.discrepancySecondPartTab.Controls.Add(this._discrepancySecondPartContainer);
            this.discrepancySecondPartTab.Location = new System.Drawing.Point(-10000, -10000);
            this.discrepancySecondPartTab.Name = "discrepancySecondPartTab";
            this.discrepancySecondPartTab.Size = new System.Drawing.Size(1197, 588);
            // 
            // _regionalBounds
            // 
            this._regionalBounds.Controls.Add(this._regionalBoundsContainer);
            this._regionalBounds.Location = new System.Drawing.Point(-10000, -10000);
            this._regionalBounds.Name = "_regionalBounds";
            this._regionalBounds.Size = new System.Drawing.Size(1197, 588);
            // 
            // _selectionListTab
            // 
            this._selectionListTab.Controls.Add(this._selectionListGridContainer);
            this._selectionListTab.Location = new System.Drawing.Point(-10000, -10000);
            this._selectionListTab.Name = "_selectionListTab";
            this._selectionListTab.Size = new System.Drawing.Size(1197, 588);
            // 
            // _expandableCommonInfo
            // 
            this._expandableCommonInfo.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this._expandableCommonInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this._expandableCommonInfo.ExpandedSize = new System.Drawing.Size(1199, 150);
            this._expandableCommonInfo.Location = new System.Drawing.Point(0, 26);
            this._expandableCommonInfo.Name = "_expandableCommonInfo";
            this._expandableCommonInfo.Size = new System.Drawing.Size(1199, 150);
            this._expandableCommonInfo.TabIndex = 2;
            this._expandableCommonInfo.Text = "Общие сведения";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.AutoSize = true;
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._templateCommonInformationl);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._selectionMainParams);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1193, 128);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // upHeader
            // 
            // 
            // upHeader.ClientArea
            // 
            this.upHeader.ClientArea.Controls.Add(ulSelectionTypeCaption);
            this.upHeader.ClientArea.Controls.Add(this.ulSelectionType);
            this.upHeader.ClientArea.Controls.Add(this._ulSelectionStatusCaption);
            this.upHeader.ClientArea.Controls.Add(this._ulSelectionStatus);
            this.upHeader.ClientArea.Controls.Add(ulSelectionNumberCaption);
            this.upHeader.ClientArea.Controls.Add(this.ulSelectionNumber);
            this.upHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.upHeader.Location = new System.Drawing.Point(0, 0);
            this.upHeader.Name = "upHeader";
            this.upHeader.Size = new System.Drawing.Size(1199, 26);
            this.upHeader.TabIndex = 1;
            // 
            // ulSelectionTypeCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            ulSelectionTypeCaption.Appearance = appearance2;
            ulSelectionTypeCaption.Location = new System.Drawing.Point(533, 3);
            ulSelectionTypeCaption.Name = "ulSelectionTypeCaption";
            ulSelectionTypeCaption.Size = new System.Drawing.Size(100, 20);
            ulSelectionTypeCaption.TabIndex = 11;
            ulSelectionTypeCaption.Text = "Тип:";
            // 
            // ulSelectionType
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.ulSelectionType.Appearance = appearance1;
            this.ulSelectionType.Location = new System.Drawing.Point(639, 3);
            this.ulSelectionType.Name = "ulSelectionType";
            this.ulSelectionType.Size = new System.Drawing.Size(465, 20);
            this.ulSelectionType.TabIndex = 10;
            // 
            // _ulSelectionStatusCaption
            // 
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            this._ulSelectionStatusCaption.Appearance = appearance6;
            this._ulSelectionStatusCaption.Location = new System.Drawing.Point(215, 3);
            this._ulSelectionStatusCaption.Name = "_ulSelectionStatusCaption";
            this._ulSelectionStatusCaption.Size = new System.Drawing.Size(100, 20);
            this._ulSelectionStatusCaption.TabIndex = 9;
            this._ulSelectionStatusCaption.Text = "Статус:";
            // 
            // _ulSelectionStatus
            // 
            appearance7.FontData.BoldAsString = "True";
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this._ulSelectionStatus.Appearance = appearance7;
            this._ulSelectionStatus.Location = new System.Drawing.Point(321, 3);
            this._ulSelectionStatus.Name = "_ulSelectionStatus";
            this._ulSelectionStatus.Size = new System.Drawing.Size(217, 20);
            this._ulSelectionStatus.TabIndex = 8;
            // 
            // ulSelectionNumberCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            ulSelectionNumberCaption.Appearance = appearance4;
            ulSelectionNumberCaption.Location = new System.Drawing.Point(3, 3);
            ulSelectionNumberCaption.Name = "ulSelectionNumberCaption";
            ulSelectionNumberCaption.Size = new System.Drawing.Size(100, 20);
            ulSelectionNumberCaption.TabIndex = 7;
            ulSelectionNumberCaption.Text = "№:";
            // 
            // ulSelectionNumber
            // 
            appearance5.FontData.BoldAsString = "True";
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.ulSelectionNumber.Appearance = appearance5;
            this.ulSelectionNumber.Location = new System.Drawing.Point(109, 3);
            this.ulSelectionNumber.Name = "ulSelectionNumber";
            this.ulSelectionNumber.Size = new System.Drawing.Size(100, 20);
            this.ulSelectionNumber.TabIndex = 6;
            // 
            // _declarationListContainer
            // 
            this._declarationListContainer.AutoScroll = true;
            this._declarationListContainer.AutoSize = true;
            this._declarationListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declarationListContainer.Location = new System.Drawing.Point(0, 0);
            this._declarationListContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._declarationListContainer.Name = "_declarationListContainer";
            this._declarationListContainer.Size = new System.Drawing.Size(1197, 588);
            this._declarationListContainer.TabIndex = 0;
            // 
            // _discrepancyListContainer
            // 
            this._discrepancyListContainer.AutoScroll = true;
            this._discrepancyListContainer.AutoSize = true;
            this._discrepancyListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrepancyListContainer.Location = new System.Drawing.Point(0, 0);
            this._discrepancyListContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._discrepancyListContainer.Name = "_discrepancyListContainer";
            this._discrepancyListContainer.Size = new System.Drawing.Size(1197, 588);
            this._discrepancyListContainer.TabIndex = 1;
            // 
            // _historyContainer
            // 
            this._historyContainer.AutoScroll = true;
            this._historyContainer.AutoSize = true;
            this._historyContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._historyContainer.Location = new System.Drawing.Point(0, 0);
            this._historyContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._historyContainer.Name = "_historyContainer";
            this._historyContainer.Size = new System.Drawing.Size(1197, 588);
            this._historyContainer.TabIndex = 0;
            // 
            // _discrepancySecondPartContainer
            // 
            this._discrepancySecondPartContainer.AutoScroll = true;
            this._discrepancySecondPartContainer.AutoSize = true;
            this._discrepancySecondPartContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrepancySecondPartContainer.Location = new System.Drawing.Point(0, 0);
            this._discrepancySecondPartContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._discrepancySecondPartContainer.Name = "_discrepancySecondPartContainer";
            this._discrepancySecondPartContainer.Size = new System.Drawing.Size(1197, 588);
            this._discrepancySecondPartContainer.TabIndex = 0;
            // 
            // _regionalBoundsContainer
            // 
            this._regionalBoundsContainer.AutoScroll = true;
            this._regionalBoundsContainer.AutoSize = true;
            this._regionalBoundsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._regionalBoundsContainer.Location = new System.Drawing.Point(0, 0);
            this._regionalBoundsContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._regionalBoundsContainer.Name = "_regionalBoundsContainer";
            this._regionalBoundsContainer.Size = new System.Drawing.Size(1197, 588);
            this._regionalBoundsContainer.TabIndex = 0;
            // 
            // _selectionListGridContainer
            // 
            this._selectionListGridContainer.AutoScroll = true;
            this._selectionListGridContainer.AutoSize = true;
            this._selectionListGridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._selectionListGridContainer.Location = new System.Drawing.Point(0, 0);
            this._selectionListGridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._selectionListGridContainer.Name = "_selectionListGridContainer";
            this._selectionListGridContainer.Size = new System.Drawing.Size(1197, 588);
            this._selectionListGridContainer.TabIndex = 0;
            // 
            // _templateCommonInformationl
            // 
            this._templateCommonInformationl.AutoScroll = true;
            this._templateCommonInformationl.BackColor = System.Drawing.Color.Transparent;
            this._templateCommonInformationl.Dock = System.Windows.Forms.DockStyle.Left;
            this._templateCommonInformationl.Location = new System.Drawing.Point(0, 100);
            this._templateCommonInformationl.Name = "_templateCommonInformationl";
            this._templateCommonInformationl.SelectionCreationDate = "";
            this._templateCommonInformationl.SelectionName = "";
            this._templateCommonInformationl.SelectionRegion = "";
            this._templateCommonInformationl.Size = new System.Drawing.Size(495, 28);
            this._templateCommonInformationl.TabIndex = 0;
            this._templateCommonInformationl.TemplateAuthor = "";
            this._templateCommonInformationl.Visible = false;
            // 
            // _selectionMainParams
            // 
            this._selectionMainParams.AutoScroll = true;
            this._selectionMainParams.AutoSize = true;
            this._selectionMainParams.BackColor = System.Drawing.Color.Transparent;
            this._selectionMainParams.CountDeclarations = " ";
            this._selectionMainParams.CountDiscrepancies = " ";
            this._selectionMainParams.DiscrepancyAmount = " ";
            this._selectionMainParams.Dock = System.Windows.Forms.DockStyle.Top;
            this._selectionMainParams.Location = new System.Drawing.Point(0, 0);
            this._selectionMainParams.Margin = new System.Windows.Forms.Padding(4);
            this._selectionMainParams.Name = "_selectionMainParams";
            this._selectionMainParams.SelectionAnalyst = " ";
            this._selectionMainParams.SelectionApprover = " ";
            this._selectionMainParams.SelectionChangeDate = " ";
            this._selectionMainParams.SelectionCreationDate = " ";
            this._selectionMainParams.SelectionName = " ";
            this._selectionMainParams.SelectionRegion = " ";
            this._selectionMainParams.SelectionStatusDate = " ";
            this._selectionMainParams.Size = new System.Drawing.Size(1193, 100);
            this._selectionMainParams.TabIndex = 0;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.selectionTabs);
            this.Controls.Add(this._expandableCommonInfo);
            this.Controls.Add(this.upHeader);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1199, 785);
            this.Load += new System.EventHandler(this.ViewLoad);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectionTabs)).EndInit();
            this.selectionTabs.ResumeLayout(false);
            this.filterTab.ResumeLayout(false);
            this._tablePanel.ResumeLayout(false);
            this.declarationTab.ResumeLayout(false);
            this.declarationTab.PerformLayout();
            this.discrepancyTab.ResumeLayout(false);
            this.discrepancyTab.PerformLayout();
            pageHistory.ResumeLayout(false);
            pageHistory.PerformLayout();
            this.discrepancySecondPartTab.ResumeLayout(false);
            this.discrepancySecondPartTab.PerformLayout();
            this._regionalBounds.ResumeLayout(false);
            this._regionalBounds.PerformLayout();
            this._selectionListTab.ResumeLayout(false);
            this._selectionListTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._expandableCommonInfo)).EndInit();
            this._expandableCommonInfo.ResumeLayout(false);
            this._expandableCommonInfo.PerformLayout();
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            this.upHeader.ClientArea.ResumeLayout(false);
            this.upHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upHeader;
        private Infragistics.Win.Misc.UltraLabel ulSelectionType;
        private Infragistics.Win.Misc.UltraLabel _ulSelectionStatus;
        private Infragistics.Win.Misc.UltraLabel ulSelectionNumber;
        private Infragistics.Win.Misc.UltraExpandableGroupBox _expandableCommonInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private SelectionCommonInformation _selectionMainParams;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl discrepancySecondPartTab;
        private UI.Controls.Grid.Controls.GridContainer _discrepancySecondPartContainer;
        private UI.Controls.Grid.Controls.GridContainer _historyContainer;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl discrepancyTab;
        private UI.Controls.Grid.Controls.GridContainer _discrepancyListContainer;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl declarationTab;
        private UI.Controls.Grid.Controls.GridContainer _declarationListContainer;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl filterTab;
        private TableLayoutPanel _tablePanel;
        private Panel _manualFilterEditArea;
        private Infragistics.Win.Misc.UltraButton _filterAddGroup;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl selectionTabs;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl _regionalBounds;
        private UI.Controls.Grid.Controls.GridContainer _regionalBoundsContainer;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl _selectionListTab;
        private UI.Controls.Grid.Controls.GridContainer _selectionListGridContainer;
        private Infragistics.Win.Misc.UltraLabel _ulSelectionStatusCaption;
        private TemplateCommonInformation _templateCommonInformationl;
    }
}
