﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancyListEventArgs : EventArgs
    {
        public bool FirstSide { get; set; }
        public object DiscrepancyList { get; set; }
    }
}
