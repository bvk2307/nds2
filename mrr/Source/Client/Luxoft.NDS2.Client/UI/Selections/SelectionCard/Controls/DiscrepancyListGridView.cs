﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.ViewModels;
using System.IO;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class DiscrepancyListGridView : UserControl, IExtendedGridView
    {
        public DiscrepancyListGridView()
        {
            InitializeComponent();
            GridEvents = new UltraGridEventsHandler(_grid);
            InitColumnsCollection();
            ApplyColumnFormatters();
            _grid.SetRowContextMenu(_contextMenu);
            _grid.SetBaseConfig();
        }

        private UltraGrid GetGrid()
        {
            return _grid;
        }

        /// <summary>
        /// Клонирует грид со всеми видимыми колонками
        /// Данные не клонируются.
        /// </summary>
        /// <returns></returns>
        public UltraGrid CloneGrid()
        {
            var cloneGrid = new DiscrepancyListGridView().GetGrid();
            if (cloneGrid == null)
                return null;

            foreach (var originalCol in _grid.DisplayLayout.Bands[0].Columns)
            {
                foreach (var col in cloneGrid.DisplayLayout.Bands[0].Columns)
                {
                    if (col.Key == originalCol.Key)
                    {
                        col.Hidden = originalCol.Hidden;
                        break;
                    }
                }
            }
            foreach (var originalGroup in _grid.DisplayLayout.Bands[0].Groups)
            {
                foreach (var group in cloneGrid.DisplayLayout.Bands[0].Groups)
                {
                    if (group.Key == originalGroup.Key)
                    {
                        group.Hidden = originalGroup.Hidden;
                        break;
                    }
                }
            }
            return cloneGrid;
        }

        /// <summary>
        ///     Возвращает выбранную строку
        /// </summary>
        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                    return null;

                return _grid.ActiveRow.ListObject;
            }
        }

        //public bool ReadOnly
        //{
        //    set
        //    {
        //        _grid.Enabled = !value;
        //    }
        //}

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }


        private SurColumnFilterView _buyerSurFilterView;
        private SurColumnFilterView _sellerSurFilterView;
        private ExcludeColumnFilterView _excludeFilterView;


        /// <summary>
        ///     Инициализирует коллекуию колонок грида
        /// </summary>
        public void InitColumnsCollection()
        {
            _buyerSurFilterView = new SurColumnFilterView(_grid.DisplayLayout.Bands[0].Columns[
                TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerSur)]);

            _sellerSurFilterView = new SurColumnFilterView(_grid.DisplayLayout.Bands[0].Columns[
                TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerSur)]);

            _excludeFilterView = new ExcludeColumnFilterView(_grid.DisplayLayout.Bands[0].Columns[
                TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.ExcludeFromClaimType)]);

            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(new[]
                        {
                            TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerInnDeclarant),
                            TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerKppDeclarant),
                            TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerInnDeclarant),
                            TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerKppDeclarant),
                            TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.FoundDate)
                        }
                    ),
                    new GridColumnCreator()
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Included),
                            column =>
                                new BooleanColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Included)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.DiscrepancyType),
                            column =>
                                new DiscrepancyTypeColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.DiscrepancyType)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Subtype),
                            column =>
                                new DiscrepancySubtypeColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Subtype)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SidePrimaryProcessing),
                            column =>
                                new DiscrepancySidePrimaryColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SidePrimaryProcessing)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerControlRatio),
                            column =>
                                new DiscrepancBuyerControlRatioColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerControlRatio)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerDeclarationSign),
                            column =>
                                new DiscrepancyDeclSignColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerDeclarationSign)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerChapter),
                            column =>
                                new DiscrepancyBuyerChapterColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerChapter)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerControlRatio),
                            column =>
                                new DiscrepancSellerControlRatioColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerControlRatio)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerDeclarationSign),
                            column =>
                                new DiscrepancyDeclSignColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerDeclarationSign)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerChapter),
                            column =>
                                new DiscrepancySellerChapterColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerChapter)]))
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.ExcludeFromClaimType),
                            column => _excludeFilterView)
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(t => t.BuyerSur),
                            column => _buyerSurFilterView)

                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(t => t.SellerSur),
                            column => _sellerSurFilterView)
                        .WithFilterViewFor(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(t => t.BuyerOperation),
                            (column) => new OperationCodesColumnFilterView(column))

                );
        }

        private void ApplyColumnFormatters()
        {
            DiscrepancyTypeColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.DiscrepancyType)]);

            DiscrepancySubtypeColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Subtype)]);

            DiscrepancySidePrimaryColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SidePrimaryProcessing)]);

            DiscrepancBuyerControlRatioColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerControlRatio)]);

            DiscrepancyDeclSignColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerDeclarationSign)]);

            DiscrepancyBuyerChapterColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerChapter)]);

            DiscrepancSellerControlRatioColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerControlRatio)]);

            DiscrepancyDeclSignColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerDeclarationSign)]);


            DiscrepancySellerChapterColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerChapter)]);
        }

        public event EventHandler ItemSelected;
        public event EventHandler OpenBuyerCardClicked;
        public event EventHandler OpenSellerCardClicked;
        public event EventHandler<DictionaryEventArgs> DiscrepancyListLoaded;

        private void OnSelectRow(object sender, ClickCellEventArgs e)
        {
            if (ItemSelected != null)
                ItemSelected(sender, e);
        }

        private void OnCellActivate(object sender, EventArgs e)
        {
            if (ItemSelected != null)
                ItemSelected(sender, e);
        }

        private void OnOpenBuyerCardClick(object sender, EventArgs e)
        {
            if (OpenBuyerCardClicked != null)
                OpenBuyerCardClicked(SelectedItem, e);
        }

        private void OnOpenSellerCardClick(object sender, EventArgs e)
        {
            if (OpenSellerCardClicked != null)
                OpenSellerCardClicked(SelectedItem, e);
        }

        private void DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            OnOpenSellerCardClick(SelectedItem, e);
        }

        public void ExportToExcel(object grid, ref string fileName)
        {
            _excelExporter.Export((UltraGrid)grid, fileName);
        }

        private DictionarySur _sur;
        private List<DictionaryItem> _excludeReasonDictionary;
        private void DiscrepancyListGridViewLoad(object sender, EventArgs e)
        {
            if (DiscrepancyListLoaded != null)
            {
                var arg = new DictionaryEventArgs();
                DiscrepancyListLoaded(sender, arg);
                _sur = arg.SurDictionary;

                _buyerSurFilterView.DictionarySur = _sur;
                _sellerSurFilterView.DictionarySur = _sur;
                _excludeFilterView.ExcludeReasonDictionary = arg.ExcludeReasonDictionary;
                _excludeReasonDictionary = arg.ExcludeReasonDictionary;

                Columns.FindByKey(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerSur))
                    .ApplyFormatter(
                        new SurColumnFormatter<SelectionDiscrepancyModel>(_sur, _grid));

                Columns.FindByKey(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerSur))
                    .ApplyFormatter(
                        new ToolTipFormatter<SelectionDiscrepancyModel>(model => _sur.Description((int)model.BuyerSur), _grid));

                Columns.FindByKey(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerSur))
                    .ApplyFormatter(
                        new SurColumnFormatter<SelectionDiscrepancyModel>(_sur, _grid));

                Columns.FindByKey(TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerSur))
                    .ApplyFormatter(
                        new ToolTipFormatter<SelectionDiscrepancyModel>(model => _sur.Description((int)model.SellerSur), _grid));

                ExcludeColumnView.Apply(_grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.ExcludeFromClaimType)], arg.ExcludeReasonDictionary);

            }
        }

        private void CellExported(object sender,
            Infragistics.Win.UltraWinGrid.ExcelExport.CellExportedEventArgs e)
        {
            int iRdex = e.CurrentRowIndex;
            int iCdex = e.CurrentColumnIndex;

            WorksheetCell cell =
                e.CurrentWorksheet.Rows[iRdex].Cells[iCdex];
            if (e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerSur) ||
                e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerSur))
            {
                cell.Value = _sur.Description((int)e.GridRow.Cells[e.GridColumn].Value);
            }
            if (e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Included))
            {
                cell.Value = (bool)e.GridRow.Cells[e.GridColumn].Value ? "Да" : "Нет";
            }
            if (e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.ExcludeFromClaimType))
            {
                cell.Value = _excludeReasonDictionary.First(x => x.Id == (int)e.GridRow.Cells[e.GridColumn].Value).Description;
            }
            if (e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerDeclarationNdsAmount) ||
                e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.SellerDeclarationNdsAmount) ||
                e.GridColumn.Key == TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Amount))
            {
                var format = e.CurrentWorksheet.Rows[e.CurrentRowIndex].Cells[e.CurrentColumnIndex].CellFormat;
                // Set format object property for bottom border
                format.FormatString = "### ### ### ### ##0.00";
            }
        }
    }
}