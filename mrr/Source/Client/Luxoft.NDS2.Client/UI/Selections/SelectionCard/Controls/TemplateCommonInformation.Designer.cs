﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    partial class TemplateCommonInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel2;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel3;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel4;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._tbName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbCreationDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbRegion = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbAuthor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbAuthor)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextHAlignAsString = "Right";
            ultraLabel1.Appearance = appearance1;
            ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel1.Location = new System.Drawing.Point(3, 3);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(186, 14);
            ultraLabel1.TabIndex = 0;
            ultraLabel1.Text = "Дата создания:";
            // 
            // ultraLabel2
            // 
            appearance2.TextHAlignAsString = "Right";
            ultraLabel2.Appearance = appearance2;
            ultraLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel2.Location = new System.Drawing.Point(3, 23);
            ultraLabel2.Name = "ultraLabel2";
            ultraLabel2.Size = new System.Drawing.Size(186, 14);
            ultraLabel2.TabIndex = 1;
            ultraLabel2.Text = "Наименование:";
            // 
            // ultraLabel3
            // 
            appearance3.TextHAlignAsString = "Right";
            ultraLabel3.Appearance = appearance3;
            ultraLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel3.Location = new System.Drawing.Point(3, 43);
            ultraLabel3.Name = "ultraLabel3";
            ultraLabel3.Size = new System.Drawing.Size(186, 14);
            ultraLabel3.TabIndex = 2;
            ultraLabel3.Text = "Количество регионов в шаблоне:";
            // 
            // ultraLabel4
            // 
            appearance4.TextHAlignAsString = "Right";
            ultraLabel4.Appearance = appearance4;
            ultraLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel4.Location = new System.Drawing.Point(3, 63);
            ultraLabel4.Name = "ultraLabel4";
            ultraLabel4.Size = new System.Drawing.Size(186, 18);
            ultraLabel4.TabIndex = 3;
            ultraLabel4.Text = "Автор:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(ultraLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(ultraLabel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(ultraLabel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(ultraLabel4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this._tbName, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this._tbCreationDate, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this._tbRegion, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this._tbAuthor, 1, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(481, 84);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // _tbName
            // 
            appearance8.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this._tbName.Appearance = appearance8;
            this._tbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tbName.Location = new System.Drawing.Point(195, 23);
            this._tbName.Name = "_tbName";
            this._tbName.Size = new System.Drawing.Size(283, 21);
            this._tbName.TabIndex = 4;
            this._tbName.TextChanged += new System.EventHandler(this.NameTextChanged);
            // 
            // _tbCreationDate
            // 
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._tbCreationDate.Appearance = appearance5;
            this._tbCreationDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tbCreationDate.Location = new System.Drawing.Point(195, 3);
            this._tbCreationDate.Name = "_tbCreationDate";
            this._tbCreationDate.ReadOnly = true;
            this._tbCreationDate.Size = new System.Drawing.Size(283, 21);
            this._tbCreationDate.TabIndex = 5;
            // 
            // _tbRegion
            // 
            appearance6.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this._tbRegion.Appearance = appearance6;
            this._tbRegion.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tbRegion.Location = new System.Drawing.Point(195, 43);
            this._tbRegion.Name = "_tbRegion";
            this._tbRegion.ReadOnly = true;
            this._tbRegion.Size = new System.Drawing.Size(283, 21);
            this._tbRegion.TabIndex = 6;
            // 
            // _tbAuthor
            // 
            appearance7.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this._tbAuthor.Appearance = appearance7;
            this._tbAuthor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tbAuthor.Location = new System.Drawing.Point(195, 63);
            this._tbAuthor.Name = "_tbAuthor";
            this._tbAuthor.ReadOnly = true;
            this._tbAuthor.Size = new System.Drawing.Size(283, 21);
            this._tbAuthor.TabIndex = 7;
            // 
            // TemplateCommonInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "TemplateCommonInformation";
            this.Size = new System.Drawing.Size(495, 92);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbAuthor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbCreationDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbRegion;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbAuthor;
    }
}
