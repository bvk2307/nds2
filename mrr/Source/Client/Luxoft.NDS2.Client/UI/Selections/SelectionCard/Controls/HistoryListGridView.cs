﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class HistoryListGridView : UserControl, IExtendedGridView
    {
        public HistoryListGridView()
        {
            InitializeComponent();
            GridEvents = new NullGridEventsHandler();
            InitColumnsCollection();
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }

        public void InitColumnsCollection()
        {
            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(),
                    new GridColumnCreator()
                );
        }
    }
}