﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Base.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class DeclarationListGridView : UserControl, IExtendedGridView
    {

        private readonly Dictionary<int, string> closeKNPDescription;

        private const string OpenKNPDescription = "Открыта";
        public DeclarationListGridView()
        {
            InitializeComponent();
            GridEvents = new UltraGridEventsHandler(_grid);
            InitColumnsCollection();
            _grid.SetRowContextMenu(_contextMenu);


            closeKNPDescription = new Dictionary<int, string>()
            {
                {-1, ""},
                { 1, "Загружена новая корректировка"},
                { 2, "Получены сведения о закрытии КНП из СЭОД."},
                { 3, "Корректировка аннулирована"}
            };

            _grid.SetBaseConfig();
        }

        public bool ReadOnly
        {
            set
            {
                Columns.FindByKey(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Included)).ReadOnly =
                    value;
            }
        }


        /// <summary>
        ///     Возвращает выбранную строку
        /// </summary>
        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                    return null;

                return _grid.ActiveRow.ListObject;
            }
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }

        private SurColumnFilterView _surFilterView;

        private ExcludeColumnFilterView _excludeFilterView;

        /// <summary>
        ///     Инициализирует коллекуию колонок грида
        /// </summary>
        public void InitColumnsCollection()
        {
            _surFilterView = new SurColumnFilterView(_grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Sur)]);

            _excludeFilterView = new ExcludeColumnFilterView(_grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.ExcludeFromClaimType)]);
            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(
                        new[]
                        {
                            TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Zip),
                            TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.CloseKNPReason)
                        }),
                    new GridColumnCreator()
                        .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.IsActual),
                            column => new BooleanColumnFilterView(column))
                         .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Category),
                            column => new BooleanColumnFilterView(column))
                         .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Type),
                            column => new DeclarationTypeColumnFilterView(column))
                        .WithFilterViewFor(
                            TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.ExcludeFromClaimType),
                            column => _excludeFilterView)
                        .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Sur),
                            column => _surFilterView)
                        .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.DeclarationSign),
                            column =>
                                new DiscrepancyDeclSignColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.DeclarationSign)]))
                        .WithFilterViewFor(TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.StatusCode),
                            column => new DeclarationKnpStatusColumnFilterView(
                                    _grid.DisplayLayout.Bands[0].Columns[
                                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.StatusCode)]))

                );

            Columns.FindByKey(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.StatusCode))
                .ApplyFormatter(
                    new ToolTipFormatter<DeclarationListItemViewModel>(GetStatusToolTip, _grid));

            Columns.FindByKey(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.IsActual))
              .ApplyFormatter(
                  new ToolTipFormatter<DeclarationListItemViewModel>(GetActualToolTip, _grid));

            BooleanColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.IsActual)]);

            DiscrepancyDeclSignColumnView.Apply(
                 _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.DeclarationSign)]);

            DeclarationKnpStatusView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.StatusCode)]);

            DeclarationTypeView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Type)]);
        }

        /// <summary>
        /// Tooltip для колонки Дата статуса КНП
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetStatusToolTip(DeclarationListItemViewModel model)
        {
            return closeKNPDescription.ContainsKey(model.CloseKNPReason) ? closeKNPDescription[model.CloseKNPReason] : OpenKNPDescription;
        }

        /// <summary>
        /// Tooltip для кологки Актуальность 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetActualToolTip(DeclarationListItemViewModel model)
        {
           return model.IsActual ? "Данные актуальны" : "Получены уточненные данные";
        }

        public event EventHandler OpenTaxPayerCardClicked;
        public event EventHandler OpenDeclarartionCardClicked;
      
        private void OpenTaxPayerCardClick(object sender, EventArgs e)
        {
            if (OpenTaxPayerCardClicked != null)
                OpenTaxPayerCardClicked(sender, e);
        }

        private void OnOpenDeclarationCardClick(object sender, EventArgs e)
        {
            if (OpenDeclarartionCardClicked != null)
                OpenDeclarartionCardClicked(sender, e);
        }

        private void DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            OnOpenDeclarationCardClick(sender, e);
        }

        public object[] Rows
        {
            get
            {
                return _grid.Rows.Select(x=>x.ListObject).ToArray();
            }
        }

        /// <summary>
        /// Событие изменения состояния чекбокса в шапке таблицы
        /// </summary>
        public event EventHandler AfterGroupDeclarationCheckStateChanged;

        private void AfterGroupCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if(AfterGroupDeclarationCheckStateChanged != null)
                AfterGroupDeclarationCheckStateChanged(sender, e);
        }

        /// <summary>
        /// Изменение состояние чек бокса в колонке Include
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellChange(object sender, CellEventArgs e)
        {
            if (AfterGroupDeclarationCheckStateChanged != null)
                AfterGroupDeclarationCheckStateChanged(sender, e);
        }

        /// <summary>
        /// Проверяет может ли пользователь поменять состояние чекбокса декларации
        /// </summary>
        public event EventHandler<DeclarationItemEventArgs> BeforeCellActivated;
        private void BeforeCellActivate(object sender, CancelableCellEventArgs e)
        {
            if (e.Cell.Column.Key == TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Included)
                && BeforeCellActivated != null)
            {
                var args = new DeclarationItemEventArgs();
                BeforeCellActivated(sender, args);
                e.Cell.Activation = args.AllowInclude ? Activation.AllowEdit : Activation.NoEdit;
            }
        }

        /// <summary>
        /// Проверяет может ли пользователь поменять состояние чекбокса в шапке таблицы
        /// </summary>
        private void BeforeHeaderCheckStateChanged(object sender, BeforeHeaderCheckStateChangedEventArgs e)
        {
            if (e.Column.Key == TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Included)
               && BeforeCellActivated != null)
            {
                var args = new DeclarationItemEventArgs();
                BeforeCellActivated(sender, args);
                e.Column.CellActivation = args.AllowInclude ? Activation.AllowEdit : Activation.NoEdit;
            }
        }


        public void CheckAllRows(bool check)
        {
            foreach (var row in _grid.Rows)
            {
                row.Cells[TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Included)].Value = check;
            }
        }

        public bool AllRowsCheckedState(bool isChecked)
        {
            int checkedCount = _grid.Rows.Count( x=> bool.Parse(x.Cells[TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Included)].Value.ToString()) ==
                    isChecked);
            return _grid.Rows.Count == checkedCount;
        }

        public bool AllIncluded
        {
            get
            {
                return
                    _grid.DisplayLayout.Bands[0].Columns[
                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Included)].GetHeaderCheckedState(_grid.Rows) ==
                    CheckState.Checked;
            }
            set
            {
                _grid.DisplayLayout.Bands[0].Columns[
                        TypeHelper<DeclarationListItemViewModel>.GetMemberName(t => t.Included)].SetHeaderCheckedState(_grid.Rows, value) ;
            }
        }

        public event EventHandler<DictionaryEventArgs> DeclarationViewLoaded;

        private void DeclarationListGridViewLoad(object sender, EventArgs e)
        {
            if (DeclarationViewLoaded != null)
            {
                var arg = new DictionaryEventArgs();
                DeclarationViewLoaded(sender, arg);

                _surFilterView.DictionarySur = arg.SurDictionary;
                _excludeFilterView.ExcludeReasonDictionary = arg.ExcludeReasonDictionary;

                Columns.FindByKey(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Sur))
                    .ApplyFormatter(
                        new SurColumnFormatter<DeclarationListItemViewModel>(arg.SurDictionary, _grid));

                Columns.FindByKey(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Sur))
                    .ApplyFormatter(
                        new ToolTipFormatter<DeclarationListItemViewModel>(model => arg.SurDictionary.Description((int)model.Sur), _grid));

                ExcludeColumnView.Apply(
                    _grid.DisplayLayout.Bands[0].Columns[
                 TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.ExcludeFromClaimType)], arg.ExcludeReasonDictionary);
            }
        }

        public EventHandler<DeclarationItemEventArgs> CellClicked;
        private void GridMouseUp(object sender, MouseEventArgs e)
        {
            if (CellClicked != null)
            {
                var args = new DeclarationItemEventArgs();
                CellClicked(SelectedItem, args);
                _openDeclarationMenuItem.Visible = args.IsDeclaration;
                _openJournalMenuItem.Visible = !args.IsDeclaration;
            }

        }
    }
}