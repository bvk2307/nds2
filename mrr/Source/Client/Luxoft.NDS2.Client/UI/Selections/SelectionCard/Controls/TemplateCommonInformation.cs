﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class TemplateCommonInformation : UserControl
    {
        public TemplateCommonInformation()
        {
            InitializeComponent();
        }

        public string SelectionName
        {
            get
            {
                return _tbName.Text;
            }
            set
            {
                _tbName.Text = value;
            }
        }

        public bool SelectionNameReadonly
        {
            set 
            {
               _tbName.ReadOnly = value;
            }
        }

        public string SelectionCreationDate
        {
            get
            {
                return _tbCreationDate.Text;
            }
            set
            {
                _tbCreationDate.Text = value;
            }
        }

        public string SelectionRegion
        {
            get
            {
                return _tbRegion.Text;
            }
            set
            {
                _tbRegion.Text = value;
            }
        }

        public string TemplateAuthor
        {
            get
            {
                return _tbAuthor.Text;
            }
            set
            {
                _tbAuthor.Text = value;
            }
        }

        public event EventHandler SelectionNameTextChange;

        private void NameTextChanged(object sender, EventArgs e)
        {
            if (SelectionNameTextChange != null)
            {
                SelectionNameTextChange(this, new EventArgs());
            }
        }

    }
}