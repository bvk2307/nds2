﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class RegionalBoundsListGridView : UserControl, IExtendedGridView
    {
        public RegionalBoundsListGridView()
        {
            InitializeComponent();
            GridEvents = new NullGridEventsHandler();
            InitColumnsCollection();
        }

        public void InitColumnsCollection()
        {
            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(),
                    new GridColumnCreator()
                );
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public bool ReadOnly
        {
            set
            {
                Columns.FindByKey(TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x => x.Include)).ReadOnly =
                    value;
                Columns.FindByKey(TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x => x.DiscrTotalAmt)).ReadOnly =
                    value;
                Columns.FindByKey(TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x => x.DiscrMinAmt)).ReadOnly =
                    value;
                Columns.FindByKey(TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x => x.DiscrGapTotalAmt)).ReadOnly =
                    value;
                Columns.FindByKey(TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x => x.DiscrGapMinAmt)).ReadOnly =
                    value;
            }
        }

        /// <summary>
        ///     Возвращает выбранную строку
        /// </summary>
        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                    return null;

                return _grid.ActiveRow.ListObject;
            }
        }

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }

        private void CellDataError(object sender, Infragistics.Win.UltraWinGrid.CellDataErrorEventArgs e)
        {
            e.StayInEditMode = false;
            e.RaiseErrorEvent = false;
            e.RestoreOriginalValue = true;
        }

        public void UpdateCellChangedData()
        {
            _grid.PerformAction(UltraGridAction.ExitEditMode);
            _grid.UpdateData();
        }

        public event EventHandler<SelectedCellEventArgs> CellChanged;
        private void CellChange(object sender, CellEventArgs e)
        {
            if(e.Cell.Column.Key == TypeHelper<SelectionRegionalBoundsViewModel>.GetMemberName(x=>x.Include))
            {
                if (CellChanged != null)
                {
                    CellChanged(sender, new SelectedCellEventArgs { Value = e.Cell.Value, ColumnKey = e.Cell.Column.Key });
                }
            }
        }

        /// <summary>
        /// Событие изменения состояния чекбокса в шапке таблицы
        /// </summary>
        public event EventHandler AfterGroupRegBoundsCheckStateChanged;
        private void AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (AfterGroupRegBoundsCheckStateChanged != null)
                AfterGroupRegBoundsCheckStateChanged(sender, e);
        }
    }
}
