﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    partial class DeclarationListGridView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Included");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsActual");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ExcludeFromClaimType");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Type");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Sur", -1, null, 25920159, 0, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn38 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Inn", -1, null, 25920159, 1, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Kpp", -1, null, 25920159, 2, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxPayer", -1, null, 25920159, 3, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InnReorganized");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Zip");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DeclarationSign");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NdsAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyCount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountChapter8", -1, null, 25920160, 0, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountChapter9", -1, null, 25920160, 1, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountTotal", -1, null, 25920160, 2, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountMin", -1, null, 25920160, 3, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountMax", -1, null, 25920160, 4, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyAmountAvg", -1, null, 25920160, 5, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxPeriod");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DeclarationVersion");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SubmitDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusDate", -1, null, 25920161, 0, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Category");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SonoCode", -1, null, 25920162, 0, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SonoName", -1, null, 25920162, 1, 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Inspector");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CloseKNPReason");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Налогоплательщик", 25920159);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Сумма расхождений по книгам", 25920160);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("КНП", 25920161);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Инспекция", 25920162);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Регион", 25920163);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 25920164);
            Infragistics.Win.UltraWinGrid.RowLayout rowLayout1 = new Infragistics.Win.UltraWinGrid.RowLayout("try1");
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo1 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Included", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo2 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "IsActual", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo3 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ExcludeFromClaimType", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo4 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Type", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo5 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Sur", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo6 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Inn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo7 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Kpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo8 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxPayer", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo9 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "InnReorganized", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo10 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "RegNumber", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo11 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Zip", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo12 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DeclarationSign", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo13 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NdsAmount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo14 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyCount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo15 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountChapter8", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo16 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountChapter9", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo17 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountTotal", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo18 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountMin", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo19 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountMax", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo20 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyAmountAvg", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo21 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxPeriod", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo22 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DeclarationVersion", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo23 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SubmitDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo24 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "StatusDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo25 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Category", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo26 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SonoCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo27 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SonoName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo28 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "RegionCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo29 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "RegionName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo30 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Inspector", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo31 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "StatusCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo32 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CloseKNPReason", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo33 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Налогоплательщик", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo34 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Сумма расхождений по книгам", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo35 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "КНП", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo36 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Инспекция", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo37 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Регион", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo38 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup0", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._filterProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openTaxpayerCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._openDeclarationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._openJournalMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this._contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance13;
            ultraGridColumn33.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn33.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            ultraGridColumn33.Header.Caption = "";
            ultraGridColumn33.Header.CheckBoxAlignment = Infragistics.Win.UltraWinGrid.HeaderCheckBoxAlignment.Center;
            ultraGridColumn33.Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            ultraGridColumn33.Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Always;
            ultraGridColumn33.Header.ToolTipText = "Флаг выбора записи";
            ultraGridColumn33.Header.VisiblePosition = 0;
            ultraGridColumn33.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn33.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn33.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(44, 0);
            ultraGridColumn33.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn33.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn33.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn33.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn34.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn34.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn34.Header.Caption = "Актуальность";
            ultraGridColumn34.Header.ToolTipText = "Актуальность данных";
            ultraGridColumn34.Header.VisiblePosition = 1;
            ultraGridColumn34.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn34.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridColumn34.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn35.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn35.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn35.Header.Caption = "Исключена из АТ";
            ultraGridColumn35.Header.VisiblePosition = 13;
            ultraGridColumn35.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn35.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn35.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn35.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn36.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn36.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn36.Header.Caption = "Тип";
            ultraGridColumn36.Header.ToolTipText = "НД по НДС или Журнал учета";
            ultraGridColumn36.Header.VisiblePosition = 22;
            ultraGridColumn36.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn36.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn36.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn36.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn36.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn37.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn37.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn37.Header.Caption = "СУР";
            ultraGridColumn37.Header.ToolTipText = "СУР налогоплательщика стороны первичной отработки";
            ultraGridColumn37.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn37.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn37.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn37.RowLayoutColumnInfo.ParentGroupKey = "Налогоплательщик";
            ultraGridColumn37.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(83, 0);
            ultraGridColumn37.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn37.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn37.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn38.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn38.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn38.Header.Caption = "ИНН";
            ultraGridColumn38.Header.ToolTipText = "ИНН налогоплательщика стороны первичной отработки";
            ultraGridColumn38.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn38.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn38.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn38.RowLayoutColumnInfo.ParentGroupKey = "Налогоплательщик";
            ultraGridColumn38.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn38.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn38.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn38.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn39.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn39.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.BasedOnDataType;
            ultraGridColumn39.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn39.Header.Caption = "КПП";
            ultraGridColumn39.Header.ToolTipText = "КПП налогоплательщика стороны первичной отработки";
            ultraGridColumn39.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn39.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn39.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn39.RowLayoutColumnInfo.ParentGroupKey = "Налогоплательщик";
            ultraGridColumn39.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn39.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn40.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn40.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn40.Header.Caption = "Наименование";
            ultraGridColumn40.Header.ToolTipText = "Наименование налогоплательщика первичной отработки";
            ultraGridColumn40.RowLayoutColumnInfo.OriginX = 5;
            ultraGridColumn40.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupKey = "Налогоплательщик";
            ultraGridColumn40.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(109, 0);
            ultraGridColumn40.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn40.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn41.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn41.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn41.Header.Caption = "Реорганизованный НП";
            ultraGridColumn41.Header.ToolTipText = "ИНН реорганизованного НП";
            ultraGridColumn41.Header.VisiblePosition = 8;
            ultraGridColumn41.RowLayoutColumnInfo.OriginX = 15;
            ultraGridColumn41.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn41.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn41.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn41.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn42.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn42.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn42.Header.Caption = "Регистрационный №";
            ultraGridColumn42.Header.ToolTipText = "Регистрационный № СЭОД";
            ultraGridColumn42.Header.VisiblePosition = 12;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 17;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn42.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn43.Header.VisiblePosition = 26;
            ultraGridColumn43.Hidden = true;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 100;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 3;
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn44.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn44.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn44.Header.Caption = "Признак НД";
            ultraGridColumn44.Header.ToolTipText = "Признак декларации";
            ultraGridColumn44.Header.VisiblePosition = 20;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 19;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(101, 0);
            ultraGridColumn44.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn45.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn45.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn45.Format = "N2";
            ultraGridColumn45.Header.Caption = "Сумма НДС";
            ultraGridColumn45.Header.ToolTipText = "Сумма налога, исчисленная к \"+\" уплате / \"-\" возмещению в бюджет";
            ultraGridColumn45.Header.VisiblePosition = 23;
            ultraGridColumn45.RowLayoutColumnInfo.OriginX = 21;
            ultraGridColumn45.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn45.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn45.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn46.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn46.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn46.Header.Caption = "Кол-во расхождений";
            ultraGridColumn46.Header.ToolTipText = "Количество расхождений по СФ в НД";
            ultraGridColumn46.Header.VisiblePosition = 28;
            ultraGridColumn46.RowLayoutColumnInfo.OriginX = 23;
            ultraGridColumn46.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn46.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn46.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn47.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn47.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn47.Format = "N2";
            ultraGridColumn47.Header.Caption = "Покупок (раздел 8)";
            ultraGridColumn47.Header.ToolTipText = "Сумма расхождений по книге покупок";
            ultraGridColumn47.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn47.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn47.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn47.RowLayoutColumnInfo.ParentGroupKey = "Сумма расхождений по книгам";
            ultraGridColumn47.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(153, 0);
            ultraGridColumn47.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn47.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn48.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn48.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn48.Format = "N2";
            ultraGridColumn48.Header.Caption = "Продаж (раздел 9 и 12)";
            ultraGridColumn48.Header.ToolTipText = "Сумма расхождений по книге продаж, включая раздел 12";
            ultraGridColumn48.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn48.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupKey = "Сумма расхождений по книгам";
            ultraGridColumn48.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(143, 0);
            ultraGridColumn48.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn48.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn48.Width = 160;
            ultraGridColumn49.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn49.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn49.Format = "N2";
            ultraGridColumn49.Header.Caption = "Общая";
            ultraGridColumn49.Header.ToolTipText = "Общая сумма расхождений по СФ в НД";
            ultraGridColumn49.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn49.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn49.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(79, 0);
            ultraGridColumn49.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn50.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn50.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn50.Format = "N2";
            ultraGridColumn50.Header.Caption = "Минимальная";
            ultraGridColumn50.Header.ToolTipText = "Минимальная сумма расхождения по СФ в НД";
            ultraGridColumn50.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn50.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(110, 0);
            ultraGridColumn50.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn51.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn51.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn51.Format = "N2";
            ultraGridColumn51.Header.Caption = "Максимальная";
            ultraGridColumn51.Header.ToolTipText = "Максимальная сумма расхождения СФ в НД";
            ultraGridColumn51.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn51.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn51.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(115, 0);
            ultraGridColumn51.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn52.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn52.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn52.Format = "N2";
            ultraGridColumn52.Header.Caption = "Средняя";
            ultraGridColumn52.Header.ToolTipText = "Средняя сумма расхождения = Общая сумма / кол-во расхождений";
            ultraGridColumn52.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn52.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn52.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(82, 0);
            ultraGridColumn52.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn52.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn53.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn53.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn53.Header.Caption = "Отчетный период";
            ultraGridColumn53.Header.ToolTipText = "Период за который подается декларация/журнал";
            ultraGridColumn53.Header.VisiblePosition = 9;
            ultraGridColumn53.RowLayoutColumnInfo.OriginX = 36;
            ultraGridColumn53.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn53.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn53.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn54.Header.Caption = "№ корр./версии";
            ultraGridColumn54.Header.ToolTipText = "Номер корректировки декларации/версии журнала";
            ultraGridColumn54.Header.VisiblePosition = 29;
            ultraGridColumn54.RowLayoutColumnInfo.OriginX = 40;
            ultraGridColumn54.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn54.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn54.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn55.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn55.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn55.Header.Caption = "Дата подачи";
            ultraGridColumn55.Header.ToolTipText = "Дата подачи в НО декларации/журнала";
            ultraGridColumn55.Header.VisiblePosition = 21;
            ultraGridColumn55.RowLayoutColumnInfo.OriginX = 38;
            ultraGridColumn55.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn55.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn55.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn55.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn56.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn56.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn56.Header.Caption = "Дата статуса";
            ultraGridColumn56.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupKey = "КНП";
            ultraGridColumn56.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn57.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn57.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn57.Header.Caption = "Крупнейший";
            ultraGridColumn57.Header.ToolTipText = "НП является крупнейшим";
            ultraGridColumn57.Header.VisiblePosition = 10;
            ultraGridColumn57.RowLayoutColumnInfo.OriginX = 46;
            ultraGridColumn57.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn57.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn57.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn58.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn58.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn58.Header.Caption = "Код";
            ultraGridColumn58.Header.ToolTipText = "Код инспекции";
            ultraGridColumn58.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn58.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupKey = "Инспекция";
            ultraGridColumn58.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn59.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn59.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn59.Header.Caption = "Наименование";
            ultraGridColumn59.Header.ToolTipText = "Наименование инспекции";
            ultraGridColumn59.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn59.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn59.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn59.RowLayoutColumnInfo.ParentGroupKey = "Инспекция";
            ultraGridColumn59.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn59.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn59.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn60.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn60.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn60.Header.Caption = "Код";
            ultraGridColumn60.Header.ToolTipText = "Код региона";
            ultraGridColumn60.Header.VisiblePosition = 2;
            ultraGridColumn60.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn60.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn60.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn60.RowLayoutColumnInfo.ParentGroupKey = "Регион";
            ultraGridColumn60.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(75, 0);
            ultraGridColumn60.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn60.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn61.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn61.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn61.Header.Caption = "Наименование";
            ultraGridColumn61.Header.ToolTipText = "Наименование региона";
            ultraGridColumn61.Header.VisiblePosition = 3;
            ultraGridColumn61.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupKey = "Регион";
            ultraGridColumn61.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn61.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn62.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn62.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn62.Header.Caption = "Инспектор";
            ultraGridColumn62.Header.VisiblePosition = 11;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 56;
            ultraGridColumn62.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn62.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 44);
            ultraGridColumn62.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn63.Header.Caption = "Статус";
            ultraGridColumn63.Header.VisiblePosition = 30;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn63.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn63.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn63.RowLayoutColumnInfo.ParentGroupKey = "КНП";
            ultraGridColumn63.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn63.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn64.Header.VisiblePosition = 31;
            ultraGridColumn64.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn38,
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51,
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56,
            ultraGridColumn57,
            ultraGridColumn58,
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64});
            ultraGridGroup1.Key = "Налогоплательщик";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 7;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.TextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            ultraGridGroup2.Key = "Сумма расхождений по книгам";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 25;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 3;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Key = "КНП";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 42;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 5;
            ultraGridGroup4.Key = "Инспекция";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 48;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup5.Key = "Регион";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 52;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup6.Header.Caption = "Сумма расхождений в НД";
            ultraGridGroup6.Key = "NewGroup0";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 28;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 8;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 3;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6});
            rowLayoutColumnInfo2.OriginX = 2;
            rowLayoutColumnInfo2.OriginY = 0;
            rowLayoutColumnInfo2.SpanX = 2;
            rowLayoutColumnInfo2.SpanY = 2;
            rowLayoutColumnInfo3.OriginX = 4;
            rowLayoutColumnInfo3.OriginY = 0;
            rowLayoutColumnInfo3.SpanX = 2;
            rowLayoutColumnInfo3.SpanY = 2;
            rowLayoutColumnInfo4.OriginX = 6;
            rowLayoutColumnInfo4.OriginY = 0;
            rowLayoutColumnInfo4.SpanX = 2;
            rowLayoutColumnInfo4.SpanY = 2;
            rowLayoutColumnInfo5.OriginX = 0;
            rowLayoutColumnInfo5.OriginY = 0;
            rowLayoutColumnInfo5.ParentGroupIndex = 0;
            rowLayoutColumnInfo5.ParentGroupKey = "Налогоплательщик";
            rowLayoutColumnInfo5.PreferredCellSize = new System.Drawing.Size(463, 0);
            rowLayoutColumnInfo5.SpanX = 2;
            rowLayoutColumnInfo5.SpanY = 2;
            rowLayoutColumnInfo6.OriginX = 10;
            rowLayoutColumnInfo6.OriginY = 2;
            rowLayoutColumnInfo6.SpanX = 2;
            rowLayoutColumnInfo6.SpanY = 2;
            rowLayoutColumnInfo7.OriginX = 12;
            rowLayoutColumnInfo7.OriginY = 2;
            rowLayoutColumnInfo7.SpanX = 2;
            rowLayoutColumnInfo7.SpanY = 2;
            rowLayoutColumnInfo8.OriginX = 14;
            rowLayoutColumnInfo8.OriginY = 2;
            rowLayoutColumnInfo8.SpanX = 2;
            rowLayoutColumnInfo8.SpanY = 2;
            rowLayoutColumnInfo10.OriginX = 12;
            rowLayoutColumnInfo10.OriginY = 0;
            rowLayoutColumnInfo10.SpanX = 2;
            rowLayoutColumnInfo10.SpanY = 2;
            rowLayoutColumnInfo12.OriginX = 14;
            rowLayoutColumnInfo12.OriginY = 0;
            rowLayoutColumnInfo12.SpanX = 2;
            rowLayoutColumnInfo12.SpanY = 2;
            rowLayoutColumnInfo13.OriginX = 16;
            rowLayoutColumnInfo13.OriginY = 0;
            rowLayoutColumnInfo13.SpanX = 2;
            rowLayoutColumnInfo13.SpanY = 2;
            rowLayoutColumnInfo14.OriginX = 18;
            rowLayoutColumnInfo14.OriginY = 0;
            rowLayoutColumnInfo14.SpanX = 2;
            rowLayoutColumnInfo14.SpanY = 2;
            rowLayoutColumnInfo15.OriginX = 0;
            rowLayoutColumnInfo15.OriginY = 0;
            rowLayoutColumnInfo15.ParentGroupIndex = 1;
            rowLayoutColumnInfo15.ParentGroupKey = "Сумма расхождений по книгам";
            rowLayoutColumnInfo15.SpanX = 2;
            rowLayoutColumnInfo15.SpanY = 2;
            rowLayoutColumnInfo16.OriginX = 20;
            rowLayoutColumnInfo16.OriginY = 4;
            rowLayoutColumnInfo16.SpanX = 2;
            rowLayoutColumnInfo16.SpanY = 2;
            rowLayoutColumnInfo17.OriginX = 20;
            rowLayoutColumnInfo17.OriginY = 6;
            rowLayoutColumnInfo17.SpanX = 2;
            rowLayoutColumnInfo17.SpanY = 2;
            rowLayoutColumnInfo18.OriginX = 20;
            rowLayoutColumnInfo18.OriginY = 8;
            rowLayoutColumnInfo18.SpanX = 2;
            rowLayoutColumnInfo18.SpanY = 2;
            rowLayoutColumnInfo19.OriginX = 20;
            rowLayoutColumnInfo19.OriginY = 10;
            rowLayoutColumnInfo19.SpanX = 2;
            rowLayoutColumnInfo19.SpanY = 2;
            rowLayoutColumnInfo20.OriginX = 20;
            rowLayoutColumnInfo20.OriginY = 12;
            rowLayoutColumnInfo20.SpanX = 2;
            rowLayoutColumnInfo20.SpanY = 2;
            rowLayoutColumnInfo21.OriginX = 22;
            rowLayoutColumnInfo21.OriginY = 0;
            rowLayoutColumnInfo21.SpanX = 2;
            rowLayoutColumnInfo21.SpanY = 2;
            rowLayoutColumnInfo23.OriginX = 26;
            rowLayoutColumnInfo23.OriginY = 0;
            rowLayoutColumnInfo23.SpanX = 2;
            rowLayoutColumnInfo23.SpanY = 2;
            rowLayoutColumnInfo24.OriginX = 28;
            rowLayoutColumnInfo24.OriginY = 6;
            rowLayoutColumnInfo24.SpanX = 2;
            rowLayoutColumnInfo24.SpanY = 2;
            rowLayoutColumnInfo25.OriginX = 30;
            rowLayoutColumnInfo25.OriginY = 0;
            rowLayoutColumnInfo25.SpanX = 2;
            rowLayoutColumnInfo25.SpanY = 2;
            rowLayoutColumnInfo26.OriginX = 0;
            rowLayoutColumnInfo26.OriginY = 0;
            rowLayoutColumnInfo26.ParentGroupIndex = 3;
            rowLayoutColumnInfo26.ParentGroupKey = "Инспекция";
            rowLayoutColumnInfo26.SpanX = 2;
            rowLayoutColumnInfo26.SpanY = 2;
            rowLayoutColumnInfo27.OriginX = 32;
            rowLayoutColumnInfo27.OriginY = 4;
            rowLayoutColumnInfo27.SpanX = 2;
            rowLayoutColumnInfo27.SpanY = 2;
            rowLayoutColumnInfo28.OriginX = 34;
            rowLayoutColumnInfo28.OriginY = 2;
            rowLayoutColumnInfo28.SpanX = 2;
            rowLayoutColumnInfo28.SpanY = 2;
            rowLayoutColumnInfo29.OriginX = 34;
            rowLayoutColumnInfo29.OriginY = 4;
            rowLayoutColumnInfo29.SpanX = 2;
            rowLayoutColumnInfo29.SpanY = 2;
            rowLayoutColumnInfo30.OriginX = 36;
            rowLayoutColumnInfo30.OriginY = 0;
            rowLayoutColumnInfo30.SpanX = 2;
            rowLayoutColumnInfo30.SpanY = 2;
            rowLayoutColumnInfo33.LabelSpan = 1;
            rowLayoutColumnInfo33.OriginX = 8;
            rowLayoutColumnInfo33.OriginY = 0;
            rowLayoutColumnInfo33.SpanX = 2;
            rowLayoutColumnInfo33.SpanY = 4;
            rowLayoutColumnInfo34.LabelSpan = 1;
            rowLayoutColumnInfo34.OriginX = 20;
            rowLayoutColumnInfo34.OriginY = 0;
            rowLayoutColumnInfo34.SpanX = 2;
            rowLayoutColumnInfo34.SpanY = 4;
            rowLayoutColumnInfo35.LabelSpan = 1;
            rowLayoutColumnInfo35.OriginX = 28;
            rowLayoutColumnInfo35.OriginY = 0;
            rowLayoutColumnInfo35.SpanX = 2;
            rowLayoutColumnInfo35.SpanY = 4;
            rowLayoutColumnInfo36.LabelSpan = 1;
            rowLayoutColumnInfo36.OriginX = 32;
            rowLayoutColumnInfo36.OriginY = 0;
            rowLayoutColumnInfo36.SpanX = 2;
            rowLayoutColumnInfo36.SpanY = 4;
            rowLayout1.ColumnInfos.AddRange(new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo[] {
            rowLayoutColumnInfo1,
            rowLayoutColumnInfo2,
            rowLayoutColumnInfo3,
            rowLayoutColumnInfo4,
            rowLayoutColumnInfo5,
            rowLayoutColumnInfo6,
            rowLayoutColumnInfo7,
            rowLayoutColumnInfo8,
            rowLayoutColumnInfo9,
            rowLayoutColumnInfo10,
            rowLayoutColumnInfo11,
            rowLayoutColumnInfo12,
            rowLayoutColumnInfo13,
            rowLayoutColumnInfo14,
            rowLayoutColumnInfo15,
            rowLayoutColumnInfo16,
            rowLayoutColumnInfo17,
            rowLayoutColumnInfo18,
            rowLayoutColumnInfo19,
            rowLayoutColumnInfo20,
            rowLayoutColumnInfo21,
            rowLayoutColumnInfo22,
            rowLayoutColumnInfo23,
            rowLayoutColumnInfo24,
            rowLayoutColumnInfo25,
            rowLayoutColumnInfo26,
            rowLayoutColumnInfo27,
            rowLayoutColumnInfo28,
            rowLayoutColumnInfo29,
            rowLayoutColumnInfo30,
            rowLayoutColumnInfo31,
            rowLayoutColumnInfo32,
            rowLayoutColumnInfo33,
            rowLayoutColumnInfo34,
            rowLayoutColumnInfo35,
            rowLayoutColumnInfo36,
            rowLayoutColumnInfo37,
            rowLayoutColumnInfo38});
            rowLayout1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand1.RowLayouts.AddRange(new Infragistics.Win.UltraWinGrid.RowLayout[] {
            rowLayout1});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this._grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this._grid.DisplayLayout.MaxColScrollRegions = 1;
            this._grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this._grid.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this._grid.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this._grid.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.WithinGroup;
            this._grid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._grid.DisplayLayout.Override.CellAppearance = appearance20;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.CellPadding = 0;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterProvider;
            this._grid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            this._grid.DisplayLayout.Override.GroupHeaderTextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            appearance22.TextHAlignAsString = "Center";
            this._grid.DisplayLayout.Override.HeaderAppearance = appearance22;
            this._grid.DisplayLayout.Override.HeaderCheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            this._grid.DisplayLayout.Override.MaxSelectedRows = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this._grid.DisplayLayout.Override.RowAppearance = appearance23;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this._grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this._grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(1057, 156);
            this._grid.TabIndex = 0;
            this._grid.Text = "ultraGrid1";
            this._grid.UseAppStyling = false;
            this._grid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.CellChange);
            this._grid.BeforeCellActivate += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.BeforeCellActivate);
            this._grid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.DoubleClickRow);
            this._grid.BeforeHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.BeforeHeaderCheckStateChangedEventHandler(this.BeforeHeaderCheckStateChanged);
            this._grid.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.AfterGroupCheckStateChanged);
            this._grid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GridMouseUp);
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models.DeclarationListViewModel);
            // 
            // _contextMenu
            // 
            this._contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openTaxpayerCardToolStripMenuItem,
            this._openDeclarationMenuItem,
            this._openJournalMenuItem});
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(196, 70);
            // 
            // openTaxpayerCardToolStripMenuItem
            // 
            this.openTaxpayerCardToolStripMenuItem.Name = "openTaxpayerCardToolStripMenuItem";
            this.openTaxpayerCardToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openTaxpayerCardToolStripMenuItem.Text = "Открыть карточку НП";
            this.openTaxpayerCardToolStripMenuItem.Click += new System.EventHandler(this.OpenTaxPayerCardClick);
            // 
            // _openDeclarationMenuItem
            // 
            this._openDeclarationMenuItem.Name = "_openDeclarationMenuItem";
            this._openDeclarationMenuItem.Size = new System.Drawing.Size(195, 22);
            this._openDeclarationMenuItem.Text = "Открыть декларацию";
            this._openDeclarationMenuItem.Click += new System.EventHandler(this.OnOpenDeclarationCardClick);
            // 
            // _openJournalMenuItem
            // 
            this._openJournalMenuItem.Name = "_openJournalMenuItem";
            this._openJournalMenuItem.Size = new System.Drawing.Size(195, 22);
            this._openJournalMenuItem.Text = "Открыть журнал";
            this._openJournalMenuItem.Click += new System.EventHandler(this.OnOpenDeclarationCardClick);
            // 
            // DeclarationListGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this._grid);
            this.Name = "DeclarationListGridView";
            this.Size = new System.Drawing.Size(1057, 156);
            this.Load += new System.EventHandler(this.DeclarationListGridViewLoad);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this._contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterProvider;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.ToolStripMenuItem openTaxpayerCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _openDeclarationMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _openJournalMenuItem;
    }
}
