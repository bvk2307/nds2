﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    partial class DiscrepancyListGridView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("SelectionDiscrepancyModel", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Included");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FoundDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CreatedByInvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CreatedByInvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyType");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Subtype");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CompareRule");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Descending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrepancyId");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SidePrimaryProcessing");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerRegionCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerRegionName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerSonoCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerSonoName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInnDeclarant");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerControlRatio");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDeclarationSign");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDeclarationSubmitDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDeclarationNdsAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDeclarationPeriod");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerOperation");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerChapter");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerSur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerRegionCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerRegionName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerSonoCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerSonoName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInnDeclarant");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerControlRatio");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDeclarationSign");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDeclarationSubmitDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDeclarationNdsAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDeclarationPeriod");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerChapter");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerSur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn38 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKppDeclarant");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKppDeclarant");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ExcludeFromClaimType");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 50954891);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 50954892);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 50954893);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 50954894);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 50954895);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 50954896);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup6", 50954897);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup7", 50954898);
            Infragistics.Win.UltraWinGrid.RowLayout rowLayout1 = new Infragistics.Win.UltraWinGrid.RowLayout("tttt");
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo1 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Included", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo2 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "FoundDate", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo3 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CreatedByInvoiceNumber", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo4 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CreatedByInvoiceDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo5 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyType", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo6 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Subtype", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo7 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CompareRule", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo8 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Amount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo9 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DiscrepancyId", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo10 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SidePrimaryProcessing", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo11 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerRegionCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo12 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerRegionName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo13 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerSonoCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo14 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerSonoName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo15 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo16 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerInnDeclarant", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo17 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerControlRatio", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo18 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDeclarationSign", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo19 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDeclarationSubmitDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo20 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDeclarationNdsAmount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo21 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDeclarationPeriod", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo22 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerOperation", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo23 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerChapter", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo24 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerSur", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo25 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerRegionCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo26 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerRegionName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo27 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerSonoCode", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo28 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerSonoName", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo29 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo30 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerInnDeclarant", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo31 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerControlRatio", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo32 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDeclarationSign", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo33 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDeclarationSubmitDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo34 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDeclarationNdsAmount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo35 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDeclarationPeriod", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo36 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerChapter", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo37 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerSur", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo38 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo39 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerKppDeclarant", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo40 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo41 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerKppDeclarant", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo42 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ExcludeFromClaimType", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo43 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup0", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo44 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup1", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo45 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup2", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo46 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup3", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo47 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup4", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo48 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup5", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo49 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup6", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo50 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup7", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._filterProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openBuyerCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSellerCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._excelExporter = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this._contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            ultraGridColumn1.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn1.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn1.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn1.Header.Caption = "";
            ultraGridColumn1.Header.ToolTipText = "Флаг выбора записи";
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn1.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn1.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 113);
            ultraGridColumn1.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn1.RowLayoutColumnInfo.SpanY = 8;
            ultraGridColumn2.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn2.Header.VisiblePosition = 37;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn3.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn3.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn3.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn3.Header.Caption = "№ СФ";
            ultraGridColumn3.Header.ToolTipText = "Номер счета-фактуры";
            ultraGridColumn3.Header.VisiblePosition = 1;
            ultraGridColumn3.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn3.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn3.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn3.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn3.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn3.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn3.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn3.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn4.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn4.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn4.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo)));
            ultraGridColumn4.Header.ToolTipText = "Дата счета-фактуры";
            ultraGridColumn4.Header.VisiblePosition = 2;
            ultraGridColumn4.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn4.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn4.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn4.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn4.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn4.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn4.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn5.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn5.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn5.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn5.Header.ToolTipText = "Вид расхождения";
            ultraGridColumn5.Header.VisiblePosition = 3;
            ultraGridColumn5.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn5.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn5.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn5.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn5.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn5.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn5.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn5.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn6.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn6.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn6.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn6.Header.ToolTipText = "Подвид расхождения";
            ultraGridColumn6.Header.VisiblePosition = 4;
            ultraGridColumn6.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn6.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn6.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn6.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn6.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn6.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn6.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn6.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn7.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn7.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn7.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn7.Header.ToolTipText = "Номер правила сопоставления";
            ultraGridColumn7.Header.VisiblePosition = 5;
            ultraGridColumn7.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn7.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn7.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn7.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn7.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn7.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn7.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn8.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn8.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn8.Format = "N2";
            ultraGridColumn8.Header.ToolTipText = "Сумма расхождения в руб.";
            ultraGridColumn8.Header.VisiblePosition = 6;
            ultraGridColumn8.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn8.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn8.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn8.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn8.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn9.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn9.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn9.Header.ToolTipText = "Идентификатор расхождения";
            ultraGridColumn9.Header.VisiblePosition = 7;
            ultraGridColumn9.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn9.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn9.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn9.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn9.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn9.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn9.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn9.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn10.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn10.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn10.Header.ToolTipText = "Сторона первичной отработки";
            ultraGridColumn10.Header.VisiblePosition = 8;
            ultraGridColumn10.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn10.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn10.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 93);
            ultraGridColumn10.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn10.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn10.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn11.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn11.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn11.Header.ToolTipText = "Код региона покупателя";
            ultraGridColumn11.Header.VisiblePosition = 9;
            ultraGridColumn11.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn11.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn11.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn11.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn11.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn11.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn12.Header.ToolTipText = "Наименование региона покупателя";
            ultraGridColumn12.Header.VisiblePosition = 10;
            ultraGridColumn12.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn12.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn12.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn12.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn12.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn12.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn12.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn13.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn13.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn13.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn13.Header.ToolTipText = "Код инспекции покупателя";
            ultraGridColumn13.Header.VisiblePosition = 11;
            ultraGridColumn13.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn13.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn13.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn13.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn13.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn13.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn14.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn14.Header.ToolTipText = "Наименование инспекции покупателя";
            ultraGridColumn14.Header.VisiblePosition = 12;
            ultraGridColumn14.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn14.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn14.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn14.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn14.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn14.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn14.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn15.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn15.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn15.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn15.Header.ToolTipText = "ИНН покупателя";
            ultraGridColumn15.Header.VisiblePosition = 13;
            ultraGridColumn15.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn15.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn15.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn15.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn15.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn15.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn15.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn16.Header.VisiblePosition = 38;
            ultraGridColumn16.Hidden = true;
            ultraGridColumn16.RowLayoutColumnInfo.OriginX = 74;
            ultraGridColumn16.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn16.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn16.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn17.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn17.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn17.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn17.Header.ToolTipText = "Левая часть КС 1.28 должна равняться левой части КС 1.33 (или 1.32)";
            ultraGridColumn17.Header.VisiblePosition = 15;
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn17.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn17.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn17.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn17.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn18.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn18.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn18.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn18.Header.ToolTipText = "Признак декларации покупателя";
            ultraGridColumn18.Header.VisiblePosition = 16;
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn18.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn18.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn19.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn19.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn19.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo)));
            ultraGridColumn19.Header.ToolTipText = "Дата представления покупателем декларации/журнала в НО";
            ultraGridColumn19.Header.VisiblePosition = 17;
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn19.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn20.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn20.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn20.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn20.Format = "N2";
            ultraGridColumn20.Header.ToolTipText = "Сумма НДМ по НД покупателя";
            ultraGridColumn20.Header.VisiblePosition = 18;
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn20.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn21.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn21.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn21.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn21.Header.ToolTipText = "Период, за который подается декларация/журнал покупателя";
            ultraGridColumn21.Header.VisiblePosition = 19;
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn21.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn22.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn22.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn22.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn22.Header.ToolTipText = "Код вида операции покупателя";
            ultraGridColumn22.Header.VisiblePosition = 20;
            ultraGridColumn22.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn22.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn22.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn22.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn22.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn23.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn23.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn23.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn23.Header.ToolTipText = "Источник записи о СФ покупателя";
            ultraGridColumn23.Header.VisiblePosition = 21;
            ultraGridColumn23.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn23.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn23.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn23.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn23.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn24.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn24.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn24.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn24.Header.ToolTipText = "Показатель СУР покупателя ";
            ultraGridColumn24.Header.VisiblePosition = 22;
            ultraGridColumn24.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn24.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn24.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn24.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn24.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn24.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn24.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn25.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn25.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn25.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)(((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotMatch) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn25.Header.ToolTipText = "Код региона продавца";
            ultraGridColumn25.Header.VisiblePosition = 32;
            ultraGridColumn25.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn25.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupKey = "NewGroup6";
            ultraGridColumn25.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridColumn25.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn26.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn26.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn26.Header.ToolTipText = "Наименование региона продавца";
            ultraGridColumn26.Header.VisiblePosition = 33;
            ultraGridColumn26.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupKey = "NewGroup6";
            ultraGridColumn26.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridColumn26.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn26.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn27.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn27.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn27.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn27.Header.ToolTipText = "Код инспекции продавца";
            ultraGridColumn27.Header.VisiblePosition = 34;
            ultraGridColumn27.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn27.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn27.RowLayoutColumnInfo.ParentGroupIndex = 7;
            ultraGridColumn27.RowLayoutColumnInfo.ParentGroupKey = "NewGroup7";
            ultraGridColumn27.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridColumn27.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn27.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn28.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn28.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn28.Header.ToolTipText = "Наименование инспекции продавца";
            ultraGridColumn28.Header.VisiblePosition = 35;
            ultraGridColumn28.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn28.RowLayoutColumnInfo.ParentGroupIndex = 7;
            ultraGridColumn28.RowLayoutColumnInfo.ParentGroupKey = "NewGroup7";
            ultraGridColumn28.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridColumn28.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn28.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn29.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn29.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn29.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn29.Header.ToolTipText = "ИНН продавца";
            ultraGridColumn29.Header.VisiblePosition = 23;
            ultraGridColumn29.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn29.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn29.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn29.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn29.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn30.Header.VisiblePosition = 39;
            ultraGridColumn30.Hidden = true;
            ultraGridColumn31.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn31.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn31.Header.ToolTipText = "Признак наличия расхождения по КС 1.27 на стороне продавца";
            ultraGridColumn31.Header.VisiblePosition = 25;
            ultraGridColumn31.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn31.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn31.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn31.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn31.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn31.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn31.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn31.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn32.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn32.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn32.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn32.Header.ToolTipText = "Признак декларации продавца";
            ultraGridColumn32.Header.VisiblePosition = 26;
            ultraGridColumn32.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn32.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn32.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn32.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn32.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn32.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn32.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn32.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn33.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn33.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn33.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo)));
            ultraGridColumn33.Header.ToolTipText = "Дата представления продавцом декларации /журнала в НО";
            ultraGridColumn33.Header.VisiblePosition = 27;
            ultraGridColumn33.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn33.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn33.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn33.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn33.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn33.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn33.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn33.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn34.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn34.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn34.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn34.Format = "N2";
            ultraGridColumn34.Header.ToolTipText = "Сумма НДС по НД продавца";
            ultraGridColumn34.Header.VisiblePosition = 28;
            ultraGridColumn34.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn34.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn34.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn34.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn34.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn34.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn34.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn35.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn35.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn35.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn35.Header.ToolTipText = "Период, за который подается декларация/журнал продавца";
            ultraGridColumn35.Header.VisiblePosition = 29;
            ultraGridColumn35.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn35.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn35.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn35.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn35.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn35.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn36.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn36.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn36.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn36.Header.ToolTipText = "Источник записи о СФ продавца";
            ultraGridColumn36.Header.VisiblePosition = 30;
            ultraGridColumn36.HiddenWhenGroupBy = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn36.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn36.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn36.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn36.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn36.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn36.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn36.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn37.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn37.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn37.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals)));
            ultraGridColumn37.Header.ToolTipText = "Показатель СУР продавца";
            ultraGridColumn37.Header.VisiblePosition = 31;
            ultraGridColumn37.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn37.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn37.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn37.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn37.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn37.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn37.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn37.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn38.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn38.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn38.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn38.Header.ToolTipText = "КПП покупателя";
            ultraGridColumn38.Header.VisiblePosition = 14;
            ultraGridColumn38.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn38.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn38.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn38.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn38.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 52);
            ultraGridColumn38.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn38.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn38.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn39.Header.VisiblePosition = 40;
            ultraGridColumn39.Hidden = true;
            ultraGridColumn39.RowLayoutColumnInfo.OriginX = 76;
            ultraGridColumn39.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn39.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn40.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn40.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn40.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            ultraGridColumn40.Header.ToolTipText = "КПП продавца";
            ultraGridColumn40.Header.VisiblePosition = 24;
            ultraGridColumn40.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn40.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn40.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn40.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn41.Header.VisiblePosition = 41;
            ultraGridColumn41.Hidden = true;
            ultraGridColumn42.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn42.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn42.Header.Caption = "Исключено из АТ";
            ultraGridColumn42.Header.VisiblePosition = 36;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 72;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 113);
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 8;
            ultraGridColumn42.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn22,
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn38,
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42});
            ultraGridGroup1.Header.Caption = "Расхождение";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 14;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 8;
            ultraGridGroup2.Header.Caption = "Стороны отработки";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 16;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 56;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 8;
            ultraGridGroup3.Header.Caption = "Покупатель";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupIndex = 1;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupKey = "NewGroup1";
            ultraGridGroup3.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 29);
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 28;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 6;
            ultraGridGroup4.Header.Caption = "Продавец";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 30;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupIndex = 1;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupKey = "NewGroup1";
            ultraGridGroup4.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 29);
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 26;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 6;
            ultraGridGroup5.Header.Caption = "Регион";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupIndex = 2;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupKey = "NewGroup2";
            ultraGridGroup5.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 4;
            ultraGridGroup6.Header.Caption = "Инспекция";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupIndex = 2;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupKey = "NewGroup2";
            ultraGridGroup6.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 32);
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 4;
            ultraGridGroup7.Header.Caption = "Регион";
            ultraGridGroup7.Key = "NewGroup6";
            ultraGridGroup7.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupIndex = 3;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupKey = "NewGroup3";
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 4;
            ultraGridGroup8.Header.Caption = "Инспекция";
            ultraGridGroup8.Key = "NewGroup7";
            ultraGridGroup8.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 22;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup8.RowLayoutGroupInfo.ParentGroupIndex = 3;
            ultraGridGroup8.RowLayoutGroupInfo.ParentGroupKey = "NewGroup3";
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 4;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8});
            rowLayoutColumnInfo1.OriginX = 0;
            rowLayoutColumnInfo1.OriginY = 0;
            rowLayoutColumnInfo1.PreferredLabelSize = new System.Drawing.Size(0, 113);
            rowLayoutColumnInfo1.SpanX = 2;
            rowLayoutColumnInfo1.SpanY = 8;
            rowLayoutColumnInfo3.OriginX = 0;
            rowLayoutColumnInfo3.OriginY = 0;
            rowLayoutColumnInfo3.ParentGroupIndex = 0;
            rowLayoutColumnInfo3.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo3.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo3.SpanX = 2;
            rowLayoutColumnInfo3.SpanY = 6;
            rowLayoutColumnInfo4.OriginX = 2;
            rowLayoutColumnInfo4.OriginY = 0;
            rowLayoutColumnInfo4.ParentGroupIndex = 0;
            rowLayoutColumnInfo4.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo4.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo4.SpanX = 2;
            rowLayoutColumnInfo4.SpanY = 6;
            rowLayoutColumnInfo5.OriginX = 4;
            rowLayoutColumnInfo5.OriginY = 0;
            rowLayoutColumnInfo5.ParentGroupIndex = 0;
            rowLayoutColumnInfo5.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo5.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo5.SpanX = 2;
            rowLayoutColumnInfo5.SpanY = 6;
            rowLayoutColumnInfo6.OriginX = 6;
            rowLayoutColumnInfo6.OriginY = 0;
            rowLayoutColumnInfo6.ParentGroupIndex = 0;
            rowLayoutColumnInfo6.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo6.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo6.SpanX = 2;
            rowLayoutColumnInfo6.SpanY = 6;
            rowLayoutColumnInfo7.OriginX = 8;
            rowLayoutColumnInfo7.OriginY = 0;
            rowLayoutColumnInfo7.ParentGroupIndex = 0;
            rowLayoutColumnInfo7.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo7.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo7.SpanX = 2;
            rowLayoutColumnInfo7.SpanY = 6;
            rowLayoutColumnInfo8.OriginX = 10;
            rowLayoutColumnInfo8.OriginY = 0;
            rowLayoutColumnInfo8.ParentGroupIndex = 0;
            rowLayoutColumnInfo8.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo8.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo8.SpanX = 2;
            rowLayoutColumnInfo8.SpanY = 6;
            rowLayoutColumnInfo9.OriginX = 12;
            rowLayoutColumnInfo9.OriginY = 0;
            rowLayoutColumnInfo9.ParentGroupIndex = 0;
            rowLayoutColumnInfo9.ParentGroupKey = "NewGroup0";
            rowLayoutColumnInfo9.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo9.SpanX = 2;
            rowLayoutColumnInfo9.SpanY = 6;
            rowLayoutColumnInfo10.OriginX = 0;
            rowLayoutColumnInfo10.OriginY = 0;
            rowLayoutColumnInfo10.ParentGroupIndex = 1;
            rowLayoutColumnInfo10.ParentGroupKey = "NewGroup1";
            rowLayoutColumnInfo10.PreferredLabelSize = new System.Drawing.Size(0, 93);
            rowLayoutColumnInfo10.SpanX = 2;
            rowLayoutColumnInfo10.SpanY = 6;
            rowLayoutColumnInfo11.OriginX = 0;
            rowLayoutColumnInfo11.OriginY = 0;
            rowLayoutColumnInfo11.ParentGroupIndex = 4;
            rowLayoutColumnInfo11.ParentGroupKey = "NewGroup4";
            rowLayoutColumnInfo11.SpanX = 2;
            rowLayoutColumnInfo11.SpanY = 2;
            rowLayoutColumnInfo12.OriginX = 2;
            rowLayoutColumnInfo12.OriginY = 0;
            rowLayoutColumnInfo12.ParentGroupIndex = 4;
            rowLayoutColumnInfo12.ParentGroupKey = "NewGroup4";
            rowLayoutColumnInfo12.SpanX = 2;
            rowLayoutColumnInfo12.SpanY = 2;
            rowLayoutColumnInfo13.OriginX = 0;
            rowLayoutColumnInfo13.OriginY = 0;
            rowLayoutColumnInfo13.ParentGroupIndex = 5;
            rowLayoutColumnInfo13.ParentGroupKey = "NewGroup5";
            rowLayoutColumnInfo13.SpanX = 2;
            rowLayoutColumnInfo13.SpanY = 2;
            rowLayoutColumnInfo14.OriginX = 2;
            rowLayoutColumnInfo14.OriginY = 0;
            rowLayoutColumnInfo14.ParentGroupIndex = 5;
            rowLayoutColumnInfo14.ParentGroupKey = "NewGroup5";
            rowLayoutColumnInfo14.SpanX = 2;
            rowLayoutColumnInfo14.SpanY = 2;
            rowLayoutColumnInfo15.OriginX = 8;
            rowLayoutColumnInfo15.OriginY = 0;
            rowLayoutColumnInfo15.ParentGroupIndex = 2;
            rowLayoutColumnInfo15.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo15.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo15.SpanX = 2;
            rowLayoutColumnInfo15.SpanY = 4;
            rowLayoutColumnInfo16.OriginX = 74;
            rowLayoutColumnInfo16.OriginY = 0;
            rowLayoutColumnInfo16.SpanX = 2;
            rowLayoutColumnInfo16.SpanY = 2;
            rowLayoutColumnInfo17.OriginX = 12;
            rowLayoutColumnInfo17.OriginY = 0;
            rowLayoutColumnInfo17.ParentGroupIndex = 2;
            rowLayoutColumnInfo17.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo17.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo17.SpanX = 2;
            rowLayoutColumnInfo17.SpanY = 4;
            rowLayoutColumnInfo18.OriginX = 14;
            rowLayoutColumnInfo18.OriginY = 0;
            rowLayoutColumnInfo18.ParentGroupIndex = 2;
            rowLayoutColumnInfo18.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo18.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo18.SpanX = 2;
            rowLayoutColumnInfo18.SpanY = 4;
            rowLayoutColumnInfo19.OriginX = 16;
            rowLayoutColumnInfo19.OriginY = 0;
            rowLayoutColumnInfo19.ParentGroupIndex = 2;
            rowLayoutColumnInfo19.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo19.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo19.SpanX = 2;
            rowLayoutColumnInfo19.SpanY = 4;
            rowLayoutColumnInfo20.OriginX = 18;
            rowLayoutColumnInfo20.OriginY = 0;
            rowLayoutColumnInfo20.ParentGroupIndex = 2;
            rowLayoutColumnInfo20.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo20.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo20.SpanX = 2;
            rowLayoutColumnInfo20.SpanY = 4;
            rowLayoutColumnInfo21.OriginX = 20;
            rowLayoutColumnInfo21.OriginY = 0;
            rowLayoutColumnInfo21.ParentGroupIndex = 2;
            rowLayoutColumnInfo21.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo21.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo21.SpanX = 2;
            rowLayoutColumnInfo21.SpanY = 4;
            rowLayoutColumnInfo22.OriginX = 22;
            rowLayoutColumnInfo22.OriginY = 0;
            rowLayoutColumnInfo22.ParentGroupIndex = 2;
            rowLayoutColumnInfo22.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo22.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo22.SpanX = 2;
            rowLayoutColumnInfo22.SpanY = 4;
            rowLayoutColumnInfo23.OriginX = 24;
            rowLayoutColumnInfo23.OriginY = 0;
            rowLayoutColumnInfo23.ParentGroupIndex = 2;
            rowLayoutColumnInfo23.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo23.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo23.SpanX = 2;
            rowLayoutColumnInfo23.SpanY = 4;
            rowLayoutColumnInfo24.OriginX = 26;
            rowLayoutColumnInfo24.OriginY = 0;
            rowLayoutColumnInfo24.ParentGroupIndex = 2;
            rowLayoutColumnInfo24.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo24.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo24.SpanX = 2;
            rowLayoutColumnInfo24.SpanY = 4;
            rowLayoutColumnInfo25.OriginX = 0;
            rowLayoutColumnInfo25.OriginY = 0;
            rowLayoutColumnInfo25.ParentGroupIndex = 6;
            rowLayoutColumnInfo25.ParentGroupKey = "NewGroup6";
            rowLayoutColumnInfo25.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo25.SpanX = 2;
            rowLayoutColumnInfo25.SpanY = 2;
            rowLayoutColumnInfo26.OriginX = 2;
            rowLayoutColumnInfo26.OriginY = 0;
            rowLayoutColumnInfo26.ParentGroupIndex = 6;
            rowLayoutColumnInfo26.ParentGroupKey = "NewGroup6";
            rowLayoutColumnInfo26.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo26.SpanX = 2;
            rowLayoutColumnInfo26.SpanY = 2;
            rowLayoutColumnInfo27.OriginX = 0;
            rowLayoutColumnInfo27.OriginY = 0;
            rowLayoutColumnInfo27.ParentGroupIndex = 7;
            rowLayoutColumnInfo27.ParentGroupKey = "NewGroup7";
            rowLayoutColumnInfo27.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo27.SpanX = 2;
            rowLayoutColumnInfo27.SpanY = 2;
            rowLayoutColumnInfo28.OriginX = 2;
            rowLayoutColumnInfo28.OriginY = 0;
            rowLayoutColumnInfo28.ParentGroupIndex = 7;
            rowLayoutColumnInfo28.ParentGroupKey = "NewGroup7";
            rowLayoutColumnInfo28.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo28.SpanX = 2;
            rowLayoutColumnInfo28.SpanY = 2;
            rowLayoutColumnInfo29.OriginX = 0;
            rowLayoutColumnInfo29.OriginY = 0;
            rowLayoutColumnInfo29.ParentGroupIndex = 3;
            rowLayoutColumnInfo29.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo29.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo29.SpanX = 2;
            rowLayoutColumnInfo29.SpanY = 4;
            rowLayoutColumnInfo31.OriginX = 4;
            rowLayoutColumnInfo31.OriginY = 0;
            rowLayoutColumnInfo31.ParentGroupIndex = 3;
            rowLayoutColumnInfo31.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo31.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo31.SpanX = 2;
            rowLayoutColumnInfo31.SpanY = 4;
            rowLayoutColumnInfo32.OriginX = 6;
            rowLayoutColumnInfo32.OriginY = 0;
            rowLayoutColumnInfo32.ParentGroupIndex = 3;
            rowLayoutColumnInfo32.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo32.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo32.SpanX = 2;
            rowLayoutColumnInfo32.SpanY = 4;
            rowLayoutColumnInfo33.OriginX = 8;
            rowLayoutColumnInfo33.OriginY = 0;
            rowLayoutColumnInfo33.ParentGroupIndex = 3;
            rowLayoutColumnInfo33.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo33.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo33.SpanX = 2;
            rowLayoutColumnInfo33.SpanY = 4;
            rowLayoutColumnInfo34.OriginX = 10;
            rowLayoutColumnInfo34.OriginY = 0;
            rowLayoutColumnInfo34.ParentGroupIndex = 3;
            rowLayoutColumnInfo34.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo34.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo34.SpanX = 2;
            rowLayoutColumnInfo34.SpanY = 4;
            rowLayoutColumnInfo35.OriginX = 12;
            rowLayoutColumnInfo35.OriginY = 0;
            rowLayoutColumnInfo35.ParentGroupIndex = 3;
            rowLayoutColumnInfo35.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo35.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo35.SpanX = 2;
            rowLayoutColumnInfo35.SpanY = 4;
            rowLayoutColumnInfo36.OriginX = 14;
            rowLayoutColumnInfo36.OriginY = 0;
            rowLayoutColumnInfo36.ParentGroupIndex = 3;
            rowLayoutColumnInfo36.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo36.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo36.SpanX = 2;
            rowLayoutColumnInfo36.SpanY = 4;
            rowLayoutColumnInfo37.OriginX = 16;
            rowLayoutColumnInfo37.OriginY = 0;
            rowLayoutColumnInfo37.ParentGroupIndex = 3;
            rowLayoutColumnInfo37.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo37.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo37.SpanX = 2;
            rowLayoutColumnInfo37.SpanY = 4;
            rowLayoutColumnInfo38.OriginX = 10;
            rowLayoutColumnInfo38.OriginY = 0;
            rowLayoutColumnInfo38.ParentGroupIndex = 2;
            rowLayoutColumnInfo38.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo38.PreferredLabelSize = new System.Drawing.Size(0, 52);
            rowLayoutColumnInfo38.SpanX = 2;
            rowLayoutColumnInfo38.SpanY = 4;
            rowLayoutColumnInfo39.OriginX = 76;
            rowLayoutColumnInfo39.OriginY = 0;
            rowLayoutColumnInfo39.SpanX = 2;
            rowLayoutColumnInfo39.SpanY = 2;
            rowLayoutColumnInfo40.OriginX = 2;
            rowLayoutColumnInfo40.OriginY = 0;
            rowLayoutColumnInfo40.ParentGroupIndex = 3;
            rowLayoutColumnInfo40.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo40.PreferredLabelSize = new System.Drawing.Size(0, 64);
            rowLayoutColumnInfo40.SpanX = 2;
            rowLayoutColumnInfo40.SpanY = 4;
            rowLayoutColumnInfo42.OriginX = 72;
            rowLayoutColumnInfo42.OriginY = 0;
            rowLayoutColumnInfo42.PreferredLabelSize = new System.Drawing.Size(0, 113);
            rowLayoutColumnInfo42.SpanX = 2;
            rowLayoutColumnInfo42.SpanY = 8;
            rowLayoutColumnInfo43.LabelSpan = 1;
            rowLayoutColumnInfo43.OriginX = 2;
            rowLayoutColumnInfo43.OriginY = 0;
            rowLayoutColumnInfo43.SpanX = 14;
            rowLayoutColumnInfo43.SpanY = 8;
            rowLayoutColumnInfo44.LabelSpan = 1;
            rowLayoutColumnInfo44.OriginX = 16;
            rowLayoutColumnInfo44.OriginY = 0;
            rowLayoutColumnInfo44.SpanX = 56;
            rowLayoutColumnInfo44.SpanY = 8;
            rowLayoutColumnInfo45.LabelSpan = 1;
            rowLayoutColumnInfo45.OriginX = 2;
            rowLayoutColumnInfo45.OriginY = 0;
            rowLayoutColumnInfo45.ParentGroupIndex = 1;
            rowLayoutColumnInfo45.ParentGroupKey = "NewGroup1";
            rowLayoutColumnInfo45.PreferredLabelSize = new System.Drawing.Size(0, 29);
            rowLayoutColumnInfo45.SpanX = 28;
            rowLayoutColumnInfo45.SpanY = 6;
            rowLayoutColumnInfo46.LabelSpan = 1;
            rowLayoutColumnInfo46.OriginX = 30;
            rowLayoutColumnInfo46.OriginY = 0;
            rowLayoutColumnInfo46.ParentGroupIndex = 1;
            rowLayoutColumnInfo46.ParentGroupKey = "NewGroup1";
            rowLayoutColumnInfo46.PreferredLabelSize = new System.Drawing.Size(0, 29);
            rowLayoutColumnInfo46.SpanX = 26;
            rowLayoutColumnInfo46.SpanY = 6;
            rowLayoutColumnInfo47.LabelSpan = 1;
            rowLayoutColumnInfo47.OriginX = 0;
            rowLayoutColumnInfo47.OriginY = 0;
            rowLayoutColumnInfo47.ParentGroupIndex = 2;
            rowLayoutColumnInfo47.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo47.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo47.SpanX = 4;
            rowLayoutColumnInfo47.SpanY = 4;
            rowLayoutColumnInfo48.LabelSpan = 1;
            rowLayoutColumnInfo48.OriginX = 4;
            rowLayoutColumnInfo48.OriginY = 0;
            rowLayoutColumnInfo48.ParentGroupIndex = 2;
            rowLayoutColumnInfo48.ParentGroupKey = "NewGroup2";
            rowLayoutColumnInfo48.PreferredLabelSize = new System.Drawing.Size(0, 32);
            rowLayoutColumnInfo48.SpanX = 4;
            rowLayoutColumnInfo48.SpanY = 4;
            rowLayoutColumnInfo49.LabelSpan = 1;
            rowLayoutColumnInfo49.OriginX = 18;
            rowLayoutColumnInfo49.OriginY = 0;
            rowLayoutColumnInfo49.ParentGroupIndex = 3;
            rowLayoutColumnInfo49.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo49.SpanX = 4;
            rowLayoutColumnInfo49.SpanY = 4;
            rowLayoutColumnInfo50.LabelSpan = 1;
            rowLayoutColumnInfo50.OriginX = 22;
            rowLayoutColumnInfo50.OriginY = 0;
            rowLayoutColumnInfo50.ParentGroupIndex = 3;
            rowLayoutColumnInfo50.ParentGroupKey = "NewGroup3";
            rowLayoutColumnInfo50.SpanX = 4;
            rowLayoutColumnInfo50.SpanY = 4;
            rowLayout1.ColumnInfos.AddRange(new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo[] {
            rowLayoutColumnInfo1,
            rowLayoutColumnInfo2,
            rowLayoutColumnInfo3,
            rowLayoutColumnInfo4,
            rowLayoutColumnInfo5,
            rowLayoutColumnInfo6,
            rowLayoutColumnInfo7,
            rowLayoutColumnInfo8,
            rowLayoutColumnInfo9,
            rowLayoutColumnInfo10,
            rowLayoutColumnInfo11,
            rowLayoutColumnInfo12,
            rowLayoutColumnInfo13,
            rowLayoutColumnInfo14,
            rowLayoutColumnInfo15,
            rowLayoutColumnInfo16,
            rowLayoutColumnInfo17,
            rowLayoutColumnInfo18,
            rowLayoutColumnInfo19,
            rowLayoutColumnInfo20,
            rowLayoutColumnInfo21,
            rowLayoutColumnInfo22,
            rowLayoutColumnInfo23,
            rowLayoutColumnInfo24,
            rowLayoutColumnInfo25,
            rowLayoutColumnInfo26,
            rowLayoutColumnInfo27,
            rowLayoutColumnInfo28,
            rowLayoutColumnInfo29,
            rowLayoutColumnInfo30,
            rowLayoutColumnInfo31,
            rowLayoutColumnInfo32,
            rowLayoutColumnInfo33,
            rowLayoutColumnInfo34,
            rowLayoutColumnInfo35,
            rowLayoutColumnInfo36,
            rowLayoutColumnInfo37,
            rowLayoutColumnInfo38,
            rowLayoutColumnInfo39,
            rowLayoutColumnInfo40,
            rowLayoutColumnInfo41,
            rowLayoutColumnInfo42,
            rowLayoutColumnInfo43,
            rowLayoutColumnInfo44,
            rowLayoutColumnInfo45,
            rowLayoutColumnInfo46,
            rowLayoutColumnInfo47,
            rowLayoutColumnInfo48,
            rowLayoutColumnInfo49,
            rowLayoutColumnInfo50});
            rowLayout1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand1.RowLayouts.AddRange(new Infragistics.Win.UltraWinGrid.RowLayout[] {
            rowLayout1});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this._grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this._grid.DisplayLayout.MaxColScrollRegions = 1;
            this._grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this._grid.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this._grid.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this._grid.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.WithinGroup;
            this._grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._grid.DisplayLayout.Override.CellAppearance = appearance8;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.CellPadding = 0;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterProvider;
            this._grid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Center";
            this._grid.DisplayLayout.Override.HeaderAppearance = appearance10;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortMulti;
            this._grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this._grid.DisplayLayout.Override.RowAppearance = appearance11;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this._grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this._grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._grid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.Horizontal;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(1408, 249);
            this._grid.TabIndex = 0;
            this._grid.Text = "ultraGrid1";
            this._grid.UseAppStyling = false;
            this._grid.AfterCellActivate += new System.EventHandler(this.OnCellActivate);
            this._grid.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.OnSelectRow);
            this._grid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.DoubleClickRow);
            // 
            // _bindingSource
            // 
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Common.Models.ViewModels.SelectionDiscrepancyModel);
            // 
            // _contextMenu
            // 
            this._contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openBuyerCardToolStripMenuItem,
            this.openSellerCardToolStripMenuItem});
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(241, 48);
            // 
            // openBuyerCardToolStripMenuItem
            // 
            this.openBuyerCardToolStripMenuItem.Name = "openBuyerCardToolStripMenuItem";
            this.openBuyerCardToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.openBuyerCardToolStripMenuItem.Text = "Открыть карточку покупателя";
            this.openBuyerCardToolStripMenuItem.Click += new System.EventHandler(this.OnOpenBuyerCardClick);
            // 
            // openSellerCardToolStripMenuItem
            // 
            this.openSellerCardToolStripMenuItem.Name = "openSellerCardToolStripMenuItem";
            this.openSellerCardToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.openSellerCardToolStripMenuItem.Text = "Открыть карточку продавца";
            this.openSellerCardToolStripMenuItem.Click += new System.EventHandler(this.OnOpenSellerCardClick);
            // 
            // _excelExporter
            // 
            this._excelExporter.CellExported += new Infragistics.Win.UltraWinGrid.ExcelExport.CellExportedEventHandler(this.CellExported);
            // 
            // DiscrepancyListGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "DiscrepancyListGridView";
            this.Size = new System.Drawing.Size(1408, 249);
            this.Load += new System.EventHandler(this.DiscrepancyListGridViewLoad);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this._contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterProvider;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.ToolStripMenuItem openBuyerCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSellerCardToolStripMenuItem;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter _excelExporter;
    }
}
