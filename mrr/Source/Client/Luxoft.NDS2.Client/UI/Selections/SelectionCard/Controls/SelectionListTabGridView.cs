﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class SelectionListTabGridView : UserControl, IExtendedGridView
    {
        protected SelectionStatusColumnFilterView _statusColumnFilter;

        public SelectionListTabGridView()
        {
            InitializeComponent();
            GridEvents = new UltraGridEventsHandler(_grid);
            InitColumnsCollection();
            _grid.SetRowContextMenu(_contextMenu);
            _grid.SetBaseConfig();
        }

        /// <summary>
        ///     Возвращает выбранную строку
        /// </summary>
        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                    return null;

                return _grid.ActiveRow.ListObject;
            }
        }

        public bool ReadOnly
        {
            set
            {
                Columns.FindByKey(TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.IsSelected)).ReadOnly =
                    value;
            }
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns { get; protected set; }
        public IGridEventsHandler GridEvents { get; private set; }

        /// <summary>
        ///     Инициализирует коллекуию колонок грида
        /// </summary>
        public void InitColumnsCollection()
        {
            var defaultFilter = new ColumnFilter(_grid.DisplayLayout.Bands[0].Columns[
            TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.StatusId)], FilterLogicalOperator.Or);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.Approved);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.Draft);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.Loading);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.LoadingError);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.RequestForApproval);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.ClaimCreating);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, (int)SelectionStatus.ClaimCreated);
            defaultFilter.FilterConditions.Add(FilterComparisionOperator.Equals, 0);

            _grid.DisplayLayout.Bands[0].ColumnFilters[TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.StatusId)].LogicalOperator = defaultFilter.LogicalOperator;
            foreach (var condition in defaultFilter.FilterConditions)
            {
                _grid.DisplayLayout.Bands[0].ColumnFilters[TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.StatusId)]
                    .FilterConditions.Add((FilterCondition)condition);
            }

            _statusColumnFilter =
                new SelectionStatusColumnFilterView(
                    _grid.DisplayLayout.Bands[0].Columns[
                        TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.StatusId)], defaultFilter);

            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(new[]
                    {
                        TypeHelper<SelectionListItemViewModel>.GetMemberName(t=>t.TypeCode),
                        TypeHelper<SelectionListItemViewModel>.GetMemberName(t=>t.TemplateId),
                    }),
                    new GridColumnCreator()
                        .WithFilterViewFor(TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.StatusId),
                            column => _statusColumnFilter
                        )
                );
        }

        /// <summary>
        ///     Инициализирует фильтр колонок грида
        /// </summary>
        public void InitFilterView(DictionarySelectionStatus dictionarySelectionStatus)
        {
            _statusColumnFilter.DictionarySelectionStatus = dictionarySelectionStatus;
            StatusDictionaryColumnView.Apply(
                _grid.DisplayLayout.Bands[0].Columns[
                    TypeHelper<SelectionListItemViewModel>.GetMemberName(x => x.StatusId)], dictionarySelectionStatus);
        }

        public event EventHandler RowSelected;
        private void AfterRowActivate(object sender, System.EventArgs e)
        {
            if (RowSelected != null)
                RowSelected(sender, e);
        }

        public event EventHandler<SelectionListOpenedEventArgs> SelectionListLoaded;
        private void SelectionListTabGridViewLoad(object sender, EventArgs e)
        {
            if (SelectionListLoaded != null)
            {
                var args = new SelectionListOpenedEventArgs();
                SelectionListLoaded(sender, args);
                InitFilterView(args.DictionarySelectionStatus);
            }
        }

        public event EventHandler<EventArgs> SelectionChecked;
        private void CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.IsSelected) &&
                SelectionChecked != null)
                SelectionChecked(sender, e);
        }

        private void AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (e.Column.Key == TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.IsSelected) &&
                SelectionChecked != null)
                SelectionChecked(sender, e);
        }

        public event EventHandler RowDoubleClicked;
        private void DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (RowDoubleClicked != null)
                RowDoubleClicked(SelectedItem, e);
        }

        private void OpenMenuItemClick(object sender, EventArgs e)
        {
            if (RowDoubleClicked != null)
                RowDoubleClicked(SelectedItem, e);
        }
    }
}