﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    partial class SelectionCommonInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraLabel tbRegionCaption;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbNameCaption;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbCreationDateCaption;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbCountDeclarationsCaption;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbCountDiscrepanciesCaption;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbDiscrepancyAmountCaption;
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbChangeDateCaption;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbApproverCaption;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbAnalystCaption;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel tbStatusChangeDateCaption;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("SelectionRegionalBoundsViewModel", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SelectionTemplateId");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Include");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrTotalAmt");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrMinAmt");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrGapTotalAmt");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DiscrGapMinAmt");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HasChanges");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 25180340);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 25180418);
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            this.pnlSelectionChangeDataContent = new System.Windows.Forms.Panel();
            this._tbChangeDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbApprover = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbAnalyst = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbStatusChangeDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbRegion = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbCreationDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbCountDeclarations = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbCountDiscrepancies = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._tbDiscrepancyAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._pnlInfo1Content = new System.Windows.Forms.Panel();
            this.pnlSelectionSummaryContent = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._regionalBoundsGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._regionalBoundsLabel = new Infragistics.Win.Misc.UltraLabel();
            this._dataSource = new System.Windows.Forms.BindingSource(this.components);
            tbRegionCaption = new Infragistics.Win.Misc.UltraLabel();
            tbNameCaption = new Infragistics.Win.Misc.UltraLabel();
            tbCreationDateCaption = new Infragistics.Win.Misc.UltraLabel();
            tbCountDeclarationsCaption = new Infragistics.Win.Misc.UltraLabel();
            tbCountDiscrepanciesCaption = new Infragistics.Win.Misc.UltraLabel();
            tbDiscrepancyAmountCaption = new Infragistics.Win.Misc.UltraLabel();
            tbChangeDateCaption = new Infragistics.Win.Misc.UltraLabel();
            tbApproverCaption = new Infragistics.Win.Misc.UltraLabel();
            tbAnalystCaption = new Infragistics.Win.Misc.UltraLabel();
            tbStatusChangeDateCaption = new Infragistics.Win.Misc.UltraLabel();
            this.pnlSelectionChangeDataContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tbChangeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbApprover)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbAnalyst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbStatusChangeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCreationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCountDeclarations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCountDiscrepancies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbDiscrepancyAmount)).BeginInit();
            this._pnlInfo1Content.SuspendLayout();
            this.pnlSelectionSummaryContent.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._regionalBoundsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tbRegionCaption
            // 
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            tbRegionCaption.Appearance = appearance5;
            tbRegionCaption.Location = new System.Drawing.Point(4, 43);
            tbRegionCaption.Name = "tbRegionCaption";
            tbRegionCaption.Size = new System.Drawing.Size(90, 21);
            tbRegionCaption.TabIndex = 28;
            tbRegionCaption.Text = "Регион:";
            // 
            // tbNameCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            tbNameCaption.Appearance = appearance2;
            tbNameCaption.Location = new System.Drawing.Point(4, 23);
            tbNameCaption.Name = "tbNameCaption";
            tbNameCaption.Size = new System.Drawing.Size(90, 21);
            tbNameCaption.TabIndex = 22;
            tbNameCaption.Text = "Название:";
            // 
            // tbCreationDateCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            tbCreationDateCaption.Appearance = appearance4;
            tbCreationDateCaption.Location = new System.Drawing.Point(4, 3);
            tbCreationDateCaption.Name = "tbCreationDateCaption";
            tbCreationDateCaption.Size = new System.Drawing.Size(90, 21);
            tbCreationDateCaption.TabIndex = 26;
            tbCreationDateCaption.Text = "Дата создания:";
            // 
            // tbCountDeclarationsCaption
            // 
            appearance1.TextHAlignAsString = "Right";
            appearance1.TextVAlignAsString = "Middle";
            tbCountDeclarationsCaption.Appearance = appearance1;
            tbCountDeclarationsCaption.Location = new System.Drawing.Point(3, 7);
            tbCountDeclarationsCaption.Name = "tbCountDeclarationsCaption";
            tbCountDeclarationsCaption.Size = new System.Drawing.Size(195, 16);
            tbCountDeclarationsCaption.TabIndex = 40;
            tbCountDeclarationsCaption.Text = "Отобрано деклараций:";
            // 
            // tbCountDiscrepanciesCaption
            // 
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            tbCountDiscrepanciesCaption.Appearance = appearance6;
            tbCountDiscrepanciesCaption.Location = new System.Drawing.Point(3, 27);
            tbCountDiscrepanciesCaption.Name = "tbCountDiscrepanciesCaption";
            tbCountDiscrepanciesCaption.Size = new System.Drawing.Size(195, 16);
            tbCountDiscrepanciesCaption.TabIndex = 42;
            tbCountDiscrepanciesCaption.Text = "Отобрано расхождений:";
            // 
            // tbDiscrepancyAmountCaption
            // 
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            tbDiscrepancyAmountCaption.Appearance = appearance11;
            tbDiscrepancyAmountCaption.Location = new System.Drawing.Point(3, 47);
            tbDiscrepancyAmountCaption.Name = "tbDiscrepancyAmountCaption";
            tbDiscrepancyAmountCaption.Size = new System.Drawing.Size(195, 16);
            tbDiscrepancyAmountCaption.TabIndex = 44;
            tbDiscrepancyAmountCaption.Text = "Общая сумма расхождений (в руб.):";
            // 
            // tbChangeDateCaption
            // 
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            tbChangeDateCaption.Appearance = appearance8;
            tbChangeDateCaption.Location = new System.Drawing.Point(3, 6);
            tbChangeDateCaption.Name = "tbChangeDateCaption";
            tbChangeDateCaption.Size = new System.Drawing.Size(127, 16);
            tbChangeDateCaption.TabIndex = 56;
            tbChangeDateCaption.Text = "Дата редактирования:";
            // 
            // tbApproverCaption
            // 
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            tbApproverCaption.Appearance = appearance10;
            tbApproverCaption.Location = new System.Drawing.Point(3, 66);
            tbApproverCaption.Name = "tbApproverCaption";
            tbApproverCaption.Size = new System.Drawing.Size(127, 16);
            tbApproverCaption.TabIndex = 54;
            tbApproverCaption.Text = "Согласующий:";
            // 
            // tbAnalystCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            tbAnalystCaption.Appearance = appearance9;
            tbAnalystCaption.Location = new System.Drawing.Point(3, 46);
            tbAnalystCaption.Name = "tbAnalystCaption";
            tbAnalystCaption.Size = new System.Drawing.Size(127, 16);
            tbAnalystCaption.TabIndex = 52;
            tbAnalystCaption.Text = "Аналитик:";
            // 
            // tbStatusChangeDateCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            tbStatusChangeDateCaption.Appearance = appearance3;
            tbStatusChangeDateCaption.Location = new System.Drawing.Point(3, 26);
            tbStatusChangeDateCaption.Name = "tbStatusChangeDateCaption";
            tbStatusChangeDateCaption.Size = new System.Drawing.Size(127, 16);
            tbStatusChangeDateCaption.TabIndex = 50;
            tbStatusChangeDateCaption.Text = "Дата смены статуса:";
            // 
            // pnlSelectionChangeDataContent
            // 
            this.pnlSelectionChangeDataContent.BackColor = System.Drawing.Color.Transparent;
            this.pnlSelectionChangeDataContent.Controls.Add(this._tbChangeDate);
            this.pnlSelectionChangeDataContent.Controls.Add(tbChangeDateCaption);
            this.pnlSelectionChangeDataContent.Controls.Add(this._tbApprover);
            this.pnlSelectionChangeDataContent.Controls.Add(tbApproverCaption);
            this.pnlSelectionChangeDataContent.Controls.Add(this._tbAnalyst);
            this.pnlSelectionChangeDataContent.Controls.Add(tbAnalystCaption);
            this.pnlSelectionChangeDataContent.Controls.Add(this._tbStatusChangeDate);
            this.pnlSelectionChangeDataContent.Controls.Add(tbStatusChangeDateCaption);
            this.pnlSelectionChangeDataContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSelectionChangeDataContent.Location = new System.Drawing.Point(356, 3);
            this.pnlSelectionChangeDataContent.Name = "pnlSelectionChangeDataContent";
            this.pnlSelectionChangeDataContent.Size = new System.Drawing.Size(347, 94);
            this.pnlSelectionChangeDataContent.TabIndex = 52;
            // 
            // _tbChangeDate
            // 
            appearance19.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this._tbChangeDate.Appearance = appearance19;
            this._tbChangeDate.Location = new System.Drawing.Point(136, 3);
            this._tbChangeDate.Name = "_tbChangeDate";
            this._tbChangeDate.ReadOnly = true;
            this._tbChangeDate.Size = new System.Drawing.Size(252, 21);
            this._tbChangeDate.TabIndex = 57;
            this._tbChangeDate.Text = " ";
            // 
            // _tbApprover
            // 
            appearance22.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this._tbApprover.Appearance = appearance22;
            this._tbApprover.Location = new System.Drawing.Point(136, 63);
            this._tbApprover.Name = "_tbApprover";
            this._tbApprover.ReadOnly = true;
            this._tbApprover.Size = new System.Drawing.Size(252, 21);
            this._tbApprover.TabIndex = 55;
            this._tbApprover.Text = " ";
            // 
            // _tbAnalyst
            // 
            appearance21.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this._tbAnalyst.Appearance = appearance21;
            this._tbAnalyst.Location = new System.Drawing.Point(136, 43);
            this._tbAnalyst.Name = "_tbAnalyst";
            this._tbAnalyst.ReadOnly = true;
            this._tbAnalyst.Size = new System.Drawing.Size(252, 21);
            this._tbAnalyst.TabIndex = 53;
            this._tbAnalyst.Text = " ";
            // 
            // _tbStatusChangeDate
            // 
            appearance20.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this._tbStatusChangeDate.Appearance = appearance20;
            this._tbStatusChangeDate.Location = new System.Drawing.Point(136, 23);
            this._tbStatusChangeDate.Name = "_tbStatusChangeDate";
            this._tbStatusChangeDate.ReadOnly = true;
            this._tbStatusChangeDate.Size = new System.Drawing.Size(252, 21);
            this._tbStatusChangeDate.TabIndex = 51;
            this._tbStatusChangeDate.Text = " ";
            // 
            // _tbRegion
            // 
            appearance14.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this._tbRegion.Appearance = appearance14;
            this._tbRegion.Location = new System.Drawing.Point(100, 50);
            this._tbRegion.Multiline = true;
            this._tbRegion.Name = "_tbRegion";
            this._tbRegion.ReadOnly = true;
            this._tbRegion.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this._tbRegion.Size = new System.Drawing.Size(250, 50);
            this._tbRegion.TabIndex = 29;
            this._tbRegion.Text = " ";
            // 
            // _tbName
            // 
            appearance13.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this._tbName.Appearance = appearance13;
            this._tbName.Location = new System.Drawing.Point(100, 26);
            this._tbName.Name = "_tbName";
            this._tbName.Size = new System.Drawing.Size(250, 21);
            this._tbName.TabIndex = 23;
            this._tbName.Text = " ";
            this._tbName.TextChanged += new System.EventHandler(this.NameTextChanged);
            // 
            // _tbCreationDate
            // 
            appearance12.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this._tbCreationDate.Appearance = appearance12;
            this._tbCreationDate.Location = new System.Drawing.Point(100, 3);
            this._tbCreationDate.Name = "_tbCreationDate";
            this._tbCreationDate.ReadOnly = true;
            this._tbCreationDate.Size = new System.Drawing.Size(250, 21);
            this._tbCreationDate.TabIndex = 27;
            this._tbCreationDate.Text = " ";
            // 
            // _tbCountDeclarations
            // 
            appearance15.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this._tbCountDeclarations.Appearance = appearance15;
            this._tbCountDeclarations.Location = new System.Drawing.Point(204, 3);
            this._tbCountDeclarations.Name = "_tbCountDeclarations";
            this._tbCountDeclarations.ReadOnly = true;
            this._tbCountDeclarations.Size = new System.Drawing.Size(252, 21);
            this._tbCountDeclarations.TabIndex = 41;
            this._tbCountDeclarations.Text = " ";
            // 
            // _tbCountDiscrepancies
            // 
            appearance16.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this._tbCountDiscrepancies.Appearance = appearance16;
            this._tbCountDiscrepancies.Location = new System.Drawing.Point(204, 23);
            this._tbCountDiscrepancies.Name = "_tbCountDiscrepancies";
            this._tbCountDiscrepancies.ReadOnly = true;
            this._tbCountDiscrepancies.Size = new System.Drawing.Size(252, 21);
            this._tbCountDiscrepancies.TabIndex = 43;
            this._tbCountDiscrepancies.Text = " ";
            // 
            // _tbDiscrepancyAmount
            // 
            appearance17.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this._tbDiscrepancyAmount.Appearance = appearance17;
            this._tbDiscrepancyAmount.Location = new System.Drawing.Point(204, 43);
            this._tbDiscrepancyAmount.Name = "_tbDiscrepancyAmount";
            this._tbDiscrepancyAmount.ReadOnly = true;
            this._tbDiscrepancyAmount.Size = new System.Drawing.Size(252, 21);
            this._tbDiscrepancyAmount.TabIndex = 45;
            this._tbDiscrepancyAmount.Text = " ";
            // 
            // _pnlInfo1Content
            // 
            this._pnlInfo1Content.BackColor = System.Drawing.Color.Transparent;
            this._pnlInfo1Content.Controls.Add(this._tbCreationDate);
            this._pnlInfo1Content.Controls.Add(tbNameCaption);
            this._pnlInfo1Content.Controls.Add(this._tbName);
            this._pnlInfo1Content.Controls.Add(tbCreationDateCaption);
            this._pnlInfo1Content.Controls.Add(tbRegionCaption);
            this._pnlInfo1Content.Controls.Add(this._tbRegion);
            this._pnlInfo1Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pnlInfo1Content.Location = new System.Drawing.Point(3, 3);
            this._pnlInfo1Content.Name = "_pnlInfo1Content";
            this._pnlInfo1Content.Size = new System.Drawing.Size(347, 94);
            this._pnlInfo1Content.TabIndex = 51;
            // 
            // pnlSelectionSummaryContent
            // 
            this.pnlSelectionSummaryContent.BackColor = System.Drawing.Color.Transparent;
            this.pnlSelectionSummaryContent.Controls.Add(tbCountDeclarationsCaption);
            this.pnlSelectionSummaryContent.Controls.Add(this._tbCountDeclarations);
            this.pnlSelectionSummaryContent.Controls.Add(tbCountDiscrepanciesCaption);
            this.pnlSelectionSummaryContent.Controls.Add(this._tbCountDiscrepancies);
            this.pnlSelectionSummaryContent.Controls.Add(tbDiscrepancyAmountCaption);
            this.pnlSelectionSummaryContent.Controls.Add(this._tbDiscrepancyAmount);
            this.pnlSelectionSummaryContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSelectionSummaryContent.Location = new System.Drawing.Point(709, 3);
            this.pnlSelectionSummaryContent.Name = "pnlSelectionSummaryContent";
            this.pnlSelectionSummaryContent.Size = new System.Drawing.Size(348, 94);
            this.pnlSelectionSummaryContent.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this._pnlInfo1Content, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnlSelectionSummaryContent, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnlSelectionChangeDataContent, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._regionalBoundsGrid, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._regionalBoundsLabel, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1060, 184);
            this.tableLayoutPanel1.TabIndex = 53;
            // 
            // _regionalBoundsGrid
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._regionalBoundsGrid, 3);
            this._regionalBoundsGrid.DataSource = this._dataSource;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._regionalBoundsGrid.DisplayLayout.Appearance = appearance7;
            this._regionalBoundsGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridColumn12.Header.VisiblePosition = 0;
            ultraGridColumn12.Hidden = true;
            ultraGridColumn13.Header.VisiblePosition = 1;
            ultraGridColumn13.Hidden = true;
            ultraGridColumn14.Header.VisiblePosition = 2;
            ultraGridColumn14.Hidden = true;
            ultraGridColumn15.Header.VisiblePosition = 3;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn16.Header.VisiblePosition = 4;
            ultraGridColumn16.Hidden = true;
            ultraGridColumn17.Header.VisiblePosition = 5;
            ultraGridColumn17.Hidden = true;
            ultraGridColumn18.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn18.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn18.Format = "N2";
            ultraGridColumn18.Header.Caption = "Общая сумма расхождений у НП";
            ultraGridColumn18.Header.VisiblePosition = 6;
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn19.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn19.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn19.Format = "N2";
            ultraGridColumn19.Header.Caption = "Минимальная сумма расхождения";
            ultraGridColumn19.Header.VisiblePosition = 7;
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn20.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn20.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn20.Format = "N2";
            ultraGridColumn20.Header.Caption = "Общая сумма расхождений у НП";
            ultraGridColumn20.Header.VisiblePosition = 8;
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn21.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn21.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn21.Format = "N2";
            ultraGridColumn21.Header.Caption = "Минимальная сумма расхождения";
            ultraGridColumn21.Header.VisiblePosition = 9;
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn22.Header.VisiblePosition = 10;
            ultraGridColumn22.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn22});
            ultraGridGroup1.Header.Caption = "Проверка НДС";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 4;
            ultraGridGroup2.Header.Caption = "Разрыв";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 4;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._regionalBoundsGrid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._regionalBoundsGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._regionalBoundsGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this._regionalBoundsGrid.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this._regionalBoundsGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this._regionalBoundsGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this._regionalBoundsGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this._regionalBoundsGrid.DisplayLayout.MaxColScrollRegions = 1;
            this._regionalBoundsGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this._regionalBoundsGrid.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.Color.White;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this._regionalBoundsGrid.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this._regionalBoundsGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this._regionalBoundsGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._regionalBoundsGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this._regionalBoundsGrid.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._regionalBoundsGrid.DisplayLayout.Override.CellAppearance = appearance28;
            this._regionalBoundsGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this._regionalBoundsGrid.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this._regionalBoundsGrid.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Center";
            this._regionalBoundsGrid.DisplayLayout.Override.HeaderAppearance = appearance30;
            this._regionalBoundsGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            this._regionalBoundsGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this._regionalBoundsGrid.DisplayLayout.Override.RowAppearance = appearance31;
            this._regionalBoundsGrid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.DisableFilteredOutRows;
            this._regionalBoundsGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this._regionalBoundsGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this._regionalBoundsGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._regionalBoundsGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._regionalBoundsGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this._regionalBoundsGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._regionalBoundsGrid.Location = new System.Drawing.Point(3, 132);
            this._regionalBoundsGrid.Name = "_regionalBoundsGrid";
            this._regionalBoundsGrid.Size = new System.Drawing.Size(1054, 72);
            this._regionalBoundsGrid.TabIndex = 53;
            this._regionalBoundsGrid.Text = "ultraGrid1";
            this._regionalBoundsGrid.UseAppStyling = false;
            this._regionalBoundsGrid.Visible = false;
            // 
            // _regionalBoundsLabel
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._regionalBoundsLabel, 2);
            this._regionalBoundsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._regionalBoundsLabel.Location = new System.Drawing.Point(3, 103);
            this._regionalBoundsLabel.Name = "_regionalBoundsLabel";
            this._regionalBoundsLabel.Size = new System.Drawing.Size(700, 23);
            this._regionalBoundsLabel.TabIndex = 54;
            this._regionalBoundsLabel.Text = "Границы отбора расхождений по региону:";
            this._regionalBoundsLabel.Visible = false;
            // 
            // _dataSource
            // 
            this._dataSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models.SelectionRegionalBoundsViewModel);
            // 
            // SelectionCommonInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SelectionCommonInformation";
            this.Size = new System.Drawing.Size(1060, 184);
            this.pnlSelectionChangeDataContent.ResumeLayout(false);
            this.pnlSelectionChangeDataContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tbChangeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbApprover)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbAnalyst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbStatusChangeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCreationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCountDeclarations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbCountDiscrepancies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbDiscrepancyAmount)).EndInit();
            this._pnlInfo1Content.ResumeLayout(false);
            this._pnlInfo1Content.PerformLayout();
            this.pnlSelectionSummaryContent.ResumeLayout(false);
            this.pnlSelectionSummaryContent.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._regionalBoundsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbRegion;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbCreationDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbCountDeclarations;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbCountDiscrepancies;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbDiscrepancyAmount;
        private System.Windows.Forms.Panel _pnlInfo1Content;
        private System.Windows.Forms.Panel pnlSelectionSummaryContent;
        private System.Windows.Forms.Panel pnlSelectionChangeDataContent;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbChangeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbApprover;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbAnalyst;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _tbStatusChangeDate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid _regionalBoundsGrid;
        private System.Windows.Forms.BindingSource _dataSource;
        private Infragistics.Win.Misc.UltraLabel _regionalBoundsLabel;
    }
}
