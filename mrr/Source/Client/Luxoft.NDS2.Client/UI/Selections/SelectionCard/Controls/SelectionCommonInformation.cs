﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls
{
    public partial class SelectionCommonInformation : UserControl
    {
        public SelectionCommonInformation()
        {
            InitializeComponent();
        }

        public string SelectionName
        {
            get
            {
                return _tbName.Text;
            }
            set
            {
                _tbName.Text = value;
            }
        }

        public bool SelectionNameReadonly
        {
            set 
            {
               _tbName.ReadOnly = value;
            }
        }

        public string SelectionCreationDate
        {
            get
            {
                return _tbCreationDate.Text;
            }
            set
            {
                _tbCreationDate.Text = value;
            }
        }

        public string SelectionRegion
        {
            get
            {
                return _tbRegion.Text;
            }
            set
            {
                _tbRegion.Text = value;
            }
        }

        public string SelectionStatusDate
        {
            get
            {
                return _tbStatusChangeDate.Text;
            }
            set
            {
                _tbStatusChangeDate.Text = value;
            }
        }

        public string SelectionChangeDate
        {
            get
            {
                return _tbChangeDate.Text;
            }
            set
            {
                _tbChangeDate.Text = value;
            }
        }

        public string SelectionAnalyst
        {
            get
            {
                return _tbAnalyst.Text;
            }
            set
            {
                _tbAnalyst.Text = value;
            }
        }

        public string SelectionApprover
        {
            get
            {
                return _tbApprover.Text;
            }
            set
            {
                _tbApprover.Text = value;
            }
        }

        public string CountDeclarations
        {
            get
            {
                return _tbCountDeclarations.Text;
            }
            set
            {
                _tbCountDeclarations.Text = value;
            }
        }

        public string CountDiscrepancies
        {
            get
            {
                return _tbCountDiscrepancies.Text;
            }
            set
            {
                _tbCountDiscrepancies.Text = value;
            }
        }

        public string DiscrepancyAmount
        {
            get
            {
                return _tbDiscrepancyAmount.Text;
            }
            set
            {
                _tbDiscrepancyAmount.Text = value;
            }
        }

        public bool RegionalBoundsVisibility
        {
            set
            {
                _regionalBoundsGrid.Visible = value;
                _regionalBoundsLabel.Visible = value;
            }
        }

        public int RegionalBoundHeight
        {
            get { return _pnlInfo1Content.Height + _regionalBoundsGrid.Height + _regionalBoundsLabel.Height; }
        }

        public SelectionRegionalBoundsViewModel RegionalBounds
        {
            set { _dataSource.DataSource = value; }
        }

        public event EventHandler SelectionNameTextChange;

        private void NameTextChanged(object sender, EventArgs e)
        {
            if (SelectionNameTextChange != null)
            {
                SelectionNameTextChange(this, new EventArgs());
            }
        }
    }
}