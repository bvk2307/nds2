﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class PresenterCreator
    {
        private readonly IClientLogger _logger;
        private readonly SelectionDetailsModel _model;
        private readonly ISelectionView _view;
        private readonly ISelectionService _service;
        private readonly ISelectionServiceProxy _proxy;
        private readonly IDictionaryDataService _dictionaryService;
        private readonly INotifier _notifier;
        private readonly DictionarySur _sur;


        public PresenterCreator(
            ISelectionView view,
            SelectionDetailsModel model,
            INotifier notifier,
            IClientLogger logger,
            ISelectionService service,
            ISelectionServiceProxy proxy, 
            IDictionaryDataService dictionaryService,
            DictionarySur sur
            )
        {
            _view = view;
            _model = model;
            _logger = logger;
            _notifier = notifier;
            _service = service;
            _proxy = proxy;
            _dictionaryService = dictionaryService;
            _sur = sur;
        }

        public DeclarationListPresenter DeclarationList()
        {
            var p = new DeclarationListPresenter(new SelectionCardDataProvider<SelectionDeclaration>(
                    _view,
                    _logger, q => _service.SearchDeclarations(_model.Id, q)),
                _model.Declarations,
                _sur,
                _view.DeclarationGrid,
                _view.DeclarationGridPager);
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;
        }

        public DiscrepancyListPresenter DiscrepancyList()
        {
            var p = new DiscrepancyListPresenter(new SelectionCardDataProvider<SelectionDiscrepancy>(
                    _view,
                    _logger,
                    q => _service.SearchDiscrepancies(_model.Id, true, q)),
                    q => _proxy.AllDiscrepancies(_model.Id, true, q),
                _model.Discrepancies,
                _view.DiscrepancyGrid,
                _view.DiscrepancyGridPager);
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;
        }

        public DiscrepancyListPresenter DiscrepancySecondPartList()
        {
            var p = new DiscrepancyListPresenter(new SelectionCardDataProvider<SelectionDiscrepancy>(
                    _view,
                    _logger, 
                    q => _service.SearchDiscrepancies(_model.Id, false, q)),
                    q => _proxy.AllDiscrepancies(_model.Id, false, q),
                _model.DiscrepanciesSecondPart,
                _view.DiscrepancySecondPartGrid,
                _view.DiscrepancySecondPartGridPager);
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;
        }

        public HistoryListPresenter HistoryList()
        {
            var p = new HistoryListPresenter(new HistoryDataProvider(
                    _view,
                    _logger,
                    q => _service.GetActionsHistory(_model.Id)),
                _model.HistoryList,
                _view.HistoryGrid);
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;

        }

        public RegionalBoundsListPresenter RegionalBoundsList()
        {
            var p = new RegionalBoundsListPresenter(new RegionalBoundsDataProvider(
                    _view,
                    _logger, 
                    q => GetListRegionalBounds()), 
                _model.RegionalBoundsList,
                _view.RegionalBoundsGrid);
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;

        }

        private OperationResult<List<SelectionRegionalBoundsDataItem>> GetListRegionalBounds()
        {
            var result = new List<SelectionRegionalBoundsDataItem>();
            var id = _model.IsNew && _model.TemplateId != null ? _model.TemplateId : _model.Id;
            var boundItems = _service.GetRegionalBounds((long)id);
            var regionDictonary = _dictionaryService.GetRegionDictionary();
            foreach (var region in regionDictonary.Result)
            {
                var item = boundItems.Result.SingleOrDefault(x=> x.RegionCode == region.Code);
                if (item != null)
                {
                    item.RegionName = region.Name;
                    if (_model.IsNew && _model.TemplateId != null)
                    {
                        item.Id = null;
                        item.SelectionTemplateId = _model.Id;
                    }
                    result.Add(item);
                }
                else
                {
                    result.Add(new SelectionRegionalBoundsDataItem
                    {
                        SelectionTemplateId = _model.Id,
                        RegionCode = region.Code,
                        RegionName = region.Name
                    });
                }
            }

            return new OperationResult<List<SelectionRegionalBoundsDataItem>>(result);
        }

        public SelectionListPresenter SelectionList()
        {
            var p = new SelectionListPresenter(
                  _view.SelectionListGrid,
                  _model.SelectionList,
                  new SelectionListDataProvider(_notifier, _logger, q => _service.SearchByTemplate(_model.Id, q)));
            p.WithLogger(_logger);
            p.WithNotifier(_notifier);
            return p;
        }
    }
  
}