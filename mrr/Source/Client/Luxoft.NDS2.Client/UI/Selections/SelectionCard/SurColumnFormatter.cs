﻿using System;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    /// <summary>
    /// Этот класс форматирует UltraGridColumn для отображения иконок вместо значений
    /// </summary>
    public class SurColumnFormatter<TCellData> : IGridColumnFormatter
    {
        private readonly DictionarySur _dictionarySur;

        private readonly Control _owner;

        private readonly Dictionary<TCellData, Image> _imageCache = new Dictionary<TCellData,Image>();

        public SurColumnFormatter(DictionarySur dictionarySur, Control owner)
        {
            _dictionarySur = dictionarySur;
            _owner = owner;
        }
        public UltraGridColumn ApplyFormat(UltraGridColumn column)
        {
            column.DrawSur(_dictionarySur);
          return column;
        }

        public List<UltraGridColumn> ApplyFormat(List<UltraGridColumn> columns)
        {
            columns.ForEach(x=> x.DrawSur(_dictionarySur));
            return columns;
        }
    }

}
