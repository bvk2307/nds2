﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Utils;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class SelectionListPresenter :
        GridViewPresenter<SelectionListItemViewModel, SelectionListItemData, List<SelectionListItemData>>
    {
        private readonly SelectionListTabGridView _view;
        private readonly SelectionListViewModel _model;

        public SelectionListPresenter(
            SelectionListTabGridView view,
            SelectionListViewModel model,
            IServerGridDataProvider<List<SelectionListItemData>> dataProvider) : base(dataProvider, model, view)
        {
            _view = view;
            _model = model;
            QueryConditions.Filter.Add(
                new FilterQuery
                {
                    ColumnName = TypeHelper<SelectionListItemData>.GetMemberName(t => t.StatusId),
                    UserDefined = false,
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    GroupOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<Common.Contracts.DTO.Query.ColumnFilter>
                    {
                        new Common.Contracts.DTO.Query.ColumnFilter
                        {
                            ComparisonOperator = Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotEquals,
                            Value = SelectionStatus.Deleted
                        }
                    }
                });
            view.SelectionChecked += SelectionChecked;
            view.RowDoubleClicked += OnRowDoubleClicked;
        }

        public event EventHandler SelectionInTemplateChecked;
        public event EventHandler RowDoubleClicked;
        private void SelectionChecked(object sender, EventArgs e)
        {
            var cellChanged = e as CellEventArgs;

            if (cellChanged != null)
            {
                bool isChecked = bool.Parse(cellChanged.Cell.Text);
                RowCheckedChange(
                    cellChanged.Cell.Row.ListObject,
                    isChecked);
            }

            var headerEventArgs = e as AfterHeaderCheckStateChangedEventArgs;

            if (headerEventArgs != null)
            {
                var headerState = headerEventArgs.Column.GetHeaderCheckedState(
                    headerEventArgs.Rows);
                if (headerState == CheckState.Indeterminate)
                    return;

                _model.ListItems.ForAll(x => x.IsSelected = headerState == CheckState.Checked);
            }

            if (SelectionInTemplateChecked != null)
                SelectionInTemplateChecked(sender, e);
        }

        private void OnRowDoubleClicked(object sender, EventArgs e)
        {
            if (RowDoubleClicked != null)
                RowDoubleClicked(sender, e);
        }


        private void RowCheckedChange(object rowDataItem, bool isChecked)
        {
            var item = (SelectionListItemViewModel)rowDataItem;
            var modelItem = _model.ListItems.First(x => x.Id == item.Id);
            modelItem.IsSelected = isChecked;
        }


        protected override void BeforeDataLoaded()
        {
            QueryConditions.PaginationDetails.SkipTotalAvailable = false;
            QueryConditions.PaginationDetails.SkipTotalMatches = false;
        }

        public void RefreshView()
        {
            if (_view.Visible && UpdateRequired)
            {
                BeginLoad();
                UpdateRequired = false;
            }
        }

        private bool _updateRequired;
        public bool UpdateRequired
        {
            get { return _updateRequired; }
            set
            {
                if (value != _updateRequired)
                {
                    _updateRequired = value;
                    RefreshView();
                }
            }
        }

        public event EventHandler SelectionListTabLoaded;
        protected override void AfterDataLoaded(List<SelectionListItemData> data)
        {
            if (SelectionListTabLoaded != null)
                SelectionListTabLoaded(this, EventArgs.Empty);
        }
    }
}