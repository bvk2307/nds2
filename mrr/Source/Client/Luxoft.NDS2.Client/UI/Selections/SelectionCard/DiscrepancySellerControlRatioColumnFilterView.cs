﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancSellerControlRatioColumnFilterView : GridColumnFilterView
    {
        public DiscrepancSellerControlRatioColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var dictionaryItem in DiscrepancSellerControlRatioColumnView.SellerControlRatioDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class DiscrepancSellerControlRatioColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<SellerControlRatio, string> SellerControlRatioDictionary =
            new Dictionary<SellerControlRatio, string>()
           {
                { SellerControlRatio.NotExist, "Нет"},
                { SellerControlRatio.Exist, "Есть"},
                { SellerControlRatio.NotCalculated, "КС не рассчитан"}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();
            foreach (var dictionaryItem in SellerControlRatioDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
