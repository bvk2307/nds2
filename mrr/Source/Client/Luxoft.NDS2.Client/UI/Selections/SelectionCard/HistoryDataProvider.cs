﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    /// <summary>
    ///     Класс реализует загрузку данных карточки выборки/История
    /// </summary>
    public class HistoryDataProvider :
        ServiceProxyBase<List<ActionHistory>>,
        IServerGridDataProvider<List<ActionHistory>>
    {
        private readonly Func<QueryConditions, OperationResult<List<ActionHistory>>> _dataLoader;

        public HistoryDataProvider(
            INotifier notifier,
            IClientLogger logger,
            Func<QueryConditions, OperationResult<List<ActionHistory>>> dataLoader) : base(notifier, logger)
        {
            _dataLoader = dataLoader;
        }

        public event EventHandler<DataLoadedEventArgs<List<ActionHistory>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _dataLoader(queryConditions));
        }

        protected override void CallBack(List<ActionHistory> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<ActionHistory>>(result));
        }
    }
}