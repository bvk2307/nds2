﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancyDeclSignColumnFilterView : GridColumnFilterView
    {
        public DiscrepancyDeclSignColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            DiscrepancyDeclSignColumnView.DeclSignDictionary[Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.Empty] = "(Пусто)";
            foreach (var dictionaryItem in DiscrepancyDeclSignColumnView.DeclSignDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class DiscrepancyDeclSignColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType, string>
            DeclSignDictionary =
                new Dictionary<Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType, string>()
                {
                    {Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.Empty, " "},
                    {Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.ToPay, "К уплате"},
                    {Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.ToCharge, "К возмещению"},
                    {Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.Zero, "Нулевая"}
                };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();
            DiscrepancyDeclSignColumnView.DeclSignDictionary[Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.Empty] = " ";
            foreach (var dictionaryItem in DeclSignDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
