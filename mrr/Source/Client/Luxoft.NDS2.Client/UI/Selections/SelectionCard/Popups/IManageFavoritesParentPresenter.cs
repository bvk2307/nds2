﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    /// <summary>
    /// Этот интерфейс описывает методы родительского презентера для ManageFavoritesPresenter-а
    /// </summary>
    internal interface IManageFavoritesParentPresenter 
    {
        Filter CurrentFilter { get; }

        void SetFilter(KeyValuePair<long, string> favoriteData, Filter filter);
    }
}
