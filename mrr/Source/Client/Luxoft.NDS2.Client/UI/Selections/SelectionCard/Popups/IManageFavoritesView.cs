﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    /// <summary>
    /// Этот интерфейс описывает методы и свойства представления работы с избранными фильтрами выборки
    /// </summary>
    public interface IManageFavoritesView
    {
        # region Управление состоянием

        /// <summary>
        /// Возвращает идентификатор выбранного пользователем фильтра
        /// </summary>
        KeyValuePair<long, string>? SelectedItem
        {
            get;
        }

        /// <summary>
        /// Возвращает имя нового фильтра введенного пользователем
        /// </summary>
        string NewFavoriteFilterName
        {
            get;
        }

        /// <summary>
        /// Возвращает или задает список избранных фильтров
        /// </summary>
        IEnumerable<KeyValuePair<long, string>> List
        {
            get;
            set;
        }

        /// <summary>
        /// Включает или выключает режим добавления фильтра в избранное
        /// </summary>
        /// <param name="allowAdd">Признак, указывающий на то, доступна ли функция добавления фильтра в избранное или нет</param>
        /// <returns>Ссылка на представление</returns>
        IManageFavoritesView WithAdd(bool allowAdd);

        /// <summary>
        /// Включает или выключает режим применения/удаления фильтра из избранного
        /// </summary>
        /// <param name="allowEdit">Признак, определяющий доступность функций применить и удалить фильтр из избранного</param>
        /// <returns>Ссылка на представление</returns>
        IManageFavoritesView WithEdit(bool allowEdit);

        /// <summary>
        /// Задает заголовок окна представления
        /// </summary>
        /// <param name="text">Текст заголовка окна представления</param>
        /// <returns>Ссылка на представление</returns>
        IManageFavoritesView WithTitle(string text);

        # endregion 

        # region Управление поведением

        /// <summary>
        /// Запрашивает подтверждение действий у пользователя
        /// </summary>
        /// <param name="text">Текст подтверждения</param>
        /// <returns>Признак того ,что пользователь подтвердил операцию</returns>
        bool Confirm(string text);

        /// <summary>
        /// Закрывает представление
        /// </summary>
        void Close();

        /// <summary>
        /// Показывает представление пользователю в диалоговом режиме
        /// </summary>
        void OpenDialog();

        # endregion
    }
}
