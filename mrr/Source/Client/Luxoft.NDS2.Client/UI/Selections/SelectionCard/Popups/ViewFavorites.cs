﻿using Infragistics.Win.UltraWinListView;
using Luxoft.NDS2.Client.UI.Proposals.CommonPopups;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    /// <summary>
    /// Этот класс представляет форму управления избранными фильрами выборки
    /// </summary>
    public partial class ViewFavorites : Form, IManageFavoritesView
    {
        # region Поля

        private IManageFavoritesPresenter _presenter;

        private IEnumerable<KeyValuePair<long, string>> _data;

        # endregion

        # region Конструкторы

        public ViewFavorites()
        {
            InitializeComponent();
        }

        internal ViewFavorites(IManageFavoritesPresenter presenter)
            : this()
        {
            _presenter = presenter;
            WithAdd(false);
            WithEdit(false);
        }

        # endregion

        # region Реализация интерфейса IManageFavoritesView

        public IManageFavoritesView WithAdd(bool allowAdd)
        {
            txtFavName.Visible = allowAdd;
            btnAdd.Visible = allowAdd;

            if (allowAdd)
            {
                txtFavName.Focus();
            }

            return this;
        }

        public IManageFavoritesView WithEdit(bool allowEdit)
        {
            btnApply.Visible = allowEdit;
            btnDelete.Visible = allowEdit;

            if (allowEdit)
            {
                lstFavorites.Focus();
            }

            return this;
        }

        public KeyValuePair<long, string>? SelectedItem
        {
            get 
            {
                var selectedItem = lstFavorites.SelectedItem as ListViewItemLight;

                if (selectedItem != null)
                {
                    return selectedItem.Convert();
                }

                return null;
            }
        }

        public string NewFavoriteFilterName
        {
            get { return txtFavName.Text.Trim(); }
        }

        public IEnumerable<KeyValuePair<long, string>> List
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                RefreshList();
            }
        }

        public bool Confirm(string text)
        {
            return MessageYesNo.Show(ResourceManagerNDS2.Attention, text) == DialogResult.Yes;
        }

        public IManageFavoritesView WithTitle(string text)
        {
            Text = text;

            return this;
        }

        public void OpenDialog()
        {
            ShowDialog();
        }

        # endregion

        # region Обработчики событий

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _presenter.Add();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            _presenter.Apply();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _presenter.Delete();
        }

        private void lstFavorites_DoubleClick(object sender, EventArgs e)
        {
            if (btnApply.Visible)
                _presenter.Apply();
        }

        private void ViewFavorites_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (btnApply.Visible && lstFavorites.SelectedItem is ListViewItemLight)
                {
                    _presenter.Apply();
                }
                else if (btnAdd.Visible && txtFavName.Text.Trim() != string.Empty)
                {
                    _presenter.Add();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void lstFavorites_SelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelected = lstFavorites.SelectedItem as ListViewItemLight;

            if (itemSelected != null)
                txtFavName.Text = itemSelected.ToString();
        }

        # endregion

        # region Вспомогательные методы

        private void RefreshList()
        {
            lstFavorites.Items.Clear();
            lstFavorites.Items.AddRange(
                _data.Select(
                    pair => new ListViewItemLight() 
                    { 
                        Value = pair.Key, 
                        Text = pair.Value })
                        .OrderBy(item => item.Text)
                        .ToArray()
                );
        }

        # endregion

        # region ListViewItemLight

        private class ListViewItemLight
        {
            public long Value
            {
                get;
                set;
            }

            public string Text
            {
                get;
                set;
            }

            public override string ToString()
            {
                return Text;
            }

            public KeyValuePair<long, string> Convert()
            {
                return new KeyValuePair<long, string>(Value, Text);
            }
        }

        # endregion
    }
}
