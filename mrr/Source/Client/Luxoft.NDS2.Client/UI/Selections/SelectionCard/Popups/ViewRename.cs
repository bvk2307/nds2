﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    public partial class ViewRename : Form
    {
        public ViewRename()
        {
            InitializeComponent();
        }

        private readonly string _name;

        public ViewRename(string name)
        {
            InitializeComponent();
            _name = name;
            _selectionNameEditor.Text = name;
        }

        private void SaveClick(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void CancelClick(object sender, EventArgs e)
        {
            Close();
        }

        public string GetName()
        {
            return _selectionNameEditor.Text;
        }

    }
}
