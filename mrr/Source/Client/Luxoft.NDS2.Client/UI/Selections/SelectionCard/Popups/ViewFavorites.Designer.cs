﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    partial class ViewFavorites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.txtFavName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.lstFavorites = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtFavName)).BeginInit();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.bookmarks_star;
            this.btnApply.Appearance = appearance2;
            this.btnApply.Location = new System.Drawing.Point(199, 184);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(90, 25);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Загрузить";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // txtFavName
            // 
            this.txtFavName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFavName.Location = new System.Drawing.Point(2, 184);
            this.txtFavName.Name = "txtFavName";
            this.txtFavName.Size = new System.Drawing.Size(287, 21);
            this.txtFavName.TabIndex = 3;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.star_add;
            this.btnAdd.Appearance = appearance1;
            this.btnAdd.Location = new System.Drawing.Point(292, 184);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(90, 25);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Сохранить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.star_delete;
            this.btnDelete.Appearance = appearance3;
            this.btnDelete.Location = new System.Drawing.Point(292, 184);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 25);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lstFavorites
            // 
            this.lstFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFavorites.FormattingEnabled = true;
            this.lstFavorites.Location = new System.Drawing.Point(2, 5);
            this.lstFavorites.Name = "lstFavorites";
            this.lstFavorites.Size = new System.Drawing.Size(380, 173);
            this.lstFavorites.TabIndex = 0;
            this.lstFavorites.SelectedIndexChanged += new System.EventHandler(this.lstFavorites_SelectedIndexChanged);
            this.lstFavorites.DoubleClick += new System.EventHandler(this.lstFavorites_DoubleClick);
            // 
            // ViewFavorites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 212);
            this.Controls.Add(this.lstFavorites);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtFavName);
            this.Controls.Add(this.btnApply);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 400);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 250);
            this.Name = "ViewFavorites";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Избранное";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ViewFavorites_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.txtFavName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFavName;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private System.Windows.Forms.ListBox lstFavorites;

    }
}