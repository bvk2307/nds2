﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    /// <summary>
    /// Этот класс реализует IManageFavoritesPresenter
    /// </summary>
    internal class ManageFavoritesPresenter : IManageFavoritesPresenter
    {
        # region Поля

        private IManageFavoritesView _view;
        private readonly IManageFavoritesParentPresenter _parent;
        private readonly IManageFavoritesServiceProxy _proxy;
        # endregion

        # region Конструкторы

        public ManageFavoritesPresenter(
            IManageFavoritesParentPresenter parent,
            IManageFavoritesServiceProxy proxy
            )
        {
            _parent = parent;
            _proxy = proxy;
        }

        # endregion

        # region Представление

        public void SetView(IManageFavoritesView view)
        {
            _view = view;
        }

        # endregion

        # region Реализация интерфейса IManageFavoritesPresenter

        public void Add()
        {
            var result = _proxy.CreateFavorite(_view.NewFavoriteFilterName,
                _parent.CurrentFilter);

            OnAdd(result);

            if(result.Status != ResultStatus.Error)
            {
                _view.Close();
            }
        }

        public void Delete()
        {
            var selectedFilter = _view.SelectedItem;

            if (!selectedFilter.HasValue)
            {
                return;
            }

            if (_view.Confirm(
                string.Format(
                    ResourceManagerNDS2.FavoriteSelectionFilterDeleteConfirm, 
                    selectedFilter.Value.Value)))               
            {
                if (_proxy.TryDeleteFavorite(selectedFilter.Value.Key))
                {
                    GetList();
                }
            }
        }

        public void Apply()
        {
            var selectedFilter = _view.SelectedItem;

            if (!selectedFilter.HasValue)
            {
                return;
            }

            var result = _proxy.TryGetFavoriteSelectionFilterVersionTwo(selectedFilter.Value.Key);
            if (result.Status == ResultStatus.Success)
                {
                    _parent.SetFilter(_view.SelectedItem.Value, result.Result);
                    _view.Close();
                };
        }

        public void GetList()
        {
            var result = _proxy.TryGetFavoriteFilters();
            if (result.Status == ResultStatus.Success)
            {
                _view.List = result.Result;
            }
        }

        # endregion

        # region Делегаты

        public void OnAdd(OperationResult<KeyValuePair<long, string>> result)
        {
            if (result.Status == ResultStatus.ConstraintViolation
                && _view.Confirm(ResourceManagerNDS2.FavoritSelectionFilterOverwirteConfirm))
            {
                if (_proxy.TryOverwriteFavorite(_view.NewFavoriteFilterName,
                    _parent.CurrentFilter))
                {
                    GetList();
                }
            }

           // GetList();
        }

        # endregion
    }
}
