﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    /// <summary>
    /// Этот интерфейс описывает методы презентера форму работы с избранными фильтрами выборки
    /// </summary>
    internal interface IManageFavoritesPresenter
    {
        /// <summary>
        /// Добавляет текущий фильтр в избранное
        /// </summary>
        void Add();

        /// <summary>
        /// Удаляет выбранный фильтр из избранного
        /// </summary>
        void Delete();

        /// <summary>
        /// Применяет выбранный фильт
        /// </summary>
        void Apply();

        /// <summary>
        /// Обновить данные
        /// </summary>
        void GetList();
    }
}
