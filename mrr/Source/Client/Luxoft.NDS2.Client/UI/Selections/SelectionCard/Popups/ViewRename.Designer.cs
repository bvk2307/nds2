﻿namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    partial class ViewRename
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnSave = new Infragistics.Win.Misc.UltraButton();
            this._btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel = new Infragistics.Win.Misc.UltraLabel();
            this._selectionNameEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this._selectionNameEditor)).BeginInit();
            this.SuspendLayout();
            // 
            // _btnSave
            // 
            this._btnSave.Location = new System.Drawing.Point(12, 99);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(154, 23);
            this._btnSave.TabIndex = 0;
            this._btnSave.Text = "Сохранить";
            this._btnSave.Click += new System.EventHandler(this.SaveClick);
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(218, 99);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(154, 23);
            this._btnCancel.TabIndex = 1;
            this._btnCancel.Text = "Отменить";
            this._btnCancel.Click += new System.EventHandler(this.CancelClick);
            // 
            // ultraLabel
            // 
            this.ultraLabel.Location = new System.Drawing.Point(12, 26);
            this.ultraLabel.Name = "ultraLabel";
            this.ultraLabel.Size = new System.Drawing.Size(119, 25);
            this.ultraLabel.TabIndex = 3;
            this.ultraLabel.Text = "Введите название:";
            // 
            // _selectionNameEditor
            // 
            this._selectionNameEditor.Location = new System.Drawing.Point(12, 57);
            this._selectionNameEditor.Name = "_selectionNameEditor";
            this._selectionNameEditor.Size = new System.Drawing.Size(360, 21);
            this._selectionNameEditor.TabIndex = 4;
            // 
            // ViewRename
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 165);
            this.Controls.Add(this._selectionNameEditor);
            this.Controls.Add(this.ultraLabel);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewRename";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Переименовать";
            ((System.ComponentModel.ISupportInitialize)(this._selectionNameEditor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _btnSave;
        private Infragistics.Win.Misc.UltraButton _btnCancel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _selectionNameEditor;
    }
}