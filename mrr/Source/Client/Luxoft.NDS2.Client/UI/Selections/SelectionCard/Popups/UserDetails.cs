﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    public class UserDetails
    {
        public string Sid { get; set; }

        public string Name { get; set; }
    }
}