﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups
{
    public partial class RequestDataProgress : Form
    {
        public Action WorkDelegate { get; set; }

        public RequestDataProgress()
        {
            InitializeComponent();
        }

        private void RequestDataProgress_Load(object sender, EventArgs e)
        {
            if(WorkDelegate == null)
            {
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                WorkDelegate();
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
