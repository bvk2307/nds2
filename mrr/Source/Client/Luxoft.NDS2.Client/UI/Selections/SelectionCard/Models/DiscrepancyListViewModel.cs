﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.ViewModels;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class DiscrepancyListViewModel : ListViewModel<SelectionDiscrepancyModel, SelectionDiscrepancy>
    {
        public DiscrepancyListViewModel()
        {
            AddSortBuilder(
              TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Amount),
              new DefaultColumnSortBuilder(
                  TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.Amount)));

            AddFilterConverter(
                TypeHelper<SelectionDiscrepancyModel>.GetMemberName(x => x.BuyerOperation),
                new ColumnNameReplacer(
                    TypeHelper<SelectionDiscrepancy>.GetMemberName(x => x.OperationCodesBit)));
        }
        protected override SelectionDiscrepancyModel ConvertToModel(SelectionDiscrepancy dataItem)
        {
            return new SelectionDiscrepancyModel(dataItem);
        }
    }
}