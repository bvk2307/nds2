﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class DeclarationListViewModel : ListViewModel<DeclarationListItemViewModel, SelectionDeclaration>
    {
        public DeclarationListViewModel()
        {
            AddSortBuilder(
              TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.NdsAmount),
              new DefaultColumnSortBuilder(
                  TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.NdsAmount)));
        }

        protected override DeclarationListItemViewModel ConvertToModel(SelectionDeclaration dataItem)
        {
            return new DeclarationListItemViewModel(dataItem);
        }
    }
}