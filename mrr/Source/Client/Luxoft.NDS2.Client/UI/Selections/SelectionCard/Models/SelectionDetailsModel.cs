﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class SelectionDetailsModel : INotifyPropertyChanged    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        # region Конструкторы

        private string[] _lockers;

        private readonly DictionarySelectionStatus _dictionarySelectionStatus;

        public SelectionDetailsModel(
            Selection data,
            string[] lockers,
            DictionarySelectionStatus dictionarySelectionStatus,
            bool allowEdit)
        {
            _data = data;
            _lockers = lockers;
            _dictionarySelectionStatus = dictionarySelectionStatus;
            _appliedFilter = Filter.Copy();

            Filter = Filter.Copy();
            AllowEdit = allowEdit;
            Declarations = new DeclarationListViewModel();
            Discrepancies = new DiscrepancyListViewModel();
            DiscrepanciesSecondPart = new DiscrepancyListViewModel();
            HistoryList = new HistoryListViewModel();
            RegionalBoundsList = new SelectionRegionalBoundsListViewModel();
            SelectionList = new SelectionListViewModel();
        }

        # endregion

        # region Обновление данных

        public Selection GetData()
        {
            return _data;
        }

        private Selection _data;
        public Selection Data
        {
            set { _data = value; }
        }

        public string[] Lockers
        {
            set { _lockers = value; }
        }

        # endregion

        # region Данные выборки

        public long Id
        {
            get { return _data.Id; }
            set { _data.Id = value; }
        }


        public long? TemplateId
        {
            get { return _data.TemplateId; }
        }

        public string Number
        {
            get { return IsNew ? string.Empty : Id.ToString(CultureInfo.InvariantCulture); }
        }

        public string Name
        {
            get { return _data.Name; }
            set
            {
                if (value != _data.Name)
                {
                    _data.Name = value;
                    IsSelectionNameChanged = true;
                    OnPropertyChanged(TypeHelper<SelectionDetailsModel>.GetMemberName(x => x.Name));
                }
            }
        }

        public DateTime? CreatedAt
        {
            get { return _data.CreationDate; }
        }

        public DateTime? ModifiedAt
        {
            get { return _data.ModificationDate; }
        }

        public SelectionStatus? Status
        {
            get { return _data.Status; }
            set { _data.Status = value; }
        }

        public string StatusName
        {
            get { return _data.Status == null || _data.Status == 0 ? string.Empty : _dictionarySelectionStatus.Description((int) _data.Status); }
        }

        public DateTime? StatusDate
        {
            get { return _data.StatusDate; }
        }

        public SelectionType Type
        {
            get { return _data.TypeCode; }
        }

        public string TypeName
        {
            get { return _data.Type; }
        }

        public string Analyst
        {
            get { return _data.Analytic; }
        }

        public string AnalystSid
        {
            get { return _data.AnalyticSid; }
            set { _data.AnalyticSid = value; }
        }

        public string Manager
        {
            get { return _data.Manager; }
        }

        public int DeclarationQuantity
        {
            get { return _data.DeclarationCount; }
        }

        public int DiscrepancyQuantity
        {
            get { return _data.DiscrepanciesCount; }
        }

        public decimal? TotalAmount
        {
            get { return _data.TotalAmount; }
        }

        public bool IsNew
        {
            get { return _data.IsNew(); }
        }

        public bool IsLocked
        {
            get { return _lockers.Any(); }
        }

        public string LockedBy
        {
            get { return IsLocked ? _lockers.First() : string.Empty; }
        }

        public bool AllowEdit { get; set; }

        public string Regions
        {
            get { return _data.Regions; }
            set { _data.Regions = value; }
        }

       
        # endregion

        # region Фильтр выборки

        private Filter _appliedFilter;
        public Filter Filter
        {
            get { return _data.Filter; }
            set { _data.Filter = value; }
        }

        public void ApplyFilter()
        {
            _appliedFilter = Filter.Copy();
        }

        public void ResetFilter()
        {
            Filter = _appliedFilter.Copy();
        }

        public void ClearFilter()
        {
            Filter = new Filter().Copy();

            if (_appliedFilter != null)
                _appliedFilter = Filter.Copy();

            ClearDiscrepancies();
            ClearDiscrepanciesSecondPart();
            ClearDeclarations();
        }

        public bool LoadingFilterInProcess { get; set; }

        # endregion


        # region Декларации и расхождения

        public DeclarationListViewModel Declarations { get; set; }

        public DiscrepancyListViewModel Discrepancies { get; set; }

        public DiscrepancyListViewModel DiscrepanciesSecondPart { get; set; }

        public HistoryListViewModel HistoryList { get; set; }

        public SelectionRegionalBoundsListViewModel RegionalBoundsList { get; set; }

        public SelectionListViewModel SelectionList { get; set; }

        private void ClearDeclarations()
        {
            if (Declarations != null)
                Declarations.ListItems.Clear();
        }

        private void ClearDiscrepancies()
        {
            if (Discrepancies != null)
                Discrepancies.ListItems.Clear();
        }

        private void ClearDiscrepanciesSecondPart()
        {
            if (DiscrepanciesSecondPart != null)
                DiscrepanciesSecondPart.ListItems.Clear();
        }

        public bool IsFilterTakeData
        {
            get
            {
                return Declarations.ListItems.Any()
                       || Discrepancies.ListItems.Any();
            }
        }

        # endregion

        # region Отслеживание изменений

        public bool IsChanged
        {
            get { return IsSelectionNameChanged || IsFilterChanged; }
        }

        public bool IsFilterChanged
        {
            get
            {
                return (_appliedFilter != null)
                       && !_appliedFilter.IsEquals(Filter);
            }
        }

        public bool IsSelectionNameChanged { get; set; }

        # endregion

        # region Вспомогательные методы

        public static bool IsNullOrNew(SelectionDetailsModel model)
        {
            return (model == null) || model.IsNew;
        }

        public string GetDiscrepancyFileName()
        {
            return string.Format("{0} - {1}_{2:yyyyMMdd_HH-mm}.xlsx", Id, Name, DateTime.Now);
        }

        # endregion
    }
}