﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class SelectionRegionalBoundsViewModel
    {
        private readonly SelectionRegionalBoundsDataItem _data;
        private readonly SelectionRegionalBoundsDataItem _originData;
        public SelectionRegionalBoundsViewModel(SelectionRegionalBoundsDataItem data)
        {
            _data = data;
            _originData = _data.Clone();
        }

        public SelectionRegionalBoundsDataItem Data
        {
            get { return _data; }
        }
        /// <summary>
        /// Идентификатор 
        /// </summary>
        public long? Id {
            get { return _data.Id; }
        }

        /// <summary>
        /// Идентификатор шаблона
        /// </summary>
        public long SelectionTemplateId { get { return _data.SelectionTemplateId; } }

        /// <summary>
        /// Код региона
        /// </summary>
        public string RegionCode { get { return _data.RegionCode; } }

        /// <summary>
        /// Название региона
        /// </summary>
        public string RegionName { get { return _data.RegionName; } }

        /// <summary>
        /// Признак включения/исключения
        /// </summary>
        public bool Include
        {
            get { return _data.Include == null ? true : _data.Include == 1; }
            set
            {
                _data.Include = value ? 1 : 0;
            }
        }

        /// <summary>
        /// Общая сумма расхождений
        /// </summary>
        public uint DiscrTotalAmt {
            get
            {
                return _data.DiscrTotalAmt;
            }
            set
            {
                _data.DiscrTotalAmt = value;
            } }

        /// <summary>
        /// Минимальная сумма расхождений
        /// </summary>
        public uint DiscrMinAmt
        {
            get
            {
                return _data.DiscrMinAmt;
            }
            set
            {
                _data.DiscrMinAmt = value;
            }
        }


        /// <summary>
        /// Разрыв. Общая сумма расхождений
        /// </summary>
        public uint DiscrGapTotalAmt
        {
            get
            {
                return _data.DiscrGapTotalAmt;
            }
            set
            {
                _data.DiscrGapTotalAmt = value;
            }
        }

        /// <summary>
        /// Разрыв. Минимальная сумма расхождений
        /// </summary>
        public uint DiscrGapMinAmt
        {
            get
            {
                return _data.DiscrGapMinAmt;
            }
            set
            {
                _data.DiscrGapMinAmt = value;
            }
        }

        public bool HasChanges
        {
            get
            {
                return _data.DiscrTotalAmt != _originData.DiscrTotalAmt ||
                       _data.DiscrMinAmt != _originData.DiscrMinAmt ||
                       _data.DiscrGapTotalAmt != _originData.DiscrGapTotalAmt ||
                       _data.DiscrGapMinAmt != _originData.DiscrGapMinAmt ||
                       _data.Include != _originData.Include;
            }
        }
    }
}
