﻿using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class HistoryListViewModel : ListViewModel<HistoryListItemViewModel, ActionHistory>
    {
        protected override HistoryListItemViewModel ConvertToModel(ActionHistory dataItem)
        {
            return new HistoryListItemViewModel(dataItem);
        }
    }
}