﻿using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class SelectionRegionalBoundsListViewModel : ListViewModel<SelectionRegionalBoundsViewModel, SelectionRegionalBoundsDataItem>
    {
        protected override SelectionRegionalBoundsViewModel ConvertToModel(SelectionRegionalBoundsDataItem dataItem)
        {
            return new SelectionRegionalBoundsViewModel(dataItem);
        }
    }
}