﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class DeclarationListItemViewModel : INotifyPropertyChanged
    {
        private readonly SelectionDeclaration _data;

        public DeclarationListItemViewModel(SelectionDeclaration data)
        {
            _data = data;
        }


        public bool Included
        {
            get { return _data.Included; }
            set
            {
                _data.Included = value;
                NotifyPropertyChanged(TypeHelper<DeclarationListItemViewModel>.GetMemberName(x => x.Included));
            }
        }

        /// <summary>
        ///     Признак актуальности
        /// </summary>
        public bool IsActual
        {
            get { return _data.IsActual; }
        }

        /// <summary>
        ///     Исключение из АТ
        /// </summary>
        public ExcludeFromClaimType ExcludeFromClaimType
        {
            get { return (ExcludeFromClaimType) _data.ExcludeFromClaimType; }
        }

        /// <summary>
        ///     Тип(Журнал или Декларация)
        /// </summary>
        public Common.Contracts.DTO.Selections.Enums.DeclarationType Type
        {
            get { return _data.TypeId == 0 ? 
                    Common.Contracts.DTO.Selections.Enums.DeclarationType.Declaration :
                    Common.Contracts.DTO.Selections.Enums.DeclarationType.Journal; }
        }

        /// <summary>
        ///     СУР Налогоплательщика
        /// </summary>
        public SurCode Sur
        {
            get { return (SurCode)_data.Sur; }
        }

        /// <summary>
        ///     ИНН налогоплательщика
        /// </summary>
        public string Inn
        {
            get { return _data.Inn; }
        }

        /// <summary>
        ///     КПП налогоплательщика
        /// </summary>
        public string Kpp
        {
            get { return _data.Kpp; }
        }

        /// <summary>
        ///     Налогоплательщик
        /// </summary>
        public string TaxPayer
        {
            get { return _data.TaxPayer; }
        }

        /// <summary>
        ///     Реорганизованный НП
        /// </summary>
        public string InnReorganized
        {
            get { return _data.InnReorganized; }
        }

        /// <summary>
        ///     Регистрационный номер
        /// </summary>
        public int? RegNumber
        {
            get { return _data.RegNumber; }
        }

        public long Zip
        {
            get { return _data.Zip; }
        }

        /// <summary>
        ///     Признак НД
        /// </summary>
        public Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType DeclarationSign
        {
            get { return string.IsNullOrEmpty(_data.DeclarationSign) ?
                    Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType.Empty :
                    (Common.Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType)int.Parse(_data.DeclarationSign); }
        }

        /// <summary>
        ///     Сумма НДС
        /// </summary>
        public decimal? NdsAmount
        {
            get { return _data.NdsAmount; }
        }

        /// <summary>
        ///     К-во расхождений
        /// </summary>
        public long? DiscrepancyCount
        {
            get { return _data.DiscrepancyCount; }
        }

        /// <summary>
        ///     Сумма расхождений по книгам покупок раздел 8
        /// </summary>
        public decimal? DiscrepancyAmountChapter8
        {
            get { return _data.DiscrepancyAmountChapter8; }
        }

        /// <summary>
        ///     Сумма расхождений по книгам покупок раздел 9 и 12
        /// </summary>
        public decimal? DiscrepancyAmountChapter9
        {
            get { return _data.DiscrepancyAmountChapter9; }
        }

        /// <summary>
        ///     Общая сумма расхождений
        /// </summary>
        public decimal? DiscrepancyAmountTotal
        {
            get { return _data.DiscrepancyAmountTotal; }
        }

        /// <summary>
        ///     Минимальная сумма расхождений
        /// </summary>
        public decimal? DiscrepancyAmountMin
        {
            get { return _data.DiscrepancyAmountMin; }
        }

        /// <summary>
        ///     Максимальная сумма расхождений
        /// </summary>
        public decimal? DiscrepancyAmountMax
        {
            get { return _data.DiscrepancyAmountMax; }
        }

        /// <summary>
        ///     Средняя сумма расхождений
        /// </summary>
        public decimal? DiscrepancyAmountAvg
        {
            get { return _data.DiscrepancyAmountAvg; }
        }

        /// <summary>
        ///     Отчетный период
        /// </summary>
        public string TaxPeriod
        {
            get { return _data.TaxPeriod; }
        }

        /// <summary>
        ///     Номер корректировки
        /// </summary>
        public string DeclarationVersion
        {
            get { return _data.DeclarationVersion; }
        }

        /// <summary>
        ///     Дата подачи
        /// </summary>
        public DateTime? SubmitDate
        {
            get { return _data.SubmitDate; }
        }

        /// <summary>
        ///     КНП дата статуса
        /// </summary>
        public DateTime? StatusDate
        {
            get { return _data.StatusDate; }
        }

        /// <summary>
        ///     Крупнейший
        /// </summary>
        public bool Category
        {
            get { return _data.Category; }
        }

        /// <summary>
        ///     Код инспекции
        /// </summary>
        public string SonoCode
        {
            get { return _data.SonoCode; }
        }

        /// <summary>
        ///     Наименование инспекции
        /// </summary>
        public string SonoName
        {
            get { return _data.SonoName; }
        }

        /// <summary>
        ///     Код региона
        /// </summary>
        public string RegionCode
        {
            get { return _data.RegionCode; }
        }

        /// <summary>
        ///     Наименование региона
        /// </summary>
        public string RegionName
        {
            get { return _data.RegionName; }
        }

        /// <summary>
        ///     Инспектор
        /// </summary>
        public string Inspector
        {
            get { return _data.Inspector; }
        }

        /// <summary>
        ///     КНП статус
        /// </summary>
        public KnpStatus StatusCode
        {
            get { return _data.StatusCode == null ? KnpStatus.Empty : (KnpStatus)_data.StatusCode ; }
        }

        public int CloseKNPReason
        {
            get { return _data.CalcStatus ?? -1; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string sPropName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(sPropName));
        }
    }
}