﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models
{
    public class HistoryListItemViewModel
    {
        private readonly ActionHistory _data;
        public HistoryListItemViewModel(ActionHistory data)
        {
            _data = data;
        }

        /// <summary>
        /// Дата сохранения изменений
        /// </summary>
        public DateTime? ChangeDate {
            get { return _data.ChangeAt; }
        }

        /// <summary>
        /// Инициатор изменения
        /// </summary>
        public string ChangedBy { get { return string.IsNullOrEmpty(_data.ChangedBy) ? _data.ChangedBySid : _data.ChangedBy; } }

        /// <summary>
        /// Статус выборки/шаблона
        /// </summary>
        public string StatusId { get { return _data.Status; } }

        /// <summary>
        /// Действие
        /// </summary>
        public string Action { get { return _data.Action; } }

    }
}
