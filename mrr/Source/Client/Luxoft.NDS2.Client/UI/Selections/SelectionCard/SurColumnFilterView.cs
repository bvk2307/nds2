﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class SurColumnFilterView : GridColumnFilterView
    {
        private DictionarySur _sur;
        public SurColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        public DictionarySur DictionarySur
        {
            get
            {
                return _sur ?? new DictionarySur(Enumerable.Empty<Common.Contracts.DTO.Dictionaries.SurCode>());
            }
            set
            {
                _sur = value;
            }
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var dictionaryItem in _sur.SurCodes)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Code, dictionaryItem.Description));
            }
        }
    }
}
