﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class RegionsDataProvider : ILookupDataProvider<RegionModel>
    {
        private readonly IDataService _dataService;

        public RegionsDataProvider(
            IDataService dataService)
        {
            _dataService = dataService;
        }

        public IDataRequestResult<RegionModel> Search(IDataRequestArgs args)
        {

            return new DataRequestResult<RegionModel>(
                _dataService
                    .SearchRegions(args.SearchPattern, args.MaxQuantity)
                    .Result
                    .Select(dto => new RegionModel() { Code = dto.Key, Name = dto.Value })
                    .AsEnumerable());
        }
    }
}
