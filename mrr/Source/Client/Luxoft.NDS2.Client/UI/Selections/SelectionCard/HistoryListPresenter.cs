﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class HistoryListPresenter :
        GridViewPresenter<HistoryListItemViewModel, ActionHistory, List<ActionHistory>>
    {
        private readonly HistoryListGridView _view;
        public HistoryListPresenter(
            IServerGridDataProvider<List<ActionHistory>> dataProvider,
            IChangeableListViewModel<HistoryListItemViewModel, ActionHistory> model,
            HistoryListGridView view) : base(dataProvider, model, view)
        {
            _view = view;
        }

        public void RefreshView()
        {
            if (_view.Visible && UpdateRequired)
            {
                BeginLoad();
                UpdateRequired = false;
            }
        }

        private bool _updateRequired;
        public bool UpdateRequired
        {
            get { return _updateRequired; }
            set
            {
                if (value != _updateRequired)
                {
                    _updateRequired = value;
                    RefreshView();
                }
            }
        }
    }
}