﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    class SelectionCardPresenterCreator : BasePresenter<ISelectionView>
    {
        public SelectionCardPresenterCreator(PresentationContext presentationContext, WorkItem wi, ISelectionView view) :
            base(presentationContext, wi, view)
        {
            
        }
        public SelectionCardPresenter Create(
            ISelectionView view, 
            ISelectionListView listView, 
            ISelectionDetailsViewer viewer,
            ISelectionServiceProxy proxy,
            SelectionOpenModeEnum mode, long? selectionId )
        {
            var selectionService = WorkItem.GetWcfServiceProxy<ISelectionService>();
            var notifier = WorkItem.Services.Get<INotifier>(true);
            var logger = WorkItem.Services.Get<IClientLogger>(true);
            var windowManagerService = WorkItem.Services.Get<IWindowsManagerService>();
            var manageFavProxy = new ManageFavoritesServiceProxy(selectionService, notifier, logger);
            var dictionaryService = GetServiceProxy<IDictionaryDataService>();
            var dataService = GetServiceProxy<IDataService>();

            var p = new SelectionCardPresenter(
                view,
                selectionService,
                GetDeclarationCardOpener(),
                GetTaxPayerDetailsViewer(), 
                dictionaryService,
                SecurityLogger,
                windowManagerService,
                manageFavProxy,
                dataService,
                logger,
                notifier,
                Sur,
                listView,
                proxy,
                viewer,
                mode,
                selectionId
            );
            return p;
        }

    }
}
