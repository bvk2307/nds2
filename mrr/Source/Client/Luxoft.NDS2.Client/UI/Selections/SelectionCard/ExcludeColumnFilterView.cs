﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class ExcludeColumnFilterView : GridColumnFilterView
    {
        public List<DictionaryItem> _exludeReasonDict;
        public ExcludeColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        public List<DictionaryItem> ExcludeReasonDictionary
        {
            get
            {
                return _exludeReasonDict ?? new List<DictionaryItem>();
            }
            set
            {
                _exludeReasonDict = value;
            }
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            ExcludeReasonDictionary.FirstOrDefault(x => x.Id == (int)ExcludeFromClaimType.Include).Description = "(Пусто)";

            foreach (var dictionaryItem in ExcludeReasonDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem((ExcludeFromClaimType)dictionaryItem.Id, dictionaryItem.Description));
            }
        }
    }
    public static class ExcludeColumnView
    {
        public static void Apply(UltraGridColumn column, List<DictionaryItem> excludeReasonDictionary)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems(excludeReasonDictionary);
        }

        private static ValueList FillColumnItems(List<DictionaryItem> excludeReasonDictionary)
        {
            excludeReasonDictionary.FirstOrDefault(x => x.Id == (int)ExcludeFromClaimType.Include).Description = " ";

            var valueList = new ValueList();
            foreach (var dictionaryItem in excludeReasonDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem((ExcludeFromClaimType)dictionaryItem.Id, dictionaryItem.Description));
            }
            return valueList;
        }
    }
}
