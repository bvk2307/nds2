﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DictionaryEventArgs : EventArgs
    {
        public DictionarySur SurDictionary { get; set; }

        public List<DictionaryItem> ExcludeReasonDictionary { get; set; }

    }
}
