﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DeclarationItemEventArgs : EventArgs
    {
        public bool AllowInclude { get; set; }

        public bool IsChecked { get; set; }

        public long DeclarationId { get; set; }

        public bool IsDeclaration { get; set; }
    }
}
