﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    /// <summary>
    ///     Класс реализует загрузку данных карточки выборки/История
    /// </summary>
    public class RegionalBoundsDataProvider :
        ServiceProxyBase<List<SelectionRegionalBoundsDataItem>>,
        IServerGridDataProvider<List<SelectionRegionalBoundsDataItem>>
    {
        private readonly Func<QueryConditions, OperationResult<List<SelectionRegionalBoundsDataItem>>> _dataLoader;

        public RegionalBoundsDataProvider(
            INotifier notifier,
            IClientLogger logger,
            Func<QueryConditions, OperationResult<List<SelectionRegionalBoundsDataItem>>> dataLoader) : base(notifier, logger)
        {
            _dataLoader = dataLoader;
        }

        public event EventHandler<DataLoadedEventArgs<List<SelectionRegionalBoundsDataItem>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _dataLoader(queryConditions));
        }

        protected override void CallBack(List<SelectionRegionalBoundsDataItem> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<SelectionRegionalBoundsDataItem>>(result));
        }
    }
}