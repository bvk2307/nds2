﻿
using System;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Helpers
{
    public static class TypeConvertHelper
    {
        public static string AsString(this decimal? value)
        {
            return value.HasValue ? value.Value.ToString("N2") : string.Empty;
        }

        public static string AsString(this DateTime? value)
        {
            return value.HasValue ? value.Value.ToString() : string.Empty;
        }

        public static string AsString(this int value)
        {
            return value.ToString("N0");
        }        
    }
}
