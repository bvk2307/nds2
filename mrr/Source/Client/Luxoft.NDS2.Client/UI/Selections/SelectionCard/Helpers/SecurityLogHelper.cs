﻿using CommonComponents.Security.SecurityLogger;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.Security;
using System;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Helpers
{
    public static class SecurityLogHelper
    {
        public static bool Execute(
            this ISecurityLoggerService logger,
            Func<CommandResult> action,
            LogObject operationInfo)
        {
            var result = action();

            if (result == CommandResult.Refuse)
            {
                return true;
            }

            logger.Write(operationInfo, result == CommandResult.Success);

            return result == CommandResult.Success;
        }

        public static LogObject BuildLogObject(
            this LogOperation operation,
            long selectionId,
            string user)
        {
            return new LogObject
            {
                ObjectId = selectionId,
                IssueDate = DateTime.Now,
                Operation = operation,
                UserName = user
            };
        }
    }
}
