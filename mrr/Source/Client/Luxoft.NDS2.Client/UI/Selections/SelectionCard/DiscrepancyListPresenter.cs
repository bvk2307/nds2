﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Models.ViewModels;
using QueryConditions = Luxoft.NDS2.Common.Contracts.DTO.Query.QueryConditions;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancyListPresenter :
        PagingGridViewPresenter<SelectionDiscrepancyModel, SelectionDiscrepancy, PageResult<SelectionDiscrepancy>>
    {
        private readonly DiscrepancyListGridView _view;
        private readonly IGridPagerView _pager;
        private readonly Func<QueryConditions, List<SelectionDiscrepancy>> _allDataLoader;

        public DiscrepancyListPresenter(
            IServerGridDataProvider<PageResult<SelectionDiscrepancy>> dataProvider,
            Func<QueryConditions, List<SelectionDiscrepancy>> allDataLoader,
            IChangeableListViewModel<SelectionDiscrepancyModel, SelectionDiscrepancy> model,
            DiscrepancyListGridView gridView, IGridPagerView pagerView) : base(dataProvider, model, gridView, pagerView)
        {
            _allDataLoader = allDataLoader;
            _view = gridView;
            _pager = pagerView;
        }

        public void RefreshView()
        {
            if (_view.Visible && UpdateRequired)
            {
                QueryConditions.PaginationDetails.SkipTotalAvailable = false;
                QueryConditions.PaginationDetails.SkipTotalMatches = false;
                BeginLoad();
                UpdateRequired = false;
            }
        }
        public void ResetPagerIndex()
        {
            QueryConditions.PaginationDetails.RowsToSkip = 0;
            _pager.ResetPageIndex();
        }

        private bool _updateRequired;
        public bool UpdateRequired
        {
            get { return _updateRequired; }
            set
            {
                if (value != _updateRequired)
                {
                    _updateRequired = value;
                    RefreshView();
                }
            }
         }

        public List<SelectionDiscrepancy> LoadAll()
        {
            var cloneQuery = (QueryConditions)QueryConditions.Clone();
            cloneQuery.PaginationDetails.RowsToSkip = 0;
            cloneQuery.PaginationDetails.RowsToTake = (uint)_pager.RowMatches;

            return _allDataLoader(cloneQuery);
        }
    }
}