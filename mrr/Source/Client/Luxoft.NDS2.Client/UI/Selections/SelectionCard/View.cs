﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Controls;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Ribbon;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;


namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public partial class View : BaseView, ISelectionView
    {
        #region Конструкторы

        private const int ColumnChooserWidth = 300;

        private const int ColumnChooserHeigth = 350;

        private readonly GridPagerView _declarationListPager = new GridPagerView();

        private readonly GridPagerView _discrepancyListPager = new GridPagerView();

        private readonly GridPagerView _discrepancySecondSideListPager = new GridPagerView();

        private readonly DeclarationListGridView _declarationListGrid = new DeclarationListGridView();

        private readonly DiscrepancyListGridView _discrepancyListGrid = new DiscrepancyListGridView();

        private readonly DiscrepancyListGridView _discrepancySecondSideListGrid = new DiscrepancyListGridView();

        private readonly HistoryListGridView _historyListGrid = new HistoryListGridView();

        private readonly RegionalBoundsListGridView _regionalBoundsListGridView = new RegionalBoundsListGridView();

        private readonly SelectionListTabGridView _selectionsListGridView = new SelectionListTabGridView();


        public View() 
        {
            InitializeComponent();
        }

        public View(
            PresentationContext context,
            WorkItem workItem,
            DictionarySur surItems)
            : base(context, workItem)
        {
            InitializeComponent();

            SurItems = surItems;

            InitRibbonMenu();
            InitListComponents();

            _declarationListGrid.OpenDeclarartionCardClicked += OpenDeclarartionCardClicked;
            _declarationListGrid.OpenTaxPayerCardClicked += OnOpenTaxPayerCardClicked;
            _declarationListGrid.BeforeCellActivated += DeclarationIncludeCellActivated;
            _declarationListGrid.DeclarationViewLoaded += OnDeclarationViewLoaded;
            _discrepancyListGrid.OpenSellerCardClicked += OnOpenSellerCardClicked;
            _discrepancyListGrid.OpenBuyerCardClicked += OnOpenBuyerCardClicked;
            _discrepancyListGrid.DiscrepancyListLoaded += OnDiscrepancyListLoaded;

            _discrepancySecondSideListGrid.OpenSellerCardClicked += OnOpenSellerCardClicked;
            _discrepancySecondSideListGrid.OpenBuyerCardClicked += OnOpenBuyerCardClicked;
            _discrepancySecondSideListGrid.DiscrepancyListLoaded += OnDiscrepancyListLoaded;
            _discrepancySecondSideListGrid.DiscrepancyListLoaded += OnDiscrepancyListLoaded;

            _selectionsListGridView.SelectionListLoaded += OnSelectionListLoaded;
            
            _manualFilterEditArea.Controls.Add(_filterEditor);
            _filterEditor.Dock = DockStyle.Fill;

            _selectionMainParams.SelectionNameTextChange += SelectionNameTextChanged;
            _templateCommonInformationl.SelectionNameTextChange += SelectionNameTextChanged;
        }

        private void OnSelectionListLoaded(object sender, SelectionListOpenedEventArgs selectionListOpenedEventArgs)
        {
            if(SelectionListViewLoaded!=null)
                SelectionListViewLoaded(sender, selectionListOpenedEventArgs);
        }

        private void OnDiscrepancyListLoaded(object sender, DictionaryEventArgs dictionaryEventArgs)
        {
            if (DiscrepancyViewLoaded != null)
                DiscrepancyViewLoaded(sender, dictionaryEventArgs);
        }

        private void OnDeclarationViewLoaded(object sender, DictionaryEventArgs dictionaryEventArgs)
        {
            if (DeclarationViewLoaded != null)
                DeclarationViewLoaded(sender, dictionaryEventArgs);
        }

        private void OnOpenBuyerCardClicked(object sender, EventArgs eventArgs)
        {
            if (BuyerClicked != null)
                BuyerClicked(sender);
        }

        private void OnOpenSellerCardClicked(object sender, EventArgs eventArgs)
        {
            if (SellerClicked != null)
                SellerClicked(sender);
        }

        private void OnOpenTaxPayerCardClicked(object sender, EventArgs eventArgs)
        {
            if (DeclarationTaxpayerClicked != null)
                DeclarationTaxpayerClicked(_declarationListGrid.SelectedItem);
        }

        private void OpenDeclarartionCardClicked(object sender, EventArgs eventArgs)
        {
            if (DeclarationDoubleClicked != null)
                DeclarationDoubleClicked(_declarationListGrid.SelectedItem);
        }

        private void SelectionNameTextChanged(object sender, EventArgs e)
        {
            if (SelectionNameTextChange != null)
                SelectionNameTextChange(this, new EventArgs());
        }

        #endregion

        #region Гриды

        public DeclarationListGridView DeclarationGrid
        {
            get { return _declarationListGrid; }
        }

        public DiscrepancyListGridView DiscrepancyGrid
        {
            get { return _discrepancyListGrid; }
        }

        public DiscrepancyListGridView DiscrepancySecondPartGrid
        {
            get { return _discrepancySecondSideListGrid; }
        }

        public HistoryListGridView HistoryGrid
        {
            get { return _historyListGrid; }
        }

        public RegionalBoundsListGridView RegionalBoundsGrid
        {
            get { return _regionalBoundsListGridView; }
        }

        public SelectionListTabGridView SelectionListGrid
        {
            get { return _selectionsListGridView; }
        }

        /// <summary>
        ///     Инициализирует гриды
        /// </summary>
        private void InitListComponents()
        {
            _declarationListContainer
                .WithGrid(_declarationListGrid)
                .WithPager(_declarationListPager)
                .WithGridResetter(new GridColumnSortResetter(_declarationListGrid))
                .WithGridResetter(new GridColumnFilterResetter(_declarationListGrid))
                .WithGridResetter(new GridColumnsVisibilityResetter(_declarationListGrid))
                .WithColumnChooser(new GridColumnChooserView(
                    ColumnChooserWidth,
                    ColumnChooserHeigth));

            _discrepancyListContainer
                .WithGrid(_discrepancyListGrid)
                .WithPager(_discrepancyListPager)
                .WithGridResetter(new GridColumnSortResetter(_discrepancyListGrid))
                .WithGridResetter(new GridColumnFilterResetter(_discrepancyListGrid))
                .WithGridResetter(new GridColumnsVisibilityResetter(_discrepancyListGrid))
                .WithColumnChooser(new GridColumnChooserView(
                    ColumnChooserWidth,
                    ColumnChooserHeigth))
                .WithGridExcelExporter(new GridExcelExporter(() => DiscrepancyListExportToExcel(true)));

            _discrepancySecondPartContainer
                .WithGrid(_discrepancySecondSideListGrid)
                .WithPager(_discrepancySecondSideListPager)
                .WithGridResetter(new GridColumnSortResetter(_discrepancySecondSideListGrid))
                .WithGridResetter(new GridColumnFilterResetter(_discrepancySecondSideListGrid))
                .WithGridResetter(new GridColumnsVisibilityResetter(_discrepancySecondSideListGrid))
                .WithColumnChooser(new GridColumnChooserView(
                    ColumnChooserWidth,
                    ColumnChooserHeigth))
                .WithGridExcelExporter(new GridExcelExporter(() => DiscrepancyListExportToExcel(false)));


            _historyContainer
                .WithGrid(_historyListGrid);

            _regionalBoundsContainer
                .WithGrid(_regionalBoundsListGridView);

            _selectionListGridContainer
                .WithGrid(_selectionsListGridView)
                .WithGridResetter(new GridColumnSortResetter(_selectionsListGridView))
                .WithGridResetter(new GridColumnFilterResetter(_selectionsListGridView));
        }

        public event EventHandler<DiscrepancyListEventArgs> DiscrepancyListRequested;
        void DiscrepancyListExportToExcel(bool firstSide)
        {
            if (DiscrepancyListExport != null)
            {
                var cloneGrid = firstSide ? _discrepancyListGrid.CloneGrid() : _discrepancySecondSideListGrid.CloneGrid();
                if (cloneGrid == null)
                    return;

                var eventArgs = new DiscrepancyListEventArgs();
                if (DiscrepancyListRequested != null)
                {
                    eventArgs.FirstSide = firstSide;
                    DiscrepancyListRequested(this, eventArgs);
                    cloneGrid.BindingContext = new BindingContext();
                    var bindingSource = new BindingSource();
                    bindingSource.DataSource = eventArgs.DiscrepancyList;
                    cloneGrid.DataSource = bindingSource;
                    cloneGrid.DataBind();
                    DiscrepancyListExport(cloneGrid, new EventArgs());
                    cloneGrid.Dispose();
                }
           }
        }


        public void ExportDiscrepancyListToExcel(object grid, ref string fileName)
        {
            _discrepancyListGrid.ExportToExcel(grid, ref fileName);
        }

        #endregion

        #region События

        public event ParameterlessEventHandler OnViewInitializing;

        public event GenericEventHandler<string> OnCommandRequested;

        public event GenericEventHandler<object> DeclarationDoubleClicked;

        public event GenericEventHandler<object> BuyerClicked;

        public event GenericEventHandler<object> SellerClicked;

        public event GenericEventHandler<object> DeclarationTaxpayerClicked;

        public event ParameterlessEventHandler ViewClosed;

        public event EventHandler SelectionNameTextChange;

        public event EventHandler<DeclarationItemEventArgs> DeclarationIncludeCellActivated;

        public event EventHandler<DictionaryEventArgs> DeclarationViewLoaded;

        public event EventHandler<DictionaryEventArgs> DiscrepancyViewLoaded;

        public event EventHandler<SelectionListOpenedEventArgs> SelectionListViewLoaded;

        public event EventHandler DiscrepancyListExport;

        public event EventHandler DeclarationTabSelected;

        public event EventHandler DiscrepancyTabSelected;
        
        public event EventHandler DiscrepancySecondSideTabSelected;

        public event EventHandler RegionalBoundsTabSelected;

        public event EventHandler SelectionListTabSelected;

        public event EventHandler HistoryTabSelected;


        #endregion

        #region Меню инструментов

        private RibbonManager _ribbonMenu;

        private void InitRibbonMenu()
        {
            _ribbonMenu =
                new RibbonManager(
                        _presentationContext,
                        WorkItem,
                        "NDS2SelectionDetails",
                        _presentationContext.WindowTitle,
                        _presentationContext.WindowTitle)
                    .WithGroup("NDS2SelectionDetailsFilter", "Фильтр выборки")
                    .WithUpdate(_presentationContext)
                    .WithApplySelectionFilter(_presentationContext)
                    .WithFilterClear(_presentationContext)
                    .WithFilterReset(_presentationContext)
                    .WithSaveSelectionFilter(_presentationContext)
                    .WithLoadSelectionFilter(_presentationContext)
                    .WithGroup("NDS2SeectionDetailsWorkflow", "Процесс")
                    .WithApprove(_presentationContext)
                    .WithRejectForCorrection(_presentationContext) 
                    .WithRequestApproval(_presentationContext)
                    .WithGroup("NDS2SelectionDetailsOperations", "Функции")
                    .WithRemove(_presentationContext)
                    .WithRenameSelection(_presentationContext)
                    .WithTakeInWorkSelection(_presentationContext);

            _ribbonMenu.Init();
        }

        private void RaiseCommandEvent(string command)
        {
            if (OnCommandRequested != null)
                OnCommandRequested(command);
        }

        [CommandHandler("SelectionRejectForCorrection")]
        public virtual void RejectForCorrectionClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionRejectForCorrection");
        }

        [CommandHandler("SelectionRequestApproval")]
        public virtual void RequestApprovalClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionRequestApproval");
        }

        [CommandHandler("SelectionApprove")]
        public virtual void ApproveClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionApprove");
        }

        [CommandHandler("SelectionRemove")]
        public virtual void RemoveClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionRemove");
        }

        [CommandHandler("SelectionDetailsRename")]
        public virtual void RenameClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsRename");
        }

        [CommandHandler("SelectionDetailsTakeInWork")]
        public virtual void TakeInWorkClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsTakeInWork");
        }

        [CommandHandler("SelectionDetailsManageFavorites")]
        public virtual void OnManageFavoritesClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsManageFavorites");
        }

        [CommandHandler("SelectionDetailsApplyFilter")]
        public virtual void OnFindDiscrepanciesClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsApplyFilter");
        }

        [CommandHandler("SelectionDetailsUpdate")]
        public virtual void OnUpdateClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsUpdate");
        }

        [CommandHandler("SelectionDetailsClearFilter")]
        public virtual void FilterClearClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsClearFilter");
        }

        [CommandHandler("SelectionDetailsAddFilterToFavorites")]
        public virtual void AddToFavoritesClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsAddFilterToFavorites");
        }

        [CommandHandler("SelectionDetailsResetFilter")]
        public virtual void FilterResetClick(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsResetFilter");
        }

        [CommandHandler("SelectionDetailsClose")]
        public virtual void SelectionClose(object sender, EventArgs e)
        {
            RaiseCommandEvent("SelectionDetailsClose");
        }

        # endregion

        # region Управление состоянием

        public void HideAllMenuItems()
        {
            _ribbonMenu.HideAll();
        }

        public void ToggleMenuItemsVisibility(string[] commands, bool visible = true)
        {
            _ribbonMenu.ToggleVisibility(commands, visible);
        }

        public void ToggleMenuItemsAvailability(string[] commands, bool available)
        {
            _ribbonMenu.ToggleAvailability(commands, available);
        }

        public void RefreshMenu()
        {
            Action action = () => _ribbonMenu.Refresh();

            if (InvokeRequired)
                BeginInvoke(action);
            else
                action();
        }

        public string Number
        {
            get { return ulSelectionNumber.Text; }
            set { ulSelectionNumber.Text = value; }
        }

        public string Type
        {
            get { return ulSelectionType.Text; }
            set { ulSelectionType.Text = value; }
        }

        public string Status
        {
            get { return _ulSelectionStatus.Text; }
            set { _ulSelectionStatus.Text = value; }
        }

        public string SelectionName
        {
            get
            {
                return _selectionType == SelectionType.Template
                    ? _templateCommonInformationl.SelectionName
                    : _selectionMainParams.SelectionName;
            }
            set
            {
                if(_selectionType == SelectionType.Template)
                    _templateCommonInformationl.SelectionName = value;
                else _selectionMainParams.SelectionName = value;
            }
        }

        public bool SelectionNameReadonly
        {
            set
            {
                if (_selectionType == SelectionType.Template)
                    _templateCommonInformationl.SelectionNameReadonly = value;
                else _selectionMainParams.SelectionNameReadonly = value;
            }
        }

        public string CreatedAt
        {
            set
            {
                if (_selectionType == SelectionType.Template)
                    _templateCommonInformationl.SelectionCreationDate = value;
                else _selectionMainParams.SelectionCreationDate = value;
            }
        }

        public string Regions
        {
            set
            {
                if (_selectionType == SelectionType.Template)
                    _templateCommonInformationl.SelectionRegion = value;
                else _selectionMainParams.SelectionRegion = value;
            }
        }

        public string StatusChangedAt
        {
            set { _selectionMainParams.SelectionStatusDate = value; }
        }

        public string ModifiedAt
        {
            set { _selectionMainParams.SelectionChangeDate = value; }
        }

        public string Analyst
        {
            set
            {
                if (_selectionType == SelectionType.Template)
                    _templateCommonInformationl.TemplateAuthor = value;
                else _selectionMainParams.SelectionAnalyst = value;
            }
        }

        public string Approver
        {
            set { _selectionMainParams.SelectionApprover = value; }
        }

        public string DeclarationsQuantity
        {
            set { _selectionMainParams.CountDeclarations = value; }
        }

        public string DiscrepancyQuantity
        {
            set { _selectionMainParams.CountDiscrepancies = value; }
        }

        public string TotalAmount
        {
            set { _selectionMainParams.DiscrepancyAmount = value; }
        }

        public SelectionRegionalBoundsViewModel RegionalBounds
        {
            set { _selectionMainParams.RegionalBounds = value ; }
        }

        public void ToggleEdit(bool allowEdit)
        {
            _filterEditor.SetAllowEdit(allowEdit);

            foreach (var groupView in _groupControls.Values)
                groupView.IsEditable = allowEdit;
            _filterAddGroup.Enabled = allowEdit;

            _declarationListGrid.ReadOnly = !allowEdit;
            _selectionsListGridView.ReadOnly = !allowEdit;
            _regionalBoundsListGridView.ReadOnly = !allowEdit;
        }

        public void ToggleEditWithDisabledFilter(bool allowEdit)
        {
            _filterEditor.SetAllowEdit(false);

            foreach (var groupView in _groupControls.Values)
                groupView.IsEditable = false;
            _filterAddGroup.Enabled = false;

            _declarationListGrid.ReadOnly = !allowEdit;
            _selectionsListGridView.ReadOnly = !allowEdit;
            _regionalBoundsListGridView.ReadOnly = !allowEdit;
        }
        #endregion

        #region Работа с фильтром выборки

        private readonly SelectionFilterEditor _filterEditor =
            new SelectionFilterEditor(new GroupViewCreator());

        public IView FilterEditor
        {
            get { return _filterEditor; }
        }

        # endregion

        #region Новый фильтр выборки

        private FilterViewCreator _viewCreator;
        private FilterModel _filterModel;

        private readonly Dictionary<FilterGroupModel, GroupView> _groupControls =
            new Dictionary<FilterGroupModel, GroupView>();

        public void InitFilterControl(FilterModel filterModel)
        {
            _viewCreator = new FilterViewCreator(Notifier, SurItems);
            _filterModel = filterModel;
            _manualFilterEditArea.Controls.Clear();
            _groupControls.Clear();
        }

        public void SetExpandState()
        {
            if (_groupControls.Count > 0)
                _groupControls.Values.First().IsExpanded = true;
        }

        public void AddGroupView(FilterGroupModel group, SelectionType type)
        {
            // DN для сохранения очередности следования групп на форме их нужно добавлять в обратном порядке
            _manualFilterEditArea.Controls.Clear();

            var groupControl = new GroupView(group, _viewCreator)
            {
                Dock = DockStyle.Top,
                GroupForTemplate = type != SelectionType.Hand
            };
            _groupControls[group] = groupControl;
            group.Deleting += OnDeleteGroup;

            foreach (var control in _groupControls.Values.Reverse())
                _manualFilterEditArea.Controls.Add(control);
            SetDeleteButtons(true);
        }

        private void OnDeleteGroup(object sender, EventArgs e)
        {
            var model = sender as FilterGroupModel;

            if (model == null)
                return;

            // Может, стоит вынести подписку в модель
            _filterModel.DeleteGroup(model);
            _manualFilterEditArea.Controls.Remove(_groupControls[model]);
            _groupControls.Remove(model);
            SetDeleteButtons(true);
        }

        public void SetDeleteButtons(bool allowEdit)
        {
            if (_groupControls.Count == 1)
                _groupControls.Values.First().IsAllowDeleteOrDisable = false;
            else
                if(allowEdit)
                foreach (var group in _groupControls.Values)
                    group.IsAllowDeleteOrDisable = true;
        }

        private void AddNewGroup(object sender, EventArgs e)
        {
            _filterModel.AddGroup();
        }

        #endregion

        #region Таблицы

        public DictionarySur SurItems { get; private set; }

        public GridPagerView DeclarationGridPager
        {
            get { return _declarationListPager; }
        }

        public GridPagerView DiscrepancyGridPager
        {
            get { return _discrepancyListPager; }
        }

        public GridPagerView DiscrepancySecondPartGridPager
        {
            get { return _discrepancySecondSideListPager; }
        }

        #endregion

        #region Обработка событий формы

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            base.OnClosed(closeContext);

            if (ViewClosed != null)
                ViewClosed();
        }

        public override void OnUnload()
        {
            Dispose();
            base.OnUnload();
        }

        # endregion

        private readonly RequestDataProgress _requestWindow = new RequestDataProgress();

        public event ActionRequiredEvent BeforeClosing;

        public override void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            base.OnBeforeClosing(closeContext, eventArgs);

            if (BeforeClosing != null)
                eventArgs.Cancel |= BeforeClosing() != CommandResult.Success;
        }

        public void ShowLoadingWindow(Action startWorkDelegate)
        {
            _requestWindow.WorkDelegate = startWorkDelegate;
            _requestWindow.ShowDialog();
        }

        public void HideLoadingWindow()
        {
            _requestWindow.DialogResult = DialogResult.OK;
        }

        private void ViewLoad(object sender, EventArgs e)
        {
            if (OnViewInitializing != null)
            {
                OnViewInitializing();
            }
        }

        private const int VerticalPadding = 40;

        private SelectionType _selectionType = SelectionType.Hand;

        public void SetMode(SelectionType selectionType)
        {
            _selectionType = selectionType;
            if (selectionType == SelectionType.Template)
            {
                selectionTabs.Tabs[1].Visible = true;
                selectionTabs.Tabs[2].Visible = true;
                selectionTabs.Tabs[3].Visible = false;
                selectionTabs.Tabs[4].Visible = false;
                _ulSelectionStatusCaption.Visible = false;
                _ulSelectionStatus.Visible = false;
                _templateCommonInformationl.Visible = true;
                _selectionMainParams.Visible = false;
            }
            if (selectionType == SelectionType.SelectionByTemplate)
            {
                selectionTabs.Tabs[5].Visible = false;

                _expandableCommonInfo.Height = _selectionMainParams.RegionalBoundHeight + VerticalPadding;
                _selectionMainParams.RegionalBoundsVisibility = true;
            }
        }

        private void SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            if (e.Tab.Key == "_regionalBoundsContainer" && RegionalBoundsTabSelected != null)
                RegionalBoundsTabSelected(sender, e);
            if (e.Tab.Key == "_selectionListGridContainer" && SelectionListTabSelected != null)
                SelectionListTabSelected(sender, e);
            if (e.Tab.Key == "_declarationListContainer" && DeclarationTabSelected != null)
                DeclarationTabSelected(sender, e);
            if (e.Tab.Key == "_discrepancyListContainer" && DiscrepancyTabSelected != null)
                DiscrepancyTabSelected(sender, e);
            if (e.Tab.Key == "_discrepancySecondPartContainer" && DiscrepancySecondSideTabSelected != null)
                DiscrepancySecondSideTabSelected(sender, e);
            if (e.Tab.Key == "_historyContainer" && HistoryTabSelected != null)
                HistoryTabSelected(sender, e);

        }

        bool ISelectionView.IsDisposed
        {
            get { return IsDisposed; }
        }

    }
}