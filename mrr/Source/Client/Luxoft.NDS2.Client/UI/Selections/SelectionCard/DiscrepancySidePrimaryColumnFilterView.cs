﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancySidePrimaryColumnFilterView : GridColumnFilterView
    {
        public DiscrepancySidePrimaryColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var dictionaryItem in DiscrepancySidePrimaryColumnView.SidePrimaryProcessingDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class DiscrepancySidePrimaryColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<SidePrimaryProcessing, string> SidePrimaryProcessingDictionary =
            new Dictionary<SidePrimaryProcessing, string>()
           {
                { SidePrimaryProcessing.Buyer, "Покупатель"},
                { SidePrimaryProcessing.Seller, "Продавец"}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();
            foreach (var dictionaryItem in SidePrimaryProcessingDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
