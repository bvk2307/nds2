﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.Controls;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Selections.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public interface ISelectionView : IViewBase
    {

        #region События

        event ParameterlessEventHandler OnViewInitializing;

        event GenericEventHandler<string> OnCommandRequested;

        event GenericEventHandler<object> DeclarationDoubleClicked;

        event GenericEventHandler<object> BuyerClicked;

        event GenericEventHandler<object> SellerClicked;

        event GenericEventHandler<object> DeclarationTaxpayerClicked;

        event ParameterlessEventHandler ViewClosed;

        event EventHandler SelectionNameTextChange;

        event EventHandler<DeclarationItemEventArgs> DeclarationIncludeCellActivated;

        event EventHandler<DictionaryEventArgs> DeclarationViewLoaded;

        event EventHandler<DictionaryEventArgs> DiscrepancyViewLoaded;

        event EventHandler<SelectionListOpenedEventArgs> SelectionListViewLoaded;

        event EventHandler DiscrepancyListExport;

        event EventHandler DeclarationTabSelected;

        event EventHandler DiscrepancyTabSelected;

        event EventHandler DiscrepancySecondSideTabSelected;

        event EventHandler RegionalBoundsTabSelected;

        event EventHandler SelectionListTabSelected;

        event EventHandler HistoryTabSelected;

        event ActionRequiredEvent BeforeClosing;

        event EventHandler<DiscrepancyListEventArgs> DiscrepancyListRequested;

        #endregion События

        #region Гриды

        DeclarationListGridView DeclarationGrid { get; }

        DiscrepancyListGridView DiscrepancyGrid { get; }

        DiscrepancyListGridView DiscrepancySecondPartGrid { get; }

        HistoryListGridView HistoryGrid { get; }

        RegionalBoundsListGridView RegionalBoundsGrid { get; }

        SelectionListTabGridView SelectionListGrid { get; }

        GridPagerView DeclarationGridPager { get; }

        GridPagerView DiscrepancyGridPager { get; }

        GridPagerView DiscrepancySecondPartGridPager { get; }

        #endregion Гриды

        #region Ribbon Menu

        void HideAllMenuItems();

        void ToggleMenuItemsVisibility(string[] commands, bool visible = true);

        void ToggleMenuItemsAvailability(string[] commands, bool available);

        void ToggleEditWithDisabledFilter(bool allowEdit);

        void ToggleEdit(bool allowEdit);

        void RefreshMenu();


        #endregion

        #region Поля

        string SelectionName { get; set; }

        string Regions { set; }

        SelectionRegionalBoundsViewModel RegionalBounds { set; }

        string Number { get; set; }

        string Type { get; set; }

        string Status { get; set; }

        string CreatedAt { set; }

        string StatusChangedAt { set; }

        string ModifiedAt { set; }

        string Analyst { set; }

        string Approver { set; }

        string DeclarationsQuantity { set; }

        string DiscrepancyQuantity { set; }

        string TotalAmount { set; }


        #endregion

        #region Cостояние компонент на форме

        void InitFilterControl(FilterModel filterModel);

        void SetMode(SelectionType selectionType);

        void SetDeleteButtons(bool allowEdit);

        bool SelectionNameReadonly { set; }

        void ExportDiscrepancyListToExcel(object grid, ref string fileName);

        bool IsDisposed { get; }

        void AddGroupView(FilterGroupModel group, SelectionType type);

        void SetExpandState();

        void SetWindowTitle(string title);

        string GetWindowTitle();

        void HideLoadingWindow();

        void ShowLoadingWindow(Action startWorkDelegate);


        #endregion



    }
}
