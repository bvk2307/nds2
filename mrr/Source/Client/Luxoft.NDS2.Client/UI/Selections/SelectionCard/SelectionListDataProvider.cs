﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    /// <summary>
    ///     Класс реализует загрузку списка выборок для шаблона
    /// </summary>
    public class SelectionListDataProvider : ServiceProxyBase<List<SelectionListItemData>>,
        IServerGridDataProvider<List<SelectionListItemData>>
    {
        private readonly Func<QueryConditions, OperationResult<List<SelectionListItemData>>> _dataLoader;

        public SelectionListDataProvider(
            INotifier notifier,
            IClientLogger logger,
            Func<QueryConditions, OperationResult<List<SelectionListItemData>>> dataLoader) : base(notifier, logger)
        {
           _dataLoader = dataLoader;
        }

        public event EventHandler<DataLoadedEventArgs<List<SelectionListItemData>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _dataLoader.Invoke(queryConditions));
        }

        protected override void CallBack(List<SelectionListItemData> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<SelectionListItemData>>(result));
        }
    }
}