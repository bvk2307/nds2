﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancySubtypeColumnFilterView : GridColumnFilterView
    {
        public DiscrepancySubtypeColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var dictionaryItem in DiscrepancySubtypeColumnView.SubtypeDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class DiscrepancySubtypeColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<DiscrepancySubtype, string> SubtypeDictionary =
            new Dictionary<DiscrepancySubtype, string>()
           {
                { DiscrepancySubtype.NoneMultiple, "Не множественный"},
                { DiscrepancySubtype.Multiple, "Множественный"}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();
            foreach (var dictionaryItem in SubtypeDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
