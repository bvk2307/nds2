﻿using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Helpers;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Popups;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using CommonComponents.Security.SecurityLogger;
using CommonComponents.Utils;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using System.Threading;
using System.Windows.Forms;
using Filter = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Filter;

public enum OperationExecutionResult
{
    Success,
    Failure,
    Refuse
}

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class SelectionCardPresenter : IDataTableSelector, IManageFavoritesParentPresenter
    {
        # region Тексты сообщений

        private const string SelectionLockedPattern =
            "Изменение выборки невозможно, так как данная выборка заблокирована пользователем {0}";

        private const string LockFailurePattern =
            "Ошибка при блокировке выборке: {0}";

        private const string InWorkDiscrepancyNotFound =
            "Выборка не содержит ни одной декларации. Согласование невозможно";

        private const string RegionIsRequired = "Регион является обязательным параметром для группы фильтров";

        private const string RegionIsRequiredForTemplate =
            "Применение шаблона невозможно. Необходимо выбрать хотя бы один регион";

        private const string DiscrepancyTypeIsRequiredForTemplate =
            "Применение шаблона невозможно. Необходимо выбрать тип расхождения";

        private const string NameIsRequired = "Необходимо ввести название";

        private const string SelectionAlreadyExists = "{0} с таким названием уже существует";

        private const string IncorrectAttributesInFilter = "В фильтре есть некорректно заданные атрибуты";

        private const int TypeCodeTemplateParameterId = 205;

        # endregion

        # region Модель данных

        private long? _selectionId;

        private SelectionDetailsModel _model;

        # endregion

        # region Конструкторы

        private readonly IDeclarationCardOpener _declarationCardOpener;

        private readonly IClientLogger _logger;

        private readonly INotifier _notifier;

        private readonly ISelectionServiceProxy _proxy;

        private readonly ISelectionListView _selectionListView;

        private SelectionOpenModeEnum _openMode;

        private readonly ISelectionDetailsViewer _viewer;

        private readonly ISelectionView _view;

        private readonly DictionarySur _sur;

        private readonly ITaxPayerDetails _taxPayerDetailsViewer;

        private readonly ISecurityLoggerService _securityLogger;

        private readonly IWindowsManagerService _managerService;

        private readonly IDataService _dataService;

        private readonly IManageFavoritesServiceProxy _manageFavoritesProxy;

        public SelectionCardPresenter(
            ISelectionView view,
            ISelectionService selectionService,
            IDeclarationCardOpener declarationCardOpener,
            ITaxPayerDetails taxPayerDetailsViewer,
            IDictionaryDataService dictionaryService,
            ISecurityLoggerService securityLogger,
            IWindowsManagerService managerServise,
            IManageFavoritesServiceProxy manageFavoritesProxy,
            IDataService dataService,
            IClientLogger logger,
            INotifier notifier,
            DictionarySur sur,
            ISelectionListView selectionListView,
            ISelectionServiceProxy proxy,
            ISelectionDetailsViewer viewer,
            SelectionOpenModeEnum openMode,
            long? selectionId = null)
        {
            _view = view;
            _selectionListView = selectionListView;
            _proxy = proxy;
            _viewer = viewer;
            _selectionId = selectionId;
            _selectionService = selectionService;
            _dictionaryService = dictionaryService;
            _securityLogger = securityLogger;
            _managerService = managerServise;
            _dataService = dataService;
            _manageFavoritesProxy = manageFavoritesProxy;
            _taxPayerDetailsViewer = taxPayerDetailsViewer;
            _declarationCardOpener = declarationCardOpener;
            _logger = logger;
            _notifier = notifier;
            _sur = sur;
            _openMode = openMode;

            InitCommandToOperationMapper();
            InitLogOperationMapper();
            BindView();
        }

        # endregion

        # region Обработка событий представления

        /// <summary>
        /// Подписывается на событья view
        /// </summary>
        private void BindView()
        {
            _view.OnViewInitializing += Init;
            _view.OnCommandRequested += ExecuteCommand;
            _view.DeclarationDoubleClicked += DeclarationSelected;
            _view.BuyerClicked += ViewBuyerDetails;
            _view.SellerClicked += ViewSellerDetails;
            _view.DeclarationTaxpayerClicked += ViewTaxPayerDeclarationByPeriod;
            _view.ViewClosed += TryReleaseLock;
            _view.BeforeClosing += ValidateUnsavedChanges;
            _view.SelectionNameTextChange += ViewSelectionNameTextChange;
            _view.DiscrepancyListExport += DiscrepancyListExport;
            _view.DeclarationIncludeCellActivated += BeforeDeclarartionIncludeCellActivated;
            _view.DeclarationViewLoaded += OnListViewLoaded;
            _view.DiscrepancyViewLoaded += OnListViewLoaded;
            _view.SelectionListViewLoaded += OnSelectionListViewLoaded;
            _view.DiscrepancyListRequested += OnDiscrepancyListRequested;
        }

        private void OnDiscrepancyListRequested(object sender, DiscrepancyListEventArgs e)
        {
            DiscrepancyListViewModel model = new DiscrepancyListViewModel();
            model.UpdateList(e.FirstSide ? _discrepancyListPresenter.LoadAll() : _discrepancySecondPartListPresenter.LoadAll());
            e.DiscrepancyList = model;
        }

        private void OnSelectionListViewLoaded(object sender, SelectionListOpenedEventArgs selectionListOpenedEventArgs)
        {
            selectionListOpenedEventArgs.DictionarySelectionStatus = _proxy.TryLoadStatusDictionary();
        }

        private void OnListViewLoaded(object sender, DictionaryEventArgs dictionaryEventArgs)
        {
            dictionaryEventArgs.SurDictionary = _sur;
            dictionaryEventArgs.ExcludeReasonDictionary = _proxy.TryLoadExcludeReasonDictionary().ToList();
        }

        /// <summary>
        /// Экспортирует список расхождений в Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void DiscrepancyListExport(object sender, EventArgs eventArgs)
        {
            var fileName = string.Format(ResourceManagerNDS2.SelectionCard.ExcelExportFileName, DateTime.Now);

            if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
            {
                try
                {
                    _view.ExportDiscrepancyListToExcel(sender, ref fileName);
                }
                catch (Exception error)
                {
                    _logger.LogError(error, "DiscrepancyList.ExportToExcel");
                    _notifier.ShowError("Не удалось экспортировать cписок расхождений в файл");
                    return;
                }

                if (_view.ShowQuestion(
                        ResourceManagerNDS2.ExportExcelMessages.Caption,
                        string.Format(
                            ResourceManagerNDS2.ExportExcelMessages.OpenQuery,
                            fileName)) != DialogResult.Yes)
                {
                    return;
                }

                try
                {
                    new FileOpener().Open(fileName);
                }
                catch (Exception error)
                {
                    _logger.LogError(error, "DiscrepancyList.OpenExcelFile");
                    _view.ShowNotification(string.Format(ResourceManagerNDS2.ExportExcelMessages.FileOpenFailed,
                        fileName));
                }
            }
        }

        /// <summary>
        /// Если у выборки статус Формирование Ат или Сформировано АТ,
        /// то запрещаем менять состояние включения/исключения декларации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void BeforeDeclarartionIncludeCellActivated(object sender, DeclarationItemEventArgs eventArgs)
        {
            eventArgs.AllowInclude = _model.Status != SelectionStatus.ClaimCreating
                                     && _model.Status != SelectionStatus.ClaimCreated;
        }

        private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TypeHelper<SelectionDetailsModel>.GetMemberName(x => x.Name)
                && _model.Name != _view.SelectionName)
                _view.SelectionName = _model.Name;
        }

        /// <summary>
        /// Событие на изменение состояния чекбокса в гриде списка деклараций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void AfterDeclarationCheckStateChanged(object sender, DeclarationItemEventArgs eventArgs)
        {
            if (eventArgs.IsChecked)
                _proxy.IncludeDeclaration(_model.Id, eventArgs.DeclarationId);
            else
            {
                _proxy.ExcludeDeclaration(_model.Id, eventArgs.DeclarationId);
            }

            {
                if (!_view.IsDisposed)
                {
                    _discrepancyListPresenter.UpdateRequired = true;
                    _historyListPresenter.UpdateRequired = true;
                }
            }
        }

        /// <summary>
        /// Событие на изменение состояния чекбокса в шапке грида списка деклараций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void AfterGroupDeclarationCheckStateChanged(object sender, DeclarationItemEventArgs eventArgs)
        {
            if (!_model.Declarations.ListItems.Any())
                return;

            if (eventArgs.IsChecked)
                _proxy.IncludeDeclarations(_model.Id,
                    _model.Declarations.ListItems.Select(x => x.Zip).ToArray());
            else
                _proxy.ExcludeDeclarations(_model.Id,
                    _model.Declarations.ListItems.Select(x => x.Zip).ToArray());

            if (!_view.IsDisposed)
            {
                _discrepancyListPresenter.UpdateRequired = true;
                _historyListPresenter.UpdateRequired = true;
            }
        }

        /// <summary>
        /// Событие на изменение имени выборки в карточке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSelectionNameTextChange(object sender, EventArgs e)
        {
            if (_model != null)
            {
                _model.Name = _view.SelectionName;
                CalculateViewMenuItemsProperty();
            }
        }

        private CommandResult ValidateUnsavedChanges()
        {
            return AllowClose();
        }

        /// <summary>
        /// Событие двойного щелчка мыши на записи в таблице деклараций 
        /// Открытие карточки декларации
        /// </summary>
        /// <param name="item"></param>
        private void DeclarationSelected(object item)
        {
            item.DoWithConvertion<DeclarationListItemViewModel>(x =>
            {
                var result = _declarationCardOpener.Open(x.Zip);

                if (!result.IsSuccess)
                {
                    _view.ShowWarning(result.Message);
                }
            });
        }

        private void ViewTaxPayerByKppOriginal(string inn, string kpp, Direction direction = Direction.Invariant)
        {
            var result = _taxPayerDetailsViewer.ViewByKppOriginal(inn, kpp, direction);

            if (!result.IsSuccess)
            {
                _view.ShowError(result.Message);
            }
        }

        /// <summary>
        /// Открытие карточки налогоплательщика
        /// </summary>
        /// <param name="item"></param>
        private void ViewTaxPayerDeclarationByPeriod(object item)
        {
            item.DoWithConvertion<DeclarationListItemViewModel>(
                x => ViewTaxPayerByKppOriginal(x.Inn, x.Kpp));
        }

        /// <summary>
        /// Открытие карточки покупателя
        /// </summary>
        /// <param name="item"></param>
        private void ViewBuyerDetails(object item)
        {
            item.DoWithConvertion<SelectionDiscrepancyModel>(
                discrepancy =>
                    ViewTaxPayerByKppOriginal(discrepancy.BuyerInn, discrepancy.BuyerKpp));
        }

        /// <summary>
        /// Открытие карточки продавца
        /// </summary>
        /// <param name="item"></param>
        private void ViewSellerDetails(object item)
        {
            item.DoWithConvertion<SelectionDiscrepancyModel>(
                discrepancy =>
                    ViewTaxPayerByKppOriginal(discrepancy.SellerInn, discrepancy.SellerKpp));
        }

        /// <summary>
        /// Инициализация вью
        /// </summary>
        private void Init()
        {
            Action init =
                () =>
                {
                    if (LoadModel(CreateModel))
                    {
                        InitView();
                    }
                    else
                    {
                        Close();
                    }
                };
            init();
        }

        /// <summary>
        /// Вызов команды Ribbon Button
        /// </summary>
        /// <param name="command"></param>
        private void ExecuteCommand(string command)
        {
            var operation = _commandsMapper.OperationId(command);

            if (_logOperationMapper.ContainsKey(operation))
            {
                _securityLogger
                    .Execute(
                        () => _commandsMapper.Execute(command),
                        _logOperationMapper[operation]()
                            .BuildLogObject(_model.Id, _proxy.GetCurrentUserDisplayName()));
            }
            else
            {
                _commandsMapper.Execute(command);
            }
        }

        # endregion

        # region Маппинг команд на операции

        private CommandToOperationMapper _commandsMapper;

        private readonly Dictionary<string, Func<LogOperation>> _logOperationMapper =
            new Dictionary<string, Func<LogOperation>>();

        private void InitCommandToOperationMapper()
        {
            _commandsMapper =
                new CommandToOperationMapper()
                    .WithCommand(
                        "SelectionDetailsApplyFilter",
                        Constants.SystemPermissions.Operations.SelectionFilterApply,
                        DoApplyFilter)
                    .WithCommand(
                        "SelectionDetailsClearFilter",
                        Constants.SystemPermissions.Operations.SelectionFilterReset,
                        DoClearFilter)
                    .WithCommand(
                        "SelectionDetailsResetFilter",
                        Constants.SystemPermissions.Operations.SelectionFilterReturnParams,
                        DoResetFilter)
                    .WithCommand(
                        "SelectionDetailsAddFilterToFavorites",
                        Constants.SystemPermissions.Operations.SelectionFilterFavoritesAdd,
                        DoAddToFavorites)
                    .WithCommand(
                        "SelectionDetailsManageFavorites",
                        Constants.SystemPermissions.Operations.SelectionFilterFavorites,
                        DoManageFavorites)
                    .WithCommand(
                        "SelectionDetailsRename",
                        Constants.SystemPermissions.Operations.SelectionChange,
                        DoRename)
                    .WithCommand(
                        "SelectionDetailsTakeInWork",
                        Constants.SystemPermissions.Operations.SelectionTakeInWork,
                        DoAssign)
                    .WithCommand(
                        "SelectionRemove",
                        Constants.SystemPermissions.Operations.SelectionRemove,
                        DoRemove)
                    .WithCommand(
                        "SelectionRejectForCorrection",
                        Constants.SystemPermissions.Operations.SelectionToCorrect,
                        DoRejectForCorrection)
                    .WithCommand(
                        "SelectionApprove",
                        Constants.SystemPermissions.Operations.SelectionApproved,
                        DoApprove)
                    .WithCommand(
                        "SelectionRequestApproval",
                        Constants.SystemPermissions.Operations.SelectionToApprove,
                        DoRequestApproval)
                    .WithCommand(
                        "SelectionDetailsUpdate",
                        Constants.SystemPermissions.Operations.SelectionUpdate,
                        DoRefresh);
        }

        private void InitLogOperationMapper()
        {
            _logOperationMapper.Add(
                Constants.SystemPermissions.Operations.SelectionRemove,
                () => LogOperation.SelectionRemove);

            _logOperationMapper.Add(
                Constants.SystemPermissions.Operations.SelectionApproved,
                () => LogOperation.SelectionApproved);

            _logOperationMapper.Add(
                Constants.SystemPermissions.Operations.SelectionToCorrect,
                () => LogOperation.SelectionToCorrect);

            _logOperationMapper.Add(
                Constants.SystemPermissions.Operations.SelectionToApprove,
                () => LogOperation.SelectionToApprove);
        }

        # endregion

        # region Загрузка/обновление модели данных

        private readonly ISelectionService _selectionService;
        private readonly IDictionaryDataService _dictionaryService;

        private bool LoadModel(Action<SelectionDetails> modelUpdater)
        {
            var retVal = false;
            var type = _openMode == SelectionOpenModeEnum.NewTemplate ||
                       _openMode == SelectionOpenModeEnum.EditTemplate ||
                       _openMode == SelectionOpenModeEnum.CopyTemplate ||
                       _openMode == SelectionOpenModeEnum.ViewTemplate
                ? SelectionType.Template
                : (_openMode == SelectionOpenModeEnum.NewSelection ||
                   _openMode == SelectionOpenModeEnum.EditSelection ||
                   _openMode == SelectionOpenModeEnum.ViewSelection ||
                   _openMode == SelectionOpenModeEnum.ApproveSelection
                    ? SelectionType.Hand
                    : SelectionType.SelectionByTemplate);

            if (_openMode == SelectionOpenModeEnum.CopyTemplate)
            {
                var selectionDetails = _proxy.NewFromTemplate((long) _selectionId);
                if (selectionDetails != null)
                {
                    retVal = true;
                    modelUpdater(selectionDetails);
                }
            }
            else
            {
                var selection = _proxy.Load(_selectionId, _lockKey, type);
                if (selection != null)
                {
                    retVal = true;
                    modelUpdater(selection);
                }
            }

            return retVal;
        }

        # endregion

        # region Разграничение прав на доступ

        private SelectionOperation[] _applicableOperations;
        private SelectionOperation[] _applicableOperationsOriginal;

        private void SetViewMenuItemsVisibility()
        {
            _view.HideAllMenuItems();
            _view.ToggleMenuItemsVisibility(
                _applicableOperations
                    .SelectMany(operation => _commandsMapper.Commands(operation.Id))
                    .ToArray());
        }

        private void SetViewMenuItemsAvailability()
        {
            foreach (var operation in _applicableOperations)
            {
                _view.ToggleMenuItemsAvailability(
                    _commandsMapper.Commands(operation.Id).ToArray(),
                    operation.IsAvailable);
            }
        }

        # endregion

        # region Управление состоянием View

        private void InitView()
        {
            InitSubPresenters();

            SetView();
        }

        private DeclarationListPresenter _declarationListPresenter;
        private DiscrepancyListPresenter _discrepancyListPresenter;
        private DiscrepancyListPresenter _discrepancySecondPartListPresenter;
        private HistoryListPresenter _historyListPresenter;
        private RegionalBoundsListPresenter _regionalBoundsPresenter;
        private SelectionListPresenter _selectionListPresenter;


        /// <summary>
        /// Инициализирует дочерние презентеры
        /// </summary>
        private void InitSubPresenters()
        {
            var gridPresentersCreator = new PresenterCreator(
                _view,
                _model,
                _notifier,
                _logger,
                _selectionService,
                _proxy,
                _dictionaryService,
                _sur);

            _declarationListPresenter = gridPresentersCreator.DeclarationList();
            _discrepancyListPresenter = gridPresentersCreator.DiscrepancyList();
            _discrepancySecondPartListPresenter = gridPresentersCreator.DiscrepancySecondPartList();
            _historyListPresenter = gridPresentersCreator.HistoryList();
            _declarationListPresenter.AfterGroupDeclarationCheckStateChanged += AfterGroupDeclarationCheckStateChanged;
            _declarationListPresenter.AfterDeclarationCheckStateChanged += AfterDeclarationCheckStateChanged;
            _regionalBoundsPresenter = gridPresentersCreator.RegionalBoundsList();
            _regionalBoundsPresenter.RegionalBoundsLoaded += OnRegionalBoundsLoaded;
            _regionalBoundsPresenter.RegionalBoundsChecked += (sender, args) => CalculateViewMenuItemsProperty();
            _selectionListPresenter = gridPresentersCreator.SelectionList();
            _selectionListPresenter.SelectionInTemplateChecked += (sender, args) => CalculateViewMenuItemsProperty();
            _selectionListPresenter.RowDoubleClicked += OnOpenSelectionCard;
            _selectionListPresenter.SelectionListTabLoaded += (sender, args) => CalculateViewMenuItemsProperty();

            _view.RegionalBoundsTabSelected += (sender, e) => { _regionalBoundsPresenter.RefreshView(); };
            _view.SelectionListTabSelected += (sender, e) => { _selectionListPresenter.RefreshView(); };
            _view.DeclarationTabSelected += (sender, e) => { _declarationListPresenter.RefreshView(); };
            _view.DiscrepancyTabSelected += (sender, e) => { _discrepancyListPresenter.RefreshView(); };
            _view.DiscrepancySecondSideTabSelected += (sender, e) =>
            {
                _discrepancySecondPartListPresenter.RefreshView();
            };
            _view.HistoryTabSelected += (sender, e) => { _historyListPresenter.RefreshView(); };
        }

        /// <summary>
        /// Обновляем значение к-во регионов для шаблона в общей информации на форме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void OnRegionalBoundsLoaded(object sender, EventArgs eventArgs)
        {
            if (!_model.IsNew)
                _view.Regions = _model.RegionalBoundsList.ListItems.Count(x => x.Include).ToString();
            CalculateViewMenuItemsProperty();
        }

        private void OnOpenSelectionCard(object sender, EventArgs eventArgs)
        {
            var item = (SelectionListItemViewModel) sender;

            if (item == null)
                return;

            SelectionOpenModeEnum mode = SelectionOpenModeEnum.ViewSelectionByTemplate;

            if (item.StatusId == SelectionStatus.RequestForApproval)
                mode = SelectionOpenModeEnum.ApproveSelectionByTemplate;

            _viewer.Open(item, _selectionListView, _proxy, mode);
        }

        private void SetView()
        {
            _model.IsSelectionNameChanged = false;

            CalculateViewMenuSelectionToApprove();
            CalculateViewMenuApply();
            CalculateViewMenuReturnParams();
            CalculateViewMenuResetFilter();
            CalculateViewMenuAddFavorite();

            SetViewMenuItemsVisibility();
            SetViewMenuItemsAvailability();
            _view.RefreshMenu();

            _view.SetMode(_model.Type);

            SetViewMainData();
            InitFilter();
            _model.ApplyFilter();
            CalculateViewMenuItemsProperty();

            SetViewEdit();

            //Устанавливаем доступность кнопки Удалить группу и чекбокса выбора
            _view.SetDeleteButtons(_model.AllowEdit && InEditMode());

            if (_model.IsLocked)
            {
                _view.ShowWarning(string.Format(SelectionLockedPattern, _model.LockedBy));
            }

            if (_model.AllowEdit)
            {
                TrySetLock();
            }

            if (!_model.IsNew)
            {
                _historyListPresenter.UpdateRequired = true;
                _declarationListPresenter.UpdateRequired = true;
                _discrepancyListPresenter.UpdateRequired = true;
                _discrepancySecondPartListPresenter.UpdateRequired = true;
            }
            if (_model.Type == SelectionType.Template)
            {
                _regionalBoundsPresenter.BeginLoad();
                _selectionListPresenter.BeginLoad();
            }
        }

        private void SetViewMainData()
        {
            _view.Number = _model.Number;
            _view.Type = _model.TypeName;
            _view.CreatedAt = _model.CreatedAt.AsString();
            _view.Status = _model.StatusName;
            _view.StatusChangedAt = _model.StatusDate.AsString();
            _view.SelectionName = _model.Name;
            _view.ModifiedAt = _model.ModifiedAt.AsString();
            _view.Regions = GetRegionsInfo();
            _view.Analyst = _model.Type == SelectionType.Template ? _model.Manager : _model.Analyst;
            _view.Approver = _model.Manager;
            _view.DeclarationsQuantity = _model.DeclarationQuantity.ToString();
            _view.DiscrepancyQuantity = _model.DiscrepancyQuantity.ToString();
            _view.TotalAmount = _model.TotalAmount.ToString();
            _view.SelectionNameReadonly = !_model.IsNew;
            if (_model.Type == SelectionType.SelectionByTemplate)
                _view.RegionalBounds = new SelectionRegionalBoundsViewModel(_selectionService
                    .GetRegionalBounds((long) _model.TemplateId, _model.Regions)
                    .Result);
        }


        private string GetRegionsInfo()
        {
            if (string.IsNullOrEmpty(_model.Regions))
                return string.Empty;

            char[] delimeters = {','};
            var regions = _model.Regions.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);

            string result = string.Empty;
            var regionDict = _dictionaryService.GetRegionDictionary().Result;
            if (_model.Type != SelectionType.Template)
            {
                foreach (var region in regions)
                {
                    if (!string.IsNullOrEmpty(result))
                        result += ", ";

                    var name = regionDict.FirstOrDefault(x => x.Code == region.Trim()).Name;
                    result += string.Format("{0}_{1}", region, name);
                }
            }
            else
            {
                result = regions.Length.ToString();
            }

            return result;
        }

        private void SetViewEdit()
        {
            if (_model.Type == SelectionType.SelectionByTemplate)
                _view.ToggleEditWithDisabledFilter(_model.AllowEdit && InEditMode());
            else
                _view.ToggleEdit(_model.AllowEdit && InEditMode());
        }

        private bool InEditMode()
        {
            return _openMode == SelectionOpenModeEnum.CopyTemplate ||
                   _openMode == SelectionOpenModeEnum.EditSelection ||
                   _openMode == SelectionOpenModeEnum.EditSelectionByTemplate ||
                   _openMode == SelectionOpenModeEnum.EditTemplate ||
                   _openMode == SelectionOpenModeEnum.NewSelection ||
                   _openMode == SelectionOpenModeEnum.NewTemplate;
        }

        private void Close()
        {
            Guid smartPartId;

            if (_managerService.TryGetSmartPartId(_view, out smartPartId))
                _managerService.TryClose(smartPartId);
        }

        private void CalculateViewMenuItemsProperty()
        {
            ReloadApplicableOperations();
            CalculateViewMenuApply();
            CalculateViewMenuReturnParams();
            CalculateViewMenuResetFilter();
            CalculateViewMenuAddFavorite();
            CalculateViewMenuSelectionToApprove();
            CalculateViewMenuSelectionToCorrect();
            CalculateViewMenuSelectionApprove();
            CalculateViewMenuSelectionRemove();
            CalculateViewMenuSelectionRename();

            SetViewMenuItemsVisibility();
            SetViewMenuItemsAvailability();
            _view.RefreshMenu();
        }

        private void CalculateViewMenuApply()
        {
            if (_filterModel == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionFilterApply);
            if (operation == null || !operation.IsAvailable)
                return;

            operation.IsAvailable = !_loading && _filterModel.IsValid && (
                                        (_model.Type == SelectionType.Hand && _filterModel.Groups.Any(g => g.IsEnabled) &&
                                         _filterModel.Groups.Where(g => g.IsEnabled)
                                             .All(g => g.Regions.Length > 0 && g.Periods.Length > 0)) ||
                                        (_model.Type == SelectionType.Template &&
                                         _model.RegionalBoundsList.ListItems.Any(x => x.Include) && _filterModel.Groups.Any(g => g.IsEnabled) &&
                                         _filterModel.Groups.Where(g => g.IsEnabled).All(g => g.Periods.Length > 0) &&
                                         (!_model.SelectionList.ListItems.Any() ||
                                          _model.SelectionList.ListItems.All(
                                              x => x.StatusId != SelectionStatus.Loading))));
        }

        private void CalculateViewMenuResetFilter()
        {
            if (_filterModel == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionFilterReset);
            if (operation == null || !operation.IsAvailable)
                return;

            operation.IsAvailable = _filterModel.Groups.Sum(g => g.Parameters.Length) > 0;
        }

        private void CalculateViewMenuReturnParams()
        {
            if (_model == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionFilterReturnParams);
            if (operation == null || !operation.IsAvailable)
                return;

            operation.IsAvailable = _model.IsChanged;
        }

        private void CalculateViewMenuAddFavorite()
        {
            if (_filterModel == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionFilterFavoritesAdd);
            if (operation == null || !operation.IsAvailable)
                return;

            operation.IsAvailable = _filterModel.Groups.Sum(g => g.Parameters.Length) > 0;
        }

        private void CalculateViewMenuSelectionRemove()
        {
            if (_model == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionRemove);
            if (operation == null || !operation.IsAvailable)
                return;

            if (_model.Type == SelectionType.SelectionByTemplate)
            {
                operation.IsAvailable = false;
                return;
            }
            if (_model.Type == SelectionType.Template)
            {
                operation.IsAvailable = !_loading && _proxy.CanDeleteTemplate(_model.Id) && !_model.IsNew;
                return;
            }

            operation.IsAvailable = !_loading && !_model.IsNew;
        }

        private void CalculateViewMenuSelectionRename()
        {
            if (_model == null)
                return;

            var operation =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionChange);
            if (operation == null || !operation.IsAvailable)
                return;

            operation.IsAvailable = !_loading && !_model.IsNew;
        }

        private void CalculateViewMenuSelectionToApprove()
        {
            if (_model == null)
                return;

            var selOper =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionToApprove);
            if (selOper == null || !selOper.IsAvailable)
                return;

            if (_loading ||
                _model.IsSelectionNameChanged ||
                _model.IsFilterChanged ||
                _model.IsNew ||
                string.IsNullOrEmpty(_model.AnalystSid))
            {
                selOper.IsAvailable = false;
            }
        }

        private void CalculateViewMenuSelectionToCorrect()
        {
            if (_model == null)
                return;

            var selOper =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionToCorrect);
            if (selOper == null || !selOper.IsAvailable)
                return;

            if (_model.Type == SelectionType.Template)
            {
                selOper.IsAvailable = _model.SelectionList.ListItems.Where(x => x.IsSelected).Any() &&
                                      _model.SelectionList.ListItems.Where(x => x.IsSelected)
                                          .All(x => x.StatusId == SelectionStatus.RequestForApproval);
            }
        }

        private void CalculateViewMenuSelectionApprove()
        {
            if (_model == null)
                return;

            var selOper =
                _applicableOperations.SingleOrDefault(
                    p => p.Id == Constants.SystemPermissions.Operations.SelectionApproved);
            if (selOper == null || !selOper.IsAvailable)
                return;

            if (_model.Type == SelectionType.Template)
            {
                selOper.IsAvailable = _model.SelectionList.ListItems.Where(x => x.IsSelected).Any() &&
                                      _model.SelectionList.ListItems.Where(x => x.IsSelected)
                                          .All(x => x.StatusId == SelectionStatus.RequestForApproval);
            }
        }

        # endregion

        # region Управление состоянием модели

        private void CreateModel(SelectionDetails data)
        {
            if (_model == null)
            {
                _model = new SelectionDetailsModel(
                    data.Data,
                    data.Lockers,
                    _proxy.TryLoadStatusDictionary(),
                    AllowEdit(data.ApplicableOperations));
            }
            else
            {
                _model.Data = data.Data;
                _model.Lockers = data.Lockers;
                _model.AllowEdit = AllowEdit(data.ApplicableOperations);
            }
            _applicableOperations = data.ApplicableOperations;
            _applicableOperationsOriginal = CloneApplicableOperations(data.ApplicableOperations);

            _model.PropertyChanged += ModelOnPropertyChanged;
        }

        private void UpdateModel(SelectionDetails data)
        {
            _model.Data = data.Data;
            _model.Lockers = data.Lockers;
            _model.AllowEdit = AllowEdit(data.ApplicableOperations);
            _applicableOperations = data.ApplicableOperations;
            _applicableOperationsOriginal = CloneApplicableOperations(data.ApplicableOperations);
        }

        private void ReloadApplicableOperations()
        {
            _applicableOperations = _applicableOperationsOriginal;
            _applicableOperationsOriginal = CloneApplicableOperations(_applicableOperations);
        }

        private SelectionOperation[] CloneApplicableOperations(SelectionOperation[] applicableOperations)
        {
            return applicableOperations.Select(item =>
                    new SelectionOperation
                    {
                        Id = item.Id,
                        IsAvailable = item.IsAvailable
                    })
                .ToArray();
        }

        private bool AllowEdit(SelectionOperation[] applicableOperations)
        {
            var editOperation =
                applicableOperations
                    .FirstOrDefault(x => x.Id == Constants.SystemPermissions.Operations.SelectionChange ||
                                         x.Id == Constants.SystemPermissions.Operations.SelectionTemplateChange);

            return editOperation != null && editOperation.IsAvailable;
        }

        # endregion

        #region Новый фильтр

        private FilterModel _filterModel;
        private FilterParameterCreator _modelCreator;

        private void InitFilter()
        {
            var svc = new FilterDictionaryServiceAdapter(_selectionService, _notifier, _logger);
            _modelCreator = new FilterParameterCreator(svc, _notifier, _model.Type);

            var filter = _model.Filter;

            _filterModel = new FilterModel(
                filter,
                _modelCreator,
                _model.IsNew && _openMode != SelectionOpenModeEnum.CopyTemplate && !_model.LoadingFilterInProcess);
            _view.InitFilterControl(_filterModel);
            _filterModel.Changed += (sender, args) =>
            {
                if (args.Model != null)
                {
                    _view.AddGroupView(args.Model, _model.Type);
                    args.Model.Deleting += (o, eventArgs) => CalculateViewMenuItemsProperty();
                }
                _model.Filter = _filterModel.ToDto();
                CalculateViewMenuItemsProperty();
            };

            foreach (var group in _filterModel.Groups)
            {
                _view.AddGroupView(group, _model.Type);
            }

            _view.SetExpandState();
            CalculateViewMenuItemsProperty();
        }

        #endregion

        # region Блокировка выборки

        private string _lockKey;
        private bool _lockSet;


        private void TryReleaseLock()
        {
            if (SelectionDetailsModel.IsNullOrNew(_model)
                || _model.IsLocked
                || !_lockSet)
            {
                return;
            }

            _proxy.UnLock(_lockKey);
            _lockSet = false;
        }

        private void TrySetLock()
        {
            if (SelectionDetailsModel.IsNullOrNew(_model)
                || _model.IsLocked
                || _lockSet)
            {
                return;
            }

            string lckData = String.Empty;

            if (!_proxy.Lock(_model.Id, (int) ObjectType.Selection, ref lckData))
            {
                _view.ShowWarning(string.Format(LockFailurePattern, lckData));
            }
            else
            {
                _lockKey = lckData;
                _lockSet = true;
            }
        }

        # endregion

        # region Операции над выборкой

        private CommandResult AllowClose()
        {
            if (_model.IsChanged)
            {
                var questionText =
                    _model.IsFilterChanged
                        ? (_model.Type != SelectionType.Template
                            ? ResourceManagerNDS2.SelectionCloseWithoutFiltersSave
                            : ResourceManagerNDS2.TemplateCloseWithoutFiltersSave)
                        : ResourceManagerNDS2.SelectionCloseWithoutNameSave;

                if (_view.ShowQuestion(ResourceManagerNDS2.Attention, questionText) != DialogResult.Yes)
                    return CommandResult.Refuse;
            }

            return CommandResult.Success;
        }

        private CommandResult DoRename()
        {
            var renameView = new ViewRename(_model.Name);

            if (renameView.ShowDialog() == DialogResult.OK)
            {
                var newName = renameView.GetName();
                if (string.IsNullOrEmpty(newName.Trim()))
                {
                    DialogHelper.Error(ResourceManagerNDS2.RenameSelectionEmptyName);
                    return CommandResult.Success;
                }

                if (newName == _model.Name)
                    return CommandResult.Success;

                if (!_selectionService.IsNameUnique(newName, _selectionId == null ? -1 : _selectionId.Value,
                    _model.Type))
                {
                    DialogHelper.Error(ResourceManagerNDS2.RenameSelectionAlreadyExist);
                    return CommandResult.Success;
                }

                _view.SetWindowTitle(_view.GetWindowTitle().Replace(_model.Name, newName));

                _model.Name = newName;
                if (TrySave(ActionType.Edit))
                {
                    _model.IsSelectionNameChanged = false;
                    CalculateViewMenuItemsProperty();
                    return CommandResult.Success;
                }
            }
            else
            {
                return CommandResult.Refuse;
            }
            return CommandResult.Failure;
        }

        private CommandResult DoAssign()
        {
            _model.AnalystSid = _proxy.GetCurrentUserSid();
            if (TrySave(ActionType.Edit))
            {
                _openMode = SelectionOpenModeEnum.EditSelectionByTemplate;
                TryReloadModel();
                _view.SetWindowTitle(string.Format(ResourceManagerNDS2.SelectionCard.EditSelectionByTemplateTitle,
                    _model.Name));
                return CommandResult.Success;
            }
            return CommandResult.Failure;
        }

        private CommandResult DoApplyFilter()
        {
            // LogActivity(ActivityLogEntry.New(ActivityLogType.ManualSelectionCount));
            var sw = Stopwatch.StartNew();

            if (string.IsNullOrEmpty(_model.Name))
            {
                _view.ShowNotification(NameIsRequired);
                return CommandResult.Success;
            }

            if (!_selectionService.IsNameUnique(_model.Name, _selectionId == null ? -1 : _selectionId.Value,
                _model.Type))
            {
                _view.ShowNotification(string.Format(SelectionAlreadyExists,
                    _model.Type == SelectionType.Hand ? "Выборка" : "Шаблон"));
                return CommandResult.Success;
            }

            bool isFilterRegionEmpty = CheckGroupFilterIfFilterRegionEmpty();
            if (isFilterRegionEmpty && _model.Type != SelectionType.Template)
            {
                _view.ShowNotification(RegionIsRequired);
                return CommandResult.Success;
            }

            if (_model.Type == SelectionType.Template && !_model.RegionalBoundsList.ListItems.Any(x => x.Include))
            {
                _view.ShowNotification(RegionIsRequiredForTemplate);
                return CommandResult.Success;
            }

            if (_model.Type == SelectionType.Template &&
                !_model.Filter.Groups
                    .Any(x => x.IsEnabled && x.Parameters
                                  .Any(y => y.AttributeId == TypeCodeTemplateParameterId && y.Values
                                                .Any(z => !string.IsNullOrEmpty(z.Value)))))
            {
                _view.ShowNotification(DiscrepancyTypeIsRequiredForTemplate);
                return CommandResult.Success;
            }

            if (!_filterModel.IsValid)
            {
                _view.ShowNotification(IncorrectAttributesInFilter);
                return CommandResult.Success;
            }

            if (_model.Type == SelectionType.Template)
                _regionalBoundsPresenter.UpdateDateBeforeSave();
            var isNew = _model.IsNew;

            var asyncWorker = new AsyncWorker<bool>();
            asyncWorker.DoWork +=
                (sender, args) =>
                {
                    _loading = true;

                    CalculateViewMenuItemsProperty();

                    if (_model.IsNew && !TrySave(ActionType.Create))
                    {
                        return;
                    }
                    if (TrySaveFilter() && TrySaveRegionalBounds() &&
                        TryApplyFilter(isNew ? ActionType.Create : ActionType.SecondaryApply))
                    {
                        UpdateSelectionRegions();
                        WaitDiscrepanciesFeed();
                    }
                };
            asyncWorker.Complete +=
                (sender, args) =>
                {
                    if (!_view.IsDisposed)
                    {
                        _declarationListPresenter.ResetPagerIndex();
                        _discrepancyListPresenter.ResetPagerIndex();
                        _discrepancySecondPartListPresenter.ResetPagerIndex();
                        TryReloadModel();
                        _view.HideLoadingWindow();
                    }
                    _loading = false;
                    CalculateViewMenuItemsProperty();
                    sw.Stop();
                };

            asyncWorker.Failed +=
                (sender, args) =>
                {
                    sw.Stop();
                    if (!_view.IsDisposed)
                    {
                        TryReloadModel();
                        _view.HideLoadingWindow();
                    }
                    _loading = false;
                    CalculateViewMenuItemsProperty();
                };

            _view.ShowLoadingWindow(asyncWorker.Start);

            return CommandResult.Success;
        }

        private void UpdateSelectionRegions()
        {
            if (_model.Type == SelectionType.Hand)
            {
                string regions =
                    string.Join(",",
                        _filterModel.Groups.Where(x => x.IsEnabled)
                            .SelectMany(x => x.Regions)
                            .Distinct()
                            .Select(
                                x => string.Join(",", x.Code)));

                if (regions != _model.Regions)
                {
                    _model.Regions = regions;
                    _proxy.UpdateSelectionRegions(_model.Id, _model.Regions);
                }
            }
        }

        private CommandResult DoRefresh()
        {
            if (LoadModel(CreateModel))
            {
                SetView();
                _historyListPresenter.UpdateRequired = true;
                _declarationListPresenter.ResetPagerIndex();
                _declarationListPresenter.UpdateRequired = true;
                _discrepancyListPresenter.ResetPagerIndex();
                _discrepancyListPresenter.UpdateRequired = true;
                _discrepancySecondPartListPresenter.ResetPagerIndex();
                _discrepancySecondPartListPresenter.UpdateRequired = true;
                if (_model.Type == SelectionType.Template)
                {
                    _regionalBoundsPresenter.BeginLoad();
                    _selectionListPresenter.BeginLoad();
                }
                return CommandResult.Success;
            }

            return CommandResult.Failure;
        }

        private CommandResult DoRemove()
        {
            if (_model.Type == SelectionType.Hand)
            {
                if (_view.ShowQuestion(
                        ResourceManagerNDS2.Attention,
                        string.Format(ResourceManagerNDS2.SelectionDeleteConfirmMessage, _model.Name)) ==
                    DialogResult.No)
                {
                    return CommandResult.Refuse;
                }

                SelectionCommandParam par = SelectionCommandParam.Empty;
                par.LockKey = _lockKey;

                if (_proxy.TryDelete(_model.GetData(), par))
                {
                    Close();

                    _historyListPresenter.UpdateRequired = true;
                    _selectionListView.RefreshView();

                    return CommandResult.Success;
                }
            }
            if (_model.Type == SelectionType.Template)
            {
                if (_view.ShowQuestion(
                        ResourceManagerNDS2.Attention,
                        string.Format(ResourceManagerNDS2.TemplateSelectionDeleteConfirmMessage, _model.Name)) ==
                    DialogResult.No)
                {
                    return CommandResult.Refuse;
                }

                if (!_proxy.CanDeleteTemplate(_model.Id))
                {
                    _view.ShowInfo(ResourceManagerNDS2.Attention,
                        ResourceManagerNDS2.TemplateSelectionCannotBeDeletedMessage);
                    return CommandResult.Success;
                }

                if (_proxy.TryDeleteSelectionsByTemplate(_model.Id))
                {
                    Close();

                    _historyListPresenter.UpdateRequired = true;
                    _selectionListView.RefreshView();

                    return CommandResult.Success;
                }
            }

            return CommandResult.Failure;
        }

        private CommandResult DoRequestApproval()
        {
            CommandResult result = CommandResult.Failure;
            var statistic = _proxy.TryGetDiscrepancyStatistic(_model.Id, _model.Type);
            if (statistic == null || !statistic.Any())
            {
                _view.ShowNotification(InWorkDiscrepancyNotFound);
                result = CommandResult.Failure;
            }
            else
            {
                SelectionDiscrepancyStatistic discrepStatItems = statistic.FirstOrDefault();
                if (discrepStatItems == null || discrepStatItems.InProcessQty == 0)
                {
                    _view.ShowNotification(InWorkDiscrepancyNotFound);
                    result = CommandResult.Failure;
                }
                else if (TryRequetsApprove())
                {
                    _selectionListView.RefreshView();
                    Close();
                    result = CommandResult.Success;
                }
            }

            return result;
        }

        private bool TryRequetsApprove()
        {
            return _proxy.TrySendToAproval(_model.GetData());
        }


        private CommandResult DoRejectForCorrection()
        {
            if (_proxy.TrySendBackForCorrections(
                _model.GetData(),
                _model.SelectionList.ListItems.Where(x => x.IsSelected).Select(x => x.Id).ToList()))
            {
                if (_model.Type == SelectionType.Template)
                {
                    _selectionListPresenter.BeginLoad();
                }
                else
                    Close();

                _historyListPresenter.UpdateRequired = true;
                _selectionListView.RefreshView();

                return CommandResult.Success;
            }

            return CommandResult.Failure;
        }

        private CommandResult DoApprove()
        {
            var result = CommandResult.Failure;

            var discrepStatItems = _proxy.TryGetDiscrepancyStatistic(_model.Id, _model.Type);
            if (discrepStatItems == null || !discrepStatItems.Any())
                return CommandResult.Failure;

            var discrepStatItemsIds = discrepStatItems.Select(x => x.SelectionId);
            if (_model.Type == SelectionType.Template)
            {
                if (_model.SelectionList.ListItems.Where(x => x.IsSelected)
                        .All(x => discrepStatItemsIds.Contains(x.Id)) &&
                    _model.SelectionList.ListItems.Where(x => x.IsSelected)
                        .All(x => discrepStatItems.FirstOrDefault(y => y.SelectionId == x.Id).InProcessQty > 0))
                {
                    if (TryApprove())
                    {
                        _selectionListPresenter.BeginLoad();
                        result = CommandResult.Success;
                    }
                }
                else if (_view.ShowQuestion(
                             ResourceManagerNDS2.Attention,
                             string.Format(ResourceManagerNDS2.NotAllSelectionsCanBeApproved,
                                 string.Join(", ",
                                     discrepStatItems.Where(x => x.InProcessQty == 0)
                                         .Select(x => x.SelectionName))))
                         == DialogResult.Yes)
                {
                    var idsNotIncluded = _model.SelectionList.ListItems.Where(x => x.IsSelected)
                        .Where(x => !discrepStatItemsIds.Contains(x.Id)
                                    || (discrepStatItemsIds.Contains(x.Id) &&
                                        discrepStatItems.First(y => y.SelectionId == x.Id).InProcessQty == 0))
                        .Select(x => x.Id);
                    _model.SelectionList.ListItems.Where(x => idsNotIncluded.Contains(x.Id))
                        .ForAll(x => x.IsSelected = false);
                    if (TryApprove())
                    {
                        _selectionListPresenter.BeginLoad();
                        result = CommandResult.Success;
                    }
                }
            }
            else
            {
                var statistic = discrepStatItems.SingleOrDefault();
                if (statistic != null && statistic.InProcessQty == 0)
                {
                    _view.ShowNotification(InWorkDiscrepancyNotFound);
                    TryReloadModel();
                    _view.HideLoadingWindow();
                }
                else if (TryApprove())
                {
                    result = CommandResult.Success;
                    Close();
                }
            }

            return result;
        }

        private bool _loading;

        private void WaitDiscrepanciesFeed()
        {
            var keepWaiting = true;
            _model.ApplyFilter();

            while (keepWaiting)
            {
                var response = _selectionService.SyncWithRequestStatus(_model.Id, _model.Type);

                if (response.Status == ResultStatus.Success)
                {
                    if (_model.Type == SelectionType.Hand)
                    {
                        if (response.Result.Count != 1)
                            throw new ApplicationException(
                                string.Format(ResourceManagerNDS2.SelectionRequestNotFound, _model.Id));
                        else
                        {
                            switch (response.Result.First().RequestStatus)
                            {
                                case SelectionRequestStatus.New:
                                case SelectionRequestStatus.InQueue:
                                case SelectionRequestStatus.AcceptedInQueue:
                                case SelectionRequestStatus.PlanReceived:
                                case SelectionRequestStatus.SelectionCreated:
                                case SelectionRequestStatus.SelectionRecorded:
                                    Thread.Sleep(2000);
                                    break;
                                case SelectionRequestStatus.DeclarationsRecorded:
                                    keepWaiting = false;
                                    break;
                                case SelectionRequestStatus.PlanError:
                                case SelectionRequestStatus.SelectionError:
                                case SelectionRequestStatus.SelectionRecordError:
                                case SelectionRequestStatus.DeclarationsRecordError:
                                    throw new ApplicationException(string.Format(ResourceManagerNDS2.LoadError,
                                        response.Result.First().RequestStatus));
                                default:
                                    throw new ApplicationException(string.Format(
                                        ResourceManagerNDS2.SelectionUnknownSatus,
                                        response.Result.First().RequestStatus));
                            }
                        }
                    }
                    else if (_model.Type == SelectionType.Template)
                    {
                        if (response.Result.Count ==
                            response.Result.Where(x => x.RequestStatus == SelectionRequestStatus.DeclarationsRecorded ||
                                                       x.RequestStatus == SelectionRequestStatus.PlanError ||
                                                       x.RequestStatus == SelectionRequestStatus.SelectionError ||
                                                       x.RequestStatus == SelectionRequestStatus.SelectionRecordError ||
                                                       x.RequestStatus ==
                                                       SelectionRequestStatus.DeclarationsRecordError)
                                .Count())
                        {
                            keepWaiting = false;
                            if (_openMode == SelectionOpenModeEnum.CopyTemplate)
                                _openMode = SelectionOpenModeEnum.EditTemplate;
                        }
                        else
                        {
                            Thread.Sleep(2000);
                        }
                    }
                }
            }
        }

        private bool TryApplyFilter(ActionType action)
        {
            if (_model.Type == SelectionType.Hand && action == ActionType.SecondaryApply)
            {
                //Обновляем статус в Загружается
                TrySave(action);
            }
            return _model.Type == SelectionType.Hand
                ? _proxy.TryRequestSelection(_model.GetData(), _model.Type, _model.Filter, action)
                : _proxy.TryRequestSelections(_model.Id, _model.Name, _model.Filter, _model
                    .RegionalBoundsList.ListItems.Select(x =>
                    {
                        var data = x.Data;
                        data.Include = data.Include ?? 1;
                        return data;
                    })
                    .ToList(), action);
        }

        private bool TryReloadModel()
        {
            if (!LoadModel(UpdateModel))
                return false;

            SetView();

            return true;
        }

        private bool TrySave(ActionType action)
        {
            var savedSelection = _proxy.TrySave(_model.GetData(), action);
            if (savedSelection == null)
                return false;

            _selectionId = savedSelection.Id;
            _model.Id = _selectionId.Value;
            _model.Status = _model.Status ?? (_model.Type != SelectionType.Template
                                ? SelectionStatus.Loading
                                : (SelectionStatus?) null);
            _selectionListView.RefreshView();
            if (!_view.IsDisposed)
            {
                _historyListPresenter.UpdateRequired = true;
            }
            return true;
        }

        private bool TrySaveFilter()
        {
            return _proxy.TrySaveFilter(_model.Id, _model.Filter);
        }

        private bool TrySaveRegionalBounds()
        {
            if (_model.Type != SelectionType.Template)
                return true;

            return _proxy.TryUpdateRegionalBounds(_model.Id,
                _model.RegionalBoundsList.ListItems.Select(x => x.Data).ToList());
        }

        private bool TryApprove()
        {
            bool ret = false;
            if (_proxy.TryApprove(_model.GetData(),
                _model.SelectionList.ListItems.Where(x => x.IsSelected).Select(x => x.Id).ToList()))
            {
                ret = true;
                _historyListPresenter.UpdateRequired = true;
                _selectionListView.RefreshView();

                if (_model.Type == SelectionType.Template)
                {
                    _selectionListPresenter.UpdateRequired = true;
                }
            }
            return ret;
        }

        # endregion

        # region Работа с избранным

        /// <summary>
        /// Инициализирует представление ViewFavorites  в режиме сохранения нового фильтра
        /// </summary>
        private CommandResult DoAddToFavorites()
        {
            var presenter = new ManageFavoritesPresenter(this, _manageFavoritesProxy);
            var view = new ViewFavorites(presenter)
                .WithAdd(true)
                .WithEdit(false)
                .WithTitle(ResourceManagerNDS2.AddFavoriteSelectionFilterTitle);

            presenter.SetView(view);
            presenter.GetList();

            view.OpenDialog();

            return CommandResult.Success;
        }

        /// <summary>
        /// Инициализирует представление ViewFavorites в режиме просмотра/применения/удаления фильтров
        /// </summary>
        private CommandResult DoManageFavorites()
        {
            var presenter = new ManageFavoritesPresenter(this, _manageFavoritesProxy);
            var view = new ViewFavorites(presenter)
                .WithAdd(false)
                .WithEdit(true)
                .WithTitle(ResourceManagerNDS2.ManageFavoriteSelectionFilterTitle);

            presenter.SetView(view);
            presenter.GetList();

            view.OpenDialog();

            return CommandResult.Success;
        }

        private CommandResult DoResetFilter()
        {
            _model.ResetFilter();
            InitFilter();

            return CommandResult.Success;
        }

        private CommandResult DoClearFilter()
        {
            var hasDiscrepancies = _model.IsFilterTakeData;

            if (_filterModel.Groups.Any()
                && _view.ShowQuestion(
                    ResourceManagerNDS2.Attention,
                    hasDiscrepancies
                        ? ResourceManagerNDS2.ParamFiltrationAndResultResetConfirm
                        : ResourceManagerNDS2.ParamFiltrationResetConfirm) == DialogResult.Yes)
            {
                _model.ClearFilter();

                if (!hasDiscrepancies)
                {
                    InitFilter();

                    return CommandResult.Success;
                }
                return
                    TryReloadModel()
                        ? CommandResult.Success
                        : CommandResult.Failure;
            }

            return CommandResult.Refuse;
        }

        /// <summary>
        /// Задает или возвращает фильтр текущей выборки
        /// </summary>
        public Filter CurrentFilter
        {
            get { return _model.Filter; }
        }

        # endregion

        # region Разное

        private bool CheckGroupFilterIfFilterRegionEmpty()
        {
            var result = false;
            foreach (var group in _filterModel.Groups)
            {
                if (group.IsEnabled && group.Regions.Length == 0)
                    result = true;
            }
            return result;
        }

        # endregion

        /// <summary>
        /// Задает фильтр текущей выборке
        /// </summary>
        /// <param name="favorite"></param>
        /// <param name="filter">Фильтр выборки</param>
        public void SetFilter(KeyValuePair<long, string> favorite, Filter filter)
        {
            _model.LoadingFilterInProcess = true;
            _model.Filter = filter;
            InitFilter();
            //     TryReloadModel();
            _model.LoadingFilterInProcess = false;
        }

        # region Выбор аналитика

        public Dictionary<string, string> GetData(string nameData, DictionaryConditions criteria = null)
        {
            return _dataService.GetDictionaryData(nameData, criteria).Result;
        }

        # endregion
    }

    public static class GridItemConvertionHelper
    {
        public static void DoWithConvertion<T>(this object gridItem, Action<T> action)
            where T : class
        {
            gridItem.WithConvertion(action)();
        }

        public static Action WithConvertion<T>(this object gridItem, Action<T> action)
            where T : class
        {
            return () =>
            {
                var typedItem = gridItem as T;

                if (typedItem != null)
                {
                    action(typedItem);
                }
            };
        }
    }
}