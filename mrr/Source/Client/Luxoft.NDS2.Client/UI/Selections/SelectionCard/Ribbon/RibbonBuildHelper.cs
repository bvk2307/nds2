﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base.Ribbon;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard.Ribbon
{
    public static class RibbonBuildHelper
    {
        public static RibbonGroup WithApplySelectionFilter(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionDetailsApplyFilter")
                        .WithText(ResourceManagerNDS2.SelectionFilterApplyTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterApplyDescription)
                        .WithCommonResource()
                        .WithImage("demo_32_32"));
        }

        public static RibbonGroup WithFilterClear(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionDetailsClearFilter")
                        .WithText(ResourceManagerNDS2.SelectionFilterResetTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterResetDescription)
                        .WithCommonResource()
                        .WithImage("view_remove_32_32"));
        }

        public static RibbonGroup WithFilterReset(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Normal,
                    context
                        .NewButton("SelectionDetailsResetFilter")
                        .WithText(ResourceManagerNDS2.SelectionFilterRetParamTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterRetParamDescription)
                        .WithCommonResource()
                        .WithImage("view_detailed_32_32"));
        }

        public static RibbonGroup WithSaveSelectionFilter(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Normal,
                    context
                        .NewButton("SelectionDetailsAddFilterToFavorites")
                        .WithText(ResourceManagerNDS2.SelectionFilterFavoritesAddTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterFavoritesAddDescription)
                        .WithCommonResource()
                        .WithImage("filesave_32_32"));
        }

        public static RibbonManager WithLoadSelectionFilter(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithLastButton(
                    UcRibbonToolSize.Normal,
                    context
                        .NewButton("SelectionDetailsManageFavorites")
                        .WithText(ResourceManagerNDS2.SelectionFilterFavoritesTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionFilterFavoritesDescription)
                        .WithCommonResource()
                        .WithImage("revert_32_32"));
                    
        }

        public static RibbonGroup WithRenameSelection(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group.WithButton(
                UcRibbonToolSize.Large,
                context
                    .NewButton("SelectionDetailsRename")
                    .WithText(ResourceManagerNDS2.SelectionRenameTitle)
                    .WithToolTip(ResourceManagerNDS2.SelectionRenameDescription)
                    .WithCommonResource()
                    .WithImage("pencil_32_32"));
        }

        public static RibbonManager WithTakeInWorkSelection(
           this RibbonGroup group,
           PresentationContext context)
        {
            return group.WithLastButton(
                UcRibbonToolSize.Large,
                context
                    .NewButton("SelectionDetailsTakeInWork")
                    .WithText(ResourceManagerNDS2.SelectionTakeInWorkTitle)
                    .WithToolTip(ResourceManagerNDS2.SelectionTakeInWorkDescription)
                    .WithCommonResource()
                    .WithImage("bookmark_toolbar_32_32"));
        }

        public static RibbonGroup WithRemove(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                    .NewButton("SelectionRemove")
                    .WithText(ResourceManagerNDS2.SelectionDeleteTitle)
                    .WithToolTip(ResourceManagerNDS2.SelectionDeleteDescription)
                    .WithCommonResource()
                    .WithImage("window_suppressed_32_32"));
        }

        public static RibbonGroup WithApprove(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionApprove")
                        .WithText(ResourceManagerNDS2.SelectionApprovedTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionApprovedDescription)
                        .WithCommonResource()
                        .WithImage("clean_32_32"));
        }

        public static RibbonGroup WithUpdate(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionDetailsUpdate")
                        .WithText(ResourceManagerNDS2.SelectionUpdateTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionUpdateDescription)
                        .WithCommonResource()
                        .WithImage("update"));
        }

        public static RibbonManager WithRequestApproval(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithLastButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionRequestApproval")
                        .WithText(ResourceManagerNDS2.SelectionToApproveTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionToApproveDescription)
                        .WithCommonResource()
                        .WithImage("up_32_32"));
        }

        public static RibbonGroup WithRejectForCorrection(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("SelectionRejectForCorrection")
                        .WithText(ResourceManagerNDS2.SelectionToCorrectTitle)
                        .WithToolTip(ResourceManagerNDS2.SelectionToCorrectDescription)
                        .WithCommonResource()
                        .WithImage("down_32_32"));
        }

        public static UcRibbonButtonToolContext WithCommonResource(
            this UcRibbonButtonToolContext button)
        {
            return button.WithResource(ResourceManagerNDS2.NDS2ClientResources);
        }
    }
}
