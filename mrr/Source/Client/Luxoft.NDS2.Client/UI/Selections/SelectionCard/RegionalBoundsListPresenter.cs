﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class RegionalBoundsListPresenter :
        GridViewPresenter<SelectionRegionalBoundsViewModel, SelectionRegionalBoundsDataItem, List<SelectionRegionalBoundsDataItem>>
    {
        public event EventHandler RegionalBoundsLoaded;

        private readonly RegionalBoundsListGridView _view;
        public RegionalBoundsListPresenter(
            IServerGridDataProvider<List<SelectionRegionalBoundsDataItem>> dataProvider,
            IChangeableListViewModel<SelectionRegionalBoundsViewModel, SelectionRegionalBoundsDataItem> model,
            RegionalBoundsListGridView view) : base(dataProvider, model, view)
        {
            _view = view;
            _view.CellChanged += CheckboxStateChanged;
            _view.AfterGroupRegBoundsCheckStateChanged += OnAfterGroupRegBoundsCheckStateChanged;

        }

        public event EventHandler RegionalBoundsChecked;

        private void CheckboxStateChanged(object sender, SelectedCellEventArgs e)
        {
            var selectedRow = (SelectionRegionalBoundsViewModel)_view.SelectedItem;
            if (selectedRow != null)
            {
                    selectedRow.Include = !(bool)e.Value;
                    if (RegionalBoundsChecked != null)
                        RegionalBoundsChecked(sender, e);
            }
        }

        private void OnAfterGroupRegBoundsCheckStateChanged(object sender, EventArgs e)
        {
           var headerEventArgs = e as AfterHeaderCheckStateChangedEventArgs;

            if (headerEventArgs != null)
            {
                var headerState = headerEventArgs.Column.GetHeaderCheckedState(
                    headerEventArgs.Rows);
                if (headerState == CheckState.Indeterminate)
                    return;

                var firstRow = headerEventArgs.Rows[0];
                if (RegionalBoundsChecked != null)
                    RegionalBoundsChecked(sender, e);
            }
        }
        public void RefreshView()
        {
            if (_view.Visible && UpdateRequired)
            {
                BeginLoad();
                UpdateRequired = false;
            }
        }

        private bool _updateRequired;
        public bool UpdateRequired
        {
            get { return _updateRequired; }
            set
            {
                if (value != _updateRequired)
                {
                    _updateRequired = value;
                    RefreshView();
                }
            }
        }

        protected override void AfterDataLoaded(List<SelectionRegionalBoundsDataItem> response)
        {
            if (RegionalBoundsLoaded != null)
            {
                RegionalBoundsLoaded(this, EventArgs.Empty);
            }
        }

        public void UpdateDateBeforeSave()
        {
            _view.UpdateCellChangedData();
        }
    }
}