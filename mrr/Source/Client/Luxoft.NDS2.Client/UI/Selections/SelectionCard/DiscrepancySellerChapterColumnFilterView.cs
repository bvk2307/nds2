﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DiscrepancySellerChapterColumnFilterView : GridColumnFilterView
    {
        public DiscrepancySellerChapterColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }
    

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var dictionaryItem in DiscrepancySellerChapterColumnView.SellerChapterDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class DiscrepancySellerChapterColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<SellerChapter, string> SellerChapterDictionary =
            new Dictionary<SellerChapter, string>()
           {
                {SellerChapter.SellBook, "Книга продаж"},
                {SellerChapter.InvoiceJournalOut, "Журнал выставленных СФ"},
                {SellerChapter.InvoiceJournalIn, "Журнал полученных СФ"},
                {SellerChapter.Chapter12, "Раздел 12"},
                {SellerChapter.Unknown, "Неизвестен" }
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();
            foreach (var dictionaryItem in SellerChapterDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
