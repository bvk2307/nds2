﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DeclarationKnpStatusColumnFilterView : GridColumnFilterView
    {
        public DeclarationKnpStatusColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            DeclarationKnpStatusView.StatusDictionary[KnpStatus.Empty] = "(Пусто)";
            foreach (var dictionaryItem in DeclarationKnpStatusView.StatusDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }
    public static class DeclarationKnpStatusView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<KnpStatus, string> StatusDictionary =
            new Dictionary<KnpStatus, string>()
           {
                {KnpStatus.Empty, " " },
                {KnpStatus.Closed, "Закрыта"},
                {KnpStatus.Opened, "Открыта"}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();

            StatusDictionary[KnpStatus.Empty] = " ";

            foreach (var dictionaryItem in StatusDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
