﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class TaxAuthorityDataProvider : ILookupDataProvider<TaxAuthorityModel>
    {
        private readonly IDataService _dataService;

        public TaxAuthorityDataProvider(IDataService dataService)
        {
            _dataService = dataService;
        }

        public IDataRequestResult<TaxAuthorityModel> Search(ITaxAuthorityRequestArgs args)
        {
            return new DataRequestResult<TaxAuthorityModel>(
                _dataService
                .SearchInspections(args.SearchPattern, args.MaxQuantity)
                .Result
                .Where(dto => !args.RestrictByRegion || args.RegionCodes.Any(reg => dto.Key.StartsWith(reg)))
                .Select(dto => new TaxAuthorityModel() { Code = dto.Key, Name = dto.Value})
                .AsEnumerable());
        }

        public IDataRequestResult<TaxAuthorityModel> Search(IDataRequestArgs args)
        {
            return new DataRequestResult<TaxAuthorityModel>(
                _dataService
                .SearchInspections(args.SearchPattern, args.MaxQuantity)
                .Result
                .Select(dto => new TaxAuthorityModel() { Code = dto.Key, Name = dto.Value })
                .AsEnumerable());
        }
    }
}
