﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class FilterEditorDataProvider : ILookupDataSource
    {
        private readonly IDataService _dataService;

        public FilterEditorDataProvider(IDataService dataService)
        {
            _dataService = dataService;
        }

        public Func<Dictionary<string, string>> SimpleLookup(string lookupName)
        {
            return () => _dataService.GetDictionaryData(lookupName, new DictionaryConditions()).Result;
        }

        public IDataRequestResult<TEntityLookup> Search<TEntityLookup>(IDataRequestArgs criteria) 
            where TEntityLookup : ILookupEntry<string>
        {
            if (typeof(TEntityLookup) == typeof(RegionModel))
            {
                return new RegionsDataProvider(_dataService).Search(criteria) as IDataRequestResult<TEntityLookup>;
            }

            if (typeof(TEntityLookup) == typeof(TaxAuthorityModel))
            {
                return new TaxAuthorityDataProvider(_dataService).Search(criteria) as IDataRequestResult<TEntityLookup>;
            }

            throw new NotSupportedException();
        }

        public IDataRequestResult<TaxAuthorityModel> Search(Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ITaxAuthorityRequestArgs criteria)
        {
            return new TaxAuthorityDataProvider(_dataService).Search(criteria);
        }
    }

    public interface ILookupDataProvider<TLookupEntry>
        where TLookupEntry : ILookupEntry<string>
    {
        IDataRequestResult<TLookupEntry> Search(IDataRequestArgs args);
    }
}
