﻿using System;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Utils;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Controls;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Diagnostics;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    public class DeclarationListPresenter :
        PagingGridViewPresenter<DeclarationListItemViewModel, SelectionDeclaration, PageResult<SelectionDeclaration>>
    {
        private readonly DeclarationListGridView _view;
        private readonly DeclarationListViewModel _model;
        private readonly IGridPagerView _pager;
        public DeclarationListPresenter(
            IServerGridDataProvider<PageResult<SelectionDeclaration>> dataProvider,
            DeclarationListViewModel model,
            DictionarySur sur,
            DeclarationListGridView gridView, 
            IGridPagerView pagerView) : base(dataProvider, model, gridView, pagerView)
        {
            _view = gridView;
            _model = model;
            _pager = pagerView;
            gridView.AfterGroupDeclarationCheckStateChanged += GridViewOnAfterDeclarartionCheckStateChanged;
            gridView.CellClicked += CellClicked;
        }

        private void CellClicked(object sender, DeclarationItemEventArgs e)
        {
            var row = sender as DeclarationListItemViewModel;
            if(row != null)
            {
                e.IsDeclaration = row.Type == Common.Contracts.DTO.Selections.Enums.DeclarationType.Declaration;
            }
        }

        public void ResetPagerIndex()
        {
            QueryConditions.PaginationDetails.RowsToSkip = 0;
            _pager.ResetPageIndex();
        }

        public void RefreshView()
        {
            if (_view.Visible && UpdateRequired)
            {
                QueryConditions.PaginationDetails.SkipTotalAvailable = false;
                QueryConditions.PaginationDetails.SkipTotalMatches = false;
                BeginLoad();
                UpdateRequired = false;
            }
        }

        private bool _updateRequired;
        public bool UpdateRequired
        {
            get { return _updateRequired; }
            set
            {
                if (value != _updateRequired)
                {
                    _updateRequired = value;
                    RefreshView();
                }
            }
        }

        public EventHandler<DeclarationItemEventArgs> AfterGroupDeclarationCheckStateChanged;
        public EventHandler<DeclarationItemEventArgs> AfterDeclarationCheckStateChanged;
        /// <summary>
        /// Событие на изменение состояния чек бокса в шапке колонки Include
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridViewOnAfterDeclarartionCheckStateChanged(object sender, EventArgs e)
        {
            var cellChanged = e as CellEventArgs;

            if (cellChanged != null)
            {
                bool isChecked = bool.Parse(cellChanged.Cell.Text);
                RowCheckedChange(
                    cellChanged.Cell.Row.ListObject,
                    isChecked);

                if (AfterDeclarationCheckStateChanged != null)
                {
                    AfterDeclarationCheckStateChanged(sender,
                        new DeclarationItemEventArgs() { IsChecked = isChecked,
                            DeclarationId = ((DeclarationListItemViewModel)cellChanged.Cell.Row.ListObject).Zip });
                }
            }

            var headerEventArgs = e as AfterHeaderCheckStateChangedEventArgs;

            if (headerEventArgs != null && !_disableHeaderEvents)
            {
                var headerState = headerEventArgs.Column.GetHeaderCheckedState(
                    headerEventArgs.Rows);
                if (headerState == CheckState.Indeterminate)
                    return;

                _model.ListItems.ForAll(x => x.Included = headerState == CheckState.Checked);
                _view.CheckAllRows(headerState == CheckState.Checked);

                if (AfterGroupDeclarationCheckStateChanged != null)
                {
                    AfterGroupDeclarationCheckStateChanged(sender,
                        new DeclarationItemEventArgs() {IsChecked = headerState == CheckState.Checked});
                }
            }
        }

        private void RowCheckedChange(object rowDataItem, bool isChecked)
        {
            var item = (DeclarationListItemViewModel)rowDataItem;
            var modelItem = _model.ListItems.First(x => x.Zip == item.Zip);
            modelItem.Included = isChecked;

            _disableHeaderEvents = true;

            if (isChecked)
            {
                _view.AllIncluded = _view.AllRowsCheckedState(true);
            }
            else
            {
                _view.AllIncluded = false;
            }

            _disableHeaderEvents = false;
        }

        public void UpdateHeaderCheckBoxState()
        {
            _disableHeaderEvents = true;
            if (_view.AllRowsCheckedState(true))
            {
                _view.AllIncluded = true;
            }
            else if (_view.AllRowsCheckedState(false))
            {
                _view.AllIncluded = false;
            }
            _disableHeaderEvents = false;
        }

        public QueryConditions GetSearchCondition()
        {
            return QueryConditions.Clone() as QueryConditions;
        }

        protected override void AfterDataLoaded(PageResult<SelectionDeclaration> data)
        {
            _disableHeaderEvents = true;
            base.AfterDataLoaded(data);
            UpdateHeaderCheckBoxState();
            _disableHeaderEvents = false;
        }

        private bool _disableHeaderEvents = false;
        protected override void BeforeDataLoaded()
        {
            _disableHeaderEvents = true;
            _view.AllIncluded = false;
        }

        
    }
}