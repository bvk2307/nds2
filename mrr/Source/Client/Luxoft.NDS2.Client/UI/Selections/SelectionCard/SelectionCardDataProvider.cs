﻿using System;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Selections.SelectionCard
{
    /// <summary>
    ///     Класс реализует загрузку данных карточки выборки
    /// </summary>
    public class SelectionCardDataProvider<TData> :
        ServiceProxyBase<PageResult<TData>>,
        IServerGridDataProvider<PageResult<TData>>
    {
        private readonly Func<QueryConditions, OperationResult<PageResult<TData>>> _dataLoader;

        public SelectionCardDataProvider(
            INotifier notifier,
            IClientLogger logger,
            Func<QueryConditions, OperationResult<PageResult<TData>>> dataLoader) : base(notifier, logger)
        {
            _dataLoader = dataLoader;
        }

        public event EventHandler<DataLoadedEventArgs<PageResult<TData>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _dataLoader(queryConditions));
        }

        protected override void CallBack(PageResult<TData> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<PageResult<TData>>(result));
        }
    }
}