﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    public class Model : IInvoiceClaimListConditionProvider, IDocSendStatusConditionProvider
    {
        public DateTime Date { get; set; }

        private int[] _statuses = {};
        
        public int[] Statuses
        {
            set
            {
                var noChanges = _statuses.Length == value.Length && _statuses.SequenceEqual(value);
                if (noChanges)
                    return;

                _statuses = value;

                var handler = StatusesChanged;
                if (handler != null)
                    handler(this, new EventArgs());
            }
            get
            {
                return _statuses;
            }
        }

        public bool ToSend { get; set; }

        public InvoiceClaimListViewModel InvoiceClaimList { get; set; }
        
        public bool AllowIncludeExclude { get; set; }
        
        public DocSendStatus SendStatus { get; set; }

        public bool AllowSend { get; private set; }

        public LookupModel StatusLookupModel { get; set; }

        public DateTime StartNewArchitectureDate { set; private get; }

        public bool AllowSelectionStatsReport {get { return DateTime.Compare(Date, StartNewArchitectureDate) >= 0; }}

        public Model()
        {
            InvoiceClaimList = new InvoiceClaimListViewModel();
            InvoiceClaimList.ListUpdateCompleted += (sender, args) =>
            {
                //TODO завести enum статусов
                AllowSend = !SendStatus.WasSent && InvoiceClaimList.ListItems.Any(x => !x.IsExcluded && x.StatusCode == -1);
                AllowIncludeExclude = !SendStatus.WasSent;
            };
            StatusLookupModel = new LookupModel(new ISelectableLookupItem[]{});

            StatusLookupModel.SelectionChanged += (sender, args) =>
            {
                Statuses = StatusLookupModel.Selection.Select(x => ((StatusLookupItem)x).Data.Id).ToArray();
            };
        }

        public event EventHandler StatusesChanged;
    }
}
