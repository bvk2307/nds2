﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    class InvoiceClaimListPresenter : PagingGridViewPresenter<InvoiceClaimViewModel, InvoiceClaim, PageResult<InvoiceClaim>>
    {
        public InvoiceClaimListPresenter(
            IServerGridDataProvider<PageResult<InvoiceClaim>> dataProvider,
            IChangeableListViewModel<InvoiceClaimViewModel, InvoiceClaim> model,
            InvoiceClaimListGridView gridView, IGridPagerView pagerView) : base(dataProvider, model, gridView, pagerView)
        {
        }

        public QueryConditions GetSearchCondition()
        {
            return QueryConditions.Clone() as QueryConditions;
        }
    }

}
