﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    internal class ReportInvoiceClaimListDataLoader : ServiceProxyBase<List<InvoiceClaimReportItem>>
    {
        private readonly IInvoiceClaimReportService _service;

        public ReportInvoiceClaimListDataLoader(IInvoiceClaimReportService service, INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        public void BeginLoad(DateTime date, List<FilterQuery> filter)
        {
            BeginInvoke(() => _service.GetInvoiceClaimList(date, filter));
        }

        public void BeginLoad(DateTime date, int[] statuses, List<FilterQuery> filter)
        {
            BeginInvoke(() => _service.GetInvoiceClaimListByStatus(date, statuses, filter));
        }
        public event EventHandler<DataLoadedEventArgs<List<InvoiceClaimReportItem>>> DataLoaded;

        protected override void CallBack(List<InvoiceClaimReportItem> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<InvoiceClaimReportItem>>(result));
        }
    }

}
