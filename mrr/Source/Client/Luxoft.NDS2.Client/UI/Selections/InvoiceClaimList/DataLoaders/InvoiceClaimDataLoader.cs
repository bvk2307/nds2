﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    public interface IInvoiceClaimListConditionProvider
    {
        DateTime Date { get; }

        int[] Statuses { get; }

        bool ToSend { get; }
    }

    /// <summary>
    /// Класс реализует загрузку списка АТ по СФ
    /// </summary>
    public class InvoiceClaimDataLoader : ServiceProxyBase<PageResult<InvoiceClaim>>,
        IServerGridDataProvider<PageResult<InvoiceClaim>>
    {
        private readonly IInvoiceClaimService _service;
        private readonly IInvoiceClaimListConditionProvider _conditionProvider;

        public InvoiceClaimDataLoader(
            IInvoiceClaimService service,
            IInvoiceClaimListConditionProvider conditionProvider,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
            _conditionProvider = conditionProvider;
        }

        public event EventHandler<DataLoadedEventArgs<PageResult<InvoiceClaim>>> DataLoaded;
        public List<FilterQuery> Filter { get; private set; }

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(
                () =>
                {
                    queryConditions.PaginationDetails.SkipTotalMatches = false;

                    var qc = (QueryConditions) queryConditions.Clone();
                    qc.ReplaceSortColumn(
                        TypeHelper<InvoiceClaim>.GetMemberName(i => i.DeclSignCode), 
                        TypeHelper<InvoiceClaim>.GetMemberName(i => i.DeclarationSign));
                    qc.ReplaceSortColumn(
                       TypeHelper<InvoiceClaim>.GetMemberName(i => i.CloseTypeId),
                       TypeHelper<InvoiceClaim>.GetMemberName(i => i.CloseType));
                    qc.ReplaceSortColumn(TypeHelper<InvoiceClaim>.GetMemberName(i => i.TaxPeriod), "TaxPeriodSort");

                    if (!qc.Sorting.Any())
                    {
                        qc.Sorting = new List<ColumnSort>
                            {
                                new ColumnSort {ColumnKey = "RegionCode", Order = ColumnSort.SortOrder.Asc},
                                new ColumnSort {ColumnKey = "TaxPeriodSort", Order = ColumnSort.SortOrder.Desc}
                            };
                    }

                    Filter = qc.Filter;

                    if (_conditionProvider.ToSend)
                        return _service.GetClaimsToSend(qc, _conditionProvider.Date);

                    return _conditionProvider.Statuses == null || _conditionProvider.Statuses.Length == 0
                        ? new OperationResult<PageResult<InvoiceClaim>>
                        {
                            Result = new PageResult<InvoiceClaim> {Rows = new List<InvoiceClaim>(), TotalMatches = 0},
                            Status = ResultStatus.Success
                        }
                        : _service.GetClaims(qc, _conditionProvider.Date, _conditionProvider.Statuses);
                }
                );
        }

        protected override void CallBack(PageResult<InvoiceClaim> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<PageResult<InvoiceClaim>>(result));
        }

    }
}
