﻿using System;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services.Claims;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class ClaimsCountLoader : ServiceProxyBase<int>
    {
        private readonly IInvoiceClaimService _service;

        public ClaimsCountLoader(INotifier notifier, IClientLogger logger, IInvoiceClaimService service)
            : base(notifier, logger)
        {
            _service = service;
        }

        public int Count(DateTime date)
        {
            int result;

            if (!Invoke(() => _service.GetCountClaimsToSend(date), out result))
            {
                _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.InvoiceClaimList.ServiceError,
                        ResourceManagerNDS2.InvoiceClaimList.GetClaimsCountError
                        ));
            }

            return result;
        }
    }
}
