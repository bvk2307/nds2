﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class TaxMonitoringDataLoader : ServiceProxyBase<List<TaxMonitoringReportItem>>
    {
        private readonly IInvoiceClaimReportService _service;

        public TaxMonitoringDataLoader(IInvoiceClaimReportService service, INotifier notifier, IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public void BeginLoadChapter(long docId, int chapter)
        {
            BeginInvoke(() => _service.GetReportForChapters(docId, chapter));
        }

        public void BeginLoadUnconfirmed(long docId)
        {
            BeginInvoke(() => _service.GetReportUnconfirmed(docId));
        }

        public event EventHandler<DataLoadedEventArgs<List<TaxMonitoringReportItem>>> DataLoaded;

        protected override void CallBack(List<TaxMonitoringReportItem> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<TaxMonitoringReportItem>>(result));
        }
    }
}
