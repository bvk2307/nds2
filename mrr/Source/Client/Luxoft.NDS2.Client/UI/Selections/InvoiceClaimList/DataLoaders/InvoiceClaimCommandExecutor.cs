﻿using System;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls;
using Luxoft.NDS2.Common.Contracts.Services.Claims;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class InvoiceClaimCommandExecutor
    {
        private readonly IInvoiceClaimService _service;
        private readonly CurrentUserInfo _userInfo;
        private readonly SimpleServiceProxy _proxy;

        public InvoiceClaimCommandExecutor(
            IInvoiceClaimService service,
            CurrentUserInfo userInfo,
            SimpleServiceProxy proxy)
        {
            _service = service;
            _userInfo = userInfo;
            _proxy = proxy;
        }

        public void BeginInclude(long docId)
        {
            _proxy.BeginInvoke(() =>
                _service.Include(docId, _userInfo.UserDisplayName),
                r => Included.Raise(this, new EventArgs()));
        }

        public void BeginExclude(long docId)
        {
            _proxy.BeginInvoke(() =>
                _service.Exclude(docId, _userInfo.UserDisplayName),
                r => Excluded.Raise(this, new EventArgs()));
        }

        public event EventHandler Included;

        public event EventHandler Excluded;

        public event EventHandler Sent;

        public void Send(DateTime date)
        {
            _proxy.BeginInvoke(() =>
                _service.Send(date, _userInfo.UserDisplayName),
                r => Sent.Raise(this, new EventArgs()));
        }
    }
}
