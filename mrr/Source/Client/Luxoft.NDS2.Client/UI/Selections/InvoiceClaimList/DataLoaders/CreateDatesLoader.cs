﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class CreateDatesLoader : ServiceProxyBase<List<DateTime>>
    {
        private readonly IInvoiceClaimService _service;

        public CreateDatesLoader(INotifier notifier, IClientLogger logger, IInvoiceClaimService service) : base(notifier, logger)
        {
            _service = service;
        }

        public DateTime[] Load()
        {
            List<DateTime> result;

            if (!Invoke(() => _service.GetCreateDates(), out result))
            {
                _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.InvoiceClaimList.ServiceError,
                        ResourceManagerNDS2.InvoiceClaimList.CreateDatesLoadingFault
                        ));
            }

            return (result != null)
                ? result.ToArray()
                : new DateTime[] {};
        }
    }
}
