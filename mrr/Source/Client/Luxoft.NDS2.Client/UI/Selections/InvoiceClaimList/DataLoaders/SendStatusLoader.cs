﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    public interface IDocSendStatusConditionProvider
    {
        DateTime Date { get; }
    }

    public class SendStatusLoader : ServiceProxyBase<DocSendStatus>
    {
        private readonly IDocSendStatusConditionProvider _conditionProvider;
        private readonly IInvoiceClaimService _service;

        public SendStatusLoader(IDocSendStatusConditionProvider conditionProvider, INotifier notifier, IClientLogger logger, IInvoiceClaimService service)
            : base(notifier, logger)
        {
            _conditionProvider = conditionProvider;
            _service = service;
        }

        public DocSendStatus Load()
        {
            DocSendStatus result;

            if (!Invoke(() => _service.GetSendStatus(_conditionProvider.Date), out result))
            {
                _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.InvoiceClaimList.ServiceError,
                        ResourceManagerNDS2.InvoiceClaimList.SendStatusLoadingFault
                        ));
            }

            return result ?? new DocSendStatus { WasSent = false };
        }
    }
}
