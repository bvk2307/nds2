﻿using System.Linq;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class StatusListDataLoader : ServiceProxyBase<List<SimpleStatusDto>>
    {
        private readonly IInvoiceClaimService _service;

        public StatusListDataLoader(INotifier notifier, IClientLogger logger, IInvoiceClaimService service)
            : base(notifier, logger)
        {
            _service = service;
        }

        public List<ClaimStatusModel> Load()
        {
            List<SimpleStatusDto> result;

            if (!Invoke(() => _service.GetStatusList(), out result))
            {
                _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.InvoiceClaimList.ServiceError,
                        ResourceManagerNDS2.InvoiceClaimList.StatusListLoadingFault
                        ));
            }

            return result.Select(x => new ClaimStatusModel(x)).ToList();
        }
    }
}
