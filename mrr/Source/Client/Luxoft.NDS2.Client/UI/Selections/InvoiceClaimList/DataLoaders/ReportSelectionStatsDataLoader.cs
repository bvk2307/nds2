﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders
{
    class ReportSelectionStatsDataLoader : ServiceProxyBase<List<SelectionClaimCompare>>
    {
        private readonly IInvoiceClaimReportService _service;

        public ReportSelectionStatsDataLoader(IInvoiceClaimReportService service, INotifier notifier, IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        public void BeginLoad(DateTime date)
        {
            BeginInvoke(() => _service.GetSelectionStats(date));
        }

        public event EventHandler<DataLoadedEventArgs<List<SelectionClaimCompare>>> DataLoaded;

        protected override void CallBack(List<SelectionClaimCompare> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<SelectionClaimCompare>>(result));
        }
    }
}
