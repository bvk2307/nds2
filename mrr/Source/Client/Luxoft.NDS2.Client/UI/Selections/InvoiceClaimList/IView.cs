﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.DatePicker;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls;
using System;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    public interface IView
    {
        IDatePickerView DatePicker { get; }

        ILookupViewer StatusLookup { get; }

        bool AllowSelectionStatsReport { set; }

        bool AllowInvoiceClaimListReport { set; }
        
        bool IsStatusSelectionEnabled { set; }

        event EventHandler ToSendCheckedChanged;

        bool ToSendChecked { get; }
        
        InvoiceClaimListGridView Grid { get; }

        IGridPagerView GridPager { get; }
        
        bool SendEnabled { set; }

        event EventHandler Load;
        
        void InitRibbon();

        bool GetFileName(ref string fileName);

        bool ActionConfirmed(string question, string caption);

        event ActionRequiredEvent BeforeClosing;

        event EventHandler UpdateRequested;

        event EventHandler SendRequested;

        event EventHandler ReportStatsRequested;

        event EventHandler ReportListRequested;

        event EventHandler ReportTaxMonitoringRequested;
        
        bool ConfirmSending(int count);

        void ShowLoadingIndicator(string text);

        void HideLoadingIndicator();
    }
}
