﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.DatePicker;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Controls;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    public partial class View : BaseView, IView
    {

        #region .ctor

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();

            InitListComponents();

            _checkToSend.CheckedChanged += (sender, args) => RaiseToSendCheckedChanged();

            _lookupBase.WithEditor(new UI.Controls.LookupEditor.LookupShort.Editor());
        }

        public ILookupViewer StatusLookup
        {
            get
            {
                return _lookupBase;
            }
        }

        public IDatePickerView DatePicker
        {
            get
            {
                return _datePickerView;
            }
        }

        [CreateNew]
        public PresenterCreator ListPresenterCreator
        {
            set
            {
                var creator = value;
                WorkItem = value.WorkItem;
                creator.Create(this, new Model());
            }
        }

        #endregion

        #region Ribbon

        private const string CmdPrefix = "iwpInvoiceClaims";

        private UcRibbonButtonToolContext _btnSend;

        private UcRibbonButtonToolContext _btnReportStats;

        private UcRibbonButtonToolContext _btnReportList;

        public bool AllowSelectionStatsReport
        {
            set
            {
                _btnReportStats.Enabled = value;
                RefreshEkp();
            }
        }

        public bool AllowInvoiceClaimListReport
        {
            set
            {
                _btnReportList.Enabled = value;
                RefreshEkp();
            }
        }

        public void InitRibbon()
        {
            var resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2InvoiceClaims")
            {
                Text = _presentationContext.WindowTitle,
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var group = tabNavigator.AddGroup("NDS2InvoiceClaimManage");
            group.Text = "Функции";
            group.Visible = true;

            var btnUpdate = new UcRibbonButtonToolContext(_presentationContext, "btnUpdate", CmdPrefix + "UpdateView")
            {
                Text = "Обновить",
                ToolTipText = "Обновить список деклараций",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _btnSend = new UcRibbonButtonToolContext(_presentationContext, "btnSend", CmdPrefix + "Send")
            {
                Text = "Отправить АТ",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "claimSend",
                SmallImageName = "claimSend",
                Enabled = false
            };

            _btnReportStats = new UcRibbonButtonToolContext(_presentationContext, "btnReportStats", CmdPrefix + "ReportStats")
            {
                Text = "Статистика по выборкам",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "excel",
                SmallImageName = "excel",
                Enabled = false
            };

            _btnReportList = new UcRibbonButtonToolContext(_presentationContext, "btnReportList", CmdPrefix + "ReportList")
            {
                Text = "Перечень АТ",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "excel",
                SmallImageName = "excel",
                Enabled = false
            };

            group.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnSend.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnReportStats.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_btnReportList.ItemName, UcRibbonToolSize.Large)
                });

            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnUpdate,
                _btnSend,
                _btnReportStats,
                _btnReportList,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler(CmdPrefix + "UpdateView")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            UpdateRequested.Raise(this, new EventArgs());
        }

        [CommandHandler(CmdPrefix + "Send")]
        public virtual void SendBtnClick(object sender, EventArgs e)
        {
            SendRequested.Raise(this, new EventArgs());
        }

        [CommandHandler(CmdPrefix + "ReportStats")]
        public virtual void ReportStatsBtnClick(object sender, EventArgs e)
        {
            ReportStatsRequested.Raise(this, new EventArgs());
        }

        [CommandHandler(CmdPrefix + "ReportList")]
        public virtual void ReportListBtnClick(object sender, EventArgs e)
        {
            ReportListRequested.Raise(this, new EventArgs());
        }

        #endregion

        #region Status combo

        public bool IsStatusSelectionEnabled
        {
            set
            {
                _lookupBase.Enabled = value;
            }
        }

        #endregion

        #region ToSend checkbox

        public bool ToSendChecked
        {
            get
            {
                return _checkToSend.Checked;
            }
        }

        public InvoiceClaimListGridView Grid
        {
            get { return _invoiceClaimListListGrid; }
        }

        public IGridPagerView GridPager
        {
            get
            {
                return _invoiceClaimListPager;
            }
        }

        public bool SendEnabled
        {
            set
            {
                _btnSend.Enabled = value;
                RefreshEkp();
            }
        }

        public event EventHandler ToSendCheckedChanged;

        private void RaiseToSendCheckedChanged()
        {
            var handler = ToSendCheckedChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        #endregion

        public override void OnBeforeClosing(WindowCloseContextBase closeContext, CancelEventArgs eventArgs)
        {
            base.OnBeforeClosing(closeContext, eventArgs);

            if (BeforeClosing != null)
                eventArgs.Cancel |= BeforeClosing() != CommandResult.Success;
        }

        public event ActionRequiredEvent BeforeClosing;

        public event EventHandler UpdateRequested;

        public event EventHandler SendRequested;

        public event EventHandler ReportStatsRequested;

        public event EventHandler ReportListRequested;

        public event EventHandler ReportTaxMonitoringRequested;

        public bool ConfirmSending(int count)
        {
            return InvoiceClaimList.Controls.ConfirmSending.Confirm(count, ParentForm);
        }

        public bool GetFileName(ref string fileName)
        {
            return DialogHelper.GetFileToSave(SelectFileType.XLSX, ref fileName);
        }

        public bool ActionConfirmed(string question, string caption)
        {
            return ShowQuestion(caption, question) == DialogResult.Yes;
        }

        #region grid

        private readonly InvoiceClaimListGridView _invoiceClaimListListGrid = new InvoiceClaimListGridView();

        private readonly GridPagerView _invoiceClaimListPager = new GridPagerView();

        private readonly LoadingIndicator _indicator = new LoadingIndicator();

        private const int ChapterColumnChooserWidth = 300;

        private const int ChapterColumnChooserHeigth = 350;

        private void InitListComponents()
        {
            _invoiceClaimListContainer
                .WithGrid(_invoiceClaimListListGrid)
                .WithPager(_invoiceClaimListPager)
                .WithLoadingIndicator(_indicator)
                .WithGridResetter(new GridColumnFilterResetter(_invoiceClaimListListGrid))
                .WithGridResetter(new GridColumnSortResetter(_invoiceClaimListListGrid))
                .WithGridResetter(new GridColumnsVisibilityResetter(_invoiceClaimListListGrid))
                .WithColumnChooser(new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        public void ShowLoadingIndicator(string text)
        {
            _indicator.Show(text);
        }


        public void HideLoadingIndicator()
        {
            _indicator.Hide();
        }
        #endregion
    }
}
