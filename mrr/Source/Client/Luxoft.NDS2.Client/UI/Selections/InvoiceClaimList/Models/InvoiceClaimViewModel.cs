﻿using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums;
using System;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models
{
    public class InvoiceClaimListViewModel : ListViewModel<InvoiceClaimViewModel, InvoiceClaim>
    {
        protected override InvoiceClaimViewModel ConvertToModel(InvoiceClaim dataItem)
        {
            return new InvoiceClaimViewModel(dataItem);
        }
    }

    public class InvoiceClaimViewModel
    {
        #region .ctors

        private readonly InvoiceClaim _data;

        public InvoiceClaimViewModel()
        {

        }

        public InvoiceClaimViewModel(InvoiceClaim data)
        {
            _data = data;
        }

        #endregion

        public bool IsExcluded
        {
            get
            {
                return _data.Excluded == 1;
            }
        }

        public ExcludeClaim Excluded
        {
            get { return _data.Excluded == null ? ExcludeClaim.Include : ExcludeClaim.Exclude; }
        }

        public TaxMonitoring TaxMonitoring
        {
            get { return _data.TaxMonitoring == null ? TaxMonitoring.Exclude : TaxMonitoring.Include; }
        }

        public string LastUser
        {
            get { return _data.LastUser; }
        }

        public long DocId
        {
            get { return _data.DocId; }
        }

        public string RegionCode
        {
            get { return _data.RegionCode; }
        }

        public string RegionName
        {
            get { return _data.RegionName; }
        }

        public string SonoCode
        {
            get { return _data.SonoCode; }
        }

        public string SonoName
        {
            get { return _data.SonoName; }
        }

        public string Inn
        {
            get { return _data.Inn; }
        }

        public string Kpp
        {
            get { return _data.Kpp; }
        }

        public string Name { get { return _data.Name; } }

        public int? BuyerDiscrepancyCount
        {
            get { return _data.BuyerDiscrepancyCount; }
        }

        public decimal? BuyerDiscrepancyAmount
        {
            get { return _data.BuyerDiscrepancyAmount; }
        }

        public int? SellerDiscrepancyCount
        {
            get { return _data.SellerDiscrepancyCount; }
        }

        public decimal? SellerDiscrepancyAmount
        {
            get { return _data.SellerDiscrepancyAmount; }
        }

        public string InnReorg { get { return _data.InnReorg; } }

        public string TaxPeriod { get { return _data.TaxPeriod; } }

        public string TaxPeriodCode { get { return _data.TaxPeriodCode; } }

        public string CorrectionNumber { get { return _data.CorrectionNumber; } }

        public DateTime? CorrectionDate { get { return _data.CorrectionDate; } }

        public string DeclarationSign { get { return _data.DeclarationSign; } }

        public DeclarationType DeclSignCode { get { return (DeclarationType)_data.DeclSignCode; } }

        public decimal? NdsAmount { get { return _data.NdsAmount; } }

        public DateTime? FaultFormingDate { get { return _data.FaultFormingDate; } }

        public DateTime? ReadyToSendDate { get { return _data.ReadyToSendDate; } }

        public DateTime? SentToSeodDate { get { return _data.SentToSeodDate; } }

        public DateTime? FaultSendToSeodDate { get { return _data.FaultSendToSeodDate; } }

        public DateTime? ReceivedBySeodDate { get { return _data.ReceivedBySeodDate; } }

        public long? SeodRegNumber { get { return _data.SeodRegNumber; } }

        public DateTime? SeodDocDate { get { return _data.SeodDocDate; } }

        public DateTime? SendToTaxpayerDate { get { return _data.SendToTaxpayerDate; } }

        public string DeliveryType { get { return _data.DeliveryType; } }

        public DateTime? TaxpayerReceiveDate { get { return _data.TaxpayerReceiveDate; } }

        public DateTime? CloseDate { get { return _data.CloseDate; } }

        public string CloseType { get { return _data.CloseType; } }

        public CloseReason CloseTypeId { get { return _data.CloseTypeId == null ? CloseReason.Empty : (CloseReason)_data.CloseTypeId; } }
        
        public int StatusCode {get { return _data.StatusCode; }}

    }
}
