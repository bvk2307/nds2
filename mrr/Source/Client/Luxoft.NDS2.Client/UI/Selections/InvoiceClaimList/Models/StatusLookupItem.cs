﻿using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models
{
    public class StatusLookupItem : ISelectableLookupItem
    {
        public ClaimStatusModel Data { get; private set; }

        public string Title
        {
            get
            {
                return Data.Description;
            }
        }

        public string Description
        {
            get
            {
                return Data.Description;
            }
        }

        private bool _isChecked;

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                if (value == _isChecked)
                    return;

                _isChecked = value;
                CheckedChanged.Raise(this, new EventArgs());
            }
        }

        public event EventHandler CheckedChanged;


        public StatusLookupItem(ClaimStatusModel data)
        {
            Data = data;
        }
    }
}
