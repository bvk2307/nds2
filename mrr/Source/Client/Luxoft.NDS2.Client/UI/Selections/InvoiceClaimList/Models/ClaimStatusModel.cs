﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models
{
    public class ClaimStatusModel
    {
        public ClaimStatusModel(int id, string description)
        {
            Id = id;
            Description = description;
        }
        
        public ClaimStatusModel(SimpleStatusDto data)
        {
            Id = data.Id;
            Description = data.Description;
        }

        public int Id { get; private set; }

        public string Description { get; private set; }
    }
}
