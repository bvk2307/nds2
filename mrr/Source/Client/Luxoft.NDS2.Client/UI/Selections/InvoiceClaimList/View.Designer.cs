﻿using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _labelStatus;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._datePickerView = new Luxoft.NDS2.Client.UI.Controls.DatePicker.DatePickerView();
            this.gridContainer1 = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._invoiceClaimListContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._checkToSend = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._lookupBase = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            _labelStatus = new Infragistics.Win.Misc.UltraLabel();
            this.SuspendLayout();
            // 
            // _labelStatus
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            _labelStatus.Appearance = appearance1;
            _labelStatus.Location = new System.Drawing.Point(410, 9);
            _labelStatus.Name = "_labelStatus";
            _labelStatus.Size = new System.Drawing.Size(45, 16);
            _labelStatus.TabIndex = 5;
            _labelStatus.Text = "Статус:";
            // 
            // _datePickerView
            // 
            this._datePickerView.BackColor = System.Drawing.Color.Transparent;
            this._datePickerView.Caption = "Дата формирования АТ:";
            this._datePickerView.Location = new System.Drawing.Point(3, 1);
            this._datePickerView.Name = "_datePickerView";
            this._datePickerView.Size = new System.Drawing.Size(267, 26);
            this._datePickerView.TabIndex = 0;
            // 
            // gridContainer1
            // 
            this.gridContainer1.AutoScroll = true;
            this.gridContainer1.AutoSize = true;
            this.gridContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridContainer1.MinimumSize = new System.Drawing.Size(0, 160);
            this.gridContainer1.Name = "gridContainer1";
            this.gridContainer1.Size = new System.Drawing.Size(0, 160);
            this.gridContainer1.TabIndex = 1;
            // 
            // _invoiceClaimListContainer
            // 
            this._invoiceClaimListContainer.AutoScroll = true;
            this._invoiceClaimListContainer.AutoSize = true;
            this._invoiceClaimListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._invoiceClaimListContainer.Location = new System.Drawing.Point(0, 0);
            this._invoiceClaimListContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._invoiceClaimListContainer.Name = "_invoiceClaimListContainer";
            this._invoiceClaimListContainer.Size = new System.Drawing.Size(917, 589);
            this._invoiceClaimListContainer.TabIndex = 2;
            // 
            // _checkToSend
            // 
            this._checkToSend.BackColor = System.Drawing.Color.Transparent;
            this._checkToSend.BackColorInternal = System.Drawing.Color.Transparent;
            this._checkToSend.Location = new System.Drawing.Point(300, 7);
            this._checkToSend.Name = "_checkToSend";
            this._checkToSend.Size = new System.Drawing.Size(80, 20);
            this._checkToSend.TabIndex = 3;
            this._checkToSend.Text = "К отправке";
            // 
            // _lookupBase
            // 
            this._lookupBase.Location = new System.Drawing.Point(461, 3);
            this._lookupBase.Name = "_lookupBase";
            this._lookupBase.Size = new System.Drawing.Size(265, 25);
            this._lookupBase.TabIndex = 6;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._lookupBase);
            this.Controls.Add(_labelStatus);
            this.Controls.Add(this._checkToSend);
            this.Controls.Add(this.gridContainer1);
            this.Controls.Add(this._datePickerView);
            this.Controls.Add(this._invoiceClaimListContainer);
            this.Name = "View";
            this.Size = new System.Drawing.Size(917, 589);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.Controls.DatePicker.DatePickerView _datePickerView;
        private UI.Controls.Grid.Controls.GridContainer gridContainer1;
        private UI.Controls.Grid.Controls.GridContainer _invoiceClaimListContainer;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _checkToSend;
        private UI.Controls.LookupEditor.LookupBase _lookupBase;

    }
}
