﻿namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    partial class ConfirmSending
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._message = new Infragistics.Win.Misc.UltraLabel();
            this.btnConfirm = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // _message
            // 
            this._message.Location = new System.Drawing.Point(12, 12);
            this._message.Name = "_message";
            this._message.Size = new System.Drawing.Size(400, 30);
            this._message.TabIndex = 1;
            this._message.Text = "«<N> автотребований по СФ готово к передачи в  СЭОД.";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(100, 86);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(100, 24);
            this.btnConfirm.TabIndex = 2;
            this.btnConfirm.Text = "Передать";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(206, 86);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 24);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отменить";
            // 
            // ConfirmSending
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 122);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this._message);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ConfirmSending";
            this.Text = "Внимание!";
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel _message;
        private Infragistics.Win.Misc.UltraButton btnConfirm;
        private Infragistics.Win.Misc.UltraButton btnCancel;
    }
}