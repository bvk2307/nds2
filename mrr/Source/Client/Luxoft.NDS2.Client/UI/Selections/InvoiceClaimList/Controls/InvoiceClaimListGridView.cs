﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public partial class InvoiceClaimListGridView : System.Windows.Forms.UserControl, IExtendedGridView
    {
        private RepopulableColumnFilterView _correctionNumFilter;
        private RepopulableColumnFilterView _taxPeriodFilter;

        public InvoiceClaimListGridView()
        {
            InitializeComponent();
            GridEvents = new UltraGridEventsHandler(_grid);
            InitColumnsCollection();
            BindEvents();

            _grid.SetBaseConfig();
            _grid.SetRowContextMenu(_contextMenu);
            _grid.DoubleClickRow += (sender, args) => Opening.Raise(this, new EventArgs());
        }

        public object SelectedItem
        {
            get
            {
                return _grid.ActiveRow != null
                    ? _grid.ActiveRow.ListObject
                    : null;
            }
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }

        public void InitColumnsCollection()
        {
            var excludeKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.Excluded);
            var monitoringKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.TaxMonitoring);
            var deliveryKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.DeliveryType);
            var closeReasonKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.CloseTypeId);
            var declSignKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.DeclSignCode);
            var correctionNumKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.CorrectionNumber);
            var taxPeriodKey = TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.TaxPeriod);
            var band = _grid.DisplayLayout.Bands[0];

            var excludeColumnFilter =
                new ExcludeColumnFilterView(band.Columns[excludeKey]);
            var taxMonitoringColumnFilter =
                new TaxMonitoringColumnFilterView(band.Columns[monitoringKey]);
            var deliveryColumnFilter =
                new DeliveryTypeColumnFilterView(band.Columns[deliveryKey]);
            var declSignColumnFilter =
                new DiscrepancyDeclSignColumnFilterView(band.Columns[declSignKey]);
            var closeReasonFilter =
                new CloseReasonColumnFilterView(band.Columns[closeReasonKey]);
            _correctionNumFilter =
                new RepopulableColumnFilterView(band.Columns[correctionNumKey]);
            _taxPeriodFilter =
                new RepopulableColumnFilterView(band.Columns[taxPeriodKey]);

            Columns =
                new GridColumnsCollection(
                    null,
                    band.Groups.AsEnumerable(),
                    band.Columns.AsEnumerable(
                        new[]
                        {
                            TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.IsExcluded),
                            TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.StatusCode),
                            TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.TaxPeriodCode),
                            TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.DeclarationSign),
                            TypeHelper<InvoiceClaimViewModel>.GetMemberName(x => x.CloseType)

                        }),
                    new GridColumnCreator()
                        .WithFilterViewFor(excludeKey, column => excludeColumnFilter)
                        .WithFilterViewFor(monitoringKey, column => taxMonitoringColumnFilter)
                        .WithFilterViewFor(deliveryKey, column => deliveryColumnFilter)
                        .WithFilterViewFor(declSignKey, column => declSignColumnFilter)
                        .WithFilterViewFor(closeReasonKey, column => closeReasonFilter)
                        .WithFilterViewFor(correctionNumKey, column => _correctionNumFilter)
                        .WithFilterViewFor(taxPeriodKey, column => _taxPeriodFilter)
                    );

            ExcludeColumnView.Apply(_grid.DisplayLayout.Bands[0].Columns[excludeKey]);
            TaxMonitoringColumnView.Apply(_grid.DisplayLayout.Bands[0].Columns[monitoringKey]);
            DiscrepancyDeclSignColumnView.Apply(_grid.DisplayLayout.Bands[0].Columns[declSignKey]);
            CloseReasonColumnView.Apply(_grid.DisplayLayout.Bands[0].Columns[closeReasonKey]);
        }

        public object[] Rows
        {
            get
            {
                return _grid.Rows.Select(x => x.ListObject).ToArray();
            }
        }

        private void GridViewLoad(object sender, EventArgs e)
        {

        }

        public void PopulateCorrectionFilter(IEnumerable<string> values)
        {
            var map = values.Distinct()
                .ToDictionary(
                    x => string.IsNullOrEmpty(x) ? null : x,
                    v => string.IsNullOrEmpty(v) ? "Пусто" : v);
            _correctionNumFilter.FilterValueMap.Clear();
            foreach (var pair in map)
            {
                _correctionNumFilter.FilterValueMap.Add(pair.Key, pair.Value);
            }
        }

        public void PopulateTaxPeriodFilter(IEnumerable<string> values)
        {
            var map = values.Distinct()
                .Where(x => !string.IsNullOrEmpty(x))
                .ToDictionary(x => x, v => v);
            _taxPeriodFilter.FilterValueMap.Clear();
            foreach (var pair in map)
            {
                _taxPeriodFilter.FilterValueMap.Add(pair.Key, pair.Value);
            }
        }

        #region Context menu

        public bool IncludeVisible
        {
            set
            {
                cmInclude.Visible = value;
                cmExclude.Visible = !value;
            }
        }

        public bool IncludeEnabled
        {
            set
            {
                cmInclude.Enabled = value;
                cmExclude.Enabled = value;
            }
        }

        public event EventHandler SetupMenuRequested;

        public event EventHandler Including;

        public event EventHandler Excluding;

        public event EventHandler Opening;

        public event EventHandler Exporting;

        private void BindEvents()
        {
            cmInclude.Click += (sender, args) => Including.Raise(this, args);
            cmExclude.Click += (sender, args) => Excluding.Raise(this, args);
            cmOpenClaimCard.Click += (sender, args) => Opening.Raise(this, args);
            cmExport.Click += (sender, args) => Exporting.Raise(this, args);

            _contextMenu
                .WithCopyCell(() => _grid.ActiveCell.Text)
                .WithCopyTable(_grid);
        }

        private void OnContextMenuOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SetupMenuRequested.Raise(this, e);
        }

        #endregion
    }

    public static class EventRaiseHelper
    {
        public static void Raise(this EventHandler handler, object sender, EventArgs e)
        {
            var h = handler;
            if (h != null)
                h(sender, e);
        }
    }
}