﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public class TaxMonitoringColumnFilterView : GridColumnFilterView
    {
        public TaxMonitoringColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            TaxMonitoringColumnView.TaxMonitoringDictionary[TaxMonitoring.Exclude] = "(Пусто)";
            foreach (var dictionaryItem in TaxMonitoringColumnView.TaxMonitoringDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }
    public static class TaxMonitoringColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<TaxMonitoring, string> TaxMonitoringDictionary =
            new Dictionary<TaxMonitoring, string>()
           {
                {TaxMonitoring.Include, "НМ"},
                {TaxMonitoring.Exclude, " "}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();

            TaxMonitoringDictionary[TaxMonitoring.Exclude] = " ";

            foreach (var dictionaryItem in TaxMonitoringDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
