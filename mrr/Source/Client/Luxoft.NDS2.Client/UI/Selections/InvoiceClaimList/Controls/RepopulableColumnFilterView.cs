﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    internal class RepopulableColumnFilterView : GridColumnFilterView
    {
        public RepopulableColumnFilterView(UltraGridColumn column)
            : base(column)
        {
            FilterValueMap = new Dictionary<object, string>();
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            foreach (var pair in FilterValueMap)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(pair.Key, pair.Value));
            }
        }

        public Dictionary<object, string>  FilterValueMap { get; private set; }

        public RepopulableColumnFilterView WithValue(object value, string title)
        {
            FilterValueMap.Add(value, title);
            return this;
        }
    }
}
