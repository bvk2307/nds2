﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public class ExcludeColumnFilterView : GridColumnFilterView
    {
        public ExcludeColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            ExcludeColumnView.ExcludeDictionary[ExcludeClaim.Include] = "(Пусто)";
            foreach (var dictionaryItem in ExcludeColumnView.ExcludeDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }
    public static class ExcludeColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<ExcludeClaim, string> ExcludeDictionary =
            new Dictionary<ExcludeClaim, string>()
           {
                {ExcludeClaim.Exclude, "Да"},
                {ExcludeClaim.Include, " "}
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();

            ExcludeDictionary[ExcludeClaim.Include] = " ";

            foreach (var dictionaryItem in ExcludeDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
