﻿using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public partial class ConfirmSending : Form
    {
        public static bool Confirm(int quantity, Form owner)
        {
            var w = new ConfirmSending
            {
                Owner = owner,
                StartPosition = FormStartPosition.CenterParent,
                _message = {Text = string.Format(ResourceManagerNDS2.InvoiceClaimList.ConfirmSending, quantity)}
            };

            return w.ShowDialog() == DialogResult.OK;
        }
        
        protected ConfirmSending()
        {
            InitializeComponent();
            DialogResult = DialogResult.None;

            btnConfirm.Click += (sender, args) =>
            {
                DialogResult = DialogResult.OK;
                Close();
            };

            btnCancel.Click += (sender, args) =>
            {
                DialogResult = DialogResult.No;
                Close();
            };
        }
    }
}
