﻿namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    partial class InvoiceClaimListGridView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsExcluded");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Excluded");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxMonitoring");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LastUser");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DocId");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RegionName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SonoCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SonoName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Inn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Kpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDiscrepancyCount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerDiscrepancyAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDiscrepancyCount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerDiscrepancyAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InnReorg");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxPeriod");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxPeriodCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DeclarationSign");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DeclSignCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NdsAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FaultFormingDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReadyToSendDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SentToSeodDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FaultSendToSeodDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceivedBySeodDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SeodRegNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SeodDocDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SendToTaxpayerDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DeliveryType");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TaxpayerReceiveDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CloseDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CloseType");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CloseTypeId");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("StatusCode");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Markers", 30404047);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Taxpayer", 30404266);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("BuyerSide", 30404485);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("SellerSide", 30404735);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Correction", 30404954);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("SeodStatuses", 30405188);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Closed", 30405407);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("Sono", 30405626);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup9 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("RegionGroup", 30405860);
            Infragistics.Win.UltraWinGrid.RowLayout rowLayout1 = new Infragistics.Win.UltraWinGrid.RowLayout("try1");
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo1 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "IsExcluded", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo2 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Excluded", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo3 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxMonitoring", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo4 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "LastUser", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo5 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DocId", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo6 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "RegionCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo7 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "RegionName", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo8 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SonoCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo9 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SonoName", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo10 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Inn", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo11 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Kpp", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo12 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Name", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo13 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDiscrepancyCount", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo14 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerDiscrepancyAmount", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo15 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDiscrepancyCount", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo16 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SellerDiscrepancyAmount", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo17 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "InnReorg", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo18 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxPeriod", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo19 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxPeriodCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo20 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CorrectionNumber", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo21 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CorrectionDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo22 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DeclarationSign", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo23 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DeclSignCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo24 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NdsAmount", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo25 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "FaultFormingDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo26 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ReadyToSendDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo27 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SentToSeodDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo28 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "FaultSendToSeodDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo29 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ReceivedBySeodDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo30 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SeodRegNumber", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo31 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SeodDocDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo32 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "SendToTaxpayerDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo33 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "DeliveryType", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo34 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "TaxpayerReceiveDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo35 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CloseDate", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo36 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CloseType", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo37 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "CloseTypeId", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo38 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "StatusCode", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo39 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Markers", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo40 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Taxpayer", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo41 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "BuyerSide", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo42 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "SellerSide", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo43 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Correction", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo44 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "SeodStatuses", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo45 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Closed", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo46 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "Sono", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo47 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "RegionGroup", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmExclude = new System.Windows.Forms.ToolStripMenuItem();
            this.cmInclude = new System.Windows.Forms.ToolStripMenuItem();
            this.cmOpenClaimCard = new System.Windows.Forms.ToolStripMenuItem();
            this.cmExport = new System.Windows.Forms.ToolStripMenuItem();
            this._filterProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            this._contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.ContextMenuStrip = this._contextMenu;
            this._grid.DataSource = this._bindingSource;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance13;
            ultraGridColumn39.Header.VisiblePosition = 0;
            ultraGridColumn39.Hidden = true;
            ultraGridColumn40.Header.Caption = "Искл.";
            ultraGridColumn40.Header.ToolTipText = "Признак исключения из отправки АТ";
            ultraGridColumn40.Header.VisiblePosition = 1;
            ultraGridColumn40.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn40.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupKey = "Markers";
            ultraGridColumn40.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn41.Header.Caption = "НМ";
            ultraGridColumn41.Header.ToolTipText = "Признак нахождения НП на налоговом мониторинге";
            ultraGridColumn41.Header.VisiblePosition = 2;
            ultraGridColumn41.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn41.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn41.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn41.RowLayoutColumnInfo.ParentGroupKey = "Markers";
            ultraGridColumn41.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn41.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn42.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn42.Header.Caption = "ФИО";
            ultraGridColumn42.Header.ToolTipText = "ФИО пользователя, изменившего признак исключения";
            ultraGridColumn42.Header.VisiblePosition = 3;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupKey = "Markers";
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn43.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn43.Header.Caption = "Учетный № АТ";
            ultraGridColumn43.Header.ToolTipText = "Учетный № АТ в АСК";
            ultraGridColumn43.Header.VisiblePosition = 4;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn43.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn44.Header.Caption = "Код";
            ultraGridColumn44.Header.ToolTipText = "Код региона получателя";
            ultraGridColumn44.Header.VisiblePosition = 5;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupIndex = 8;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupKey = "RegionGroup";
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn45.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn45.Header.Caption = "Наименование";
            ultraGridColumn45.Header.ToolTipText = "Наименование региона получателя";
            ultraGridColumn45.Header.VisiblePosition = 6;
            ultraGridColumn45.Hidden = true;
            ultraGridColumn45.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupIndex = 8;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupKey = "RegionGroup";
            ultraGridColumn45.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn45.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn46.Header.Caption = "Код";
            ultraGridColumn46.Header.ToolTipText = "Код инспекции получателя";
            ultraGridColumn46.Header.VisiblePosition = 7;
            ultraGridColumn46.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn46.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn46.RowLayoutColumnInfo.ParentGroupIndex = 7;
            ultraGridColumn46.RowLayoutColumnInfo.ParentGroupKey = "Sono";
            ultraGridColumn46.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn47.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn47.Header.Caption = "Наименование";
            ultraGridColumn47.Header.ToolTipText = "Наименование инспекции получателя";
            ultraGridColumn47.Header.VisiblePosition = 8;
            ultraGridColumn47.Hidden = true;
            ultraGridColumn47.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn47.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn47.RowLayoutColumnInfo.ParentGroupIndex = 7;
            ultraGridColumn47.RowLayoutColumnInfo.ParentGroupKey = "Sono";
            ultraGridColumn47.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn47.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn47.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn48.Header.Caption = "ИНН";
            ultraGridColumn48.Header.ToolTipText = "ИНН получателя";
            ultraGridColumn48.Header.VisiblePosition = 9;
            ultraGridColumn48.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn48.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupKey = "Taxpayer";
            ultraGridColumn48.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn48.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn49.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn49.Header.Caption = "КПП";
            ultraGridColumn49.Header.ToolTipText = "КПП получателя";
            ultraGridColumn49.Header.VisiblePosition = 10;
            ultraGridColumn49.Hidden = true;
            ultraGridColumn49.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupKey = "Taxpayer";
            ultraGridColumn49.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn49.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn50.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn50.Header.Caption = "Наименование";
            ultraGridColumn50.Header.ToolTipText = "Наименование получателя";
            ultraGridColumn50.Header.VisiblePosition = 11;
            ultraGridColumn50.Hidden = true;
            ultraGridColumn50.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn50.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupKey = "Taxpayer";
            ultraGridColumn50.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn50.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn51.Header.Caption = "Кол-во расх.";
            ultraGridColumn51.Header.ToolTipText = "Кол-во расхождений в АТ по стороне покупателя";
            ultraGridColumn51.Header.VisiblePosition = 12;
            ultraGridColumn51.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn51.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupKey = "BuyerSide";
            ultraGridColumn51.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn52.Format = "N2";
            ultraGridColumn52.Header.Caption = "Сумма расх.";
            ultraGridColumn52.Header.ToolTipText = "Сумма расхождений в АТ по стороне покупателя";
            ultraGridColumn52.Header.VisiblePosition = 13;
            ultraGridColumn52.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn52.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupKey = "BuyerSide";
            ultraGridColumn52.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn52.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn53.Header.Caption = "Кол-во расх.";
            ultraGridColumn53.Header.ToolTipText = "Кол-во расхождений в АТ по стороне продавца";
            ultraGridColumn53.Header.VisiblePosition = 14;
            ultraGridColumn53.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn53.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupKey = "SellerSide";
            ultraGridColumn53.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn54.Format = "N2";
            ultraGridColumn54.Header.Caption = "Сумма расх.";
            ultraGridColumn54.Header.ToolTipText = "Сумма расхождений в АТ по стороне продавца";
            ultraGridColumn54.Header.VisiblePosition = 15;
            ultraGridColumn54.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn54.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn54.RowLayoutColumnInfo.ParentGroupKey = "SellerSide";
            ultraGridColumn54.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn55.Header.Caption = "Реорганизованный НП";
            ultraGridColumn55.Header.ToolTipText = "ИНН реорганизованного НП, по которому сформировано АТ";
            ultraGridColumn55.Header.VisiblePosition = 16;
            ultraGridColumn55.Hidden = true;
            ultraGridColumn55.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn55.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn55.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn55.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn55.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn56.Header.Caption = "Отчетный период";
            ultraGridColumn56.Header.ToolTipText = "Отчетный период за который подана НД";
            ultraGridColumn56.Header.VisiblePosition = 17;
            ultraGridColumn56.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn56.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn56.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn56.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn57.Header.VisiblePosition = 18;
            ultraGridColumn57.Hidden = true;
            ultraGridColumn57.RowLayoutColumnInfo.OriginX = 42;
            ultraGridColumn57.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn57.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn58.Header.Caption = "№";
            ultraGridColumn58.Header.ToolTipText = "Номер корректировки";
            ultraGridColumn58.Header.VisiblePosition = 19;
            ultraGridColumn58.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn58.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn58.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn59.Header.Caption = "Дата подачи";
            ultraGridColumn59.Header.ToolTipText = "Дата подачи корректировки";
            ultraGridColumn59.Header.VisiblePosition = 20;
            ultraGridColumn59.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn59.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn59.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn59.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn59.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn59.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn59.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn60.Header.Caption = "Признак НД";
            ultraGridColumn60.Header.ToolTipText = "Признак НД";
            ultraGridColumn60.Header.VisiblePosition = 21;
            ultraGridColumn60.Hidden = true;
            ultraGridColumn60.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn60.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn60.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn60.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn60.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn60.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn60.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn60.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn61.Header.Caption = "Признак НД";
            ultraGridColumn61.Header.ToolTipText = "Признак НД";
            ultraGridColumn61.Header.VisiblePosition = 22;
            ultraGridColumn61.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn61.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn61.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn62.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn62.Format = "N2";
            ultraGridColumn62.Header.Caption = "Сумма НДС";
            ultraGridColumn62.Header.ToolTipText = "Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет";
            ultraGridColumn62.Header.VisiblePosition = 23;
            ultraGridColumn62.Hidden = true;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn62.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn62.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn62.RowLayoutColumnInfo.ParentGroupKey = "Correction";
            ultraGridColumn62.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn62.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn62.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn63.Header.Caption = "Ошибка формирования";
            ultraGridColumn63.Header.ToolTipText = "Дата присвоения статуса «Ошибка формиирования»";
            ultraGridColumn63.Header.VisiblePosition = 24;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 42;
            ultraGridColumn63.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn63.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn63.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn63.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn64.Header.Caption = "Готово к отправке";
            ultraGridColumn64.Header.ToolTipText = "Дата присвоения статуса «Готово к отправке»";
            ultraGridColumn64.Header.VisiblePosition = 25;
            ultraGridColumn64.RowLayoutColumnInfo.OriginX = 44;
            ultraGridColumn64.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn64.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn64.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn64.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn65.Header.Caption = "Отправлено в СЭОД";
            ultraGridColumn65.Header.ToolTipText = "Дата присвоения статуса «Отправлено в СЭОД»";
            ultraGridColumn65.Header.VisiblePosition = 26;
            ultraGridColumn65.RowLayoutColumnInfo.OriginX = 46;
            ultraGridColumn65.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn65.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn65.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn65.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn66.Header.Caption = "Ошибка передачи в СЭОД";
            ultraGridColumn66.Header.ToolTipText = "Дата присвоения статуса «Ошибка передачи в СЭОД»";
            ultraGridColumn66.Header.VisiblePosition = 27;
            ultraGridColumn66.RowLayoutColumnInfo.OriginX = 48;
            ultraGridColumn66.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn66.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn66.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn66.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn67.Header.Caption = "Получено СЭОД";
            ultraGridColumn67.Header.ToolTipText = "Дата получения данных в СЭОД";
            ultraGridColumn67.Header.VisiblePosition = 28;
            ultraGridColumn67.RowLayoutColumnInfo.OriginX = 50;
            ultraGridColumn67.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn67.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn67.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn67.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn68.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn68.Header.Caption = "№";
            ultraGridColumn68.Header.ToolTipText = "№ документа в СЭОД";
            ultraGridColumn68.Header.VisiblePosition = 29;
            ultraGridColumn68.Hidden = true;
            ultraGridColumn68.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn68.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn68.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn68.RowLayoutColumnInfo.ParentGroupKey = "SeodStatuses";
            ultraGridColumn68.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn68.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn68.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn68.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn69.Header.Caption = "Дата";
            ultraGridColumn69.Header.ToolTipText = "Дата документа в СЭОД";
            ultraGridColumn69.Header.VisiblePosition = 30;
            ultraGridColumn69.Hidden = true;
            ultraGridColumn69.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn69.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn69.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn69.RowLayoutColumnInfo.ParentGroupKey = "SeodStatuses";
            ultraGridColumn69.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn69.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn69.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn70.Header.Caption = "Отправлено";
            ultraGridColumn70.Header.ToolTipText = "Дата отправки документа НП";
            ultraGridColumn70.Header.VisiblePosition = 31;
            ultraGridColumn70.Hidden = true;
            ultraGridColumn70.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn70.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn70.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn70.RowLayoutColumnInfo.ParentGroupKey = "SeodStatuses";
            ultraGridColumn70.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn70.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn70.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn71.Header.Caption = "Способ";
            ultraGridColumn71.Header.ToolTipText = "Способ отправки документа НП";
            ultraGridColumn71.Header.VisiblePosition = 32;
            ultraGridColumn71.Hidden = true;
            ultraGridColumn71.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn71.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn71.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn71.RowLayoutColumnInfo.ParentGroupKey = "SeodStatuses";
            ultraGridColumn71.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn71.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn71.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn71.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn72.Header.Caption = "Вручено";
            ultraGridColumn72.Header.ToolTipText = "Дата вручения документа НП";
            ultraGridColumn72.Header.VisiblePosition = 33;
            ultraGridColumn72.Hidden = true;
            ultraGridColumn72.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn72.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn72.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn72.RowLayoutColumnInfo.ParentGroupKey = "SeodStatuses";
            ultraGridColumn72.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn72.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn72.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn73.Header.Caption = "Дата";
            ultraGridColumn73.Header.ToolTipText = "Дата закрытия АТ";
            ultraGridColumn73.Header.VisiblePosition = 34;
            ultraGridColumn73.Hidden = true;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn73.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn73.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn73.RowLayoutColumnInfo.ParentGroupKey = "Closed";
            ultraGridColumn73.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn73.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn74.Header.Caption = "Способ";
            ultraGridColumn74.Header.ToolTipText = "Способ закрытия АТ";
            ultraGridColumn74.Header.VisiblePosition = 35;
            ultraGridColumn74.Hidden = true;
            ultraGridColumn74.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn74.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn74.RowLayoutColumnInfo.ParentGroupKey = "Closed";
            ultraGridColumn74.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn74.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn74.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn75.Header.Caption = "Способ";
            ultraGridColumn75.Header.ToolTipText = "Способ закрытия АТ";
            ultraGridColumn75.Header.VisiblePosition = 36;
            ultraGridColumn75.Hidden = true;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn75.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn75.RowLayoutColumnInfo.ParentGroupKey = "Closed";
            ultraGridColumn75.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn76.Header.VisiblePosition = 37;
            ultraGridColumn76.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51,
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56,
            ultraGridColumn57,
            ultraGridColumn58,
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67,
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76});
            ultraGridGroup1.Header.Caption = "Признаки";
            ultraGridGroup1.Key = "Markers";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.Caption = "Налогоплательщик";
            ultraGridGroup2.Key = "Taxpayer";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 16;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Header.Caption = "1-ая сторона";
            ultraGridGroup3.Key = "BuyerSide";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 22;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup4.Header.Caption = "2-ая сторона";
            ultraGridGroup4.Key = "SellerSide";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 26;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup5.Header.Caption = "Корректировка";
            ultraGridGroup5.Key = "Correction";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 32;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 10;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup6.Header.Caption = "Статусы исполнения СЭОД";
            ultraGridGroup6.Key = "SeodStatuses";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 52;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 10;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup7.Header.Caption = "Закрыто";
            ultraGridGroup7.Key = "Closed";
            ultraGridGroup7.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 62;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup8.Header.Caption = "Инспекция";
            ultraGridGroup8.Key = "Sono";
            ultraGridGroup8.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 12;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup9.Header.Caption = "Регион";
            ultraGridGroup9.Key = "RegionGroup";
            ultraGridGroup9.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup9.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup9.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup9.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup9.RowLayoutGroupInfo.SpanY = 3;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8,
            ultraGridGroup9});
            rowLayout1.ColumnInfos.AddRange(new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo[] {
            rowLayoutColumnInfo1,
            rowLayoutColumnInfo2,
            rowLayoutColumnInfo3,
            rowLayoutColumnInfo4,
            rowLayoutColumnInfo5,
            rowLayoutColumnInfo6,
            rowLayoutColumnInfo7,
            rowLayoutColumnInfo8,
            rowLayoutColumnInfo9,
            rowLayoutColumnInfo10,
            rowLayoutColumnInfo11,
            rowLayoutColumnInfo12,
            rowLayoutColumnInfo13,
            rowLayoutColumnInfo14,
            rowLayoutColumnInfo15,
            rowLayoutColumnInfo16,
            rowLayoutColumnInfo17,
            rowLayoutColumnInfo18,
            rowLayoutColumnInfo19,
            rowLayoutColumnInfo20,
            rowLayoutColumnInfo21,
            rowLayoutColumnInfo22,
            rowLayoutColumnInfo23,
            rowLayoutColumnInfo24,
            rowLayoutColumnInfo25,
            rowLayoutColumnInfo26,
            rowLayoutColumnInfo27,
            rowLayoutColumnInfo28,
            rowLayoutColumnInfo29,
            rowLayoutColumnInfo30,
            rowLayoutColumnInfo31,
            rowLayoutColumnInfo32,
            rowLayoutColumnInfo33,
            rowLayoutColumnInfo34,
            rowLayoutColumnInfo35,
            rowLayoutColumnInfo36,
            rowLayoutColumnInfo37,
            rowLayoutColumnInfo38,
            rowLayoutColumnInfo39,
            rowLayoutColumnInfo40,
            rowLayoutColumnInfo41,
            rowLayoutColumnInfo42,
            rowLayoutColumnInfo43,
            rowLayoutColumnInfo44,
            rowLayoutColumnInfo45,
            rowLayoutColumnInfo46,
            rowLayoutColumnInfo47});
            rowLayout1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand1.RowLayouts.AddRange(new Infragistics.Win.UltraWinGrid.RowLayout[] {
            rowLayout1});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this._grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this._grid.DisplayLayout.MaxColScrollRegions = 1;
            this._grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this._grid.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this._grid.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this._grid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._grid.DisplayLayout.Override.CellAppearance = appearance20;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.CellPadding = 0;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterProvider;
            this._grid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            this._grid.DisplayLayout.Override.GroupHeaderTextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            appearance22.TextHAlignAsString = "Center";
            this._grid.DisplayLayout.Override.HeaderAppearance = appearance22;
            this._grid.DisplayLayout.Override.HeaderCheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this._grid.DisplayLayout.Override.RowAppearance = appearance23;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this._grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this._grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(1057, 156);
            this._grid.TabIndex = 0;
            this._grid.Text = "ultraGrid1";
            // 
            // _contextMenu
            // 
            this._contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmExclude,
            this.cmInclude,
            this.cmOpenClaimCard,
            this.cmExport});
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(230, 92);
            this._contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.OnContextMenuOpening);
            // 
            // cmExclude
            // 
            this.cmExclude.Name = "cmExclude";
            this.cmExclude.Size = new System.Drawing.Size(229, 22);
            this.cmExclude.Text = "Исключить из отправки";
            // 
            // cmInclude
            // 
            this.cmInclude.Name = "cmInclude";
            this.cmInclude.Size = new System.Drawing.Size(229, 22);
            this.cmInclude.Text = "Включить в отправку";
            // 
            // cmOpenClaimCard
            // 
            this.cmOpenClaimCard.Name = "cmOpenClaimCard";
            this.cmOpenClaimCard.Size = new System.Drawing.Size(229, 22);
            this.cmOpenClaimCard.Text = "Открыть карточку АТ по СФ";
            // 
            // cmExport
            // 
            this.cmExport.Name = "cmExport";
            this.cmExport.Size = new System.Drawing.Size(229, 22);
            this.cmExport.Text = "Выгрузить АТ по СФ";
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models.InvoiceClaimListViewModel);
            // 
            // InvoiceClaimListGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this._grid);
            this.Name = "InvoiceClaimListGridView";
            this.Size = new System.Drawing.Size(1057, 156);
            this.Load += new System.EventHandler(this.GridViewLoad);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            this._contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterProvider;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.ToolStripMenuItem cmExclude;
        private System.Windows.Forms.ToolStripMenuItem cmInclude;
        private System.Windows.Forms.ToolStripMenuItem cmOpenClaimCard;
        private System.Windows.Forms.ToolStripMenuItem cmExport;
    }
}
