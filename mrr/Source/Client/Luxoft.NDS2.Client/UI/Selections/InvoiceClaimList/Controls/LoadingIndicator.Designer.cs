﻿namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    partial class LoadingIndicator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._loadingLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _loadingLabel
            // 
            this._loadingLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._loadingLabel.AutoSize = true;
            this._loadingLabel.BackColor = System.Drawing.Color.Transparent;
            this._loadingLabel.Location = new System.Drawing.Point(0, -13);
            this._loadingLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this._loadingLabel.Name = "_loadingLabel";
            this._loadingLabel.Size = new System.Drawing.Size(0, 13);
            this._loadingLabel.TabIndex = 0;
            this._loadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LoadingIndicator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._loadingLabel);
            this.Name = "LoadingIndicator";
            this.Size = new System.Drawing.Size(3, 1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _loadingLabel;
    }
}
