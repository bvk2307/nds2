﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    internal class CloseReasonColumnFilterView : GridColumnFilterView
    {
        public CloseReasonColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            CloseReasonColumnView.CloseReasonDictionary[CloseReason.Empty] = "(Пусто)";
            foreach (var dictionaryItem in CloseReasonColumnView.CloseReasonDictionary)
            {
                e.ValueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
        }
    }

    public static class CloseReasonColumnView
    {
        public static void Apply(UltraGridColumn column)
        {
            column.Style = ColumnStyle.DropDownList;
            column.ValueList = FillColumnItems();
        }

        public static readonly Dictionary<CloseReason, string> CloseReasonDictionary =
            new Dictionary<CloseReason, string>()
           {
                {CloseReason.Empty, " "},
                {CloseReason.ClosedKnp, "Закрылась КНП"},
                {CloseReason.ClosedAllDiscrepancies, "Закрылись все расхождения"},
                {CloseReason.ExpiredWork, "Истек срок отработки"},
                {CloseReason.Annulable, "Корректировка аннулирована"},
                {CloseReason.NewCorrection, "Новая корректировка" }
           };

        private static ValueList FillColumnItems()
        {
            var valueList = new ValueList();

            CloseReasonDictionary[CloseReason.Empty] = " ";

            foreach (var dictionaryItem in CloseReasonDictionary)
            {
                valueList.ValueListItems.Add(new ValueListItem(dictionaryItem.Key, dictionaryItem.Value));
            }
            return valueList;
        }
    }
}
