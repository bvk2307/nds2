﻿using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public partial class LoadingIndicator : UserControl
    {
        public LoadingIndicator()
        {
            InitializeComponent();
        }

        public void Show(string text)
        {
            _loadingLabel.Text = text;
            Visible = true;
        }
    }
}
