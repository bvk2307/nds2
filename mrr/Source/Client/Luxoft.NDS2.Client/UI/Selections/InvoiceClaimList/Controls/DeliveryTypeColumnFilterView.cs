﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Controls
{
    public class DeliveryTypeColumnFilterView : GridColumnFilterView
    {
        public DeliveryTypeColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            e.ValueList.ValueListItems.Add(new ValueListItem("По ТКС", "По ТКС"));
            e.ValueList.ValueListItems.Add(new ValueListItem("По почте", "По почте"));
            e.ValueList.ValueListItems.Add(new ValueListItem("Лично", "Лично"));

        }
    }
}
