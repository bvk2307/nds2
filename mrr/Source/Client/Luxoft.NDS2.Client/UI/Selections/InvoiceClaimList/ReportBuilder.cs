﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.Services.ExcelReports.Claims;
using Luxoft.NDS2.Client.Services.Mock;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using System;
using System.Collections.Generic;
using System.IO;
using CommonComponents.Utils.Async;
using ErrorEventArgs = Luxoft.NDS2.Client.UI.UserTask.Report.ErrorEventArgs;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    class ReportBuilder
    {
        private readonly ICatalogService _catalogService;
        private readonly IInvoiceClaimReportService _service;
        private readonly INotifier _notifier;
        private readonly IClientLogger _logger;

        public ReportBuilder(ICatalogService catalogService, IInvoiceClaimReportService service, INotifier notifier, IClientLogger logger)
        {
            _catalogService = catalogService;
            _service = service;
            _notifier = notifier;
            _logger = logger;
        }

        public void BuildSelectionStatsReport(DateTime date, string fileName, Action complete, Action failed)
        {
            _files.Add(fileName);

            var loader = new ReportSelectionStatsDataLoader(_service, _notifier, _logger);

            loader.DataLoaded += (sender, args) =>
            {
                if (Canceled)
                {
                    _files.Remove(fileName);
                    return;
                }

                var asyncWorker = new AsyncWorker<bool>();
                asyncWorker.DoWork +=
                    (s, e) =>
                    {
                        var builder = new SelectionStatsExporter(_catalogService);
                        builder.AddWorksheetData(args.Data);
                        builder.Export(fileName);
                    };
                asyncWorker.Complete += (s, e) =>
                {
                    _files.Remove(fileName);
                    if (!Canceled)
                        complete();
                    else
                        File.Delete(fileName);
                };
                asyncWorker.Failed += (s, e) =>
                {
                    _files.Remove(fileName);
                    if (!Canceled)
                        failed();
                    else File.Delete(fileName);
                };

                asyncWorker.Start();
               
            };

            loader.BeginLoad(date);
        }

        public void BuildInvoiceClaimListReport(DateTime date, int[] statuses, List<FilterQuery> filter, string fileName, Action complete, Action failed)
        {
            _files.Add(fileName);

            var loader = new ReportInvoiceClaimListDataLoader(_service, _notifier, _logger);

            loader.DataLoaded += (sender, args) =>
            {
                if (Canceled)
                {
                    _files.Remove(fileName);
                    return;
                }

                var asyncWorker = new AsyncWorker<bool>();
                asyncWorker.DoWork +=
                    (s, e) =>
                    {
                        var builder = new InvoiceClaimListExporter(_catalogService);
                        builder.AddWorksheetData(args.Data);
                        builder.Export(fileName);
                    };
                asyncWorker.Complete += (s, e) =>
                {
                    _files.Remove(fileName);
                    if (!Canceled)
                        complete();
                    else
                        File.Delete(fileName);
                };
                asyncWorker.Failed += (s, e) =>
                {
                    _files.Remove(fileName);
                    if (!Canceled)
                        failed();
                    else File.Delete(fileName);
                };

                asyncWorker.Start();
            };

            if (statuses != null)
                loader.BeginLoad(date, statuses, filter);
            else
                loader.BeginLoad(date, filter);
        }

        public class TaxMonitoringStage
        {
            public int Step { get; set; }

            public TaxMonitoringStage()
            {
                Step = 1;
            }

            private readonly Dictionary<int, int> _chapterMap = new Dictionary<int, int>
            {
                {1, 8},
                {2, 81},
                {3, 9},
                {4, 91},
                {5, 10},
                {6, 11},
                {7, 12}
            };

            public int Chapter
            {
                get
                {
                    return _chapterMap[Step];
                }
            }
        }

        public void BuildTaxMonitoringReport(long docId, string fileName, Action complete, Action failed)
        {
            _files.Add(fileName);

            var loader = new TaxMonitoringDataLoader(_service, _notifier, _logger);

            var stage = new TaxMonitoringStage();
            var report = new TaxMonitoringReport(_catalogService, fileName);
            report.Complete += (sender, arg) =>
            {
                if (Canceled)
                {
                    _files.Remove(fileName);
                    File.Delete(fileName);
                    return;
                }

                stage.Step = arg.Value + 1;

                if (stage.Step < 8)
                    loader.BeginLoadChapter(docId, stage.Chapter);
                else if (stage.Step == 8)
                    loader.BeginLoadUnconfirmed(docId);
                else
                {
                    _files.Remove(fileName);
                    if (!Canceled)
                    {
                        report.CleanUpReport();
                        complete();
                    }
                    else
                        File.Delete(fileName);
                }
            };
            report.Failed += (sender, arg) =>
            {
                _files.Remove(fileName);
                if (!Canceled)
                    failed();
                else
                    File.Delete(fileName);
            };

            loader.DataLoaded += (sender, args) =>
            {
                if (Canceled)
                {
                    _files.Remove(fileName);
                    return;
                }

                report.PrepareReportFile();

                report.BuildReportPart(stage.Step, args.Data);
            };
            loader.BeginLoadChapter(docId, stage.Chapter);
        }

        public bool Canceled { get; set; }

        private readonly List<string> _files = new List<string>();

        public IEnumerable<string> ReportsInProgress
        {
            get { return _files; }
        }
    }
}
