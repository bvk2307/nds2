﻿using System.IO;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.DatePicker;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.Models;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    class Presenter
    {
        private const string  LoadingIndicatorLabel = "Идет формирование файла \"{0}\"";
        private readonly Model _model;
        private readonly IView _view;
        private readonly INotifier _notifier;
        private readonly ISecurityService _securityService;
        private readonly ReportBuilder _reportBuilder;
        private readonly IClaimCardOpener _claimOpener;
        private readonly InvoiceClaimCommandExecutor _executor;
        private readonly InvoiceClaimDataLoader _invoiceClaimDataLoader;
        private readonly NewArchDateLoader _baseDateLoader;
        private readonly CreateDatesLoader _createDatesLoader;
        private readonly SendStatusLoader _sendStatusDataLoader;
        private readonly StatusListDataLoader _statusListDataLoader;
        private readonly ClaimsCountLoader _claimsCountLoader;

        private string[] _operations;

        private InvoiceClaimListPresenter _gridPresenter;

        public Presenter(
            Model model,
            IView view,
            INotifier notifier,
            ISecurityService securityService,
            ReportBuilder reportBuilder,
            IClaimCardOpener claimOpener,
            InvoiceClaimCommandExecutor executor,
            InvoiceClaimDataLoader invoiceClaimDataLoader,
            ClaimsCountLoader claimsCountLoader,
            NewArchDateLoader baseDateLoader,
            CreateDatesLoader createDatesLoader,
            SendStatusLoader sendStatusDataLoader,
            StatusListDataLoader statusListDataLoader)
        {
            _model = model;
            _view = view;
            _notifier = notifier;
            _securityService = securityService;
            _reportBuilder = reportBuilder;
            _claimOpener = claimOpener;
            _executor = executor;
            _invoiceClaimDataLoader = invoiceClaimDataLoader;
            _baseDateLoader = baseDateLoader;
            _createDatesLoader = createDatesLoader;
            _sendStatusDataLoader = sendStatusDataLoader;
            _statusListDataLoader = statusListDataLoader;
            _claimsCountLoader = claimsCountLoader;

            _view.Load += (sender, args) => OnViewLoaded();
            _view.BeforeClosing += OnClosing;

            LoadOperations();
        }

        private void LoadOperations()
        {
            try
            {
                var result =
                    _securityService.ValidateAccess(
                        new[] 
                        { 
                            MrrOperations.Selection.SelectionIncludeClaim,
                            MrrOperations.Selection.SelectionSendClaim
                        });
                _operations = result.Status == ResultStatus.Success
                    ? result.Result
                    : new string[0];
            }
            catch
            {
                _operations = new string[0];
            }
        }

        private bool OperationAllowed(params string[] operations)
        {
            return _operations.Intersect(operations).Any();
        }

        private CommandResult OnClosing()
        {
            var files = _reportBuilder.ReportsInProgress.Select(Path.GetFileName).ToArray();
            if (files.Length == 0)
                return CommandResult.Success;

            var s = string.Join(", ", files);

            if (!_view.ActionConfirmed(string.Format(ResourceManagerNDS2.ExportExcelMessages.CancelQuery, s),
                ResourceManagerNDS2.ExportExcelMessages.Caption))
                return CommandResult.Refuse;

            _reportBuilder.Canceled = true;
            return CommandResult.Success;
        }

        public void OnViewLoaded()
        {
            InitDates();
            InitStatusList();
            _view.InitRibbon();

            _executor.Included += (sender, args) => _gridPresenter.BeginLoad();
            _executor.Excluded += (sender, args) => _gridPresenter.BeginLoad();
            _executor.Sent += (sender, args) =>
            {
                _model.SendStatus = _sendStatusDataLoader.Load();
                _gridPresenter.BeginLoad();
            };

            _gridPresenter = new InvoiceClaimListPresenter(
                _invoiceClaimDataLoader,
                _model.InvoiceClaimList,
                _view.Grid,
                _view.GridPager);
            _gridPresenter.WithNotifier(_notifier);
            _view.Grid.SetupMenuRequested += (sender, args) =>
            {
                var item = _view.Grid.SelectedItem as InvoiceClaimViewModel;
                if (item == null)
                    return;

                _view.Grid.IncludeVisible = item.IsExcluded;
                _view.Grid.IncludeEnabled = _model.AllowIncludeExclude && OperationAllowed(MrrOperations.Selection.SelectionIncludeClaim);
            };

            _view.Grid.Including += (sender, args) =>
            {
                var item = _view.Grid.SelectedItem as InvoiceClaimViewModel;
                if (item == null)
                    return;

                _executor.BeginInclude(item.DocId);
            };

            _view.Grid.Excluding += (sender, args) =>
            {
                var item = _view.Grid.SelectedItem as InvoiceClaimViewModel;
                if (item == null)
                    return;

                _executor.BeginExclude(item.DocId);
            };

            _view.UpdateRequested += (sender, args) => _gridPresenter.BeginLoad();

            _view.SendRequested += OnSend;

            _view.Grid.Opening += (sender, args) =>
            {
                var item = _view.Grid.SelectedItem as InvoiceClaimViewModel;
                if (item == null)
                    return;

                _claimOpener.ViewClaimDetails(item.DocId);
            };

            _view.Grid.Exporting += BuildTaxMonitoring;

            _model.SendStatus = _sendStatusDataLoader.Load();

            _view.ReportStatsRequested += BuildReportStats;
            _view.ReportListRequested += BuildReportList;


            _invoiceClaimDataLoader.DataLoaded += (sender, args) =>
            {
                _view.SendEnabled = _model.AllowSend && OperationAllowed(MrrOperations.Selection.SelectionSendClaim);
                _view.AllowSelectionStatsReport = _model.AllowSelectionStatsReport;
                _view.AllowInvoiceClaimListReport = true;

                _view.Grid.PopulateCorrectionFilter(_model.InvoiceClaimList.ListItems.Select(x => x.CorrectionNumber));
                _view.Grid.PopulateTaxPeriodFilter(_model.InvoiceClaimList.ListItems.Select(x => x.TaxPeriod));
            };

            _gridPresenter.BeginLoad();
        }

        private void BuildTaxMonitoring(object sender, EventArgs e)
        {
            var item = _view.Grid.SelectedItem as InvoiceClaimViewModel;
            if (item == null)
                return;

            var fileName = string.Format(ResourceManagerNDS2.InvoiceClaimList.DefaultTaxMonitoringFileName, _model.Date, item.TaxPeriodCode, item.Inn);
            if (!_view.GetFileName(ref fileName))
                return;

            _view.ShowLoadingIndicator(string.Format(LoadingIndicatorLabel, Path.GetFileName(fileName)));

            _reportBuilder.BuildTaxMonitoringReport(
                item.DocId,
                fileName,
                () => ReportFinished(fileName),
                () => ReportFailed(fileName)
                );

        }

        private void BuildReportList(object sender, EventArgs e)
        {
            var fileName = string.Format(ResourceManagerNDS2.InvoiceClaimList.DefaultInvoiceClaimListFileName, _model.Date, DateTime.Now);
            if (!_view.GetFileName(ref fileName))
                return;

            _view.ShowLoadingIndicator(string.Format(LoadingIndicatorLabel, Path.GetFileName(fileName)));

            _reportBuilder.BuildInvoiceClaimListReport(
                _model.Date,
                !_model.ToSend? _model.Statuses : null,
                _invoiceClaimDataLoader.Filter,
                fileName,
                () => ReportFinished(fileName),
                () => ReportFailed(fileName)
                );
        }

        private void ReportFinished(string fileName)
        {
            _view.HideLoadingIndicator();

            if (_view.ActionConfirmed(
                string.Format(ResourceManagerNDS2.ExportExcelMessages.OpenQuery,
                Path.GetFileName(fileName)), ResourceManagerNDS2.InvoiceClaimList.ReportCaption))
            {
                new FileOpener().Open(fileName);
            }
        }

        private void ReportFailed(string fileName)
        {
            _notifier.ShowWarning(
                string.Format(ResourceManagerNDS2.ExportExcelMessages.FileFormedError, fileName));
            _view.HideLoadingIndicator();
        }

        private void BuildReportStats(object sender, EventArgs e)
        {
            var fileName = string.Format(ResourceManagerNDS2.InvoiceClaimList.DefaultSelectionStatsFileName, _model.Date);
            if (!_view.GetFileName(ref fileName))
                return;

            _view.ShowLoadingIndicator(string.Format(LoadingIndicatorLabel, Path.GetFileName(fileName)));

            _reportBuilder.BuildSelectionStatsReport(
                _model.Date,
                fileName,
                () => ReportFinished(fileName),
                () => ReportFailed(fileName)
                );
        }

        private void OnSend(object sender, EventArgs e)
        {
            int count = _claimsCountLoader.Count(_model.Date);
            if (!_view.ConfirmSending(count))
                return;

            _executor.Send(_model.Date);
        }

        private void InitDates()
        {
            _model.StartNewArchitectureDate = _baseDateLoader.Load();

            var dates = _createDatesLoader.Load();
            if (dates == null || dates.Length == 0)
                dates = new[] { DateTime.Today };

            _model.Date = dates.Last();
            var model = new DatePickerModel(dates, _model.Date);
            model.SelectedDateChanged += (sender, args) =>
            {
                _model.Date = model.SelectedDate;

                _model.SendStatus = _sendStatusDataLoader.Load();

                _gridPresenter.BeginLoad();
            };
            new DatePickerPresenter(_view.DatePicker, model);
        }

        private const int StatusGenerated = -1;

        private void InitStatusList()
        {
            var list = _statusListDataLoader.Load();

            _model.StatusLookupModel.UpdateDictionary(list.Select(x => (ISelectableLookupItem)new StatusLookupItem(x)).ToArray());
            _model.StatusLookupModel.SelectAll();
            new LookupPresenter(_view.StatusLookup, _model.StatusLookupModel);

            _model.StatusesChanged += (sender, args) => _gridPresenter.BeginLoad();

            _view.ToSendCheckedChanged += (sender, args) =>
            {
                _model.ToSend = _view.ToSendChecked;
                if (_view.ToSendChecked)
                {
                    _model.StatusLookupModel.Selection =
                        _model.StatusLookupModel.Data.Where(x => ((StatusLookupItem)x).Data.Id == StatusGenerated);
                    _view.IsStatusSelectionEnabled = false;
                }
                else
                {
                    _view.IsStatusSelectionEnabled = true;
                }
                _gridPresenter.BeginLoad();
            };
        }


    }
}
