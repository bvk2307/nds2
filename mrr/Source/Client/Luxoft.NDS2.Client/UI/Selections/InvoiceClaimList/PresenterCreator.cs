﻿using CommonComponents.Catalog;
using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Uc.Infrastructure.Interface;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList.DataLoaders;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.Claims;

namespace Luxoft.NDS2.Client.UI.Selections.InvoiceClaimList
{
    public class PresenterCreator : Presenter<IView>
    {
        public object Create(IView view, Model model)
        {
            var service = WorkItem.GetWcfServiceProxy<IInvoiceClaimService>();
            var reportService = WorkItem.GetWcfServiceProxy<IInvoiceClaimReportService>();
            var securityService = WorkItem.Services.Get<IClientContextService>().CreateCommunicationProxy<ISecurityService>(Constants.EndpointName);
            var notifier = WorkItem.Services.Get<INotifier>(true);
            var logger = WorkItem.Services.Get<IClientLogger>(true);
            var uis = WorkItem.Services.Get<IUserInfoService>(true);
            var catalogService = WorkItem.Services.Get<ICatalogService>(true);

            return new Presenter(
                model,
                view,
                notifier,
                securityService,
                new ReportBuilder(catalogService, reportService, notifier, logger), 
                new ClaimDetailsViewer(WorkItem),
                new InvoiceClaimCommandExecutor(service, new CurrentUserInfo(uis), new SimpleServiceProxy(notifier, logger)),
                new InvoiceClaimDataLoader(service, model, notifier, logger),
                new ClaimsCountLoader(notifier, logger, service),
                new NewArchDateLoader(notifier, logger, service),
                new CreateDatesLoader(notifier, logger, service),
                new SendStatusLoader(model, notifier, logger, service),
                new StatusListDataLoader(notifier, logger, service)
            );
        }
    }
}
