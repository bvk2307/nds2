﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class SelectionStatusColumnFilterView : GridColumnFilterView
    {
        private DictionarySelectionStatus _dictionarySelectionStatus;
        public DictionarySelectionStatus DictionarySelectionStatus
        {
            get
            {
                return _dictionarySelectionStatus ?? new DictionarySelectionStatus(Enumerable.Empty<DictionaryItem>());
            }
            set
            {
                _dictionarySelectionStatus = value;
            }
        }

        public SelectionStatusColumnFilterView(UltraGridColumn column, Infragistics.Win.UltraWinGrid.ColumnFilter defaultFilter)
            : base(column)
        {
            DefaultColumnFilter = defaultFilter;
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            e.ValueList.ValueListItems.Add(new ValueListItem(0, "Пусто"));
            foreach (var dictionaryItem in DictionarySelectionStatus.SelectionStatusList)
            {
                var item = new ValueListItem(dictionaryItem.Id, dictionaryItem.Description);
                e.ValueList.ValueListItems.Add(item);
            }
        }
    }
}
