﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Directory;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Selections.Models;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Security;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class SelectionListPresenter :
        PagingGridViewPresenter<SelectionListItemViewModel, SelectionListItemData, PageResult<SelectionListItemData>>
    {
        private readonly ISelectionServiceProxy _proxy;
        private readonly ISelectionListView _view;
        private readonly ISelectionDetailsViewer _viewer;
        private readonly List<AccessRight> _userOperationList;
        private readonly SelectionListViewModel _model;

        # region .ctor

        public SelectionListPresenter(
            ISelectionListView view,
            SelectionListViewModel model,
            ISelectionServiceProxy proxy,
            ISelectionDetailsViewer viewer,
            IServerGridDataProvider<PageResult<SelectionListItemData>> dataProvider,
            List<AccessRight> userOperationList) :
            base(dataProvider, model, view.GetGrid(), view.SelectionListPager)
        {
            _view = view;
            _model = model;
            _proxy = proxy;
            _viewer = viewer;
            _userOperationList = userOperationList;
            _view.Opened += ViewOpened;
        }

        #endregion

        # region Инициализация представления

        /// <summary>
        ///     инициализирует поля View из Модели и подписываемся на события View
        /// </summary>
        private void InitView()
        {
            _view.RefresSelectionListClicked += RefresSelectionListClicked;
            _view.CreateSelectionClicked += CreateSelectionClicked;
            _view.CreateSelectionTemplateClicked += CreateSelectionTemplateClicked;
            _view.CopySelectionTemplateClicked += CopySelectionTemplateClicked;
            _view.OpenSelectionClicked += OpenSelectionClicked;
            _view.DeleteSelectionClicked += DeleteSelectionClicked;
            _view.CreateClaimClicked += CreateClaimClicked;
            _view.RowSelected += OnRowSelected;
            _view.RibbonButtonsInitialized += OnRowSelected;
            _view.RowDoubleClicked += OpenSelectionClicked;
        }

        public void ResetPagerIndex()
        {
            QueryConditions.PaginationDetails.RowsToSkip = 0;
            _view.SelectionListPager.ResetPageIndex();
        }

        private void OnRowSelected(object sender, EventArgs eventArgs)
        {
            var item = (SelectionListItemViewModel)_view.GetGrid().SelectedItem;

            List<string> applicableCommands = new List<string>();
            List<string> notApplicableCommands = new List<string>();

            bool notAvailableForOperations = item == null || item.StatusId == SelectionStatus.Loading || item.StatusId == SelectionStatus.Deleted;

            if (IsUserEligibleForOperation(MrrOperations.Selection.Create))
                applicableCommands.Add("CreateSelection");
            else notApplicableCommands.Add("CreateSelection");

            if (IsUserEligibleForOperation(MrrOperations.Selection.CreateTemplate))
                applicableCommands.Add("CreateSelectionTemplate");
            else notApplicableCommands.Add("CreateSelectionTemplate");

            if (IsUserEligibleForOperation(MrrOperations.Selection.CopyTemplate)
                && !notAvailableForOperations && item.TypeCode == SelectionType.Template)
                applicableCommands.Add("CopySelectionTemplate");
            else notApplicableCommands.Add("CopySelectionTemplate");

            if (CanBeOpened(item) && !notAvailableForOperations)
                applicableCommands.Add("OpenSelection");
            else notApplicableCommands.Add("OpenSelection");

            if (CanBeDeleted(item) && !notAvailableForOperations)
                applicableCommands.Add("DeleteSelection");
            else notApplicableCommands.Add("DeleteSelection");

            if (IsUserEligibleForOperation(MrrOperations.Selection.SelectionCreateClaim) && _model.ExistApprovedSelections)
                applicableCommands.Add("CreateClaim");
            else notApplicableCommands.Add("CreateClaim");

            _view.ToggleMenuItemsAvailability(applicableCommands.ToArray(), true);
            _view.ToggleMenuItemsAvailability(notApplicableCommands.ToArray(), false);
            _view.RefreshMenu();
        }

        private bool CanBeOpened(SelectionListItemViewModel item)
        {
            if (item == null)
                return false;

            return IsUserEligibleForOperation(MrrOperations.Selection.ViewSelection) ||
                   IsUserEligibleForOperation(MrrOperations.Selection.ViewTemplate) ||
                   (IsUserEligibleForOperation(MrrOperations.Selection.Edit) &&
                    item.TypeCode == SelectionType.Hand &&
                    item.LastEditedBySid == _proxy.GetCurrentUserSid()) ||
                   (IsUserEligibleForOperation(MrrOperations.Selection.EditTemplateBasedSelection) &&
                    item.TypeCode == SelectionType.SelectionByTemplate &&
                    (string.IsNullOrEmpty(item.LastEditedBySid) ||
                     item.LastEditedBySid == _proxy.GetCurrentUserSid())) ||
                     (IsUserEligibleForOperation(MrrOperations.Selection.EditTemplate) &&
                     item.LastEditedBySid == _proxy.GetCurrentUserSid());
        }

        private bool CanBeDeleted(SelectionListItemViewModel item)
        {
            if (item == null)
                return false;

            return (IsUserEligibleForOperation(MrrOperations.Selection.DeleteSelectionDraft) &&
                item.TypeCode == SelectionType.Hand &&
                (item.StatusId == SelectionStatus.Draft || item.StatusId == SelectionStatus.LoadingError) &&
                     item.LastEditedBySid == _proxy.GetCurrentUserSid()) ||
                    (IsUserEligibleForOperation(MrrOperations.Selection.DeleteSelectionApproved) &&
                    item.TypeCode == SelectionType.Hand &&
                    (item.StatusId == SelectionStatus.RequestForApproval || item.StatusId == SelectionStatus.Approved)) ||
                    (IsUserEligibleForOperation(MrrOperations.Selection.DeleteTemplate) && 
                    item.TypeCode == SelectionType.Template &&
                     item.ApprovedBySid == _proxy.GetCurrentUserSid() && _proxy.CanDeleteTemplate(item.Id));
        }

        private void CreateClaimClicked(object sender, EventArgs eventArgs)
        {
            DoCreateClaim();
        }

        private void DoCreateClaim()
        {
            
            var statusesCount = _proxy.CountApprovedSelections();

            if (statusesCount.ApprovedCount > 0)
            {
                if (_view.ShowQuestion(
                      ResourceManagerNDS2.Attention,
                      string.Format(
                        ResourceManagerNDS2.CreateClaimConfirmMessage,
                        statusesCount.ApprovedCount,
                        (statusesCount.RequestApproveCount > 0 ? string.Format(ResourceManagerNDS2.RequestApproveCountMessage, statusesCount.RequestApproveCount) : string.Empty)
                      )) == DialogResult.Yes)
                {
                    _proxy.SetClaimCreatingStatus();
                    _model.ExistApprovedSelections = false;
                }
            }
            else 
            { 
                _model.ExistApprovedSelections = false;
            }

            BeginLoad();
        }

        /// <summary>
        /// Удаляет выборку или шаблон
        /// </summary>
        private void DeleteSelectionClicked(object sender, EventArgs eventArgs)
        {
            DoRemove();
        }

        private bool IsUserEligibleForOperation(string operationId)
        {
            return _userOperationList.Any(o => o.Name.Equals(operationId));

        }

        /// <summary>
        /// Удаляет выборку или шаблон
        /// </summary>
        private void DoRemove()
        {
            var selectedItem = (SelectionListItemViewModel)_view.GetGrid().SelectedItem;

            if (!CanBeDeleted(selectedItem))
            {
                return;
            }

            var sel = new Selection
            {
                Id = selectedItem.Id,
                Status = selectedItem.StatusId.GetValueOrDefault(),
                AnalyticSid = selectedItem.LastEditedBySid,
                ManagerSid = selectedItem.ApprovedBySid,
                Name = selectedItem.Name,
                TypeCode = selectedItem.TypeCode,
                TemplateId = selectedItem.TemplateId
            };

            if (!_proxy.CanBeDeleted(sel))
            {
                _view.ShowWarning("У вас нет прав, необходимых для удаления выбранной записи");

                return;
            }

            if (sel.TypeCode == SelectionType.Hand)
                if (_view.ShowQuestion(
                  ResourceManagerNDS2.Attention,
                  string.Format(ResourceManagerNDS2.SelectionDeleteConfirmMessage, sel.Name)) == DialogResult.Yes)
                {
                    _proxy.TryDelete(sel, new SelectionCommandParam() { Comment = string.Empty });
                }

            if (sel.TypeCode == SelectionType.Template)
                if (_view.ShowQuestion(
                  ResourceManagerNDS2.Attention,
                  string.Format(ResourceManagerNDS2.TemplateSelectionDeleteConfirmMessage, sel.Name)) == DialogResult.Yes)
                {

                    bool canDelete = _proxy.CanDeleteTemplate(sel.Id);
                    if (!canDelete)
                    {
                        _view.ShowInfo(ResourceManagerNDS2.Attention, ResourceManagerNDS2.TemplateSelectionCannotBeDeletedMessage);
                        return;
                    }

                    _proxy.TryDeleteSelectionsByTemplate(sel.Id);
                }
            BeginLoad();
        }


        private void OpenSelectionClicked(object sender, EventArgs eventArgs)
        {
            var item = (SelectionListItemViewModel)_view.GetGrid().SelectedItem;

            if(item ==  null)
                return;

            if (!CanBeOpened(item) || item.StatusId == SelectionStatus.Loading || item.StatusId == SelectionStatus.Deleted)
                return;

            SelectionOpenModeEnum mode =  item.TypeCode == SelectionType.Template ? SelectionOpenModeEnum.ViewTemplate :
                (item.TypeCode == SelectionType.Hand ? SelectionOpenModeEnum.ViewSelection : SelectionOpenModeEnum.ViewSelectionByTemplate);

            if (IsUserEligibleForOperation(MrrOperations.Selection.Edit) &&
                    item.TypeCode == SelectionType.Hand &&
                    item.LastEditedBySid == _proxy.GetCurrentUserSid() && 
                    (item.StatusId == SelectionStatus.LoadingError || item.StatusId == SelectionStatus.Draft))
                mode = SelectionOpenModeEnum.EditSelection;

            if (IsUserEligibleForOperation(MrrOperations.Selection.EditTemplateBasedSelection) &&
                   item.TypeCode == SelectionType.SelectionByTemplate &&
                   item.LastEditedBySid == _proxy.GetCurrentUserSid() &&
                   (item.StatusId == SelectionStatus.LoadingError || item.StatusId == SelectionStatus.Draft))
                mode = SelectionOpenModeEnum.EditSelectionByTemplate;

            if (IsUserEligibleForOperation(MrrOperations.Selection.EditTemplate) &&
                   item.TypeCode == SelectionType.Template &&
                   item.ApprovedBySid == _proxy.GetCurrentUserSid())
                mode = SelectionOpenModeEnum.EditTemplate;

            if (IsUserEligibleForOperation(MrrOperations.Selection.SelectionApprove) &&
                (item.StatusId == SelectionStatus.RequestForApproval))
                mode = item.TypeCode == SelectionType.Hand ? SelectionOpenModeEnum.ApproveSelection : SelectionOpenModeEnum.ApproveSelectionByTemplate;

             OpenFiltersWindow(item, mode);
        }

        private void CreateSelectionTemplateClicked(object sender, EventArgs eventArgs)
        {
            OpenFiltersWindow(null, SelectionOpenModeEnum.NewTemplate);
        }

        private void CopySelectionTemplateClicked(object sender, EventArgs eventArgs)
        {
            var item = (SelectionListItemViewModel)_view.GetGrid().SelectedItem;
            if(item == null)
                return;
            
            OpenFiltersWindow(item, SelectionOpenModeEnum.CopyTemplate);
        }

        private void CreateSelectionClicked(object sender, EventArgs eventArgs)
        {
            OpenFiltersWindow(null, SelectionOpenModeEnum.NewSelection);
        }

        private void RefresSelectionListClicked(object sender, EventArgs eventArgs)
        {
            ResetPagerIndex();
            BeginLoad();
            CheckExistApprovedSelections();
        }

        private void CheckExistApprovedSelections()
        {
            if (IsUserEligibleForOperation(MrrOperations.Selection.SelectionCreateClaim))
                _model.ExistApprovedSelections = _proxy.ExistApprovedSelections();            
        }

        #region События view

        /// <summary>
        ///     Открытие View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOpened(object sender, SelectionListOpenedEventArgs e)
        {
            e.DictionarySelectionStatus = StatuDictionary;
            QueryConditions.Filter.Add(
                new FilterQuery
                {
                    ColumnName = TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.StatusId),
                    UserDefined = false,
                    FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                    GroupOperator = FilterQuery.FilterLogicalOperator.Or,
                    Filtering = new List<ColumnFilter>
                    {
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.Approved
                        },
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.ClaimCreating
                        },

                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.Draft
                        },
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.Loading
                        },
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.LoadingError
                        },
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = SelectionStatus.RequestForApproval
                        }
                    }
                });
            QueryConditions.Filter.Add(
                new FilterQuery
                {
                    ColumnName = TypeHelper<SelectionListItemViewModel>.GetMemberName(t => t.StatusId),
                    UserDefined = false,
                    FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                    GroupOperator = FilterQuery.FilterLogicalOperator.Or,
                    Filtering = new List<ColumnFilter>
                    {
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = 0
                        }
                    }
                });
            BeginLoad();
            CheckExistApprovedSelections();
            InitView();
        }

        public DictionarySelectionStatus StatuDictionary
        {
            get { return _proxy.TryLoadStatusDictionary(); }
        }


        /// <summary>
        ///     Открывает ЭФ фильтр выборки
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="mode"></param>
        /// <param name="type"></param>
        private void OpenFiltersWindow(
            SelectionListItemViewModel selection, 
            SelectionOpenModeEnum mode)
        {
            _viewer.Open(selection, _view, _proxy, mode);
        }
        #endregion
        #endregion
    }
}