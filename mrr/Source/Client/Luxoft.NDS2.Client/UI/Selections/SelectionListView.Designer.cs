﻿namespace Luxoft.NDS2.Client.UI.Selections
{
    partial class SelectionListView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._selectionListGridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.SuspendLayout();
            // 
            // _selectionListGridContainer
            // 
            this._selectionListGridContainer.AutoScroll = true;
            this._selectionListGridContainer.AutoSize = true;
            this._selectionListGridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._selectionListGridContainer.Location = new System.Drawing.Point(0, 0);
            this._selectionListGridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._selectionListGridContainer.Name = "_selectionListGridContainer";
            this._selectionListGridContainer.Size = new System.Drawing.Size(999, 634);
            this._selectionListGridContainer.TabIndex = 0;
            // 
            // SelectionListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._selectionListGridContainer);
            this.Name = "SelectionListView";
            this.Size = new System.Drawing.Size(999, 634);
            this.Load += new System.EventHandler(this.SelectionListViewLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.Controls.Grid.Controls.GridContainer _selectionListGridContainer;
    }
}
