﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.Services;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Net;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Selections
{
    public class ManageFavoritesServiceProxy : ServiceProxyBase, IManageFavoritesServiceProxy
    {
        private readonly ISelectionService _service;
        public ManageFavoritesServiceProxy(
            ISelectionService service,
            INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        public OperationResult<KeyValuePair<long, string>> CreateFavorite(string name, Filter filter)
        {
           return Invoke(() => _service.CreateFavorite(name, filter),
                () => new OperationResult<KeyValuePair<long, string>> {Status = ResultStatus.Error});

        }

        public bool TryOverwriteFavorite(string name, Filter filter)
        {
            var result = Invoke(() => _service.OverwriteFavorite(name, filter),
                () => new OperationResult { Status = ResultStatus.Error });

            return result.Status == ResultStatus.Success;

        }

        public bool TryDeleteFavorite(long id)
        {
            var result = Invoke(() => _service.DeleteFavorite(id),
                () => new OperationResult { Status = ResultStatus.Error });

            return result.Status == ResultStatus.Success;
        }

        public OperationResult<List<KeyValuePair<long, string>>> TryGetFavoriteFilters()
        {
            return Invoke(() => _service.GetFavoriteFilters(),
                () => new OperationResult<List<KeyValuePair<long, string>>> { Status = ResultStatus.Error });

        }

        public OperationResult<Filter> TryGetFavoriteSelectionFilterVersionTwo(long favoriteId)
        {
            return Invoke(() => _service.GetFavoriteSelectionFilterVersionTwo(favoriteId),
                () => new OperationResult<Filter> { Status = ResultStatus.Error });

        }


    }
}