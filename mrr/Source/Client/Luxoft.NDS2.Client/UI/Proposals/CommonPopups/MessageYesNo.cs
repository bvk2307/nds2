﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Proposals.CommonPopups
{
    /// <summary>
    /// Это локализованная замена MessageBox в режиме Yes/No
    /// </summary>
    public partial class MessageYesNo : Form
    {
        private MessageYesNo()
        {
            InitializeComponent();

            this.Text = string.Empty;
            this.labelMsg.Text = string.Empty;       
        }

        private static MessageYesNo _singl = new MessageYesNo();
        
        public static DialogResult Show(string caption, string message)
        {
            _singl.SetParam(caption, message);

            return _singl.ShowDialog();
        }

        private void SetParam(string caption, string message)
        {
            this.Text = caption;
            this.labelMsg.Text = message;
        }
    }
}
