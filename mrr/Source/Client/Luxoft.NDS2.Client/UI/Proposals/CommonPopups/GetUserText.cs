﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Proposals.CommonPopups
{
    public partial class GetUserText : Form
    {
        private GetUserText()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private static GetUserText _singl = new GetUserText();


        public static DialogResult Show(string caption, string message, string btnOkText, string btnCancelText, out string userText)
        {
            _singl.commentTextEditor.Text = string.Empty;
            _singl.Text = caption;
            _singl.commentLabel.Text = message;
            _singl.btnOK.Text = btnOkText;
            _singl.btnCancel.Text = btnCancelText;
            
            DialogResult ret = _singl.ShowDialog();

            userText = _singl.GetMessge();

            return ret;
        }

        private string GetMessge()
        {
            return commentTextEditor.Text;
        }

    }
}
