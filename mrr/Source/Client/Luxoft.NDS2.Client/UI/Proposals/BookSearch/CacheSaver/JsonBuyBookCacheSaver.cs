﻿using System;
using System.IO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Newtonsoft.Json;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch.CacheSaver
{
    public class JsonBuyBookCacheSaver : JsonCacheSaver
    {
        #region Overrides of JsonCacheSaver

        public override void Save(string cacheKey, Stream data)
        {
            SaveInternal<BuyBookLineDemo>(cacheKey, data);
        }

        protected override void FillObjectProperty(JsonTextReader reader, object obj)
        {
            var buyBook = (BuyBookLineDemo)obj;
            string propertyName = reader.Value.ToString();
            reader.Read();
            object propertyValue = reader.Value;
            switch (propertyName)
            {
                case "КодВидОпер":
                    buyBook.OperationCode = propertyValue as string;
                    break;
                case "НомСчФПрод":
                    buyBook.SellerInvoiceNum = propertyValue as string;
                    break;
                case "ДатаСчФПрод":
                    buyBook.SellerInvoiceDate =  Convert.ToDateTime(propertyValue);
                    break;
                case "ИННПрод":
                    buyBook.SellerInn = propertyValue as string;
                    break;
                case "КПППрод":
                    buyBook.SellerKpp = propertyValue as string;
                    break;
                case "СтоимПокупВ":
                    buyBook.TotalAmountByInvoice = Convert.ToDecimal(propertyValue);
                    break;
                case "СумНДСВыч":
                    buyBook.NdsAmountRub = Convert.ToDecimal(propertyValue);
                    break;
            }
        }

        protected override void FillDefaults(object obj)
        {
            var buyBook = (BuyBookLineDemo)obj;
            buyBook.BrokerInn = DataGenerator.GetRandomINN();
            buyBook.BrokerKpp = DataGenerator.GetRandomKPP();
            buyBook.BrokerName = DataGenerator.GetRandomTaxPayerName();
            buyBook.Currency = "руб";
            buyBook.CustomsDeclarationNumber = DataGenerator.GetRandomInvoiceNumber();
            buyBook.Id = TotalObjectsSaved;
            buyBook.NdsAmountRub = DataGenerator.GetRandomDecimal();
            buyBook.OperationCode = DataGenerator.GetRandomOperationCode();
            buyBook.PaymentDocDate = DataGenerator.GetRandomDateString();
            //buyBook.SellerInn = DataGenerator.GetRandomINN();
            //buyBook.SellerInvoiceDate = DataGenerator.GetRandomDateString();
            buyBook.SellerInvoiceDateChanged = DataGenerator.GetRandomDateString();
            buyBook.SellerInvoiceDateCorrected = DataGenerator.GetRandomDateString();
            //buyBook.SellerInvoiceNum = DataGenerator.GetRandomInvoiceNumber();
            buyBook.SellerInvoiceNumChanged = DataGenerator.GetRandomInvoiceNumber();
            buyBook.SellerInvoiceNumChangedAndCorrected = DataGenerator.GetRandomInvoiceNumber();
            buyBook.SellerInvoiceNumCorrected = DataGenerator.GetRandomInvoiceNumber();
            //buyBook.SellerKpp = DataGenerator.GetRandomKPP();
            buyBook.SellerName = DataGenerator.GetRandomINN();
            buyBook.TotalAmountByInvoice = DataGenerator.GetRandomDecimal();

        }

        #endregion
    }
}
