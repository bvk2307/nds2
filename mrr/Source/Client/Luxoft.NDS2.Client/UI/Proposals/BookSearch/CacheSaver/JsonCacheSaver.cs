﻿using System;
using System.IO;
using System.Threading;
using Luxoft.NDS2.Client.Helpers.CacheProviders;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch.CacheSaver
{
    public abstract class JsonCacheSaver
    {
        public long TotalObjectsSaved { get; protected set; }
        private uint _stepToNotify = 100;

        public event EventHandler OnStateChanged;

        private bool _cancellationRequested;
        public void Cancel()
        {
            _cancellationRequested=true;
        }

        public abstract void Save(string cacheKey, Stream data);

        protected void SaveInternal<T>(string cacheKey, Stream data) where T : class, new()
        {
            _cancellationRequested = false;
            TotalObjectsSaved = 0;
            int step = 0;
            var cacheProvider = new SqlCeCacheProvider();
            cacheProvider.CreateCache(cacheKey, typeof(T));

            bool enteredToArrayOfLines = false;
            bool enteredToObject = false;
            T currentBookLine = null;
            using (cacheProvider.StartTransaction())
            {
                using (var textReader = new StreamReader(data))
                {
                    using (var reader = new JsonTextReader(textReader))
                    {
                        while (reader.Read())
                        {
                            if(_cancellationRequested)
                            {
                                return;
                            }

                            switch (reader.TokenType)
                            {
                                case JsonToken.StartArray:
                                    enteredToArrayOfLines = true;
                                    break;
                                case JsonToken.StartObject:
                                    if (enteredToArrayOfLines)
                                    {
                                        currentBookLine = new T();
                                        FillDefaults(currentBookLine);
                                        enteredToObject = true;
                                    }
                                    break;
                                case JsonToken.PropertyName:
                                    if(reader.Value.Equals("status"))
                                    {
                                        reader.Read();
                                        var status = reader.Value;
                                    }

                                    if (enteredToArrayOfLines & enteredToObject)
                                    {
                                        FillObjectProperty(reader, currentBookLine);
                                    }
                                    break;
                                case JsonToken.EndObject:
                                    if (enteredToArrayOfLines & enteredToObject)
                                    {
                                        cacheProvider.AddToCache(cacheKey, currentBookLine);
//                                        //NOTE:Эмуляция сетевой задержки
//                                        Thread.Sleep(10);

                                        TotalObjectsSaved++;
                                        if(step++ > _stepToNotify)
                                        {
                                            NotifyStateChanges();
                                            step = 0;
                                        }
                                        enteredToObject = false;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }

        private void NotifyStateChanges()
        {
            if(OnStateChanged != null)
            {
                OnStateChanged(this, EventArgs.Empty);
            }
        }

        protected abstract void FillObjectProperty(JsonTextReader reader, object obj);

        protected abstract void FillDefaults(object obj);
    }
}
