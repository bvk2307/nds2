﻿using System;
using System.IO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Newtonsoft.Json;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch.CacheSaver
{
    public class JsonSellBookCacheSaver : JsonCacheSaver
    {
        public override void Save(string cacheKey, Stream data)
        {
            base.SaveInternal<SellBookLineDemo>(cacheKey, data);
        }

        protected override void FillObjectProperty(JsonTextReader reader, object obj)
        {
            var sellBook = (SellBookLineDemo)obj;
            string propertyName = reader.Value.ToString();
            reader.Read();
            object propertyValue = reader.Value;
            switch (propertyName)
            {
                case "КодВидОпер":
                    sellBook.OperationCode = propertyValue as string;
                    break;
                case "НаимПок":
                    sellBook.BuyerName = propertyValue as string;
                    break;
                case "НомСчФПрод":
                    sellBook.SellerInvoiceNum = propertyValue as string;
                    break;
                case "ДатаСчФПрод":
                    sellBook.SellerInvoiceDate = Convert.ToDateTime(propertyValue);
                    break;
                case "ИННПокуп":
                    sellBook.BuyerInn = propertyValue as string;
                    break;
                case "КПППокуп":
                    sellBook.BuyerInn = propertyValue as string;
                    break;
                case "СтоимПродСФ":
                    sellBook.InvoiceTotalNdsInclude = Convert.ToDecimal(propertyValue);
                    break;
                case "СтоимПродСФ18":
                    sellBook.AmountTaxed18Rub = Convert.ToDecimal(propertyValue);
                    break;
                case "СтоимПродСФ10":
                    sellBook.AmountTaxed10Rub = Convert.ToDecimal(propertyValue);
                    break;
                case "СтоимПродСФ0":
                    sellBook.AmountTaxed0 = Convert.ToDecimal(propertyValue);
                    break;
                case "СумНДССФ18":
                    sellBook.AmountTaxedNds18Rub = Convert.ToDecimal(propertyValue);
                    break;
                case "СумНДССФ10":
                    sellBook.AmountTaxedNds10Rub = Convert.ToDecimal(propertyValue);
                    break;
                case "СтоимПродОсв":
                    sellBook.TaxFreeAmountRub = Convert.ToDecimal(propertyValue);
                    break;
            }
        }

        protected override void FillDefaults(object obj)
        {
            var sellBook = (SellBookLineDemo)obj;
            //sellBook.AmountTaxed0 = DataGenerator.GetRandomDecimal();
            //sellBook.AmountTaxed10Rub = DataGenerator.GetRandomDecimal();
            //sellBook.AmountTaxed18Rub = DataGenerator.GetRandomDecimal();
            //sellBook.AmountTaxedNds10Rub = DataGenerator.GetRandomDecimal();
            //sellBook.AmountTaxedNds18Rub = DataGenerator.GetRandomDecimal();

            sellBook.BrokerInn = DataGenerator.GetRandomINN();
            sellBook.BrokerKpp = DataGenerator.GetRandomKPP();
            sellBook.BrokerName = DataGenerator.GetRandomTaxPayerName();

            //sellBook.BuyerInn = DataGenerator.GetRandomINN();
            //sellBook.BuyerKpp = DataGenerator.GetRandomKPP();
            //sellBook.BuyerName = DataGenerator.GetRandomTaxPayerName();

            sellBook.Currency = "руб.";

            sellBook.Id = TotalObjectsSaved;

            //sellBook.InvoiceTotalNdsInclude = DataGenerator.GetRandomDecimal();
            //sellBook.InvoiceTotalRubNdsInclude = DataGenerator.GetRandomDecimal();
            //sellBook.OperationCode = DataGenerator.GetRandomOperationCode();
            sellBook.PaymentDocDate = DataGenerator.GetRandomDateString();
            sellBook.PaymentDocNumber = DataGenerator.GetRandomInvoiceNumber();
            sellBook.SellerInvoiceDate = DataGenerator.GetRandomDateString();
            sellBook.SellerInvoiceDateChanged = DataGenerator.GetRandomDateString();
            sellBook.SellerInvoiceDateChangedAndCorrected = DataGenerator.GetRandomDateString();
            sellBook.SellerInvoiceDateCorrected = DataGenerator.GetRandomDateString();
            sellBook.SellerInvoiceNum = DataGenerator.GetRandomInvoiceNumber();
            sellBook.SellerInvoiceNumChanged = DataGenerator.GetRandomInvoiceNumber();
            sellBook.SellerInvoiceNumChangedAndCorrected = DataGenerator.GetRandomInvoiceNumber();
            sellBook.SellerInvoiceNumCorrected = DataGenerator.GetRandomInvoiceNumber();

            //sellBook.TaxFreeAmountRub = DataGenerator.GetRandomDecimal();



        }
    }
}
