﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Helpers.BookDownload;
using Luxoft.NDS2.Client.Helpers.CacheProviders;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Proposals.BookView;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.ServiceAgents;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch
{
    public class Presenter : BasePresenter<View>
    {
        public Presenter()
        {
            
        }

        protected Presenter(PresentationContext presentationContext, WorkItem wi, View view) : base(presentationContext, wi, view)
        {
        }

        public List<string> GetBookTypes()
        {
            return new List<string>() { "Книга покупок", "Книга продаж" };
        }

        public List<string> GetQuarters()
        {
            return GetServiceProxy<IDataService>().GetQuarters().Select(i=>i.Key).ToList();
        }

        public List<string> GetYears()
        {
            return GetServiceProxy<IDataService>().GetYears();
        }

        public OperationResult<List<SellBookLineDemo>> SearchSellBook(string inn, string kpp, string period, string year)
        {
            return GetServiceProxy<IDataService>().GetSellBook(inn, kpp, period, year);
        }

        public ISovServiceServiceAgent GetSovServiceAgent()
        {
            return WorkItem.Services.Get<ISovServiceServiceAgent>();
        }

        public OperationResult<List<BuyBookLineDemo>> SearchBuyBook(string inn, string kpp, string period, string year)
        {
            return GetServiceProxy<IDataService>().GetBuyBook(inn, kpp, period, year);
        }

        public void SaveSessionBook(string key, List<BusinessObject> data)
        {
            SqlCeCacheProvider prov = new SqlCeCacheProvider();
            prov.CacheItems(key, data);
        }

        public void OpenBookView(BookViewParameters parameters)
        {
            var managementService = WorkItem.Services.Get<IWindowsManagerService>();

            Guid viewId = Guid.NewGuid();

            FeatureContextBase fc = new FeatureContextBase(Guid.NewGuid());
            PresentationContext pc = new PresentationContext(viewId, null, fc);

            var windowTitle = parameters.BookType == BookType.Sell ? Constants.BOOK_SELL_TYPE : Constants.BOOK_BUY_TYPE;
            
            if(!managementService.Contains(viewId))
            {
                pc.WindowTitle = windowTitle;
                pc.WindowDescription = windowTitle;

                var view = managementService.AddNewSmartPart<BookView.View>(WorkItem, viewId.ToString(), pc, WorkItem, parameters);

                managementService.Show(pc, WorkItem, view);
            }
        }

        public void OpenResultView()
        {
            var managementService = WorkItem.Services.Get<IWindowsManagerService>();

            Guid viewId = Guid.NewGuid();
            Guid featureId = new Guid("114B8B48-3309-4C7D-B06C-20AF4028E0A2");
            var resultWorkItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

            FeatureContextBase fc = new FeatureContextBase(featureId);
            PresentationContext pc = new PresentationContext(viewId, null, fc);

            if (!managementService.Contains(viewId))
            {
                pc.WindowTitle = Constants.RESULT_FORM_TITLE;
                pc.WindowDescription = Constants.RESULT_FORM_DESCRIPTION;

                var view = managementService.AddNewSmartPart<Selections.ResultView.View>(resultWorkItem, viewId.ToString(), pc, resultWorkItem);

                managementService.Show(pc, resultWorkItem, view);
            }
        }

        public BookViewParameters StartSearch(string inn, string year, string period, BookType bookType)
        {
            var parameters = new BookDownloadParameters { Inn = inn, TaxPeriod = period, TaxYear = year, TypeOfBook = bookType };
            var cacheKey = GetCacheKey(parameters);
            var ceProvider = new SqlCeCacheProvider();
            var downloader = new BookDownloader(base.WorkItem.Services.Get<ISovServiceServiceAgent>());

            Action<object> saveToDbDelegate;

            Type dbMetadataType = null;

            if (bookType == BookType.Buy)
            {
                dbMetadataType = typeof(BuyBookLineDemo);
                saveToDbDelegate = o => ceProvider.AddToCache( cacheKey, o as BuyBookLineDemo);
            }
            else
            {
                dbMetadataType = typeof(SellBookLineDemo);
                saveToDbDelegate = o => ceProvider.AddToCache(cacheKey, o as SellBookLineDemo);
//                downloader.DataDownloadCompletedDelegate = () =>
//                {
//                    ceProvider.CreateIndex(cacheKey, "BROKERINN");
//                    ceProvider.CreateIndex(cacheKey, "SELLERINN");
//                    ceProvider.CreateIndex(cacheKey, "ID");
//                };
            }

            ceProvider.CreateCache(cacheKey, dbMetadataType);
            downloader.ProcessEntitiesToNotify = 300;//TODO: Magic number
            downloader.DownloadAsync(parameters, saveToDbDelegate);

            while (downloader.IsRunning)
            {
                downloader.Wait(TimeSpan.FromSeconds(1));
                if(downloader.ReadedObjects > 100)//TODO: Magic number
                {
                    break; 
                }
            }


            //TODO: Handle errors
            if(!downloader.IsRunning && downloader.ReadedObjects == 0)
            {
                View.ShowMessage("Книга не найдена");
                return null;
            }
            else
            {
                return new BookViewParameters()
                {
                    BookType = bookType,
                    Year = year,
                    Downloader = downloader,
                    INN = inn,
                    Period = period,
                    CacheKey = cacheKey
                };                
            }
        }

        private string GetCacheKey(BookDownloadParameters parameters)
        {
            return "T_" + parameters.Inn + parameters.TaxYear + parameters.TaxPeriod;
        }

       
    }
}
