﻿namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch.Popup
{
    partial class SearchDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this.pnlRoot = new Infragistics.Win.Misc.UltraPanel();
            this.ucYear = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.ucBookType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ucPeriod = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uteKpp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uteInn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ubSearch = new Infragistics.Win.Misc.UltraButton();
            this.lblBookType = new Infragistics.Win.Misc.UltraLabel();
            this.lblPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.lblKpp = new Infragistics.Win.Misc.UltraLabel();
            this.lblInn = new Infragistics.Win.Misc.UltraLabel();
            this.pnlRoot.ClientArea.SuspendLayout();
            this.pnlRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ucYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucBookType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteKpp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteInn)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRoot
            // 
            // 
            // pnlRoot.ClientArea
            // 
            this.pnlRoot.ClientArea.Controls.Add(this.ucYear);
            this.pnlRoot.ClientArea.Controls.Add(this.progressBar1);
            this.pnlRoot.ClientArea.Controls.Add(this.ucBookType);
            this.pnlRoot.ClientArea.Controls.Add(this.ucPeriod);
            this.pnlRoot.ClientArea.Controls.Add(this.uteKpp);
            this.pnlRoot.ClientArea.Controls.Add(this.uteInn);
            this.pnlRoot.ClientArea.Controls.Add(this.ubSearch);
            this.pnlRoot.ClientArea.Controls.Add(this.lblBookType);
            this.pnlRoot.ClientArea.Controls.Add(this.lblPeriod);
            this.pnlRoot.ClientArea.Controls.Add(this.lblKpp);
            this.pnlRoot.ClientArea.Controls.Add(this.lblInn);
            this.pnlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRoot.Location = new System.Drawing.Point(0, 0);
            this.pnlRoot.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRoot.Name = "pnlRoot";
            this.pnlRoot.Size = new System.Drawing.Size(368, 131);
            this.pnlRoot.TabIndex = 0;
            // 
            // ucYear
            // 
            this.ucYear.CheckedListSettings.CheckStateMember = "";
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ucYear.DisplayLayout.Appearance = appearance1;
            ultraGridBand1.ColHeadersVisible = false;
            ultraGridBand1.GroupHeadersVisible = false;
            this.ucYear.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ucYear.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ucYear.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ucYear.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucYear.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ucYear.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucYear.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ucYear.DisplayLayout.MaxColScrollRegions = 1;
            this.ucYear.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ucYear.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ucYear.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ucYear.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ucYear.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ucYear.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ucYear.DisplayLayout.Override.CellAppearance = appearance8;
            this.ucYear.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ucYear.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ucYear.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ucYear.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ucYear.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ucYear.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ucYear.DisplayLayout.Override.RowAppearance = appearance11;
            this.ucYear.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ucYear.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ucYear.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ucYear.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ucYear.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ucYear.Location = new System.Drawing.Point(162, 61);
            this.ucYear.Name = "ucYear";
            this.ucYear.Size = new System.Drawing.Size(89, 22);
            this.ucYear.TabIndex = 12;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 115);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(344, 14);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 10;
            this.progressBar1.Visible = false;
            // 
            // ucBookType
            // 
            this.ucBookType.CheckedListSettings.CheckStateMember = "";
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ucBookType.DisplayLayout.Appearance = appearance13;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridBand2.GroupHeadersVisible = false;
            this.ucBookType.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ucBookType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ucBookType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ucBookType.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucBookType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.ucBookType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucBookType.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.ucBookType.DisplayLayout.MaxColScrollRegions = 1;
            this.ucBookType.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ucBookType.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ucBookType.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.ucBookType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ucBookType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.ucBookType.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ucBookType.DisplayLayout.Override.CellAppearance = appearance20;
            this.ucBookType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ucBookType.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.ucBookType.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance17.TextHAlignAsString = "Left";
            this.ucBookType.DisplayLayout.Override.HeaderAppearance = appearance17;
            this.ucBookType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ucBookType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ucBookType.DisplayLayout.Override.RowAppearance = appearance23;
            this.ucBookType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ucBookType.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.ucBookType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ucBookType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ucBookType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ucBookType.Location = new System.Drawing.Point(113, 86);
            this.ucBookType.Name = "ucBookType";
            this.ucBookType.Size = new System.Drawing.Size(138, 22);
            this.ucBookType.TabIndex = 9;
            // 
            // ucPeriod
            // 
            this.ucPeriod.CheckedListSettings.CheckStateMember = "";
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ucPeriod.DisplayLayout.Appearance = appearance26;
            ultraGridBand3.ColHeadersVisible = false;
            ultraGridBand3.GroupHeadersVisible = false;
            this.ucPeriod.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.ucPeriod.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ucPeriod.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.ucPeriod.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucPeriod.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.ucPeriod.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucPeriod.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.ucPeriod.DisplayLayout.MaxColScrollRegions = 1;
            this.ucPeriod.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ucPeriod.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ucPeriod.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.ucPeriod.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ucPeriod.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.ucPeriod.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            appearance33.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ucPeriod.DisplayLayout.Override.CellAppearance = appearance33;
            this.ucPeriod.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ucPeriod.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.ucPeriod.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance35.TextHAlignAsString = "Left";
            this.ucPeriod.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.ucPeriod.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ucPeriod.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.ucPeriod.DisplayLayout.Override.RowAppearance = appearance36;
            this.ucPeriod.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ucPeriod.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.ucPeriod.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ucPeriod.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ucPeriod.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ucPeriod.Location = new System.Drawing.Point(113, 61);
            this.ucPeriod.Name = "ucPeriod";
            this.ucPeriod.Size = new System.Drawing.Size(43, 22);
            this.ucPeriod.TabIndex = 8;
            // 
            // uteKpp
            // 
            this.uteKpp.Enabled = false;
            this.uteKpp.Location = new System.Drawing.Point(113, 37);
            this.uteKpp.Name = "uteKpp";
            this.uteKpp.Size = new System.Drawing.Size(138, 21);
            this.uteKpp.TabIndex = 6;
            this.uteKpp.Text = "524701001";
            // 
            // uteInn
            // 
            this.uteInn.Location = new System.Drawing.Point(113, 12);
            this.uteInn.Name = "uteInn";
            this.uteInn.Size = new System.Drawing.Size(138, 21);
            this.uteInn.TabIndex = 5;
            this.uteInn.Text = "1061986529";
            // 
            // ubSearch
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.BackColor2 = System.Drawing.Color.Transparent;
            appearance25.ForeColor = System.Drawing.Color.Transparent;
            appearance25.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance25.ImageBackground = global::Luxoft.NDS2.Client.Properties.Resources.edit_find;
            appearance25.ImageBackgroundDisabled = global::Luxoft.NDS2.Client.Properties.Resources.edit_find;
            appearance25.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance25.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance25.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.ubSearch.Appearance = appearance25;
            this.ubSearch.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.ubSearch.Location = new System.Drawing.Point(257, 12);
            this.ubSearch.Name = "ubSearch";
            this.ubSearch.Size = new System.Drawing.Size(97, 96);
            this.ubSearch.TabIndex = 4;
            this.ubSearch.UseAppStyling = false;
            this.ubSearch.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.ubSearch.Click += new System.EventHandler(this.ubSearch_Click);
            // 
            // lblBookType
            // 
            this.lblBookType.Location = new System.Drawing.Point(12, 90);
            this.lblBookType.Name = "lblBookType";
            this.lblBookType.Size = new System.Drawing.Size(92, 19);
            this.lblBookType.TabIndex = 3;
            this.lblBookType.Text = "Тип книги";
            // 
            // lblPeriod
            // 
            this.lblPeriod.Location = new System.Drawing.Point(12, 65);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(103, 19);
            this.lblPeriod.TabIndex = 2;
            this.lblPeriod.Text = "Отчетный период";
            // 
            // lblKpp
            // 
            this.lblKpp.Location = new System.Drawing.Point(12, 40);
            this.lblKpp.Name = "lblKpp";
            this.lblKpp.Size = new System.Drawing.Size(56, 19);
            this.lblKpp.TabIndex = 1;
            this.lblKpp.Text = "КПП НП";
            // 
            // lblInn
            // 
            this.lblInn.Location = new System.Drawing.Point(13, 14);
            this.lblInn.Name = "lblInn";
            this.lblInn.Size = new System.Drawing.Size(56, 19);
            this.lblInn.TabIndex = 0;
            this.lblInn.Text = "ИНН НП";
            // 
            // SearchDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 131);
            this.Controls.Add(this.pnlRoot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SearchDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Поиск книги";
            this.Load += new System.EventHandler(this.SearchDialog_Load);
            this.pnlRoot.ClientArea.ResumeLayout(false);
            this.pnlRoot.ClientArea.PerformLayout();
            this.pnlRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ucYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucBookType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteKpp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteInn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel pnlRoot;
        private Infragistics.Win.UltraWinGrid.UltraCombo ucBookType;
        private Infragistics.Win.UltraWinGrid.UltraCombo ucPeriod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteKpp;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteInn;
        private Infragistics.Win.Misc.UltraButton ubSearch;
        private Infragistics.Win.Misc.UltraLabel lblBookType;
        private Infragistics.Win.Misc.UltraLabel lblPeriod;
        private Infragistics.Win.Misc.UltraLabel lblKpp;
        private Infragistics.Win.Misc.UltraLabel lblInn;
        private System.Windows.Forms.ProgressBar progressBar1;
        private Infragistics.Win.UltraWinGrid.UltraCombo ucYear;
    }
}