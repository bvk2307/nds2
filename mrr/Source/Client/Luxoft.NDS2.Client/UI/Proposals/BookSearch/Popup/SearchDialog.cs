﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Helpers.BookDownload;
using Luxoft.NDS2.Client.Helpers.CacheProviders;
using Luxoft.NDS2.Client.UI.Proposals.BookSearch.CacheSaver;
using Luxoft.NDS2.Client.UI.Proposals.BookView;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
using Luxoft.NDS2.Common.ServiceAgents;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch.Popup
{
    public partial class SearchDialog : Form
    {
        public BookType BookType { get { return ucBookType.SelectedRow.ListObject.ToString().Equals("Книга покупок") ? BookType.Buy : BookType.Sell; } }
        public BookViewParameters BookViewParameters { get; set; }


        private Presenter _presenter;

        public SearchDialog(Presenter presenter)
        {
            _presenter = presenter;
            InitializeComponent();

            ucBookType.DataSource = _presenter.GetBookTypes();
            ucPeriod.DataSource = _presenter.GetQuarters();
            ucYear.DataSource = _presenter.GetYears();
        }

        private void ubSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(uteKpp.Text) || string.IsNullOrEmpty(uteInn.Text))
            {
                MessageBox.Show("Заполнены не все поля поиска", "Сообщение", MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            progressBar1.Visible = true;
            var result = _presenter.StartSearch(uteInn.Text, SelectedYear, SelectedPeriod, SelectedBookType);

            progressBar1.Visible = false;
            if (result != null)
            {
                BookViewParameters = result;
                DialogResult = DialogResult.OK;                                
            }
        }

        public string SelectedInn
        {
            get { return uteInn.Text; }
        }

        public string SelectedKpp
        {
            get { return uteKpp.Text; }
        }

        public string SelectedPeriod
        {
            get { return ucPeriod.SelectedRow.ListObject.ToString(); }
        }

        public string SelectedYear
        {
            get { return ucYear.SelectedRow.ListObject.ToString(); }
        }

        private BookType SelectedBookType
        {
            get
            {
                return ucBookType.SelectedRow.ListObject.ToString().Equals("Книга покупок")
                           ? BookType.Buy
                           : BookType.Sell;
            }
        }


        private void SearchDialog_Load(object sender, EventArgs e)
        {
            ucPeriod.Rows[0].Selected = true;
            ucBookType.Rows[0].Selected = true;
            ucPeriod.Rows[0].Selected = true;
            ucYear.Rows[0].Selected = true;
        }

        private void ShowError(string message, params object[] parameters)
        {
            MessageBox.Show(string.Format(message, parameters));
        }
    }
}
