﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Demo;
using Luxoft.NDS2.Client.UI.Proposals.BookSearch.Popup;
using Luxoft.NDS2.Client.UI.Proposals.BookView;
using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.UI.Proposals.BookSearch
{
    public class View : BaseView
    {
        private Infragistics.Win.Misc.UltraButton btnSearch;

        private PresentationContext presentationContext;
        private Infragistics.Win.Misc.UltraButton btnResult;

        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.View = this;
                _presenter.PresentationContext = presentationContext;
                this.WorkItem = _presenter.WorkItem;
            }
        }

        public View(PresentationContext context)
        {
            presentationContext = context;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.btnSearch = new Infragistics.Win.Misc.UltraButton();
            this.btnResult = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnSearch.Appearance = appearance2;
            this.btnSearch.Location = new System.Drawing.Point(30, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(143, 38);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Работа с книгами";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnResult
            // 
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnResult.Appearance = appearance3;
            this.btnResult.Location = new System.Drawing.Point(30, 80);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(143, 38);
            this.btnResult.TabIndex = 0;
            this.btnResult.Text = "Работа с результатами";
            this.btnResult.Click += new System.EventHandler(this.btnResult_Click);
            // 
            // View
            // 
            this.Controls.Add(this.btnResult);
            this.Controls.Add(this.btnSearch);
            this.Name = "View";
            this.Size = new System.Drawing.Size(716, 515);
            this.ResumeLayout(false);

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchDialog dlg = new SearchDialog(_presenter);
            if(dlg.ShowDialog() == DialogResult.OK)
            {
                _presenter.OpenBookView(dlg.BookViewParameters);
            }
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            _presenter.OpenResultView();
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}
