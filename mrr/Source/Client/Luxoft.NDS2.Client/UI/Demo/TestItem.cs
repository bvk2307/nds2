﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Demo
{
    public class TestItem : ILookupEntry<long>
    {
        [ListDisplaySettings(Title="Имя")]
        public string Name { get; set; }

        [ListDisplaySettings(Title = "ФИО")]
        public string FullName { get; set; }

        public long Key
        {
            get;            
            set;
        }

        public bool IsSelected
        {
            get;
            set;
        }

        public string Title
        {
            get { return Name; }
        }
    }
}
