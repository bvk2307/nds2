﻿using System;
using System.Collections.Generic;
using CommonComponents.Directory;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Demo
{
    public class DemoPresenter : BasePresenter<DemoView>
    {
        public DemoPresenter()
        {
        }

        //DemoPresenter
        public DemoPresenter(PresentationContext presentationContext, WorkItem wi, DemoView view)
            : base(presentationContext, wi, view)
        {
        }

        public string PingAdmin(string messasge)
        {
            var sProxy = GetServiceProxy<ISampleService>();

            return sProxy.PingAdmin(messasge);
        }

        public string PingInspector(string messasge)
        {
            var sProxy = GetServiceProxy<ISampleService>();

            return sProxy.PingInspector(messasge);
        }

        public string PingManager(string messasge)
        {
            var sProxy = GetServiceProxy<ISampleService>();

            return sProxy.PingManager(messasge);
        }

        public int PingDatabase()
        {
            var sProxy = GetServiceProxy<ISampleService>();

            return sProxy.PingDatabase();
        }

        public void HandleException()
        {
            var sProxy = GetServiceProxy<ISampleService>();
            sProxy.RaiseExceptionOnServer();
        }

        public IList<AccessRight> GetMyRigths()
        {
            return Rigths;
        }

        public IList<AccessRight> GetMyOperations()
        {
            return UserOperations;
        }

        public IList<string> GetSubsystemRoles()
        {
            IList<string> result = null;
            if (ExecuteServiceCall(GetServiceProxy<ISecurityService>().GetSubsystemRoles,
                res => { result = res.Result; }))
                return result;

            return new List<string>();
        }

        public IList<string> GetSubsystemAssignments()
        {
            var uis = WorkItem.Services.Get<IUserInfoService>();
            IList<string> result = null;
            if (ExecuteServiceCall(GetServiceProxy<ISecurityService>().GetSubsystemUsers,
                res => { result = res.Result; }))
            {
                var users = new List<string>();
                for (var i = 0; i < result.Count - 1; i++)
                    try
                    {
                        var uinfo = uis.GetUserInfoBySID(result[i]);
                        users.Add(uinfo.DisplayName);
                    }
                    catch (Exception ex)
                    {
                        LogError(ex.Message);
                        users.Add(result[i]);
                    }

                return users;
            }

            return new List<string>();
        }

        public bool ChangeRole(string roleName)
        {
            if (ExecuteServiceCall(() => GetServiceProxy<ISecurityService>().ChangeUserRole(roleName),
                res => { }))
            {
                ClearPermissionCache();
                View.ShowNotification("Выполнено");
                return true;
            }

            return false;
        }

        public DiscrepancyDocumentInfo GetDiscrepancyDocumentInfo(long docId)
        {
            DiscrepancyDocumentInfo ddi = null;
            if (ExecuteServiceCall(() => GetServiceProxy<IDiscrepancyDocumentService>().GetDocument(docId),
                res => { ddi = res.Result; }))
            {
            }
            return ddi;
        }

        public void GetUserAcess(string p)
        {
            UserAccessPolicy rez = null;

            if (ExecuteServiceCall(() => GetServiceProxy<ISecurityService>().GetUserAccess(p),
                res => { rez = res.Result; }))
                View.ShowNotification(rez.ToString());
        }

        public void SendToMC()
        {
            var service = GetServiceProxy<IExplainReplyDataService>();
            var dataService = GetServiceProxy<IDataService>();
            var logService = GetServiceProxy<ILogProvider>();

            var clarifications = dataService.GetShortClarifications();

            foreach (var clarification in clarifications.Result)
                try
                {
                    var explainId = Convert.ToInt64(clarification.EXPLAIN_ID);
                    var zip = Convert.ToInt64(clarification.ZIP);
                    service.SendExplainXMLtoMC(explainId, zip);
                }
                catch (Exception e)
                {
                    logService.LogError(e.Message, "ResendToMC", e);
                }
        }

        public List<string> GetInspectors(string sonoCode)
        {
            var list = new List<string>();

            if (ExecuteServiceCall(() => GetServiceProxy<IUserInformationService>().GetInspectorListBySono(sonoCode),
                res => { res.Result.ForEach(x => list.Add(string.Format("{0} - {1}", x.Name, x.EmployeeNum))); }))
            {
            }
            return list;
        }
    }
}