﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Demo
{
    public partial class DemoView : BaseView
    {

        private PresentationContext presentationContext;

        private DemoPresenter _presenter;

        [CreateNew]
        public DemoPresenter Presenter
        {
            set 
            { 
                _presenter = value;
                _presenter.View = this;
                _presenter.PresentationContext = presentationContext;

                WorkItem = _presenter.WorkItem;
            }
        }

        public DemoView(PresentationContext context) : base(context)
        {
            this.presentationContext = context;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
//            Presenter.ViewDiscrepancyDetails();
//            CardOpenner.OpenDiscrepancyCard(_presenter.WorkItem, 1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_presenter.PingInspector("hello"));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_presenter.PingManager( "hello"));
        }

        private void btnPingDatabase_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_presenter.PingDatabase().ToString());
        }

        private void btnExceptionHandling_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.HandleException();
            }
            catch (Exception ex)
            {
                _presenter.LogError(ex.Message, null, ex);
                MessageBox.Show("Произошла ошибка на сервере");
            }
        }

        private void btnGarbageCollection_Click( object sender, EventArgs e )
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void DemoView_Load(object sender, EventArgs e)
        {
            cmbRoles.DataSource = _presenter.GetSubsystemRoles();
            lbMyPermissions.DataSource = _presenter.GetMyRigths();
            //lbAssignments.DataSource = _presenter.GetSubsystemAssignments();
        }

        private void btnChangeRole_Click(object sender, EventArgs e)
        {
            if (_presenter.ChangeRole(cmbRoles.SelectedItem.ToString()))
            {
                lbMyPermissions.DataSource = _presenter.GetMyRigths();
            }
        }

        private void button4Click(object sender, EventArgs e)
        {
            long explainReplayId = Convert.ToInt64(textBox1.Text);
				var explainCardData = _presenter.GetExplainCardRcd(explainReplayId);
				_presenter.ViewExplainReplyDetails(explainCardData);
        }

        private void button5Click(object sender, EventArgs e)
        {
            _presenter.GetUserAcess(textBox1.Text);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.ShowQuestion("Выбор", "Вы уверены ?");
            this.ShowInfo("шапка", "текст");
        }

        private void btnResendClarifications_Click(object sender, EventArgs e)
        {
            _presenter.SendToMC();
        }
    }
}
