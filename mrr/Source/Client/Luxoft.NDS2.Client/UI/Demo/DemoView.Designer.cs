﻿namespace Luxoft.NDS2.Client.UI.Demo
{
    partial class DemoView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnPingDatabase = new System.Windows.Forms.Button();
            this.btnExceptionHandling = new System.Windows.Forms.Button();
            this.btnGarbageCollection = new System.Windows.Forms.Button();
            this.lbMyPermissions = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbRoles = new System.Windows.Forms.ComboBox();
            this.btnChangeRole = new System.Windows.Forms.Button();
            this.lbAssignments = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.btnResendClarifications = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(20, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Admin ping";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(20, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Inspector ping";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(20, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(138, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Manager ping";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnPingDatabase
            // 
            this.btnPingDatabase.Location = new System.Drawing.Point(20, 99);
            this.btnPingDatabase.Name = "btnPingDatabase";
            this.btnPingDatabase.Size = new System.Drawing.Size(138, 23);
            this.btnPingDatabase.TabIndex = 3;
            this.btnPingDatabase.Text = "Ping Db";
            this.btnPingDatabase.UseVisualStyleBackColor = true;
            this.btnPingDatabase.Click += new System.EventHandler(this.btnPingDatabase_Click);
            // 
            // btnExceptionHandling
            // 
            this.btnExceptionHandling.Location = new System.Drawing.Point(20, 128);
            this.btnExceptionHandling.Name = "btnExceptionHandling";
            this.btnExceptionHandling.Size = new System.Drawing.Size(138, 23);
            this.btnExceptionHandling.TabIndex = 4;
            this.btnExceptionHandling.Text = "Handle an exception";
            this.btnExceptionHandling.UseVisualStyleBackColor = true;
            this.btnExceptionHandling.Click += new System.EventHandler(this.btnExceptionHandling_Click);
            // 
            // btnGarbageCollection
            // 
            this.btnGarbageCollection.Location = new System.Drawing.Point(20, 157);
            this.btnGarbageCollection.Name = "btnGarbageCollection";
            this.btnGarbageCollection.Size = new System.Drawing.Size(138, 23);
            this.btnGarbageCollection.TabIndex = 5;
            this.btnGarbageCollection.Text = "Garbage Collection";
            this.btnGarbageCollection.UseVisualStyleBackColor = true;
            this.btnGarbageCollection.Click += new System.EventHandler(this.btnGarbageCollection_Click);
            // 
            // lbMyPermissions
            // 
            this.lbMyPermissions.FormattingEnabled = true;
            this.lbMyPermissions.Location = new System.Drawing.Point(206, 27);
            this.lbMyPermissions.Margin = new System.Windows.Forms.Padding(2);
            this.lbMyPermissions.Name = "lbMyPermissions";
            this.lbMyPermissions.Size = new System.Drawing.Size(155, 225);
            this.lbMyPermissions.TabIndex = 6;
            this.lbMyPermissions.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(203, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Мои права";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(361, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Сменить роль";
            this.label2.Visible = false;
            // 
            // cmbRoles
            // 
            this.cmbRoles.FormattingEnabled = true;
            this.cmbRoles.Location = new System.Drawing.Point(364, 28);
            this.cmbRoles.Name = "cmbRoles";
            this.cmbRoles.Size = new System.Drawing.Size(121, 21);
            this.cmbRoles.TabIndex = 10;
            this.cmbRoles.Visible = false;
            // 
            // btnChangeRole
            // 
            this.btnChangeRole.Location = new System.Drawing.Point(489, 26);
            this.btnChangeRole.Name = "btnChangeRole";
            this.btnChangeRole.Size = new System.Drawing.Size(75, 23);
            this.btnChangeRole.TabIndex = 11;
            this.btnChangeRole.Text = "сменить";
            this.btnChangeRole.UseVisualStyleBackColor = true;
            this.btnChangeRole.Visible = false;
            this.btnChangeRole.Click += new System.EventHandler(this.btnChangeRole_Click);
            // 
            // lbAssignments
            // 
            this.lbAssignments.FormattingEnabled = true;
            this.lbAssignments.Location = new System.Drawing.Point(576, 27);
            this.lbAssignments.Margin = new System.Windows.Forms.Padding(2);
            this.lbAssignments.Name = "lbAssignments";
            this.lbAssignments.Size = new System.Drawing.Size(334, 225);
            this.lbAssignments.TabIndex = 12;
            this.lbAssignments.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Location = new System.Drawing.Point(206, 299);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(704, 167);
            this.panel1.TabIndex = 13;
            this.panel1.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(7, 115);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(675, 41);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(678, 43);
            this.textBox1.TabIndex = 1;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(161, 61);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(148, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "Get UserAcess for operation";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(7, 61);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(148, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Open ExplainReplyCard";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(213, 483);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(120, 23);
            this.button10.TabIndex = 0;
            this.button10.Text = "test MessageBox";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnResendClarifications
            // 
            this.btnResendClarifications.Location = new System.Drawing.Point(20, 186);
            this.btnResendClarifications.Name = "btnResendClarifications";
            this.btnResendClarifications.Size = new System.Drawing.Size(138, 23);
            this.btnResendClarifications.TabIndex = 15;
            this.btnResendClarifications.Text = "Resend notes to MC";
            this.btnResendClarifications.UseVisualStyleBackColor = true;
            this.btnResendClarifications.Click += new System.EventHandler(this.btnResendClarifications_Click);
            // 
            // DemoView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnResendClarifications);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbAssignments);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.btnChangeRole);
            this.Controls.Add(this.cmbRoles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMyPermissions);
            this.Controls.Add(this.btnGarbageCollection);
            this.Controls.Add(this.btnExceptionHandling);
            this.Controls.Add(this.btnPingDatabase);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "DemoView";
            this.Size = new System.Drawing.Size(963, 551);
            this.Load += new System.EventHandler(this.DemoView_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnPingDatabase;
        private System.Windows.Forms.Button btnExceptionHandling;
        private System.Windows.Forms.Button btnGarbageCollection;
        private System.Windows.Forms.ListBox lbMyPermissions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbRoles;
        private System.Windows.Forms.Button btnChangeRole;
        private System.Windows.Forms.ListBox lbAssignments;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnResendClarifications;
        private System.Windows.Forms.Button button10;
    }
}
