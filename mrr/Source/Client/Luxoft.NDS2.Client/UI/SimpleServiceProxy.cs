﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Threading;

namespace Luxoft.NDS2.Client.UI
{
    public class SimpleServiceProxy : ServiceProxyBase
    {
        public SimpleServiceProxy(INotifier notifier, IClientLogger logger)
            : base(notifier, logger)
        {
        }

        public CancellationTokenSource BeginInvoke(Func<OperationResult> action)
        {
            return BeginInvoke<OperationResult>(action, result => { });
        }

        public CancellationTokenSource BeginInvoke(Func<OperationResult> action, Action<OperationResult> result)
        {
            return BeginInvoke<OperationResult>(action, result);
        }
    }

    public static class SimpleServiceProxyHelper
    {
        public static CancellationTokenSource DoAsync(
            this INotifier notifier, 
            IClientLogger logger, 
            Func<OperationResult> serviceMethod)
        {
            var proxy = new SimpleServiceProxy(notifier, logger);

            return proxy.BeginInvoke(serviceMethod);
        }
    }
}
