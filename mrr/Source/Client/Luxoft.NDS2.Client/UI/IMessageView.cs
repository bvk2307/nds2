using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI
{
    public interface IMessageView
    {
        DialogResult ShowQuestion(string caption, string message);

        void ShowInfo(string caption, string message);
    }
}