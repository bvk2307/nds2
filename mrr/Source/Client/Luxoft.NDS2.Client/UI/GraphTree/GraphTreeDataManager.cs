﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    /// <summary> A data manager of <see cref="Presenter"/>. </summary>
    public class GraphTreeDataManager : ServiceCallDataManager, IGraphTreeDataManager
    {
        private const int CallDispatcherInputBoundCapacity      = 1;
        private const int CallDispatcherLowInputBoundCapacity   = 1000;

        private readonly int MaxParallelBatchServiceRequestCount = ServiceCallExecutorManagerFactory.MaxExecutorNumber;

        private readonly IPyramidDataService _pyramidDataService;

        public GraphTreeDataManager( IServiceCallDispatcherFactory serviceCallDispatcherFactory, IServiceCallWrapper serviceCallWrapper, 
            IExecutorWErrorHandling executorWErrorHandling, IPyramidDataService pyramidDataService, IClientLogger clientLogger )
            : base( serviceCallDispatcherFactory, serviceCallWrapper, executorWErrorHandling, clientLogger )
        {
            _pyramidDataService = pyramidDataService;
        }

        /// <summary> Creates a new service call dispatcher connected to the pool of client executors for graph tree nodes. </summary>
        /// <param name="dataTransofrmHandler"> A data tranform hadnler. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="cancelSource"> </param>
        /// <param name="faultOrCancelHandler"> An error or cancellation handler. Optional. Does not used if it is not defined. </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new service call dispatcher created. </returns>
        public IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
            InitNodesDataDispatcher( 
                Func<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>, 
                     CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>> 
                    dataTransofrmHandler, 
                Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken> 
                    dataLoadedHandler, 
                CancellationTokenSource cancelSource = null, Action<Task> faultOrCancelHandler = null, string postProcessNameFormat = null )
        {
            Func<GraphNodesKeyParameters, CancellationToken, IReadOnlyCollection<GraphData>> dataLoadHandler =
	//apopov 23.5.2016	//DEBUG!!!
                ( keyParamsPar, cancelTokenPar ) => GetNodeDatas( keyParamsPar, cancelTokenPar, cancelSource );
                //( keyParamsPar, cancelTokenPar ) => GetBatchNodeDatas( keyParamsPar, cancelTokenPar, cancelSource );

            return base.InitServiceCallDispatcher( 
                new ServiceCallDispatcherOptions
                { CancelSource = cancelSource, InputBoundCapacity = CallDispatcherInputBoundCapacity,
                    LowInputBoundCapacity = CallDispatcherLowInputBoundCapacity, PostprocessNameFormat = postProcessNameFormat }, 
                dataTransofrmHandler, dataLoadHandler, dataLoadedHandler, faultOrCancelHandler );
        }

        /// <summary> Creates a new batch service call dispatcher connected to the pool of client executors for graph tree nodes. </summary>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="finishStepHandler"> A handler called on the dispatcher finish step before completion. </param>
        /// <param name="onFinishHandler"> A final handler called after finish step <paramref name="finishStepHandler"/>. It is called in the main UI thread. </param>
        /// <param name="layersDepthLimit"> A maximum depth layer number where the root node has depth 0. </param>
        /// <param name="cancelSource"> </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new batch service call dispatcher created. </returns>
        public IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string> 
            InitServiceCallBatchDispatcher( 
                Action<CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>, CancellationToken> dataLoadedHandler, 
                Func<CancellationToken, Exception, string> finishStepHandler, 
                Action<string, Exception, CancellationToken, CancellationToken> onFinishHandler, 
                int layersDepthLimit, CancellationTokenSource cancelSource = null, string postProcessNameFormat = null )
        {
            Func<GraphNodesKeyParameters, CancellationToken, IEnumerable<GraphData>> dataLoadHandler =
                ( keyParamsPar, cancelTokenPar ) => GetBatchNodeDatas( keyParamsPar, cancelTokenPar, cancelSource );

            var dataLoadStrategyOptions = new BatchDataLoadStrategyOptions( 
                maxLevel: layersDepthLimit, maxParallelRequestCount: MaxParallelBatchServiceRequestCount );

            return base.InitServiceCallBatchDispatcher( 
                new ServiceCallBatchDispatcherOptions( TaskScheduler.Default ) { CancelSource = cancelSource, InputBoundCapacity = 256, PostprocessNameFormat = postProcessNameFormat }, 
                dataLoadHandler, dataLoadStrategy: new GraphTreeReportBatchDataManager( dataLoadStrategyOptions ), 
                dataLoadedHandler: dataLoadedHandler, finishStepHandler: finishStepHandler, onFinishHandler: onFinishHandler );
        }

        /// <summary> Creates a new service call dispatcher connected to the pool of client executors for tax payer deduction sums. </summary>
        /// <param name="dataTransofrmHandler"> A data tranform hadnler. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="cancelSource"> </param>
        /// <param name="faultOrCancelHandler"> An error or cancellation handler. Optional. Does not used if it is not defined. </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new service call dispatcher created. </returns>
        public IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> 
            InitDeductionDetailsDataDispatcher( 
                Func<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, 
                     CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>> 
                     dataTransofrmHandler, 
                Action<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, CancellationToken> dataLoadedHandler, 
                CancellationTokenSource cancelSource = null, Action<Task> faultOrCancelHandler = null, string postProcessNameFormat = null )
            {
                Func<DeductionDetailsKeyParameters, CancellationToken, IReadOnlyCollection<DeductionDetail>> dataLoadHandler =
                    ( keyParamsPar, cancelTokenPar ) => GetDeductionDetailDatas( keyParamsPar, cancelTokenPar, cancelSource );

                return base.InitServiceCallDispatcher( 
                    new ServiceCallDispatcherOptions { CancelSource = cancelSource, InputBoundCapacity = CallDispatcherInputBoundCapacity,
                        LowInputBoundCapacity = CallDispatcherLowInputBoundCapacity, PostprocessNameFormat = postProcessNameFormat }, 
                    dataTransofrmHandler, dataLoadHandler, dataLoadedHandler, faultOrCancelHandler );
            }

        /// <summary> Posts a request on graph tree nodes to <paramref name="nodeDataDispatcher"/>. A result will be processed in the data loaded handler initilized in 
        /// <see cref="GraphTreeDataManager.InitServiceCallDispatcher(ServiceCallDispatcherOptions, Func{GraphNodesKeyParameters, CancellationToken, List{GraphData}}, Action{CallExecContext{GraphNodesKeyParameters, int, string, List{GraphData}}, CancellationToken, Action{Task})"/>. </summary>
        /// <param name="nodeDataDispatcher"> A service call dispatcher to request. </param>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        /// <param name="priority"> A request execution priority. </param>
        /// <returns> A request result. </returns>
        public bool PostTo( 
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
                nodeDataDispatcher, 
            GraphNodesKeyParameters keyParameters, int instanceKey, Guid contextKey, CallExecPriority priority = CallExecPriority.Normal )
        {
            return base.PostTo( nodeDataDispatcher, keyParameters, instanceKey, contextKey, priority );
        }

        /// <summary> Posts a request on tax payer deduction sums to <paramref name="nodeDataDispatcher"/>. A result will be processed in the data loaded handler initilized in 
        /// <see cref="GraphTreeDataManager.InitServiceCallDispatcher(ServiceCallDispatcherOptions, Func{DeductionDetailsKeyParameters, CancellationToken, List{DeductionDetail}}, Action{CallExecContext{DeductionDetailsKeyParameters, int, string, List{DeductionDetail}}, CancellationToken, Action{Task})"/>. </summary>
        /// <param name="nodeDataDispatcher"> A service call dispatcher to request. </param>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        /// <param name="priority"> A request execution priority. </param>
        /// <returns> A request result. </returns>
        public bool PostTo( IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> nodeDataDispatcher, 
                            DeductionDetailsKeyParameters keyParameters, int instanceKey, Guid contextKey, CallExecPriority priority = CallExecPriority.Normal )
        {
            return base.PostTo( nodeDataDispatcher, keyParameters, instanceKey, contextKey, priority );
        }

        /// <summary> Gets nodes from the server. Executes in background thread. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        private IReadOnlyCollection<GraphData> GetNodeDatas( GraphNodesKeyParameters nodesKeyParameters, 
            CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<GraphData>>() != null );

            IReadOnlyCollection<GraphData> result = null;
            if ( !cancelToken.IsCancellationRequested )
            {
                OperationResult<IReadOnlyCollection<GraphData>> resultSet = ServiceCallWrapper.ExecuteServiceCall( 
                                cancelTokenPar => _pyramidDataService.LoadGraphNodes( nodesKeyParameters ), cancelToken );
                if ( resultSet.Status == ResultStatus.Success )
                    result = resultSet.Result.ToReadOnly();
                else if ( cancelSource != null )   //here the error has been already logged and shown to user inside ExecuteServiceCallDirectly()
                    cancelSource.Cancel();
            }
            return result ?? new GraphData[0].ToReadOnly();
        }

        /// <summary> Gets nodes of a batch from the server. Executes in background thread. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        private IReadOnlyCollection<GraphData> GetBatchNodeDatas( GraphNodesKeyParameters nodesKeyParameters, 
            CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<GraphData>>() != null );

            IReadOnlyCollection<GraphData> result = null;
            if ( !cancelToken.IsCancellationRequested )
            {
                OperationResult<IReadOnlyCollection<GraphData>> resultSet = ServiceCallWrapper.ExecuteServiceCall( 
                                cancelTokenPar => _pyramidDataService.LoadReportGraphNodes( nodesKeyParameters ), cancelToken );
                if ( resultSet.Status == ResultStatus.Success )
                    result = resultSet.Result.ToReadOnly();
                else if ( cancelSource != null )   //here the error has been already logged and shown to user inside ExecuteServiceCallDirectly()
                    cancelSource.Cancel();
            }
            return result ?? new GraphData[0].ToReadOnly();
        }

        /// <summary> Gets tax payer deduction sums from the server. Executes in background thread. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        private IReadOnlyCollection<DeductionDetail> GetDeductionDetailDatas( DeductionDetailsKeyParameters deductionKeyParameters, 
            CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null )
        {
            Contract.Requires( deductionKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<DeductionDetail>>() != null );

            IReadOnlyCollection<DeductionDetail> result = null;
            if ( !cancelToken.IsCancellationRequested )
            {
                OperationResult<IReadOnlyCollection<DeductionDetail>> resultSet = ServiceCallWrapper.ExecuteServiceCall( 
                                cancelTokenPar => _pyramidDataService.LoadDeductionDetails( deductionKeyParameters ), cancelToken );
                if ( resultSet.Status == ResultStatus.Success )
                    result = resultSet.Result.ToReadOnly();
                else if ( cancelSource != null )   //here the error has been already logged and shown to user inside ExecuteServiceCallDirectly()
                    cancelSource.Cancel();
            }
            return result ?? new DeductionDetail[0].ToReadOnly();
        }

        /// <summary> Gets taxpayers kpps by inn. </summary>
        /// <param name="inn"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        public IReadOnlyCollection<string> GetTaxpayersKpp(string inn, CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null)
        {
            IReadOnlyCollection<string> result = null;
            if (!cancelToken.IsCancellationRequested)
            {
                OperationResult<IReadOnlyCollection<string>> resultSet = ServiceCallWrapper.ExecuteServiceCall(
                                cancelTokenPar => _pyramidDataService.LoadTaxpayersKpp(inn), cancelToken);
                if (resultSet.Status == ResultStatus.Success)
                    result = resultSet.Result.ToReadOnly();
                else if (cancelSource != null)   //here the error has been already logged and shown to user inside ExecuteServiceCallDirectly()
                    cancelSource.Cancel();
            }
            return result ?? new string[0].ToReadOnly();
        }
    }
}