﻿using System;
using System.Collections.Generic;
using System.Linq;

using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    public class GraphTreeContainerData
    {
        private const int maxNodesInLevel = 5;
        private const int mostImportantContractors = -1;
        private const int layersLimit = 2;
        private const decimal sharedNdsPercent = 1;
        
        public long? Id { get; private set; }
        public string Inn { get; private set; }
        public string Name { get; private set; }
        public int TaxYear { get; private set; }
        public int TaxQuarter { get; private set; }
        public bool IsPurchase { get; private set; }
        public string ErrorMessage { get; private set; }
        public string WarningMessage { get; private set; }

        public int MaxNodesInLevel
        {
            get { return maxNodesInLevel; }
        }

        public int MostImportantContractors
        {
            get { return mostImportantContractors; }
        }

        public int LayersLimit
        {
            get { return layersLimit; }
        }

        public decimal SharedNdsPercent
        {
            get { return sharedNdsPercent; }
        }
        
        public GraphTreeContainerData(DeclarationSummary dt)
        {
            Id = dt.ID;
            Inn = dt.INN;
            Name = dt.NAME;
            CalculatePeriod(dt.FULL_TAX_PERIOD);
            IsPurchase = dt.COMPENSATION_AMNT < 0;

            CreateMessges(dt.LOAD_MARK, dt.ProcessingStage);
        }

        public GraphTreeContainerData(DeclarationBrief dt)
        {
            Id = dt.ID;
            Inn = dt.INN;
            Name = dt.NAME;
            CalculatePeriod(dt.FULL_TAX_PERIOD);
            IsPurchase = dt.COMPENSATION_AMNT.HasValue 
                && dt.COMPENSATION_AMNT.Value < 0;

            CreateMessges(dt.LOAD_MARK, dt.ProcessingStage);
        }

        public GraphTreeContainerData(string inn, int year, int quarter, bool isPurchase)
        {
            Inn = inn;
            IsPurchase = isPurchase;
            TaxYear = year;
            TaxQuarter = quarter;
        }

        public GraphTreeContainerData()
        {
        }

        private void CalculatePeriod(string period)
        {
            try
            {
                TaxYear = Convert.ToInt32(period.Split(' ').Last());
            }
            catch (Exception)
            {
                TaxYear = DateTime.Now.Year;
            }

            try
            {
                TaxQuarter = Convert.ToInt32(period.Split(' ').First());
            }
            catch (Exception)
            {
                TaxQuarter = (DateTime.Now.Month - 1) / 3 + 1;
            }
        }

        private void CreateMessges(DeclarationProcessignStage allRevisionStage, DeclarationProcessignStage currentRevisionStage)
        {
            string valueBuffer;

            ErrorMessage = String.Empty;
            WarningMessage = String.Empty;

            var item = Tuple.Create(allRevisionStage, currentRevisionStage);
            
            var errorMessagesDictionary =
            new Dictionary<Tuple<DeclarationProcessignStage, DeclarationProcessignStage>, string>()
                {
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.Seod, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY },
                    { Tuple.Create(DeclarationProcessignStage.LoadedInMs, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.UNABLE_TO_OPEN_RELATION_TREE_NO_READY }
                };

            if (errorMessagesDictionary.TryGetValue(item, out valueBuffer))
                ErrorMessage = valueBuffer;
               


            var warningMessagesDictionary =
            new Dictionary<Tuple<DeclarationProcessignStage, DeclarationProcessignStage>, string>()
                {
                    { Tuple.Create(DeclarationProcessignStage.ProcessedByMs, DeclarationProcessignStage.Seod), ResourceManagerNDS2.DeclarationMessages.OPEN_RELATION_TREE_PARTIAL_NO_DATA },
                    { Tuple.Create(DeclarationProcessignStage.ProcessedByMs, DeclarationProcessignStage.LoadedInMs), ResourceManagerNDS2.DeclarationMessages.OPEN_RELATION_TREE_PARTIAL_NO_READY }
                };


            if (warningMessagesDictionary.TryGetValue(item, out valueBuffer))
                WarningMessage = valueBuffer;
        }
    }
}
