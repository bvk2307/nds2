﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    public interface IPresenter
    {
        TaxPeriodModel TaxPeriodModel { get; }
        
        IDeclarationUserPermissionsPolicy UserPermissionsPolicy { get; }

        DictionarySur Sur { get; }

        /// <summary> Parameters of the sheet with tree graph. </summary>
        GraphTreeParameters TreeTabParameters { get; }

        /// <summary> Parameters of the sheet with tree table. </summary>
        GraphTreeParameters TableTabParameters { get; }

        void InitActions();

        void StartLoadTree( GraphTreeParameters treeParameters, IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> surs, bool forceCheckDeclarationExist = false );

        /// <summary> Stops to process operations denedening on data. </summary>
        /// <param name="clearTreeNodes"> 'true' to clear graph tree. </param>
        void StopDataLoadingAndClear( bool clearTreeNodes = false );

        GraphTreeParameters InitializeTableTab( GraphTreeParameters treeParameters, TaxPayerId taxPayerId, int year, int quarter, bool isPurchase );

        void ViewPyramidReport( GraphTreeParameters treeParameters, TaxPayerId taxPayerId, bool isByPurchase, int year, int qrt );
        
        GridSetup CreateGridSetup(string key);

        void OpenDeclarationCard(string inn, string kpp, int year, int period, bool direction);

        void StartGenerateTableExcelAsync(string filePath, GridControl dataGrid, GraphTreeNodeReportData treeTableData);

        void StartGenerateAllTreeExcelAsync( string filePath, GraphTreeNodeReportData treeReportData );

        string GenerateAllTreeExcelFileName( GraphTreeNodeReportData treeTableData );

        string GenerateTableExcelFileName(GraphTreeNodeReportData treeTableData);

        void CancelTableExportToExcel();
    }
}
