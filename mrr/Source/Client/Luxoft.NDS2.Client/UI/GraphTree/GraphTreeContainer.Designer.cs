﻿#define NOKPPLIST

namespace Luxoft.NDS2.Client.UI.GraphTree
{ 
    partial class GraphTreeContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraLabel innLabel;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel countLabel;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel percentLabel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.treeTabPageControl = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.treePanel = new Infragistics.Win.Misc.UltraPanel();
            this.filterPanel = new Infragistics.Win.Misc.UltraPanel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.printButton = new Infragistics.Win.Misc.UltraButton();
            this.reloadButton = new Infragistics.Win.Misc.UltraButton();
            this.excelAllButton = new Infragistics.Win.Misc.UltraButton();
            this.excelVisibleButton = new Infragistics.Win.Misc.UltraButton();
            this.panelFilters = new System.Windows.Forms.Panel();
#if !NOKPPLIST
            this.lblKPP = new Infragistics.Win.Misc.UltraLabel();
            this.comboKpp = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
#endif //!NOKPPLIST
            this.showRevertedCheckbox = new System.Windows.Forms.CheckBox();
            this.percentEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.comboTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.countEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.innEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.labelTaxPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.optionSetByPurchase = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.tableTabPageControl = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGridPanel = new Infragistics.Win.Misc.UltraPanel();
            this.groupBoxTableData = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.groupBoxTableDataPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.lbNdsPrcVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbClacNdsVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbDeductionNdsVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbLevelVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbIsBiggestVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbInspectionVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbRegionVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbIsPurchase = new Infragistics.Win.Misc.UltraLabel();
            this.lbIsPurchaseVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbSurVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbNameVal = new Infragistics.Win.Misc.UltraLabel();
            this.lbInnVal = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.lbDeductionNds = new Infragistics.Win.Misc.UltraLabel();
            this.lbNdsPrc = new Infragistics.Win.Misc.UltraLabel();
            this.lbClacNds = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelReportType = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelND = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelBigest = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelSur = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelinspection = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelInn = new Infragistics.Win.Misc.UltraLabel();
            this._saveFileDialogReport = new System.Windows.Forms.SaveFileDialog();
            this.treeTabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.tabTree = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.toolTipControl = new System.Windows.Forms.ToolTip(this.components);
            innLabel = new Infragistics.Win.Misc.UltraLabel();
            countLabel = new Infragistics.Win.Misc.UltraLabel();
            percentLabel = new Infragistics.Win.Misc.UltraLabel();
            this.treeTabPageControl.SuspendLayout();
            this.treePanel.SuspendLayout();
            this.filterPanel.ClientArea.SuspendLayout();
            this.filterPanel.SuspendLayout();
            this.panelButtons.SuspendLayout();
            this.panelFilters.SuspendLayout();
#if !NOKPPLIST
            ((System.ComponentModel.ISupportInitialize)(this.comboKpp)).BeginInit();
#endif //!NOKPPLIST
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optionSetByPurchase)).BeginInit();
            this.tableTabPageControl.SuspendLayout();
            this.ultraGridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTableData)).BeginInit();
            this.groupBoxTableData.SuspendLayout();
            this.groupBoxTableDataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeTabControl)).BeginInit();
            this.treeTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // innLabel
            // 
            appearance7.TextVAlignAsString = "Bottom";
            innLabel.Appearance = appearance7;
            innLabel.Location = new System.Drawing.Point(16, 10);
            innLabel.Name = "innLabel";
            innLabel.Padding = new System.Drawing.Size(0, 5);
            innLabel.Size = new System.Drawing.Size(35, 24);
            innLabel.TabIndex = 29;
            innLabel.Text = "ИНН:";
            // 
            // countLabel
            // 
            appearance2.TextVAlignAsString = "Bottom";
            countLabel.Appearance = appearance2;
            countLabel.Location = new System.Drawing.Point(325, 30);
            countLabel.Name = "countLabel";
            countLabel.Padding = new System.Drawing.Size(0, 5);
            countLabel.Size = new System.Drawing.Size(222, 34);
            countLabel.TabIndex = 20;
            countLabel.Text = "Количество видимых элементов:";
            // 
            // percentLabel
            // 
            appearance1.TextVAlignAsString = "Bottom";
            percentLabel.Appearance = appearance1;
            percentLabel.Location = new System.Drawing.Point(325, -2);
            percentLabel.Name = "percentLabel";
            percentLabel.Padding = new System.Drawing.Size(0, 5);
            percentLabel.Size = new System.Drawing.Size(219, 36);
            percentLabel.TabIndex = 18;
            percentLabel.Text = "Порог отбора контрагентов по НДС (%):";
            // 
            // treeTabPageControl
            // 
            this.treeTabPageControl.Controls.Add(this.treePanel);
            this.treeTabPageControl.Controls.Add(this.filterPanel);
            this.treeTabPageControl.Location = new System.Drawing.Point(1, 23);
            this.treeTabPageControl.Name = "treeTabPageControl";
            this.treeTabPageControl.Size = new System.Drawing.Size(1495, 637);
            // 
            // treePanel
            // 
            this.treePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treePanel.Location = new System.Drawing.Point(0, 100);
            this.treePanel.Name = "treePanel";
            this.treePanel.Size = new System.Drawing.Size(1495, 537);
            this.treePanel.TabIndex = 1;
            // 
            // filterPanel
            // 
            // 
            // filterPanel.ClientArea
            // 
            this.filterPanel.ClientArea.Controls.Add(this.panelButtons);
            this.filterPanel.ClientArea.Controls.Add(this.panelFilters);
            this.filterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.filterPanel.Location = new System.Drawing.Point(0, 0);
            this.filterPanel.Name = "filterPanel";
            this.filterPanel.Size = new System.Drawing.Size(1495, 100);
            this.filterPanel.TabIndex = 0;
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.printButton);
            this.panelButtons.Controls.Add(this.reloadButton);
            this.panelButtons.Controls.Add(this.excelAllButton);
            this.panelButtons.Controls.Add(this.excelVisibleButton);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelButtons.Location = new System.Drawing.Point(630, 0);
            this.panelButtons.MinimumSize = new System.Drawing.Size(140, 73);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(865, 100);
            this.panelButtons.TabIndex = 34;
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.printButton.Enabled = false;
            this.printButton.Location = new System.Drawing.Point(535, 9);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(155, 24);
            this.printButton.TabIndex = 36;
            this.printButton.Text = "Печать";
            this.printButton.Click += new System.EventHandler(this.PrintButtonClick);
            // 
            // reloadButton
            // 
            this.reloadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadButton.Location = new System.Drawing.Point(696, 9);
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.Size = new System.Drawing.Size(155, 24);
            this.reloadButton.TabIndex = 22;
            this.reloadButton.Text = "Построить";
            this.reloadButton.Click += new System.EventHandler(this.ReloadButtonClick);
            // 
            // excelAllButton
            // 
            this.excelAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelAllButton.Enabled = false;
            this.excelAllButton.Location = new System.Drawing.Point(535, 40);
            this.excelAllButton.Name = "excelAllButton";
            this.excelAllButton.Size = new System.Drawing.Size(155, 24);
            this.excelAllButton.TabIndex = 35;
            this.excelAllButton.Text = "Выгрузить всё";
            this.excelAllButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AllTreeExcelButtonClick);
            // 
            // excelVisibleButton
            // 
            this.excelVisibleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelVisibleButton.Enabled = false;
            this.excelVisibleButton.Location = new System.Drawing.Point(696, 40);
            this.excelVisibleButton.Name = "excelVisibleButton";
            this.excelVisibleButton.Size = new System.Drawing.Size(155, 24);
            this.excelVisibleButton.TabIndex = 31;
            this.excelVisibleButton.Text = "Выгрузить открытые узлы";
            this.excelVisibleButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisibleTreeExcelButtonClick);
            // 
            // panelFilters
            // 

#if !NOKPPLIST
            this.panelFilters.Controls.Add(this.lblKPP);
            this.panelFilters.Controls.Add(this.comboKpp);
#endif //!NOKPPLIST

            this.panelFilters.Controls.Add(this.showRevertedCheckbox);
            this.panelFilters.Controls.Add(percentLabel);
            this.panelFilters.Controls.Add(this.percentEditor);
            this.panelFilters.Controls.Add(innLabel);
            this.panelFilters.Controls.Add(countLabel);
            this.panelFilters.Controls.Add(this.comboTaxPeriod);
            this.panelFilters.Controls.Add(this.countEditor);
            this.panelFilters.Controls.Add(this.innEditor);
            this.panelFilters.Controls.Add(this.labelTaxPeriod);
            this.panelFilters.Controls.Add(this.optionSetByPurchase);
            this.panelFilters.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelFilters.Location = new System.Drawing.Point(0, 0);
            this.panelFilters.Name = "panelFilters";
            this.panelFilters.Size = new System.Drawing.Size(630, 100);
            this.panelFilters.TabIndex = 33;

#if !NOKPPLIST

            // 
            // lblKPP
            // 
            this.lblKPP.Location = new System.Drawing.Point(16, 44);
            this.lblKPP.Name = "lblKPP";
            this.lblKPP.Size = new System.Drawing.Size(32, 20);
            this.lblKPP.TabIndex = 33;
            this.lblKPP.Text = "КПП:";
            // 
            // comboKpp
            // 
            this.comboKpp.DisplayMember = "DESCRIPTION";
            this.comboKpp.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboKpp.Location = new System.Drawing.Point(69, 40);
            this.comboKpp.Name = "comboKpp";
            this.comboKpp.Size = new System.Drawing.Size(98, 21);
            this.comboKpp.TabIndex = 32;

#endif //!NOKPPLIST

            // 
            // showRevertedCheckbox
            // 
            this.showRevertedCheckbox.Checked = true;
            this.showRevertedCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showRevertedCheckbox.Location = new System.Drawing.Point(325, 70);
            this.showRevertedCheckbox.Name = "showRevertedCheckbox";
            this.showRevertedCheckbox.Size = new System.Drawing.Size(307, 30);
            this.showRevertedCheckbox.TabIndex = 31;
            this.showRevertedCheckbox.Text = "Отображать контрагентов, не указанных продавцами";
            this.showRevertedCheckbox.UseVisualStyleBackColor = true;
            // 
            // percentEditor
            // 
            this.percentEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.percentEditor.Location = new System.Drawing.Point(550, 12);
            this.percentEditor.MinValue = 0;
            this.percentEditor.Name = "percentEditor";
            this.percentEditor.PromptChar = ' ';
            this.percentEditor.Size = new System.Drawing.Size(32, 20);
            this.percentEditor.TabIndex = 19;
            // 
            // comboTaxPeriod
            // 
            this.comboTaxPeriod.DisplayMember = "DESCRIPTION";
            this.comboTaxPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTaxPeriod.Name = "comboTaxPeriod";
#if !NOKPPLIST
            this.comboTaxPeriod.Location = new System.Drawing.Point(69, 70);
            this.comboTaxPeriod.Size = new System.Drawing.Size(98, 21);
#else //NOKPPLIST
            this.comboTaxPeriod.Location = new System.Drawing.Point(69, 40);
            this.comboTaxPeriod.Size = new System.Drawing.Size(98, 21);
#endif //!NOKPPLIST
            this.comboTaxPeriod.TabIndex = 28;
            // 
            // countEditor
            // 
            this.countEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.countEditor.Location = new System.Drawing.Point(550, 42);
            this.countEditor.MinValue = 0;
            this.countEditor.Name = "countEditor";
            this.countEditor.PromptChar = ' ';
            this.countEditor.Size = new System.Drawing.Size(32, 20);
            this.countEditor.TabIndex = 21;
            // 
            // innEditor
            // 
            this.innEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.innEditor.InputMask = "999999999999";
            this.innEditor.Location = new System.Drawing.Point(69, 10);
            this.innEditor.MaxValue = "999999999999";
            this.innEditor.MinValue = 0;
            this.innEditor.Name = "innEditor";
            this.innEditor.PromptChar = ' ';
            this.innEditor.Size = new System.Drawing.Size(98, 20);
            this.innEditor.TabIndex = 30;
#if !NOKPPLIST
            this.innEditor.TextChanged += new System.EventHandler(this.InnFilterChanged);
#endif //!NOKPPLIST
            // 
            // labelTaxPeriod
            // 
            this.labelTaxPeriod.Name = "labelTaxPeriod";
#if !NOKPPLIST
            this.labelTaxPeriod.Location = new System.Drawing.Point(16, 74);
#else //NOKPPLIST
            this.labelTaxPeriod.Location = new System.Drawing.Point(16, 44);
#endif //!NOKPPLIST
            this.labelTaxPeriod.Size = new System.Drawing.Size(47, 20);
            this.labelTaxPeriod.TabIndex = 27;
            this.labelTaxPeriod.Text = "Период:";
            // 
            // optionSetByPurchase
            // 
            this.optionSetByPurchase.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem4.DataValue = false;
            valueListItem4.DisplayText = "От продавца";
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = true;
            valueListItem1.DisplayText = "От покупателя";
            this.optionSetByPurchase.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem1});
            this.optionSetByPurchase.ItemSpacingHorizontal = 20;
            this.optionSetByPurchase.ItemSpacingVertical = 16;
            this.optionSetByPurchase.Location = new System.Drawing.Point(197, 7);
            this.optionSetByPurchase.Name = "optionSetByPurchase";
            this.optionSetByPurchase.Size = new System.Drawing.Size(103, 64);
            this.optionSetByPurchase.TabIndex = 2;
            this.optionSetByPurchase.ValueChanged += new System.EventHandler(this.OptionSetByPurchaseOnValueChanged);
            // 
            // tableTabPageControl
            // 
            this.tableTabPageControl.Controls.Add(this.ultraGridPanel);
            this.tableTabPageControl.Controls.Add(this.groupBoxTableData);
            this.tableTabPageControl.Enabled = false;
            this.tableTabPageControl.Location = new System.Drawing.Point(-10000, -10000);
            this.tableTabPageControl.Name = "tableTabPageControl";
            this.tableTabPageControl.Size = new System.Drawing.Size(1495, 637);
            // 
            // ultraGridPanel
            // 
            this.ultraGridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridPanel.Location = new System.Drawing.Point(0, 224);
            this.ultraGridPanel.Name = "ultraGridPanel";
            this.ultraGridPanel.Size = new System.Drawing.Size(1495, 413);
            this.ultraGridPanel.TabIndex = 1;
            // 
            // groupBoxTableData
            // 
            this.groupBoxTableData.Controls.Add(this.groupBoxTableDataPanel);
            this.groupBoxTableData.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxTableData.ExpandedSize = new System.Drawing.Size(1495, 224);
            this.groupBoxTableData.Location = new System.Drawing.Point(0, 0);
            this.groupBoxTableData.Name = "groupBoxTableData";
            this.groupBoxTableData.Size = new System.Drawing.Size(1495, 224);
            this.groupBoxTableData.TabIndex = 0;
            this.groupBoxTableData.Text = "Данные по НП, контрагенты которого отображены в таблице";
            // 
            // groupBoxTableDataPanel
            // 
            this.groupBoxTableDataPanel.Controls.Add(this.lbNdsPrcVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbClacNdsVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbDeductionNdsVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbLevelVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbIsBiggestVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbInspectionVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbRegionVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbIsPurchase);
            this.groupBoxTableDataPanel.Controls.Add(this.lbIsPurchaseVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbSurVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbNameVal);
            this.groupBoxTableDataPanel.Controls.Add(this.lbInnVal);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelRegion);
            this.groupBoxTableDataPanel.Controls.Add(this.lbDeductionNds);
            this.groupBoxTableDataPanel.Controls.Add(this.lbNdsPrc);
            this.groupBoxTableDataPanel.Controls.Add(this.lbClacNds);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelReportType);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelND);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelBigest);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelSur);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelinspection);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelName);
            this.groupBoxTableDataPanel.Controls.Add(this.ultraLabelInn);
            this.groupBoxTableDataPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxTableDataPanel.Location = new System.Drawing.Point(3, 19);
            this.groupBoxTableDataPanel.Name = "groupBoxTableDataPanel";
            this.groupBoxTableDataPanel.Size = new System.Drawing.Size(1489, 202);
            this.groupBoxTableDataPanel.TabIndex = 0;
            // 
            // lbNdsPrcVal
            // 
            this.lbNdsPrcVal.Location = new System.Drawing.Point(285, 180);
            this.lbNdsPrcVal.Name = "lbNdsPrcVal";
            this.lbNdsPrcVal.Size = new System.Drawing.Size(715, 23);
            this.lbNdsPrcVal.TabIndex = 22;
            // 
            // lbClacNdsVal
            // 
            this.lbClacNdsVal.Location = new System.Drawing.Point(285, 152);
            this.lbClacNdsVal.Name = "lbClacNdsVal";
            this.lbClacNdsVal.Size = new System.Drawing.Size(715, 23);
            this.lbClacNdsVal.TabIndex = 21;
            // 
            // lbDeductionNdsVal
            // 
            this.lbDeductionNdsVal.Location = new System.Drawing.Point(285, 124);
            this.lbDeductionNdsVal.Name = "lbDeductionNdsVal";
            this.lbDeductionNdsVal.Size = new System.Drawing.Size(715, 23);
            this.lbDeductionNdsVal.TabIndex = 20;
            // 
            // lbLevelVal
            // 
            this.lbLevelVal.Location = new System.Drawing.Point(88, 96);
            this.lbLevelVal.Name = "lbLevelVal";
            this.lbLevelVal.Size = new System.Drawing.Size(100, 23);
            this.lbLevelVal.TabIndex = 19;
            // 
            // lbIsBiggestVal
            // 
            this.lbIsBiggestVal.Location = new System.Drawing.Point(382, 96);
            this.lbIsBiggestVal.Name = "lbIsBiggestVal";
            this.lbIsBiggestVal.Size = new System.Drawing.Size(618, 23);
            this.lbIsBiggestVal.TabIndex = 18;
            // 
            // lbInspectionVal
            // 
            this.lbInspectionVal.Location = new System.Drawing.Point(382, 68);
            this.lbInspectionVal.Name = "lbInspectionVal";
            this.lbInspectionVal.Size = new System.Drawing.Size(618, 23);
            this.lbInspectionVal.TabIndex = 17;
            // 
            // lbRegionVal
            // 
            this.lbRegionVal.Location = new System.Drawing.Point(382, 40);
            this.lbRegionVal.Name = "lbRegionVal";
            this.lbRegionVal.Size = new System.Drawing.Size(618, 23);
            this.lbRegionVal.TabIndex = 16;
            // 
            // lbIsPurchase
            // 
            this.lbIsPurchase.Location = new System.Drawing.Point(88, 68);
            this.lbIsPurchase.Name = "lbIsPurchase";
            this.lbIsPurchase.Size = new System.Drawing.Size(150, 23);
            this.lbIsPurchase.TabIndex = 15;
            // 
            // lbIsPurchaseVal
            // 
            this.lbIsPurchaseVal.Location = new System.Drawing.Point(88, 71);
            this.lbIsPurchaseVal.Name = "lbIsPurchaseVal";
            this.lbIsPurchaseVal.Size = new System.Drawing.Size(178, 23);
            this.lbIsPurchaseVal.TabIndex = 14;
            // 
            // lbSurVal
            // 
            this.lbSurVal.Location = new System.Drawing.Point(75, 40);
            this.lbSurVal.Name = "lbSurVal";
            this.lbSurVal.Size = new System.Drawing.Size(191, 23);
            this.lbSurVal.TabIndex = 13;
            // 
            // lbNameVal
            // 
            this.lbNameVal.Location = new System.Drawing.Point(382, 12);
            this.lbNameVal.Name = "lbNameVal";
            this.lbNameVal.Size = new System.Drawing.Size(618, 23);
            this.lbNameVal.TabIndex = 12;
            // 
            // lbInnVal
            // 
            this.lbInnVal.Location = new System.Drawing.Point(88, 12);
            this.lbInnVal.Name = "lbInnVal";
            this.lbInnVal.Size = new System.Drawing.Size(166, 23);
            this.lbInnVal.TabIndex = 11;
            // 
            // ultraLabelRegion
            // 
            this.ultraLabelRegion.Location = new System.Drawing.Point(285, 40);
            this.ultraLabelRegion.Name = "ultraLabelRegion";
            this.ultraLabelRegion.Size = new System.Drawing.Size(49, 23);
            this.ultraLabelRegion.TabIndex = 10;
            this.ultraLabelRegion.Text = "Регион:";
            // 
            // lbDeductionNds
            // 
            this.lbDeductionNds.Location = new System.Drawing.Point(22, 124);
            this.lbDeductionNds.Name = "lbDeductionNds";
            this.lbDeductionNds.Size = new System.Drawing.Size(260, 23);
            this.lbDeductionNds.TabIndex = 9;
            this.lbDeductionNds.Text = "Сумма НДС, подлежащая вычету у НП, ВСЕГО:";
            // 
            // lbNdsPrc
            // 
            this.lbNdsPrc.Location = new System.Drawing.Point(22, 180);
            this.lbNdsPrc.Name = "lbNdsPrc";
            this.lbNdsPrc.Size = new System.Drawing.Size(115, 23);
            this.lbNdsPrc.TabIndex = 8;
            this.lbNdsPrc.Text = "Доля вычетов у НП:";
            // 
            // lbClacNds
            // 
            this.lbClacNds.Location = new System.Drawing.Point(22, 152);
            this.lbClacNds.Name = "lbClacNds";
            this.lbClacNds.Size = new System.Drawing.Size(184, 23);
            this.lbClacNds.TabIndex = 7;
            this.lbClacNds.Text = "НДС исчисленный НП, ВСЕГО:";
            // 
            // ultraLabelReportType
            // 
            this.ultraLabelReportType.Location = new System.Drawing.Point(22, 68);
            this.ultraLabelReportType.Name = "ultraLabelReportType";
            this.ultraLabelReportType.Size = new System.Drawing.Size(100, 23);
            this.ultraLabelReportType.TabIndex = 6;
            this.ultraLabelReportType.Text = "Тип отчета:";
            // 
            // ultraLabelND
            // 
            this.ultraLabelND.Location = new System.Drawing.Point(22, 96);
            this.ultraLabelND.Name = "ultraLabelND";
            this.ultraLabelND.Size = new System.Drawing.Size(60, 23);
            this.ultraLabelND.TabIndex = 5;
            this.ultraLabelND.Text = "Уровень:";
            // 
            // ultraLabelBigest
            // 
            this.ultraLabelBigest.Location = new System.Drawing.Point(285, 96);
            this.ultraLabelBigest.Name = "ultraLabelBigest";
            this.ultraLabelBigest.Size = new System.Drawing.Size(77, 23);
            this.ultraLabelBigest.TabIndex = 4;
            this.ultraLabelBigest.Text = "Крупнейший:";
            // 
            // ultraLabelSur
            // 
            this.ultraLabelSur.Location = new System.Drawing.Point(22, 40);
            this.ultraLabelSur.Name = "ultraLabelSur";
            this.ultraLabelSur.Size = new System.Drawing.Size(34, 23);
            this.ultraLabelSur.TabIndex = 3;
            this.ultraLabelSur.Text = "СУР:";
            // 
            // ultraLabelinspection
            // 
            this.ultraLabelinspection.Location = new System.Drawing.Point(285, 68);
            this.ultraLabelinspection.Name = "ultraLabelinspection";
            this.ultraLabelinspection.Size = new System.Drawing.Size(100, 23);
            this.ultraLabelinspection.TabIndex = 2;
            this.ultraLabelinspection.Text = "Инспекция:";
            // 
            // ultraLabelName
            // 
            this.ultraLabelName.Location = new System.Drawing.Point(285, 12);
            this.ultraLabelName.Name = "ultraLabelName";
            this.ultraLabelName.Size = new System.Drawing.Size(100, 23);
            this.ultraLabelName.TabIndex = 1;
            this.ultraLabelName.Text = "Наименование:";
            // 
            // ultraLabelInn
            // 
            this.ultraLabelInn.Location = new System.Drawing.Point(22, 12);
            this.ultraLabelInn.Name = "ultraLabelInn";
            this.ultraLabelInn.Size = new System.Drawing.Size(100, 23);
            this.ultraLabelInn.TabIndex = 0;
            this.ultraLabelInn.Text = "ИНН НП:";
            // 
            // treeTabControl
            // 
            this.treeTabControl.Controls.Add(this.tabTree);
            this.treeTabControl.Controls.Add(this.treeTabPageControl);
            this.treeTabControl.Controls.Add(this.tableTabPageControl);
            this.treeTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeTabControl.Location = new System.Drawing.Point(0, 0);
            this.treeTabControl.Name = "treeTabControl";
            this.treeTabControl.SharedControlsPage = this.tabTree;
            this.treeTabControl.Size = new System.Drawing.Size(1499, 663);
            this.treeTabControl.TabIndex = 0;
            ultraTab1.TabPage = this.treeTabPageControl;
            ultraTab1.Text = "Дерево";
            ultraTab2.TabPage = this.tableTabPageControl;
            ultraTab2.Text = "Таблица";
            this.treeTabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // tabTree
            // 
            this.tabTree.Location = new System.Drawing.Point(-10000, -10000);
            this.tabTree.Name = "tabTree";
            this.tabTree.Size = new System.Drawing.Size(1495, 637);
            // 
            // GraphTreeContainer
            // 
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(1070, 250);
            this.AutoSize = true;
            this.Controls.Add(this.treeTabControl);
            this.Name = "GraphTreeContainer";
            this.Size = new System.Drawing.Size(1499, 663);
            this.treeTabPageControl.ResumeLayout(false);
            this.treePanel.ResumeLayout(false);
            this.filterPanel.ClientArea.ResumeLayout(false);
            this.filterPanel.ResumeLayout(false);
            this.panelButtons.ResumeLayout(false);
            this.panelFilters.ResumeLayout(false);
            this.panelFilters.PerformLayout();
#if !NOKPPLIST
            ((System.ComponentModel.ISupportInitialize)(this.comboKpp)).EndInit();
#endif //!NOKPPLIST
            ((System.ComponentModel.ISupportInitialize)(this.comboTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optionSetByPurchase)).EndInit();
            this.tableTabPageControl.ResumeLayout(false);
            this.ultraGridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTableData)).EndInit();
            this.groupBoxTableData.ResumeLayout(false);
            this.groupBoxTableDataPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeTabControl)).EndInit();
            this.treeTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl treeTabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage tabTree;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl treeTabPageControl;
        private Infragistics.Win.Misc.UltraPanel treePanel;
        private Infragistics.Win.Misc.UltraPanel filterPanel;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tableTabPageControl;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optionSetByPurchase;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groupBoxTableData;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel groupBoxTableDataPanel;
        private Infragistics.Win.Misc.UltraLabel lbDeductionNds;
        private Infragistics.Win.Misc.UltraLabel lbNdsPrc;
        private Infragistics.Win.Misc.UltraLabel lbClacNds;
        private Infragistics.Win.Misc.UltraLabel ultraLabelReportType;
        private Infragistics.Win.Misc.UltraLabel ultraLabelND;
        private Infragistics.Win.Misc.UltraLabel ultraLabelBigest;
        private Infragistics.Win.Misc.UltraLabel ultraLabelSur;
        private Infragistics.Win.Misc.UltraLabel ultraLabelinspection;
        private Infragistics.Win.Misc.UltraLabel ultraLabelName;
        private Infragistics.Win.Misc.UltraLabel ultraLabelInn;
        private Infragistics.Win.Misc.UltraLabel ultraLabelRegion;
        private Controls.Grid.V1.GridControl grid;
        private Infragistics.Win.Misc.UltraPanel ultraGridPanel;
        private Infragistics.Win.Misc.UltraLabel lbIsPurchase;
        private Infragistics.Win.Misc.UltraLabel lbIsPurchaseVal;
        private Infragistics.Win.Misc.UltraLabel lbSurVal;
        private Infragistics.Win.Misc.UltraLabel lbNameVal;
        private Infragistics.Win.Misc.UltraLabel lbInnVal;
        private Infragistics.Win.Misc.UltraLabel lbRegionVal;
        private Infragistics.Win.Misc.UltraLabel lbLevelVal;
        private Infragistics.Win.Misc.UltraLabel lbIsBiggestVal;
        private Infragistics.Win.Misc.UltraLabel lbInspectionVal;
        private Infragistics.Win.Misc.UltraLabel lbNdsPrcVal;
        private Infragistics.Win.Misc.UltraLabel lbClacNdsVal;
        private Infragistics.Win.Misc.UltraLabel lbDeductionNdsVal;
        private System.Windows.Forms.SaveFileDialog _saveFileDialogReport;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit percentEditor;
        private Infragistics.Win.Misc.UltraButton printButton;
        private Infragistics.Win.Misc.UltraButton reloadButton;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit countEditor;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTaxPeriod;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriod;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit innEditor;
        private Infragistics.Win.Misc.UltraButton excelAllButton;
        private Infragistics.Win.Misc.UltraButton excelVisibleButton;
        private System.Windows.Forms.ToolTip toolTipControl;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Panel panelFilters;
        private System.Windows.Forms.CheckBox showRevertedCheckbox;
        private Infragistics.Win.Misc.UltraLabel lblKPP;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboKpp;
    }
}
