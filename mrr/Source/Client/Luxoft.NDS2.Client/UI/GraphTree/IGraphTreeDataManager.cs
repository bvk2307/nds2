using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    /// <summary> A data manager of <see cref="IPresenter"/>. </summary>
    public interface IGraphTreeDataManager
    {
        /// <summary> Creates a new service call dispatcher connected to the pool of client executors for graph tree nodes. </summary>
        /// <param name="dataTransofrmHandler"> A data tranform hadnler. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="cancelSource"></param>
        /// <param name="faultOrCancelHandler"> An error or cancellation handler. Optional. Does not used if it is not defined. </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new service call dispatcher created. </returns>
        IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
            InitNodesDataDispatcher( 
                Func<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>, 
                     CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>> 
                     dataTransofrmHandler, 
                Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken> 
                    dataLoadedHandler, 
                CancellationTokenSource cancelSource = null, Action<Task> faultOrCancelHandler = null, string postProcessNameFormat = null );

        /// <summary> Creates a new batch service call dispatcher connected to the pool of client executors for graph tree nodes. </summary>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="finishStepHandler"> A handler called on the dispatcher finish step before completion. </param>
        /// <param name="onFinishHandler"> A final handler called after finish step <paramref name="finishStepHandler"/>. It is called in the main UI thread. </param>
        /// <param name="layersDepthLimit"> A maximum depth layer number where the root node has depth 0. </param>
        /// <param name="cancelSource"> </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new batch service call dispatcher created. </returns>
        IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string> 
            InitServiceCallBatchDispatcher( 
                Action<CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>, CancellationToken> dataLoadedHandler, 
                Func<CancellationToken, Exception, string> finishStepHandler, 
                Action<string, Exception, CancellationToken, CancellationToken> onFinishHandler, 
                int layersDepthLimit, CancellationTokenSource cancelSource = null, string postProcessNameFormat = null );

        /// <summary> Creates a new service call dispatcher connected to the pool of client executors for tax payer deduction sums. </summary>
        /// <param name="dataTransofrmHandler"> A data tranform hadnler. </param>
        /// <param name="dataLoadedHandler"> A data loaded hadnler. </param>
        /// <param name="cancelSource"> </param>
        /// <param name="faultOrCancelHandler"> An error or cancellation handler. Optional. Does not used if it is not defined. </param>
        /// <param name="postProcessNameFormat"></param>
        /// <returns> A new service call dispatcher created. </returns>
        IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> 
            InitDeductionDetailsDataDispatcher( 
                Func<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, 
                     CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>> 
                     dataTransofrmHandler, 
                Action<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, CancellationToken> dataLoadedHandler, 
                CancellationTokenSource cancelSource = null, Action<Task> faultOrCancelHandler = null, string postProcessNameFormat = null );

        /// <summary> Posts a request on graph tree nodes to <paramref name="nodeDataDispatcher"/>. A result will be processed in the data loaded handler initilized in 
        /// <see cref="ServiceCallDataManager.InitServiceCallDispatcher{TParam,TInstanceKey,TContextKey,TData,TResult}"/>. </summary>
        /// <param name="nodeDataDispatcher"> A service call dispatcher to request. </param>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        /// <param name="priority"> A request execution priority. </param>
        /// <returns> A request result. </returns>
        bool PostTo( 
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
                nodeDataDispatcher, 
            GraphNodesKeyParameters keyParameters, int instanceKey, Guid contextKey, CallExecPriority priority = CallExecPriority.Normal );

        /// <summary> Posts a request on tax payer deduction sums to <paramref name="nodeDataDispatcher"/>. A result will be processed in the data loaded handler initilized in 
        /// <see cref="ServiceCallDataManager.InitServiceCallDispatcher{TParam,TInstanceKey,TContextKey,TResult}"/>. </summary>
        /// <param name="nodeDataDispatcher"> A service call dispatcher to request. </param>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        /// <param name="priority"> A request execution priority. </param>
        /// <returns> A request result. </returns>
        bool PostTo( 
            IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> nodeDataDispatcher, 
            DeductionDetailsKeyParameters keyParameters, int instanceKey, Guid contextKey, CallExecPriority priority = CallExecPriority.Normal );


        /// <summary> Gets taxpayers kpps by inn. </summary>
        /// <param name="inn"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        IReadOnlyCollection<string> GetTaxpayersKpp(string inn, CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null);
    }
}