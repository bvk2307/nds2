﻿//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree.Actions
{
    /// <summary> An action to load data for building a new graph tree branch. </summary>
    public sealed class LoadGraphTreeBranchAction : ILoadGraphTreeBranchAction, IDisposable
    {
        #region Private members 

        private readonly IGraphTreeDataManager _graphTreeDataManager;
        private readonly IDirectExecutor _callExecutor;
        private readonly IBuildGraphTreeBranchAction _buildBranchAction;
        private readonly 
            Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken> 
                _nodesDataLoadedHandler;

        private GraphTreeParameters _treeBranchParameters = null;
        private ICommandTransactionManager _commandManager = null;
        private 
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
                _treeBranchDispatcher = null;
        private GraphTreeObjectTransformer _graphTreeObjectManager;

        #endregion Private members 

        public LoadGraphTreeBranchAction( 
            NodeTree nodeTree, IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor, 
            IBuildGraphTreeBranchAction buildBranchAction, 
            Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken> 
                nodesDataLoadedHandler )
        {
            Contract.Requires( nodeTree != null );
            Contract.Requires( graphTreeDataManager != null );
            Contract.Requires( callExecutor != null );
            Contract.Requires( buildBranchAction != null );
            Contract.Requires( nodesDataLoadedHandler != null );

            _graphTreeDataManager       = graphTreeDataManager;
            _callExecutor               = callExecutor;
            _buildBranchAction          = buildBranchAction;
            _nodesDataLoadedHandler     = nodesDataLoadedHandler;

            nodeTree.AcceptAction( this );
        }

        public IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
            Reinit( ICommandTransactionManager commandManager, Action<Task> faultOrCancelHandler, 
                    CancellationTokenSource cancelSource, GraphTreeObjectTransformer graphTreeObjectManager )
        {
            Contract.Requires( commandManager != null );
            Contract.Requires( graphTreeObjectManager != null );

            _commandManager             = commandManager;
            _graphTreeObjectManager     = graphTreeObjectManager;

	//apopov 21.6.2016	//DEBUG!!!  //call this action at the background thread no more than one at the same time to Transfer nodes
            _treeBranchDispatcher = _graphTreeDataManager.InitNodesDataDispatcher( 
                TransformNodeData, _nodesDataLoadedHandler, //OnNodesDataLoaded, 	//apopov 22.6.2016	//TODO!!!   //replace when LoadGraphTreeBranchAction and InfoNodeChildrenCountAction will be use separate service call dispatchers
                cancelSource, faultOrCancelHandler, "OnNodesDataLoaded {0}, ID {1} LoadGraphTreeBranchAction" );

            return _treeBranchDispatcher;
        }

        /// <summary> Starts tree node data loading. </summary>
        /// <param name="treeBranchParameters"></param>
        /// <param name="parentNodeId"> </param>
        public void StartDataRequest( GraphTreeParameters treeBranchParameters, Guid parentNodeId )
        {
            Contract.Requires( treeBranchParameters != null );
            Contract.Assume( _commandManager != null );

            _treeBranchParameters = treeBranchParameters;

            int requestCmdManagerInstanceKey = CommandManager.InstanceKey;
            bool success = IsRequestOfActualCommandManager( requestCmdManagerInstanceKey );
            if ( success )
            {
                OnStartDataRequest();

                success = this._graphTreeDataManager.PostTo( _treeBranchDispatcher, _treeBranchParameters.KeyParameters,
                    requestCmdManagerInstanceKey, contextKey: parentNodeId, priority: CallExecPriority.Normal ); //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
            }
            if ( !success )
            {
                CommandManager.RollbackTransaction();

                IgnoreOfDataRequest( requestCmdManagerInstanceKey ); //ignores user calls those could not be serviced by the service call dispatcher
            }
        }

        /// <summary> Called in only one background thread at the same time. </summary>
        /// <param name="callContext"></param>
        /// <returns></returns>
        private CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> 
            
            TransformNodeData( CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>> callContext )
        {
            Guid parentNodeKey                      = callContext.ContextKey;    //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
            IReadOnlyCollection<GraphData> nodes    = callContext.Result;
            CallExecPriority priority               = callContext.Priority;
            IEnumerable<GraphData> nodesToProcess = parentNodeKey == GraphTreeNodeData.NoNodeKey    //builds a new tree if 'parentNodeKey == GraphTreeNodeData.NoNodeKey'
                        ? nodes : nodes.Where( n => n.ParentInn != null );    //excludes the parent node. It does not use a value of the parent node identifier only flag 'does a node have a parent or not?'

            IEnumerable<GraphTreeNodeData> graphNodes = priority == CallExecPriority.Background
                ? nodesToProcess.Select( graphData => new GraphTreeNodeData( parentNodeKey, graphData, treeParameters: null ) ) //request key parameters are needless for counter's GraphTreeNodeData-s
                : _graphTreeObjectManager.TransformTreeData( nodesToProcess, parentNodeKey );

            var contextTranformed = 
                new CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>(
                    callContext.Parameters, callContext.InstanceKey, callContext.ContextKey, graphNodes.ToReadOnly(), callContext.Priority );

            return contextTranformed;
        }

        /// <summary> Called in only one background thread at the same time. </summary>
        public void OnNodesDataLoaded( 
            CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> callContext, 
            CancellationToken cancelToken )
        {
            _callExecutor.Execute( (Func<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken, bool>)
                ProcessData, callContext, cancelToken );
        }

        /// <summary> Called in only one background thread at the same time. </summary>
        private bool ProcessData( 
            CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> callContext, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            IReadOnlyCollection<GraphTreeNodeData> graphNodes   = callContext.Result;
            Guid parentNodeKey                                  = callContext.ContextKey;    //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
            int instanceKey                                     = callContext.InstanceKey;

            bool success = false;
            bool toRollback = false;

#if ( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "BuildGraphTreeBranch" );

#endif //DONOTDISABLE
            try
            {
                success = ProcessData( graphNodes, parentNodeKey, instanceKey, cancelToken );
            }catch( Exception )
            {
                toRollback = true;

                throw;
            }finally
            {
                if ( !success || toRollback )
                     CommandManager.RollbackTransaction();
            }
            if ( !success )
                IgnoreOfDataRequest( instanceKey ); //ignores user calls those could not be serviced by the service call dispatcher

            return success;
        }

        private bool ProcessData( IReadOnlyCollection<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey, CancellationToken cancelToken )
        {
            bool success = IsRequestOfActualCommandManager( instanceKey ) && graphNodes != null;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )
                    _buildBranchAction.BuildTreeBranch( graphNodes, parentNodeKey, instanceKey, cancelToken );
            }
            return success;
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores a node data request. </summary>
        /// <param name="instanceKey"></param>
        private void IgnoreOfDataRequest( int instanceKey )
        {
        }

        private void OnStartDataRequest()
        {
#if !( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "BuildGraphTreeBranch" );

#endif //!DONOTDISABLE
        }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        private bool IsRequestOfActualCommandManager( int instanceKey )
        {
            return CommandManager.InstanceKey == instanceKey;
        }

        #region Implementation of IDisposable

        /// <summary> Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary> Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. </summary>
        /// <param name="disposing"></param>
        private void Dispose( bool disposing )
        {
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
                dispatcher = _treeBranchDispatcher;
            _treeBranchDispatcher = null;
            if ( dispatcher != null )
                dispatcher.Complete();

            GC.SuppressFinalize( this );
        }

        ~LoadGraphTreeBranchAction()
        {
            Dispose( false );
        }

        #endregion
    }
}