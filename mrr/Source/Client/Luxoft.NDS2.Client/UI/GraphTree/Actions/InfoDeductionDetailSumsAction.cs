﻿//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree.Actions
{
    /// <summary> An action to info about Deduction detail sums for graph tree. </summary>
    public sealed class InfoDeductionDetailSumsAction : IInfoDeductionDetailSumsAction
    {
        #region Private members

        private readonly NodeTree _nodeTree;
        private readonly IGraphTreeDataManager _graphTreeDataManager;
        private readonly IDirectExecutor _callExecutor;

        private ICommandTransactionManager _commandManager = null;
        private IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> 
            _deductionDetailsDispatcher = null;

        #endregion Private members

        public InfoDeductionDetailSumsAction( NodeTree nodeTree, IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor )
        {
            Contract.Requires( nodeTree != null );
            Contract.Requires( graphTreeDataManager != null );
            Contract.Requires( callExecutor != null );

            _nodeTree                           = nodeTree;
            _graphTreeDataManager               = graphTreeDataManager;
            _callExecutor                       = callExecutor;
        }

        public void Reinit( ICommandTransactionManager commandManager, Action<Task> faultOrCancelHandler, CancellationTokenSource cancelSource )
        {
            Contract.Requires( commandManager != null );

            _commandManager = commandManager;

            _deductionDetailsDispatcher = _graphTreeDataManager.InitDeductionDetailsDataDispatcher(
                DoNothing, OnDataLoaded, cancelSource, faultOrCancelHandler, "OnDataLoaded {0}, ID {1} InfoDeductionDetailSumsAction" );
        }

        private static CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> 
            DoNothing( CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> context )
        {
            return context;
        }

        /// <summary> Starts the data request of graph tree data of deduction detail sums. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <param name="parentNodeId"> </param>
        public void StartDataRequest( DeductionDetailsKeyParameters deductionKeyParameters, Guid parentNodeId )
        {
            Contract.Requires( deductionKeyParameters != null );
            Contract.Requires( parentNodeId != GraphTreeNodeData.NoNodeKey );
            Contract.Assume( _commandManager != null );

            int requestCmdManagerInstanceKey = CommandManager.InstanceKey;
            bool success = IsRequestOfActualCommandManager( requestCmdManagerInstanceKey );
            if ( success )
            {
                OnStartDataRequest();

                success = this._graphTreeDataManager.PostTo( _deductionDetailsDispatcher, new DeductionDetailsKeyParameters( deductionKeyParameters ),
                                                             requestCmdManagerInstanceKey, contextKey: parentNodeId );
            }
            if ( !success )
            {
                CommandManager.RollbackTransaction();

                IgnoreOfDataRequest( requestCmdManagerInstanceKey, deductionKeyParameters ); //ignores user calls those could not be serviced by the service call dispatcher
            }
        }

        private void OnDataLoaded( 
            CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> callResult, CancellationToken cancelToken )
        {
            _callExecutor.Execute( (Func<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, CancellationToken, bool>)
                ProcessData, callResult, cancelToken );
        }

        private bool ProcessData( CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> callResult, 
                                  CancellationToken cancelToken = default(CancellationToken) )
        {
            IReadOnlyCollection<DeductionDetail> deductionDetails = callResult.Result;
            Guid parentNodeId             = callResult.ContextKey;
            DeductionDetailsKeyParameters nodesKeyParameters = callResult.Parameters;
            int instanceKey                 = callResult.InstanceKey;

            bool success = false;
            bool toRollback = false;

#if ( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "InfoDeductionDetailSums" );

#endif //DONOTDISABLE
            try
            {
                success = ProcessData( deductionDetails, parentNodeId, instanceKey, cancelToken );
            }catch( Exception )
            {
                toRollback = true;

                throw;
            }finally
            {
                if ( !success || toRollback )
                     CommandManager.RollbackTransaction();
                else CommandManager.CommitTransaction( "InfoDeductionDetailSums" );
            }
            if ( !success )
                IgnoreOfDataRequest( instanceKey, nodesKeyParameters ); //ignores user calls those could not be serviced by the service call dispatcher

            return success;
        }

        private bool ProcessData( 
            IReadOnlyCollection<DeductionDetail> deductionDetails, Guid parentNodeId, int instanceKey, CancellationToken cancelToken )
        {
            bool success = IsRequestOfActualCommandManager( instanceKey ) && deductionDetails != null;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )
                    success = _nodeTree.InfoDeductionDetailSums( parentNodeId, deductionDetails );
            }
            return success;
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores deduction detail sums data request. </summary>
        /// <param name="instanceKey"></param>
        /// <param name="keyParameters"></param>
        private void IgnoreOfDataRequest( int instanceKey, DeductionDetailsKeyParameters keyParameters )
        {
        }

        private void OnStartDataRequest()
        {
#if !( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "InfoDeductionDetailSums" );

#endif //!DONOTDISABLE
        }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        private bool IsRequestOfActualCommandManager( int instanceKey )
        {
            return CommandManager.InstanceKey == instanceKey;
        }
    }
}