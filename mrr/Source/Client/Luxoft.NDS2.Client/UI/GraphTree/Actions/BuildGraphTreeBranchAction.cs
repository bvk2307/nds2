﻿//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree.Actions
{
    /// <summary> An action to build a new graph tree branch. </summary>
    public sealed class BuildGraphTreeBranchAction : IBuildGraphTreeBranchAction
    {
        private readonly GraphTreeContainer _view;
        private readonly IDirectExecutor _callExecutor;

        private ICommandTransactionManager _commandManager = null;

        public BuildGraphTreeBranchAction( GraphTreeContainer view, IDirectExecutor callExecutor )
        {
            Contract.Requires( view != null );
            Contract.Requires( callExecutor != null );

            _view           = view;
            _callExecutor   = callExecutor;
        }

        public void Reinit( ICommandTransactionManager commandManager )
        {
            Contract.Requires( commandManager != null );

            _commandManager = commandManager;
        }

        public void BuildTreeBranch( IEnumerable<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey, CancellationToken cancelToken )
        {
            _callExecutor.Execute( (Func<IEnumerable<GraphTreeNodeData>, Guid, int, CancellationToken, bool>)
                ProcessData, graphNodes, parentNodeKey, instanceKey, cancelToken );
        }

        private bool ProcessData( IEnumerable<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey, CancellationToken cancelToken )
        {
            bool success = false;
            bool toRollback = false;

            try
            {
                success = ProcessData( cancelToken, graphNodes, parentNodeKey, instanceKey );
            }catch( Exception )
            {
                toRollback = true;

                throw;
            }finally
            {
                if ( !success || toRollback )
                     CommandManager.RollbackTransaction();
                else CommandManager.CommitTransaction( "BuildGraphTreeBranch" );
            }
            if ( !success )
                IgnoreOfDataProcessing( instanceKey ); //ignores user calls those could not be serviced by the service call dispatcher

            return success;
        }

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="graphNodes"></param>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="instanceKey"></param>
        /// <returns></returns>
        private bool ProcessData( CancellationToken cancelToken, IEnumerable<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey )
        {
            bool success = IsRequestOfActualCommandManager( instanceKey ) && graphNodes != null;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )	//apopov 22.6.2016	//TODO!!! replace call of GraphTreeContainter.BuildTreeBranch() by call NodeTree.BuildTreeBranch()
                    success = _view.BuildTreeBranch( parentNodeKey, graphNodes );
            }
            return success;
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores a node data request. </summary>
        /// <param name="instanceKey"></param>
        private void IgnoreOfDataProcessing( int instanceKey )
        {
        }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        private bool IsRequestOfActualCommandManager( int instanceKey )
        {
            return CommandManager.InstanceKey == instanceKey;
        }
    }
}