﻿//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree.Actions
{
    /// <summary> An action to load node children count. </summary>
    public sealed class InfoNodeChildrenCountAction : IInfoNodeChildrenCountAction
    {
        #region Private members 

        private readonly NodeTree _nodeTree;
        private readonly IGraphTreeDataManager _graphTreeDataManager;
        private readonly IDirectExecutor _callExecutor;

        private ICommandTransactionManager _commandManager = null;
        private IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
            _treeBranchDispatcher = null;

        #endregion Private members 

        public InfoNodeChildrenCountAction( NodeTree nodeTree, IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor )
        {
            Contract.Requires( nodeTree != null );
            Contract.Requires( graphTreeDataManager != null );
            Contract.Requires( callExecutor != null );

            _nodeTree                   = nodeTree;
            _graphTreeDataManager       = graphTreeDataManager;
            _callExecutor               = callExecutor;

            _nodeTree.AcceptAction( this );
        }

        public void Reinit( 
            ICommandTransactionManager commandManager, Action<Task> faultOrCancelHandler, CancellationTokenSource cancelSource, 
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> treeBranchDispatcher )
        {
            Contract.Requires( commandManager != null );

            _commandManager = commandManager;

	//apopov 21.6.2016	//DEBUG!!!  //call this action at the background thread no more than one at the same time to Transfer nodes
            _treeBranchDispatcher = treeBranchDispatcher;	//apopov 22.6.2016	//TODO!!!   //replace when LoadGraphTreeBranchAction and InfoNodeChildrenCountAction will be use separate service call dispatchers
                                    //_graphTreeDataManager.InitNodesDataDispatcher( OnNodesDataLoaded, cancelSource, faultOrCancelHandler, "OnNodesDataLoaded {0}, ID {1} LoadNodeChildrenCountAction" );
        }

        /// <summary> Starts tree node data loading. </summary>
        /// <param name="treeBranchParameters"></param>
        /// <param name="parentNodeId"> </param>
        public void StartDataRequest( GraphTreeParameters treeBranchParameters, Guid parentNodeId )
        {
            Contract.Requires( treeBranchParameters != null );

            int requestCmdManagerInstanceKey = CommandManager.InstanceKey;
            bool success = IsRequestOfActualCommandManager( requestCmdManagerInstanceKey );
            if ( success )
                success = _graphTreeDataManager.PostTo( _treeBranchDispatcher, treeBranchParameters.KeyParameters,
                    requestCmdManagerInstanceKey, contextKey: parentNodeId, priority: CallExecPriority.Background ); //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
            if ( !success )
                IgnoreOfDataRequest( requestCmdManagerInstanceKey ); //ignores user calls those could not be serviced by the service call dispatcher
        }

        public void OnNodesDataLoaded( 
            CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> callContext, 
            CancellationToken cancelToken )
        {
            _callExecutor.Execute( (Func<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken, bool>)
                ProcessData, callContext, cancelToken );
        }

        private bool ProcessData( 
            CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> callContext, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            IReadOnlyCollection<GraphTreeNodeData> graphNodes   = callContext.Result;
            Guid parentNodeKey                      = callContext.ContextKey;    //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
            int instanceKey                         = callContext.InstanceKey;

            bool success = ProcessData( graphNodes, parentNodeKey, instanceKey, cancelToken );
            if ( !success )
                IgnoreOfDataRequest( instanceKey ); //ignores user calls those could not be serviced by the service call dispatcher

            return success;
        }

        private bool ProcessData( IReadOnlyCollection<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey, CancellationToken cancelToken )
        {
            bool success = IsRequestOfActualCommandManager( instanceKey ) && graphNodes != null;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )
                {
                    int revertedNodesToProcessCount = graphNodes.Count( x => x.IsReverted );

                    _nodeTree.SetChildCounter( parentNodeKey, graphNodes.Count - revertedNodesToProcessCount, revertedNodesToProcessCount );
                }
            }
            return success;
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores a node data request. </summary>
        /// <param name="instanceKey"></param>
        private void IgnoreOfDataRequest( int instanceKey )
        {
        }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        private bool IsRequestOfActualCommandManager( int instanceKey )
        {
            return CommandManager.InstanceKey == instanceKey;
        }
    }
}