﻿#define NET40

//#define ALLNODEREPORT_ATONETIME

//#define DONOTDISABLE
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Config;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.GraphTree.Actions;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    public class Presenter : BasePresenter<GraphTreeContainer>, IPresenter, IInstanceProvider<ICommandTransactionManager>
    {
        #region Private members

        ////apopov 11.7.2016	//DEBUG!!!
        ///// <summary> A checker called with delay by GC finalizer in a pool thread. It warns if all service call executors should be returned alread but not yet. </summary>
        ///// <remarks> No any operations are nedeed with it in code. </remarks>
        //private readonly ServiceCallExecutorManagerChecker _executorManagerChecker = new ServiceCallExecutorManagerChecker();

        private readonly IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy;
        private readonly IPyramidDataService _pyramidDataService;
        private readonly IGraphTreeDataManager _graphTreeDataManager;
        private readonly DependencyTreeSection _dependencyTreeSection;
        private readonly IDeclarationCardOpener _declarationCardOpener;
        private readonly IDirectExecutor _directExecutor;

        private BuildGraphTreeBranchAction _buildGraphTreeBranchAction;
        private InfoDeductionDetailSumsAction _infoDeductionDetailSumsAction;
        private LoadGraphTreeBranchAction _loadGraphTreeBranchAction = null;
        private InfoNodeChildrenCountAction _infoNodeChildrenCountAction = null;

        private IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string> _nodeAllTreeDispatcher = null;

        private ICommandTransactionManager _commandManager = null;
        /// <summary> Parameters of the sheet with tree graph. </summary>
        private GraphTreeParameters _treeTabTreeParameters = null;
        /// <summary> Parameters of the sheet with tree table. </summary>
        private GraphTreeParameters _tableTabTreeParameters = null;

        //<< Reports
        private readonly IInstanceProvider<INodeAllTreeReporter> _nodeAllTreeProvider;
        private readonly IInstanceProvider<INodeVisibleTreeReporter> _nodeVisibleTreeProvider;
        private readonly IInstanceProvider<INodeTableReporter> _nodeTableProvider;

        private INodeAllTreeReporter _nodeAllTreeReporter = null;
        private INodeVisibleTreeReporter _nodeVisibleTreeReporter = null;
        private INodeTableReporter _nodeTableReporter = null;
        private ICollection<GraphDataNode> _tabTabReportTable = null;

        private CancellationTokenSource _cancelSourceAllTreeExport = null;
        private CancellationTokenSource _cancelSourceVisibleTreeExport = null;
        private CancellationTokenSource _cancelSourceTableExport = null;
        // Reports >>

        #endregion Private members

        public Presenter( 
            GraphTreeParameters treeParameters, PresentationContext context, WorkItem workItem, GraphTreeContainer view, 
            IPyramidDataService pyramidDataService, IGraphTreeDataManager graphTreeDataManager, 
            IDeclarationUserPermissionsPolicy declarationUserPermissionsPolicy, IInstanceProvider<INodeAllTreeReporter> nodeAllTreeProvider, 
            IInstanceProvider<INodeVisibleTreeReporter> nodeVisibleTreeProvider, IInstanceProvider<INodeTableReporter> nodeTableProvider, 
            IDeclarationCardOpener declarationCardOpener, DependencyTreeSection dependencyTreeSection, IDirectExecutor directExecutor )
            : base( context, workItem, view )
        {
            Contract.Requires( treeParameters != null );
            Contract.Requires( pyramidDataService != null );
            Contract.Requires( graphTreeDataManager != null );
            Contract.Requires( declarationUserPermissionsPolicy != null );
            Contract.Requires( nodeAllTreeProvider != null );
            Contract.Requires( nodeVisibleTreeProvider != null );
            Contract.Requires( nodeTableProvider != null );
            Contract.Requires( declarationCardOpener != null );
            Contract.Requires( dependencyTreeSection != null );
            Contract.Requires( directExecutor != null );

            _treeTabTreeParameters              = treeParameters;
            _pyramidDataService                 = pyramidDataService;
            _graphTreeDataManager               = graphTreeDataManager;
            _declarationUserPermissionsPolicy   = declarationUserPermissionsPolicy;
            _nodeAllTreeProvider                = nodeAllTreeProvider;
            _nodeVisibleTreeProvider            = nodeVisibleTreeProvider;
            _nodeTableProvider                  = nodeTableProvider;
            _declarationCardOpener              = declarationCardOpener;
            _dependencyTreeSection              = dependencyTreeSection;
            _directExecutor                     = directExecutor;
        }

        #region Overrides of Presenter<GraphTreeContainer>

        protected override void Dispose( bool disposing )
        {
            using ( _loadGraphTreeBranchAction )
            {
                _loadGraphTreeBranchAction = null;
            }
            base.Dispose( disposing );
        }

        #endregion

        public void StartLoadTree( 
            GraphTreeParameters treeParameters, IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> surs, bool forceCheckDeclarationExist = false )
        {
            StopDataLoadingAndClear( clearTreeNodes: true );

            GraphNodesKeyParameters keyParameters = treeParameters.KeyParameters;

            if ( !forceCheckDeclarationExist || IsTreeRelationEligibleOrShowError( treeParameters.KeyParameters.TaxPayerId ) )
            {
                int? layersLimit = _declarationUserPermissionsPolicy.GetTreeRelationMaxLayersLimit( keyParameters.IsPurchase );
                _treeTabTreeParameters = layersLimit.HasValue
                    ? GraphTreeParameters.New( treeParameters, graphLayersDepthLimit: layersLimit, isRoot: true ) 
                    : GraphTreeParameters.New( treeParameters, isRoot: true );

                CancellationTokenSource cancelSource = new CancellationTokenSource();

                _commandManager = View.InitAndDelayLoadTree( _treeTabTreeParameters, cancelSource );

                ReinitActions( new GraphTreeObjectTransformer( 
                    _treeTabTreeParameters, UserPermissionsPolicy, surs ), _commandManager, cancelSource );
            }
        }

        private bool IsTreeRelationEligibleOrShowError(TaxPayerId id)
        {
            bool success = _declarationUserPermissionsPolicy.IsTreeRelationEligible( id );
            if ( !success )
                View.ShowWarning( ResourceManagerNDS2.NotEnoughRightsToAccessGraphTree );
            
            return success;
        }

        /// <summary> Stops to process operations depending on data. </summary>
        /// <remarks> this method is called only in the main UI thread. </remarks>
        /// <param name="clearTreeNodes"> 'true' to clear graph tree. </param>
        public void StopDataLoadingAndClear( bool clearTreeNodes = false )
        {
            ICommandTransactionManager commandManager = _commandManager;
            _commandManager = null; //will be initiated again in StartLoadTree() on tree rebuild

            if ( clearTreeNodes && commandManager != null )    //otherwise there is nothing to clear yet
                commandManager.ExecuteInTransaction( "ClearTreeNodes", View.ClearTreeNodes );
            StopDataLoading( commandManager );
        }

        private static void StopDataLoading( ICommandTransactionManager commandManager )
        {
            using ( commandManager ) { }
        }

        private void ReinitActions( 
            GraphTreeObjectTransformer graphTreeObjectManager, ICommandTransactionManager commandManager, CancellationTokenSource cancelSource )
        {
            Contract.Requires( graphTreeObjectManager != null );
            Contract.Requires( _loadGraphTreeBranchAction != null );
            Contract.Requires( _infoNodeChildrenCountAction != null );
            Contract.Requires( _buildGraphTreeBranchAction != null );
            Contract.Requires( _infoDeductionDetailSumsAction != null );

            Action<Task> faultOrCancelHandler = 
                dataProcessorCompletionPar => OnDataProcessorFaultOrCancel( dataProcessorCompletionPar, commandManager );

            _buildGraphTreeBranchAction.Reinit( commandManager );
            IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphTreeNodeData>> 
                treeBranchDispatcher = _loadGraphTreeBranchAction.Reinit( commandManager, faultOrCancelHandler, cancelSource, graphTreeObjectManager );
            _infoNodeChildrenCountAction.Reinit( commandManager, faultOrCancelHandler, cancelSource, treeBranchDispatcher );
            _infoDeductionDetailSumsAction.Reinit( commandManager, faultOrCancelHandler, cancelSource );
        }

        private void OnDataProcessorFaultOrCancel( Task dataProcessorCompletion, ICommandTransactionManager commandManager )
        {
            StopDataLoading( commandManager );

            if ( !dataProcessorCompletion.IsCanceled )
                dataProcessorCompletion.Wait(); //to rethrow an exception only if it has been caused. 'dataProcessorCompletion' is already completed here
        }

        /// <summary> Parameters of the sheet with tree graph. </summary>
        public GraphTreeParameters TreeTabParameters { get { return _treeTabTreeParameters; } }

        /// <summary> Parameters of the sheet with tree table. </summary>
        public GraphTreeParameters TableTabParameters { get { return _tableTabTreeParameters; } }

        public IDeclarationUserPermissionsPolicy UserPermissionsPolicy
        {
            get { return _declarationUserPermissionsPolicy; }
        }

        public void InitActions()
        {
            _buildGraphTreeBranchAction         = View.CreateBuildGraphTreeBranchAction( _directExecutor );

            _loadGraphTreeBranchAction          = View.CreateLoadGraphTreeBranchAction( 
                _graphTreeDataManager, _directExecutor, buildBranchAction: _buildGraphTreeBranchAction, nodesDataLoadedHandler: OnNodesDataLoaded );
            _infoNodeChildrenCountAction        = View.CreateInfoNodeChildrenCountAction( _graphTreeDataManager, _directExecutor );
            _infoDeductionDetailSumsAction      = View.CreateDeductionDetailsAction( _graphTreeDataManager, _directExecutor );
        }

        public GraphTreeParameters InitializeTableTab( GraphTreeParameters treeParameters, TaxPayerId taxPayerId, int year, int quarter, bool isPurchase )
        {
            return _tableTabTreeParameters = GraphTreeParameters.New( treeParameters, taxPayerId, isPurchase, year, quarter );
        }

        /// <summary> Called in one background thread at the same time. </summary>
        private void OnNodesDataLoaded( 
            CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>> callContext, 
            CancellationToken cancelToken )
        {
            if ( callContext.Priority == CallExecPriority.Background )
                _infoNodeChildrenCountAction.OnNodesDataLoaded( callContext, cancelToken );
            else 
                _loadGraphTreeBranchAction.OnNodesDataLoaded( callContext, cancelToken );
        }

        private object GetTableTabNodesByPage( QueryConditions conditions, out long totalRows )
        {
            conditions.Filter = new List<FilterQuery>();    //apopov 5.2.2016	//is it really needed?

            _tabTabReportTable = GetTableTabNodesByPage( TableTabParameters.KeyParameters );
            ICollection<GraphDataNode> graphDatas = ExcludeRootLevelNodes( _tabTabReportTable );
            totalRows = graphDatas.Count;

            return graphDatas;
        }

        private ICollection<GraphDataNode> GetTableTabNodesByPage( 
            GraphNodesKeyParameters queryParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            OperationResult<PageResult<GraphData>> callResult = 
                ExecuteServiceCall(
                    cancelTokenPar => _pyramidDataService.LoadGraphNodeChildrenByPage( queryParameters ),
                    cancelToken );

            IEnumerable<GraphDataNode> result = null;
            if ( callResult.Status == ResultStatus.Success )
                result = callResult.Result.Rows.Select( graphData => new GraphDataNode( graphData ) );

            return result == null ? (ICollection<GraphDataNode>)Enumerable.Empty<GraphDataNode>() : result.ToArray();
        }

        private List<GraphDataNode> ExcludeRootLevelNodes(IEnumerable<GraphDataNode> graphDatas)
        {
            List<GraphDataNode> woRootNode = graphDatas.Where(node => node.Level > 1).ToList();

            return woRootNode;
        }

        public void ViewPyramidReport( GraphTreeParameters treeParameters, TaxPayerId taxPayerId, bool isByPurchase, int year, int qrt )
        {
            var cardData = GraphTreeParameters.New( treeParameters, taxPayerId, isByPurchase, year, qrt );
            base.ViewPyramidReport( cardData );
        }

        public GridSetup CreateGridSetup(string key)
        {
            var gridSetup = new GridSetup(SettingsProvider(key));
            gridSetup.GetData = GetTableTabNodesByPage;

            return gridSetup;
        }

        public void OpenDeclarationCard(string inn, string kpp, int year, int period, bool direction)
        {
            Direction dir = direction ? Direction.Purchase : Direction.Sales;
            CardOpenResult result = _declarationCardOpener.Open( 
                inn, kpp, ConvertYearToString( year ), ConvertQuarterToString( period ), dir );

            if ( !result.IsSuccess )
                View.ShowError( result.Message );
        }

        private static string ConvertYearToString( int year )
        {
            return NumericFormatHelper.ToString( year );
        }

        private static string ConvertQuarterToString( int period )
        {
            var declPeriod = "00";
            switch ( period )
            {
                case 1:
                    declPeriod = "21";
                    break;
                case 2:
                    declPeriod = "22";
                    break;
                case 3:
                    declPeriod = "23";
                    break;
                case 4:
                    declPeriod = "24";
                    break;
            }
            return declPeriod;
        }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        public IReadOnlyCollection<string> GetTaxpayersKpp(string inn)
        {
            return _graphTreeDataManager.GetTaxpayersKpp(inn);
        }

        #region Implementation of IInstanceProvider<ICommandTransactionManager>

        ICommandTransactionManager IInstanceProvider<ICommandTransactionManager>.Instance { get { return CommandManager; } }

#endregion Implementation of IInstanceProvider<ICommandTransactionManager>

        #region Excel export

        private INodeAllTreeReporter NodeAllTreeReporter
        {
            get
            {
                if ( _nodeAllTreeReporter == null )
                {
                    _nodeAllTreeReporter = _nodeAllTreeProvider.Instance;

                    _nodeAllTreeReporter.Init( progress: null );
                }
                return _nodeAllTreeReporter;
            }
        }

        private INodeVisibleTreeReporter NodeVisibleTreeReporter
        {
            get
            {
                if ( _nodeVisibleTreeReporter == null )
                {
                    _nodeVisibleTreeReporter = _nodeVisibleTreeProvider.Instance;

                    _nodeVisibleTreeReporter.Init( progress: null );
                }
                return _nodeVisibleTreeReporter;
            }
        }

        private INodeTableReporter NodeTableReporter
        {
            get
            {
                if ( _nodeTableReporter == null )
                {
                    _nodeTableReporter = _nodeTableProvider.Instance;
                    var progress = new Progress<Tuple<long?, bool?>>( OnExcelExportProgress );

                    _nodeTableReporter.Init( progress );
                }
                return _nodeTableReporter;
            }
        }

        private void OnExcelExportProgress( Tuple<long?, bool?> progress )
        {
            if ( progress.Item2.HasValue )
                View.SetExportExcelStateVisible( progress.Item2.Value );
            if ( progress.Item1.HasValue )
                View.SetExportExcelState( progress.Item1.Value );
        }

        private void OnErrorTableExcelExport( Exception exception, bool isCanceled, string reportFileFullSpec )
        {
            View.ShowError( string.Format( ResourceManagerNDS2.ExportExcelMessages.FileFormedError, Path.GetFileName( reportFileFullSpec ) ) );
        }

        private void OnErrorOrCancelAllTreeExcelExport( Exception exception, bool isCanceled, string reportFileFullSpec )
        {
            View.UpdateExcelAllButtonState();  
            if ( isCanceled )   //restores enabling of the report button that has been reset on <Cancel>
                View.SetAllTreeExcelButtonEnabled( enabled: true );
            else
                View.ShowError( string.Format( ResourceManagerNDS2.ExportExcelMessages.FileFormedError, Path.GetFileName( reportFileFullSpec ) ) );
        }

        private void OnErrorVisibleTreeExcelExport( Exception exception, bool isCanceled, string reportFileFullSpec )
        {
            if ( isCanceled )   //restores enabling of the report button that has been reset on <Cancel>
                View.SetVisibleTreeExcelButtonEnabled( enabled: true );
            else
                View.ShowError(string.Format(ResourceManagerNDS2.ExportExcelMessages.FileFormedError, Path.GetFileName(reportFileFullSpec)));
        }

        private void OnFinishingAlwaysTableExcelExport()
        {
            OnExcelExportProgress( Tuple.Create( (long?)null, (bool?)false ) );
        }

        private void OnFinishingAlwaysAllTreeExcelExport()
        {
            View.UpdateExcelAllButtonState();
            View.SetVisibleTreeExcelButtonEnabled(enabled: true); 
            View.SetPrintButtonEnabled(enabled: true);
        }

        private void OnFinishingAlwaysVisibleTreeExcelExport()
        {
            View.UpdateExcelVisibleButtonState(); 
            View.SetAllTreeExcelButtonEnabled(enabled: true);
            View.SetPrintButtonEnabled(enabled: true);
        }

        private void OnSuccessfullyFinishedTableExcelExport( string fileDirectorySpec, bool isExportCanceled )
        {
            if ( !isExportCanceled )
                View.OpenExcelFile( fileDirectorySpec );
        }

        private void OnSuccessfullyFinishedAllTreeExcelExport( string fileDirectorySpec )
        {
            if ( fileDirectorySpec != null )
                View.OpenExcelFile( fileDirectorySpec );
        }

        private void OnSuccessfullyFinishedVisibleTreeExcelExport( string fileDirectorySpec, bool isExportCanceled )
        {
            if ( isExportCanceled )   //restores enabling of the report button that has been reset on <Cancel>
                View.SetVisibleTreeExcelButtonEnabled( enabled: true );

            if ( !isExportCanceled && fileDirectorySpec != null )
                View.OpenExcelFile( fileDirectorySpec );
        }

        public 
#if !NET40
            async 
#endif //!NET40
            void StartGenerateTableExcelAsync( string filePath, GridControl dataGrid, GraphTreeNodeReportData treeTableData )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( filePath ) );
            Contract.Requires( dataGrid != null );
            Contract.Requires( treeTableData != null );

            IReadOnlyCollection<ColumnBase> columns = 
                NodeTableReporter.CopyVisibleColumnsOnly( dataGrid.Setup.Columns.ToReadOnly() );   //columns passed into a background thread
            bool isCanceled = false;
            _cancelSourceTableExport = new CancellationTokenSource();

#if NET40

            TaskScheduler uiTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            Task<string> buildReport = NodeTableReporter.StartReportBuildAsync(
                    filePath, columns, treeTableData, _tabTabReportTable, _cancelSourceTableExport.Token )
                .ContinueWith(
                    taskPrev =>
                    {
                        isCanceled = _cancelSourceTableExport.IsCancellationRequested;
                        _cancelSourceTableExport = null;

                        OnFinishingAlwaysTableExcelExport();

                        return taskPrev.Result;
                    },
                    uiTaskScheduler );

            buildReport.ContinueWith( 
                taskPrev => _directExecutor.Execute(
                    ( usellessPar, cancelTokenNonePar ) => OnErrorTableExcelExport( taskPrev.Exception, isCanceled, filePath ), 
                    _cancelSourceTableExport ),
                CancellationToken.None, 
                TaskContinuationOptions.NotOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                uiTaskScheduler );

            buildReport.ContinueWith( 
                taskPrev => _directExecutor.Execute(
                    ( usellessPar, cancelTokenNonePar ) => OnSuccessfullyFinishedTableExcelExport( taskPrev.Result, isCanceled ), 
                    _cancelSourceTableExport ),
                CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                uiTaskScheduler );

#else //!NET40

            string fileFullSpec = null;
            try
            {
                fileFullSpec = await NodeTableReporter.StartReportBuildAsync(
                    filePath, columns, treeTableData, _tabTabReportTable, _cancelSourceTableExport.Token );
            }
            catch ( Exception exception )
            {
                OnErrorTableExcelExport( exception, _cancelSourceTableExport.IsCancellationRequested, filePath )
            }
            finally
            {
                isCanceled = _cancelSourceTableExport.IsCancellationRequested;
                _cancelSourceTableExport = null;

                OnFinishingAlwaysTableExcelExport();
            }
            OnSuccessfullyFinishedTableExcelExport( fileFullSpec, isCanceled );

#endif //NET40
        }

#if ALLNODEREPORT_ATONETIME

        public
#if !NET40
            async 
#endif //!NET40
            void StartGenerateAllTreeExcelAsync( string filePath, GraphTreeNodeReportData treeTableData )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( filePath ) );
            Contract.Requires( treeTableData != null );

            int layersDepthLimitAllTree = GetLayersDepthLimitAllTree();
            int layerMaxNodesLimit = GetLayerMaxNodesExportLimit();
            _cancelSourceAllTreeExport = new CancellationTokenSource();

            View.SetVisibleTreeExcelButtonEnabled( enabled: false );

            GraphNodesKeyParameters reportKeyParameters = GraphNodesKeyParameters.New( _treeTabTreeParameters.KeyParameters, 
                layersLimit: layersDepthLimitAllTree, mostImportantContractors: layerMaxNodesLimit );
            bool isCanceled = false;

#if NET40

            TaskScheduler uiTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            Task<string> buildReport = NodeAllTreeReporter.StartReportBuildAsync(
                filePath, treeTableData, GetTableTabNodesByPage, reportKeyParameters, _cancelSourceAllTreeExport.Token )
                .ContinueWith(
                    taskPrev =>
                    { 
                        isCanceled = _cancelSourceAllTreeExport.IsCancellationRequested;
                        _cancelSourceAllTreeExport = null;

                        OnFinishingAlwaysAllTreeExcelExport();

                        return taskPrev.Result;
                    }, 
                    uiTaskScheduler );

            buildReport.ContinueWith(  
                taskPrev => _directExecutor.Execute(
                    ( usellessPar, cancelTokenNonePar ) => OnErrorOrCancelAllTreeExcelExport( taskPrev.Exception, isCanceled, filePath ), 
                    _cancelSourceAllTreeExport ),
                CancellationToken.None, 
                TaskContinuationOptions.NotOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                uiTaskScheduler );

            buildReport.ContinueWith( 
                taskPrev => _directExecutor.Execute(
                    ( usellessPar, cancelTokenNonePar ) => OnSuccessfullyFinishedAllTreeExcelExport( taskPrev.Result ), 
                    _cancelSourceAllTreeExport ),
                CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                uiTaskScheduler );

#else //!NET40

            string fileFullSpec = null;

            try
            {
                fileFullSpec = await NodeAllTreeReporter.StartReportBuildAsync(
                    filePath, treeTableData, GetTableTabNodesByPageImpl, reportKeyParameters, _cancelSourceAllTreeExport.Token );
            }
            catch ( Exception exception )
            {
                OnErrorOrCancelAllTreeExcelExport( exception, _cancelSourceAllTreeExport.IsCancellationRequested, filePath )
            }
            finally
            {
                isCanceled = _cancelSourceAllTreeExport.IsCancellationRequested;
                _cancelSourceAllTreeExport = null;

                OnFinishingAlwaysAllTreeExcelExport();
            }
            OnSuccessfullyFinishedAllTreeExcelExport( fileFullSpec );

#endif //NET40
        }

#else //!ALLNODEREPORT_ATONETIME

        public void StartGenerateAllTreeExcelAsync( string filePath, GraphTreeNodeReportData treeReportData )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( filePath ) );
            Contract.Requires( treeReportData != null );

            int layersDepthLimitAllTree = GetLayersDepthLimitAllTree();
            int layerMaxNodesLimit = GetLayerMaxNodesExportLimit();
            _cancelSourceAllTreeExport = new CancellationTokenSource();
            CancellationToken cancelToken = _cancelSourceAllTreeExport.Token;

            GraphTreeLimitedLayersNodeReportData limitedLayersReportData =
                new GraphTreeLimitedLayersNodeReportData( treeReportData, layersDepthLimitAllTree + 1 );

            View.SetVisibleTreeExcelButtonEnabled( enabled: false );
            View.SetPrintButtonEnabled( enabled: false );

            var ids = new[] { treeReportData.TaxPayerId };
            GraphNodesKeyParameters reportKeyParameters = GraphNodesKeyParameters.New(
                _treeTabTreeParameters.KeyParameters, mostImportantContractors: layerMaxNodesLimit, ids: ids);
           
            Action<string, Exception, CancellationToken, CancellationToken> onFinishHandler = 
                ( fileSpec, exception, dispatcherCancelToken, cancelTokenNonUsed ) =>
                {
                    _cancelSourceAllTreeExport = null;

                    OnFinishingAlwaysAllTreeExcelExport();

                    if ( dispatcherCancelToken.IsCancellationRequested || exception != null )
                        OnErrorOrCancelAllTreeExcelExport( exception, dispatcherCancelToken.IsCancellationRequested, filePath );    //does not use the result file specification in 'fileSpec' because it is not defined in error or cancel case
                    else
                        OnSuccessfullyFinishedAllTreeExcelExport( fileSpec );
                };

            _nodeAllTreeDispatcher = _graphTreeDataManager.InitServiceCallBatchDispatcher( 
                NodeAllTreeReporter.Process, NodeAllTreeReporter.FinishReportBuild, onFinishHandler, 
                layersDepthLimitAllTree, _cancelSourceAllTreeExport, "NodeAllTreeReporter.Process {0}, ID {1} GraphTreePresenter" );

            NodeAllTreeReporter.StartReportBuild( filePath, limitedLayersReportData, reportKeyParameters, cancelToken );

            _nodeAllTreeDispatcher.Start( reportKeyParameters, _commandManager.InstanceKey, contextKey: null );
        }

#endif //ALLNODEREPORT_ATONETIME

        public
#if !NET40
            async 
#endif //!NET40
            void StartGenerateVisibleTreeExcelAsync(string filePath, GraphTreeLimitedLayersNodeReportData treeTableData, IReadOnlyCollection<GraphDataNode> reportRows)
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( filePath ) );
            Contract.Requires( treeTableData != null );
            Contract.Requires( reportRows != null );

            _cancelSourceVisibleTreeExport = new CancellationTokenSource();

            View.SetAllTreeExcelButtonEnabled( enabled: false );
            View.SetPrintButtonEnabled( enabled: false );

            bool isCanceled = false;

#if NET40

            TaskScheduler uiTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            Task<string> buildReport = NodeVisibleTreeReporter.StartReportBuildAsync( 
                                filePath, treeTableData, reportRows, _cancelSourceVisibleTreeExport.Token )
                .ContinueWith(
                    taskPrev =>
                    { 
                        isCanceled = _cancelSourceVisibleTreeExport.IsCancellationRequested;
                        _cancelSourceVisibleTreeExport = null;

                        OnFinishingAlwaysVisibleTreeExcelExport();

                        return taskPrev.Result;
                    }, 
                    uiTaskScheduler );

                buildReport.ContinueWith(  
                    taskPrev => _directExecutor.Execute(
                        ( usellessPar, cancelTokenNonePar ) => OnErrorVisibleTreeExcelExport( taskPrev.Exception, isCanceled, filePath ), 
                        _cancelSourceVisibleTreeExport ),
                    CancellationToken.None, 
                    TaskContinuationOptions.NotOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                    uiTaskScheduler );

                buildReport.ContinueWith( 
                    taskPrev => _directExecutor.Execute(
                        ( usellessPar, cancelTokenNonePar ) => OnSuccessfullyFinishedVisibleTreeExcelExport( taskPrev.Result, isCanceled ), 
                        _cancelSourceVisibleTreeExport ),
                    CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously, 
                    uiTaskScheduler );

#else //!NET40

            string fileFullSpec = null;

            try
            {
                fileFullSpec = await NodeVisibleTreeReporter.StartReportBuildAsync(
                    filePath, treeTableData, GetTableTabNodesByPageImpl, _cancelSourceVisibleTreeExport.Token );
            }
            catch ( Exception exception )
            {
                OnErrorVisibleTreeExcelExport( exception, _cancelSourceVisibleTreeExport.IsCancellationRequested, filePath )
            }
            finally
            {
                isCanceled = _cancelSourceVisibleTreeExport.IsCancellationRequested;
                _cancelSourceVisibleTreeExport = null;

                OnFinishingAlwaysVisibleTreeExcelExport();
            }
            OnSuccessfullyFinishedVisibleTreeExcelExport( fileFullSpec, isCanceled );

#endif //NET40
        }

        public bool IsNowAllTreeExportToExcel { get { return _cancelSourceAllTreeExport != null; } }

        public bool IsNowVisibleTreeExportToExcel { get { return _cancelSourceVisibleTreeExport != null; } }

        public bool IsNowTableExportToExcel { get { return _cancelSourceTableExport != null; } }

        public void CancelAllTreeExportToExcel()
        {
            if ( IsNowAllTreeExportToExcel )
                _cancelSourceAllTreeExport.Cancel();
        }

        public void CancelVisibleTreeExportToExcel()
        {
            if ( IsNowVisibleTreeExportToExcel )
                _cancelSourceVisibleTreeExport.Cancel();
        }

        public void CancelTableExportToExcel()
        {
            if ( IsNowTableExportToExcel )
                _cancelSourceTableExport.Cancel();
        }

        public string GenerateAllTreeExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return NodeAllTreeReporter.GenerateExcelFileName( treeTableData );
        }

        public string GenerateVisibleTreeExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return NodeVisibleTreeReporter.GenerateExcelFileName( treeTableData );
        }

        public string GenerateTableExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return NodeTableReporter.GenerateExcelFileName( treeTableData );
        }

        public int GetLayersDepthLimitAllTree()
        {
            int layersDepthLimitAllTree = _dependencyTreeSection.ReportSettings.LayersDepthLimit;

            return layersDepthLimitAllTree;
        }

        public int GetLayerMaxNodesExportLimit()
        {
            int layerMaxNodesLimit = _dependencyTreeSection.ReportSettings.LayerMaxNodesExportLimit;

            return layerMaxNodesLimit;
        }

        #endregion Excel export
    }
}
