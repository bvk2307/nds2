﻿using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    /// <summary> Report parameters for mode of visible tree nodes for report <see cref="NodeTreePlainReporter"/>. </summary>
    public sealed class GraphTreeLimitedLayersNodeReportData : GraphTreeNodeReportData
    {
        public GraphTreeLimitedLayersNodeReportData( GraphTreeNodeData treeNodeData, GraphNodesKeyParameters nodesKeyParameters, int layersDepthLimit ) 
            : base( treeNodeData, nodesKeyParameters )
        {
            LayersLimit = layersDepthLimit;
        }

        public GraphTreeLimitedLayersNodeReportData( GraphTreeNodeReportData graphTreeNodeReportData, int layersDepthLimit ) 
            : base( graphTreeNodeReportData.NodeData, graphTreeNodeReportData.NodesKeyParameters )
        {
            LayersLimit = layersDepthLimit;
        }

        /// <summary> A limitation on requested node layer number. </summary>
        public int LayersLimit { get; private set; }
    }
}