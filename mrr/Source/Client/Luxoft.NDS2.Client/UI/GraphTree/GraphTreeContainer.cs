﻿#define NOKPPLIST
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using CommonComponents.Configuration;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using FLS.Common.Controls.Wpf.Brushes;
using FLS.Common.Lib.Collections.Extensions;
using FLS.Common.Lib.Primitives;
using FLS.CommonComponents.App.Processes;
using FLS.CommonComponents.Lib.Execution;
using FLS.CommonComponents.Lib.Interfaces;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Luxoft.NDS2.Client.Config;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Model.TaxPeriods;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.GraphTree.Actions;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.GraphTree
{ 
    public partial class GraphTreeContainer : BaseView, IInstanceProvider<ICommandTransactionManager>
    {
        #region Private members

        private const long MaxInnValue = 999999999999;

        private readonly IThreadInvokerWErrorHandling _threadInvoker;
        private readonly NodeTree _nodeTree;
        private readonly Int32Counter _commandTransactionCounter;

        private GraphTreeNodeReportData _treeReportData;
        private GraphTreeNodeReportData _tableReportData;
        private GraphTreeParameters _treeParameters;
        private Presenter _presenter = null;
        private FileProcessStarter _fileProcessStarter = null;

        /// <summary> Counter of disabling|enabling of all nodes Excel export button <see cref="SetAllTreeExcelButtonEnabled"/> according Excel report build state. </summary>
        private Int32Counter _excelExportDisablingAllExcelExportCounter = new Int32Counter();
        /// <summary> Counter of disabling|enabling of visible nodes Excel export button <see cref="SetVisibleTreeExcelButtonEnabled"/> according Excel report build state. </summary>
        private Int32Counter _excelExportDisablingVisibleExcelExportCounter = new Int32Counter();
        /// <summary> Counter of disabling|enabling of print button <see cref="SetVisibleTreeExcelButtonEnabled"/> according Excel report build state. </summary>
        private Int32Counter _excelExportDisablingPrintExcelExportCounter = new Int32Counter();
        
        /// <summary> Counter of disabling|enabling of all nodes Excel export button <see cref="SetAllTreeExcelButtonEnabled"/> according graph tree build state. </summary>
        private Int32Counter _excelExportDisablingAllTreeBuildCounter = null;
        /// <summary> Counter of disabling|enabling of visible nodes Excel export button <see cref="SetVisibleTreeExcelButtonEnabled"/> according graph tree build state. </summary>
        private Int32Counter _excelExportDisablingVisibleTreeBuildCounter = null;
        /// <summary> Counter of disabling|enabling of print button <see cref="SetVisibleTreeExcelButtonEnabled"/> according graph tree build state. </summary>
        private Int32Counter _excelExportDisablingPrintTreeBuildCounter = null;
         
        private IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> _surs = null;

        #endregion Private members

        public GraphTreeContainer( 
            PresentationContext context, WorkItem workItem, GraphTreeParameters tabTreeParameters, IThreadInvokerWErrorHandling threadInvoker )
            : base( context, workItem )
        {
            Contract.Requires( tabTreeParameters != null );
            Contract.Requires( threadInvoker != null );

            _threadInvoker = threadInvoker;

            InitializeComponent();

            innEditor.MaxValue = MaxInnValue;
            innEditor.ValueChanged += InnEditorOnValueChanged;

            optionSetByPurchase.CreationFilter = new OptionSetByPurchaseCreationFilter();

            toolTipControl.SetToolTip( reloadButton, ResourceManagerNDS2.ReloadButtonToolTip );
            toolTipControl.SetToolTip( excelAllButton, ResourceManagerNDS2.ExcelReportAllButtonToolTip );
            toolTipControl.SetToolTip( excelVisibleButton, ResourceManagerNDS2.ExcelReportVisibleButtonToolTip );

            var configurationDataService = workItem.Services.Get<IConfigurationDataService>();
            var dependencyTreeSection = configurationDataService.GetSection<DependencyTreeSection>(ProfileInfo.Default);
            var nodesLimit = dependencyTreeSection.TreeSettings.LayerMaxNodesLimit;
            _treeParameters = GraphTreeParameters.New( tabTreeParameters, mostImportantContractors: nodesLimit );

            SetTitle();
            var host = new ElementHost();
            treePanel.ClientArea.Controls.Add( host );
            host.Dock = DockStyle.Fill;
            _commandTransactionCounter = new Int32Counter();

            _nodeTree = new NodeTree();
            host.Child = _nodeTree;
        }

        private void SetTitle()
        {
            string title = string.Format("{0} (Дерево связей)", _treeParameters.KeyParameters.Inn);

            SetWindowTitle( title );
            SetWindowDesciption( title );
        }

        public override void OnLoad()
        {  
            base.OnLoad();

            SetFilter();  
             
            var surs = new Dictionary<int, Tuple<SolidColorBrush, string>>();
            foreach ( SurItem s in _presenter.Sur.Items )
            {
                surs.Add( s.Code, new Tuple<SolidColorBrush, string>( ArgbToColorBrush( s.ColorARGB ), s.Description ) );
            }
            _surs = surs;

            _nodeTree.InitGraph( _surs );

            _presenter.InitActions();

            StartLoadTree();
        }

        public override void OnClosing( WindowCloseContextBase closeContext, CancelEventArgs eventArgs )
        {
            bool cancelClosing = !IsCancelAndContinueExportTableExcelIfNeed();
            if ( !cancelClosing )
            {
                cancelClosing = !IsCancelAndContinueExportVisibleTreeExcelIfNeed();
                if ( !cancelClosing )
                    cancelClosing = !IsCancelAndContinueExportAllTreeExcelIfNeed();
            }
            if ( cancelClosing )
                eventArgs.Cancel = true;

            base.OnClosing( closeContext, eventArgs );
        }

        public override void OnUnload()
        {
            _presenter.StopDataLoadingAndClear();

            UnsubscribeAll();

            using ( _presenter ) { _presenter = null; }

            base.OnUnload();

	//apopov 13.7.2016	//DEBUG!!!
            this.Dispose();
        }

        public LoadGraphTreeBranchAction CreateLoadGraphTreeBranchAction( 
            IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor, 
            IBuildGraphTreeBranchAction buildBranchAction, 
            Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphTreeNodeData>>, CancellationToken> 
                nodesDataLoadedHandler )
        {
            return new LoadGraphTreeBranchAction( 
                _nodeTree, graphTreeDataManager, callExecutor, buildBranchAction, nodesDataLoadedHandler );
        }

        public InfoNodeChildrenCountAction CreateInfoNodeChildrenCountAction( 
            IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor )
        {
            return new InfoNodeChildrenCountAction( _nodeTree, graphTreeDataManager, callExecutor );
        }

        public BuildGraphTreeBranchAction CreateBuildGraphTreeBranchAction( IDirectExecutor callExecutor )
        {
            return new BuildGraphTreeBranchAction( this, callExecutor );
        }

        public InfoDeductionDetailSumsAction CreateDeductionDetailsAction( 
            IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor )
        {
            return new InfoDeductionDetailSumsAction( _nodeTree, graphTreeDataManager, callExecutor );
        }

        private void UnsubscribeAll()
        {
            _nodeTree.ShowTrayMessage                -= OnShowTrayMessage;
            _nodeTree.OpenDeclarationCard            -= OpenDeclarationCard;
            _nodeTree.ShowTableDataSelected          -= ShowTableData;
            _nodeTree.ShowTreeInNewWindowSelected    -= ShowTreeInNewWindow;
        }

        /// <summary> Loads tree branch nodes into the tree. </summary>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public bool BuildTreeBranch( Guid parentNodeKey, IEnumerable<GraphTreeNodeData> nodes )
        {
            GraphTreeNodeData parentNode = _nodeTree.BuildTreeBranch( parentNodeKey, nodes );

	//apopov 21.6.2016	//TODO!!!   //move implementation into event NodeTree.NewTreeBuilt and replace this method by NodeTree.BuildTreeBranch()
            bool success = parentNode != null;
            if ( parentNodeKey == GraphTreeNodeData.NoNodeKey && success )  //successful new tree build
            {
                _treeReportData = new GraphTreeNodeReportData( parentNode, _presenter.TreeTabParameters.KeyParameters );

                SetAllTreeExcelButtonEnabled(enabled: true, treeBuilding: true);
                SetVisibleTreeExcelButtonEnabled(enabled: true, treeBuilding: true);
                SetPrintButtonEnabled(enabled: true, treeBuilding: true);
            }
            return success;
        }

        private void OnShowTrayMessage( object sender, GraphTreeMessageEventArgs graphTreeMessageEventArgs )
        {
            ShowWarning( graphTreeMessageEventArgs.Message );
        }

        private void SetFilter()
        { 
            try
            { 
                innEditor.Value = _treeParameters.KeyParameters.Inn;
            }
            catch (Exception ex)
            {
                throw new Exception("Неверное значениеи ИНН", ex);
            }

            optionSetByPurchase.CheckedIndex = _treeParameters.KeyParameters.IsPurchase ? 1 : 0;
            percentEditor.Value = _treeParameters.KeyParameters.SharedNdsPercent;
            countEditor.Value = _treeParameters.MaxNodesInLevel;
            SetTaxPeriodModel(_presenter.TaxPeriodModel);
            SetExcelExportAllButtonVisibility(); 
        }

        private void SetExcelExportAllButtonVisibility()
        {
            if (!_presenter.UserPermissionsPolicy.IsTreeRelationAllTreeReportEligible())
            {
                excelAllButton.Visible = false;
            }
        }

        public void SetIsPurchase(bool isPurchase)
        {
            optionSetByPurchase.CheckedIndex = isPurchase ? 1 : 0;
            _treeParameters = GraphTreeParameters.New(_treeParameters, isByPurchase: isPurchase);
            _nodeTree.TreeParameters = _treeParameters;
        }

        private void SetTaxPeriodModel(TaxPeriodModel taxPeriodModel)
        {
            var periods = taxPeriodModel.TaxPeriods.Where(p => !string.IsNullOrEmpty(p.Code)).ToList();
            comboTaxPeriod.DataSource = periods;
            SetTaxPeriodInitialValue(periods);
            comboTaxPeriod.Refresh();
        }

        private void SetTaxPeriodInitialValue(IList<TaxPeriodBase> taxPeriods)
        {
            if (taxPeriods.Any())
            {
                comboTaxPeriod.SelectedIndex = -1;
                int index = 0;
                foreach (var item in taxPeriods)
                {
                    if (CompareTaxPeriod(item, _treeParameters.KeyParameters.TaxYear, _treeParameters.KeyParameters.TaxQuarter))
                    {
                        comboTaxPeriod.SelectedIndex = index;
                        break;
                    }
                    index++;
                }
            }
            else
                comboTaxPeriod.SelectedIndex = -1;
        }

        private bool CompareTaxPeriod(TaxPeriodBase taxPeriod, int year, int qrt)
        {
            try
            {
                return taxPeriod != null && Convert.ToInt32(taxPeriod.Year) == year && Convert.ToInt32(taxPeriod.Code) - 20 == qrt;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void GetComboTaxPeriodValue(out int? year, out int? qrt)
        {
            year = null;
            qrt = null;

            if (comboTaxPeriod.SelectedIndex > -1)
            {
                var taxPeriod = (TaxPeriodBase)comboTaxPeriod.SelectedItem.ListObject;

                try
                {
                    year = Convert.ToInt32(taxPeriod.Year);
                }
                catch (Exception)
                {
                    year = null;
                }

                try
                {
                    qrt = Convert.ToInt32(taxPeriod.Code) - 20;
                }
                catch (Exception)
                {
                    qrt = null;
                }
            }
        }

        private void ShowTreeInNewWindow( object sender, GraphTreeEventArgs e )
        {
            _presenter.ViewPyramidReport( _treeParameters, e.NodeData.TaxPayerId, e.IsPurchase, e.TaxYear, e.TaxQuarter );
        }

        private void ShowTableData( object sender, GraphTreeEventArgs e )
        {
            GraphTreeNodeData nodeData = e.NodeData;
            GraphTreeParameters filter = _presenter.InitializeTableTab( 
                                            _treeParameters, nodeData.TaxPayerId, e.TaxYear, e.TaxQuarter, e.IsPurchase );

            _tableReportData = new GraphTreeNodeReportData( nodeData, _presenter.TableTabParameters.KeyParameters );

            lbInnVal.Text           = _tableReportData.TaxPayerId.Inn;
            lbNameVal.Text          = _tableReportData.Name;
            lbDeductionNdsVal.Text  = _tableReportData.DeductionNdsVal;
            lbClacNdsVal.Text       = _tableReportData.ClacNdsVal;
            lbNdsPrcVal.Text        = _tableReportData.NdsPercentageVal;
            lbIsPurchase.Text       = _tableReportData.ByPurchaseOrSellerText;
            lbSurVal.Text           = nodeData.SurDescription;
            lbRegionVal.Text        = _tableReportData.RegionName;
            lbLevelVal.Text         = _tableReportData.Level;
            lbInspectionVal.Text    = _tableReportData.Soun;
            lbIsBiggestVal.Text     = nodeData.GraphDataNode.IsLarge;    
            InitializeGrid();

            GridSetup setup = _presenter.CreateGridSetup( string.Format( "{0}_InitializeGraphTreeNodeGrid", GetType() ) );
            SetupGrid( setup, filter.KeyParameters.IsPurchase );

            tableTabPageControl.Enabled = true;
            this.treeTabControl.SelectedTab = this.treeTabControl.Tabs[1];
        }

        private void OpenDeclarationCard(object sender, GraphTreeEventArgs e)
        {
            if (e.NodeData != null)
            {
                _presenter.OpenDeclarationCard(e.NodeData.Inn, e.NodeData.Kpp, e.TaxYear, e.TaxQuarter, e.IsPurchase);
            }
        }

        public void AcceptPresenter( Presenter p )
        {
            _presenter = p;

            _nodeTree.Init( _threadInvoker, this, _commandTransactionCounter );

            _nodeTree.ShowTreeInNewWindowSelected    += ShowTreeInNewWindow;
            _nodeTree.ShowTableDataSelected          += ShowTableData;
            _nodeTree.OpenDeclarationCard            += OpenDeclarationCard;
            _nodeTree.ShowTrayMessage                += OnShowTrayMessage;
        }

        private void ReloadButtonClick(object sender, EventArgs e)
        {
            StartLoadTree( forceCheckDeclarationExist: true );
        }

        private void PrintButtonClick(object sender, EventArgs e)
        {
            var dlg = new System.Windows.Controls.PrintDialog();
            dlg.PrintTicket.PageOrientation = System.Printing.PageOrientation.Landscape; 
            var result = dlg.ShowDialog();

            if ( result == true )
            {
                _nodeTree.GraphPrintManager.Print(dlg);   
            } 
        }

        private void ExportVisibleTree() 
        {  
            ICollection<GraphTreeNodeData> nodes = _nodeTree.GetVisibleNonGroupDirectNodeDatas();

            int layersDepthLimit = 0;
            IReadOnlyCollection<GraphDataNode> filteredNodesData = 
                nodes.Select( 
                    n =>
                    {
                        if ( layersDepthLimit < n.TreeLevel )
                            layersDepthLimit = n.TreeLevel;

                        return n.GraphDataNode;
                    }
                ).ToReadOnly();

            Contract.Assume( layersDepthLimit >= 0 );

            var visibleReportData = new GraphTreeLimitedLayersNodeReportData(_treeReportData, layersDepthLimit + 1);

            ExportVisibleTreeExcel( visibleReportData,
                _presenter.GenerateVisibleTreeExcelFileName( visibleReportData ), filteredNodesData );
        }

        public virtual void AllTreeExcelButtonClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if ( _presenter.IsNowAllTreeExportToExcel )
            {
                IsCancelAndContinueExportAllTreeExcelIfNeed();
            }
            else
            { 
                ExportAllTreeExcel( _treeReportData, _presenter.GenerateAllTreeExcelFileName( _treeReportData ) );
            }
        }
         
        public virtual void VisibleTreeExcelButtonClick(object sender, EventArgs e)
        {
            if ( _presenter.IsNowVisibleTreeExportToExcel )
            {
                IsCancelAndContinueExportVisibleTreeExcelIfNeed();
            }
            else
            {
                ExportVisibleTree();
            } 
        }

        public void UpdateExcelAllButtonState(bool updateStateToCancel = false)
        {
            if (updateStateToCancel)
            {
                excelAllButton.Text = ResourceManagerNDS2.CancelExcelReportButtonText;
                toolTipControl.SetToolTip(excelAllButton, ResourceManagerNDS2.CancelExcelReportButtonToolTip); 
            }
            else
            {
                excelAllButton.Text = ResourceManagerNDS2.ExcelReportAllButtonText;
                toolTipControl.SetToolTip(excelAllButton, ResourceManagerNDS2.ExcelReportVisibleButtonText); 
            }  
        }

        public void UpdateExcelVisibleButtonState(bool updateStateToCancel = false)
        {
            if ( updateStateToCancel )
            {
                excelVisibleButton.Text = ResourceManagerNDS2.CancelExcelReportButtonText;
                toolTipControl.SetToolTip( excelVisibleButton, ResourceManagerNDS2.CancelExcelReportButtonToolTip );
            }
            else
            {
                excelVisibleButton.Text = ResourceManagerNDS2.ExcelReportVisibleButtonText;
                toolTipControl.SetToolTip(excelVisibleButton, ResourceManagerNDS2.ExcelReportVisibleButtonToolTip);
            }
        }
         
        public void SetAllTreeExcelButtonEnabled( bool enabled )
        {
            SetAllTreeExcelButtonEnabled( enabled, treeBuilding: false );
        }

        private void SetAllTreeExcelButtonEnabled( bool enabled, bool treeBuilding )
        {
            if ( enabled )
            {
                if ( treeBuilding )
                    --_excelExportDisablingAllTreeBuildCounter;
                else
                    --_excelExportDisablingAllExcelExportCounter;
            }
            if (_excelExportDisablingAllTreeBuildCounter == 0 && _excelExportDisablingAllExcelExportCounter == 0)
            {
                excelAllButton.Enabled = enabled; 
            }
            if ( !enabled )
            {
                if ( treeBuilding )
                    ++_excelExportDisablingAllTreeBuildCounter;
                else
                    ++_excelExportDisablingAllExcelExportCounter;
            }
        }

        public void SetVisibleTreeExcelButtonEnabled( bool enabled )
        {
            SetVisibleTreeExcelButtonEnabled( enabled, treeBuilding: false );
        }

        private void SetVisibleTreeExcelButtonEnabled( bool enabled, bool treeBuilding )
        {
            if ( enabled )
            {
                if ( treeBuilding )
                    --_excelExportDisablingVisibleTreeBuildCounter;
                else
                    --_excelExportDisablingVisibleExcelExportCounter;
            }
            if (_excelExportDisablingVisibleTreeBuildCounter == 0 && _excelExportDisablingVisibleExcelExportCounter == 0)
            {
                excelVisibleButton.Enabled = enabled;
            }
            if ( !enabled )
            {
                if ( treeBuilding )
                    ++_excelExportDisablingVisibleTreeBuildCounter;
                else
                    ++_excelExportDisablingVisibleExcelExportCounter;
            }
        }
         
        public void SetPrintButtonEnabled( bool enabled )
        {
            SetPrintButtonEnabled(enabled, treeBuilding: false);
        }

        private void SetPrintButtonEnabled( bool enabled, bool treeBuilding )
        {
            if ( enabled )
            {
                if ( treeBuilding )
                    --_excelExportDisablingPrintTreeBuildCounter;
                else
                    --_excelExportDisablingPrintExcelExportCounter;
            }
            if (_excelExportDisablingPrintTreeBuildCounter == 0 && _excelExportDisablingPrintExcelExportCounter == 0)
            {
                printButton.Enabled = enabled;
            }
            if ( !enabled )
            {
                if ( treeBuilding )
                    ++_excelExportDisablingPrintTreeBuildCounter;
                else
                    ++_excelExportDisablingPrintExcelExportCounter;
            }
        }
          
        private void StartLoadTree( bool forceCheckDeclarationExist = false )
        {
            _excelExportDisablingAllTreeBuildCounter = new Int32Counter();  //each tree build resets enabling|disabling counter of all nodes Excel export like in a new view start
            _excelExportDisablingVisibleTreeBuildCounter = new Int32Counter();  //each tree build resets enabling|disabling counter of visible nodes Excel export like in a new view start
            _excelExportDisablingPrintTreeBuildCounter = new Int32Counter();  //each tree build resets enabling|disabling counter of print like in a new view start

            SetAllTreeExcelButtonEnabled( enabled: false, treeBuilding: true );
            SetVisibleTreeExcelButtonEnabled( enabled: false, treeBuilding: true );
            SetPrintButtonEnabled( enabled: false, treeBuilding: true );

            _treeParameters = ReadCurrentKeyParameters( _treeParameters );
            SetTitle();

            _presenter.StartLoadTree( _treeParameters, _surs, forceCheckDeclarationExist );
        }

        private GraphTreeParameters ReadCurrentKeyParameters( GraphTreeParameters templateParameters )
        {
            string inn = innEditor.Value.ToString();

            string kpp = string.Empty;

#if !NOKPPLIST

            if ( comboKpp.SelectedItem != null )
                kpp = comboKpp.SelectedItem.ToString();

#endif //!NOKPPLIST

            bool isPurchase = (bool)optionSetByPurchase.CheckedItem.DataValue;

            bool? showReverted = null;

            if ( !isPurchase )
            {
                showReverted = showRevertedCheckbox.Checked;
            }

            int? year;
            int? qrt;
            GetComboTaxPeriodValue( out year, out qrt );

            int? percent;
            try
            {
                percent = Convert.ToInt32( percentEditor.Value );
            }
            catch ( Exception )
            {
                percent = null;
            }

            int? count;
            try
            {
                count = Convert.ToInt32( countEditor.Value );
            }
            catch ( Exception )
            {
                count = null;
            }

	//apopov 21.4.2016	//TODO!!!
            return GraphTreeParameters.New(templateParameters, new TaxPayerId(inn, kpp), isPurchase, year, qrt, percent, count, showReverted: showReverted);
        }

        /// <summary> Creates a new command manager and delays a request to load root data later. </summary>
        /// <param name="graphTreeParameters"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        public ICommandTransactionManager InitAndDelayLoadTree( GraphTreeParameters graphTreeParameters, CancellationTokenSource cancelSource )
        {
            return _nodeTree.InitAndDelayLoadTree( graphTreeParameters, cancelSource );
        }

        public void ClearTreeNodes()
        {
            _nodeTree.ClearTreeNodes();
        }

        #region Setup grid
        private void InitializeGrid()
        {
            ultraGridPanel.ClientArea.Controls.Clear();
            grid = new GridControl();
            grid.AllowRowFiltering = DefaultableBoolean.False;
            grid.AggregatePanelVisible = true;
            grid.ColumnsAutoFitStyle = AutoFitStyle.ResizeAllColumns;
            grid.Dock = DockStyle.Fill;
            grid.FooterVisible = true;
            grid.PanelLoadingVisible = false;
            grid.PanelPagesVisible = true;
            grid.ExportExcelVisible = true;
            grid.ExportExcelCancelVisible = false;
            grid.SetExportExcelCancelEnabled( isEnabled: false );
            grid.FilterResetVisible = false;
            SetExportExcelState( "Экспорт в MS Excel" );

            ultraGridPanel.ClientArea.Controls.Add(grid);
        }

        private void SetupGrid( GridSetup setup, bool isPurchase )
        {
            var helper = new GridSetupHelper<GraphDataNode>();

            var formatInfoZeroEmpty = new IntValueZeroFormatter();

            #region Декларация по продавцу/покупателю

            //Assigning ColumnDefinition's Caption and ToolTip properties below overrides display attributes 
            //for model object' properites (GraphData). So this is made only where it's needed.
            var purchaseString = isPurchase ? "продавца" : "покупателя";
            var firstGroupName = isPurchase ? "Данные по продавцу" : "Данные по покупателю";

            var group = new ColumnGroupDefinition { Caption = firstGroupName, RowSpan = 1 };

            group.Columns.Add(helper.CreateColumnDefinition(o => o.Inn, d =>
            {
                d.Caption = string.Format("ИНН {0}", purchaseString);
                d.ToolTip = string.Format("ИНН {0}", purchaseString);
            }));
            group.Columns.Add(helper.CreateColumnDefinition(o => o.DeclarationType));
            //group.Columns.Add(helper.CreateSurColumn(o => o.Sur, _presenter.Sur, 3));
            group.Columns.Add(helper.CreateColumnDefinition(o => o.NdsPercentage, d =>
            {
                d.Caption = string.Format("Доля вычетов в исчисленном НДС у {0}", purchaseString); 
                d.ToolTip = string.Format("Доля вычетов в исчисленном НДС у {0}", purchaseString);
                d.FormatInfo = formatInfoZeroEmpty;
            }));
            group.Columns.Add(helper.CreateColumnDefinition(o => o.DeductionNds, d =>
            {
                d.Caption = string.Format( "Сумма НДС, подлежащая вычету у {0}, ВСЕГО", purchaseString );
                d.ToolTip = string.Format( "Сумма НДС, подлежащая вычету у {0}, ВСЕГО", purchaseString );
                d.FormatInfo = formatInfoZeroEmpty;
            }));
            group.Columns.Add(helper.CreateColumnDefinition(o => o.CalcNds, d =>
            {
                d.Caption = string.Format("НДС исчисленный у {0}, ВСЕГО", purchaseString);
                d.ToolTip = string.Format("НДС исчисленный у {0}, ВСЕГО", purchaseString);
                d.FormatInfo = formatInfoZeroEmpty;
            }));
            group.Columns.ForEach(c =>c.SortIndicator = false);
            setup.Columns.Add(group);

            #endregion

            #region Данные декларации покупателя/продавца

            var secondGroupName = isPurchase ? "Данные декларации продавца" : "Данные декларации покупателя";
            group = new ColumnGroupDefinition() { Caption = secondGroupName, RowSpan = 1 };

            if (isPurchase)
            {
                group.Columns.Add(helper.CreateColumnDefinition(o => o.ParentNotMappedAmnt, 
                    d => d.FormatInfo = formatInfoZeroEmpty));

                group.Columns.Add(helper.CreateColumnDefinition(o => o.ParentClientNdsPercentage, 
                    d => d.FormatInfo = formatInfoZeroEmpty));
            }
            else
            {
                group.Columns.Add(helper.CreateColumnDefinition(o => o.ParentMappedAmnt, 
                    d => d.FormatInfo = formatInfoZeroEmpty));

                group.Columns.Add(helper.CreateColumnDefinition(o => o.ParentSellerNdsPercentage, 
                    d => d.FormatInfo = formatInfoZeroEmpty));
            }
            group.Columns.ForEach(c => c.SortIndicator = false);
            setup.Columns.Add(group);

            #endregion

            #region Данные декларации продавца/покупателя

            var thirdGroupName = isPurchase ? "Данные декларации покупателя" : "Данные декларации продавца";
            group = new ColumnGroupDefinition { Caption = thirdGroupName, RowSpan = 1 };

            if (isPurchase)
            {
                group.Columns.Add(helper.CreateColumnDefinition(o => o.NotMappedAmnt, 
                    d => d.FormatInfo = formatInfoZeroEmpty));

                group.Columns.Add(helper.CreateColumnDefinition(o => o.ClientNdsPercentage, 
                    d => d.FormatInfo = formatInfoZeroEmpty));
            }
            else
            {
                group.Columns.Add(helper.CreateColumnDefinition(o => o.MappedAmount, 
                    d => d.FormatInfo = formatInfoZeroEmpty));

                group.Columns.Add(helper.CreateColumnDefinition(o => o.SellerNdsPercentage, 
                    d => d.FormatInfo = formatInfoZeroEmpty));
            }
            group.Columns.ForEach(c => c.SortIndicator = false);
            setup.Columns.Add(group);

            #endregion

            #region Данные по расхождениям

            group = new ColumnGroupDefinition { Caption = "Данные по расхождениям", RowSpan = 1 };

            if (isPurchase)
            {
                //group.Columns.Add(helper.CreateColumnDefinition(o => o.GapDiscrepancyTotal, d =>
                //    {
                //        d.Caption = "Сумма разрывов по продавцу (по стороне продавца), ВСЕГО";
                //        d.ToolTip = "Сумма разрывов по продавцу (по стороне продавца), ВСЕГО";
                //        d.FormatInfo = formatInfoZeroEmpty;
                //    }));
            }
            else
            {
                group.Columns.Add(helper.CreateColumnDefinition(o => o.GapDiscrepancyTotal,d =>
                    {
                        d.Caption = "Сумма разрывов по покупателю (по стороне покупателя), ВСЕГО";
                        d.ToolTip = "Сумма разрывов по покупателю (по стороне покупателя), ВСЕГО";
                        d.FormatInfo = formatInfoZeroEmpty;
                    }));
            }

            //var colName = isPurchase ? "Сумма завышения вычетов по НДС по продавцу (по сторне продавца), ВСЕГО" : "Сумма завышения вычетов по НДС по покупателю (по стороне покупателя),  ВСЕГО";
            //group.Columns.Add(helper.CreateColumnDefinition(o => 0, d =>
            //    {
            //        d.Caption = colName;
            //        d.ToolTip = colName;
            //        d.FormatInfo = formatInfoZeroEmpty;
            //    }));

            group.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancyAmnt, 
                d => d.FormatInfo = formatInfoZeroEmpty));

            //group.Columns.Add(helper.CreateColumnDefinition(o => 0, d =>
            //    {
            //        d.Caption = "Сумма превышения НДС у покупателя с продавцом";
            //        d.ToolTip = "Сумма превышения НДС у покупателя с продавцом";
            //        d.FormatInfo = formatInfoZeroEmpty;
            //    }));
            group.Columns.ForEach(c => c.SortIndicator = false);
            setup.Columns.Add(group);

            #endregion

            grid.OnExportExcel += OnExportTableExcel;
            grid.OnExportExcelCancel += OnExportTableExcelCancel;

            grid.Setup = setup;
        }

        private SolidColorBrush ArgbToColorBrush( int argb )
        {
            System.Drawing.Color color = System.Drawing.Color.FromArgb( argb );
            var brush = SolidColorBrushHelper.GetColorBrush( Color.FromArgb( color.A, color.R, color.G, color.B ) );
            brush.Freeze(); //does not need of static handlers of bruch color changes inside SolidBrush and must not change the brush later (and in an other thread) 

            return brush;
        }
        #endregion

        private void OnExportTableExcel( object sender, EventArgs e )
        {
            ExportTableExcel( _tableReportData, _presenter.GenerateTableExcelFileName( _tableReportData ) );
        }

        private void OnExportTableExcelCancel( object sender, EventArgs eventArgs )
        {
            IsCancelAndContinueExportTableExcelIfNeed();
        }

        public void SetExportExcelStateVisible(bool isVisible)
        {
            grid.SetExportExcelEnabled( isEnabled: !isVisible );
            grid.ExportExcelVisible = !isVisible;
            grid.ExportExcelCancelVisible = isVisible;
            grid.SetExportExcelCancelEnabled( isEnabled: isVisible );
            grid.PanelExportExcelStateVisible = isVisible;
            grid.SetPanelExportExcelStateWidth( 230 );
        }

        public void SetExportExcelState( long rowsCount )
        {
            SetExportExcelState( string.Join( " ", rowsCount.ToString( CultureInfo.InvariantCulture ), "строк экспортировано" ) );
        }

        public void SetExportExcelState( string stateMessage )
        {
            grid.SetExportExcelState( stateMessage );
        }

        private void ExportTableExcel( GraphTreeNodeReportData reportData, string fileFullSpec )
        {
            Contract.Requires( reportData != null );
            Contract.Requires( !string.IsNullOrWhiteSpace( fileFullSpec ) );

            bool run = ShowDialogIsFileSave( fileFullSpec );
            if ( run )
                _presenter.StartGenerateTableExcelAsync( _saveFileDialogReport.FileName, grid, reportData );
        }

        private void ExportAllTreeExcel( GraphTreeNodeReportData reportData, string fileFullSpec )
        {
            Contract.Requires( reportData != null );
            Contract.Requires( !string.IsNullOrWhiteSpace( fileFullSpec ) );

            bool run = ShowDialogIsFileSave( fileFullSpec );
            if ( run )
            {
                _presenter.StartGenerateAllTreeExcelAsync(_saveFileDialogReport.FileName, reportData);
                UpdateExcelAllButtonState(true);                 
            }
        }

        private void ExportVisibleTreeExcel(GraphTreeLimitedLayersNodeReportData reportData, string fileFullSpec, IReadOnlyCollection<GraphDataNode> reportRows)
        {
            Contract.Requires( reportData != null );
            Contract.Requires( !string.IsNullOrWhiteSpace( fileFullSpec ) );
            Contract.Requires( reportRows != null );

            bool run = ShowDialogIsFileSave( fileFullSpec );
            if ( run )
            {
                _presenter.StartGenerateVisibleTreeExcelAsync(_saveFileDialogReport.FileName, reportData, reportRows);
                UpdateExcelVisibleButtonState(true);            
            }
        }

        private bool IsCancelAndContinueExportAllTreeExcelIfNeed()
        {
            bool isCancelledAndShoudContinue = true;
            if ( _presenter.IsNowAllTreeExportToExcel )
            { 
                isCancelledAndShoudContinue = ShowDialogIsExportCancel();
                if ( isCancelledAndShoudContinue )
                { 
                    SetAllTreeExcelButtonEnabled(false);
                    _presenter.CancelAllTreeExportToExcel(); 
                } 
            }
            return isCancelledAndShoudContinue;
        }

        private bool IsCancelAndContinueExportVisibleTreeExcelIfNeed()
        {
            bool isCancelledAndShoudContinue = true;
            if ( _presenter.IsNowVisibleTreeExportToExcel )
            { 
                isCancelledAndShoudContinue = ShowDialogIsExportCancel();
                if ( isCancelledAndShoudContinue )
                { 
                    SetVisibleTreeExcelButtonEnabled(false);
                    _presenter.CancelVisibleTreeExportToExcel(); 
                } 
            }
            return isCancelledAndShoudContinue;
        }

        private bool IsCancelAndContinueExportTableExcelIfNeed()
        {
            bool isCancelledAndShoudContinue = true;
            if ( _presenter.IsNowTableExportToExcel )
            {
                isCancelledAndShoudContinue = ShowDialogIsExportCancel();
                if (isCancelledAndShoudContinue) { }
                    _presenter.CancelTableExportToExcel();
            }
            return isCancelledAndShoudContinue;
        }

        private bool ShowDialogIsFileSave( string fileFullSpec )
        {
            _saveFileDialogReport.Title = ResourceManagerNDS2.ExportExcelMessages.ExcelExportDialogTitle;
            _saveFileDialogReport.FileName = fileFullSpec;
            const string fileExtensionWDot = BaseNodeReporter<NodeReportManager>.ReportExcelFileExtensionName;
            _saveFileDialogReport.DefaultExt = string.Concat( ".", fileExtensionWDot );
            _saveFileDialogReport.Filter = string.Format( "{0} отчёты|*{1}", ResourceManagerNDS2.ExportExcelMessages.MSExcelAppName, fileExtensionWDot );
            DialogResult dlgRez = _saveFileDialogReport.ShowDialog();

            return dlgRez == DialogResult.OK || dlgRez == DialogResult.Yes;
        }

        private bool ShowDialogIsExportCancel()
        {
            DialogResult ret = ShowQuestion( ResourceManagerNDS2.ExportExcelMessages.ExcelExportDialogTitle, ResourceManagerNDS2.ExportExcelMessages.DoFileExportCancel );

            return ret == DialogResult.Yes;
        }

        public void OpenExcelFile( string reportExcelFilePath )
        {
            ProcessStarter.StartFileProcessAndShowErrorIfPresent(
                reportExcelFilePath, ResourceManagerNDS2.ExportExcelMessages.ExcelExportDialogTitle,
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileFormedIsItOpen, reportExcelFilePath ),
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileExcelOpenError, reportExcelFilePath ) );
        }

        private void InnEditorOnValueChanged(object sender, EventArgs eventArgs)
        { 
            var text = innEditor.Text;

            if (!string.IsNullOrEmpty(text) && (text.Length == 10 || text.Length == 12))
            {
#if !NOKPPLIST
                var kpps = _presenter.GetTaxpayersKpp(text);
                comboKpp.DataSource = kpps;
                comboKpp.SelectedIndex = kpps.Count > 0 ? 0 : -1;
                comboKpp.ReadOnly = kpps.Count < 2;
#endif //!NOKPPLIST
                SetButtonsEnabled(true);
            }
            else
            {
                SetButtonsEnabled(false);
#if !NOKPPLIST
                comboKpp.SelectedIndex = -1;
#endif //!NOKPPLIST
            }
        }

        private void SetButtonsEnabled(bool enabled)
        {
            excelAllButton.Enabled = enabled;
            excelVisibleButton.Enabled = enabled;
            reloadButton.Enabled = enabled;
            printButton.Enabled = enabled;
        }

        private void OptionSetByPurchaseOnValueChanged(object sender, EventArgs e)
        {
            if ( optionSetByPurchase.CheckedIndex == 0 )
            {
                showRevertedCheckbox.Visible = true;
            }
            else
            {
                showRevertedCheckbox.Visible = false; 
            }
        }


        private void InnFilterChanged(object sender, EventArgs e)
        {
            var text = ((UltraMaskedEdit)sender).Text;

            if (!string.IsNullOrEmpty(text) && (text.Length == 10 || text.Length == 12))
            {
                var kpps = _presenter.GetTaxpayersKpp(text);
                comboKpp.DataSource = kpps;
                comboKpp.SelectedIndex = kpps.Count > 0 ? 0 : -1;
                comboKpp.ReadOnly = kpps.Count < 2;
            }
            else
            {
                comboKpp.SelectedIndex = -1;
            }
        }

        private FileProcessStarter ProcessStarter
        {
            get
            {
                return _fileProcessStarter ?? ( _fileProcessStarter = new FileProcessStarter( MessageService ) );
            }
        }

        #region Implementation of IInstanceProvider<ICommandTransactionManager>

        ICommandTransactionManager IInstanceProvider<ICommandTransactionManager>.Instance
        {
            get { return ( (IInstanceProvider<ICommandTransactionManager>)_presenter ).Instance; }
        }

        #endregion Implementation of IInstanceProvider<ICommandTransactionManager>  
    }
}
