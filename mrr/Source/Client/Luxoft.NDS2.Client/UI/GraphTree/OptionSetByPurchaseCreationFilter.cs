﻿using System.Drawing;

using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    public class OptionSetByPurchaseCreationFilter : IUIElementCreationFilter
    {
        void IUIElementCreationFilter.AfterCreateChildElements(UIElement parent)
        {
            if (parent is OptionSetEmbeddableUIElement)
            {
                foreach (UIElement element in parent.ChildElements)
                {
                    if (element is OptionSetOptionButtonUIElement)
                    {
                        Rectangle rect = element.Rect;
                        DrawUtility.AdjustHAlign(HAlign.Right, ref rect, parent.RectInsideBorders);
                        element.Rect = rect;
                    }
                }
            }
        }

        bool IUIElementCreationFilter.BeforeCreateChildElements(UIElement parent)
        {
            var element = parent as OptionSetOptionButtonUIElement;
            if (element != null)
            {
                element.CheckAlign = ContentAlignment.MiddleRight;
            }

            return false;
        }
    }
}
