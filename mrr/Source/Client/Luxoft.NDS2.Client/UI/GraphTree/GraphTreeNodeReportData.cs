﻿using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    /// <summary> Report parameters for reports of <see cref="BaseNodeReporter{TManager}"/>. </summary>
    public class GraphTreeNodeReportData
    {
        private readonly GraphTreeNodeData _treeNodeData;
        private readonly GraphNodesKeyParameters _nodesKeyParameters;

        public GraphTreeNodeReportData( GraphTreeNodeData treeNodeData, GraphNodesKeyParameters nodesKeyParameters )
        {
            _treeNodeData       = treeNodeData;
            _nodesKeyParameters = nodesKeyParameters;
        }

        public GraphTreeNodeData NodeData { get { return _treeNodeData; } }

        public GraphNodesKeyParameters NodesKeyParameters { get { return _nodesKeyParameters; } }

        public TaxPayerId TaxPayerId { get { return NodeData.TaxPayerId; } }

        public string Name { get { return NodeData.Name; } }

        public Period Period { get { return NodesKeyParameters.Period; } }

        public string PeriodText { get { return NodesKeyParameters.Period.ToString(); } }

        /// <summary> Вычет по НДС, руб., в строковом представлении </summary>
        public string DeductionNdsVal { get { return NodeData.DeductionNdsVal; } }

        /// <summary> Исчисленный НДС, руб., в строковом представлении </summary>
        public string ClacNdsVal { get { return NodeData.ClacNdsVal; } }

        /// <summary> Доля вычетов в исчисленном НДС у контрагента, %%, в строковом представлении </summary>
        public string NdsPercentageVal { get { return NodeData.NdsPercentageVal; } }

        public bool IsPurchase { get { return NodesKeyParameters.IsPurchase; } }

        public string ByPurchaseOrSellerText { get { return NodesKeyParameters.IsPurchase ? "от покупателя" : "от продавца";  } }

        public string RegionName { get { return NodeData.GraphDataNode.RegionName; } }

        public string Soun { get { return NodeData.GraphDataNode.Inspection.ToString(); } }

        public string Level { get { return NodeData.TreeLevel.ToString(); } }
    }
}
