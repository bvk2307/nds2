﻿#define OPTIMIZE_SPEC_INNS
//#define USE_PARENT_KPP

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.CorLibAddins.Helpers;

using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.GraphTree
{
    /// <summary> A strategy controls batch data processing. </summary>
    /// <remarks> Does not support concurrent usage from the different threads at the same time. </remarks>

    public sealed class GraphTreeReportBatchDataManager : IBatchDataLoadStrategy<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>, IEnumerable<GraphDataNode>>
    {
        private readonly BatchDataLoadStrategyOptions _dataLoadStrategyOptions;


        private GraphNodesKeyParameters _rootKeyParameters = null;
        private CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>> _callExecContext = null;

        //корень дерева связей
        private GraphDataBranch _root = null;

        //Узел, до которого все дерево загружено
        private GraphDataBranch _nodeToGetChildren = null;
        //Узел, до которого все дерево отправлено в отчет
        private GraphDataBranch _nodeToSendInReport = null;

        private Dictionary<TaxPayerId, GraphDataBranch> _allNodes;

        public GraphTreeReportBatchDataManager(BatchDataLoadStrategyOptions dataLoadStrategyOptions)
        {
            Contract.Requires(dataLoadStrategyOptions != null);
            _allNodes = new Dictionary<TaxPayerId, GraphDataBranch>();
            _dataLoadStrategyOptions = dataLoadStrategyOptions;
        }

        public GraphNodesKeyParameters KeyParameters
        {
            get { return _rootKeyParameters; }
        }

        /// <summary> Initializes the strategy parameters. </summary>
        /// <param name="keyParameters"></param>
        public void Init(GraphNodesKeyParameters keyParameters)
        {

            Contract.Requires(keyParameters != null);

            _rootKeyParameters = keyParameters;
        }

        private TaxPayerId GetTaxPayerId(TaxPayerId id)
        {
#if USE_PARENT_KPP
            return id;
#endif
            return new TaxPayerId(id.Inn, null);
        }

        /// <summary> Buffers recieved datas in the internal buffer before post processing. </summary>
        /// <param name="callExecContext"></param>
        public void Buffer(CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>> callExecContext)
        {
            Contract.Requires( callExecContext != null );
            Contract.Requires( callExecContext.Result != null );
            Contract.Requires( callExecContext.Parameters != null );
            Contract.Requires( callExecContext.Parameters.TaxPayerIds != null && callExecContext.Parameters.TaxPayerIds.Count > 0 );
            Contract.Ensures( _callExecContext != null );

            //строим дерево
            GraphDataBranch parent = null;

            TaxPayerId parentId = null;
            List<GraphDataBranch> children = new List<GraphDataBranch>(16);
            List<TaxPayerId> parameters = new List<TaxPayerId>(callExecContext.Parameters.TaxPayerIds);

            //строим все остальное
            foreach (GraphData graphData in callExecContext.Result)
            {
                //строим корень
                if (_root == null)
                {
                    if ( graphData.ParentInn !=null )
                        throw new InvalidOperationException( "Unexpected first node" );
                    if ( SpecialInnsHelper.IsSpecialInn( graphData.TaxPayerId.Inn ) )
                        throw new InvalidOperationException( string.Format( "The root node can not have a special INN: '{0}'", graphData.TaxPayerId.Inn ) );

                    _root = new GraphDataBranch(graphData) { Status = BranchStatus.ChildrenRequested };
                    _nodeToSendInReport = _root;
                    _nodeToGetChildren = _root;
                    _allNodes.Add(GetTaxPayerId(graphData.TaxPayerId), _root);
                    //корень по второму разу не трогаем
                    continue;
                }
                if (graphData.ParentInn == null)
                {
                    continue;
                }

                parentId = GetTaxPayerId(graphData.ParentTaxPayerId);

                if (parent == null || parentId != GetTaxPayerId(parent.TaxPayerId))
                {
                    //если это не первая часть данных
                    SetChildrenLoaded(parent, children);
                    //пока нет parentKPP сравниваем так
                    parameters.RemoveAll(x => parentId != null && GetTaxPayerId(x) == parentId);
                    parent = FindNode(GetTaxPayerId(graphData.ParentTaxPayerId)); //ищем нового парента
                    if (parent == null)
                        throw new InvalidOperationException("Parent node isn't found.");
                }
                SetZombie(graphData);

                bool isSpecialInn = SpecialInnsHelper.IsSpecialInn( graphData.TaxPayerId.Inn );
                bool isLeaf = parent.TreeLevel == _dataLoadStrategyOptions.MaxLevel - 1 
                                || graphData.IsZombie
#if OPTIMIZE_SPEC_INNS
                                || isSpecialInn
#endif //OPTIMIZE_SPEC_INNS
                                ;
                var currentGraphDataBranch = new GraphDataBranch(graphData)
                {
                    Parent = parent,
                    TreeLevel = parent.TreeLevel + 1,
                    IndexInParent = children.Count,
                    Status = isLeaf ? BranchStatus.AllChildrenLoaded | BranchStatus.ChildrenLoaded : 0
                };
                children.Add(currentGraphDataBranch);
                if (!currentGraphDataBranch.IsZombie
#if OPTIMIZE_SPEC_INNS
                    && !isSpecialInn
#endif //OPTIMIZE_SPEC_INNS
                    )
                {
                    _allNodes.Add(GetTaxPayerId(currentGraphDataBranch.TaxPayerId), currentGraphDataBranch);
                }
            }
            //последняя часть данных
            SetChildrenLoaded(parent, children);

            //пока нет parentKPP сравниваем так
            parameters.RemoveAll(x => parentId != null && x.Inn == parentId.Inn);
            //проверка тех параметров, которые отправили, но результатов не получили
            //чтобы поставить AllChildrenLoaded
            if (parameters.Count > 0 && _root != null)
            {
                List<GraphDataBranch> toCheck = new List<GraphDataBranch>(1);
                foreach (var taxPayerId in parameters)
                {
                    parent = FindNode(GetTaxPayerId(taxPayerId));
                    if (parent != null)
                    {
                        parent.Status |= BranchStatus.ChildrenLoaded | BranchStatus.AllChildrenLoaded;
                        if (parent.ParentOfBranch != null && !toCheck.Contains(parent.ParentOfBranch))
                        {
                            toCheck.Add(parent.ParentOfBranch);
                        }
                    }
                }
                toCheck.ForEach(x => x.CheckAllChildrenLoaded());
            }
            _callExecContext = callExecContext;
        }

        private void SetZombie(GraphData graphData)
        {
            if (_allNodes.ContainsKey(GetTaxPayerId(graphData.TaxPayerId)))
            {
                graphData.IsZombie = true;
            }
        }

        private void SetChildrenLoaded(GraphDataBranch parent, List<GraphDataBranch> children)
        {
            if (parent != null)
            {
                //складываем найденное в дерево
                parent.SetChildren(new List<GraphDataBranch>(children));
                parent.Status |= BranchStatus.ChildrenLoaded;
                if ( parent.TreeLevel == _dataLoadStrategyOptions.MaxLevel - 1 
                                         || children.All( x => x.IsZombie
#if OPTIMIZE_SPEC_INNS
                                                               || SpecialInnsHelper.IsSpecialInn( x.TaxPayerId.Inn )
#endif //OPTIMIZE_SPEC_INNS
                    ) )
                {
                    parent.Status |= BranchStatus.AllChildrenLoaded;
                    parent.ParentOfBranch.CheckAllChildrenLoaded();
                }
                children.Clear();
            }
        }

        private GraphDataBranch FindNode(TaxPayerId nodeId)
        {
            GraphDataBranch value;
            if (_allNodes.TryGetValue(nodeId, out value))
            {
                return value;
            }
            return null;
        }

        /// <summary> Returns next data requests that should be requested at the bounds of the batch. </summary>
        /// <returns> The next data requests or 'null' if the completion is requested instead of. </returns>
        public IReadOnlyCollection<GraphNodesKeyParameters> GetNextToPostOrFinish()
        {
            Contract.Ensures( Contract.Result<IReadOnlyCollection<GraphNodesKeyParameters>>() == null 
                || Contract.Exists( Contract.Result<IReadOnlyCollection<GraphNodesKeyParameters>>(), keyParameters => keyParameters.TaxPayerIds.Count > 0 ) );

            if (_root == null || _root.Status.HasFlag(BranchStatus.AllChildrenLoaded))
            {
                return null;
            }
            List<GraphNodesKeyParameters> toPost = new List<GraphNodesKeyParameters>();
            List<TaxPayerId> idList = new List<TaxPayerId>();
            var nodeToGetChildren = _nodeToGetChildren;
            int startFromIndex = GetTaxPayerId(_nodeToGetChildren.TaxPayerId) == GetTaxPayerId(_root.TaxPayerId) ? 0 : -1;  //'-1' -- to start enumeration from the current node, 
                                                                                                                            // '0' -- to start from the next node
            //Подвигаем указатель, с которого ищем новые параметры для запроса
            foreach (GraphDataBranch node in GetTreeFromNode(nodeToGetChildren, startFromIndex, branch => !branch.Status.HasFlag(BranchStatus.AllChildrenLoaded)))
            {
                if (node != null)
                {
                    _nodeToGetChildren = node;
                    if (!node.Status.HasFlag(BranchStatus.ChildrenLoaded))
                    {
                        break;
                    }
                }
            }
            nodeToGetChildren = _nodeToGetChildren;
            startFromIndex = GetTaxPayerId(_nodeToGetChildren.TaxPayerId) == GetTaxPayerId(_root.TaxPayerId) ? 0 : -1;  //'-1' -- to start enumeration from the current node, 
                                                                                                                        // '0' -- to start from the next node
            //от узла, где все предыдущие загружены начинаем искать новые параметры для запроса
            foreach (GraphDataBranch node in GetTreeFromNode(nodeToGetChildren, startFromIndex, branch => !branch.Status.HasFlag(BranchStatus.AllChildrenLoaded)))
            {
                if (toPost.Count < _dataLoadStrategyOptions.MaxParallelRequestCount)
                {
                    if ( node != null && !node.Status.HasFlag( BranchStatus.ChildrenRequested ) && !node.Status.HasFlag( BranchStatus.AllChildrenLoaded ) )
                    {
                        TaxPayerId taxpayerId = node.TaxPayerId;
                        idList.Add( taxpayerId );
                        node.Status |= BranchStatus.ChildrenRequested;
                    }
                    if (idList.Count >= _dataLoadStrategyOptions.MaxInnNumber)
                    {
                        var parameters = GraphNodesKeyParameters.New(KeyParameters, ids: new List<TaxPayerId>(idList), isRoot: false);
                        toPost.Add(parameters);
                        idList.Clear();
                    }
                }
                else break;
            }
            if (toPost.Count < _dataLoadStrategyOptions.MaxParallelRequestCount && idList.Count > 0)
            {
                var parameters = GraphNodesKeyParameters.New(KeyParameters, ids: new List<TaxPayerId>(idList), isRoot: false);
                toPost.Add(parameters);
            }
            return toPost.ToReadOnly();
        }

        /// <summary> Takes next datas from the internal buffer to post process. </summary>
        /// <returns></returns>
        public CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> GetNextToOffer()
        {
            Contract.Ensures( Contract.Result<CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>>() != null );
            Contract.Ensures( Contract.Result<CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>>().Result != null );

            List<GraphDataNode> res = new List<GraphDataNode>();
            var nodeToReport = _nodeToSendInReport;
            if (nodeToReport != null)
            {
                foreach (GraphDataBranch node in GetTreeFromNode(nodeToReport))
                {
                    if (node != null)
                    {
                        _nodeToSendInReport = node;

                        if (!node.Status.HasFlag(BranchStatus.SentInReport))
                        {
                            res.Add(node);
                            node.Status = node.Status | BranchStatus.SentInReport;
                        }
                        node.CheckAllSentInReport();
                        //Дальше данные недогружены, больше ничего не отправить в отчет, пока они не пришли
                        if (!node.Status.HasFlag(BranchStatus.ChildrenLoaded))
                        {
                            break;
                        }
                    }
                }
            }
            DeleteUnusedNodes(_root);
            return new CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>(
                _rootKeyParameters, _callExecContext.InstanceKey, _callExecContext.ContextKey, res);
        }

        /// <summary>
        /// Удаляет из поддерева узлы, обработка которых закончена
        /// </summary>
        /// <param name="toCheckForDelete"></param>
        private void DeleteUnusedNodes(GraphDataBranch toCheckForDelete)
        {
            if (toCheckForDelete == null)
            {
                return;
            }
            for (int i = 0; i < toCheckForDelete.ChildrenList.Count; i++)
            {
                GraphDataBranch currentChild = toCheckForDelete.ChildrenList[i];
                if (currentChild != null)
                {
                    if (currentChild.Status.HasFlag(BranchStatus.AllChildrenSentInReport))
                    {
                        if (!currentChild.IsZombie
                            && _allNodes.ContainsKey(GetTaxPayerId(currentChild.TaxPayerId)))
                        {
                            foreach (var subchildNode in GetAllChildNodes(currentChild))
                            {
                                if (!subchildNode.IsZombie)
                                {
                                    _allNodes[GetTaxPayerId(subchildNode.TaxPayerId)] = null;
                                }
                            }
                            _allNodes[GetTaxPayerId(currentChild.TaxPayerId)] = null;
                        }
                        toCheckForDelete.ChildrenList[i] = null;
                    }
                    else
                    {
                        DeleteUnusedNodes(currentChild);
                    }
                }
            }
        }
        /// <summary>
        /// Обходит все дерево обходом в глубину начиная с заданной вершины до конца
        /// </summary>
        /// <param name="parentNode">вершина начала обхода</param>
        /// <param name="currentChild">номер потомка, начиная с которого обходим, -1 - сама вершина</param>
        /// <param name="ifCheckCildrenPredicate">проверка, обходить ли потомков текущей вершины</param>
        /// <returns></returns>
        private IEnumerable<GraphDataBranch> GetTreeFromNode(
            GraphDataBranch parentNode, int currentChild = -1,
            Func<GraphDataBranch, bool> ifCheckCildrenPredicate = null)
        {
            if (parentNode != null)
            {
                //текущая вершина
                if (currentChild == -1)
                {
                    yield return parentNode;
                    currentChild = 0;
                }
                GraphDataBranch currentNode = parentNode;
                //потомки, начиная с заданного
                while (currentNode.ChildrenList.Count > currentChild)
                {
                    var child = currentNode.ChildrenList[currentChild];
                    if (child != null)
                    {
                        yield return child;
                        if (ifCheckCildrenPredicate == null || ifCheckCildrenPredicate(child))
                        {
                            foreach (var newChild in GetAllChildNodes(child, ifCheckCildrenPredicate))
                            {
                                yield return newChild;
                            }
                        }
                    }
                    currentChild++;
                }
                //следующий потомок родительской вершины
                if (currentNode.Parent != null)
                {
                    foreach (GraphDataBranch node in 
                        GetTreeFromNode(currentNode.ParentOfBranch, currentNode.IndexInParent + 1))
                    {
                        yield return node;
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает все поддерево обходом в глубину 
        /// </summary>
        /// <param name="parentNode">вершина начала обхода</param>
        /// <param name="ifCheckCildrenPredicate">проверка, обходить ли потомков текущей вершины</param>
        /// <returns></returns>
        private IEnumerable<GraphDataBranch> GetAllChildNodes(GraphDataBranch parentNode,
            Func<GraphDataBranch, bool> ifCheckCildrenPredicate = null)
        {
            if (parentNode != null)
            {
                foreach (var childNode in parentNode.ChildrenList)
                {
                    if (childNode != null)
                    {
                        yield return childNode;
                        if (ifCheckCildrenPredicate == null || ifCheckCildrenPredicate(childNode))
                        {
                            foreach (var newChild in GetAllChildNodes(childNode, ifCheckCildrenPredicate))
                            {
                                yield return newChild;
                            }
                        }
                    }
                }
            }
        }
    }
}