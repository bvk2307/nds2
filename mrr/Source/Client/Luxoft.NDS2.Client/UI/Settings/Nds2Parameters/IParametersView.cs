﻿using System;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public interface IParametersView
    {
        # region Открытие окна

        event EventHandler Opened;

        void EnableEdit();

        #endregion

        #region Максимальное количество счетов-фактур в требовании и истребовании
        /// <summary>
        /// Задает максимальное количество записей о СФ в АТ
        int ClaimInvoiceMaxQuantity { set; }

        /// <summary>
        /// Задает максимальное количество записей о СФ в АИ
        ///  </summary>
        int ReclaimInvoiceMaxQuantity { set; }

        # endregion

        # region Повторная отправка автотребования

        /// <summary>
        /// Возвращает Или задает время ожидания ответа от СЭОД
        /// </summary>
        int ClaimResendingTimeout { get; set; }

        event EventHandler ClaimResendingChanged;

        /// <summary>
        /// Возвращает или задает количество повторных отправок
        /// </summary>
        int ResendingAttempts { get; set; }

        event EventHandler ResendingAttemptsChanged;

        # endregion

        # region Время ожидания

        /// <summary>
        /// Возвращает или задает время ожидания вручения автотребования/автоистребования
        /// </summary>
        int ClaimDeliveryTimeout { get; set; }

        event EventHandler ClaimDeliveryTimeoutChanged;

        /// <summary>
        /// Возвращает или задает время ожидания ответа на автотребование
        /// </summary>
        int ClaimExplainTimeout { get; set; }

        event EventHandler ClaimExplainTimeoutChanged;

        /// <summary>
        /// Возвращает или задает время ожидания на автоистребование
        /// </summary>
        int ReclaimReplyTimeout { get; set; }

        event EventHandler ReclaimReplyTimeoutChanged;

        /// <summary>
        /// Возвращает или задает время ожидания ввода ответа на автоистребование
        /// </summary>
        int ReplyEntryTimeout { get; set; }

        event EventHandler ReplyEntryTimeoutChanged;

        # endregion

        # region Сохранение

        bool HasChanges { set; }

        event EventHandler Saving;

        event EventHandler Closing;

        /// <summary>
        /// Показывает диалог с сообщением об успешном сохранении настроек
        /// </summary>
        void ShowNotification();

        # endregion
    }
}
