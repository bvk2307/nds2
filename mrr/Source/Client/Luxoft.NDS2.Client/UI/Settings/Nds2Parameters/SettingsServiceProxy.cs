﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public class SettingsServiceProxy : ServiceProxyBase, ISettingsServiceProxy
    {
        private readonly ISystemSettingsService _service;

        public SettingsServiceProxy(
            ISystemSettingsService service,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        /// <summary>
        /// Загружает параметры
        /// </summary>
        /// <param name="settings">загруженные параметры</param>
        /// <returns></returns>
        public bool TryLoad(out SystemSettings settings)
        {
            settings = 
                Invoke(
                    () => _service.Load(), 
                    () => new OperationResult<SystemSettings>())
                .Result;

            return settings != null;
        }

        /// <summary>
        /// Сохраняет параметры
        /// </summary>
        /// <param name="settings">параметры для сохранения</param>
        /// <returns></returns>
        public bool TrySave(SystemSettings settings)
        {
            var result =
                Invoke(
                    () => _service.Save(settings),
                    () => new OperationResult
                    { 
                        Status = ResultStatus.Error 
                    });

            return result.Status == ResultStatus.Success;
        }        
    }
}
