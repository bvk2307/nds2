﻿namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    partial class ParametersView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BasePanel = new Infragistics.Win.Misc.UltraPanel();
            this.LayoutBasePanel = new System.Windows.Forms.TableLayoutPanel();
            this.MainParamGroup = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this._claimInvoiceMaxQtyEditor = new Infragistics.Win.Misc.UltraLabel();
            this._reclaimInvoiceMaxQtyEditor = new Infragistics.Win.Misc.UltraLabel();
            this.AutoSelectionGroup = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this._claimResendingTimeoutEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this._resendingAttemptsEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ClaimGroup = new Infragistics.Win.Misc.UltraGroupBox();
            this.LayoutClaimGroup = new System.Windows.Forms.TableLayoutPanel();
            this.LabelClaimTimeoutForDelivery = new Infragistics.Win.Misc.UltraLabel();
            this._claimDeliveryTimeoutEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.Label1ClaimTimeoutForDeliveryDaysMark = new Infragistics.Win.Misc.UltraLabel();
            this.LabelClaimTimeoutAnswerForAutoclaim = new Infragistics.Win.Misc.UltraLabel();
            this._claimExplainTimeoutEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark = new Infragistics.Win.Misc.UltraLabel();
            this.LabelClaimTimeoutAnswerForAutooverclaim = new Infragistics.Win.Misc.UltraLabel();
            this._reclaimReplyTimeoutEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark = new Infragistics.Win.Misc.UltraLabel();
            this.LabelClaimTimeoutInputAnswerForAutooverclaim = new Infragistics.Win.Misc.UltraLabel();
            this._replyEntryTimeoutEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark = new Infragistics.Win.Misc.UltraLabel();
            this.BasePanel.ClientArea.SuspendLayout();
            this.BasePanel.SuspendLayout();
            this.LayoutBasePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainParamGroup)).BeginInit();
            this.MainParamGroup.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutoSelectionGroup)).BeginInit();
            this.AutoSelectionGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._claimResendingTimeoutEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._resendingAttemptsEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimGroup)).BeginInit();
            this.ClaimGroup.SuspendLayout();
            this.LayoutClaimGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._claimDeliveryTimeoutEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._claimExplainTimeoutEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._reclaimReplyTimeoutEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._replyEntryTimeoutEditor)).BeginInit();
            this.SuspendLayout();
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // BasePanel
            // 
            // 
            // BasePanel.ClientArea
            // 
            this.BasePanel.ClientArea.Controls.Add(this.LayoutBasePanel);
            this.BasePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BasePanel.Location = new System.Drawing.Point(0, 0);
            this.BasePanel.Name = "BasePanel";
            this.BasePanel.Size = new System.Drawing.Size(742, 695);
            this.BasePanel.TabIndex = 2;
            // 
            // LayoutBasePanel
            // 
            this.LayoutBasePanel.BackColor = System.Drawing.Color.Transparent;
            this.LayoutBasePanel.ColumnCount = 3;
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutBasePanel.Controls.Add(this.MainParamGroup, 1, 1);
            this.LayoutBasePanel.Controls.Add(this.AutoSelectionGroup, 1, 2);
            this.LayoutBasePanel.Controls.Add(this.ClaimGroup, 1, 3);
            this.LayoutBasePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutBasePanel.Location = new System.Drawing.Point(0, 0);
            this.LayoutBasePanel.Name = "LayoutBasePanel";
            this.LayoutBasePanel.RowCount = 4;
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.LayoutBasePanel.Size = new System.Drawing.Size(742, 695);
            this.LayoutBasePanel.TabIndex = 0;
            // 
            // MainParamGroup
            // 
            this.MainParamGroup.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.HeaderSolid;
            this.MainParamGroup.Controls.Add(this.tableLayoutPanel1);
            this.MainParamGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainParamGroup.Location = new System.Drawing.Point(23, 8);
            this.MainParamGroup.Name = "MainParamGroup";
            this.MainParamGroup.Size = new System.Drawing.Size(696, 94);
            this.MainParamGroup.TabIndex = 0;
            this.MainParamGroup.Text = "Максимальное количество записей о СФ";
            this.MainParamGroup.UseAppStyling = false;
            this.MainParamGroup.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2000;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ultraLabel2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ultraLabel3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._claimInvoiceMaxQtyEditor, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this._reclaimInvoiceMaxQtyEditor, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(694, 77);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // ultraLabel2
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance5;
            this.ultraLabel2.Location = new System.Drawing.Point(63, 3);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 18);
            this.ultraLabel2.TabIndex = 4;
            this.ultraLabel2.Text = "В автотребовании";
            // 
            // ultraLabel3
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance6;
            this.ultraLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel3.Location = new System.Drawing.Point(63, 41);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(134, 33);
            this.ultraLabel3.TabIndex = 6;
            this.ultraLabel3.Text = "В автоистребовании";
            // 
            // _claimInvoiceMaxQtyEditor
            // 
            appearance21.BorderColor = System.Drawing.Color.DarkGray;
            appearance21.TextHAlignAsString = "Right";
            this._claimInvoiceMaxQtyEditor.Appearance = appearance21;
            this._claimInvoiceMaxQtyEditor.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this._claimInvoiceMaxQtyEditor.Location = new System.Drawing.Point(203, 3);
            this._claimInvoiceMaxQtyEditor.Name = "_claimInvoiceMaxQtyEditor";
            this._claimInvoiceMaxQtyEditor.Size = new System.Drawing.Size(94, 23);
            this._claimInvoiceMaxQtyEditor.TabIndex = 8;
            this._claimInvoiceMaxQtyEditor.Text = "5000";
            // 
            // _reclaimInvoiceMaxQtyEditor
            // 
            appearance22.BorderColor = System.Drawing.Color.DarkGray;
            appearance22.TextHAlignAsString = "Right";
            this._reclaimInvoiceMaxQtyEditor.Appearance = appearance22;
            this._reclaimInvoiceMaxQtyEditor.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this._reclaimInvoiceMaxQtyEditor.Location = new System.Drawing.Point(203, 41);
            this._reclaimInvoiceMaxQtyEditor.Name = "_reclaimInvoiceMaxQtyEditor";
            this._reclaimInvoiceMaxQtyEditor.Size = new System.Drawing.Size(94, 23);
            this._reclaimInvoiceMaxQtyEditor.TabIndex = 9;
            this._reclaimInvoiceMaxQtyEditor.Text = "99";
            // 
            // AutoSelectionGroup
            // 
            this.AutoSelectionGroup.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.HeaderSolid;
            this.AutoSelectionGroup.Controls.Add(this.tableLayoutPanel2);
            this.AutoSelectionGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.AutoSelectionGroup.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.AutoSelectionGroup.Location = new System.Drawing.Point(23, 108);
            this.AutoSelectionGroup.Name = "AutoSelectionGroup";
            this.AutoSelectionGroup.Size = new System.Drawing.Size(696, 94);
            this.AutoSelectionGroup.TabIndex = 1;
            this.AutoSelectionGroup.Text = "Повторная отправка автотребования";
            this.AutoSelectionGroup.UseAppStyling = false;
            this.AutoSelectionGroup.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2000;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.ultraLabel7, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this._claimResendingTimeoutEditor, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.ultraLabel8, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.ultraLabel9, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this._resendingAttemptsEditor, 5, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(694, 77);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // ultraLabel7
            // 
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance17;
            this.tableLayoutPanel2.SetColumnSpan(this.ultraLabel7, 3);
            this.ultraLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel7.Location = new System.Drawing.Point(63, 3);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(254, 32);
            this.ultraLabel7.TabIndex = 9;
            this.ultraLabel7.Text = "Время ожидания ответа от СЭОД";
            // 
            // _claimResendingTimeoutEditor
            // 
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance4.TextHAlignAsString = "Right";
            this._claimResendingTimeoutEditor.Appearance = appearance4;
            this.tableLayoutPanel2.SetColumnSpan(this._claimResendingTimeoutEditor, 3);
            this._claimResendingTimeoutEditor.ContextMenuStrip = this._contextMenu;
            this._claimResendingTimeoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._claimResendingTimeoutEditor.Location = new System.Drawing.Point(323, 3);
            this._claimResendingTimeoutEditor.MaxValue = 3;
            this._claimResendingTimeoutEditor.MinValue = 0;
            this._claimResendingTimeoutEditor.Name = "_claimResendingTimeoutEditor";
            this._claimResendingTimeoutEditor.PromptChar = ' ';
            this._claimResendingTimeoutEditor.Size = new System.Drawing.Size(114, 21);
            this._claimResendingTimeoutEditor.TabIndex = 10;
            this._claimResendingTimeoutEditor.Value = 3;
            this._claimResendingTimeoutEditor.ValueChanged += new System.EventHandler(this.ClaimResendingTimeoutValueChanged);
            this._claimResendingTimeoutEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // ultraLabel8
            // 
            appearance18.TextHAlignAsString = "Right";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance18;
            this.tableLayoutPanel2.SetColumnSpan(this.ultraLabel8, 2);
            this.ultraLabel8.Location = new System.Drawing.Point(443, 3);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(44, 18);
            this.ultraLabel8.TabIndex = 11;
            this.ultraLabel8.Text = "(дней)";
            // 
            // ultraLabel9
            // 
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance19;
            this.tableLayoutPanel2.SetColumnSpan(this.ultraLabel9, 3);
            this.ultraLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel9.Location = new System.Drawing.Point(63, 41);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(254, 33);
            this.ultraLabel9.TabIndex = 12;
            this.ultraLabel9.Text = "Количество повторных отправок";
            // 
            // _resendingAttemptsEditor
            // 
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance7.TextHAlignAsString = "Right";
            this._resendingAttemptsEditor.Appearance = appearance7;
            this.tableLayoutPanel2.SetColumnSpan(this._resendingAttemptsEditor, 3);
            this._resendingAttemptsEditor.ContextMenuStrip = this._contextMenu;
            this._resendingAttemptsEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._resendingAttemptsEditor.Location = new System.Drawing.Point(323, 41);
            this._resendingAttemptsEditor.MaxValue = 3;
            this._resendingAttemptsEditor.MinValue = 0;
            this._resendingAttemptsEditor.Name = "_resendingAttemptsEditor";
            this._resendingAttemptsEditor.PromptChar = ' ';
            this._resendingAttemptsEditor.Size = new System.Drawing.Size(114, 21);
            this._resendingAttemptsEditor.TabIndex = 13;
            this._resendingAttemptsEditor.Value = 3;
            this._resendingAttemptsEditor.ValueChanged += new System.EventHandler(this.ResendingAttemptsEditorValueChanged);
            this._resendingAttemptsEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // ClaimGroup
            // 
            this.ClaimGroup.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.HeaderSolid;
            this.ClaimGroup.Controls.Add(this.LayoutClaimGroup);
            this.ClaimGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClaimGroup.Location = new System.Drawing.Point(23, 208);
            this.ClaimGroup.Name = "ClaimGroup";
            this.ClaimGroup.Size = new System.Drawing.Size(696, 484);
            this.ClaimGroup.TabIndex = 2;
            this.ClaimGroup.Text = "Время ожидания";
            this.ClaimGroup.UseAppStyling = false;
            // 
            // LayoutClaimGroup
            // 
            this.LayoutClaimGroup.ColumnCount = 10;
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutClaimGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutForDelivery, 2, 0);
            this.LayoutClaimGroup.Controls.Add(this._claimDeliveryTimeoutEditor, 5, 0);
            this.LayoutClaimGroup.Controls.Add(this.Label1ClaimTimeoutForDeliveryDaysMark, 8, 0);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutAnswerForAutoclaim, 2, 1);
            this.LayoutClaimGroup.Controls.Add(this._claimExplainTimeoutEditor, 5, 1);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutAnswerForAutoclaimDaysMark, 8, 1);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutAnswerForAutooverclaim, 2, 2);
            this.LayoutClaimGroup.Controls.Add(this._reclaimReplyTimeoutEditor, 5, 2);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark, 8, 2);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutInputAnswerForAutooverclaim, 2, 3);
            this.LayoutClaimGroup.Controls.Add(this._replyEntryTimeoutEditor, 5, 3);
            this.LayoutClaimGroup.Controls.Add(this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark, 8, 3);
            this.LayoutClaimGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.LayoutClaimGroup.Location = new System.Drawing.Point(1, 16);
            this.LayoutClaimGroup.Name = "LayoutClaimGroup";
            this.LayoutClaimGroup.RowCount = 4;
            this.LayoutClaimGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.LayoutClaimGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.LayoutClaimGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.LayoutClaimGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.LayoutClaimGroup.Size = new System.Drawing.Size(694, 154);
            this.LayoutClaimGroup.TabIndex = 0;
            // 
            // LabelClaimTimeoutForDelivery
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutForDelivery.Appearance = appearance9;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutForDelivery, 3);
            this.LabelClaimTimeoutForDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelClaimTimeoutForDelivery.Location = new System.Drawing.Point(63, 3);
            this.LabelClaimTimeoutForDelivery.Name = "LabelClaimTimeoutForDelivery";
            this.LabelClaimTimeoutForDelivery.Size = new System.Drawing.Size(254, 32);
            this.LabelClaimTimeoutForDelivery.TabIndex = 9;
            this.LabelClaimTimeoutForDelivery.Text = "Вручения автотребования/автоистребования";
            // 
            // _claimDeliveryTimeoutEditor
            // 
            appearance8.TextHAlignAsString = "Right";
            this._claimDeliveryTimeoutEditor.Appearance = appearance8;
            this.LayoutClaimGroup.SetColumnSpan(this._claimDeliveryTimeoutEditor, 3);
            this._claimDeliveryTimeoutEditor.ContextMenuStrip = this._contextMenu;
            this._claimDeliveryTimeoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._claimDeliveryTimeoutEditor.Location = new System.Drawing.Point(323, 3);
            this._claimDeliveryTimeoutEditor.MaxValue = 6;
            this._claimDeliveryTimeoutEditor.MinValue = 0;
            this._claimDeliveryTimeoutEditor.Name = "_claimDeliveryTimeoutEditor";
            this._claimDeliveryTimeoutEditor.PromptChar = ' ';
            this._claimDeliveryTimeoutEditor.Size = new System.Drawing.Size(114, 21);
            this._claimDeliveryTimeoutEditor.TabIndex = 10;
            this._claimDeliveryTimeoutEditor.Value = 6;
            this._claimDeliveryTimeoutEditor.ValueChanged += new System.EventHandler(this.ClaimDeliveryTimeoutEditorValueChanged);
            this._claimDeliveryTimeoutEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // Label1ClaimTimeoutForDeliveryDaysMark
            // 
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            this.Label1ClaimTimeoutForDeliveryDaysMark.Appearance = appearance10;
            this.LayoutClaimGroup.SetColumnSpan(this.Label1ClaimTimeoutForDeliveryDaysMark, 2);
            this.Label1ClaimTimeoutForDeliveryDaysMark.Location = new System.Drawing.Point(443, 3);
            this.Label1ClaimTimeoutForDeliveryDaysMark.Name = "Label1ClaimTimeoutForDeliveryDaysMark";
            this.Label1ClaimTimeoutForDeliveryDaysMark.Size = new System.Drawing.Size(44, 18);
            this.Label1ClaimTimeoutForDeliveryDaysMark.TabIndex = 11;
            this.Label1ClaimTimeoutForDeliveryDaysMark.Text = "(дней)";
            // 
            // LabelClaimTimeoutAnswerForAutoclaim
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutAnswerForAutoclaim.Appearance = appearance11;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutAnswerForAutoclaim, 3);
            this.LabelClaimTimeoutAnswerForAutoclaim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelClaimTimeoutAnswerForAutoclaim.Location = new System.Drawing.Point(63, 41);
            this.LabelClaimTimeoutAnswerForAutoclaim.Name = "LabelClaimTimeoutAnswerForAutoclaim";
            this.LabelClaimTimeoutAnswerForAutoclaim.Size = new System.Drawing.Size(254, 32);
            this.LabelClaimTimeoutAnswerForAutoclaim.TabIndex = 12;
            this.LabelClaimTimeoutAnswerForAutoclaim.Text = "Ответа на автотребование";
            // 
            // _claimExplainTimeoutEditor
            // 
            appearance15.TextHAlignAsString = "Right";
            this._claimExplainTimeoutEditor.Appearance = appearance15;
            this.LayoutClaimGroup.SetColumnSpan(this._claimExplainTimeoutEditor, 3);
            this._claimExplainTimeoutEditor.ContextMenuStrip = this._contextMenu;
            this._claimExplainTimeoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._claimExplainTimeoutEditor.Location = new System.Drawing.Point(323, 41);
            this._claimExplainTimeoutEditor.MaxValue = 8;
            this._claimExplainTimeoutEditor.MinValue = 0;
            this._claimExplainTimeoutEditor.Name = "_claimExplainTimeoutEditor";
            this._claimExplainTimeoutEditor.PromptChar = ' ';
            this._claimExplainTimeoutEditor.Size = new System.Drawing.Size(114, 21);
            this._claimExplainTimeoutEditor.TabIndex = 13;
            this._claimExplainTimeoutEditor.Value = 8;
            this._claimExplainTimeoutEditor.ValueChanged += new System.EventHandler(this.ClaimExplainTimeoutEditorValueChanged);
            this._claimExplainTimeoutEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // LabelClaimTimeoutAnswerForAutoclaimDaysMark
            // 
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.Appearance = appearance12;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutAnswerForAutoclaimDaysMark, 2);
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.Location = new System.Drawing.Point(443, 41);
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.Name = "LabelClaimTimeoutAnswerForAutoclaimDaysMark";
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.Size = new System.Drawing.Size(44, 18);
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.TabIndex = 14;
            this.LabelClaimTimeoutAnswerForAutoclaimDaysMark.Text = "(дней)";
            // 
            // LabelClaimTimeoutAnswerForAutooverclaim
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutAnswerForAutooverclaim.Appearance = appearance13;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutAnswerForAutooverclaim, 3);
            this.LabelClaimTimeoutAnswerForAutooverclaim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelClaimTimeoutAnswerForAutooverclaim.Location = new System.Drawing.Point(63, 79);
            this.LabelClaimTimeoutAnswerForAutooverclaim.Name = "LabelClaimTimeoutAnswerForAutooverclaim";
            this.LabelClaimTimeoutAnswerForAutooverclaim.Size = new System.Drawing.Size(254, 32);
            this.LabelClaimTimeoutAnswerForAutooverclaim.TabIndex = 15;
            this.LabelClaimTimeoutAnswerForAutooverclaim.Text = "Ответа на автоистребование";
            // 
            // _reclaimReplyTimeoutEditor
            // 
            appearance20.TextHAlignAsString = "Right";
            this._reclaimReplyTimeoutEditor.Appearance = appearance20;
            this.LayoutClaimGroup.SetColumnSpan(this._reclaimReplyTimeoutEditor, 3);
            this._reclaimReplyTimeoutEditor.ContextMenuStrip = this._contextMenu;
            this._reclaimReplyTimeoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._reclaimReplyTimeoutEditor.Location = new System.Drawing.Point(323, 79);
            this._reclaimReplyTimeoutEditor.MaxValue = 8;
            this._reclaimReplyTimeoutEditor.MinValue = 0;
            this._reclaimReplyTimeoutEditor.Name = "_reclaimReplyTimeoutEditor";
            this._reclaimReplyTimeoutEditor.PromptChar = ' ';
            this._reclaimReplyTimeoutEditor.Size = new System.Drawing.Size(114, 21);
            this._reclaimReplyTimeoutEditor.TabIndex = 16;
            this._reclaimReplyTimeoutEditor.Value = 8;
            this._reclaimReplyTimeoutEditor.ValueChanged += new System.EventHandler(this.ReclaimReplyTimeoutEditor_ValueChanged);
            this._reclaimReplyTimeoutEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // LabelClaimTimeoutAnswerForAutooverclaimDaysMark
            // 
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.Appearance = appearance14;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark, 2);
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.Location = new System.Drawing.Point(443, 79);
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.Name = "LabelClaimTimeoutAnswerForAutooverclaimDaysMark";
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.Size = new System.Drawing.Size(44, 18);
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.TabIndex = 17;
            this.LabelClaimTimeoutAnswerForAutooverclaimDaysMark.Text = "(дней)";
            // 
            // LabelClaimTimeoutInputAnswerForAutooverclaim
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Appearance = appearance16;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutInputAnswerForAutooverclaim, 3);
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Location = new System.Drawing.Point(63, 117);
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Name = "LabelClaimTimeoutInputAnswerForAutooverclaim";
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Size = new System.Drawing.Size(254, 34);
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.TabIndex = 18;
            this.LabelClaimTimeoutInputAnswerForAutooverclaim.Text = "Ввода ответа на автоистребование";
            // 
            // _replyEntryTimeoutEditor
            // 
            appearance1.TextHAlignAsString = "Right";
            this._replyEntryTimeoutEditor.Appearance = appearance1;
            this.LayoutClaimGroup.SetColumnSpan(this._replyEntryTimeoutEditor, 3);
            this._replyEntryTimeoutEditor.ContextMenuStrip = this._contextMenu;
            this._replyEntryTimeoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._replyEntryTimeoutEditor.Location = new System.Drawing.Point(323, 117);
            this._replyEntryTimeoutEditor.MaxValue = 3;
            this._replyEntryTimeoutEditor.MinValue = 0;
            this._replyEntryTimeoutEditor.Name = "_replyEntryTimeoutEditor";
            this._replyEntryTimeoutEditor.PromptChar = ' ';
            this._replyEntryTimeoutEditor.Size = new System.Drawing.Size(114, 21);
            this._replyEntryTimeoutEditor.TabIndex = 19;
            this._replyEntryTimeoutEditor.Value = 3;
            this._replyEntryTimeoutEditor.ValueChanged += new System.EventHandler(this.ReplyEntryTimeoutEditorValueChanged);
            this._replyEntryTimeoutEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDown);
            // 
            // LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark
            // 
            appearance25.TextHAlignAsString = "Right";
            appearance25.TextVAlignAsString = "Middle";
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.Appearance = appearance25;
            this.LayoutClaimGroup.SetColumnSpan(this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark, 2);
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.Location = new System.Drawing.Point(443, 117);
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.Name = "LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark";
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.Size = new System.Drawing.Size(44, 18);
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.TabIndex = 20;
            this.LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark.Text = "(дней)";
            // 
            // ParametersView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BasePanel);
            this.Name = "ParametersView";
            this.Size = new System.Drawing.Size(742, 733);
            this.Load += new System.EventHandler(this.ViewOpened);
            this.BasePanel.ClientArea.ResumeLayout(false);
            this.BasePanel.ResumeLayout(false);
            this.LayoutBasePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainParamGroup)).EndInit();
            this.MainParamGroup.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AutoSelectionGroup)).EndInit();
            this.AutoSelectionGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._claimResendingTimeoutEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._resendingAttemptsEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimGroup)).EndInit();
            this.ClaimGroup.ResumeLayout(false);
            this.LayoutClaimGroup.ResumeLayout(false);
            this.LayoutClaimGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._claimDeliveryTimeoutEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._claimExplainTimeoutEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._reclaimReplyTimeoutEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._replyEntryTimeoutEditor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel BasePanel;
        private System.Windows.Forms.TableLayoutPanel LayoutBasePanel;
        private Infragistics.Win.Misc.UltraGroupBox MainParamGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraGroupBox AutoSelectionGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _claimResendingTimeoutEditor;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _resendingAttemptsEditor;
        private Infragistics.Win.Misc.UltraGroupBox ClaimGroup;
        private System.Windows.Forms.TableLayoutPanel LayoutClaimGroup;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutForDelivery;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _claimDeliveryTimeoutEditor;
        private Infragistics.Win.Misc.UltraLabel Label1ClaimTimeoutForDeliveryDaysMark;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutAnswerForAutoclaim;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _claimExplainTimeoutEditor;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutAnswerForAutoclaimDaysMark;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutAnswerForAutooverclaim;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _reclaimReplyTimeoutEditor;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutAnswerForAutooverclaimDaysMark;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutInputAnswerForAutooverclaim;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _replyEntryTimeoutEditor;
        private Infragistics.Win.Misc.UltraLabel LabelClaimTimeoutInputAnswerForAutooverclaimDaysMark;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private Infragistics.Win.Misc.UltraLabel _claimInvoiceMaxQtyEditor;
        private Infragistics.Win.Misc.UltraLabel _reclaimInvoiceMaxQtyEditor;
    }
}
