﻿using CommonComponents.Uc.Infrastructure.Interface;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public class PresenterCreator : Presenter<IParametersView>
    {
        public object Create(IParametersView view)
        {
            return new ParametersPresenter(view, new SettingsServiceProxy(
                        WorkItem.GetWcfServiceProxy<ISystemSettingsService>(),
                        WorkItem.Services.Get<INotifier>(ensureExists: true),
                        WorkItem.Services.Get<IClientLogger>(ensureExists: true)));
        }
    }
}
