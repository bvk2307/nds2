﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.Helpers;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public partial class ParametersView : BaseView, IParametersView
    {
        #region Создание Presenter-а

        [CreateNew]
        public PresenterCreator PresenterCreator
        {
            set
            {
                var creator = value;
                WorkItem = value.WorkItem;
                creator.Create(this);
            }
        }

        # endregion

        # region .ctor

        public ParametersView()
        {
            InitializeComponent();
        }

        public ParametersView(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
        }

        private void InitializeRibbon()
        {
            var resourceManagersService =
                WorkItem.Services.Get<IUcResourceManagersService>(true);

            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(
                    ResourceManagerNDS2.NDS2ClientResources,
                    Properties.Resources.ResourceManager);
            }
            //закладка
            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2SystemSettings")
            {
                Text = _presentationContext.WindowTitle,
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var groupFuncs = tabNavigator.AddGroup("NDS2ContragentParamsManage");
            groupFuncs.Text = "Функции";
            groupFuncs.Visible = true;

            var btnSave = new UcRibbonButtonToolContext(_presentationContext, "btnSaveSettings", "cmdSaveSystemSettings")
            {
                Text = "Сохранить",
                ToolTipText = "Сохранить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "_16_save",
                SmallImageName = "_16_save",
                Enabled = true,
                Visible = true
            };

            groupFuncs.ToolList.AddRange(new[]
                {
                    new UcRibbonToolInstanceSettings(btnSave.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
           {
                btnSave,
                tabNavigator
           });
            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }
        # endregion

        # region IParametersView

        public event EventHandler Opened;

        public void EnableEdit()
        {
        }

        public int ClaimInvoiceMaxQuantity
        {
            set
            {
                _claimInvoiceMaxQtyEditor.Text = value.ToString();
            }
        }

        public int ReclaimInvoiceMaxQuantity
        {
            set
            {
                _reclaimInvoiceMaxQtyEditor.Text = value.ToString();
            }
        }

        public int ClaimResendingTimeout
        {
            get
            {
                return (int) _claimResendingTimeoutEditor.Value;
            }
            set
            {
                _claimResendingTimeoutEditor.Value = value;
            }
        }

        public event EventHandler ClaimResendingChanged;

        public int ResendingAttempts
        {
            get
            {
                return (int)_resendingAttemptsEditor.Value;
            }
            set
            {
                _resendingAttemptsEditor.Value = value;
            }
        }

        public event EventHandler ResendingAttemptsChanged;

        public int ClaimDeliveryTimeout
        {
            get
            {
                return (int)_claimDeliveryTimeoutEditor.Value;
            }
            set
            {
                _claimDeliveryTimeoutEditor.Value = value;
            }
        }

        public event EventHandler ClaimDeliveryTimeoutChanged;

        public int ClaimExplainTimeout
        {
            get
            {
                return (int) _claimExplainTimeoutEditor.Value;
            }
            set
            {
                _claimExplainTimeoutEditor.Value = value;
            }
        }

        public event EventHandler ClaimExplainTimeoutChanged;

        public int ReclaimReplyTimeout
        {
            get
            {
                return (int) _reclaimReplyTimeoutEditor.Value;
            }
            set
            {
                _reclaimReplyTimeoutEditor.Value = value;
            }
        }

        public event EventHandler ReclaimReplyTimeoutChanged;

        public int ReplyEntryTimeout
        {
            get
            {
                return (int) _replyEntryTimeoutEditor.Value;
            }
            set
            {
                _replyEntryTimeoutEditor.Value = value;
            }
        }

        public event EventHandler ReplyEntryTimeoutChanged;

        private bool _hasChanges;
        public bool HasChanges
        {
            set { _hasChanges = value; }
        }

        public event EventHandler Saving;

        public event EventHandler Closing;

        #endregion

        private void ClaimResendingTimeoutValueChanged(object sender, EventArgs e)
        {
            if (ClaimResendingChanged != null)
                ClaimResendingChanged(sender, e);
        }

        private void ResendingAttemptsEditorValueChanged(object sender, EventArgs e)
        {
            if (ResendingAttemptsChanged != null)
                ResendingAttemptsChanged(sender, e);
        }

        private void ClaimDeliveryTimeoutEditorValueChanged(object sender, EventArgs e)
        {
            if (ClaimDeliveryTimeoutChanged != null)
                ClaimDeliveryTimeoutChanged(sender, e);
        }

        private void ClaimExplainTimeoutEditorValueChanged(object sender, EventArgs e)
        {
            if (ClaimExplainTimeoutChanged != null)
                ClaimExplainTimeoutChanged(sender, e);
        }

        private void ReclaimReplyTimeoutEditor_ValueChanged(object sender, EventArgs e)
        {
            if (ReclaimReplyTimeoutChanged != null)
                ReclaimReplyTimeoutChanged(sender, e);
        }

        private void ReplyEntryTimeoutEditorValueChanged(object sender, EventArgs e)
        {
            if (ReplyEntryTimeoutChanged != null)
                ReplyEntryTimeoutChanged(sender, e);
        }

        private void ViewOpened(object sender, EventArgs e)
        {
            if (Visible && Opened != null)
            {
                Opened(sender, e);
                InitializeRibbon();
                _claimInvoiceMaxQtyEditor.BackColor = _claimInvoiceMaxQtyEditor.BackColor;
            }
        }

        [CommandHandler("cmdSaveSystemSettings")]
        public void CmdSave(object sender, EventArgs e)
        {
            if (Saving != null)
                Saving(sender, e);
        }

        public void ShowNotification()
        {
            //WorkItem.Services.Get<INotifier>(ensureExists: true).ShowNotification("Изменения сохранены");
            DialogHelper.Inform("Изменения сохранены");
        }

        private void KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            e.SuppressKeyPress = e.IsPaste();
        }
    }
}
