﻿using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using System;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public class ParametersPresenter
    {
        # region .ctor

        public ParametersPresenter(IParametersView view, ISettingsServiceProxy proxy)
        {
            _view = view;
            _proxy = proxy;

            _view.Opened += ViewOpened;
        }

        # endregion

        # region Загрузка\Сохранение данных

        private readonly ISettingsServiceProxy _proxy;

        private ParametersModel _model;

        private void ViewOpened(object sender, EventArgs e)
        {
            SystemSettings data;
            if (_proxy.TryLoad(out data))
            {
                _model = new ParametersModel(data);
                InitView();                
            }
        }

        # endregion

        # region Инициализация представления

        private readonly IParametersView _view;

        /// <summary>
        /// инициализирует поля View из Модели и подписываемся на события View
        /// </summary>
        private void InitView()
        {
            _view.ClaimDeliveryTimeout = _model.ClaimDeliveryTimeout;
            _view.ClaimDeliveryTimeoutChanged += ClaimDeliveryTimeoutChanged;

            _view.ClaimExplainTimeout = _model.ClaimExplainTimeout;
            _view.ClaimExplainTimeoutChanged += ClaimExplainTimeoutChanged;

            _view.ResendingAttempts = _model.ResendingAttempts;
            _view.ResendingAttemptsChanged += ResendingAttemptsChanged;

            _view.ClaimResendingTimeout = _model.ClaimResendingTimeout;
            _view.ClaimResendingChanged += ClaimResendingChanged;

            _view.ReclaimReplyTimeout = _model.ReclaimReplyTimeout;
            _view.ReclaimReplyTimeoutChanged += ReclaimReplyTimeoutChanged;

            _view.ReplyEntryTimeout = _model.ReplyEntryTimeout;
            _view.ReplyEntryTimeoutChanged += ReplyEntryTimeoutChanged;

            _view.Saving += SaveSettings;
            HasChangesSet();
            _view.EnableEdit();
        }

        /// <summary>
        /// Обработака события, когда параметр Время ожиданя ввода ответа на АИ был изменен
        /// </summary>
        private void ReplyEntryTimeoutChanged(object sender, EventArgs e)
        {
            _model.ReplyEntryTimeout = (int)(sender as UltraNumericEditor).Value;
        }
        
        /// <summary>
        /// Обработака события, когда параметр время ожидания Вручения АТ/АИ был изменен
        /// </summary>
        private void ClaimDeliveryTimeoutChanged(object sender, EventArgs e)
        {
            _model.ClaimDeliveryTimeout = (int)(sender as UltraNumericEditor).Value;
        }

        /// <summary>
        /// Обработака события, когда параметр время ожидания ответа на АТ 
        /// </summary>
        private void ClaimExplainTimeoutChanged(object sender, EventArgs e)
        {
            _model.ClaimExplainTimeout = (int)(sender as UltraNumericEditor).Value;
        }

        /// <summary>
        /// Обработака события, когда параметр количество повторных отправок был изменен
        /// </summary>
        private void ResendingAttemptsChanged(object sender, EventArgs e)
        {
            _model.ResendingAttempts = (int)(sender as UltraNumericEditor).Value;
        }

        /// <summary>
        /// Обработака события, когда параметр время ожидания ответа от СЭОД был изменен
        /// </summary>
        private void ClaimResendingChanged(object sender, EventArgs e)
        {
            _model.ClaimResendingTimeout = (int)(sender as UltraNumericEditor).Value;
        }

        /// <summary>
        /// Обработака события, когда параметр время ожидания ответа на АИ был изменен
        /// </summary>
        private void ReclaimReplyTimeoutChanged(object sender, EventArgs e)
        {
            _model.ReclaimReplyTimeout = (int)(sender as UltraNumericEditor).Value;
        }

        /// <summary>
        /// Сохранение параметров
        /// </summary>
        private void SaveSettings(object sender, EventArgs e)
        {
            var data = _model.GetData();
            if (_model.HasChanges)
            {
                if (_proxy.TrySave(data))
                {
                    _model.ResetChanges();
                    _view.ShowNotification();
                }
            }
        }

        private void HasChangesSet()
        {
            _view.HasChanges = _model.HasChanges;
        }

        # endregion
    }
}
