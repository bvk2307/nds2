﻿using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    /// <summary>
    /// Интерфейс реализующий работу с настройками Параметры АСК НДС-2
    /// </summary>
    public interface ISettingsServiceProxy
    {
        /// <summary>
        /// Загружает параметры
        /// </summary>
        /// <param name="settings">загруженные параметры</param>
        /// <returns></returns>
        bool TryLoad(out SystemSettings settings);

        /// <summary>
        /// Сохраняет параметры
        /// </summary>
        /// <param name="settings">параметры для сохранения</param>
        /// <returns></returns>
        bool TrySave(SystemSettings settings);
    }
}
