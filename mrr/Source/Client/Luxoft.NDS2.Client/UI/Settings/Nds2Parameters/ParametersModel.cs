﻿using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    /// <summary>
    /// Этот класс реализует модель параметров АСК
    /// </summary>
    public class ParametersModel
    {
        # region .ctor

        private readonly SystemSettings _data;

        /// <summary>
        /// Создает новый экземпляр класса ParametersModel
        /// </summary>
        /// <param name="data">Значения параметров АСК</param>
        public ParametersModel(SystemSettings data)
        {
            _data = data;
            ResetChanges();
        }

        # endregion

        # region Повторная отправка автотребования

        /// <summary>
        /// Возвращает или задает время ожидания ответа от СЭОД
        /// </summary>
        public int ClaimResendingTimeout
        {
            get
            {
                return _data.ClaimResendingTimeout.SecondsToDays();
            }
            set
            {
                _data.ClaimResendingTimeout = value.DaysToSeconds();
            }
        }

        /// <summary>
        /// Возвращает или задает количество повторных отправок
        /// </summary>
        public int ResendingAttempts
        {
            get
            {
                return _data.ResendingAttempts;
            }
            set
            {
                _data.ResendingAttempts = value;
            }
        }

        # endregion

        # region Время ожидания

        /// <summary>
        /// Возвращает или задает время ожидания вручения автотребования/автоистребования
        /// </summary>
        public int ClaimDeliveryTimeout
        {
            get
            {
                return _data.ClaimDeliveryTimeout.SecondsToDays();
            }
            set
            {
                _data.ClaimDeliveryTimeout = value.DaysToSeconds();
            }
        }

        /// <summary>
        /// Возвращает или задает время ожидания ответа на автотребование
        /// </summary>
        public int ClaimExplainTimeout
        {
            get
            {
                return _data.ClaimExplainTimeout.SecondsToDays();
            }
            set
            {
                _data.ClaimExplainTimeout = value.DaysToSeconds();
            }
        }

        /// <summary>
        /// Возвращает или задает время ожидания на автоистребование
        /// </summary>
        public int ReclaimReplyTimeout
        {
            get
            {
                return _data.ReclaimReplyTimeout.SecondsToDays();
            }
            set
            {
                _data.ReclaimReplyTimeout = value.DaysToSeconds();
            }
        }

        /// <summary>
        /// Возвращает или задает время ожидания ввода ответа на автоистребование
        /// </summary>
        public int ReplyEntryTimeout
        {
            get
            {
                return _data.ReplyEntryTimeout.SecondsToDays();
            }
            set
            {
                _data.ReplyEntryTimeout = value.DaysToSeconds();
            }
        }

        # endregion

        # region Отслеживание изменений

        private SystemSettings _dataCopy;

        /// <summary>
        /// Определяет были ли изменены настройки.
        /// </summary>
        public bool HasChanges
        {
            get
            {
                return !_data.EqualsTo(_dataCopy);
            }
        }

        /// <summary>
        /// Сбрасывает изменение настроек
        /// </summary>
        public void ResetChanges()
        {
            _dataCopy = _data.Clone();
        }

        /// <summary>
        /// Возвращает объект с измененными настройками
        /// </summary>
        /// <returns></returns>
        public SystemSettings GetData()
        {
            return _data;
        }
        # endregion
    }
}
