﻿using System;

namespace Luxoft.NDS2.Client.UI.Settings.Nds2Parameters
{
    public static class TimeoutConvertionHelper
    {
        /// <summary>
        /// Вычисляет целое количество дней в секундах
        /// </summary>
        /// <param name="seconds">Количество секунд</param>
        /// <returns>Количество дней</returns>
        public static int SecondsToDays(this int seconds)
        {
            var timeSpan = new TimeSpan(TimeSpan.TicksPerSecond * seconds);

            return (int)Math.Ceiling(timeSpan.TotalDays);
        }

        /// <summary>
        /// Вычисляет количество секунд в днях
        /// </summary>
        /// <param name="days">Количество дней</param>
        /// <returns>Количество секунд</returns>
        public static int DaysToSeconds(this int days)
        {
            var timespan = new TimeSpan(TimeSpan.TicksPerDay * days);

            return (int)Math.Ceiling(timespan.TotalSeconds);
        }
    }
}
