﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public static class Constants
    {
        public static char InnDelimeterComma = ',';
        public static char InnDelimeterWhiteSpace = ' ';
    }
}
