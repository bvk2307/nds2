﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    /// <summary>
    /// Класс реализует загрузку БС-ов
    /// </summary>
    public class WhiteListsDataLoader : ServiceProxyBase<List<SystemSettingsList>>,
        IServerGridDataProvider<List<SystemSettingsList>>
    {
        private readonly ISettingsListsService _service;

        public WhiteListsDataLoader(ISettingsListsService service, INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        public event EventHandler<DataLoadedEventArgs<List<SystemSettingsList>>> DataLoaded;

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _service.All(queryConditions));
        }

        protected override void CallBack(List<SystemSettingsList> result)
        {
            if (DataLoaded != null)
                DataLoaded(this, new DataLoadedEventArgs<List<SystemSettingsList>>(result));
        }
    }
}