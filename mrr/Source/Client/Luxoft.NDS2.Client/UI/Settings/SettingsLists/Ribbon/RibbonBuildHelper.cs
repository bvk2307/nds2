﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Ribbon
{
    public static class RibbonBuildHelper
    {
        public static RibbonGroup WithRefreshWhiteLists(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("RefreshWhiteLists")
                        .WithText(ResourceManagerNDS2.UpdateWhiteListsTitle)
                        .WithToolTip(ResourceManagerNDS2.UpdateWhiteListsTooltip)
                        .WithCommonResource()
                        .WithImage("update"));
        }

        public static RibbonGroup WithCreateWhiteList(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("CreateWhiteList")
                        .WithText(ResourceManagerNDS2.CreateWhiteListTitle)
                        .WithToolTip(ResourceManagerNDS2.CreateWhiteListTooltip)
                        .WithCommonResource()
                        .WithImage("window_new_32_32"));
        }

        public static RibbonGroup WithExcludeList(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("ExcludeList")
                        .WithText(ResourceManagerNDS2.ExcludeListTitle)
                        .WithToolTip(ResourceManagerNDS2.ExcludeListListTooltip)
                        .WithCommonResource()
                        .WithImage("settings_list_32_32"));
        }

        public static RibbonManager WithTaxMonitorList(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithLastButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("TaxMonitorList")
                        .WithText(ResourceManagerNDS2.TaxMonitorListTitle)
                        .WithToolTip(ResourceManagerNDS2.TaxMonitorListListTooltip)
                        .WithCommonResource()
                        .WithImage("settings_list_32_32"));
        }

       public static RibbonGroup WithBack(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group
                .WithButton(
                    UcRibbonToolSize.Large,
                    context
                        .NewButton("GoToPrevious")
                        .WithText(ResourceManagerNDS2.BackButtonTitle)
                        .WithToolTip(ResourceManagerNDS2.BackButtonTooltip)
                        .WithCommonResource()
                        .WithImage("back_arrow_32_32"));
        }

        public static RibbonManager WithSave(
            this RibbonGroup group,
            PresentationContext context)
        {
            return group.WithLastButton(
                UcRibbonToolSize.Large,
                context
                    .NewButton("SaveSettingsList")
                    .WithText(ResourceManagerNDS2.SaveListTitle)
                    .WithToolTip(ResourceManagerNDS2.SaveListTooltip)
                    .WithCommonResource()
                    .WithImage("_16_save"));
        }

        public static UcRibbonButtonToolContext WithCommonResource(
            this UcRibbonButtonToolContext button)
        {
            return button.WithResource(ResourceManagerNDS2.NDS2ClientResources);
        }
    }
}
