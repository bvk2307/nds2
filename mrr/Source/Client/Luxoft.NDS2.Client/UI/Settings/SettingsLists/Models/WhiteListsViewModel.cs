﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models
{
    public class WhiteListsViewModel : ListViewModel<SettingsListViewModel, SystemSettingsList>
    {

        public WhiteListsViewModel()
        {
        }

        protected override SettingsListViewModel ConvertToModel(SystemSettingsList dataItem)
        {
            return new SettingsListViewModel(dataItem);
        }
    }
}