﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models
{
    public class SettingsListDetailViewModel  : INotifyPropertyChanged
    {
        private string _innList;
        private string _listName;
        private SettingsListType _listType;
        private bool _isActive;
        public SettingsListDetailViewModel() 
        {
        }

        /// <summary>
        /// Возвращает список INN
        /// </summary>
        public string InnList {
            get { return _innList; }
            set
            {
                _innList = value;
                NotifyPropertyChanged(TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.InnList));
            }
        }

        /// <summary>
        /// Устанавливает ИНН список
        /// </summary>
        /// <param name="innList"></param>
        public void SetInnList(string[] innList)
        {
            InnList = string.Empty;
            InnList = innList != null ? string.Join(Constants.InnDelimeterComma + " ", innList) : string.Empty;
        }

        /// <summary>
        /// Возвращает тип списка
        /// </summary>
        public virtual SettingsListType ListType
        {
            get { return _listType; }
            set
            {
                _listType = value;
                NotifyPropertyChanged(TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.ListType));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Возвращает true, если список ИНН валиден
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
           return this.Validate();
        }

        /// <summary>
        /// Название списка
        /// </summary>
        public virtual string ListName
        {
            get { return _listName; }
            set
            {
                _listName = value;
                NotifyPropertyChanged(TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.ListName));
            }
        }

        /// <summary>
        /// Признак активирован список или нет
        /// </summary>
        public virtual bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                NotifyPropertyChanged(TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.IsActive));
            }
        }

        private void NotifyPropertyChanged(string sPropName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(sPropName));
            }
        }
    }
}
