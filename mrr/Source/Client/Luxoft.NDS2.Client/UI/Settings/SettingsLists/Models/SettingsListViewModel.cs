﻿using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using System;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models
{
    public class SettingsListViewModel
    {
        private SystemSettingsList _data;

        public SettingsListViewModel(SystemSettingsList data)
        {
            _data = data;
        }

        /// <summary>
        ///     Возвращает активен ли список
        /// </summary>
        public bool IsActive
        {
            get { return _data.IsActive; }
        }

        /// <summary>
        ///     Возвращает название списка
        /// </summary>
        public string Name
        {
            get { return _data.Name; }
        }

        /// <summary>
        ///     Возвращает дату создания
        /// </summary>
        public DateTime CreatedDate
        {
            get { return _data.CreatedDate; }
        }

        /// <summary>
        ///     Возвращает кем был создан список
        /// </summary>
        public string CreatedBy
        {
            get { return _data.CreatedBy; }
        }

        /// <summary>
        ///    Тип списка
        /// </summary>
        public SettingsListType ListType
        {
            get { return (SettingsListType) _data.ListType; }
        }
        
        /// <summary>
        /// Идентификатор списка
        /// </summary>
        public long Id
        {
            get { return _data.Id; }
        }

        /// <summary>
        ///     Возвращает неизмененные данные формы
        /// </summary>
        /// <returns></returns>
        public SystemSettingsList GetData()
        {
            return _data;
        }

        /// <summary>
        ///     Устанавливает данные для инициализации формы
        /// </summary>
        /// <returns></returns>
        public void SetData(SystemSettingsList data)
        {
            _data = data;
        }
    }
}