﻿using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using System;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public interface IWhiteListsView 
    {
        event EventHandler Opened;
        event EventHandler MoveBack;
        event EventHandler SavingList;
        event EventHandler RefreshWhiteListsClicked;
        event EventHandler CreateWhiteListClicked;
        event EventHandler TaxMonitorListClicked;
        event EventHandler ExcludeListClicked;

        /// <summary>
        /// Возвращает контрол списка 
        /// </summary>
        /// <returns></returns>
        WhiteListsGridView GetGrid();

        /// <summary>
        /// Устанавливает видимость контрола списка
        /// </summary>
        bool GridVisible { set; }

        /// <summary>
        /// Возвращает форму детализации списка
        /// </summary>
        /// <returns></returns>
        SettingsListDetailView GetSettingsListDetailView();

        /// <summary>
        /// Устанавливает видимость кнопки "Назад"
        /// </summary>
        bool RibbonFunctionalButtonsVisible { set; }

        /// <summary>
        /// Устанавливает видимость кнопки "Сохранить"
        /// </summary>
        bool RibbonSaveButtonVisible { set; }

        /// <summary>
        /// Устанавливает видимость кнопкам по работе со списками
        /// </summary>
        bool RibbonListButtonsVisible { set; }

        /// <summary>
        /// Показывает форму детализации ИВ
        /// </summary>
        void ShowExcludeListView();

        /// <summary>
        /// Показывает форму детализации списка НМ
        /// </summary>
        void ShowTaxMonitorListView();

        /// <summary>
        /// Показывает форму создания БС
        /// </summary>
        void ShowWhiteListEditableView();

        /// <summary>
        /// показывает форму детализации БС
        /// </summary>
        void ShowWhiteListView();

        /// <summary>
        /// показывает список БС-ов
        /// </summary>
        void ShowListsView();
     
    }
}
