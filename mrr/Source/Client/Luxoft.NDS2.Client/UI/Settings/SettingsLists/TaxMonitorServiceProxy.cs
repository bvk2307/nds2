﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.Services;


namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class TaxMonitorServiceProxy : ServiceProxyBase, ITaxMonitorListServiceProxy
    {
        private readonly ISettingsListsService _service;

        public TaxMonitorServiceProxy(ISettingsListsService service, INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        /// <summary>
        ///  Сохраняет список НМ
        /// </summary>
        /// <returns></returns>
        public bool TrySave(SystemSettingsList list)
        {
            list.Name = ResourceManagerNDS2.TaxMonitorListTitle;

            var result =
                Invoke(
                    () => _service.SaveTaxMonitorList(list),
                    () => new OperationResult
                    {
                        Status = ResultStatus.Error
                    });

            return result.Status == ResultStatus.Success;
        }
    }
}