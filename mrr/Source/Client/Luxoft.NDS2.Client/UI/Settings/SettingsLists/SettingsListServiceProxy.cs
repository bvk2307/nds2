﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class SettingsListServiceProxy : ServiceProxyBase, ISettingsListServiceProxy
    {
        private readonly ISettingsListsService _service;

        public SettingsListServiceProxy(ISettingsListsService service, INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        /// <summary>
        ///     Загружает список БС-ов
        /// </summary>
        /// <param name="lists">загруженые списки</param>
        /// <returns></returns>
        public bool TryLoadWhiteLists(QueryConditions qc, out List<SystemSettingsList> lists)
        {
            lists =
                Invoke(
                        () => _service.All(qc),
                        () => new OperationResult<List<SystemSettingsList>>())
                    .Result;

            return lists != null;
        }

        /// <summary>
        ///     Загружает НМ спиоск
        /// </summary>
        /// <param name="list">загруженный список</param>
        /// <returns></returns>
        public bool TryLoadTaxMonitorList(out SystemSettingsList list)
        {
            list =
                Invoke(
                        () => _service.LoadTaxMonitorList(),
                        () => new OperationResult<SystemSettingsList>())
                    .Result;

            return list != null;
        }

        /// <summary>
        ///     Загружает ИВ список
        /// </summary>
        /// <param name="list">загруженый список</param>
        /// <returns></returns>
        public bool TryLoadExcludeList(out SystemSettingsList list)
        {
            list =
                Invoke(
                        () => _service.LoadExcludeList(),
                        () => new OperationResult<SystemSettingsList>())
                    .Result;

            return list != null;
        }

        /// <summary>
        ///     Загружает список ИНН
        /// </summary>
        /// <param name="id"></param>
        /// <param name="innList"></param>
        /// <returns></returns>
        public bool TryLoadInnList(long id, out string[] innList)
        {
            innList =
                Invoke(
                        () => _service.LoadInnList(id),
                        () => new OperationResult<string[]>())
                    .Result;

            return innList != null;
        }

        /// <summary>
        ///     Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        public bool TryActivate(long id)
        {
            var result =
                Invoke(
                    () => _service.Activate(id),
                    () => new OperationResult
                    {
                        Status = ResultStatus.Error
                    });

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        ///     Деактивирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        public bool TryDeactivate(long id)
        {
            var result =
                Invoke(
                    () => _service.Deactivate(id),
                    () => new OperationResult
                    {
                        Status = ResultStatus.Error
                    });

            return result.Status == ResultStatus.Success;
        }

        /// <summary>
        ///     Возвращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        public string GetUserDisplayName()
        {
            return (string) Invoke(
                    () => _service.GetUserDisplayName(),
                    () => new OperationResult<object>())
                .Result;
        }
    }
}