﻿using System;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public static class ValidateHelper
    {
        private const int UlInnLength = 10; //длина ИНН юр лица
        private const int FlInnLength = 12; //длиина ИНН физ лица

        /// <summary>
        ///     Валидирует модель списка. Возвращает true, если список ИНН валиден
        /// </summary>
        /// <returns></returns>
        public static bool Validate(this SettingsListDetailViewModel model)
        {
            var hasError = false;
            char[] delimeters = {Constants.InnDelimeterComma, Constants.InnDelimeterWhiteSpace};
            var resInnList = model.InnList.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < resInnList.Length; i++)
            {
                var inn = resInnList[i].Trim();
                if ((inn.Length == UlInnLength) || (inn.Length == FlInnLength))
                {
                    long numInn;
                    long.TryParse(inn, out numInn);
                    if (numInn == 0)
                    {
                        hasError = true;
                        break;
                    }
                }
                else
                {
                    hasError = true;
                    break;
                }
            }
            return !hasError;
        }
    }
}