﻿using CommonComponents.Uc.Infrastructure.Interface;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class ListsPresenterCreator : Presenter<IWhiteListsView>
    {
        public object Create(IWhiteListsView view, WhiteListsViewModel model)
        {
            var service = WorkItem.GetWcfServiceProxy<ISettingsListsService>();
            var notifier = WorkItem.Services.Get<INotifier>(true);
            var logger = WorkItem.Services.Get<IClientLogger>(true);
            return new WhiteListsPresenter(view,
                model,
                new SettingsListServiceProxy(service, notifier, logger),
                new WhiteListServiceProxy(service, notifier, logger),
                new TaxMonitorServiceProxy(service, notifier, logger),
                new ExcludeListServiceProxy(service, notifier, logger),  
                new WhiteListsDataLoader(service, notifier, logger)
            );
        }
    }
}