﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.Services;


namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class WhiteListServiceProxy : ServiceProxyBase, IWhiteListServiceProxy
    {
        private readonly ISettingsListsService _service;

        public WhiteListServiceProxy(ISettingsListsService service, INotifier notifier,
            IClientLogger logger) : base(notifier, logger)
        {
            _service = service;
        }

        /// <summary>
        /// Создает БС
        /// </summary>
        /// <returns></returns>
        public bool TrySave(SystemSettingsList list)
        {
            var result =
                Invoke(
                    () => _service.SaveWhiteList(list),
                    () => new OperationResult
                    {
                        Status = ResultStatus.Error
                    });

            if (result.Status == ResultStatus.Success)
                result =
                    Invoke(
                        () => _service.HiveSyncWhiteList(),
                        () => new OperationResult
                        {
                            Status = ResultStatus.Error
                        });
            return result.Status == ResultStatus.Success;
        }
    }
}