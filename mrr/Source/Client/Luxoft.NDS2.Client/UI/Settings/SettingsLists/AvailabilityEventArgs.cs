﻿using System;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class AvailabilityEventArgs : EventArgs
    {
        public bool Available { get; set; }
    }
}
