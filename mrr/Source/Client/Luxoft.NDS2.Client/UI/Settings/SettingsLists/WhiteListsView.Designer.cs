﻿using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    partial class WhiteListsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._listsContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._settingsListDetailView = new Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls.SettingsListDetailView();
            this.SuspendLayout();
            // 
            // _listsContainer
            // 
            this._listsContainer.AutoScroll = true;
            this._listsContainer.AutoSize = true;
            this._listsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listsContainer.Location = new System.Drawing.Point(0, 0);
            this._listsContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._listsContainer.Name = "_listsContainer";
            this._listsContainer.Size = new System.Drawing.Size(920, 639);
            this._listsContainer.TabIndex = 0;
            // 
            // _settingsListDetailView
            // 
            this._settingsListDetailView.AutoSize = true;
            this._settingsListDetailView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._settingsListDetailView.InnList = "";
            this._settingsListDetailView.IsActive = false;
            this._settingsListDetailView.ListName = "";
            this._settingsListDetailView.Location = new System.Drawing.Point(0, 0);
            this._settingsListDetailView.Name = "_settingsListDetailView";
            this._settingsListDetailView.Size = new System.Drawing.Size(920, 639);
            this._settingsListDetailView.TabIndex = 1;
            this._settingsListDetailView.Visible = false;
            // 
            // WhiteListsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._settingsListDetailView);
            this.Controls.Add(this._listsContainer);
            this.Name = "WhiteListsView";
            this.Size = new System.Drawing.Size(920, 639);
            this.Load += new System.EventHandler(this.SettingsListsViewLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.Controls.Grid.Controls.GridContainer _listsContainer;
        private SettingsListDetailView _settingsListDetailView;
    }
}
