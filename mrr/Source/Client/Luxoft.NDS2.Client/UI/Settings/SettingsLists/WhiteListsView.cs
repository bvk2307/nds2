﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Extensions;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Ribbon;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    /// <summary>
    /// Этот класс реализует представление экранной формы "Настройка списков"
    /// </summary>
    public partial class WhiteListsView : BaseView, IWhiteListsView
    {

        [CreateNew]
        public ListsPresenterCreator ListPresenterCreator
        {
            set
            {
                var creator = value;
                WorkItem = value.WorkItem;
                creator.Create(this, new WhiteListsViewModel());
            }
        }

        public WhiteListsView()
        {
            InitializeComponent();
            InitListsComponent();
        }

        public WhiteListsView(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
            InitListsComponent();
        }

        private readonly WhiteListsGridView _listsGrid = new WhiteListsGridView();

        /// <summary>
        /// Инициализирует грид со списко БС
        /// </summary>
        private void InitListsComponent()
        {
            _listsContainer
                .WithGrid(_listsGrid)
                .WithGridResetter(new GridColumnSortResetter(_listsGrid))
                .WithGridResetter(new GridColumnFilterResetter(_listsGrid));

            GetSettingsListDetailView().CutAvailable = !GetSettingsListDetailView().IsReadonly;
            GetSettingsListDetailView().PasteAvailable = !GetSettingsListDetailView().IsReadonly;
        }

        #region События

        public event EventHandler Opened;
        public event EventHandler MoveBack;
        public event EventHandler SavingList;
        public event EventHandler RefreshWhiteListsClicked;
        public event EventHandler CreateWhiteListClicked;
        public event EventHandler TaxMonitorListClicked;
        public event EventHandler ExcludeListClicked;

        /// <summary>
        /// Событие Load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsListsViewLoad(object sender, EventArgs e)
        {
            if (Opened != null)
            {
                Opened(sender, e);
                InitializeListRibbon();
                _listsContainer
                .WithGridSettingsStorage(new ColumnSortOrderStorage(_listsGrid.Columns,
                    new LocalStorage.LocalStorage(WorkItem), "WhiteListsView_SortStorage"))
                .WithGridSettingsStorage(new ColumnFilterStorage(_listsGrid.Columns,
                    new LocalStorage.LocalStorage(WorkItem), "WhiteListsView_FilterStorage"));
                _listsGrid.GridEvents.Filter();
            }
        }

        #endregion

        #region Компоненты view

        /// <summary>
        /// Возвращает контрол списка 
        /// </summary>
        /// <returns></returns>
        public WhiteListsGridView GetGrid()
        {
            return _listsGrid;
        }
        
        /// <summary>
        /// Возвращает форму детализации списка
        /// </summary>
        /// <returns></returns>
        public SettingsListDetailView GetSettingsListDetailView()
        {
            return _settingsListDetailView;
        }

        #endregion

        #region Ribbon Panel

        private RibbonManager _ribbonMenu;

        /// <summary>
        /// Инициализирует панель с кнопками
        /// </summary>
        private void InitializeListRibbon()
        {
            _ribbonMenu =
                new RibbonManager(
                    _presentationContext,
                    WorkItem,
                    "SystemSettingsLists",
                    _presentationContext.WindowTitle,
                    _presentationContext.WindowTitle)
                .WithGroup("SystemSettingsListsGroup", "Списки")
                    .WithRefreshWhiteLists(_presentationContext)
                    .WithCreateWhiteList(_presentationContext)
                    .WithExcludeList(_presentationContext)
                    .WithTaxMonitorList(_presentationContext)
                .WithGroup("SystemSettingsListsFunctionsGroup", "Функции")
                    .WithBack(_presentationContext)
                    .WithSave(_presentationContext);

            _ribbonMenu.Init();
            RibbonListButtonsVisible = true;
            RibbonFunctionalButtonsVisible = false;
        }

        /// <summary>
        /// Нажатие на кнопку на панели "Обновить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("RefreshWhiteLists")]
        public void RefreshWhiteLists(object sender, EventArgs e)
        {
            if (RefreshWhiteListsClicked != null)
                RefreshWhiteListsClicked(sender, e);
        }

        /// <summary>
        /// Нажатие на кнопку на панели "Создать БС"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("CreateWhiteList")]
        public void CreateWhiteList(object sender, EventArgs e)
        {
            if (CreateWhiteListClicked != null)
                CreateWhiteListClicked(sender, e);
        }

        /// <summary>
        /// Нажатие на кнопку на панели "список ИВ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("ExcludeList")]
        public void ExcludeList(object sender, EventArgs e)
        {
            if (ExcludeListClicked != null)
                ExcludeListClicked(sender, e);
        }

        /// <summary>
        /// Нажатие на кнопку на панели "Список НМ"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("TaxMonitorList")]
        public void TaxMonitorList(object sender, EventArgs e)
        {
            if (TaxMonitorListClicked != null)
                TaxMonitorListClicked(sender, e);
        }

        /// <summary>
        /// Нажатие на кнопку на панели "Назад"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("GoToPrevious")]
        public void GoToPrevious(object sender, EventArgs e)
        {
            if (MoveBack != null)
                MoveBack(sender, e);
        }

        /// <summary>
        /// Нажатие на кнопку на панели "Сохранить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [CommandHandler("SaveSettingsList")]
        public void SaveSettingsList(object sender, EventArgs e)
        {
            if (SavingList != null)
                SavingList(sender, e);
        }


        #endregion

        #region Работа с видимостью компонент на форме

        /// <summary>
        /// показывает форму детализации БС
        /// </summary>
        public void ShowWhiteListView()
        {
            _settingsListDetailView.NamePanelVisible = true;
            _settingsListDetailView.ActiveCheckBoxPanelVisible = false;
            _settingsListDetailView.ActiveLabelPanelVisible = true;
            _settingsListDetailView.ReadOnly = true;
            _settingsListDetailView.Visible = true;

            GridVisible = false;

            RibbonFunctionalButtonsVisible = true;
            RibbonSaveButtonVisible = false;
            RibbonListButtonsVisible = false;

            SetWindowTitle("Просмотр БС");
        }

        /// <summary>
        /// Показывает форму создания БС
        /// </summary>
        public void ShowWhiteListEditableView()
        {
            _settingsListDetailView.NamePanelVisible = true;
            _settingsListDetailView.ActiveCheckBoxPanelVisible = true;
            _settingsListDetailView.ActiveLabelPanelVisible = false;
            _settingsListDetailView.ReadOnly = false;
            _settingsListDetailView.Visible = true;

            GridVisible = false;

            RibbonFunctionalButtonsVisible = true;
            RibbonSaveButtonVisible = true;
            RibbonListButtonsVisible = false;

            SetWindowTitle("Создание БС");
        }

        /// <summary>
        /// Показывает форму детализации ИВ
        /// </summary>
        public void ShowExcludeListView()
        {
            _settingsListDetailView.NamePanelVisible = false;
            _settingsListDetailView.ActiveCheckBoxPanelVisible = false;
            _settingsListDetailView.ActiveLabelPanelVisible = false;
            _settingsListDetailView.ReadOnly = false;
            _settingsListDetailView.Visible = true;

            GridVisible = false;

            RibbonFunctionalButtonsVisible = true;
            RibbonSaveButtonVisible = true;
            RibbonListButtonsVisible = false;

            SetWindowTitle("Список ИВ");
        }

        /// <summary>
        /// Показывает форму детализации списка НМ
        /// </summary>
        public void ShowTaxMonitorListView()
        {
            ShowExcludeListView();
            SetWindowTitle("Список НМ");
        }

        /// <summary>
        /// Устанавливает видимость кнопки "Сохранить"
        /// </summary>
        public bool RibbonSaveButtonVisible
        {
            set {
                _ribbonMenu.ToggleAvailability(new string[] {"SaveSettingsList"}, value);
                _ribbonMenu.Refresh();
            }
        }

        /// <summary>
        /// Устанавливает видимость кнопкам по работе со списками
        /// </summary>
        public bool RibbonListButtonsVisible
        {
            set
            {
                _ribbonMenu.ToggleVisibility(
                    new[] {"RefreshWhiteLists", "CreateWhiteList", "ExcludeList", "TaxMonitorList"}, value);
                _ribbonMenu.Refresh();

            }
        }

        /// <summary>
        /// Устанавливает видимость кнопки "Назад"
        /// </summary>
        public bool RibbonFunctionalButtonsVisible
        {
            set {
                _ribbonMenu.ToggleVisibility(new[] {"GoToPrevious", "SaveSettingsList"}, value);
                _ribbonMenu.Refresh();
            }
        }

        /// <summary>
        /// Устанавливает видимость контрола списка
        /// </summary>
        public bool GridVisible
        {
            set { _listsContainer.Visible = value; }
        }

        /// <summary>
        /// показывает список БС-ов
        /// </summary>
        public void ShowListsView()
        {
            GridVisible = true;
            GetSettingsListDetailView().Visible = false;

            RibbonFunctionalButtonsVisible = false;
            RibbonSaveButtonVisible = false;
            RibbonListButtonsVisible = true;

            SetWindowTitle("Настройка списков");
        }

        public bool ExcludeListAreaVisible { get; set; }

        #endregion

    }
}
