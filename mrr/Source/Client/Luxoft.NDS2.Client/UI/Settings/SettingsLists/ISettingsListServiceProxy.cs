﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    /// <summary>
    /// Интерфейс реализующий работу с настройками списков
    /// </summary>
    public interface ISettingsListServiceProxy
    {
        /// <summary>
        /// Загружает список БС
        /// </summary>
        /// <param name="lists">загруженные БС</param>
        /// <returns></returns>
        bool TryLoadWhiteLists(QueryConditions qc,out List<SystemSettingsList> lists);

        /// <summary>
        /// Загружает список НМ
        /// </summary>
        /// <param name="list">список НМ</param>
        /// <returns></returns>
        bool TryLoadTaxMonitorList(out SystemSettingsList list);

        /// <summary>
        /// Загружает список ИВ
        /// </summary>
        /// <param name="list">список ИВ</param>
        /// <returns></returns>
        bool TryLoadExcludeList(out SystemSettingsList list);

        /// <summary>
        /// Загружает список INN
        /// </summary>
        /// <param name="id">идентификатор списка</param>
        /// <param name="innList">список загруженных INN</param>
        /// <returns></returns>
        bool TryLoadInnList(long id, out string[] innList);

        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        bool TryActivate(long id);

        /// <summary>
        /// Деактивирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        bool TryDeactivate(long id);

        /// <summary>
        /// Возвращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetUserDisplayName();
    }
}
