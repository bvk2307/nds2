﻿using System;
using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public interface ISettingsListDetailView
    {
        event EventHandler Copying;
        event EventHandler Cutting;
        event EventHandler Pasting;
        event EventHandler ReadOnlyPropertyChanged;
        event EventHandler InnListValueChanged;
        event EventHandler ListNameValueChanged;
        event EventHandler ActivatedChanged;


        /// <summary>
        ///     Устанавливает или возвращает список ИНН
        /// </summary>
        string InnList { get; set; }

        /// <summary>
        ///     Устанавливает или возвращает название списка
        /// </summary>
        string ListName { get; set; }

        /// <summary>
        ///     Устанавливает или возвращает тип списка
        /// </summary>
        SettingsListType ListType { get; set; }

        /// <summary>
        /// Возвращает активирован или нет БС
        /// </summary>
        /// <returns></returns>
        bool IsActive { get; set; }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей контролы Наименования листа
        /// </summary>
        bool NamePanelVisible { set; }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей текст об активации списка
        /// </summary>
        bool ActiveLabelPanelVisible { set; }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей чекбокс для активации списка
        /// </summary>
        bool ActiveCheckBoxPanelVisible { set; }

        /// <summary>
        ///     Устанавливает ReadOnly свойство всем эдиторам
        /// </summary>
        bool ReadOnly { set; }

        /// <summary>
        /// Вставка
        /// </summary>
        /// <returns></returns>
        void Paste();

        /// <summary>
        /// Копирование
        /// </summary>
        /// <returns></returns>
        void Copy();

        /// <summary>
        /// Вырезка
        /// </summary>
        /// <returns></returns>
        void Cut();

        /// <summary>
        /// Свойство, отвечающее за наличе пункта меню "Вырезать"
        /// </summary>
        bool CutAvailable { set; }

        /// <summary>
        /// Свойство, отвечающее за наличе пункта меню "Вставить"
        /// </summary>
        bool PasteAvailable { set; }

        /// <summary>
        /// Свойство, отвечающее за доступность пункта меню "Копировать"
        /// </summary>
        bool CopyApplicable { set; }

        /// <summary>
        /// Свойство, отвечающее за доступность пункта меню "Вырезать"
        /// </summary>
        bool CutApplicable { set; }

        /// <summary>
        /// Возвращает редактируема ли форма
        /// </summary>
        bool IsReadonly { get; }

        /// <summary>
        /// Устанавливает доступность пунктам контекстного меню
        /// </summary>
        void EnableMenuItems();

        /// <summary>
        /// Возвращает true, если ИНН список пустой
        /// </summary>
        bool IsInnEmpty { get; }

    }
}