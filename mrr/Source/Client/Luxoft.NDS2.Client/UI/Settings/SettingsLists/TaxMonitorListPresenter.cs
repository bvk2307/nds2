﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Common.Contracts.Helpers;


namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class TaxMonitorListPresenter : ListPresenter
    {
        private readonly ITaxMonitorListServiceProxy _proxy;
        private readonly IWhiteListsView _view;


        public TaxMonitorListPresenter(IWhiteListsView view, ITaxMonitorListServiceProxy proxy, ListBasePresenter basePresenter) :
            base(basePresenter)
        {
            _proxy = proxy;
            _view = view;
        }

        private void InitModel(string[] innList)
        {
            _basePresenter.Model.SetInnList(innList);
            _basePresenter.Model.ListType = SettingsListType.TaxMonitorList;
        }


        /// <summary>
        /// Сохраняет спиок НМ
        /// </summary>
        /// <param name="userDisplayName"></param>
        /// <returns></returns>
        public override bool Save(string userDisplayName)
        {
            var data = _basePresenter.BeforeSave(userDisplayName);
            if (data == null)
                return false;

            return _proxy.TrySave(data);
        }

        /// <summary>
        /// Открывает ЭФ списка НМ
        /// </summary>
        /// <param name="innList"></param>
        /// <param name="editable"></param>
        public override void Show(string[] innList, bool editable = true)
        {
            InitModel(innList);
            _view.ShowWhiteListView();
            _view.ShowTaxMonitorListView();
        }
    }
}