﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public abstract class ListPresenter
    {
        protected ListBasePresenter _basePresenter;

        protected ListPresenter(ListBasePresenter basePresenter)
        {
            _basePresenter = basePresenter;
            _basePresenter.InputChanged += (sender, e) => { if (InputChanged != null) InputChanged(sender, e); };
        }
        /// <summary>
        /// Сохраняет список
        /// </summary>
        public abstract bool Save(string userDisplayName);

        /// <summary>
        /// Валидирует модель
        /// </summary>
        public bool Validate()
        {
            return _basePresenter.Validate();
        }

        /// <summary>
        /// Открывает экранную форму модели
        /// </summary>
        /// <param name="innList"></param>
        /// <param name="editable"></param>
        public abstract void Show(string[] innList, bool editable = true);

        public EventHandler<AvailabilityEventArgs> InputChanged;

    }
}