﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class WhiteListPresenter : ListPresenter
    {
        private readonly IWhiteListServiceProxy _proxy;
        private readonly IWhiteListsView _view;

        public WhiteListPresenter(IWhiteListsView view, IWhiteListServiceProxy proxy, ListBasePresenter basePresenter) :
            base(basePresenter)
        {
            _view = view;
            _proxy = proxy;
        }

        /// <summary>
        /// Инициализирует модель
        /// </summary>
        /// <param name="innList"></param>
        /// <param name="editable"></param>
        private void InitModel(string[] innList, bool editable)
        {
            var item = _view.GetGrid().SelectedItem != null
                ? ((SettingsListViewModel) _view.GetGrid().SelectedItem).GetData()
                : null;
            _basePresenter.Model.SetInnList(innList);
            _basePresenter.Model.ListType = SettingsListType.WhiteList;

            if (editable)
            {
                _basePresenter.Model.ListName = string.Empty;
                _basePresenter.Model.IsActive = false;

            }
            else
            {
                if (item != null)
                {
                    _basePresenter.Model.ListName = item.Name;
                    _basePresenter.Model.IsActive = item.IsActive;
                }
            }
        }

        /// <summary>
        /// Сохраняет список
        /// </summary>
        public override bool Save(string userDisplayName)
        {
            var data = _basePresenter.BeforeSave(userDisplayName);
            if (data == null)
                return false;

            return _proxy.TrySave(data);
        }

        /// <summary>
        /// Открывает экранную форму модели
        /// </summary>
        /// <param name="innList"></param>
        /// <param name="editable"></param>
        public override void Show(string[] innList, bool editable = true)
        {
            InitModel(innList, editable);
            if (editable)
                _view.ShowWhiteListEditableView();
            else _view.ShowWhiteListView();
        }
    }
}