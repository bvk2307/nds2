﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class ListBasePresenter
    {
        public readonly SettingsListDetailViewModel Model;
        private readonly ISettingsListDetailView _view;

        public ListBasePresenter(ISettingsListDetailView view, SettingsListDetailViewModel model)
        {
            _view = view;
            Model = model;
            Model.PropertyChanged += new PropertyChangedEventHandler(ModelPropertyChanged);
            _view.InnListValueChanged += InnListValueChanged;
            _view.ListNameValueChanged += ListNameValueChanged;
            _view.ActivatedChanged += ActivatedChanged;
            _view.Copying += CopyInnList;
            _view.Cutting += CutInnList;
            _view.Pasting += PasteInnList;
            _view.ReadOnlyPropertyChanged += ReadOnlyPropertyChanged;

        }

        private void ReadOnlyPropertyChanged(object sender, EventArgs eventArgs)
        {
            _view.CutAvailable = !_view.IsReadonly;
            _view.PasteAvailable = !_view.IsReadonly;
            _view.EnableMenuItems();
        }

        /// <summary>
        /// Вставка в контрол списка ИНН
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void PasteInnList(object sender, EventArgs eventArgs)
        {
            _view.Paste();

        }

        /// <summary>
        /// Вырезка из контрола ИНН список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void CutInnList(object sender, EventArgs eventArgs)
        {
            _view.Cut();
        }

        /// <summary>
        /// Копирование из контрола ИНН список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void CopyInnList(object sender, EventArgs eventArgs)
        {
            _view.Copy();
        }

        /// <summary>
        /// Обработка события от View. Изменение активирования списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ActivatedChanged(object sender, EventArgs eventArgs)
        {
            if (Model.IsActive != _view.IsActive)
                Model.IsActive = _view.IsActive;
        }

        public EventHandler<AvailabilityEventArgs> InputChanged;
        /// <summary>
        /// Обработка события от View. Изменение Названия списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ListNameValueChanged(object sender, EventArgs eventArgs)
        {
            if (Model.ListName != _view.ListName)
                Model.ListName = _view.ListName;

            if (InputChanged != null && Model.ListType == SettingsListType.WhiteList)
                InputChanged(this, new AvailabilityEventArgs()
                {
                    Available = !string.IsNullOrEmpty(Model.ListName.Trim())
                    && !string.IsNullOrEmpty(Model.InnList.Trim())
                });
        }

        /// <summary>
        /// Обработка события от View. Изменение ИНН списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void InnListValueChanged(object sender, EventArgs eventArgs)
        {
            if (Model.InnList != _view.InnList)
                Model.InnList = _view.InnList;
            if (InputChanged != null && Model.ListType == SettingsListType.WhiteList)
                InputChanged(this, new AvailabilityEventArgs()
                {
                    Available = !string.IsNullOrEmpty(Model.ListName.Trim())
                    && !string.IsNullOrEmpty(Model.InnList.Trim())
                });
        }

        /// <summary>
        /// Обраюотка события от Модели. Изменение свойств модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.InnList)
                && Model.InnList != _view.InnList)
                _view.InnList = Model.InnList;

            if (e.PropertyName == TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.ListName)
                && Model.ListName != _view.ListName)
                _view.ListName = Model.ListName;

            if (e.PropertyName == TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.IsActive)
               && Model.IsActive != _view.IsActive)
                _view.IsActive = Model.IsActive;

            if (e.PropertyName == TypeHelper<SettingsListDetailViewModel>.GetMemberName(x => x.ListType)
              && Model.ListType != _view.ListType)
                _view.ListType = Model.ListType;

            _view.EnableMenuItems();

        }

        public bool Validate()
        {
            if (!Model.IsValid())
            {
                DialogHelper.Error(ResourceManagerNDS2.SettingsListSaveError);
                return false;
            }
            return true;
        }

        public SystemSettingsList BeforeSave(string userDisplayName)
        {
            char[] delimeters = { Constants.InnDelimeterComma, Constants.InnDelimeterWhiteSpace };
            var resInnList = Model.InnList.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);
            resInnList = resInnList.Select(x => x.Trim()).ToArray();
            return new SystemSettingsList()
            {
                CreatedBy = userDisplayName,
                ListType = (int)Model.ListType,
                Name = Model.ListName,
                IsActive = Model.IsActive,
                InnList = resInnList
            };

        }
    }
}