﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class WhiteListsPresenter :
        GridViewPresenter<SettingsListViewModel, SystemSettingsList, List<SystemSettingsList>>
    {
        private readonly ISettingsListServiceProxy _proxy;

        private readonly IWhiteListsView _view;
        private readonly Dictionary<SettingsListType, ListPresenter> _subPresenters;

        # region .ctor

        public WhiteListsPresenter(IWhiteListsView view,
            WhiteListsViewModel model,
            ISettingsListServiceProxy proxy,
            IWhiteListServiceProxy whiteListServiceProxy,
            ITaxMonitorListServiceProxy taxMonitorListServiceProxy,
            IExcludeListServiceProxy excludeListServiceProxy,
            IServerGridDataProvider<List<SystemSettingsList>> dataProvider) :
            base(dataProvider, model, view.GetGrid())
        {
            _view = view;
            _proxy = proxy;
            _subPresenters = new Dictionary<SettingsListType, ListPresenter>();
            var basePresenter = new ListBasePresenter(_view.GetSettingsListDetailView(), new SettingsListDetailViewModel());
            _subPresenters.Add(SettingsListType.WhiteList, new WhiteListPresenter(view, whiteListServiceProxy, basePresenter));
            _subPresenters.Add(SettingsListType.ExcludeList, new ExcludeListPresenter(view, excludeListServiceProxy, basePresenter));
            _subPresenters.Add(SettingsListType.TaxMonitorList, new TaxMonitorListPresenter(view, taxMonitorListServiceProxy, basePresenter));
            _subPresenters[SettingsListType.WhiteList].InputChanged += ToggleAvailabilitySaveButton;
            _view.Opened += ViewOpened;
        }

        #endregion

        private void ToggleAvailabilitySaveButton(object sender, AvailabilityEventArgs e)
        {
            _view.RibbonSaveButtonVisible = e.Available;
        }

        # region Инициализация представления

        /// <summary>
        ///     инициализирует поля View из Модели и подписываемся на события View
        /// </summary>
        private void InitView()
        {
            _view.GetGrid().ItemViewing += OpenDetailsView;
            _view.GetGrid().ItemActivate += ItemActivate;
            _view.GetGrid().ItemDeactivate += ItemDeactivate;
            _view.GetGrid().ItemSelected += ItemSelected;
            _view.SavingList += SaveList;
            _view.RefreshWhiteListsClicked += ViewRefreshed;
            _view.CreateWhiteListClicked += CreateWhiteListClicked;
            _view.ExcludeListClicked += ExcludeListDetailsClicked;
            _view.TaxMonitorListClicked += TaxMonitorListDetailsClicked;
            _view.MoveBack += MoveBack;
        }

        #region Соытия view

        /// <summary>
        /// Событие на нажатие кнопки "Назад"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void MoveBack(object sender, EventArgs eventArgs)
        {
            if (_view.GetGrid().Visible)
                return;
            if (_view.GetSettingsListDetailView().Visible)
            {
                _view.ShowListsView();
            }
        }

        /// <summary>
        /// Открытие View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOpened(object sender, EventArgs e)
        {
            BeginLoad();
            InitView();
        }

        /// <summary>
        /// Обновление view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewRefreshed(object sender, EventArgs e)
        {
            BeginLoad();
        }

        /// <summary>
        /// Событие пользовательского селекта на строке в гриде
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void ItemSelected(object sender, EventArgs args)
        {
            var selectedRow = (SettingsListViewModel)_view.GetGrid().SelectedItem;
            if (selectedRow != null)
            {
                _view.GetGrid().AllowActivate = !selectedRow.GetData().IsActive;
                _view.GetGrid().AllowDeactivate = selectedRow.GetData().IsActive;
            }
        }

        #endregion

        #endregion

        #region Активация БС


        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ItemActivate(object sender, EventArgs eventArgs)
        {
            var selectedRow = (SettingsListViewModel)_view.GetGrid().SelectedItem;
            if (selectedRow != null)
                if (_proxy.TryActivate(selectedRow.Id))
                {
                    var dataSource = (BindingList<SettingsListViewModel>)_view.GetGrid().GetDataSource();
                    ChangeActivation(dataSource, selectedRow.Id);
                }
        }

        /// <summary>
        /// Деактивирует БС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ItemDeactivate(object sender, EventArgs eventArgs)
        {
            var selectedRow = (SettingsListViewModel)_view.GetGrid().SelectedItem;
            if (selectedRow != null)
                if (_proxy.TryDeactivate(selectedRow.Id))
                {
                    var dataSource = (BindingList<SettingsListViewModel>)_view.GetGrid().GetDataSource();
                    ChangeActivation(dataSource, selectedRow.Id);
                }
        }

        private void ChangeActivation(BindingList<SettingsListViewModel> listItems, long id)
        {
            foreach (var item in listItems)
            {
                if (item.Id == id)
                {
                    var listData = item.GetData();
                    listData.IsActive = !listData.IsActive;
                    item.SetData(listData);
                    break;
                }
            }
            _view.GetGrid().Refresh();
        }


        #endregion

        #region Открытие ЭФ


        /// <summary>
        /// Открывает ЭФ детализации БС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void OpenDetailsView(object sender, EventArgs eventArgs)
        {
            var item = ((SettingsListViewModel)_view.GetGrid().SelectedItem).GetData();
            string[] innList;
            if (_proxy.TryLoadInnList(item.Id, out innList))
            {
              _subPresenters[SettingsListType.WhiteList].Show(innList, false);
            }
            else
            {
                DialogHelper.Inform("Ошибка загрузки данных");
            }
        }


        /// <summary>
        /// Открывает ЭФ создания БС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void CreateWhiteListClicked(object sender, EventArgs eventArgs)
        {
            _subPresenters[SettingsListType.WhiteList].Show(null);
            _view.RibbonSaveButtonVisible = false;
        }

        /// <summary>
        /// Открытие ЭФ список ИВ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ExcludeListDetailsClicked(object sender, EventArgs eventArgs)
        {
            SystemSettingsList list = null;
            string[] innList = null;
            if (_proxy.TryLoadExcludeList(out list))
                _proxy.TryLoadInnList(list.Id, out innList);
               
            _subPresenters[SettingsListType.ExcludeList].Show(innList);
        }

        /// <summary>
        /// Открытие ЭФ НМ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void TaxMonitorListDetailsClicked(object sender, EventArgs eventArgs)
        {
            SystemSettingsList list = null;
            string[] innList = null;
            if (_proxy.TryLoadTaxMonitorList(out list))
                _proxy.TryLoadInnList(list.Id, out innList);

            _subPresenters[SettingsListType.TaxMonitorList].Show(innList);
        }

        #endregion

        #region Загрузка/Сохранение данных
       
        /// <summary>
        /// Сохраняет списков
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void SaveList(object sender, EventArgs eventArgs)
        {
            var listType = _view.GetSettingsListDetailView().ListType;
            if (_subPresenters[listType].Validate())
            {
                if (_subPresenters[listType].Save(_proxy.GetUserDisplayName()))
                {
                    DialogHelper.Inform("Список сохранен");
                    if (listType == SettingsListType.WhiteList)
                        ViewRefreshed(sender, eventArgs);
                }
                else DialogHelper.Error("Ошибка при сохранении списка");

                _view.ShowListsView();
            }
        }
       
        #endregion
    }
}