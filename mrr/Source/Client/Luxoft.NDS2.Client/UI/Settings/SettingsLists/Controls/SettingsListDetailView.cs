﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls
{
    public partial class SettingsListDetailView : UserControl, ISettingsListDetailView
    {
        public SettingsListDetailView()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Тип списка
        /// </summary>
        public SettingsListType ListType { get; set; }

        public event EventHandler Copying;
        public event EventHandler Cutting;
        public event EventHandler Pasting;
        public event EventHandler ReadOnlyPropertyChanged;
        public event EventHandler InnListValueChanged;
        public event EventHandler ListNameValueChanged;
        public event EventHandler ActivatedChanged;

        /// <summary>
        ///     Устанавливает или возвращает список ИНН
        /// </summary>
        public string InnList
        {
            get { return _innEditor.Text; }
            set { _innEditor.Text = value; }
        }

        /// <summary>
        ///     Устанавливает или возвращает название списка
        /// </summary>
        public string ListName
        {
            get { return _nameEditor.Text; }
            set { _nameEditor.Text = value; }
        }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей контролы Наименования листа
        /// </summary>
        public bool NamePanelVisible
        {
            set { _namePanelLayout.Visible = value; }
        }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей текст об активации списка
        /// </summary>
        public bool ActiveLabelPanelVisible
        {
            set
            {
                _activeLabelPanelLayout.Visible = value;
            }
        }

        /// <summary>
        ///     Устанавливает видимость панели, содержащей чекбокс для активации списка
        /// </summary>
        public bool ActiveCheckBoxPanelVisible
        {
            set { _activeCheckBoxPanelLayout.Visible = value; }
        }

        /// <summary>
        ///     Устанавливает ReadOnly свойство всем эдиторам
        /// </summary>
        public bool ReadOnly
        {
            set
            {
                _nameEditor.ReadOnly = value;
                _innEditor.ReadOnly = value;
            }
        }

        /// <summary>
        /// Возвращает активирован или нет БС
        /// </summary>
        /// <returns></returns>
        public bool IsActive
        {
            get { return _activateCheckBox.Checked; }
            set
            {
                _activateCheckBox.Checked = value;
                _activeLabel.Text = value ? ResourceManagerNDS2.WhiteListActivated : ResourceManagerNDS2.WhiteListDeactivated;
            }
        }

        /// <summary>
        /// Устанавливает доступность пунктам контекстного меню
        /// </summary>
        public void EnableMenuItems()
        {
            CopyApplicable = !IsInnEmpty;
            CutApplicable = !IsInnEmpty;
        }

        /// <summary>
        /// Возвращает true, если ИНН список пустой
        /// </summary>
        public bool IsInnEmpty { get { return _innEditor.TextLength == 0; } }

        /// <summary>
        /// Возвращает редактируема ли форма
        /// </summary>
        public bool IsReadonly
        {
            get { return _innEditor.ReadOnly; }
        }

        /// <summary>
        /// Свойство, отвечающее за наличе пункта меню "Вырезать"
        /// </summary>
        public bool CutAvailable
        {
            set { _cutMenuItem.Visible = value; }
        }

        /// <summary>
        /// Свойство, отвечающее за наличе пункта меню "Вставить"
        /// </summary>
        public bool PasteAvailable
        {
            set { _pasteMenuItem.Visible = value; }
        }


        /// <summary>
        /// Свойство, отвечающее за доступность пункта меню "Копировать"
        /// </summary>
        public bool CopyApplicable
        {
            set { _copyMenuItem.Enabled = value; }
        }

        /// <summary>
        /// Свойство, отвечающее за доступность пункта меню "Вырезать"
        /// </summary>
        public bool CutApplicable
        {
            set { _cutMenuItem.Enabled = value; }
        }

        private void CutContextMenuItemClicked(object sender, System.EventArgs e)
        {
            if (Cutting != null)
                Cutting(sender, e);
        }

        private void CopyContextMenuItemClicked(object sender, System.EventArgs e)
        {
            if (Copying != null)
                Copying(sender, e);
        }

        private void PasteContextMenuItemClicked(object sender, System.EventArgs e)
        {
            if (Pasting != null)
                Pasting(sender, e);
        }

        /// <summary>
        /// Вставка
        /// </summary>
        /// <returns></returns>
        public void Paste()
        {
            _innEditor.Paste();
        }

        /// <summary>
        /// Вставка
        /// </summary>
        /// <returns></returns>
        public void Copy()
        {
            _innEditor.Copy();
        }

        /// <summary>
        /// Вырезка
        /// </summary>
        /// <returns></returns>
        public void Cut()
        {
            _innEditor.Cut();
        }

        private void InnEditorPropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
        {
            if((PropertyID)e.ChangeInfo.PropId == PropertyID.ReadOnly && ReadOnlyPropertyChanged != null)
            {
                ReadOnlyPropertyChanged(sender, e);
            }
        }

        private void InnListValueChange(object sender, EventArgs e)
        {
            if (InnListValueChanged != null)
            {
                InnListValueChanged(sender, e);
            }
        }

        private void ListNameChanged(object sender, EventArgs e)
        {
            if (ListNameValueChanged != null)
            {
                ListNameValueChanged(sender, e);
            }
        }

        private void ActivatedCheckedChanged(object sender, EventArgs e)
        {
            if (ActivatedChanged != null)
            {
                _activeLabel.Text = IsActive ? ResourceManagerNDS2.WhiteListActivated : ResourceManagerNDS2.WhiteListDeactivated;

                ActivatedChanged(sender, e);
            }
        }
    }
}