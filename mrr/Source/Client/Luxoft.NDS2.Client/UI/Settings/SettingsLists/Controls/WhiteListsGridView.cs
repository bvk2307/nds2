﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Base.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls
{
    public partial class WhiteListsGridView : UserControl, IExtendedGridView
    {
        public WhiteListsGridView()
        {
            InitializeComponent();
            GridEvents = new UltraGridEventsHandler(_grid);
            InitColumnsCollection();
            _grid.SetRowContextMenu(_gridMenu);
            _grid.SetBaseConfig();
        }

        /// <summary>
        /// Инициализирует коллекуию колонок грида
        /// </summary>
        private void InitColumnsCollection()
        {
            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(new[]
                        {
                            TypeHelper<SettingsListViewModel>.GetMemberName(x => x.Id)
                        }
                    ), new GridColumnCreator()
                    .WithFilterViewFor(
                        TypeHelper<SettingsListViewModel>.GetMemberName(x => x.IsActive),
                        (column) => new BooleanColumnFilterView(column)));

            BooleanColumnView.Apply(
               _grid.DisplayLayout.Bands[0].Columns[
                   TypeHelper<SettingsListViewModel>.GetMemberName(x => x.IsActive)]);
        }

        /// <summary>
        /// Устанавливает источник данных
        /// </summary>
        /// <param name="dataSource"></param>
        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        /// <summary>
        /// Возвращает источник данных
        /// </summary>
        /// <returns></returns>
        public object GetDataSource()
        {
            return _bindingSource.DataSource;
        }

        /// <summary>
        /// Возвращает выбранную строку
        /// </summary>
        public object SelectedItem
        {
            get
            {
                if (_grid.ActiveRow == null)
                {
                    return null;
                }

                return _grid.ActiveRow.ListObject;
            }
        }

        public event EventHandler ItemSelected;

        public event EventHandler ItemViewing;

        public event EventHandler ItemActivate;

        public event EventHandler ItemDeactivate;

        public IGridColumnsCollection Columns { get; private set; }
        public IGridEventsHandler GridEvents { get; private set; }

        /// <summary>
        /// Событие срабатывает на пользовательское действие клик по строке.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowSelected(object sender, EventArgs e)
        {
            if (ItemSelected != null)
                ItemSelected(sender, e);
        }

        /// <summary>
        /// Св-во активации
        /// </summary>
        public bool AllowActivate
        {
            set { _activateMinuItem.Visible = value; }
        }

        /// <summary>
        /// Св-во деактивации
        /// </summary>
        public bool AllowDeactivate
        {
            set { _deactivateMenuItem.Visible = value; }
        }
        /// <summary>
        /// Событие контекстного меню - открыть
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenDetails(object sender, EventArgs e)
        {
            if (ItemViewing != null)
            {
                ItemViewing(sender, e);
            }
        }

        /// <summary>
        /// Событие контекстного меню - Активирровать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Activate(object sender, EventArgs e)
        {
            if (ItemActivate != null)
            {
                ItemActivate(sender, e);
            }
        }

        /// <summary>
        /// Событие контекстного меню - Деактивировать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Deactivate(object sender, EventArgs e)
        {
            if (ItemDeactivate != null)
            {
                ItemDeactivate(sender, e);
            }
        }
    }
}
