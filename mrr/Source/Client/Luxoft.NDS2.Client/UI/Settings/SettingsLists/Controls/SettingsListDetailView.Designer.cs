﻿namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls
{
    partial class SettingsListDetailView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._namePanelLayout = new System.Windows.Forms.TableLayoutPanel();
            this._nameEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._activeLabelPanelLayout = new System.Windows.Forms.TableLayoutPanel();
            this._activeLabel = new Infragistics.Win.Misc.UltraLabel();
            this._activeCheckBoxPanelLayout = new System.Windows.Forms.TableLayoutPanel();
            this._activateCheckBox = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._copyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._pasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._innEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this._namePanelLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nameEditor)).BeginInit();
            this._activeLabelPanelLayout.SuspendLayout();
            this._activeCheckBoxPanelLayout.SuspendLayout();
            this._contextMenu.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._innEditor)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            ultraLabel1.Location = new System.Drawing.Point(3, 3);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(144, 31);
            ultraLabel1.TabIndex = 0;
            ultraLabel1.Text = "Наименование списка:";
            // 
            // _namePanelLayout
            // 
            this._namePanelLayout.ColumnCount = 2;
            this._namePanelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this._namePanelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._namePanelLayout.Controls.Add(ultraLabel1, 0, 0);
            this._namePanelLayout.Controls.Add(this._nameEditor, 1, 0);
            this._namePanelLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._namePanelLayout.Location = new System.Drawing.Point(3, 3);
            this._namePanelLayout.Name = "_namePanelLayout";
            this._namePanelLayout.RowCount = 2;
            this._namePanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this._namePanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this._namePanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this._namePanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this._namePanelLayout.Size = new System.Drawing.Size(812, 30);
            this._namePanelLayout.TabIndex = 0;
            // 
            // _nameEditor
            // 
            this._nameEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._nameEditor.Location = new System.Drawing.Point(153, 3);
            this._nameEditor.Name = "_nameEditor";
            this._nameEditor.ReadOnly = true;
            this._nameEditor.Size = new System.Drawing.Size(656, 21);
            this._nameEditor.TabIndex = 1;
            this._nameEditor.ValueChanged += new System.EventHandler(this.ListNameChanged);
            // 
            // _activeLabelPanelLayout
            // 
            this._activeLabelPanelLayout.ColumnCount = 1;
            this._activeLabelPanelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._activeLabelPanelLayout.Controls.Add(this._activeLabel, 0, 0);
            this._activeLabelPanelLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this._activeLabelPanelLayout.ForeColor = System.Drawing.Color.Red;
            this._activeLabelPanelLayout.Location = new System.Drawing.Point(3, 39);
            this._activeLabelPanelLayout.Name = "_activeLabelPanelLayout";
            this._activeLabelPanelLayout.RowCount = 2;
            this._activeLabelPanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._activeLabelPanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._activeLabelPanelLayout.Size = new System.Drawing.Size(812, 30);
            this._activeLabelPanelLayout.TabIndex = 1;
            // 
            // _activeLabel
            // 
            this._activeLabelPanelLayout.SetColumnSpan(this._activeLabel, 2);
            this._activeLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this._activeLabel.Location = new System.Drawing.Point(3, 3);
            this._activeLabel.Name = "_activeLabel";
            this._activeLabel.Size = new System.Drawing.Size(241, 24);
            this._activeLabel.TabIndex = 3;
            this._activeLabel.Text = "Список активирован";
            // 
            // _activeCheckBoxPanelLayout
            // 
            this._activeCheckBoxPanelLayout.ColumnCount = 1;
            this._activeCheckBoxPanelLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._activeCheckBoxPanelLayout.Controls.Add(this._activateCheckBox, 0, 0);
            this._activeCheckBoxPanelLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._activeCheckBoxPanelLayout.Location = new System.Drawing.Point(3, 75);
            this._activeCheckBoxPanelLayout.Name = "_activeCheckBoxPanelLayout";
            this._activeCheckBoxPanelLayout.RowCount = 1;
            this._activeCheckBoxPanelLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._activeCheckBoxPanelLayout.Size = new System.Drawing.Size(812, 30);
            this._activeCheckBoxPanelLayout.TabIndex = 2;
            // 
            // _activateCheckBox
            // 
            this._activeCheckBoxPanelLayout.SetColumnSpan(this._activateCheckBox, 2);
            this._activateCheckBox.Location = new System.Drawing.Point(3, 3);
            this._activateCheckBox.Name = "_activateCheckBox";
            this._activateCheckBox.Size = new System.Drawing.Size(179, 14);
            this._activateCheckBox.TabIndex = 6;
            this._activateCheckBox.Text = "Активировать список";
            this._activateCheckBox.CheckedChanged += new System.EventHandler(this.ActivatedCheckedChanged);
            // 
            // _contextMenu
            // 
            this._contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cutMenuItem,
            this._copyMenuItem,
            this._pasteMenuItem});
            this._contextMenu.Name = "contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(140, 70);
            // 
            // _cutMenuItem
            // 
            this._cutMenuItem.Name = "_cutMenuItem";
            this._cutMenuItem.Size = new System.Drawing.Size(139, 22);
            this._cutMenuItem.Text = "Вырезать";
            this._cutMenuItem.Click += new System.EventHandler(this.CutContextMenuItemClicked);
            // 
            // _copyMenuItem
            // 
            this._copyMenuItem.Name = "_copyMenuItem";
            this._copyMenuItem.Size = new System.Drawing.Size(139, 22);
            this._copyMenuItem.Text = "Копировать";
            this._copyMenuItem.Click += new System.EventHandler(this.CopyContextMenuItemClicked);
            // 
            // _pasteMenuItem
            // 
            this._pasteMenuItem.Name = "_pasteMenuItem";
            this._pasteMenuItem.Size = new System.Drawing.Size(139, 22);
            this._pasteMenuItem.Text = "Вставить";
            this._pasteMenuItem.Click += new System.EventHandler(this.PasteContextMenuItemClicked);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this._activeCheckBoxPanelLayout, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._namePanelLayout, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._activeLabelPanelLayout, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._innEditor, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(818, 313);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // _innEditor
            // 
            this._innEditor.ContextMenuStrip = this._contextMenu;
            this._innEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._innEditor.Location = new System.Drawing.Point(3, 111);
            this._innEditor.Multiline = true;
            this._innEditor.Name = "_innEditor";
            this._innEditor.NullText = "Введите ИНН через запятую или пробел";
            appearance1.FontData.ItalicAsString = "True";
            this._innEditor.NullTextAppearance = appearance1;
            this._innEditor.ReadOnly = true;
            this._innEditor.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this._innEditor.Size = new System.Drawing.Size(812, 199);
            this._innEditor.TabIndex = 5;
            this._innEditor.ValueChanged += new System.EventHandler(this.InnListValueChange);
            this._innEditor.PropertyChanged += new Infragistics.Win.PropertyChangedEventHandler(this.InnEditorPropertyChanged);
            // 
            // SettingsListDetailView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SettingsListDetailView";
            this.Size = new System.Drawing.Size(818, 313);
            this._namePanelLayout.ResumeLayout(false);
            this._namePanelLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nameEditor)).EndInit();
            this._activeLabelPanelLayout.ResumeLayout(false);
            this._activeCheckBoxPanelLayout.ResumeLayout(false);
            this._contextMenu.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._innEditor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _namePanelLayout;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _nameEditor;
        private System.Windows.Forms.TableLayoutPanel _activeLabelPanelLayout;
        private Infragistics.Win.Misc.UltraLabel _activeLabel;
        private System.Windows.Forms.TableLayoutPanel _activeCheckBoxPanelLayout;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _activateCheckBox;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.ToolStripMenuItem _cutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _copyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _pasteMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _innEditor;
    }
}
