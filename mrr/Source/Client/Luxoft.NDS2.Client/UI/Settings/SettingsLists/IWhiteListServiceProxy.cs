﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    /// <summary>
    /// Интерфейс реализующий работу с БС
    /// </summary>
    public interface IWhiteListServiceProxy
    {

        /// <summary>
        /// Создает БС
        /// </summary>
        /// <returns></returns>
        bool TrySave(SystemSettingsList list);
    }
}
