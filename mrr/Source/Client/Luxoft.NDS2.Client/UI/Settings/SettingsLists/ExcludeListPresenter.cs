﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Models;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Common.Contracts.Helpers;


namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    public class ExcludeListPresenter : ListPresenter
    {
        private readonly IExcludeListServiceProxy _proxy;
        private readonly IWhiteListsView _view;

        public ExcludeListPresenter(IWhiteListsView view, IExcludeListServiceProxy proxy, ListBasePresenter basePresenter) :
            base(basePresenter)
        {
            _view = view;
            _proxy = proxy;
        }

        private void InitModel(string[] innList)
        {
            _basePresenter.Model.SetInnList(innList);
            _basePresenter.Model.ListType = SettingsListType.ExcludeList;
        }

        /// <summary>
        /// Сохраняет спиок НМ
        /// </summary>
        public override bool Save(string userDisplayName)
        {
            var data = _basePresenter.BeforeSave(userDisplayName);
            if (data == null)
                return false;

            return _proxy.TrySave(data);
        }

        /// <summary>
        /// Открывает экранную форму модели
        /// </summary>
        /// <param name="innList"></param>
        /// <param name="editable"></param>
        public override void Show(string[] innList, bool editable = true)
        {
            InitModel(innList);
            _view.ShowWhiteListView();
            _view.ShowExcludeListView();
        }
    }
}