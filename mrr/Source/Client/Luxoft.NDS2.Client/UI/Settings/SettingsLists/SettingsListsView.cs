﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Settings.SettingsLists.Controls;
using Microsoft.Practices.ObjectBuilder;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;

namespace Luxoft.NDS2.Client.UI.Settings.SettingsLists
{
    /// <summary>
    /// Этот класс реализует представление экранной формы "Настройка списков"
    /// </summary>
    public partial class SettingsListsView : BaseView, ISettingsListsView
    {

        [CreateNew]
        public ListsPresenterCreator ListPresenterCreator
        {
            set
            {
                var creator = value;
                WorkItem = value.WorkItem;
                creator.Create(this);
            }
        }

        public SettingsListsView()
        {
            InitializeComponent();
            InitListsComponent();
        }

        private readonly SettingsListsGridView _listsGrid = new SettingsListsGridView();
        private void InitListsComponent()
        {
            _listsContainer
                .WithGrid(_listsGrid)
                .WithGridResetter(new GridColumnsVisibilityResetter(_listsGrid))
                .WithGridResetter(new GridColumnSortResetter(_listsGrid))
                .WithGridResetter(new GridColumnFilterResetter(_listsGrid));
        }

        public event EventHandler Opened;

        private void SettingsListsViewLoad(object sender, EventArgs e)
        {
            if (Opened != null)
            {
                Opened(sender, e);
            }
        }

        public SettingsListsGridView GetGrid()
        {
            return _listsGrid;
        }
    }
}
