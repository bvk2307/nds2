﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ReportsList
{
    public class Presenter : BasePresenter<View>
    {
        public Presenter()
        {
        }

        public Presenter(PresentationContext presentationContext, WorkItem wi, View view)
            : base(presentationContext, wi, view)
        {
        }

        private List<ReportDefinition> _reportDefinitions;

        private void CreateReportDefinitions()
        {
            if (null != _reportDefinitions)
                return;

            _reportDefinitions = new List<ReportDefinition>();
            FillReportDefinitionsLight(_reportDefinitions);
        }

        private void FillReportDefinitionsLight(List<ReportDefinition> reportDefinitions)
        {
            reportDefinitions.Clear();

            //reportDefinitions.Add(new ReportDefinition { Key = ReportNames.CheckControlRatio, Caption = ResourceManagerNDS2.CheckControlRatio });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.ActsAndDecisions, Caption = ResourceManagerNDS2.ActsAndDecisions });

        }

        private void FillReportDefinitionsFull(List<ReportDefinition> reportDefinitions)
        {
            reportDefinitions.Clear();

            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.InspectorMonitoringWork, Caption = ResourceManagerNDS2.InspectorMonitoringWork });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.LoadingInspection, Caption = ResourceManagerNDS2.LoadingInspection });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.MonitorProcessDeclaration, Caption = ResourceManagerNDS2.MonitorProcessDeclaration });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.CheckControlRatio, Caption = ResourceManagerNDS2.CheckControlRatio });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.MatchingRule, Caption = ResourceManagerNDS2.MatchingRule });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.CheckLogicControl, Caption = ResourceManagerNDS2.CheckLogicControl });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.DeclarationStatistic, Caption = ResourceManagerNDS2.DeclarationStatistic });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.DynamicTechnoParameter, Caption = ResourceManagerNDS2.DynamicTechnoParameter });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.DynamicDeclarationStatistic, Caption = ResourceManagerNDS2.DynamicDeclarationStatistic });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.InfoResultsOfMatching, Caption = ResourceManagerNDS2.InfoResultsOfMatching });
            reportDefinitions.Add(new ReportDefinition { Key = ReportNames.ActsAndDecisions, Caption = ResourceManagerNDS2.ActsAndDecisions });
        }

        public List<ReportDefinition> GetReportDefinitions()
        {
            CreateReportDefinitions();
            return _reportDefinitions;
        }

        private string GetReportCaption(string reportKey)
        {
            var find = _reportDefinitions.FirstOrDefault(p => p.Key == reportKey);
            return (find != null) ? find.Caption : string.Empty;
        }

        public void OpenReportGeneralWindow(string reportKey)
        {
            var managementService = WorkItem.Services.Get<IWindowsManagerService>();

            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();

            var selectionWorkItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());

            var fc = new FeatureContextBase(featureId);
            var pc = new PresentationContext(viewId, null, fc);

            if (!managementService.Contains(viewId))
            {
                LogActivity(ActivityLogEntry.New(ActivityLogType.ReportCount));
                var sw = Stopwatch.StartNew();
                string title = GetReportCaption(reportKey);

                pc.WindowTitle = title;
                pc.WindowDescription = title;

                if (reportKey == ReportNames.ActsAndDecisions)
                {
                    var opener = new ActAndDecisionReportOpener(WorkItem);

                    try
                    {
                        opener.Open(
                            selectionWorkItem,
                            pc,
                            viewId,
                            () =>
                            {
                                sw.Stop();
                                LogActivity(ActivityLogEntry.New(ActivityLogType.ReportTime, sw.ElapsedMilliseconds));
                            });
                    }
                    catch (Exception error)
                    {
                        ClientLogger.LogError(error, "ActAndDecisionReportOpen");
                        Notifier.ShowError("Ошибка загрузки данных");
                    }

                    return;
                }

                var dictionaryEditorView = managementService.AddNewSmartPart<ReportCard.Base.View>(selectionWorkItem, viewId.ToString(), pc, selectionWorkItem, reportKey);
                dictionaryEditorView.AfterLoad += () =>
                {
                    sw.Stop();
                    LogActivity(ActivityLogEntry.New(ActivityLogType.ReportTime, sw.ElapsedMilliseconds));
                };
                managementService.Show(pc, selectionWorkItem, dictionaryEditorView);
            }

        }

    }
}
