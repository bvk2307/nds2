﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportsList
{
    public class ReportNames
    {
        public const string MonitorProcessDeclaration = "ReportMonitoringProcessingDeclaration";
        public const string InspectorMonitoringWork = "ReportInspectorMonitoringWork";
        public const string LoadingInspection = "ReportLoadingInspection";
        public const string CheckControlRatio = "ReportCheckControlRatio";
        public const string MatchingRule = "MatchingRule";
        public const string CheckLogicControl = "CheckLogicControl";
        public const string DeclarationStatistic = "DeclarationStatistic";
        public const string DynamicTechnoParameter = "DynamicTechnoParameter";
        public const string DynamicDeclarationStatistic = "DynamicDeclarationStatistic";
        public const string InfoResultsOfMatching = "InfoResultsOfMatching";
        public const string ActsAndDecisions = "ReportActsAndDecisions";
    }
}
