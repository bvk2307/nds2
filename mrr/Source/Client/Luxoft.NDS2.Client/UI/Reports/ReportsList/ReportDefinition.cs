﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportsList
{
    public class ReportDefinition
    {
        public string Key { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }
}
