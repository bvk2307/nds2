﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Microsoft.Practices.ObjectBuilder;
using Infragistics.Win.UltraWinGrid;
using Microsoft.Practices.CompositeUI;
using Infragistics.Win;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.ControlContracts;

namespace Luxoft.NDS2.Client.UI.Reports.ReportsList
{
    public partial class View : BaseView
    {
        private PresentationContext presentationContext;
        private Presenter _presenter;
        private System.Data.DataTable reportList = null;
        public string ReportKey { get; set; }

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.View = this;
                _presenter.PresentationContext = presentationContext;
                this.WorkItem = _presenter.WorkItem;
                InitData();
            }
        }

        public View(PresentationContext context)
        {
            presentationContext = context;
            InitializeComponent();

            this.gridReportList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.gridReportList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.gridReportList.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.gridReportList.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            CreateDataTableReportList();
        }

        private void CreateDataTableReportList()
        {
            if (null == this.reportList)
            {
                this.reportList = new System.Data.DataTable("ReportList");

                DataColumn columnName = this.reportList.Columns.Add("Name", typeof(string));
                columnName.ReadOnly = true;
                columnName.Caption = "Название";
                DataColumn columnDesc = this.reportList.Columns.Add("Description", typeof(string));
                columnDesc.ReadOnly = true;
                columnDesc.Caption = "Описание";
                DataColumn columnId = this.reportList.Columns.Add("Key", typeof(string));
                columnId.ReadOnly = true;
                columnId.Caption = "Ключ";
            }
        }

        private void InitData()
        {
            foreach (ReportDefinition item in _presenter.GetReportDefinitions())
            {
                this.reportList.Rows.Add(new object[] { item.Caption, item.Description, item.Key });
            }
            this.gridReportList.DataSource = this.reportList;

            this.gridReportList.ActiveRow = this.gridReportList.ActiveRowScrollRegion.FirstRow;
        }

        private string GetCurrentReportKey()
        {
            return GetCurrentStringField("Key");
        }

        private string GetCurrentReportCaption()
        {
            return GetCurrentStringField("Name");
        }

        private string GetCurrentStringField(string namefield)
        {
            UltraGridRow currentRow = this.gridReportList.ActiveRow;
            string ret = String.Empty;
            if (currentRow != null)
            {
                ret = (string)currentRow.Cells[namefield].Value;
            }
            return ret;
        }

        private void SelectOneReport()
        {
            this.ReportKey = GetCurrentReportKey();
            _presenter.OpenReportGeneralWindow(this.ReportKey);
        }

        private void gridReportList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            SelectOneReport();
        }
    }
}
