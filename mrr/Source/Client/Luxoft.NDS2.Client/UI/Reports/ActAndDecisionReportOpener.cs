﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.CommonFunctions;
using Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions;
using Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Microsoft.Practices.CompositeUI;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports
{
    public class ActAndDecisionReportOpener
    {
        private readonly WorkItem _workItem;

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        public ActAndDecisionReportOpener(WorkItem workItem)
        {
            _workItem = workItem;
            _notifier = workItem.Services.Get<INotifier>(true);
            _logger = workItem.Services.Get<IClientLogger>(true);
        }

        public void Open(
            WorkItem newWorkItem, 
            PresentationContext presentationContext,
            Guid viewId,
            Action afterLoad)
        {
            # region Загрузка справочников

            Sono[] sonoList = null;
            Region[] regionsList = null;
            FederalDistrict[] districtsList = null;
            EffectiveTaxPeriod[] periods = null;
            AccessContext context = null;
            bool allDictionariesLoaded = false;

            var dictionaryDataService = _workItem.GetWcfServiceProxy<IDictionaryDataService>();
            var reportDataService = _workItem.GetWcfServiceProxy<IActDecisionReportService>();

            allDictionariesLoaded =
                dictionaryDataService.ExecuteNoHandling(
                    proxy => proxy.GetSonoDictionary(), 
                    out sonoList)
                && dictionaryDataService.ExecuteNoHandling(
                    proxy => proxy.GetRegionDictionary(), 
                    out regionsList)
                && dictionaryDataService.ExecuteNoHandling(
                    proxy => proxy.GetFederalDistrictDictionary(), 
                    out districtsList)
                && reportDataService.ExecuteNoHandling(
                    proxy => proxy.GetAvailableTaxPeriods(),
                    out periods)
                && reportDataService.ExecuteNoHandling(
                    proxy => proxy.GetAccesContext(),
                    out context);

            # endregion

            # region Проверка возможности открытия формы отчета

            if (!allDictionariesLoaded)
            {
                _notifier.ShowError("Ошибка загрузки данных");
                return;
            }

            if (!context.Operations.Any())
            {
                _notifier.ShowError("У Вас нет прав на совершение данной операции");
                return;
            }

            if (!periods.Any())
            {
                _notifier.ShowError("Отсутствуют данные для построения отчета");
                return;
            }

            # endregion

            var model = new ReportViewModel(periods, sonoList, regionsList, context);

            # region Инициализация коллекции фабрик обработчиков данных отчета

            var reportFactoryCollection = new ReportFactoryCollection();

            reportFactoryCollection.AddFactory(
                MrrOperations.ActAndDecision.ActDecisionReportByCountry, 
                new ReportByCountryFactory(reportDataService, _notifier, _logger));

            reportFactoryCollection.AddFactory(
                MrrOperations.ActAndDecision.ActDecisionReportByFederalDistrict,
                new ReportByDistrictFactory(reportDataService, _notifier, _logger, districtsList, regionsList));

            reportFactoryCollection.AddFactory(
                MrrOperations.ActAndDecision.ActDecisionReportByRegion,
                new ReportByRegionFactory(reportDataService, _notifier, _logger, sonoList));

            reportFactoryCollection.AddFactory(
                MrrOperations.ActAndDecision.ActDecisionReportBySono,
                new ReportBySonoFactory(reportDataService, _notifier, _logger, sonoList));

            # endregion

            var reportFinder = new ReportFinder(reportDataService, _notifier, _logger);

            var windowsService = _workItem.Services.Get<IWindowsManagerService>();
            var view = windowsService.AddNewSmartPart<ReportView>(newWorkItem, viewId.ToString(), presentationContext, newWorkItem);
            view.AfterLoad += () => afterLoad();

            var presenter = 
                new ReportPresenter(
                    view, 
                    model, 
                    reportFinder, 
                    reportFactoryCollection, 
                    _logger, 
                    new FileOpener());
            windowsService.Show(presentationContext, newWorkItem, view);
        }
    }
}
