﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportDataConvertor
    {
        IEnumerable<IReportRowItem> ToModel(KnpResultReportSummary[] dataFrom, KnpResultReportSummary[] dataTo);
    }
}
