﻿namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    partial class ReportGridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("District");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActQuantity");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActDiscrepancyNdsQuantity");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActDiscrepancyNdsAmount");
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActGapQuantity");
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActGapAmount");
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActDiscrepancyTotalQuantity");
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ActDiscrepancyTotalAmount");
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionQuantity");
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionDiscrepancyNdsQuantity");
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionDiscrepancyNdsAmount");
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionGapQuantity");
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionGapAmount");
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionDiscrepancyTotalQuantity");
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DecisionDiscrepancyTotalAmount");
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("ActGroup", 110532169);
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("ActNdsGroup", 110532309);
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("ActGaspGroup", 110532512);
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("ActTotalGroup", 110532731);
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("DecNdsGroup", 110532933);
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("DecGaspGroup", 110533074);
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("DecTotalGroup", 110533214);
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("DecisionGroup", 110533401);
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._dataExporter = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            this._grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridColumn16.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance36.TextHAlignAsString = "Center";
            appearance36.TextVAlignAsString = "Middle";
            ultraGridColumn16.Header.Appearance = appearance36;
            ultraGridColumn16.Header.Caption = "<Федеральный округ>";
            ultraGridColumn16.Header.VisiblePosition = 0;
            ultraGridColumn16.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn16.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn16.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(300, 0);
            ultraGridColumn16.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn16.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn16.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn17.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance37.TextHAlignAsString = "Center";
            appearance37.TextVAlignAsString = "Middle";
            ultraGridColumn17.Header.Appearance = appearance37;
            ultraGridColumn17.Header.Caption = "Количество актов";
            ultraGridColumn17.Header.VisiblePosition = 1;
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn17.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(250, 0);
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn17.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn18.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            ultraGridColumn18.Header.Appearance = appearance38;
            ultraGridColumn18.Header.Caption = "Количество";
            ultraGridColumn18.Header.VisiblePosition = 2;
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupKey = "ActNdsGroup";
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn18.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn19.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            ultraGridColumn19.Header.Appearance = appearance39;
            ultraGridColumn19.Header.Caption = "Сумма";
            ultraGridColumn19.Header.VisiblePosition = 3;
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupKey = "ActNdsGroup";
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn19.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn20.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance40.TextHAlignAsString = "Center";
            appearance40.TextVAlignAsString = "Middle";
            ultraGridColumn20.Header.Appearance = appearance40;
            ultraGridColumn20.Header.Caption = "Количество";
            ultraGridColumn20.Header.VisiblePosition = 4;
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupKey = "ActGaspGroup";
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn20.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn21.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance41.TextHAlignAsString = "Center";
            appearance41.TextVAlignAsString = "Middle";
            ultraGridColumn21.Header.Appearance = appearance41;
            ultraGridColumn21.Header.Caption = "Сумма";
            ultraGridColumn21.Header.VisiblePosition = 5;
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupKey = "ActGaspGroup";
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn21.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn22.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance42.TextHAlignAsString = "Center";
            appearance42.TextVAlignAsString = "Middle";
            ultraGridColumn22.Header.Appearance = appearance42;
            ultraGridColumn22.Header.Caption = "Количество";
            ultraGridColumn22.Header.VisiblePosition = 6;
            ultraGridColumn22.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn22.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupKey = "ActTotalGroup";
            ultraGridColumn22.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn22.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn22.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn23.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance43.TextHAlignAsString = "Center";
            appearance43.TextVAlignAsString = "Middle";
            ultraGridColumn23.Header.Appearance = appearance43;
            ultraGridColumn23.Header.Caption = "Сумма";
            ultraGridColumn23.Header.VisiblePosition = 7;
            ultraGridColumn23.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupKey = "ActTotalGroup";
            ultraGridColumn23.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn23.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn24.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance44.TextHAlignAsString = "Center";
            appearance44.TextVAlignAsString = "Middle";
            ultraGridColumn24.Header.Appearance = appearance44;
            ultraGridColumn24.Header.Caption = "Количество решений";
            ultraGridColumn24.Header.VisiblePosition = 8;
            ultraGridColumn24.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn24.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn24.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(250, 0);
            ultraGridColumn24.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.SpanY = 6;
            ultraGridColumn24.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn25.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance45.TextHAlignAsString = "Center";
            appearance45.TextVAlignAsString = "Middle";
            ultraGridColumn25.Header.Appearance = appearance45;
            ultraGridColumn25.Header.Caption = "Количество";
            ultraGridColumn25.Header.VisiblePosition = 9;
            ultraGridColumn25.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn25.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupKey = "DecNdsGroup";
            ultraGridColumn25.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn25.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn26.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            ultraGridColumn26.Header.Appearance = appearance46;
            ultraGridColumn26.Header.Caption = "Сумма";
            ultraGridColumn26.Header.VisiblePosition = 10;
            ultraGridColumn26.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupKey = "DecNdsGroup";
            ultraGridColumn26.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn26.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn27.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            ultraGridColumn27.Header.Appearance = appearance47;
            ultraGridColumn27.Header.Caption = "Количество";
            ultraGridColumn27.Header.VisiblePosition = 11;
            ultraGridColumn27.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn27.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn27.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn27.RowLayoutColumnInfo.ParentGroupKey = "DecGaspGroup";
            ultraGridColumn27.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn27.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn27.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn28.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            ultraGridColumn28.Header.Appearance = appearance48;
            ultraGridColumn28.Header.Caption = "Сумма";
            ultraGridColumn28.Header.VisiblePosition = 12;
            ultraGridColumn28.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn28.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn28.RowLayoutColumnInfo.ParentGroupKey = "DecGaspGroup";
            ultraGridColumn28.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn28.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn29.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance49.TextHAlignAsString = "Center";
            appearance49.TextVAlignAsString = "Middle";
            ultraGridColumn29.Header.Appearance = appearance49;
            ultraGridColumn29.Header.Caption = "Количество";
            ultraGridColumn29.Header.VisiblePosition = 13;
            ultraGridColumn29.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn29.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupKey = "DecTotalGroup";
            ultraGridColumn29.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn29.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn29.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn30.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            appearance50.TextHAlignAsString = "Center";
            appearance50.TextVAlignAsString = "Middle";
            ultraGridColumn30.Header.Appearance = appearance50;
            ultraGridColumn30.Header.Caption = "Сумма";
            ultraGridColumn30.Header.VisiblePosition = 14;
            ultraGridColumn30.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn30.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn30.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn30.RowLayoutColumnInfo.ParentGroupKey = "DecTotalGroup";
            ultraGridColumn30.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn30.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn30.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn22,
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30});
            appearance51.TextHAlignAsString = "Center";
            appearance51.TextVAlignAsString = "Middle";
            ultraGridGroup1.Header.Appearance = appearance51;
            ultraGridGroup1.Header.Caption = "Расхождения, включенные в акт";
            ultraGridGroup1.Key = "ActGroup";
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 12;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 6;
            appearance52.TextHAlignAsString = "Center";
            appearance52.TextVAlignAsString = "Middle";
            ultraGridGroup2.Header.Appearance = appearance52;
            ultraGridGroup2.Header.Caption = "НДС";
            ultraGridGroup2.Key = "ActNdsGroup";
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.ParentGroupIndex = 0;
            ultraGridGroup2.RowLayoutGroupInfo.ParentGroupKey = "ActGroup";
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 4;
            appearance53.TextHAlignAsString = "Center";
            appearance53.TextVAlignAsString = "Middle";
            ultraGridGroup3.Header.Appearance = appearance53;
            ultraGridGroup3.Header.Caption = "Разрыв";
            ultraGridGroup3.Key = "ActGaspGroup";
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupIndex = 0;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupKey = "ActGroup";
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 4;
            appearance54.TextHAlignAsString = "Center";
            appearance54.TextVAlignAsString = "Middle";
            ultraGridGroup4.Header.Appearance = appearance54;
            ultraGridGroup4.Header.Caption = "всего";
            ultraGridGroup4.Key = "ActTotalGroup";
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupIndex = 0;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupKey = "ActGroup";
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 4;
            appearance55.TextHAlignAsString = "Center";
            appearance55.TextVAlignAsString = "Middle";
            ultraGridGroup5.Header.Appearance = appearance55;
            ultraGridGroup5.Header.Caption = "НДС";
            ultraGridGroup5.Key = "DecNdsGroup";
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupIndex = 7;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupKey = "DecisionGroup";
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 4;
            appearance56.TextHAlignAsString = "Center";
            appearance56.TextVAlignAsString = "Middle";
            ultraGridGroup6.Header.Appearance = appearance56;
            ultraGridGroup6.Header.Caption = "Разрыв";
            ultraGridGroup6.Key = "DecGaspGroup";
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupIndex = 7;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupKey = "DecisionGroup";
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 4;
            appearance57.TextHAlignAsString = "Center";
            appearance57.TextVAlignAsString = "Middle";
            ultraGridGroup7.Header.Appearance = appearance57;
            ultraGridGroup7.Header.Caption = "всего";
            ultraGridGroup7.Key = "DecTotalGroup";
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupIndex = 7;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupKey = "DecisionGroup";
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 4;
            appearance58.TextHAlignAsString = "Center";
            appearance58.TextVAlignAsString = "Middle";
            ultraGridGroup8.Header.Appearance = appearance58;
            ultraGridGroup8.Header.Caption = "Расхождения, включенные в решение";
            ultraGridGroup8.Key = "DecisionGroup";
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 12;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 6;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.ColumnChooserEnabled = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this._grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this._grid.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this._grid.DisplayLayout.MaxColScrollRegions = 1;
            this._grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this._grid.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this._grid.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.None;
            this._grid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._grid.DisplayLayout.Override.CellAppearance = appearance8;
            this._grid.DisplayLayout.Override.CellPadding = 0;
            appearance6.BackColor = System.Drawing.SystemColors.Control;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this._grid.DisplayLayout.Override.GroupByRowAppearance = appearance6;
            appearance5.TextHAlignAsString = "Left";
            this._grid.DisplayLayout.Override.HeaderAppearance = appearance5;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this._grid.DisplayLayout.Override.RowAppearance = appearance11;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this._grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this._grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(876, 517);
            this._grid.TabIndex = 13;
            this._grid.Text = "ultraGrid1";
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportDataSource);
            // 
            // ReportGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ReportGridView";
            this.Size = new System.Drawing.Size(876, 517);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter _dataExporter;
        private System.Windows.Forms.BindingSource _bindingSource;

    }
}
