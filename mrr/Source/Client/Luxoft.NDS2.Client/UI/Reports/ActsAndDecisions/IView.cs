﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using System;
using System.Collections;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IView : INotifier
    {
        # region Выбор налогового периода

        void SetPeriodsList(IEnumerable allTaxPeriods);

        object SelectedTaxPeriod { get; set; }

        event EventHandler SelectedDateModeChanged;

        ReportDateMode SelectedDateMode { get; set; }

        event EventHandler SelectedDateFromChanged;

        DateTime DateFrom { get; set; }

        bool DateToVisible { set; }

        event EventHandler SelectedDateToChanged;

        DateTime DateTo { get; set; }

        # endregion

        # region Выбор вида отчета

        void SetReports(IEnumerable reportsData);

        event EventHandler SelectedReportChanged;

        object SelectedReport
        {
            get;
            set;
        }

        # endregion

        # region Выбор регионов\испекций

        bool AllowSelectRegion { set; }

        ILookupViewer Region { get; }

        bool AllowSelectSono { set; }

        ILookupViewer Inspection { get; }

        # endregion      

        # region Отображение данных отчета

        bool AllowLoad { set; }

        event EventHandler ReportLoading;

        event EventHandler ExportRequested;

        bool AllowExport { set; }

        IReportGridView GridView { get; }

        # endregion

        # region Разное

        bool Ask(string caption, string message);

        # endregion
    }
}
