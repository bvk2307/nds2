﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Models;
using Luxoft.NDS2.Common.Models.AccessContext;
using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public enum ReportDateMode { OnDate, RangeOfDates };

    /// <summary>
    /// Этот класс реализует ViewModel экранной формы "Отчет по актам и решениям"
    /// </summary>
    public class ReportViewModel
    {
        # region .ctor

        /// <summary>
        /// Создает экземпляр класса ReportViewModel
        /// </summary>
        /// <param name="taxPeriods">Коллекция налоговых периодов, для которых возможно построение отчета</param>
        /// <param name="sonoCodes">Коллекция всех кодов инспекций</param>
        /// <param name="regions">Коллекция всех регионов</param>
        /// <param name="federalDistricts">Колекция федеральных округов</param>
        /// <param name="accessContext">Набор прав и ограничений текущего пользователя</param>
        public ReportViewModel(
            IEnumerable<EffectiveTaxPeriod> taxPeriods,
            Sono[] sonoCodes,
            Region[] regions,
            AccessContext accessContext)
        {
            if (taxPeriods == null || !taxPeriods.Any())
            {
                throw new ArgumentNullException("taxPeriods");
            }

            if (accessContext == null || accessContext.Operations == null || !accessContext.Operations.Any())
            {
                throw new ArgumentNullException("accessContext");
            }

            AllTaxPeriods = taxPeriods;
            _sonoCodes = sonoCodes;
            _regions = regions;
            _accessContext = accessContext;

            AvailableReports = _accessContext.ToModel();
            AllowSelectRegion = new Changeable<bool>();
            AllowSelectSono = new Changeable<bool>();
            AllowBuildReport = new Changeable<bool>();
            ReportData = new ReportDataSource();

            InitLookups();
            SetDefaults();                     
        }

        private void SetDefaults()
        {
            TaxPeriod =
                AllTaxPeriods.Any(x => x.Compare(DateTime.Now.Date) == PeriodToDateComparisonResult.Before)
                    ? AllTaxPeriods.First(x => x.Compare(DateTime.Now.Date) == PeriodToDateComparisonResult.Before)
                    : AllTaxPeriods.Last();
            Report = AvailableReports.First();
            DateFrom = DateTime.Now.Date;
            DateTo = DateTime.Now.Date;
        }

        # endregion

        # region Период формирования отчета

        /// <summary>
        /// Возвращает список доступных налоговых периодов
        /// </summary>
        public IEnumerable<EffectiveTaxPeriod> AllTaxPeriods
        {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает текущий отчетный период
        /// </summary>
        public EffectiveTaxPeriod TaxPeriod
        {
            get;
            set;
        }

        private ReportDateMode _dateMode = ReportDateMode.OnDate;

        /// <summary>
        /// Возвращает или задает режим выбора дат построения отчета (на дату или на период)
        /// </summary>
        public ReportDateMode DateMode
        {
            get
            {
                return _dateMode;
            }
            set
            {
                if (_dateMode != value)
                {
                    _dateMode = value;

                    if (AllowEditDateToChanged != null)
                    {
                        AllowEditDateToChanged(this, new EventArgs());
                    }

                    SetAllowBuildReport();
                }
            }
        }

        private DateTime _dateFrom;
        public DateTime DateFrom
        {
            get { return _dateFrom; }
            set
            {
                _dateFrom = value;
                SetAllowBuildReport();
            }
        }

        public long FromDateReportId
        {
            get;
            set;
        }

        public event EventHandler AllowEditDateToChanged;

        public bool AllowEditDateTo
        {
            get
            {
                return _dateMode == ReportDateMode.RangeOfDates;
            }
        }

        private DateTime _dateTo;
        public DateTime DateTo
        {
            get { return _dateTo; }
            set
            {
                _dateTo = value;
                SetAllowBuildReport();
            }
        }

        public long? ToDateReportId
        {
            get;
            set;
        }

        # endregion

        # region Выбор отчета

        private readonly AccessContext _accessContext;

        public IEnumerable<OperationModel> AvailableReports
        {
            get;
            private set;
        }

        private OperationModel _report;

        public OperationModel Report
        {
            get
            {
                return _report;
            }
            set
            {
                if (_report == null || !_report.Equals(value))
                {
                    _report = value;
                    UpdateLookups();
                    SetAllowBuildReport();
                }
            }
        }

        # endregion

        # region Ограничение по региону\инспекции

        public readonly Region[] _regions;

        public Changeable<bool> AllowSelectRegion
        {
            get;
            private set;
        }

        private void SetAllowSelectRegion()
        {
            AllowSelectRegion.Value = Report.Name == MrrOperations.ActAndDecision.ActDecisionReportBySono;
        }

        public LookupModel RegionsModel 
        { 
            get; 
            private set; 
        }

        public readonly Sono[] _sonoCodes;

        public Changeable<bool> AllowSelectSono
        {
            get;
            private set;
        }

        private void SetAllowSelectSono()
        {
            AllowSelectSono.Value = 
                Report.Name == MrrOperations.ActAndDecision.ActDecisionReportBySono
                && RegionsModel.Selection.Any();
        }

        public LookupModel InspectionsModel 
        { 
            get; 
            private set; 
        }

        private void InitLookups()
        {
            RegionsModel = new LookupModel(new ISelectableLookupItem[0]);
            InspectionsModel = new LookupModel(new ISelectableLookupItem[0]);

            RegionsModel.SelectionChanged += 
                (sender, args) => 
                { 
                    SetAllowSelectSono();

                    var availableSonoCodes = AvailableSonoCodes();
                    InspectionsModel.UpdateDictionary(availableSonoCodes);

                    if (availableSonoCodes.Count() == 1)
                    {
                        InspectionsModel.SelectAll();
                    }

                    SetAllowBuildReport();
                };
        }

        private void UpdateLookups()
        {
            SetAllowSelectRegion();
            SetAllowSelectSono();

            var availableRegions = AvailableRegions();            
            RegionsModel.UpdateDictionary(availableRegions);

            if (availableRegions.Count() == 1)
            {
                RegionsModel.SelectAll();
            }
        }

        private RegionLookupItem[] AvailableRegions()
        {
            if (!AllowSelectRegion.Value)
            {
                return new RegionLookupItem[0];
            }

            var allowedRegions = _report.GetRegions();
            var result = allowedRegions.Any() ? _regions.Where(x => allowedRegions.Contains(x.Code)) : _regions;

            return result.Select(region => new RegionLookupItem(region, _sonoCodes)).ToArray();
        }

        private ISelectableLookupItem[] AvailableSonoCodes()
        {
            if (!AllowSelectSono.Value)
            {
                return new ISelectableLookupItem[0];
            }

            var allowSonoCodes = _report.GetSonoCodes();
            var selectedRegions = RegionsModel.Selection.SelectMany(region => ((RegionLookupItem)region).Inspections);

            return !allowSonoCodes.Any()
                ? (ISelectableLookupItem[])selectedRegions.ToArray()
                : selectedRegions.Where(x => allowSonoCodes.Contains(x.Title)).ToArray();
        }

        public string[] GetRegionCodes()
        {
            if (AllowSelectRegion.Value && RegionsModel.Selection.Any())
            {
                return RegionsModel.Selection.Select(x => x.Title).ToArray();
            }

            return Report.GetRegions();            
        }

        public string[] GetSonoCodes()
        {
            if (AllowSelectRegion.Value && InspectionsModel.Selection.Any())
            {
                return InspectionsModel.Selection.Select(x => x.Title).ToArray();
            }

            return Report.GetSonoCodes();
        }

        # endregion

        # region Данные отчета

        private IEnumerable<IReportRowItem> _data;

        public Changeable<bool> AllowBuildReport
        {
            get;
            private set;
        }

        private void SetAllowBuildReport()
        {
            AllowBuildReport.Value = 
                (Report.Name != MrrOperations.ActAndDecision.ActDecisionReportBySono 
                    || RegionsModel.Selection.Any())
                && (DateMode == ReportDateMode.OnDate || DateTo >= DateFrom);
        }

        public ReportDataSource ReportData
        {
            get;
            private set;
        }

        # endregion
    }
}
