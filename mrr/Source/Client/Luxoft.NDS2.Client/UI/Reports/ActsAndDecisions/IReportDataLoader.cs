﻿using System;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportDataLoader
    {
        CancellationTokenSource StartLoading(long reportFromId, long? reportToId = null);

        event EventHandler<ReportDataLoadedEventArgs> DataLoaded;
    }
}
