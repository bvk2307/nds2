﻿namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportFactory
    {
        IReportDataLoader Loader(string[] regionCodes, string[] sonoCodes);

        IReportDataConvertor Convertor(string[] regionCodes, string[] sonoCodes);
    }
}
