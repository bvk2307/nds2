﻿
namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    partial class ReportView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraGroupBox _selectParameters;
            Infragistics.Win.Misc.UltraLabel labMode;
            Infragistics.Win.Misc.UltraLabel labArea;
            Infragistics.Win.Misc.UltraLabel labelPeriod;
            Infragistics.Win.Misc.UltraGroupBox reportGrid;
            this._dateToLabel = new Infragistics.Win.Misc.UltraLabel();
            this._editDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._selectReportDateMode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._selectReport = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._selectTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._dateFromLabel = new Infragistics.Win.Misc.UltraLabel();
            this._editDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._sonoLabel = new Infragistics.Win.Misc.UltraLabel();
            this._sonoLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            this._regionSelectLabel = new Infragistics.Win.Misc.UltraLabel();
            this._regionLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            this._reportDataGrid = new Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportGridView();
            _selectParameters = new Infragistics.Win.Misc.UltraGroupBox();
            labMode = new Infragistics.Win.Misc.UltraLabel();
            labArea = new Infragistics.Win.Misc.UltraLabel();
            labelPeriod = new Infragistics.Win.Misc.UltraLabel();
            reportGrid = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(_selectParameters)).BeginInit();
            _selectParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectReportDateMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(reportGrid)).BeginInit();
            reportGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // _selectParameters
            // 
            _selectParameters.Controls.Add(this._dateToLabel);
            _selectParameters.Controls.Add(this._editDateTo);
            _selectParameters.Controls.Add(this._selectReportDateMode);
            _selectParameters.Controls.Add(this._selectReport);
            _selectParameters.Controls.Add(this._selectTaxPeriod);
            _selectParameters.Controls.Add(this._dateFromLabel);
            _selectParameters.Controls.Add(this._editDateFrom);
            _selectParameters.Controls.Add(this._sonoLabel);
            _selectParameters.Controls.Add(this._sonoLookup);
            _selectParameters.Controls.Add(this._regionSelectLabel);
            _selectParameters.Controls.Add(this._regionLookup);
            _selectParameters.Controls.Add(labMode);
            _selectParameters.Controls.Add(labArea);
            _selectParameters.Controls.Add(labelPeriod);
            _selectParameters.Dock = System.Windows.Forms.DockStyle.Top;
            _selectParameters.Location = new System.Drawing.Point(0, 0);
            _selectParameters.Name = "_selectParameters";
            _selectParameters.Size = new System.Drawing.Size(959, 172);
            _selectParameters.TabIndex = 0;
            _selectParameters.Text = "Параметры отбора";
            // 
            // _dateToLabel
            // 
            this._dateToLabel.Location = new System.Drawing.Point(765, 31);
            this._dateToLabel.Name = "_dateToLabel";
            this._dateToLabel.Size = new System.Drawing.Size(160, 18);
            this._dateToLabel.TabIndex = 16;
            this._dateToLabel.Text = "Дата окончания периода:";
            // 
            // _editDateTo
            // 
            this._editDateTo.Location = new System.Drawing.Point(765, 55);
            this._editDateTo.Name = "_editDateTo";
            this._editDateTo.Size = new System.Drawing.Size(95, 21);
            this._editDateTo.TabIndex = 7;
            this._editDateTo.ValueChanged += new System.EventHandler(this._editDateToValueChanged);
            // 
            // _selectReportDateMode
            // 
            this._selectReportDateMode.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._selectReportDateMode.Location = new System.Drawing.Point(383, 55);
            this._selectReportDateMode.Name = "_selectReportDateMode";
            this._selectReportDateMode.Size = new System.Drawing.Size(200, 21);
            this._selectReportDateMode.TabIndex = 5;
            this._selectReportDateMode.ValueChanged += new System.EventHandler(this.SelectDateModeValueChanged);
            // 
            // _selectReport
            // 
            this._selectReport.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._selectReport.Location = new System.Drawing.Point(156, 55);
            this._selectReport.Name = "_selectReport";
            this._selectReport.Size = new System.Drawing.Size(140, 21);
            this._selectReport.TabIndex = 1;
            this._selectReport.ValueChanged += new System.EventHandler(this.SelectedReportValueChanged);
            // 
            // _selectTaxPeriod
            // 
            this._selectTaxPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._selectTaxPeriod.Location = new System.Drawing.Point(17, 55);
            this._selectTaxPeriod.Name = "_selectTaxPeriod";
            this._selectTaxPeriod.Size = new System.Drawing.Size(110, 21);
            this._selectTaxPeriod.TabIndex = 0;
            // 
            // _dateFromLabel
            // 
            this._dateFromLabel.Location = new System.Drawing.Point(613, 31);
            this._dateFromLabel.Name = "_dateFromLabel";
            this._dateFromLabel.Size = new System.Drawing.Size(160, 18);
            this._dateFromLabel.TabIndex = 11;
            this._dateFromLabel.Text = "Дата формирования отчета:";
            // 
            // _editDateFrom
            // 
            this._editDateFrom.Location = new System.Drawing.Point(613, 55);
            this._editDateFrom.Name = "_editDateFrom";
            this._editDateFrom.Size = new System.Drawing.Size(95, 21);
            this._editDateFrom.TabIndex = 6;
            this._editDateFrom.ValueChanged += new System.EventHandler(this._editDateFromValueChanged);
            // 
            // _sonoLabel
            // 
            this._sonoLabel.Location = new System.Drawing.Point(423, 105);
            this._sonoLabel.Name = "_sonoLabel";
            this._sonoLabel.Size = new System.Drawing.Size(350, 18);
            this._sonoLabel.TabIndex = 9;
            this._sonoLabel.Text = "Инспекция:";
            // 
            // _sonoLookup
            // 
            this._sonoLookup.Location = new System.Drawing.Point(423, 129);
            this._sonoLookup.Name = "_sonoLookup";
            this._sonoLookup.Size = new System.Drawing.Size(350, 25);
            this._sonoLookup.TabIndex = 4;
            // 
            // _regionSelectLabel
            // 
            this._regionSelectLabel.Location = new System.Drawing.Point(156, 105);
            this._regionSelectLabel.Name = "_regionSelectLabel";
            this._regionSelectLabel.Size = new System.Drawing.Size(250, 18);
            this._regionSelectLabel.TabIndex = 7;
            this._regionSelectLabel.Text = "Регион:";
            // 
            // _regionLookup
            // 
            this._regionLookup.Location = new System.Drawing.Point(156, 129);
            this._regionLookup.Name = "_regionLookup";
            this._regionLookup.Size = new System.Drawing.Size(250, 25);
            this._regionLookup.TabIndex = 3;
            // 
            // labMode
            // 
            labMode.Location = new System.Drawing.Point(383, 31);
            labMode.Name = "labMode";
            labMode.Size = new System.Drawing.Size(200, 18);
            labMode.TabIndex = 5;
            labMode.Text = "Режим:";
            // 
            // labArea
            // 
            labArea.Location = new System.Drawing.Point(156, 31);
            labArea.Name = "labArea";
            labArea.Size = new System.Drawing.Size(140, 18);
            labArea.TabIndex = 3;
            labArea.Text = "Область формирования:";
            // 
            // labelPeriod
            // 
            labelPeriod.Location = new System.Drawing.Point(17, 31);
            labelPeriod.Name = "labelPeriod";
            labelPeriod.Size = new System.Drawing.Size(110, 18);
            labelPeriod.TabIndex = 1;
            labelPeriod.Text = "Отчетный период:";
            // 
            // reportGrid
            // 
            reportGrid.Controls.Add(this._reportDataGrid);
            reportGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            reportGrid.Location = new System.Drawing.Point(0, 172);
            reportGrid.Name = "reportGrid";
            reportGrid.Size = new System.Drawing.Size(959, 297);
            reportGrid.TabIndex = 1;
            reportGrid.Text = "Отчет";
            // 
            // _reportDataGrid
            // 
            this._reportDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._reportDataGrid.Location = new System.Drawing.Point(3, 16);
            this._reportDataGrid.Name = "_reportDataGrid";
            this._reportDataGrid.Size = new System.Drawing.Size(953, 278);
            this._reportDataGrid.TabIndex = 0;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(reportGrid);
            this.Controls.Add(_selectParameters);
            this.Name = "View";
            this.Size = new System.Drawing.Size(959, 469);
            ((System.ComponentModel.ISupportInitialize)(_selectParameters)).EndInit();
            _selectParameters.ResumeLayout(false);
            _selectParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectReportDateMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(reportGrid)).EndInit();
            reportGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.LookupEditor.LookupBase _sonoLookup;
        private Controls.LookupEditor.LookupBase _regionLookup;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _editDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _selectTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _selectReportDateMode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _selectReport;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _editDateTo;
        private Infragistics.Win.Misc.UltraLabel _dateFromLabel;
        private Infragistics.Win.Misc.UltraLabel _dateToLabel;
        private ReportGridView _reportDataGrid;
        private Infragistics.Win.Misc.UltraLabel _regionSelectLabel;
        private Infragistics.Win.Misc.UltraLabel _sonoLabel;
    }
}
