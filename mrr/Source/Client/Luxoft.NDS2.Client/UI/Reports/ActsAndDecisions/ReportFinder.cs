﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using System;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public class ReportFinder : ServiceProxyBase<long>, IReportFinder
    {
        private readonly IActDecisionReportService _serviceProxy;

        public ReportFinder(IActDecisionReportService serviceProxy, INotifier notifier, IClientLogger clientLogger)
            : base(notifier, clientLogger)
        {
            _serviceProxy = serviceProxy;
        }

        private DateTime _date;

        private EffectiveTaxPeriod _period;

        public bool TryGetReportId(DateTime date, EffectiveTaxPeriod period, out long reportId)
        {
            _date = date;
            _period = period;

            return Invoke(() => _serviceProxy.ReportId(date, period.Year, (int)period.Quarter), out reportId);
        }

        protected override void OnNoDataFound()
        {
            _notifier.ShowError(string.Format("Данные за {0} на {1:d} отсутствуют", _period, _date));
        }
    }
}
