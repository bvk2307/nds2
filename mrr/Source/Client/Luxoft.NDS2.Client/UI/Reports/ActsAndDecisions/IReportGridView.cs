﻿using Luxoft.NDS2.Client.UI.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportGridView 
    {
        void SetDataSource(object dataSource);

        string TitleColumnHeaderText { set; }

        void ExportToExcel(ref string defaultFileName);
    }
}
