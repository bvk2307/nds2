﻿namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportFactoryCollection
    {
        IReportFactory this[string reportName]
        {
            get;
        }
    }
}
