﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.Helpers;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public partial class ReportGridView : UserControl, IReportGridView
    {
        public ReportGridView()
        {
            InitializeComponent();
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public string TitleColumnHeaderText
        {
            set 
            { 
                var titleColumnName = TypeHelper<IReportRowItem>.GetMemberName(x => x.District);
                _grid.DisplayLayout.Bands[0].Columns[titleColumnName].Header.Caption = value; 
            }
        }

        public void ExportToExcel(ref string fileName)
        {
            DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName);
            _dataExporter.Export(_grid, fileName);
        }
    }
}
