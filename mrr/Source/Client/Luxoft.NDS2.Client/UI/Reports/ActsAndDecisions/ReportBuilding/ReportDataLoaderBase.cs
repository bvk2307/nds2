﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using System;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public abstract class ReportDataLoaderBase: ServiceProxyBase<KnpResultReportSummary[]>, IReportDataLoader
    {
        private readonly IActDecisionReportService _serviceProxy;

        protected readonly string[] _regionCodes;

        protected readonly string[] _sonoCodes;

        private CancellationTokenSource _tokenSource;

        private long _reportToId;

        private long? _reportFromId;

        private bool _dataToLoaded;

        private KnpResultReportSummary[] _dataTo;

        protected ReportDataLoaderBase(
            IActDecisionReportService serviceProxy,
            string[] regionCodes,
            string[] sonoCodes,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _serviceProxy = serviceProxy;
            _regionCodes = regionCodes;
            _sonoCodes = sonoCodes;
        }

        public CancellationTokenSource StartLoading(long reportToId, long? reportFromId = null)
        {
            if (_tokenSource != null)
            {
                do
                {
                    Thread.Sleep(10);
                }
                while (_tokenSource != null);
            }

            _reportFromId = reportFromId;
            _reportToId = reportToId;
            _tokenSource = new CancellationTokenSource();

            BeginInvoke(_tokenSource, () => GetData(_serviceProxy, reportToId));

            return _tokenSource;
        }

        protected abstract OperationResult<KnpResultReportSummary[]> GetData(IActDecisionReportService service, long reportId);

        public event EventHandler<ReportDataLoadedEventArgs> DataLoaded;

        protected override void CallBack(KnpResultReportSummary[] result)
        {
            if (_tokenSource.Token.IsCancellationRequested)
            {
                return;
            }

            if (_dataToLoaded)
            {                
                RaiseDataLoaded(result);
            }
            else
            {
                _dataTo = result;
                _dataToLoaded = true;

                if (_reportFromId.HasValue)
                {
                    if (_reportFromId.Value == _reportToId)
                    {
                        RaiseDataLoaded(_dataTo);
                    }
                    else
                    {
                        BeginInvoke(_tokenSource, () => GetData(_serviceProxy, _reportFromId.Value));
                    }
                }
                else
                {
                    RaiseDataLoaded(new KnpResultReportSummary[0]);
                }
            }
        }

        private void RaiseDataLoaded(KnpResultReportSummary[] dataFrom)
        {
            _tokenSource = null;
            if (DataLoaded != null)
            {
                DataLoaded(this, new ReportDataLoadedEventArgs(_regionCodes, _sonoCodes, dataFrom, _dataTo));
            }
        }
    }
}
