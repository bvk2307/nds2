﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByDistrictRowModel : ReportRowModel
    {
        private readonly FederalDistrict _federalDistrict;

        public ReportByDistrictRowModel(
            FederalDistrict federalDistrict,
            IEnumerable<KnpResultReportSummary> dataTo,
            IEnumerable<KnpResultReportSummary> dataFrom)
            : base(dataTo, dataFrom)
        {
            _federalDistrict = federalDistrict;
        }

        public override string District
        {
            get
            {
                return _federalDistrict.Name;
            }
        }
    }
}
