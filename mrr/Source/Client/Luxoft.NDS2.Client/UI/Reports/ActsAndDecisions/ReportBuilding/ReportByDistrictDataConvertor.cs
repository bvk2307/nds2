﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByDistrictDataConvertor : IReportDataConvertor
    {
        private readonly FederalDistrict[] _districts;

        private readonly Dictionary<string, Region> _regions;

        public ReportByDistrictDataConvertor(FederalDistrict[] districts, Region[] regions)
        {
            _districts = districts;
            _regions = regions.ToDictionary(x => x.Code);
        }

        public IEnumerable<IReportRowItem> ToModel(KnpResultReportSummary[] dataFrom, KnpResultReportSummary[] dataTo)
        {
            var groupedDataFrom = dataFrom.GroupToDictionaryBy(x => _regions[x.RegionCode].FederalDistrictId);
            var groupedDataTo = dataTo.GroupToDictionaryBy(x => _regions[x.RegionCode].FederalDistrictId);

            foreach (var district in _districts)
            {
                yield return new ReportByDistrictRowModel(
                    district,                   
                    groupedDataTo.GetOrEmpty(district.Id),
                    groupedDataFrom.GetOrEmpty(district.Id));
            }
        }
    }
}
