﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public static class DictionaryHelper
    {
        public static Dictionary<TKey, List<TValue>> GroupToDictionaryBy<TKey, TValue>(
            this TValue[] collection,
            Func<TValue, TKey> keySelector)
        {
            var result = new Dictionary<TKey, List<TValue>>();

            foreach (var item in collection)
            {
                result.AddOrAppend(keySelector(item), item);
            }

            return result;
        }

        private static void AddOrAppend<TKey, TValue>(
            this Dictionary<TKey, List<TValue>> dictionary,
            TKey key,
            TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary[key] = new List<TValue> { value };
            }
            else
            {
                dictionary[key].Add(value);
            }
        }

        public static IEnumerable<TValue> GetOrEmpty<TKey, TValue>(
            this Dictionary<TKey, List<TValue>> dictionary,
            TKey key)
        {
            return dictionary.ContainsKey(key) 
                ? (IEnumerable<TValue>)dictionary[key] 
                : Enumerable.Empty<TValue>();
        }

        public static IEnumerable<TValue> GetOrEmpty<TKey, TValue>(
            this Dictionary<TKey, TValue> dictionary,
            TKey key)
        {
            return dictionary.ContainsKey(key)
                ? new TValue[] { dictionary[key] }
                : Enumerable.Empty<TValue>();
        }
    }
}
