﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportBySonoDataConvertor : IReportDataConvertor
    {
        private readonly IEnumerable<Sono> _ifnsList;

        public ReportBySonoDataConvertor(Sono[] sonoCodes, string[] restrictByRegion, string[] restrictBySono)
        {
            _ifnsList = sonoCodes.Where(x => x.SonoType == SonoType.Ifns || x.SonoType == SonoType.MILarge);

            if (restrictBySono.Any())
            {
                _ifnsList = _ifnsList.Where(x => restrictBySono.Contains(x.Code));
            }
            else
            {
                _ifnsList = _ifnsList.Where(x => restrictByRegion.Contains(x.RegionCode));
            }
        }

        public IEnumerable<IReportRowItem> ToModel(KnpResultReportSummary[] dataFrom, KnpResultReportSummary[] dataTo)
        {
            var groupedDataFrom = dataFrom.ToDictionary(x => x.SonoCode);
            var groupedDataTo = dataTo.ToDictionary(x => x.SonoCode);

            foreach (var ifns in _ifnsList)
            {
                yield return new ReportBySonoRowModel(
                    groupedDataTo.GetOrEmpty(ifns.Code),
                    groupedDataFrom.GetOrEmpty(ifns.Code),
                    ifns);
            }
        }
    }
}
