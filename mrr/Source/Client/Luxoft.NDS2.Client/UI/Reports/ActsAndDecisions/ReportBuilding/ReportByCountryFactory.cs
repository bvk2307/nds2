﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByCountryFactory : IReportFactory
    {
        private readonly IActDecisionReportService _serviceProxy;

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        public ReportByCountryFactory(
            IActDecisionReportService serviceProxy,
            INotifier notifier,
            IClientLogger logger)
        {
            _serviceProxy = serviceProxy;
            _notifier = notifier;
            _logger = logger;
        }

        public IReportDataLoader Loader(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportByCountryDataLoader(_serviceProxy, regionCodes, sonoCodes, _notifier, _logger);
        }

        public IReportDataConvertor Convertor(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportByCountryDataConvertor();
        }
    }
}
