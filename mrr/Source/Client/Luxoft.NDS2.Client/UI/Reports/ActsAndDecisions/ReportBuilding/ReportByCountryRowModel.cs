﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByCountryRowModel : ReportRowModel
    {
        public ReportByCountryRowModel(
            IEnumerable<KnpResultReportSummary> dataTo,
            IEnumerable<KnpResultReportSummary> dataFrom)
            : base(dataTo, dataFrom)
        {
        }

        public override string District
        {
            get { return "По стране"; }
        }
    }
}
