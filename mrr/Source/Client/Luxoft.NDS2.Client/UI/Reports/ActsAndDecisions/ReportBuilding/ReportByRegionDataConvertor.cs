﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByRegionDataConvertor : IReportDataConvertor
    {
        private readonly IEnumerable<Sono> _ufnsList;

        public ReportByRegionDataConvertor(Sono[] sonoCodes)
        {
            _ufnsList = sonoCodes.Where(x => x.SonoType == SonoType.Ufns);
        }

        public IEnumerable<IReportRowItem> ToModel(KnpResultReportSummary[] dataFrom, KnpResultReportSummary[] dataTo)
        {
            var groupedDataFrom = dataFrom.GroupToDictionaryBy(x => x.RegionCode);
            var groupedDataTo = dataTo.GroupToDictionaryBy(x => x.RegionCode);

            foreach (var ufns in _ufnsList)
            {
                yield return new ReportByRegionRowModel(                    
                    groupedDataTo.GetOrEmpty(ufns.RegionCode),
                    groupedDataFrom.GetOrEmpty(ufns.RegionCode),
                    ufns);
            }
        }
    }
}
