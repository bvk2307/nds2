﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByCountryDataConvertor : IReportDataConvertor
    {
        public IEnumerable<IReportRowItem> ToModel(
            KnpResultReportSummary[] dataFrom, 
            KnpResultReportSummary[] dataTo)
        {
            return new IReportRowItem[] { new ReportByCountryRowModel(dataTo, dataFrom) };
        }
    }
}
