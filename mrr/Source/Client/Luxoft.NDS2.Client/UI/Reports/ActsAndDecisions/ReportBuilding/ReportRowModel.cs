﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public abstract class ReportRowModel : IReportRowItem
    {
        protected ReportRowModel(IEnumerable<KnpResultReportSummary> dataTo, IEnumerable<KnpResultReportSummary> dataFrom)
        {
            foreach (var dataItem in dataTo)
            {
                ActQuantity += dataItem.ActQuantity;
                ActDiscrepancyNdsQuantity += dataItem.ActNdsDiscrepancyQuantity;
                ActDiscrepancyNdsAmount += dataItem.ActNdsDiscrepancyAmount;
                ActGapAmount += dataItem.ActGapAmount;
                ActGapQuantity += dataItem.ActGapQuantity;
                DecisionQuantity += dataItem.DecisionQuantity;
                DecisionDiscrepancyNdsQuantity += dataItem.DecisionNdsDiscrepancyQuantity;
                DecisionDiscrepancyNdsAmount += dataItem.DecisionNdsDiscrepancyAmount;
                DecisionGapAmount += dataItem.DecisionGapAmount;
                DecisionGapQuantity += dataItem.DecisionGapQuantity;
            }

            foreach (var dataItem in dataFrom)
            {
                ActQuantity -= dataItem.ActQuantity;
                ActDiscrepancyNdsQuantity -= dataItem.ActNdsDiscrepancyQuantity;
                ActDiscrepancyNdsAmount -= dataItem.ActNdsDiscrepancyAmount;
                ActGapAmount -= dataItem.ActGapAmount;
                ActGapQuantity -= dataItem.ActGapQuantity;
                DecisionQuantity -= dataItem.DecisionQuantity;
                DecisionDiscrepancyNdsQuantity -= dataItem.DecisionNdsDiscrepancyQuantity;
                DecisionDiscrepancyNdsAmount -= dataItem.DecisionNdsDiscrepancyAmount;
                DecisionGapAmount -= dataItem.DecisionGapAmount;
                DecisionGapQuantity -= dataItem.DecisionGapQuantity;
            }

            ActDiscrepancyTotalAmount = ActDiscrepancyNdsAmount + ActGapAmount;
            ActDiscrepancyTotalQuantity = ActDiscrepancyNdsQuantity + ActGapQuantity;
            DecisionDiscrepancyTotalAmount = DecisionDiscrepancyNdsAmount + DecisionGapAmount;
            DecisionDiscrepancyTotalQuantity = DecisionDiscrepancyNdsQuantity + DecisionGapQuantity;
        }

        public abstract string District
        {
            get;
        }

        public int ActQuantity
        {
            get;
            private set;
        }

        public int ActDiscrepancyNdsQuantity
        {
            get;
            private set;
        }

        public decimal ActDiscrepancyNdsAmount
        {
            get;
            private set;
        }

        public int ActGapQuantity
        {
            get;
            private set;
        }

        public decimal ActGapAmount
        {
            get;
            private set;
        }

        public int ActDiscrepancyTotalQuantity
        {
            get;
            private set;
        }

        public decimal ActDiscrepancyTotalAmount
        {
            get;
            private set;
        }

        public int DecisionQuantity
        {
            get;
            private set;
        }

        public int DecisionDiscrepancyNdsQuantity
        {
            get;
            private set;
        }

        public decimal DecisionDiscrepancyNdsAmount
        {
            get;
            private set;
        }

        public int DecisionGapQuantity
        {
            get;
            private set;
        }

        public decimal DecisionGapAmount
        {
            get;
            private set;
        }

        public int DecisionDiscrepancyTotalQuantity
        {
            get;
            private set;
        }

        public decimal DecisionDiscrepancyTotalAmount
        {
            get;
            private set;
        }
    }
}
