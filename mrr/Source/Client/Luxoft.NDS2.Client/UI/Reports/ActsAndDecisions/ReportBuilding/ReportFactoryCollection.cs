﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportFactoryCollection : IReportFactoryCollection
    {
        private readonly Dictionary<string, IReportFactory> _factories =
            new Dictionary<string, IReportFactory>();

        public void AddFactory(string reportName, IReportFactory factory)
        {
            _factories.Add(reportName, factory);
        }

        public IReportFactory this[string reportName]
        {
            get 
            { 
                return _factories[reportName]; 
            }
        }
    }
}
