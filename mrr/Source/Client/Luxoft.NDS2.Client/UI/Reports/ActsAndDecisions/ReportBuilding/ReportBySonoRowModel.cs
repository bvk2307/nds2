﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Models;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportBySonoRowModel : ReportRowModel
    {
        private readonly Sono _ifns;

        public ReportBySonoRowModel(
            IEnumerable<KnpResultReportSummary> dataTo,
            IEnumerable<KnpResultReportSummary> dataFrom,
            Sono ifns)
            : base(dataTo, dataFrom)
        {
            _ifns = ifns;
        }

        public override string District
        {
            get 
            {
                return _ifns.GetTitle();
            }
        }
    }
}
