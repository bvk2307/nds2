﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportBySonoFactory : IReportFactory
    {
        private readonly IActDecisionReportService _serviceProxy;

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        private readonly Sono[] _sonoCodes;

        public ReportBySonoFactory(
            IActDecisionReportService serviceProxy,
            INotifier notifier,
            IClientLogger logger,
            Sono[] sonoCodes)
        {
            _serviceProxy = serviceProxy;
            _notifier = notifier;
            _logger = logger;
            _sonoCodes = sonoCodes;
        }

        public IReportDataLoader Loader(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportBySonoDataLoader(_serviceProxy, regionCodes, sonoCodes, _notifier, _logger);
        }

        public IReportDataConvertor Convertor(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportBySonoDataConvertor(_sonoCodes, regionCodes, sonoCodes);
        }
    }
}
