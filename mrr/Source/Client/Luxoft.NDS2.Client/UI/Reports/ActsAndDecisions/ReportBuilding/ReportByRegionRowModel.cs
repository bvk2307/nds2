﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Models;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByRegionRowModel : ReportRowModel
    {
        private readonly Sono _ufns;

        public ReportByRegionRowModel(
            IEnumerable<KnpResultReportSummary> dataTo,
            IEnumerable<KnpResultReportSummary> dataFrom, 
            Sono ufns)
            : base(dataTo, dataFrom)
        {
            _ufns = ufns;
        }

        public override string District
        {
            get 
            { 
                return _ufns.GetTitle(); 
            }
        }
    }
}
