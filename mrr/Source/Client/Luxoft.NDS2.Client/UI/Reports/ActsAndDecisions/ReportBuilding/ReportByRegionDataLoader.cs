﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByRegionDataLoader : ReportDataLoaderBase
    {
        public ReportByRegionDataLoader(
            IActDecisionReportService serviceProxy,
            string[] regionCodes,
            string[] sonoCodes,
            INotifier notifier,
            IClientLogger logger)
            : base(serviceProxy, regionCodes, sonoCodes, notifier, logger)
        {
        }

        protected override OperationResult<KnpResultReportSummary[]> GetData(IActDecisionReportService service, long reportId)
        {
            return service.DataByRegion(reportId);
        }
    }
}
