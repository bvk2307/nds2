﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportBuilding
{
    public class ReportByDistrictFactory : IReportFactory
    {
        private readonly IActDecisionReportService _serviceProxy;

        private readonly INotifier _notifier;

        private readonly IClientLogger _logger;

        private readonly FederalDistrict[] _districts;

        private readonly Region[] _regions;

        public ReportByDistrictFactory(
            IActDecisionReportService serviceProxy,
            INotifier notifier,
            IClientLogger logger,
            FederalDistrict[] districts,
            Region[] regions)
        {
            _serviceProxy = serviceProxy;
            _notifier = notifier;
            _logger = logger;
            _districts = districts;
            _regions = regions;
        }

        public IReportDataLoader Loader(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportByDistrictDataLoader(_serviceProxy, regionCodes, sonoCodes, _notifier, _logger);
        }

        public IReportDataConvertor Convertor(string[] regionCodes, string[] sonoCodes)
        {
            return new ReportByDistrictDataConvertor(_districts, _regions);
        }
    }
}
