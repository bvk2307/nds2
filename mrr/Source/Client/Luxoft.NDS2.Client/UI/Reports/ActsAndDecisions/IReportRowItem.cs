﻿namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportRowItem
    {
        string District { get; }

        int ActQuantity { get; }

        int ActDiscrepancyNdsQuantity { get; }

        decimal ActDiscrepancyNdsAmount { get; }

        int ActGapQuantity { get; }

        decimal ActGapAmount { get; }

        int ActDiscrepancyTotalQuantity { get; }

        decimal ActDiscrepancyTotalAmount { get; }

        int DecisionQuantity { get; }

        int DecisionDiscrepancyNdsQuantity { get; }

        decimal DecisionDiscrepancyNdsAmount { get; }

        int DecisionGapQuantity { get; }

        decimal DecisionGapAmount { get; }

        int DecisionDiscrepancyTotalQuantity { get; }

        decimal DecisionDiscrepancyTotalAmount { get; }
    }
}
