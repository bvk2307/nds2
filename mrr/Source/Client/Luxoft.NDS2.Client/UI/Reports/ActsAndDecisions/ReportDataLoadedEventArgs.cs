﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public class ReportDataLoadedEventArgs : EventArgs
    {
        public ReportDataLoadedEventArgs(
            string[] regionCodes,
            string[] sonoCodes,
            KnpResultReportSummary[] dataFrom,
            KnpResultReportSummary[] dataTo)
        {
            RegionCodes = regionCodes;
            SonoCodes = sonoCodes;
            DataFrom = dataFrom;
            DataTo = dataTo;
        }

        public string[] SonoCodes
        {
            get;
            private set;
        }

        public string[] RegionCodes
        {
            get;
            private set;
        }

        public KnpResultReportSummary[] DataFrom
        {
            get;
            private set;
        }

        public KnpResultReportSummary[] DataTo
        {
            get;
            private set;
        }
    }
}
