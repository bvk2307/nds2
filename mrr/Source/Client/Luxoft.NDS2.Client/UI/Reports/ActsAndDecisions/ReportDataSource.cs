﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public class ReportDataSource : ListViewModel<IReportRowItem, IReportRowItem>
    {
        protected override IReportRowItem ConvertToModel(IReportRowItem dataItem)
        {
            return dataItem;
        }
    }
}
