﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public interface IReportFinder
    {
        bool TryGetReportId(DateTime date, EffectiveTaxPeriod period, out long reportId);
    }
}
