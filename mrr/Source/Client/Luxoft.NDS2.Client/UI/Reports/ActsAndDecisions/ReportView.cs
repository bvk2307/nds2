﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Infragistics.Win;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using System;
using System.Collections;
using System.Windows.Forms;
using FullEditor = Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupFull.Editor;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{
    public partial class ReportView : BaseView, IView
    {
        # region .ctor

        public ReportView()
        {
            InitializeComponent();
        }

        public ReportView(PresentationContext presentationContext, WorkItem workItem)
            :base(presentationContext, workItem)
        {
            InitializeComponent();
            InitRibbon();

            _selectReportDateMode.Items.Add(new ValueListItem(ReportDateMode.OnDate, "Данные на дату"));
            _selectReportDateMode.Items.Add(new ValueListItem(ReportDateMode.RangeOfDates, "Изменение значений между датами"));          

            _regionLookup.WithEditor(new FullEditor());
            _sonoLookup.WithEditor(new FullEditor());

            _editDateFrom.DisableManualInput();
            _editDateTo.DisableManualInput();
        }

        # endregion

        #region Инициализация панели комманд

        private const string LoadCommand = "ActsDecisionsReportLoad";

        private const string ExportCommand = "ActsAndDecisionsExport";

        private UcRibbonButtonToolContext _buildReport;

        private UcRibbonButtonToolContext _exportReport;

        public bool AllowLoad
        {
            set
            {
                _buildReport.Enabled = value;
                RefreshEkp();
            }
        }

        public bool AllowExport
        {
            set
            {
                _exportReport.Enabled = value;
                RefreshEkp();
            }
        }

        private void InitRibbon()
        {
            if (WorkItem == null)
                return;

            var resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2ActsAndDecisionReportResult")
            {
                Text = _presentationContext.WindowTitle,
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var navigation = tabNavigator.AddGroup("NDS2ActsAndDecisionReport");
            navigation.Text = "Функции";
            navigation.Visible = true;

            _buildReport = new UcRibbonButtonToolContext(_presentationContext, "btnLoad", LoadCommand)
            {
                Text = "Сформировать",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "request",
                SmallImageName = "request",
                Enabled = false
            };

            _exportReport = new UcRibbonButtonToolContext(_presentationContext, "btnExport", ExportCommand)
            {
                Text = "Экспортировать",
                ToolTipText = "",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "excel_create",
                SmallImageName = "excel_create",
                Enabled = false
            };

            navigation.ToolList.AddRange(
                new[] { 
                    new UcRibbonToolInstanceSettings(_buildReport.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_exportReport.ItemName, UcRibbonToolSize.Large)
                });

            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                _buildReport,
                _exportReport,
                tabNavigator
            });

            RefreshEkp();
        }

        [CommandHandler(LoadCommand)]
        public virtual void LoadClick(object sender, EventArgs e)
        {
            var handler = ReportLoading;
            if (handler != null)
                handler(this, new EventArgs());
        }

        [CommandHandler(ExportCommand)]
        public virtual void ExportClick(object sender, EventArgs e)
        {
            var handler = ExportRequested;
            if (handler != null)
                handler(this, new EventArgs());
        }

        #endregion

        # region Выбор периода

        public void SetPeriodsList(IEnumerable allTaxPeriods)
        {
            _selectTaxPeriod.SetDataSource(allTaxPeriods);
        }

        public object SelectedTaxPeriod
        {
            get
            {
                return _selectTaxPeriod.GetValue();
            }
            set
            {
                _selectTaxPeriod.SetValue(value);
            }
        }

        public event EventHandler SelectedDateModeChanged;

        private void SelectDateModeValueChanged(object sender, EventArgs e)
        {
            if (SelectedDateModeChanged != null)
            {
                SelectedDateModeChanged(this, e);
            }
        }

        public ReportDateMode SelectedDateMode
        {
            get
            {
                return _selectReportDateMode.GetValue<ReportDateMode>();
            }
            set
            {
                _selectReportDateMode.SetValue(value);
            }
        }

        public event EventHandler SelectedDateFromChanged;

        public DateTime DateFrom
        {
            get
            {
                return _editDateFrom.DateTime;
            }
            set
            {
                _editDateFrom.DateTime = value;
            }
        }

        public bool DateToVisible
        {
            set
            {
                _editDateTo.Visible = value;
                _dateFromLabel.Text = value ? "Дата начала периода:" : "Дата формирования отчета:";
                _dateToLabel.Visible = value;
            }
        }

        public event EventHandler SelectedDateToChanged;

        public DateTime DateTo
        {
            get
            {
                return _editDateTo.DateTime;
            }
            set
            {
                _editDateTo.DateTime = value;
            }
        }

        private void _editDateFromValueChanged(object sender, EventArgs e)
        {
            if (SelectedDateFromChanged != null)
            {
                SelectedDateFromChanged(this, EventArgs.Empty);
            }
        }

        private void _editDateToValueChanged(object sender, EventArgs e)
        {
            if (SelectedDateToChanged != null)
            {
                SelectedDateToChanged(this, EventArgs.Empty);
            }
        }

        # endregion

        #region Выбор отчета

        public void SetReports(IEnumerable reportsData)
        {
            _selectReport.SetDataSource(reportsData);
        }

        public event EventHandler SelectedReportChanged;

        private void SelectedReportValueChanged(object sender, EventArgs e)
        {
            if (SelectedReportChanged != null)
            {
                SelectedReportChanged(sender, e);
            }
        }

        public object SelectedReport
        {
            get
            {
                return _selectReport.GetValue();
            }
            set
            {
                _selectReport.SetValue(value);
            }
        }

        #endregion

        #region Регионы и инспекции

        public bool AllowSelectRegion
        {
            set 
            { 
                _regionSelectLabel.Visible = value;
                _regionLookup.Visible = value;
            }
        }

        ILookupViewer IView.Region 
        { 
            get 
            { 
                return _regionLookup; 
            } 
        }

        public bool AllowSelectSono
        {
            set 
            { 
                _sonoLabel.Visible = value;
                _sonoLookup.Visible = value;
            }
        }

        public ILookupViewer Inspection 
        { 
            get 
            { 
                return _sonoLookup; 
            } 
        }

        #endregion

        #region Данные отчета

        public event EventHandler ReportLoading;

        public IReportGridView GridView
        {
            get { return _reportDataGrid; }
        }

        public bool SelectSaveFile(ref string fileName)
        {
            return DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName);
        }

        public event EventHandler ExportRequested;

        # endregion

        # region Разное

        public bool Ask(string caption, string text)
        {
            return ShowQuestion(caption, text) == DialogResult.Yes;
        }

        #endregion
    }
}
