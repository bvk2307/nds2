﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Models.AccessContext;
using System;
using System.Linq;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions
{   
    /// <summary>
    /// Этот класс реализует Presenter экранной формы "Отчет об актах и решениях"
    /// </summary>
    public class ReportPresenter
    {
        # region .ctor

        private readonly IView _view;

        private readonly ReportViewModel _model;

        private readonly IReportFinder _reportFinder;

        private readonly IReportFactoryCollection _reportFactories;

        private readonly IClientLogger _logger;

        private readonly IFileOpener _fileOpener;

        public ReportPresenter(
            IView view, 
            ReportViewModel model, 
            IReportFinder reportFinder,
            IReportFactoryCollection reportFactories,
            IClientLogger logger,
            IFileOpener fileOpener)
        {
            _view = view;
            _model = model;
            _reportFinder = reportFinder;
            _reportFactories = reportFactories;
            _logger = logger;
            _fileOpener = fileOpener;
            var regionPresenter = new LookupPresenter(_view.Region, _model.RegionsModel);
            var inspectionsPresenter = new LookupPresenter(_view.Inspection, model.InspectionsModel);

            InitViewLists();
            InitViewState();
            BindViewEvents();
            BindModelEvents();         
        }

        private void InitViewLists()
        {
            _view.GridView.SetDataSource(_model.ReportData);
            _view.SetPeriodsList(_model.AllTaxPeriods);
            _view.SetReports(_model.AvailableReports);
        }

        private void InitViewState()
        {
            _view.SelectedTaxPeriod = _model.TaxPeriod;
            _view.SelectedDateMode = _model.DateMode;
            _view.DateFrom = _model.DateFrom;            
            _view.DateTo = _model.DateTo;
            _view.SelectedReport = _model.Report;            

            SetDateToVisibility();
            SetAllowSelectRegion();
            SetAllowSelectSono();
            SetAllowBuildReport();
            SetAllowExport();
            SetTitleHeader();
        }

        private void BindViewEvents()
        {
            _view.SelectedDateModeChanged += SelectedDateModeChanged;
            _view.SelectedReportChanged += SelectedReportChanged;
            _view.ReportLoading += ReportLoading;
            _view.ExportRequested += OnExportRequested;
            _view.SelectedDateFromChanged += SelectedDateFromChanged;
            _view.SelectedDateToChanged += SelectedDateToChanged;
        }

        private void BindModelEvents()
        {
            _model.AllowEditDateToChanged += (sender, e) => SetDateToVisibility();
            _model.AllowSelectRegion.PropertyChanged +=
                (sender, e) => SetAllowSelectRegion();
            _model.AllowSelectSono.PropertyChanged +=
                (sender, e) => SetAllowSelectSono();
            _model.AllowBuildReport.PropertyChanged +=
                (sender, e) => SetAllowBuildReport();
            _model.ReportData.ListItems.ListChanged +=
                (sender, e) => SetAllowExport();
        }

        # endregion

        # region Обработка событий View

        private void SetTitleHeader()
        {
            _view.GridView.TitleColumnHeaderText = _model.Report.Description;
        }

        private void SelectedDateModeChanged(object sender, EventArgs e)
        {
            _model.DateMode = _view.SelectedDateMode;
        }

        private void SelectedDateFromChanged(object sender, EventArgs e)
        {
            _model.DateFrom = _view.DateFrom;
        }

        private void SelectedDateToChanged(object sender, EventArgs e)
        {
            _model.DateTo = _view.DateTo;
        }

        private void SelectedReportChanged(object sender, EventArgs e)
        {
            _model.Report = (OperationModel)_view.SelectedReport;
        }

        private CancellationTokenSource reportLoaderTokenSource;

        private void ReportLoading(object sender, EventArgs e)
        {
            _model.TaxPeriod = (EffectiveTaxPeriod)_view.SelectedTaxPeriod;
            _model.DateFrom = _view.DateFrom;
            _model.DateTo = _view.DateTo;

            if (reportLoaderTokenSource != null 
                && reportLoaderTokenSource.Token != null
                && reportLoaderTokenSource.Token.CanBeCanceled)
            {
                reportLoaderTokenSource.Cancel();
            }

            long toReportId;
            var fromReportId = default(long);
            var dateTo = _model.AllowEditDateTo ? _model.DateTo : _model.DateFrom;

            if (!_reportFinder.TryGetReportId(dateTo, _model.TaxPeriod, out toReportId))
            {
                return;
            }

            if (_model.AllowEditDateTo
                && !_reportFinder.TryGetReportId(_model.DateFrom.AddDays(-1), _model.TaxPeriod, out fromReportId))
            {
                return;
            }

            var reportLoader =
                _reportFactories[_model.Report.Name]
                    .Loader(_model.GetRegionCodes(), _model.GetSonoCodes());
            reportLoader.DataLoaded += DataLoaded;

            SetTitleHeader();
            _model.ReportData.UpdateList(Enumerable.Empty<IReportRowItem>());

            reportLoaderTokenSource = 
                reportLoader.StartLoading(
                    toReportId, 
                    _model.AllowEditDateTo ? (Nullable<long>)fromReportId : null);
        }

        private void DataLoaded(object sender, ReportDataLoadedEventArgs e)
        {
            var reportConverter =
                _reportFactories[_model.Report.Name]
                    .Convertor(e.RegionCodes, e.SonoCodes);
            _model.ReportData.UpdateList(reportConverter.ToModel(e.DataFrom, e.DataTo));            
        }

        private void OnExportRequested(object sender, EventArgs e)
        {
            var fileName = string.Format(ResourceManagerNDS2.ActsAndDecisionsMessages.DefaultExcelFileName, DateTime.Now);

            try
            {
                _view.GridView.ExportToExcel(ref fileName);
            }
            catch (Exception error)
            {
                _logger.LogError(error, "ActDecisionReport.ExportToExcel");
                _view.ShowError("Не удалось экспортировать данные отчета в файл");
                return;
            }

            if (!_view.Ask(
                    ResourceManagerNDS2.ExportExcelMessages.Caption,
                    string.Format(
                        ResourceManagerNDS2.ExportExcelMessages.OpenQuery,
                        fileName)))
            {
                return;
            }

            try
            {
                _fileOpener.Open(fileName);
            }
            catch(Exception error)
            {
                _logger.LogError(error, "ActDecisionReport.OpenExcelFile");
                _view.ShowNotification(string.Format(ResourceManagerNDS2.ExportExcelMessages.FileOpenFailed, fileName));
            }
        }

        # endregion

        # region Обработка событий ViewModel

        private void SetDateToVisibility()
        {
            _view.DateToVisible = _model.AllowEditDateTo;
        }

        private void SetAllowSelectRegion()
        {
            _view.AllowSelectRegion = _model.AllowSelectRegion.Value;
        }

        private void SetAllowSelectSono()
        {
            _view.AllowSelectSono = _model.AllowSelectSono.Value;
        }

        private void SetAllowBuildReport()
        {
            _view.AllowLoad = _model.AllowBuildReport.Value;
        }

        private void SetAllowExport()
        {
            _view.AllowExport = _model.ReportData.ListItems.Any();
        }
        
        # endregion
    }
}
