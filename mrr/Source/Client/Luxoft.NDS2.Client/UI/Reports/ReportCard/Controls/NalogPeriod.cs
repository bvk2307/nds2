﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public class NalogPeriod : DictionaryNalogPeriods
    {
        public int Year { get; set; }
    }

    public class NalogPeriodCheck : NalogPeriod
    {
        public bool Check { get; set; }
    }
}
