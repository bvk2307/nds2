﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class MatchingRuleSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Description", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 3);
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            this.labelAggragationLevelNO = new Infragistics.Win.Misc.UltraLabel();
            this.comboAggregateLevelNO = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraTabControlNO = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPageNO = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageCountry = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageFederalDistrict = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelFederalDistrict = new Infragistics.Win.Misc.UltraLabel();
            this.comboFederalDistrict = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTabPageRegion = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageInspection = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarOneDay = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateOneDay = new Infragistics.Win.Misc.UltraLabel();
            this.lookupMultiRegion = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.lookupMultiInspection = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).BeginInit();
            this.ultraTabControlNO.SuspendLayout();
            this.ultraTabPageFederalDistrict.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).BeginInit();
            this.ultraTabPageRegion.SuspendLayout();
            this.ultraTabPageInspection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarOneDay)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAggragationLevelNO
            // 
            this.labelAggragationLevelNO.Location = new System.Drawing.Point(9, 17);
            this.labelAggragationLevelNO.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelAggragationLevelNO.Name = "labelAggragationLevelNO";
            this.labelAggragationLevelNO.Size = new System.Drawing.Size(232, 17);
            this.labelAggragationLevelNO.TabIndex = 83;
            this.labelAggragationLevelNO.Text = "Уровень агрегации по налоговым органам:";
            // 
            // comboAggregateLevelNO
            // 
            this.comboAggregateLevelNO.DisplayMember = "NAME";
            this.comboAggregateLevelNO.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboAggregateLevelNO.Location = new System.Drawing.Point(9, 35);
            this.comboAggregateLevelNO.Margin = new System.Windows.Forms.Padding(1);
            this.comboAggregateLevelNO.Name = "comboAggregateLevelNO";
            this.comboAggregateLevelNO.Size = new System.Drawing.Size(280, 21);
            this.comboAggregateLevelNO.TabIndex = 82;
            // 
            // ultraTabControlNO
            // 
            this.ultraTabControlNO.Controls.Add(this.ultraTabSharedControlsPageNO);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageCountry);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageFederalDistrict);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageRegion);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageInspection);
            this.ultraTabControlNO.Location = new System.Drawing.Point(8, 59);
            this.ultraTabControlNO.Name = "ultraTabControlNO";
            this.ultraTabControlNO.SharedControlsPage = this.ultraTabSharedControlsPageNO;
            this.ultraTabControlNO.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.ultraTabControlNO.Size = new System.Drawing.Size(447, 49);
            this.ultraTabControlNO.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.ultraTabControlNO.TabIndex = 84;
            ultraTab4.Key = "tContry";
            ultraTab4.TabPage = this.ultraTabPageCountry;
            ultraTab4.Text = "tabContry";
            ultraTab5.Key = "tFederalDistrict";
            ultraTab5.TabPage = this.ultraTabPageFederalDistrict;
            ultraTab5.Text = "tabFederalDistrict";
            ultraTab6.Key = "tRegion";
            ultraTab6.TabPage = this.ultraTabPageRegion;
            ultraTab6.Text = "tabRegion";
            ultraTab7.Key = "tInspection";
            ultraTab7.TabPage = this.ultraTabPageInspection;
            ultraTab7.Text = "tabInspection";
            this.ultraTabControlNO.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7});
            this.ultraTabControlNO.UseAppStyling = false;
            // 
            // ultraTabSharedControlsPageNO
            // 
            this.ultraTabSharedControlsPageNO.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPageNO.Name = "ultraTabSharedControlsPageNO";
            this.ultraTabSharedControlsPageNO.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraTabPageCountry
            // 
            this.ultraTabPageCountry.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageCountry.Name = "ultraTabPageCountry";
            this.ultraTabPageCountry.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraTabPageFederalDistrict
            // 
            this.ultraTabPageFederalDistrict.Controls.Add(this.ultraLabelFederalDistrict);
            this.ultraTabPageFederalDistrict.Controls.Add(this.comboFederalDistrict);
            this.ultraTabPageFederalDistrict.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageFederalDistrict.Name = "ultraTabPageFederalDistrict";
            this.ultraTabPageFederalDistrict.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelFederalDistrict
            // 
            this.ultraLabelFederalDistrict.Location = new System.Drawing.Point(1, 3);
            this.ultraLabelFederalDistrict.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelFederalDistrict.Name = "ultraLabelFederalDistrict";
            this.ultraLabelFederalDistrict.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelFederalDistrict.TabIndex = 76;
            this.ultraLabelFederalDistrict.Text = "Федеральные округа:";
            // 
            // comboFederalDistrict
            // 
            this.comboFederalDistrict.CheckedListSettings.CheckStateMember = "";
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboFederalDistrict.DisplayLayout.Appearance = appearance2;
            ultraGridColumn5.Header.Caption = "Название";
            ultraGridColumn5.Header.VisiblePosition = 2;
            ultraGridColumn5.Width = 400;
            ultraGridColumn6.DataType = typeof(bool);
            ultraGridColumn6.Header.Caption = "";
            ultraGridColumn6.Header.VisiblePosition = 0;
            ultraGridColumn6.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn6.Width = 30;
            ultraGridColumn7.Header.Caption = "Код";
            ultraGridColumn7.Header.VisiblePosition = 1;
            ultraGridColumn7.Hidden = true;
            ultraGridColumn8.Header.VisiblePosition = 3;
            ultraGridColumn8.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8});
            this.comboFederalDistrict.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.comboFederalDistrict.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboFederalDistrict.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboFederalDistrict.DisplayLayout.Override.CellAppearance = appearance7;
            this.comboFederalDistrict.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboFederalDistrict.DisplayLayout.Override.CellPadding = 0;
            appearance8.BackColor = System.Drawing.SystemColors.Control;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.Override.GroupByRowAppearance = appearance8;
            appearance9.TextHAlignAsString = "Left";
            this.comboFederalDistrict.DisplayLayout.Override.HeaderAppearance = appearance9;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.comboFederalDistrict.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.comboFederalDistrict.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboFederalDistrict.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboFederalDistrict.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboFederalDistrict.DisplayMember = "FullName";
            this.comboFederalDistrict.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboFederalDistrict.Location = new System.Drawing.Point(1, 21);
            this.comboFederalDistrict.Margin = new System.Windows.Forms.Padding(1);
            this.comboFederalDistrict.Name = "comboFederalDistrict";
            this.comboFederalDistrict.Size = new System.Drawing.Size(445, 22);
            this.comboFederalDistrict.TabIndex = 75;
            this.comboFederalDistrict.UseAppStyling = false;
            // 
            // ultraTabPageRegion
            // 
            this.ultraTabPageRegion.Controls.Add(this.ultraLabelRegion);
            this.ultraTabPageRegion.Controls.Add(this.lookupMultiRegion);
            this.ultraTabPageRegion.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageRegion.Name = "ultraTabPageRegion";
            this.ultraTabPageRegion.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelRegion
            // 
            this.ultraLabelRegion.Location = new System.Drawing.Point(2, 2);
            this.ultraLabelRegion.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelRegion.Name = "ultraLabelRegion";
            this.ultraLabelRegion.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelRegion.TabIndex = 75;
            this.ultraLabelRegion.Text = "Регионы:";
            // 
            // ultraTabPageInspection
            // 
            this.ultraTabPageInspection.Controls.Add(this.ultraLabel1);
            this.ultraTabPageInspection.Controls.Add(this.lookupMultiInspection);
            this.ultraTabPageInspection.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageInspection.Name = "ultraTabPageInspection";
            this.ultraTabPageInspection.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(2, 2);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(232, 17);
            this.ultraLabel1.TabIndex = 73;
            this.ultraLabel1.Text = "Инспекции:";
            // 
            // ultraCalendarOneDay
            // 
            this.ultraCalendarOneDay.DateButtons.Add(dateButton2);
            this.ultraCalendarOneDay.Location = new System.Drawing.Point(306, 35);
            this.ultraCalendarOneDay.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarOneDay.Name = "ultraCalendarOneDay";
            this.ultraCalendarOneDay.NonAutoSizeHeight = 21;
            this.ultraCalendarOneDay.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarOneDay.TabIndex = 92;
            this.ultraCalendarOneDay.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateOneDay
            // 
            this.labelDateOneDay.Location = new System.Drawing.Point(306, 5);
            this.labelDateOneDay.Name = "labelDateOneDay";
            this.labelDateOneDay.Size = new System.Drawing.Size(148, 29);
            this.labelDateOneDay.TabIndex = 93;
            this.labelDateOneDay.Text = "Дата, на которую формируется отчёт:";
            // 
            // lookupMultiRegion
            // 
            this.lookupMultiRegion.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiRegion.Name = "lookupMultiRegion";
            this.lookupMultiRegion.PopupHeight = 400;
            this.lookupMultiRegion.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiRegion.TabIndex = 1;
            // 
            // lookupMultiInspection
            // 
            this.lookupMultiInspection.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiInspection.Name = "lookupMultiInspection";
            this.lookupMultiInspection.PopupHeight = 400;
            this.lookupMultiInspection.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiInspection.TabIndex = 2;
            // 
            // MatchingRuleSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraCalendarOneDay);
            this.Controls.Add(this.labelDateOneDay);
            this.Controls.Add(this.labelAggragationLevelNO);
            this.Controls.Add(this.comboAggregateLevelNO);
            this.Controls.Add(this.ultraTabControlNO);
            this.Name = "MatchingRuleSearch";
            this.Size = new System.Drawing.Size(503, 116);
            this.Load += new System.EventHandler(this.MatchingRuleSearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).EndInit();
            this.ultraTabControlNO.ResumeLayout(false);
            this.ultraTabPageFederalDistrict.ResumeLayout(false);
            this.ultraTabPageFederalDistrict.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).EndInit();
            this.ultraTabPageRegion.ResumeLayout(false);
            this.ultraTabPageInspection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarOneDay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel labelAggragationLevelNO;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboAggregateLevelNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPageNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageCountry;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageFederalDistrict;
        private Infragistics.Win.Misc.UltraLabel ultraLabelFederalDistrict;
        private Infragistics.Win.UltraWinGrid.UltraCombo comboFederalDistrict;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageRegion;
        private Infragistics.Win.Misc.UltraLabel ultraLabelRegion;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiRegion;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageInspection;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiInspection;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarOneDay;
        private Infragistics.Win.Misc.UltraLabel labelDateOneDay;
    }
}
