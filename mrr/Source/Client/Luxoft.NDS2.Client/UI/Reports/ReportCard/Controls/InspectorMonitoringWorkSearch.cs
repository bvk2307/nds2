﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Infragistics.Win;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Reports.ReportsList;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class InspectorMonitoringWorkSearch : BaseControlSearch, IControlSearchCriteria
    {
        #region Переменные

        private string typeDateControl = String.Empty;
        private List<NalogPeriod> _nalogPeriods = new List<NalogPeriod>();
        private List<FederalDistrict> _federalDistricts = new List<FederalDistrict>();
        private List<RegionDetails> _regions = new List<RegionDetails>();
        private List<DictionaryNalogOrgan> _nalogOrgans = new List<DictionaryNalogOrgan>();
        public event EventHandler ChangeControlParameters;
        private string _reportKey = String.Empty;

        #endregion

        public InspectorMonitoringWorkSearch()
            : base()
        {
            InitializeComponent();
        }

        #region Реализация IControlSearchCriteria

        public override void SetupDependingFromReportKey(string reportKey)
        {
            _reportKey = reportKey;
            if (reportKey == ReportNames.MatchingRule)
            {
                typeDateControl = "NotDate";
                firstRowSelector.SelectedTab = firstRowSelector.Tabs[2];
            }
            else if (reportKey == ReportNames.CheckControlRatio)
            {
                typeDateControl = "NotDate";
                firstRowSelector.SelectedTab = firstRowSelector.Tabs[2];
            }
            else if (reportKey == ReportNames.CheckLogicControl)
            {
                typeDateControl = "DateCreateReport";
                firstRowSelector.SelectedTab = firstRowSelector.Tabs[1];
            }
            else
            {
                typeDateControl = "DateBeginEnd";
                firstRowSelector.SelectedTab = firstRowSelector.Tabs[0];
            }
        }

        public override string GetSearchCriteriaDescription()
        {
            string ret = String.Empty;
            if (comboReportingPeriod.SelectedIndex > -1)
            {
                ret = _nalogPeriods[comboReportingPeriod.SelectedIndex].NAME;
            }
            return ret;
        }

        public override QueryConditions GetQueryConditions()
        {
            QueryConditions queryCond = new QueryConditions();

            if (comboReportingPeriod.SelectedIndex > -1)
            {
                string period = _nalogPeriods[comboReportingPeriod.SelectedIndex].CODE;
                FilterQuery filterQueryPeriod = new FilterQuery();
                filterQueryPeriod.ColumnName = "NalogPeriod";
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                columnFilter.Value = period;
                filterQueryPeriod.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQueryPeriod);

                FilterQuery filterQueryYear = new FilterQuery();
                filterQueryYear.ColumnName = "Year";
                ColumnFilter columnFilterYear = new ColumnFilter();
                columnFilterYear.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                columnFilterYear.Value = _nalogPeriods[comboReportingPeriod.SelectedIndex].Year;
                filterQueryYear.Filtering.Add(columnFilterYear);
                queryCond.Filter.Add(filterQueryYear);
            }

            if (typeDateControl == "DateCreateReport")
            {
                if (ultraCalendarCreateReport.Value != null)
                {
                    DateTime dtBegin = (DateTime)ultraCalendarCreateReport.Value;
                    FilterQuery filterQueryDT = new FilterQuery();
                    filterQueryDT.ColumnName = "DateCreateReport";
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = dtBegin;
                    filterQueryDT.Filtering.Add(columnFilter);
                    queryCond.Filter.Add(filterQueryDT);
                }
            }
            else if (typeDateControl == "DateBeginEnd")
            {
                if (ultraCalendarBegin.Value != null)
                {
                    DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
                    FilterQuery filterQueryDTBegin = new FilterQuery();
                    filterQueryDTBegin.ColumnName = "DateBegin";
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo;
                    columnFilter.Value = dtBegin;
                    filterQueryDTBegin.Filtering.Add(columnFilter);
                    queryCond.Filter.Add(filterQueryDTBegin);
                }
                if (ultraCalendarEnd.Value != null)
                {
                    DateTime dtEnd = (DateTime)ultraCalendarEnd.Value;
                    FilterQuery filterQueryDTend = new FilterQuery();
                    filterQueryDTend.ColumnName = "DateEnd";
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo;
                    columnFilter.Value = dtEnd;
                    filterQueryDTend.Filtering.Add(columnFilter);
                    queryCond.Filter.Add(filterQueryDTend);
                }
            }

            FilterQuery filterQueryFD = new FilterQuery();
            filterQueryFD.ColumnName = "FederalDistrict";
            filterQueryFD.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
            List<FederalDistrict> fds = GetCurrentFederalDistrict();
            if (fds.Where(p => p.Check).Count() > 0)
            {
                foreach (FederalDistrict item in fds.Where(p => p.Check))
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Id;
                    filterQueryFD.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryFD);
            }
            else
            {
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThan;
                columnFilter.Value = -1;
            }

            FilterQuery filterQueryRegion = new FilterQuery();
            filterQueryRegion.ColumnName = "TaxPayerRegionCode";
            filterQueryRegion.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
            List<RegionDetails> regs = GetCurrentRegions();
            if (regs.Where(p => p.Check).Count() > 0)
            {
                foreach (RegionDetails item in regs.Where(p => p.Check))
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Code;
                    filterQueryRegion.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryRegion);
            }
            else
            {
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThan;
                columnFilter.Value = -1;
            }

            FilterQuery filterQueryNO = new FilterQuery();
            filterQueryNO.ColumnName = "NalogOrganCode";
            filterQueryNO.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
            List<DictionaryNalogOrgan> inspecs = GetCurrentInscpections();
            if (inspecs.Where(p => p.Check).Count() > 0)
            {
                foreach (DictionaryNalogOrgan item in inspecs.Where(p => p.Check))
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.S_CODE;
                    filterQueryNO.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryNO);
            }
            else
            {
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThan;
                columnFilter.Value = -1;
            }

            return queryCond;
        }

        public override bool IsCorrectParameters()
        {
            bool ret = true;

            DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
            DateTime dtEnd = (DateTime)ultraCalendarEnd.Value;

            if (dtBegin > dtEnd)
            {
                ret = false;
            }

            if (_federalDistricts.Where(p => p.Check).Count() == 0 ||
                _regions.Where(p => p.Check).Count() == 0 ||
                _nalogOrgans.Where(p => p.Check).Count() == 0)
            {
                ret = false;
            }

            return ret;
        }

        #endregion

        #region Установка данных

        public void SetNalogPeriods(List<NalogPeriod> nalogPeriods)
        {
            _nalogPeriods.Clear();
            _nalogPeriods.AddRange(nalogPeriods);
        }

        public void SetRegions(List<RegionDetails> regions)
        {
            _regions.Clear();
            _regions.AddRange(regions);
        }

        public void SetNalogOrgans(List<DictionaryNalogOrgan> nalogOrgans)
        {
            _nalogOrgans.Clear();
            _nalogOrgans.AddRange(nalogOrgans);
            foreach (DictionaryNalogOrgan item in _nalogOrgans)
            {
                if (string.IsNullOrWhiteSpace(item.S_PARENT_CODE))
                {
                    if (item.S_CODE != null && item.S_CODE.Length >= 2)
                    {
                        item.S_PARENT_CODE = item.S_CODE.Substring(0, 2) + "00";
                    }
                }
            }
        }

        public void SetFederalDistricts(List<FederalDistrict> federalDistricts)
        {
            _federalDistricts.Clear();
            _federalDistricts.AddRange(federalDistricts);
        }

        #endregion

        #region Инициализация

        private void InspectorMonitoringWorkSearch_Load(object sender, EventArgs e)
        {
            InitViewFederalDistrict();
            InitViewRegion();
            InitViewInspection();
            InitData();
        }

        public void InitData()
        {
            comboReportingPeriod.DataSource = _nalogPeriods;
            comboReportingPeriod.Refresh();

            DateTime dtNow = DateTime.Now.Date;

            var dtOfPreviousQuarter = QuarterCalculator
                .CalculatePreviousQuarterWithOffset(dtNow);

            comboReportingPeriod.SelectedIndex = -1;
            ultraCalendarBegin.Value = dtNow;
            ultraCalendarEnd.Value = dtNow;
            ultraCalendarCreateReport.Value = dtNow;

            int count = _nalogPeriods.Count;
            for (int i = -1; ++i < count; )
            {
                NalogPeriod item = _nalogPeriods[i];
                if (dtOfPreviousQuarter >= item.DateBegin && dtOfPreviousQuarter <= item.DateEnd)
                {
                    comboReportingPeriod.SelectedIndex = i;
                    break;
                }
            }

            foreach (RegionDetails itemRegion in _regions)
            {
                itemRegion.Check = true;
            }
            foreach (DictionaryNalogOrgan itemNO in _nalogOrgans)
            {
                itemNO.Check = true;
            }

            AllComboControlsEventRemove();

            ultraComboFederalDistrict.DataSource = _federalDistricts;
            ultraComboFederalDistrict.Refresh();

            CheckAllFederalDistrict();

            ultraComboRegion.DataSource = _regions;
            ultraComboRegion.Refresh();

            ucInspection.DataSource = _nalogOrgans;
            ucInspection.Refresh();

            CheckAllRegions();
            CheckAllInspection(100);

            GenerateChangeParameters();
            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();
        }

        private void InitViewFederalDistrict()
        {
            ultraComboFederalDistrict.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = ultraComboFederalDistrict.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            ultraComboFederalDistrict.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            ultraComboFederalDistrict.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            ultraComboFederalDistrict.CheckedListSettings.ListSeparator = " / ";
        }

        private void InitViewRegion()
        {
            ultraComboRegion.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = ultraComboRegion.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            ultraComboRegion.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            ultraComboRegion.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            ultraComboRegion.CheckedListSettings.ListSeparator = " / ";
        }

        private void InitViewInspection()
        {
            ucInspection.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = ucInspection.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            ucInspection.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            ucInspection.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            ucInspection.CheckedListSettings.ListSeparator = " / ";
        }

        #endregion

        #region Вспомогательные методы

        private void SetDateFromNalogPeriod(NalogPeriod itemCurrent)
        {
            if (itemCurrent != null)
            {
                ultraCalendarBegin.Value = GetDateBegin(DecodeNalogPeriod(itemCurrent.CODE), itemCurrent.Year);
                ultraCalendarEnd.Value = GetDateEnd(DecodeNalogPeriod(itemCurrent.CODE), itemCurrent.Year);
            }
        }

        private int DecodeNalogPeriod(string code)
        {
            int ret = 1;
            if (code == "21") { ret = 1; }
            else if (code == "22") { ret = 2; }
            else if (code == "23") { ret = 3; }
            else if (code == "24") { ret = 4; }
            return ret;
        }

        private DateTime GetDateBegin(int numKvartal, int year)
        {
            DateTime dtRet = DateTime.Now;
            if (numKvartal == 1)
            {
                dtRet = new DateTime(year, 4, 1);
            }
            else if (numKvartal == 2)
            {
                dtRet = new DateTime(year, 7, 1);
            }
            else if (numKvartal == 3)
            {
                dtRet = new DateTime(year, 10, 1);
            }
            else if (numKvartal == 4)
            {
                dtRet = new DateTime(year + 1, 1, 1);
            }
            return dtRet;
        }

        private DateTime GetDateEnd(int numKvartal, int year)
        {
            DateTime dtRet = DateTime.Now;
            if (numKvartal == 1)
            {
                dtRet = new DateTime(year, 7, 1);
            }
            else if (numKvartal == 2)
            {
                dtRet = new DateTime(year, 10, 1);
            }
            else if (numKvartal == 3)
            {
                dtRet = new DateTime(year + 1, 1, 1);
            }
            else if (numKvartal == 4)
            {
                dtRet = new DateTime(year + 1, 4, 1);
            }
            return dtRet;
        }

        private void DateTimeControlsEventAdd()
        {
            ultraCalendarBegin.ValueChanged += new EventHandler(ultraCalendarBegin_ValueChanged);
            ultraCalendarEnd.ValueChanged += new EventHandler(ultraCalendarEnd_ValueChanged);
            comboReportingPeriod.ValueChanged += new EventHandler(comboReportingPeriod_ValueChanged);

            ultraCalendarBegin.TextChanged += new EventHandler(ultraCalendarCommon_TextChanged);
            ultraCalendarEnd.TextChanged += new EventHandler(ultraCalendarCommon_TextChanged);
        }

        private void AllComboControlsEventRemove()
        {
            ultraComboFederalDistrict.ValueChanged -= new EventHandler(FederalDistrictOnValueChanged);
            ultraComboRegion.ValueChanged -= new EventHandler(RegionOnValueChanged);
            ucInspection.ValueChanged -= new EventHandler(InspectionOnValueChanged);
        }

        private void AllComboControlsEventAdd()
        {
            ultraComboFederalDistrict.ValueChanged += new EventHandler(FederalDistrictOnValueChanged);
            ultraComboRegion.ValueChanged += new EventHandler(RegionOnValueChanged);
            ucInspection.ValueChanged += new EventHandler(InspectionOnValueChanged);
        }

        private List<FederalDistrict> GetCurrentFederalDistrict()
        {
            List<FederalDistrict> fds;
            if (ultraComboFederalDistrict.DataSource != null)
            {
                fds = (List<FederalDistrict>)ultraComboFederalDistrict.DataSource;
            }
            else { fds = new List<FederalDistrict>(); }
            return fds;
        }

        private List<RegionDetails> GetCurrentRegions()
        {
            List<RegionDetails> regs;
            if (ultraComboRegion.DataSource != null)
            {
                regs = (List<RegionDetails>)ultraComboRegion.DataSource;
            }
            else { regs = new List<RegionDetails>(); }
            return regs;
        }

        private List<DictionaryNalogOrgan> GetCurrentInscpections()
        {
            List<DictionaryNalogOrgan> inspecs;
            if (ucInspection.DataSource != null)
            {
                inspecs = (List<DictionaryNalogOrgan>)ucInspection.DataSource;
            }
            else { inspecs = new List<DictionaryNalogOrgan>(); }
            return inspecs;
        }

        private void CheckAllFederalDistrict()
        {
            ICheckedItemList itemListFederal = (ICheckedItemList)ultraComboFederalDistrict;
            for (int i = 0; i < ultraComboFederalDistrict.Rows.Count; i++)
            {
                itemListFederal.SetCheckState(i, CheckState.Checked);
            }
        }

        private void CheckAllRegions()
        {
            ICheckedItemList itemListRegion = (ICheckedItemList)ultraComboRegion;
            for (int i = 0; i < ultraComboRegion.Rows.Count; i++)
            {
                itemListRegion.SetCheckState(i, CheckState.Checked);
            }
        }

        private void CheckAllInspection(int maxFirstCount)
        {
            ICheckedItemList itemListNalogOrgan = (ICheckedItemList)ucInspection;

            int count = ucInspection.Rows.Count;
            if (count > maxFirstCount)
            {
                count = maxFirstCount;
            }

            for (int i = 0; i < count; i++)
            {
                itemListNalogOrgan.SetCheckState(i, CheckState.Checked);
            }
        }

        #endregion

        #region События

        private void FederalDistrictOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
            if (sender == ultraComboFederalDistrict)
            {
                isChangeRegionFromFederal = true;

                LoadRegionsOfFederalDistrict();
                LoadInspectionsOfRegion();

                CheckRegionsOfFederalDistrict();

                isChangeRegionFromFederal = false;
            }
        }

        private void LoadRegionsOfFederalDistrict()
        {
            List<RegionDetails> regionsOfFederalDistricts = new List<RegionDetails>();
            foreach (FederalDistrict itemFD in GetCurrentFederalDistrict().Where(p=>p.Check))
            {
                regionsOfFederalDistricts.AddRange(_regions.Where(p => itemFD.RegionCodes.Contains(p.Code)).ToList());
            }
            foreach (RegionDetails itemRegion in regionsOfFederalDistricts.Where(p=>!p.Check))
            {
                itemRegion.Check = true;
            }
            ultraComboRegion.DataSource = regionsOfFederalDistricts.OrderBy(p => p.FullName).ToList();
            ultraComboRegion.Refresh();
        }

        private void CheckRegionsOfFederalDistrict()
        {
            ICheckedItemList itemListRegion = (ICheckedItemList)ultraComboRegion;
            foreach (FederalDistrict itemFD in _federalDistricts)
            {
                for (int i = 0; i < ultraComboRegion.Rows.Count; i++)
                {
                    RegionDetails regionFind = (RegionDetails)ultraComboRegion.Rows[i].ListObject;
                    if (itemFD.RegionCodes.Contains(regionFind.Code))
                    {
                        if (itemFD.Check)
                        {
                            itemListRegion.SetCheckState(i, CheckState.Checked);
                        }
                        else
                        {
                            itemListRegion.SetCheckState(i, CheckState.Unchecked);
                        }
                    }
                }
            }
        }

        private void RegionOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
            if (sender == ultraComboRegion)
            {
                isChangeInspectionFromRegion = true;

                LoadInspectionsOfRegion();

                isChangeInspectionFromRegion = false;
            }
        }

        private void LoadInspectionsOfRegion()
        {
            List<DictionaryNalogOrgan> inspectionsOfRegion = new List<DictionaryNalogOrgan>();
            List<RegionDetails> regions = GetCurrentRegions();
            foreach (RegionDetails itemRegion in regions.Where(p => p.Check))
            {
                inspectionsOfRegion.AddRange(_nalogOrgans.Where(p => p.S_PARENT_CODE != null && p.S_PARENT_CODE == (itemRegion.Code + "00")).ToList());
            }
            foreach (DictionaryNalogOrgan itemNO in inspectionsOfRegion.Where(p=>!p.Check))
            {
                itemNO.Check = true;
            }
            ucInspection.DataSource = inspectionsOfRegion.OrderBy(p => p.FullName).ToList();
            ucInspection.Refresh();
        }

        private void CheckInspectionOfRegion()
        {
            ICheckedItemList itemListNalogOrgan = (ICheckedItemList)ucInspection;
            foreach (RegionDetails itemRegion in _regions)
            {
                for (int i = 0; i < ucInspection.Rows.Count; i++)
                {
                    DictionaryNalogOrgan nalogOrganFind = (DictionaryNalogOrgan)ucInspection.Rows[i].ListObject;
                    if (nalogOrganFind.S_PARENT_CODE != null && nalogOrganFind.S_PARENT_CODE == (itemRegion.Code + "00"))
                    {
                        if (itemRegion.Check)
                        {
                            itemListNalogOrgan.SetCheckState(i, CheckState.Checked);
                        }
                        else
                        {
                            itemListNalogOrgan.SetCheckState(i, CheckState.Unchecked);
                        }
                    }
                }
            }
        }

        private void CheckAllRegionForFederalDistrict()
        {
            if (!isChangeRegionFromFederal)
            {
                ultraComboFederalDistrict.ValueChanged -= new EventHandler(FederalDistrictOnValueChanged);

                ICheckedItemList itemListFederal = (ICheckedItemList)ultraComboFederalDistrict;
                for (int i = 0; i < ultraComboFederalDistrict.Rows.Count; i++)
                {
                    FederalDistrict federalCurrent = (FederalDistrict)ultraComboFederalDistrict.Rows[i].ListObject;
                    int countChecked = _regions.Where(p => federalCurrent.RegionCodes.Contains(p.Code) && p.Check).Count();
                    if (countChecked > 0)
                    {
                        itemListFederal.SetCheckState(i, CheckState.Checked);
                    }
                    else
                    {
                        itemListFederal.SetCheckState(i, CheckState.Unchecked);
                    }
                }

                ultraComboFederalDistrict.ValueChanged += new EventHandler(FederalDistrictOnValueChanged);
            }
        }

        private bool isChangeRegionFromFederal = false;
        private bool isChangeInspectionFromRegion = false;

        private void InspectionOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
            //--- отключил
            return;

            if (sender == ucInspection && !isChangeInspectionFromRegion)
            {
                ultraComboRegion.ValueChanged -= new EventHandler(RegionOnValueChanged);

                ICheckedItemList itemListRegion = (ICheckedItemList)ultraComboRegion;
                for (int i = 0; i < ultraComboRegion.Rows.Count; i++)
                {
                    RegionDetails regionCurrent = (RegionDetails)ultraComboRegion.Rows[i].ListObject;
                    int countAny = _nalogOrgans.Where(p => p.S_PARENT_CODE == (regionCurrent.Code + "00")).Count();
                    if (countAny > 0)
                    {
                        int countChecked = _nalogOrgans.Where(p => p.S_PARENT_CODE == (regionCurrent.Code + "00") && p.Check).Count();
                        if (countChecked > 0)
                        {
                            itemListRegion.SetCheckState(i, CheckState.Checked);
                        }
                        else
                        {
                            itemListRegion.SetCheckState(i, CheckState.Unchecked);
                        }
                    }
                }

                CheckAllRegionForFederalDistrict();

                ultraComboRegion.ValueChanged += new EventHandler(RegionOnValueChanged);
            }
        }

        private void ultraCalendarBegin_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarEnd_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboReportingPeriod_ValueChanged(object sender, EventArgs e)
        {
            if (comboReportingPeriod.SelectedIndex > -1)
            {
                NalogPeriod itemCurrent = _nalogPeriods[comboReportingPeriod.SelectedIndex];
                SetDateFromNalogPeriod(itemCurrent);
            }
            GenerateChangeParameters();
        }

        private void ultraCalendarCommon_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        #endregion
    }
}
