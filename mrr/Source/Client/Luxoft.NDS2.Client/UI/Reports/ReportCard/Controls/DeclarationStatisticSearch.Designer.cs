﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class DeclarationStatisticSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Code", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FederalCode", 4);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.comboTypeGenerate = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelTypeGenerate = new Infragistics.Win.Misc.UltraLabel();
            this.labelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.comboRegim = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraCalendarEnd = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateBegin = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateEnd = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboRegion = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.comboReportingPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeGenerate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboRegim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboReportingPeriod)).BeginInit();
            this.SuspendLayout();
            // 
            // comboTypeGenerate
            // 
            this.comboTypeGenerate.DisplayMember = "NAME";
            this.comboTypeGenerate.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTypeGenerate.Location = new System.Drawing.Point(153, 30);
            this.comboTypeGenerate.Margin = new System.Windows.Forms.Padding(1);
            this.comboTypeGenerate.Name = "comboTypeGenerate";
            this.comboTypeGenerate.Size = new System.Drawing.Size(198, 21);
            this.comboTypeGenerate.TabIndex = 22;
            // 
            // labelTypeGenerate
            // 
            this.labelTypeGenerate.Location = new System.Drawing.Point(153, 12);
            this.labelTypeGenerate.Margin = new System.Windows.Forms.Padding(1);
            this.labelTypeGenerate.Name = "labelTypeGenerate";
            this.labelTypeGenerate.Size = new System.Drawing.Size(169, 17);
            this.labelTypeGenerate.TabIndex = 23;
            this.labelTypeGenerate.Text = "Область формирования:";
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(153, 65);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(100, 17);
            this.labelRegion.TabIndex = 25;
            this.labelRegion.Text = "Регион:";
            // 
            // comboRegim
            // 
            this.comboRegim.DisplayMember = "NAME";
            this.comboRegim.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboRegim.Location = new System.Drawing.Point(362, 30);
            this.comboRegim.Margin = new System.Windows.Forms.Padding(1);
            this.comboRegim.Name = "comboRegim";
            this.comboRegim.Size = new System.Drawing.Size(239, 21);
            this.comboRegim.TabIndex = 26;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(362, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 17);
            this.ultraLabel1.TabIndex = 27;
            this.ultraLabel1.Text = "Режим:";
            // 
            // ultraCalendarBegin
            // 
            this.ultraCalendarBegin.DateButtons.Add(dateButton1);
            this.ultraCalendarBegin.Location = new System.Drawing.Point(617, 30);
            this.ultraCalendarBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarBegin.Name = "ultraCalendarBegin";
            this.ultraCalendarBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarBegin.TabIndex = 28;
            this.ultraCalendarBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // ultraCalendarEnd
            // 
            this.ultraCalendarEnd.DateButtons.Add(dateButton2);
            this.ultraCalendarEnd.Location = new System.Drawing.Point(617, 83);
            this.ultraCalendarEnd.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarEnd.Name = "ultraCalendarEnd";
            this.ultraCalendarEnd.NonAutoSizeHeight = 21;
            this.ultraCalendarEnd.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarEnd.TabIndex = 29;
            this.ultraCalendarEnd.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateBegin
            // 
            this.labelDateBegin.Location = new System.Drawing.Point(617, 12);
            this.labelDateBegin.Name = "labelDateBegin";
            this.labelDateBegin.Size = new System.Drawing.Size(169, 17);
            this.labelDateBegin.TabIndex = 30;
            this.labelDateBegin.Text = "Дата формирования отчета:";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.Location = new System.Drawing.Point(617, 65);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(169, 17);
            this.labelDateEnd.TabIndex = 31;
            this.labelDateEnd.Text = "Дата окончания периода:";
            // 
            // ultraComboRegion
            // 
            this.ultraComboRegion.CheckedListSettings.CheckStateMember = "";
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraComboRegion.DisplayLayout.Appearance = appearance1;
            ultraGridColumn13.Header.Caption = "Название";
            ultraGridColumn13.Header.VisiblePosition = 2;
            ultraGridColumn13.Width = 440;
            ultraGridColumn14.Header.Caption = "Код";
            ultraGridColumn14.Header.VisiblePosition = 1;
            ultraGridColumn14.Width = 50;
            ultraGridColumn15.Header.VisiblePosition = 3;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn15.TabIndex = 1;
            ultraGridColumn15.Width = 100;
            ultraGridColumn16.DataType = typeof(bool);
            ultraGridColumn16.Header.Caption = "";
            ultraGridColumn16.Header.VisiblePosition = 0;
            ultraGridColumn16.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn16.TabIndex = 0;
            ultraGridColumn16.Width = 30;
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn5.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn5});
            this.ultraComboRegion.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraComboRegion.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraComboRegion.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboRegion.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraComboRegion.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboRegion.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraComboRegion.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraComboRegion.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraComboRegion.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraComboRegion.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraComboRegion.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraComboRegion.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraComboRegion.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraComboRegion.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraComboRegion.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraComboRegion.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraComboRegion.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraComboRegion.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraComboRegion.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraComboRegion.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraComboRegion.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraComboRegion.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraComboRegion.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraComboRegion.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraComboRegion.DisplayMember = "FullName";
            this.ultraComboRegion.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.ultraComboRegion.Location = new System.Drawing.Point(153, 83);
            this.ultraComboRegion.Margin = new System.Windows.Forms.Padding(1);
            this.ultraComboRegion.Name = "ultraComboRegion";
            this.ultraComboRegion.Size = new System.Drawing.Size(448, 22);
            this.ultraComboRegion.TabIndex = 32;
            this.ultraComboRegion.UseAppStyling = false;
            // 
            // comboReportingPeriod
            // 
            this.comboReportingPeriod.DisplayMember = "NAME";
            this.comboReportingPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboReportingPeriod.Location = new System.Drawing.Point(8, 30);
            this.comboReportingPeriod.Margin = new System.Windows.Forms.Padding(1);
            this.comboReportingPeriod.Name = "comboReportingPeriod";
            this.comboReportingPeriod.Size = new System.Drawing.Size(134, 21);
            this.comboReportingPeriod.TabIndex = 33;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(8, 12);
            this.ultraLabel2.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(134, 17);
            this.ultraLabel2.TabIndex = 34;
            this.ultraLabel2.Text = "Налоговый период:";
            // 
            // DeclarationStatisticSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.comboReportingPeriod);
            this.Controls.Add(this.ultraComboRegion);
            this.Controls.Add(this.labelDateEnd);
            this.Controls.Add(this.labelDateBegin);
            this.Controls.Add(this.ultraCalendarEnd);
            this.Controls.Add(this.ultraCalendarBegin);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.comboRegim);
            this.Controls.Add(this.labelRegion);
            this.Controls.Add(this.labelTypeGenerate);
            this.Controls.Add(this.comboTypeGenerate);
            this.Name = "DeclarationStatisticSearch";
            this.Size = new System.Drawing.Size(806, 119);
            this.Load += new System.EventHandler(this.DeclarationStatisticSearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeGenerate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboRegim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboReportingPeriod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTypeGenerate;
        private Infragistics.Win.Misc.UltraLabel labelTypeGenerate;
        private Infragistics.Win.Misc.UltraLabel labelRegion;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboRegim;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarBegin;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarEnd;
        private Infragistics.Win.Misc.UltraLabel labelDateBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateEnd;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraComboRegion;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboReportingPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
    }
}
