﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Infragistics.Win;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard;
using Luxoft.NDS2.Common.Contracts.Services;
using ultraGrid = Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class DynamicDeclarationStatisticSearch : BaseControlSearch, IControlSearchCriteria
    {
        #region Переменные

        private List<AggregateByNalogOrgan> _aggregateByNalogOrgans = new List<AggregateByNalogOrgan>();
        private List<AggregateByTime> _aggregateByTimes = new List<AggregateByTime>();
        private List<TypeReport> _typeReports = new List<TypeReport>();
        private List<DictionaryNalogPeriods> _dictNalogPeriods = new List<DictionaryNalogPeriods>();
        private List<DynamicParamDeclStat> _dynamicParameters = new List<DynamicParamDeclStat>();
        private List<int> _fiscalYears = new List<int>();
        private List<MonthInfo> _month = new List<MonthInfo>();
        private List<int> _years = new List<int>();
        private List<FederalDistrict> _federalDistricts = new List<FederalDistrict>();
        private IDataService _dataService;
        private Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel> regionSelectPresenter;
        private Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel> taxAuthorityPresenter;

        #endregion

        public DynamicDeclarationStatisticSearch(IDataService dataService)
            : base()
        {
            InitializeComponent();

            _dataService = dataService;
        }

        #region Реализация IControlSearchCriteria

        public override string GetSearchCriteriaDescription()
        {
            DictionaryNalogPeriods currentNalogPeriod = GetCurrentNalogPeriods();
            AggregateByTime aggregateByTime = GetCurrentAggregateByTime();

            DateTime? dtBegin = null;
            DateTime? dtEnd = null;
            if (aggregateByTime != null)
            {
                if (aggregateByTime.Type == AggregateByTimeType.Day)
                {
                    dtBegin = (DateTime)ultraCalendarDayBegin.Value;
                    dtEnd = (DateTime)ultraCalendarDayEnd.Value;
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Week)
                {
                    dtBegin = (DateTime)ultraCalendarWeekBegin.Value;
                    dtEnd = (DateTime)ultraCalendarWeekEnd.Value;
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Month)
                {
                    if (comboYearBegin.Value != null && comboMonthBegin.Value != null)
                    {
                        int year = (int)comboYearBegin.Value;
                        int month = (int)comboMonthBegin.Value;
                        dtBegin = new DateTime(year, month, 1);
                    }
                    if (comboYearEnd.Value != null && comboMonthEnd.Value != null)
                    {
                        int year = (int)comboYearEnd.Value;
                        int month = (int)comboMonthEnd.Value;
                        dtEnd = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
           
            if (currentNalogPeriod != null)
            {
                sb.Append(" за ");
                sb.Append(GetNalogPeriodDescription(currentNalogPeriod));
            }

            if (dtBegin != null || dtEnd != null)
            {
                sb.Append(" за период с ");
                sb.Append(FormatDateTime(dtBegin, "."));
                sb.Append(" по ");
                sb.Append(FormatDateTime(dtEnd, "."));
            }
            if (aggregateByTime != null)
            {
                sb.Append(" (");
                sb.Append(GetAggreageteTimeDescription(aggregateByTime));
                sb.Append(")");
            }
            return sb.ToString();
        }

        public  string AddNalogPeriodToSearchCriteriaDescription()
        {
            StringBuilder sb = new StringBuilder();

            AggregateByNalogOrgan aggregateByNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateByNalogOrgan != null)
            {
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Country)
                {
                    sb.Append(" по стране");
                }
                else if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    sb.Append(" по федеральным округам ");
                    bool isFirst = true;
                    foreach (FederalDistrict itemFD in GetCheckedFederalDistrict())
                    {
                        if (!isFirst)
                            sb.Append(", ");
                        else isFirst = false;
                        sb.Append(GetFederalDescriptionShort(itemFD.Description));
                    }
                }
                else if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    sb.Append(" по регионам ");
                    bool isFirst = true;
                    foreach (KeyValuePair<string, RegionModel> itemRegion in regionSelectPresenter.SelectedItems)
                    {
                        if (!isFirst)
                            sb.Append(", ");
                        else isFirst = false;
                        sb.Append(itemRegion.Key);
                    }
                }
                else if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    sb.Append(" по ИФНС № ");
                    bool isFirst = true;
                    foreach (KeyValuePair<string, TaxAuthorityModel> itemSoun in taxAuthorityPresenter.SelectedItems)
                    {
                        if (!isFirst)
                            sb.Append(", ");
                        else isFirst = false;
                        sb.Append(itemSoun.Key);
                    }
                }
            }
            return sb.ToString();
        }

        private string GetFederalDescriptionShort(string desc)
        {
            return desc.Replace("федеральный округ", String.Empty).Trim();
        }

        private string GetAggreageteTimeDescription(AggregateByTime aggregateByTime)
        {
            string ret = String.Empty;
            if (aggregateByTime != null)
            {
                Dictionary<AggregateByTimeType, string> dictDescription = new Dictionary<AggregateByTimeType, string>();
                dictDescription.Add(AggregateByTimeType.Day, "по дням");
                dictDescription.Add(AggregateByTimeType.Week, "понедельно");
                dictDescription.Add(AggregateByTimeType.Month, "помесячно");

                if (dictDescription.ContainsKey(aggregateByTime.Type))
                {
                    ret = dictDescription[aggregateByTime.Type];
                }
            }
            return ret;
        }

        private string GetNalogPeriodDescription(DictionaryNalogPeriods nalogPeriod)
        {
            string ret = String.Empty;
            if (nalogPeriod != null)
            {
                Dictionary<string, string> dictDescription = new Dictionary<string, string>();
                dictDescription.Add("21", "1-ый квартал");
                dictDescription.Add("22", "2-ой квартал");
                dictDescription.Add("23", "3-ий квартал");
                dictDescription.Add("24", "4-ый квартал");

                if (dictDescription.ContainsKey(nalogPeriod.CODE))
                {
                    ret = dictDescription[nalogPeriod.CODE];
                }
                else
                {
                    ret = nalogPeriod.FullName;
                }
            }
            return ret;
        }

        public override QueryConditions GetQueryConditions()
        {
            QueryConditions queryCond = new QueryConditions();

            AggregateByNalogOrgan aggregateByNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateByNalogOrgan != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_AggregateByNalogOrgan, (int)aggregateByNalogOrgan.Type);
            }

            AggregateByTime aggregateByTime = GetCurrentAggregateByTime();
            if (aggregateByTime != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_AggregateByTime, (int)aggregateByTime.Type);
            }

            TypeReport typeReport = GetCurrentTypeReport();
            if (typeReport != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_TypeReport, (int)typeReport.Type);
            }

            DictionaryNalogPeriods dictNalogPeriod = GetCurrentNalogPeriods();
            if (dictNalogPeriod != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_NalogPeriod, dictNalogPeriod.CODE);
            }

            int? fiscalYear = GetCurrentFiscalYear();
            if (fiscalYear != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_FiscalYear, fiscalYear);
            }

            if (aggregateByTime != null)
            {
                if (aggregateByTime.Type == AggregateByTimeType.Day)
                {
                    if (ultraCalendarDayBegin.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfDate(queryCond, TaskParamCriteria.DDS_DateBeginDay, (DateTime)ultraCalendarDayBegin.Value);
                    }
                    if (ultraCalendarDayEnd.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfDate(queryCond, TaskParamCriteria.DDS_DateEndDay, (DateTime)ultraCalendarDayEnd.Value);
                    }
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Week)
                {
                    if (ultraCalendarWeekBegin.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfDate(queryCond, TaskParamCriteria.DDS_DateBeginWeek, (DateTime)ultraCalendarWeekBegin.Value);
                    }
                    if (ultraCalendarWeekEnd.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfDate(queryCond, TaskParamCriteria.DDS_DateEndWeek, (DateTime)ultraCalendarWeekEnd.Value);
                    }
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Month)
                {
                    if (comboYearBegin.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_YearBegin, (int)comboYearBegin.Value);
                    }
                    if (comboMonthBegin.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_MonthBegin, (int)comboMonthBegin.Value);
                    }
                    if (comboYearEnd.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_YearEnd, (int)comboYearEnd.Value);
                    }
                    if (comboMonthEnd.Value != null)
                    {
                        SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.DDS_MonthEnd, (int)comboMonthEnd.Value);
                    }
                }
            }

            AddFilterDynamicParameters(queryCond);
            if (aggregateByNalogOrgan != null)
            {
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    SearchCriteriaHelper.AddFilterFederalDistrict(queryCond, TaskParamCriteria.DDS_FederalDistrict, GetCheckedFederalDistrict());
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    SearchCriteriaHelper.AddFilterRegion(queryCond, TaskParamCriteria.DDS_TaxPayerRegionCode, regionSelectPresenter);
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    SearchCriteriaHelper.AddFilterNalogOrgan(queryCond, TaskParamCriteria.DDS_NalogOrganCode, taxAuthorityPresenter);
                }
            }

            return queryCond;
        }

        private void AddFilterDynamicParameters(QueryConditions queryCond)
        {
            List<DynamicParamDeclStat> checkedDynamicParamters = GetCheckedDynamicParameters();
            if (checkedDynamicParamters.Count() > 0)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.ColumnName = TaskParamCriteria.DDS_DynamicParameters;
                filterQuery.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                foreach (DynamicParamDeclStat item in checkedDynamicParamters)
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Id;
                    filterQuery.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQuery);
            }
        }

        public override bool IsCorrectParameters()
        {
            bool ret = true;
            if (GetCheckedDynamicParameters().Count() == 0)
            {
                ret = false;
            }
            return ret;
        }

        private void CheckCorrectParameters()
        {
            string errorMessage = String.Empty;
            if (GetCheckedDynamicParameters().Count() == 0)
            {
                errorMessage = "Не выбран ни один показатель, отображаемый в отчете";
            }
            labelMessage.Text = errorMessage;
        }

        #endregion

        #region Установка данных

        public void SetAggregateByNalogOrgan(List<AggregateByNalogOrgan> aggregateByNalogOrgans)
        {
            _aggregateByNalogOrgans.Clear();
            _aggregateByNalogOrgans.AddRange(aggregateByNalogOrgans);
        }

        public void SetFederalDistricts(List<FederalDistrict> federalDistricts)
        {
            _federalDistricts.AddRange(federalDistricts);
        }

        public void SetAggregateByTime(List<AggregateByTime> aggregateByTimes)
        {
            _aggregateByTimes.Clear();
            _aggregateByTimes.AddRange(aggregateByTimes);
        }

        public void SetTypeReports(List<TypeReport> typeReports)
        {
            _typeReports.Clear();
            _typeReports.AddRange(typeReports);
        }

        public void SetDictionaryNalogPeriods(List<DictionaryNalogPeriods> dictNalogPeriods)
        {
            _dictNalogPeriods.Clear();
            _dictNalogPeriods.AddRange(dictNalogPeriods);
        }

        public void SetFiscalYears(List<int> fiscalYears)
        {
            _fiscalYears.Clear();
            _fiscalYears.AddRange(fiscalYears);
        }

        public void SetYears(List<int> fiscalYears)
        {
            _years.Clear();
            _years.AddRange(fiscalYears);
        }

        public void SetDynamicParameters(List<DynamicParamDeclStat> dynamicParameters)
        {
            _dynamicParameters.Clear();
            _dynamicParameters.AddRange(dynamicParameters);
        }

        #endregion

        #region Инициализация

        private void DynamicDeclarationStatisticSearch_Load(object sender, EventArgs e)
        {
            InitViewFederalDistrict();
            InitViewDynamicParameters();
            InitData();
            CheckCorrectParameters();
        }

        public void InitData()
        {
            labelMessage.Text = String.Empty;

            comboAggregateLevelNO.DataSource = _aggregateByNalogOrgans;
            comboAggregateLevelTime.DataSource = _aggregateByTimes;
            comboTypeReport.DataSource = _typeReports;
            comboNalogPeriod.DataSource = _dictNalogPeriods;
            comboFiscalYear.DataSource = _fiscalYears;
            comboDynamicParameter.DataSource = _dynamicParameters;
            comboFederalDistrict.DataSource = _federalDistricts;

            InitMonth();
            comboMonthBegin.DataSource = _month;
            comboMonthEnd.DataSource = _month;
            comboYearBegin.DataSource = _years;
            comboYearEnd.DataSource = _years;

            ILookupDataSource lookupDataSource = LookupDataSource();
            regionSelectPresenter = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel>(
                lookupMultiRegion,
                (args) => lookupDataSource.Search<RegionModel>(args));

            taxAuthorityPresenter =
                new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel>(
                    lookupMultiInspection,
                    (args) => lookupDataSource.Search<TaxAuthorityModel>(args));

            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            if (comboAggregateLevelNO.Items.Count > 0)
                comboAggregateLevelNO.SelectedIndex = 0;
            else
                comboAggregateLevelNO.SelectedIndex = -1;

            if (comboAggregateLevelTime.Items.Count > 0)
                comboAggregateLevelTime.SelectedIndex = 0;
            else
                comboAggregateLevelTime.SelectedIndex = -1;

            if (comboTypeReport.Items.Count > 0)
                comboTypeReport.SelectedIndex = 0;
            else
                comboTypeReport.SelectedIndex = -1;

            if (comboNalogPeriod.Items.Count > 0)
                comboNalogPeriod.SelectedIndex = 0;
            else
                comboNalogPeriod.SelectedIndex = -1;

            if (comboFiscalYear.Items.Count > 0)
                comboFiscalYear.SelectedIndex = 0;
            else
                comboFiscalYear.SelectedIndex = -1;

            DateTime dtDefault = DateTime.Now.Date.AddDays(-1);
            ultraCalendarDayBegin.Value = dtDefault;
            ultraCalendarDayEnd.Value = dtDefault;
            ultraCalendarWeekBegin.Value = dtDefault;
            ultraCalendarWeekEnd.Value = dtDefault;

            if (comboYearBegin.Items.Count > 0)
                comboYearBegin.SelectedIndex = 0;
            else
                comboYearBegin.SelectedIndex = -1;

            if (comboYearEnd.Items.Count > 0)
                comboYearEnd.SelectedIndex = 0;
            else
                comboYearEnd.SelectedIndex = -1;

            if (comboMonthBegin.Items.Count > 0)
                comboMonthBegin.SelectedIndex = 0;
            else
                comboMonthBegin.SelectedIndex = -1;

            if (comboMonthEnd.Items.Count > 0)
                comboMonthEnd.SelectedIndex = 0;
            else
                comboMonthEnd.SelectedIndex = -1;
        }

        private ILookupDataSource LookupDataSource()
        {
            return new FilterEditorDataProvider(_dataService);
        }

        private void InitViewDynamicParameters()
        {
            this.comboDynamicParameter.CheckedListSettings.CheckStateMember = "Check";
            ultraGrid.UltraGridColumn column = this.comboDynamicParameter.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            this.comboDynamicParameter.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            this.comboDynamicParameter.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            this.comboDynamicParameter.CheckedListSettings.ListSeparator = " / ";

            column.Style = ultraGrid.ColumnStyle.CheckBox;
            column.Header.CheckBoxAlignment = ultraGrid.HeaderCheckBoxAlignment.Left;
            column.Header.CheckBoxSynchronization = ultraGrid.HeaderCheckBoxSynchronization.RowsCollection;
            column.Header.CheckBoxVisibility = ultraGrid.HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            column.SortIndicator = ultraGrid.SortIndicator.None;
        }

        private void InitMonth()
        {
            _month.Clear();
            _month.Add(new MonthInfo() { Id = 1, Name = "январь" });
            _month.Add(new MonthInfo() { Id = 2, Name = "февраль" });
            _month.Add(new MonthInfo() { Id = 3, Name = "март" });
            _month.Add(new MonthInfo() { Id = 4, Name = "апрель" });
            _month.Add(new MonthInfo() { Id = 5, Name = "май" });
            _month.Add(new MonthInfo() { Id = 6, Name = "июнь" });
            _month.Add(new MonthInfo() { Id = 7, Name = "июль" });
            _month.Add(new MonthInfo() { Id = 8, Name = "август" });
            _month.Add(new MonthInfo() { Id = 9, Name = "сентябрь" });
            _month.Add(new MonthInfo() { Id = 10, Name = "октябрь" });
            _month.Add(new MonthInfo() { Id = 11, Name = "ноябрь" });
            _month.Add(new MonthInfo() { Id = 12, Name = "декабрь" });
        }

        private void InitViewFederalDistrict()
        {
            this.comboFederalDistrict.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = this.comboFederalDistrict.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            this.comboFederalDistrict.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            this.comboFederalDistrict.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            this.comboFederalDistrict.CheckedListSettings.ListSeparator = " / ";
        }

        #endregion

        #region Вспомогательные методы

        private AggregateByNalogOrgan GetCurrentAggregateByNalogOrgan()
        {
            AggregateByNalogOrgan aggregateByNalogOrgan = null;
            int ind = comboAggregateLevelNO.SelectedIndex;
            if (ind > -1 && ind < _aggregateByNalogOrgans.Count)
            {
                aggregateByNalogOrgan = _aggregateByNalogOrgans[ind];
            }
            return aggregateByNalogOrgan;
        }

        private AggregateByTime GetCurrentAggregateByTime()
        {
            AggregateByTime aggregateByTime = null;
            int ind = comboAggregateLevelTime.SelectedIndex;
            if (ind > -1 && ind < _aggregateByTimes.Count)
            {
                aggregateByTime = _aggregateByTimes[ind];
            }
            return aggregateByTime;
        }

        private TypeReport GetCurrentTypeReport()
        {
            TypeReport typeReport = null;
            int ind = comboTypeReport.SelectedIndex;
            if (ind > -1 && ind < _typeReports.Count)
            {
                typeReport = _typeReports[ind];
            }
            return typeReport;
        }

        private DictionaryNalogPeriods GetCurrentNalogPeriods()
        {
            DictionaryNalogPeriods dictionaryNalogPeriods = null;
            int ind = comboNalogPeriod.SelectedIndex;
            if (ind > -1 && ind < _dictNalogPeriods.Count)
            {
                dictionaryNalogPeriods = _dictNalogPeriods[ind];
            }
            return dictionaryNalogPeriods;
        }

        private int? GetCurrentFiscalYear()
        {
            int? fiscalYear = null;
            int ind = comboFiscalYear.SelectedIndex;
            if (ind > -1 && ind < comboFiscalYear.Items.Count)
            {
                fiscalYear = (int)comboFiscalYear.Items[ind].DataValue;
            }
            return fiscalYear;
        }

        private List<DynamicParamDeclStat> GetCheckedDynamicParameters()
        {
            List<DynamicParamDeclStat> rets;
            if (comboDynamicParameter.DataSource != null)
            {
                rets = (List<DynamicParamDeclStat>)comboDynamicParameter.DataSource;
            }
            else { rets = new List<DynamicParamDeclStat>(); }
            return rets.Where(p => p.Check).ToList();
        }

        private void AllComboControlsEventAdd()
        {
            this.comboAggregateLevelNO.ValueChanged += new EventHandler(this.comboAggregateLevelNO_ValueChanged);
            this.comboAggregateLevelTime.ValueChanged += new EventHandler(this.comboAggregateLevelTime_ValueChanged);
            this.comboTypeReport.ValueChanged += new EventHandler(this.comboTypeReport_ValueChanged);
            this.comboDynamicParameter.ValueChanged += new EventHandler(this.comboDynamicParameter_ValueChanged);
            this.comboNalogPeriod.ValueChanged += new EventHandler(this.comboNalogPeriod_ValueChanged);
            this.comboFiscalYear.ValueChanged += new EventHandler(this.comboFiscalYear_ValueChanged);
            this.comboYearBegin.ValueChanged += new System.EventHandler(this.comboYearAndMonth_ValueChanged);
            this.comboYearEnd.ValueChanged += new System.EventHandler(this.comboYearAndMonth_ValueChanged);
            this.comboMonthBegin.ValueChanged += new System.EventHandler(this.comboYearAndMonth_ValueChanged);
            this.comboMonthEnd.ValueChanged += new System.EventHandler(this.comboYearAndMonth_ValueChanged);
            this.comboFederalDistrict.ValueChanged += new EventHandler(this.FederalDistrictOnValueChanged);
            this.lookupMultiRegion.OnSave += lookupMulti_OnSave;
            this.lookupMultiInspection.OnSave += lookupMulti_OnSave;
        }

        private void DateTimeControlsEventAdd()
        {
            this.ultraCalendarDayBegin.ValueChanged += new System.EventHandler(this.ultraCalendarDay_ValueChanged);
            this.ultraCalendarDayEnd.ValueChanged += new System.EventHandler(this.ultraCalendarDay_ValueChanged);
            this.ultraCalendarWeekBegin.ValueChanged += new System.EventHandler(this.ultraCalendarWeek_ValueChanged);
            this.ultraCalendarWeekEnd.ValueChanged += new System.EventHandler(this.ultraCalendarWeek_ValueChanged);

            this.ultraCalendarDayBegin.TextChanged += new System.EventHandler(this.ultraCalendarDay_TextChanged);
            this.ultraCalendarDayEnd.TextChanged += new System.EventHandler(this.ultraCalendarDay_TextChanged);
            this.ultraCalendarWeekBegin.TextChanged += new System.EventHandler(this.ultraCalendarWeek_TextChanged);
            this.ultraCalendarWeekEnd.TextChanged += new System.EventHandler(this.ultraCalendarWeek_TextChanged);
        }

        private void SetPeriodDateVisible()
        {
            AggregateByTime aggregateByTime = GetCurrentAggregateByTime();
            if (aggregateByTime != null)
            {
                if (aggregateByTime.Type == AggregateByTimeType.Day)
                {
                    tabControlPeriodDate.SelectedTab = tabControlPeriodDate.Tabs["tDay"];
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Week)
                {
                    tabControlPeriodDate.SelectedTab = tabControlPeriodDate.Tabs["tWeek"];
                }
                else if (aggregateByTime.Type == AggregateByTimeType.Month)
                {
                    tabControlPeriodDate.SelectedTab = tabControlPeriodDate.Tabs["tMonth"];
                }
            }
        }

        private void SetNalogOrganVisible()
        {
            AggregateByNalogOrgan aggregateNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateNalogOrgan != null)
            {
                if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Country)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tContry"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tFederalDistrict"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tRegion"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tInspection"];
                }
            }
        }

        private string FormatDateTime(DateTime? dt, string separator)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                DateTime dtF = (DateTime)dt;
                ret = string.Format("{0:00}{1}{2:00}{3}{4:00}", dtF.Day, separator, dtF.Month, separator, dtF.Year);
            }
            return ret;
        }

        private string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                DateTime dtF = (DateTime)dt;
                ret = string.Format("{0:00}_{1:00}_{2:00}", dtF.Day, dtF.Month, dtF.Year);
            }
            return ret;
        }

        private List<FederalDistrict> GetCheckedFederalDistrict()
        {
            List<FederalDistrict> fds;
            if (comboFederalDistrict.DataSource != null)
            {
                fds = (List<FederalDistrict>)comboFederalDistrict.DataSource;
            }
            else { fds = new List<FederalDistrict>(); }
            return fds.Where(p=>p.Check).ToList();
        }

        private void CheckAllFederalDistrict()
        {
            ICheckedItemList itemListFederal = (ICheckedItemList)this.comboFederalDistrict;
            for (int i = 0; i < this.comboFederalDistrict.Rows.Count; i++)
            {
                itemListFederal.SetCheckState(i, CheckState.Checked);
            }
        }

        #endregion

        #region События

        private void comboAggregateLevelNO_ValueChanged(object sender, EventArgs e)
        {
            SetNalogOrganVisible();
            GenerateChangeParameters();
        }

        private void comboAggregateLevelTime_ValueChanged(object sender, EventArgs e)
        {
            SetPeriodDateVisible();
            GenerateChangeParameters();
        }

        private void comboTypeReport_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboNalogPeriod_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboFiscalYear_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboYearAndMonth_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboDynamicParameter_ValueChanged(object sender, EventArgs e)
        {
            CheckCorrectParameters();
            GenerateChangeParameters();
            GenerateRequireChangeColumns();
        }

        private void ultraCalendarDay_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarWeek_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarDay_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarWeek_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void FederalDistrictOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void lookupMulti_OnSave()
        {
            GenerateChangeParameters();
        }

        #endregion
    }
}
