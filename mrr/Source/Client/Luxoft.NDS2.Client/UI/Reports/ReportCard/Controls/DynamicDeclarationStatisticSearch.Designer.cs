﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class DynamicDeclarationStatisticSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton5 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton6 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton7 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton8 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Description", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 3);
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ModuleType", 4);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ModuleName", 5);
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.ultraTabPageDay = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraCalendarDayBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateDayBegin = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateDayEnd = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarDayEnd = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraTabPageWeek = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraCalendarWeekBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateWeekBegin = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateWeekEnd = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarWeekEnd = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraTabPageMonth = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.comboMonthBegin = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.comboMonthEnd = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.comboYearBegin = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelYearMonthEnd = new Infragistics.Win.Misc.UltraLabel();
            this.comboYearEnd = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelYearMonthBegin = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageCountry = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageFederalDistrict = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelFederalDistrict = new Infragistics.Win.Misc.UltraLabel();
            this.comboFederalDistrict = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTabPageRegion = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.lookupMultiRegion = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.ultraTabPageInspection = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lookupMultiInspection = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.comboDynamicParameter = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.labelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.labelAggragationLevelNO = new Infragistics.Win.Misc.UltraLabel();
            this.comboAggregateLevelNO = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelAggragationLevelTime = new Infragistics.Win.Misc.UltraLabel();
            this.comboAggregateLevelTime = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelTypeReport = new Infragistics.Win.Misc.UltraLabel();
            this.comboTypeReport = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelNalogPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.comboNalogPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.comboFiscalYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelFiscalYear = new Infragistics.Win.Misc.UltraLabel();
            this.tabControlPeriodDate = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPagePeriodDate = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBoxTimePeriod = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTabControlNO = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPageNO = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.labelMessage = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageDay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarDayBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarDayEnd)).BeginInit();
            this.ultraTabPageWeek.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarWeekBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarWeekEnd)).BeginInit();
            this.ultraTabPageMonth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboMonthBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboMonthEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboYearBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboYearEnd)).BeginInit();
            this.ultraTabPageFederalDistrict.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).BeginInit();
            this.ultraTabPageRegion.SuspendLayout();
            this.ultraTabPageInspection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDynamicParameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboNalogPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboFiscalYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlPeriodDate)).BeginInit();
            this.tabControlPeriodDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTimePeriod)).BeginInit();
            this.ultraGroupBoxTimePeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).BeginInit();
            this.ultraTabControlNO.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageDay
            // 
            this.ultraTabPageDay.Controls.Add(this.ultraCalendarDayBegin);
            this.ultraTabPageDay.Controls.Add(this.labelDateDayBegin);
            this.ultraTabPageDay.Controls.Add(this.labelDateDayEnd);
            this.ultraTabPageDay.Controls.Add(this.ultraCalendarDayEnd);
            this.ultraTabPageDay.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageDay.Name = "ultraTabPageDay";
            this.ultraTabPageDay.Size = new System.Drawing.Size(242, 93);
            // 
            // ultraCalendarDayBegin
            // 
            this.ultraCalendarDayBegin.DateButtons.Add(dateButton5);
            this.ultraCalendarDayBegin.Location = new System.Drawing.Point(53, 22);
            this.ultraCalendarDayBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarDayBegin.Name = "ultraCalendarDayBegin";
            this.ultraCalendarDayBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarDayBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarDayBegin.TabIndex = 51;
            this.ultraCalendarDayBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateDayBegin
            // 
            this.labelDateDayBegin.Location = new System.Drawing.Point(53, 4);
            this.labelDateDayBegin.Name = "labelDateDayBegin";
            this.labelDateDayBegin.Size = new System.Drawing.Size(144, 17);
            this.labelDateDayBegin.TabIndex = 52;
            this.labelDateDayBegin.Text = "Дата начала периода:";
            // 
            // labelDateDayEnd
            // 
            this.labelDateDayEnd.Location = new System.Drawing.Point(53, 48);
            this.labelDateDayEnd.Name = "labelDateDayEnd";
            this.labelDateDayEnd.Size = new System.Drawing.Size(148, 17);
            this.labelDateDayEnd.TabIndex = 54;
            this.labelDateDayEnd.Text = "Дата окончания периода:";
            // 
            // ultraCalendarDayEnd
            // 
            this.ultraCalendarDayEnd.DateButtons.Add(dateButton6);
            this.ultraCalendarDayEnd.Location = new System.Drawing.Point(53, 66);
            this.ultraCalendarDayEnd.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarDayEnd.Name = "ultraCalendarDayEnd";
            this.ultraCalendarDayEnd.NonAutoSizeHeight = 21;
            this.ultraCalendarDayEnd.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarDayEnd.TabIndex = 53;
            this.ultraCalendarDayEnd.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // ultraTabPageWeek
            // 
            this.ultraTabPageWeek.Controls.Add(this.ultraCalendarWeekBegin);
            this.ultraTabPageWeek.Controls.Add(this.labelDateWeekBegin);
            this.ultraTabPageWeek.Controls.Add(this.labelDateWeekEnd);
            this.ultraTabPageWeek.Controls.Add(this.ultraCalendarWeekEnd);
            this.ultraTabPageWeek.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageWeek.Name = "ultraTabPageWeek";
            this.ultraTabPageWeek.Size = new System.Drawing.Size(242, 93);
            // 
            // ultraCalendarWeekBegin
            // 
            this.ultraCalendarWeekBegin.DateButtons.Add(dateButton7);
            this.ultraCalendarWeekBegin.Location = new System.Drawing.Point(53, 22);
            this.ultraCalendarWeekBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarWeekBegin.Name = "ultraCalendarWeekBegin";
            this.ultraCalendarWeekBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarWeekBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarWeekBegin.TabIndex = 55;
            this.ultraCalendarWeekBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateWeekBegin
            // 
            this.labelDateWeekBegin.Location = new System.Drawing.Point(18, 4);
            this.labelDateWeekBegin.Name = "labelDateWeekBegin";
            this.labelDateWeekBegin.Size = new System.Drawing.Size(216, 17);
            this.labelDateWeekBegin.TabIndex = 56;
            this.labelDateWeekBegin.Text = "Дата начала первой недели периода:";
            // 
            // labelDateWeekEnd
            // 
            this.labelDateWeekEnd.Location = new System.Drawing.Point(5, 48);
            this.labelDateWeekEnd.Name = "labelDateWeekEnd";
            this.labelDateWeekEnd.Size = new System.Drawing.Size(241, 17);
            this.labelDateWeekEnd.TabIndex = 58;
            this.labelDateWeekEnd.Text = "Дата окончания последней недели периода:";
            // 
            // ultraCalendarWeekEnd
            // 
            this.ultraCalendarWeekEnd.DateButtons.Add(dateButton8);
            this.ultraCalendarWeekEnd.Location = new System.Drawing.Point(53, 66);
            this.ultraCalendarWeekEnd.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarWeekEnd.Name = "ultraCalendarWeekEnd";
            this.ultraCalendarWeekEnd.NonAutoSizeHeight = 21;
            this.ultraCalendarWeekEnd.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarWeekEnd.TabIndex = 57;
            this.ultraCalendarWeekEnd.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // ultraTabPageMonth
            // 
            this.ultraTabPageMonth.Controls.Add(this.comboMonthBegin);
            this.ultraTabPageMonth.Controls.Add(this.comboMonthEnd);
            this.ultraTabPageMonth.Controls.Add(this.comboYearBegin);
            this.ultraTabPageMonth.Controls.Add(this.labelYearMonthEnd);
            this.ultraTabPageMonth.Controls.Add(this.comboYearEnd);
            this.ultraTabPageMonth.Controls.Add(this.labelYearMonthBegin);
            this.ultraTabPageMonth.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageMonth.Name = "ultraTabPageMonth";
            this.ultraTabPageMonth.Size = new System.Drawing.Size(242, 93);
            // 
            // comboMonthBegin
            // 
            this.comboMonthBegin.DisplayMember = "NAME";
            this.comboMonthBegin.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboMonthBegin.Location = new System.Drawing.Point(126, 21);
            this.comboMonthBegin.Margin = new System.Windows.Forms.Padding(1);
            this.comboMonthBegin.Name = "comboMonthBegin";
            this.comboMonthBegin.Size = new System.Drawing.Size(97, 21);
            this.comboMonthBegin.TabIndex = 63;
            // 
            // comboMonthEnd
            // 
            this.comboMonthEnd.DisplayMember = "NAME";
            this.comboMonthEnd.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboMonthEnd.Location = new System.Drawing.Point(126, 64);
            this.comboMonthEnd.Margin = new System.Windows.Forms.Padding(1);
            this.comboMonthEnd.Name = "comboMonthEnd";
            this.comboMonthEnd.Size = new System.Drawing.Size(97, 21);
            this.comboMonthEnd.TabIndex = 66;
            // 
            // comboYearBegin
            // 
            this.comboYearBegin.DisplayMember = "NAME";
            this.comboYearBegin.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboYearBegin.Location = new System.Drawing.Point(26, 21);
            this.comboYearBegin.Margin = new System.Windows.Forms.Padding(1);
            this.comboYearBegin.Name = "comboYearBegin";
            this.comboYearBegin.Size = new System.Drawing.Size(97, 21);
            this.comboYearBegin.TabIndex = 61;
            // 
            // labelYearMonthEnd
            // 
            this.labelYearMonthEnd.Location = new System.Drawing.Point(25, 46);
            this.labelYearMonthEnd.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelYearMonthEnd.Name = "labelYearMonthEnd";
            this.labelYearMonthEnd.Size = new System.Drawing.Size(194, 17);
            this.labelYearMonthEnd.TabIndex = 65;
            this.labelYearMonthEnd.Text = "Год и месяц окончания периода:";
            // 
            // comboYearEnd
            // 
            this.comboYearEnd.DisplayMember = "NAME";
            this.comboYearEnd.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboYearEnd.Location = new System.Drawing.Point(26, 64);
            this.comboYearEnd.Margin = new System.Windows.Forms.Padding(1);
            this.comboYearEnd.Name = "comboYearEnd";
            this.comboYearEnd.Size = new System.Drawing.Size(97, 21);
            this.comboYearEnd.TabIndex = 64;
            // 
            // labelYearMonthBegin
            // 
            this.labelYearMonthBegin.Location = new System.Drawing.Point(26, 3);
            this.labelYearMonthBegin.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelYearMonthBegin.Name = "labelYearMonthBegin";
            this.labelYearMonthBegin.Size = new System.Drawing.Size(195, 17);
            this.labelYearMonthBegin.TabIndex = 62;
            this.labelYearMonthBegin.Text = "Год и месяц начала периода:";
            // 
            // ultraTabPageCountry
            // 
            this.ultraTabPageCountry.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageCountry.Name = "ultraTabPageCountry";
            this.ultraTabPageCountry.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraTabPageFederalDistrict
            // 
            this.ultraTabPageFederalDistrict.Controls.Add(this.ultraLabelFederalDistrict);
            this.ultraTabPageFederalDistrict.Controls.Add(this.comboFederalDistrict);
            this.ultraTabPageFederalDistrict.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageFederalDistrict.Name = "ultraTabPageFederalDistrict";
            this.ultraTabPageFederalDistrict.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelFederalDistrict
            // 
            this.ultraLabelFederalDistrict.Location = new System.Drawing.Point(1, 3);
            this.ultraLabelFederalDistrict.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelFederalDistrict.Name = "ultraLabelFederalDistrict";
            this.ultraLabelFederalDistrict.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelFederalDistrict.TabIndex = 76;
            this.ultraLabelFederalDistrict.Text = "Федеральные округа:";
            // 
            // comboFederalDistrict
            // 
            this.comboFederalDistrict.CheckedListSettings.CheckStateMember = "";
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboFederalDistrict.DisplayLayout.Appearance = appearance1;
            ultraGridColumn1.Header.Caption = "Название";
            ultraGridColumn1.Header.VisiblePosition = 2;
            ultraGridColumn1.Width = 400;
            ultraGridColumn2.DataType = typeof(bool);
            ultraGridColumn2.Header.Caption = "";
            ultraGridColumn2.Header.VisiblePosition = 0;
            ultraGridColumn2.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn2.Width = 30;
            ultraGridColumn3.Header.Caption = "Код";
            ultraGridColumn3.Header.VisiblePosition = 1;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4});
            this.comboFederalDistrict.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.comboFederalDistrict.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboFederalDistrict.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboFederalDistrict.DisplayLayout.Override.CellAppearance = appearance21;
            this.comboFederalDistrict.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboFederalDistrict.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.comboFederalDistrict.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.comboFederalDistrict.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.comboFederalDistrict.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboFederalDistrict.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboFederalDistrict.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboFederalDistrict.DisplayMember = "FullName";
            this.comboFederalDistrict.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboFederalDistrict.Location = new System.Drawing.Point(1, 21);
            this.comboFederalDistrict.Margin = new System.Windows.Forms.Padding(1);
            this.comboFederalDistrict.Name = "comboFederalDistrict";
            this.comboFederalDistrict.Size = new System.Drawing.Size(445, 22);
            this.comboFederalDistrict.TabIndex = 75;
            this.comboFederalDistrict.UseAppStyling = false;
            // 
            // ultraTabPageRegion
            // 
            this.ultraTabPageRegion.Controls.Add(this.ultraLabelRegion);
            this.ultraTabPageRegion.Controls.Add(this.lookupMultiRegion);
            this.ultraTabPageRegion.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageRegion.Name = "ultraTabPageRegion";
            this.ultraTabPageRegion.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelRegion
            // 
            this.ultraLabelRegion.Location = new System.Drawing.Point(2, 2);
            this.ultraLabelRegion.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelRegion.Name = "ultraLabelRegion";
            this.ultraLabelRegion.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelRegion.TabIndex = 75;
            this.ultraLabelRegion.Text = "Регионы:";
            // 
            // lookupMultiRegion
            // 
            this.lookupMultiRegion.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiRegion.Name = "lookupMultiRegion";
            this.lookupMultiRegion.PopupHeight = 400;
            this.lookupMultiRegion.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiRegion.TabIndex = 1;
            // 
            // ultraTabPageInspection
            // 
            this.ultraTabPageInspection.Controls.Add(this.ultraLabel1);
            this.ultraTabPageInspection.Controls.Add(this.lookupMultiInspection);
            this.ultraTabPageInspection.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageInspection.Name = "ultraTabPageInspection";
            this.ultraTabPageInspection.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(2, 2);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(232, 17);
            this.ultraLabel1.TabIndex = 73;
            this.ultraLabel1.Text = "Инспекции:";
            // 
            // lookupMultiInspection
            // 
            this.lookupMultiInspection.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiInspection.Name = "lookupMultiInspection";
            this.lookupMultiInspection.PopupHeight = 400;
            this.lookupMultiInspection.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiInspection.TabIndex = 2;
            // 
            // comboDynamicParameter
            // 
            this.comboDynamicParameter.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            this.comboDynamicParameter.CheckedListSettings.CheckStateMember = "";
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboDynamicParameter.DisplayLayout.Appearance = appearance34;
            ultraGridColumn8.Header.Caption = "Название";
            ultraGridColumn8.Header.VisiblePosition = 2;
            ultraGridColumn8.Width = 670;
            ultraGridColumn9.Header.VisiblePosition = 3;
            ultraGridColumn9.Hidden = true;
            ultraGridColumn9.TabIndex = 1;
            ultraGridColumn9.Width = 640;
            ultraGridColumn10.DataType = typeof(bool);
            ultraGridColumn10.Header.Caption = "";
            ultraGridColumn10.Header.VisiblePosition = 0;
            ultraGridColumn10.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn10.TabIndex = 0;
            ultraGridColumn10.Width = 30;
            ultraGridColumn11.Header.VisiblePosition = 4;
            ultraGridColumn11.Hidden = true;
            ultraGridColumn6.Header.VisiblePosition = 5;
            ultraGridColumn6.Hidden = true;
            ultraGridColumn7.Header.Caption = "Модуль";
            ultraGridColumn7.Header.VisiblePosition = 1;
            ultraGridColumn7.Hidden = true;
            ultraGridColumn7.Width = 50;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn6,
            ultraGridColumn7});
            this.comboDynamicParameter.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.comboDynamicParameter.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.comboDynamicParameter.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.Appearance = appearance35;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.comboDynamicParameter.DisplayLayout.MaxColScrollRegions = 1;
            this.comboDynamicParameter.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboDynamicParameter.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.comboDynamicParameter.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.comboDynamicParameter.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboDynamicParameter.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboDynamicParameter.DisplayLayout.Override.CellAppearance = appearance29;
            this.comboDynamicParameter.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboDynamicParameter.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.comboDynamicParameter.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.comboDynamicParameter.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboDynamicParameter.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.comboDynamicParameter.DisplayLayout.Override.RowAppearance = appearance32;
            this.comboDynamicParameter.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comboDynamicParameter.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.comboDynamicParameter.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboDynamicParameter.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboDynamicParameter.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboDynamicParameter.DisplayMember = "Name";
            this.comboDynamicParameter.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboDynamicParameter.Location = new System.Drawing.Point(8, 191);
            this.comboDynamicParameter.Margin = new System.Windows.Forms.Padding(1);
            this.comboDynamicParameter.Name = "comboDynamicParameter";
            this.comboDynamicParameter.Size = new System.Drawing.Size(724, 22);
            this.comboDynamicParameter.TabIndex = 46;
            this.comboDynamicParameter.UseAppStyling = false;
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(7, 173);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(270, 17);
            this.labelRegion.TabIndex = 45;
            this.labelRegion.Text = "Показатели, отображаемые в отчете:";
            // 
            // labelAggragationLevelNO
            // 
            this.labelAggragationLevelNO.Location = new System.Drawing.Point(7, 30);
            this.labelAggragationLevelNO.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelAggragationLevelNO.Name = "labelAggragationLevelNO";
            this.labelAggragationLevelNO.Size = new System.Drawing.Size(232, 17);
            this.labelAggragationLevelNO.TabIndex = 48;
            this.labelAggragationLevelNO.Text = "Уровень агрегации по налоговым органам:";
            // 
            // comboAggregateLevelNO
            // 
            this.comboAggregateLevelNO.DisplayMember = "NAME";
            this.comboAggregateLevelNO.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboAggregateLevelNO.Location = new System.Drawing.Point(7, 48);
            this.comboAggregateLevelNO.Margin = new System.Windows.Forms.Padding(1);
            this.comboAggregateLevelNO.Name = "comboAggregateLevelNO";
            this.comboAggregateLevelNO.Size = new System.Drawing.Size(270, 21);
            this.comboAggregateLevelNO.TabIndex = 47;
            // 
            // labelAggragationLevelTime
            // 
            this.labelAggragationLevelTime.Location = new System.Drawing.Point(293, 17);
            this.labelAggragationLevelTime.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelAggragationLevelTime.Name = "labelAggragationLevelTime";
            this.labelAggragationLevelTime.Size = new System.Drawing.Size(158, 30);
            this.labelAggragationLevelTime.TabIndex = 50;
            this.labelAggragationLevelTime.Text = "Уровень агрегации по времени:";
            // 
            // comboAggregateLevelTime
            // 
            this.comboAggregateLevelTime.DisplayMember = "NAME";
            this.comboAggregateLevelTime.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboAggregateLevelTime.Location = new System.Drawing.Point(293, 48);
            this.comboAggregateLevelTime.Margin = new System.Windows.Forms.Padding(1);
            this.comboAggregateLevelTime.Name = "comboAggregateLevelTime";
            this.comboAggregateLevelTime.Size = new System.Drawing.Size(158, 21);
            this.comboAggregateLevelTime.TabIndex = 49;
            // 
            // labelTypeReport
            // 
            this.labelTypeReport.Location = new System.Drawing.Point(292, 126);
            this.labelTypeReport.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelTypeReport.Name = "labelTypeReport";
            this.labelTypeReport.Size = new System.Drawing.Size(166, 17);
            this.labelTypeReport.TabIndex = 56;
            this.labelTypeReport.Text = "Вид отчета:";
            // 
            // comboTypeReport
            // 
            this.comboTypeReport.DisplayMember = "NAME";
            this.comboTypeReport.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTypeReport.Location = new System.Drawing.Point(293, 144);
            this.comboTypeReport.Margin = new System.Windows.Forms.Padding(1);
            this.comboTypeReport.Name = "comboTypeReport";
            this.comboTypeReport.Size = new System.Drawing.Size(158, 21);
            this.comboTypeReport.TabIndex = 55;
            // 
            // labelNalogPeriod
            // 
            this.labelNalogPeriod.Location = new System.Drawing.Point(7, 126);
            this.labelNalogPeriod.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelNalogPeriod.Name = "labelNalogPeriod";
            this.labelNalogPeriod.Size = new System.Drawing.Size(155, 17);
            this.labelNalogPeriod.TabIndex = 58;
            this.labelNalogPeriod.Text = "Налоговый период:";
            // 
            // comboNalogPeriod
            // 
            this.comboNalogPeriod.DisplayMember = "NAME";
            this.comboNalogPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboNalogPeriod.Location = new System.Drawing.Point(7, 144);
            this.comboNalogPeriod.Margin = new System.Windows.Forms.Padding(1);
            this.comboNalogPeriod.Name = "comboNalogPeriod";
            this.comboNalogPeriod.Size = new System.Drawing.Size(166, 21);
            this.comboNalogPeriod.TabIndex = 57;
            // 
            // comboFiscalYear
            // 
            this.comboFiscalYear.DisplayMember = "";
            this.comboFiscalYear.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboFiscalYear.Location = new System.Drawing.Point(180, 144);
            this.comboFiscalYear.Margin = new System.Windows.Forms.Padding(1);
            this.comboFiscalYear.Name = "comboFiscalYear";
            this.comboFiscalYear.Size = new System.Drawing.Size(97, 21);
            this.comboFiscalYear.TabIndex = 59;
            // 
            // labelFiscalYear
            // 
            this.labelFiscalYear.Location = new System.Drawing.Point(182, 126);
            this.labelFiscalYear.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelFiscalYear.Name = "labelFiscalYear";
            this.labelFiscalYear.Size = new System.Drawing.Size(78, 17);
            this.labelFiscalYear.TabIndex = 60;
            this.labelFiscalYear.Text = "Год:";
            // 
            // tabControlPeriodDate
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            this.tabControlPeriodDate.ActiveTabAppearance = appearance37;
            this.tabControlPeriodDate.Controls.Add(this.ultraTabSharedControlsPagePeriodDate);
            this.tabControlPeriodDate.Controls.Add(this.ultraTabPageDay);
            this.tabControlPeriodDate.Controls.Add(this.ultraTabPageMonth);
            this.tabControlPeriodDate.Controls.Add(this.ultraTabPageWeek);
            this.tabControlPeriodDate.Location = new System.Drawing.Point(13, 33);
            this.tabControlPeriodDate.Name = "tabControlPeriodDate";
            this.tabControlPeriodDate.SharedControlsPage = this.ultraTabSharedControlsPagePeriodDate;
            this.tabControlPeriodDate.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.tabControlPeriodDate.Size = new System.Drawing.Size(242, 93);
            this.tabControlPeriodDate.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.tabControlPeriodDate.TabIndex = 67;
            ultraTab1.Key = "tDay";
            ultraTab1.TabPage = this.ultraTabPageDay;
            ultraTab1.Text = "tabDay";
            ultraTab3.Key = "tWeek";
            ultraTab3.TabPage = this.ultraTabPageWeek;
            ultraTab3.Text = "tabWeek";
            ultraTab2.Key = "tMonth";
            ultraTab2.TabPage = this.ultraTabPageMonth;
            ultraTab2.Text = "tabMonth";
            this.tabControlPeriodDate.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3,
            ultraTab2});
            this.tabControlPeriodDate.UseAppStyling = false;
            // 
            // ultraTabSharedControlsPagePeriodDate
            // 
            this.ultraTabSharedControlsPagePeriodDate.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPagePeriodDate.Name = "ultraTabSharedControlsPagePeriodDate";
            this.ultraTabSharedControlsPagePeriodDate.Size = new System.Drawing.Size(242, 93);
            // 
            // ultraGroupBoxTimePeriod
            // 
            this.ultraGroupBoxTimePeriod.Controls.Add(this.tabControlPeriodDate);
            this.ultraGroupBoxTimePeriod.Location = new System.Drawing.Point(462, 30);
            this.ultraGroupBoxTimePeriod.Name = "ultraGroupBoxTimePeriod";
            this.ultraGroupBoxTimePeriod.Size = new System.Drawing.Size(270, 137);
            this.ultraGroupBoxTimePeriod.TabIndex = 69;
            this.ultraGroupBoxTimePeriod.Text = "Период формирования отчёта (в зависимости от уровня агрегации по времени):";
            // 
            // ultraTabControlNO
            // 
            this.ultraTabControlNO.Controls.Add(this.ultraTabSharedControlsPageNO);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageCountry);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageFederalDistrict);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageRegion);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageInspection);
            this.ultraTabControlNO.Location = new System.Drawing.Point(6, 72);
            this.ultraTabControlNO.Name = "ultraTabControlNO";
            this.ultraTabControlNO.SharedControlsPage = this.ultraTabSharedControlsPageNO;
            this.ultraTabControlNO.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.ultraTabControlNO.Size = new System.Drawing.Size(447, 49);
            this.ultraTabControlNO.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.ultraTabControlNO.TabIndex = 78;
            ultraTab4.Key = "tContry";
            ultraTab4.TabPage = this.ultraTabPageCountry;
            ultraTab4.Text = "tabContry";
            ultraTab5.Key = "tFederalDistrict";
            ultraTab5.TabPage = this.ultraTabPageFederalDistrict;
            ultraTab5.Text = "tabFederalDistrict";
            ultraTab6.Key = "tRegion";
            ultraTab6.TabPage = this.ultraTabPageRegion;
            ultraTab6.Text = "tabRegion";
            ultraTab7.Key = "tInspection";
            ultraTab7.TabPage = this.ultraTabPageInspection;
            ultraTab7.Text = "tabInspection";
            this.ultraTabControlNO.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7});
            this.ultraTabControlNO.UseAppStyling = false;
            // 
            // ultraTabSharedControlsPageNO
            // 
            this.ultraTabSharedControlsPageNO.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPageNO.Name = "ultraTabSharedControlsPageNO";
            this.ultraTabSharedControlsPageNO.Size = new System.Drawing.Size(447, 49);
            // 
            // labelMessage
            // 
            appearance13.ForeColor = System.Drawing.Color.Red;
            this.labelMessage.Appearance = appearance13;
            this.labelMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMessage.Location = new System.Drawing.Point(0, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(774, 17);
            this.labelMessage.TabIndex = 79;
            this.labelMessage.Text = "Не выбран ни один показатель, отображаемый в отчете";
            // 
            // DynamicDeclarationStatisticSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.ultraTabControlNO);
            this.Controls.Add(this.ultraGroupBoxTimePeriod);
            this.Controls.Add(this.labelFiscalYear);
            this.Controls.Add(this.comboFiscalYear);
            this.Controls.Add(this.labelNalogPeriod);
            this.Controls.Add(this.comboNalogPeriod);
            this.Controls.Add(this.labelTypeReport);
            this.Controls.Add(this.comboTypeReport);
            this.Controls.Add(this.labelAggragationLevelTime);
            this.Controls.Add(this.comboAggregateLevelTime);
            this.Controls.Add(this.labelAggragationLevelNO);
            this.Controls.Add(this.comboAggregateLevelNO);
            this.Controls.Add(this.comboDynamicParameter);
            this.Controls.Add(this.labelRegion);
            this.Name = "DynamicDeclarationStatisticSearch";
            this.Size = new System.Drawing.Size(774, 222);
            this.Load += new System.EventHandler(this.DynamicDeclarationStatisticSearch_Load);
            this.ultraTabPageDay.ResumeLayout(false);
            this.ultraTabPageDay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarDayBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarDayEnd)).EndInit();
            this.ultraTabPageWeek.ResumeLayout(false);
            this.ultraTabPageWeek.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarWeekBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarWeekEnd)).EndInit();
            this.ultraTabPageMonth.ResumeLayout(false);
            this.ultraTabPageMonth.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboMonthBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboMonthEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboYearBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboYearEnd)).EndInit();
            this.ultraTabPageFederalDistrict.ResumeLayout(false);
            this.ultraTabPageFederalDistrict.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).EndInit();
            this.ultraTabPageRegion.ResumeLayout(false);
            this.ultraTabPageInspection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboDynamicParameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboNalogPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboFiscalYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlPeriodDate)).EndInit();
            this.tabControlPeriodDate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTimePeriod)).EndInit();
            this.ultraGroupBoxTimePeriod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).EndInit();
            this.ultraTabControlNO.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo comboDynamicParameter;
        private Infragistics.Win.Misc.UltraLabel labelRegion;
        private Infragistics.Win.Misc.UltraLabel labelAggragationLevelNO;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboAggregateLevelNO;
        private Infragistics.Win.Misc.UltraLabel labelAggragationLevelTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboAggregateLevelTime;
        private Infragistics.Win.Misc.UltraLabel labelDateDayBegin;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarDayBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateDayEnd;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarDayEnd;
        private Infragistics.Win.Misc.UltraLabel labelTypeReport;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTypeReport;
        private Infragistics.Win.Misc.UltraLabel labelNalogPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboNalogPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboFiscalYear;
        private Infragistics.Win.Misc.UltraLabel labelFiscalYear;
        private Infragistics.Win.Misc.UltraLabel labelYearMonthBegin;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboYearBegin;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboMonthBegin;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboMonthEnd;
        private Infragistics.Win.Misc.UltraLabel labelYearMonthEnd;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboYearEnd;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControlPeriodDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPagePeriodDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageDay;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageMonth;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageWeek;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarWeekBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateWeekBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateWeekEnd;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarWeekEnd;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTimePeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabelFederalDistrict;
        private Infragistics.Win.UltraWinGrid.UltraCombo comboFederalDistrict;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPageNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageCountry;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageFederalDistrict;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageRegion;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageInspection;
        private Infragistics.Win.Misc.UltraLabel ultraLabelRegion;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiRegion;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiInspection;
        private Infragistics.Win.Misc.UltraLabel labelMessage;
    }
}
