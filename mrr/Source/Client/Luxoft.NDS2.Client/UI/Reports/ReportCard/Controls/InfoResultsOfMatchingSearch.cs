﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard;
using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_InfoResultsOfMatching;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class InfoResultsOfMatchingSearch : BaseControlSearch, IControlSearchCriteria
    {
        #region Переменные

        private List<AggregateByNalogOrgan> _aggregateByNalogOrgans = new List<AggregateByNalogOrgan>();
        private List<FederalDistrict> _federalDistricts = new List<FederalDistrict>();
        private IDataService _dataService;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel> regionSelectPresenter;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel> taxAuthorityPresenter;
        private List<NalogPeriodCheck> _nalogPeriods = new List<NalogPeriodCheck>();
        private List<TimePeriod> _timePeriods = new List<TimePeriod>();

        #endregion

        public InfoResultsOfMatchingSearch(IDataService dataService)
            : base()
        {
            InitializeComponent();
            _dataService = dataService;
        }

        #region Реализация IControlSearchCriteria

        public override QueryConditions GetQueryConditions()
        {
            QueryConditions queryCond = new QueryConditions();

            AggregateByNalogOrgan aggregateByNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateByNalogOrgan != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_AggregateByNalogOrgan, (int)aggregateByNalogOrgan.Type);
            }

            if (aggregateByNalogOrgan != null)
            {
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    SearchCriteriaHelper.AddFilterFederalDistrict(queryCond, TaskParamCriteria.IRM_FederalDistrict, GetCheckedFederalDistrict());
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    SearchCriteriaHelper.AddFilterRegion(queryCond, TaskParamCriteria.IRM_TaxPayerRegionCode, regionSelectPresenter);
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    SearchCriteriaHelper.AddFilterNalogOrgan(queryCond, TaskParamCriteria.IRM_NalogOrganCode, taxAuthorityPresenter);
                }
            }

            SearchCriteriaHelper.AddFilterNalogPeriod(queryCond, TaskParamCriteria.IRM_NalogOrganCheck, GetCheckedNalogPeriod());

            TimePeriod timePeriod = GetCurrentTimePeriod();
            if (timePeriod != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_TimePeriod, (int)timePeriod.Type);
                if (timePeriod.Type == TimePeriodType.OneDay)
                {
                    SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_DateOneDay, (DateTime)ultraCalendarOneDay.Value);
                }
                else if (timePeriod.Type == TimePeriodType.Period)
                {
                    SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_DateBegin, (DateTime)ultraCalendarBegin.Value);
                    SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_DateEnd, (DateTime)ultraCalendarEnd.Value);
                }
            }

            return queryCond;
        }

        #endregion

        #region Установка данных

        public void SetAggregateByNalogOrgan(List<AggregateByNalogOrgan> aggregateByNalogOrgans)
        {
            _aggregateByNalogOrgans.Clear();
            _aggregateByNalogOrgans.AddRange(aggregateByNalogOrgans);
        }

        public void SetFederalDistricts(List<FederalDistrict> federalDistricts)
        {
            _federalDistricts.Clear();
            _federalDistricts.AddRange(federalDistricts);
        }

        public void SetNalogPeriods(List<NalogPeriodCheck> nalogPeriods)
        {
            _nalogPeriods.Clear();
            _nalogPeriods.AddRange(nalogPeriods);
        }

        public void SetTimePeriods(List<TimePeriod> timePeriods)
        {
            _timePeriods.Clear();
            _timePeriods.AddRange(timePeriods);
        }

        #endregion

        #region Инициализация

        private void InfoResultsOfMatchingSearch_Load(object sender, EventArgs e)
        {
            InitViewFederalDistrict();
            InitViewNalogPeriod();
            InitData();
        }

        public void InitData()
        {
            comboAggregateLevelNO.DataSource = _aggregateByNalogOrgans;
            comboFederalDistrict.DataSource = _federalDistricts;
            comboTimePeriod.DataSource = _timePeriods;
            comboNalogPeriod.DataSource = _nalogPeriods;

            ILookupDataSource lookupDataSource = LookupDataSource();
            regionSelectPresenter = new UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel>(
                lookupMultiRegion,
                (args) => lookupDataSource.Search<RegionModel>(args));

            taxAuthorityPresenter =
                new UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel>(
                    lookupMultiInspection,
                    (args) => lookupDataSource.Search<TaxAuthorityModel>(args));

            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            if (comboAggregateLevelNO.Items.Count > 0)
                comboAggregateLevelNO.SelectedIndex = 0;
            else
                comboAggregateLevelNO.SelectedIndex = -1;

            if (comboTimePeriod.Items.Count > 0)
                comboTimePeriod.SelectedIndex = 0;
            else
                comboTimePeriod.SelectedIndex = -1;

            DateTime dtDefault = DateTime.Now.Date.AddDays(-1);
            ultraCalendarOneDay.Value = dtDefault;
            ultraCalendarBegin.Value = dtDefault;
            ultraCalendarEnd.Value = dtDefault;
        }

        private ILookupDataSource LookupDataSource()
        {
            return new FilterEditorDataProvider(_dataService);
        }

        private void InitViewFederalDistrict()
        {
            comboFederalDistrict.CheckedListSettings.CheckStateMember = "Check";
            UltraGridColumn column = comboFederalDistrict.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            comboFederalDistrict.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            comboFederalDistrict.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            comboFederalDistrict.CheckedListSettings.ListSeparator = " / ";
        }

        private void InitViewNalogPeriod()
        {
            comboNalogPeriod.CheckedListSettings.CheckStateMember = "Check";
            UltraGridColumn column = comboNalogPeriod.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            comboNalogPeriod.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            comboNalogPeriod.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            comboNalogPeriod.CheckedListSettings.ListSeparator = " / ";

            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
        }

        #endregion

        #region Вспомогательные методы

        private AggregateByNalogOrgan GetCurrentAggregateByNalogOrgan()
        {
            AggregateByNalogOrgan aggregateByNalogOrgan = null;
            int ind = comboAggregateLevelNO.SelectedIndex;
            if (ind > -1 && ind < _aggregateByNalogOrgans.Count)
            {
                aggregateByNalogOrgan = _aggregateByNalogOrgans[ind];
            }
            return aggregateByNalogOrgan;
        }

        private TimePeriod GetCurrentTimePeriod()
        {
            TimePeriod timePeriod = null;
            int ind = comboTimePeriod.SelectedIndex;
            if (ind > -1 && ind < _timePeriods.Count)
            {
                timePeriod = _timePeriods[ind];
            }
            return timePeriod;
        }

        private void AllComboControlsEventAdd()
        {
            comboAggregateLevelNO.ValueChanged += new EventHandler(comboAggregateLevelNO_ValueChanged);
            comboFederalDistrict.ValueChanged += new EventHandler(FederalDistrictOnValueChanged);
            lookupMultiRegion.OnSave += lookupMulti_OnSave;
            lookupMultiInspection.OnSave += lookupMulti_OnSave;
            comboTimePeriod.ValueChanged += comboTimePeriod_ValueChanged;
            comboNalogPeriod.ValueChanged += comboNalogPeriod_ValueChanged;
        }

        private void DateTimeControlsEventAdd()
        {
            ultraCalendarBegin.ValueChanged += new EventHandler(ultraCalendar_ValueChanged);
            ultraCalendarEnd.ValueChanged += new EventHandler(ultraCalendar_ValueChanged);

            ultraCalendarBegin.TextChanged += new EventHandler(ultraCalendar_TextChanged);
            ultraCalendarEnd.TextChanged += new EventHandler(ultraCalendar_TextChanged);
        }

        private void SetNalogOrganVisible()
        {
            AggregateByNalogOrgan aggregateNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateNalogOrgan != null)
            {
                if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Country)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tContry"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tFederalDistrict"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tRegion"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tInspection"];
                }
            }
        }

        private void SetDatePeriodVisible()
        {
            TimePeriod timePeriod = GetCurrentTimePeriod();
            if (timePeriod != null)
            {
                if (timePeriod.Type == TimePeriodType.OneDay)
                {
                    ultraTabControlTimePeriod.SelectedTab = ultraTabControlTimePeriod.Tabs["tOneDay"];
                }
                else if (timePeriod.Type == TimePeriodType.Period)
                {
                    ultraTabControlTimePeriod.SelectedTab = ultraTabControlTimePeriod.Tabs["tPeriod"];
                }
            }
        }

        private List<FederalDistrict> GetCheckedFederalDistrict()
        {
            List<FederalDistrict> fds;
            if (comboFederalDistrict.DataSource != null)
            {
                fds = (List<FederalDistrict>)comboFederalDistrict.DataSource;
            }
            else { fds = new List<FederalDistrict>(); }
            return fds.Where(p => p.Check).ToList();
        }

        private List<NalogPeriodCheck> GetCheckedNalogPeriod()
        {
            List<NalogPeriodCheck> nalogPeriods;
            if (comboFederalDistrict.DataSource != null)
            {
                nalogPeriods = (List<NalogPeriodCheck>)comboNalogPeriod.DataSource;
            }
            else { nalogPeriods = new List<NalogPeriodCheck>(); }
            return nalogPeriods.Where(p => p.Check).ToList();
        }

        #endregion

        #region События

        private void comboAggregateLevelNO_ValueChanged(object sender, EventArgs e)
        {
            SetNalogOrganVisible();
            GenerateChangeParameters();
        }

        private void FederalDistrictOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void lookupMulti_OnSave()
        {
            GenerateChangeParameters();
        }

        private void ultraCalendar_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendar_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboTimePeriod_ValueChanged(object sender, EventArgs e)
        {
            SetDatePeriodVisible();
            GenerateChangeParameters();
        }

        private void comboNalogPeriod_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        #endregion
    }
}
