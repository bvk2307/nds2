﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using ColumnFilter = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class DeclarationStatisticSearch : BaseControlSearch
    {
        #region Переменные

        private readonly List<RegionDetails> _regions = new List<RegionDetails>();
        private readonly List<DictionaryStatisticType> _typeGenerates = new List<DictionaryStatisticType>();
        private readonly List<DictionaryStatisticRegim> _regims = new List<DictionaryStatisticRegim>();
        private readonly List<NalogPeriod> _nalogPeriods = new List<NalogPeriod>();
        private readonly AccessContext _accessContext;
        private readonly string _sonoCode;

        #endregion

        public DeclarationStatisticSearch(AccessContext accessContext, string sonoCode)
        {
            _accessContext = accessContext;
            _sonoCode = sonoCode;
            InitializeComponent();
            InitTypeGenerates();
            InitRegims();
        }

        #region Реализация IControlSearchCriteria

        public override string GetSearchCriteriaDescription()
        {
            return (comboReportingPeriod.SelectedIndex > -1) ? _nalogPeriods[comboReportingPeriod.SelectedIndex].NAME : string.Empty;
        }

        public override IEnumerable<ColumnSort> GetQuerySorting(IEnumerable<ColumnSort> list)
        {
            if ((list != null) && list.Any())
            {
                Dictionary<string, string> dict = DeclarationStatistic.GetDisplayNameKeyDictionary();
                foreach (var item in list)
                {
                    if (dict.ContainsKey(item.ColumnKey))
                    {
                        item.ColumnKey = dict[item.ColumnKey];
                    }
                }
            }
            return list;
        }

        public override QueryConditions GetQueryConditions()
        {
            var queryCond = new QueryConditions();

            var filterQuerySonoCode = new FilterQuery { ColumnName = "SonoCode" };
            filterQuerySonoCode.Filtering.Add(new ColumnFilter
            {
                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                Value = _sonoCode
            });
            queryCond.Filter.Add(filterQuerySonoCode);

            if (comboReportingPeriod.SelectedIndex > -1)
            {
                var filterQueryPeriod = new FilterQuery {ColumnName = "NalogPeriod"};
                var columnFilter = new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = _nalogPeriods[comboReportingPeriod.SelectedIndex].CODE
                };
                filterQueryPeriod.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQueryPeriod);

                var filterQueryYear = new FilterQuery {ColumnName = "Year"};
                var columnFilterYear = new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = _nalogPeriods[comboReportingPeriod.SelectedIndex].Year
                };
                filterQueryYear.Filtering.Add(columnFilterYear);
                queryCond.Filter.Add(filterQueryYear);
            }

            DictionaryStatisticType currentType = GetCurrentType();
            if (currentType != null)
            {
                var filterQueryType = new FilterQuery {ColumnName = "DeclarationStatisticType"};
                var columnFilterType = new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = currentType.Id
                };
                filterQueryType.Filtering.Add(columnFilterType);
                queryCond.Filter.Add(filterQueryType);

                if (currentType.Id == (int)DictionaryStatisticTypeConst.Inspection)
                {
                    var filterQueryRegion = new FilterQuery
                    {
                        ColumnName = "TaxPayerRegionCode",
                        FilterOperator = FilterQuery.FilterLogicalOperator.Or
                    };
                    List<RegionDetails> regs = GetCurrentRegions();
                    if (regs.Any(p => p.Check))
                    {
                        foreach (RegionDetails item in regs.Where(p => p.Check))
                        {
                            var columnFilter = new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = item.Code
                            };
                            filterQueryRegion.Filtering.Add(columnFilter);
                        }
                        queryCond.Filter.Add(filterQueryRegion);
                    }
                }
            }

            DictionaryStatisticRegim currentRegim = GetCurrentRegim();
            if (currentRegim != null)
            {
                var filterQueryRegim = new FilterQuery {ColumnName = "DeclarationStatisticRegim"};
                var columnFilterRegim = new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = currentRegim.Id
                };
                filterQueryRegim.Filtering.Add(columnFilterRegim);
                queryCond.Filter.Add(filterQueryRegim);

                if (currentRegim.Id == (int)DictionaryStatisticRegimConst.OnCurrentDate)
                {
                    if (ultraCalendarBegin.Value != null)
                    {
                        var dtBegin = (DateTime)ultraCalendarBegin.Value;
                        var filterQueryDT = new FilterQuery {ColumnName = "DateCreateReport"};
                        var columnFilter = new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = dtBegin
                        };
                        filterQueryDT.Filtering.Add(columnFilter);
                        queryCond.Filter.Add(filterQueryDT);
                    }
                }
                else 
                if (currentRegim.Id == (int)DictionaryStatisticRegimConst.OnBetweenDates)
                {
                    if (ultraCalendarBegin.Value != null)
                    {
                        var dtBegin = (DateTime)ultraCalendarBegin.Value;
                        var filterQueryDTBegin = new FilterQuery {ColumnName = "DateBegin"};
                        var columnFilter = new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                            Value = dtBegin
                        };
                        filterQueryDTBegin.Filtering.Add(columnFilter);
                        queryCond.Filter.Add(filterQueryDTBegin);
                    }
                    if (ultraCalendarEnd.Value != null)
                    {
                        var dtEnd = (DateTime)ultraCalendarEnd.Value;
                        var filterQueryDTend = new FilterQuery {ColumnName = "DateEnd"};
                        var columnFilter = new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                            Value = dtEnd
                        };
                        filterQueryDTend.Filtering.Add(columnFilter);
                        queryCond.Filter.Add(filterQueryDTend);
                    }
                }
            }

            return queryCond;
        }

        #endregion

        #region Инициализация

        public void SetRegions(IEnumerable<RegionDetails> regions)
        {
            _regions.Clear();
            _regions.AddRange(regions);
            if (_regions.Count() == 1)
            {
                _regions[0].Check = true;
            }
        }

        public void SetNalogPeriods(IEnumerable<NalogPeriod> nalogPeriods)
        {
            _nalogPeriods.AddRange(nalogPeriods);
        }

        private void InitTypeGenerates()
        {
            _typeGenerates.Clear();

            if (_accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsCountry))
                _typeGenerates.Add(new DictionaryStatisticType() { Id = 1, Name = "По стране" });
            if (_accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions))
                _typeGenerates.Add(new DictionaryStatisticType() { Id = 2, Name = "По федеральным округам" });
            if (_accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsRegions))
                _typeGenerates.Add(new DictionaryStatisticType() { Id = 3, Name = "По регионам" });
            if (_accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsInspections))
                _typeGenerates.Add(new DictionaryStatisticType() { Id = 4, Name = "По инспекциям" });
        }

        private void InitRegims()
        {
            _regims.Clear();
            _regims.Add(new DictionaryStatisticRegim() { Id = 1, Name = "Данные на дату" });
            _regims.Add(new DictionaryStatisticRegim() { Id = 2, Name = "Изменение значений между датами" });
        }

        private void InitViewRegion()
        {
            ultraComboRegion.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = ultraComboRegion.DisplayLayout.Bands[0].Columns["Check"];
            var checkEditor = new CheckEditor {CheckAlign = ContentAlignment.MiddleCenter};
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            ultraComboRegion.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            ultraComboRegion.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            ultraComboRegion.CheckedListSettings.ListSeparator = @" / ";
        }

        private void DeclarationStatisticSearch_Load(object sender, EventArgs e)
        {
            InitViewRegion();
            InitData();
        }

        private void InitData()
        {
            var dtNow = DateTime.Now.Date;

            var dtOfPreviousQuarter = QuarterCalculator
                .CalculatePreviousQuarterWithOffset(dtNow);

            comboReportingPeriod.DataSource = _nalogPeriods;
            comboReportingPeriod.Refresh();
            comboReportingPeriod.SelectedIndex = _nalogPeriods.Count - 1;

            comboTypeGenerate.DataSource = _typeGenerates;
            comboTypeGenerate.Enabled = _typeGenerates.Count > 1;
            comboTypeGenerate.Refresh();

            comboRegim.DataSource = _regims;
            comboRegim.Refresh();

            ultraCalendarBegin.Value = dtNow;
            ultraCalendarEnd.Value = dtNow;

            AllComboControlsEventRemove();

            ultraComboRegion.DataSource = _regions;
            ultraComboRegion.Enabled = _regions.Count > 1;
            ultraComboRegion.Refresh();

            GenerateChangeParameters();
            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();

            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            comboTypeGenerate.SelectedIndex = (_typeGenerates.Count > 0) ? 0 : -1;
            comboRegim.SelectedIndex = (_regims.Count > 0) ? 0 : -1;
        }

        #endregion

        #region Вспомогательные методы

        private void AllComboControlsEventRemove()
        {
            ultraComboRegion.ValueChanged -= RegionOnValueChanged;
            comboTypeGenerate.ValueChanged -= comboTypeGenerate_ValueChanged;
            comboRegim.ValueChanged -= comboRegim_ValueChanged;
        }

        private void AllComboControlsEventAdd()
        {
            ultraComboRegion.ValueChanged += RegionOnValueChanged;
            comboTypeGenerate.ValueChanged += comboTypeGenerate_ValueChanged;
            comboRegim.ValueChanged += comboRegim_ValueChanged;
        }

        private void DateTimeControlsEventAdd()
        {
            ultraCalendarBegin.ValueChanged += ultraCalendarBegin_ValueChanged;
            ultraCalendarEnd.ValueChanged += ultraCalendarEnd_ValueChanged;
            comboReportingPeriod.ValueChanged += comboReportingPeriod_ValueChanged;

            ultraCalendarBegin.TextChanged += ultraCalendarCommon_TextChanged;
            ultraCalendarEnd.TextChanged += ultraCalendarCommon_TextChanged;
        }

        private DictionaryStatisticType GetCurrentType()
        {
            return (comboTypeGenerate.SelectedIndex > -1) ? _typeGenerates[comboTypeGenerate.SelectedIndex] : null;
        }

        private DictionaryStatisticRegim GetCurrentRegim()
        {
            return (comboRegim.SelectedIndex > -1) ? _regims[comboRegim.SelectedIndex] : null;
        }

        private List<RegionDetails> GetCurrentRegions()
        {
            return (ultraComboRegion.DataSource != null) ? (List<RegionDetails>)ultraComboRegion.DataSource : new List<RegionDetails>();
        }

        #endregion

        #region События

        private void RegionOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarBegin_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarEnd_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendarCommon_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void comboTypeGenerate_ValueChanged(object sender, EventArgs e)
        {
            if (comboTypeGenerate.SelectedIndex > -1)
            {
                DictionaryStatisticType itemCurrent = _typeGenerates[comboTypeGenerate.SelectedIndex];
                labelRegion.Visible = (itemCurrent.Id == (int)DictionaryStatisticTypeConst.Inspection);
                ultraComboRegion.Visible = labelRegion.Visible;
            }
            GenerateChangeParameters();
        }

        private void comboRegim_ValueChanged(object sender, EventArgs e)
        {
            if (comboRegim.SelectedIndex > -1)
            {
                DictionaryStatisticRegim itemCurrent = _regims[comboRegim.SelectedIndex];
                labelDateEnd.Visible = (itemCurrent.Id == (int)DictionaryStatisticRegimConst.OnBetweenDates);
                labelDateBegin.Text = labelDateEnd.Visible ? "Дата начала периода:" : "Дата формирования отчета:";                
                ultraCalendarEnd.Visible = labelDateEnd.Visible;
            }
            GenerateChangeParameters();
        }

        private void comboReportingPeriod_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        #endregion
    }
}
