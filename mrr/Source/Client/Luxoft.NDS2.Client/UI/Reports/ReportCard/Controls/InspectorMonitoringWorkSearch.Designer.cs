﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class InspectorMonitoringWorkSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton3 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Description", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 3);
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Code", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FederalCode", 4);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("S_CODE", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("S_PARENT_CODE", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("S_NAME", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 4);
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            this.tabEdit = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutDateBeginEnd = new System.Windows.Forms.TableLayoutPanel();
            this.labelBeginCaption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraCalendarEnd = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelEndCaption = new Infragistics.Win.Misc.UltraLabel();
            this.tabList = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutDateCreateReport = new System.Windows.Forms.TableLayoutPanel();
            this.ultraCalendarCreateReport = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateCreateReportCaption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelFirstRow = new System.Windows.Forms.TableLayoutPanel();
            this.firstRowSelector = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.styleSharedTab = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraLabelReportingPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.comboReportingPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.tableLayoutFederalDistrict = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabelFederalDistrict = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboFederalDistrict = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutRegion = new System.Windows.Forms.TableLayoutPanel();
            this.labelRegionCaption = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboRegion = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutInspection = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabelInspection = new Infragistics.Win.Misc.UltraLabel();
            this.ucInspection = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabEdit.SuspendLayout();
            this.tableLayoutDateBeginEnd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).BeginInit();
            this.tabList.SuspendLayout();
            this.tableLayoutDateCreateReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCreateReport)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelFirstRow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstRowSelector)).BeginInit();
            this.firstRowSelector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboReportingPeriod)).BeginInit();
            this.tableLayoutFederalDistrict.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboFederalDistrict)).BeginInit();
            this.tableLayoutRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboRegion)).BeginInit();
            this.tableLayoutInspection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ucInspection)).BeginInit();
            this.SuspendLayout();
            // 
            // tabEdit
            // 
            this.tabEdit.Controls.Add(this.tableLayoutDateBeginEnd);
            this.tabEdit.Location = new System.Drawing.Point(0, 0);
            this.tabEdit.Name = "tabEdit";
            this.tabEdit.Size = new System.Drawing.Size(420, 25);
            // 
            // tableLayoutDateBeginEnd
            // 
            this.tableLayoutDateBeginEnd.ColumnCount = 5;
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutDateBeginEnd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutDateBeginEnd.Controls.Add(this.labelBeginCaption, 0, 0);
            this.tableLayoutDateBeginEnd.Controls.Add(this.ultraCalendarBegin, 1, 0);
            this.tableLayoutDateBeginEnd.Controls.Add(this.ultraCalendarEnd, 3, 0);
            this.tableLayoutDateBeginEnd.Controls.Add(this.labelEndCaption, 2, 0);
            this.tableLayoutDateBeginEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutDateBeginEnd.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutDateBeginEnd.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutDateBeginEnd.Name = "tableLayoutDateBeginEnd";
            this.tableLayoutDateBeginEnd.RowCount = 1;
            this.tableLayoutDateBeginEnd.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutDateBeginEnd.Size = new System.Drawing.Size(420, 25);
            this.tableLayoutDateBeginEnd.TabIndex = 1;
            // 
            // labelBeginCaption
            // 
            this.labelBeginCaption.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelBeginCaption.Location = new System.Drawing.Point(40, 3);
            this.labelBeginCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelBeginCaption.Name = "labelBeginCaption";
            this.labelBeginCaption.Size = new System.Drawing.Size(14, 18);
            this.labelBeginCaption.TabIndex = 23;
            this.labelBeginCaption.Text = "с";
            // 
            // ultraCalendarBegin
            // 
            this.ultraCalendarBegin.DateButtons.Add(dateButton1);
            this.ultraCalendarBegin.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraCalendarBegin.Location = new System.Drawing.Point(56, 1);
            this.ultraCalendarBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarBegin.Name = "ultraCalendarBegin";
            this.ultraCalendarBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarBegin.TabIndex = 24;
            this.ultraCalendarBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // ultraCalendarEnd
            // 
            this.ultraCalendarEnd.DateButtons.Add(dateButton2);
            this.ultraCalendarEnd.Location = new System.Drawing.Point(262, 1);
            this.ultraCalendarEnd.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarEnd.Name = "ultraCalendarEnd";
            this.ultraCalendarEnd.NonAutoSizeHeight = 21;
            this.ultraCalendarEnd.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarEnd.TabIndex = 26;
            this.ultraCalendarEnd.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelEndCaption
            // 
            this.labelEndCaption.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelEndCaption.Location = new System.Drawing.Point(240, 3);
            this.labelEndCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelEndCaption.Name = "labelEndCaption";
            this.labelEndCaption.Size = new System.Drawing.Size(20, 19);
            this.labelEndCaption.TabIndex = 25;
            this.labelEndCaption.Text = "по";
            // 
            // tabList
            // 
            this.tabList.Controls.Add(this.tableLayoutDateCreateReport);
            this.tabList.Location = new System.Drawing.Point(-10000, -10000);
            this.tabList.Name = "tabList";
            this.tabList.Size = new System.Drawing.Size(420, 25);
            // 
            // tableLayoutDateCreateReport
            // 
            this.tableLayoutDateCreateReport.ColumnCount = 3;
            this.tableLayoutDateCreateReport.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tableLayoutDateCreateReport.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutDateCreateReport.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 288F));
            this.tableLayoutDateCreateReport.Controls.Add(this.ultraCalendarCreateReport, 1, 0);
            this.tableLayoutDateCreateReport.Controls.Add(this.labelDateCreateReportCaption, 0, 0);
            this.tableLayoutDateCreateReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutDateCreateReport.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutDateCreateReport.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutDateCreateReport.Name = "tableLayoutDateCreateReport";
            this.tableLayoutDateCreateReport.RowCount = 1;
            this.tableLayoutDateCreateReport.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutDateCreateReport.Size = new System.Drawing.Size(420, 25);
            this.tableLayoutDateCreateReport.TabIndex = 4;
            // 
            // ultraCalendarCreateReport
            // 
            this.ultraCalendarCreateReport.DateButtons.Add(dateButton3);
            this.ultraCalendarCreateReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraCalendarCreateReport.Location = new System.Drawing.Point(261, 1);
            this.ultraCalendarCreateReport.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarCreateReport.Name = "ultraCalendarCreateReport";
            this.ultraCalendarCreateReport.NonAutoSizeHeight = 23;
            this.ultraCalendarCreateReport.Size = new System.Drawing.Size(148, 23);
            this.ultraCalendarCreateReport.TabIndex = 1;
            this.ultraCalendarCreateReport.Value = new System.DateTime(2014, 12, 8, 0, 0, 0, 0);
            // 
            // labelDateCreateReportCaption
            // 
            this.labelDateCreateReportCaption.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelDateCreateReportCaption.Location = new System.Drawing.Point(49, 2);
            this.labelDateCreateReportCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDateCreateReportCaption.Name = "labelDateCreateReportCaption";
            this.labelDateCreateReportCaption.Size = new System.Drawing.Size(210, 21);
            this.labelDateCreateReportCaption.TabIndex = 0;
            this.labelDateCreateReportCaption.Text = "Дата, на которую формируется отчет: ";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(420, 25);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelFirstRow, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutFederalDistrict, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutRegion, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutInspection, 0, 4);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 6;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(774, 110);
            this.tableLayoutPanelMain.TabIndex = 33;
            // 
            // tableLayoutPanelFirstRow
            // 
            this.tableLayoutPanelFirstRow.ColumnCount = 4;
            this.tableLayoutPanelFirstRow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanelFirstRow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelFirstRow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 420F));
            this.tableLayoutPanelFirstRow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanelFirstRow.Controls.Add(this.firstRowSelector, 2, 0);
            this.tableLayoutPanelFirstRow.Controls.Add(this.ultraLabelReportingPeriod, 0, 0);
            this.tableLayoutPanelFirstRow.Controls.Add(this.comboReportingPeriod, 1, 0);
            this.tableLayoutPanelFirstRow.Location = new System.Drawing.Point(0, 5);
            this.tableLayoutPanelFirstRow.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelFirstRow.Name = "tableLayoutPanelFirstRow";
            this.tableLayoutPanelFirstRow.RowCount = 1;
            this.tableLayoutPanelFirstRow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFirstRow.Size = new System.Drawing.Size(763, 25);
            this.tableLayoutPanelFirstRow.TabIndex = 0;
            // 
            // firstRowSelector
            // 
            this.firstRowSelector.Controls.Add(this.styleSharedTab);
            this.firstRowSelector.Controls.Add(this.tabList);
            this.firstRowSelector.Controls.Add(this.tabEdit);
            this.firstRowSelector.Controls.Add(this.ultraTabPageControl3);
            this.firstRowSelector.Location = new System.Drawing.Point(270, 0);
            this.firstRowSelector.Margin = new System.Windows.Forms.Padding(0);
            this.firstRowSelector.Name = "firstRowSelector";
            this.firstRowSelector.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.firstRowSelector.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.firstRowSelector.Size = new System.Drawing.Size(420, 25);
            this.firstRowSelector.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.firstRowSelector.TabIndex = 34;
            ultraTab3.TabPage = this.tabEdit;
            ultraTab3.Text = "tabDateBeginEnd";
            ultraTab1.TabPage = this.tabList;
            ultraTab1.Text = "tabDateCurrent";
            ultraTab2.TabPage = this.ultraTabPageControl3;
            ultraTab2.Text = "tab1";
            this.firstRowSelector.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab1,
            ultraTab2});
            this.firstRowSelector.TabStop = false;
            // 
            // styleSharedTab
            // 
            this.styleSharedTab.Location = new System.Drawing.Point(-10000, -10000);
            this.styleSharedTab.Name = "styleSharedTab";
            this.styleSharedTab.Size = new System.Drawing.Size(420, 25);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(420, 25);
            // 
            // ultraLabelReportingPeriod
            // 
            this.ultraLabelReportingPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelReportingPeriod.Location = new System.Drawing.Point(1, 1);
            this.ultraLabelReportingPeriod.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelReportingPeriod.Name = "ultraLabelReportingPeriod";
            this.ultraLabelReportingPeriod.Size = new System.Drawing.Size(118, 23);
            this.ultraLabelReportingPeriod.TabIndex = 22;
            this.ultraLabelReportingPeriod.Text = "Отчетный период:";
            // 
            // comboReportingPeriod
            // 
            this.comboReportingPeriod.DisplayMember = "NAME";
            this.comboReportingPeriod.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboReportingPeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboReportingPeriod.Location = new System.Drawing.Point(121, 1);
            this.comboReportingPeriod.Margin = new System.Windows.Forms.Padding(1);
            this.comboReportingPeriod.Name = "comboReportingPeriod";
            this.comboReportingPeriod.Size = new System.Drawing.Size(148, 21);
            this.comboReportingPeriod.TabIndex = 21;
            // 
            // tableLayoutFederalDistrict
            // 
            this.tableLayoutFederalDistrict.ColumnCount = 3;
            this.tableLayoutFederalDistrict.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutFederalDistrict.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 560F));
            this.tableLayoutFederalDistrict.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutFederalDistrict.Controls.Add(this.ultraLabelFederalDistrict, 0, 0);
            this.tableLayoutFederalDistrict.Controls.Add(this.ultraComboFederalDistrict, 1, 0);
            this.tableLayoutFederalDistrict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutFederalDistrict.Location = new System.Drawing.Point(0, 32);
            this.tableLayoutFederalDistrict.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.tableLayoutFederalDistrict.Name = "tableLayoutFederalDistrict";
            this.tableLayoutFederalDistrict.RowCount = 1;
            this.tableLayoutFederalDistrict.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFederalDistrict.Size = new System.Drawing.Size(774, 24);
            this.tableLayoutFederalDistrict.TabIndex = 1;
            // 
            // ultraLabelFederalDistrict
            // 
            this.ultraLabelFederalDistrict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelFederalDistrict.Location = new System.Drawing.Point(1, 1);
            this.ultraLabelFederalDistrict.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelFederalDistrict.Name = "ultraLabelFederalDistrict";
            this.ultraLabelFederalDistrict.Size = new System.Drawing.Size(118, 22);
            this.ultraLabelFederalDistrict.TabIndex = 27;
            this.ultraLabelFederalDistrict.Text = "Федеральный округ:";
            // 
            // ultraComboFederalDistrict
            // 
            this.ultraComboFederalDistrict.CheckedListSettings.CheckStateMember = "";
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraComboFederalDistrict.DisplayLayout.Appearance = appearance13;
            ultraGridColumn1.Header.Caption = "Название";
            ultraGridColumn1.Header.VisiblePosition = 2;
            ultraGridColumn1.Width = 400;
            ultraGridColumn2.DataType = typeof(bool);
            ultraGridColumn2.Header.Caption = "";
            ultraGridColumn2.Header.VisiblePosition = 0;
            ultraGridColumn2.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn2.Width = 30;
            ultraGridColumn3.Header.Caption = "Код";
            ultraGridColumn3.Header.VisiblePosition = 1;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4});
            this.ultraComboFederalDistrict.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraComboFederalDistrict.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboFederalDistrict.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboFederalDistrict.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.ultraComboFederalDistrict.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboFederalDistrict.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraComboFederalDistrict.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            this.ultraComboFederalDistrict.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraComboFederalDistrict.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraComboFederalDistrict.DisplayLayout.Override.CellAppearance = appearance21;
            this.ultraComboFederalDistrict.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraComboFederalDistrict.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboFederalDistrict.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.ultraComboFederalDistrict.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.ultraComboFederalDistrict.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraComboFederalDistrict.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.ultraComboFederalDistrict.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraComboFederalDistrict.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraComboFederalDistrict.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraComboFederalDistrict.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraComboFederalDistrict.DisplayMember = "FullName";
            this.ultraComboFederalDistrict.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraComboFederalDistrict.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.ultraComboFederalDistrict.Location = new System.Drawing.Point(121, 1);
            this.ultraComboFederalDistrict.Margin = new System.Windows.Forms.Padding(1);
            this.ultraComboFederalDistrict.Name = "ultraComboFederalDistrict";
            this.ultraComboFederalDistrict.Size = new System.Drawing.Size(558, 22);
            this.ultraComboFederalDistrict.TabIndex = 28;
            this.ultraComboFederalDistrict.UseAppStyling = false;
            // 
            // tableLayoutRegion
            // 
            this.tableLayoutRegion.ColumnCount = 3;
            this.tableLayoutRegion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutRegion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 560F));
            this.tableLayoutRegion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutRegion.Controls.Add(this.labelRegionCaption, 0, 0);
            this.tableLayoutRegion.Controls.Add(this.ultraComboRegion, 1, 0);
            this.tableLayoutRegion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutRegion.Location = new System.Drawing.Point(0, 57);
            this.tableLayoutRegion.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutRegion.Name = "tableLayoutRegion";
            this.tableLayoutRegion.RowCount = 1;
            this.tableLayoutRegion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutRegion.Size = new System.Drawing.Size(774, 26);
            this.tableLayoutRegion.TabIndex = 2;
            // 
            // labelRegionCaption
            // 
            this.labelRegionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRegionCaption.Location = new System.Drawing.Point(1, 1);
            this.labelRegionCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelRegionCaption.Name = "labelRegionCaption";
            this.labelRegionCaption.Size = new System.Drawing.Size(118, 24);
            this.labelRegionCaption.TabIndex = 29;
            this.labelRegionCaption.Text = "Регион:";
            // 
            // ultraComboRegion
            // 
            this.ultraComboRegion.CheckedListSettings.CheckStateMember = "";
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraComboRegion.DisplayLayout.Appearance = appearance1;
            ultraGridColumn13.Header.Caption = "Название";
            ultraGridColumn13.Header.VisiblePosition = 2;
            ultraGridColumn13.Width = 440;
            ultraGridColumn14.Header.Caption = "Код";
            ultraGridColumn14.Header.VisiblePosition = 1;
            ultraGridColumn14.Width = 50;
            ultraGridColumn15.Header.VisiblePosition = 3;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn15.TabIndex = 1;
            ultraGridColumn15.Width = 100;
            ultraGridColumn16.DataType = typeof(bool);
            ultraGridColumn16.Header.Caption = "";
            ultraGridColumn16.Header.VisiblePosition = 0;
            ultraGridColumn16.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn16.TabIndex = 0;
            ultraGridColumn16.Width = 30;
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn5.Hidden = true;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn5});
            this.ultraComboRegion.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ultraComboRegion.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraComboRegion.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboRegion.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraComboRegion.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraComboRegion.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraComboRegion.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraComboRegion.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraComboRegion.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraComboRegion.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraComboRegion.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraComboRegion.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraComboRegion.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraComboRegion.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraComboRegion.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraComboRegion.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraComboRegion.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraComboRegion.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraComboRegion.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraComboRegion.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraComboRegion.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraComboRegion.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraComboRegion.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraComboRegion.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraComboRegion.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraComboRegion.DisplayMember = "FullName";
            this.ultraComboRegion.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraComboRegion.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.ultraComboRegion.Location = new System.Drawing.Point(121, 1);
            this.ultraComboRegion.Margin = new System.Windows.Forms.Padding(1);
            this.ultraComboRegion.Name = "ultraComboRegion";
            this.ultraComboRegion.Size = new System.Drawing.Size(558, 22);
            this.ultraComboRegion.TabIndex = 30;
            this.ultraComboRegion.UseAppStyling = false;
            // 
            // tableLayoutInspection
            // 
            this.tableLayoutInspection.ColumnCount = 3;
            this.tableLayoutInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 560F));
            this.tableLayoutInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutInspection.Controls.Add(this.ultraLabelInspection, 0, 0);
            this.tableLayoutInspection.Controls.Add(this.ucInspection, 1, 0);
            this.tableLayoutInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutInspection.Location = new System.Drawing.Point(0, 83);
            this.tableLayoutInspection.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutInspection.Name = "tableLayoutInspection";
            this.tableLayoutInspection.RowCount = 1;
            this.tableLayoutInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutInspection.Size = new System.Drawing.Size(774, 26);
            this.tableLayoutInspection.TabIndex = 3;
            // 
            // ultraLabelInspection
            // 
            this.ultraLabelInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabelInspection.Location = new System.Drawing.Point(1, 1);
            this.ultraLabelInspection.Margin = new System.Windows.Forms.Padding(1);
            this.ultraLabelInspection.Name = "ultraLabelInspection";
            this.ultraLabelInspection.Size = new System.Drawing.Size(118, 24);
            this.ultraLabelInspection.TabIndex = 31;
            this.ultraLabelInspection.Text = "Инспекция:";
            // 
            // ucInspection
            // 
            this.ucInspection.CheckedListSettings.CheckStateMember = "";
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ucInspection.DisplayLayout.Appearance = appearance14;
            ultraGridColumn17.Header.Caption = "Код";
            ultraGridColumn17.Header.VisiblePosition = 0;
            ultraGridColumn18.Header.VisiblePosition = 1;
            ultraGridColumn18.Hidden = true;
            ultraGridColumn19.Header.Caption = "Название";
            ultraGridColumn19.Header.VisiblePosition = 2;
            ultraGridColumn19.Width = 400;
            ultraGridColumn20.Header.VisiblePosition = 3;
            ultraGridColumn20.Hidden = true;
            ultraGridColumn21.DataType = typeof(bool);
            ultraGridColumn21.Header.Caption = "";
            ultraGridColumn21.Header.VisiblePosition = 4;
            ultraGridColumn21.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21});
            this.ucInspection.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.ucInspection.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ucInspection.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.ucInspection.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucInspection.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.ucInspection.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ucInspection.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.ucInspection.DisplayLayout.MaxColScrollRegions = 1;
            this.ucInspection.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ucInspection.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ucInspection.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.ucInspection.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ucInspection.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.ucInspection.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ucInspection.DisplayLayout.Override.CellAppearance = appearance43;
            this.ucInspection.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ucInspection.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.ucInspection.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.ucInspection.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.ucInspection.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ucInspection.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.ucInspection.DisplayLayout.Override.RowAppearance = appearance46;
            this.ucInspection.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ucInspection.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.ucInspection.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ucInspection.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ucInspection.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ucInspection.DisplayMember = "FullName";
            this.ucInspection.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucInspection.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.ucInspection.Location = new System.Drawing.Point(121, 1);
            this.ucInspection.Margin = new System.Windows.Forms.Padding(1);
            this.ucInspection.Name = "ucInspection";
            this.ucInspection.Size = new System.Drawing.Size(558, 22);
            this.ucInspection.TabIndex = 32;
            this.ucInspection.UseAppStyling = false;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(420, 25);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(420, 25);
            // 
            // InspectorMonitoringWorkSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "InspectorMonitoringWorkSearch";
            this.Size = new System.Drawing.Size(774, 111);
            this.Load += new System.EventHandler(this.InspectorMonitoringWorkSearch_Load);
            this.tabEdit.ResumeLayout(false);
            this.tableLayoutDateBeginEnd.ResumeLayout(false);
            this.tableLayoutDateBeginEnd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).EndInit();
            this.tabList.ResumeLayout(false);
            this.tableLayoutDateCreateReport.ResumeLayout(false);
            this.tableLayoutDateCreateReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCreateReport)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelFirstRow.ResumeLayout(false);
            this.tableLayoutPanelFirstRow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstRowSelector)).EndInit();
            this.firstRowSelector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboReportingPeriod)).EndInit();
            this.tableLayoutFederalDistrict.ResumeLayout(false);
            this.tableLayoutFederalDistrict.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboFederalDistrict)).EndInit();
            this.tableLayoutRegion.ResumeLayout(false);
            this.tableLayoutRegion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboRegion)).EndInit();
            this.tableLayoutInspection.ResumeLayout(false);
            this.tableLayoutInspection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ucInspection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabelInspection;
        private Infragistics.Win.UltraWinGrid.UltraCombo ucInspection;
        private Infragistics.Win.Misc.UltraLabel labelRegionCaption;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraComboRegion;
        private Infragistics.Win.Misc.UltraLabel ultraLabelFederalDistrict;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraComboFederalDistrict;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutFederalDistrict;
        private System.Windows.Forms.TableLayoutPanel tableLayoutRegion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutInspection;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFirstRow;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl firstRowSelector;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage styleSharedTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutDateCreateReport;
        private Infragistics.Win.Misc.UltraLabel labelDateCreateReportCaption;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarCreateReport;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabEdit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutDateBeginEnd;
        private Infragistics.Win.Misc.UltraLabel labelBeginCaption;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarBegin;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarEnd;
        private Infragistics.Win.Misc.UltraLabel labelEndCaption;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.Misc.UltraLabel ultraLabelReportingPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboReportingPeriod;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;

    }
}
