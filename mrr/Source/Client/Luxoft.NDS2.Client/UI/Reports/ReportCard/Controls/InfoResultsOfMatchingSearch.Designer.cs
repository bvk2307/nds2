﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class InfoResultsOfMatchingSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Description", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 3);
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton3 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Year", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CODE", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NAME", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DateBegin", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DateEnd", 4);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 5);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 6);
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageCountry = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageFederalDistrict = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelFederalDistrict = new Infragistics.Win.Misc.UltraLabel();
            this.comboFederalDistrict = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTabPageRegion = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.lookupMultiRegion = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.ultraTabPageInspection = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lookupMultiInspection = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.ultraTabPageOneDay = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraCalendarOneDay = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateOneDay = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePeriod = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraCalendarBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraCalendarEnd = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelDateBegin = new Infragistics.Win.Misc.UltraLabel();
            this.labelDateEnd = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControlNO = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPageNO = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.labelAggragationLevelNO = new Infragistics.Win.Misc.UltraLabel();
            this.comboAggregateLevelNO = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.comboNalogPeriod = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabelNalogPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.labelTimePeriod = new Infragistics.Win.Misc.UltraLabel();
            this.comboTimePeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraTabControlTimePeriod = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageFederalDistrict.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).BeginInit();
            this.ultraTabPageRegion.SuspendLayout();
            this.ultraTabPageInspection.SuspendLayout();
            this.ultraTabPageOneDay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarOneDay)).BeginInit();
            this.ultraTabPagePeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).BeginInit();
            this.ultraTabControlNO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboNalogPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTimePeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlTimePeriod)).BeginInit();
            this.ultraTabControlTimePeriod.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageCountry
            // 
            this.ultraTabPageCountry.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageCountry.Name = "ultraTabPageCountry";
            this.ultraTabPageCountry.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraTabPageFederalDistrict
            // 
            this.ultraTabPageFederalDistrict.Controls.Add(this.ultraLabelFederalDistrict);
            this.ultraTabPageFederalDistrict.Controls.Add(this.comboFederalDistrict);
            this.ultraTabPageFederalDistrict.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageFederalDistrict.Name = "ultraTabPageFederalDistrict";
            this.ultraTabPageFederalDistrict.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelFederalDistrict
            // 
            this.ultraLabelFederalDistrict.Location = new System.Drawing.Point(1, 3);
            this.ultraLabelFederalDistrict.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelFederalDistrict.Name = "ultraLabelFederalDistrict";
            this.ultraLabelFederalDistrict.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelFederalDistrict.TabIndex = 76;
            this.ultraLabelFederalDistrict.Text = "Федеральные округа:";
            // 
            // comboFederalDistrict
            // 
            this.comboFederalDistrict.CheckedListSettings.CheckStateMember = "";
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboFederalDistrict.DisplayLayout.Appearance = appearance2;
            ultraGridColumn5.Header.Caption = "Название";
            ultraGridColumn5.Header.VisiblePosition = 2;
            ultraGridColumn5.Width = 400;
            ultraGridColumn6.DataType = typeof(bool);
            ultraGridColumn6.Header.Caption = "";
            ultraGridColumn6.Header.VisiblePosition = 0;
            ultraGridColumn6.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn6.Width = 30;
            ultraGridColumn7.Header.Caption = "Код";
            ultraGridColumn7.Header.VisiblePosition = 1;
            ultraGridColumn7.Hidden = true;
            ultraGridColumn8.Header.VisiblePosition = 3;
            ultraGridColumn8.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8});
            this.comboFederalDistrict.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.comboFederalDistrict.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboFederalDistrict.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboFederalDistrict.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboFederalDistrict.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboFederalDistrict.DisplayLayout.Override.CellAppearance = appearance7;
            this.comboFederalDistrict.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboFederalDistrict.DisplayLayout.Override.CellPadding = 0;
            appearance8.BackColor = System.Drawing.SystemColors.Control;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.comboFederalDistrict.DisplayLayout.Override.GroupByRowAppearance = appearance8;
            appearance9.TextHAlignAsString = "Left";
            this.comboFederalDistrict.DisplayLayout.Override.HeaderAppearance = appearance9;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboFederalDistrict.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.comboFederalDistrict.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.comboFederalDistrict.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboFederalDistrict.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboFederalDistrict.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboFederalDistrict.DisplayMember = "FullName";
            this.comboFederalDistrict.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboFederalDistrict.Location = new System.Drawing.Point(1, 21);
            this.comboFederalDistrict.Margin = new System.Windows.Forms.Padding(1);
            this.comboFederalDistrict.Name = "comboFederalDistrict";
            this.comboFederalDistrict.Size = new System.Drawing.Size(445, 22);
            this.comboFederalDistrict.TabIndex = 75;
            this.comboFederalDistrict.UseAppStyling = false;
            // 
            // ultraTabPageRegion
            // 
            this.ultraTabPageRegion.Controls.Add(this.ultraLabelRegion);
            this.ultraTabPageRegion.Controls.Add(this.lookupMultiRegion);
            this.ultraTabPageRegion.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageRegion.Name = "ultraTabPageRegion";
            this.ultraTabPageRegion.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabelRegion
            // 
            this.ultraLabelRegion.Location = new System.Drawing.Point(2, 2);
            this.ultraLabelRegion.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelRegion.Name = "ultraLabelRegion";
            this.ultraLabelRegion.Size = new System.Drawing.Size(232, 17);
            this.ultraLabelRegion.TabIndex = 75;
            this.ultraLabelRegion.Text = "Регионы:";
            // 
            // lookupMultiRegion
            // 
            this.lookupMultiRegion.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiRegion.Name = "lookupMultiRegion";
            this.lookupMultiRegion.PopupHeight = 400;
            this.lookupMultiRegion.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiRegion.TabIndex = 1;
            // 
            // ultraTabPageInspection
            // 
            this.ultraTabPageInspection.Controls.Add(this.ultraLabel1);
            this.ultraTabPageInspection.Controls.Add(this.lookupMultiInspection);
            this.ultraTabPageInspection.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageInspection.Name = "ultraTabPageInspection";
            this.ultraTabPageInspection.Size = new System.Drawing.Size(447, 49);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(2, 2);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(232, 17);
            this.ultraLabel1.TabIndex = 73;
            this.ultraLabel1.Text = "Инспекции:";
            // 
            // lookupMultiInspection
            // 
            this.lookupMultiInspection.Location = new System.Drawing.Point(1, 20);
            this.lookupMultiInspection.Name = "lookupMultiInspection";
            this.lookupMultiInspection.PopupHeight = 400;
            this.lookupMultiInspection.Size = new System.Drawing.Size(445, 26);
            this.lookupMultiInspection.TabIndex = 2;
            // 
            // ultraTabPageOneDay
            // 
            this.ultraTabPageOneDay.Controls.Add(this.ultraCalendarOneDay);
            this.ultraTabPageOneDay.Controls.Add(this.labelDateOneDay);
            this.ultraTabPageOneDay.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageOneDay.Name = "ultraTabPageOneDay";
            this.ultraTabPageOneDay.Size = new System.Drawing.Size(236, 101);
            // 
            // ultraCalendarOneDay
            // 
            this.ultraCalendarOneDay.DateButtons.Add(dateButton1);
            this.ultraCalendarOneDay.Location = new System.Drawing.Point(6, 30);
            this.ultraCalendarOneDay.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarOneDay.Name = "ultraCalendarOneDay";
            this.ultraCalendarOneDay.NonAutoSizeHeight = 21;
            this.ultraCalendarOneDay.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarOneDay.TabIndex = 90;
            this.ultraCalendarOneDay.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateOneDay
            // 
            this.labelDateOneDay.Location = new System.Drawing.Point(6, 0);
            this.labelDateOneDay.Name = "labelDateOneDay";
            this.labelDateOneDay.Size = new System.Drawing.Size(148, 29);
            this.labelDateOneDay.TabIndex = 91;
            this.labelDateOneDay.Text = "Дата, на которую формируется отчёт:";
            // 
            // ultraTabPagePeriod
            // 
            this.ultraTabPagePeriod.Controls.Add(this.ultraCalendarBegin);
            this.ultraTabPagePeriod.Controls.Add(this.ultraCalendarEnd);
            this.ultraTabPagePeriod.Controls.Add(this.labelDateBegin);
            this.ultraTabPagePeriod.Controls.Add(this.labelDateEnd);
            this.ultraTabPagePeriod.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePeriod.Name = "ultraTabPagePeriod";
            this.ultraTabPagePeriod.Size = new System.Drawing.Size(236, 101);
            // 
            // ultraCalendarBegin
            // 
            this.ultraCalendarBegin.DateButtons.Add(dateButton2);
            this.ultraCalendarBegin.Location = new System.Drawing.Point(6, 30);
            this.ultraCalendarBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarBegin.Name = "ultraCalendarBegin";
            this.ultraCalendarBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarBegin.TabIndex = 88;
            this.ultraCalendarBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // ultraCalendarEnd
            // 
            this.ultraCalendarEnd.DateButtons.Add(dateButton3);
            this.ultraCalendarEnd.Location = new System.Drawing.Point(6, 75);
            this.ultraCalendarEnd.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarEnd.Name = "ultraCalendarEnd";
            this.ultraCalendarEnd.NonAutoSizeHeight = 21;
            this.ultraCalendarEnd.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarEnd.TabIndex = 90;
            this.ultraCalendarEnd.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelDateBegin
            // 
            this.labelDateBegin.Location = new System.Drawing.Point(7, 12);
            this.labelDateBegin.Name = "labelDateBegin";
            this.labelDateBegin.Size = new System.Drawing.Size(144, 17);
            this.labelDateBegin.TabIndex = 89;
            this.labelDateBegin.Text = "Дата начала периода:";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.Location = new System.Drawing.Point(7, 57);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(148, 17);
            this.labelDateEnd.TabIndex = 91;
            this.labelDateEnd.Text = "Дата окончания периода:";
            // 
            // ultraTabControlNO
            // 
            this.ultraTabControlNO.Controls.Add(this.ultraTabSharedControlsPageNO);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageCountry);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageFederalDistrict);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageRegion);
            this.ultraTabControlNO.Controls.Add(this.ultraTabPageInspection);
            this.ultraTabControlNO.Location = new System.Drawing.Point(10, 57);
            this.ultraTabControlNO.Name = "ultraTabControlNO";
            this.ultraTabControlNO.SharedControlsPage = this.ultraTabSharedControlsPageNO;
            this.ultraTabControlNO.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.ultraTabControlNO.Size = new System.Drawing.Size(447, 49);
            this.ultraTabControlNO.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.ultraTabControlNO.TabIndex = 81;
            ultraTab4.Key = "tContry";
            ultraTab4.TabPage = this.ultraTabPageCountry;
            ultraTab4.Text = "tabContry";
            ultraTab5.Key = "tFederalDistrict";
            ultraTab5.TabPage = this.ultraTabPageFederalDistrict;
            ultraTab5.Text = "tabFederalDistrict";
            ultraTab6.Key = "tRegion";
            ultraTab6.TabPage = this.ultraTabPageRegion;
            ultraTab6.Text = "tabRegion";
            ultraTab7.Key = "tInspection";
            ultraTab7.TabPage = this.ultraTabPageInspection;
            ultraTab7.Text = "tabInspection";
            this.ultraTabControlNO.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7});
            this.ultraTabControlNO.UseAppStyling = false;
            // 
            // ultraTabSharedControlsPageNO
            // 
            this.ultraTabSharedControlsPageNO.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPageNO.Name = "ultraTabSharedControlsPageNO";
            this.ultraTabSharedControlsPageNO.Size = new System.Drawing.Size(447, 49);
            // 
            // labelAggragationLevelNO
            // 
            this.labelAggragationLevelNO.Location = new System.Drawing.Point(11, 15);
            this.labelAggragationLevelNO.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelAggragationLevelNO.Name = "labelAggragationLevelNO";
            this.labelAggragationLevelNO.Size = new System.Drawing.Size(232, 17);
            this.labelAggragationLevelNO.TabIndex = 80;
            this.labelAggragationLevelNO.Text = "Уровень агрегации по налоговым органам:";
            // 
            // comboAggregateLevelNO
            // 
            this.comboAggregateLevelNO.DisplayMember = "NAME";
            this.comboAggregateLevelNO.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboAggregateLevelNO.Location = new System.Drawing.Point(11, 33);
            this.comboAggregateLevelNO.Margin = new System.Windows.Forms.Padding(1);
            this.comboAggregateLevelNO.Name = "comboAggregateLevelNO";
            this.comboAggregateLevelNO.Size = new System.Drawing.Size(270, 21);
            this.comboAggregateLevelNO.TabIndex = 79;
            // 
            // comboNalogPeriod
            // 
            this.comboNalogPeriod.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            this.comboNalogPeriod.CheckedListSettings.CheckStateMember = "";
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboNalogPeriod.DisplayLayout.Appearance = appearance10;
            ultraGridColumn9.Header.VisiblePosition = 0;
            ultraGridColumn9.Hidden = true;
            ultraGridColumn10.Header.VisiblePosition = 5;
            ultraGridColumn10.Hidden = true;
            ultraGridColumn11.Header.Caption = "Название";
            ultraGridColumn11.Header.VisiblePosition = 2;
            ultraGridColumn11.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn12.Header.VisiblePosition = 3;
            ultraGridColumn12.Hidden = true;
            ultraGridColumn13.Header.VisiblePosition = 4;
            ultraGridColumn13.Hidden = true;
            ultraGridColumn14.Header.Caption = "";
            ultraGridColumn14.Header.VisiblePosition = 1;
            ultraGridColumn14.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn14.Width = 40;
            ultraGridColumn15.Header.VisiblePosition = 6;
            ultraGridColumn15.Hidden = true;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15});
            this.comboNalogPeriod.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.comboNalogPeriod.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.comboNalogPeriod.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.comboNalogPeriod.DisplayLayout.GroupByBox.Appearance = appearance11;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboNalogPeriod.DisplayLayout.GroupByBox.BandLabelAppearance = appearance13;
            this.comboNalogPeriod.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboNalogPeriod.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.comboNalogPeriod.DisplayLayout.MaxColScrollRegions = 1;
            this.comboNalogPeriod.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboNalogPeriod.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.comboNalogPeriod.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.comboNalogPeriod.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboNalogPeriod.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.comboNalogPeriod.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboNalogPeriod.DisplayLayout.Override.CellAppearance = appearance24;
            this.comboNalogPeriod.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboNalogPeriod.DisplayLayout.Override.CellPadding = 0;
            appearance15.BackColor = System.Drawing.SystemColors.Control;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.comboNalogPeriod.DisplayLayout.Override.GroupByRowAppearance = appearance15;
            appearance14.TextHAlignAsString = "Left";
            this.comboNalogPeriod.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.comboNalogPeriod.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboNalogPeriod.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.comboNalogPeriod.DisplayLayout.Override.RowAppearance = appearance27;
            this.comboNalogPeriod.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comboNalogPeriod.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.comboNalogPeriod.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboNalogPeriod.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboNalogPeriod.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboNalogPeriod.DisplayMember = "Name";
            this.comboNalogPeriod.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboNalogPeriod.Location = new System.Drawing.Point(294, 33);
            this.comboNalogPeriod.Name = "comboNalogPeriod";
            this.comboNalogPeriod.Size = new System.Drawing.Size(162, 22);
            this.comboNalogPeriod.TabIndex = 83;
            this.comboNalogPeriod.UseAppStyling = false;
            // 
            // ultraLabelNalogPeriod
            // 
            this.ultraLabelNalogPeriod.Location = new System.Drawing.Point(296, 14);
            this.ultraLabelNalogPeriod.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.ultraLabelNalogPeriod.Name = "ultraLabelNalogPeriod";
            this.ultraLabelNalogPeriod.Size = new System.Drawing.Size(147, 17);
            this.ultraLabelNalogPeriod.TabIndex = 85;
            this.ultraLabelNalogPeriod.Text = "Налоговые периоды:";
            // 
            // labelTimePeriod
            // 
            this.labelTimePeriod.Location = new System.Drawing.Point(478, 15);
            this.labelTimePeriod.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.labelTimePeriod.Name = "labelTimePeriod";
            this.labelTimePeriod.Size = new System.Drawing.Size(146, 17);
            this.labelTimePeriod.TabIndex = 87;
            this.labelTimePeriod.Text = "Временной период:";
            // 
            // comboTimePeriod
            // 
            this.comboTimePeriod.DisplayMember = "NAME";
            this.comboTimePeriod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTimePeriod.Location = new System.Drawing.Point(478, 33);
            this.comboTimePeriod.Margin = new System.Windows.Forms.Padding(1);
            this.comboTimePeriod.Name = "comboTimePeriod";
            this.comboTimePeriod.Size = new System.Drawing.Size(151, 21);
            this.comboTimePeriod.TabIndex = 86;
            // 
            // ultraTabControlTimePeriod
            // 
            this.ultraTabControlTimePeriod.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControlTimePeriod.Controls.Add(this.ultraTabPageOneDay);
            this.ultraTabControlTimePeriod.Controls.Add(this.ultraTabPagePeriod);
            this.ultraTabControlTimePeriod.Location = new System.Drawing.Point(637, 3);
            this.ultraTabControlTimePeriod.Name = "ultraTabControlTimePeriod";
            this.ultraTabControlTimePeriod.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControlTimePeriod.Size = new System.Drawing.Size(236, 101);
            this.ultraTabControlTimePeriod.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.ultraTabControlTimePeriod.TabIndex = 92;
            ultraTab1.Key = "tOneDay";
            ultraTab1.TabPage = this.ultraTabPageOneDay;
            ultraTab1.Text = "tabOneDay";
            ultraTab2.Key = "tPeriod";
            ultraTab2.TabPage = this.ultraTabPagePeriod;
            ultraTab2.Text = "tabPeriod";
            this.ultraTabControlTimePeriod.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControlTimePeriod.UseAppStyling = false;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(236, 101);
            // 
            // InfoResultsOfMatchingSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelAggragationLevelNO);
            this.Controls.Add(this.comboAggregateLevelNO);
            this.Controls.Add(this.ultraTabControlTimePeriod);
            this.Controls.Add(this.ultraTabControlNO);
            this.Controls.Add(this.labelTimePeriod);
            this.Controls.Add(this.comboTimePeriod);
            this.Controls.Add(this.ultraLabelNalogPeriod);
            this.Controls.Add(this.comboNalogPeriod);
            this.Name = "InfoResultsOfMatchingSearch";
            this.Size = new System.Drawing.Size(925, 113);
            this.Load += new System.EventHandler(this.InfoResultsOfMatchingSearch_Load);
            this.ultraTabPageFederalDistrict.ResumeLayout(false);
            this.ultraTabPageFederalDistrict.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFederalDistrict)).EndInit();
            this.ultraTabPageRegion.ResumeLayout(false);
            this.ultraTabPageInspection.ResumeLayout(false);
            this.ultraTabPageOneDay.ResumeLayout(false);
            this.ultraTabPageOneDay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarOneDay)).EndInit();
            this.ultraTabPagePeriod.ResumeLayout(false);
            this.ultraTabPagePeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlNO)).EndInit();
            this.ultraTabControlNO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevelNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboNalogPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboTimePeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlTimePeriod)).EndInit();
            this.ultraTabControlTimePeriod.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPageNO;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageCountry;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageFederalDistrict;
        private Infragistics.Win.Misc.UltraLabel ultraLabelFederalDistrict;
        private Infragistics.Win.UltraWinGrid.UltraCombo comboFederalDistrict;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageRegion;
        private Infragistics.Win.Misc.UltraLabel ultraLabelRegion;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiRegion;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageInspection;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView lookupMultiInspection;
        private Infragistics.Win.Misc.UltraLabel labelAggragationLevelNO;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboAggregateLevelNO;
        private Infragistics.Win.UltraWinGrid.UltraCombo comboNalogPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabelNalogPeriod;
        private Infragistics.Win.Misc.UltraLabel labelTimePeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTimePeriod;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateBegin;
        private Infragistics.Win.Misc.UltraLabel labelDateEnd;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarEnd;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlTimePeriod;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageOneDay;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarOneDay;
        private Infragistics.Win.Misc.UltraLabel labelDateOneDay;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePeriod;
    }
}
