﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class BaseControlSearch : UserControl, IControlSearchCriteria
    {
        public BaseControlSearch()
        {
            InitializeComponent();
        }

        public event EventHandler ChangeControlParameters;
        public event EventHandler RequireChangeColumns;

        public virtual QueryConditions GetQueryConditions()
        {
            return new QueryConditions();
        }

        public virtual bool IsCorrectParameters()
        {
            return true;
        }

        public virtual void SetupDependingFromReportKey(string reportKey)
        {
        }

        public virtual string GetSearchCriteriaDescription()
        {
            return string.Empty;
        }

        public virtual IEnumerable<ColumnSort> GetQuerySorting(IEnumerable<ColumnSort> list)
        {
            return list;
        }

        protected void GenerateChangeParameters()
        {
            if (ChangeControlParameters != null)
            {
                ChangeControlParameters(this, new EventArgs());
            }
        }

        protected void GenerateRequireChangeColumns()
        {
            if (RequireChangeColumns != null)
            {
                RequireChangeColumns(this, new EventArgs());
            }
        }

    }
}
