﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    partial class DynamicTechnoParameterSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FullName", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Check", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ModuleType", 4);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ModuleName", 5);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.labelDateBegin = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCalendarBegin = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.labelRegion = new Infragistics.Win.Misc.UltraLabel();
            this.labelAggragationLevel = new Infragistics.Win.Misc.UltraLabel();
            this.comboAggregateLevel = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.numericTimeCounting = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.comboDynamicParameter = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnClearParameters = new Infragistics.Win.Misc.UltraButton();
            this.labelMessage = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTimeCounting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboDynamicParameter)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDateBegin
            // 
            this.labelDateBegin.Location = new System.Drawing.Point(11, 25);
            this.labelDateBegin.Name = "labelDateBegin";
            this.labelDateBegin.Size = new System.Drawing.Size(148, 29);
            this.labelDateBegin.TabIndex = 38;
            this.labelDateBegin.Text = "Дата начала построения отчета:";
            // 
            // ultraCalendarBegin
            // 
            this.ultraCalendarBegin.DateButtons.Add(dateButton1);
            this.ultraCalendarBegin.Location = new System.Drawing.Point(11, 55);
            this.ultraCalendarBegin.Margin = new System.Windows.Forms.Padding(1);
            this.ultraCalendarBegin.Name = "ultraCalendarBegin";
            this.ultraCalendarBegin.NonAutoSizeHeight = 21;
            this.ultraCalendarBegin.Size = new System.Drawing.Size(148, 21);
            this.ultraCalendarBegin.TabIndex = 36;
            this.ultraCalendarBegin.Value = new System.DateTime(2014, 9, 11, 0, 0, 0, 0);
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(10, 86);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(187, 23);
            this.labelRegion.TabIndex = 35;
            this.labelRegion.Text = "Технологические параметры:";
            // 
            // labelAggragationLevel
            // 
            this.labelAggragationLevel.Location = new System.Drawing.Point(175, 31);
            this.labelAggragationLevel.Name = "labelAggragationLevel";
            this.labelAggragationLevel.Size = new System.Drawing.Size(157, 23);
            this.labelAggragationLevel.TabIndex = 42;
            this.labelAggragationLevel.Text = "Уровень агрегации:";
            // 
            // comboAggregateLevel
            // 
            this.comboAggregateLevel.DisplayMember = "NAME";
            this.comboAggregateLevel.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboAggregateLevel.Location = new System.Drawing.Point(175, 55);
            this.comboAggregateLevel.Margin = new System.Windows.Forms.Padding(1);
            this.comboAggregateLevel.Name = "comboAggregateLevel";
            this.comboAggregateLevel.Size = new System.Drawing.Size(172, 21);
            this.comboAggregateLevel.TabIndex = 41;
            // 
            // numericTimeCounting
            // 
            this.numericTimeCounting.Location = new System.Drawing.Point(365, 55);
            this.numericTimeCounting.MaxValue = 365;
            this.numericTimeCounting.MinValue = 1;
            this.numericTimeCounting.Name = "numericTimeCounting";
            this.numericTimeCounting.PromptChar = ' ';
            this.numericTimeCounting.Size = new System.Drawing.Size(129, 21);
            this.numericTimeCounting.TabIndex = 43;
            // 
            // comboDynamicParameter
            // 
            this.comboDynamicParameter.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            this.comboDynamicParameter.CheckedListSettings.CheckStateMember = "";
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboDynamicParameter.DisplayLayout.Appearance = appearance1;
            ultraGridColumn13.Header.Caption = "Название";
            ultraGridColumn13.Header.VisiblePosition = 2;
            ultraGridColumn13.Width = 640;
            ultraGridColumn15.Header.VisiblePosition = 3;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn15.TabIndex = 1;
            ultraGridColumn15.Width = 100;
            ultraGridColumn16.DataType = typeof(bool);
            ultraGridColumn16.Header.Caption = "";
            ultraGridColumn16.Header.VisiblePosition = 0;
            ultraGridColumn16.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ultraGridColumn16.TabIndex = 0;
            ultraGridColumn16.Width = 30;
            ultraGridColumn2.Header.VisiblePosition = 4;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn3.Header.VisiblePosition = 5;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn4.Header.Caption = "Модуль";
            ultraGridColumn4.Header.VisiblePosition = 1;
            ultraGridColumn4.Width = 70;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn13,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4});
            this.comboDynamicParameter.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.comboDynamicParameter.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.comboDynamicParameter.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.comboDynamicParameter.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.comboDynamicParameter.DisplayLayout.MaxColScrollRegions = 1;
            this.comboDynamicParameter.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboDynamicParameter.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.comboDynamicParameter.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.comboDynamicParameter.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.comboDynamicParameter.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.comboDynamicParameter.DisplayLayout.Override.CellAppearance = appearance8;
            this.comboDynamicParameter.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.comboDynamicParameter.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.comboDynamicParameter.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.comboDynamicParameter.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.comboDynamicParameter.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.comboDynamicParameter.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.comboDynamicParameter.DisplayLayout.Override.RowAppearance = appearance11;
            this.comboDynamicParameter.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comboDynamicParameter.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.comboDynamicParameter.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.comboDynamicParameter.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.comboDynamicParameter.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.comboDynamicParameter.DisplayMember = "FullName";
            this.comboDynamicParameter.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.comboDynamicParameter.Location = new System.Drawing.Point(11, 110);
            this.comboDynamicParameter.Margin = new System.Windows.Forms.Padding(1);
            this.comboDynamicParameter.Name = "comboDynamicParameter";
            this.comboDynamicParameter.Size = new System.Drawing.Size(687, 22);
            this.comboDynamicParameter.TabIndex = 44;
            this.comboDynamicParameter.UseAppStyling = false;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(365, 25);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(138, 29);
            this.ultraLabel1.TabIndex = 45;
            this.ultraLabel1.Text = "Количество временных отсчетов:";
            // 
            // btnClearParameters
            // 
            this.btnClearParameters.Location = new System.Drawing.Point(509, 53);
            this.btnClearParameters.Name = "btnClearParameters";
            this.btnClearParameters.Size = new System.Drawing.Size(189, 40);
            this.btnClearParameters.TabIndex = 46;
            this.btnClearParameters.Text = "Сбросить технологические параметры";
            this.btnClearParameters.Click += new System.EventHandler(this.btnClearParameters_Click);
            // 
            // labelMessage
            // 
            appearance13.ForeColor = System.Drawing.Color.Red;
            this.labelMessage.Appearance = appearance13;
            this.labelMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMessage.Location = new System.Drawing.Point(0, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(751, 19);
            this.labelMessage.TabIndex = 47;
            this.labelMessage.Text = "Количество выбранных параметров превышает максимальное значение, равное 30";
            // 
            // DynamicTechnoParameterSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.btnClearParameters);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.comboDynamicParameter);
            this.Controls.Add(this.numericTimeCounting);
            this.Controls.Add(this.labelAggragationLevel);
            this.Controls.Add(this.comboAggregateLevel);
            this.Controls.Add(this.labelDateBegin);
            this.Controls.Add(this.ultraCalendarBegin);
            this.Controls.Add(this.labelRegion);
            this.Name = "DynamicTechnoParameterSearch";
            this.Size = new System.Drawing.Size(751, 145);
            this.Load += new System.EventHandler(this.DynamicTechnoParameterSearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboAggregateLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTimeCounting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboDynamicParameter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel labelDateBegin;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarBegin;
        private Infragistics.Win.Misc.UltraLabel labelRegion;
        private Infragistics.Win.Misc.UltraLabel labelAggragationLevel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboAggregateLevel;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numericTimeCounting;
        private Infragistics.Win.UltraWinGrid.UltraCombo comboDynamicParameter;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnClearParameters;
        private Infragistics.Win.Misc.UltraLabel labelMessage;
    }
}
