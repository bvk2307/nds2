﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicTechnoParameter;
using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class DynamicTechnoParameterSearch : BaseControlSearch, IControlSearchCriteria
    {
        #region Переменные

        private List<AggregateLevel> _aggregateLevels = new List<AggregateLevel>();
        private List<AggregateLevel> _aggregateAllLevels = new List<AggregateLevel>();
        private List<DynamicParameter> _dynamicParameters = new List<DynamicParameter>();
        private List<DateTime> _datesTechnoReport = new List<DateTime>(); 

        #endregion

        public DynamicTechnoParameterSearch()
            : base()
        {
            InitializeComponent();
            labelMessage.Text = String.Empty;
        }

        #region Реализация IControlSearchCriteria

        public override string GetSearchCriteriaDescription()
        {
            StringBuilder sb = new StringBuilder();
            if (ultraCalendarBegin.Value != null)
            {
                DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
                sb.Append(FormatDateTime(dtBegin));
                sb.Append(" ");
            }

            AggregateLevel aggregateLevel = GetCurrentAggregateLevel();
            if (aggregateLevel != null)
            {
                sb.Append(aggregateLevel.Name);
                sb.Append(" ");
            }

            object timeCounting = numericTimeCounting.Value;
            if (timeCounting != null)
            {
                sb.Append("(");
                sb.Append(timeCounting.ToString());
                sb.Append(") ");
            }
            return sb.ToString();
        }

        private string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                DateTime dtF = (DateTime)dt;
                ret = string.Format("{0:00}_{1:00}_{2:00}", dtF.Day, dtF.Month, dtF.Year);
            }
            return ret;
        }

        public override QueryConditions GetQueryConditions()
        {
            QueryConditions queryCond = new QueryConditions();

            AggregateLevel aggregateLevel = GetCurrentAggregateLevel();
            if (aggregateLevel != null)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.ColumnName = TaskParamCriteria.DynamicAggregateLevel;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                columnFilter.Value = (int)aggregateLevel.Type;
                filterQuery.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQuery);
            }

            object timeCounting = numericTimeCounting.Value;
            if (timeCounting != null)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.ColumnName = TaskParamCriteria.DynamicTimeCounting;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                columnFilter.Value = timeCounting;
                filterQuery.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQuery);
            }

            if (ultraCalendarBegin.Value != null)
            {
                DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
                FilterQuery filterQueryDTBegin = new FilterQuery();
                filterQueryDTBegin.ColumnName = TaskParamCriteria.DynamicDateBegin;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo;
                columnFilter.Value = dtBegin;
                filterQueryDTBegin.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQueryDTBegin);
            }

            if (_dynamicParameters.Where(p=>p.Check).Count() > 0)
            {
                FilterQuery filterQuery = new FilterQuery();
                filterQuery.ColumnName = TaskParamCriteria.DynamicParameters;
                filterQuery.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                List<DynamicParameter> dynamicParams = GetCurrentDynamicParameters();
                foreach (DynamicParameter item in dynamicParams.Where(p => p.Check))
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Id;
                    filterQuery.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQuery);
            }
            return queryCond;
        }

        public override bool IsCorrectParameters()
        {
            bool ret = true;
            int countParams = _dynamicParameters.Where(p => p.Check).Count();
            if (countParams == 0 || countParams >= 30 || CheckDateLimit())
            {
                ret = false;
            }
            return ret;
        }

        private bool CheckDateLimit()
        {
            bool isDateLimit = false;

            AggregateLevel aggregateLevel = GetCurrentAggregateLevel();
            object timeCounting = numericTimeCounting.Value;
            if (ultraCalendarBegin.Value != null && aggregateLevel != null && timeCounting != null)
            {
                DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
                int timeSc = (int)timeCounting;
                DateTime dtMax = dtBegin.Date;
                if (aggregateLevel.Type == AggregateLevelType.Day)
                {
                    dtMax = dtMax.Date.AddDays(timeSc - 1);
                }
                else if (aggregateLevel.Type == AggregateLevelType.Week)
                {
                    dtMax = dtMax.Date.AddDays(7 * timeSc - 1);
                }
                else if (aggregateLevel.Type == AggregateLevelType.Month)
                {
                    dtMax = dtMax.Date.AddMonths(timeSc);
                }
                if (_datesTechnoReport.Count > 0)
                {
                    DateTime dtMaxReal = _datesTechnoReport.Max();
                    if (dtMax > dtMaxReal)
                    {
                        isDateLimit = true;
                    }
                }
                else
                {
                    isDateLimit = true;
                }
            }
            return isDateLimit;
        }

        private void CheckCorrectParameters()
        {
            string errorMessage = String.Empty;
            if (_dynamicParameters.Where(p => p.Check).Count() >= 30)
            {
                errorMessage = "Количество выбранных технологических параметров превышает максимальное значение, равное 30";
            }
            else
            {
                errorMessage = String.Empty;
            }

            bool isDateLimit = CheckDateLimit();
            if (isDateLimit && string.IsNullOrWhiteSpace(errorMessage))
            {
                errorMessage = "Превышен объем имеющейся информации по статистике технологических параметров";
            }

            labelMessage.Text = errorMessage;
        }

        #endregion

        #region Установка данных

        public void SetAggregateLevels(List<AggregateLevel> aggregateLevels)
        {
            _aggregateAllLevels.Clear();
            _aggregateAllLevels.AddRange(aggregateLevels);
            _aggregateLevels.Clear();
            _aggregateLevels.AddRange(aggregateLevels);
        }

        public void SetDynamicParameters(List<DynamicParameter> dynamicParameters)
        {
            _dynamicParameters.Clear();
            _dynamicParameters.AddRange(dynamicParameters);
        }

        public void SetDatesTechnoReport(List<DateTime> datesTechnoReport)
        {
            _datesTechnoReport.Clear();
            _datesTechnoReport.AddRange(datesTechnoReport);
        }

        #endregion

        #region Инициализация

        private void DynamicTechnoParameterSearch_Load(object sender, EventArgs e)
        {
            InitViewDynamicParameters();
            InitData();
        }

        public void InitData()
        {
            CalculateVisibleAggregateLevel();

            SetDefaultDynamicParameters();

            comboDynamicParameter.DataSource = _dynamicParameters;
            comboDynamicParameter.Refresh();

            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();

            numericTimeCounting.Value = 1;
            DateTime dtDefault = DateTime.Now.Date.AddDays(-1);
            ultraCalendarBegin.Value = dtDefault;
        }

        private void InitViewDynamicParameters()
        {
            this.comboDynamicParameter.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = this.comboDynamicParameter.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            this.comboDynamicParameter.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            this.comboDynamicParameter.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            this.comboDynamicParameter.CheckedListSettings.ListSeparator = " / ";
        }

        private void SetDefaultDynamicParameters()
        {
            List<int> defaultValues = new List<int>() { 1, 5, 7, 9, 11, 101, 103, 107, 108, 105, 110, 111, 112, 113, 114, 995, 996, 997, 1004 };
            foreach (DynamicParameter item in _dynamicParameters.Where(p => defaultValues.Contains(p.Id)))
            {
                item.Check = true;
            }
        }

        #endregion

        #region Вспомогательные методы

        private void AllComboControlsEventRemove()
        {
            this.comboAggregateLevel.ValueChanged -= new EventHandler(this.comboAggregateLevel_ValueChanged);
            this.comboDynamicParameter.ValueChanged -= new EventHandler(this.comboDynamicParameter_ValueChanged);
            this.numericTimeCounting.ValueChanged -= new EventHandler(this.numericTimeCounting_ValueChanged);
        }

        private void AllComboControlsEventAdd()
        {
            this.comboAggregateLevel.ValueChanged += new EventHandler(this.comboAggregateLevel_ValueChanged);
            this.comboDynamicParameter.ValueChanged += new EventHandler(this.comboDynamicParameter_ValueChanged);
            this.numericTimeCounting.ValueChanged += new EventHandler(this.numericTimeCounting_ValueChanged);
        }

        private void DateTimeControlsEventAdd()
        {
            this.ultraCalendarBegin.ValueChanged += new System.EventHandler(this.ultraCalendarBegin_ValueChanged);
            this.ultraCalendarBegin.TextChanged += new System.EventHandler(this.ultraCalendarBegin_TextChanged);
        }

        private AggregateLevel GetCurrentAggregateLevel()
        {
            AggregateLevel aggregateLevelCurrent = null;
            if (comboAggregateLevel.SelectedIndex > -1)
            {
                if (comboAggregateLevel.SelectedIndex < _aggregateLevels.Count)
                {
                    aggregateLevelCurrent = _aggregateLevels[comboAggregateLevel.SelectedIndex];
                }
            }
            return aggregateLevelCurrent;
        }

        private List<DynamicParameter> GetCurrentDynamicParameters()
        {
            List<DynamicParameter> regs;
            if (comboDynamicParameter.DataSource != null)
            {
                regs = (List<DynamicParameter>)comboDynamicParameter.DataSource;
            }
            else { regs = new List<DynamicParameter>(); }
            return regs;
        }

        private void CalculateVisibleAggregateLevel()
        {
            DateTime dtBegin = (DateTime)ultraCalendarBegin.Value;
            bool week = false;
            bool month = false;
            if (_datesTechnoReport.Count > 0)
            {
                DateTime dtMax = _datesTechnoReport.Max(p => p).Date;
                double diffDays = (dtMax - dtBegin).TotalDays;

                if (dtBegin.DayOfWeek == DayOfWeek.Monday && diffDays >= 7 && _datesTechnoReport.Where(p => p.Date >= dtBegin.Date).Count() > 0)
                {
                    week = true;
                }
                if (dtBegin.Day == 1 && diffDays >= 30 && _datesTechnoReport.Where(p => p.Date >= dtBegin.Date).Count() > 0)
                {
                    month = true;
                }
            }

            AggregateLevel beforeSelAggregate = null;
            if (comboAggregateLevel.SelectedIndex > -1)
            {
                beforeSelAggregate = _aggregateLevels[comboAggregateLevel.SelectedIndex];
            }

            _aggregateLevels.Clear();
            _aggregateLevels.AddRange(_aggregateAllLevels.Where(p => p.Type == AggregateLevelType.Day));
            if (week)
            {
                _aggregateLevels.AddRange(_aggregateAllLevels.Where(p => p.Type == AggregateLevelType.Week));
            }
            if (month)
            {
                _aggregateLevels.AddRange(_aggregateAllLevels.Where(p => p.Type == AggregateLevelType.Month));
            }

            comboAggregateLevel.Items.Clear();
            comboAggregateLevel.DataSource = null;
            foreach (AggregateLevel item in _aggregateLevels)
            {
                comboAggregateLevel.Items.Add(item.Type, item.Name);
            }
            comboAggregateLevel.DataSource = _aggregateLevels;
            comboAggregateLevel.Refresh();
            comboAggregateLevel.RefreshList();

            bool isFind = false;
            if (beforeSelAggregate != null)
            {
                for (int i = -1; ++i < comboAggregateLevel.Items.Count; )
                {
                    if (_aggregateLevels[i].Type == beforeSelAggregate.Type)
                    {
                        comboAggregateLevel.SelectedIndex = i;
                        isFind = true;
                        break;
                    }
                }
            }
            if (!isFind)
            {
                if (_aggregateLevels.Count > 0)
                {
                    comboAggregateLevel.SelectedIndex = 0;
                }
                else
                {
                    comboAggregateLevel.SelectedIndex = -1;
                }
            }
        }

        #endregion

        #region События

        private void numericTimeCounting_ValueChanged(object sender, EventArgs e)
        {
            CheckCorrectParameters();
            GenerateChangeParameters();
        }

        private void comboAggregateLevel_ValueChanged(object sender, EventArgs e)
        {
            CheckCorrectParameters();
            GenerateChangeParameters();
        }

        private void comboDynamicParameter_ValueChanged(object sender, EventArgs e)
        {
            CheckCorrectParameters();
            GenerateChangeParameters();
            GenerateRequireChangeColumns();
        }

        private void ultraCalendarBegin_ValueChanged(object sender, EventArgs e)
        {
            CalculateVisibleAggregateLevel();

            GenerateChangeParameters();
        }

        private void ultraCalendarBegin_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void btnClearParameters_Click(object sender, EventArgs e)
        {
            ICheckedItemList itemListDynParameter = (ICheckedItemList)comboDynamicParameter;
            for (int i = 0; i < comboDynamicParameter.Rows.Count; i++)
            {
                DynamicParameter dynParameterCurrent = (DynamicParameter)comboDynamicParameter.Rows[i].ListObject;
                if (dynParameterCurrent.Check)
                {
                    itemListDynParameter.SetCheckState(i, CheckState.Unchecked);
                }
            }
            GenerateChangeParameters();
        }

        #endregion
    }
}
