﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public class SearchCriteriaHelper
    {
        public static void AddFilter(QueryConditions queryCond, string columnName, FilterQuery.FilterLogicalOperator filterQueryOperator,
            object value, ColumnFilter.FilterComparisionOperator comparisionOperator)
        {
            FilterQuery filterQuery = queryCond.Filter.Where(p => p.ColumnName == columnName).SingleOrDefault();
            if (filterQuery == null)
            {
                filterQuery = new FilterQuery();
                filterQuery.ColumnName = columnName;
                filterQuery.FilterOperator = filterQueryOperator;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = comparisionOperator;
                columnFilter.Value = value;
                filterQuery.Filtering.Add(columnFilter);
                queryCond.Filter.Add(filterQuery);
            }
            else
            {
                if (filterQuery.Filtering.Where(p => ((p.Value != null && value != null && p.Value.Equals(value)) || (p.Value == null && value == null)) &&
                    p.ComparisonOperator == comparisionOperator).Count() == 0)
                {
                    filterQuery.FilterOperator = filterQueryOperator;
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = comparisionOperator;
                    columnFilter.Value = value;
                    filterQuery.Filtering.Add(columnFilter);
                }
            }
        }

        public static void AddFilterOfAny(QueryConditions queryCond, string columnName, object value, ColumnFilter.FilterComparisionOperator comparisionOperator)
        {
            FilterQuery filterQuery = new FilterQuery();
            filterQuery.ColumnName = columnName;
            ColumnFilter columnFilter = new ColumnFilter();
            columnFilter.ComparisonOperator = comparisionOperator;
            columnFilter.Value = value;
            filterQuery.Filtering.Add(columnFilter);
            queryCond.Filter.Add(filterQuery);
        }

        public static void AddFilterOfAny(QueryConditions queryCond, string columnName, object value)
        {
            AddFilterOfAny(queryCond, columnName, value, ColumnFilter.FilterComparisionOperator.Equals);
        }

        public static void AddFilterOfDate(QueryConditions queryCond, string columnName, DateTime dt)
        {
            FilterQuery filterQueryDT = new FilterQuery();
            filterQueryDT.ColumnName = columnName;
            ColumnFilter columnFilter = new ColumnFilter();
            columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
            columnFilter.Value = dt;
            filterQueryDT.Filtering.Add(columnFilter);
            queryCond.Filter.Add(filterQueryDT);
        }

        public static void AddFilterFederalDistrict(QueryConditions queryCond, string columnName, List<FederalDistrict> fds)
        {
            if (fds.Count() > 0)
            {
                FilterQuery filterQueryFD = new FilterQuery();
                filterQueryFD.ColumnName = columnName;
                filterQueryFD.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                foreach (FederalDistrict item in fds)
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Id;
                    filterQueryFD.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryFD);
            }
        }

        public static void AddFilterRegion(QueryConditions queryCond, string columnName, Presenter<string, RegionModel> regionSelectPresenter)
        {
            if (regionSelectPresenter.SelectedItems.Count > 0)
            {
                FilterQuery filterQueryRegion = new FilterQuery();
                filterQueryRegion.ColumnName = columnName;
                filterQueryRegion.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                foreach (KeyValuePair<string, RegionModel> item in regionSelectPresenter.SelectedItems)
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Key;
                    filterQueryRegion.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryRegion);
            }
        }

        public static void AddFilterNalogOrgan(QueryConditions queryCond, string columnName, Presenter<string, TaxAuthorityModel> taxAuthorityPresenter)
        {
            if (taxAuthorityPresenter.SelectedItems.Count > 0)
            {
                FilterQuery filterQueryNO = new FilterQuery();
                filterQueryNO.ColumnName = columnName;
                filterQueryNO.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                foreach (KeyValuePair<string, TaxAuthorityModel> item in taxAuthorityPresenter.SelectedItems)
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.Key;
                    filterQueryNO.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryNO);
            }
        }

        public static void AddFilterNalogPeriod(QueryConditions queryCond, string columnName, List<NalogPeriodCheck> nalogPeriods)
        {
            if (nalogPeriods.Count > 0)
            {
                FilterQuery filterQueryNO = new FilterQuery();
                filterQueryNO.ColumnName = columnName;
                filterQueryNO.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                foreach (NalogPeriodCheck item in nalogPeriods)
                {
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals;
                    columnFilter.Value = item.CODE + "-" + item.Year.ToString();
                    filterQueryNO.Filtering.Add(columnFilter);
                }
                queryCond.Filter.Add(filterQueryNO);
            }
        }
    }
}
