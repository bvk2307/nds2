﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public interface IControlSearchCriteria
    {
        QueryConditions GetQueryConditions();
        IEnumerable<ColumnSort> GetQuerySorting(IEnumerable<ColumnSort> list);
        event EventHandler ChangeControlParameters;
        event EventHandler RequireChangeColumns;
        bool IsCorrectParameters();
        void SetupDependingFromReportKey(string reportKey);
        string GetSearchCriteriaDescription();
    }
}
