﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls
{
    public partial class MatchingRuleSearch : BaseControlSearch, IControlSearchCriteria
    {
        #region Переменные

        private List<AggregateByNalogOrgan> _aggregateByNalogOrgans = new List<AggregateByNalogOrgan>();
        private List<FederalDistrict> _federalDistricts = new List<FederalDistrict>();
        private IDataService _dataService;
        private Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel> regionSelectPresenter;
        private Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel> taxAuthorityPresenter;

        #endregion

        public MatchingRuleSearch(IDataService dataService)
            : base()
        {
            InitializeComponent();
            _dataService = dataService;
        }

        #region Реализация IControlSearchCriteria

        public override QueryConditions GetQueryConditions()
        {
            QueryConditions queryCond = new QueryConditions();

            AggregateByNalogOrgan aggregateByNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateByNalogOrgan != null)
            {
                SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.IRM_AggregateByNalogOrgan, (int)aggregateByNalogOrgan.Type);
            }

            if (aggregateByNalogOrgan != null)
            {
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    SearchCriteriaHelper.AddFilterFederalDistrict(queryCond, TaskParamCriteria.MR_FederalDistrict, GetCheckedFederalDistrict());
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    SearchCriteriaHelper.AddFilterRegion(queryCond, TaskParamCriteria.MR_TaxPayerRegionCode, regionSelectPresenter);
                }
                if (aggregateByNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    SearchCriteriaHelper.AddFilterNalogOrgan(queryCond, TaskParamCriteria.MR_NalogOrganCode, taxAuthorityPresenter);
                }
            }

            SearchCriteriaHelper.AddFilterOfAny(queryCond, TaskParamCriteria.MR_DateOneDay, (DateTime)ultraCalendarOneDay.Value);

            return queryCond;
        }

        #endregion

        #region Установка данных

        public void SetAggregateByNalogOrgan(List<AggregateByNalogOrgan> aggregateByNalogOrgans)
        {
            _aggregateByNalogOrgans.Clear();
            _aggregateByNalogOrgans.AddRange(aggregateByNalogOrgans);
        }

        public void SetFederalDistricts(List<FederalDistrict> federalDistricts)
        {
            _federalDistricts.Clear();
            _federalDistricts.AddRange(federalDistricts);
        }

        #endregion

        #region Инициализация

        private void MatchingRuleSearch_Load(object sender, EventArgs e)
        {
            InitViewFederalDistrict();
            InitData();
        }

        public void InitData()
        {
            comboAggregateLevelNO.DataSource = _aggregateByNalogOrgans;
            comboFederalDistrict.DataSource = _federalDistricts;

            ILookupDataSource lookupDataSource = LookupDataSource();
            regionSelectPresenter = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, RegionModel>(
                lookupMultiRegion,
                (args) => lookupDataSource.Search<RegionModel>(args));

            taxAuthorityPresenter =
                new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.Presenter<string, TaxAuthorityModel>(
                    lookupMultiInspection,
                    (args) => lookupDataSource.Search<TaxAuthorityModel>(args));

            DateTimeControlsEventAdd();
            AllComboControlsEventAdd();
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            if (comboAggregateLevelNO.Items.Count > 0)
                comboAggregateLevelNO.SelectedIndex = 0;
            else
                comboAggregateLevelNO.SelectedIndex = -1;

            DateTime dtDefault = DateTime.Now.Date.AddDays(-1);
            ultraCalendarOneDay.Value = dtDefault;
        }

        private ILookupDataSource LookupDataSource()
        {
            return new FilterEditorDataProvider(_dataService);
        }

        private void InitViewFederalDistrict()
        {
            this.comboFederalDistrict.CheckedListSettings.CheckStateMember = "Check";
            Infragistics.Win.UltraWinGrid.UltraGridColumn column = this.comboFederalDistrict.DisplayLayout.Bands[0].Columns["Check"];
            CheckEditor checkEditor = new CheckEditor();
            checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
            column.Editor = checkEditor;
            column.Header.VisiblePosition = 0;

            this.comboFederalDistrict.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            this.comboFederalDistrict.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            this.comboFederalDistrict.CheckedListSettings.ListSeparator = " / ";
        }

        #endregion

        #region Вспомогательные методы

        private AggregateByNalogOrgan GetCurrentAggregateByNalogOrgan()
        {
            AggregateByNalogOrgan aggregateByNalogOrgan = null;
            int ind = comboAggregateLevelNO.SelectedIndex;
            if (ind > -1 && ind < _aggregateByNalogOrgans.Count)
            {
                aggregateByNalogOrgan = _aggregateByNalogOrgans[ind];
            }
            return aggregateByNalogOrgan;
        }

        private List<FederalDistrict> GetCheckedFederalDistrict()
        {
            List<FederalDistrict> fds;
            if (comboFederalDistrict.DataSource != null)
            {
                fds = (List<FederalDistrict>)comboFederalDistrict.DataSource;
            }
            else { fds = new List<FederalDistrict>(); }
            return fds.Where(p => p.Check).ToList();
        }

        private void AllComboControlsEventAdd()
        {
            this.comboAggregateLevelNO.ValueChanged += new EventHandler(this.comboAggregateLevelNO_ValueChanged);
            this.comboFederalDistrict.ValueChanged += new EventHandler(this.FederalDistrictOnValueChanged);
            this.lookupMultiRegion.OnSave += lookupMulti_OnSave;
            this.lookupMultiInspection.OnSave += lookupMulti_OnSave;
        }

        private void DateTimeControlsEventAdd()
        {
            this.ultraCalendarOneDay.ValueChanged += new System.EventHandler(this.ultraCalendar_ValueChanged);
            this.ultraCalendarOneDay.TextChanged += new System.EventHandler(this.ultraCalendar_TextChanged);
        }

        private void SetNalogOrganVisible()
        {
            AggregateByNalogOrgan aggregateNalogOrgan = GetCurrentAggregateByNalogOrgan();
            if (aggregateNalogOrgan != null)
            {
                if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Country)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tContry"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.FederalDistrict)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tFederalDistrict"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Region)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tRegion"];
                }
                else if (aggregateNalogOrgan.Type == AggregateByNalogOrganType.Inspection)
                {
                    ultraTabControlNO.SelectedTab = ultraTabControlNO.Tabs["tInspection"];
                }
            }
        }

        #endregion

        #region События

        private void comboAggregateLevelNO_ValueChanged(object sender, EventArgs e)
        {
            SetNalogOrganVisible();
            GenerateChangeParameters();
        }

        private void FederalDistrictOnValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void lookupMulti_OnSave()
        {
            GenerateChangeParameters();
        }

        private void ultraCalendar_ValueChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        private void ultraCalendar_TextChanged(object sender, EventArgs e)
        {
            GenerateChangeParameters();
        }

        #endregion
    }
}
