﻿using System;
using System.Data;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelManagerReport : ExcelManagerBase
    {
        public void FillWorkbook( 
            string filePath, Func<uint> GetPageCount, Presenter.GetPageDataDelegate GetPageData, Predicate<DataRow> predicIsCanceled)
        {
            using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(filePath, true))
            {
                WorkbookPart workbookPart = myDoc.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                string origninalSheetId = workbookPart.GetIdOfPart(worksheetPart);

                WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
                string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

                using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
                using ( OpenXmlWriter writer = OpenXmlWriter.Create( replacementPart ) )
                {
                    bool isBreak = false;
                    Row r = new Row();
                    while (reader.Read())
                    {
                        if (reader.ElementType == typeof(SheetData))
                        {
                            if (reader.IsEndElement)
                                continue;
                            writer.WriteStartElement(new SheetData());

                            //--------------------
                            DataRow dr;
                            long rowAbsolute = -1;
                            int currentTotalRowCount = 0;
                            long rowsTotalMatches = 0;
                            uint pageCount = GetPageCount();
                            for (uint page = 1; page <= pageCount; page++)
                            {
                                DataTable dtRows = GetPageData(page, out rowsTotalMatches);
                                if (dtRows.Rows.Count > 0)
                                {
                                    for (int row = 0; row < dtRows.Rows.Count; row++)
                                    {
                                        dr = dtRows.Rows[row];
                                        rowAbsolute++;
                                        SetRowHeight(r, rowAbsolute);
                                        ExcelRowStyle excelRowStyle = GetExcelRowStyle(rowAbsolute);

                                        writer.WriteStartElement(r);
                                        for (int col = 0; col < dtRows.Columns.Count; col++)
                                        {
                                            Type typeValue = dr[col].GetType();
                                            Cell c = CreateCell(rowAbsolute, excelRowStyle, dr[col]);
                                            if (ReportGeneralHelper.IsNumberType(typeValue))
                                            {
                                                c.DataType = CellValues.Number;
                                                c.CellValue = new CellValue(GenerateExcelNumberValue(dr[col], typeValue));
                                            }
                                            else
                                            {
                                                c.DataType = CellValues.InlineString;
                                                InlineString inlineString = new InlineString();
                                                Text t = new Text();
                                                t.Text = dr[col].ToString();
                                                inlineString.AppendChild(t);
                                                c.AppendChild(inlineString);
                                            }
                                            writer.WriteElement(c);
                                        }
                                        writer.WriteEndElement();

                                        if (predicIsCanceled(dr))
                                        {
                                            isBreak = true;
                                            break;
                                        }
                                        currentTotalRowCount++;
                                        GenerateEvent(currentTotalRowCount);
                                    }
                                    if (isBreak) { break; }
                                }
                                else { break; }

                                if (rowsTotalMatches < pageCount)
                                {
                                    break;
                                }
                            }
                            //--------------------

                            writer.WriteEndElement();
                        }
                        else
                        {
                            if (reader.IsStartElement)
                            {
                                writer.WriteStartElement(reader);
                            }
                            else if (reader.IsEndElement)
                            {
                                writer.WriteEndElement();
                            }
                        }
                    }
                }
                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(origninalSheetId)).First();
                sheet.Id.Value = replacementPartId;
                workbookPart.DeletePart(worksheetPart);
            }
        }
    }
}
