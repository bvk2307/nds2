﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelManagerCreator
    {
        public static ExcelManagerReport CreateExcelManagerReport()
        {
            return new ExcelManagerReport();
        }

        public static ExcelManagerInvoice CreateExcelManagerInvoice()
        {
            return new ExcelManagerInvoice();
        }
    }
}
