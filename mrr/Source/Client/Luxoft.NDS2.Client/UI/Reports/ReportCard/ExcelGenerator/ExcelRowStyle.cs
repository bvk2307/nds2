﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelRowStyle
    {
        public bool IsHorizontalAlignmentCenter { get; set; }
        public bool IsVerticalAlignmentCenter { get; set; }

        public ExcelRowStyle()
        {
            IsHorizontalAlignmentCenter = false;
            IsVerticalAlignmentCenter = false;
        }
    }
}
