﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelManagerBase
    {
        #region Переменные

        public delegate void ExcelGenerateDelegate(ExportExcelParam p, long count);
        public event ExcelGenerateDelegate ExcelGenerateEventExt;
        public event Action<int> ExcelGenerateEvent;
        private Dictionary<long, double> _rowsHeight = new Dictionary<long, double>();
        private double _rowHeightDefault = 0;
        private List<long> _headerRows = new List<long>();
        private IDictionary<long, ExcelRowStyle> _rowsStyle = new Dictionary<long, ExcelRowStyle>();
        public ExportExcelParam ExportExcelParam { get; set; }
        private Dictionary<int, UInt32> sheetIds = new Dictionary<int, uint>();

        #endregion

        #region Публичные методы

        public virtual void CreateExcel(string filepath, string sheetName, List<double> columnsWidth)
        {
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();

                WorkbookStylesPart wbsp = wbp.AddNewPart<WorkbookStylesPart>();
                wbsp.Stylesheet = CreateStylesheet();
                wbsp.Stylesheet.Save();

                Columns columns = new Columns();
                uint columnIndex = 1;
                foreach (double itemLength in columnsWidth)
                {
                    columns.Append(CreateColumnData(columnIndex, columnIndex, itemLength));
                    columnIndex++;
                }
                ws.Append(columns);

                ws.Append(sd);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = sheetName;
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                sheets.Append(sheet);
                wb.Append(fv);
                wb.Append(sheets);

                xl.WorkbookPart.Workbook = wb;
                xl.WorkbookPart.Workbook.Save();
                xl.Close();
            }
        }

        public void SetupRowHeight(Dictionary<long, double> rowsHeight, double rowHeightDefault)
        {
            _rowsHeight = rowsHeight;
            _rowHeightDefault = rowHeightDefault;
        }

        public void SetupRowStyle( IDictionary<long, ExcelRowStyle> rowsStyle )
        {
            _rowsStyle = rowsStyle;
        }

        public void SetupHeaderRowCount(int headerRowCount)
        {
            _headerRows.Clear();
            for (int i = -1; ++i < headerRowCount; )
            {
                _headerRows.Add(i);
            }
        }

        public void SetupHeaderRows(IEnumerable<long> headerRows)
        {
            _headerRows.Clear();
            _headerRows.AddRange(headerRows);
        }

        public void MergeTwoCells( string docName, string sheetName, string cell1Name, string cell2Name )
        {
            // Open the document for editing.
            using ( SpreadsheetDocument document = SpreadsheetDocument.Open( docName, true ) )
            {
                Worksheet worksheet = GetWorksheet(document, sheetName);
                if ( worksheet == null || string.IsNullOrEmpty( cell1Name ) || string.IsNullOrEmpty( cell2Name ) )
                    return;

                // Verify if the specified cells exist, and if they do not exist, create them.
                CreateSpreadsheetCellIfNotExist( worksheet, cell1Name );
                CreateSpreadsheetCellIfNotExist( worksheet, cell2Name );

                MergeTwoCells( cell1Name, cell2Name, worksheet );

                worksheet.Save();
            }
        }

        public void MergeMergeCells( string reportTempFileFullSpec, string sheetName, ICollection<ExcelMergeCell> mergeCells )
        {
            // Open the document for editing.
            using ( SpreadsheetDocument document = SpreadsheetDocument.Open( reportTempFileFullSpec, true ) )
            {
                Worksheet worksheet = GetWorksheet(document, sheetName);
                if ( worksheet == null )
                    return;

                foreach ( ExcelMergeCell item in mergeCells )
                {
                    // Verify if the specified cells exist, and if they do not exist, create them.
                    CreateSpreadsheetCellIfNotExist( worksheet, item.BeginCell );
                    CreateSpreadsheetCellIfNotExist( worksheet, item.EndCell );
                }
                MergeMergeCells( worksheet, mergeCells );

                worksheet.Save();
            }
        }

        public static void MergeMergeCells( Worksheet worksheet, IEnumerable<ExcelMergeCell> mergeCells )
        {
            foreach ( ExcelMergeCell item in mergeCells )
            {
                MergeTwoCells( item.BeginCell, item.EndCell, worksheet );
            }
        }

        public static void MergeTwoCells( string cell1Name, string cell2Name, Worksheet worksheet )
        {
            MergeCells mergeCells = worksheet.Elements<MergeCells>().FirstOrDefault();
            if ( mergeCells == null )
            {
                mergeCells = new MergeCells();

                // Insert a MergeCells object into the specified position.
                OpenXmlElement xmlElement = worksheet.Elements<CustomSheetView>().FirstOrDefault();
                if ( xmlElement == null )
                {
                    xmlElement = worksheet.Elements<DataConsolidate>().FirstOrDefault();
                    if ( xmlElement == null )
                    {
                        xmlElement = worksheet.Elements<SortState>().FirstOrDefault();
                        if ( xmlElement == null )
                        {
                            xmlElement = worksheet.Elements<AutoFilter>().FirstOrDefault();
                            if ( xmlElement == null )
                            {
                                xmlElement = worksheet.Elements<Scenarios>().FirstOrDefault();
                                if ( xmlElement == null )
                                {
                                    xmlElement = worksheet.Elements<ProtectedRanges>().FirstOrDefault();
                                    if ( xmlElement == null )
                                    {
                                        xmlElement = worksheet.Elements<SheetProtection>().FirstOrDefault();
                                        if ( xmlElement == null )
                                        {
                                            xmlElement = worksheet.Elements<SheetCalculationProperties>().FirstOrDefault();
                                            if ( xmlElement == null )
                                                xmlElement = worksheet.Elements<SheetData>().First();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                worksheet.InsertAfter( mergeCells, xmlElement );
            }

            // Create the merged cell and append it to the MergeCells collection.
            MergeCell mergeCell = new MergeCell { Reference = new StringValue( string.Join( ":", cell1Name, cell2Name ) ) };
            mergeCells.Append( mergeCell );
        }

        #endregion

        #region Вспомогательные методы

        protected WorksheetPart InsertWorksheet( SpreadsheetDocument spreadSheet, int numSheet, List<double> columnsWidth )
        {
            // Add a blank WorksheetPart.
            WorksheetPart newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new worksheet.
            uint sheetId = sheets.Elements<Sheet>().MaxWDefault(s => s.SheetId.Value, defaultValue: 0) + 1;
            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet { Id = relationshipId, SheetId = sheetId, Name = ExportExcelParam.GetSheetName(numSheet) };
            SetSheetId(numSheet, sheet.SheetId);
            sheets.Append(sheet);

            //-------------- создание колонок в этой вкладке
            Worksheet workSheetNew = ((WorksheetPart)spreadSheet.WorkbookPart.GetPartById(sheet.Id)).Worksheet;
            // Check if the column collection exists
            Columns cs = workSheetNew.Elements<Columns>().FirstOrDefault();
            if ((cs == null))
            {
                // If Columns appended to worksheet after sheetdata Excel will throw an error.
                SheetData sd = workSheetNew.Elements<SheetData>().FirstOrDefault();
                if ((sd != null))
                {
                    cs = workSheetNew.InsertBefore(new Columns(), sd);
                }
                else
                {
                    cs = new Columns();
                    workSheetNew.Append(cs);
                }
            }
            uint columnIndex = 1;
            foreach (double itemLength in columnsWidth)
            {
                cs.Append(CreateColumnData(columnIndex, columnIndex, itemLength));
                columnIndex++;
            }
            return newWorksheetPart;
        }

        protected Stylesheet CreateStylesheet()
        {
            Stylesheet stylesheet1 = new Stylesheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } };
            stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");
            //common font
            Fonts fonts1 = new Fonts() { Count = (UInt32Value)2U, KnownFonts = true };

            DocumentFormat.OpenXml.Spreadsheet.Font font1 = new DocumentFormat.OpenXml.Spreadsheet.Font();
            FontSize fontSize1 = new FontSize() { Val = 11D };
            DocumentFormat.OpenXml.Spreadsheet.Color colorFont1 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Theme = (UInt32Value)1U };
            FontName fontName1 = new FontName() { Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme1 = new FontScheme() { Val = FontSchemeValues.Minor };

            font1.Append(fontSize1);
            font1.Append(colorFont1);
            font1.Append(fontName1);
            font1.Append(fontFamilyNumbering1);
            font1.Append(fontScheme1);

            fonts1.Append(font1);

            //====================================== header font
            DocumentFormat.OpenXml.Spreadsheet.Font font2 = new DocumentFormat.OpenXml.Spreadsheet.Font();
            Bold fontBold2 = new Bold();
            FontSize fontSize2 = new FontSize() { Val = 11D };
            DocumentFormat.OpenXml.Spreadsheet.Color colorFont2 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Theme = (UInt32Value)1U };
            FontName fontName2 = new FontName() { Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering2 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme2 = new FontScheme() { Val = FontSchemeValues.Minor };

            font2.Append(fontSize2);
            font2.Append(fontBold2);
            font2.Append(colorFont2);
            font2.Append(fontName2);
            font2.Append(fontFamilyNumbering2);
            font2.Append(fontScheme2);

            fonts1.Append(font2);
            //======================================

            Fills fills1 = new Fills() { Count = (UInt32Value)5U };

            // FillId = 0
            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.None };
            fill1.Append(patternFill1);

            // FillId = 1
            Fill fill2 = new Fill();
            PatternFill patternFill2 = new PatternFill() { PatternType = PatternValues.Gray125 };
            fill2.Append(patternFill2);

            // FillId = 2, RED
            Fill fill3 = new Fill();
            PatternFill patternFill3 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor() { Rgb = "FFFF0000" };
            BackgroundColor backgroundColor1 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill3.Append(foregroundColor1);
            patternFill3.Append(backgroundColor1);
            fill3.Append(patternFill3);

            // FillId = 3, Very Light Gray
            Fill fill4 = new Fill();
            PatternFill patternFill4 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor2 = new ForegroundColor() { Rgb = "FFDCDCDC" };
            BackgroundColor backgroundColor2 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill4.Append(foregroundColor2);
            patternFill4.Append(backgroundColor2);
            fill4.Append(patternFill4);

            // FillId = 4, Light Gray
            Fill fill5 = new Fill();
            PatternFill patternFill5 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor3 = new ForegroundColor() { Rgb = "FFC0C0C0" };
            BackgroundColor backgroundColor3 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill5.Append(foregroundColor3);
            patternFill5.Append(backgroundColor3);
            fill5.Append(patternFill5);

            fills1.Append(fill1);
            fills1.Append(fill2);
            fills1.Append(fill3);
            fills1.Append(fill4);
            fills1.Append(fill5);

            Borders borders1 = new Borders() { Count = (UInt32Value)2U };

            Border border1 = new Border();
            LeftBorder leftBorder1 = new LeftBorder();
            RightBorder rightBorder1 = new RightBorder();
            TopBorder topBorder1 = new TopBorder();
            BottomBorder bottomBorder1 = new BottomBorder();
            DiagonalBorder diagonalBorder1 = new DiagonalBorder();

            border1.Append(leftBorder1);
            border1.Append(rightBorder1);
            border1.Append(topBorder1);
            border1.Append(bottomBorder1);
            border1.Append(diagonalBorder1);

            borders1.Append(border1);

            //=====================================
            Border border2 = new Border();
            LeftBorder leftBorder2 = new LeftBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color colorBor1 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            leftBorder2.Append(colorBor1);

            RightBorder rightBorder2 = new RightBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color2 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            rightBorder2.Append(color2);

            TopBorder topBorder2 = new TopBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color3 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            topBorder2.Append(color3);

            BottomBorder bottomBorder2 = new BottomBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color4 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            bottomBorder2.Append(color4);
            DiagonalBorder diagonalBorder2 = new DiagonalBorder();

            border2.Append(leftBorder2);
            border2.Append(rightBorder2);
            border2.Append(topBorder2);
            border2.Append(bottomBorder2);
            border2.Append(diagonalBorder2);
            borders1.Append(border2);
            //=====================================

            CellStyleFormats cellStyleFormats1 = new CellStyleFormats() { Count = (UInt32Value)1U };
            CellFormat cellFormat1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };

            cellStyleFormats1.Append(cellFormat1);

            CellFormats cellFormats1 = new CellFormats() { Count = (UInt32Value)4U };
            CellFormat cellFormat2_1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat3 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat4 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat5 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat6 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat2_2 = new CellFormat() { NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat2_3 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat2_4 = new CellFormat() { NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat7_1 = new CellFormat() { NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat7_2 = new CellFormat() { NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };

            //=================================================
            CellformatSetup(cellFormat2_1, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: false);
            CellformatSetup(cellFormat3, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: true);
            CellformatSetup(cellFormat4, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: false);
            CellformatSetup(cellFormat5, IsHorizontalAlignmentCenter: true, IsVerticalAlignmentCenter: false);
            CellformatSetup(cellFormat6, IsHorizontalAlignmentCenter: true, IsVerticalAlignmentCenter: true);
            CellformatSetup(cellFormat2_2, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: false);
            CellformatSetup(cellFormat2_3, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: true);
            CellformatSetup(cellFormat2_4, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: true);
            CellformatSetup(cellFormat7_1, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: false);
            CellformatSetup(cellFormat7_2, IsHorizontalAlignmentCenter: false, IsVerticalAlignmentCenter: true);
            //=================================================
                                                    //values of CellType.StyleIndex
            cellFormats1.Append(cellFormat2_1);     //0U
            cellFormats1.Append(cellFormat3);       //1U header style
            cellFormats1.Append(cellFormat4);       //2U header style
            cellFormats1.Append(cellFormat5);       //3U header style
            cellFormats1.Append(cellFormat6);       //4U header style
            cellFormats1.Append(cellFormat2_2);     //5U      numeric right alignment
            cellFormats1.Append(cellFormat2_3);     //6U                                    vertical alignment center
            cellFormats1.Append(cellFormat2_4);     //7U      numeric rught alignment       vertical alignment center
            cellFormats1.Append(cellFormat7_1);     //8U red  numeric rught alignment
            cellFormats1.Append(cellFormat7_2);     //9U red  numeric rught alignment       vertical alignment center


            CellStyles cellStyles1 = new CellStyles() { Count = (UInt32Value)1U };
            CellStyle cellStyle1 = new CellStyle() { Name = "Normal", FormatId = (UInt32Value)0U, BuiltinId = (UInt32Value)0U };

            cellStyles1.Append(cellStyle1);
            DifferentialFormats differentialFormats1 = new DifferentialFormats() { Count = (UInt32Value)0U };
            TableStyles tableStyles1 = new TableStyles() { Count = (UInt32Value)0U, DefaultTableStyle = "TableStyleMedium2", DefaultPivotStyle = "PivotStyleMedium9" };

            StylesheetExtensionList stylesheetExtensionList1 = new StylesheetExtensionList();

            StylesheetExtension stylesheetExtension1 = new StylesheetExtension() { Uri = "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" };
            stylesheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");
            X14.SlicerStyles slicerStyles1 = new X14.SlicerStyles() { DefaultSlicerStyle = "SlicerStyleLight1" };

            stylesheetExtension1.Append(slicerStyles1);

            stylesheetExtensionList1.Append(stylesheetExtension1);

            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellStyleFormats1);
            stylesheet1.Append(cellFormats1);
            stylesheet1.Append(cellStyles1);
            stylesheet1.Append(differentialFormats1);
            stylesheet1.Append(tableStyles1);
            stylesheet1.Append(stylesheetExtensionList1);
            return stylesheet1;
        }

        protected void CellformatSetup(CellFormat cellFormat2, bool IsHorizontalAlignmentCenter, bool IsVerticalAlignmentCenter)
        {
            cellFormat2.ApplyAlignment = true;
            if (cellFormat2.Alignment == null)
                cellFormat2.Alignment = new Alignment() { WrapText = true };
            Alignment a = cellFormat2.Alignment;
            if (a.WrapText == null || a.WrapText.Value == false)
                a.WrapText = new BooleanValue(true);
            if (IsHorizontalAlignmentCenter)
            {
                a.Horizontal = HorizontalAlignmentValues.Center;
            }
            if (IsVerticalAlignmentCenter)
            {
                a.Vertical = VerticalAlignmentValues.Center;
            }
        }

        protected Column CreateColumnData(UInt32 StartColumnIndex, UInt32 EndColumnIndex, double ColumnWidth)
        {
            Column column;
            column = new Column();
            column.Min = StartColumnIndex;
            column.Max = EndColumnIndex;
            column.Width = ColumnWidth;
            column.CustomWidth = true;
            return column;
        }

        protected string GenerateExcelNumberValue(object value, Type typeValue)
        {
            string strValue = value.ToString();
            if (typeValue == typeof(decimal) || typeValue == typeof(decimal?))
            {
                strValue = (decimal.Round((decimal)value, 2)).ToString();
            }
            else if (typeValue == typeof(double) || typeValue == typeof(double?))
            {
                strValue = (Math.Round((double)value, 2)).ToString();
            }
            strValue = strValue.Replace(",", ".");
            return strValue;
        }

        protected void SetRowHeight(Row r, long rowAbsolute)
        {
            if (_rowsHeight.ContainsKey(rowAbsolute))
            {
                r.CustomHeight = true;
                r.Height = _rowsHeight[rowAbsolute];
            }
            else if (_rowHeightDefault > 0)
            {
                r.CustomHeight = true;
                r.Height = _rowHeightDefault;
            }
            else
            {
                r.CustomHeight = false;
            }
        }

        protected ExcelRowStyle GetExcelRowStyle(long rowAbsolute)
        {
            ExcelRowStyle excelRowStyle = null;
            if (_rowsStyle.ContainsKey(rowAbsolute))
            {
                excelRowStyle = _rowsStyle[rowAbsolute];
            }
            return excelRowStyle;
        }

        /// <summary> </summary>
        /// <param name="rowAbsolute"></param>
        /// <param name="excelRowStyle"></param>
        /// <param name="value"> Cell data value. Optional. It is 'null' by default. </param>
        /// <param name="cellColumn"> A column of the cell. Optional. </param>
        /// <param name="cellStyleSelectior"> A cell style celector getting <paramref name="value"/> and <paramref name="cellColumn"/> as parameters. </param>
        /// <param name="applyRowStyleToNoHeader"> 'true' to apply cell styles from <paramref name="excelRowStyle"/> for rows are not in <see cref="_headerRows"/>. By default is 'false'. </param>
        /// <param name="valueDataType"></param>
        /// <returns></returns>
        protected Cell CreateCell( long rowAbsolute, ExcelRowStyle excelRowStyle, object value = null, DataColumn cellColumn = null, 
            Func<object, DataColumn, ExcelCellStyle> cellStyleSelectior = null, bool applyRowStyleToNoHeader = false, Type valueDataType = null )
        {
            if ( valueDataType == null && value == null &&  cellColumn == null )
                throw new ArgumentNullException( "value", string.Format( "The parameter must be not 'null' if parameters '{0}' and '{1}' are 'null'", "nameCellColumn", "valueDataType" ) );

            if ( valueDataType == null )
            {
                if ( value != null )
                    valueDataType = value.GetType();
                else
                    valueDataType = cellColumn.DataType;
            }
            Cell c = new Cell();
            if ( _headerRows.Contains( rowAbsolute ) )
            {
                if ( excelRowStyle != null )
                {
                    if ( excelRowStyle.IsHorizontalAlignmentCenter )
                    {
                        if ( excelRowStyle.IsVerticalAlignmentCenter )
                            c = new Cell() { StyleIndex = (UInt32Value)4U };
                        else
                            c = new Cell() { StyleIndex = (UInt32Value)3U };
                    }
                    else
                    {
                        if ( excelRowStyle.IsVerticalAlignmentCenter )
                            c = new Cell() { StyleIndex = (UInt32Value)1U };
                        else
                            c = new Cell() { StyleIndex = (UInt32Value)2U };
                    }
                }
                else
                {
                    c = new Cell() { StyleIndex = (UInt32Value)2U };
                }
            }
            else if ( applyRowStyleToNoHeader && excelRowStyle != null && excelRowStyle.IsVerticalAlignmentCenter )
            {
                if ( ReportGeneralHelper.IsNumberType( valueDataType ) )
                {
                    if ( cellStyleSelectior != null && CellFillColorStyle.Red == cellStyleSelectior( value, cellColumn ).FillColorStyle )
                        c = new Cell() { StyleIndex = (UInt32Value)9U };
                    else
                        c = new Cell() { StyleIndex = (UInt32Value)7U };
                }
                else
                {
                    c = new Cell() { StyleIndex = (UInt32Value)6U };
                }
            }
            else
            {
                if ( ReportGeneralHelper.IsNumberType( valueDataType ) )
                {
                    if ( cellStyleSelectior != null && CellFillColorStyle.Red == cellStyleSelectior( value, cellColumn ).FillColorStyle )
                        c = new Cell() { StyleIndex = (UInt32Value)8U };
                    else
                        c = new Cell() { StyleIndex = (UInt32Value)5U };
                }
                else
                    c = new Cell() { StyleIndex = (UInt32Value)0U };
            }
            return c;
        }

        protected void GenerateEvent(int count)
        {
            Action<int> handlers = ExcelGenerateEvent;
            if (handlers != null)
            {
                handlers(count);
            }
        }

        protected void GenerateEventExt(long count)
        {
            ExcelGenerateDelegate handlers = ExcelGenerateEventExt;
            if (handlers != null)
            {
                handlers(ExportExcelParam, count);
            }
        }

        protected Worksheet GetWorksheet(SpreadsheetDocument document, string worksheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook
                .Descendants<Sheet>().Where(s => s.Name == worksheetName);
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart
                .GetPartById(sheets.First().Id);
            return worksheetPart.Worksheet;
        }

        protected void CreateSpreadsheetCell(Worksheet worksheet, string cellName)
        {
            string columnName = GetColumnName(cellName);
            uint rowIndex = 2;
            IEnumerable<Row> rows = worksheet.Descendants<Row>().Where(r => r
                .RowIndex.Value == rowIndex);
            Row row = rows.First();
            IEnumerable<Cell> cells = row.Elements<Cell>().Where(c => c.CellReference
                .Value == cellName);
        }

        protected void CreateSpreadsheetCellIfNotExist( Worksheet worksheet, string cellName )
        {
            uint rowIndex = GetRowIndex( cellName );

            //apopov 1.2.2016	//Is this a workable query for an existent row?
            Row row = worksheet.Descendants<Row>().FirstOrDefault( r => r.RowIndex != null && r.RowIndex.Value == rowIndex );

            // If the Worksheet does not contain the specified row, create the specified row.
            // Create the specified cell in that row, and insert the row into the Worksheet.
            if ( row == null )
            {
                row = new Row { RowIndex = new UInt32Value(rowIndex) };
                Cell cell = new Cell { CellReference = new StringValue( cellName ) };
                row.Append( cell );
                worksheet.Descendants<SheetData>().First().Append( row );
                worksheet.Save();
            }
            else
            {
                // If the row does not contain the specified cell, create the specified cell.
                if ( row.Elements<Cell>().All( c => c.CellReference.Value != cellName ) )
                {
                    Cell cell = new Cell { CellReference = new StringValue( cellName ) };
                    row.Append( cell );
                    worksheet.Save();
                }
            }
        }

        protected uint GetRowIndex(string cellName)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellName);

            return uint.Parse(match.Value);
        }

        protected string GetColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);
            return match.Value;
        }

        protected void SetSheetId(int num, UInt32 sheetId)
        {
            if (sheetIds.ContainsKey(num))
            {
                sheetIds[num] = sheetId;
            }
            else
            {
                sheetIds.Add(num, sheetId);
            }
        }

        protected UInt32 GetSheetId(int num)
        {
            UInt32 sheetId = 0;
            if (sheetIds.ContainsKey(num))
            {
                sheetId = sheetIds[num];
            }
            return sheetId;
        }

        #endregion
    }
}