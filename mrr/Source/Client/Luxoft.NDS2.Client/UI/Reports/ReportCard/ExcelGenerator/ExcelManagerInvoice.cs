﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelManagerInvoice : ExcelManagerBase
    {
        public delegate DataTable GetPageDataInvoiceDelegate(uint pageFrom, uint page, ExportExcelParam p, out long rowsTotalMatches, out long rowCountHeader);

        public override void CreateExcel(string filepath, string sheetName, List<double> columnsWidth)
        {
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();

                WorkbookStylesPart wbsp = wbp.AddNewPart<WorkbookStylesPart>();
                wbsp.Stylesheet = CreateStylesheet();
                wbsp.Stylesheet.Save();

                Columns columns = new Columns();
                uint columnIndex = 1;
                foreach (double itemLength in columnsWidth)
                {
                    columns.Append(CreateColumnData(columnIndex, columnIndex, itemLength));
                    columnIndex++;
                }
                ws.Append(columns);

                ws.Append(sd);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.SheetId = 1;
                sheet.Name = sheetName;
                SetSheetId(0, sheet.SheetId);
                sheet.Id = wbp.GetIdOfPart(wsp);
                sheets.Append(sheet);
                wb.Append(fv);
                wb.Append(sheets);

                xl.WorkbookPart.Workbook = wb;
                xl.WorkbookPart.Workbook.Save();

                for (int i = 0; ++i < ExportExcelParam.SheetCount; )
                {
                    InsertWorksheet(xl, i, columnsWidth);
                }

                xl.Close();
            }
        }

        public long FillWorkbook(string filePath, int numSheet, long rowTotalCountLast, GetPageDataInvoiceDelegate GetPageData)
        {
            long rowTotalCount = rowTotalCountLast;
            using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(filePath, true))
            {
                WorkbookPart workbookPart = myDoc.WorkbookPart;
                List<Sheet> sheetsAll = myDoc.WorkbookPart.Workbook.Descendants<Sheet>().ToList();
                Sheet sheetOne = sheetsAll.Where(p => p.SheetId == GetSheetId(numSheet)).Single();
                WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheetOne.Id);
                string origninalSheetId = workbookPart.GetIdOfPart(worksheetPart);

                WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
                string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

                OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
                OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

                bool isBreak = false;
                Row r = new Row();
                while (reader.Read())
                {
                    if (reader.ElementType == typeof(SheetData))
                    {
                        if (reader.IsEndElement)
                            continue;
                        writer.WriteStartElement(new SheetData());

                        //--------------------
                        DataRow dr;
                        long rowAbsolute = -1;
                        long sheetTotalRowCount = 0;
                        long rowsTotalMatches = 0;
                        long rowCountHeader = 0;
                        uint pageFrom = (uint)(ExportExcelParam.SheetSizePage * (numSheet) + 1);
                        uint pageTo = (uint)(ExportExcelParam.SheetSizePage * (numSheet + 1) + 1);
                        for (uint page = pageFrom; page < pageTo; page++)
                        {
                            DataTable dtRows = GetPageData(pageFrom, page, ExportExcelParam, out rowsTotalMatches, out rowCountHeader);

                            bool isGood = true;
                            if (page < (pageTo - 1) && dtRows.Rows.Count < ExportExcelParam.PageSize)
                                isGood = false;
                            else if (page == (pageTo - 1) && dtRows.Rows.Count < (ExportExcelParam.InvoiceCount - ((pageTo - 2) * ExportExcelParam.PageSize)))
                                isGood = false;

                            if (!isGood)
                            {
                                for (int repeate = -1; ++repeate < 5; )
                                {
                                    dtRows = GetPageData(pageFrom, page, ExportExcelParam, out rowsTotalMatches, out rowCountHeader);
                                    if (page < (pageTo - 1) && dtRows.Rows.Count >= ExportExcelParam.PageSize)
                                    { isGood = true; break; }
                                    else if (page == (pageTo - 1) && dtRows.Rows.Count >= (ExportExcelParam.InvoiceCount - ((pageTo - 2) * ExportExcelParam.PageSize)))
                                    { isGood = true; break; }
                                }
                                if (!isGood)
                                    throw new Exception("Сервер вернул пустые или неполные данные");
                            }
                            for (int row = 0; row < dtRows.Rows.Count; row++)
                            {
                                dr = dtRows.Rows[row];
                                rowAbsolute++;
                                SetRowHeight(r, rowAbsolute);
                                ExcelRowStyle excelRowStyle = GetExcelRowStyle(rowAbsolute);

                                writer.WriteStartElement(r);
                                for (int col = 0; col < dtRows.Columns.Count; col++)
                                {
                                    Type typeValue = dr[col].GetType();
                                    Cell c = CreateCell(rowAbsolute, excelRowStyle, dr[col]);
                                    if (ReportGeneralHelper.IsNumberType(typeValue))
                                    {
                                        c.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                        c.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(GenerateExcelNumberValue(dr[col], typeValue));
                                    }
                                    else
                                    {
                                        c.DataType = CellValues.InlineString;
                                        InlineString inlineString = new InlineString();
                                        Text t = new Text();
                                        t.Text = dr[col].ToString();
                                        inlineString.AppendChild(t);
                                        c.AppendChild(inlineString);
                                    }
                                    writer.WriteElement(c);
                                }
                                writer.WriteEndElement();

                                if (!ExportExcelParam.ResetEvent.IsSet)
                                {
                                    isBreak = true;
                                    break;
                                }
                                sheetTotalRowCount++;
                                sheetTotalRowCount = sheetTotalRowCount - rowCountHeader;
                                if (sheetTotalRowCount < 0)
                                    sheetTotalRowCount = 0;
                                rowTotalCount = rowTotalCountLast + sheetTotalRowCount;
                                GenerateEventExt(rowTotalCount);
                            }
                            if (isBreak) { break; }
                        }
                        //--------------------

                        writer.WriteEndElement();
                    }
                    else
                    {
                        if (reader.IsStartElement)
                        {
                            writer.WriteStartElement(reader);
                        }
                        else if (reader.IsEndElement)
                        {
                            writer.WriteEndElement();
                        }
                    }
                }

                reader.Close();
                writer.Close();

                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(origninalSheetId)).First();
                sheet.Id.Value = replacementPartId;
                workbookPart.DeletePart(worksheetPart);
            }
            return rowTotalCount;
        }
    }
}
