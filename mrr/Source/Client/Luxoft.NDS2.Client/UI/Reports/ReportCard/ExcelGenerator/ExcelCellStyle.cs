﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator
{
    public class ExcelCellStyle
    {
        public CellFillColorStyle FillColorStyle = CellFillColorStyle.Default;
    }

    public enum CellFillColorStyle
    {
        Default = 0,
        Red
    }
}