﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard
{
    public interface IBasePresenter : IServiceCallWrapper
    {
        object GetSummaryReportByColumnName(string name);
        void ExcelSetup();
        GridSetup CommonSetup(string key);
        ExcelManagerReport GetExcelManager();
        string GetReportNameGeneric();
        string GetReportCaptionGeneric();
        string GetReportTitle();
        IDataService GetDataService();

        ISecurityService SecurityHandling { get; }
        IEnumerable<AccessRight> CsudAccessRights { get; }
    }
}
