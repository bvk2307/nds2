﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class ColumnFormats
    {
        public static readonly string FormatPercent = "{0:0.0}%";
        public static readonly string FormatPercentGrid = "0:0.0";
        public static readonly string FormatRuble = "{0:N2}р.";
        public static readonly string FormatRubleGrid = "0:N2";
        public static readonly string FormatDecimal = "{0:N}";
        public static readonly string FormatDecimalGrid = "0:N";
        public static readonly string FormatLong = "{0:N0}";
        public static readonly string FormatLongGrid = "0:N0";
        public static readonly IFormatProvider FormatInfoPercent = new ColumnCustomFormatter("%");
        public static readonly IFormatProvider FormatInfoRuble = new ColumnCustomFormatter("р.");
        public static readonly IFormatProvider FormatInfoDigit = new ColumnCustomFormatter();
        public static readonly IFormatProvider FormatInfoDigitOrDate = new DigitOrDateFormatter();
        
    }
}
