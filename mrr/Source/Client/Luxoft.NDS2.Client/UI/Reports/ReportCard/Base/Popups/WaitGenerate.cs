﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base.Popups
{
    public partial class WaitGenerate : Form
    {
        public Action WorkDelegate { get; set; }

        public WaitGenerate()
        {
            InitializeComponent();

            _timer.Interval = 200;
            _timer.Tick += timer_Tick;
        }

        private void ultraButtonGo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private int ExcelRowCount = 0;
        private Timer _timer = new Timer();

        public void SetCountGenerateRows(int countRows)
        {
            ExcelRowCount = countRows;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            ultraLabelExcelRowCount.Text = ExcelRowCount.ToString();
        }

        private void WaitGenerate_Load(object sender, EventArgs e)
        {
            if (WorkDelegate == null)
            {
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                ExcelRowCount = 0;
                ultraLabelExcelRowCount.Text = "0";
                _timer.Enabled = true;
                WorkDelegate();
            }
        }

        private void WaitGenerate_FormClosed(object sender, FormClosedEventArgs e)
        {
            _timer.Enabled = false;
        }
    }
}
