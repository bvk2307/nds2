﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class ReportSummaryCalculator : ICustomSummaryCalculator
    {
        private Func<string, object> _GetSummary;

        public ReportSummaryCalculator(Func<string, object> GetSummary)
        {
            _GetSummary = GetSummary;
        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {

        }

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            return _GetSummary(summarySettings.SourceColumn.Key);
        }
    }
}
