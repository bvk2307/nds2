﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Microsoft.Practices.CompositeUI.Commands;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base.Popups;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportsList;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public partial class View : BaseView
    {
        private readonly PresentationContext presentationContext;
        private Presenter _presenter;
        private UserControl _ctrlSearchCritera = null;
        private readonly WaitGenerate _frmWaitGenerate = new WaitGenerate();
        private string ReportKey { get; set; }
        private readonly Dictionary<string, Func<QueryConditions, bool>> _reportsChecks = new Dictionary<string, Func<QueryConditions, bool>>();

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.View = this;
                _presenter.PresentationContext = presentationContext;
                base._presentationContext = presentationContext;
                this.WorkItem = _presenter.WorkItem;
                InitData();
            }
        }

        public View(PresentationContext context)
        {
            ReportKey = "ReportTest";
            presentationContext = context;
            InitializeComponent();
        }

        public View(PresentationContext context, string reportKey)
            : base(context)
        {
            presentationContext = context;
            InitializeComponent();
            ReportKey = reportKey;
            InitCheckQueryConditionActions();
        }

        public View(PresentationContext context, WorkItem wi, string reportKey)
            : base(context, wi)
        {
            presentationContext = context;
            base.WorkItem = wi;
            InitializeComponent();
            ReportKey = reportKey;
            InitCheckQueryConditionActions();
        }

        private void InitData()
        {
            _presenter.Init();
            _presenter.ReportKey = ReportKey;
            SetupControlSearchCriteria();
            InitializeRibbon();
            grid.Setup = _presenter.GetGridSetup();
            grid.AggregatePanelVisible = _presenter.IsGridAggreagateVisible();
        }

        private void SetupControlSearchCriteria()
        {
            _ctrlSearchCritera = _presenter.GetControlSearchCriteria();
            _ctrlSearchCritera.Parent = ultraGroupBoxSearchCriteria;
            IControlSearchCriteria ctrl = (IControlSearchCriteria)_ctrlSearchCritera;

            ctrl.SetupDependingFromReportKey(ReportKey);
            tableLayoutPanelMain.RowStyles[0].Height = _ctrlSearchCritera.Height + 30;
            ultraGroupBoxSearchCriteria.Height = _ctrlSearchCritera.Height + 30;
            _ctrlSearchCritera.Dock = DockStyle.Fill;

            ctrl.ChangeControlParameters += (sender, e) =>
            {
                Dictionary<string, GridColumnHeaderChanage> changeColumns = _presenter.GetChangeGridColumn();
                GetGridMain().ChangeColumnsCaption(changeColumns);
                IControlSearchCriteria ctrlSearch = (IControlSearchCriteria)_ctrlSearchCritera;
                bool isCorrectParameters = ctrlSearch.IsCorrectParameters();
                UpdateRibbonButton(isCorrectParameters);

                ultraGroupBoxGridList.Text = _presenter.GetReportCaption();
                if (ultraGroupBoxGridList.Text.Contains("фоновых"))
                    grid.Title = _presenter.GetReportTitle();
            };

            ctrl.RequireChangeColumns += (sender, e) =>
            {
                if (_presenter.IsClearResultDataAfterChangeCriteria())
                {
                    _presenter.IsFixCriteria = false;
                    grid.UpdateData();
                }
            };
        }

        private string GetNalogPeriodName(IControlSearchCriteria ctrlSearch)
        {
            QueryConditions qc = ctrlSearch.GetQueryConditions();
            int nalogPeriod = (int)_presenter.GetFilterFromCondition(qc, "NalogPeriod");
            int year = (int)_presenter.GetFilterFromCondition(qc, "Year");
            string nalogPeriodName = string.Format("{0} кв. {1}", nalogPeriod);
            return nalogPeriodName;
        }

        private UcRibbonButtonToolContext btnReportCreate;
        private UcRibbonButtonToolContext btnReportToExcel;

        private void UpdateRibbonButton(bool isEnabled)
        {
            btnReportCreate.Enabled = isEnabled;
            btnReportToExcel.Enabled = isEnabled;
            _presenter.RefreshEKP();
        }

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            btnReportCreate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Create", "cmdReport" + "Create")
            {
                Text = "Сформировать",
                ToolTipText = "Сформировать отчет",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "report_create",
                SmallImageName = "report_create",
                Enabled = true
            };

            btnReportToExcel = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "ToExcel", "cmdReport" + "ToExcel")
            {
                Text = "Экспортировать",
                ToolTipText = "Экспортировать в Excel",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "excel_create",
                SmallImageName = "excel_create",
                Enabled = true
            };


            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnReportCreate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnReportToExcel.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            base._presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnReportCreate,
                btnReportToExcel,
                tabNavigator
            });

            base._presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdReportCreate")]
        public virtual void ReportCreateBtnClick(object sender, EventArgs e)
        {
            ReportCreate();
        }

        [CommandHandler("cmdReportToExcel")]
        public virtual void ReportToExcelBtnClick(object sender, EventArgs e)
        {
            try
            {
                saveFileDialogReport.FileName = _presenter.GenerateReportFileName();
                DialogResult dlgRez = saveFileDialogReport.ShowDialog();
                if (dlgRez == DialogResult.OK || dlgRez == DialogResult.Yes)
                {
                    _presenter.ReportExcelFilePathTemp = _presenter.GenerateReportFileNameTemp();
                    _presenter.ReportExcelFilePath = saveFileDialogReport.FileName;
                    _presenter.GenerateExcelStart();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        public void OpenExcelFile(string reportExcelFilePath)
        {
            var ret = ShowQuestion("Генерация отчета", string.Format("Отчет \"{0}\" сформирован. Открыть файл?", _presenter.GetReportName()));
            if (ret == DialogResult.Yes)
            {
                _presenter.OpenReportExcelFile(reportExcelFilePath);
            }
        }

        public void ShowWaitGenerateWindow(Action startWorkDelegate)
        {
            _frmWaitGenerate.WorkDelegate = startWorkDelegate;
            _frmWaitGenerate.ShowDialog();
        }

        public void HideWaitGenerateWindow()
        {
            _frmWaitGenerate.DialogResult = DialogResult.OK;
        }

        public WaitGenerate GetFrmWaitGenerate()
        {
            return _frmWaitGenerate;
        }

        private void InitCheckQueryConditionActions()
        {
            _reportsChecks.Clear();
            _reportsChecks.Add(ReportNames.InspectorMonitoringWork, CheckQueryConditionDefault);
            _reportsChecks.Add(ReportNames.MonitorProcessDeclaration, CheckQueryConditionDefault);
            _reportsChecks.Add(ReportNames.LoadingInspection, CheckQueryConditionDefault);
            _reportsChecks.Add(ReportNames.CheckControlRatio, CheckQueryConditionDefault);
            _reportsChecks.Add(ReportNames.MatchingRule, CheckQueryConditionEmpty);
            _reportsChecks.Add(ReportNames.CheckLogicControl, CheckQueryConditionDefault);
            _reportsChecks.Add(ReportNames.DeclarationStatistic, CheckQueryConditionEmpty);
        }

        private void ReportCreate()
        {
            QueryConditions conditionsCtrl = ((IControlSearchCriteria)_presenter.GetControlSearchCriteria()).GetQueryConditions();
            if (CheckQueryCondition(conditionsCtrl))
            {
                if (grid.Setup.GetData == null)
                {
                    grid.Setup.GetData = _presenter.GetRows;
                }
                grid.ApplyPageNumberWithoutRequest();
                QueryConditions conditionsGrid = grid.QueryConditions;
                _presenter.GetRowsFromBD(conditionsGrid, false);
            }
        }

        private bool CheckQueryCondition(QueryConditions conditions)
        {
            bool ret = true;
            if (_reportsChecks.ContainsKey(ReportKey))
            {
                ret = _reportsChecks[ReportKey].Invoke(conditions);
            }
            return ret; 
        }

        private bool CheckQueryConditionDefault(QueryConditions conditions)
        {
            bool ret = true;
            if (conditions.Filter.Where(p => p.ColumnName == "FederalDistrict").Count() == 0)
            {
                ret = false;
                ShowNotification("Необходимо указать не менее одного федерального округа");
            }
            else if (conditions.Filter.Where(p => p.ColumnName == "TaxPayerRegionCode").Count() == 0)
            {
                ret = false;
                ShowNotification("Необходимо указать не менее одного региона");
            }
            else if (conditions.Filter.Where(p => p.ColumnName == "NalogOrganCode").Count() == 0)
            {
                ret = false;
                ShowNotification("Необходимо указать не менее одной инспекции");
            }

            return ret;
        }

        private bool CheckQueryConditionEmpty(QueryConditions conditions)
        {
            return true;
        }

        public GridControl GetGridMain()
        {          
            return grid;
        }
    }
}
