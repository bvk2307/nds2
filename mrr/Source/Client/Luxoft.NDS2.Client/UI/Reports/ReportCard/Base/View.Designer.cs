﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.ultraGroupBoxGridList = new Infragistics.Win.Misc.UltraGroupBox();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.ultraGroupBoxSearchCriteria = new Infragistics.Win.Misc.UltraGroupBox();
            this.saveFileDialogReport = new System.Windows.Forms.SaveFileDialog();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGridList)).BeginInit();
            this.ultraGroupBoxGridList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxSearchCriteria)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.ultraGroupBoxGridList, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.ultraGroupBoxSearchCriteria, 0, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1079, 546);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // ultraGroupBoxGridList
            // 
            this.ultraGroupBoxGridList.Controls.Add(this.grid);
            this.ultraGroupBoxGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxGridList.Location = new System.Drawing.Point(3, 188);
            this.ultraGroupBoxGridList.Name = "ultraGroupBoxGridList";
            this.ultraGroupBoxGridList.Size = new System.Drawing.Size(1073, 355);
            this.ultraGroupBoxGridList.TabIndex = 0;
            this.ultraGroupBoxGridList.Text = "Отчет";
            // 
            // grid
            // 
            this.grid.AddVirtualCheckColumn = false;
            this.grid.AggregatePanelVisible = true;
            this.grid.AllowMultiGrouping = true;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.grid.AllowSaveFilterSettings = false;
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.grid.DefaultPageSize = "200";
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.ExportExcelCancelVisible = false;
            this.grid.ExportExcelVisible = false;
            this.grid.FilterResetVisible = false;
            this.grid.FooterVisible = true;
            this.grid.GridContextMenuStrip = null;
            this.grid.Location = new System.Drawing.Point(3, 16);
            this.grid.Name = "grid";
            this.grid.PanelExportExcelStateVisible = false;
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = true;
            this.grid.RowDoubleClicked = null;
            this.grid.Setup = null;
            this.grid.Size = new System.Drawing.Size(1067, 336);
            this.grid.TabIndex = 13;
            this.grid.Title = "";
            // 
            // ultraGroupBoxSearchCriteria
            // 
            this.ultraGroupBoxSearchCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBoxSearchCriteria.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBoxSearchCriteria.Name = "ultraGroupBoxSearchCriteria";
            this.ultraGroupBoxSearchCriteria.Size = new System.Drawing.Size(1073, 154);
            this.ultraGroupBoxSearchCriteria.TabIndex = 1;
            this.ultraGroupBoxSearchCriteria.Text = "Параметры отбора";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1079, 546);
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGridList)).EndInit();
            this.ultraGroupBoxGridList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxSearchCriteria)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxGridList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxSearchCriteria;
        private System.Windows.Forms.SaveFileDialog saveFileDialogReport;
        private Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl grid;
    }
}
