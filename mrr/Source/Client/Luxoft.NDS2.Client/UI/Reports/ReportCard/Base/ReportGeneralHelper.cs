﻿using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.App.Processes;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;
using FLS.CommonComponents.Lib.Serialization;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class ReportGeneralHelper
    {
        public string _region = null;

        /// <summary> Copies visible columns in new ones, for example, to do not share originals with other thread. </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ColumnBase> CopyVisibleColumnsOnly( IReadOnlyCollection<ColumnBase> columns )
        {
            List<ColumnBase> rets = new List<ColumnBase>( columns.Count );
            ColumnDefinition colDefinition = null;
            ColumnGroupDefinition colGroupCurrent = null, colGroupPrevious = null;
            int groupCountCur = 0;

            Action<ColumnGroupDefinition, int, ColumnBase, bool> addColumnCopyHandler = 
                ( colGroupPrevPar, groupCountPar, columnPar, forceAdd ) => 
                {
                    if ( colGroupPrevPar != null && groupCountPar > 0 || forceAdd )
                        rets.Add( SerializationDeepCloner.Clone( columnPar ) );
                };

            foreach ( Tuple<ColumnDefinition, ColumnGroupDefinition> colDefinitions in new ColumnDependencyIterator( columns ) )
            {
                colDefinition = colDefinitions.Item1;
                colGroupCurrent = colDefinitions.Item2;

                if ( colGroupCurrent == null || !object.ReferenceEquals( colGroupCurrent, colGroupPrevious ) ) //the previous group has been end (if it presents)
                {
                    addColumnCopyHandler( colGroupPrevious, groupCountCur, colGroupPrevious, false );  //ignores if 'colGroupPrevious != null && groupCountCur > 0'

                    colGroupPrevious = colGroupCurrent;
                    groupCountCur = 0;
                }
                if ( colDefinition.Visibility == ColumnVisibility.Visible )
                {
                    if ( colGroupCurrent == null ) //has not a group column
                        addColumnCopyHandler( null, 0, colDefinition, true );   //forces to add a column without a group one
                    else   //has not a group column
                        ++groupCountCur;
                }
            }
            //adds the last group if it presents and is not added yet
            addColumnCopyHandler( colGroupPrevious, groupCountCur, colGroupPrevious, false );  //ignores if 'colGroupPrevious != null && groupCountCur > 0'

            return rets.ToReadOnly();
        }

        public List<ColumnBase> GetVisibleColumns(List<ColumnBase> columns)
        {
            List<ColumnBase> rets = new List<ColumnBase>();
            ColumnDefinition columnDef;
            ColumnGroupDefinition columnGroupDef;
            foreach (ColumnBase itemColumn in columns)
            {
                columnDef = itemColumn as ColumnDefinition;
                if (columnDef != null)
                {
                    if (columnDef.Visibility == ColumnVisibility.Visible)
                    {
                        rets.Add(itemColumn);
                    }
                }
                else
                {
                    columnGroupDef = itemColumn as ColumnGroupDefinition;
                    if (columnGroupDef != null)
                    {
                        List<ColumnDefinition> columnsGroupVisibility = new List<ColumnDefinition>();
                        foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                        {
                            if (itemColumnDef != null)
                            {
                                if (itemColumnDef.Visibility == ColumnVisibility.Visible)
                                {
                                    columnsGroupVisibility.Add(itemColumnDef);
                                }
                            }
                        }
                        columnGroupDef.Columns.Clear();
                        columnGroupDef.Columns.AddRange(columnsGroupVisibility);
                        if (columnsGroupVisibility.Count > 0)
                        {
                            rets.Add(columnGroupDef);
                        }
                    }
                }
            }
            return rets;
        }

        public void AddHeaderForExcel( DataTable dtRows, IReadOnlyCollection<ColumnBase> columns, bool isObjectNumbers = true )
        {
            List<object> objsGroup = new List<object>();
            List<object> objs = new List<object>( columns.Count );
            List<object> objsNumbers = null;
            if ( isObjectNumbers )
                objsNumbers = new List<object>( columns.Count );
            bool isFirstInGroup, hasGroup = false;
            ColumnDefinition colDefinition = null;
            ColumnGroupDefinition colGroupCurrent = null, colGroupPrevious = null;

            foreach ( Tuple<ColumnDefinition, ColumnGroupDefinition> colDefinitions in new ColumnDependencyIterator( columns ) )
            {
                colDefinition = colDefinitions.Item1;
                colGroupCurrent = colDefinitions.Item2;
                isFirstInGroup = !object.ReferenceEquals( colGroupCurrent, colGroupPrevious );
                string groupCaption = string.Empty;

                if ( colGroupCurrent == null || isFirstInGroup ) //the previous group has been end (if it presents)
                    colGroupPrevious = colGroupCurrent;

                if ( colGroupCurrent != null ) //has a group column
                {
                    hasGroup = true;
                    if ( isFirstInGroup ) //it is the first column in a group
                        groupCaption = colGroupCurrent.Caption;
                }
                objs.Add( colDefinition.Caption );
                if ( isObjectNumbers )
                    objsNumbers.Add( string.Concat( "(", objs.Count, ")" ) );
                objsGroup.Add( groupCaption );
            }
            if ( !string.IsNullOrWhiteSpace( _region ) )
                dtRows.Rows.Add( _region );
            if ( hasGroup )
                dtRows.Rows.Add( objsGroup.ToArray() );
            dtRows.Rows.Add( objs.ToArray() );
            if ( isObjectNumbers )
                dtRows.Rows.Add( objsNumbers.ToArray() );
        }

        public void AddSummaryRowForExcel(DataTable dtRows, List<ColumnBase> columns, Dictionary<string, object> _summaryRow)
        {
            List<object> objs = new List<object>( columns.Count );

            foreach ( ColumnDefinition columnDefinition in new ColumnDefinitionIterator( columns ) )
            {
                objs.Add( GetSummaryRowItemValue( columnDefinition.Key, _summaryRow ) );
            }
            objs[0] = "ИТОГО";
            dtRows.Rows.Add( objs.ToArray() );
        }

        private string GetSummaryRowItemValue(string key, Dictionary<string, object> _summaryRow)
        {
            string ret = "0";
            if (_summaryRow.ContainsKey(key))
            {
                ret = _summaryRow[key].ToString();
            }
            return ret;
        }

        /// <summary> </summary>
        /// <param name="itemColumnDef"></param>
        /// <param name="objs"></param>
        /// <param name="dicts"></param>
        public void AddRowItem( ColumnDefinition itemColumnDef, List<object> objs, Dictionary<string, object> dicts)
        {
            object objAdded;
            AddRowItem(itemColumnDef, objs, dicts, out objAdded);
        }

        /// <summary> </summary>
        /// <param name="itemColumnDef"></param>
        /// <param name="objs"></param>
        /// <param name="dicts"></param>
        /// <param name="objectAdded"></param>
        /// <returns> The added data item if it is found otherwise 'null'. </returns>
        public bool AddRowItem(ColumnDefinition itemColumnDef, List<object> objs, Dictionary<string, object> dicts, out object objectAdded)
        {
            objectAdded = null;
            bool added = false;
            foreach (KeyValuePair<string, object> itemRow in dicts)
            {
                if (itemColumnDef.Key == itemRow.Key)
                {
                    objs.Add(objectAdded = itemRow.Value);
                    added = true;
                    break;
                }
            }
            return added;
        }

        /// <summary> Converts entities <paramref name="entities"/> to dictionaries. </summary>
        /// <remarks> ATTENTION! The method converts to a dictionary of all public properties of <paramref name="entities"/> if <paramref name="transformer"/> is not defined. </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="transformer"> An optional transformer. The method uses reflection to convert entities <paramref name="entities"/> to dictionaries of its all public properties if this is not defined (by default). </param>
        /// <returns></returns>
        public List<Dictionary<string, object>> ConvertToDictionary<T>( 
                            ICollection<T> entities, Func<T, Dictionary<string, object>> transformer = null )
        {
            if ( transformer == null )
                transformer = ConvertToDictionaryAllPublicProperties;
            List<Dictionary<string, object>> rowsDict = new List<Dictionary<string, object>>( entities.Count );
            foreach ( T entity in entities )
            {
                Dictionary<string, object> row = transformer( entity );
                rowsDict.Add( row );
            }
            return rowsDict;
        }

        /// <summary> Creates a dictionary pair by an entity getting propery expression. </summary>
        /// <typeparam name="TEntity"> An entity type. </typeparam>
        /// <typeparam name="TValue"> A property value type. </typeparam>
        /// <param name="propertyExpression"> An entity getting propery expression (only like: 'entity => entity.SomeProperty'). </param>
        /// <param name="entity"></param>
        /// <returns> A new dictionary pair with a proerty name as a key and property value as a value. </returns>
        public static KeyValuePair<string, object> GetDictionaryItem<TEntity, TValue>( Expression<Func<TEntity, TValue>> propertyExpression, TEntity entity )
        {
            MemberExpression propertyBody = (MemberExpression)propertyExpression.Body;
            string propertyName = propertyBody.Member.Name;
            TValue value = propertyExpression.Compile().Invoke( entity );
            KeyValuePair<string, object> keyValuePair = new KeyValuePair<string, object>( propertyName, value );

            return keyValuePair;
        }

        /// <summary> Places values of all public properties of <paramref name="entity"/> into a new dictionary. </summary>
        /// <remarks> ATTENTION! It uses reflection inside. </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ConvertToDictionaryAllPublicProperties<T>( T entity )
        {
            Dictionary<string, object> row = new Dictionary<string, object>( 16 );
            Type typeClass = entity.GetType();
            PropertyInfo[] properties = typeClass.GetProperties();
            foreach ( PropertyInfo itemProp in properties )
            {
                string name = itemProp.Name;
                object value = itemProp.GetValue( entity, null );
                row.Add( name, value );
            }
            return row;
        }

        public List<Dictionary<string, object>> ConvertToDictionaryWithNormalType<T>(ICollection<T> entities)
        {
            List<Dictionary<string, object>> rowsDict = new List<Dictionary<string, object>>(entities.Count);
            foreach (object itemTest in entities)
            {
                Dictionary<string, object> row = new Dictionary<string, object>(16);
                Type typeClass = itemTest.GetType();
                PropertyInfo[] properties = typeClass.GetProperties();
                foreach (PropertyInfo itemProp in properties)
                {
                    string name = itemProp.Name;
                    object value = itemProp.GetValue(itemTest, null);
                    row.Add(name, ConvertValueToNormalType(value));
                }
                rowsDict.Add(row);
            }
            return rowsDict;
        }

        private object ConvertValueToNormalType(object obj)
        {
            object objRet = obj;
            if (obj != null)
            {
                string strValue = obj.ToString();
                decimal decVal = 0;
                long longVal = 0;
                int intVal = 0;
                double doubleVal = 0;
                TimeSpan timeSpan = new TimeSpan();
                if (decimal.TryParse(strValue, out decVal))
                {
                    objRet = decVal;
                }
                else if (long.TryParse(strValue, out longVal))
                {
                    objRet = longVal;
                }
                else if (int.TryParse(strValue, out intVal))
                {
                    objRet = intVal;
                }
                else if (double.TryParse(strValue, out doubleVal))
                {
                    objRet = longVal;
                }
                else if (TimeSpan.TryParse(strValue, out timeSpan))
                {
                    objRet = timeSpan;
                }
            }
            return objRet;
        }

        public List<Dictionary<string, object>> ConvertToColumnFormat( ICollection<Dictionary<string, object>> rows, bool priorityOfExcelFormats = false )
        {
            List<Dictionary<string, object>> rowsNew = new List<Dictionary<string, object>>( rows.Count );
            foreach ( Dictionary<string, object> itemRow in rows )
            {
                rowsNew.Add( ConvertToColumnFormat( itemRow, priorityOfExcelFormats ) );
            }
            return rowsNew;
        }

        public Dictionary<string, object> ConvertToColumnFormat( IDictionary<string, object> row, bool priorityOfExcelFormats = false )
        {
            Dictionary<string, object> rowNew = new Dictionary<string, object>( row.Count );
            foreach ( KeyValuePair<string, object> item in row )
            {
                bool? convertedToString = null;
                if ( priorityOfExcelFormats )
                {
                    if ( !( convertedToString = ConvertToColumnExcelFormat( item, rowNew ) ).HasValue )
                        convertedToString = ConvertForCommonTypesToExcel( item, rowNew );
                }
                else
                {
                    if ( !( convertedToString = ConvertForCommonTypesToExcel( item, rowNew ) ).HasValue )
                        convertedToString = ConvertToColumnExcelFormat( item, rowNew, forceFormatToString: true );
                }
                if ( !convertedToString.HasValue )
                    rowNew.Add( item.Key, item.Value ); //no covertion, adds as it is
            }
            return rowNew;
        }

        /// <summary> Converts a value of <paramref name="itemValue"/> according to its type if this type is common one to represent in Excel's report. </summary>
        /// <param name="itemValue"></param>
        /// <param name="newValue"> Returns a converted value if the conversaition was. </param>
        /// <returns> 'true' if a value of <paramref name="itemValue"/> is converted to <see cref="string"/>, 'false' if it is conveted to some other type or 'null' if the conversation was not. </returns>
        public static bool? ConvertForCommonTypesToExcel( object itemValue, out object newValue )
        {
            newValue = null;
            bool? convertedToString = null;

            //--- для числовых значений передаем без форматирования и преобразование в строку
            //--- хотя зачем тогда форматирование для Экселя
            if ( IsNumberType( itemValue ) )
            {
                newValue = itemValue;

                convertedToString = false;
            }
            else if ( itemValue is bool )
            {
                string strValue = (bool)itemValue ? "Да" : "Нет";
                newValue = strValue;

                convertedToString = true;
            }
            else if ( itemValue is DateTime )
            {
                string strValue = ( (DateTime)itemValue ).ToShortDateString();
                newValue = strValue;

                convertedToString = true;
            }
            return convertedToString;
        }

        private bool? ConvertForCommonTypesToExcel( KeyValuePair<string, object> item, IDictionary<string, object> rowNew )
        {
            object newValue;
            bool? convertedToString = ConvertForCommonTypesToExcel( item.Value, out newValue );
            if ( convertedToString.HasValue )
                rowNew.Add( item.Key, newValue );
            return convertedToString;
        }

        private bool? ConvertToColumnExcelFormat( 
            KeyValuePair<string, object> item, IDictionary<string, object> rowNew, bool forceFormatToString = false )
        {
            bool? convertedToString = null;

            string formatExcel = GetColumnExcelFormat( item.Key );
            if ( formatExcel == null )
            {
                if ( forceFormatToString )
                    formatExcel = "{0}";
            }
            if ( formatExcel != null )
            {
                IFormatProvider formatInfoExcel = GetColumnExcelFormatInfo( item.Key );
                rowNew.Add( item.Key, ConvertToFormatToExcel( item.Value, formatExcel, formatInfoExcel ) );

                convertedToString = true;
            }
            return convertedToString;
        }

        public static string ConvertToFormatToExcel( object itemValue, string formatExcel, IFormatProvider formatInfoExcel = null )
        {
            Contract.Requires( formatExcel != null );

            return string.Format( formatInfoExcel, formatExcel, itemValue );
        }

        public static bool IsNumberType(object value)
        {
            bool ret = false;
            if (value != null)
            {
                ret = IsNumberType(value.GetType());
            }
            return ret;
        }

        /// <summary> Is a type if known numeric one? Knows the same types that <see cref="GetExcelNumericValue"/>() knows too. </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNumberType(Type type)
        {
            bool ret = false;
            if (type == typeof(decimal) || type == typeof(double) || type == typeof(long) || type == typeof(int) ||
                type == typeof(decimal?) || type == typeof(double?) || type == typeof(long?) || type == typeof(int?))
            {
                ret = true;
            }
            return ret;
        }

        /// <summary> Retrieves a numeric value from <paramref name="value"/>. Knows only the types from <see cref="IsNumberType"/>(). </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="InvalidCastException"> If <paramref name="value"/> is not of numeric type. </exception>
        /// <exception cref="ArgumentException"> If a parameter type is unknown. </exception>
        public static decimal GetExcelNumericValue( object value )
        {
            IConvertible convertible = value as IConvertible;
            if ( convertible == null )
                throw new InvalidCastException( string.Format( "A value: '{0}' of parameter 'value' does not implement IConvertible", value ) );

            TypeCode typeCode = convertible.GetTypeCode();
            decimal dValue = 0;
            switch ( typeCode )
            {case TypeCode.Decimal:
                dValue = (decimal)convertible;
                break;;
            case TypeCode.Double:
                dValue = (decimal)(double)convertible;
                break;;
            case TypeCode.Int64:
                dValue = (long)convertible;
                break;;
            case TypeCode.Int32:
                dValue = (int)convertible;
                break;;
             default:
                throw new ArgumentException( string.Format( "The type: {0} of the parameter with value: '{1}' is unknown", typeCode, value ), "value" );
            }

            return dValue;
        }

        private Dictionary<string, string> columnsFormat = null;
        private Dictionary<string, IFormatProvider> columnsFormatInfo = null;

        public void GenerateColumnsFormat( IReadOnlyCollection<ColumnBase> columns )
        {
            if ( null == columnsFormat )
            {
                columnsFormat = new Dictionary<string, string>( columns.Count );
                columnsFormatInfo = new Dictionary<string, IFormatProvider>( columns.Count );
                foreach ( ColumnDefinition columnDefinition in new ColumnDefinitionIterator( columns ) )
                {
                    if ( columnDefinition.FormatExcel != null )
                        columnsFormat.Add( columnDefinition.Key, columnDefinition.FormatExcel );
                    if ( columnDefinition.FormatInfoExcel != null )
                        columnsFormatInfo.Add( columnDefinition.Key, columnDefinition.FormatInfoExcel );
                }
            }
        }

        private string GetColumnExcelFormat(string key)
        {
            string formatExcel = null;
            if (columnsFormat != null && columnsFormat.ContainsKey(key))
            {
                formatExcel = columnsFormat[key]; 
            }
            return formatExcel;
        }

        private IFormatProvider GetColumnExcelFormatInfo(string key)
        {
            IFormatProvider formatInfoExcel = null;
            if (columnsFormatInfo != null && columnsFormatInfo.ContainsKey(key))
            {
                formatInfoExcel = columnsFormatInfo[key];
            }
            return formatInfoExcel;
        }

        public void OpenReportExcelFile(string reportExcelFilePath)
        {
            if (!FileProcessStarter.StartFileProcess(reportExcelFilePath))
                MessageBox.Show(string.Format("Ошибка открытия Excel файла отчета, = {0}", reportExcelFilePath));
        }

        public List<ExcelMergeCell> GetExcelMergeCells( IReadOnlyCollection<ColumnBase> columns, int numRowMerge = 1 )
        {
            int cellCurrentNumber = 0;
            List<string> excelColumnNames = GetExcelColumnNames();
            List<ExcelMergeCell> mergeCells = new List<ExcelMergeCell>();

            Action<ColumnGroupDefinition, ExcelMergeCell, int> endGroupCellHandler = 
                ( colGroupPrevPar, mergeCellPar, cellNumberPar ) =>
                {
                    if ( colGroupPrevPar != null && mergeCellPar != null ) //there is the previous group and the group cell has been opened so ends it
                    {
                        mergeCellPar.EndCell = string.Format( "{0}{1}", excelColumnNames[cellNumberPar], numRowMerge );
                        mergeCells.Add( mergeCellPar );
                    }
                };

            ColumnGroupDefinition colGroupCurrent = null, colGroupPrevious = null;
            bool isFirstInGroup;
            ExcelMergeCell mergeCell = null;

            foreach ( Tuple<ColumnDefinition, ColumnGroupDefinition> colDefinitions in new ColumnDependencyIterator( columns ) )
            {
                colGroupCurrent = colDefinitions.Item2;
                isFirstInGroup = !object.ReferenceEquals( colGroupCurrent, colGroupPrevious );

                if ( colGroupCurrent == null || isFirstInGroup )  //the previous group has been end (if it presents)
                {
                    endGroupCellHandler( colGroupPrevious, mergeCell, cellCurrentNumber - 1 );  //ignores if 'colGroupPrevious == null || mergeCell == null'

                    colGroupPrevious = colGroupCurrent;
                    mergeCell = null;
                }
                if ( colGroupCurrent != null ) //has a group column
                {
                    if ( mergeCell == null )
                    {
                        mergeCell = new ExcelMergeCell();
                        mergeCell.BeginCell = string.Format( "{0}{1}", excelColumnNames[cellCurrentNumber], numRowMerge );
                    }
                }
                cellCurrentNumber++;
            }
            //ends the last group cell if it presents and is not ended yet
            endGroupCellHandler( colGroupPrevious, mergeCell, cellCurrentNumber - 1 );  //ignores if 'colGroupPrevious == null || mergeCell == null'

            return mergeCells;
        }

        public List<string> GetExcelColumnNames()
        {
            return Enumerable.Range(0, 100).Select(x => GetExcelColumnOneName(x)).ToList();
        }

        private string GetExcelColumnOneName(int index)
        {
            if (index < 26)
            {
                return ((char)('A' + index)).ToString();
            }
            return GetExcelColumnOneName(index / 26 - 1) + GetExcelColumnOneName(index % 26);
        }

        public string GetDateByFormat(DateTime dt)
        {
            return string.Format("{0:00}.{1:00}.{2:0000}", dt.Day, dt.Month, dt.Year);
        }

        public void GetDateBeginAndEnd(QueryConditions conditions, out string sDtBegin, out string sDtEnd)
        {
            DateTime dtBegin = (DateTime)GetFilterFromCondition(conditions, "DateBegin");
            DateTime dtEnd = (DateTime)GetFilterFromCondition(conditions, "DateEnd");
            sDtBegin = GetDateByFormat(dtBegin);
            sDtEnd = GetDateByFormat(dtEnd);
        }

        public object GetFilterFromCondition(QueryConditions criteria, string columnName)
        {
            object ret = null;
            FilterQuery filterQuery = criteria.Filter.Where(p => p.ColumnName == columnName).FirstOrDefault();
            if (filterQuery != null)
            {
                ColumnFilter columnFilter = filterQuery.Filtering.FirstOrDefault();
                if (columnFilter != null)
                {
                    ret = columnFilter.Value;
                }
            }
            return ret;
        }

        public void ChangeColumnsCaption(List<ColumnBase> columns, Dictionary<string, GridColumnHeaderChanage> changeColumns)
        {
            ColumnDefinition columnDef;
            ColumnGroupDefinition columnGroupDef;
            foreach (KeyValuePair<string, GridColumnHeaderChanage> item in changeColumns)
            {
                foreach (ColumnBase itemColumn in columns)
                {
                    columnDef = itemColumn as ColumnDefinition;
                    if (columnDef != null)
                    {
                        if (columnDef.Key == item.Key)
                        {
                            columnDef.Caption = item.Value.Caption;
                            columnDef.ToolTip = item.Value.ToolTipText;
                            columnDef.Visibility = item.Value.Visibility;
                            break;
                        }
                    }
                    else
                    {
                        columnGroupDef = itemColumn as ColumnGroupDefinition;
                        if (columnGroupDef != null)
                        {
                            if (columnGroupDef.Key == item.Key)
                            {
                                columnGroupDef.Caption = item.Value.Caption;
                                columnGroupDef.ToolTip = item.Value.ToolTipText;
                                break;
                            }
                            bool isBreak = false;
                            foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                            {
                                if (itemColumnDef.Key == item.Key)
                                {
                                    itemColumnDef.Caption = item.Value.Caption;
                                    itemColumnDef.ToolTip = item.Value.ToolTipText;
                                    itemColumnDef.Visibility = item.Value.Visibility;
                                    isBreak = true;
                                    break;
                                }
                            }
                            if (isBreak) break;
                        }
                    }
                }
            }
        }
    }
}
