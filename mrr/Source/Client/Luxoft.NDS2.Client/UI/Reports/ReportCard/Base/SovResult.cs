﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class SovResult
    {
        public long RequestId { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsWarning { get; set; }
        public string WarningMessage { get; set; }
    }
}
