﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Reports.ReportsList;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class DeclarationStatisticView: View
    {
        public DeclarationStatisticView(PresentationContext context) : base(context, ReportNames.DeclarationStatistic)
        {
        }

        public DeclarationStatisticView(PresentationContext context, WorkItem wi) : base(context, wi, ReportNames.DeclarationStatistic)
        {
        }
    }
}
