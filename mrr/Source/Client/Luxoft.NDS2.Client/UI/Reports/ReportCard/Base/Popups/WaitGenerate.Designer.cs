﻿namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base.Popups
{
    partial class WaitGenerate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBarWait = new System.Windows.Forms.ProgressBar();
            this.ultraLabelInfo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabelExcelRowCount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButtonGo = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // progressBarWait
            // 
            this.progressBarWait.Location = new System.Drawing.Point(12, 11);
            this.progressBarWait.Name = "progressBarWait";
            this.progressBarWait.Size = new System.Drawing.Size(354, 23);
            this.progressBarWait.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarWait.TabIndex = 0;
            // 
            // ultraLabelInfo
            // 
            this.ultraLabelInfo.Location = new System.Drawing.Point(54, 44);
            this.ultraLabelInfo.Name = "ultraLabelInfo";
            this.ultraLabelInfo.Size = new System.Drawing.Size(159, 23);
            this.ultraLabelInfo.TabIndex = 1;
            this.ultraLabelInfo.Text = "Кол-во выгруженных строк:";
            // 
            // ultraLabelExcelRowCount
            // 
            this.ultraLabelExcelRowCount.Location = new System.Drawing.Point(219, 44);
            this.ultraLabelExcelRowCount.Name = "ultraLabelExcelRowCount";
            this.ultraLabelExcelRowCount.Size = new System.Drawing.Size(100, 23);
            this.ultraLabelExcelRowCount.TabIndex = 2;
            this.ultraLabelExcelRowCount.Text = "456";
            // 
            // ultraButtonGo
            // 
            this.ultraButtonGo.Location = new System.Drawing.Point(117, 71);
            this.ultraButtonGo.Name = "ultraButtonGo";
            this.ultraButtonGo.Size = new System.Drawing.Size(130, 23);
            this.ultraButtonGo.TabIndex = 3;
            this.ultraButtonGo.Text = "Продолжить";
            this.ultraButtonGo.Click += new System.EventHandler(this.ultraButtonGo_Click);
            // 
            // WaitGenerate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 106);
            this.ControlBox = false;
            this.Controls.Add(this.ultraButtonGo);
            this.Controls.Add(this.ultraLabelExcelRowCount);
            this.Controls.Add(this.ultraLabelInfo);
            this.Controls.Add(this.progressBarWait);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "WaitGenerate";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WaitGenerate_FormClosed);
            this.Load += new System.EventHandler(this.WaitGenerate_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarWait;
        private Infragistics.Win.Misc.UltraLabel ultraLabelInfo;
        private Infragistics.Win.Misc.UltraLabel ultraLabelExcelRowCount;
        private Infragistics.Win.Misc.UltraButton ultraButtonGo;
    }
}