﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class ExcelMergeCell
    {
        public string BeginCell { get; set; }
        public string EndCell { get; set; }
    }
}
