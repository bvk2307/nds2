﻿using CommonComponents.Utils.Async;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Client.UI.Reports.ReportsList;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Base
{
    public class Presenter : BasePresenter<View>, IBasePresenter
    {
        #region переменные

        private UserControl ctrlSearchCriteria = null;
        private AsyncWorker<OperationResult<bool>> _reportToExcelWorker;
        private AsyncWorker<OperationResult<int>> _reportCreateWorker;
        public string ReportKey { get; set; }
        private delegate object GetRowsDelegate(QueryConditions queryConditions, out long totalRowsNumber);
        private delegate List<Dictionary<string, object>> GetRowsDelegateExcel(QueryConditions queryConditions, out long totalRowsNumber);
        public delegate DataTable GetPageDataDelegate(uint page, out long rowsTotalMatches);
        private System.Windows.Forms.Timer _timerOpenReport = new System.Windows.Forms.Timer();
        private ExcelManagerReport excelManager = ExcelManagerCreator.CreateExcelManagerReport();
        public string ReportExcelFilePathTemp { get; set; }
        public string ReportExcelFilePath { get; set; }
        private readonly ReportGeneralHelper helper = new ReportGeneralHelper();
        private Dictionary<string, object> _summaryRow;
        private Dictionary<string, IReportPresenter> reportsPresenter = new Dictionary<string, IReportPresenter>();
        private Dictionary<string, int> reportNums = new Dictionary<string, int>();
        private RequestStateAsync _requestStateAsync = new RequestStateAsync();
        private IDataService _dataService;
        private bool _isFixCriteria = false;
        public bool IsFixCriteria { get { return _isFixCriteria; } set { _isFixCriteria = value; } }

        #endregion

        public Presenter()
            : base()
        {
        }

        public void Init()
        {
            _dataService = base.GetServiceProxy<IDataService>();
            CsudAccessRights = base.Rigths;
            CreateActionsReports();
            GenerateExcelSetup();
            _timerOpenReport.Tick += timerOpenReport_Tick;
        }

        #region InitActions

        private void CreateActionsReports()
        {
            reportsPresenter.Clear();
            var reportService = base.GetServiceProxy<IReportService>();
            reportsPresenter.Add(ReportNames.InspectorMonitoringWork, new Implementation.Report_InspectorMonitoringWork.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.LoadingInspection, new Implementation.Report_LoadingInspection.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.MonitorProcessDeclaration, new Implementation.Report_MonitorProcessDeclaration.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.CheckControlRatio, new Implementation.Report_CheckControlRatio.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.MatchingRule, new Implementation.Report_MatchingRule.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.CheckLogicControl, new Implementation.Report_CheckLogicControl.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.DeclarationStatistic, new Implementation.Report_DeclarationStatistic.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.DynamicTechnoParameter, new Implementation.Report_DynamicTechnoParameter.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.DynamicDeclarationStatistic, new Implementation.Report_DynamicDeclarationStatistic.Presenter(helper, reportService, this));
            reportsPresenter.Add(ReportNames.InfoResultsOfMatching, new Implementation.Report_InfoResultsOfMatching.Presenter(helper, reportService, this));

            reportNums.Add(ReportNames.InspectorMonitoringWork, (int)ReportNum.InspectorMonitoringWork);
            reportNums.Add(ReportNames.MonitorProcessDeclaration, (int)ReportNum.MonitorProcessDeclaration);
            reportNums.Add(ReportNames.LoadingInspection, (int)ReportNum.LoadingInspection);
            reportNums.Add(ReportNames.CheckControlRatio, (int)ReportNum.CheckControlRatio);
            reportNums.Add(ReportNames.MatchingRule, (int)ReportNum.MatchingRule);
            reportNums.Add(ReportNames.CheckLogicControl, (int)ReportNum.CheckLogicControl);
            reportNums.Add(ReportNames.DeclarationStatistic, (int)ReportNum.DeclarationStatistic);
            reportNums.Add(ReportNames.DynamicTechnoParameter, (int)ReportNum.DynamicTechnoParameter);
            reportNums.Add(ReportNames.DynamicDeclarationStatistic, (int)ReportNum.DynamicDeclarationStatistic);
            reportNums.Add(ReportNames.InfoResultsOfMatching, (int)ReportNum.InfoResultsOfMatching);
        }

        public ISecurityService SecurityHandling { get { return this.SecurityService; } }

        public IReportPresenter GetReportPresenter()
        {
            IReportPresenter ret = null;
            if (reportsPresenter.ContainsKey(ReportKey))
            {
                ret = reportsPresenter[ReportKey];
            }
            return ret;
        }

        #endregion

        #region реализация интерфейса IBasePresenter

        public object GetSummaryReportByColumnName(string name)
        {
            object ret = 0;
            if (_summaryRow != null && _summaryRow.ContainsKey(name))
            {
                ret = _summaryRow[name];
            }
            return ret;
        }

        public void ExcelSetup()
        {
            excelManager.SetupHeaderRowCount(3);

            Dictionary<long, ExcelRowStyle> rowsStyle = new Dictionary<long, ExcelRowStyle>();
            rowsStyle.Add(0, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            rowsStyle.Add(1, new ExcelRowStyle() { IsVerticalAlignmentCenter = true });
            rowsStyle.Add(2, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            excelManager.SetupRowStyle(rowsStyle);
        }

        public ExcelManagerReport GetExcelManager()
        {
            return excelManager;
        }

        public string GetReportCaptionGeneric()
        {
            string reportName = GetReportName();
            string reportDesc = GetSearchCriteriaDescription();
            if (string.IsNullOrWhiteSpace(reportDesc))
            {
                return reportName;
            }
            else
            {
                return string.Format("{0} ({1})", GetReportName(), GetSearchCriteriaDescription());
            }
        }

        public string GetReportNameGeneric()
        {
            return PresentationContext.WindowTitle;
        }

        public string GetReportCaption()
        {
            return GetReportPresenter().GetReportCaption();
        }

        public string GetReportTitle()
        {
            helper._region = GetReportPresenter().GetReportTitle();
            return GetReportPresenter().GetReportTitle();
        }  

        public string GetReportName()
        {
            return GetReportPresenter().GetReportName();
        }

        public IDataService GetDataService()
        {
            if (null == _dataService)
            {
                _dataService = base.GetServiceProxy<IDataService>();
            }
            return _dataService;
        }

        #endregion

        #region GetControlSearchCriteria получить контрол для поиска

        public UserControl GetControlSearchCriteria()
        {
            if (null == ctrlSearchCriteria)
            {
                ctrlSearchCriteria = GetReportPresenter().GetControlSearchCriteria();
                if (null == ctrlSearchCriteria)
                {
                    ctrlSearchCriteria = new UserControl();
                }
            }
            return ctrlSearchCriteria;
        }

        public Dictionary<string, GridColumnHeaderChanage> GetChangeGridColumn()
        {
            QueryConditions conditions = ((IControlSearchCriteria)GetControlSearchCriteria()).GetQueryConditions();
            return GetReportPresenter().GetChangeColumns(conditions);
        }

        public bool IsClearResultDataAfterChangeCriteria()
        {
            return GetReportPresenter().IsClearResultDataAfterChangeCriteria();
        }

        public object GetFilterFromCondition(QueryConditions criteria, string columnName)
        {
            return helper.GetFilterFromCondition(criteria, columnName);
        }

        private string GetSearchCriteriaDescription()
        {
            IControlSearchCriteria ctrlSearch = (IControlSearchCriteria)GetControlSearchCriteria();
            return ctrlSearch.GetSearchCriteriaDescription();
        }

        #endregion

        #region GetGridSetup

        public GridSetup GetGridSetup()
        {
            GridSetup setup = GetReportPresenter().GetGridSetup();
            if (null == setup)
            {
                setup = CommonSetup(string.Format("{0}_ReportBase", GetType()));
            }
            return setup;
        }

        public bool IsGridAggreagateVisible()
        {
            return GetReportPresenter().IsGridAggregateVisible();
        }
        
        #endregion

        #region GetRows получить строки для Грида

        public object GetRows(QueryConditions conditions, out long totalRowsNumber)
        {
            if (IsClearResultDataAfterChangeCriteria() && !IsFixCriteria)
            {
                totalRowsNumber = 0;
                return GetReportPresenter().GetEmptyObjects(); 
            }

            if (_requestStateAsync.State == StateAsync.None ||
                _requestStateAsync.State == StateAsync.Start)
            {
                GetRowsFromBD(conditions, false);
            }
            else if (_requestStateAsync.State == StateAsync.Complete)
            {
                if (_requestStateAsync.QueryConditionsSend.PaginationDetails.RowsToSkip != conditions.PaginationDetails.RowsToSkip ||
                    _requestStateAsync.QueryConditionsSend.PaginationDetails.RowsToTake != conditions.PaginationDetails.RowsToTake)
                {
                    GetRowsFromBD(conditions, true);
                }
            }
            totalRowsNumber = _requestStateAsync.TotalRowsNumber;
            return _requestStateAsync.ResultData;
        }

        private void SetRequstStateAsyncEmpty()
        {
            _requestStateAsync.ResultData = GetReportPresenter().GetEmptyObjects();
            _requestStateAsync.TotalRowsNumber = 0;
        }

        public void GetRowsFromBD(QueryConditions conditionsGrid, bool isPageChanged)
        {
            IsFixCriteria = true;
            _requestStateAsync.QueryConditionsSend = (QueryConditions)conditionsGrid.Clone();
            _requestStateAsync.State = StateAsync.Start;
            SetRequstStateAsyncEmpty();

            _reportCreateWorker = new AsyncWorker<OperationResult<int>>();
            _reportCreateWorker.DoWork += (sender, args) =>
            {
                _requestStateAsync.State = StateAsync.Send;
                _summaryRow = null;

                QueryConditions conditions = ((IControlSearchCriteria)ctrlSearchCriteria).GetQueryConditions();
                IEnumerable<ColumnSort> listColumnSort =
                    ((IControlSearchCriteria)ctrlSearchCriteria).GetQuerySorting(_requestStateAsync.QueryConditionsSend.Sorting);
                conditions.Sorting = listColumnSort != null ? listColumnSort.ToList() : null;
                conditions.PaginationDetails.RowsToSkip = _requestStateAsync.QueryConditionsSend.PaginationDetails.RowsToSkip;
				conditions.PaginationDetails.RowsToTake = _requestStateAsync.QueryConditionsSend.PaginationDetails.RowsToTake;
                _requestStateAsync.QueryConditionsSend = conditions;

                Dictionary<string, GridColumnHeaderChanage> changeColumns = GetReportPresenter().GetChangeColumns(conditions);
                View.ExecuteInUiThread(c =>
                {
                    View.GetGridMain().ChangeColumnsCaption(changeColumns);
                });

                long requestId = -1;
                if (!isPageChanged)
                {
                    if (!RunRequestInSOV(conditions, out requestId))
                    {
                        //--- SaveTask
                        long taskId = -1;
                        if (ExecuteServiceCall<OperationResult<long>>(
                            () => { return base.GetServiceProxy<IReportService>().SaveTask(conditions, requestId, GetReportNum()); },
                            (r1) => { taskId = r1.Result; },
                            (err) =>
                            {
                                View.ExecuteInUiThread(c =>
                                {
                                    View.ShowError(string.Format("Ошибка построения отчета('{0}')", err.Message));
                                });
                            }
                        ))
                        {
                            TaskStatus taskStatus = TaskStatus.None;
                            while (taskStatus != TaskStatus.Complete)
                            {
                                if (ExecuteServiceCall<OperationResult<TaskStatus>>(
                                    () => base.GetServiceProxy<IReportService>().GetTaskStatus(taskId),
                                    (r) => { taskStatus = r.Result; },
                                    (err) =>
                                    {
                                        View.ExecuteInUiThread(c =>
                                        {
                                            View.ShowError(string.Format("Ошибка построения отчета('{0}')", err.Message));
                                        });
                                    }
                                    ))
                                {
                                    Thread.Sleep(2000);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        _requestStateAsync.TaskId = taskId;
                        RecieveDataFromBD(conditions, taskId);
                    }
                }
                else
                {
                    RecieveDataFromBD(conditions, _requestStateAsync.TaskId);
                }
            };

            _reportCreateWorker.Complete += (sender, args) =>
            {
                _requestStateAsync.State = StateAsync.Complete;
                View.GetGridMain().PanelLoadingVisible = false;
                View.GetGridMain().UpdateData();
            };

            _reportCreateWorker.Failed += (sender, args) =>
            {
                SetRequstStateAsyncEmpty();
                _requestStateAsync.State = StateAsync.Complete;
                View.GetGridMain().PanelLoadingVisible = false;
            };

            View.GetGridMain().PanelLoadingVisible = true;
            _reportCreateWorker.Start();
        }

        private bool RunRequestInSOV(QueryConditions criteria, out long requestId)
        {
            bool isSovError = false;
            requestId = -1;
            if (ReportKey == ReportNames.CheckLogicControl)
            {
                SovResult sovResult = GetRowsFromSov(criteria);
                requestId = sovResult.RequestId;
                isSovError = sovResult.IsError || sovResult.IsWarning;
                if (isSovError)
                {
                    View.ExecuteInUiThread(c =>
                    {
                        if (sovResult.IsError)
                        {
                            View.ShowError(sovResult.ErrorMessage);
                        }
                        else if (sovResult.IsWarning)
                        {
                            View.ShowNotification(sovResult.WarningMessage);
                        }
                    });
                }
            }
            return isSovError;
        }

        private SovResult GetRowsFromSov(QueryConditions conditions)
        {
            SovResult sovResult = new SovResult();
            sovResult.RequestId = -1;
            sovResult.IsError = false;
            IReportService reposrtService = base.GetServiceProxy<IReportService>();
            var rez = reposrtService.StartBuildReport(conditions, GetReportNum());
            if (rez.Status == ResultStatus.Success)
            {
                sovResult.RequestId = rez.Result;
                bool isInProgress = true;
                while (isInProgress)
                {
                    OperationResult<DataRequestStatusWitnInitialize> internalResult = reposrtService.CheckBuildReport(sovResult.RequestId);
                    if (internalResult.Status == ResultStatus.Success)
                    {
                        if (internalResult.Result.Type != RequestStatusType.InProcess)
                        {
                            isInProgress = false;
                            switch (internalResult.Result.Type)
                            {
                                case RequestStatusType.Completed:
                                    break;
                                case RequestStatusType.Error:
                                    sovResult.IsError = true;
                                    sovResult.ErrorMessage = string.Format("Ошибка построения отчета ('{0}')", "Ошибка сервиса взаимодействия");
                                    break;
                                case RequestStatusType.NotRegistered:
                                    sovResult.IsError = true;
                                    sovResult.ErrorMessage = string.Format("Ошибка построения отчета ('{0}')", "Ошибка регистрации запроса");
                                    break;
                                case RequestStatusType.NotSuccessInNightMode:
                                    sovResult.IsWarning = true;
                                    sovResult.WarningMessage = ResourceManagerNDS2.SovNotSuccessInNightModeMessage;
                                    break;
                                default:
                                    sovResult.IsError = true;
                                    sovResult.ErrorMessage = string.Format("Ошибка построения отчета ('{0}')", "Ошибочный ответ от сервера");
                                    break;
                            }
                        }
                    }
                    else
                    {
                        sovResult.IsError = true;
                        sovResult.ErrorMessage = string.Format("Ошибка построения отчета ('{0}')", internalResult.Message);
                        isInProgress = false;
                    }
                    if (isInProgress)
                    {
                        Thread.Sleep(3000);
                    }
                }
            }
            else
            {
                sovResult.IsError = true;
                sovResult.ErrorMessage = string.Format("Ошибка построения отчета ('{0}')", rez.Message);
            }
            return sovResult;
        }

        private void RecieveDataFromBD(QueryConditions conditions, long taskId)
        {
            long totalRowsNumber = 0;
            object rows = GetReportPresenter().GetRows(conditions, taskId, out totalRowsNumber);
            if (null == rows)
            {
                rows = GetReportPresenter().GetEmptyObjects();
            }
            _requestStateAsync.ResultData = rows;
            _requestStateAsync.TotalRowsNumber = totalRowsNumber;
            _summaryRow = GetSummaryReport(ReportKey);
        }

        private int GetReportNum()
        {
            int ret = 0;
            if (reportNums.ContainsKey(ReportKey))
            {
                ret = reportNums[ReportKey];
            }
            return ret;
        }

        private Dictionary<string, object> GetSummaryReport(string ReportKey)
        {
            QueryConditions conditions = ((IControlSearchCriteria)ctrlSearchCriteria).GetQueryConditions();
            Dictionary<string, object> summaryRow = GetReportPresenter().GetSummaryRow(conditions);
            if (null == summaryRow)
            {
                summaryRow = new Dictionary<string, object>();
            }
            return summaryRow;
        }

        #endregion

        #region GetRowsExcel получить строки для Экселя

        private uint pageSizeExcel = 10000;
        private uint pageFirst = 1;

        private List<Dictionary<string, object>> GetRowsExcel(uint page, out long rowsTotalMatches)
        {
            QueryConditions conditions = ((IControlSearchCriteria)ctrlSearchCriteria).GetQueryConditions();
			conditions.PaginationDetails.RowsToSkip = ((uint)page - 1) * (uint)page;
            List<Dictionary<string, object>> rows = null;
            rowsTotalMatches = 0;

            helper.GenerateColumnsFormat(GetGridSetup().Columns.ToReadOnly());

            long requestId = -1;
            if (!RunRequestInSOV(conditions, out requestId))
            {
                long taskId = -1;
                if (ExecuteServiceCall<OperationResult<long>>(
                    () => { return base.GetServiceProxy<IReportService>().SaveTask(conditions, requestId, GetReportNum()); },
                    (r1) => { taskId = r1.Result; },
                    (err) =>
                    {
                        View.ExecuteInUiThread(c =>
                        {
                            View.ShowError(string.Format("Ошибка построения отчета('{0}')", err.Message));
                        });
                    }
                ))
                {
                    TaskStatus taskStatus = TaskStatus.None;
                    while (taskStatus != TaskStatus.Complete)
                    {
                        if (ExecuteServiceCall<OperationResult<TaskStatus>>(
                            () => base.GetServiceProxy<IReportService>().GetTaskStatus(taskId),
                            (r) => { taskStatus = r.Result; },
                            (err) =>
                            {
                                View.ExecuteInUiThread(c =>
                                {
                                    View.ShowError(string.Format("Ошибка построения отчета('{0}')", err.Message));
                                });
                            }
                            ))
                        {
                            Thread.Sleep(2000);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                rows = GetReportPresenter().GetRowsExcel(conditions, taskId, out rowsTotalMatches);
            }
            else
            {
                GetReportPresenter().GetEmptyObjects();
            }

            if (null == rows)
            {
                rows = new List<Dictionary<string, object>>();
            }
            return rows;
        }

        private void AddColumnsWidth(ColumnDefinition columnDef, List<double> columnsWidth)
        {
            if (columnsWidth != null)
            {
                if (columnDef != null && columnDef.WidthColumnExcel != null)
                {
                    columnsWidth.Add((double)columnDef.WidthColumnExcel);
                }
                else
                {
                    columnsWidth.Add(18);
                }
            }
        }

        private DataTable GetPageDataExcel(uint page, out long rowsTotalMatches)
        {
            rowsTotalMatches = 0;
            List<ColumnBase> columns = GetColumnsWithModification();
            DataTable dtRows = CreateDataRowsExcel(columns);
            List<Dictionary<string, object>> rows = GetRowsExcel(page, out rowsTotalMatches);
            if (page == pageFirst)
            {
                helper.AddHeaderForExcel(dtRows, columns.ToReadOnly());
            }
            ColumnDefinition columnDef;
            ColumnGroupDefinition columnGroupDef;
            foreach (Dictionary<string, object> dicts in rows)
            {
                List<object> objs = new List<object>();
                foreach (ColumnBase itemColumn in columns)
                {
                    columnDef = itemColumn as ColumnDefinition;
                    if (columnDef != null)
                    {
                        helper.AddRowItem(columnDef, objs, dicts);
                    }
                    else
                    {
                        columnGroupDef = itemColumn as ColumnGroupDefinition;
                        if (columnGroupDef != null)
                        {
                            foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                            {
                                helper.AddRowItem(itemColumnDef, objs, dicts);
                            }
                        }
                    }
                }
                dtRows.Rows.Add(objs.ToArray());
            }
            return dtRows;
        }

        private uint GetPageCountExcel()
        {
            //return uint.MaxValue;
            return 10000;
        }

        private DataTable CreateDataRowsExcel(IEnumerable<ColumnBase> columns, List<double> columnsWidth = null)
        {
            DataTable dtRows = new System.Data.DataTable("dtRows");
            ColumnDefinition columnDef;
            ColumnGroupDefinition columnGroupDef; 
            foreach (ColumnBase elem in columns)
            {
                columnDef = elem as ColumnDefinition;
                if (columnDef != null)
                {
                    DataColumn dataColumn = dtRows.Columns.Add(columnDef.Key, typeof(object));
                    dataColumn.ReadOnly = true;
                    dataColumn.Caption = columnDef.Caption;
                    AddColumnsWidth(columnDef, columnsWidth);
                }
                else
                {
                    columnGroupDef = elem as ColumnGroupDefinition;
                    if (columnGroupDef != null)
                    {
                        foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                        {
                            DataColumn dataColumn = dtRows.Columns.Add(itemColumnDef.Key, typeof(object));
                            dataColumn.ReadOnly = true;
                            dataColumn.Caption = itemColumnDef.Caption;
                            AddColumnsWidth(itemColumnDef, columnsWidth);
                        }
                    }
                }
            }
            return dtRows;
        }

        #endregion

        #region GenerateExcel генерация эксель-файла

        private void GenerateExcelSetup()
        {
            _reportToExcelWorker = new AsyncWorker<OperationResult<bool>>();

            _reportToExcelWorker.DoWork += (sender, args) =>
            {
                FileHelper.FileDeleteIfExists(ReportExcelFilePathTemp);

                string sheetName = "Отчет";
                List<double> columnsWidth = new List<double>();
                var columns = GetColumnsWithModification().ToReadOnly();
                DataTable dtRows = CreateDataRowsExcel(columns, columnsWidth);
                excelManager.CreateExcel(ReportExcelFilePathTemp, sheetName, columnsWidth);
                helper.AddHeaderForExcel(dtRows, columns);

                GetReportPresenter().ExcelSetup();
                excelManager.MergeMergeCells(ReportExcelFilePathTemp, sheetName, helper.GetExcelMergeCells(columns));

                excelManager.FillWorkbook(ReportExcelFilePathTemp, GetPageCountExcel, GetPageDataExcel, 
                                          predicIsCanceled: dataRow => !Synhro.mResetEvent.IsSet);
                if (Synhro.mResetEvent.IsSet)
                {
                    File.Copy(ReportExcelFilePathTemp, ReportExcelFilePath, true);
                    args.Result = new OperationResult<bool>() { Status = ResultStatus.Success };
                }
                else
                {
                    FileHelper.FileDeleteIfExists(ReportExcelFilePathTemp);
                    args.Result = new OperationResult<bool>() { Status = ResultStatus.Error };
                }
            };

            _reportToExcelWorker.Complete += (sender, args) =>
            {
                Synhro.mResetEvent.Reset();
                View.HideWaitGenerateWindow();
                if (args.Result.Status == ResultStatus.Success)
                {
                    _timerOpenReport.Enabled = true;
                }
            };

            _reportToExcelWorker.Failed += (sender, args) =>
            {
                Synhro.mResetEvent.Reset();
                View.HideWaitGenerateWindow();
                _timerOpenReport.Enabled = false;
            };
        }

        private List<ColumnBase> GetColumnsWithModification()
        {
            QueryConditions conditions = ((IControlSearchCriteria)ctrlSearchCriteria).GetQueryConditions();
            List<ColumnBase> columns = GetGridSetup().Columns;
            helper.ChangeColumnsCaption(columns, GetReportPresenter().GetChangeColumns(conditions));
            columns = helper.GetVisibleColumns(columns);
            return columns;
        }

        private void timerOpenReport_Tick(object sender, EventArgs e)
        {
            _timerOpenReport.Enabled = false;
            View.OpenExcelFile(ReportExcelFilePath);
        }

        public void GenerateExcelStart()
        {
            excelManager.ExcelGenerateEvent += View.GetFrmWaitGenerate().SetCountGenerateRows;
            View.ShowWaitGenerateWindow(() => 
            {
                Synhro.mResetEvent.Set();
                _reportToExcelWorker.Start(); 
            });
            Synhro.mResetEvent.Reset();
        }

        public void OpenReportExcelFile(string reportExcelFilePath)
        {
            helper.OpenReportExcelFile(reportExcelFilePath);
        }

        public string GenerateReportFileNameTemp()
        {
            string basePath = Path.GetTempPath();
            return string.Format("{0}\\temp.xlsx", basePath);
        }

        public string GenerateReportFileName()
        {
            string reportCaption = GetReportCaption();
            DateTime dtNow = DateTime.Now;
            return string.Format("{0} {1:0000}-{2:00}-{3:00}-{4:00}-{5:00}-{6:00}.xlsx", reportCaption, dtNow.Year, dtNow.Month, dtNow.Day, dtNow.Hour, dtNow.Minute, dtNow.Second);
        }

        public IEnumerable<AccessRight> CsudAccessRights { get; private set; }

        #endregion

    }
}
