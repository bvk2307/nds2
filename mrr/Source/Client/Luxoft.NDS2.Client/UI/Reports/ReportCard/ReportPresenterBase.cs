﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard
{
    public abstract class ReportPresenterBase
    {
        #region Переменные

        protected readonly ReportGeneralHelper _helper;
        protected readonly IReportService _reportService;
        protected readonly IBasePresenter _basePresenter;
        protected AccessContext accessContext;
        protected string sonoCode;
        private List<DictionaryNalogPeriods> _dictNalogPeriods;
        private GridSetup _gridSetup = null;

        #endregion

        public ReportPresenterBase(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
        {
            _helper = helper;
            _reportService = reportService;
            _basePresenter = basePresenter;
        }

        #region Основные методы

        public virtual GridSetup GetGridSetup()
        {
            if (null == _gridSetup)
            {
                _gridSetup = CreateGridSetup();
            }
            return _gridSetup;
        }

        protected virtual GridSetup CreateGridSetup()
        {
            return _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportGeneric", GetType()));
        }

        public virtual void ExcelSetup()
        {
            _basePresenter.ExcelSetup();
        }

        public virtual Dictionary<string, object> GetSummaryRow(QueryConditions conditions)
        {
            return new Dictionary<string, object>();
        }

        public virtual Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            return new Dictionary<string, GridColumnHeaderChanage>();
        }

        public virtual bool IsClearResultDataAfterChangeCriteria()
        {
            return false;
        }

        public virtual bool IsGridAggregateVisible()
        {
            return true;
        }

        public virtual string GetReportCaption()
        {
            return _basePresenter.GetReportCaptionGeneric();
        }

        public virtual string GetReportTitle()
        {
            return _basePresenter.GetReportTitle();
        }

        public virtual string GetReportName()
        {
            return _basePresenter.GetReportNameGeneric();
        }

        protected AccessContext LoadAccessContext(IEnumerable<string> operations)
        {
            try
            {
                var result = _basePresenter.SecurityHandling.GetAccessContext(operations.ToArray());
                return result.Status == ResultStatus.Success ? result.Result : null;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region GetDefaults

        protected UserControl GetControlSearchCriteriaDefault()
        {
            var ctrl = new InspectorMonitoringWorkSearch();

            ctrl.SetNalogPeriods(GetNalogPeriod());
            ctrl.SetNalogOrgans(GetNalogOrgans());
            ctrl.SetRegions(GetRegions());
            ctrl.SetFederalDistricts(GetFederalDistricts());
            return ctrl;
        }

        protected QueryConditions GetGridDefaultQueryConditions()
        {
            return new QueryConditions();
        }

        #endregion

        #region Получение справочников

        protected List<DictionaryNalogPeriods> GetDictionaryNalogPeriods()
        {
            if (null == _dictNalogPeriods)
            {
                _dictNalogPeriods = GetDictionaryNalogPeriodsDB();
            }
            return _dictNalogPeriods;
        }

        private List<DictionaryNalogPeriods> GetDictionaryNalogPeriodsDB()
        {
            List<DictionaryNalogPeriods> result = null;
            if (_basePresenter.ExecuteServiceCall(() => _basePresenter.GetDataService().GetNalogPeriods(),
                res => { result = res.Result; }))
            {
                return result;
            }
            return new List<DictionaryNalogPeriods>();
        }

        public List<DictionaryNalogOrgan> GetNalogOrgans()
        {
            List<DictionaryNalogOrgan> result = null;
            if (_basePresenter.ExecuteServiceCall(_basePresenter.GetDataService().GetNalogOrgans,
                res => { result = res.Result; }))
            {
                return result;
            }
            return new List<DictionaryNalogOrgan>();
        }

        protected List<RegionDetails> GetRegions(bool useAccessContext = false)
        {
            List<RegionDetails> result = null;
            if (_basePresenter.ExecuteServiceCall(_basePresenter.GetDataService().GetRegions,
                res => { result = res.Result; }))
            {
                if (useAccessContext)
                {
                    if (accessContext == null)
                        return new List<RegionDetails>();

                    if (accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsCountry)
                        || accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions))
                        return result;

                    if (accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsInspections)
                        || accessContext.Operations.Any(n => n.Name == MrrOperations.Macroreports.ReportBackgroundMetricsRegions))
                        return
                            result.Where(
                                n =>
                                    accessContext.Operations[0].AvailableInspections.Any(
                                        x => x.Substring(0, 2) == n.Code)).ToList();

                }
                else
                {
                    return result;
                }
            }
            return new List<RegionDetails>();
        }

        protected List<FederalDistrict> GetFederalDistricts()
        {
            List<FederalDistrict> result = null;
            if (_basePresenter.ExecuteServiceCall(_basePresenter.GetDataService().GetFederalDistricts,
                res => { result = res.Result; }))
            {
                return result;
            }
            return new List<FederalDistrict>();
        }

        public List<NalogPeriod> GetNalogPeriod(IEnumerable<int> years)
        {
            List<DictionaryNalogPeriods> nalogPeriods = GetDictionaryNalogPeriods();
            List<NalogPeriod> nalogPeriodAndYears = new List<NalogPeriod>();
            foreach (int itemYear in years.OrderBy(p => p))
            {
                nalogPeriodAndYears.AddRange(GenerateNalogPeriod(nalogPeriods, itemYear));
            }
            return nalogPeriodAndYears;
        }

        protected List<NalogPeriod> GetNalogPeriod()
        {
            var results = new List<NalogPeriod>();

            var periods = GetNalogPeriodLastYears(QuarterCalculator.LIMIT_DURATION_NALOGPERIOD_IN_YEAR + 2);

            var dtOfCurrentQuarter = QuarterCalculator
                .CalculateQuarterOffset(DateTime.Now);

            for (int i = 0; i < QuarterCalculator.COUNT_QUARTER_IN_YEAR * QuarterCalculator.LIMIT_DURATION_NALOGPERIOD_IN_YEAR; i++)
            {
                var period = periods
                                .SingleOrDefault(p =>
                                                    dtOfCurrentQuarter >= p.DateBegin
                                                    && dtOfCurrentQuarter <= p.DateEnd
                                                    && DateTime.Now > p.DateEnd);
                if (period != null)
                    results.Add(period);

                dtOfCurrentQuarter = QuarterCalculator
                    .CalculatePreviousQuarter(dtOfCurrentQuarter);
            }

            return results.OrderBy(p => p.DateBegin).ToList();
        }

        protected List<NalogPeriod> GetNalogPeriodLastYears(int countLastYear)
        {
            int beginYear = DateTime.Now.Year - countLastYear + 1;
            List<int> years = new List<int>();
            for (int i = -1; ++i < countLastYear;)
            {
                years.Add(beginYear + i);
            }
            return GetNalogPeriod(years);
        }

        private List<NalogPeriod> GenerateNalogPeriod(List<DictionaryNalogPeriods> nalogPeriods, int year)
        {
            var nalogPeriodAndYears = new List<NalogPeriod>();
            var name = string.Empty;
            NalogPeriod np;
            foreach (DictionaryNalogPeriods itemNP in nalogPeriods)
            {
                np = new NalogPeriod();
                np.CODE = itemNP.CODE;
                np.Year = year;

                if (itemNP.CODE == "21")
                {
                    np.NAME = string.Format("1 кв. {0} г.", year);
                    np.DateBegin = new DateTime(year, 01, 01, 0, 0, 0);
                    np.DateEnd = new DateTime(year, 03, 31, 23, 59, 59);
                }
                else if (itemNP.CODE == "22")
                {
                    np.NAME = string.Format("2 кв. {0} г.", year);
                    np.DateBegin = new DateTime(year, 04, 01, 0, 0, 0);
                    np.DateEnd = new DateTime(year, 06, 30, 23, 59, 59);
                }
                else if (itemNP.CODE == "23")
                {
                    np.NAME = string.Format("3 кв. {0} г.", year);
                    np.DateBegin = new DateTime(year, 07, 01, 0, 0, 0);
                    np.DateEnd = new DateTime(year, 09, 30, 23, 59, 59);
                }
                else if (itemNP.CODE == "24")
                {
                    np.NAME = string.Format("4 кв. {0} г.", year);
                    np.DateBegin = new DateTime(year, 10, 01, 0, 0, 0);
                    np.DateEnd = new DateTime(year, 12, 31, 23, 59, 59);
                }
                nalogPeriodAndYears.Add(np);
            }
            return nalogPeriodAndYears;
        }

        protected static List<AggregateByNalogOrgan> GetAggregateByNalogOrgan()
        {
            var rets = new List<AggregateByNalogOrgan>
            {
                new AggregateByNalogOrgan() {Type = AggregateByNalogOrganType.Country, Name = "По стране"},
                new AggregateByNalogOrgan() {Type = AggregateByNalogOrganType.FederalDistrict, Name = "По федеральным округам"},
                new AggregateByNalogOrgan() {Type = AggregateByNalogOrganType.Region, Name = "По регионам"},
                new AggregateByNalogOrgan() {Type = AggregateByNalogOrganType.Inspection, Name = "По конкретным ИФНС"}
            };
            return rets;
        }

        #endregion
    }
}
