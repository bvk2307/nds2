﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic
{
    public enum AggregateByNalogOrganType
    {
        Country = 1,
        FederalDistrict = 2,
        Region = 3,
        Inspection = 4
    }

    public class AggregateByNalogOrgan
    {
        public AggregateByNalogOrganType Type { get; set; }
        public string Name { get; set; }
    }
}
