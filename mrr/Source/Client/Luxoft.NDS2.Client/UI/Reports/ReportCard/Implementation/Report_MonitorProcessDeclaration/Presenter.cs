﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_MonitorProcessDeclaration
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private ColumnGroupDefinition groupUrepairedDiscrepancyFrom;
        private ColumnGroupDefinition groupUrepairedDiscrepancyTo;
        private ColumnGroupDefinition groupRepairedDiscrepancy;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            return GetControlSearchCriteriaDefault();
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportMonitorProcessDeclaration", GetType()));
            GridSetupHelper<MonitoringProcessingDeclaration> helper = new GridSetupHelper<MonitoringProcessingDeclaration>();

            ColumnGroupDefinition groupFederalDistrict = new ColumnGroupDefinition() { Caption = String.Empty };
            groupFederalDistrict.Columns.Add(helper.CreateColumnDefinition(o => o.FederalDistrict, d => { d.SortIndicator = false; } ));
            setup.Columns.Add(groupFederalDistrict);

            ColumnGroupDefinition groupRegion = new ColumnGroupDefinition() { Caption = String.Empty };
            groupRegion.Columns.Add(helper.CreateColumnDefinition(o => o.Region, d => { d.WidthColumnExcel = 23; d.SortIndicator = false;}));
            setup.Columns.Add(groupRegion);

            ColumnGroupDefinition groupInspection = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspection.Columns.Add(helper.CreateColumnDefinition(o => o.Inspection, d => { d.WidthColumnExcel = 40; d.SortIndicator = false;}));
            setup.Columns.Add(groupInspection);

            ColumnGroupDefinition groupCorrectionsCount = new ColumnGroupDefinition() { Caption = String.Empty };
            groupCorrectionsCount.Columns.Add(helper.CreateColumnDefinition(o => o.TakeUpdatesCount, d => { d.SortIndicator = false; }));
            setup.Columns.Add(groupCorrectionsCount);

            ColumnGroupDefinition groupTaxPayerInfo = new ColumnGroupDefinition() { Caption = "Сведения о налогоплательщике", };
            groupTaxPayerInfo.Columns.Add(helper.CreateColumnDefinition(o => o.TaxpayerINN, d => { d.SortIndicator = false; }));
            groupTaxPayerInfo.Columns.Add(helper.CreateColumnDefinition(o => o.TaxpayerKPP, d => { d.SortIndicator = false; }));
            groupTaxPayerInfo.Columns.Add(helper.CreateColumnDefinition(o => o.TaxpayerName, d => { d.SortIndicator = false; }));
            setup.Columns.Add(groupTaxPayerInfo);

            ColumnGroupDefinition groupInspector = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspector.Columns.Add(helper.CreateColumnDefinition(o => o.Inspector, d => { d.SortIndicator = false; }));
            setup.Columns.Add(groupInspector);

            string formatRuble = ColumnFormats.FormatRuble;
            IFormatProvider formatInfoRuble = ColumnFormats.FormatInfoRuble;
            string formatRubleGrid = ColumnFormats.FormatRubleGrid;
            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;

            groupUrepairedDiscrepancyFrom = new ColumnGroupDefinition() { Caption = "Не устраненные расхождения на [с]", };
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromGeneralSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromErrorLKCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromCurrencySum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromNDSSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromInexactComparisonSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyFrom.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyFromBreakSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            setup.Columns.Add(groupUrepairedDiscrepancyFrom);

            groupUrepairedDiscrepancyTo = new ColumnGroupDefinition() { Caption = "Не устраненные расхождения на [по]", };
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToGeneralSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToErrorLKCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToCurrencySum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit;  d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToNDSSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToInexactComparisonSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupUrepairedDiscrepancyTo.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyToBreakSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            setup.Columns.Add(groupUrepairedDiscrepancyTo);

            groupRepairedDiscrepancy = new ColumnGroupDefinition() { Caption = "Устраненные расхождения с [c] по [по]", };
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyGeneralSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyCurrencySum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyNDSSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyInexactComparisonSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyBreakSum, d => { d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false;}));
            setup.Columns.Add(groupRepairedDiscrepancy);

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            Dictionary<string, GridColumnHeaderChanage> columns = new Dictionary<string, GridColumnHeaderChanage>();
            string sDtBegin = String.Empty;
            string sDtEnd = String.Empty;
            _helper.GetDateBeginAndEnd(conditions, out sDtBegin, out sDtEnd);

            if (groupUrepairedDiscrepancyFrom != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Не устраненные расхождения на {0}", sDtBegin);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupUrepairedDiscrepancyFrom.Key, columnChange);
            }
            if (groupUrepairedDiscrepancyTo != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Не устраненные расхождения на {0}", sDtEnd);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupUrepairedDiscrepancyTo.Key, columnChange);
            }
            if (groupRepairedDiscrepancy != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Устраненные расхождения с {0} по {1}", sDtBegin, sDtEnd);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupRepairedDiscrepancy.Key, columnChange);
            }

            return columns;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<MonitoringProcessingDeclaration> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchMonitoringProcessingDeclaration(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<MonitoringProcessingDeclaration>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<MonitoringProcessingDeclaration> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchMonitoringProcessingDeclaration(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<MonitoringProcessingDeclaration>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public object GetEmptyObjects()
        {
            return new List<MonitoringProcessingDeclaration>();
        }
    }
}
