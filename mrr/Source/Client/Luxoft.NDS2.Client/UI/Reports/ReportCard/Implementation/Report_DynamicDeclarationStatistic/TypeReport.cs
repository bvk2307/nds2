﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic
{
    public enum TypeReportType
    {
        Absolute = 1,
        Changing = 2
    }

    public class TypeReport
    {
        public TypeReportType Type { get; set; }
        public string Name { get; set; }
    }
}
