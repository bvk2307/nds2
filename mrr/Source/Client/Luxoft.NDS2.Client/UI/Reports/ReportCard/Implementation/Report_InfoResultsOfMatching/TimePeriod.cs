﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_InfoResultsOfMatching
{
    public enum TimePeriodType
    {
        OneDay = 1,
        Period = 2
    }

    public class TimePeriod
    {
        public TimePeriodType Type { get; set; }
        public string Name { get; set; }
    }
}
