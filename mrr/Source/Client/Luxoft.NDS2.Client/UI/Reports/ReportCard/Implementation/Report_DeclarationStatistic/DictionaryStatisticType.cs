﻿
namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DeclarationStatistic
{
    public class DictionaryStatisticType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
