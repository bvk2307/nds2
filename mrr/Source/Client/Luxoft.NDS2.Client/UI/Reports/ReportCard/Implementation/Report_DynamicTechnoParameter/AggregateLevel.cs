﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicTechnoParameter
{
    public enum AggregateLevelType
    {
        Day = 1,
        Week = 2,
        Month = 3
    }

    public class AggregateLevel
    {
        public AggregateLevelType Type { get; set; }
        public string Name { get; set; }
    }
}
