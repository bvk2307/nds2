﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_InfoResultsOfMatching
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private InfoResultsOfMatchingSearch _controlSearchCriteria;
        private ColumnDefinition colAggregateName;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            if (null == _controlSearchCriteria)
            {
                _controlSearchCriteria = new InfoResultsOfMatchingSearch(_basePresenter.GetDataService());
                _controlSearchCriteria.SetAggregateByNalogOrgan(GetAggregateByNalogOrgan());
                _controlSearchCriteria.SetFederalDistricts(GetFederalDistricts());
                _controlSearchCriteria.SetNalogPeriods(GetNalogPeriodCheck());
                _controlSearchCriteria.SetTimePeriods(GetTimePeriods());
            }
            return _controlSearchCriteria;
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportInfoResultsOfMatching", GetType()));
            GridSetupHelper<InfoResultsOfMatching> helper = new GridSetupHelper<InfoResultsOfMatching>();

            bool sortInd = false;

            ColumnGroupDefinition g01 = new ColumnGroupDefinition() { Caption = String.Empty };
            colAggregateName = helper.CreateColumnDefinition(o => o.AggregateName, d =>
            {
                d.Width = 150; d.WidthColumnExcel = 50; d.Caption = "Регион построения"; d.ToolTip = "Регион построения";
                d.SortIndicator = sortInd; d.CellMultiLine = true;
            });
            g01.Columns.Add(colAggregateName);
            setup.Columns.Add(g01);


            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;
            string formatDecimal = ColumnFormats.FormatDecimal;
            string formatDecimalGrid = ColumnFormats.FormatDecimalGrid;

            ColumnGroupDefinition g02 = new ColumnGroupDefinition() { Caption = "Количество представленных деклараций" };
            g02.Columns.Add(helper.CreateColumnDefinition(o => o.SubmittingDeclarationPrimaryCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g02.Columns.Add(helper.CreateColumnDefinition(o => o.SubmittingDeclarationCorrectCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g02.Columns.Add(helper.CreateColumnDefinition(o => o.SubmittingDeclarationBrokeringCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g02);

            ColumnGroupDefinition g03 = new ColumnGroupDefinition() { Caption = String.Empty };
            g03.Columns.Add(helper.CreateColumnDefinition(o => o.SubmittingJournalCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g03);

            ColumnGroupDefinition g04 = new ColumnGroupDefinition() { Caption = "Количество операций в декларациях" };
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationTotal, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationR08, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationR09, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationR10, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationR11, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g04.Columns.Add(helper.CreateColumnDefinition(o => o.CountOperationInDeclarationR12, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g04);

            ColumnGroupDefinition g05 = new ColumnGroupDefinition() { Caption = String.Empty };
            g05.Columns.Add(helper.CreateColumnDefinition(o => o.CountExactDiscrepancy, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g05);

            ColumnGroupDefinition g06 = new ColumnGroupDefinition() { Caption = "Всего деклараций с расхождениями" };
            g06.Columns.Add(helper.CreateColumnDefinition(o => o.TotalDeclarationDiscrepancyPrimary, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.Width = 115; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g06.Columns.Add(helper.CreateColumnDefinition(o => o.TotalDeclarationDiscrepancyCorrection, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.Width = 115; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g06);

            ColumnGroupDefinition g07 = new ColumnGroupDefinition() { Caption = String.Empty };
            g07.Columns.Add(helper.CreateColumnDefinition(o => o.TotalDeclarationDiscrepancyControlRation, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g07);

            ColumnGroupDefinition g08 = new ColumnGroupDefinition() { Caption = "Выявлено расхождений" };
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentDiscrepancyTotalCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentDiscrepancyTotalAmount, d => { d.SortIndicator = sortInd; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentNotExactDiscrepancyOverflowCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentNotExactDiscrepancyOverflowAmount, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentNotExactDiscrepancyNotOverflowCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyOverflowCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyOverflowAmount, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyBreakAmount, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g08.Columns.Add(helper.CreateColumnDefinition(o => o.IdentExactDiscrepancyCurrencyAmount, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g08);

            ColumnGroupDefinition g09 = new ColumnGroupDefinition() { Caption = String.Empty };
            g09.Columns.Add(helper.CreateColumnDefinition(o => o.SpecificWeightDiscrepancyInTotalOperation, d => { d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g09);

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = "Расхождения, отправленные в ТНО" };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancySendToTNOCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.Width = 105; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancySendToTNOAmount, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.Width = 105; d.SortIndicator = sortInd; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(g10);

            setup.QueryCondition = GetGridDefaultQueryConditions();
            return setup;
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            Dictionary<string, GridColumnHeaderChanage> columns = new Dictionary<string, GridColumnHeaderChanage>();
            if (colAggregateName != null)
            {
                object oAggregateByNalogOrgan = _helper.GetFilterFromCondition(conditions, TaskParamCriteria.IRM_AggregateByNalogOrgan);
                if (oAggregateByNalogOrgan != null)
                {
                    int aggregateByNalogOrgan = (int)oAggregateByNalogOrgan;
                    string nameGroup = String.Empty;
                    Dictionary<int, string> nameGroups = new Dictionary<int, string>();
                    nameGroups.Add((int)AggregateByNalogOrganType.Country, "Страна");
                    nameGroups.Add((int)AggregateByNalogOrganType.FederalDistrict, "Федеральный округ");
                    nameGroups.Add((int)AggregateByNalogOrganType.Region, "Регион");
                    nameGroups.Add((int)AggregateByNalogOrganType.Inspection, "Испекция");

                    if (nameGroups.ContainsKey(aggregateByNalogOrgan))
                    {
                        nameGroup = nameGroups[aggregateByNalogOrgan];
                    }
                    GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                    columnChange.Caption = nameGroup;
                    columnChange.ToolTipText = columnChange.Caption;
                    columns.Add(colAggregateName.Key, columnChange);
                }
            }
            return columns;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<InfoResultsOfMatching> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchInfoResultsOfMatching(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<InfoResultsOfMatching>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<InfoResultsOfMatching> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchInfoResultsOfMatching(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<InfoResultsOfMatching>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public object GetEmptyObjects()
        {
            return new List<InfoResultsOfMatching>();
        }

        public List<NalogPeriodCheck> GetNalogPeriodCheck()
        {
            List<NalogPeriodCheck> rets = new List<NalogPeriodCheck>();
            foreach (NalogPeriod item in GetNalogPeriodLastYears(3))
            {
                rets.Add(CreateNalogPeriofCheck(item));
            }
            return rets;
        }

        private NalogPeriodCheck CreateNalogPeriofCheck(NalogPeriod np)
        {
            NalogPeriodCheck obj = new NalogPeriodCheck();

            obj.CODE = np.CODE;
            obj.DateBegin = np.DateBegin;
            obj.DateEnd = np.DateEnd;
            obj.NAME = np.NAME;
            obj.Year = np.Year;
            obj.Check = false;

            return obj;
        }

        private List<TimePeriod> GetTimePeriods()
        {
            List<TimePeriod> rets = new List<TimePeriod>();
            rets.Add(new TimePeriod() { Type = TimePeriodType.OneDay, Name = "За дату" });
            rets.Add(new TimePeriod() { Type = TimePeriodType.Period, Name = "За период" });

            return rets;
        }
    }
}
