﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private DynamicDeclarationStatisticSearch _controlSearchCriteria;
        private Dictionary<int, ColumnDefinition> columnsParameters = new Dictionary<int, ColumnDefinition>();
        private List<DynamicParamDeclStat> _dictDynParameters;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public System.Windows.Forms.UserControl GetControlSearchCriteria()
        {
            if (null == _controlSearchCriteria)
            {
                _controlSearchCriteria = new DynamicDeclarationStatisticSearch(_basePresenter.GetDataService());
                _controlSearchCriteria.SetAggregateByNalogOrgan(GetAggregateByNalogOrgan());
                _controlSearchCriteria.SetAggregateByTime(GetAggregateByTime());
                _controlSearchCriteria.SetTypeReports(GetTypeReports());
                _controlSearchCriteria.SetDictionaryNalogPeriods(GetDictionaryNalogPeriods());
                _controlSearchCriteria.SetFiscalYears(GetFiscalYears());
                _controlSearchCriteria.SetYears(GetYears());
                _controlSearchCriteria.SetFederalDistricts(GetFederalDistricts());
                _dictDynParameters = GetDynamicParameters();
                _controlSearchCriteria.SetDynamicParameters(_dictDynParameters);
            }
            return _controlSearchCriteria;
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportDynamicDeclarationStatistic", GetType()));
            GridSetupHelper<DynamicDeclarationStatistic> helper = new GridSetupHelper<DynamicDeclarationStatistic>();

            columnsParameters.Clear();
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.Period, d =>
            {
                d.Width = 150; d.WidthColumnExcel = 50; d.Caption = "Период"; d.ToolTip = "Период";
                d.SortIndicator = false; d.CellMultiLine = true;
            }));

            IFormatProvider formatInfoDigitOrDate = ColumnFormats.FormatInfoDigitOrDate;

            CreateColumn(helper, setup, o => o.P01, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P02, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P03, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P04, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P05, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P06, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P07, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P08, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P09, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P10, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P11, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P12, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P13, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P14, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public override void ExcelSetup()
        {
            ExcelManagerReport excelManager = _basePresenter.GetExcelManager();
            excelManager.SetupHeaderRowCount(3);

            Dictionary<long, ExcelRowStyle> rowsStyle = new Dictionary<long, ExcelRowStyle>();
            rowsStyle.Add(0, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            rowsStyle.Add(1, new ExcelRowStyle() { IsVerticalAlignmentCenter = true });
            rowsStyle.Add(2, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            excelManager.SetupRowStyle(rowsStyle);
        }

        private void CreateColumn<TProperty>(GridSetupHelper<DynamicDeclarationStatistic> helper, GridSetup setup,
            Expression<Func<DynamicDeclarationStatistic, TProperty>> propertyExpresion, Action<ColumnDefinition> initializer)
        {
            ColumnDefinition col = helper.CreateColumnDefinition(propertyExpresion, initializer);
            setup.Columns.Add(col);
            int dictIndex = columnsParameters.Count + 1;
            columnsParameters.Add(dictIndex, col);
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            Dictionary<string, GridColumnHeaderChanage> columns = new Dictionary<string, GridColumnHeaderChanage>();
            Dictionary<int, int> dynParams = GetDynamicParametersFromCondition(conditions, TaskParamCriteria.DDS_DynamicParameters);

            foreach (KeyValuePair<int, ColumnDefinition> itemColumn in columnsParameters)
            {
                KeyValuePair<int, int> itemSource = dynParams.Where(p => p.Key == itemColumn.Key).SingleOrDefault();
                if (itemSource.Key != 0 && itemSource.Value != 0)
                {
                    DynamicParamDeclStat dynParamFind = DictDynamicParameters.Where(p => p.Id == itemSource.Value).SingleOrDefault();
                    if (dynParamFind != null)
                    {
                        GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                        columnChange.Caption = dynParamFind.Name;
                        columnChange.ToolTipText = columnChange.Caption;
                        columnChange.Hidden = false;
                        columnChange.Visibility = ColumnVisibility.Visible;
                        columns.Add(itemColumn.Value.Key, columnChange);
                    }
                }
                else
                {
                    GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                    columnChange.Caption = String.Empty;
                    columnChange.ToolTipText = String.Empty;
                    columnChange.Hidden = true;
                    columnChange.Visibility = ColumnVisibility.Hidden;
                    columns.Add(itemColumn.Value.Key, columnChange);
                }
            }
            return columns;
        }

        private Dictionary<int, int> GetDynamicParametersFromCondition(QueryConditions criteria, string columnName)
        {
            Dictionary<int, int> retSort = new Dictionary<int, int>();
            List<int> rets = new List<int>();
            FilterQuery filterQuery = criteria.Filter.Where(p => p.ColumnName == columnName).FirstOrDefault();
            if (filterQuery != null)
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    rets.Add((int)columnFilter.Value);
                }
            }
            int i = 1;
            foreach (int item in rets.OrderBy(p => p))
            {
                retSort.Add(i, item);
                i++;
            }
            return retSort;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<DynamicDeclarationStatistic> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDynamicDeclarationStatistic(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    UpdateFieldsWithFormat(rows);
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<DynamicDeclarationStatistic>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<DynamicDeclarationStatistic> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDynamicDeclarationStatistic(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionaryWithNormalType<DynamicDeclarationStatistic>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        private void UpdateFieldsWithFormat(List<DynamicDeclarationStatistic> rows)
        {
            IFormatProvider formatInfoDigitOrDate = ColumnFormats.FormatInfoDigitOrDate;
            foreach (DynamicDeclarationStatistic item in rows)
            {
                item.P01 = string.Format(formatInfoDigitOrDate, "{0}", item.P01);
                item.P02 = string.Format(formatInfoDigitOrDate, "{0}", item.P02);
                item.P03 = string.Format(formatInfoDigitOrDate, "{0}", item.P03);
                item.P04 = string.Format(formatInfoDigitOrDate, "{0}", item.P04);
                item.P05 = string.Format(formatInfoDigitOrDate, "{0}", item.P05);
                item.P06 = string.Format(formatInfoDigitOrDate, "{0}", item.P06);
                item.P07 = string.Format(formatInfoDigitOrDate, "{0}", item.P07);
                item.P08 = string.Format(formatInfoDigitOrDate, "{0}", item.P08);
                item.P09 = string.Format(formatInfoDigitOrDate, "{0}", item.P09);
                item.P10 = string.Format(formatInfoDigitOrDate, "{0}", item.P10);
                item.P11 = string.Format(formatInfoDigitOrDate, "{0}", item.P11);
                item.P12 = string.Format(formatInfoDigitOrDate, "{0}", item.P12);
                item.P13 = string.Format(formatInfoDigitOrDate, "{0}", item.P13);
                item.P14 = string.Format(formatInfoDigitOrDate, "{0}", item.P14);
            }
        }

        private List<AggregateByTime> GetAggregateByTime()
        {
            List<AggregateByTime> rets = new List<AggregateByTime>();
            rets.Add(new AggregateByTime() { Type = AggregateByTimeType.Day, Name = "День" });
            rets.Add(new AggregateByTime() { Type = AggregateByTimeType.Week, Name = "Неделя" });
            rets.Add(new AggregateByTime() { Type = AggregateByTimeType.Month, Name = "Месяц" });
            return rets;
        }

        private List<TypeReport> GetTypeReports()
        {
            List<TypeReport> rets = new List<TypeReport>();
            rets.Add(new TypeReport() { Type = TypeReportType.Absolute, Name = "Абсолютные показатели" });
            rets.Add(new TypeReport() { Type = TypeReportType.Changing, Name = "Изменения" });
            return rets;
        }

        private List<int> GetYears()
        {
            return GetYearsBase(2015, DateTime.Now.Year + 10);
        }

        private List<int> GetFiscalYears()
        {
            return GetYearsBase(2014, DateTime.Now.Year + 10);
        }

        private List<int> GetYearsBase(int beginYear, int endYear)
        {
            List<int> rets = new List<int>();
            for (int i = beginYear - 1; ++i <= endYear; )
            {
                rets.Add(i);
            }
            return rets;
        }

        private List<DynamicParamDeclStat> DictDynamicParameters
        {
            get
            {
                if (null == _dictDynParameters)
                {
                    _dictDynParameters = GetDynamicParameters();
                }
                return _dictDynParameters;
            }
        }

        private List<DynamicParamDeclStat> GetDynamicParameters()
        {
            List<DynamicParamDeclStat> rets = new List<DynamicParamDeclStat>();

            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 1, Name = "Подано деклараций всего" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 2, Name = "Подано деклараций нулевых" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 3, Name = "Подано деклараций к возмещению" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 4, Name = "Подано деклараций к уплате" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 5, Name = "Не представленные декларации(но имеется ссылка в декларации другого налогопалательщика)" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 6, Name = "Подано уточненных деклараций всего" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 7, Name = "Налоговая база в декларациях к возмещению" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 8, Name = "Налоговая база в декларациях к уплате" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 9, Name = "Сумма исчисленного НДС с налоговой бызы в декларациях к возмещению" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 10, Name = "Сумма исчисленного НДС с налоговой бызы в декларациях к уплате" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 11, Name = "Сумма вычетов по НДС в декларациях к возмещению" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 12, Name = "Сумма вычетов по НДС в декларациях к уплате" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 13, Name = "Сумма НДС к возмещению" });
            rets.Add(new DynamicParamDeclStat() { Check = false, Id = 14, Name = "Сумма НДС к уплате" });

            return rets;
        }

        private List<DateTime> GetDatesTechnoReport()
        {
            List<DateTime> result = null;
            if (_basePresenter.ExecuteServiceCall(_reportService.GetDatesTechnoReport,
                res => { result = res.Result; }))
            {
                return result;
            }
            return new List<DateTime>();
        }

        public object GetEmptyObjects()
        {
            return new List<DynamicDeclarationStatistic>();
        }

        public override bool IsClearResultDataAfterChangeCriteria()
        {
            return true;
        }

        public override bool IsGridAggregateVisible()
        {
            return false;
        }

        public override string GetReportName()
        {
            return "Отчет по изменению фоновых показателей процесса подачи и обработки НД по НДС";
        }

        public override string GetReportTitle()
        {
            return _controlSearchCriteria.AddNalogPeriodToSearchCriteriaDescription();
        }

        public override string GetReportCaption()
        {
            string criteriaDescription = _controlSearchCriteria.GetSearchCriteriaDescription();
            string reportCaption = string.Format("{0} {1}", GetReportName(), criteriaDescription);
            return reportCaption;
        }
    }
}
