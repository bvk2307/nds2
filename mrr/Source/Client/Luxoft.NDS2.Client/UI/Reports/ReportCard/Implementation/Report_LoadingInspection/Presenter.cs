﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_LoadingInspection
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            return GetControlSearchCriteriaDefault();
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportInspectorMonitoringWork", GetType()));
            GridSetupHelper<LoadingInspection> helper = new GridSetupHelper<LoadingInspection>();

            ColumnGroupDefinition groupFederalDistrict = new ColumnGroupDefinition() { Caption = String.Empty };
            groupFederalDistrict.Columns.Add(helper.CreateColumnDefinition(o => o.FederalDistrict, d => { d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupFederalDistrict);

            ColumnGroupDefinition groupRegion = new ColumnGroupDefinition() { Caption = String.Empty };
            groupRegion.Columns.Add(helper.CreateColumnDefinition(o => o.Region, d => { d.WidthColumnExcel = 23; d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupRegion);

            ColumnGroupDefinition groupInspection = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspection.Columns.Add(helper.CreateColumnDefinition(o => o.Inspection, d => { d.WidthColumnExcel = 40; d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupInspection);

            ColumnGroupDefinition groupInspectorCount = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspectorCount.Columns.Add(helper.CreateColumnDefinition(o => o.InspectorCount, d => { d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupInspectorCount);

            string formatPercent = ColumnFormats.FormatPercent;
            string formatPercentGrid = ColumnFormats.FormatPercentGrid;
            string formatRuble = ColumnFormats.FormatRuble;
            IFormatProvider formatInfoPercent = ColumnFormats.FormatInfoPercent;
            IFormatProvider formatInfoRuble = ColumnFormats.FormatInfoRuble;
            string formatRubleGrid = ColumnFormats.FormatRubleGrid;
            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;

            ColumnGroupDefinition groupAllDiscrepancy = new ColumnGroupDefinition() { Caption = "Всего расхождений", };
            groupAllDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancyGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupAllDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.DiscrepancyGeneralSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupAllDiscrepancy);

            ColumnGroupDefinition groupSentToWorkDiscrepancy = new ColumnGroupDefinition() { Caption = "Расхождения, отправленные на отработку", };
            groupSentToWorkDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.SentToWorkDiscrepancyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupSentToWorkDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.SentToWorkDiscrepancyPercentage, d => { d.AllowSummary = true; d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupSentToWorkDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.SentToWorkDiscrepancySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupSentToWorkDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.SentToWorkDiscrepancyPercentageSum, d => { d.AllowSummary = true; d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupSentToWorkDiscrepancy);

            ColumnGroupDefinition groupRepairedDiscrepancy = new ColumnGroupDefinition() { Caption = "Устраненные расхождения", };
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyPercentage, d => { d.AllowSummary = true; d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupRepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.RepairedDiscrepancyPercentageSum, d => { d.AllowSummary = true; d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupRepairedDiscrepancy);

            ColumnGroupDefinition UnrepairedDiscrepancy = new ColumnGroupDefinition() { Caption = "Не устраненные расхождения (из отправленных)", };
            UnrepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.Width = 140; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            UnrepairedDiscrepancy.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.Width = 140; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(UnrepairedDiscrepancy);

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<LoadingInspection> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchLoadingInspection(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<LoadingInspection>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<LoadingInspection> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchLoadingInspection(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<LoadingInspection>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public object GetEmptyObjects()
        {
            return new List<LoadingInspection>();
        }
    }
}
