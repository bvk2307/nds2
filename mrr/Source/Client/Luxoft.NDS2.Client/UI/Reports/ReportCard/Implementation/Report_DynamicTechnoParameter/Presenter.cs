﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;


namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicTechnoParameter
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private DynamicTechnoParameterSearch _controlSearchCriteria;
        private Dictionary<int, ColumnDefinition> columnsParameters = new Dictionary<int, ColumnDefinition>();
        private List<DynamicParameter> _dictDynParameters;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public System.Windows.Forms.UserControl GetControlSearchCriteria()
        {
            if (null == _controlSearchCriteria)
            {
                _controlSearchCriteria = new DynamicTechnoParameterSearch();
                _controlSearchCriteria.SetAggregateLevels(GetAggregateLevels());
                _dictDynParameters = GetDynamicParameters();
                _controlSearchCriteria.SetDynamicParameters(_dictDynParameters);
                _controlSearchCriteria.SetDatesTechnoReport(GetDatesTechnoReport());
            }
            return _controlSearchCriteria;
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportDynamicTechnoParameter", GetType()));
            GridSetupHelper<DynamicTechnoParameter> helper = new GridSetupHelper<DynamicTechnoParameter>();

            columnsParameters.Clear();
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.Period, d =>
            {
                d.Width = 150; d.WidthColumnExcel = 50; d.Caption = "Дата (интервал дат)"; d.ToolTip = "Дата (интервал дат)"; 
                d.SortIndicator = false; d.CellMultiLine = true; 
            }));

            IFormatProvider formatInfoDigitOrDate = ColumnFormats.FormatInfoDigitOrDate;

            CreateColumn(helper, setup, o => o.P01, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P02, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P03, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P04, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P05, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P06, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P07, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P08, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P09, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P10, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P11, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P12, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P13, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P14, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P15, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P16, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P17, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P18, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P19, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P20, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P21, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P22, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P23, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P24, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P25, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P26, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P27, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P28, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P29, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });
            CreateColumn(helper, setup, o => o.P30, d => { d.FormatInfo = formatInfoDigitOrDate; d.FormatInfoExcel = formatInfoDigitOrDate; d.Visibility = ColumnVisibility.Hidden; d.SortIndicator = false; d.Align = ColumnAlign.Right; });

            setup.QueryCondition = GetGridDefaultQueryConditions();
            return setup;
        }

        private void CreateColumn<TProperty>(GridSetupHelper<DynamicTechnoParameter> helper, GridSetup setup,
            Expression<Func<DynamicTechnoParameter, TProperty>> propertyExpresion, Action<ColumnDefinition> initializer)
        {
            ColumnDefinition col = helper.CreateColumnDefinition(propertyExpresion, initializer);
            setup.Columns.Add(col);
            int dictIndex = columnsParameters.Count + 1;
            columnsParameters.Add(dictIndex, col);
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            Dictionary<string, GridColumnHeaderChanage> columns = new Dictionary<string, GridColumnHeaderChanage>();
            Dictionary<int, int> dynParams = GetDynamicParametersFromCondition(conditions, TaskParamCriteria.DynamicParameters);

            foreach (KeyValuePair<int, ColumnDefinition> itemColumn in columnsParameters)
            {
                KeyValuePair<int, int> itemSource = dynParams.Where(p => p.Key == itemColumn.Key).SingleOrDefault();
                if (itemSource.Key != 0 && itemSource.Value != 0)
                {
                    DynamicParameter dynParamFind = DictDynamicParameters.Where(p => p.Id == itemSource.Value).SingleOrDefault();
                    if (dynParamFind != null)
                    {
                        GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                        columnChange.Caption = dynParamFind.Name;
                        columnChange.ToolTipText = columnChange.Caption;
                        columnChange.Hidden = false;
                        columnChange.Visibility = ColumnVisibility.Visible;
                        columns.Add(itemColumn.Value.Key, columnChange);
                    }
                }
                else
                {
                    GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                    columnChange.Caption = String.Empty;
                    columnChange.ToolTipText = String.Empty;
                    columnChange.Hidden = true;
                    columnChange.Visibility = ColumnVisibility.Hidden;
                    columns.Add(itemColumn.Value.Key, columnChange);
                }
            }
            return columns;
        }

        public override void ExcelSetup()
        {
            ExcelManagerReport excelManager = _basePresenter.GetExcelManager();
            excelManager.SetupHeaderRowCount(2);

            Dictionary<long, ExcelRowStyle> rowsStyle = new Dictionary<long, ExcelRowStyle>();
            rowsStyle.Add(0, new ExcelRowStyle() { IsVerticalAlignmentCenter = true });
            rowsStyle.Add(1, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            excelManager.SetupRowStyle(rowsStyle);
        }

        private Dictionary<int, int> GetDynamicParametersFromCondition(QueryConditions criteria, string columnName)
        {
            Dictionary<int, int> retSort = new Dictionary<int, int>();
            List<int> rets = new List<int>();
            FilterQuery filterQuery = criteria.Filter.Where(p => p.ColumnName == columnName).FirstOrDefault();
            if (filterQuery != null)
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    rets.Add((int)columnFilter.Value);
                }
            }
            int i = 1;
            foreach (int item in rets.OrderBy(p => p))
            {
                retSort.Add(i, item);
                i++;
            }
            return retSort;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<DynamicTechnoParameter> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDynamicTechnoParameter(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    UpdateFieldsWithFormat(rows);
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<DynamicTechnoParameter>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<DynamicTechnoParameter> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDynamicTechnoParameter(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionaryWithNormalType<DynamicTechnoParameter>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        private void UpdateFieldsWithFormat(List<DynamicTechnoParameter> rows)
        {
            IFormatProvider formatInfoDigitOrDate = ColumnFormats.FormatInfoDigitOrDate;
            foreach (DynamicTechnoParameter item in rows)
            {
                item.P01 = string.Format(formatInfoDigitOrDate, "{0}", item.P01);
                item.P02 = string.Format(formatInfoDigitOrDate, "{0}", item.P02);
                item.P03 = string.Format(formatInfoDigitOrDate, "{0}", item.P03);
                item.P04 = string.Format(formatInfoDigitOrDate, "{0}", item.P04);
                item.P05 = string.Format(formatInfoDigitOrDate, "{0}", item.P05);
                item.P06 = string.Format(formatInfoDigitOrDate, "{0}", item.P06);
                item.P07 = string.Format(formatInfoDigitOrDate, "{0}", item.P07);
                item.P08 = string.Format(formatInfoDigitOrDate, "{0}", item.P08);
                item.P09 = string.Format(formatInfoDigitOrDate, "{0}", item.P09);
                item.P10 = string.Format(formatInfoDigitOrDate, "{0}", item.P10);
                item.P11 = string.Format(formatInfoDigitOrDate, "{0}", item.P11);
                item.P12 = string.Format(formatInfoDigitOrDate, "{0}", item.P12);
                item.P13 = string.Format(formatInfoDigitOrDate, "{0}", item.P13);
                item.P14 = string.Format(formatInfoDigitOrDate, "{0}", item.P14);
                item.P15 = string.Format(formatInfoDigitOrDate, "{0}", item.P15);
                item.P16 = string.Format(formatInfoDigitOrDate, "{0}", item.P16);
                item.P17 = string.Format(formatInfoDigitOrDate, "{0}", item.P17);
                item.P18 = string.Format(formatInfoDigitOrDate, "{0}", item.P18);
                item.P19 = string.Format(formatInfoDigitOrDate, "{0}", item.P19);
                item.P20 = string.Format(formatInfoDigitOrDate, "{0}", item.P20);
                item.P21 = string.Format(formatInfoDigitOrDate, "{0}", item.P21);
                item.P22 = string.Format(formatInfoDigitOrDate, "{0}", item.P22);
                item.P23 = string.Format(formatInfoDigitOrDate, "{0}", item.P23);
                item.P24 = string.Format(formatInfoDigitOrDate, "{0}", item.P24);
                item.P25 = string.Format(formatInfoDigitOrDate, "{0}", item.P25);
                item.P26 = string.Format(formatInfoDigitOrDate, "{0}", item.P26);
                item.P27 = string.Format(formatInfoDigitOrDate, "{0}", item.P27);
                item.P28 = string.Format(formatInfoDigitOrDate, "{0}", item.P28);
                item.P29 = string.Format(formatInfoDigitOrDate, "{0}", item.P29);
                item.P30 = string.Format(formatInfoDigitOrDate, "{0}", item.P30);
            }
        }

        private List<AggregateLevel> GetAggregateLevels()
        {
            List<AggregateLevel> rets = new List<AggregateLevel>();
            rets.Add(new AggregateLevel() { Type = AggregateLevelType.Day, Name = "день" });
            rets.Add(new AggregateLevel() { Type = AggregateLevelType.Week, Name = "неделя" });
            rets.Add(new AggregateLevel() { Type = AggregateLevelType.Month , Name = "месяц" });
            return rets;
        }

        private List<DynamicParameter> DictDynamicParameters
        {
            get
            {
                if (null == _dictDynParameters)
                {
                    _dictDynParameters = GetDynamicParameters();
                }
                return _dictDynParameters;
            }
        }

        private List<DynamicParameter> GetDynamicParameters()
        {
            List<DynamicParameter> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDictionaryDynamicParameters(DynamicModuleType.All),
                res =>
                {
                    rows = res.Result;
                }))
            {
                return rows;
            }
            return new List<DynamicParameter>();
        }

        private List<DateTime> GetDatesTechnoReport()
        {
            List<DateTime> result = null;
            if (_basePresenter.ExecuteServiceCall(_reportService.GetDatesTechnoReport,
                res => { result = res.Result; }))
            {
                return result;
            }
            return new List<DateTime>();
        }

        public object GetEmptyObjects()
        {
            return new List<DynamicTechnoParameter>();
        }

        public override bool IsClearResultDataAfterChangeCriteria()
        {
            return true;
        }

        public override bool IsGridAggregateVisible()
        {
            return false;
        }
    }
}
