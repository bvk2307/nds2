﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_CheckLogicControl
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            return GetControlSearchCriteriaDefault();
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportCheckLogicControl", GetType()));
            GridSetupHelper<CheckLogicControl> helper = new GridSetupHelper<CheckLogicControl>();

            string formatPercent = ColumnFormats.FormatPercent;
            string formatPercentGrid = ColumnFormats.FormatPercentGrid;
            IFormatProvider formatInfoPercent = ColumnFormats.FormatInfoPercent;
            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;

            setup.Columns.Add(helper.CreateColumnDefinition(o => o.Num, d => { d.SortIndicator = false; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.Name, d => { d.Width = 300; d.WidthColumnExcel = 45; d.SortIndicator = false; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorCountPercentage, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorFrequencyInvoiceNotExact, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorFrequencyInvoiceGap, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorFrequencyInvoiceExact, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorFrequencyInnerInvoice, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ErrorFrequencyOuterInvoice, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<CheckLogicControl> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchCheckLogicControl(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<CheckLogicControl>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<CheckLogicControl> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchCheckLogicControl(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<CheckLogicControl>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public override void ExcelSetup()
        {
            ExcelManagerReport excelManager = _basePresenter.GetExcelManager();
            excelManager.SetupHeaderRowCount(2);

            Dictionary<long, ExcelRowStyle> rowsStyle = new Dictionary<long, ExcelRowStyle>();
            rowsStyle.Add(0, new ExcelRowStyle() { IsVerticalAlignmentCenter = true });
            rowsStyle.Add(1, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            excelManager.SetupRowStyle(rowsStyle);
        }

        public object GetEmptyObjects()
        {
            return new List<CheckLogicControl>();
        }
    }
}
