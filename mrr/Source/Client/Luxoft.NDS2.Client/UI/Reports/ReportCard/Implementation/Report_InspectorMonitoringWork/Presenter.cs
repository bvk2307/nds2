﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_InspectorMonitoringWork
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private ColumnGroupDefinition groupUnrepaired;
        private ColumnGroupDefinition groupIdentifiedAndUnrepaired;
        private ColumnGroupDefinition groupIdentifiedAndRepaired;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            return GetControlSearchCriteriaDefault();
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportInspectorMonitoringWork", GetType()));
            GridSetupHelper<InspectorMonitoringWork> helper = new GridSetupHelper<InspectorMonitoringWork>();

            ColumnGroupDefinition groupFederalDistrict = new ColumnGroupDefinition() { Caption = String.Empty };
            groupFederalDistrict.Columns.Add(helper.CreateColumnDefinition(o => o.FederalDistrict, d => { d.Width = 100; d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupFederalDistrict);

            ColumnGroupDefinition groupRegion = new ColumnGroupDefinition() { Caption = String.Empty };
            groupRegion.Columns.Add(helper.CreateColumnDefinition(o => o.Region, d => { d.WidthColumnExcel = 23; d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupRegion);

            ColumnGroupDefinition groupInspection = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspection.Columns.Add(helper.CreateColumnDefinition(o => o.Inspection, d => { d.WidthColumnExcel = 40; d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupInspection);

            ColumnGroupDefinition groupInspector = new ColumnGroupDefinition() { Caption = String.Empty };
            groupInspector.Columns.Add(helper.CreateColumnDefinition(o => o.Inspector, d => { d.AllowSummary = true; d.SortIndicator = false; }));
            setup.Columns.Add(groupInspector);

            string formatRuble = ColumnFormats.FormatRuble;
            IFormatProvider formatInfoRuble = ColumnFormats.FormatInfoRuble;
            string formatRubleGrid = ColumnFormats.FormatRubleGrid;
            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;

            groupUnrepaired = new ColumnGroupDefinition() { Caption = "Не устраненные расхождения, направленные НП на [с]", };
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyGeneralSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyCurrencySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyNDSSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyInexactComparisonSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.UnrepairedDiscrepancyBreakSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupUnrepaired);

            groupIdentifiedAndUnrepaired = new ColumnGroupDefinition() { Caption = "Расхождения выявленные на [с] и НЕ УСТРАНЕННЫЕ на [по]", };
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyGeneralSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyCurrencySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyNDSSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyInexactComparisonSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndUnrepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndUnrepairedDiscrepancyBreakSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupIdentifiedAndUnrepaired);

            groupIdentifiedAndRepaired = new ColumnGroupDefinition() { Caption = "Расхождения выявленные на [с] и УСТРАНЕННЫЕ на [по]", };
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyGeneralCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyGeneralSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyCurrencyCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyCurrencySum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyNDSCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyNDSSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyInexactComparisonCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyInexactComparisonSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyBreakCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.AllowSummary = true; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupIdentifiedAndRepaired.Columns.Add(helper.CreateColumnDefinition(o => o.IdentifiedAndRepairedDiscrepancyBreakSum, d => { d.AllowSummary = true; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupIdentifiedAndRepaired);

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            Dictionary<string, GridColumnHeaderChanage> columns = new Dictionary<string, GridColumnHeaderChanage>();
            string sDtBegin = String.Empty;
            string sDtEnd = String.Empty;
            _helper.GetDateBeginAndEnd(conditions, out sDtBegin, out sDtEnd);

            if (groupUnrepaired != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Не устраненные расхождения, направленные НП на {0}", sDtBegin);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupUnrepaired.Key, columnChange);
            }
            if (groupIdentifiedAndUnrepaired != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Расхождения выявленные на {0} и НЕ УСТРАНЕННЫЕ на {1}", sDtBegin, sDtEnd);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupIdentifiedAndUnrepaired.Key, columnChange);
            }
            if (groupIdentifiedAndRepaired != null)
            {
                GridColumnHeaderChanage columnChange = new GridColumnHeaderChanage();
                columnChange.Caption = string.Format("Расхождения выявленные на {0} и УСТРАНЕННЫЕ на {1}", sDtBegin, sDtEnd);
                columnChange.ToolTipText = columnChange.Caption;
                columns.Add(groupIdentifiedAndRepaired.Key, columnChange);
            }

            return columns;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<InspectorMonitoringWork> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchInspectorMonitoringWork(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<InspectorMonitoringWork>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<InspectorMonitoringWork> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchInspectorMonitoringWork(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<InspectorMonitoringWork>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public object GetEmptyObjects()
        {
            return new List<InspectorMonitoringWork>();
        }
    }
}
