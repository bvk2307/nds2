﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic
{
    public class MonthInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
