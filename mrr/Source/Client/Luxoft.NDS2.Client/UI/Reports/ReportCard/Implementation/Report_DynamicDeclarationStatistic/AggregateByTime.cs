﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DynamicDeclarationStatistic
{
    public enum AggregateByTimeType
    {
        Day = 1,
        Week = 2,
        Month = 3
    }

    public class AggregateByTime
    {
        public AggregateByTimeType Type { get; set; }
        public string Name { get; set; }
    }
}
