﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_MatchingRule
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private MatchingRuleSearch _controlSearchCriteria;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
        }

        public UserControl GetControlSearchCriteria()
        {
            if (null == _controlSearchCriteria)
            {
                _controlSearchCriteria = new MatchingRuleSearch(_basePresenter.GetDataService());
                _controlSearchCriteria.SetAggregateByNalogOrgan(GetAggregateByNalogOrgan());
                _controlSearchCriteria.SetFederalDistricts(GetFederalDistricts());
            }
            return _controlSearchCriteria;
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportMatchingRule", GetType()));
            GridSetupHelper<MatchingRule> helper = new GridSetupHelper<MatchingRule>();

            string formatPercent = ColumnFormats.FormatPercent;
            string formatPercentGrid = ColumnFormats.FormatPercentGrid;
            string formatRuble = ColumnFormats.FormatRuble;
            IFormatProvider formatInfoPercent = ColumnFormats.FormatInfoPercent;
            IFormatProvider formatInfoRuble = ColumnFormats.FormatInfoRuble;
            string formatRubleGrid = ColumnFormats.FormatRubleGrid;
            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;

            setup.Columns.Add(helper.CreateColumnDefinition(o => o.RuleGroupCaption, d => { d.Width = 150;  d.SortIndicator = false; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.RuleNumberCaption, d => { d.Width = 150; d.SortIndicator = false; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.RuleNumberComment, d => { d.Width = 150; d.SortIndicator = false; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.MatchCount, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.MatchAmount, d => { d.Width = 150; d.FormatExcel = formatRuble; d.FormatInfo = formatInfoRuble; d.Format = formatRubleGrid; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.PercentByCount, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.PercentByAmount, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.PercentInnerInvoiceErrorLC, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.PercentOuterInvoiceErrorLC, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.PercentInnerOuterInvoiceErrorLC, d => { d.Format = formatPercentGrid; d.FormatExcel = formatPercent; d.FormatInfo = formatInfoPercent; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));

            setup.QueryCondition = GetGridDefaultQueryConditions();

            return setup;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<MatchingRule> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchMatchingRule(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<MatchingRule>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<MatchingRule> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchMatchingRule(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<MatchingRule>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public override void ExcelSetup()
        {
            ExcelManagerReport excelManager = _basePresenter.GetExcelManager();
            excelManager.SetupHeaderRowCount(2);

            Dictionary<long, ExcelRowStyle> rowsStyle = new Dictionary<long, ExcelRowStyle>();
            rowsStyle.Add(0, new ExcelRowStyle() { IsVerticalAlignmentCenter = true });
            rowsStyle.Add(1, new ExcelRowStyle() { IsHorizontalAlignmentCenter = true });
            excelManager.SetupRowStyle(rowsStyle);
        }

        public object GetEmptyObjects()
        {
            return new List<MatchingRule>();
        }
    }
}
