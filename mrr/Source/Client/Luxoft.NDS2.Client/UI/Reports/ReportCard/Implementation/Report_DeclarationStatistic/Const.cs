﻿
namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DeclarationStatistic
{
    public enum DictionaryStatisticTypeConst
    {
        Country = 1,
        FederalDistrict = 2,
        Region = 3,
        Inspection = 4
    }

    public enum DictionaryStatisticRegimConst
    {
        OnCurrentDate = 1,
        OnBetweenDates = 2
    }
}
