﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard.Implementation.Report_DeclarationStatistic
{
    public class Presenter : ReportPresenterBase, IReportPresenter
    {
        private DeclarationStatisticSearch _controlSearchCriteria;
        private ColumnDefinition colNameGroup;

        public Presenter(ReportGeneralHelper helper, IReportService reportService, IBasePresenter basePresenter)
            : base(helper, reportService, basePresenter)
        {
            accessContext = LoadAccessContext(new[]
            {
                MrrOperations.Macroreports.ReportBackgroundMetricsCountry,
                MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions,
                MrrOperations.Macroreports.ReportBackgroundMetricsRegions,
                MrrOperations.Macroreports.ReportBackgroundMetricsInspections
            });
            AccessRight right =
                basePresenter.CsudAccessRights.FirstOrDefault(n => n.PermType == PermissionType.Operation);
            sonoCode = right != null ? right.StructContext : string.Empty;
        }

        public UserControl GetControlSearchCriteria()
        {
            if (null != _controlSearchCriteria)
                return _controlSearchCriteria;

            _controlSearchCriteria = new DeclarationStatisticSearch(accessContext, sonoCode);
            _controlSearchCriteria.SetRegions(GetRegions(true));
            _controlSearchCriteria.SetNalogPeriods(GetNalogPeriod());
            return _controlSearchCriteria;
        }

        protected override GridSetup CreateGridSetup()
        {
            GridSetup setup = _basePresenter.CommonSetup(string.Format("{0}_GetGridSetupReportDeclarationStatistic", GetType()));
            GridSetupHelper<DeclarationStatistic> helper = new GridSetupHelper<DeclarationStatistic>();

            string formatLong = ColumnFormats.FormatLong;
            string formatLongGrid = ColumnFormats.FormatLongGrid;
            IFormatProvider formatInfoDigit = ColumnFormats.FormatInfoDigit;
            string formatDecimal = ColumnFormats.FormatDecimal;
            string formatDecimalGrid = ColumnFormats.FormatDecimalGrid;

            colNameGroup = helper.CreateColumnDefinition(o => o.NameGroup, d => { d.Width = 240; d.WidthColumnExcel = 40; d.SortIndicator = false; d.RowSpan = 2; });
            setup.Columns.Add(colNameGroup);
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountTotal, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountNull, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountCompensation, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountPayment, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 3; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountNotSubmit, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.Width = 180; d.WidthColumnExcel = 30; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.DeclCountRevisedTotal, d => { d.Format = formatLongGrid; d.FormatExcel = formatLong; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));

            ColumnGroupDefinition groupNdsBase = new ColumnGroupDefinition() { Caption = "Налоговая база" };
            groupNdsBase.Columns.Add(helper.CreateColumnDefinition(o => o.TaxBaseSumCompensation, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupNdsBase.Columns.Add(helper.CreateColumnDefinition(o => o.TaxBaseSumPayment, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupNdsBase);

            ColumnGroupDefinition groupNdsCalculated = new ColumnGroupDefinition() { Caption = "Сумма исчисленного НДС с налоговой базы" };
            groupNdsCalculated.Columns.Add(helper.CreateColumnDefinition(o => o.NdsCalculatedSumCompensation, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Width = 130; d.WidthColumnExcel = 20; d.Align = ColumnAlign.Right; }));
            groupNdsCalculated.Columns.Add(helper.CreateColumnDefinition(o => o.NdsCalculatedSumPayment, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Width = 130; d.WidthColumnExcel = 20; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupNdsCalculated);

            ColumnGroupDefinition groupNdsDeduction = new ColumnGroupDefinition() { Caption = "Сумма вычетов по НДС" };
            groupNdsDeduction.Columns.Add(helper.CreateColumnDefinition(o => o.NdsDeductionSumCompensation, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            groupNdsDeduction.Columns.Add(helper.CreateColumnDefinition(o => o.NdsDeductionSumPayment, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(groupNdsDeduction);

            setup.Columns.Add(helper.CreateColumnDefinition(o => o.NdsSumCompensation, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.NdsSumPayment, d => { d.Format = formatDecimalGrid; d.FormatExcel = formatDecimal; d.FormatInfo = formatInfoDigit; d.SortIndicator = false; d.RowSpan = 2; d.Align = ColumnAlign.Right; }));

            setup.QueryCondition = GetGridDefaultQueryConditions();

            setup.OrderColumns();

            return setup;
        }

        public override Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions)
        {
            var columns = new Dictionary<string, GridColumnHeaderChanage>();
            if (colNameGroup != null)
            {
                object oDeclType = _helper.GetFilterFromCondition(conditions, "DeclarationStatisticType");
                if (oDeclType != null)
                {
                    var declarationStatisticType = (int)oDeclType;
                    string nameGroup = string.Empty;
                    var nameGroups = new Dictionary<int, string>
                    {
                        {(int) DictionaryStatisticTypeConst.Country, "Страна"},
                        {(int) DictionaryStatisticTypeConst.FederalDistrict, "Федеральный округ"},
                        {(int) DictionaryStatisticTypeConst.Region, "Регион"},
                        {(int) DictionaryStatisticTypeConst.Inspection, "Инспекция"}
                    };

                    if (nameGroups.ContainsKey(declarationStatisticType))
                    {
                        nameGroup = nameGroups[declarationStatisticType];
                    }
                    var columnChange = new GridColumnHeaderChanage {Caption = nameGroup};
                    columnChange.ToolTipText = columnChange.Caption;
                    columns.Add(colNameGroup.Key, columnChange);
                }
            }
            return columns;
        }

        public object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber)
        {
            totalRowsNumber = 0;
            long totalMatches = 0;
            List<DeclarationStatistic> rows = null;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDeclarationStatistic(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    totalMatches = res.Result.TotalMatches;
                }))
            {
                totalRowsNumber = totalMatches;
                return rows;
            }
            return new List<DeclarationStatistic>();
        }

        public List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches)
        {
            List<DeclarationStatistic> rows = null;
            int rowsTotal = 0;
            rowsTotalMatches = 0;
            if (_basePresenter.ExecuteServiceCall(() => _reportService.SearchDeclarationStatistic(conditions, taskId),
                res =>
                {
                    rows = res.Result.Rows;
                    rowsTotal = res.Result.TotalMatches;
                }))
            {
                rowsTotalMatches = rowsTotal;
                List<Dictionary<string, object>> rez = _helper.ConvertToDictionary<DeclarationStatistic>(rows);
                return _helper.ConvertToColumnFormat(rez);
            }
            return new List<Dictionary<string, object>>();
        }

        public override void ExcelSetup()
        {
            ExcelManagerReport excelManager = _basePresenter.GetExcelManager();
            excelManager.SetupHeaderRowCount(3);

            var rowsStyle = new Dictionary<long, ExcelRowStyle>
            {
                {0, new ExcelRowStyle() {IsHorizontalAlignmentCenter = true}},
                {1, new ExcelRowStyle() {IsVerticalAlignmentCenter = true}},
                {2, new ExcelRowStyle() {IsHorizontalAlignmentCenter = true}}
            };
            excelManager.SetupRowStyle(rowsStyle);
        }

        public object GetEmptyObjects()
        {
            return new List<DeclarationStatistic>();
        }
    }
}
