﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard
{
    public interface IReportPresenter
    {
        UserControl GetControlSearchCriteria();
        GridSetup GetGridSetup();
        object GetRows(QueryConditions conditions, long taskId, out long totalRowsNumber);
        List<Dictionary<string, object>> GetRowsExcel(QueryConditions conditions, long taskId, out long rowsTotalMatches);
        void ExcelSetup();
        Dictionary<string, object> GetSummaryRow(QueryConditions conditions);
        Dictionary<string, GridColumnHeaderChanage> GetChangeColumns(QueryConditions conditions);
        object GetEmptyObjects();
        bool IsClearResultDataAfterChangeCriteria();
        bool IsGridAggregateVisible();
        string GetReportName();
        string GetReportCaption();
        string GetReportTitle();
    }
}
