﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Reports.ReportCard
{
    public static class QuarterCalculator
    {
        public const int COUNT_MONTH_IN_QUARTER = 3;

        public const int COUNT_QUARTER_IN_YEAR = 4;

        public const int LIMIT_DURATION_NALOGPERIOD_IN_YEAR = 2;

        public const int QUARTER_OFFSET_IN_DAYS = 25;

        public static DateTime CalculatePreviousQuarter(DateTime date)
        {
            return date
                .AddMonths(-COUNT_MONTH_IN_QUARTER);
        }

        public static DateTime CalculateQuarterOffset(DateTime date)
        {
            return date
                .AddDays(-QUARTER_OFFSET_IN_DAYS);
        }

        public static DateTime CalculatePreviousQuarterWithOffset(DateTime date)
        {
            return date
                .AddDays(-QUARTER_OFFSET_IN_DAYS)
                .AddMonths(-COUNT_MONTH_IN_QUARTER);
        }
    }
}
