﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.Reports.Interfaces
{
    /// <summary> A file creator for a node table export in graph tree. </summary>
    public interface INodeAllTreeReporter : INodeReporter
    {
        void StartReportBuild( string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData,
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) );

        void Process( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> callExecContext, 
                      CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="exception"> Not 'null' if some exception is caught. </param>
        /// <returns> A created target report file full specification or 'null' if it is not created on error or cancelling.  </returns>
        string FinishReportBuild( CancellationToken cancelToken = default(CancellationToken), Exception exception = null );

        Task<string> StartReportBuildAsync( string reportFileFullSpec, GraphTreeNodeReportData reportData,
            Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>> dataProviderHandler, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) );
    }
}