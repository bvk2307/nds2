﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Reports.Interfaces
{
    /// <summary> A file creator for a node table export in graph tree. </summary>
    public interface INodeReporter : IFileCreator
    {
        /// <summary> Inits the reporter before execution. </summary>
        void Init( IProgress<Tuple<long?, bool?>> progress );

        string GenerateExcelFileName( GraphTreeNodeReportData treeTableData );

        /// <summary> Copies visible columns in new ones, for example, to do not share originals with other thread. </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        IReadOnlyCollection<ColumnBase> CopyVisibleColumnsOnly( IReadOnlyCollection<ColumnBase> columns );
    }
}