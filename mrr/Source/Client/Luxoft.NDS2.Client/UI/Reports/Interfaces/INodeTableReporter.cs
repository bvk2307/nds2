﻿using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Client.UI.Reports.Interfaces
{
    /// <summary> A file creator for a node table export in graph tree. </summary>
    public interface INodeTableReporter : INodeReporter
    {
        Task<string> StartReportBuildAsync( string reportFileFullSpec, IReadOnlyCollection<ColumnBase> reportColumns, GraphTreeNodeReportData reportData, IEnumerable<GraphDataNode> reportRows, CancellationToken cancelToken = default(CancellationToken) );
    }
}