﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.UI.Reports.Interfaces
{
    /// <summary> A file creator for a node table export in graph tree. </summary>
    public interface INodeVisibleTreeReporter : INodeReporter
    {
        Task<string> StartReportBuildAsync(string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData, IReadOnlyCollection<GraphDataNode> reportRows, 
            CancellationToken cancelToken = default(CancellationToken) );
    }
}