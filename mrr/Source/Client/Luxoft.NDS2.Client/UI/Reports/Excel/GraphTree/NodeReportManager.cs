﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeReportManager : ExcelManagerBase
    {
        /// <summary> A maximum sheet size as row number. </summary>
        private readonly int _rowsNumberPerSheetLimit;

        /// <summary> The reference flag is used to define that the manager is busy by Excel build. </summary>
        private SpreadsheetDocument _document = null;

        private OpenXmlReader _reader = null;
        private OpenXmlWriter _writer = null;
        private WorksheetPart _worksheetPart = null;
        private string _origninalSheetId = null;
        private string _replacementPartId = null;
        private long _currentRowAbsoluteNumber = -1;
        private uint _lastSheetId = 0;

        public NodeReportManager( int sheetMaxDataRowsLimit )
        {
            _rowsNumberPerSheetLimit = sheetMaxDataRowsLimit <= 0 ? 100000 : sheetMaxDataRowsLimit;
        }

        /// <summary> A maximum sheet size as row number. </summary>
        public int RowsNumberPerSheetLimit
        {
            get { return _rowsNumberPerSheetLimit; }
        }

        public Worksheet FillWorkbook( SpreadsheetDocument document, string filePath, Func<int, DataTable> getPageDataHandler, 
            List<double> columnsWidths, CancellationToken cancelToken = default(CancellationToken), 
            Func<object, DataColumn, ExcelCellStyle> cellStyleSelectior = null )
        {
            StartReadWrite( document );
            BeginReadWriteSheet( document, columnsWidths );

            Worksheet worksheet = null;
            try
            {
                ReadWriteCycle( getPageDataHandler, cellStyleSelectior, cancelToken );
            }
            finally
            {
                worksheet = EndReadWriteSheet( cancelToken );
                FinishReadWrite();
            }
            return worksheet;
        }

        public void StartReadWrite( SpreadsheetDocument document )
        {
            Contract.Requires( document != null && document.WorkbookPart != null );
            Contract.Ensures( _document != null );

            if ( null != Interlocked.CompareExchange( ref _document, document, null ) )
                throw new InvalidOperationException( "Other Excel build has not completed yet" );
        }

        public void BeginReadWriteSheet( SpreadsheetDocument document, List<double> columnsWidths )
        {
            Contract.Requires( document != null && document.WorkbookPart != null );
            Contract.Ensures( _writer != null );
            Contract.Ensures( _reader != null );
            Contract.Ensures( _replacementPartId != null );
            Contract.Ensures( _origninalSheetId != null );
            Contract.Ensures( _worksheetPart != null );

            _document = document;   //replaces the previous one that can be disposed already

            WorkbookPart workbookPart = _document.WorkbookPart;
            _worksheetPart = workbookPart.WorksheetParts.Last();
            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            if ( _lastSheetId == 0 )
            {
                _lastSheetId = sheets.Elements<Sheet>().MaxWDefault( sheet => sheet.SheetId );
            }
            else
            {
                Contract.Assume( ExportExcelParam != null );

                _worksheetPart = InsertWorksheet( _document, (int)++_lastSheetId, columnsWidths );
                _worksheetPart.Worksheet.Save();
            }
            _origninalSheetId = workbookPart.GetIdOfPart( _worksheetPart );

            WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
            _replacementPartId = workbookPart.GetIdOfPart( replacementPart );

            _currentRowAbsoluteNumber = -1;

            _reader = OpenXmlReader.Create( _worksheetPart );
            _writer = OpenXmlWriter.Create( replacementPart );
        }

        public Worksheet EndReadWriteSheet( CancellationToken cancelToken )
        {
            Contract.Ensures( _writer == null );
            Contract.Ensures( _reader == null );
            Contract.Ensures( _replacementPartId == null );
            Contract.Ensures( _origninalSheetId == null );
            Contract.Ensures( cancelToken.IsCancellationRequested || Contract.Result<Worksheet>() != null );
            Contract.Assume( _document != null );

            IDisposable     reader              = _reader;
            IDisposable     writer              = _writer;
            string          origninalSheetId    = _origninalSheetId;
            string          replacementPartId   = _replacementPartId;

            _reader             = null;
            _writer             = null;
            _origninalSheetId   = null;
            _replacementPartId  = null;

            _currentRowAbsoluteNumber = -1;

            using ( writer ) { }
            using ( reader ) { }

            WorksheetPart worksheetPartEdited = null;
            if ( !cancelToken.IsCancellationRequested )
            {
                WorkbookPart workbookPart = _document.WorkbookPart;
                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().First( s => s.Id.Value.Equals( origninalSheetId ) );
                sheet.Id.Value = replacementPartId;
                workbookPart.DeletePart( _worksheetPart );

                worksheetPartEdited = (WorksheetPart)workbookPart.GetPartById( replacementPartId );
            }

            return worksheetPartEdited == null ? null : worksheetPartEdited.Worksheet;
        }

        public void FinishReadWrite()
        {
            Contract.Ensures( _lastSheetId == 0 );
            Contract.Ensures( _worksheetPart == null );
            Contract.Ensures( _document == null );

            _lastSheetId        = 0;
            _worksheetPart      = null;

            if ( null == Interlocked.Exchange( ref _document, null ) )
                throw new InvalidOperationException( "Unexpected situation: building flag has been reset from somewhere during the current Excel build" );
        }

        /// <summary> Is the read|write cycle in progress now? </summary>
        /// <returns> 'true' if the read|write cycle is in progress now otherwise 'false'. </returns>
        public bool IsReadWriteInProgress()
        {
            return null != Interlocked.CompareExchange( ref _document, null, null );  //reads only a value of '_document'
        }

        private void ReadWriteCycle( Func<int, DataTable> getPageDataHandler, 
            Func<object, DataColumn, ExcelCellStyle> cellStyleSelectior, CancellationToken cancelToken = default(CancellationToken) )
        {
            BeginReadWriteContent( cancelToken );

            WriteRowCycle( getPageDataHandler, cellStyleSelectior, cancelToken );

            EndReadWriteContent( cancelToken );
        }

        public void BeginReadWriteContent( CancellationToken cancelToken )
        {
            bool sheetFound = ReadWriteUntilSheetOrEnd( _reader, _writer, cancelToken );

            Contract.Assume( sheetFound );

            _writer.WriteStartElement( new SheetData() );
        }

        public void EndReadWriteContent( CancellationToken cancelToken )
        {
            _writer.WriteEndElement();

            bool sheetFound = ReadWriteUntilSheetOrEnd( _reader, _writer, cancelToken );

            Contract.Assume( !sheetFound );
        }

        private bool ReadWriteUntilSheetOrEnd( OpenXmlReader reader, OpenXmlWriter writer, CancellationToken cancelToken = default(CancellationToken) )
        {
            bool sheetFound = false;

            while ( !cancelToken.IsCancellationRequested && reader.Read() )
            {
                if ( reader.ElementType == typeof(SheetData) )
                {
                    if ( !reader.IsEndElement )
                    {
                        sheetFound = true;                        

                        break;
                    }
                }
                else if ( reader.IsStartElement )
                {
                    writer.WriteStartElement( reader );
                }
                else if ( reader.IsEndElement )
                {
                    writer.WriteEndElement();
                }
            }
            return sheetFound;
        }

        private void WriteRowCycle( Func<int, DataTable> getPageDataHandler, Func<object, DataColumn, ExcelCellStyle> cellStyleSelectior, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            DataTable dtRows;
            int pageCount = -1;
            Row rowTemplate = CreateTemplateRow();

            while ( null != ( dtRows = getPageDataHandler( ++pageCount ) ) )
            {
                WriteRows( dtRows, cellStyleSelectior, rowTemplate: rowTemplate, cancelToken: cancelToken );

                if ( cancelToken.IsCancellationRequested )
                    break;
            }
        }

        /// <summary> Writes rows into the Excel writer. </summary>
        /// <param name="dtRows"></param>
        /// <param name="cellStyleSelectior"></param>
        /// <param name="rowStartIndex"> A start row index in the row collectio of <paramref name="dtRows"/> to beging write from. Do not separate by sheet if it is <see cref="int.MinValue"/>. </param>
        /// <param name="rowTemplate"></param>
        /// <param name="cancelToken"></param>
        /// <returns> An index of the first row from ones left not written or a negative value if all rows has already been written. </returns>
        public int WriteRows( DataTable dtRows, Func<object, DataColumn, ExcelCellStyle> cellStyleSelectior = null, 
            int rowStartIndex = 0, Row rowTemplate = null, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( dtRows != null );
            Contract.Requires( rowStartIndex >= 0 || rowStartIndex == int.MinValue );
            Contract.Ensures( Contract.Result<int>() == int.MinValue || Contract.Result<int>() >= 0 );

            DataRow row;
            if ( rowTemplate == null )
                rowTemplate = CreateTemplateRow();

            long nextSheetLimit = 
                ( _currentRowAbsoluteNumber == -1 ? 0 : _currentRowAbsoluteNumber % RowsNumberPerSheetLimit ) + RowsNumberPerSheetLimit;
            const int signalShift = 10;   //byte binary shift size (step is 2^10 == 1024)
            long nextSignalLimit = _currentRowAbsoluteNumber == -1 
                ? 0 : ( ( _currentRowAbsoluteNumber + 1 >> signalShift ) << signalShift ) + ( 1 << signalShift );

            bool separateBySheet = rowStartIndex != int.MinValue;
            int rowIndex = separateBySheet ? rowStartIndex : 0;
            for ( ; rowIndex < dtRows.Rows.Count; rowIndex++ )
            {
                ++_currentRowAbsoluteNumber;
                row = dtRows.Rows[ rowIndex ];
                SetRowHeight( rowTemplate, _currentRowAbsoluteNumber );
                ExcelRowStyle excelRowStyle = GetExcelRowStyle( _currentRowAbsoluteNumber );

                _writer.WriteStartElement( rowTemplate );

                DataColumnCollection columns = dtRows.Columns;
                for ( int colIndex = 0; colIndex < columns.Count; colIndex++ )
                {
                    object cellValue = row[ colIndex ];
                    Type typeValue = cellValue.GetType();
                    Cell c = CreateCell( _currentRowAbsoluteNumber, excelRowStyle, cellValue, cellColumn: columns[ colIndex ],
                                         cellStyleSelectior: cellStyleSelectior, applyRowStyleToNoHeader: true );
                    if ( ReportGeneralHelper.IsNumberType( typeValue ) )
                    {
                        c.DataType = CellValues.Number;
                        c.CellValue = new CellValue( GenerateExcelNumberValue( cellValue, typeValue ) );
                    }
                    else
                    {
                        c.DataType = CellValues.InlineString;
                        InlineString inlineString = new InlineString();
                        Text t = new Text();
                        t.Text = cellValue.ToString();
                        inlineString.AppendChild( t );
                        c.AppendChild( inlineString );
                    }
                    _writer.WriteElement( c );
                }
                _writer.WriteEndElement();

                if ( cancelToken.IsCancellationRequested )
                    break;

                if ( separateBySheet && _currentRowAbsoluteNumber == nextSheetLimit )
                {
                    GenerateEventExt( _currentRowAbsoluteNumber );

                    break;  //in suppose that extenal call recall this method with returned 'rowIndex' after adding of a new sheet
                }
                if ( _currentRowAbsoluteNumber == nextSignalLimit )
                {
                    nextSignalLimit += 1 << signalShift;;
                    WorkbookPart workbookPart = _document.WorkbookPart;

                    workbookPart.Workbook.Save( workbookPart );

                    GenerateEventExt( _currentRowAbsoluteNumber );
                }
            }
            return rowIndex < dtRows.Rows.Count ? rowIndex : int.MinValue;  //'int.MinValue' is a signal that all rows are written
        }

        private static Row CreateTemplateRow()
        {
            return new Row();
        }
    }
}
