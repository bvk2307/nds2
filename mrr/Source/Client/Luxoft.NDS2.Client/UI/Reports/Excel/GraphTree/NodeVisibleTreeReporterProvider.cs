﻿using System.Diagnostics.Contracts;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeVisibleTreeReporterProvider : IInstanceProvider<INodeVisibleTreeReporter>
    {
        private readonly IClientLogger _clientLogger;

        private INodeVisibleTreeReporter _nodeTreeReporter = null;

        public NodeVisibleTreeReporterProvider( IClientLogger clientLogger )
        {
            Contract.Requires( clientLogger != null );

            _clientLogger = clientLogger;
        }

        /// <summary> A provided instance. The provider does not guarantee that returns the same instance on each call. </summary>
        public INodeVisibleTreeReporter Instance
        {
            get
            {
                if ( _nodeTreeReporter == null )
                    _nodeTreeReporter = new NodeTreePlainReporter( _clientLogger );

                return _nodeTreeReporter;
            }
        }
    }
}