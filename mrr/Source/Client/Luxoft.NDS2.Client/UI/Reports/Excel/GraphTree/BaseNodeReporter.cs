﻿//#define SEPARATEBYSHEETS	//apopov 2.11.2016	//TODO!!!   //uncommented when defect will be fixed: (КТР) ДС. Выгрузка в Excel. Есть разбиение на страницы https://sentinel2.luxoft.com/sen/issues/browse/GNIVC_NDS2-5691

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using CommonComponents.Instrumentation;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Common;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Helpers.IO;
using Luxoft.NDS2.Common.Helpers.Iteration;
using Tuple = System.Tuple;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    /// <summary> Remove debug code before release in <see cref="BeginReportBuild"/>() ! </summary>
    /// <typeparam name="TManager"></typeparam>
    public abstract class BaseNodeReporter<TManager> : INodeReporter where TManager : ExcelManagerBase
    {
        public const string ReportExcelFileExtensionName = ".xlsx";

        #region Private members

        protected const string ReportSheetNameText = "Стр. {0}";

        private const string ReportExcelFilePartIsPurchase = "покупатель";
        private const string ReportExcelFilePartIsSeller = "продавец";

        private readonly IClientLogger _clientLogger;

        private GraphTreeNodeReportData _reportData = null;
        private IProfilingEntry _profilingEntry = null;

        private IProgress<Tuple<long?, bool?>> _progressHandler = null;

        private readonly ReportGeneralHelper _helper = new ReportGeneralHelper();
        private readonly TManager _excelManager;
        private readonly bool _considerRootAsLineToo;
        private readonly Lazy<TimeProfilingHelper> _lzTimeProfilingHelper = null;

        /// <summary> A target report file full specification requested by user. </summary>
        private string _targetFileFullSpec = null;
        /// <summary> A temporary report file full specification. It will be deleted after copying into the target one <see cref="_createdFileFullSpec"/>. </summary>
        private string _tempFileFullSpec = null;
        /// <summary> Report table column widths. </summary>
        private List<double> _columnsWidths = null;
        /// <summary> Report table columns. </summary>
        private IReadOnlyCollection<ColumnBase> _reportColumns = null;
        /// <summary> Excel document of the report. </summary>
        private SpreadsheetDocument _spreadsheetDocument = null;
        /// <summary> Cells to merge. </summary>
        private List<ExcelMergeCell> _mergeCells = null;
        /// <summary> The current Excel table datas. </summary>
        private DataTable _dataTableExcel = null;
        /// <summary> The current datas prepared to fill the current excel table <see cref="_dataTableExcel"/>. </summary>
        private List<Dictionary<string, object>> _preparedDataRows = null;
        /// <summary> The current part row enumerator. </summary>
        private CountLimitedEnumerator<GraphDataNode> _reportRowEnumerator = null;
        /// <summary> The number of fetched report rows currently. </summary>
        private long _reportAllRowsFetchedCount = 0;
        /// <summary> The current absolute row index on sheet for the next new row based on 0. </summary>
        private int _currentNewRowIndexOnSheet = int.MinValue;

        #endregion Private members

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant( _dataTableExcel != null || _preparedDataRows == null ); //both '_dataTableExcel' and '_preparedDataRows' are equal to 'null' or both are not equal to 'null'
        }

        public BaseNodeReporter( TManager excelManager, IClientLogger clientLogger, bool considerRootAsLineToo = false )
        {
            Contract.Requires( excelManager != null );
            Contract.Requires( clientLogger != null );

            _excelManager           = excelManager;
            _clientLogger           = clientLogger;
            _considerRootAsLineToo  = considerRootAsLineToo;

            _lzTimeProfilingHelper  = new Lazy<TimeProfilingHelper>( () => new TimeProfilingHelper( _clientLogger.Instance ) );
        }

        protected abstract IReadOnlyCollection<ColumnBase> DefineColumns( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters );

        protected abstract void AddColumnsWidth( ColumnDefinition columnDef, List<double> columnsWidth );

        /// <summary> Fills the workbook with this one call. Use only <see cref="OnFillWorkbook"/>() or only <see cref="OnWriteRows"/>() at the same report build. </summary>
        /// <param name="document"></param>
        /// <param name="reportTempFileFullSpec"></param>
        /// <param name="processPageBlockHandler"></param>
        /// <param name="columnsWidths"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        protected virtual Worksheet OnFillWorkbook( SpreadsheetDocument document, string reportTempFileFullSpec, 
            Func<int, DataTable> processPageBlockHandler, List<double> columnsWidths, CancellationToken cancelToken )
        {
            throw new NotImplementedException( "BaseNodeReporter.OnFillWorkbook() must been overrided to be used" ); 
        }

        /// <summary> Inits the workbook before sheet writing by <see cref="OnWriteRows"/>(). Do not use this method with <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        /// <param name="document"></param>
        /// <param name="columnsWidths"></param>
        /// <param name="isFirstSheet"> Is read|write starting for the first sheet? </param>
        /// <param name="cancelToken"></param>
        protected virtual void OnBeginReadWriteSheet( SpreadsheetDocument document, List<double> columnsWidths, 
            bool isFirstSheet, CancellationToken cancelToken = default(CancellationToken) )
        {
            throw new NotImplementedException( "BaseNodeReporter.OnBeginReadWriteSheet() must been overrided to be used" ); 
        }

        /// <summary> Deinits the workbook after sheet writing by <see cref="OnWriteRows"/>(). Do not use this method with <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        protected virtual Worksheet OnEndReadWriteSheet( CancellationToken cancelToken = default(CancellationToken) )
        {
            throw new NotImplementedException( "BaseNodeReporter.OnEndReadWriteSheet() must been overrided to be used" ); 
        }

        /// <summary> Are workbook read|write deinitialization and saving not needed? This base impelementation always returns 'false'. </summary>
        /// <returns></returns>
        protected virtual bool IsDeinitializationAndSavingNotNeeded()
        {
            return false;
        }

        /// <summary> Writes rows into the Excel writer. Can be call in a cycle. Use only <see cref="OnWriteRows"/>() or only <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        /// <param name="dtRows"></param>
        /// <param name="rowStartIndex"> A start row index in the row collectio of <paramref name="dtRows"/> to beging write from. Do not separate by sheet if it is <see cref="int.MinValue"/>. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An index of the first row from ones left not written or a negative value if all rows has already been written. </returns>
        protected virtual int OnWriteRows( DataTable dtRows, int rowStartIndex, CancellationToken cancelToken = default(CancellationToken) )
        {
            throw new NotImplementedException( "BaseNodeReporter.OnWriteRows() must been overrided to be used" ); 
        }

        protected abstract int OnFillBlockExcelData( GraphTreeNodeReportData reportData, IReadOnlyCollection<ColumnBase> reportColumns, 
            ref int lastAddedBlockLevel, int rowCountCurrent, List<ExcelMergeCell> mergeCells, GraphDataNode parentGraphData, 
            List<Dictionary<string, object>> dataRows, DataTable dataTable, bool isFirstBlock, 
            Dictionary<long, ExcelRowStyle> rowStyles, List<long> headerRowIds );

        /// <summary> Fills excel table datas in <paramref name="dataTable"/> from <paramref name="dataRows"/>. This base implementaition does nothing and always returns 0. </summary>
        /// <param name="reportColumns"></param>
        /// <param name="rowCountCurrent"></param>
        /// <param name="dataRows"></param>
        /// <param name="dataTable"></param>
        /// <param name="partRowStyles"></param>
        /// <returns></returns>
        protected virtual int OnFillExcelTableData( IReadOnlyCollection<ColumnBase> reportColumns, int rowCountCurrent,
            List<Dictionary<string, object>> dataRows, DataTable dataTable, Dictionary<long, ExcelRowStyle> partRowStyles )
        {
            return 0;
        }

        /// <summary> A maximum sheet size as row number. </summary>
        protected virtual int RowsNumberPerSheetLimit { get { return 0; } }

        protected TManager ExcelManager { get { return _excelManager; } }

        protected TimeProfilingHelper ProfilingHelper { get { return _lzTimeProfilingHelper.Value;  } }

        public ReportGeneralHelper ReportHelper { get { return _helper; } }

        private Lazy<TimeProfilingHelper> TimeProfilingHelper
        {
            get { return _lzTimeProfilingHelper; }
        }

        /// <summary> Inits the reporter before execution. </summary>
        /// <param name="progress"></param>
        public void Init( IProgress<Tuple<long?, bool?>> progress )
        {
            _progressHandler   = progress;

            ExcelManager.ExcelGenerateEventExt += ( parameters, rowCount ) => ReportProgress( rowCount: rowCount, exportExcelParameters: parameters );
        }

        protected List<ExcelMergeCell> GetExcelRowMergeCells( int beginindex, int endIndex, int rowBegin, int rowEnd )
        {
            var mergeCell = new List<ExcelMergeCell>();
            for ( int i = rowBegin; i <= rowEnd; i++ )
            {
                mergeCell.Add( GetExcelRowMergeCell( beginindex, endIndex, i ) );
            }
            return mergeCell;
        }

        private ExcelMergeCell GetExcelRowMergeCell( int beginIndex, int endIndex, int numRowMerge = 1 )
        {
            List<string> excelColumnNames = ReportHelper.GetExcelColumnNames();
            ExcelMergeCell mergeCell = new ExcelMergeCell();
            mergeCell.BeginCell = string.Format( "{0}{1}", excelColumnNames[beginIndex], numRowMerge );
            mergeCell.EndCell = string.Format( "{0}{1}", excelColumnNames[endIndex], numRowMerge );
            return mergeCell;
        }

        /// <summary> Copies visible columns in new ones, for example, to do not share originals with other thread. </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ColumnBase> CopyVisibleColumnsOnly( IReadOnlyCollection<ColumnBase> columns )
        {
            return ReportHelper.CopyVisibleColumnsOnly( columns );
        }

        protected void BeginReportBuild( GraphTreeNodeReportData reportData, string reportFileFullSpec )
        {
            if ( reportData == null )
                throw new ArgumentNullException( "reportData" );
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.EndContractBlock();

            if ( null != Interlocked.CompareExchange( ref _reportData, reportData, null ) )
                throw new InvalidOperationException( string.Format( "Other build has not completed yet for INN: {0} KPP: {1}", _reportData.TaxPayerId.Inn, _reportData.TaxPayerId.Kpp ) );

            Contract.Assume( _targetFileFullSpec == null );

            _targetFileFullSpec = reportFileFullSpec;

            _profilingEntry = ProfilingHelper.InitTracing( "Dependency Tree's report building", Constants.Instrumentation.DEPENDENCYTREEREPORT_PROFILING_PROFILE_TYPE_NAME, 
                                                           InstrumentationLevel.Troubleshooting );
        }

        protected void Process( GraphNodesKeyParameters nodesKeyParameters, IEnumerable<GraphDataNode> nodeRows, CancellationToken cancelToken )
        {
            Contract.Requires( nodeRows != null );
            Contract.Ensures( _reportRowEnumerator != null || cancelToken.IsCancellationRequested );
            Contract.Ensures( _currentNewRowIndexOnSheet >= 0 || cancelToken.IsCancellationRequested );
            Contract.Ensures( _mergeCells != null || cancelToken.IsCancellationRequested );
            Contract.Ensures( _reportColumns != null );

            bool beginReportBuilding = _reportColumns == null;
            try
            {
                if ( beginReportBuilding )
                {
                    Contract.Assume( nodesKeyParameters != null );
                    Contract.Assume( _reportColumns == null );
                    Contract.Assume( _currentNewRowIndexOnSheet < 0 );
                    Contract.Assume( _mergeCells == null );

                    _reportColumns = DefineColumns( _reportData, nodesKeyParameters );

                    if ( !cancelToken.IsCancellationRequested )
                    {
                        PrepareTempFile();

                        Contract.Assume( _tempFileFullSpec != null );

                        _columnsWidths = new List<double>(16);
                        DataTable dtRows = CreateExcelTable( _reportColumns, _columnsWidths );

                        //if ( !cancelToken.IsCancellationRequested )   //commneted to do not interrupt temporary file creation by user cancellation. So '_tempFileFullSpec != null' means that the temporary file is created
                        {
                            ExcelManager.CreateExcel( _tempFileFullSpec, string.Format( ReportSheetNameText, 1 ), _columnsWidths ); //the fist sheet # 1 will be added

                            if ( !cancelToken.IsCancellationRequested )
                            {
                                ReportHelper.AddHeaderForExcel( dtRows, _reportColumns, isObjectNumbers: false );
                                ReportHelper.GenerateColumnsFormat( _reportColumns );

#if !SEPARATEBYSHEETS                                
                                _spreadsheetDocument = SpreadsheetDocument.Open( _tempFileFullSpec, isEditable: true );

                                OnBeginReadWriteSheet( _spreadsheetDocument, _columnsWidths, 
                                                       isFirstSheet: true, cancelToken: cancelToken );

                                _mergeCells = new List<ExcelMergeCell>( 16 );
                                _currentNewRowIndexOnSheet = 0; //the first row on sheet will have an index 0
#endif //!SEPARATEBYSHEETS                                
                            }
                        }
                    }
                }
                bool isFirstPart = beginReportBuilding;
                int startCountValue = 0, sheetSize = RowsNumberPerSheetLimit;
                //at the first block: '_reportAllRowsFetchedCount == startCountValue'
                //at the non first blcok: '_reportAllRowsFetchedCount == ( sheetSize + 1 ) + N * _reportRowEnumerator.BlockSize + startCountValue'
                if ( !isFirstPart )
                    startCountValue = _reportAllRowsFetchedCount < sheetSize + 1
                        ? (int)_reportAllRowsFetchedCount
                        : (int)( ( _reportAllRowsFetchedCount - sheetSize - 1 ) % _reportRowEnumerator.BlockSize );
                int startCountLimit = isFirstPart || _reportAllRowsFetchedCount < sheetSize + 1    //the root node does not have a row in Excel table and only has datas in the report caption. So the first sheet has size: 'sheetSize + 1' and does not have 'sheetSize'
                                      ? sheetSize + 1 : sheetSize;

                _reportRowEnumerator = new CountLimitedEnumerator<GraphDataNode>( 
                                        nodeRows, startCountValue, startCountLimit, startBlockSize: sheetSize );

                while ( !cancelToken.IsCancellationRequested 
                    && ReadWrite( _reportRowEnumerator, isFirstPart, cancelToken ) )
                {
                    isFirstPart = false;
                    _reportRowEnumerator.CountLimit += _reportRowEnumerator.BlockSize;
                }
                _reportAllRowsFetchedCount += _reportRowEnumerator.Count - startCountValue;
            }
            catch ( Exception exc )
            {
                _clientLogger.LogError( exc );

                throw;
            }
        }

        /// <summary> Does a reading|writing iteration. </summary>
        /// <param name="reportRowEnumerator"></param>
        /// <param name="isFirstPart"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        private bool ReadWrite( CountLimitedEnumerator<GraphDataNode> reportRowEnumerator, bool isFirstPart, CancellationToken cancelToken )
        {
            Dictionary<long, ExcelRowStyle> partRowStyles = new Dictionary<long, ExcelRowStyle>( 128 );
            bool isStartNewSheet = _dataTableExcel == null;
            if ( isStartNewSheet )
            {
#if SEPARATEBYSHEETS

                if ( _spreadsheetDocument != null )
                {
                    Worksheet worksheet = OnEndReadWriteSheet( cancelToken );
                    if ( !cancelToken.IsCancellationRequested )
                    {
                        worksheet.Save();

                        if ( !cancelToken.IsCancellationRequested )
                            ExcelManagerBase.MergeMergeCells( worksheet, _mergeCells );
                    }
                    using ( _spreadsheetDocument ) { _spreadsheetDocument = null; }
                }
                _mergeCells = new List<ExcelMergeCell>( 16 );
                _currentNewRowIndexOnSheet = 0; //the first row on sheet will have an index 0
            }
            bool isNotTableHeader = !isStartNewSheet;

#else //!SEPARATEBYSHEETS

            }
            bool isNotTableHeader = !isFirstPart;

#endif //SEPARATEBYSHEETS

            int lastAddedBlockLevel = 1;
            bool isNeededToContinue = false;

            if ( !cancelToken.IsCancellationRequested )
            {
                _dataTableExcel = GetSequencePartExcelData( 
                    _reportColumns, reportRowEnumerator, ref lastAddedBlockLevel, out _preparedDataRows, isFirstPart, 
                    isNotTableHeader, partRowStyles, _mergeCells, cancelToken );

                if ( _dataTableExcel != null && !cancelToken.IsCancellationRequested )    //'dataTable == null' if 'nodeRows.Count <= 0' (that is common situation) or if there were not any data items in 'reportRowEnumerator'
                {
                    _excelManager.SetupRowStyle( partRowStyles );

                    _currentNewRowIndexOnSheet += OnFillExcelTableData( 
                        _reportColumns, _currentNewRowIndexOnSheet, _preparedDataRows, _dataTableExcel, partRowStyles );

                    if ( !cancelToken.IsCancellationRequested )
                    {
    #if SEPARATEBYSHEETS

                        if ( isStartNewSheet )
                        {
                            _spreadsheetDocument = SpreadsheetDocument.Open( _tempFileFullSpec, isEditable: true );

                            if ( !cancelToken.IsCancellationRequested )
                                OnBeginReadWriteSheet( _spreadsheetDocument, _columnsWidths,
                                    isFirstSheet: isFirstPart, cancelToken: cancelToken );
                        }
    #endif //SEPARATEBYSHEETS

                        if ( !cancelToken.IsCancellationRequested )
                            OnWriteRows( _dataTableExcel, rowStartIndex: int.MinValue, cancelToken: cancelToken );
                    }
                }
                isNeededToContinue = reportRowEnumerator.IsCountLimitReached;
                if ( isNeededToContinue )
                {
                    _preparedDataRows = null;    _dataTableExcel = null;   //signal to open a new sheet on the next call of this method
                }
            }
            return isNeededToContinue;
        }

        protected string FinishProcessing( Exception exception = null, CancellationToken cancelToken = default(CancellationToken) )
        {
            string createdFileFullSpec = null;
            bool isTempFileCreated = _tempFileFullSpec != null;
            string tempFileFullSpec = _tempFileFullSpec;
            string targetFileFullSpec = _targetFileFullSpec;
            try
            {
                try
                {
                    if ( exception == null && !cancelToken.IsCancellationRequested 
                         && _spreadsheetDocument != null && !IsDeinitializationAndSavingNotNeeded() )
                    {
                        Worksheet worksheet = OnEndReadWriteSheet( cancelToken );
                        if ( !cancelToken.IsCancellationRequested )
                            ExcelManagerBase.MergeMergeCells( worksheet, _mergeCells );
                    }
                }
                catch ( Exception exc )
                {
                    if ( exception == null )
                        exception = exc;

                    throw;
                }
                finally
                {
                    EndReportBuild( exception );
                }
            }
            catch ( Exception exc )
            {
                if ( isTempFileCreated )
                {
                    if ( exception == null )
                        exception = exc;

                    MoveOrDeleteTempFile( tempFileFullSpec, targetFileFullSpec, exception: exception );
                }
                throw;
            }
            createdFileFullSpec = MoveOrDeleteTempFile( tempFileFullSpec, targetFileFullSpec, cancelToken, exception );

            return createdFileFullSpec;
        }

        /// <summary> </summary>
        /// <param name="exception"> Not 'null' if some exception is caught. </param>
        protected void EndReportBuild( Exception exception )
        {
            Contract.Ensures( _mergeCells == null );
            Contract.Ensures( _reportColumns == null );
            Contract.Ensures( _columnsWidths == null );

            if ( null == Interlocked.Exchange( ref _reportData, null ) )
                throw new InvalidOperationException( "Unexpected situation: report building flag has been reset from somewhere during the current build" );

            //clearing on finish
            _dataTableExcel             = null;
            _preparedDataRows           = null;
            _reportRowEnumerator        = null;
            _reportAllRowsFetchedCount  = 0;
            _currentNewRowIndexOnSheet  = int.MinValue;
            _mergeCells                 = null;
            _reportColumns              = null;
            _columnsWidths              = null;
            _targetFileFullSpec         = null;
            _tempFileFullSpec           = null;

            using ( _spreadsheetDocument ) { _spreadsheetDocument = null; }

            if ( exception == null )
            {
                ProfilingHelper.TracePoint( _profilingEntry, "Report built" );
                ProfilingHelper.WriteTracing( _profilingEntry );
            }
        }

        /// <summary> Is the report building now? </summary>
        /// <returns> 'true' if the report is building now otherwise 'false'. </returns>
        protected bool IsBuildingReport()
        {
            return null != Interlocked.CompareExchange( ref _reportData, null, null );  //reads only a value of '_reportData'
        }

        private void OnReportDataLoaded()
        {
            ProfilingHelper.TracePoint( _profilingEntry, "Report data loaded" );
        }

        public string BuildReportFile( 
            Func<IReadOnlyCollection<ColumnBase>, IEnumerable<GraphDataNode>, CancellationToken, string> reportFileHandler, 
            string reportFileFullSpec, IReadOnlyCollection<ColumnBase> reportColumns, IEnumerable<GraphDataNode> reportRows, 
            CancellationToken cancelToken )
        {
            Contract.Ensures( Contract.Result<string>() == null || !string.IsNullOrWhiteSpace( Contract.Result<string>() ) );
            Contract.Assume( _reportData != null );

            string createdFileFullSpec = null;
            Exception exception = null;
            try
            {
                createdFileFullSpec = reportFileHandler( reportColumns, reportRows, cancelToken );
            }
            catch ( Exception exc )
            {
                exception = exc;
                _clientLogger.LogError( exception );

                throw;
            }
            finally
            {
                EndReportBuild( exception );
            }
            return createdFileFullSpec;
        }

        public string BuildReportFile( 
            Func<IReadOnlyCollection<GraphDataNode>, CancellationToken, GraphNodesKeyParameters, string> reportFileHandler, 
            IReadOnlyCollection<GraphDataNode> reportRows, GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken )
        {
            Contract.Ensures( Contract.Result<string>() == null || !string.IsNullOrWhiteSpace( Contract.Result<string>() ) );
            Contract.Assume( _reportData != null );

            string createdFileFullSpec = null;
            Exception exception = null;
            try
            {
                createdFileFullSpec = reportFileHandler( reportRows, cancelToken, nodesKeyParameters );
            }
            catch ( Exception exc )
            {
                exception = exc;
                _clientLogger.LogError( exception );

                throw;
            }
            finally
            {
                EndReportBuild( exception );
            }
            return createdFileFullSpec;
        }

        /// <summary> Used in GraphTreeViewwer only. </summary>
        /// <param name="reportFileHandler"></param>
        /// <param name="dataProviderHandler"></param>
        /// <param name="nodesKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        public string BuildReportFile( 
            Func<Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>>, GraphNodesKeyParameters, CancellationToken, string> reportFileHandler, 
            Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>> dataProviderHandler, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken )
        {
            Contract.Ensures( Contract.Result<string>() == null || !string.IsNullOrWhiteSpace( Contract.Result<string>() ) );
            Contract.Assume( _reportData != null );

            string createdFileFullSpec = null;
            Exception exception = null;
            try
            {
                createdFileFullSpec = reportFileHandler( dataProviderHandler, nodesKeyParameters, cancelToken );
            }
            catch ( Exception exc )
            {
                exception = exc;
                _clientLogger.LogError( exception );

                throw;
            }
            finally
            {
                EndReportBuild( exception );
            }
            return createdFileFullSpec;
        }

        public string LoadDataAndCreateReportFile( Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>> dataProviderHandler, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken )
        {
            Contract.Assume( _reportData != null );

            ICollection<GraphDataNode> reportRows = dataProviderHandler(nodesKeyParameters, cancelToken);

            string createdFileFullSpec = null;
            if ( reportRows.Count > 0 && !cancelToken.IsCancellationRequested )
            {
                OnReportDataLoaded();

                IReadOnlyCollection<ColumnBase> reportColumns = DefineColumns( _reportData, nodesKeyParameters );

                createdFileFullSpec = RemoveTempAndCreateReportFile( reportColumns, reportRows, cancelToken );
            }
            return createdFileFullSpec;
        }

        public string DefineColumnsAndCreateReportFile( 
            IReadOnlyCollection<GraphDataNode> reportRows, CancellationToken cancelToken, GraphNodesKeyParameters nodesKeyParameters = null )
        {
            Contract.Assume( _reportData != null );

            string createdFileFullSpec = null;
            if ( reportRows.Count > 0 && !cancelToken.IsCancellationRequested )
            {
                IReadOnlyCollection<ColumnBase> reportColumns = DefineColumns( _reportData, nodesKeyParameters );

                createdFileFullSpec = RemoveTempAndCreateReportFile( reportColumns, reportRows, cancelToken );
            }
            return createdFileFullSpec;
        }

        public string RemoveTempAndCreateReportFile( IReadOnlyCollection<ColumnBase> reportColumns, IEnumerable<GraphDataNode> reportRows, CancellationToken cancelToken )
        {
            Contract.Ensures( _reportColumns != null );
            Contract.Ensures( Contract.Result<string>() == null || !string.IsNullOrWhiteSpace( Contract.Result<string>() ) );
            Contract.Assume( _reportColumns == null );

            _reportColumns = reportColumns.ToReadOnly();
            PrepareTempFile();

            string createdFileFullSpec = null;
            try
            {
                if ( !cancelToken.IsCancellationRequested )
                    BuildReport( reportRows, cancelToken );
            }
            catch ( Exception exc )
            {
                createdFileFullSpec = MoveOrDeleteTempFile( _tempFileFullSpec, _targetFileFullSpec, exception: exc );

                throw;
            }
            createdFileFullSpec = MoveOrDeleteTempFile( _tempFileFullSpec, _targetFileFullSpec, cancelToken );

            return createdFileFullSpec;
        }

        private void PrepareTempFile()
        {
            _tempFileFullSpec = GenerateExcelFileNameTemp();
            FileHelper.FileDeleteIfExists( _tempFileFullSpec ); //apopov 27.1.2016	//is it really needed?
        }

        protected string MoveOrDeleteTempFile( 
            string tempFileFullSpec, string targetFileFullSpec, CancellationToken cancelToken = default(CancellationToken), Exception exception = null )
        {
            string createdFileFullSpec = null;

            if ( exception != null || cancelToken.IsCancellationRequested )
            {
                FileHelper.FileDeleteIfExists( tempFileFullSpec );
            }
            else
            {
                File.Copy( tempFileFullSpec, targetFileFullSpec, overwrite: true );
                createdFileFullSpec = targetFileFullSpec;
            }
            return createdFileFullSpec;
        }

        private void BuildReport( IEnumerable<GraphDataNode> reportRows, CancellationToken cancelToken )
        {
            var columnsWidths = new List<double>(16);
            DataTable dtRows = CreateExcelTable( _reportColumns, columnsWidths );

            if ( !cancelToken.IsCancellationRequested )
                CreateAndFillReport( dtRows, reportRows, columnsWidths, string.Format( ReportSheetNameText, 1 ), cancelToken ); //only one sheet #1
        }

        private void CreateAndFillReport( 
            DataTable dtRows, IEnumerable<GraphDataNode> reportRows, List<double> columnsWidths, string sheetName, CancellationToken cancelToken )
        {
            ExcelManager.CreateExcel( _tempFileFullSpec, sheetName, columnsWidths );

            if ( !cancelToken.IsCancellationRequested )
            {
                ReportHelper.AddHeaderForExcel( dtRows, _reportColumns, isObjectNumbers: false );

                if ( !cancelToken.IsCancellationRequested )
	                FillReport( _tempFileFullSpec, reportRows, columnsWidths, cancelToken );
            }
        }

        /// <summary> </summary>
        /// <param name="reportTempFileFullSpec"></param>
        /// <param name="reportRows"></param>
        /// <param name="columnsWidths"></param>
        /// <param name="cancelToken"></param>
        /// <returns> A number of row added. </returns>
        private void FillReport( 
            string reportTempFileFullSpec, IEnumerable<GraphDataNode> reportRows, List<double> columnsWidths, CancellationToken cancelToken )
        {
            Contract.Requires( _mergeCells == null );
            Contract.Ensures( _mergeCells != null );

            IEnumerator<GraphDataNode> reportRowEnumerator = reportRows.GetEnumerator();
            GraphDataNode lastGraphData = null;
            _mergeCells = new List<ExcelMergeCell>( 16 );
            int rowCountCurrent = 0, lastAddedBlockLevel = 1;

            Func<int, DataTable> processPageBlockHandler = pageNumber => GetBlockExcelData( 
                _reportColumns, reportRowEnumerator, ref lastGraphData, ref lastAddedBlockLevel, pageNumber, 
                ref rowCountCurrent, _mergeCells );

            ReportHelper.GenerateColumnsFormat( _reportColumns );

            using ( _spreadsheetDocument = SpreadsheetDocument.Open( reportTempFileFullSpec, true ) )
            {
	            Worksheet worksheet = OnFillWorkbook( 
                    _spreadsheetDocument, reportTempFileFullSpec, processPageBlockHandler, columnsWidths, cancelToken );

                if ( !cancelToken.IsCancellationRequested )
                    ExcelManagerBase.MergeMergeCells( worksheet, _mergeCells );

                _spreadsheetDocument = null;
            }
        }

        /// <summary> Generates a file name with an extension like: "2333444555(продавец)-1кв.2016_20160413_17-21.xlsx". </summary>
        public string GenerateExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return string.Concat( GenerateExcelFileNameWOExtension( treeTableData ), ReportExcelFileExtensionName );
        }

        private string GenerateExcelFileNameTemp()
        {
            string basePath = Path.GetTempPath();
            string fileNameWOExtension = GenerateExcelFileNameWOExtension( _reportData );
            string fullFileSpec = Path.Combine( 
                basePath, string.Concat( Path.GetFileNameWithoutExtension( fileNameWOExtension ), "_temp", ReportExcelFileExtensionName ) );

            return fullFileSpec;
        }

        /// <summary> Generates a file name without an extension like: "2333444555(продавец)-1кв.2016_20160413_17-21". </summary>
        /// <param name="treeTableData"></param>
        /// <returns></returns>
        public string GenerateExcelFileNameWOExtension( GraphTreeNodeReportData treeTableData )
        {
            Contract.Requires( treeTableData != null );
            Contract.Ensures( !string.IsNullOrWhiteSpace( Contract.Result<string>() ) );

            DateTime dtNow = DateTime.Now;
            StringBuilder sbFileName = new StringBuilder( 64 );

            // "<ИНН>(покупатель)-<отчетный.период>_ГГГГММДД_ЧЧ-ММ" for example: "2333444555(продавец)-1кв.2016_20160413_17-21"
            sbFileName.Append( treeTableData.TaxPayerId.Inn ).Append( "(" ).Append( treeTableData.IsPurchase ? ReportExcelFilePartIsPurchase : ReportExcelFilePartIsSeller )
                .Append( ")-" ).Append( treeTableData.Period.ToDelimiterFreeString() ).Append( "_" ).Append( dtNow.Year.ToString( "0000", CultureInfo.InvariantCulture ) )
                .Append( dtNow.Month.ToString( "00", CultureInfo.InvariantCulture ) ).Append( dtNow.Day.ToString( "00", CultureInfo.InvariantCulture ) ).Append( "_" )
                .Append( dtNow.Hour.ToString( "00", CultureInfo.InvariantCulture ) ).Append( "-" ).Append( dtNow.Minute.ToString( "00", CultureInfo.InvariantCulture ) );

            string fileNameWOExtension = sbFileName.ToString();

            return fileNameWOExtension;
        }

        protected DataTable GetBlockExcelData( IReadOnlyCollection<ColumnBase> reportColumns, IEnumerator<GraphDataNode> reportRowEnumerator, 
            ref GraphDataNode lastGraphData, ref int lastAddedBlockLevel, int page, ref int rowCountCurrent, List<ExcelMergeCell> mergeCells )
        {
            DataTable dataTable = null;
            GraphDataNode parentGraphData;

            List<Dictionary<string, object>> dataRows = 
                GetBlockExcelRows( reportRowEnumerator, ref lastGraphData, page, out parentGraphData );
            if ( dataRows != null )
            {
                dataTable = CreateExcelTable( reportColumns );

                Dictionary<long, ExcelRowStyle> pageRowStyles = new Dictionary<long, ExcelRowStyle>();
                List<long> pageHeaderRowIds = new List<long>();

                rowCountCurrent += OnFillBlockExcelData( 
                    _reportData, reportColumns, ref lastAddedBlockLevel, rowCountCurrent, mergeCells, parentGraphData, dataRows, 
                    dataTable, isFirstBlock: page == 0, rowStyles: pageRowStyles, headerRowIds: pageHeaderRowIds );

                rowCountCurrent += OnFillExcelTableData( reportColumns, rowCountCurrent, dataRows, dataTable, pageRowStyles );

                _excelManager.SetupHeaderRows( pageHeaderRowIds );
                _excelManager.SetupRowStyle( pageRowStyles );
            }
            return dataTable;
        }

        private List<Dictionary<string, object>> GetBlockExcelRows(
            IEnumerator<GraphDataNode> reportRowEnumerator, ref GraphDataNode lastGraphData, int page, out GraphDataNode parentGraphData)
        {
            parentGraphData = null;

            bool nextPresent = false;
            List<Dictionary<string, object>> excelRows = null;
            List<GraphDataNode> blockRows = new List<GraphDataNode>(16);
            if ( page == 0 )    //start of report datas
            {
                nextPresent = reportRowEnumerator.MoveNext();
                if ( nextPresent )
                {
                    parentGraphData = reportRowEnumerator.Current;  //the root node
                    if ( _considerRootAsLineToo )   //adds the root node as the first row of the first page too
                        blockRows.Add( parentGraphData );
                }
            }
            else if ( lastGraphData != null )  //the last node was not included in the previous block so adds it here
            {
                parentGraphData = lastGraphData.Parent;
                blockRows.Add( lastGraphData );
            }
            //else 'lastGraphData == null' //end of report datas on the previous block's pass
            if ( lastGraphData != null || nextPresent )
            {
                // ReSharper disable once AssignmentInConditionalExpression
                while ( nextPresent = reportRowEnumerator.MoveNext() )
                {
                    lastGraphData = reportRowEnumerator.Current;
                    if ( IsParentSuitable( lastGraphData, parentGraphData ) )
                    {
                        blockRows.Add( lastGraphData );
                        lastGraphData = null;
                    }
                    else
                    {
                        break;
                    }
                }
                if ( !nextPresent )
                    lastGraphData = null;
                if ( parentGraphData != null )
                {
                    excelRows = ReportHelper.ConvertToDictionary( blockRows, ConvertToValueDictionary );
                    excelRows = ReportHelper.ConvertToColumnFormat( excelRows, priorityOfExcelFormats: true );
                }
            }
            return excelRows;
        }

        protected DataTable GetSequencePartExcelData( IReadOnlyCollection<ColumnBase> reportColumns, 
            CountLimitedEnumerator<GraphDataNode> reportRowEnumerator, ref int lastAddedBlockLevel, 
            out List<Dictionary<string, object>> dataRows, bool isFirstPart, bool isNotTableHeader, 
            Dictionary<long, ExcelRowStyle> partRowStyles, List<ExcelMergeCell> mergeCells, CancellationToken cancelToken )
        {
            DataTable dataTable = null;
            GraphDataNode parentGraphData;

            dataRows = GetSequencePartExcelRows( reportRowEnumerator, isFirstPart, out parentGraphData );

            if ( dataRows == null && parentGraphData != null )
                dataRows = new List<Dictionary<string, object>>( 0 );

            if ( dataRows != null )
            {
                dataTable = CreateExcelTable( reportColumns );

                if ( !isNotTableHeader )
                {
                    List<long> partHeaderRowIds = new List<long>();

                    _currentNewRowIndexOnSheet += OnFillBlockExcelData( 
                        _reportData, reportColumns, ref lastAddedBlockLevel, _currentNewRowIndexOnSheet, mergeCells, parentGraphData, 
                        dataRows, dataTable, isFirstBlock: isFirstPart, rowStyles: partRowStyles, headerRowIds: partHeaderRowIds );

                    _excelManager.SetupHeaderRows( partHeaderRowIds );
                }
            }
            return dataTable;
        }

        private List<Dictionary<string, object>> GetSequencePartExcelRows( 
            CountLimitedEnumerator<GraphDataNode> reportRowEnumerator, bool isFirstPart, out GraphDataNode parentGraphData )
        {
            parentGraphData = null;

            List<Dictionary<string, object>> excelRows = null;
            List<GraphDataNode> blockRows = new List<GraphDataNode>(16);
            if ( isFirstPart )    //start of report datas
            {
                Contract.Assume( reportRowEnumerator.Count == 0 );

                if ( reportRowEnumerator.GetNext( out parentGraphData ) )   //the root node
                {
                    if ( _considerRootAsLineToo )   //adds the root node as the first row of the first page too
                        blockRows.Add( parentGraphData );
                }
            }
            Contract.Assume( reportRowEnumerator.Count >= 0 );

            GraphDataNode node;
            bool? limitedSuccess;
            while ( ( limitedSuccess = reportRowEnumerator.GetNextLimited( out node ) ).HasValue && limitedSuccess.Value )
            {
                blockRows.Add( node );
            }
            if ( blockRows.Count > 0 )
            {
                excelRows = ReportHelper.ConvertToDictionary( blockRows, ConvertToValueDictionary );
                excelRows = ReportHelper.ConvertToColumnFormat( excelRows, priorityOfExcelFormats: true );
            }

            return excelRows;
        }

        protected virtual bool IsParentSuitable(GraphDataNode lastGraphData, GraphDataNode parentGraphData)
        {
	//apopov 12.4.2016	//DEBUG!!!
            return true;
            //return lastGraphData.Parent == null   //for tree plain report without assigning GraphData.Parent
            //       || object.ReferenceEquals( lastGraphData.Parent, parentGraphData );
        }

        /// <summary> Converts base properties' values of <see cref="entity"/> to a value dictionary. </summary>
        /// <param name="graphData/param>
        /// <returns></returns>
        protected virtual Dictionary<string, object> ConvertToValueDictionary( GraphDataNode graphData )
        {
            IDictionary<string, object> row = new Dictionary<string, object>( 16 );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.NotMappedAmnt, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.ClientNdsPercentage, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.SurDescription, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.DeclarationType, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.MappedAmount, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.CalcNds, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.DeductionNds, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.VatTotal, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.GapDiscrepancyTotal, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.DiscrepancySumAmnt, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.SellerNdsPercentage, graphData));
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.DiscrepancySumAmntTotal, graphData));

            return (Dictionary<string, object>)row;
        }

        protected int AddBStandartBlockHeader(string reportType, string reportPeriod, GraphDataNode parentGraphData, DataTable dataTable)
        {
            int rowCountOnStart = dataTable.Rows.Count;

            AddHeaderItemPair( dataTable, "Данные исходного НП" );
            AddHeaderItemPair( dataTable, "ИНН",                                                parentGraphData.Inn );
            AddHeaderItemPair( dataTable, "Наименование",                                       parentGraphData.Name );
            AddHeaderItemPair( dataTable, "Отчетный период",                                    reportPeriod );
            AddHeaderItemPair( dataTable, "Регион",                                             parentGraphData.RegionName );
            AddHeaderItemPair( dataTable, "Инспекция",                                          parentGraphData.Inspection.EntryValue );
            AddHeaderItemPair( dataTable, "Тип отчета",                                         reportType );
            AddHeaderItemPair( dataTable, "Сумма НДС, подлежащая вычету у исходного НП, ВСЕГО", parentGraphData.DeductionNds );
            AddHeaderItemPair( dataTable, "НДС исчисленный исходным НП, ВСЕГО",                 parentGraphData.CalcNds );
            AddHeaderItemPair( dataTable, "Доля вычетов у исходного НП",                        parentGraphData.NdsPercentage);
            AddHeaderItemPair( dataTable, "СУР",                                                parentGraphData.SurDescription );
            AddHeaderItemPair( dataTable, "Крупнейший",                                         KppCodeHelper.IsLargeTaxPayerByKpp( parentGraphData.Kpp ) );
            AddHeaderItemPair( dataTable, "Признак НД",                                         parentGraphData.DeclarationType );

            return dataTable.Rows.Count - rowCountOnStart;
        }

        protected static void AddHeaderItemPair( DataTable dataTable, string caption, object value = null, string excelFormatString = null )
        {
            object newValue;
            if ( excelFormatString == null )
            {
                if ( !ReportGeneralHelper.ConvertForCommonTypesToExcel( value, out newValue ).HasValue )
                    newValue = value;
            }
            else
            {
                newValue = ReportGeneralHelper.ConvertToFormatToExcel( value, ColumnFormats.FormatLong );
            }
            dataTable.Rows.Add( caption, null, null, null, newValue );
        }

        protected void RegisterStandartBlockHeader( int columnCount, int startRowIndex, int headerHeight, List<ExcelMergeCell> mergeCells )
        {
            Contract.Requires( headerHeight > 6 + 3 );

            //string header part 1
            int startRowNumber = RowNumberFromRowIndex( startRowIndex );
            int finishRowNumber = startRowNumber + 6 - 1;
            int lastRowNumber = startRowNumber + headerHeight - 1;

            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 0, endIndex: 3, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );
            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 4, endIndex: columnCount - 1, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );

            //numeric header part 2
            startRowNumber = finishRowNumber + 1;
            finishRowNumber = startRowNumber + 3 - 1;

            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 0, endIndex: 3, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );
            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 5, endIndex: columnCount - 1, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );

            //string header part 3
            startRowNumber = finishRowNumber + 1;
            finishRowNumber = lastRowNumber;

            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 0, endIndex: 3, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );
            mergeCells.AddRange( GetExcelRowMergeCells( 
                beginindex: 4, endIndex: columnCount - 1, rowBegin: startRowNumber, rowEnd: finishRowNumber ) );
        }

        protected void RegisterRowAsFullHeaderLine( int columnCount, int startRowIndex, List<ExcelMergeCell> mergeCells, List<long> headerRowIds )
        {
            headerRowIds.AddRange( new long[] { startRowIndex } );

            int rowNumber = RowNumberFromRowIndex( startRowIndex );
            mergeCells.AddRange( GetExcelRowMergeCells( beginindex: 0, endIndex: columnCount - 1, rowBegin: rowNumber, rowEnd: rowNumber ) );
        }

        protected int AddBlockTableHeader( IReadOnlyCollection<ColumnBase> reportColumns, DataTable dataTable, bool addEmptyLineUpHeader = false )
        {
            int rowCountOnStart = dataTable.Rows.Count;

            if ( addEmptyLineUpHeader )
                dataTable.Rows.Add();
            ReportHelper.AddHeaderForExcel( dataTable, reportColumns, isObjectNumbers: false );

            return dataTable.Rows.Count - rowCountOnStart;
        }

        protected int AddBlockTableData( IReadOnlyCollection<ColumnBase> reportColumns, List<Dictionary<string, object>> dataRows, DataTable dataTable )
        {
            foreach ( Dictionary<string, object> dicRowData in dataRows )
            {
                var objs = new List<object>();
                foreach ( ColumnDefinition columnDefinition in new ColumnDefinitionIterator( reportColumns ) )
                {
                    object objAddedUseless;
                    if ( !ReportHelper.AddRowItem( columnDefinition, objs, dicRowData, out objAddedUseless ) )
                        //apopov 29.1.2016	//TODO!!!   //remove empty data item adding when all data will present
                        objs.Add( item: null );
                }
                dataTable.Rows.Add( objs.ToArray() );
            }
            return dataRows.Count;
        }

        private DataTable CreateExcelTable( IEnumerable<ColumnBase> columns, List<double> columnsWidth = null )
        {
            DataTable dtRows = new DataTable("dtRows");

            foreach ( ColumnDefinition columnDefinition in new ColumnDefinitionIterator( columns ) )
            {
                DataColumn dataColumn = dtRows.Columns.Add( columnDefinition.Key, typeof(object) );
                dataColumn.ReadOnly = true;
                dataColumn.Caption = columnDefinition.Caption;
                if ( columnsWidth != null )
                    AddColumnsWidth( columnDefinition, columnsWidth );
            }
            return dtRows;
        }

        protected void ReportProgress( bool? isVisible = null, long? rowCount = null, ExportExcelParam exportExcelParameters = null )
        {
            if ( _progressHandler != null )
                _progressHandler.Report( Tuple.Create( rowCount, isVisible ) );
        }

        protected int RowNumberFromRowIndex( int rowIndex )
        {
            return rowIndex + 1;
        }
    }
}