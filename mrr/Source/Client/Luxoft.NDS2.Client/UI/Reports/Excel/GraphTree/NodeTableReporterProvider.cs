﻿using System.Diagnostics.Contracts;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeTableReporterProvider : IInstanceProvider<INodeTableReporter>
    {
        private readonly IClientLogger _clientLogger;

        private NodeTableReporter _nodeTableReporter = null;

        public NodeTableReporterProvider( IClientLogger clientLogger )
        {
            Contract.Requires( clientLogger != null );

            _clientLogger = clientLogger;
        }

        /// <summary> A provided instance. The provider does not guarantee that returns the same instance on each call. </summary>
        public INodeTableReporter Instance
        {
            get
            {
                if ( _nodeTableReporter == null )
                    _nodeTableReporter = new NodeTableReporter( _clientLogger );

                return _nodeTableReporter;
            }
        }
    }
}