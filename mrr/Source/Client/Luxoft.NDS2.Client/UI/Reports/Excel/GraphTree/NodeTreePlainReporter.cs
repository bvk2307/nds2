﻿//#define ERRRORLOG
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.Lib.Exceptions;
using Luxoft.NDS2.Client.Config.GraphTree;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.ExportExcel;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Helpers.Executors;
using Luxoft.NDS2.Common.Helpers.Serialization;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeTreePlainReporter : BaseNodeReporter<NodeReportManager>, INodeAllTreeReporter, INodeVisibleTreeReporter
    {
        #region Private members

        private const string ReportItemDelimiter = @"/";

        private static readonly SequentStringParser s_stringParser = 
            new SequentStringParser( recordDelimiters: new[] { "^|" }, recordItemDelimiters: new [] { "^~" } );

        private readonly int _rowsNumberPerSheetLimit;

        private bool _useParentInsteadOfContractorSpec; 
        private GridSetupHelper<GraphDataNode> _columnSetupHelper = null;
        private string _columnNameDiscrepancySumAmnt = null;
        private string _columnNameDiscrepancySumAmntTotal = null;

        #endregion Private members

        public NodeTreePlainReporter( IClientLogger clientLogger, GraphTreeReportSettings reportSettings = null ) 
            : base( new NodeReportManager( reportSettings == null ? 0 : reportSettings.SheetMaxDataRowsLimit ), 
                    clientLogger, considerRootAsLineToo: false )
        {
            ExcelManager.ExportExcelParam = new ExportExcelParam();
            _rowsNumberPerSheetLimit = ExcelManager.RowsNumberPerSheetLimit;
        }

        public void StartReportBuild( string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData,
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportData != null );
            Contract.Requires( nodesKeyParameters != null );

            BeginReportBuild( reportData, reportFileFullSpec );

            _useParentInsteadOfContractorSpec = true;  //uses GraphData.Parent and does not use GraphData.ContractorSpecificationChain

            ReportProgress( isVisible: true );
        }

        public void Process( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> callExecContext, 
                             CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( callExecContext != null );

            GraphNodesKeyParameters keyParameters = callExecContext.Parameters;
            IEnumerable<GraphDataNode> nodes = callExecContext.Result;

//            //apopov 5.5.2016	//DEBUG!!!
//#if DEBUG
//            StringBuilder sb = new StringBuilder( 128 );
//            sb.Append( ">>> Nodes with INNs will be processed:" );
//            foreach ( GraphDataNode graphDataNode in nodes )
//            {
//                sb.Append( string.Concat( " ", graphDataNode.Inn ) );
//            }
//            Debug.WriteLine( sb.ToString() );
//#endif //DEBUG

            Process( keyParameters, nodes, cancelToken );
        }

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="exception"> Not 'null' if some exception is caught. </param>
        /// <returns> A created target report file full specification or 'null' if it is not created on error or cancelling.  </returns>
        public string FinishReportBuild( CancellationToken cancelToken = default(CancellationToken), Exception exception = null )
        {
            bool isExcelManagerFinishNeeded = !IsDeinitializationAndSavingNotNeeded();

            string createdFileFullSpec = FinishProcessing( exception, cancelToken );

            if ( isExcelManagerFinishNeeded ) //otherwise ignores call of FinishProcessing() in suppose that report read|write initialization has not been called by an error
                ExcelManager.FinishReadWrite();

            return createdFileFullSpec;
        }

        public Task<string> StartReportBuildAsync( string reportFileFullSpec, GraphTreeNodeReportData reportData,
            Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>> dataProviderHandler, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportData != null );
            Contract.Requires( dataProviderHandler != null );
            Contract.Requires( nodesKeyParameters != null );

            BeginReportBuild( reportData, reportFileFullSpec );

            _useParentInsteadOfContractorSpec = false;  //uses GraphData.ContractorSpecificationChain and does not use GraphData.Parent

            ReportProgress( isVisible: true );

            Task<string> taskBuild = Task.Run(
                () => BuildReportFile( LoadDataAndCreateReportFile, dataProviderHandler, nodesKeyParameters, cancelToken ),
                cancelToken );

            return taskBuild;
        }

        public Task<string> StartReportBuildAsync(string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData, IReadOnlyCollection<GraphDataNode> reportRows, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportData != null );
            Contract.Requires( reportRows != null );

            _useParentInsteadOfContractorSpec = true;  //uses GraphData.Parent and does not use GraphData.ContractorSpecificationChain

            BeginReportBuild( reportData, reportFileFullSpec );

            ReportProgress( isVisible: true );

            Task<string> taskBuild = Task.Run(
                () => BuildReportFile( DefineColumnsAndCreateReportFile, reportRows, nodesKeyParameters: null, cancelToken: cancelToken ),
                cancelToken );

            return taskBuild;
        }

        /// <summary> A maximum sheet size as row number. </summary>
        protected override int RowsNumberPerSheetLimit { get { return _rowsNumberPerSheetLimit; } }

        private GridSetupHelper<GraphDataNode> ColumnSetupHelper
        {
            get
            {
                if (_columnSetupHelper == null)
                    _columnSetupHelper = new GridSetupHelper<GraphDataNode>();

                return _columnSetupHelper;
            }
        }

        private string ColumnNameDiscrepancySumAmnt
        {
            get
            {
                if (_columnNameDiscrepancySumAmnt == null)
                    _columnNameDiscrepancySumAmnt = ColumnSetupHelper.GetMemberName(data => data.DiscrepancySumAmnt);

                return _columnNameDiscrepancySumAmnt;
            }
        }

        private string ColumnNameDiscrepancySumAmntTotal
        {
            get
            {
                if (_columnNameDiscrepancySumAmntTotal == null)
                    _columnNameDiscrepancySumAmntTotal = ColumnSetupHelper.GetMemberName(data => data.DiscrepancySumAmntTotal);

                return _columnNameDiscrepancySumAmntTotal;
            }
        }

        protected override IReadOnlyCollection<ColumnBase> DefineColumns( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters )
        {
            Contract.Requires( reportData != null || nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<ColumnBase>>() != null && Contract.Result<IReadOnlyCollection<ColumnBase>>().Count > 0 );

            bool isByPurchase = GetIsByPurchase( reportData, nodesKeyParameters );
            int treeLevelCount = GetTreeLevelCount( reportData, nodesKeyParameters );
            List<ColumnBase> reportColumns = null;
            List<ColumnDefinition> levelColumns;
            if ( isByPurchase )
            {
                reportColumns = new List<ColumnBase>
                {
                    new ColumnGroupDefinition
                    {
                        Caption = "Уровни дерева связей", Columns = levelColumns = new List<ColumnDefinition>( treeLevelCount ) //will be filled below
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации покупателя", Columns = new List<ColumnDefinition>
                        {
                            ColumnSetupHelper.CreateColumnDefinition( data => data.NotMappedAmnt, "Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.ClientNdsPercentage, "Доля вычетов у покупателя по операциям с продавцом" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации продавца", Columns = new List<ColumnDefinition>
                        {
                            ColumnSetupHelper.CreateColumnDefinition( data => data.SurDescription, "СУР" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DeclarationType, "Признак НД" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.MappedAmount, "Сумма НДС, исчисленная продавцом по операциям с покупателем" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.CalcNds, "НДС исчисленный у продавца, ВСЕГО" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DeductionNds, "Сумма НДС, подлежащая вычету у продавца, ВСЕГО" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.VatTotal, "Сумма НДС, исчисленная к уплате (+) или заявленная к возмещению (-)" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DiscrepancySumAmntTotal, ColumnNameDiscrepancySumAmntTotal, "Сумма расхождений по стороне продавца, ВСЕГО", toolTipText: null, initializer: null ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DiscrepancySumAmnt, ColumnNameDiscrepancySumAmnt, "Сумма расхождений у продавца-контрагента по операциям с налогоплательщиком-покупателем", toolTipText: null, initializer: null )
                        }
                    }
                };
            }
            else    //is by seller
            {
                reportColumns = new List<ColumnBase>
                {
                    new ColumnGroupDefinition
                    {
                        Caption = "Уровни дерева связей", Columns = levelColumns = new List<ColumnDefinition>( treeLevelCount ) //will be filled below
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации продавца", Columns = new List<ColumnDefinition>
                        {
                            ColumnSetupHelper.CreateColumnDefinition( data => data.MappedAmount, "Сумма НДС, исчисленная продавцом по операциям с покупателем" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.SellerNdsPercentage, "Доля исчисленного НДС продавцом по операциям с покупателем" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации покупателя", Columns = new List<ColumnDefinition>
                        {
                            ColumnSetupHelper.CreateColumnDefinition( data => data.SurDescription, "СУР" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DeclarationType, "Признак НД" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.NotMappedAmnt, "Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.CalcNds, "НДС исчисленный у покупателя, ВСЕГО" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DeductionNds, "Сумма НДС, подлежащая вычету у покупателя, ВСЕГО" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.VatTotal, "Сумма НДС, исчисленная к уплате (+) или заявленная к возмещению (-)" ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DiscrepancySumAmntTotal, ColumnNameDiscrepancySumAmntTotal, "Сумма расхождений по стороне покупателя, ВСЕГО", toolTipText: null, initializer: null ),
                            ColumnSetupHelper.CreateColumnDefinition( data => data.DiscrepancySumAmnt, ColumnNameDiscrepancySumAmnt, "Сумма расхождений у покупателя-контрагента по операциям с налогоплательщиком-продавцом", toolTipText: null, initializer: null )
                        }
                    },
                };
            }
            for ( int levelIndex = 0; levelIndex < treeLevelCount; levelIndex++ )
            {
                levelColumns.Add( new ColumnDefinition( NodeTreePlainReporter.LevelColumnKey( levelIndex ) )
                                  { Caption = string.Join( " ", "уровень", NumericFormatHelper.ToString( levelIndex ) ), WidthColumnExcel = 22 } );
            }
            return reportColumns.ToReadOnly();
        }

        private bool GetIsByPurchase( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters )
        {
            GraphTreeLimitedLayersNodeReportData limitedLayersTreeNodeReportData = reportData as GraphTreeLimitedLayersNodeReportData;
            bool isByPurchase = limitedLayersTreeNodeReportData == null ? nodesKeyParameters.IsPurchase : limitedLayersTreeNodeReportData.IsPurchase;

            return isByPurchase;
        }

        private int GetTreeLevelCount( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters )
        {  
            GraphTreeLimitedLayersNodeReportData limitedLayersTreeNodeReportData = reportData as GraphTreeLimitedLayersNodeReportData;
            return limitedLayersTreeNodeReportData == null ? nodesKeyParameters.LayersLimit : limitedLayersTreeNodeReportData.LayersLimit; 
        }

        /// <summary> Adds additional columns' datas to the base columns datas from <see cref="BaseNodeReporter{NodeReportManager}.ConvertToValueDictionary"/>. </summary>
        /// <param name="graphData"></param>
        /// <returns></returns>
        protected override Dictionary<string, object> ConvertToValueDictionary( GraphDataNode graphData )
        {
            IDictionary<string, object> row = base.ConvertToValueDictionary( graphData );

            int nI = 0;
            foreach ( ContractorSpecification specification in 
                _useParentInsteadOfContractorSpec ? GetContractorSpecificationsByParents( graphData ) : SplitContractorSpecifications( graphData ) )
            {
                row.Add( NodeTreePlainReporter.LevelColumnKey( nI ), 
                    ContractorLevelSpecification( levelIndex: nI++, contractorSpecification: specification ) );
            }
            return (Dictionary<string, object>)row;
        }

        private string ContractorLevelSpecification( int levelIndex, ContractorSpecification contractorSpecification )
        {
            string specification = string.Join( ReportItemDelimiter, contractorSpecification.Inn, contractorSpecification.Kpp );
            specification = string.Join( " ", specification, contractorSpecification.Name );

            return specification;
        }

        private //static 
            IEnumerable<ContractorSpecification> SplitContractorSpecifications(GraphDataNode graphData)
        {
            IEnumerable<ContractorSpecification> specs = null;

#if ERRRORLOG
            try
            {
#endif //ERRRORLOG

            IEnumerable<string[]> recordsFields = s_stringParser.CreateRecordsIterator( graphData.ContractorSpecificationChain );

            foreach ( string[] fields in recordsFields )
            {
                Contract.Assume( fields != null && 1 <= fields.Length && fields.Length <= 3 );

                ContractorSpecification contractorSpecification;
                try
                {
                    contractorSpecification = new ContractorSpecification { Inn = fields[0], Kpp = fields[1], Name = fields[2] };
                }
                catch ( Exception exc )
                {
                    var sbFields = new StringBuilder( 32 );
                    if ( fields.Length > 0 )
                    {
                        sbFields.Append( "'" ).Append( fields[0] );
                        if ( fields.Length > 1 )
                        {
                            sbFields.Append( "|" ).Append( fields[1] );
                            if ( fields.Length > 2 )
                                sbFields.Append( "|" ).Append( fields[2] );
                        }
                        sbFields.Append( "'" );
                    }
                    var agrs = new ParsingExceptionArgs
                        { FailedRecord = sbFields.ToString(), Source = graphData.ContractorSpecificationChain };

                    throw new GenericException<ParsingExceptionArgs>( agrs, "Parsing error of contractor specification chain", exc );
                }
                yield return contractorSpecification;
            }

#if ERRRORLOG

            specs = specs.ToArray();
            }
            catch ( Exception exc)
            {
                string message = "SPLITTING ERROR:";
                message += "\r\n" + "chain:'" + graphData.ContractorSpecificationChain + "'";
                message += "\r\n" + "INN:'" + graphData.Inn + "'";
                message += "\r\n" + "parent INN:'" + graphData.ParentInn + "'";
                message += "\r\n" + "level:'" + graphData.Level + "'";
                message += "\r\n" + "child order number:'" + graphData.Id + "'";
                message += "\r\n" + exc.ToString();
                _clientLogger.LogError( ex: null, message: message );

                throw;
            }
#endif //ERRRORLOG
        }

        private static IEnumerable<ContractorSpecification> GetContractorSpecificationsByParents( GraphDataNode graphData )
        {
            return from data in graphData.IterateParents( beginFromParent: true )
                   select new ContractorSpecification { Inn = data.Inn, Kpp = data.Kpp, Name = data.Name };
        }

        private static string LevelColumnKey( int levelIndex )
        {
            string levelColumnKey = string.Concat( "ColKeyLevel", NumericFormatHelper.ToString( levelIndex ) );

            return levelColumnKey;
        }

        protected override void AddColumnsWidth( ColumnDefinition columnDef, List<double> columnsWidth )
        {
            if ( columnDef != null && columnDef.WidthColumnExcel != null )
                columnsWidth.Add( (double)columnDef.WidthColumnExcel );
            else
                columnsWidth.Add( 18 );
        }

        /// <summary> Fills the workbook with this one call. Use only <see cref="OnFillWorkbook"/>() or only <see cref="OnWriteRows"/>() at the same report build. </summary>
        /// <param name="document"></param>
        /// <param name="reportTempFileFullSpec"></param>
        /// <param name="processPageBlockHandler"></param>
        /// <param name="columnsWidths"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        protected override Worksheet OnFillWorkbook( SpreadsheetDocument document, string reportTempFileFullSpec, 
            Func<int, DataTable> processPageBlockHandler, List<double> columnsWidths, CancellationToken cancelToken )
        {
            Worksheet worksheet = ExcelManager.FillWorkbook( document, filePath: reportTempFileFullSpec,
                getPageDataHandler: processPageBlockHandler, columnsWidths: columnsWidths, cancelToken: cancelToken, cellStyleSelectior: OnCellStyleSelection );
            return worksheet;
        }

        /// <summary> Inits the workbook before writing by <see cref="OnWriteRows"/>(). Do not use this method with <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        /// <param name="document"></param>
        /// <param name="columnsWidths"></param>
        /// <param name="isFirstSheet"> Is read|write starting for the first sheet? </param>
        /// <param name="cancelToken"></param>
        protected override void OnBeginReadWriteSheet( SpreadsheetDocument document, List<double> columnsWidths, 
            bool isFirstSheet, CancellationToken cancelToken = default(CancellationToken) )
        {
            if ( isFirstSheet )
                ExcelManager.StartReadWrite( document );

            Dictionary<int, string> sheetNames = ExcelManager.ExportExcelParam.SheetNames;
            int sheetNumber = sheetNames.Count + 1;
            sheetNames.Add( sheetNumber, string.Format( ReportSheetNameText, sheetNumber ) );

            ExcelManager.BeginReadWriteSheet( document, columnsWidths ); 

            ExcelManager.BeginReadWriteContent( cancelToken );
        }

        /// <summary> Deinits the workbook after writing by <see cref="OnWriteRows"/>(). Do not use this method with <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        protected override Worksheet OnEndReadWriteSheet( CancellationToken cancelToken = default(CancellationToken) )
        {
            ExcelManager.EndReadWriteContent( cancelToken );
            Worksheet worksheet = ExcelManager.EndReadWriteSheet( cancelToken ); 

            return worksheet;
        }

        /// <summary> Are workbook read|write deinitialization and saving not needed? </summary>
        /// <returns></returns>
        protected override bool IsDeinitializationAndSavingNotNeeded()
        {
            return !ExcelManager.IsReadWriteInProgress();
        }

        /// <summary> Writes rows into the Excel writer. Can be call in a cycle. Use only <see cref="OnWriteRows"/>() or only <see cref="OnFillWorkbook"/>() at the same report build. </summary>
        /// <param name="dtRows"></param>
        /// <param name="rowStartIndex"> A start row index in the row collectio of <paramref name="dtRows"/> to beging write from. Do not separate by sheet if it is <see cref="int.MinValue"/>. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An index of the first row from ones left not written or a negative value if all rows has already been written. </returns>
        protected override int OnWriteRows( DataTable dtRows, int rowStartIndex, CancellationToken cancelToken = default(CancellationToken) )
        {
            return ExcelManager.WriteRows( dtRows, OnCellStyleSelection, rowStartIndex, cancelToken: cancelToken ); 
        }

        private ExcelCellStyle OnCellStyleSelection( object cellValue, DataColumn cellColumn )
        {
            var excelCellStyle = new ExcelCellStyle();
            if ( 0 == string.Compare( ColumnNameDiscrepancySumAmnt, cellColumn.ColumnName, Constants.KeysComparison )
                    && 0 != ReportGeneralHelper.GetExcelNumericValue( cellValue )
                 || 0 == string.Compare( ColumnNameDiscrepancySumAmntTotal, cellColumn.ColumnName, Constants.KeysComparison)
                    && 0 != ReportGeneralHelper.GetExcelNumericValue( cellValue ) )
                excelCellStyle.FillColorStyle = CellFillColorStyle.Red;

            return excelCellStyle;
        }

        protected override int OnFillBlockExcelData( GraphTreeNodeReportData reportData, IReadOnlyCollection<ColumnBase> reportColumns, 
            ref int lastAddedBlockLevel, int rowCountCurrent, List<ExcelMergeCell> mergeCells, GraphDataNode parentGraphData, 
            List<Dictionary<string, object>> dataRows, DataTable dataTable, bool isFirstBlock, 
            Dictionary<long, ExcelRowStyle> rowStyles, List<long> headerRowIds )
        {
            int height = 0, heightSum = height;
            if ( isFirstBlock )
            {
                height = AddBStandartBlockHeader( reportData.ByPurchaseOrSellerText, reportData.PeriodText, parentGraphData, dataTable );

                RegisterRowAsFullHeaderLine( 
                    dataTable.Columns.Count, startRowIndex: rowCountCurrent, mergeCells: mergeCells, headerRowIds: headerRowIds );
                RegisterStandartBlockHeader( 
                    dataTable.Columns.Count, startRowIndex: rowCountCurrent + 1, headerHeight: height - 1, mergeCells: mergeCells );

                heightSum += height;    rowCountCurrent += height;
            }
            height = AddBlockTableHeader( reportColumns, dataTable, addEmptyLineUpHeader: isFirstBlock );   //adds an empty delimiter row upper table header only if 'isFirstBlock == true'

            RegisterBlockTableHeader( reportColumns, columnCount: dataTable.Columns.Count, startRowIndex: rowCountCurrent,
                mergeCells: mergeCells, pageHeaderRowIds: headerRowIds, partRowStyles: rowStyles, isEmptyLineUpHeaderAdded: isFirstBlock );

            heightSum += height;

            return heightSum;
        }

        protected override int OnFillExcelTableData( IReadOnlyCollection<ColumnBase> reportColumns, int rowCountCurrent, 
            List<Dictionary<string, object>> dataRows, DataTable dataTable, Dictionary<long, ExcelRowStyle> partRowStyles )
        {
            int height = AddBlockTableData( reportColumns, dataRows, dataTable );

            RegisterBlockTableData( startRowIndex: rowCountCurrent, dataHeight: height, partRowStyles: partRowStyles );

            return height;
        }

        private void RegisterBlockTableHeader( IReadOnlyCollection<ColumnBase> reportColumns, int columnCount, int startRowIndex, 
            List<ExcelMergeCell> mergeCells, List<long> pageHeaderRowIds, Dictionary<long, ExcelRowStyle> partRowStyles, bool isEmptyLineUpHeaderAdded )
        {
            if ( !isEmptyLineUpHeaderAdded )    //to compensate unexistent empty delimiter line
                startRowIndex -= 1;

            //the first row with index: 'startRowIndex' is empty delimiter only
            pageHeaderRowIds.AddRange( new long[] { startRowIndex + 1, startRowIndex + 2 } );

            partRowStyles.Add( startRowIndex + 1, new ExcelRowStyle { IsHorizontalAlignmentCenter = true } );
            partRowStyles.Add( startRowIndex + 2, new ExcelRowStyle { IsHorizontalAlignmentCenter = true, IsVerticalAlignmentCenter = true } );

            if ( isEmptyLineUpHeaderAdded )
                mergeCells.AddRange( GetExcelRowMergeCells( beginindex: 0, endIndex: columnCount - 1, 
                    rowBegin: RowNumberFromRowIndex( startRowIndex ), rowEnd: RowNumberFromRowIndex( startRowIndex ) ) );
            mergeCells.AddRange( ReportHelper.GetExcelMergeCells( reportColumns, RowNumberFromRowIndex( startRowIndex ) + 1 ) );
        }

        private void RegisterBlockTableData( int startRowIndex, int dataHeight, Dictionary<long, ExcelRowStyle> partRowStyles )
        {
            for ( int nI = startRowIndex; nI < startRowIndex + dataHeight; nI++ )
                partRowStyles.Add( nI, new ExcelRowStyle { IsVerticalAlignmentCenter = true } );
        }

        private struct ContractorSpecification
        {
            public string Inn;
            public string Kpp;
            public string Name;
        }
    }
}