﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeTreeLevelsReporter : BaseNodeReporter<NodeReportManager>, INodeAllTreeReporter
    {
        public NodeTreeLevelsReporter( IClientLogger clientLogger ) 
            : base(  new NodeReportManager( sheetMaxDataRowsLimit: 0 ), clientLogger )
        { }

        public void StartReportBuild( string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData,
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( reportData != null );
            Contract.Requires( nodesKeyParameters != null );

            throw new NotImplementedException( String.Format( "{0} does not support StartReportBuild() yet", this.GetType().Name ) );
        }

        public void Process( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> callExecContext, 
                             CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( callExecContext != null );

            throw new NotImplementedException( String.Format( "{0} does not support Process() yet", this.GetType().Name ) );
        }

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="exception"> Not 'null' if some exception is caught. </param>
        public string FinishReportBuild( CancellationToken cancelToken = default(CancellationToken), Exception exception = null )
        {
            throw new NotImplementedException( String.Format( "{0} does not support FinishReportBuild() yet", this.GetType().Name ) );
        }

        public Task<string> StartReportBuildAsync( string reportFileFullSpec, GraphTreeNodeReportData reportData,
            Func<GraphNodesKeyParameters, CancellationToken, ICollection<GraphDataNode>> dataProviderHandler, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportData != null );
            Contract.Requires( dataProviderHandler != null );
            Contract.Requires( nodesKeyParameters != null );

            BeginReportBuild( reportData, reportFileFullSpec );

            ReportProgress( isVisible: true );

            Task<string> taskBuild = Task.Run(
                () => BuildReportFile( LoadDataAndCreateReportFile, dataProviderHandler, nodesKeyParameters, cancelToken ),
                cancelToken );

            return taskBuild;
        }

        public Task<string> StartReportBuildAsync(string reportFileFullSpec, GraphTreeLimitedLayersNodeReportData reportData, IReadOnlyCollection<GraphDataNode> reportRows, 
            GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken))
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportData != null );
            Contract.Requires( reportRows != null );

            BeginReportBuild( reportData, reportFileFullSpec );

            ReportProgress( isVisible: true );

            Task<string> taskBuild = Task.Run(
                () => BuildReportFile( DefineColumnsAndCreateReportFile, reportRows, nodesKeyParameters, cancelToken ),
                cancelToken );

            return taskBuild;
        }

        protected override IReadOnlyCollection<ColumnBase> DefineColumns( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters )
        {
            Contract.Requires( reportData != null || nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<ColumnBase>>() != null && Contract.Result<IReadOnlyCollection<ColumnBase>>().Count > 0 );

            bool isByPurchase = nodesKeyParameters.IsPurchase;
            var columnHelper = new GridSetupHelper<GraphDataNode>();
            List<ColumnBase> reportColumns = null;
            ColumnDefinition colLevelOrderNumber = null;
            if ( isByPurchase )
            {
                reportColumns = new List<ColumnBase>
                {
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные по продавцу", Columns = new List<ColumnDefinition>
                        {
	//apopov 9.2.2016	//DEBUG!!!
                            //columnHelper.CreateColumnDefinition( data => data.LevelOrderNumber, "№" ),
                            //columnHelper.CreateColumnDefinition( data => data.Id, "№" ),
                            ( colLevelOrderNumber = columnHelper.CreateColumnDefinition( data => data.Id, "№" ) ),
                            //( colLevelOrderNumber = columnHelper.CreateColumnDefinition( data => data.LevelOrderNumber, "№" ) ),
                            columnHelper.CreateColumnDefinition( data => data.Inn, "ИНН продавца" ),
                            columnHelper.CreateColumnDefinition( data => data.DeclarationType, "Признак НД" ),
                            columnHelper.CreateColumnDefinition( data => data.SurDescription, "СУР" ),
                            columnHelper.CreateColumnDefinition( data => data.NdsPercentage, "Доля вычетов у продавца" ),
                            columnHelper.CreateColumnDefinition( data => data.DeductionNds, "Сумма НДС, подлежащая вычету у продавца, ВСЕГО" ),
                            columnHelper.CreateColumnDefinition( data => data.CalcNds, "НДС исчисленный у продавца, ВСЕГО" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации покупателя", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.NotMappedAmnt, "Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом" ),
                            columnHelper.CreateColumnDefinition( data => data.ClientNdsPercentage, "Доля вычетов у покупателя по операциям с продавцом" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации продавца", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.MappedAmount, "НДС исчисленный продавцом по операциям с покупателем" ),
                            columnHelper.CreateColumnDefinition( data => data.SellerNdsPercentage, "Доля исчисленного НДС продавцом по операциям с покупателем" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные по расхождениям", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.DiscrepancySumAmntTotal, "Сумма расхождений по стороне покупателя, ВСЕГО" ),
                            //apopov 29.1.2016	//DEBUG!!!
                            //apopov 29.1.2016	//TODO!!!   //replace when data will be present in GraphData
                            //columnHelper.CreateColumnDefinition( data => data.Name, "Сумма завышения вычетов по НДС по покупателю (по стороне покупателя), ВСЕГО" ),
                            new ColumnDefinition { Key = "ColKeySumNDSExcessByPurchase", Caption = "Сумма завышения вычетов по НДС по покупателю (по стороне покупателя), ВСЕГО" },

                            columnHelper.CreateColumnDefinition( data => data.DiscrepancySumAmnt, "Сумма расхождений у покупателя-контрагента по операциям с налогоплательщиком-продавцом" ),

                            //apopov 29.1.2016	//DEBUG!!!
                            //apopov 29.1.2016	//TODO!!!   //replace when data will be present in GraphData
                            //columnHelper.CreateColumnDefinition( data => data.Name, "Сумма превышения НДС у покупателя с продавцом" )
                            new ColumnDefinition { Key = "ColKeyNDSExcessPurchaseUnderSeller", Caption = "Сумма превышения НДС у покупателя с продавцом" }
                        }
                    }
                };
            }
            else    //is by seller
            {
                reportColumns = new List<ColumnBase>
                {
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные по покупателю", Columns = new List<ColumnDefinition>
                        {
	//apopov 9.2.2016	//DEBUG!!!
                            //columnHelper.CreateColumnDefinition( data => data.LevelOrderNumber, "№" ),
                            //columnHelper.CreateColumnDefinition( data => data.Id, "№" ),
                            ( colLevelOrderNumber = columnHelper.CreateColumnDefinition( data => data.Id, "№" ) ),
                            //( colLevelOrderNumber = columnHelper.CreateColumnDefinition( data => data.LevelOrderNumber, "№" ) ),
                            columnHelper.CreateColumnDefinition( data => data.Inn, "ИНН покупателя" ),
                            columnHelper.CreateColumnDefinition( data => data.DeclarationType, "Признак НД" ),
                            columnHelper.CreateColumnDefinition( data => data.SurDescription, "СУР" ),
                            columnHelper.CreateColumnDefinition( data => data.NdsPercentage, "Доля вычетов у покупателя" ),
                            columnHelper.CreateColumnDefinition( data => data.DeductionNds, "Сумма НДС, подлежащая вычету у покупателя, ВСЕГО" ),
                            columnHelper.CreateColumnDefinition( data => data.CalcNds, "НДС исчисленный у покупателя, ВСЕГО" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации продавца", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.MappedAmount, "НДС исчисленный продавцом по операциям с покупателем" ),
                            columnHelper.CreateColumnDefinition( data => data.SellerNdsPercentage, "Доля исчисленного НДС продавцом по операциям с покупателем" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные декларации покупателя", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.NotMappedAmnt, "Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом" ),
                            columnHelper.CreateColumnDefinition( data => data.ClientNdsPercentage, "Доля вычетов у покупателя по операциям с продавцом" )
                        }
                    },
                    new ColumnGroupDefinition
                    {
                        Caption = "Данные по расхождениям", Columns = new List<ColumnDefinition>
                        {
                            columnHelper.CreateColumnDefinition( data => data.DiscrepancySumAmntTotal, "Сумма расхождений по стороне продавца, ВСЕГО" ),
                            //apopov 29.1.2016	//DEBUG!!!
                            //apopov 29.1.2016	//TODO!!!   //replace when data will be present in GraphData
                            //columnHelper.CreateColumnDefinition( data => data.Name, "Сумма завышения вычетов по НДС по продавцу (по стороне продавца), ВСЕГО" ),
                            new ColumnDefinition { Key = "ColKeySumNDSExcessBySeller", Caption = "Сумма завышения вычетов по НДС по продавцу (по стороне продавца), ВСЕГО" },

                            columnHelper.CreateColumnDefinition( data => data.DiscrepancySumAmnt, "Сумма расхождений у продавца-контрагента по операциям с налогоплательщиком-покупателем" ),

                            //apopov 29.1.2016	//TODO!!!   //add when data will be present in GraphData
                            //columnHelper.CreateColumnDefinition( data => data.Name, "Сумма превышения НДС у покупателя с продавцом" )
                            new ColumnDefinition { Key = "ColKeyNDSExcessPurchaseUnderSeller", Caption = "Сумма превышения НДС у покупателя с продавцом" }
                        }
                    }
                };
            }
            colLevelOrderNumber.FormatExcel = ColumnFormats.FormatLong;

            return reportColumns.ToReadOnly();
        }

        protected override bool IsParentSuitable(GraphDataNode lastGraphData, GraphDataNode parentGraphData)
        {
            return object.ReferenceEquals( lastGraphData.Parent, parentGraphData );
        }

        /// <summary> Adds additional columns' datas to the base columns datas from <see cref="BaseNodeReporter{NodeReportManager}.ConvertToValueDictionary"/>. </summary>
        /// <param name="graphData/param>
        /// <returns></returns>
        protected override Dictionary<string, object> ConvertToValueDictionary(GraphDataNode graphData)
        {
            IDictionary<string, object> row = base.ConvertToValueDictionary( graphData );

            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.Id, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.Inn, graphData ) );
            row.Add( ReportGeneralHelper.GetDictionaryItem( data => data.NdsPercentage, graphData ) );

            return (Dictionary<string, object>)row;
        }

        protected override void AddColumnsWidth( ColumnDefinition columnDef, List<double> columnsWidth )
        {
            if ( columnDef != null && columnDef.WidthColumnExcel != null )
                columnsWidth.Add( (double)columnDef.WidthColumnExcel );
            else
                columnsWidth.Add( 18 );
        }

        protected override Worksheet OnFillWorkbook( SpreadsheetDocument document, string reportTempFileFullSpec, 
            Func<int, DataTable> processPageBlockHandler, List<double> columnsWidths, CancellationToken cancelToken )
        {
            Worksheet worksheet = ExcelManager.FillWorkbook( document: document, filePath: reportTempFileFullSpec,
                                        getPageDataHandler: processPageBlockHandler, columnsWidths: columnsWidths, cancelToken: cancelToken );
            return worksheet;
        }

        protected override int OnFillBlockExcelData( GraphTreeNodeReportData reportData, IReadOnlyCollection<ColumnBase> reportColumns, 
            ref int lastAddedBlockLevel, int rowCountCurrent, List<ExcelMergeCell> mergeCells, GraphDataNode parentGraphData, 
            List<Dictionary<string, object>> dataRows, DataTable dataTable, bool isFirstBlock, 
            Dictionary<long, ExcelRowStyle> rowStyles, List<long> headerRowIds )
        {
            int blockLevelCurrent = (int)parentGraphData.Level;

            if ( blockLevelCurrent != lastAddedBlockLevel ) //delimiter of level 2 or more
            {
                lastAddedBlockLevel = blockLevelCurrent;

                RegisterLevelOfBlocks( columnCount: dataTable.Columns.Count, startRowIndex: rowCountCurrent,
                    mergeCells: mergeCells, pageHeaderRowIds: headerRowIds );

                rowCountCurrent += AddLevelOfBlocks( blockLevelCurrent, dataTable: dataTable );
            }
            int height = AddBlockHeader( blockLevelCurrent, reportData.ByPurchaseOrSellerText, reportData.PeriodText, 
                                         nodeData: parentGraphData, dataTable: dataTable );

            if ( blockLevelCurrent == 1 ) //the first report row: "Данные исходного НП"
                RegisterRowAsFullHeaderLine( 
                    dataTable.Columns.Count, startRowIndex: rowCountCurrent, mergeCells: mergeCells, headerRowIds: headerRowIds );
            RegisterStandartBlockHeader( dataTable.Columns.Count, startRowIndex: rowCountCurrent + 1, headerHeight: height - 1, mergeCells: mergeCells );

            int heightSum = height;     rowCountCurrent += height;

            if ( blockLevelCurrent == 1 ) //level 1 delimiter
            {
                RegisterLevelOfBlocks( dataTable.Columns.Count, startRowIndex: rowCountCurrent, mergeCells: mergeCells, pageHeaderRowIds: headerRowIds );

                height = AddLevelOfBlocks( blockLevelCurrent, dataTable: dataTable );
                heightSum += height;    rowCountCurrent += height;
            }
            height = AddBlockTableHeader( reportColumns, dataTable, addEmptyLineUpHeader: true );

            RegisterBlockTableHeader( reportColumns, columnCount: dataTable.Columns.Count, startRowIndex: rowCountCurrent,
                mergeCells: mergeCells, pageHeaderRowIds: headerRowIds, pageRowStyles: rowStyles );
            heightSum += height;        rowCountCurrent += height;

            height = AddBlockTableData( reportColumns, dataRows, dataTable );
            heightSum += height;

            return heightSum;
        }

        private void RegisterBlockTableHeader( IReadOnlyCollection<ColumnBase> reportColumns, int columnCount, int startRowIndex, 
            List<ExcelMergeCell> mergeCells, List<long> pageHeaderRowIds, Dictionary<long, ExcelRowStyle> pageRowStyles )
        {
            //the first row with index: 'startRowIndex' is empty delimiter only
            pageHeaderRowIds.AddRange( new long[] { startRowIndex + 1, startRowIndex + 2 } );

            pageRowStyles.Add( startRowIndex + 1, new ExcelRowStyle { IsHorizontalAlignmentCenter = true } );
            pageRowStyles.Add( startRowIndex + 2, new ExcelRowStyle { IsHorizontalAlignmentCenter = true, IsVerticalAlignmentCenter = true } );

            mergeCells.AddRange( GetExcelRowMergeCells( beginindex: 0, endIndex: columnCount - 1, 
                rowBegin: RowNumberFromRowIndex( startRowIndex ), rowEnd: RowNumberFromRowIndex( startRowIndex ) ) );
            mergeCells.AddRange( ReportHelper.GetExcelMergeCells( reportColumns, RowNumberFromRowIndex( startRowIndex ) + 1 ) );
        }

        private void RegisterLevelOfBlocks( int columnCount, int startRowIndex, List<ExcelMergeCell> mergeCells, List<long> pageHeaderRowIds )
        {
            pageHeaderRowIds.AddRange( new long[] { startRowIndex + 1 } );

            int startRowNumber = RowNumberFromRowIndex( startRowIndex );
            for ( int rowNumber = startRowNumber; rowNumber < startRowNumber + 1 + 1; rowNumber++ )
            {
                mergeCells.AddRange( GetExcelRowMergeCells( beginindex: 0, endIndex: columnCount - 1, rowBegin: rowNumber, rowEnd: rowNumber ) );
            }
        }

        private int AddBlockHeader(int blockLevelCurrent, string reportType, string reportPeriod, GraphDataNode nodeData, DataTable dataTable)
        {
            int rowCountOnStart = dataTable.Rows.Count;

            var ndsPrecentage = nodeData.CalcNds != 0 ? NumericFormatHelper.ToTwoDigitAfterDot(nodeData.NdsPercentage) : "НДС не исчислен";

            if ( blockLevelCurrent == 1 )
                AddHeaderItemPair( dataTable, "Данные исходного НП" );
            AddOrderNumbersAndInns( blockLevelCurrent, nodeData, dataTable );
            AddHeaderItemPair( dataTable, "Наименование",                                       nodeData.Name );
            if ( blockLevelCurrent == 1 )
                AddHeaderItemPair( dataTable, "Отчетный период",                                reportPeriod );
            AddHeaderItemPair( dataTable, "Регион",                                             nodeData.RegionName );
            AddHeaderItemPair( dataTable, "Инспекция",                                          nodeData.Inspection.EntryValue );
            if ( blockLevelCurrent == 1 )
                AddHeaderItemPair( dataTable, "Тип отчета",                                     reportType );
            AddHeaderItemPair( dataTable, "Сумма НДС, подлежащая вычету у исходного НП, ВСЕГО", nodeData.DeductionNds );
            AddHeaderItemPair( dataTable, "НДС исчисленный исходным НП, ВСЕГО",                 nodeData.CalcNds );
            AddHeaderItemPair(dataTable, "Доля вычетов у исходного НП",                         ndsPrecentage);
            AddHeaderItemPair( dataTable, "СУР",                                                nodeData.SurDescription );
            AddHeaderItemPair( dataTable, "Крупнейший",                                         KppCodeHelper.IsLargeTaxPayerByKpp( nodeData.Kpp ) );
            AddHeaderItemPair( dataTable, "Признак НД",                                         nodeData.DeclarationType );

            return dataTable.Rows.Count - rowCountOnStart;
        }

        private static void AddOrderNumbersAndInns(int blockLevel, GraphDataNode nodeData, DataTable dataTable)
        {
            int blockLevelCurrent = 0;
            while ( ++blockLevelCurrent <= blockLevel )
            {
                string innCapture = null, orderNumber = null;

                switch ( blockLevel )
                {case 1:    //level 1
                    innCapture      = "ИНН";
                    break;
                case 2:     //level 2
                    if ( blockLevelCurrent < blockLevel )
                    {
                        orderNumber = "№ п/п";
                        innCapture  = string.Concat( 
                            "ИНН НП уровня ", ReportGeneralHelper.ConvertToFormatToExcel( blockLevelCurrent, ColumnFormats.FormatLong ) );
                    }
                    break;
                default:    //level of more than 2
                    if ( blockLevelCurrent < blockLevel )
                    {
                        string blockLevelCurrentString = ReportGeneralHelper.ConvertToFormatToExcel( blockLevelCurrent, ColumnFormats.FormatLong );
                        orderNumber = string.Concat( "№ п/п уровня ", blockLevelCurrentString );
                        innCapture  = string.Concat( "ИНН НП уровня ", blockLevelCurrentString );
                    }
                    break;
                }
                if ( orderNumber != null || innCapture != null )
                {
                    GraphDataNode nodeOrParent = GetParentNodeDataOfLevel(nodeData, blockLevelCurrent);
                    if ( orderNumber != null )
                        AddHeaderItemPair( dataTable, orderNumber, nodeOrParent.Id, ColumnFormats.FormatLong );
                    AddHeaderItemPair( dataTable, innCapture, nodeOrParent.Inn );
                }
            }
        }

        private static GraphDataNode GetParentNodeDataOfLevel(GraphDataNode nodeData, int blockLevel)
        {
            GraphDataNode graphDataCurrent = nodeData;
            while ( --blockLevel > 0 )
            {
                graphDataCurrent = graphDataCurrent.Parent;
            }
            return graphDataCurrent;
        }

        private int AddLevelOfBlocks( int blockLevel, DataTable dataTable )
        {
            int rowCountOnStart = dataTable.Rows.Count;

            dataTable.Rows.Add();
            string levelNumberString = string.Concat( "Уровень ", ReportGeneralHelper.ConvertToFormatToExcel( blockLevel, ColumnFormats.FormatLong ) );
            AddHeaderItemPair( dataTable, levelNumberString );

            return dataTable.Rows.Count - rowCountOnStart;
        }
    }
}