﻿using System.Diagnostics.Contracts;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Config.GraphTree;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeAllTreeReporterProvider : IInstanceProvider<INodeAllTreeReporter>
    {
        private readonly IClientLogger _clientLogger;
        private readonly GraphTreeReportSettings _reportSettings;

        private INodeAllTreeReporter _nodeTreeReporter = null;

        public NodeAllTreeReporterProvider( IClientLogger clientLogger, GraphTreeReportSettings reportSettings = null )
        {
            Contract.Requires( clientLogger != null );

            _clientLogger = clientLogger;
            _reportSettings = reportSettings;
        }

        /// <summary> A provided instance. The provider does not guarantee that returns the same instance on each call. </summary>
        public INodeAllTreeReporter Instance
        {
            get
            {
                if ( _nodeTreeReporter == null )
                    _nodeTreeReporter = new NodeTreePlainReporter( _clientLogger, _reportSettings );

                return _nodeTreeReporter;
            }
        }
    }
}