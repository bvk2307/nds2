﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Base;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.ExcelGenerator;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree
{
    public sealed class NodeTableReporter : BaseNodeReporter<NodeReportManager>, INodeTableReporter
    {
        public NodeTableReporter( IClientLogger clientLogger ) 
            : base(  new NodeReportManager( sheetMaxDataRowsLimit: 0 ), clientLogger )
        { }

        public Task<string> StartReportBuildAsync( string reportFileFullSpec, IReadOnlyCollection<ColumnBase> reportColumns, GraphTreeNodeReportData reportData, 
            IEnumerable<GraphDataNode> reportRows, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( !string.IsNullOrWhiteSpace( reportFileFullSpec ) );
            Contract.Requires( reportColumns != null );
            Contract.Requires( reportData != null );
            Contract.Requires( reportRows != null );

            BeginReportBuild( reportData, reportFileFullSpec );

            ReportProgress( isVisible: true );

            Task<string> taskBuild = Task.Run( 
                () => BuildReportFile( RemoveTempAndCreateReportFile, reportFileFullSpec, reportColumns, reportRows, cancelToken ), 
                cancelToken );

            return taskBuild;
        }

        protected override IReadOnlyCollection<ColumnBase> DefineColumns( GraphTreeNodeReportData reportData, GraphNodesKeyParameters nodesKeyParameters )
        {
            throw new InvalidOperationException( string.Format( "DefineColumns() is not used by {0}", GetType().Name ) );
        }

        /// <summary> Places values of all public properties of <paramref name="graphData into a new dictionary. </summary>
        /// <remarks> Fully overrides the base implementation. </remarks>
        /// <param name="graphData/param>
        /// <returns></returns>
        protected override Dictionary<string, object> ConvertToValueDictionary(GraphDataNode graphData)
        {
            Dictionary<string, object> row = ReportGeneralHelper.ConvertToDictionaryAllPublicProperties( graphData );

            return row;
        }

        protected override void AddColumnsWidth( ColumnDefinition columnDef, List<double> columnsWidth )
        {
            if ( columnDef != null && columnDef.WidthColumnExcel != null )
                columnsWidth.Add( (double)columnDef.WidthColumnExcel );
            else
                columnsWidth.Add( 18 );
        }

        protected override Worksheet OnFillWorkbook( SpreadsheetDocument document, string reportTempFileFullSpec, 
            Func<int, DataTable> processPageBlockHandler, List<double> columnsWidths, CancellationToken cancelToken )
        {
            Worksheet worksheet = ExcelManager.FillWorkbook( document: document, filePath: reportTempFileFullSpec,
                                                getPageDataHandler: processPageBlockHandler, columnsWidths: columnsWidths, cancelToken: cancelToken );
            return worksheet;
        }

        protected override int OnFillBlockExcelData( GraphTreeNodeReportData reportData, IReadOnlyCollection<ColumnBase> reportColumns, 
            ref int lastAddedBlockLevel, int rowCountCurrent, List<ExcelMergeCell> mergeCells, GraphDataNode parentGraphData, 
            List<Dictionary<string, object>> dataRows, DataTable dataTable, bool isFirstBlock, 
            Dictionary<long, ExcelRowStyle> rowStyles, List<long> headerRowIds )
        {
            int height = AddBStandartBlockHeader( 
                reportData.ByPurchaseOrSellerText, reportData.PeriodText, parentGraphData: parentGraphData, dataTable: dataTable );

            RegisterRowAsFullHeaderLine( 
                dataTable.Columns.Count, startRowIndex: rowCountCurrent, mergeCells: mergeCells, headerRowIds: headerRowIds );
            RegisterStandartBlockHeader( 
                dataTable.Columns.Count, startRowIndex: rowCountCurrent + 1, headerHeight: height - 1, mergeCells: mergeCells );

            int heightSum = height;     rowCountCurrent += height;

            height = AddBlockTableHeader( reportColumns, dataTable, addEmptyLineUpHeader: true );

            RegisterBlockTableHeader( reportColumns, columnCount: dataTable.Columns.Count, startRowIndex: rowCountCurrent,
                mergeCells: mergeCells, pageHeaderRowIds: headerRowIds, pageRowStyles: rowStyles );
            heightSum += height;        rowCountCurrent += height;

            height = AddBlockTableData( reportColumns, dataRows, dataTable );
            heightSum += height;

            return heightSum;
        }

        private void RegisterBlockTableHeader( IReadOnlyCollection<ColumnBase> reportColumns, int columnCount, int startRowIndex, 
            List<ExcelMergeCell> mergeCells, List<long> pageHeaderRowIds, Dictionary<long, ExcelRowStyle> pageRowStyles )
        {
            //the first row with index: 'startRowIndex' is empty delimiter only
            pageHeaderRowIds.AddRange( new long[] { startRowIndex + 1, startRowIndex + 2 } );

            pageRowStyles.Add( startRowIndex + 1, new ExcelRowStyle { IsHorizontalAlignmentCenter = true } );
            pageRowStyles.Add( startRowIndex + 2, new ExcelRowStyle { IsHorizontalAlignmentCenter = true, IsVerticalAlignmentCenter = true } );

            mergeCells.AddRange( GetExcelRowMergeCells( beginindex: 0, endIndex: columnCount - 1, 
                rowBegin: RowNumberFromRowIndex( startRowIndex ), rowEnd: RowNumberFromRowIndex( startRowIndex ) ) );
            mergeCells.AddRange( ReportHelper.GetExcelMergeCells( reportColumns, RowNumberFromRowIndex( startRowIndex ) + 1 ) );
        }
    }
}