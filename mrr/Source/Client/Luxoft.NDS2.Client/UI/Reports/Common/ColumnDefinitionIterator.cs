﻿using System.Collections;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Reports.Common
{
    public sealed class ColumnDefinitionIterator : IEnumerable<ColumnDefinition>
    {
        private readonly IEnumerable<ColumnBase> _columns;

        public ColumnDefinitionIterator( IEnumerable<ColumnBase> columns )
        {
            _columns = columns;
        }

        /// <summary> Returns an enumerator that iterates through the collection. </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<ColumnDefinition> GetEnumerator()
        {
            ColumnGroupDefinition columnGroupDef;
            foreach ( ColumnBase elem in _columns )
            {
                columnGroupDef = elem as ColumnGroupDefinition;
                if ( columnGroupDef == null )
                {
                    yield return (ColumnDefinition)elem;
                }
                else
                {
                    foreach ( ColumnDefinition itemColumnDef in columnGroupDef.Columns )
                    {
                        yield return itemColumnDef;
                    }
                }
            }
        }

        /// <summary> Returns an enumerator that iterates through a collection. </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}