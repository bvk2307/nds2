namespace Luxoft.NDS2.Client.UI
{
    public interface INotifier
    {
        void ShowError(string text);

        void ShowWarning(string message);

        void ShowNotification(string text);
    }
}
