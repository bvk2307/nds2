﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.Model.Oper;

namespace Luxoft.NDS2.Client.Model.Bargain
{
    public class DictionaryBargain
    {
        private readonly List<OperCode> _dataItems = new List<OperCode>();

        public DictionaryBargain(IEnumerable<OperCode> dataItems)
        {
            _dataItems.AddRange(dataItems);
        }

        public DictionaryBargain()
        {
            // TODO: Complete member initialization
        }

        public IEnumerable<OperItem> Items
        {
            get
            {
                return _dataItems
                    .OrderBy(item => item.ID)
                    .Select(item => new OperItem(item.ID, item.NAME));
            }
        }

        public string NAME(string id)
        {
            var opercode = Find(id);
            return opercode != null ? opercode.NAME : "Не определено";
        }

        private OperCode Find(string id)
        {
            return _dataItems.FirstOrDefault(item => item.ID == id);
        }

    }
}
