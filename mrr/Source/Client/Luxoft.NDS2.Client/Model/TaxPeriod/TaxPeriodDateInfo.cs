﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriod
{
    public class TaxPeriodDateInfo
    {
        public DateTime DateBegin { get; private set; }
        public DateTime DateEnd { get; private set; }

        public TaxPeriodDateInfo(int quarterNumber, int year)
        {
            this.DateBegin = new DateTime(year, (quarterNumber - 1) * 3 + 1, 1);
            this.DateEnd = DateBegin.AddMonths(3).AddDays(-1);
        }

        public TaxPeriodDateInfo(DateTime dateBegin, DateTime dateEnd)
        {
            this.DateBegin = dateBegin;
            this.DateEnd = dateEnd;
        }
    }
}
