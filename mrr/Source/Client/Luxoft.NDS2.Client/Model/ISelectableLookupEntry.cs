﻿namespace Luxoft.NDS2.Client.Model
{
    /// <summary>
    /// Этот интерфейс описывает возможности, которые необходимо реализовать в модели, описывающий 1 элемент справочника
    /// </summary>
    /// <typeparam name="TLookupEntryKey">Тип ключа элементов справочника</typeparam>
    public interface ILookupEntry<TLookupEntryKey> : ISelectableEntry, IIdentifiableEntity<TLookupEntryKey>
    {
    }
}
