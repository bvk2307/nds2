﻿namespace Luxoft.NDS2.Client.Model.Oper
{
    public class OperItem
    {
        public OperItem(string id, string name)
        {
            ID = id;
            NAME = name;
        }

        public string ID
        {
            get;
             set;
        }

        public string NAME
        {
            get;
            private set;
        }
    }
}
