﻿namespace Luxoft.NDS2.Client.Model
{
    public enum VersionVerificationState 
    { 
        Pending, 
        Failed, 
        UpdateAvailable, 
        UpToDate };
}
