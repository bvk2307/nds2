﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model
{
    public interface ISelectableEntry
    {
        /// <summary>
        /// Возвращает или задает признак того, выбран ли элемент справочника пользователем или нет
        /// </summary>
        bool IsSelected { get; set; }

        /// <summary>
        /// Возвращает строковое значение, которое отображается в списке выбранных элементов справочника
        /// </summary>
        string Title { get; }
    }
}
