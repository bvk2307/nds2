﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class TaxPeriodHelper
    {
        #region ExistsQuarters

        public static QuartersExistsResult CreateExistsQuarters(List<DictTaxPeriod> dictionaryTaxPeriods, TaxPeriodConfiguration taxPeriodConfiguration)
        {
            int fiscalYearBegin = taxPeriodConfiguration.FiscalYearBegin;
            int quarterBegin = dictionaryTaxPeriods.Where(p => p.Code == taxPeriodConfiguration.TaxPeriodBegin).Single().Quarter;

            DateTime dateNow = DateTime.Now;
            int fiscalYearEnd = dateNow.Year;
            int quarterEnd = (dateNow.Month - 1) / 3 + 1;

            return CreateExistsQuarters(fiscalYearBegin, quarterBegin, fiscalYearEnd, quarterEnd);
        }

        public static QuartersExistsResult CreateExistsQuarters(int fiscalYearBegin, int quarterBegin, int fiscalYearEnd, int quarterEnd)
        {
            var result = new QuartersExistsResult();
            result.YearQuarterDefault = null;

            YearQuarter yearQuarterLast = null;
            var quarters = new List<YearQuarter>();

            for (int fiscalYear = fiscalYearBegin; fiscalYear <= fiscalYearEnd; fiscalYear++)
            {
                List<int> quartersInYear = new List<int>();
                for (int quarter = 1; quarter <= 4; quarter++)
                {
                    if ((fiscalYear == fiscalYearBegin && quarter < quarterBegin) ||
                        (fiscalYear == fiscalYearEnd && quarter > quarterEnd))
                        continue;
                    quartersInYear.Add(quarter);
                    result.YearQuarterDefault = yearQuarterLast;
                    yearQuarterLast = new YearQuarter() { Year = fiscalYear, Quarter = quarter };
                }
                if (quartersInYear.Count() > 0)
                    result.QuarterExists.Add(fiscalYear, quartersInYear);
            }

            return result;
        }

        #endregion

        #region ExistsTaxPeriod

        public static TaxPeriodResult CreateExistsTaxPeriod(List<DictTaxPeriod> dictionaryTaxPeriods, TaxPeriodConfiguration taxPeriodConfiguration)
        {
            var quartersExistsResult = CreateExistsQuarters(dictionaryTaxPeriods, taxPeriodConfiguration);

            var result = new TaxPeriodResult();
            result.TaxPeriods.Add(new TaxPeriodAll());

            foreach (KeyValuePair<int, List<int>> itemYear in quartersExistsResult.QuarterExists)
            {
                foreach (int quarter in itemYear.Value)
                {
                    var taxPeriod = CreataMainTaxPeriod(itemYear.Key, quarter, dictionaryTaxPeriods);
                    result.TaxPeriods.Add(taxPeriod);

                    if (quartersExistsResult.YearQuarterDefault != null &&
                        quartersExistsResult.YearQuarterDefault.Year == itemYear.Key &&
                        quartersExistsResult.YearQuarterDefault.Quarter == quarter)
                        result.TaxPeriodDefault = taxPeriod;
                }
            }

            return result;
        }

        private static TaxPeriod CreataMainTaxPeriod(int year, int quarter, List<DictTaxPeriod> dictionaryTaxPeriods)
        {
            var childTaxPeriods = new List<TaxPeriodBase>();
            foreach (var dictTaxPeriodChild in dictionaryTaxPeriods.Where(p => p.Quarter == quarter && !p.IsMainInQuarter))
                childTaxPeriods.Add(CreateTaxPeriod(dictTaxPeriodChild, year));

            var dictTaxPeriod = dictionaryTaxPeriods.Where(p => p.Quarter == quarter && p.IsMainInQuarter).Single();
            return CreateTaxPeriod(dictTaxPeriod, year, childTaxPeriods);
        }

        private static TaxPeriod CreateTaxPeriod(DictTaxPeriod dictTaxPeriod, int year)
        {
            return new TaxPeriod(
                dictTaxPeriod.Code,
                year.ToString(),
                string.Format("{0} {1}", dictTaxPeriod.Description, year));
        }

        private static TaxPeriod CreateTaxPeriod(DictTaxPeriod dictTaxPeriod, int year, List<TaxPeriodBase> childTaxPeriods)
        {
            return new TaxPeriod(
                dictTaxPeriod.Code,
                year.ToString(),
                string.Format("{0} {1}", dictTaxPeriod.Description, year),
                childTaxPeriods);
        }

        #endregion
    }
}
