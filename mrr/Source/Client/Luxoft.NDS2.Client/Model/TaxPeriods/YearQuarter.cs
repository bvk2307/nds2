﻿
namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class YearQuarter
    {
        public int Year { get; set; }
        public int Quarter { get; set; }
    }
}
