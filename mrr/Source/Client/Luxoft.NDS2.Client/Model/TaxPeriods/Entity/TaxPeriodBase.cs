﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public abstract class TaxPeriodBase
    {
        public string Id { get; private set; }
        public string Code { get; private set; }
        public string Year { get; private set; }
        public string Description { get; private set; }
        protected List<TaxPeriodBase> _childTaxPeriods = new List<TaxPeriodBase>();

        public TaxPeriodBase(string code, string year, string description)
        {
            InitInternal(code, year, description);
        }

        public TaxPeriodBase(string code, string year, string description, List<TaxPeriodBase> childTaxPeriods)
        {
            InitInternal(code, year, description);
            this._childTaxPeriods.AddRange(childTaxPeriods);
        }

        private void InitInternal(string code, string year, string description)
        {
            this.Code = code;
            this.Year = year;
            this.Id = string.Format("{0}{1}", code, year);
            this.Description = description;
        }

        public virtual List<TaxPeriodBase> GetTaxPeriodsNeed()
        {
            throw new NotImplementedException();
        }
    }
}
