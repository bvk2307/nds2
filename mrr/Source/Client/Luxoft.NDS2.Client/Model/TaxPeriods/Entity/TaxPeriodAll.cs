﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class TaxPeriodAll : TaxPeriodBase
    {
        public TaxPeriodAll()
            : base(String.Empty, "All", "Все периоды")
        {
        }

        public override List<TaxPeriodBase> GetTaxPeriodsNeed()
        {
            return new List<TaxPeriodBase>();
        }
    }
}
