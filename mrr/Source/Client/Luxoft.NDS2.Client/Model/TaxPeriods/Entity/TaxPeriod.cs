﻿using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class TaxPeriod : TaxPeriodBase
    {
        public TaxPeriod(string code, string year, string description) 
            : base(code, year, description)
        {
        }

        public TaxPeriod(string code, string year, string description, List<TaxPeriodBase> childTaxPeriods)
            : base(code, year, description, childTaxPeriods)
        {
        }

        public override List<TaxPeriodBase> GetTaxPeriodsNeed()
        {
            var taxPeriodsNeed = new List<TaxPeriodBase>();
            taxPeriodsNeed.Add(this);
            taxPeriodsNeed.AddRange(_childTaxPeriods);
            return taxPeriodsNeed;
        }
    }
}
