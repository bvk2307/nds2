﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class TaxPeriodResult
    {
        public List<TaxPeriodBase> TaxPeriods { get; set; }
        public TaxPeriodBase TaxPeriodDefault { get; set; }

        public TaxPeriodResult()
        {
            TaxPeriods = new List<TaxPeriodBase>();
        }
    }

    public class QuartersExistsResult
    {
        public Dictionary<int, List<int>> QuarterExists { get; set; }
        public YearQuarter YearQuarterDefault { get; set; }

        public QuartersExistsResult()
        {
            QuarterExists = new Dictionary<int, List<int>>();
        }
    }
}
