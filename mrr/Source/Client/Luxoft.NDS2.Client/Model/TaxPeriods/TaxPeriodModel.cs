﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Model.TaxPeriods
{
    public class TaxPeriodModel
    {
        private readonly List<TaxPeriodBase> _taxPeriods = new List<TaxPeriodBase>();

        private TaxPeriodBase _taxPeriodDefault;

        public TaxPeriodModel(List<TaxPeriodBase> taxPeriods, TaxPeriodBase taxPeriodDefault)
        {
            _taxPeriods.AddRange(taxPeriods);
            _taxPeriodDefault = taxPeriodDefault;
        }

        public TaxPeriodBase TaxPeriodDefault
        {
            get
            {
                return _taxPeriodDefault;
            }
        }

        public List<TaxPeriodBase> TaxPeriods
        {
            get
            {
                return _taxPeriods.Select(p => p).ToList();
            }
        }

        public TaxPeriod SelectedTaxPeriod
        {
            get;
            set;
        }
    }
}
