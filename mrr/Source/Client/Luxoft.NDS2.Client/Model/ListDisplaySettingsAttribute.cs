﻿using System;

namespace Luxoft.NDS2.Client.Model
{
    /// <summary>
    /// Этот класс описывает аттрибут с настройками отображения свойства объекта в списках
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ListDisplaySettingsAttribute : Attribute
    {
        /// <summary>
        /// Возвращает или задает заголовок поля
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Возвращает или задает описание поля
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Возвращает или задает признак того, что поле должно быть скрыто по умолчанию
        /// </summary>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Возвращает или задает ширину поля
        /// </summary>
        public int Width { get; set; }
    }
}
