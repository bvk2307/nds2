﻿namespace Luxoft.NDS2.Client.Model
{
    public interface IIdentifiableEntity<TKey>
    {
        TKey Key { get; }
    }
}
