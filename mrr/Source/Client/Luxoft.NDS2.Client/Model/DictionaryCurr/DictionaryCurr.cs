﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.Model.Oper;

namespace Luxoft.NDS2.Client.Model.Curr
{
    public class DictionaryCurr
    {
        private readonly List<OperCode> _dataItems = new List<OperCode>();

        public DictionaryCurr(IEnumerable<OperCode> dataItems)
        {
            _dataItems.AddRange(dataItems);
        }

        public DictionaryCurr()
        {
            // TODO: Complete member initialization
        }

        public IEnumerable<OperItem> Items
        {
            get
            {
                return _dataItems
                    .OrderBy(item => item.ID)
                    .Select(item => new OperItem(item.ID, item.NAME));
            }
        }

        public string NAME(string id)
        {
            string[] separators = { ",", ".", "!", "?", ";", ":", " " };
            string value = id;
            string ret = "";

            if (value != null)
            {
                string[] words = value.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                if (words.Length < 2)
                {
                    ret = FindCatch(id);
                }
                else
                    foreach (var word in words)
                    {
                        ret += word + "-" + FindCatch(word) + "\r\n";
                    }
            }
            return ret;
        }

        private string FindCatch(string id)
        {
            var opercode = Find(id);
            return opercode != null ? opercode.NAME : "Не определено";
        }

        private OperCode Find(string id)
        {
            return _dataItems.FirstOrDefault(item => item.ID == id);
        }

    }
}
