﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.Model.DictionaryAnnulmentReason
{
    public class DictionaryAnnulmentReason
    {
        private readonly List<AnnulmentReason> _items;
        private readonly Dictionary<int, AnnulmentReason> _itemsById;

        public DictionaryAnnulmentReason(List<AnnulmentReason> items)
        {
            _items = items.OrderBy(item => item.Id).ToList();
            _itemsById = items.ToDictionary(item => item.Id);
        }

        public List<AnnulmentReason> Items
        {
            get { return _items; }
        }

        public Dictionary<object, string> GetAutoFilterDictionary()
        {
            return _items.ToDictionary(item => (object) item.Id, item => item.Name);
        }

        public string GetName(int id)
        {
            return _itemsById[id].Name;
        }

    }
}
