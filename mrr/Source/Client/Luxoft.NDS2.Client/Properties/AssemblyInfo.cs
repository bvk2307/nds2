﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Luxoft.NDS2.Client")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4338e6ca-d9e6-4ff8-b340-fbd204754285")]

//apopov 26.2.2016
// Dependency Tree Utility (see namespace Luxoft.NDS2.Client.GraphTreeViewer)
[assembly: InternalsVisibleTo( "GraphTreeViewer, PublicKey =" +
"00240000048000009400000006020000002400005253413100040000010001001df4696af02d9d" +
"e3d32fd3d5f96c4fa74b38e613cd4815984659ddc13bad666f153f95008354f78997421b633925" +
"e7bd6892dc11914ed9dd8a70c531c58e2aa79cf6711b399a26470d04ec3eeeb0c784ea10b4fcb4" +
"53cc7df8484cf3a161e0824459660d6cb52ca12883540997d50cd21144ec24971f951814be962c" +
"0879acd4" )]