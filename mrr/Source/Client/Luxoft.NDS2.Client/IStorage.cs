﻿using System;

namespace Luxoft.NDS2.Client
{
    /// <summary>
    /// Этот инстерфейс описывает словарь-хранилище сериализованных объектов
    /// </summary>
    public interface IStorage
    {
        string Load(string key);

        void Save(string key, string data);
    }
}
