﻿using System.ComponentModel;
using System.Configuration;
using Luxoft.NDS2.Client.Config.GraphTree;

namespace Luxoft.NDS2.Client.Config
{
    /// <summary> Конфигурация Дерева связей Клиента НДС2 </summary>
    public sealed class DependencyTreeSection : ConfigurationSection
    {
        private static readonly ConfigurationProperty s_propName = 
            new ConfigurationProperty( "name", typeof(string), defaultValue: (object)null, typeConverter: (TypeConverter)null, 
                validator: (ConfigurationValidatorBase)new StringValidator( minLength: 1 ), 
                options: ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey );
        private static readonly ConfigurationProperty s_graphTreeSettings = 
            new ConfigurationProperty( "treeSettings", typeof(GraphTreeSettings), defaultValue: (object)null );
        private static readonly ConfigurationProperty s_graphTreeReportSettings = 
            new ConfigurationProperty( "reportSettings", typeof(GraphTreeReportSettings), defaultValue: (object)null );
        private static readonly ConfigurationProperty s_graphTreeDataCacheSettings = 
            new ConfigurationProperty( "dataCacheSettings", typeof(GraphTreeDataCacheSettings), defaultValue: (object)null );
        private readonly ConfigurationPropertyCollection s_properties;

        [ConfigurationProperty( "name" )]
        public string Name
        {
            get
            {
                return (string)this[DependencyTreeSection.s_propName];
            }
            set
            {
                this[DependencyTreeSection.s_propName] = (object)value;
            }
        }

        /// <summary> Конфигурация визуального Дерева связей </summary>
        [ConfigurationProperty( "treeSettings" )]
        public GraphTreeSettings TreeSettings
        {
            get
            {
                return (GraphTreeSettings)this[DependencyTreeSection.s_graphTreeSettings];
            }
        }

        /// <summary> Конфигурация отчётов Дерева связей </summary>
        [ConfigurationProperty( "reportSettings" )]
        public GraphTreeReportSettings ReportSettings
        {
            get
            {
                return (GraphTreeReportSettings)this[DependencyTreeSection.s_graphTreeReportSettings];
            }
        }

        /// <summary> Конфигурация кеша данных Дерева связей </summary>
        [ConfigurationProperty( "dataCacheSettings" )]
        public GraphTreeDataCacheSettings DataCacheSettings
        {
            get
            {
                return (GraphTreeDataCacheSettings)this[DependencyTreeSection.s_graphTreeDataCacheSettings];
            }
        }

        public DependencyTreeSection()
        {
            this.s_properties = new ConfigurationPropertyCollection();
            this.s_properties.Add( DependencyTreeSection.s_propName );
        }
    }
}
