﻿using System.ComponentModel;
using System.Configuration;

namespace Luxoft.NDS2.Client.Config.GraphTree
{
    /// <summary> Конфигурация кеша данных Дерева связей Клиента НДС2 </summary>
    public sealed class GraphTreeDataCacheSettings : ConfigurationElement
    {
        private static readonly ConfigurationProperty s_propName = 
            new ConfigurationProperty( "name", typeof(string), defaultValue: typeof(GraphTreeDataCacheSettings).Name, typeConverter: (TypeConverter)null, 
                validator: (ConfigurationValidatorBase)new StringValidator( minLength: 1 ), 
                options: ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey );
        private static readonly ConfigurationProperty s_propNodesExpirationTimeSec = 
            new ConfigurationProperty( "nodesExpirationTimeSec", typeof(int), defaultValue: 90, 
                typeConverter: (TypeConverter) null, 
                validator: (ConfigurationValidatorBase) new IntegerValidator( minValue: 1, maxValue: int.MaxValue ), 
                options: ConfigurationPropertyOptions.None );

        private static ConfigurationPropertyCollection s_properties = new ConfigurationPropertyCollection();

        internal string Key
        {
            get
            {
                return this.Name;
            }
        }

        [ConfigurationProperty( "name", DefaultValue = "GraphTreeDataCacheSettings", Options = ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey )]
        public string Name
        {
            get
            {
                return (string)this[GraphTreeDataCacheSettings.s_propName];
            }
            set
            {
                this[GraphTreeDataCacheSettings.s_propName] = (object)value;
            }
        }

        /// <summary> Интервал хранения узлов Дерева связей в секундах </summary>
        [ConfigurationProperty( "nodesExpirationTimeSec", DefaultValue = "90" )]
        public int NodesExpirationTimeSec
        {
            get
            {
                return (int)this[GraphTreeDataCacheSettings.s_propNodesExpirationTimeSec];
            }
            set
            {
                this[GraphTreeDataCacheSettings.s_propNodesExpirationTimeSec] = (object)value;
            }
        }

        static GraphTreeDataCacheSettings()
        {
            GraphTreeDataCacheSettings.s_properties.Add(GraphTreeDataCacheSettings.s_propName);
            GraphTreeDataCacheSettings.s_properties.Add(GraphTreeDataCacheSettings.s_propNodesExpirationTimeSec);
        }
    }
}
