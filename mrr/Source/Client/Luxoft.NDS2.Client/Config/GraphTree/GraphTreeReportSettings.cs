﻿using System.ComponentModel;
using System.Configuration;

namespace Luxoft.NDS2.Client.Config.GraphTree
{
    /// <summary> Конфигурация отчётов Дерева связей Клиента НДС2 </summary>
    public sealed class GraphTreeReportSettings : ConfigurationElement
    {
        private static readonly ConfigurationProperty s_propName =
            new ConfigurationProperty("name", typeof(string), defaultValue: typeof(GraphTreeReportSettings).Name, typeConverter: (TypeConverter)null, 
                validator: (ConfigurationValidatorBase)new StringValidator( minLength: 1 ), 
                options: ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey );
        private static readonly ConfigurationProperty s_propLayersDepthLimitAllTree = 
            //new ConfigurationProperty( "layersDepthLimitAllTree", typeof(int), defaultValue: 6, 
            new ConfigurationProperty("layersDepthLimit", typeof(int), defaultValue: 4, 
                typeConverter: (TypeConverter) null, 
                validator: (ConfigurationValidatorBase) new IntegerValidator( minValue: 1, maxValue: int.MaxValue ), 
                options: ConfigurationPropertyOptions.None );
        //private static readonly ConfigurationProperty s_propLayersDepthLimitVisibleTree = 
        //    new ConfigurationProperty( "layersDepthLimitVisibleTree", typeof(int), defaultValue: 4, 
        //        typeConverter: (TypeConverter) null, 
        //        validator: (ConfigurationValidatorBase) new IntegerValidator( minValue: 1, maxValue: int.MaxValue ), 
        //        options: ConfigurationPropertyOptions.None );
        private static readonly ConfigurationProperty s_propLayerMaxNodesExportLimit =
            new ConfigurationProperty("layerMaxNodesExportLimit", typeof(int), defaultValue: 30,
                typeConverter: (TypeConverter)null,
                validator: (ConfigurationValidatorBase)new IntegerValidator( minValue: 1, maxValue: int.MaxValue ),
                options: ConfigurationPropertyOptions.None );
        private static readonly ConfigurationProperty s_propSheetMaxDataRowsLimit =
            new ConfigurationProperty("sheetMaxDataRowsLimit", typeof(int), defaultValue: 100000,
                typeConverter: (TypeConverter)null,
                validator: (ConfigurationValidatorBase)new IntegerValidator( minValue: 1, maxValue: int.MaxValue ),
                options: ConfigurationPropertyOptions.None );

        private static ConfigurationPropertyCollection s_properties = new ConfigurationPropertyCollection();

        internal string Key
        {
            get
            {
                return this.Name;
            }
        }

        [ConfigurationProperty( "name", DefaultValue = "GraphTreeReportSettings", Options = ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey )]
        public string Name
        {
            get
            {
                return (string)this[GraphTreeReportSettings.s_propName];
            }
            set
            {
                this[GraphTreeReportSettings.s_propName] = (object)value;
            }
        }

        /// <summary> Ограничение глубины дерева для отчёта всех узлов Дерева связей </summary>
        [ConfigurationProperty("layersDepthLimit", DefaultValue = "4")]
        public int LayersDepthLimit
        {
            get
            {
                return (int)this[GraphTreeReportSettings.s_propLayersDepthLimitAllTree];
            }
            set
            {
                this[GraphTreeReportSettings.s_propLayersDepthLimitAllTree] = (object)value;
            }
        }

        ///// <summary> Ограничение глубины дерева для отчёта видимых узлов Дерева связей </summary>
        //[ConfigurationProperty( "layersDepthLimitVisibleTree", DefaultValue = "6" )]
        //public int LayersDepthLimitVisibleTree
        //{
        //    get
        //    {
        //        return (int)this[GraphTreeReportSettings.s_propLayersDepthLimitVisibleTree];
        //    }
        //    set
        //    {
        //        this[GraphTreeReportSettings.s_propLayersDepthLimitVisibleTree] = (object)value;
        //    }
        //}

        /// <summary> Ограничение количества дочерних узлов для отчётов Дерева связей </summary>
        [ConfigurationProperty("layerMaxNodesExportLimit", DefaultValue = "30")]
        public int LayerMaxNodesExportLimit
        {
            get
            {
                return (int)this[GraphTreeReportSettings.s_propLayerMaxNodesExportLimit];
            }
            set
            {
                this[GraphTreeReportSettings.s_propLayerMaxNodesExportLimit] = (object)value;
            }
        }

        /// <summary> Ограничение количества строк данных на странице Excel-документа для отчётов Дерева связей </summary>
        [ConfigurationProperty("sheetMaxDataRowsLimit", DefaultValue = "100000")]
        public int SheetMaxDataRowsLimit
        {
            get
            {
                return (int)this[GraphTreeReportSettings.s_propSheetMaxDataRowsLimit];
            }
            set
            {
                this[GraphTreeReportSettings.s_propSheetMaxDataRowsLimit] = (object)value;
            }
        }

        static GraphTreeReportSettings()
        {
            GraphTreeReportSettings.s_properties.Add(GraphTreeReportSettings.s_propName);
            GraphTreeReportSettings.s_properties.Add(GraphTreeReportSettings.s_propLayersDepthLimitAllTree);
            //GraphTreeReportSettings.s_properties.Add(GraphTreeReportSettings.s_propLayersDepthLimitVisibleTree);
            GraphTreeReportSettings.s_properties.Add(GraphTreeReportSettings.s_propLayerMaxNodesExportLimit);
            GraphTreeReportSettings.s_properties.Add(GraphTreeReportSettings.s_propSheetMaxDataRowsLimit);
        }
    }
}
