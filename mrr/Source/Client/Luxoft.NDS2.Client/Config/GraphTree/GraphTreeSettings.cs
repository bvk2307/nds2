﻿using System.ComponentModel;
using System.Configuration;

namespace Luxoft.NDS2.Client.Config.GraphTree
{
    /// <summary> Конфигурация визуального Дерева связей Клиента НДС2 </summary>
    public sealed class GraphTreeSettings : ConfigurationElement
    {
        private static readonly ConfigurationProperty s_propName =
            new ConfigurationProperty("name", typeof(string), defaultValue: typeof(GraphTreeSettings).Name, typeConverter: (TypeConverter)null, 
                validator: (ConfigurationValidatorBase)new StringValidator( minLength: 1 ), 
                options: ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey );
        private static readonly ConfigurationProperty s_propLayerMaxNodesLimit =
            new ConfigurationProperty("layerMaxNodesLimit", typeof(int), defaultValue: 250,
                typeConverter: (TypeConverter)null,
                validator: (ConfigurationValidatorBase)new IntegerValidator( minValue: 1, maxValue: int.MaxValue ),
                options: ConfigurationPropertyOptions.None );

        //private static readonly ConfigurationProperty _propParameters = new ConfigurationProperty("parameters", typeof (ParameterItemSettingsCollection), (object) null, ConfigurationPropertyOptions.IsDefaultCollection);

        private static ConfigurationPropertyCollection s_properties = new ConfigurationPropertyCollection();

        internal string Key
        {
            get
            {
                return this.Name;
            }
        }

        [ConfigurationProperty( "name", DefaultValue = "GraphTreeSettings", Options = ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey )]
        public string Name
        {
            get
            {
                return (string)this[GraphTreeSettings.s_propName];
            }
            set
            {
                this[GraphTreeSettings.s_propName] = (object)value;
            }
        }

        /// <summary> Ограничение количества дочерних узлов для визуальных отчётов Дерева связей </summary>
        [ConfigurationProperty("layerMaxNodesLimit", DefaultValue = "250")]
        public int LayerMaxNodesLimit
        {
            get
            {
                return (int)this[GraphTreeSettings.s_propLayerMaxNodesLimit];
            }
            set
            {
                this[GraphTreeSettings.s_propLayerMaxNodesLimit] = (object)value;
            }
        }

	//apopov 19.2.2016	//DEBUG!!!
        //[ConfigurationProperty("parameters", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        //public ParameterItemSettingsCollection ProviderParameters
        //{
        //  get
        //  {
        //    return (ParameterItemSettingsCollection) this[GraphTreeSettings._propParameters];
        //  }
        //}

        static GraphTreeSettings()
        {
            GraphTreeSettings.s_properties.Add(GraphTreeSettings.s_propName);
            GraphTreeSettings.s_properties.Add(GraphTreeSettings.s_propLayerMaxNodesLimit);
            //GraphTreeSettings.s_properties.Add(GraphTreeSettings._propParameters);
        }
    }
}
