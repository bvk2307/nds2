﻿using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.ThriftProxy.Implementation;

namespace Luxoft.NDS2.Server.ThriftProxy
{
    /// <summary>
    /// Этот класс создает экземпляр клиента Thrift сервиса
    /// </summary>
    public class ClientProxyCreator
    {
        /// <summary>
        /// Создает новый экземпляр класса, реализующего интерфейс IClientProxy
        /// </summary>
        /// <param name="host">Хост сервиса</param>
        /// <param name="port">Порт сервиса</param>
        /// <returns>Ссылка на клиента</returns>
        public static IClientProxy Create(string host, int port)
        {
            return new ClientProxy(host, port);
        }

        public static IClientProxy Create(string host, int port, ILogProvider logger)
        {
            return new ClientProxy(host, port, logger);
        }
    }
}
