﻿using Luxoft.NDS2.Server.Common;
using System;
using Thrift.Transport;

namespace Luxoft.NDS2.Server.ThriftProxy.Helpers
{
    public static class ThriftHelper
    {
        public static TResult Execute<TResult>(
            this TTransport transport,
            Func<TResult> action)
        {
            TResult result;

            try
            {
                transport.Open();
                result = action();
            }
            catch (Exception ex)
            {
                throw new ThriftDataTransferException(ex);
            }
            finally
            {
                if (transport.IsOpen)
                {
                    transport.Close();
                }
            }

            return result;
        }
    }
}
