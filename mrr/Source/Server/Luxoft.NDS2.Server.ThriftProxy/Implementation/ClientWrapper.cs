﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using pyramid = Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Server.ThriftProxy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Thrift;
using Thrift.Protocol;
using Thrift.Transport;

namespace Luxoft.NDS2.Server.ThriftProxy.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс IClientProxy
    /// </summary>
    internal class ClientWrapper : IClientProxy
    {
        # region Константы

        private long DefaultClientId = default(long);

        # endregion

        # region Поля

        private TTransport _transport;
        private RequestService.Client _client;

        # endregion

        # region Конструкторы

        /// <summary>
        /// Создает экземпляр класса ClientWrapper
        /// </summary>
        /// <param name="host">Имя хоста сервиса</param>
        /// <param name="port">Номер порта сервиса</param>
        public ClientWrapper(string host, int port)
        {
            _transport = new TSocket(host, port); 
            _client = 
                new RequestService.Client(
                    new TBinaryProtocol(_transport));
        }

        # endregion

        # region Реализация интерфейса IClientProxy

        public long RequestInvoiceData(List<KeyValuePair<int, string>> invoices, bool loadMismatches = false, bool loadOnlyTargets = false)
        {
            var invoiceRecords = new Dictionary<int, List<string>>();
            
            invoices
                .Select(pair => pair.Key)
                .Distinct()
                .ToList()
                .ForEach(
                    chapter => 
                        invoiceRecords.Add(
                            chapter, 
                            invoices.Where(p => p.Key == chapter).Select(p => p.Value).ToList()));

            return _transport.Execute<long>(
                () => _client.sendInvoiceDataRequest(
                    new InvoiceDataRequest()
                    {
                        InvoiceRecords = invoiceRecords,
                        LoadMismatches = loadMismatches,
                        LoadOnlyTargets = loadOnlyTargets
                    }));
        }

        public long RequestDiscrepancies(SelectionFilter filter, long selectionId)
        {
            return _transport.Execute<long>(
                () => _client.sendMismatchRequest(
                    new MismatchRequest()
                    {
                         Criterias = filter.Convert(),
                    }));
        }

        public long RequestBookData(
            string inn, 
            int period, 
            int year, 
            int partitionNumber,
            int correctionNumber = 0)
        {
            return _transport.Execute<long>(
                () => _client.sendBookDataRequest(
                    new BookDataRequest()
                    {
                        CorrectionNumber = correctionNumber,
                        Inn = inn,
                        Year = year,
                        PartitionNumber = partitionNumber,
                        Period = period
                    }));
        }

        public long RequestNdsDiscrepancies(
            DeclarationRequestData declaration,
            int discrepancyType, 
            int discrepancyKind)
        {
            int correctionNum;

            if (!int.TryParse(declaration.CORRECTION_NUMBER, out correctionNum))
            {
                correctionNum = 0;
            }

            return _transport.Execute<long>(
                () =>
                    _client.sendNdsMismatchRequest(
                        new NdsMismatchRequest()
                        {
                             CorrectionNumber = correctionNum,
                             MismatchKind = discrepancyKind,
                             MismatchType = discrepancyType,
                             Inn = declaration.INN.Trim(),
                             Period = int.Parse(declaration.TAX_PERIOD),
                             Year = int.Parse(declaration.FISCAL_YEAR)
                        }));
        }

        public long RequestPyramidData(pyramid.Request request)
        {
            return _transport.Execute<long>(
                () =>
                    _client.sendPyramidRequest(
                        new PyramidRequest()
                        {
                             Inn = request.Inn,
                             Kpp = request.Kpp,
                             MinMatchStreamSum = 
                                request.MinMappedAmount.HasValue 
                                ? request.MinMappedAmount.Value.ToString() 
                                : string.Empty,
                             MinNotMatchStreamSum = 
                                request.MinNotmappedAmount.HasValue 
                                ? request.MinNotmappedAmount.Value.ToString() 
                                : string.Empty,
                             Period = request.Period,
                             Year = request.Year,
                             TreeType = (int)request.Type
                        }));
        }

        public long RequestReportLogicalChecks(int year, List<string> periods, string date, List<string> inspections)
        {
            return _transport.Execute<long>(
                () => _client.sendLogicalChecksRequest(
                    new LogicalChecksRequest()
                    {
                        Year = year,
                        Periods = periods,
                        Date = date,
                        Inspections = inspections
                    }));
        }

        public long RequestContragentData(string inn, int period, int year, int correctionNumber = 0)
        {
            return _transport.Execute<long>(
                () => _client.sendContragentDataRequest(
                    new ContragentDataRequest
                    {
                        CorrectionNumber = correctionNumber,
                        Inn = inn,
                        Year = year,
                        Period = period
                    }));
        }

        public long RequestContragentParamsData(string inn, string contractorInn, int period, int year, int correctionNumber = 0)
        {
            return _transport.Execute<long>(
                () => _client.sendContragentParamRequest(
                    new ContragentParamRequest()
                    {
                        CorrectionNumber = correctionNumber,
                        Inn = inn,
                        ContractorInn = contractorInn,
                        Year = year,
                        Period = period
                    }));
        }
        
        # endregion
    }
}
