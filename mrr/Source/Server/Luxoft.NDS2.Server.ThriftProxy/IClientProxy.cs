﻿using System.Diagnostics;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using navigator = Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using pyramid = Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.ThriftProxy
{
    /// <summary>
    /// Этот интерфейс описывает методы работы с Thrift сервисом
    /// </summary>
    public interface IClientProxy
    {
        /// <summary>
        /// Запрашивает данные счетов-фактур в модуле сопоставления
        /// </summary>
        /// <param name="invoices"></param>
        /// <param name="loadMismatches">флаг выгрузки расхождений по заданным СФ</param>
        /// <param name="loadOnlyTargets">если запрашивается агрегат, то true - грузит только его составляющие. false - составляющие и сам агрегат. Если не агрегат – опция игнорируется.</param>
        /// <returns></returns>
        long RequestInvoiceData(List<KeyValuePair<int, string>> invoices, bool loadMismatches = false, bool loadOnlyTargets = false);

        /// <summary>
        /// Запрашивает данные о расхождениях выборки
        /// </summary>
        /// <param name="filter">Ссылка на фильтр выборки</param>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <returns>Идентификатор запроса расхождений</returns>
        long RequestDiscrepancies(SelectionFilter filter, long selectionId);

        /// <summary>
        /// Отправляет запрос на получение книги декларации в SOV 
        /// </summary>
        /// <param name="inn">ИНН НП</param>
        /// <param name="period">Период подачи декларации</param>
        /// <param name="year">Год подачи декларации</param>
        /// <param name="partitionNumber">Номер раздела</param>
        /// <param name="correctionNumber">Номер исправления</param>
        /// <param name="priority">Приоритет запроса для СОВ. рассчитывается на основании объема СФ в разделе</param>
        /// <returns>Идентификатор запроса</returns>
        long RequestBookData(
            string inn,
            int period,
            int year,
            int partitionNumber,
            int correctionNumber = 0, int priority = 1);

        /// <summary>
        /// Отправляет запрос данных расхождений в SOV
        /// </summary>
        /// <param name="declaration">Декларация</param>
        /// <param name="discrepancyType">Тип расхождений</param>
        /// <param name="discrepancyKind">Вид расхождений</param>
        /// <returns>Идентификатор запроса</returns>
        long RequestNdsDiscrepancies(
            DeclarationRequestData declaration,
            int discrepancyType,
            int discrepancyKind);

        /// <summary>
        /// Отправляет запрос в SOV для получения данных о связях контрагента для отчета "Пирамида"
        /// </summary>
        /// <param name="request">Параметры запроса</param>
        /// <returns>Идентификатор запроса</returns>
        long RequestPyramidData(pyramid.Request request);

        /// <summary>
        /// Запрашивает отчет по проверкам ЛК
        /// </summary>
        /// <param name="year">год</param>
        /// <param name="periods">отчетные периоды</param>
        /// <param name="date">дата посотроения отчета</param>
        /// <param name="inspections">список инспекций</param>
        /// <returns>Идентификатор запроса отчет по проверкам ЛК</returns>
        long RequestReportLogicalChecks(int year, List<string> periods, string date, List<string> inspections);

        /// <summary>
        /// Отправляет запрос на получение списка контрагентов в SOV 
        /// </summary>
        /// <param name="inn">ИНН НП</param>
        /// <param name="period">Период подачи декларации</param>
        /// <param name="year">Год подачи декларации</param>
        /// <param name="correctionNumber">Номер исправления</param>
        /// <returns>Идентификатор запроса</returns>
        long RequestContragentData(
            string inn,
            int period,
            int year,
            int correctionNumber = 0);

        long RequestContragentParamsData(
            string inn,
            string contractorInn,
            int period,
            int year,
            int correctionNumber = 0);

        long RequestNavigatorData(navigator.Request request);
    }
}
