﻿using CommonComponents.GenericHost;
using CommonComponents.Shared;

namespace Luxoft.NDS2.Server.TaskScheduler
{
    public class LaunchStrategy
    {
        [PreHandler(ApplicationHostEvents.Activate)]
        public void OnActivate(IStrategyContext context)
        {
            var serv = context.ServiceCollection.Get<TaskSchedulerService>();
            serv.Run();
        }


        [PreHandler(ApplicationHostEvents.Deactivate)]
        public void OnDeactivate(IStrategyContext context)
        {
            var serv = context.ServiceCollection.Get<TaskSchedulerService>();
            serv.Shutdown();
        }
    }
}
