﻿using Luxoft.NDS2.Common.Contracts.DTO.ExportExcel;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Excel.Excel;
using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation;
using Luxoft.NDS2.Common.Models.TableSetup;
using Luxoft.NDS2.Common.Models.ViewModels;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Selections;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using IServiceProvider = Luxoft.NDS2.Server.DAL.IServiceProvider;

namespace Luxoft.NDS2.Server.TaskScheduler.Excel
{
    public interface IExportSelectionDiscrepancyCallback
    {
        void LogExportedRows(int count);
        bool CheckCanceled();
    }

    /// <summary>
    ///     Экспорт расхождений в Excel
    /// </summary>
    public class ExportSelectionDiscrepancy
    {
        private readonly IServiceProvider _helper;
        private readonly ExportExcelItem _item;
        private const int Seed = 100;
        private const int GarbageSeed = 10000;

        public ExportSelectionDiscrepancy(IServiceProvider helper, ExportExcelItem item)
        {
            _helper = helper;
            _item = item;
        }

        /// <summary>
        ///     Экспортирует расхождения в файл
        /// </summary>
        /// <param name="fileName">Имя файла (полный путь)</param>
        /// <param name="callback">Колбэк для логирования выгрузки и проверки отмены экспорта</param>
        /// <returns>Количество выгруженных расхождений</returns>
        public int DoExport(string fileName, IExportSelectionDiscrepancyCallback callback)
        {
            if (callback.CheckCanceled())
                return 0;

            var counter = 0;

            var discrepancyAdapter = _helper.SelectionDiscrepanciesAdapter();

            var surCodes = TableAdapterCreator.DictionarySUR(_helper).GetList();

            var columns = new SelectionDiscrepancyTableConfigurator(
                new ColumnFactory<SelectionDiscrepancyModel>()
                ).GetColumns(surCodes.ToArray());

            var excelColumns = columns.ConvertToExcelColumns().ToArray();

            var excelWriter = new ExcelFileWriter(fileName, excelColumns,
                string.Format("Расхождения выборки {0} - {{0}}", _item.SELECTION_ID));
            excelWriter.WriteHeader();

            var filter = _item.ROW_FILTER.ParseXML<QueryConditionsLite>() ?? new QueryConditionsLite();
            var criteria = filter.ToQueryConditions();

            discrepancyAdapter.ForAll(criteria.Filter.ToFilterGroup(), criteria.Sorting, _item.SELECTION_ID,
                discrepancy =>
                {
                    var model = new SelectionDiscrepancyModel(discrepancy);
                    excelWriter.Write(model);

                    ++counter;
                    if (counter % Seed == 0)
                    {
                        if (callback.CheckCanceled())
                        {
                            excelWriter.Close();
                            File.Delete(fileName);
                            return false;
                        }
                        callback.LogExportedRows(counter);
                    }
                    if (counter % GarbageSeed == 0)
                    {
                        GC.Collect(2);
                    }
                    return true;
                }
                );

            excelWriter.Close();

            return counter;
        }
    }

    public static class ParseHelper
    {
        public static T ParseXML<T>(this string s) where T : class
        {
            if (string.IsNullOrEmpty(s))
                return null;

            var reader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(s.Trim())));
            var readerXML = XmlReader.Create(
                reader,
                new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Document }
                );
            return new XmlSerializer(typeof(T)).Deserialize(readerXML) as T;
        }
    }
}