﻿using System;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.Logging.Simple;
using CommonComponents.Shared;
using Luxoft.NDS2.Server.Services.Helpers;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace Luxoft.NDS2.Server.TaskScheduler
{
    public class TaskSchedulerService
    {
        private IReadOnlyServiceCollection _runtime;

        private ServiceHelpers _helper;
        private IScheduler _scheduler;
        private Task _worker;

        public TaskSchedulerService(IReadOnlyServiceCollection runtime)
        {
            _runtime = runtime;
        }

        public void Run()
        {
            Console.WriteLine("----- Service running -----");

            _helper = new ServiceHelpers(_runtime);

            LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter { Level = LogLevel.Warn };

            Console.WriteLine("Начало конфигурирования службы автоматических заданий МРР");
            _helper.LogNotification("Начало конфигурирования службы автоматических заданий МРР", "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Start");
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _scheduler.Context["ServiceHelpers"] = _helper;
            _scheduler.Start();

            _helper.Do(() =>
            {
                var triggerState = _scheduler.GetTriggerState(new TriggerKey("SELECTION_TRG", "TASKSCHEDULER"));
                var selectionTrigger = _scheduler.GetTrigger(new TriggerKey("SELECTION_TRG", "TASKSCHEDULER"));
                if (selectionTrigger != null && triggerState == TriggerState.Blocked)
                {
                    _scheduler.RescheduleJob(selectionTrigger.Key, selectionTrigger);
                }

                Console.WriteLine("Текущее время(UTC) {0}", DateTime.UtcNow);
                _helper.LogNotification(string.Format("Текущее время(UTC) {0}", DateTime.UtcNow), "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Start");
                var taskListSb = new StringBuilder("Список запланированных заданий: ");
                taskListSb.AppendLine();
                var index = 1;
                foreach (
                    var triggerKey in _scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.GroupEquals("TASKSCHEDULER")))
                {
                    var trigger = _scheduler.GetTrigger(triggerKey);
                    taskListSb.AppendLine(String.Format("{0}. Ключ задания: \"{1}\". Следующее время запуска задания(UTC) {2} ", index, trigger.JobKey.Name, trigger.GetNextFireTimeUtc()));
                    index++;
                }
                Console.WriteLine(taskListSb.ToString(), "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Start");
                _helper.LogNotification(taskListSb.ToString(), "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Start");
                Console.WriteLine("Конфигурирование службы автоматических заданий МРР завершилось успешно");
                _helper.LogNotification("Конфигурирование службы автоматических заданий МРР завершилось успешно", "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Start");
            });
        }

        public void Shutdown()
        {
            Console.WriteLine("----- Service shutting down -----");
            _helper.Do(() =>
            {
                _scheduler.Shutdown();
                Console.WriteLine("Остановка службы автоматических заданий МРР завершилась успешно");
                _helper.LogNotification("Остановка службы автоматических заданий МРР завершилась успешно",
                    "Luxoft.NDS2.Server.TaskScheduler.TaskSchedulerModule.Stop");
            });
        }
    }
}
