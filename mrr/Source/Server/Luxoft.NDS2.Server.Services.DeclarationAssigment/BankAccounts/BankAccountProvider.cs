﻿using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Server.DAL.BankAccounts;
using System;
using System.IO;
using System.Linq;
using System.Xml.Schema;
using TransactionScopeHelper = Luxoft.NDS2.Server.Common.Helpers.TransactionScopeHelper;

namespace Luxoft.NDS2.Server.TaskScheduler.BankAccounts
{
    public interface IBankAccountCallback
    {
        void Log(MessageType type, string message);
    }

    public class BankAccountProvider
    {
        private const string XmlSchemaName = "TAX3EXCH_NDS2_CAM_05_01.xsd";
        private readonly IBankAccountsAdapter _bankAccountsAdapter;
        private readonly IBankAccountCallback _callback;

        public BankAccountProvider(IBankAccountsAdapter bankAccountsAdapter, IBankAccountCallback callback)
        {
            _bankAccountsAdapter = bankAccountsAdapter;
            _callback = callback;
        }

        public void ProcessQueue()
        {
            var limit = _bankAccountsAdapter.GetPullLimit();

            XmlSchemaSet xsd = null;

            for (var i = 0; i < limit; ++i)
            {
                try
                {
                    var request = _bankAccountsAdapter.PullRequest();

                    if (request == null)
                        return;

                    if (xsd == null)
                    {
                        var schema = _bankAccountsAdapter.GetXmlSchema(XmlSchemaName);
                        xsd = XmlProducer.GetSchema(schema);
                    }

                    ProcessRequest(request, xsd);
                }
                catch (Exception e)
                {
                    _callback.Log(MessageType.Error, e.Message);
                }
            }
        }

        private void ProcessRequest(BankAccountRequest request, XmlSchemaSet xsd)
        {
            try
            {
                if (request.RequestActor == RequestActor.Auto)
                {
                    request.Accounts = _bankAccountsAdapter.GetAccounts(
                        request.Inn,
                        request.Kpp,
                        request.Bik,
                        request.InnBank,
                        request.KppBank,
                        request.BankNumber,
                        request.FilialNumber,
                        request.PeriodBegin,
                        request.PeriodEnd).ToArray();
                }

                if (request.Accounts.Length == 0)
                {
                    _bankAccountsAdapter.DropRequest(request.Id);
                    return;
                }

                var validator = new Validator(_callback);

                bool result;

                using (var scope = TransactionScopeHelper.DefaultTransactionScope())
                {
                    var docId = _bankAccountsAdapter.GetDocId();
                    var xml = XmlProducer.Generate(docId, request);

                    XmlProducer.Validate(xsd, xml, validator.ValidationHandler);

                    var accounts = request.Accounts.Select(a => a.Account).ToArray();

                    result = _bankAccountsAdapter.UpdateXml(request.Id, docId, accounts, xml);

                    if (result)
                    {
                        scope.Complete();
                    }
                }

                if (result && validator.IsErrorsFound)
                {
                    _bankAccountsAdapter.SetStatus(request.Id, BankAccountRequestStatus.XmlValidationError);
                }
                if (!result)
                {
                    _bankAccountsAdapter.SetStatus(request.Id, BankAccountRequestStatus.Created);
                }
            }
            catch (Exception e)
            {
                _callback.Log(MessageType.Error, e.Message);
                _bankAccountsAdapter.SetStatus(request.Id, BankAccountRequestStatus.XmlValidationError);
            }
        }

        private class Validator
        {
            private readonly IBankAccountCallback _callback;

            public bool IsErrorsFound { get; private set; }

            public Validator(IBankAccountCallback callback)
            {
                _callback = callback;
                IsErrorsFound = false;
            }

            public void ValidationHandler(object sender, EventArgs arg)
            {
                IsErrorsFound = true;

                var validationArg = arg as ValidationEventArgs;
                if (validationArg != null)
                {
                    _callback.Log(MessageType.Error, validationArg.Message);
                    return;
                }
                var errorArg = arg as ErrorEventArgs;
                if (errorArg != null)
                {
                    _callback.Log(MessageType.Error, errorArg.GetException().Message);
                }
            }
        }
    }
}
