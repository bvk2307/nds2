﻿using CommonComponents.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public static class ReclaimXmlCatalogAddressHelper
    {
        public static CatalogAddress Create(string xsdShemaName)
        {
            CatalogAddress xsdSchemaAddres = new CatalogAddress(
                CatalogAddressSchemas.LogicalCatalog,
                "RoleCatalog",
                "NDS2Subsystem",
                "FileSystems",
                string.Format("XMLSchemas/{0}", xsdShemaName));

            return xsdSchemaAddres;
        }
    }
}
