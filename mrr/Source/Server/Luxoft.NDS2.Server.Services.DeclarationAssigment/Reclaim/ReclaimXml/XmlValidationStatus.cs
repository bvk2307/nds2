﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public enum XmlValidationStatus
    {
        Successfully = 1,
        Warning = 2,
        Error = 3
    }
}
