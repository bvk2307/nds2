﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public class XmlValidator : IXmlValidator
    {
        private ServiceHelpers _helper;

        private event ValidationEventHandler validationEventHandler;

        private XmlValidationResult _xmlValidationResult = new XmlValidationResult();

        private Dictionary<int, XmlValidationStatus> _validatiionStatuses = new Dictionary<int, XmlValidationStatus>()
        {
            { (int)XmlSeverityType.Error, XmlValidationStatus.Error },
            { (int)XmlSeverityType.Warning, XmlValidationStatus.Warning }
        };

        public XmlValidator(ServiceHelpers helper)
        {
            if (helper == null)
                throw new ArgumentNullException();

            _helper = helper;

            validationEventHandler += XmlValidatorValidationEventHandler;
        }

        private void XmlValidatorValidationEventHandler(object sender, ValidationEventArgs e)
        {
            _xmlValidationResult.Exception = e.Exception;
            _xmlValidationResult.Message = e.Message;
            _xmlValidationResult.ValidationStatus = _validatiionStatuses[(int)e.Severity];
        }

        public XmlValidationResult Validate(string xmlBody, CatalogAddress catalogAddress)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlBody);

            try
            {
                var catalogService = _helper.Services.Get<ICatalogService>();
                var contentStream = catalogService.CreateInstance<Stream>(catalogAddress);

                var reader = XmlReader.Create(contentStream);

                xmlDocument.Schemas.Add(null, reader);

                _xmlValidationResult.Clean();

                xmlDocument.Validate(validationEventHandler);
            }
            catch (Exception exception)
            {
                _xmlValidationResult.Exception = exception;
                _xmlValidationResult.Message = exception.Message;
                _xmlValidationResult.ValidationStatus = XmlValidationStatus.Error;
            }

            return (XmlValidationResult)_xmlValidationResult.Clone();
        }
    }
}
