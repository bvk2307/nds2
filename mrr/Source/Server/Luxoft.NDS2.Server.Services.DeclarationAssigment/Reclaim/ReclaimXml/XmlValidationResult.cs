﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public class XmlValidationResult : ICloneable
    {
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public XmlValidationStatus ValidationStatus { get; set; }

        public XmlValidationResult()
        {
            Clean();
        }

        public void Clean()
        {
            Exception = null;
            Message = String.Empty;
            ValidationStatus = XmlValidationStatus.Successfully;
        }

        public object Clone()
        {
            var result = new XmlValidationResult();
            result.Exception = this.Exception;
            result.Message = this.Message;
            result.ValidationStatus = this.ValidationStatus;

            return result;
        }
    }
}
