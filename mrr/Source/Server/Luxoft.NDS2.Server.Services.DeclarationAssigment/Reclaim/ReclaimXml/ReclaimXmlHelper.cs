﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public static class ReclaimXmlHelper
    {
        public static string GenerateXmlBody<T>(T entity)
        {
            string body = String.Empty;

            var xmlSerializerNamespaces = new XmlSerializerNamespaces();
            var xmlSerializer = new XmlSerializer(entity.GetType());
            xmlSerializerNamespaces.Add(String.Empty, String.Empty);

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };

            using (var textWriter = new Win1251StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlSerializer.Serialize(xmlWriter, entity, xmlSerializerNamespaces);
                }
                body = textWriter.ToString();
            }

            return body;
        }
    }

    public class Win1251StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.GetEncoding("windows-1251"); }
        }
    }
}
