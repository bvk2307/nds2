﻿using CommonComponents.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml
{
    public interface IXmlValidator
    {
        XmlValidationResult Validate(string xmlBody, CatalogAddress catalogAddress);
    }
}
