﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    class TaskReclaimBothSides : TaskReclaimBase, ITask
    {
        public TaskReclaimBothSides(
            ServiceHelpers serviceHelper,
            ReclaimQueue reclaimQueue,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimDiscrepancyAdapter reclaimDiscrepancyAdapter,
            IReclaimAdapter reclaimAdapter,
            IReclaimInvoiceAdapter reclaimInvoiceAdapter,
            IXmlValidator xmlValidator,
            string xsdSchemaName)
            : base(
                serviceHelper,
                reclaimQueue,
                reclaimLogAdapter,
                reclaimDiscrepancyAdapter,
                reclaimAdapter,
                reclaimInvoiceAdapter,
                xmlValidator,
                xsdSchemaName
            )
        {
        }

        public override IEnumerable<ReclaimDiscrepancy> SelectDiscrpanciesAll(long queueElemId)
        {
            IEnumerable<ReclaimDiscrepancy> side1_res = _reclaimDiscrepancyAdapter.SelectSideFirstAll(queueElemId);
            IEnumerable<ReclaimDiscrepancy> side2_res = _reclaimDiscrepancyAdapter.SelectSideSecondAll(queueElemId);

            IEnumerable<ReclaimDiscrepancy> result = side1_res.Concat<ReclaimDiscrepancy>(side2_res);

            return result;
        }
    }
}
