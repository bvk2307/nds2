﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    public abstract class TaskReclaimBase : ITask
    {
        protected readonly ServiceHelpers _serviceHelper;

        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        protected readonly IReclaimDiscrepancyAdapter _reclaimDiscrepancyAdapter;

        protected readonly IReclaimAdapter _reclaimAdapter;

        protected readonly IReclaimInvoiceAdapter _reclaimInvoiceAdapter;

        protected readonly IXmlValidator _xmlValidator;

        protected const string TITLE_CREATE_XML = "Формирование XML";

        protected const int PAGE_SIZE = 99;

        protected string _xsdSchemaName;

        public TaskReclaimBase(
            ServiceHelpers serviceHelper,
            ReclaimQueue reclaimQueue,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimDiscrepancyAdapter reclaimDiscrepancyAdapter,
            IReclaimAdapter reclaimAdapter,
            IReclaimInvoiceAdapter reclaimInvoiceAdapter,
            IXmlValidator xmlValidator,
            string xsdSchemaName)
        {
            if (serviceHelper == null)
                throw new ArgumentNullException("serviceHelper");

            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            if (reclaimDiscrepancyAdapter == null)
                throw new ArgumentNullException("reclaimDiscrepancyAdapter");

            if (reclaimAdapter == null)
                throw new ArgumentNullException("reclaimAdapter");

            if (reclaimInvoiceAdapter == null)
                throw new ArgumentNullException("reclaimInvoiceAdapter");

            if (xmlValidator == null)
                throw new ArgumentNullException("xmlValidator");

            if (string.IsNullOrWhiteSpace(xsdSchemaName))
                throw new ArgumentNullException("xsdSchemaName");

            _serviceHelper = serviceHelper;

            _reclaimQueue = reclaimQueue;

            _reclaimLogAdapter = reclaimLogAdapter;

            _reclaimDiscrepancyAdapter = reclaimDiscrepancyAdapter;

            _reclaimAdapter = reclaimAdapter;

            _reclaimInvoiceAdapter = reclaimInvoiceAdapter;

            _xmlValidator = xmlValidator;

            _xsdSchemaName = xsdSchemaName;
        }

        public abstract IEnumerable<ReclaimDiscrepancy> SelectDiscrpanciesAll(long queueElemId);

        public virtual void Execute()
        {
            LogInfo(TITLE_CREATE_XML, "Начало");

            ReclaimXmlDocument currentXmlDocument = null;

            try
            {
                ReclaimDeclaration currentTaxPayerKey = null;
                ContragentKey currentContragentKey = null;
                int countDiscrepancyCurrent = 0;
                bool hasError = false;

                foreach (var discrepancy in SelectDiscrpanciesAll(_reclaimQueue.Id))
                {
                    if (discrepancy.RegNumber == null)
                    {
                        continue;
                    }
                    // Поменялся НП для кого формируется АИ
                    if ((currentTaxPayerKey == null) ||
                        (currentTaxPayerKey.RegNumber != discrepancy.RegNumber.Value ||
                         currentTaxPayerKey.SonoCode != discrepancy.SonoCode))
                    {
                        countDiscrepancyCurrent = 0;

                        if (currentTaxPayerKey != null && currentContragentKey != null)
                        {
                            ValidateAndSave(currentXmlDocument, currentTaxPayerKey);

                            currentXmlDocument = null;
                        }

                        currentTaxPayerKey = CreateReclaimDeclaration(discrepancy);

                        CreateReclaimDocument(discrepancy, currentTaxPayerKey, out currentContragentKey, out currentXmlDocument);
                    }
                    else
                    {
                        // Достигли лимита на кол-во расхождений в одно АИ
                        if ((currentContragentKey == null) ||
                            countDiscrepancyCurrent == PAGE_SIZE)
                        {
                            countDiscrepancyCurrent = 0;

                            if (currentContragentKey != null)
                            {
                                ValidateAndSave(currentXmlDocument, currentTaxPayerKey);

                                currentXmlDocument = null;
                            }

                            CreateReclaimDocument(discrepancy, currentTaxPayerKey, out currentContragentKey, out currentXmlDocument);
                        }
                    }

                    InsertDiscrepancy(currentXmlDocument.Id, discrepancy, out hasError);

                    if (hasError)
                    {
                        currentXmlDocument.HasDatabaseError = hasError;
                        break;
                    }

                    currentXmlDocument.Append(discrepancy);
                    countDiscrepancyCurrent++;
                }

                if (currentTaxPayerKey != null && currentContragentKey != null)
                {
                    ValidateAndSave(currentXmlDocument, currentTaxPayerKey);
                }
            }
            catch (Exception exception)
            {
                LogError(TITLE_CREATE_XML, string.Format("Ошибка = {0}", exception.ToString()));

                if (currentXmlDocument != null)
                {
                    currentXmlDocument.HasGeneralError = true;

                    new ReclaimUpdater(_reclaimQueue, _reclaimLogAdapter, _reclaimAdapter)
                        .Update(currentXmlDocument);
                }
            }

            LogInfo(TITLE_CREATE_XML, "Завершение");
        }

        private ReclaimDeclaration CreateReclaimDeclaration(ReclaimDiscrepancy reclaimDiscrepancy)
        {
            return new ReclaimDeclaration()
            {
                RegNumber = reclaimDiscrepancy.RegNumber.Value,
                SonoCode = reclaimDiscrepancy.SonoCode,
                Inn = reclaimDiscrepancy.Inn,
                Kpp = reclaimDiscrepancy.Kpp,
                KppEffective = reclaimDiscrepancy.KppEffective,
                Zip = reclaimDiscrepancy.Zip,
                PeriodCode = reclaimDiscrepancy.PeriodCode,
                FiscalYear = reclaimDiscrepancy.FiscalYear
            };
        }

        private void CreateReclaimDocument(ReclaimDiscrepancy reclaimDiscrepancy, 
            ReclaimDeclaration reclaimDeclaration,
            out ContragentKey contragentKey,
            out ReclaimXmlDocument reclaimXmlDocument)
        {
            contragentKey = new ContragentKey()
            {
                Inn = reclaimDiscrepancy.ContractorInn,
                KppEffective = reclaimDiscrepancy.ContractorKppEffective
            };

            var docId = new ReclaimCreator(_reclaimQueue, reclaimDeclaration, _reclaimLogAdapter, _reclaimAdapter)
                .Create(contragentKey);

            reclaimXmlDocument = new ReclaimXmlDocument(docId,
                ReclaimOpenKnpXmlBuilderCreator
                    .Create(docId, _reclaimQueue, reclaimDeclaration, TaskExecutor.PeriodDictionary));

            reclaimXmlDocument.HasDatabaseError = false;
        }

        private void InsertDiscrepancy(long docId, ReclaimDiscrepancy reclaimDiscrepancy, out bool hasError)
        {
            try
            {
                _reclaimDiscrepancyAdapter.Insert(
                    docId,
                    reclaimDiscrepancy.DiscrepancyId,
                    reclaimDiscrepancy.RowKey,
                    reclaimDiscrepancy.Amount,
                    reclaimDiscrepancy.AmountPvp
                    );
                hasError = false;
            }
            catch (Exception exception)
            {
                hasError = true;
                string message = string.Format("Ошибка InsertDiscrepancy = {0}", exception.Message);
                LogError(TITLE_CREATE_XML, message);
            }
        }

        private void ValidateAndSave(
            ReclaimXmlDocument reclaimDocument, 
            ReclaimDeclaration reclaimDeclaration)
        {
            try
            {
                new ReclaimIncoices(_reclaimQueue, reclaimDeclaration, _reclaimLogAdapter, _reclaimInvoiceAdapter)
                    .InsertAll(reclaimDocument.Id);
            }
            catch (Exception exception)
            {
                reclaimDocument.HasDatabaseError = true;
                string message = string.Format("Ошибка InsertAllInvoices = {0}", exception.Message);
                LogError(TITLE_CREATE_XML, message);
            }

            reclaimDocument.XmlBody = new ReclaimBuilder(_reclaimQueue, reclaimDeclaration, _reclaimLogAdapter)
                .Build(reclaimDocument);

            reclaimDocument.HasSchemaValidationErrors = new ReclaimValidator(
                _reclaimQueue,
                reclaimDeclaration,
                _reclaimLogAdapter,
                _xmlValidator,
                _xsdSchemaName)
                .Validate(reclaimDocument.XmlBody);

            new ReclaimUpdater(_reclaimQueue, _reclaimLogAdapter, _reclaimAdapter)
                .Update(reclaimDocument);
        }

        protected void LogInfo(string title, string message)
        {
            _reclaimLogAdapter.LogInformation(title, message,
                null, null, null, _reclaimQueue.Id);
        }

        protected void LogError(string title, string message)
        {
            _reclaimLogAdapter.LogError(title, message,
               null, null, null, _reclaimQueue.Id);
        }

        private List<DictTaxPeriod> GetTaxPeriodDictionary()
        {
            return TableAdapterCreator
                .DictionaryTaxPeriodAdapter(_serviceHelper)
                .GetList();
        }
    }
}
