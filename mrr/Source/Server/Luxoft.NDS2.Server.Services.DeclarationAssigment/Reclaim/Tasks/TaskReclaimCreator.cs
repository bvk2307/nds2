﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    public static class TaskReclaimCreator
    {
        private static readonly string XSD_SCHEMA_ARTICLE_93_NAME = "TAX3EXCH_NDS2_CAM_02_01.xsd";

        public static ITask CreateTaskReclaimOpenKnp(
            ServiceHelpers serviceHelper,
            ReclaimQueue reclaimQueue,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimDiscrepancyAdapter reclaimDiscrepancyAdapter,
            IReclaimAdapter reclaimAdapter,
            IReclaimInvoiceAdapter reclaimInvoiceAdapter,
            IXmlValidator xmlValidator)
        {
            if (reclaimQueue.Side == 1)
            {
                return new TaskReclaimSideOne(
                            serviceHelper,
                            reclaimQueue,
                            reclaimLogAdapter,
                            reclaimDiscrepancyAdapter,
                            reclaimAdapter,
                            reclaimInvoiceAdapter,
                            xmlValidator,
                            XSD_SCHEMA_ARTICLE_93_NAME);
            }

            if (reclaimQueue.Side == 2)
            {
                return new TaskReclaimSideTwo(
                            serviceHelper,
                            reclaimQueue,
                            reclaimLogAdapter,
                            reclaimDiscrepancyAdapter,
                            reclaimAdapter,
                            reclaimInvoiceAdapter,
                            xmlValidator,
                            XSD_SCHEMA_ARTICLE_93_NAME);
            }

            if (reclaimQueue.Side == 3)
            {
                return new TaskReclaimBothSides(
                            serviceHelper,
                            reclaimQueue,
                            reclaimLogAdapter,
                            reclaimDiscrepancyAdapter,
                            reclaimAdapter,
                            reclaimInvoiceAdapter,
                            xmlValidator,
                            XSD_SCHEMA_ARTICLE_93_NAME);
            }

            return new TaskReclaimSideUnknow(reclaimQueue, reclaimLogAdapter);
        }
    }
}
