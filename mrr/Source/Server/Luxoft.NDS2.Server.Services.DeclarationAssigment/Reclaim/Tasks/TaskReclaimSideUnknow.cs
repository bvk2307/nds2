﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    public class TaskReclaimSideUnknow : ITask
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        public TaskReclaimSideUnknow(ReclaimQueue reclaimQueue, IReclaimLogAdapter reclaimLogAdapter)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            _reclaimQueue = reclaimQueue;

            _reclaimLogAdapter = reclaimLogAdapter;
        }

        public void Execute()
        {
            _reclaimLogAdapter.LogError("Формирование XML", "Неизвестный номер стороны",
                null, null, null, _reclaimQueue.Id);
        }
    }
}
