﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimBuilder
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly ReclaimDeclaration _reclaimDeclaration;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        private List<DictTaxPeriod> _taxPeriods;

        public ReclaimBuilder(
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IReclaimLogAdapter reclaimLogAdapter)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimDeclaration == null)
                throw new ArgumentNullException("reclaimDeclaration");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            _reclaimQueue = reclaimQueue;

            _reclaimDeclaration = reclaimDeclaration;

            _reclaimLogAdapter = reclaimLogAdapter;
        }

        public string Build(ReclaimXmlDocument reclaimDocument)
        {
            var xmlBody = reclaimDocument.ReclaimXmlBuilder.Build();

            _reclaimLogAdapter.LogInformation("Формирование XML", "Сформирован xml-документ",
                _reclaimDeclaration.Zip, _reclaimDeclaration.SonoCode, _reclaimDeclaration.RegNumber, _reclaimQueue.Id);

            return xmlBody;
        }
    }
}
