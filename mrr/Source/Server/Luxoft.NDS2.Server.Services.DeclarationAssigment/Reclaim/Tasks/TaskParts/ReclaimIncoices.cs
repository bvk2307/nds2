﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimIncoices
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly ReclaimDeclaration _reclaimDeclaration;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        protected readonly IReclaimInvoiceAdapter _reclaimInvoiceAdapter;

        public ReclaimIncoices(
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimInvoiceAdapter reclaimInvoiceAdapter)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimDeclaration == null)
                throw new ArgumentNullException("reclaimDeclaration");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            if (reclaimInvoiceAdapter == null)
                throw new ArgumentNullException("reclaimInvoiceAdapter");

            _reclaimQueue = reclaimQueue;

            _reclaimDeclaration = reclaimDeclaration;

            _reclaimLogAdapter = reclaimLogAdapter;

            _reclaimInvoiceAdapter = reclaimInvoiceAdapter;
        }

        public void InsertAll(long docId)
        {
            _reclaimInvoiceAdapter.InsertAll(docId);

            _reclaimLogAdapter.LogInformation("Формирование XML", "Добавлены счет-факутры",
                _reclaimDeclaration.Zip, _reclaimDeclaration.SonoCode, _reclaimDeclaration.RegNumber, _reclaimQueue.Id);
        }
    }
}
