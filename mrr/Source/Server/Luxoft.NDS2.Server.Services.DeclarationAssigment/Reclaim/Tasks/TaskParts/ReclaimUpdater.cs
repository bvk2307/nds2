﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimUpdater
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        protected readonly IReclaimAdapter _reclaimAdapter;

        public ReclaimUpdater(
            ReclaimQueue reclaimQueue,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimAdapter reclaimAdapter)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            if (reclaimAdapter == null)
                throw new ArgumentNullException("reclaimAdapter");

            _reclaimQueue = reclaimQueue;

            _reclaimLogAdapter = reclaimLogAdapter;

            _reclaimAdapter = reclaimAdapter;
        }

        public void Update(ReclaimXmlDocument reclaimDocument)
        {
            long discrepancyCount = reclaimDocument.ReclaimXmlBuilder.Count();
            decimal discrepancyAmount = reclaimDocument.ReclaimXmlBuilder.Amount();
            decimal discrepancyAmountPvp = reclaimDocument.ReclaimXmlBuilder.AmountPvp();

            _reclaimAdapter.Update(reclaimDocument.Id, 
                discrepancyCount, 
                discrepancyAmount,
                discrepancyAmountPvp,
                reclaimDocument.HasSchemaValidationErrors,
                reclaimDocument.HasDatabaseError,
                reclaimDocument.HasGeneralError,
                reclaimDocument.XmlBody, 
                (int)DocumentProcessed.NotProcessed);

            _reclaimLogAdapter.LogInformation("Формирование XML", "Обновлен документ - успешно",
                null, null, null, _reclaimQueue.Id);
        }
    }
}
