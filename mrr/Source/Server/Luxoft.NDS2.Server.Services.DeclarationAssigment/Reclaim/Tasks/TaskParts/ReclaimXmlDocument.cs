﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimXmlDocument
    {
        public long Id { get; private set; }

        public IReclaimXmlBuilder ReclaimXmlBuilder { get; private set; }

        public string XmlBody { get; set; }

        public bool HasSchemaValidationErrors { get; set; }

        public bool HasDatabaseError { get; set; }

        public bool HasGeneralError { get; set; }

        public ReclaimXmlDocument(long docId, IReclaimXmlBuilder reclaimXmlBuilder)
        {
            Id = docId;

            ReclaimXmlBuilder = reclaimXmlBuilder;
        }

        public void Append(ReclaimDiscrepancy reclaimDicreapncy)
        {
            ReclaimXmlBuilder.Append(reclaimDicreapncy);
        }
    }
}
