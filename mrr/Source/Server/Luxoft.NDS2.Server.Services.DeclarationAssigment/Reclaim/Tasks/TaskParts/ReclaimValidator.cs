﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimValidator
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly ReclaimDeclaration _reclaimDeclaration;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        protected readonly IXmlValidator _xmlValidator;

        protected string _xsdSchemaName;

        protected const string TITLE_CREATE_XML = "Формирование XML";

        public ReclaimValidator(
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IReclaimLogAdapter reclaimLogAdapter,
            IXmlValidator xmlValidator,
            string xsdSchemaName)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimDeclaration == null)
                throw new ArgumentNullException("reclaimDeclaration");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            if (xmlValidator == null)
                throw new ArgumentNullException("xmlValidator");

            if (string.IsNullOrWhiteSpace(xsdSchemaName))
                throw new ArgumentNullException("xsdSchemaName");

            _reclaimQueue = reclaimQueue;

            _reclaimDeclaration = reclaimDeclaration;

            _reclaimLogAdapter = reclaimLogAdapter;

            _xmlValidator = xmlValidator;

            _xsdSchemaName = xsdSchemaName;
        }

        public bool Validate(string xmlBody)
        {
            var xmlValidationResult = _xmlValidator
                .Validate(xmlBody, ReclaimXmlCatalogAddressHelper.Create(_xsdSchemaName));

            var hasSchemaValidationErrors = false;
            if (xmlValidationResult.ValidationStatus == XmlValidationStatus.Error)
            {
                hasSchemaValidationErrors = true;
                LogError(TITLE_CREATE_XML,
                    string.Format("Валидация xml-документа, статус = \"ошибка\", \"{0}\"", xmlValidationResult.Message));
            }
            else
            {
                LogInfo(TITLE_CREATE_XML,
                    string.Format("Валидация xml-документа - успешно"));
            }

            return hasSchemaValidationErrors;
        }

        private void LogInfo(string title, string message)
        {
            _reclaimLogAdapter.LogInformation(title, message,
                _reclaimDeclaration.Zip, _reclaimDeclaration.SonoCode, _reclaimDeclaration.RegNumber, _reclaimQueue.Id);
        }

        private void LogError(string title, string message)
        {
            _reclaimLogAdapter.LogError(title, message,
                _reclaimDeclaration.Zip, _reclaimDeclaration.SonoCode, _reclaimDeclaration.RegNumber, _reclaimQueue.Id);
        }
    }
}
