﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ReclaimCreator
    {
        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly ReclaimDeclaration _reclaimDeclaration;

        protected readonly IReclaimLogAdapter _reclaimLogAdapter;

        protected readonly IReclaimAdapter _reclaimAdapter;

        public ReclaimCreator(
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimAdapter reclaimAdapter)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException("reclaimQueue");

            if (reclaimDeclaration == null)
                throw new ArgumentNullException("reclaimDeclaration");

            if (reclaimLogAdapter == null)
                throw new ArgumentNullException("reclaimLogAdapter");

            if (reclaimAdapter == null)
                throw new ArgumentNullException("reclaimAdapter");

            _reclaimQueue = reclaimQueue;

            _reclaimDeclaration = reclaimDeclaration;

            _reclaimLogAdapter = reclaimLogAdapter;

            _reclaimAdapter = reclaimAdapter;
        }

        public long Create(ContragentKey contragentKey)
        {
            var docId = _reclaimAdapter
                .Insert(
                    _reclaimDeclaration.RegNumber,
                    (int)DocumentType.Reclaim,
                    _reclaimQueue.Side,
                    _reclaimDeclaration.SonoCode,
                    0,
                    0,
                    (int)DocumentProcessed.NotProcessed,
                    _reclaimDeclaration.Inn,
                    _reclaimDeclaration.KppEffective,
                    _reclaimQueue.Id,
                    _reclaimDeclaration.Zip,
                    Period.GetQuarterByCode(_reclaimDeclaration.PeriodCode),
                    Int32.Parse(_reclaimDeclaration.FiscalYear.Trim()));

            _reclaimLogAdapter.LogInformation(
                "Формирование XML", 
                string.Format("Создан документ, docId = {0}, для контрагента ИНН = {1}, КПП эффективный = {2}",
                    docId, contragentKey.Inn, contragentKey.KppEffective),
                _reclaimDeclaration.Zip, _reclaimDeclaration.SonoCode, _reclaimDeclaration.RegNumber, _reclaimQueue.Id);

            return docId;
        }
    }
}
