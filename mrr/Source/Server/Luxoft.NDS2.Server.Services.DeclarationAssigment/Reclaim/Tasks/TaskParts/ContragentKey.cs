﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public class ContragentKey
    {
        public string Inn { get; set; }
        public string KppEffective { get; set; }
    }
}
