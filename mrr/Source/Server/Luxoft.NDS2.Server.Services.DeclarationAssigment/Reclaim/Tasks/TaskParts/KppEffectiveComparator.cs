﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts
{
    public static class KppEffectiveComparator
    {
        public static bool Compare(string kppEffectiveFirst, string kppEffectiveSecond)
        {
            bool ret = false;

            if (string.IsNullOrEmpty(kppEffectiveFirst) ||
                string.IsNullOrEmpty(kppEffectiveSecond))
                ret = true;
            else
            {
                ret = kppEffectiveFirst == kppEffectiveSecond;
            }

            return ret;
        }
    }
}
