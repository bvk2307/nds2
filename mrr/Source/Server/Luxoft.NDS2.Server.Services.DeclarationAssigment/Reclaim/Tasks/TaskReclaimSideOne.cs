﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    public class TaskReclaimSideOne : TaskReclaimBase, ITask
    {
        public TaskReclaimSideOne(
            ServiceHelpers serviceHelper,
            ReclaimQueue reclaimQueue,
            IReclaimLogAdapter reclaimLogAdapter,
            IReclaimDiscrepancyAdapter reclaimDiscrepancyAdapter,
            IReclaimAdapter reclaimAdapter,
            IReclaimInvoiceAdapter reclaimInvoiceAdapter,
            IXmlValidator xmlValidator,
            string xsdSchemaName)
            : base(
                serviceHelper,
                reclaimQueue,
                reclaimLogAdapter,
                reclaimDiscrepancyAdapter,
                reclaimAdapter,
                reclaimInvoiceAdapter,
                xmlValidator,
                xsdSchemaName
            )
        {
        }

        public override IEnumerable<ReclaimDiscrepancy> SelectDiscrpanciesAll(long queueElemId)
        {
            return _reclaimDiscrepancyAdapter.SelectSideFirstAll(queueElemId);
        }
    }
}
