﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks
{
    public interface ITask
    {
        void Execute();
    }
}
