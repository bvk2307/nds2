﻿using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim
{
    internal class TaskExecutor 
    {
        private ServiceHelpers _serviceHelpers;

        private ConcurrentBag<long> queueProcessed = new ConcurrentBag<long>();

        private int _threadFinish = 0;

        public static IPeriodDictionary PeriodDictionary;

        public TaskExecutor(ServiceHelpers serviceHelpers)
        {
            _serviceHelpers = serviceHelpers;

            var periods = TableAdapterCreator
                    .DictionaryTaxPeriodAdapter(_serviceHelpers)
                    .GetList();

            PeriodDictionary = new PeriodDictionary(periods);
        }

        public void Run(int countThread)
        {
            for (int i = -1; ++i < countThread; )
                ThreadPool.QueueUserWorkItem(DoTask);
        }

        public List<long> GetQueueProcessed()
        {
            return queueProcessed.ToList();
        }

        public int GetThreadFinish()
        {
            return _threadFinish;
        }

        private void ThreadFinish()
        {
            Interlocked.Increment(ref _threadFinish);
        }

        private void DoTask(object instance)
        {
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_serviceHelpers))
            {
                var reclaimQueueAdapter = ReclaimQueueAdapterCreator
                    .Create(_serviceHelpers, connectionFactory);

                var reclaimLogAdapter = ReclaimLogAdapterCreator
                    .Create(_serviceHelpers, connectionFactory);

                var reclaimDiscrepancyAdpater = ReclaimDiscrepancyAdapterCreator
                    .Create(_serviceHelpers, connectionFactory);

                var reclaimAdpater = ReclaimAdapterCreator
                    .Create(_serviceHelpers, connectionFactory);

                var reclaimInvoiceAdpater = ReclaimInvoiceAdapterCreator
                    .Create(_serviceHelpers, connectionFactory);

                IXmlValidator xmlValidator = new XmlValidator(_serviceHelpers);

                while(true)
                {
                    ReclaimQueue queue;

                    try
                    {
                        var result = reclaimQueueAdapter
                            .TryPull(out queue);

                        if (result && queue != null)
                        {
                            TaskReclaimCreator
                                .CreateTaskReclaimOpenKnp(
                                    _serviceHelpers,
                                    queue,
                                    reclaimLogAdapter,
                                    reclaimDiscrepancyAdpater,
                                    reclaimAdpater,
                                    reclaimInvoiceAdpater,
                                    xmlValidator)
                                .Execute();

                            reclaimQueueAdapter
                                .UpdateQueue(queue.Id, ReclaimQueueState.Processed, ReclaimLocked.Processed);

                            queueProcessed.Add(queue.Id);
                        }
                        else
                        {
                            ThreadFinish();
                            break;
                        }
                    }
                    catch (Exception exception)
                    {
                        var textError = string.Format("ReclaimQueue Ошибка: ", exception.ToString());
                        Console.WriteLine(textError);
                        _serviceHelpers.LogError(textError, "Luxoft.NDS2.Server.TaskScheduler.Reclaim.DoTask", exception);

                        ThreadFinish();
                        break;
                    }
                }
            }
        }
    }
}
