﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    public abstract class ReclaimXmlBuilderBase : IReclaimXmlBuilder
    {
        protected long _docId;

        protected readonly ReclaimQueue _reclaimQueue;

        protected readonly ReclaimDeclaration _reclaimDeclaration;

        protected readonly List<ReclaimDiscrepancy> _reclaimDiscrepacies = new List<ReclaimDiscrepancy>();

        protected readonly IPeriodDictionary _periodDictionary;

        public ReclaimXmlBuilderBase(
            long docId,
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IPeriodDictionary periodDictionary)
        {
            if (reclaimQueue == null)
                throw new ArgumentNullException();

            if (reclaimDeclaration == null)
                throw new ArgumentNullException();

            if (periodDictionary == null)
                throw new ArgumentNullException();

            _docId = docId;
            _reclaimQueue = reclaimQueue;
            _reclaimDeclaration = reclaimDeclaration;
            _periodDictionary = periodDictionary;
        }

        public abstract string Build();

        public void Append(ReclaimDiscrepancy reclaimDiscrepancy)
        {
            _reclaimDiscrepacies.Add(reclaimDiscrepancy);
        }

        public int Count()
        {
            return _reclaimDiscrepacies.Count();
        }

        public decimal Amount()
        {
            return _reclaimDiscrepacies.Sum(p => p.Amount.GetValueOrDefault(0));
        }

        public decimal AmountPvp()
        {
            return _reclaimDiscrepacies.Sum(p => p.AmountPvp.GetValueOrDefault(0));
        }

        protected int ConvertToInt(string value)
        {
            int retVal;

            if (!int.TryParse(value, out retVal))
            {
                throw new ArgumentException(string.Format("ошибка конвертации переменной fiscalYear из string в int, value = \"{0}\"", value));
            }

            return retVal;
        }
    }
}
