﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    public static class ReclaimOpenKnpXmlBuilderCreator
    {
        public static IReclaimXmlBuilder Create(
            long docId, 
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IPeriodDictionary periodDictionary)
        {
            return new ReclaimOpenKnpXmlBuilder(docId, reclaimQueue, reclaimDeclaration, periodDictionary);
        }
    }
}
