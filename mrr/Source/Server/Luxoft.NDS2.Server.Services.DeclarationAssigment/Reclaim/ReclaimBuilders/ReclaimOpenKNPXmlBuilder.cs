﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.Reclaim93;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    public class ReclaimOpenKnpXmlBuilder : ReclaimXmlBuilderBase, IReclaimXmlBuilder
    {
        public ReclaimOpenKnpXmlBuilder(
            long docId,
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IPeriodDictionary periodDictionary)
            : base(docId, reclaimQueue, reclaimDeclaration, periodDictionary)
        {
        }

        public override string Build()
        {
            var document = new Истреб93();

            document.КодНО = _reclaimDeclaration.SonoCode;
            document.РегНомДек = _reclaimDeclaration.RegNumber.ToString();
            document.УчНомИстреб = _docId.ToString();
            document.ТипИнф = Истреб93ТипИнф.TAX3EXCH_NDS2_CAM_02;

            var documentTypes = new List<ДокТип>();

            FillInvoices(documentTypes);

            document.Истреб93Док = documentTypes.ToArray();

            return ReclaimXmlHelper.GenerateXmlBody<Истреб93>(document);
        }

        private void FillInvoices(List<ДокТип> documentTypes)
        {
            foreach (var itemInvoice in _reclaimDiscrepacies)
            {
                ДокТип documentType;

                if (!string.IsNullOrEmpty(itemInvoice.CorrectionNum))
                {
                    if (itemInvoice.CorrectionDate.HasValue)
                    {
                        documentType = CreateDocumentTypeCorrectionInvoiceDate(
                            itemInvoice.CorrectionDate.Value,
                            itemInvoice.CorrectionNum);
                    }
                    else
                    {
                        documentType = CreateDocumentTypeCorrectionInvoicePeriod(
                            _periodDictionary.Get(itemInvoice.PeriodCode),
                            ConvertToInt(itemInvoice.FiscalYear),
                            itemInvoice.CorrectionNum);
                    }
                }
                else 
                {
                    if (itemInvoice.InvoiceDate.HasValue)
                    {
                        documentType = CreateDocumentTypeInvoiceDate(
                            itemInvoice.InvoiceDate.Value,
                            itemInvoice.InvoiceNum);
                    }
                    else
                    {
                        documentType = CreatreDocumentTypeInvoicePeriod(
                            _periodDictionary.Get(itemInvoice.PeriodCode),
                            ConvertToInt(itemInvoice.FiscalYear),
                            itemInvoice.InvoiceNum);
                    }
                }

                documentTypes.Add(documentType);
            }
        }

        private ДокТип CreatreDocumentTypeInvoicePeriod(
            PeriodBase period, 
            int year,
            string documentNumber)
        {
            var periodDocument = new ДокТипСФПериодДок();
            periodDocument.НачПер = XmlFieldFormatter.FormatDateTime(period.BeginDate(year));
            periodDocument.ОконПер = XmlFieldFormatter.FormatDateTime(period.EndDate(year));

            var documentTypeInvoice = new ДокТипСФ();
            documentTypeInvoice.Item = periodDocument;
            documentTypeInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeInvoice);
        }

        private ДокТип CreateDocumentTypeInvoiceDate(
            DateTime docuemntDate,
            string documentNumber)
        {
            var documentTypeInvoice = new ДокТипСФ();
            documentTypeInvoice.Item = XmlFieldFormatter.FormatDateTime(docuemntDate);
            documentTypeInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeInvoice);
        }

        private ДокТип CreateDocumentTypeCorrectionInvoicePeriod(
            PeriodBase period, 
            int year,
            string documentNumber)
        {
            var periodDocument = new ДокТипКСФПериодДок();
            periodDocument.НачПер = XmlFieldFormatter.FormatDateTime(period.BeginDate(year));
            periodDocument.ОконПер = XmlFieldFormatter.FormatDateTime(period.EndDate(year));

            var documentTypeCorrectionInvoice = new ДокТипКСФ();
            documentTypeCorrectionInvoice.Item = periodDocument;
            documentTypeCorrectionInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeCorrectionInvoice); 
        }

        private ДокТип CreateDocumentTypeCorrectionInvoiceDate(
            DateTime documentDate,
            string documentNumber)
        {
            var documentTypeCorrectionInvoice = new ДокТипКСФ();
            documentTypeCorrectionInvoice.Item = XmlFieldFormatter.FormatDateTime(documentDate);
            documentTypeCorrectionInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeCorrectionInvoice);
        }

        private ДокТип CreateDocumentType(object item)
        {
            var documentType = new ДокТип();
            documentType.Item = item;

            return documentType;
        }
    }
}
