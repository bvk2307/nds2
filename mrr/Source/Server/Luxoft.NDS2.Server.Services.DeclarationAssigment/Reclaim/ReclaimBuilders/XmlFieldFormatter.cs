﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    public static class XmlFieldFormatter
    {
        public static string FormatDateTime(DateTime? dateTime)
        {
            string retValue = String.Empty;

            if (dateTime != null && dateTime.Value > DateTime.MinValue)
                retValue = string.Format("{0:00}.{1:00}.{2:0000}", dateTime.Value.Day, dateTime.Value.Month, dateTime.Value.Year);

            return retValue;
        }
    }
}
