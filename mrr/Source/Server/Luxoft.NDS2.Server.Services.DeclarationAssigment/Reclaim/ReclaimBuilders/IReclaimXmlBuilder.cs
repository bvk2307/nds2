﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    public interface IReclaimXmlBuilder
    {
        string Build();
        void Append(ReclaimDiscrepancy reclaimDiscrepancy);
        int Count();
        decimal Amount();
        decimal AmountPvp();
    }
}
