﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.Reclaim931;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders
{
    [Obsolete("по требованиям отменили формирование документа по ст.93.1 (когда у 2-ой стороны закрыта КНП)")]
    public class ReclaimCloseKnpXmlBuilder : ReclaimXmlBuilderBase, IReclaimXmlBuilder
    {
        public ReclaimCloseKnpXmlBuilder(
            long docId,
            ReclaimQueue reclaimQueue,
            ReclaimDeclaration reclaimDeclaration,
            IPeriodDictionary periodDictionary)
            : base(docId, reclaimQueue, reclaimDeclaration, periodDictionary)
        {
        }

        public override string Build()
        {
            var document = new Истреб931();

            document.КодНО = _reclaimDeclaration.SonoCode;
            document.РегНомДек = _reclaimDeclaration.RegNumber.ToString();
            document.УчНомИстреб = _docId.ToString();
            document.ТипИнф = Истреб931ТипИнф.TAX3EXCH_NDS2_CAM_03;

            document.КодНОИсполн = _reclaimDeclaration.SonoCode;
            document.СведИст = new Истреб931СведИст();

            var reclaimOrganization = new Истреб931СведИстОргИст();
            reclaimOrganization.ИННЮЛ = "ИНН контрагента";              //--- надо доработать
            reclaimOrganization.КПП = "КПП контрагента";                //--- надо доработать
            reclaimOrganization.НаимОрг = "Наименование конрагента";    //--- надо доработать

            document.СведИст.Item = reclaimOrganization;

            var documentTypes = new List<ДокТип>();

            FillInvoices(documentTypes);

            document.Истреб931Док = documentTypes.ToArray();

            return ReclaimXmlHelper.GenerateXmlBody<Истреб931>(document);
        }

        private void FillInvoices(List<ДокТип> documentTypes)
        {
            foreach (var itemInvoice in _reclaimDiscrepacies)
            {
                ДокТип documentType = null;

                if (!string.IsNullOrEmpty(itemInvoice.CorrectionNum))
                {
                    if (itemInvoice.CorrectionDate != null)
                    {
                        documentType = CreateDocumentTypeCorrectionInvoiceDate(
                            (DateTime)itemInvoice.CorrectionDate,
                            itemInvoice.CorrectionNum);
                    }
                    else
                    {
                        documentType = CreateDocumentTypeCorrectionInvoicePeriod(
                            _periodDictionary.Get(itemInvoice.PeriodCode),
                            ConvertToInt(itemInvoice.FiscalYear),
                            itemInvoice.CorrectionNum);
                    }
                }
                else
                {
                    if (itemInvoice.InvoiceDate != null)
                    {
                        documentType = CreateDocumentTypeInvoiceDate(
                            (DateTime)itemInvoice.InvoiceDate,
                            itemInvoice.InvoiceNum);
                    }
                    else
                    {
                        documentType = CreatreDocumentTypeInvoicePeriod(
                            _periodDictionary.Get(itemInvoice.PeriodCode),
                            ConvertToInt(itemInvoice.FiscalYear),
                            itemInvoice.InvoiceNum);
                    }
                }

                documentTypes.Add(documentType);
            }
        }

        private ДокТип CreatreDocumentTypeInvoicePeriod(
            PeriodBase period,
            int year,
            string documentNumber)
        {
            var periodDocument = new ДокТипСФПериодДок();
            periodDocument.НачПер = XmlFieldFormatter.FormatDateTime(period.BeginDate(year));
            periodDocument.ОконПер = XmlFieldFormatter.FormatDateTime(period.EndDate(year));

            var documentTypeInvoice = new ДокТипСФ();
            documentTypeInvoice.Item = periodDocument;
            documentTypeInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeInvoice);
        }

        private ДокТип CreateDocumentTypeInvoiceDate(
            DateTime docuemntDate,
            string documentNumber)
        {
            var documentTypeInvoice = new ДокТипСФ();
            documentTypeInvoice.Item = XmlFieldFormatter.FormatDateTime(docuemntDate);
            documentTypeInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeInvoice);
        }

        private ДокТип CreateDocumentTypeCorrectionInvoicePeriod(
            PeriodBase period,
            int year,
            string documentNumber)
        {
            var periodDocument = new ДокТипКСФПериодДок();
            periodDocument.НачПер = XmlFieldFormatter.FormatDateTime(period.BeginDate(year));
            periodDocument.ОконПер = XmlFieldFormatter.FormatDateTime(period.EndDate(year));

            var documentTypeCorrectionInvoice = new ДокТипКСФ();
            documentTypeCorrectionInvoice.Item = periodDocument;
            documentTypeCorrectionInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeCorrectionInvoice);
        }

        private ДокТип CreateDocumentTypeCorrectionInvoiceDate(
            DateTime documentDate,
            string documentNumber)
        {
            var documentTypeCorrectionInvoice = new ДокТипКСФ();
            documentTypeCorrectionInvoice.Item = XmlFieldFormatter.FormatDateTime(documentDate);
            documentTypeCorrectionInvoice.НомДок = documentNumber;

            return CreateDocumentType(documentTypeCorrectionInvoice);
        }

        private ДокТип CreateDocumentType(object item)
        {
            var documentType = new ДокТип();
            documentType.Item = item;

            return documentType;
        }
    }
}
