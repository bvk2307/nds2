﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.TaskScheduler.Reclaim
{
    internal class QueueHandler
    {
        private ServiceHelpers _serviceHelpers;

        public QueueHandler(ServiceHelpers serviceHelpers)
        {
            _serviceHelpers = serviceHelpers;
        }

        public void Execute()
        {
            var exists = ExistsInQueue();

            Console.WriteLine(string.Format("ReclaimQueue Наличие элементов в очереди рег.номеров готовых для обработки = {0}", exists ? "ДА" : "НЕТ"));

            if (exists)
            {
                var taskExecutor = new TaskExecutor(_serviceHelpers);

                var countThread = GetReclaimJobThreadCountMax();

                taskExecutor.Run(countThread);

                while(true)
                {
                    var countThreadFinish = taskExecutor.GetThreadFinish();

                    var countQueueProcessed = taskExecutor.GetQueueProcessed().Count();

                    Console.WriteLine(string.Format("ReclaimQueue кол-во обработанных элементов очереди рег. номеров = {0}", countQueueProcessed));

                    Console.WriteLine(string.Format("ReclaimQueue кол-во работющих задач = {0}", countThread - countThreadFinish));

                    if (countThreadFinish < countThread)
                        Thread.Sleep(2000);
                    else
                        break;
                }
            }
        }

        private int GetReclaimJobThreadCountMax()
        {
            var configuration = TableAdapterCreator
                .Configuration(_serviceHelpers)
                .Read();
            int ret = 0;
            if (configuration != null && 
                configuration.ReclaimJobThreadCountMax != null)
            {
                ret = configuration.ReclaimJobThreadCountMax.Value;
            }
            return ret;
        }

        private bool ExistsInQueue()
        {
            var rez = _serviceHelpers.DoEx(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_serviceHelpers))
                {
                    return ReclaimQueueAdapterCreator
                        .Create(_serviceHelpers, connectionFactory)
                        .ExistsInQueue();
                }
            });
            if (rez.Status == ResultStatus.Success)
            {
                return rez.Result;
            }
            else
                return false;
        }
    }
}
