﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO.ExportExcel;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Packages;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Excel;
using Quartz;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class ExportExcelJob : IJob
    {
        private const string FileNamePattern = "{0}Export_{2:yyyy-MM-dd}_{1}.xlsx";

        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers) context.Scheduler.Context["ServiceHelpers"];
            Task.Factory.StartNew(() =>
            {
                var processId = Guid.NewGuid().ToString();
                Console.WriteLine("Экспорт в Excel, ID процесса: {0},\n\tзапуск: {1}(UTC)", processId, DateTime.UtcNow);
                helper.LogNotification(string.Format("Экспорт в Excel, {0}", processId), "ExportExcelJob.Execute");
                helper.Do(() => Work(helper, processId));
                helper.LogNotification(string.Format("Экспорт в Excel завершен, {0}", processId));
                Console.WriteLine("Экспорт в Excel, ID процесса: {0},\n\tзавершен {1}(UTC)", processId, DateTime.UtcNow);
            });
        }

        class ExportCallback : IExportSelectionDiscrepancyCallback
        {
            private readonly IExportExcelPackageAdapter _packageAdapter;
            private readonly long _itemId;

            public ExportCallback(IExportExcelPackageAdapter packageAdapter, long itemId)
            {
                _packageAdapter = packageAdapter;
                _itemId = itemId;
            }

            public void LogExportedRows(int count)
            {
                _packageAdapter.Update(
                    _itemId,
                    ExportExcelState.Success,
                    count,
                    string.Empty,
                    string.Empty);
            }
            public bool CheckCanceled()
            {
                var info = _packageAdapter.FindById(_itemId);
                return !string.IsNullOrEmpty(info.ERROR_DESCRIPTION);
            }
        }

        private static void Export(ServiceHelpers helper, IExportExcelPackageAdapter packageAdapter,
            ExportExcelItem item)
        {
            try
            {
                helper.LogNotification(
                    string.Format(
                        "Экспорт в Excel списка расхождений для выборки {0}, SID пользователя: {1}",
                        item.SELECTION_ID,
                        item.USER_SID)
                    );
                var exporter = new ExportSelectionDiscrepancy(helper, item);

                var fileName = string.Format(FileNamePattern, packageAdapter.GetExportPath(), item.QUEUE_ITEM_ID,
                    item.STARTED);
                var result = exporter.DoExport(fileName, new ExportCallback(packageAdapter, item.QUEUE_ITEM_ID));
                packageAdapter.Update(item.QUEUE_ITEM_ID,
                    ExportExcelState.Success,
                    result,
                    fileName,
                    string.Empty);
                helper.LogNotification(
                    string.Format(
                        "Экспорт в Excel списка расхождений для выборки {0} завершен, выгружено расхождений: {1}",
                        item.SELECTION_ID,
                        result)
                    );
            }
            catch (Exception e)
            {
                packageAdapter.Update(item.QUEUE_ITEM_ID, ExportExcelState.Failed, 0, string.Empty, e.Message);
            }
        }

        private static void Work(ServiceHelpers helper, string processId)
        {
            var packageAdapter = TableAdapterCreator.ExportExcelPackageAdapter(helper);
            var list = new List<Task>();
            ExportExcelItem dequeuedItem;

            do
            {
                dequeuedItem = packageAdapter.Dequeue(processId);

                if (dequeuedItem != null)
                {
                    var item = dequeuedItem;
                    var t = Task.Factory.StartNew(() => Export(helper, packageAdapter, item));
                    list.Add(t);
                }

            } while (dequeuedItem != null);

            Task.WaitAll(list.ToArray());
        }

    }

}
