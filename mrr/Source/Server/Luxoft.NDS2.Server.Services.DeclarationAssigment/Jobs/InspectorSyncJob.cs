﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using CommonComponents.Directory;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Quartz;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class InspectorSyncJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers) context.Scheduler.Context["ServiceHelpers"];
            Console.WriteLine("IDA: Начало синхронизации списка инспекторов. {0}(UTC)", DateTime.UtcNow);
            helper.LogNotification("IDA: Начало синхронизации списка инспекторов", "InspectorSyncJob.Execute");
            helper.Do(() =>
            {
                var inspectorAdapter = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(helper);
                var sonoCodes = TableAdapterCreator.SonoAdapter(helper).GetSonoCodes();
                var inspectorsFromCsud = GetInspectorsFromCsud(helper, sonoCodes);
                var inspectorsFromMrr = inspectorAdapter.GetActiveInspectors();
                var inspetorsToDeactivate =
                    inspectorsFromMrr.Except(inspectorsFromCsud, new InspectorAssignmentComparer()).ToArray();
                var inspetorsToAdd =
                    inspectorsFromCsud.Except(inspectorsFromMrr, new InspectorAssignmentComparer()).ToArray();
                foreach (var inspector in inspetorsToDeactivate)
                {
                    helper.Do(() =>
                    {
                        inspector.UpdatedBy = "InspectorSyncJob";
                        inspectorAdapter.DeactivateAssignment(inspector);
                    });
                }
                foreach (var inspector in inspetorsToAdd)
                {
                    helper.Do(
                        () =>
                        { 
                            inspector.UpdatedBy = "InspectorSyncJob";
                            inspectorAdapter.AddAssignment(inspector);
                        });
                }
            });
            helper.LogNotification("IDA: Окончание синхронизации списка инспекторов");
            Console.WriteLine("IDA: Окончание синхронизации списка инспекторов {0}(UTC)", DateTime.UtcNow);
            Console.WriteLine("IDA: Следующее время запуска: {0}(UTC)", context.NextFireTimeUtc);
        }

        private static List<InspectorSonoAssignment> GetInspectorsFromCsud(ServiceHelpers helper,
            IEnumerable<string> sonoCodes)
        {
            var inpectors = new List<InspectorSonoAssignment>();
            var authProvider = helper.Services.Get<IAuthorizationProvider>();
            var userInfoService = helper.Services.Get<IUserInfoService>();
            foreach (var sonoCode in sonoCodes)
            {
                helper.Do(() =>
                {
                    var inspectors = authProvider.GetStructContextUsersByRole(sonoCode,
                        Constants.SystemPermissions.Operations.
                            RoleInspector);
                    foreach (var inspector in inspectors)
                    {
                        var displayName = inspector.UserSid;
                        var logonName = inspector.UserSid;
                        helper.Do(() =>
                        {
                            try
                            {
                                var userInfo =
                                    userInfoService.GetUserInfoBySID(inspector.UserSid);
                                displayName = userInfo.DisplayName;
                                logonName = userInfo.LogonName;
                            }
                            catch (COMException ex)
                            {
                                helper.LogError(
                                    string.Format(
                                        "IDA: Не удалось получить полное имя инспектора sid {0}",
                                        inspector.UserSid),
                                    "InspectorSyncJob.GetInspectorsFromCsud", ex);
                            }
                        });
                        inpectors.Add(new InspectorSonoAssignment
                        {
                            SID = inspector.UserSid,
                            SonoCode = sonoCode,
                            EmployeeNum = logonName,
                            Name = displayName
                        });
                    }
                });
            }
            return inpectors;
        }

        private class InspectorAssignmentComparer : EqualityComparer<InspectorSonoAssignment>
        {
            public override bool Equals(InspectorSonoAssignment x, InspectorSonoAssignment y)
            {
                return string.Compare(x.SID, y.SID, StringComparison.Ordinal) == 0 &&
                       string.Compare(x.SonoCode, y.SonoCode, StringComparison.Ordinal) == 0;
            }

            public override int GetHashCode(InspectorSonoAssignment obj)
            {
                return obj.SID.GetHashCode() ^ obj.SonoCode.GetHashCode();
            }
        }
    }
}