﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Server.Services.Helpers;
using Quartz;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class DeclarationDistributionJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers) context.Scheduler.Context["ServiceHelpers"];
            Console.WriteLine("IDA: Начало распределения деклараций по инспекторам {0}(UTC).", DateTime.UtcNow);
            helper.LogNotification("IDA: Начало распределения деклараций по инспекторам");
            RunJob(helper);
            helper.LogNotification("IDA: Окончание распределения деклараций по инспекторам");
            Console.WriteLine("IDA: Окончание распределения деклараций по инспекторам {0}(UTC).", DateTime.UtcNow);
            Console.WriteLine("IDA: Следующее время запуска: {0}(UTC).", context.NextFireTimeUtc);
        }

        private static void RunJob(ServiceHelpers helper)
        {
            helper.Do(() =>
            {
                var inspectorAdapter = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(helper);
                var sonoCodes = TableAdapterCreator.SonoAdapter(helper).GetSonoCodes();

                foreach (var sonoCode in sonoCodes)
                {
                    try
                    {
                        //-----------------------------------------------------------------------------
                        //-------------Распределение деклараций, которым назначены индивидуальные инспектора----------------
                        inspectorAdapter.ClearNonDistributedDeclarations(sonoCode);
                        inspectorAdapter.DistributeIndividualDeclarations(sonoCode);
                        var availableInspectorsWorkload =
                            inspectorAdapter.GetAvailableInspectorsWorkload(sonoCode)
                                .Where(i => !i.IsPaused && i.IsActive)
                                .ToArray();
                        var actualInspectorWorkloads =
                            inspectorAdapter.GetActualInspectorsWorkload(sonoCode).ToArray();
                        var declarationsForAutoDistribution =
                            inspectorAdapter.GetDeclarationsForAutoDistribution(sonoCode)
                                .Where(d => d.Amount != 0)
                                .ToArray();
                        DistributePaymentDeclarations(inspectorAdapter, helper, sonoCode, actualInspectorWorkloads,
                            declarationsForAutoDistribution, availableInspectorsWorkload);
                        DistributeCompensationDeclarations(inspectorAdapter, helper, sonoCode,
                            declarationsForAutoDistribution, actualInspectorWorkloads,
                            availableInspectorsWorkload);
                    }
                    catch (Exception ex)
                    {
                        helper.LogError(string.Format("Ошибка распределение деклараций для инспекции {0}", sonoCode),
                            "DeclarationDistributionJob.RunJob", ex);
                    }
                }
            });
        }

        private static void DistributeCompensationDeclarations(
            IInspectorDeclarationAssingmentAdapter inspectorAdapter,
            ServiceHelpers helper,
            string sonoCode,
            IEnumerable<DeclarationForAutoDistribution> declarationsForAutoDistribution,
            IEnumerable<ActualInspectorWorkload> actualInspectorWorkloads,
            IEnumerable<InspectorWorkload> availableInspectorsWorkload)
        {
            var compensationDeclarations = declarationsForAutoDistribution.Where(d => d.IsCompensation).ToList();
            var compensationActualWorkloads =
                actualInspectorWorkloads.Where(w => w.HasCompensationPermission).ToArray();
            var compensationAvailableWorkloads =
                availableInspectorsWorkload.Where(w => w.HasCompensationPermission).ToArray();


            var msgCompensation = string.Format("IDA: Распределение {0} деклараций к возмещению. Код ТНО {1}",
                compensationDeclarations.Count,
                sonoCode);

            Console.WriteLine(msgCompensation);
            helper.LogNotification(msgCompensation);
            DistributeDeclarations(helper, inspectorAdapter, compensationDeclarations,
                compensationAvailableWorkloads, compensationActualWorkloads,
                (actualWorkload, declaration) =>
                    actualWorkload.Compensation += declaration.Amount,
                availableWorkload => availableWorkload.MaxCompenationCapacity,
                actualWorkload => actualWorkload == null ? 0 : actualWorkload.Compensation
                );
            msgCompensation = "IDA: Декларации  к возмещению распределены";
            Console.WriteLine(msgCompensation);
            helper.LogNotification(msgCompensation);
        }

        private static void DistributePaymentDeclarations(IInspectorDeclarationAssingmentAdapter inspectorAdapter,
            ServiceHelpers helper,
            string sonoCode,
            IEnumerable<ActualInspectorWorkload> actualInspectorWorkloads,
            IEnumerable<DeclarationForAutoDistribution> declarationsForAutoDistribution,
            IEnumerable<InspectorWorkload> availableInspectorsWorkload)
        {
            var paymentDeclarations = declarationsForAutoDistribution.Where(d => d.IsPayment).ToList();
            var paymentActualWorkloads = actualInspectorWorkloads.Where(w => w.HasPaymentPermission).ToArray();
            var paymenAvailableWorkloads =
                availableInspectorsWorkload.Where(w => w.HasPaymentPermission).ToArray();
            var msgPayment = string.Format("IDA: Распределение {0} деклараций к уплате. Код ТНО {1}",
                paymentDeclarations.Count,
                sonoCode);
            Console.WriteLine(msgPayment);
            helper.LogNotification(msgPayment);
            DistributeDeclarations(helper, inspectorAdapter, paymentDeclarations,
                paymenAvailableWorkloads, paymentActualWorkloads,
                (actualWorkload, declaration) => actualWorkload.Payment += declaration.Amount,
                availableWorkload => availableWorkload.MaxPaymentCapacity,
                actualWorkload => actualWorkload == null ? 0 : actualWorkload.Payment);

            msgPayment = "IDA: Декларации к уплате распределены";
            Console.WriteLine(msgPayment);
            helper.LogNotification(msgPayment);
        }

        private static void DistributeDeclarations(ServiceHelpers helper,
            IInspectorDeclarationAssingmentAdapter inspectorAdapter,
            List<DeclarationForAutoDistribution> declarationsToDistribute,
            InspectorWorkload[] availableWorkloads,
            ActualInspectorWorkload[] actualWorkloads,
            Action<ActualInspectorWorkload, DeclarationForAutoDistribution> workloadIncrement,
            Func<InspectorWorkload, decimal> getCapacity,
            Func<ActualInspectorWorkload, decimal> getActualWorkLoad)
        {
            var nonDistributedDeclaraions = new List<DeclarationForAutoDistribution>();
            //-------------------- Выполняем случайное упрорядовачивание, чтобы исключить попадание одинаковых деклараций ------------
            //-------------------- к одним и тем же инспекторам. -------------------------------------------------------------------
            var rnd = new Random();
            var capacity = (from availableWorkload in availableWorkloads
                join actualWorkload in actualWorkloads
                    on availableWorkload.Inspector.SID equals actualWorkload.SID into gj
                from wl in gj.DefaultIfEmpty()
                select new
                {
                    AvailableWorkload = availableWorkload,
                    ActualWorkload = wl ?? new ActualInspectorWorkload
                    {
                        Compensation = 0,
                        HasCompensationPermission =
                            availableWorkload.
                                HasCompensationPermission,
                        HasPaymentPermission =
                            availableWorkload.
                                HasPaymentPermission,
                        Payment = 0,
                        SID = availableWorkload.Inspector.SID
                    }
                }).OrderBy(r => rnd.Next()).ToArray();
            while (declarationsToDistribute.Count > 0)
            {
                var remainedWorkloads = capacity.Select
                    (
                        c =>
                            new
                            {
                                c.ActualWorkload,
                                Remained = getCapacity(c.AvailableWorkload) - getActualWorkLoad(c.ActualWorkload)
                            }
                    ).ToArray();
                var declaration = declarationsToDistribute.OrderByDescending(d => d.Amount).FirstOrDefault();
                if (declaration != null)
                {
                    var nonAssigned = true;
                    foreach (
                        var remainedWorkload in
                            remainedWorkloads.OrderBy(w => getActualWorkLoad(w.ActualWorkload)).ToArray())
                    {
                        if (remainedWorkload.Remained >= declaration.Amount)
                        {
                            nonAssigned = false;
                            helper.LogNotification(string.Format(
                                "IDA: назначение декларации {0} на инспектора SID {1}", declaration.DeclarationId,
                                remainedWorkload.ActualWorkload.SID));
                            inspectorAdapter.AssignDeclaration(declaration.DeclarationId,
                                remainedWorkload.ActualWorkload.SID);
                            workloadIncrement(remainedWorkload.ActualWorkload, declaration);
                            declarationsToDistribute.Remove(declaration);
                            break;
                        }
                    }
                    if (nonAssigned)
                    {
                        nonDistributedDeclaraions.Add(declaration);
                        declarationsToDistribute.Remove(declaration);
                    }
                }
            }
            helper.LogNotification(string.Format("IDA: Не распределено {0} деклараций", nonDistributedDeclaraions.Count));
            Console.WriteLine("IDA: Не распределено {0} деклараций", nonDistributedDeclaraions.Count);
            foreach (var nonDistributedDecl in nonDistributedDeclaraions)
            {
                inspectorAdapter.AddNonAssignedDeclaration(nonDistributedDecl.DeclarationId);
            }
        }
    }
}