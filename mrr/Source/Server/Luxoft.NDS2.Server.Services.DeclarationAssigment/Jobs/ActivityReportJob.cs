﻿using System;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reports.ActivityReport;
using Quartz;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class ActivityReportJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers)context.Scheduler.Context["ServiceHelpers"];
            helper.Do(() =>
            {
                Console.WriteLine("Начало построения отчета активности пользователей. {0}(UTC)",
                    DateTime.UtcNow);
                helper.LogNotification("Начало построения отчета активности пользователей.",
                    "Luxoft.NDS2.Server.TaskScheduler.Jobs.ActivityReportJob.Execute");
                var activityReportProvider = new ActivityReportProvider(helper.Services);
                activityReportProvider.CreateReport(DateTime.Today.AddDays(-1));
                Console.WriteLine("Завершение построения отчета активности пользователей. {0}(UTC)",
                    DateTime.UtcNow);
                helper.LogNotification("Завершение построения отчета активности пользователей.",
                    "Luxoft.NDS2.Server.TaskScheduler.Jobs.ActivityReportJob.Execute");
            });
        }
    }
}
