﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class ReclaimJob : IJob
    {
        private const int MUTEX_TIMEOUT = 1;

        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers)context.Scheduler.Context["ServiceHelpers"];
            Console.WriteLine("Проба запуска задания формирования автоистребований ... ");
            helper.LogNotification(String.Format("Проба запуска задания формирования автоистребований ... ", DateTime.UtcNow));

            try
            {
                using(var mutex = new Mutex(false, "ReclaimJob"))
                {
                    var resultMutext = mutex.WaitOne(MUTEX_TIMEOUT);

                    if (resultMutext)
                    {
                        Console.WriteLine("Запуск задания формирования автоистребований. {0}.(UTC)", DateTime.UtcNow);
                        helper.LogNotification(String.Format("Запуск задания формирования автоистребований. {0}.(UTC)", DateTime.UtcNow));

                        new QueueHandler(helper)
                            .Execute();

                        Console.WriteLine("Завершение задания формирования автоистребований. {0}.(UTC)", DateTime.UtcNow);
                        helper.LogNotification(String.Format("Завершение задания формирования автоистребований. {0}.(UTC)", DateTime.UtcNow));

                        mutex.ReleaseMutex();
                    }
                }
            }
            catch (Exception exception)
            {
                string textError = string.Format("ReclaimQueue Ошибка: ", exception.ToString());
                Console.WriteLine(textError);
                helper.LogError(textError, "Luxoft.NDS2.Server.TaskScheduler.Jobs.ReclaimJob.Execute", exception);
            }
        }
    }
}
