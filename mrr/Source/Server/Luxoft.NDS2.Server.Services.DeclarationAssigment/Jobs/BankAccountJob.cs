﻿using System.Collections.Generic;
using System.Threading;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.BankAccounts;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.BankAccounts;
using Quartz;
using System;

namespace Luxoft.NDS2.Server.TaskScheduler.Jobs
{
    [DisallowConcurrentExecution]
    public class BankAccountJob : IJob
    {
        class Callback : IBankAccountCallback
        {
            private readonly ServiceHelpers _helper;
            private readonly IBankAccountsAdapter _bankAccountsAdapter;
            private readonly Dictionary<MessageType, string> _messageTypes = new Dictionary<MessageType, string>
            {
                {MessageType.Warning, "[ВНИМАНИЕ] "},
                {MessageType.Error, "[ОШИБКА] "}
            };

            public Callback(ServiceHelpers helper, IBankAccountsAdapter bankAccountsAdapter)
            {
                _helper = helper;
                _bankAccountsAdapter = bankAccountsAdapter;
            }

            public void Log(MessageType type, string message)
            {
                Console.WriteLine("{0}{1}", GetTypeString(type), message);
                var link = new System.Diagnostics.StackFrame(1).GetMethod().Name;
                _helper.LogNotification(message, link);
                _bankAccountsAdapter.LogToDb(type, link, message);
            }

            private string GetTypeString(MessageType type)
            {
                string s;
                return _messageTypes.TryGetValue(type, out s) ? s : string.Empty;
            }
        }
        
        public void Execute(IJobExecutionContext context)
        {
            var helper = (ServiceHelpers)context.Scheduler.Context["ServiceHelpers"];
            helper.Do(() =>
            {
                Console.WriteLine("Начало формирования XML запросов в банк. {0}(UTC)", DateTime.UtcNow);
                helper.LogNotification("Начало формирования XML запросов в банк.", "Luxoft.NDS2.Server.TaskScheduler.Jobs.BankAccountJob.Execute");

                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(helper))
                {
                    var bankAccountsAdapter = helper.GetBankAccountsAdapter(connectionFactory);

                    var provider = new BankAccountProvider(bankAccountsAdapter, new Callback(helper, bankAccountsAdapter));
                    provider.ProcessQueue();
                }

                Console.WriteLine("Завершение формирования XML запросов в банк. {0}(UTC)", DateTime.UtcNow);
                helper.LogNotification("Завершение формирования XML запросов в банк.", "Luxoft.NDS2.Server.TaskScheduler.Jobs.BankAccountJob.Execute");
            });
        }
    }
}
