﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CommonComponents.Catalog;
using CommonComponents.Shared;
using DocumentFormat.OpenXml.Packaging;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Common;

namespace Luxoft.NDS2.Server.TaskScheduler.Reports.ActivityReport
{
    public class ActivityReportProvider : IActivityReportProvider
    {
        private readonly ServiceHelpers _helper;

        public ActivityReportProvider(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public void Dispose()
        {

        }

        public void CreateReport(DateTime reportDate)
        {
            var config = ReadConfig();
            if (config == null)
            {
                _helper.LogError("NDS2-AR: Невозможно прочитать конфигурацию");
                return;
            }
            Build(reportDate, config);
        }

        public void Build(DateTime reportDate, Configuration config)
        {

            var data =
                _helper.Do(() => TableAdapterCreator.ActivityLog(_helper).GetReportData(reportDate));
            var tmp =
                _helper.Do(() => TableAdapterCreator.ActivityLog(_helper).GetUserCount(reportDate));
            var users = tmp.Result != null ? tmp.Result.FirstOrDefault() : 0;

            if (data.Status != ResultStatus.Success)
            {
                _helper.LogError("NDS2-AR: Не удалось запросить данные для отчета активности");
                return;
            }

            Directory.CreateDirectory(config.TR_ReportFilesLocation.Value);
            var path = Path.Combine(config.TR_ReportFilesLocation.Value, string.Format("Активность пользователей за {0}.xlsx", DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd")));
            if (File.Exists(path))
            {
                File.Delete(path);
            }
                
            // ReSharper disable CSharpWarnings::CS0618
            var csv = _helper.Services.Get<ICatalogFactoryService>();
            var tmpl = csv.CreateInstance<Stream>(new Uri(TEMPLATE_PATH));
            // ReSharper restore CSharpWarnings::CS0618

            using (var fileStream = File.Create(path))
            {
                tmpl.Seek(0, SeekOrigin.Begin);
                tmpl.CopyTo(fileStream);
                try
                {
                    BuildActivityReport(data.Result, users, fileStream, reportDate);
                }
                catch (Exception e)
                {
                    _helper.LogError("NDS2-AR: Не удалось заполнить отчет: " + e.Message);
                }
                fileStream.Flush();
                fileStream.Close();
            }

        }

        private void BuildActivityReport(List<ActivityReportSummary> data, long users, Stream stream, DateTime reportDate)
        {
            using (var doc = SpreadsheetDocument.Open(stream, true))
            {
                var sheets = ExcelSheet.GetSheets(doc);

                var sheet = sheets.First();

                sheet.Sheet.GetCell("B", 1).SetValue(reportDate.ToString("dd MMMM yyyy"));
                sheet.Sheet.GetCell("F", 1).SetValue(users);

                var c = 'C';

                foreach (var item in data)
                {
                    var col = c.ToString(CultureInfo.InvariantCulture);

                    #region Вывод данных

                    //   sheet.Sheet.GetCell(col, 4).SetValue(item.Logons);
                    sheet.Sheet.GetCell(col, 4).SetValue(item.MaxUserForHour);
                    sheet.Sheet.GetCell(col, 5).SetValue(item.Decl_open_uniq);
                    sheet.Sheet.GetCell(col, 6).SetValue(item.Decl_ext_open_uniq);
                    sheet.Sheet.GetCell(col, 7).SetValue(item.Man_sel_call_uniq);
                    sheet.Sheet.GetCell(col, 8).SetValue(item.Reports_uniq);

                    sheet.Sheet.GetCell(col, 11).SetValue(item.Decl_open);
                    sheet.Sheet.GetCell(col, 12).SetValue(item.Decl_time_min/1000);
                    sheet.Sheet.GetCell(col, 13).SetValue(item.Decl_time_max/1000);
                    sheet.Sheet.GetCell(col, 14).SetValue(item.Decl_time_avg/1000);

                    sheet.Sheet.GetCell(col, 15).SetValue(item.Decl_ext_open);
                    sheet.Sheet.GetCell(col, 16).SetValue(item.Decl_sov_time_min/1000);
                    sheet.Sheet.GetCell(col, 17).SetValue(item.Decl_sov_time_max/1000);
                    sheet.Sheet.GetCell(col, 18).SetValue(item.Decl_sov_time_avg/1000);

                    sheet.Sheet.GetCell(col, 20).SetValue(item.Decl_size_min);
                    sheet.Sheet.GetCell(col, 21).SetValue(item.Decl_size_max);
                    sheet.Sheet.GetCell(col, 22).SetValue(item.Decl_size_avg);

                    sheet.Sheet.GetCell(col, 24).SetValue(item.Man_sel_call);
                    sheet.Sheet.GetCell(col, 25).SetValue(item.Man_sel_time_min/1000);
                    sheet.Sheet.GetCell(col, 26).SetValue(item.Man_sel_time_max/1000);
                    sheet.Sheet.GetCell(col, 27).SetValue(item.Man_sel_time_avg/1000);

                    sheet.Sheet.GetCell(col, 29).SetValue(item.Reports);
                    sheet.Sheet.GetCell(col, 30).SetValue(item.Reports_time_min/1000);
                    sheet.Sheet.GetCell(col, 31).SetValue(item.Reports_time_max/1000);
                    sheet.Sheet.GetCell(col, 32).SetValue(item.Reports_time_avg/1000);

                    #endregion

                    ++c;
                }

                sheet.Sheet.Save();
            }
        }

        private Configuration ReadConfig()
        {
            try
            {
                return TableAdapterCreator.Configuration(_helper).Read();
            }
            catch (Exception ex)
            {
                _helper.LogError(ex.ToString());
                return null;
            }
        }

        private const string TEMPLATE_PATH = "logicalCatalog://RoleCatalog/NDS2Subsystem/FileSystems/ReportTemplates/activity_log_report.xlsx";
    }
}
