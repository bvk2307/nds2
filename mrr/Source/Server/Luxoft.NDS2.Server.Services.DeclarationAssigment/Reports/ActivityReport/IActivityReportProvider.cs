﻿using System;

namespace Luxoft.NDS2.Server.TaskScheduler.Reports.ActivityReport
{
    public interface IActivityReportProvider : IDisposable
    {
        void CreateReport(DateTime reportDate);
    }
}