﻿using System.IO;
using CommonComponents.Catalog;

namespace Luxoft.NDS2.Server.TaskScheduler.Reports.Services
{
    public interface ITemplateService
    {
        Stream GetTemplateStream(CatalogAddress address);
    }
}
