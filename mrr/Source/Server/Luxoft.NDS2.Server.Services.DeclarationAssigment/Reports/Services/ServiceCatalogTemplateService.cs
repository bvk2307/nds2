﻿using System.IO;
using CommonComponents.Catalog;
using CommonComponents.Shared;

namespace Luxoft.NDS2.Server.TaskScheduler.Reports.Services
{
    public class ServiceCatalogTemplateService : ITemplateService
    {
        public IReadOnlyServiceCollection _services;

        public ServiceCatalogTemplateService(IReadOnlyServiceCollection collection)
        {
            _services = collection;
        }

        public Stream GetTemplateStream(CatalogAddress address)
        {
            return _services.Get<ICatalogService>().CreateInstance<Stream>(address);
        }
    }
}
