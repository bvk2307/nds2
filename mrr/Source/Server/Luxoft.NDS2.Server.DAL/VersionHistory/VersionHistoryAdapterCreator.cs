﻿namespace Luxoft.NDS2.Server.DAL.VersionHistory
{
    public static class VersionHistoryAdapterCreator
    {
        public static IVersionHistoryAdapter Create(IServiceProvider service)
        {
            return new VersionHistoryAdapter(service);
        }
    }
}
