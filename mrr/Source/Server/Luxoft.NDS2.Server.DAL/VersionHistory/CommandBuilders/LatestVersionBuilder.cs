﻿using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.VersionHistory.CommandBuilders
{
    internal class LatestVersionBuilder : ExecuteProcedureCommandBuilder
    {
        public LatestVersionBuilder() 
            : base(string.Format(DbConstants.ProcedurePattern, 
                                    DbConstants.VersionHistoryPackageName,
                                       DbConstants.GetLatestVersionProcedureName))
        { }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParameterCursorName));
        }
    }
}
