﻿using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.VersionHistory.CommandBuilders
{
    internal class VersionsWithReleaseNotesBuilder : ExecuteProcedureCommandBuilder
    {
        public VersionsWithReleaseNotesBuilder()
            : base(string.Format(DbConstants.ProcedurePattern,
                        DbConstants.VersionHistoryPackageName,
                            DbConstants.GetAllVersionsProcedureName))
        { }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.DecimalIn(DbConstants.ParameterReleaseNotesOnly, 1));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParameterCursorName));
        }
    }
}
