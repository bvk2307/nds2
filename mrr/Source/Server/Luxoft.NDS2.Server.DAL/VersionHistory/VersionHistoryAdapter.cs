﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using System.Linq;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.VersionHistory.CommandBuilders;

namespace Luxoft.NDS2.Server.DAL.VersionHistory
{
    internal class VersionHistoryAdapter : IVersionHistoryAdapter
    {
        private readonly IServiceProvider _service;

        public VersionHistoryAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public VersionInfo SearchLatest()
        {
            var latestVersion =  new ListCommandExecuter<VersionInfo>(
                                    new GenericDataMapper<VersionInfo>(),
                                    _service)
                                        .TryExecute(new LatestVersionBuilder())
                                        .FirstOrDefault();
            return latestVersion ?? new VersionInfo() { Undefined = true };
        }

        public IEnumerable<VersionInfo> GetVersionsWithReleaseNotes()
        {
            return new ListCommandExecuter<VersionInfo>(
                        new GenericDataMapper<VersionInfo>(),
                        _service)
                            .TryExecute(new VersionsWithReleaseNotesBuilder())
                            .ToList();
        }
    }
}
