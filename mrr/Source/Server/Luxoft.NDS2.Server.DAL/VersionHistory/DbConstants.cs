﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.VersionHistory
{
    public static class DbConstants
    {
        public const string ProcedurePattern = "{0}.{1}";
        public const string VersionHistoryPackageName = "PAC$VERSION_HISTORY";
        public const string GetLatestVersionProcedureName = "P$GET_LATEST";
        public const string GetAllVersionsProcedureName = "P$GET_ALL";

        public const string ParameterCursorName = "pCursor";
        public const string ParameterReleaseNotesOnly = "pReleaseNotesOnly";
    }
}
