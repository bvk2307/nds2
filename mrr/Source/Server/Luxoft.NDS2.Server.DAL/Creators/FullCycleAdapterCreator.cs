﻿using Luxoft.NDS2.Server.DAL.FullCycle;
using Luxoft.NDS2.Server.DAL.Packages;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public static class FullCycleAdapterCreator
    {
        public static IFullCyclePackageAdapter NewAdapter(
            IServiceProvider service, 
            ConnectionFactoryBase connection)
        {
            return new FullCyclePackageAdapter(service, connection);
        }
    }
}
