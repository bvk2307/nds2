﻿using Luxoft.NDS2.Server.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public static class ExplainASKAdapterCreator
    {
        public static IExplainASKAdapter ExplainASKAdapter(this IServiceProvider service)
        {
            return new ExplainASKAdapter(service);
        }
    }
}
