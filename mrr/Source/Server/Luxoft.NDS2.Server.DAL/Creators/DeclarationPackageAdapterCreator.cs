﻿using Luxoft.NDS2.Server.DAL.Packages;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public class DeclarationPackageAdapterCreator
    {
        public static IDeclarationPackageAdapter Create(IServiceProvider service)
        {
            return new DeclarationPackageAdapter(service);
        }
    }
}
