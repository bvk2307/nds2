﻿using Luxoft.NDS2.Server.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public static class InvoiceContractorSignCreator
    {
        public static IInvoiceContractorDeclarationAdapter InvoiceContractorDeclarationAdapter(this IServiceProvider service)
        {
            return new InvoiceContractorDeclarationAdapter(service);
        }
    }
}
