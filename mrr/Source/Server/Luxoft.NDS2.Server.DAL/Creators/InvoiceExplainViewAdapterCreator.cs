﻿using Luxoft.NDS2.Server.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public static class InvoiceExplainViewAdapterCreator
    {
        public static IInvoiceExplainViewAdapter InvoiceExplainViewAdapter(this IServiceProvider service)
        {
            return new InvoiceExplainViewAdapter(service);
        }
    }
}
