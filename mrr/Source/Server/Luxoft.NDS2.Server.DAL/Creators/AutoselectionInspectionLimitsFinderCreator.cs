﻿using Luxoft.NDS2.Server.DAL.Packages;

namespace Luxoft.NDS2.Server.DAL.Creators
{
    public static class AutoselectionInspectionLimitsFinderCreator
    {
        public static IAutoselectionInspectionLimitsFinder Create(IServiceProvider service)
        {
            return new AutoselectionPackageAdapter(service);
        }
    }
}
