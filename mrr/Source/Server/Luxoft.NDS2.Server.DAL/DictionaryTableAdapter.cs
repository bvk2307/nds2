﻿using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL
{
    internal abstract class DictionaryTableAdapter<TData> : IDictionaryTableAdapter<TData>
        where TData : new()
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        protected DictionaryTableAdapter(
            IServiceProvider service, 
            ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public IEnumerable<TData> All()
        {
            return 
                new ListCommandExecuter<TData>(DataMapper(), _service, _connection)
                    .TryExecute(CommandBuilder()).ToArray();
        }

        protected abstract IOracleCommandBuilder CommandBuilder();

        protected virtual IDataMapper<TData> DataMapper()
        {
            return new GenericDataMapper<TData>();
        }
    }
}
