﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;

namespace Luxoft.NDS2.Server.DAL
{
    internal abstract class SingleTableAdapterBase<TData> : BaseOracleTableAdapter
    {
        private const string TableAlias = "t";

        private const string ParamCountResult = "pResult"; 

        protected string TableName
        {
            get;
            private set;
        }

        protected SingleTableAdapterBase(
            string tableName, 
            IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            TableName = tableName;
        }

        public List<TData> Search(
            FilterExpressionBase filterBy,
            List<ColumnSort> sortBy,
            uint take,
            uint skip)
        {
            var queryContext =
                new DataQueryContext
                    {
                        Where = filterBy,
                        OrderBy = sortBy,
                        MaxQuantity = (int) take,
                        Skip = (int) skip
                    };

            return
                CreateExecuter()
                    .TryRead(
                        queryContext
                            .ToSQL(
                                TableName.ToSelectPatternOrdered(TableAlias),
                                GetPatternProvider(TableAlias))
                            .ToContext(),
                        BuildDataList);
        }

        public List<TData> Search(FilterExpressionBase filterBy)
        {
            var command = filterBy.ToSQL(
                TableName
                    .ToSelectPattern(TableAlias)
                    .WithCursor(ParamCountResult),
                GetPatternProvider(TableAlias)).ToContext();
            command.Parameters.Add(
                FormatCommandHelper.Cursor(ParamCountResult));

            return CreateExecuter()
                .TryRead(
                    command,
                    BuildDataList);
        }

        public int Count(FilterExpressionBase filterBy)
        {
            var command = filterBy
                .ToSQL(
                    TableName.CountPattern(TableAlias, ParamCountResult),
                    GetPatternProvider(TableAlias))
                .ToContext();
            command.Parameters.Add(FormatCommandHelper.NumericOut(ParamCountResult));

            return CreateExecuter()
                .TryReadCount(
                    command,
                    ParamCountResult);
        }

        protected abstract PatternProvider GetPatternProvider(string tableName);

        protected abstract List<TData> BuildDataList(ResultCommandHelper reader);
    }
}
