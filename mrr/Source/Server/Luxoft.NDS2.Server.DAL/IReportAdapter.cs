﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IReportAdapter
    {
        PageResult<InspectorMonitoringWork> SearchInspectorMonitoringWork(QueryConditions criteria, long taskId);
        PageResult<LoadingInspection> SearchLoadingInspection(QueryConditions criteria, long taskId);
        PageResult<MonitoringProcessingDeclaration> SearchMonitoringProcessingDeclaration(QueryConditions criteria, long taskId);
        PageResult<CheckControlRatio> SearchCheckControlRatio(QueryConditions criteria, long taskId);
        PageResult<MatchingRule> SearchMatchingRule(QueryConditions criteria, long taskId);
        PageResult<CheckLogicControl> SearchCheckLogicControl(QueryConditions criteria, long taskId);
        PageResult<DeclarationStatistic> SearchDeclarationStatistic(QueryConditions criteria, long taskId);

        PageResult<DynamicTechnoParameter> SearchDynamicTechnoParameter(QueryConditions criteria, long taskId);
        List<DynamicParameter> SearchDictionaryDynamicParameters(DynamicModuleType moduleType);
        List<DateTime> GetDatesTechnoReport();

        /// <summary>
        /// Отчёт по динамике изменения фоновых показателей
        /// </summary>
        PageResult<DynamicDeclarationStatistic> SearchDynamicDeclarationStatistic(QueryConditions criteria, long taskId);

        /// <summary>
        /// Отчет Информация о результатах сопоставлений (В2)
        /// </summary>
        PageResult<InfoResultsOfMatching> SearchInfoResultsOfMatching(QueryConditions criteria, long taskId);

        long SaveTask(QueryConditions criteria, long requestId, int reportNum);
        TaskStatus GetTaskStatus(long taskId);
        void RunCreateReport(int reportNum, long taskId, long requestId);
        DataRequestStatus Status(long requestId);
    }
}
