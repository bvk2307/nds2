﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>Этот интерфейс описывает методы адаптера добавления комментариев</summary>
    public interface ICommentHistoryAdapter
    {
        /// <summary>
        /// Получение комментариев к объекту
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <param name="type">Тип объекта</param>
        /// <param name="queryConditions"></param>
        /// <returns></returns>
        List<CommentHistory> GetCommentHistory(long id, ObjectType type, QueryConditions queryConditions);

        /// <summary>
        /// Запись комментария в БД
        /// </summary>
        /// <param name="comment">Экземпляр</param>
        void AddComment(CommentHistory comment);
    }
}