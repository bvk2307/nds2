﻿using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL.DiscrepancySelection;
using Luxoft.NDS2.Server.DAL.DiscrepancySelection.Implementation;
using Luxoft.NDS2.Server.DAL.EfficiencyMonitoring;
using Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Implementation.Selections;
using Luxoft.NDS2.Server.DAL.MacroReport;
using Luxoft.NDS2.Server.DAL.Packages;
using Luxoft.NDS2.Server.DAL.UserTask;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот класс реализует создателя БД адаптеров
    /// </summary>
    public static class TableAdapterCreator
    {
        /// <summary>
        /// Создает экземпляр класса-имплементатора интерфейса IStateTransitionAdapter
        /// </summary>
        /// <param name="service">Ссылка на реализацию служебных сервисов</param>
        /// <returns>Ссылка на адаптер</returns>
        public static IStateTransitionAdapter StateTransition(IServiceProvider service)
        {
            return new StateTransitionAdapter(service);
        }

        public static ICommentHistoryAdapter AddComment(IServiceProvider service)
        {
            return new CommentHistoryAdapter(service);
        }

        public static IDictionarySURAdapter DictionarySUR(IServiceProvider service)
        {
            return new DictionarySURAdapter(service);
        }

        public static IDictionaryOperAdapter DictionaryOper(IServiceProvider service)
        {
            return new DictionaryOperAdapter(service);
        }

        public static IDictionaryCurrencyAdapter DictionaryCurrency(IServiceProvider service)
        {
            return new DictionaryCurrencyAdapter(service);
        }

        public static IDictionaryBargainAdapter DictionaryBargain(IServiceProvider service)
        {
            return new DictionaryBargainAdapter(service);
        }
        /// <summary>
        /// Создает экземпляр класса-имплементатора интерфейса IUserToRegionAdapter
        /// </summary>
        /// <param name="service">Ссылка на реализацию служебных сервисов</param>
        /// <returns>Ссылка на адаптер</returns>
        public static IUserToRegionAdapter UserToRegion(IServiceProvider service)
        {
            return new UserToRegionAdapter(service);
        }

        public static IAISAdapter AIS(IServiceProvider service)
        {
            return new AISAdapter(service);
        }

        public static IDeclarationAdapter Declaration(IServiceProvider provider)
        {
            return new DeclarationAdapter(provider);
        }

        public static IDeclarationBriefAdapter DeclarationForInspector(IServiceProvider provider)
        {
            return new DeclarationBriefAdapter(provider);
        }

        public static IMacroReportNdsMapAdapter MacroNdsReportMap(IServiceProvider provider)
        {
            return new MacroReportNdsMapAdapter(provider);
        }

        public static IMacroReportDiscrepancyMapAdapter MacroDiscrepancyReportMap(IServiceProvider provider)
        {
            return new MacroReportDiscrepancyMapAdapter(provider);
        }

        public static IMacroReportNpAdapter MacroReportAdapter(IServiceProvider provider, ConnectionFactoryBase connectionFactory)
        {
            return new MacroReportNpAdapter(provider, connectionFactory);
        }

        public static IConfigurationAdapter Configuration(IServiceProvider service)
        {
            return new ConfigurationAdapter(service);
        }

        public static IDictionaryUtilityInfoAdapter DictionaryUtilityInfo(IServiceProvider service)
        {
            return new DictionaryUtilityInfoAdapter(service);
        }

        public static IDiscrepancyAdapter Discrepancy(IServiceProvider provider, long discrepancyId)
        {
            return new DiscrepancyAdapter(provider, discrepancyId);
        }

        public static IRegionAdapter Region(IServiceProvider provider)
        {
            return new Luxoft.NDS2.Server.DAL.Implementation.RegionAdapter(provider);
        }

        public static IReportAdapter Report(IServiceProvider service)
        {
            return new ReportAdapter(service);
        }

        public static ITaxPayerAdapter TaxPayer(IServiceProvider provider)
        {
            return new TaxPayerAdapter(provider);
        }

        public static IDocumentAdapter DiscrepancyDocument(IServiceProvider provider)
        {
            return new DocumentAdapter(provider);
        }

        public static IControlRatioAdapter ControlRatio(IServiceProvider provider)
        {
            return new ControlRatioAdapter(provider);
        }

        public static IContragentsAdapter ContragentsList(IServiceProvider provider)
        {
            return new ContragentsAdapter(provider);
        }

        public static IBookDataRequestAdapter BookDataRequest(IServiceProvider provider)
        {
            return new BookDataRequestAdapter(provider);
        }

        public static ISelectionWorkflowAdapter SelectionWorkflow(IServiceProvider provider)
        {
            return new SelectionWorkflowAdapter(provider);
        }

        public static IEmployeeDialogAdapter EmployeeDialog(IServiceProvider service)
        {
            return new EmployeeDialogAdapter(service);
        }

        public static ISounDialogAdapter SounDialog(IServiceProvider service)
        {
            return new SounDialogAdapter(service);
        }

        public static IPyramidDataAdapter Pyramid(IServiceProvider provider)
        { 
            return new PyramidDataAdapter(provider);
        }

        public static IExplainReplyAdapter ExplainReply(IServiceProvider service)
        {
            return new ExplainReplyAdapter(service);
        }

        public static ILoadTestHelpAdapter LoadTestHelper(IServiceProvider service)
        {
            return new LoadTestHelpAdapter(service);
        }

        public static ICalendarAdapter Calendar(IServiceProvider service)
        {
            return new CalendarAdapter(service);
        }

        public static IActivityLogAdapter ActivityLog(IServiceProvider service)
        {
            return new ActivityLogAdapter(service);
        }

        public static IObjectLockerAdapter ObjectLocker(IServiceProvider service)
        {
            return new ObjectLockerAdapter(service);
        }

        public static Navigator.ITaxPayerAdapter NavigatorTaxPayer(IServiceProvider service)
        {
            return new Navigator.TaxPayerAdapter(service);
        }

        public static Navigator.IReportAdapter NavigatorReport(IServiceProvider service)
        {
            return new Navigator.ReportAdapter(service);
        }        

        public static ITechnologicalReportAdapter TechnologicalReport(IServiceProvider service)
        {
            return new TechnologicalReportAdapter(service);
        }

        public static ISelectionTableAdapter SelectionTableAdapter(IServiceProvider service)
        {
            return new SelectionTableAdapter(service);
        }

        public static IEchoAdapter EchoAdapter(IServiceProvider service)
        {
            return new EchoAdapter(service);
        }

        public static IConfigExportInvoiceAdapter ConfigExportInvoice(IServiceProvider service)
        {
            return new ConfigExportInvoiceAdapter(service);
        }

        public static IVersionHistoryAdapter VersionHistory(IServiceProvider service)
        {
            return new VersionHistoryAdapter(service);
        }

        public static IConfigurationAutoselectionFilterTableAdapter ConfigurationAutoselectionFilter(
            IServiceProvider service)
        {
            return new ConfigurationAutoselectionFilterTableAdapter(service);
        }

        public static IDiscrepancySelectionAdpater DiscrepancySelectionAdpater(IServiceProvider service)
        {
            return new DiscrepancySelectionAdpater(service);
        }

        public static IDictionaryFilterTypeAdapter DictionaryFilterTypeAdapter(IServiceProvider service)
        {
            return new DictionaryFilterTypeAdapter(service);
        }

        public static IDictionaryTaxPeriodAdapter DictionaryTaxPeriodAdapter(IServiceProvider service)
        {
            return new DictionaryTaxPeriodAdapter(service);
        }

        public static ITaxPeriodConfigurationAdapter TaxPeriodConfigurationAdapter(IServiceProvider service)
        {
            return new TaxPeriodConfigurationAdapter(service);
        }

        public static ISelectionPackageAdapter SelectionPackageAdapter(IServiceProvider service)
        {
            return new SelectionPackageAdapter(service);
        }

        public static IExportExcelPackageAdapter ExportExcelPackageAdapter(IServiceProvider service)
        {
            return new ExportExcelPackageAdapter(service);
        }

        public static IInspectorDeclarationAssingmentAdapter InspectorDeclarationAssingmentAdapter(IServiceProvider service)
        {
            return new InspectorDeclarationAssignmentAdapter(service);
        }

        public static ITaxpayerAdapter IdaTaxpayerAdapter(IServiceProvider service)
        {
            return new TaxpayerAdapter(service);
        }

        public static ITaxpayerAssignmentAdapter IdaTaxpayerAssignmentAdapter(IServiceProvider service)
        {
            return new TaxpayerAssignmentAdapter(service);
        }

        public static IInspectorStatisticsAdapter IdaTaxpayerInspectorAssignmentAdapter(IServiceProvider service)
        {
            return new InspectorStatisticsAdapter(service);
        }

        public static IInspectorDeclarationAdapter InspectorDeclarationAdapter(Services.Helpers.ServiceHelpers _helper)
        {
            return new InspectorDeclarationAdapter(_helper);
        }

        public static ISonoAdapter SonoAdapter(Services.Helpers.ServiceHelpers _helper)
        {
            return new SonoAdapter(_helper);
        }

        public static EfficiencyMonitoring.ICommonAdapter CommonAdapter(IServiceProvider service)
        {
            return new CommonAdapter(service);
        }

        public static Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.IRegionAdapter RegionAdapter(IServiceProvider service)
        {
            return new Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation.RegionAdapter(service);
        }

        public static Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.IIfnsAdapter IfnsAdapter(IServiceProvider service)
        {
            return new Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation.IfnsAdapter( service );
        }
    }
}