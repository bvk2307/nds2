﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы DAL адаптера, для загрузки информации для редактирования справочников
    /// </summary>
    public interface IDictionaryUtilityInfoAdapter
    {
        List<TableDictionaryColumnInfo> GetTablesDictionaryColumnInfo();
        List<TableDictionaryInfo> GetTablesDictionaryInfo();
        List<Dictionary<string, object>> GetTableRows(string tableName, List<string> columns);
        void UpdateTableRow(string tableName, string key, string value, Dictionary<string, string> row);
        void AddTableRow(string tableName, Dictionary<string, string> row);
        void DeleteTableRow(string tableName, string key, string value);
    }
}
