﻿namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы адаптера запросов на получение данных с/ф
    /// </summary>
    public interface IInvoiceRequestAdapter
    {
        /// <summary>
        /// Создает новый запрос на получение с/ф из МС по расхождениям выборки
        /// </summary>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <returns></returns>
        long Create(long selectionId);
    }
}
