﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL
{
    internal class DbDataFetcher<TData> : IEnumerable<TData>, IEnumerator<TData>
        //where TData : new()   //apopov 16.6.2016	//constraint is commented as too limiting and needless one
    {
        # region Fields

        private readonly IDataMapper<TData> _mapper;

        private IDataReader _reader;

        # endregion

        # region Constructor

        public DbDataFetcher(
            IDataReader reader,
            IDataMapper<TData> mapper)
        {
            _mapper = mapper;
            _reader = reader;
        }

        # endregion

        # region Implementation of IEnumerator<TData>

        public TData Current
        {
            get 
            {
                return _mapper.MapData(_reader);
            }
        }

        public void Dispose()
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }

            GC.SuppressFinalize(this);
        }

        object IEnumerator.Current
        {
            get 
            {
                return Current;
            }
        }

        public bool MoveNext()
        {
            return _reader.Read();
        }

        public void Reset()
        {
        }

        # endregion

        # region Implementation of IEnumerable<TData>
    
        public IEnumerator<TData> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        # endregion
    }
}
