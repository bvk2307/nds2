﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы движения выборки по процессу
    /// </summary>
    public interface ISelectionWorkflowAdapter
    {
        /// <summary>
        /// Отправляет все выборки, которые удовлетворяют условию, в Seod
        /// </summary>
        /// <param name="selectionIds"></param>
        long SendToEOD(long[] selectionIds);

        /// <summary>
        /// Помечает все выборки как удаленные
        /// </summary>
        /// <param name="criteria">Условия фильтра выборки</param>
        /// <param name="comment">Коментарий пользователя к отправке</param>
        long Delete(QueryConditions criteria, string comment);

        /// <summary>
        /// Проверяет возможность перехода из статуса в статус
        /// </summary>
        /// <param name="fromTransition"></param>
        /// <param name="toTransition"></param>
        /// <returns></returns>
        bool HasTransition(int fromTransition, int toTransition);
    }
}
