﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ITaxPeriodConfigurationAdapter
    {
        TaxPeriodConfiguration Search();
    }
}
