﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы DAL адаптера, для загрузки таблицы переходов состояний
    /// </summary>
    public interface IStateTransitionAdapter
    {
        List<SelectionTransition> GetStateTransitions();
    }
}
