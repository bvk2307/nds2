﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ISelectionPackageAdapter
    {
        void SaveAutoselectionFilter(long id, string includeFilter, string excludeFilter);
        
        /// <summary>
        /// Синхронизирует статус выборки со статусом обработки запроса расхождений
        /// </summary>
        /// <returns>Статус выборки после синхронизации</returns>
        SelectionStatus SyncWithRequestProcessing(long id);
        /// <summary>
        /// Загружает конкретную выборку
        /// </summary>
        /// <returns>Выборка</returns>
        Selection GetSelection(long id);

        /// <summary>
        /// Устанавливает статус выборки
        /// </summary>
        /// <param name="id"></param>
        /// <param name="stateId"></param>
        /// <param name="userName"></param>
        void SetSelectionState(long id, int stateId, string userName);

        /// <summary>
        /// Запись истории изменений выборки
        /// </summary>
        /// <param name="action"></param>
        void InsertHistory(ActionHistory action);


        /// <summary>
        /// Сохранение выборки
        /// </summary>
        /// <param name="data"></param>
        /// <param name="comment"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        Selection SaveSelection(Selection data, string comment, SelectionSaveReason reason);

        /// <summary>
        /// Генерация имени выборки
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <returns>Имя выборки</returns>
        string GenerateUniqueName(string userSid);

        /// <summary>
        /// Проверка уникальности имени выборки
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <param name="selectionName">Имя выборки</param>
        /// <param name="selectionId">ID выборки</param>
        /// <returns>Новое имя выборки</returns>
        string VerifyUniqueName(string userSid, string selectionName, long selectionId);

        /// <summary>
        /// Список записий истории
        /// </summary>
        /// <param name="selectionId">ID выборки</param>
        /// <returns></returns>
        List<ActionHistory> GetActionsHistory(long selectionId);

        /// <summary>
        /// Получение объекта перехода (?)
        /// </summary>
        /// <returns></returns>
        List<SelectionTransition> GetStateTransitions();
    }
}
