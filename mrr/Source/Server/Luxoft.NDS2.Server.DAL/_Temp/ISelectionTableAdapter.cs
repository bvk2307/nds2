﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ISelectionTableAdapter
    {
        Selection Load(long id);

        void Insert(Selection data);

        void Update(Selection data);

        List<Selection> Search(DataQueryContext queryContext);

        int Count(FilterExpressionBase filter);
    }
}
