﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ICalendarAdapter
    {
        OperationResult<List<DateTime>> GetRedDays(DateTime begin, DateTime end);
        void AddRedDay(DateTime date);
        void RemoveRedDay(DateTime date);
    }
}
