﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IConfigExportInvoiceAdapter
    {
        ConfigExportInvoice Search();
    }
}
