﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common;

namespace Luxoft.NDS2.Server.DAL
{
    internal static class DataRecordExtension
    {
        # region Primitive

        private static bool HasColumnInResult(this IDataRecord reader, string name)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (name.Equals(reader.GetName(i), StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;

        }

        public static string GetString(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader[name].ToString() : string.Empty;
        }

        public static DateTime GetDateTime(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?   reader.GetDateTime(reader.GetOrdinal(name)) : default (DateTime);
        }

        public static int GetInt32(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader.GetInt32(reader.GetOrdinal(name)) : 0;
        }

        public static long GetInt64(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader.GetInt64(reader.GetOrdinal(name)) : 0;
        }

        public static decimal GetDecimal(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader.GetDecimal(reader.GetOrdinal(name)) : 0;
        }

        public static bool GetBoolean(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ? reader.GetInt32(name) > 0 : false;
        }


        public static string GetStringOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader[name].ToString() : string.Empty;
        }

        public static DateTime GetDateTimeOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetDateTime(reader.GetOrdinal(name)) : DateTime.MinValue;
        }

        public static int GetInt32OrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetInt32(reader.GetOrdinal(name)) : 0;
        }

        public static long GetInt64OrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetInt64(reader.GetOrdinal(name)) : 0;
        }

        public static decimal GetDecimalOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetDecimal(reader.GetOrdinal(name)) : 0;
        }

        public static bool GetBooleanOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) && reader.GetInt32(name) > 0;
        }





        #endregion

        #region Nullable

        public static DateTime? GetNullableDateTime(this IDataRecord reader, string name)
        {
            if (!reader.HasColumnInResult(name)) return default(DateTime?);

            var value = reader[reader.GetOrdinal(name)];
            return value == DBNull.Value ? default(DateTime?) : reader.GetDateTime(reader.GetOrdinal(name));
        }

        public static bool? GetNullableBoolean(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ? reader.GetNullableInt32(name).GetValueOrDefault(0) > 0 : default(bool?);
        }

        public static int? GetNullableInt32(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader[name].AsNullableInt32() : default (int?);
        }

        public static long? GetNullableInt64(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader[name].AsNullableInt64() : default(long?);
        }

        public static decimal? GetNullableDecimal(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader[name].AsNullableDecimal() : default(decimal?);
        }

        public static double? GetNullableDouble(this IDataRecord reader, string name)
        {
            return reader.HasColumnInResult(name) ?  reader[name].AsNullableDouble() : default (double?);
        }

        public static double? GetNullableDoubleOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetNullableDouble(name) : default(double?);
        }
        public static DateTime? GetNullableDateTimeOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            if (!metadata.Contains(name)) return default(DateTime?);

            var value = reader[reader.GetOrdinal(name)];
            return value == DBNull.Value ? default(DateTime?) : reader.GetNullableDateTime(name);
        }

        public static bool? GetNullableBooleanOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader.GetNullableInt32(name).GetValueOrDefault(0) > 0 : default(bool?);
        }

        public static int? GetNullableInt32OrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader[name].AsNullableInt32() : default(int?);
        }

        public static long? GetNullableInt64OrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader[name].AsNullableInt64() : default(long?);
        }

        public static decimal? GetNullableDecimalOrDefault(this IDataRecord reader, string name, IList<string> metadata)
        {
            return metadata.Contains(name) ? reader[name].AsNullableDecimal() : default(decimal?);
        }


        private static Int32? AsNullableInt32(this object value)
        {
            Int32 result;
            return value.IsDbNull() || !Int32.TryParse(Convert.ToString(value), out result) ? (Int32?)null : result;
        }

        private static Int64? AsNullableInt64(this object value)
        {
            Int64 result;
            return value.IsDbNull() || !Int64.TryParse(Convert.ToString(value), out result) ? (Int64?)null : result;
        }

        private static Decimal? AsNullableDecimal(this object value)
        {
            Decimal result;
            return value.IsDbNull() || !Decimal.TryParse(Convert.ToString(value), out result) ? (Decimal?)null : result;
        }

        private static Double? AsNullableDouble(this object value)
        {
            Double result;
            return value.IsDbNull() || !Double.TryParse(Convert.ToString(value), out result) ? (Double?)null : result;
        }

        private static bool IsDbNull(this object data)
        {
            return data == DBNull.Value;
        }

        #endregion
    }
}
