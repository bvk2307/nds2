﻿using System;
using Oracle.DataAccess.Client;
using System.Data;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Server.DAL
{
    public class TransactionConnectionFactory : ConnectionFactoryBase
    {
        private OracleTransaction _oraTransaction;

        private OracleConnection _oraConnection;

        public TransactionConnectionFactory(IServiceProvider configurationProvider)
            : base(configurationProvider)
        {
        }

        protected override OracleConnection CreateOracleConnectionInternal()
        {
            if (_oraTransaction == null)
            {
                _oraConnection = 
                    new OracleConnection(
                        ConfigurationProvider.GetConfigurationValue(Constants.DB_CONFIG_KEY));
                _oraConnection.Open();

                _oraTransaction = _oraConnection.BeginTransaction();                    
            }

            return _oraTransaction.Connection;
        }


        protected override void DisposeDependencies()
        {
            if (_oraTransaction != null)
            {
                _oraTransaction.Dispose();
                _oraTransaction = null;
            }

            if (_oraConnection != null)
            {
                if (_oraConnection.State != ConnectionState.Closed)
                {
                    _oraConnection.Close();
        }
                _oraConnection.Dispose();
                _oraConnection = null;
            }
        }

        public void Commit()
        {
            if (_oraTransaction != null)
            {
                _oraTransaction.Commit();
            }
        }

        public void Rollback()
        {
            if (_oraTransaction != null)
            {
                _oraTransaction.Rollback();
            }
        }
    }
}
