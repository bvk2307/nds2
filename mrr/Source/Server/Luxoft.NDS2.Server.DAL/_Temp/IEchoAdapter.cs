﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IEchoAdapter
    {
        List<string> GetDbInfo();
    }
}