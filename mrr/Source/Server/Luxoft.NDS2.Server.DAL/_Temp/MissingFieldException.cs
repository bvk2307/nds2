﻿namespace Luxoft.NDS2.Server.DAL
{
    public class MissingFieldException : DatabaseException
    {
        private const string MessageFormat = "Поля {0} ожидались в {1}, но не были возвращены БД";

        internal MissingFieldException(string[] fieldNames, string objectName)
            : base(
                string.Format(
                    MessageFormat,
                    string.Join(",", fieldNames),
                    objectName),
                null)
        {
        }
    }
}
