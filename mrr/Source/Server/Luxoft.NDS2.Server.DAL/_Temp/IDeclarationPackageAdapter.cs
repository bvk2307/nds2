﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDeclarationPackageAdapter
    {
        /// <summary>
        /// Возвращает декларацию по ZIP (также фигурирует как DECLARATION_VERSION_ID)
        /// </summary>
        /// <param name="zip">Идентификатор</param>
        /// <returns>Декларация</returns>
        DeclarationSummary GetDeclaration(long zip);

        /// <summary>
        /// Возвращает список ZIPов для комбинированного ключа ИНН + год + месяц
        /// </summary>
        /// <param name="inn">ИНН подавшего НД</param>
        /// <param name="year">Отчетный год</param>
        /// <param name="month">Месяц, входящий в отчетный период</param>
        /// <returns>Список ZIP НД, ранжированный по актуальности</returns>
        List<DeclarationRankedZip> GetContractorDeclarationZipList(long inn, int year, int month);

        /// <summary>
        /// Возвращает список документов КНП декларации
        /// </summary>
        /// <param name="innContractor"></param>
        /// <param name="kppEffective"></param>
        /// <param name="typeCode"></param>
        /// <param name="period"></param>
        /// <param name="year"></param>
        /// <param name="paginationDetails"></param>
        /// <returns>Список докуменнтов КНП</returns>
        PageResult<DocumentKNP> GetDocumentsKNP(string innContractor, string kppEffective, int typeCode, string period, string year, DataPagination paginationDetails);

        /// <summary>
        /// Получить у декларации назначенного SID инспектора
        /// </summary>
        /// <param name="innContractor">ИНН НП</param>
        /// <param name="kppEffective">Эффективный КПП </param>
        /// <param name="type">Тип декларации (декларация/журнал)</param>
        /// <param name="period">Налоговый период</param>
        /// <param name="year">Год</param>
        /// <returns>inspector sid</returns>
        string GetDeclarationOwner(string innContractor, string kppEffective, int type, string period, string year);
    }
}
