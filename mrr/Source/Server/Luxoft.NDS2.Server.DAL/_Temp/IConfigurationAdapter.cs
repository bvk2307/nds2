﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы работы с настройками системы
    /// </summary>
    public interface IConfigurationAdapter
    {
        /// <summary>
        /// Вычитывает конфигурацию из БД
        /// </summary>
        /// <returns>Параметры конфигурации</returns>
        Configuration Read();

        /// <summary>
        /// Сохраняет конфигурацию в БД
        /// </summary>
        /// <param name="config">Конфигурация</param>
        void Save(Configuration config);

        /// <summary>
        /// Сохраняет параметр конфигурации в БД
        /// </summary>
        /// <param name="par">Параметр конфигурации</param>
        void SaveParam(ConfigurationParamName par, string value);


        OperationResult<List<FilterCriteria>> GetAutoselectionFilterCriteria();

        OperationResult<List<CFGRegion>> GetRegionParam(CFGParam ParamType);
        OperationResult<CFGRegion> SetRegionParamValue(CFGRegion param);
    }
}
