﻿using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.Implementation;

namespace Luxoft.NDS2.Server.DAL
{
    public static class SovInvoiceRequestAdapterCreator
    {
        public static ISovInvoiceRequestAdapter SovInvoiceRequestAdapter(this IServiceProvider service)
        {
            return new SovInvoiceRequestAdapter(service);
        }
    }
}
