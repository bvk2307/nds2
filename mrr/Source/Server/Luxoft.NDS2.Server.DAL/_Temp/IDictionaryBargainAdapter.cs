﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDictionaryBargainAdapter
    {
        List<OperCode> GetList();
    }
}
