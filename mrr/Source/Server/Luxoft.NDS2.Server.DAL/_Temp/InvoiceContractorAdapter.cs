﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL
{
    internal abstract class InvoiceContractorAdapter
        : SingleTableAdapterBase<InvoiceContractor>
    {
        private const string InvoiceKeyColumnName = "ROW_KEY_ID";

        public InvoiceContractorAdapter(string tableName, IServiceProvider serviceProvider)
            : base(tableName, serviceProvider)
        {
        }

        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<InvoiceContractor>.GetMemberName(x => x.InvoiceKey),
                   InvoiceKeyColumnName);
        }

        protected override List<InvoiceContractor> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<InvoiceContractor>();

            while(reader.Read())
            {
                dataList.Add(new InvoiceContractor
                                 {
                                     InvoiceKey = reader.GetString(InvoiceKeyColumnName),
                                     Inn = reader.GetString(InnColumnName),
                                     Kpp = reader.GetString(KppColumnName)
                                 });
            }

            return dataList;
        }

        protected abstract string InnColumnName { get; }

        protected abstract string KppColumnName { get; }
    }
}
