﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IInvoiceContractorDeclarationAdapter
    {
        Dictionary<long, bool> Search(FilterExpressionBase expression);
    }
}
