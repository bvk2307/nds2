﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ITechnologicalReportAdapter
    {
        void CalcReportData(DateTime date);

        void SaveGP3Data(DateTime date, Dictionary<long, long[]> data);

        bool AllSubsystemsReady(DateTime date);

        Dictionary<int, string> GetReportData(DateTime date);

        void WriteTemplate(string path, string name);

        Dictionary<long, long[]> GetGp3Data(DateTime date);

        List<SystemLogEntry> GetSystemLog();
    }
}