﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDeclarationAdapter
    {
        DeclarationSummary SearchLastDeclaration(string inn, string kpp = null);

        DeclarationSummary SearchLastFullLoadedCorrection(string innContractor, string kppEffective, string period, string year, int typeCode);

        PageResult<DeclarationSummary> Search(QueryConditions conditions);

        long GetActual(string inn, string kppEffective, string year, string period, int typeCode);

        ActKNP GetActKnp(long declarationZip);

        List<DeclarationVersion> GetVersions(string innDeclarant, string kppEffective, int periodEffective, string year, int typeCode);

        DeclarationSummary Version(long declarationId);

        DeclarationSummary GetDeclaration(long zip);

        void SetInspector(string innDeclarant, string kppEffective, string period, string year, int typeCode, string nameInspector, string sidInspector);

        PageResult<Invoice> GetDeclarationInvoices(QueryConditions conditions, long requestId);    

        Dictionary<int, int> GetChaptersSellers(long declarationId, string inn);

        Dictionary<int, int> GetChaptersBuyers(long declarationId, string inn);

        int? GetPriority(long declarationId, int chapter);
    }
}
