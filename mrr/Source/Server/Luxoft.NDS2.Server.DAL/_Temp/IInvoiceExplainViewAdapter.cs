﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IInvoiceExplainViewAdapter
    {
        List<InvoiceExplain> Search(FilterExpressionBase filter);
    }
}
