﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices;

namespace Luxoft.NDS2.Server.DAL
{
    public static class SovInvoiceAdapterCreator
    {
        public static ISovInvoiceAdapter SovInvoiceAdapter(this IServiceProvider service)
        {
            return new SovInvoiceAdapter(service);
        }

        public static ISovInvoiceAdapter ImpalaInvoiceAdapter(this IServiceProvider service)
        {
            return new ImpalaInvoiceAdapter(service);
        }
    }
}
