﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы адаптера таблицы VERSION_HISTORY, которая содержит список выпускаемых версий МРР
    /// </summary>
    public interface IVersionHistoryAdapter
    {
        /// <summary>
        /// Ищет данные о версии с максимальной датой выхода в КПЭ
        /// </summary>
        /// <returns>Ссылка на объект с данными версии</returns>
        VersionInfo SearchLatest();

        /// <summary>
        /// Возвращает перечень версий отсортированный по дает выхода в КПЭ 
        /// у которых указано имя файла с составом версий.
        /// </summary>
        /// <returns></returns>
        IEnumerable<VersionInfo> GetVersionsWithReleaseNotes();
    }
}
