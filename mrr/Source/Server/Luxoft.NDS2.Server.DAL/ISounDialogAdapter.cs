﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ISounDialogAdapter
    {
        SounDataContainer Load(Region region, QueryConditions conditions);

        void Save(Region region);
    }
}