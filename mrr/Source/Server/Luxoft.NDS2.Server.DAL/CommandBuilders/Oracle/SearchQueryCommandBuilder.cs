﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class SearchQueryCommandBuilder : QueryCommandBuilder
    {
        private readonly FilterExpressionBase _searchBy;

        private readonly IEnumerable<ColumnSort> _orderBy;

        private readonly string _selectStatementPattern;

        private readonly IQueryPatternProvider _queryPattern;

        public SearchQueryCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern)
        {
            _selectStatementPattern = selectStatementPattern;
            _searchBy = searchBy;
            _orderBy = orderBy;
            _queryPattern = queryPattern;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var paramBuilder = new OracleParameterCollectionBuilder(
                parameters,
                "p_{0}");

            return string.Format(
                _selectStatementPattern,
                _searchBy.ToQuery(paramBuilder, _queryPattern),
                _orderBy.ToSQL(_queryPattern));
        }
    }
}
