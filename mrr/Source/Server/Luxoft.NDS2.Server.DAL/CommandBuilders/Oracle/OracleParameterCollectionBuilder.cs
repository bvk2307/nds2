﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class OracleParameterCollectionBuilder : IParameterBuilder
    {
        private readonly OracleParameterCollection _parameters;

        private readonly string _parameterNamePattern;

        private int _parameterIndex = 0;

        public OracleParameterCollectionBuilder(
            OracleParameterCollection parameters,
            string parameterNamePattern)
        {
            _parameters = parameters;
            _parameterNamePattern = parameterNamePattern;
        }

        public string Add(object value)
        {
            _parameterIndex++;
            var parameterName = string.Format(_parameterNamePattern, _parameterIndex);

            _parameters.Add(ConverToParameter(parameterName, value));

            return ":"+parameterName;
        }

        private OracleParameter ConverToParameter(string name, object value)
        {
            if (value is Enum)
            {
                return new OracleParameter(name, (int)value);
            }

            if (value is bool)
            {
                return new OracleParameter(name, (bool)value ? 1 : 0);
            }

            return new OracleParameter(name, value);
        }
    }
}
