﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    public abstract class QueryCommandBuilder : IOracleCommandBuilder
    {
        private const string CursorParameter = "pResult";

        private const string CommandPattern = "begin open :{0} for {1}; end;";

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.BindByName = true;
            command.CommandText = 
                string.Format(CommandPattern, CursorParameter, BuildSelectStatement(command.Parameters));

            command.Parameters.Add(FormatCommandHelper.Cursor(CursorParameter));

            return command;
        }

        protected abstract string BuildSelectStatement(OracleParameterCollection parameters);
    }
}
