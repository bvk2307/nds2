﻿using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class ExecuteProcedureCommandBuilder : ProcedureCommandBuilder
    {
        private readonly string _selectStatementPattern;

        public ExecuteProcedureCommandBuilder(string selectStatementPattern)
        {
            _selectStatementPattern = selectStatementPattern;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            DefineParameters(parameters);
            return _selectStatementPattern;
        }

        protected virtual void DefineParameters(OracleParameterCollection parameters)
        {
        }
    }
}
