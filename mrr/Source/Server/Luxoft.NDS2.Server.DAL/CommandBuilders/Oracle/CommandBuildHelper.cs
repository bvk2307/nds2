﻿using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal static class CommandBuildHelper
    {
        # region Command Text

        public static string SelectPattern(this string sourceName, string aliasName)
        {
            return string.Format("select * from {0} {1} where {2}", sourceName, aliasName, "{0} {1}");
        }

        public static string SelectPattern(this string viewName)
        {
            return string.Format("select * from {0} where {1}", viewName, "{0} {1}");
        }

        public static string CountPattern(this string viewName)
        {
            return string.Format("{0} where {1}", viewName, "{0}");
        }

        public static string PackageProcedure(string packageName, string procedureName)
        {
            return string.Format("{0}.{1}", packageName, procedureName);
        }

        # endregion

        # region Command Parameters

        public static OracleParameter Cursor(string name)
        {
            return new OracleParameter(name, OracleDbType.RefCursor, null, ParameterDirection.Output);
        }

        # endregion
    }
}
