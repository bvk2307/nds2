﻿using System.Data.Common;
using System.Data.Odbc;
using System.Data;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    public class PlainTextCommandBuilder : QueryCommandBuilder
    {
        private string _sql;
        public PlainTextCommandBuilder(string sqlQuery)
        {
            _sql = sqlQuery;
        }


        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            return _sql;

        }
    }
}
