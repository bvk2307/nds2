﻿using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal abstract class ProcedureCommandBuilder : IOracleCommandBuilder
    {
        private const string CommandPattern = "{0}";

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.BindByName = true;
            command.CommandText =
                string.Format(CommandPattern, BuildSelectStatement(command.Parameters));

            return command;
        }

        protected abstract string BuildSelectStatement(OracleParameterCollection parameters);
    }
}
