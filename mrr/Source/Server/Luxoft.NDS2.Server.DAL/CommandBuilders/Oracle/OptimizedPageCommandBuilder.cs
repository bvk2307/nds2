﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class OptimizedPageCommandBuilder : SearchQueryCommandBuilder
    {
        private const string QueryPattern = @"select * from (select (rownum - 1) rn, t.* from ({0}) T where (rownum - 1) < :{2}) R where rn >= :{1}";

        private const string ParameterFromName = "pFrom";

        private const string ParameterToName = "pTo";

        private readonly uint _toRowNumber;

        private readonly uint _fromRowNumber;

        public OptimizedPageCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern,
            uint fromRowNumber,
            uint toRowNumber)
            : base(
                selectStatementPattern,
                searchBy,
                orderBy,
                queryPattern)
        {
            _fromRowNumber = fromRowNumber;
            _toRowNumber = toRowNumber;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var innerStatement = base.BuildSelectStatement(parameters);

            parameters.Add(ParameterFromName, OracleDbType.Int32).Value = _fromRowNumber;
            parameters.Add(ParameterToName, OracleDbType.Int32).Value = _toRowNumber;

            return string.Format(QueryPattern, innerStatement, ParameterFromName, ParameterToName);
        }
    }
}
