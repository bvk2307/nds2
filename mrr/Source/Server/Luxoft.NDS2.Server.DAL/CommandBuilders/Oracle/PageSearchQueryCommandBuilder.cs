﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class PageSearchQueryCommandBuilder : SearchQueryCommandBuilder
    {
        private const string IndexedPrefix = "indexed_query";

        private const string IndexKey = "row_number";

        private const string PageQueryPattern = 
            "WITH indexed_query AS (SELECT ROWNUM as row_number, q.* FROM ({0}) q) SELECT * FROM indexed_query WHERE (indexed_query.row_number>:pFrom AND indexed_query.row_number<=:pTo)";

        private const string SkipQueryPattern =
            "WITH indexed_query AS (SELECT ROWNUM as row_number, q.* FROM ({0}) q) SELECT * FROM indexed_query WHERE (indexed_query.row_number>:pFrom)";

        private int _rowsToSkip;

        private int? _rowsToTake;

        public PageSearchQueryCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern)
            : base(selectStatementPattern, searchBy, orderBy, queryPattern)
        {
        }

        public PageSearchQueryCommandBuilder Skip(int rowsToSkip)
        {
            _rowsToSkip = rowsToSkip;

            return this;
        }

        public PageSearchQueryCommandBuilder Take(int? rowsToTake)
        {
            _rowsToTake = rowsToTake;

            return this;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var innerStatement = base.BuildSelectStatement(parameters);

            if (_rowsToTake.HasValue)
            {
                parameters.Add("pFrom", _rowsToSkip);
                parameters.Add("pTo", _rowsToTake.Value + _rowsToSkip);

                return string.Format(
                    PageQueryPattern,
                    innerStatement,
                    _rowsToSkip,
                    _rowsToTake.Value + _rowsToSkip);
            }

            if (_rowsToSkip > 0)
            {
                parameters.Add("pFrom", _rowsToSkip);

                return string.Format(
                    SkipQueryPattern,
                    innerStatement,
                    _rowsToSkip);
            }

            return innerStatement;
        }
    }
}
