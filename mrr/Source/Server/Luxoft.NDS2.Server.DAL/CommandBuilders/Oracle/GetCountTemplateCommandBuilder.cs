﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal class GetCountTemplateCommandBuilder : CountTemplateCommandBuilder
    {
        private readonly FilterExpressionBase _searchBy;

        private readonly string _selectStatementPattern;

        private readonly IQueryPatternProvider _queryPattern;

        public GetCountTemplateCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IQueryPatternProvider queryPattern)
        {
            _selectStatementPattern = selectStatementPattern;
            _searchBy = searchBy;
            _queryPattern = queryPattern;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var paramBuilder = new OracleParameterCollectionBuilder(
                parameters,
                "p_{0}");

            return string.Format(
                _selectStatementPattern,
                _searchBy.ToQuery(paramBuilder, _queryPattern));
        }
    }
}
