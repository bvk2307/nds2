﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle
{
    internal abstract class CountTemplateCommandBuilder : IOracleCommandBuilder
    {
        private const string ResultParameter = "pResult";

        private const string CommandPattern = "{0}";

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.BindByName = true;
            command.CommandText =
                string.Format(CommandPattern, BuildSelectStatement(command.Parameters));

            command.Parameters.Add(FormatCommandHelper.NumericOut(ResultParameter));

            return command;
        }

        protected abstract string BuildSelectStatement(OracleParameterCollection parameters);
    }
}
