﻿using System.Data.Common;
using System.Data.Odbc;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    public class PlainTextCommandBuilder : QueryCommandBuilder
    {
        private string _sql;
        public PlainTextCommandBuilder(string sqlQuery)
        {
            _sql = sqlQuery;
        }

        protected override string BuildSelectStatement(IDataParameterCollection parameters)
        {
            return _sql;
        }
    }
}
