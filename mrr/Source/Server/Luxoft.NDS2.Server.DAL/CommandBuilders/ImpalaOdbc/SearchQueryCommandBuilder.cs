﻿using System.Data;
using System.Data.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    internal class SearchQueryCommandBuilder : QueryCommandBuilder
    {
        private readonly FilterExpressionBase _searchBy;

        private readonly IEnumerable<ColumnSort> _orderBy;

        private string _selectStatementPattern;

        private readonly IQueryPatternProvider _queryPattern;

        public SearchQueryCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern)
        {
            _selectStatementPattern = selectStatementPattern;
            _searchBy = searchBy;
            _orderBy = orderBy;
            _queryPattern = queryPattern;
        }

        protected override string BuildSelectStatement(IDataParameterCollection parameters)
        {
            var paramBuilder = new OdbcParameterCollectionBuilder(
                parameters as DbParameterCollection,
                "p_{0}");

            return string.Format(
                _selectStatementPattern,
                _searchBy.ToQuery(paramBuilder, _queryPattern),
                _orderBy.ToSQL(_queryPattern));
        }
    }
}
