﻿using System.Data.Common;
using System.Data.Odbc;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    internal class OdbcParameterCollectionBuilder : IParameterBuilder
    {
        private readonly DbParameterCollection _parameters;

        private readonly string _parameterNamePattern;

        private int _parameterIndex = 0;

        public OdbcParameterCollectionBuilder(
            DbParameterCollection parameters,
            string parameterNamePattern)
        {
            _parameters = parameters;
            _parameterNamePattern = parameterNamePattern;
        }

        public string Add(object value)
        {
            _parameterIndex++;
            var parameterName = string.Format(_parameterNamePattern, _parameterIndex);

            _parameters.Add(ConverToParameter("@"+parameterName, value));

            return GenerateParameterValue(value);
        }

        private Dictionary<Type, Func<object, string>> CreateConvertingFunction()
        {
            var convertingFunction = new Dictionary<Type, Func<object, string>>();

            Func<object, string> funcDateTime = (value) => 
            { 
                int? timeValue = null;
                var time = value as DateTime?;
                if (time != null)
                {
                    timeValue = time.ToShortIntFormat(); 
                }
                return string.Format(" CAST({0} as int) ", timeValue);
            };

            convertingFunction.Add(typeof(Enum), (value) => { return string.Format(" CAST({0} as int) ", (int)value); });
            convertingFunction.Add(typeof(Int32), (value) => { return string.Format(" CAST({0} as int) ", (int)value); });
            convertingFunction.Add(typeof(Int64), (value) => { return string.Format(" CAST({0} as bigint) ", (long)value); });
            convertingFunction.Add(typeof(DateTime), funcDateTime);
            convertingFunction.Add(typeof(DateTime?), funcDateTime);
            convertingFunction.Add(typeof(bool), (value) => { return string.Format(" CAST({0} as tinyint) ", (bool)value ? 1 : 0); });
            convertingFunction.Add(typeof(string), (value) => { return string.Format(" CAST('{0}' as string) ", (string)value); });

            return convertingFunction;
        }

        private string GenerateParameterValue(object value)
        {
            string retValue = String.Empty;

            var convertingFunction = CreateConvertingFunction();

            var valueType = value.GetType();
            if (convertingFunction.ContainsKey(valueType))
            {
                retValue = convertingFunction[valueType].Invoke(value);
            }
            else
            {
                retValue = string.Format(" CAST('{0}' as string) ", value);
            }

            return retValue;
        }

        private DbParameter ConverToParameter(string name, object value)
        {
            if (value is Enum)
            {
                return new OdbcParameter(name, (int)value);
            }

            if (value is bool)
            {
                return new OdbcParameter(name, (bool)value ? 1 : 0);
            }

            if (value is Int64)
            {
                return new OdbcParameter(name, OdbcType.BigInt){Value = value};
            }

            var time = value as DateTime?;
            if (time != null)
            {
                return new OdbcParameter(name, OdbcType.Int) { Value = time.ToShortIntFormat() };
            }

            return new OdbcParameter(name, value);
        }
    }
}
