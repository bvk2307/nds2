﻿using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using Luxoft.NDS2.Server.DAL.Helpers;
using System;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    internal class PageSearchQueryCommandBuilder : SearchQueryCommandBuilder
    {
        private const string PageQueryPattern =
            "{0} limit {1} offset {2};";

        private const string SkipQueryPattern =
            "{0} offset {1};";

        private int _rowsToSkip;

        private int? _rowsToTake;

        public PageSearchQueryCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern)
            : base(selectStatementPattern, searchBy, orderBy, queryPattern)
        {
        }

        public PageSearchQueryCommandBuilder Skip(int rowsToSkip)
        {
            _rowsToSkip = rowsToSkip;

            return this;
        }

        public PageSearchQueryCommandBuilder Take(int? rowsToTake)
        {
            _rowsToTake = rowsToTake;

            return this;
        }

        protected override string BuildSelectStatement(IDataParameterCollection parameters)
        {
            var innerStatement = base.BuildSelectStatement(parameters);

            if (_rowsToTake.HasValue)
            {
                parameters.Add(OdbcParameterHelper.NumericIn("pLimit", _rowsToTake.Value));
                parameters.Add(OdbcParameterHelper.NumericIn("pOffset", _rowsToSkip));

                return string.Format(
                    PageQueryPattern,
                    innerStatement,
                    _rowsToTake.Value,
                    _rowsToSkip);
            }

            if (_rowsToSkip > 0)
            {
                parameters.Add(OdbcParameterHelper.NumericIn("pOffset", _rowsToSkip));

                return string.Format(
                    SkipQueryPattern,
                    innerStatement,
                    _rowsToSkip);
            }

            return innerStatement;
        }
    }

    public static class OdbcParameterHelper
    {
        /// <summary>
        /// Строит параметр типа NumericIn
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OdbcParameter NumericIn(string name, object value)
        {
            return new OdbcParameter(name, OdbcType.BigInt){ Direction = ParameterDirection.Input, Value = value };
        }
    }
}
