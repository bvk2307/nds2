﻿using System.Data.Common;
using System.Data.Odbc;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    public abstract class QueryCommandBuilder : IDbCommandBuilder
    {
        protected abstract string BuildSelectStatement(IDataParameterCollection parameters);

        public IDbCommand BuildCommand(IDbConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandText = BuildSelectStatement(command.Parameters);

            return command;
        }
    }
}
