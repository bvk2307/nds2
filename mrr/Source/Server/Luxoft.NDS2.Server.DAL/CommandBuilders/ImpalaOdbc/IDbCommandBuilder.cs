﻿using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    internal interface IDbCommandBuilder
    {

        IDbCommand BuildCommand(IDbConnection connection);
    }
}
