﻿using System.Collections.Generic;
using System.Data.Common;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc
{
    internal class LoggerParameterCollectionBuilder : IParameterBuilder
    {
        public List<object> Values { get; private set; } 

        public LoggerParameterCollectionBuilder()
        {
            Values = new List<object>();
        }


        public string Add(object value)
        {
            Values.Add(value);

            return string.Empty;
        }
    }
}
