﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IExplainReplyAdapter
    {
        ExplainReplyInfo GetExplainReplyInfo(long id);

        List<ExplainReplyInfo> GetExplainReplyList(long docId, QueryConditions conditions);

        List<ExplainReplyInfo> GetListExplainsOfDocument(long docId);

        List<ExplainReplyHistoryStatus> GetExplainReplyHistoryStatues(long explainId);

        ExplainInvoice GetOneExplainInvoice(ExplainReplyInfo explainInfo, ExplainRegim regim, string invoiceId);
        
        void UpdateInvoiceCorrect(long explainId, ExplainInvoiceCorrect correct, InvoiceCorrectTypeOperation typeOperation);
        void UpdateExplainStatus(long explainId, int explainStatusId);
        void UpdateInvoiceCorrectState(long explainId, string invoiceId, ExplainInvoiceStateInThis state);
        void DeleteInvoiceCorrectState(long explainId, string invoiceId);
        void DeleteAllInvoiceCorrectState(long explainId);
        bool ExistsInvoiceChangeOfExplain(long explainId);
        List<ExplainInvoiceCorrect> GetExplainAllInvoiceCorrect(long explainId);
        List<ExplainInvoice> GetInvoicesOnlyCorrectInThisExplain(ExplainReplyInfo explainInfo);

        void UpdateExplainFileName(long explainId, string fileName);
        void UpdateExplainZip(long explainId, long zip);
    }
}
