﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    class MacroReportDiscrepancyMapAdapter : MacroReportAbstractOracleTableAdapter, IMacroReportDiscrepancyMapAdapter
    {
        public MacroReportDiscrepancyMapAdapter(IServiceProvider service) : base(service) { }

        public List<MapResponseDataDiscrepancy> GetMapData(MapRequestData model)
        {
            var submitDate = GetMapData(MacroReportType.Discrepancy);
            if (submitDate != null)
            {
                return ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    CreateNewRequest(model, submitDate.Value.ToString("dd.MM.yyyy")),
                    CommandType.Text,
                    new OracleParameter[] {},
                    BuildMapResponseData);
            }
            return new List<MapResponseDataDiscrepancy>();
        }

        private MapResponseDataDiscrepancy BuildMapResponseData(OracleDataReader reader)
        {
            var obj = new MapResponseDataDiscrepancy();

            obj.AGGREGATE_CODE = reader.ReadString(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.AGGREGATE_CODE));
            obj.AGGREGATE_NAME = reader.ReadString(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.AGGREGATE_NAME));
            obj.FISCAL_YEAR = reader.ReadInt(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.FISCAL_YEAR));
            obj.QUARTER = reader.ReadInt(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.QUARTER));
            obj.MISMATCH_TOTAL_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.MISMATCH_TOTAL_SUM));
            obj.MISMATCH_NDS = reader.ReadDecimal(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.MISMATCH_NDS));
            obj.MISMATCH_TNO_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.MISMATCH_TNO_SUM));
            obj.GAP_AMOUNT = reader.ReadDecimal(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.GAP_AMOUNT));
            obj.WEIGHT_MISMATCH_IN_TOTAL_OPER = reader.ReadDecimal(TypeHelper<MapResponseDataDiscrepancy>.GetMemberName(t => t.WEIGHT_MISMATCH_IN_TOTAL_OPER));

            return obj;
        }

        private string CreateNewRequest(MapRequestData model, string submitDate)
         {
             //Используем переключатель:
             // 1  =   Округа, все кварталы
             // 2  =   Регионы, все кварталы
             // 3  =   Инспекции, все кварталы 

             return String.Format(
                 "select m.aggregate_code " +
                 ",(case when {0} = 1 then fd.description " +
                 "       when {0} = 2 then ssrf.s_name " +
                 "       when {0} = 3 then sono.s_name end " +
                 " ) as aggregate_name " +
                 ", m.fiscal_year " +
                 ", m.quarter " +
                 ", m.mismatch_total_sum as mismatch_total_sum " + // Сумма расхождений всего
                 ", m.weak_and_nds_sum + m.exact_and_nds_sum  as mismatch_nds " + // Cумма расхождений типа НДС
                 ", m.gap_amount        as gap_amount " + // Сумма расхождений по разрывам
                 ", m.mismatch_tno_sum  as mismatch_tno_sum " + //  Сумма расхождений отправленных в составе требований и истребований ТНО
                 ", ROUND( nvl(100 * m.mismatch_total_count / nullif(m.invoice_record_count, 0), 0), 2) as weight_mismatch_in_total_oper " + // -- Удельный вес операций с расхождениями в общем количестве операций в процентах
          
                 "from " +
                 "( " +
                 "  select " +
                 "      (case when {0} = 1 then TO_CHAR(e.district_id) " +
                 "            when {0} = 2 then e.region_code_r " +
                 "            when {0} = 3 then e.inspection_code end " +
                 "      ) as aggregate_code " +
                 "     ,(case when 2 = 3 then max(e.district_id) " +
                 "            when 2 = 2 then max(e.district_id) " +
                 "            when 2 = 4 then max(e.district_id) end) as federal_district " +
                 "     ,(case when 2 = 4 then max(e.region_code_r) end) as region_code " +
                 "     ,(case when 2 = 4 then max(e.inspection_code) end) as soun_code " +                      
                 "     ,e.fiscal_year " +
                 "     ,e.quarter " +
            
                 "     ,sum(primary_declaration_count) as primary_declaration_count " +
                 "     ,sum(correction_declaration_count) as correction_declaration_count " +
                 "     ,sum(decl_with_mediator_ops_count) as decl_with_mediator_ops_count " +
                 "     ,sum(journal_count) as journal_count " +
                 "     ,sum(invoice_record_count) as invoice_record_count " +
                 "     ,sum(invoice_record_count_chapter8) as invoice_record_count_chapter8 " +
                 "     ,sum(invoice_record_count_chapter9) as invoice_record_count_chapter9 " +
                 "     ,sum(invoice_record_count_chapter10) as invoice_record_count_chapter10 " +
                 "     ,sum(invoice_record_count_chapter11) as invoice_record_count_chapter11 " +
                 "     ,sum(invoice_record_count_chapter12) as invoice_record_count_chapter12 " +
                 "     ,sum(exact_match_count) as exact_match_count " +
                 "     ,sum(prim_decl_with_mismatch_count) as prim_decl_with_mismatch_count " +
                 "     ,sum(corr_decl_with_mismatch_count) as corr_decl_with_mismatch_count " +
                 "     ,sum(decl_with_kc_mismatch_count) as decl_with_kc_mismatch_count " +
                 "     ,sum(mismatch_total_count) as mismatch_total_count " +
                 "     ,sum(mismatch_total_sum) as mismatch_total_sum " +
                 "     ,sum(weak_and_nds_count) as weak_and_nds_count " +
                 "     ,sum(weak_and_nds_sum) as weak_and_nds_sum " +
                 "     ,sum(weak_and_no_nds_count) as weak_and_no_nds_count " +
                 "     ,sum(exact_and_nds_count) as exact_and_nds_count " +
                 "     ,sum(exact_and_nds_sum) as exact_and_nds_sum " +
                 "     ,sum(gap_count) as gap_count " +
                 "     ,sum(gap_sum) as gap_amount " +
                 "     ,sum(currency_count) as currency_count " +
                 "     ,sum(currency_sum) as currency_amount " +
                 "     ,sum(mismatch_tno_count) as mismatch_tno_count " +
                 "     ,sum(mismatch_tno_sum) as mismatch_tno_sum " +                                                                  
              
                 "  from " +
                 "  ( " +              
                 //    Выбираем всех, кроме МИ по КН и вообще с кодом 99nn 
                 "     select    r.*, fd.district_id " +
                 //         конвертируем налоговые периоды в кварталы
                 "          , case when r.fiscal_period  in ('1', '01', '02', '03', '21', '51', '71', '72', '73') then 1 " +
                 "                 when r.fiscal_period  in ('2', '04', '05', '06', '22', '54', '74', '75', '76') then 2 " +
                 "                 when r.fiscal_period  in ('3', '07', '08', '09', '23', '55', '77', '78', '79') then 3 " +
                 "                 when r.fiscal_period  in ('4', '10', '11', '12', '24', '56', '80', '81', '82') then 4 " +
                 "                 else 0 end       AS quarter  " +
                
                 "     from " +
                 "     ( " +
                 "         select sm.*, SUBSTR(sm.inspection_code, 1, 2) as region_code_r " +
                 "         from SOV_MATCH_RESULT_SUMMARY sm " +  
                 //        Исключаем инспекции 99
                 "         where  SUBSTR(sm.inspection_code, 1, 2) <> '99' " +
                   
                 "         UNION " +
                 
                 //        Инспекция по Байконуру - к Московской обл
                 "         select sm.*, '50' as region_code_r " +
                 "         from SOV_MATCH_RESULT_SUMMARY sm " +
                 "         where  sm.inspection_code = '9901' " +
                   
                 "     ) r " +
                 "     join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code_r " +
                 "     join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id " +
                 "     where " +                                                               
                 "         (case when {0} = 1 then 1  " +
                 "               when {0} = 2 and fd.district_id in ('{1}')   then 1 " + // выбор из списка
                 "               when {0} = 3 and r.region_code_r in ('{1}')  then 1 " + // выбор из списка
                 "               else 0 end) = 1 " +
                 "         and trunc(r.process_date) = trunc(TO_DATE('{2}', 'DD.MM.YYYY')) " +              
                      
                 "     UNION " +
                
                 //    МИ по КН: " +
                 "     select    r.*, fd.district_id " +
                 //      конвертируем налоговые периоды в кварталы " +
                 "      , case when r.fiscal_period  in ('1', '01', '02', '03', '21', '51', '71', '72', '73') then 1 " +
                 "             when r.fiscal_period  in ('2', '04', '05', '06', '22', '54', '74', '75', '76') then 2 " +
                 "             when r.fiscal_period  in ('3', '07', '08', '09', '23', '55', '77', '78', '79') then 3 " +
                 "             when r.fiscal_period  in ('4', '10', '11', '12', '24', '56', '80', '81', '82') then 4 " +
                 "             else 0 end       AS quarter " +
                
                 "     from " +
                 "     ( " +
                 "       select sm.*, '99' as region_code_r " +
                 "       from SOV_MATCH_RESULT_SUMMARY sm " +
                 "       where  sm.inspection_code = '9971' or sm.inspection_code = '9972' or sm.inspection_code = '9973' or sm.inspection_code = '9974' or sm.inspection_code = '9975' or sm.inspection_code = '9976' or sm.inspection_code = '9977' or sm.inspection_code = '9978' or sm.inspection_code = '9979' " +
                 "     ) r " +
                 "     join FEDERAL_DISTRICT fd on fd.district_id = '99' " +
                 "     where " +
                 "          (case when {0} = 1 then 1 " +
                 //                для МИ по КН нет регионов
                 //                -- when {0} = 2 and fd.district_id in ('99')  then 1    -- выбор из списка
                 "                when {0} = 3 and r.region_code_r in ('{1}') then 1 " + // выбор из списка 
                 "                else 0 end " +
                 "          ) = 1 " +
                 "          and trunc(r.process_date) = trunc(TO_DATE('{2}', 'DD.MM.YYYY')) " +
                 //     end МИ по КН
                 "  ) e " +

                 "  WHERE   TO_CHAR(e.fiscal_year) ||  '-' || TO_CHAR(e.quarter)   < to_char(sysdate, 'YYYY-Q') " + // отбираем только предыдущие кварталы (от текущей даты)
                 // Выходные еще надо бы учитывать (если смотрят в последний выходной день квартала - то квартал уже считается завершившимся?) 
                 "  AND     TO_CHAR(e.fiscal_year) ||  '-' || TO_CHAR(e.quarter)   >=  TO_CHAR(TO_NUMBER(TO_CHAR(sysdate, 'YYYY'))-2) || '-' ||  TO_CHAR(sysdate, 'Q') " + // отбираем кварталы -8 
                 "  AND     TO_CHAR(e.fiscal_year) ||  '-' || TO_CHAR(e.quarter)   >= '2014-4' " + // отбираем только кварталы после 2014 кв 4
                 "  group by (case when {0} = 1 then TO_CHAR(e.district_id) " +
                 "                 when {0} = 2 then e.region_code_r " +
                 "                 when {0} = 3 then e.inspection_code end), fiscal_year, quarter " +
                 ") m " +
      
                 "left outer join FEDERAL_DISTRICT fd on fd.district_id = m.aggregate_code " +
                 "left outer join V$SSRF ssrf         on ssrf.s_code = m.aggregate_code " +
                 "left outer join V$SONO sono         on sono.s_code = m.aggregate_code " +
 
                 "order by aggregate_name, fiscal_year, quarter"
                 , (int)model.MapType
                 , model.RegionId,
                 submitDate
             );
         }
    }
}
