﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.Services.Helpers;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс IDiscrepancyAdapter
    /// </summary>
    internal class DiscrepancyAdapter : BaseOracleTableAdapter_New, IDiscrepancyAdapter
    {
        # region Константы

        private const string SCOPE_NAME = "Discrepancy";

        private const string SELECT_DISCREPANCY_SUMMARY_TEMPLATE_NAME = @"SELECT_DISCREPANCY_SUMMARY_TEMPLATE";
        private const string DISCREPANCY_SIDE_MULTIPLE_DATA_TEMPLATE_NAME = "DiscrepancyCard.Sides.Invoice.MultipleData";
        private const string DISCREPANCY_SIDE_TEMPLATE_NAME = @"DiscrepancyCard.Sides";
        private const string DISCREPANCY_IS_CLOSED_TEMPLATE_NAME = @"DiscrepancyCard.DiscrepancyIsClosed";

        private const string SearchSQLCommandPattern =
            "BEGIN OPEN :pCursor FOR {0}; END;";

        private const string GetErrorsMappingSQL = @"
select
 a.Error_Code as ErrorCode,
 a.Chapter as Chapter,
 a.Is_Dop_List as IsDopList,
 a.Line_Number as LineNumber,
 b.Name_Check as Name,
 b.Descreption as Description,
 a.Property_Name as DataMembers
from Logical_Errors_Mapping a
left outer join DICT_LOGICAL_ERRORS b on b.error_code = a.error_code";


        private const string AttachToInspectorCmd = "PAC$DECLARATION.Attach_to_inspector";
        private const string UpdateUserCommentSql = @"
BEGIN 
    merge into DISCREPANCY_USER_COMMENTS tgt 
      using ( select :pDiscrepancyId as id, :pUserComment as comment_text from dual ) src 
      on (tgt.discrepancy_id = src.id)
    when matched then 
      update set tgt.comment_text = src.comment_text 
    when not matched then 
      insert (tgt.discrepancy_id, tgt.comment_text) values (src.id, src.comment_text); 
    commit;
END;";
        
        private const string ViewAlias = "vw";
        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";
        private const string pDiscrepancyId = "pDiscrepancyId";
        private const string pUserComment = "pUserComment";
        private const string pRequestId = "pRequestId";

        # endregion

        private readonly long _discrepancyId;

        private IServiceProvider _serviceProvider;

        # region Конструкторы

        public DiscrepancyAdapter(IServiceProvider service,  long discrepancyId)
            : base(service)
        {
            _discrepancyId = discrepancyId;
            _serviceProvider = service;
        }

        # endregion

        public DiscrepancySummaryInfo GetSummaryInfo()
        {
            var sqlTemplate = _serviceProvider.GetQueryText(SCOPE_NAME, SELECT_DISCREPANCY_SUMMARY_TEMPLATE_NAME);

            var context = new CommandContext
            {
                Text = sqlTemplate,
                Type = CommandType.Text,
                Parameters = new List<OracleParameter> { FormatCommandHelper.NumericIn(":pId", _discrepancyId) }
            };
            var list = ExecuteList<DiscrepancySummaryInfo>(context);
            DiscrepancySummaryInfo result = list.FirstOrDefault();
            if (result == null) return new DiscrepancySummaryInfo();

            result.ErrorsDictionary = GetErrorsMapping();

            return result;
        }

        # region Потроение объекта DiscrepancySummaryInfo

        private DiscrepancyInvoice ReadDiscrepancyInvoice(OracleDataReader reader)
        {
            return new DiscrepancyInvoice()
            {
                CodeCurrency = reader.ReadString("okv_code"),
                SalesCostInCurrency = reader.ReadDecimal("Price_sell"),
                SalesCostInRuble = reader.ReadDecimal("price_sell_in_curr")
            };
        }

        public RequestStatusType GetInvoiceRequestStatus(long requestId)
        {
            List<int> res = new List<int>();
            var data = ExecutePrimitiveList<int>(
                "select status from invoice_request where id = :p1",
                CommandType.Text,
                new OracleParameter[] { FormatCommandHelper.NumericIn(":p1", requestId) });
            return data.Count == 0 ? RequestStatusType.NotRegistered : (RequestStatusType)data[0];
        }

        private List<ErrorMapping> GetErrorsMapping()
        {
            var context = new CommandContext
            {
                Text = GetErrorsMappingSQL,
                Type = CommandType.Text,
                Parameters = new List<OracleParameter>()
            };

            var result = ExecuteList<ErrorMapping>(context);
            result.ForEach(item => item.DataMembersArray = item.DataMembers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

            return result;
        }


        public bool DiscrepancyIsClosed()
        {
            var sqlTemplate = _serviceProvider.GetQueryText(SCOPE_NAME, DISCREPANCY_IS_CLOSED_TEMPLATE_NAME);
            var result = ExecuteList<long>(
                sqlTemplate,
                CommandType.Text,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn("discrepancy_id", _discrepancyId),
                    FormatCommandHelper.Cursor("pCursor")
                });

            return result.Any();
        }

        public List<DiscrepancySide> GetDiscrepancySides()
        {
            var sqlTemplate = _serviceProvider.GetQueryText(SCOPE_NAME, DISCREPANCY_SIDE_TEMPLATE_NAME);
            var result = ExecuteList<DiscrepancySide>(
                sqlTemplate,
                CommandType.Text,
                new OracleParameter[]
            {
                FormatCommandHelper.NumericIn("discrepancy_id", _discrepancyId),
                FormatCommandHelper.Cursor("pCursor")
            });

            ReadDiscrepancySides(result);
            return result;
        }

        private void ReadDiscrepancySides(List<DiscrepancySide> sides)
        {
            var multipleDataSql = _serviceProvider.GetQueryText(SCOPE_NAME, DISCREPANCY_SIDE_MULTIPLE_DATA_TEMPLATE_NAME);
            var multipleData = ExecuteList<DiscrepancyInvoiceMultipleData>(
                multipleDataSql,
                CommandType.Text,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn("discrepancy_id", _discrepancyId),
                    FormatCommandHelper.Cursor("pCursor")
                });

            for (int i = 0; i < sides.Count; i++)
            {
                var item = sides[i];

                var parameters = new[] { 
                    FormatCommandHelper.VarcharIn("pRowKey", item.InvoiceId), 
                    FormatCommandHelper.NumericIn("pDiscrepancyId", _discrepancyId),
                    FormatCommandHelper.Cursor("pCursor") 
                };

                var buyBooks = ExecuteList<AggregateBuyBook>(FormatCommandHelper.DiscrepanciesPackageProcedure("GetBuyBooks"), CommandType.StoredProcedure, parameters);
                AppendMultipleDataToBuyBook(buyBooks, multipleData);
                if(buyBooks.Any(b => IsInvoiceAggregate(b.RowKey)))
                {
                    item.BuyBooks.AddRange(buyBooks.Where(p => IsInvoiceAggregate(p.RowKey)));
                    item.BuyBooks.First().BuyBookDetails.AddRange(buyBooks.Where(p => !IsInvoiceAggregate(p.RowKey)));
                }
                else
                {
                    item.BuyBooks = buyBooks;
                }

                var sellBooks = ExecuteList<AggregateSellBook>(FormatCommandHelper.DiscrepanciesPackageProcedure("GetSellBooks"), CommandType.StoredProcedure, parameters);
                AppendMultipleDataToSellBook(sellBooks, multipleData);
                if(sellBooks.Any(p => IsInvoiceAggregate(p.RowKey)))
                {
                    item.SellBooks.AddRange(sellBooks.Where(p => IsInvoiceAggregate(p.RowKey)));
                    item.SellBooks.First().SellBookDetails.AddRange(sellBooks.Where(p => !IsInvoiceAggregate(p.RowKey)));
                }
                else
                {
                    item.SellBooks.AddRange(sellBooks);
                }

                var sentJournals = ExecuteList<AggregateSentInvoiceJournal>(FormatCommandHelper.DiscrepanciesPackageProcedure("GetSentJournal"), CommandType.StoredProcedure, parameters);
                AppendMultipleDataToSentJournal(sentJournals, multipleData);
                if (sentJournals.Any(p => IsInvoiceAggregate(p.RowKey)))
                {
                    item.SentInvoiceJournals.AddRange(sentJournals.Where(p => IsInvoiceAggregate(p.RowKey)));
                    item.SentInvoiceJournals.First().SentInvoiceJournalDetails.AddRange(sentJournals.Where(p => !IsInvoiceAggregate(p.RowKey)));
                }
                else
                {
                    item.SentInvoiceJournals.AddRange(sentJournals);
                }

                var receiveJournal = ExecuteList<AggregateRecievedInvoiceJournal>(FormatCommandHelper.DiscrepanciesPackageProcedure("GetRecieveJournal"), CommandType.StoredProcedure, parameters);
                AppendMultipleDataToReceiveJournal(receiveJournal, multipleData);
                if (receiveJournal.Any(p => IsInvoiceAggregate(p.RowKey)))
                {
                    item.RecievedInvoiceJournals.AddRange(receiveJournal.Where(p => IsInvoiceAggregate(p.RowKey)));
                    item.RecievedInvoiceJournals.First().RecievedInvoiceJournalDetails.AddRange(receiveJournal.Where(p => !IsInvoiceAggregate(p.RowKey)));
                }
                else
                {
                    item.RecievedInvoiceJournals.AddRange(receiveJournal);
                }
            }
        }

        private void AppendMultipleDataToSellBook(List<AggregateSellBook> bookLines, List<DiscrepancyInvoiceMultipleData> multipleData)
        {
            foreach(var line in bookLines)
            {
                var rk = line.RowKey;
                line.BuyerInfoINN = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value1));
                line.BuyerInfoKPP = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value2));
                line.BuyerInfoName = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value3));
                line.OperationCode = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.OperationCode).Select(r => r.Value1));
                line.PaymentDocNumber = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.PaymentDocuments).Select(r => r.Value1));
            }
        }

        private void AppendMultipleDataToBuyBook(List<AggregateBuyBook> bookLines, List<DiscrepancyInvoiceMultipleData> multipleData)
        {
            foreach (var line in bookLines)
            {
                var rk = line.RowKey;
                line.SellerInfoINN = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value1));
                line.SellerInfoKPP = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value2));
                line.SellerInfoName = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value3));
                line.OperationCode = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.OperationCode).Select(r => r.Value1));
                line.PaymentDocNumber = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.PaymentDocuments).Select(r => r.Value1));
            }
        }

        private void AppendMultipleDataToSentJournal(List<AggregateSentInvoiceJournal> bookLines, List<DiscrepancyInvoiceMultipleData> multipleData)
        {
            foreach (var line in bookLines)
            {
                var rk = line.RowKey;
                line.SellerInfoINN = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value1));
                line.SellerInfoKPP = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value2));
                line.SellerInfoName = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value3));
                line.BuyerInfoINN = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value1));
                line.BuyerInfoKPP = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value2));
                line.BuyerInfoName = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Buuyer).Select(r => r.Value3));
                line.OperationCode = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.OperationCode).Select(r => r.Value1));
                //line.PaymentDocNumber = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.PaymentDocuments).Select(r => r.Value1));
            }
        }


        private void AppendMultipleDataToReceiveJournal(List<AggregateRecievedInvoiceJournal> bookLines, List<DiscrepancyInvoiceMultipleData> multipleData)
        {
            foreach (var line in bookLines)
            {
                var rk = line.RowKey;
                line.SellerInfoINN = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value1));
                line.SellerInfoKPP = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value2));
                line.SellerInfoName = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.Seller).Select(r => r.Value3));
                line.OperationCode = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.OperationCode).Select(r => r.Value1));
                //line.PaymentDocNumber = string.Join(",", multipleData.Where(d => d.RowKey.Equals(rk) && d.Kind == (int)DiscrepancyInvoiceMultipleDataKind.PaymentDocuments).Select(r => r.Value1));
            }
        }


        private List<int> SelectErrorCodes(string codes)
        {
            int codeValue = 0;
            var codesEnum =
                from code in codes.Split(',')
                where int.TryParse(code, out codeValue)
                select codeValue;
            return codes == null ? new List<int>() : codesEnum.ToList();
        }

        private bool IsInvoiceAggregate(string rowKey)
        {
            if (string.IsNullOrEmpty(rowKey)) return false;

            bool ret = false;
            string aggregateCode = String.Empty;
            int indexSlash = rowKey.IndexOf('/');
            if (indexSlash >= 0)
            {
                if ((rowKey.Length - indexSlash) >= 4)
                {
                    string code = rowKey.Substring(indexSlash + 1, 4);
                    indexSlash = code.IndexOf('/');
                    if (indexSlash >= 0)
                    {
                        aggregateCode = code.Substring(indexSlash, 2);
                    }
                }
            }
            if (aggregateCode == "/A")
            {
                ret = true;
            }
            return ret;
        }

        #endregion

        #region Ввод пояснений
        public void SaveComment(DetailsBase details)
        {
            string procedureName;
            OracleParameter[] parameters;

            if (details is DetailsBuyBook)
            {
                procedureName = "NDS2$Discrepancies.SaveBuyBookComment";
                parameters = DetailsBuyBookCommentParameters((DetailsBuyBook)details);
            }
            else if (details is DetailsSellBook)
            {
                procedureName = "NDS2$Discrepancies.SaveSellBookComment";
                parameters = DetailsSellBookCommentParameters((DetailsSellBook)details);
            }
            else if (details is DetailsSentInvoiceJournal)
            {
                procedureName = "NDS2$Discrepancies.SaveSentJournalComment";
                parameters = DetailsSentJournalCommentParameters((DetailsSentInvoiceJournal)details);
            }
            else if (details is DetailsRecievedInvoiceJournal)
            {
                procedureName = "NDS2$Discrepancies.SaveRecieveJournalComment";
                parameters = DetailsRecieveJournalCommentParameters((DetailsRecievedInvoiceJournal)details);
            }
            else
            {
                return;
            }
            Execute(procedureName, CommandType.StoredProcedure, parameters);
        }

        private OracleParameter[] DetailsRecieveJournalCommentParameters(DetailsRecievedInvoiceJournal obj)
        {
            return new OracleParameter[] 
            {
                FormatCommandHelper.NumericIn("p_discrepancy_id", _discrepancyId),
                FormatCommandHelper.NumericIn("p_chapter", 11),
                FormatCommandHelper.VarcharIn("p_row_key", obj.RowKey),
                FormatCommandHelper.DecimalIn("p_price_total", obj.Amount),
                FormatCommandHelper.VarcharIn("p_broker_inn", obj.SubcommissionAgentINN),
                FormatCommandHelper.VarcharIn("p_broker_kpp", obj.SubcommissionAgentKPP),
                FormatCommandHelper.NumericIn("p_deal_kind_code", obj.DealTypeCode),
                FormatCommandHelper.DateIn("p_correction_date", obj.CorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_correction_num", obj.CorrectedInvoiceNumber),
                FormatCommandHelper.DecimalIn("p_diff_correct_decrease", obj.CostDifferenceDecrease),
                FormatCommandHelper.DecimalIn("p_diff_correct_increase", obj.CostDifferenceIncrease),
                FormatCommandHelper.VarcharIn("p_currency_code", obj.CurrencyCode),
                FormatCommandHelper.DateIn("p_change_correction_date", obj.FixedCorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_correction_num", obj.FixedCorrectedInvoiceNumber),
                FormatCommandHelper.DateIn("p_change_date", obj.FixedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_num", obj.FixedInvoiceNumber),
                FormatCommandHelper.DateIn("p_invoice_date", obj.InvoiceDate),
                FormatCommandHelper.VarcharIn("p_invoice_num", obj.InvoiceNumber),
                FormatCommandHelper.VarcharIn("p_operation_code", obj.OperationCode),
                FormatCommandHelper.VarcharIn("p_seller_inn", obj.SellerInfoINN),
                FormatCommandHelper.VarcharIn("p_seller_kpp", obj.SellerInfoKPP),
                FormatCommandHelper.DateIn("p_receive_date", obj.RecieveDate),
                FormatCommandHelper.DecimalIn("p_price_nds_total", obj.TaxAmount),
                FormatCommandHelper.DecimalIn("p_diff_correct_nds_decrease", obj.TaxDifferenceDecrease),
                FormatCommandHelper.DecimalIn("p_diff_correct_nds_increase", obj.TaxDifferenceIncrease)
            };
        }

        private OracleParameter[] DetailsSentJournalCommentParameters(DetailsSentInvoiceJournal obj)
        {
            return new OracleParameter[] 
            {
                FormatCommandHelper.NumericIn("p_discrepancy_id", _discrepancyId),
                FormatCommandHelper.NumericIn("p_chapter", 10),
                FormatCommandHelper.VarcharIn("p_row_key", obj.RowKey),
                FormatCommandHelper.DecimalIn("p_price_total", obj.Amount),
                FormatCommandHelper.VarcharIn("p_buyer_inn", obj.BuyerInfoINN),
                FormatCommandHelper.VarcharIn("p_buyer_kpp", obj.BuyerInfoKPP),
                FormatCommandHelper.DateIn("p_correction_date", obj.CorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_correction_num", obj.CorrectedInvoiceNumber),
                FormatCommandHelper.DecimalIn("p_diff_correct_decrease", obj.CostDifferenceDecrease),
                FormatCommandHelper.DecimalIn("p_diff_correct_increase", obj.CostDifferenceIncrease),
                FormatCommandHelper.VarcharIn("p_currency_code", obj.CurrencyCode),
                FormatCommandHelper.DateIn("p_change_correction_date", obj.FixedCorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_correction_num", obj.FixedCorrectedInvoiceNumber),
                FormatCommandHelper.DateIn("p_change_date", obj.FixedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_num", obj.FixedInvoiceNumber),
                FormatCommandHelper.DateIn("p_invoice_date", obj.InvoiceDate),
                FormatCommandHelper.VarcharIn("p_invoice_num", obj.InvoiceNumber),
                FormatCommandHelper.VarcharIn("p_operation_code", obj.OperationCode),
                FormatCommandHelper.VarcharIn("p_seller_inn", obj.SellerInfoINN),
                FormatCommandHelper.VarcharIn("p_seller_kpp", obj.SellerInfoKPP),
                FormatCommandHelper.DateIn("p_create_date", obj.SendDate),
                FormatCommandHelper.DecimalIn("p_price_nds_total", obj.TaxAmount),
                FormatCommandHelper.DecimalIn("p_diff_correct_nds_decrease", obj.TaxDifferenceDecrease),
                FormatCommandHelper.DecimalIn("p_diff_correct_nds_increase", obj.TaxDifferenceIncrease)
            };
        }

        private OracleParameter[] DetailsSellBookCommentParameters(DetailsSellBook obj)
        {
            return new OracleParameter[] 
            {
                FormatCommandHelper.NumericIn("p_discrepancy_id", _discrepancyId),
                FormatCommandHelper.NumericIn("p_chapter", 9),
                FormatCommandHelper.VarcharIn("p_row_key", obj.RowKey),
                FormatCommandHelper.VarcharIn("p_buyer_inn", obj.BuyerInfoINN),
                FormatCommandHelper.VarcharIn("p_buyer_kpp", obj.BuyerInfoKPP),
                FormatCommandHelper.DateIn("p_receipt_doc_date", obj.PaymentDocDate),
                FormatCommandHelper.VarcharIn("p_receipt_doc_num", obj.PaymentDocNumber),
                FormatCommandHelper.DateIn("p_correction_date", obj.CorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_correction_num", obj.CorrectedInvoiceNumber),
                FormatCommandHelper.VarcharIn("p_currency_code", obj.CurrencyCode),
                FormatCommandHelper.DateIn("p_change_correction_date", obj.FixedCorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_correction_num", obj.FixedCorrectedInvoiceNumber),
                FormatCommandHelper.DateIn("p_change_date", obj.FixedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_num", obj.FixedInvoiceNumber),
                FormatCommandHelper.VarcharIn("p_broker_inn", obj.IntermediaryInfoINN),
                FormatCommandHelper.VarcharIn("p_broker_kpp", obj.IntermediaryInfoKPP),
                FormatCommandHelper.DateIn("p_invoice_date", obj.InvoiceDate),
                FormatCommandHelper.VarcharIn("p_invoice_num", obj.InvoiceNumber),
                FormatCommandHelper.VarcharIn("p_operation_code", obj.OperationCode),
                FormatCommandHelper.DecimalIn("p_price_sell_0", obj.TaxableAmount0),
                FormatCommandHelper.DecimalIn("p_price_sell_10", obj.TaxableAmount10),
                FormatCommandHelper.DecimalIn("p_price_sell_18", obj.TaxableAmount18),
                FormatCommandHelper.DecimalIn("p_price_nds_10", obj.TaxAmount10),
                FormatCommandHelper.DecimalIn("p_price_nds_18", obj.TaxAmount18),
                FormatCommandHelper.DecimalIn("p_price_tax_free", obj.TaxFreeAmount),
                FormatCommandHelper.DecimalIn("p_price_sell_in_curr", obj.TotalAmountCurrency),
                FormatCommandHelper.DecimalIn("p_price_sell", obj.TotalAmountRouble)
            };
        }

        private OracleParameter[] DetailsBuyBookCommentParameters(DetailsBuyBook obj)
        {
            return new OracleParameter[] 
            {
                FormatCommandHelper.NumericIn("p_discrepancy_id", _discrepancyId),
                FormatCommandHelper.NumericIn("p_chapter", 8),
                FormatCommandHelper.VarcharIn("p_row_key", obj.RowKey),
                FormatCommandHelper.DateIn("p_buy_accept_date", obj.AccountingDate),
                FormatCommandHelper.DateIn("p_correction_date", obj.CorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_correction_num", obj.CorrectedInvoiceNumber),
                FormatCommandHelper.VarcharIn("p_currency_code", obj.CurrencyCode),
                FormatCommandHelper.DateIn("p_change_correction_date", obj.FixedCorrectedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_correction_num", obj.FixedCorrectedInvoiceNumber),
                FormatCommandHelper.DateIn("p_change_date", obj.FixedInvoiceDate),
                FormatCommandHelper.VarcharIn("p_change_num", obj.FixedInvoiceNumber),
                FormatCommandHelper.VarcharIn("p_broker_inn", obj.IntermediaryInfoINN),
                FormatCommandHelper.VarcharIn("p_broker_kpp", obj.IntermediaryInfoKPP),
                FormatCommandHelper.DateIn("p_invoice_date", obj.InvoiceDate),
                FormatCommandHelper.VarcharIn("p_invoice_num", obj.InvoiceNumber),
                FormatCommandHelper.VarcharIn("p_customs_declaration_num", obj.NumberTD),
                FormatCommandHelper.VarcharIn("p_operation_code", obj.OperationCode),
                FormatCommandHelper.VarcharIn("p_seller_inn", obj.SellerInfoINN),
                FormatCommandHelper.VarcharIn("p_seller_kpp", obj.SellerInfoKPP),
                FormatCommandHelper.DecimalIn("p_price_nds_total", obj.TaxAmount),
                FormatCommandHelper.DecimalIn("p_price_total", obj.TotalAmount)
            };
        }
        #endregion

        public void SaveDiscrepancyUserComment(string comment)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDiscrepancyId, _discrepancyId));
            parameters.Add(FormatCommandHelper.VarcharIn(pUserComment, comment));

            Execute(UpdateUserCommentSql,
                CommandType.Text,
                parameters.ToArray());
        }
    }
}