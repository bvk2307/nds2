﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public interface ISimpleAdapter<TData>
    {
        List<TData> All();
    }

    public static class SimpleAdapterCreator
    {
        public static ISimpleAdapter<TData> Create<TData>(IServiceProvider service, string source)
            where TData : new()
        {
            return new SimpleAdapter<TData>(service, source);
        }
    }
    
    class SimpleAdapter<TData> :
        BaseOracleTableAdapter, ISimpleAdapter<TData>
        where TData : new()
    {
        private readonly string _source;

        public SimpleAdapter(IServiceProvider service, string source)
            : base(service)
        {
            _source = source;
        }

        public List<TData> All()
        {
            return new ListCommandExecuter<TData>(new GenericDataMapper<TData>(), _service)
                    .TryExecute(new CommandBuilder(_source)).ToList();
        }

        class CommandBuilder : QueryCommandBuilder
        {
            private readonly string _source;

            public CommandBuilder(string source)
            {
                _source = source;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                return string.Format("select * from {0}", _source);
            }
        }
    }

}
