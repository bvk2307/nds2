﻿using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Base
{
    abstract class ViewAdapterBase<T> : BaseOracleTableAdapter
    {
        private readonly string _viewName;

        private readonly string _keyName;

        private const string ViewAlias = "vw";

        private const string CursorAlias = "pCursor";

        private const string CountResult = "pResult";

        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";

        protected ViewAdapterBase(IServiceProvider service, string viewName, string keyName)
            : base(service)
        {
            _viewName = viewName;
            _keyName = keyName;
        }

        protected virtual string GetSqlPattern()
        {
            return string.Format("select * from {0} vw where 1=1 and {{1}} {{2}}", _viewName);
        }

        protected virtual string GetCountSqlPattern()
        {
            return string.Format("BEGIN SELECT COUNT(*) INTO :pResult FROM {0} vw WHERE 1=1 and {{1}}; END;", _viewName);
        }

        protected abstract T Read(OracleDataReader oracleDataReader);

        protected virtual PageResult<T> Search(QueryConditions criteria)
        {
            var query = criteria.ToSQL(GetSqlPattern(), ViewAlias, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                Read);

            query = criteria.ToSQL(GetCountSqlPattern(), ViewAlias, false, true);

            query.Parameters.Add(new QueryParameter(false) { Name = CountResult });

            var result = Execute(query);

            return new PageResult<T>(data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        public PageResult<T> All()
        {
            var data = ExecuteList(
                string.Format(GetSqlPattern(), "1=1", string.Empty),
                CommandType.Text,
                new OracleParameter[] { },
                Read);
            var result = Execute(
                string.Format(GetCountSqlPattern(), "1=1", string.Empty),
                CommandType.Text,
                new OracleParameter[] { });

            return new PageResult<T>(data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        public T SelectByKey(string id)
        {
            var data = ExecuteList(
                string.Format(GetSqlPattern(), string.Format("{0} = {1}", _keyName, id), string.Empty),
                CommandType.Text,
                new OracleParameter[] {},
                Read);
            return data != null && data.Count > 0 ? data.First() : default(T);
        }
    }
}
