﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class SounDialogAdapter : BaseOracleTableAdapter, ISounDialogAdapter
    {
        private const string SelectInspectionsSQL = "SELECT_INSPECTIONS";
        private const string DeleteInspectionsSQL = "DELETE_INSPECTIONS";
        private const string InsertInspectionSQL = "INSERT_INSPECTION";
        private const string AddEmptyRegionSQL = "ADD_REGION";
        private const string RemoveRegionSQL = "REMOVE_REGION";
        private const string GetInspectionsCountSQL = "GET_INSPECTIONS_COUNT";

        private const string pRegionId = "pRegionId";
        private const string pSounCode = "pSounCode";
        private const string pRegionCode = "pRegionCode";
        private const string pContextId = "pContextId";
        private const string pCursor = "pCursor";
        private const string pCount = "pCount";
        private const string vRowsCount = "vRowsCount";

        public SounDialogAdapter(IServiceProvider service) : base(service) { }


        class SortCondition
        {
            public Func<Inspection, string> MemberLambda { get; set; }
            public bool IsDescending { get; set; }
            public SortCondition(Func<Inspection, string> memberLambda, bool isDescending)
            {
                MemberLambda = memberLambda;
                IsDescending = isDescending;
            }
        }

        public SounDataContainer Load(Region region, QueryConditions conditions)
        {
            var result = new SounDataContainer();
            
            var data = ExecuteList(FormatCommandHelper.MethodologistPackageProcedure(SelectInspectionsSQL),
                CommandType.StoredProcedure,
                new []
                {
                    FormatCommandHelper.NumericIn(pRegionId, region.Id),
                    FormatCommandHelper.VarcharIn(pRegionCode, region.Code),
                    FormatCommandHelper.NumericIn(pContextId, (long)region.Context),
                    FormatCommandHelper.Cursor(pCursor)
                },
                DataReaderExtension.ReadInspection);

            #region Счетчик

            var counter = Execute(
                "begin select count(1) into :pCount from V$SONO vw where substr(vw.s_code, 1, 2) = :pRegionCode; end;",
                CommandType.Text, new[]
                {
                    FormatCommandHelper.VarcharIn(pRegionCode, region.Code),
                    FormatCommandHelper.NumericOut(pCount)
                });

            result.TotalInspectionCount = DataReaderExtension.ReadInt(counter.Output[pCount]);

            #endregion

            #region сортировка

            if (conditions != null)
            {
                var conditionList = new List<SortCondition>();
                foreach (var sort in conditions.Sorting)
                {
                    // не самый удачный перебор
                    if (sort.ColumnKey == TypeHelper<Inspection>.GetMemberName(x => x.Code))
                        conditionList.Add(new SortCondition(x => x.Code, sort.Order == ColumnSort.SortOrder.Desc));
                    else if (sort.ColumnKey == TypeHelper<Inspection>.GetMemberName(x => x.Name))
                        conditionList.Add(new SortCondition(x => x.Name, sort.Order == ColumnSort.SortOrder.Desc));
                }

                if (conditionList.Count > 0)
                {
                    IOrderedEnumerable<Inspection> linq = (conditionList[0].IsDescending)
                        ? data.OrderByDescending(conditionList[0].MemberLambda)
                        : data.OrderBy(conditionList[0].MemberLambda);
                    for (int i = 1; i < conditionList.Count; ++i)
                    {
                        linq = (conditionList[i].IsDescending)
                            ? linq.ThenByDescending(conditionList[i].MemberLambda)
                            : linq.ThenBy(conditionList[i].MemberLambda);
                    }
                    data = linq.ToList();
                }
            }

            #endregion

            result.Inspections = data;

            return result;
        }

        public void Save(Region region)
        {
            var count = InspectionsCount(region.Id);
            Func<Inspection, bool> selected = i => i.Selected;
            var fullSelection = region.Inspections.Count == region.Inspections.Count(selected);
            var commands = new List<CommandContext>();

            if (count == 0)
            {
                if (!fullSelection)
                {
                    SaveRegion(region);
                    commands.AddRange(region.Inspections.Where(selected).Select(i => InsertInspection(region.Id, i.Code)));
                    commands.Add(AddEmptyRegion(region.Code, region.Context));
                }
            }
            else
            {
                commands.Add(DeleteInpections(region.Id));
                if (region.Inspections.Count(selected) == 0 || fullSelection)
                {
                    commands.Add(RemoveRegion(region.Id));
                }
                else
                {
                    commands.AddRange(region.Inspections.Where(selected).Select(i => InsertInspection(region.Id, i.Code)));
                    commands.Add(AddEmptyRegion(region.Code, region.Context));
                }
            }
            commands.ForEach(command => Execute(command));
        }

        private CommandContext AddEmptyRegion(string code, RegionContext context)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.VarcharIn(pRegionCode, code),
                FormatCommandHelper.NumericIn(pContextId, (long)context)
            };
            return new CommandContext
            {
                Text = FormatCommandHelper.MethodologistPackageProcedure(AddEmptyRegionSQL),
                Type = CommandType.StoredProcedure,
                Parameters = parameters
            };
        }

        private CommandContext RemoveRegion(long respId)
        {
            var parameters = new List<OracleParameter> { FormatCommandHelper.NumericIn(pRegionId, respId) };
            return new CommandContext
            {
                Text = FormatCommandHelper.MethodologistPackageProcedure(RemoveRegionSQL),
                Type = CommandType.StoredProcedure,
                Parameters = parameters
            };
        }

        private CommandContext InsertInspection(long respId, string sounCode)
        {
            var parameters = new List<OracleParameter> 
            {
                FormatCommandHelper.NumericIn(pRegionId, respId),
                FormatCommandHelper.VarcharIn(pSounCode, sounCode)
            };
            return new CommandContext
            {
                Text = FormatCommandHelper.MethodologistPackageProcedure(InsertInspectionSQL),
                Type = CommandType.StoredProcedure,
                Parameters = parameters
            };
        }

        private CommandContext DeleteInpections(long respId)
        {
            var parameters = new List<OracleParameter> { FormatCommandHelper.NumericIn(pRegionId, respId) };
            return new CommandContext
            {
                Text = FormatCommandHelper.MethodologistPackageProcedure(DeleteInspectionsSQL),
                Type = CommandType.StoredProcedure,
                Parameters = parameters
            };
        }

        private int InspectionsCount(long respId)
        {
            var parameters = new List<OracleParameter> { FormatCommandHelper.NumericIn(pRegionId, respId) };
            using (var connection = base.Connection())
            using (var cmd = new OracleCommand(FormatCommandHelper.MethodologistPackageProcedure(GetInspectionsCountSQL), connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.Add(new OracleParameter(vRowsCount, OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(FormatCommandHelper.NumericIn(pRegionId, respId));
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                cmd.ExecuteNonQuery();
                return int.Parse(cmd.Parameters[vRowsCount].Value.ToString());
            }
        }

        internal void SaveRegion(Region region)
        {
            if (!region.IsNew)
            {
                return;
            }
            var command = new CommandContext
            {
                Text = "insert into metodolog_region (id, context_id, code) values (:pId, :pContextId, :pCode)",
                Type = CommandType.Text,
                Parameters = new List<OracleParameter>
                {
                    FormatCommandHelper.NumericIn(":pId", region.Id),
                    FormatCommandHelper.NumericIn(":pContextId", (long)region.Context),
                    FormatCommandHelper.VarcharIn(":pCode", region.Code)
                }
            };
            Execute(command);
        }
    }
}