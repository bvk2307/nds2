﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Packages;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class SovInvoiceRequestAdapter : BaseOracleTableAdapter, ISovInvoiceRequestAdapter
    {
        private readonly SovRequestPackageAdapter _package;

        public SovInvoiceRequestAdapter(IServiceProvider service)
            : base(service)
        {
            _package = new SovRequestPackageAdapter(service);
        }

        public SovInvoiceRequest SearchById(long requestId)
        {
            var result = _package.GetInvoiceRequest(requestId);

            if (!result.Any())
            {
                throw new ObjectNotFoundException();
            }

            return result.First();
        }

        public List<SovInvoiceRequest> SearchByDeclaration(long declarationId, int partition)
        {
            return _package.SearchInvoiceRequest(declarationId, partition);
        }
    }
}
