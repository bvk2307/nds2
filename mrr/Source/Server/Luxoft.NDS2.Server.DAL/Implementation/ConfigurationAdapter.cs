﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ConfigHelper
    {
        private readonly Dictionary<string, ParamValueContainer> _dic;

        public ConfigHelper(Dictionary<string, ParamValueContainer> dic)
        {
            _dic = dic;
        }

        public ParamValueContainer Get(string name, ParamValueContainer def = null)
        {
            if (def == null)
                def = new ParamValueContainer {Value = "", DefaultValue = ""};
            ParamValueContainer result;
            return _dic.TryGetValue(name, out result) ? result : def;
        }
    }
    /// <summary>
    /// Этот класс реализует адаптер таблицы с настройками системы
    /// </summary>
    internal class ConfigurationAdapter : BaseOracleTableAdapter, IConfigurationAdapter
    {
        private Dictionary<ConfigurationParamName, string> ConfigParamsMap = new Dictionary<ConfigurationParamName, string>()
        {
            { ConfigurationParamName.TR_GP3_InputFileNameFormat,            "tr_gp3_input_file_name_format" },
            { ConfigurationParamName.TR_GP3_InputFilesLocation,             "tr_gp3_input_files_location" },
            { ConfigurationParamName.TR_ProcessMachineName,                 "tr_process_machine_name" },
            { ConfigurationParamName.TR_ReportBuildTimeBegin,               "tr_report_build_time_begin" },
            { ConfigurationParamName.TR_ReportBuildTimeEnd,                 "tr_report_build_time_end" },
            { ConfigurationParamName.TR_ReportFileNameFormat,               "tr_report_file_name_format" },
            { ConfigurationParamName.TR_ReportFilesLocation,                "tr_report_files_location" },
            { ConfigurationParamName.TR_TimerPeriodSec,                     "tr_timer_period_sec" },

            { ConfigurationParamName.Number_invoice_warning,                "number_invoice_warning" },
            { ConfigurationParamName.ClientPath,                            "client_install_path" },
            { ConfigurationParamName.ServiceSelectionThreadCountMax,        "service_selection_thread_count_max" },
            { ConfigurationParamName.ReclaimJobThreadCountMax,              "reclaim_job_thread_count_max" },
            { ConfigurationParamName.ReclaimLogInfoMessageUse,              "reclaim_log_info_message_use" },
            { ConfigurationParamName.ReclaimManualCheckDocUse,              "reclaim_manual_check_doc_use" }
        };

        private const string CharTrue = "Y";
        private const string CharFalse = "N";

        public ConfigurationAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public Configuration Read()
        {
            
            var result = ExecuteList(
                    "select PARAMETER, VALUE, DEFAULT_VALUE from CONFIGURATION",
                    CommandType.Text,
                    new OracleParameter[0],
                    (reader) =>
                        new KeyValuePair<string, ParamValueContainer>
                        (
                            reader["PARAMETER"].ToString(),
                            new ParamValueContainer { Value = reader["VALUE"].ToString(), DefaultValue = reader["DEFAULT_VALUE"].ToString() }
                        )
                    );

            Dictionary<string, ParamValueContainer> resultDict = result.ToDictionary(x => x.Key, y => y.Value);

            var helper = new ConfigHelper(resultDict);

            Configuration ret = new Configuration()
            {
                TR_TimerPeriodSec = AsIntParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_TimerPeriodSec], ParamValueContainer.Default<int>())),

                TR_ReportBuildTimeBegin = AsTimeParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_ReportBuildTimeBegin], ParamValueContainer.Default<TimeSpan>())),
                TR_ReportBuildTimeEnd = AsTimeParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_ReportBuildTimeEnd], ParamValueContainer.Default<TimeSpan>())),

                TR_GP3_InputFileNameFormat = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_GP3_InputFileNameFormat])),
                TR_GP3_InputFilesLocation = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_GP3_InputFilesLocation])),
                TR_ProcessMachineName = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_ProcessMachineName])),
                TR_ReportFileNameFormat = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_ReportFileNameFormat])),
                TR_ReportFilesLocation = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.TR_ReportFilesLocation])),
                Number_invoice_warning = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.Number_invoice_warning])),

                ClientPath = AsStringParam(helper.Get(ConfigParamsMap[ConfigurationParamName.ClientPath])),
                ServiceSelectionThreadCountMax = AsIntParam(helper.Get(ConfigParamsMap[ConfigurationParamName.ServiceSelectionThreadCountMax], ParamValueContainer.Default<int>())),

                ReclaimJobThreadCountMax = AsIntParam(helper.Get(ConfigParamsMap[ConfigurationParamName.ReclaimJobThreadCountMax], ParamValueContainer.Default<int>())),
                ReclaimLogInfoMessageUse = AsBoolParam(helper.Get(ConfigParamsMap[ConfigurationParamName.ReclaimLogInfoMessageUse], ParamValueContainer.Default<bool>())),
                ReclaimManualCheckDocUse = AsBoolParam(helper.Get(ConfigParamsMap[ConfigurationParamName.ReclaimManualCheckDocUse], ParamValueContainer.Default<bool>())),
            };

            return ret;
        }

        public void Save(Configuration config)
        {
            SaveParam(ConfigurationParamName.TR_TimerPeriodSec, config.TR_TimerPeriodSec.Value.ToString());

            SaveParam(ConfigurationParamName.TR_ReportBuildTimeBegin, config.TR_ReportBuildTimeBegin.Value.ToString());
            SaveParam(ConfigurationParamName.TR_ReportBuildTimeEnd, config.TR_ReportBuildTimeEnd.Value.ToString());

            SaveParam(ConfigurationParamName.TR_GP3_InputFileNameFormat, config.TR_GP3_InputFileNameFormat.Value);
            SaveParam(ConfigurationParamName.TR_GP3_InputFilesLocation, config.TR_GP3_InputFilesLocation.Value);
            SaveParam(ConfigurationParamName.TR_ProcessMachineName, config.TR_ProcessMachineName.Value);
            SaveParam(ConfigurationParamName.TR_ReportFileNameFormat, config.TR_ReportFileNameFormat.Value);
            SaveParam(ConfigurationParamName.TR_ReportFilesLocation, config.TR_ReportFilesLocation.Value);
        }

        public void SaveParam(ConfigurationParamName par, string value)
        {
            bool logicParam;
            if (bool.TryParse(value, out logicParam))
                value = FromBool(logicParam);
            
            
            Execute(
                "begin update configuration set value = :pVal where parameter = :pParam; end;",
                CommandType.Text,
                new OracleParameter[] 
                {
                    FormatCommandHelper.VarcharIn("pParam", ConfigParamsMap[par]),
                    FormatCommandHelper.VarcharIn("pVal", value)
                });
        }

        public OperationResult<List<FilterCriteria>> GetAutoselectionFilterCriteria()
        {
            var result = ExecuteList<FilterCriteria>(
                    "select * from DICT_AUTOSELECTION_FILTER",
                    CommandType.Text,
                    new OracleParameter[0],
                    (reader) =>
                        new FilterCriteria()
                        {
                            Name = reader.ReadString("FILTER_TYPE"),
                            ValueTypeCode = (FilterCriteria.ValueTypes)reader.ReadInt("VALUE_TYPE_CODE"),
                            LookupTableName = reader.ReadString("LOOKUP_TABLE_NAME"),
                            InternalName = reader.ReadString("INTERNAL_NAME"),
                            IsRequired = reader.ReadBoolean("IS_REQUIRED")
                        }
                    );

            return new OperationResult<List<FilterCriteria>>()
            {
                Status = ResultStatus.Success,
                Result = result.ToList()
            };
        }

        public OperationResult<List<CFGRegion>> GetRegionParam(CFGParam ParamType)
        {

            var result = ExecuteList<CFGRegion>(
                    @"select * from V$CFG_REGION where (param_id = :pType) or (param_id is null)",
                    CommandType.Text,
                    new OracleParameter[1]
                    {
                        FormatCommandHelper.NumericIn(":pType", (int)ParamType),
                    },
                    (reader) => new CFGRegion()
                    {
                        Id = reader.ReadNullableInt64("cfg_id"),
                        Region = new Region
                        {
                            Id = reader.ReadInt64("reg_id"),
                            Code = reader.ReadString("region_code"),
                            Name = reader.ReadString("region_name"),
                            IsNew = reader.ReadBoolean("region_is_new"),
                            Context = (RegionContext)reader.ReadInt("region_context_id")
                        },
                        Param = CFGParam.DiscrepancyPVP,
                        ParamValue = reader.ReadString("param_value")
                    });

            LoadInspections(result.Select(r => r.Region).ToList());


            foreach (var rRegion in result)
            {
                if (rRegion.Region.Code == "00")
                    rRegion.RegionName = "Вся страна";
                else
                    rRegion.RegionName = string.Format("{0} – {1}", rRegion.Region.Code, rRegion.Region.Name);
                
                if (rRegion.Region.Inspections.Count == 0)
                {
                    var predicate = result.Any(rr => rr.Region.Id != rRegion.Region.Id && rr.Region.Code == rRegion.Region.Code);
                    rRegion.RegionInspections = predicate ? "<Все остальные инспекции региона>" : "<Все>";
                }
                else
                {
                    rRegion.RegionInspections = string.Join(", ", rRegion.Region.Inspections.OrderBy(i => i.Code).Select(i => i.Code));
                }
            }

            return new OperationResult<List<CFGRegion>>()
            {
                Status = ResultStatus.Success,
                Result = result.ToList()
            };
        }

        private void LoadInspections(List<Region> regions)
        {
            regions.ForEach(rr => rr.Inspections = new List<Inspection>());

            var commandText = FormatCommandHelper.MethodologistPackageProcedure(UserToRegionAdapter.SelectAllInspectionsSQL);
            var commandType = CommandType.StoredProcedure;
            var parameters = new[] { FormatCommandHelper.Cursor("pCursor") };
            Action<List<Region>, IDataReader> rowAction = (list, reader) =>
            {
                var region = list.FirstOrDefault(rr => rr.Id == reader.ReadInt64("Id") && rr.Context == (RegionContext)reader.ReadInt("context_id"));
                if (region != null)
                {
                    region.Inspections.Add(reader.ReadInspection());
                }
            };

            using (var connection = Connection())
            using (var command = new OracleCommand(commandText, connection))
            {
                command.CommandType = commandType;
                command.BindByName = true;
                command.Parameters.AddRange(parameters ?? new OracleParameter[0]);

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        rowAction(regions, reader);
                    }
                }
                connection.Close();
            }
        }

        public OperationResult<CFGRegion> SetRegionParamValue(CFGRegion param)
        {
            if (string.IsNullOrEmpty(param.ParamValue))
            {
            if (param.Id.HasValue)
            {
                    Execute(new CommandContext()
                            {
                                Text = "begin delete CFG_REGION S where S.ID = :pId; end;",
                                Type = CommandType.Text,
                                Parameters = new List<OracleParameter>() 
                                {
                                    FormatCommandHelper.NumericIn("pId", param.Id)
                                } 
                            });

                    param.Id = null;
                }
            }
            else
            {
                if (param.Id.HasValue)
                {
                Execute("begin update CFG_REGION S set S.VALUE = :pValue where S.ID = :pId; end;",
                        CommandType.Text,
                        new OracleParameter[] 
                        {
                            FormatCommandHelper.NumericIn("pId", param.Id),
                            FormatCommandHelper.VarcharIn("pValue", param.ParamValue)
                        });
            }
            else
            {
                new SounDialogAdapter(_service).SaveRegion(param.Region);
                Execute(@"begin
                            insert into CFG_REGION S (S.ID, S.REGION_CODE, S.CFG_PARAM_ID, S.VALUE) 
                            values (:pId, :pReg, :pType, :pValue); 
                        end;",
                    CommandType.Text,
                    new OracleParameter[] 
                {
                    FormatCommandHelper.NumericIn("pId", param.Region.Id),
                    FormatCommandHelper.VarcharIn("pReg", param.Region.Code),
                    FormatCommandHelper.NumericIn("pType", (int)param.Param),
                    FormatCommandHelper.VarcharIn("pValue", param.ParamValue)
                });

                param.Id = param.Region.Id;
            }
            }

            ResultStatus rez = ResultStatus.Success;

            return new OperationResult<CFGRegion>() { Status = rez, Result = param };
        }


        // DN Вычитываем разом актуальное и дефолтовое значения
        private BoolParam AsBoolParam(ParamValueContainer param)
        {
            return new BoolParam { Value = ToBool(param.Value), DefaultValue = ToBool(param.DefaultValue) };
        }

        private IntParam AsIntParam(ParamValueContainer param)
        {
            return new IntParam { Value = int.Parse(param.Value), DefaultValue = int.Parse(param.DefaultValue) };
        }

        private TimeParam AsTimeParam(ParamValueContainer param)
        {
            return new TimeParam { Value = TimeSpan.Parse(param.Value), DefaultValue = TimeSpan.Parse(param.DefaultValue) };
        }

        private StringParam AsStringParam(ParamValueContainer param)
        {
            return new StringParam { Value = param.Value, DefaultValue = param.DefaultValue };
        }

        private bool ToBool(string data)
        {
            return data.Trim() == CharTrue;
        }

        private string FromBool(bool data)
        {
            return data ? CharTrue : CharFalse;
        }
    }
}
