﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class DictionaryTaxPeriodAdapter : BaseOracleTableAdapter, IDictionaryTaxPeriodAdapter
    {
        private const string SELECT_SQL = @"select * from DICT_TAX_PERIOD";

        public DictionaryTaxPeriodAdapter(IServiceProvider service) : base(service) { }

        public List<DictTaxPeriod> GetList()
        {
            Func<OracleDataReader, DictTaxPeriod> getData = reader => new DictTaxPeriod
            {
                Code = reader.ReadString("CODE"),
                Description = reader.ReadString("DESCRIPTION"),
                Quarter = reader.ReadInt("QUARTER"),
                IsMainInQuarter = reader.ReadBoolean("IS_MAIN_IN_QUARTER"),
                Month = reader.ReadInt("MONTH")
            };

            return ExecuteList(SELECT_SQL, CommandType.Text, new OracleParameter[0], getData);
        }
    }
}
