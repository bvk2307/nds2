﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Implementation
{

    /// <summary>
    /// Этот класс реализует процессинг запроса всех расхождений в Hive
    /// </summary>
    class HiveRequestAdapter : IHiveRequestAdapter
    {
        # region Константы

        private const string RequestJsonParam = "pJson";
        private const string HashCodeParam = "pCRC32Hash";
        private const string RequestTypeParam = "pRequestType";
        private const string StatusParam = "pStatus";
        private const string RequestIdParam = "pRequestId";

        private const string GetRequestIdFunction = "PAC$HIVE_REQUEST.P$GET_REQUEST_ID";
        private const string GetRequestStatusFunction = "PAC$HIVE_REQUEST.P$GET_REQUEST_STATUS";

        # endregion

        protected readonly HiveRequestBase _hiveRequest;
        
        private IServiceProvider _serviceProvider;

        public HiveRequestAdapter(IServiceProvider service, HiveRequestBase hiveRequest)
        {
            _serviceProvider = service;
            _hiveRequest = hiveRequest;
        }

        private class GetRequestIdCommandBuilder : ExecuteProcedureCommandBuilder
        {
            private readonly string _hiveRequestJson;
            private readonly uint _hiveRequestCRC32;
            private readonly int _requestTypeId;

            public GetRequestIdCommandBuilder(HiveRequestBase hiveRequest, int requestTypeId)
                : base(GetRequestIdFunction)
            {
                _hiveRequestJson = hiveRequest.GetJsonString();
                _hiveRequestCRC32 = CRC32Helper.GalcCRC32(_hiveRequestJson);
                _requestTypeId = requestTypeId;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.ClobIn(RequestJsonParam, _hiveRequestJson));
                parameters.Add(FormatCommandHelper.NumericIn(HashCodeParam, _hiveRequestCRC32));
                parameters.Add(FormatCommandHelper.NumericIn(RequestTypeParam, _requestTypeId));
                parameters.Add(FormatCommandHelper.NumericOut(StatusParam));
                parameters.Add(FormatCommandHelper.NumericOut(RequestIdParam));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class GetRequestStatusCommandBuilder : ExecuteProcedureCommandBuilder
        {
            private readonly long _requestId;

            public GetRequestStatusCommandBuilder(long requestId)
                : base(GetRequestStatusFunction)
            {
                _requestId = requestId;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericIn(RequestIdParam, _requestId));
                parameters.Add(FormatCommandHelper.NumericOut(StatusParam));
                return base.BuildSelectStatement(parameters);
            }
        }
        
        /// <summary>
        /// Получает идентификатор последнего запроса к Hive по ключевым полям. В случае, если запрос не найден, создает его и возвращает идентификатор
        /// </summary>
        /// <param name="hiveRequest">Данные для запроса информации в Hive</param>
        public long GetRequestId(out int status)
        {
            var result = new ResultCommandExecuter(_serviceProvider)
                .TryExecute(
                    new GetRequestIdCommandBuilder(_hiveRequest, 
                                                    _hiveRequest.GetRequestType()));
                        
            status = ((OracleDecimal)result[StatusParam]).ToInt32();
            return ((OracleDecimal)result[RequestIdParam]).ToInt64(); 
        }

        /// <summary>
        /// Получает текущий статус запроса к Hive по идентификатору.
        /// </summary>
        /// <param name="requestId">Идентификатор запроса к Hive</param>
        public int GetRequestStatus(long requestId)
        {
            var result = new ResultCommandExecuter(_serviceProvider)
                .TryExecute(
                    new GetRequestStatusCommandBuilder(requestId));

            return ((OracleDecimal)result[StatusParam]).ToInt32();
        }

    }
}
