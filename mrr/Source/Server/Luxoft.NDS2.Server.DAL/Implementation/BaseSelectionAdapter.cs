﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class BaseSelectionAdapter : BaseOracleTableAdapter
    {
        public BaseSelectionAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public override CommandExecuteResult Transaction(CommandContext[] commands)
        {
            if (!commands.Any())
            {
                return new CommandExecuteResult() { AffectedRows = -1 };
            }

            var tempList = commands.ToList();

            return base.Transaction(tempList.ToArray());
        }
    }
}
