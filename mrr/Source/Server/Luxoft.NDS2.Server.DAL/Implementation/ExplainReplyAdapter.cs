﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ExplainReplyAdapter : BaseOracleTableAdapter, IExplainReplyAdapter
    {
        #region Константы

        private const string GetExplainReplyInfoSql = @"select * from V$EXPLAIN vw where vw.explain_id = :pExplainId";

        private const string GetExplainReplyListSql = @"select * from V$EXPLAIN vw where vw.doc_id = :pDocId {0} {1}";

        private const string GetExplainsOfDocumentSql = @"select * from V$EXPLAIN vw where vw.doc_id = :pDocId";

        private const string GetExplainReplyHistoryStatusSql = 
            @"select submit_date, status_id from HIST_EXPLAIN_REPLY_STATUS hs where hs.explain_id = :pExplainId";

        private const string SearchSqlCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";

        private const string OneInvoiceSearchSql = @"
SELECT inv.*
FROM V$EXPLAIN_INVOICE_EDIT inv
WHERE inv.INVOICE_ROW_KEY = :pInvoiceId and inv.doc_id = :pDocId and inv.explain_id = :pExplainId";

        private const string OneInvoiceOnlyCorrectInThisSearchSql = @"
SELECT inv.*
FROM V$EXPLAIN_INVOICE_EDIT inv
WHERE inv.INVOICE_ROW_KEY = pInvoiceId and inv.doc_id = :pDocId 
and inv.explain_id = :pExplainId
and exists(SELECT id FROM STAGE_INVOICE_CORRECTED_STATE si 
           WHERE si.invoice_original_id = inv.INVOICE_ROW_KEY 
                 and si.explain_id = :pExplainId)";

        private const string SearchExplainInvoiceCorect = @"
select sic.explain_id, sic.field_name, sic.field_value, se.incoming_date,
sic.invoice_original_id
from STAGE_INVOICE_CORRECTED sic
join NDS2_SEOD.SEOD_EXPLAIN_REPLY se on se.explain_id = sic.explain_id
where invoice_original_id = :pInvoiceId";

        private const string SearchExplainInvoiceCorectState = @"
select st.explain_id, st.invoice_original_id, st.invoice_state, ex.incoming_date, ex.RECEIVE_BY_TKS
from STAGE_INVOICE_CORRECTED_STATE st 
join NDS2_SEOD.SEOD_EXPLAIN_REPLY ex on ex.explain_id = st.explain_id
where st.invoice_original_id = :pInvoiceId";

        private const string UpdateExplainStatusSql = @"
merge into seod_explain_reply er
using (select :pExplainId as explain_id, :pExplainStatusId as status_id from dual) s on (er.explain_id = s.explain_id)
when not matched then 
	insert (EXPLAIN_ID, STATUS_ID, STATUS_SET_DATE) values (s.explain_id, s.status_id, sysdate)
when matched then
update set 
	status_id = s.status_id, 
	status_set_date = sysdate";

        private const string UpdateExplainFileNameSql = @"
merge into seod_explain_reply er
using (select :pExplainId as explain_id, :pFileName as file_name from dual) s on (er.explain_id = s.explain_id)
when not matched then 
	insert (EXPLAIN_ID, FILENAMEOUTPUT) values (s.explain_id, s.file_name)
when matched then
update set 
	FILENAMEOUTPUT = s.file_name";

        private const string UpdateExplainZipSql = @"
merge into seod_explain_reply er
using (select :pExplainId as explain_id, :pZip as zip from dual) s on (er.explain_id = s.explain_id)
when not matched then 
	insert (EXPLAIN_ID, ZIP) values (s.explain_id, s.zip)
when matched then
update set 
	ZIP = s.zip";

        private const string SearchExplainAllInvoiceCorect = @"
select sic.explain_id, sic.field_name, sic.field_value, se.incoming_date, 
sic.invoice_original_id
from STAGE_INVOICE_CORRECTED sic
join NDS2_SEOD.SEOD_EXPLAIN_REPLY se on se.explain_id = sic.explain_id
where sic.explain_id = :pExplainId";

        private const string InvoiceOnlyCorrectInThisWithoutPageSql = @"
SELECT inv.*
FROM V$EXPLAIN_INVOICE_EDIT inv
WHERE inv.doc_id = :pDocId and explain_id = :pExplainId
and exists(SELECT id FROM STAGE_INVOICE_CORRECTED_STATE si 
           WHERE si.invoice_original_id = inv.INVOICE_ROW_KEY 
                 and si.explain_id = :pExplainId)";

        private const string ParamExplainId = "pExplainId";
        private const string ParamZip = "pZip";
        private const string pDocID = "pDocID";
        private const string pDocId = "pDocId";
        private const string CursorAlias = "pCursor";
        private const string pInvoiceId = "pInvoiceId";
        #endregion

        public ExplainReplyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        #region Получение пояснения, списка пояснений

        public ExplainReplyInfo GetExplainReplyInfo(long id)
        {
            var result = ExecuteList<ExplainReplyInfo>(
               GetExplainReplyInfoSql,
               CommandType.Text,
               new[] { FormatCommandHelper.NumericIn(ParamExplainId, id) },
               DataReaderExtension.ReadExplainReplyInfo);
            return result.Single();
        }

        public List<ExplainReplyInfo> GetExplainReplyList(long docId, QueryConditions conditions)
        {
            var query = conditions.ToSQL(GetExplainReplyListSql, "vw", false, true);
            var parameters = query.Parameters.ConvertAll(param => param.Convert());
            parameters.Add(FormatCommandHelper.NumericIn(pDocID, docId));
            var result = ExecuteList(
               query.Text,
               CommandType.Text,
               parameters.ToArray(),
               DataReaderExtension.ReadExplainReplyInfo);
            return result;
        }

        public List<ExplainReplyInfo> GetListExplainsOfDocument(long docId)
        {
            var result = ExecuteList<ExplainReplyInfo>(
               GetExplainsOfDocumentSql,
               CommandType.Text,
               new[] { FormatCommandHelper.NumericIn("pDocId", docId) },
               DataReaderExtension.ReadExplainReplyInfo);
            return result;
        }

        #endregion

        #region История статусов и сохранение комментария
      
        public List<ExplainReplyHistoryStatus> GetExplainReplyHistoryStatues(long explain_id)
        {
            var result = ExecuteList<ExplainReplyHistoryStatus>(
               GetExplainReplyHistoryStatusSql,
               CommandType.Text,
               new[] { FormatCommandHelper.NumericIn(ParamExplainId, explain_id) },
               DataReaderExtension.ReadExplainReplyHistoryStatus);
            return result;
        }

        #endregion

        #region Получение СФ вместе со связанными записями

        public ExplainInvoice GetOneExplainInvoice(ExplainReplyInfo explainInfo, ExplainRegim regim, string invoiceId)
        {
            //----- Invoice
            string sql = String.Empty;
            if (regim == ExplainRegim.Edit)
            {
                sql = OneInvoiceSearchSql;
            }
            else if (regim == ExplainRegim.View)
            {
                sql = OneInvoiceOnlyCorrectInThisSearchSql;
            }

            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, explainInfo.DOC_ID));
            parameters.Add(FormatCommandHelper.VarcharIn("pInvoiceId", invoiceId));
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainInfo.EXPLAIN_ID));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoices = ExecuteList<ExplainInvoice>(
                string.Format(SearchSqlCommandPattern, sql),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadExplainInvoice);

            return invoices.Single();
        }

        private void FillRelatedInvoices(ExplainReplyInfo explainInfo, List<ExplainInvoice> invoices, ExplainRegim regim)
        {
            List<ExplainInvoiceCorrect> correctsAll = null;
            List<ExplainInvoiceCorrect> correctsThis = null;
            List<ExplainInvoiceCorrect> correctsPrevious = null;
            List<ExplainInvoiceCorrect> correctsForThis = new List<ExplainInvoiceCorrect>();

            List<ExplainInvoiceCorrectState> correctStateAll = null;
            List<ExplainInvoiceCorrectState> correctStatePrevious = null;
            List<ExplainInvoiceCorrectState> correctStateTKS = null;

            ExplainInvoice invoiceCorrect = null;
            Type typeClass = (new ExplainInvoice()).GetType();
            PropertyInfo[] properties = typeClass.GetProperties();

            foreach (ExplainInvoice itemInv in invoices)
            {
                itemInv.RefreshProperties();
                invoiceCorrect = (ExplainInvoice)itemInv.Clone();
                invoiceCorrect.Type = ExplainInvoiceType.Correct;
                invoiceCorrect.SetState(ExplainInvoiceState.NotUsing);
                invoiceCorrect.RefreshProperties();

                correctStateAll = GetExplainInvoiceCorrectState(itemInv.ROW_KEY);
                if (regim == ExplainRegim.View)
                {
                    ExplainInvoiceCorrectState state = correctStateAll.Where(p => p.ExplainId == explainInfo.EXPLAIN_ID).SingleOrDefault();
                    if (state != null)
                    {
                        SetStateInvoiceInThisExplain(itemInv, state);
                    }
                }
                else if (regim == ExplainRegim.Edit)
                {
                    ExplainInvoiceCorrectState state = correctStateAll.Where(p => p.ExplainId == explainInfo.EXPLAIN_ID).SingleOrDefault();
                    if (state != null)
                    {
                        SetStateInvoiceInThisExplain(itemInv, state);
                    }
                    else
                    {
                        correctStatePrevious = correctStateAll.Where(p => p.ExplainId != explainInfo.EXPLAIN_ID && p.ExplainIncomingDate < explainInfo.INCOMING_DATE).ToList();
                        if (correctStatePrevious.Count > 0)
                        {
                            if (correctStatePrevious.Where(p => p.State == ExplainInvoiceStateInThis.Processed).Count() > 0)
                            {
                                itemInv.AddState(ExplainInvoiceState.ProcessedInPreviousExplain);
                            }
                            else if (correctStatePrevious.Where(p => p.State == ExplainInvoiceStateInThis.Changing).Count() > 0)
                            {
                                itemInv.AddState(ExplainInvoiceState.ChangingInPreviousExplain);
                            }
                        }
                        else
                        {
                            correctStateTKS = correctStateAll.Where(p => p.ExplainId != explainInfo.EXPLAIN_ID && p.RECEIVE_BY_TKS).ToList();
                            if (correctStateTKS.Count > 0)
                            {
                                if (correctStateTKS.Where(p => p.State == ExplainInvoiceStateInThis.Processed).Count() > 0)
                                {
                                    itemInv.AddState(ExplainInvoiceState.ProcessedInPreviousExplain);
                                }
                                else if (correctStateTKS.Where(p => p.State == ExplainInvoiceStateInThis.Changing).Count() > 0)
                                {
                                    itemInv.AddState(ExplainInvoiceState.ChangingInPreviousExplain);
                                }
                            }
                        }
                    }
                }

                correctsAll = GetExplainInvoiceCorrect(itemInv.ROW_KEY);
                correctsThis = correctsAll.Where(p => p.ExplainId == explainInfo.EXPLAIN_ID).ToList();
                correctsPrevious = correctsAll.Where(p => p.ExplainId != explainInfo.EXPLAIN_ID && p.ExplainIncomingDate < explainInfo.INCOMING_DATE).ToList();

                correctsForThis.Clear();
                if (regim == ExplainRegim.View)
                {
                    correctsForThis.AddRange(correctsPrevious.OrderBy(p => p.ExplainIncomingDate));
                    correctsForThis.AddRange(correctsThis);
                }
                else if (regim == ExplainRegim.Edit)
                {
                    correctsForThis.AddRange(correctsPrevious.OrderBy(p => p.ExplainIncomingDate));
                    if (itemInv.State == ExplainInvoiceState.ChangingInThisExplain ||
                        itemInv.State == ExplainInvoiceState.ProcessedInThisExplain)
                    {
                        correctsForThis.AddRange(correctsThis);
                    }
                }

                FillExplainInvoiceFromCorrect(properties, invoiceCorrect, correctsForThis);
                invoiceCorrect.Parent = itemInv;
                itemInv.RelatedInvoices = new List<ExplainInvoice>();

                if (itemInv.State == ExplainInvoiceState.ChangingInThisExplain ||
                    itemInv.State == ExplainInvoiceState.ChangingInPreviousExplain)
                {
                    itemInv.RelatedInvoices.Add(invoiceCorrect);
                }

                //--- Заполним Предыдущее состояние СФ (с учетом ранее изменнных записей)
                FillPreviousInvoiceChange(itemInv, invoiceCorrect, correctsAll, properties, explainInfo);
            }
        }

        private void FillPreviousInvoiceChange(ExplainInvoice itemInv, ExplainInvoice invoiceCorrect, List<ExplainInvoiceCorrect> correctsAll,
            PropertyInfo[] properties, ExplainReplyInfo explainInfo)
        {
            if (itemInv.State == ExplainInvoiceState.ChangingInPreviousExplain)
            {
                itemInv.PreviousInvoiceChange = (ExplainInvoice)invoiceCorrect.Clone();
            }
            else if (itemInv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                List<ExplainInvoiceCorrect> correctsOnlyPrevious = correctsAll.Where(p => p.ExplainId != explainInfo.EXPLAIN_ID && p.ExplainIncomingDate < explainInfo.INCOMING_DATE).OrderBy(p => p.ExplainIncomingDate).ToList();
                ExplainInvoice invoiceCorrectOnlyPrevious = (ExplainInvoice)itemInv.Clone();
                FillExplainInvoiceFromCorrect(properties, invoiceCorrectOnlyPrevious, correctsOnlyPrevious);
                itemInv.PreviousInvoiceChange = (ExplainInvoice)invoiceCorrectOnlyPrevious.Clone();
            }
            else
            {
                itemInv.PreviousInvoiceChange = (ExplainInvoice)itemInv.Clone();
            }
        }

        private void FillExplainInvoiceFromCorrect(PropertyInfo[] properties, ExplainInvoice invoiceCorrect, List<ExplainInvoiceCorrect> corrects)
        {
            foreach (ExplainInvoiceCorrect itemCorr in corrects)
            {
                CultureInfo ru = new CultureInfo("ru-RU");

                PropertyInfo itemProp = properties.Where(p => p.Name == itemCorr.FieldName).SingleOrDefault();
                if (itemProp != null)
                {
                    string typ = itemProp.PropertyType.FullName.ToString();
                    if (typ.Contains("System.Decimal") && itemCorr.FieldValue != "")                          
                    {
                        string s = itemCorr.FieldValue;
                        decimal temp = Decimal.Parse(s, System.Globalization.NumberStyles.AllowDecimalPoint);
                        itemProp.SetValue(invoiceCorrect, temp, null);
                    }
                    if (typ.Contains("System.DateTime") && itemCorr.FieldValue != "")                         
                    {
                        string s = itemCorr.FieldValue;
                        DateTime temp = DateTime.ParseExact(s, "dd.MM.yyyy", ru);
                        itemProp.SetValue(invoiceCorrect, temp, null);
                    }
                    if (!typ.Contains("System.Decimal") && !typ.Contains("System.DateTime"))  
                     itemProp.SetValue(invoiceCorrect, Convert.ChangeType(itemCorr.FieldValue, itemProp.PropertyType), null);
                }
            }
        }

        private void SetStateInvoiceInThisExplain(ExplainInvoice itemInv, ExplainInvoiceCorrectState state)
        {
            if (state.State == ExplainInvoiceStateInThis.Changing)
            {
                itemInv.AddState(ExplainInvoiceState.ChangingInThisExplain);
            }
            else if (state.State == ExplainInvoiceStateInThis.Processed)
            {
                itemInv.AddState(ExplainInvoiceState.ProcessedInThisExplain);
            }
        }

        private List<ExplainInvoiceCorrect> GetExplainInvoiceCorrect(string invoiceId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(pInvoiceId, invoiceId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoicesCorrect = ExecuteList<ExplainInvoiceCorrect>(
                string.Format(SearchSqlCommandPattern, SearchExplainInvoiceCorect),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadExplainInvoiceCorrect);

            return invoicesCorrect;
        }

        private List<ExplainInvoiceCorrectState> GetExplainInvoiceCorrectState(string invoiceId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(pInvoiceId, invoiceId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoicesCorrectState = ExecuteList<ExplainInvoiceCorrectState>(
                string.Format(SearchSqlCommandPattern, SearchExplainInvoiceCorectState),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadExplainInvoiceCorrectState);

            return invoicesCorrectState;
        }

        #endregion

        #region Функции пояснения 

        public void UpdateInvoiceCorrect(long explainId, ExplainInvoiceCorrect correct, InvoiceCorrectTypeOperation typeOperation)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.VarcharIn("pInvoiceId", correct.InvoiceId));
            parameters.Add(FormatCommandHelper.VarcharIn("pFieldName", correct.FieldName));
            parameters.Add(FormatCommandHelper.VarcharIn("pFieldValue", correct.FieldValue));
            parameters.Add(FormatCommandHelper.NumericIn("pTypeOperation", (int)typeOperation));

            Execute(string.Format("{0}.{1}", "NDS2$EXPLAIN_REPLY", "P$STAGE_CORRECT_UPDATE"),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void UpdateExplainStatus(long explainId, int explainStatusId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.NumericIn("pExplainStatusId", explainStatusId));

            Execute(UpdateExplainStatusSql,
                CommandType.Text,
                parameters.ToArray());
        }

        public void UpdateExplainFileName(long explainId, string fileName)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.VarcharIn("pFileName", fileName));

            Execute(UpdateExplainFileNameSql,
                CommandType.Text,
                parameters.ToArray());
        }

        public void UpdateExplainZip(long explainId, long zip)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.NumericIn("pZip", zip));

            Execute(UpdateExplainZipSql,
                CommandType.Text,
                parameters.ToArray());
        }

        public void UpdateInvoiceCorrectState(long explainId, string invoiceId, ExplainInvoiceStateInThis state)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.VarcharIn("pInvoiceId", invoiceId));
            parameters.Add(FormatCommandHelper.NumericIn("pInvoiceState", (int)state));

            Execute(string.Format("{0}.{1}", "NDS2$EXPLAIN_REPLY", "P$STAGE_CORRECT_UPDATE_STATE"),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void DeleteInvoiceCorrectState(long explainId, string invoiceId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.VarcharIn("pInvoiceId", invoiceId));

            Execute(string.Format("{0}.{1}", "NDS2$EXPLAIN_REPLY", "P$STAGE_CORRECT_DELETE"),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void DeleteAllInvoiceCorrectState(long explainId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));

            Execute(string.Format("{0}.{1}", "NDS2$EXPLAIN_REPLY", "P$STAGE_CORRECT_DELETE_ALL"),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public bool ExistsInvoiceChangeOfExplain(long explainId)
        {
            bool ret = false;

            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.NumericOut("IsExists"));

            var result = Execute(string.Format("{0}.{1}", "NDS2$EXPLAIN_REPLY", "P$EXISTS_INVOICE_CORRECT"),
                CommandType.StoredProcedure,
                parameters.ToArray());

            int isExists = DataReaderExtension.ReadInt(result.Output["IsExists"]);
            if (isExists == 1)
            {
                ret = true;
            }
            return ret;
        }

        public List<ExplainInvoiceCorrect> GetExplainAllInvoiceCorrect(long explainId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoicesCorrect = ExecuteList<ExplainInvoiceCorrect>(
                string.Format(SearchSqlCommandPattern, SearchExplainAllInvoiceCorect),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadExplainInvoiceCorrect);

            return invoicesCorrect;
        }

        public List<ExplainInvoice> GetInvoicesOnlyCorrectInThisExplain(ExplainReplyInfo explainInfo)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, explainInfo.DOC_ID));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));
            parameters.Add(FormatCommandHelper.NumericIn(ParamExplainId, explainInfo.EXPLAIN_ID));

            var invoices = ExecuteList<ExplainInvoice>(
                string.Format(SearchSqlCommandPattern, InvoiceOnlyCorrectInThisWithoutPageSql),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadExplainInvoice);

            FillRelatedInvoices(explainInfo, invoices, ExplainRegim.View);

            return invoices;
        }
        #endregion
    }
}
