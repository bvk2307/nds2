﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    internal class SelectionTableAdapter : BaseOracleTableAdapter, ISelectionTableAdapter
    {
        private const string Scope = "Selection";

        # region Конструктор

        public SelectionTableAdapter(IServiceProvider provider)
            : base(provider)
        {
        }

        # endregion

        # region Реализация ISelectionTableAdapter

        public Selection Load(long id)
        {
            throw new System.NotImplementedException();
        }

        public void Insert(Selection data)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Selection data)
        {
            throw new System.NotImplementedException();
        }

        public List<Selection> Search(DataQueryContext queryContext)
        {
            var qText = _service.GetQueryText(Scope, SelectionListTemplateName);

            var query = queryContext.ToSQL(qText, PatternProvider());
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor("pCursor"));

            var result = ExecuteList(
                string.Format(QueryPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                reader =>
                    {
                        var s = new Selection();
                        s.Id = reader.ReadInt64("Id");
                        s.Analytic = reader.ReadString("Analytic");
                        s.AnalyticSid = reader.ReadString("AnalyticSid");
                        s.Manager = reader.ReadString("Manager");
                        s.ManagerSid = reader.ReadString("ManagerSid");
                        s.CreationDate = reader.ReadDateTime("CreationDate");
                        s.ModificationDate = reader.ReadDateTime("ModificationDate");
                        s.Name = reader.ReadString("Name");
                        s.Status = (SelectionStatus?)reader.ReadNullableInt("Status");
                        s.StatusDate = reader.ReadDateTime("StatusDate");
                        s.Type = reader.ReadString("Type");
                        s.TypeCode = (SelectionType)reader.ReadInt("TypeCode");
                        s.DeclarationCount = reader.ReadInt("DeclarationCount");
                        s.DiscrepanciesCount = reader.ReadInt("DiscrepanciesCount");
                        s.Regions = reader.ReadString("Regions");
                        return s;
                    });

            //AppendStat(result);

            return result;
        }

//        public void AppendStat(List<Selection> selections)
//        {    
//            //TODO: ММ - пробрасывать через ассоциативный массив в хранимку.
//            var withStm = string.Format("select {0} from dual", string.Join(" as id from dual union all select ", selections.Select(s => s.Id)));
//
//            var qText = string.Format(_service.GetQueryText(_scope, SelectionListStatsTemplateName), withStm);
//
//             ExecuteList(
//                qText,
//                CommandType.Text,
//                new OracleParameter[]{},
//                reader =>
//                    {
//                        var sel = selections.First(s => s.Id == reader.ReadInt64("Id"));
//
//                        //sel.RegionName = reader.ReadString("RegionName");
//                        //sel.RegionCode = reader.ReadString("RegionCode");
//                        //sel.RegionNames = reader.ReadString("RegionNames");
//                        sel.DeclarationCount = reader.ReadInt("DeclarationCount");
//                        sel.DiscrepanciesCount = reader.ReadInt("DiscrepanciesCount");
//                        
//                        sel.TotalAmount = reader.ReadDecimal("TotalAmount");
//                        sel.SelectedDiscrepanciesPVPAmount = reader.ReadDecimal("SelectedDiscrepanciesPVPAmount");
//
//                        return sel;
//                    });
//        }

        public int Count(FilterExpressionBase filter)
        {
            var qText = _service.GetQueryText(Scope, SelectionListCountTemplateName);
            var query = filter.ToSQL(qText, PatternProvider());
            query.Parameters.Add(new QueryParameter(false) { Name = "pResult" });

            return DataReaderExtension.ReadInt(Execute(query).Output["pResult"]);
        }

        # endregion

        # region Вспомогательные методы

        private const string QueryPattern =
            "begin open :pCursor for {0}; end;";

        private const string SelectionListTemplateName = "SelectionListTemplate";

        //private const string SelectionListStatsTemplateName = "SelectionListStatsTemplate";

        private const string SelectionListCountTemplateName = "SelectionListCountTemplate";

        private const string RegionPattern =
            "exists(select 1 from V$SELECTION_DECLARATION vd where vd.selectionid=vw.ID  and exists(select 1 from Metodolog_Region rr join METODOLOG_CHECK_EMPLOYEE mce on mce.resp_id = rr.id where {0} AND rr.CODE = vd.RegionCode))";

        private PatternProvider PatternProvider()
        {
            return new PatternProvider("vw")
                .WithPattern("region_user", RegionPattern)
                .WithExpression("region_user", "mce.Sid");
        }

        # endregion
    }
}
