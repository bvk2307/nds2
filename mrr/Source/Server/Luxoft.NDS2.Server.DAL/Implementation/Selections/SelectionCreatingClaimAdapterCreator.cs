﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    public static class SelectionCreatingClaimAdapterCreator
    {
        public static ISelectionCreatingClaimAdapter SelectionCreatingClaimAdapter(IServiceProvider service)
        {
            return new SelectionCreatingClaimAdapter(service);
        }
    }
}
