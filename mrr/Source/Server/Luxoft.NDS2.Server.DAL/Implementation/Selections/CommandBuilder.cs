﻿using System;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    internal class CommandBuilder
    {
        private string _withComment = null;
        private string _commentParamName = null;
        private bool _withSysdate = false;
        private string _sysdateParamName = null;
        private int? _withStatus = null;
        private string _statusParamName = null;

        private readonly QueryConditions _criteria;

        public CommandBuilder(QueryConditions criteria)
        {
            _criteria = criteria;
        }

        public CommandBuilder WithComment(string value, string paramName = "pComment")
        {
            _withComment = value;
            _commentParamName = paramName;

            return this;
        }

        public CommandBuilder WithSysdate(string paramName = "pSysDate")
        {
            _withSysdate = true;
            _sysdateParamName = paramName;

            return this;
        }

        public CommandBuilder WithStatus(int status, string paramStatus = "pStatus")
        {
            _withStatus = status;
            _statusParamName = paramStatus;

            return this;
        }

        public CommandContext Build(
            string pattern,
            string selectionViewAlias = "vw")
        {
            var query = 
                _criteria.ToSQL(                    
                    FormatCommandHelper.OraCommand(pattern), 
                    selectionViewAlias, 
                    false, 
                    true);

            var retVal =
                new CommandContext
                {
                    Type = CommandType.Text,
                    Text = query.Text,
                    Parameters = query.Parameters.Select(p => p.Convert()).ToList()
                };

            if (_withComment != null 
                && pattern.ToLower().Contains(string.Format(":{0}", _commentParamName).ToLower()))
            {
                retVal.Parameters.Add(new OracleParameter(_commentParamName, _withComment)); 
            }

            if (_withSysdate
                && pattern.ToLower().Contains(string.Format(":{0}", _sysdateParamName).ToLower()))
            {
                retVal.Parameters.Add(new OracleParameter(_sysdateParamName, DateTime.UtcNow));
            }

            if (_withStatus.HasValue
                && pattern.ToLower().Contains(string.Format(":{0}", _statusParamName).ToLower()))
            {
                retVal.Parameters.Add(new OracleParameter(_statusParamName, _withStatus.Value));
            }

            return retVal;
        }
    }
}
