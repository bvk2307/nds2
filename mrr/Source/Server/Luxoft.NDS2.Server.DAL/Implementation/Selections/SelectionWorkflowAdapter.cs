﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    internal class SelectionWorkflowAdapter : BaseOracleTableAdapter, ISelectionWorkflowAdapter
    {
        public SelectionWorkflowAdapter(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }
        
        public bool HasTransition(int fromTransition, int toTransition)
        {
            var result =
                Execute(
                    string.Format(
                        "begin select count(1) into :pResult from SELECTION_TRANSITION where STATE_FROM = {0} and STATE_TO = {1}; end;",
                        fromTransition,
                        toTransition),
                        CommandType.Text,
                        new [] {FormatCommandHelper.NumericOut("pResult")});

            return DataReaderExtension.ReadInt(result.Output["pResult"]) > 0;
        }

    }
}
