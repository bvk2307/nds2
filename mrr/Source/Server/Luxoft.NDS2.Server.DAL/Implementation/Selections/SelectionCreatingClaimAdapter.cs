﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;


namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    /// <summary>
    /// Этот класс реализует процессинг формирования АТ по списку выборок
    /// </summary>
    class SelectionCreatingClaimAdapter : ISelectionCreatingClaimAdapter
    {

        #region Constants

        public static string ExistsApprovedSelectionsProcedure = "PAC$SELECTION.P$EXISTS_APPROVED_SELECTIONS";
        public static string CountApprovedSelectionsProcedure = "PAC$SELECTION.P$COUNT_APPROVED_SELECTIONS";
        public static string SetClaimCreatingStatusProcedure = "PAC$SELECTION.P$SET_CLAIM_CREATING_STATUS";

        public static string ParamExistAppSelections = "pExists";
        public static string ParamRequestApproveCount = "pRequestApproveCount";
        public static string ParamApprovedCount = "pApprovedCount";
        #endregion

        private IServiceProvider _serviceProvider;

        public SelectionCreatingClaimAdapter(IServiceProvider service)
        {
            _serviceProvider = service;
        }

        #region Command Builders

        private class ExistApprovedSelectionsCommandBuilder : ExecuteProcedureCommandBuilder
        {
            public ExistApprovedSelectionsCommandBuilder() : base(ExistsApprovedSelectionsProcedure) { }
            
            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericOut(ParamExistAppSelections));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountApprovedSelectionsCommandBuilder : ExecuteProcedureCommandBuilder
        {
            public CountApprovedSelectionsCommandBuilder() : base(CountApprovedSelectionsProcedure) { }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericOut(ParamRequestApproveCount));
                parameters.Add(FormatCommandHelper.NumericOut(ParamApprovedCount));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class SetClaimCreatingStatusCommandBuilder : ExecuteProcedureCommandBuilder
        {
            public SetClaimCreatingStatusCommandBuilder() : base(SetClaimCreatingStatusProcedure) { }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                return base.BuildSelectStatement(parameters);
            }
        }

        #endregion

        #region Creating Claim Functionality

        /// <summary>
        /// Проверяет наличие выборок в статусе "Согласовано" в списке выборок для активирования кнопки "Сформриовать АТ"
        /// </summary>
        public bool ExistApprovedSelections()
        {
            var result = new ResultCommandExecuter(_serviceProvider).TryExecute(new ExistApprovedSelectionsCommandBuilder());

            return ((OracleDecimal)result[ParamExistAppSelections]).ToInt32() == 1;
        }

        /// <summary>
        /// Подсчитывает количество выборок в статусах "Согласовано" и "На согласовании" в списке выборок для заполнения всплывающего сообщения
        /// </summary>
        public SelectionByStatusesCount CountApprovedSelections()
        {
            var result = new ResultCommandExecuter(_serviceProvider).TryExecute(new CountApprovedSelectionsCommandBuilder());

            SelectionByStatusesCount selectStatusCount = new SelectionByStatusesCount(((OracleDecimal)result[ParamRequestApproveCount]).ToInt32(), ((OracleDecimal)result[ParamApprovedCount]).ToInt32());
            return selectStatusCount;
        }
        
        /// <summary>
        /// Переводит выборки из статуса "Согласовано" в статус "Формирование АТ" 
        /// </summary>
        public void SetClaimCreatingStatus()
        {
            var result = new ResultCommandExecuter(_serviceProvider).TryExecute(new SetClaimCreatingStatusCommandBuilder());
        }
        #endregion
    }
}
