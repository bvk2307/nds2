﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    /// <summary>
    /// Этот класс реализует адаптер истории изменений выборки
    /// </summary>
    internal class SelectionChangeHistoryAdapter : BaseOracleTableAdapter, ISelectionChangeHistoryAdapter
    {
        private const string InsertProcedure = "INSERT_ACTION_HISTORY";
        private const string SelectionId = "pSelectionId";
        private const string Comment = "pComment";
        private const string User = "pUser";
        private const string Type = "pAction";

        public SelectionChangeHistoryAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public void Insert(ActionHistory action)
        {
            Execute(
                FormatCommandHelper.SelectionPackageProcedure(InsertProcedure),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(SelectionId, action.SelectionId),
                    FormatCommandHelper.VarcharIn(Comment, action.ActionComment),
                    FormatCommandHelper.VarcharIn(User, action.User),
                    FormatCommandHelper.NumericIn(Type, (int)action.Type)
                });
        }
    }
}
