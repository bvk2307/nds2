﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    internal class SelectionAdapter : BaseSelectionAdapter, ISelectionAdapter
    {
        private const string CheckRequestProcessingStatus = "GET_SELECTION_STATUS";
        
        private const string ParamId = "pSelectionId";
        private const string ParamSelectionId = "pSelectionId";
        private const string CountResult = "pResult";
        private const string ParamSelectionName = "pSelectionName";
        private const string ParamAnalytic = "pAnalytic";
        private const string ParamCreationDate = "pCreationDate";
        private const string ParamFilter = "pFilter";
        private const string ParamCursor = "pCursor";

        private const string GetCountSelectionByName = "begin select count(ID) into :pResult from SELECTION t WHERE t.Id <> :pSelectionId and t.NAME = :pSelectionName and LOWER(t.Analytic) = LOWER(:pAnalytic) and trunc(t.Creation_Date) = :pCreationDate; end;";
        private const string UpdateSelectionFilter = "begin UPDATE SELECTION_FILTER t SET Filter = :pFilter WHERE t.SELECTION_ID = :pSelectionId; end;";

        private const string GetCountDiscrepncyNotSendSEODSql = @"
begin select count(*) into :pResult from
(
  select sd.selection_id as id
  from V$SELECTION_DISCREPANCY sd
  where sd.selection_id = :pSelectionId and sd.ischecked = 1 and sd.STAGE_STATUS_CODE = 6
) m; end;";

        private const string GetSelectionFilterContentSql = "select filter from selection_filter where selection_id = :pSelectionId";

        private readonly long _id;

        public SelectionAdapter(IServiceProvider services, long id)
            : base(services)
        {
            _id = id;
        }

        public SelectionStatus SyncWithRequestProcessing()
        {
            return ExecuteList(
                FormatCommandHelper.SelectionPackageProcedure(CheckRequestProcessingStatus),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn("selectionId", _id),
                    FormatCommandHelper.Cursor("statusCursor")
                }, reader => (SelectionStatus)reader.ReadInt("STATUS"))
                .First();
        }

        public Selection GetSelection()
        {
            List<Selection> selections = ExecuteList(
                FormatCommandHelper.SelectionPackageProcedure("P$LOAD_SELECTION"),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn("pId", _id),
                    FormatCommandHelper.Cursor(ParamCursor)
                },
                BuildSelection);
            return selections.FirstOrDefault();
        }

        private Selection BuildSelection(OracleDataReader reader)
        {
            var ret = new Selection
            {
                Id = reader.ReadInt64("Id"),
                Analytic = reader.ReadString("Analytic"),
                Analytic_Sid = reader.ReadString("Analytic_sid"),
                Manager = reader.ReadString("Manager"),
                ManagerSid = reader.ReadString("Manager_sid"),
                CreationDate = reader.ReadDateTime("CreationDate"),
                ModificationDate = reader.ReadDateTime("ModificationDate"),
                Name = reader.ReadString("Name"),
                Status =
                    new DictionaryEntry
                    {
                        EntryId = reader.ReadInt("Status"),
                        EntryValue = reader.ReadString("StatusName")
                    },
                RegionName = reader.ReadString("RegionName"),
                RegionNames = reader.ReadString("RegionNames"),
                RegionCode = reader.ReadString("RegionCode"),
                DeclarationCount = reader.ReadInt("DeclarationCount"),
                DiscrepanciesCount = reader.ReadInt("DiscrepanciesCount"),
                StatusDate = reader.ReadDateTime("StatusDate"),
                Type = reader.ReadString("Type"),
                TypeCode = reader.ReadInt("TypeCode"),
                TotalAmount = reader.ReadDecimal("TotalAmount"),
                SelectedDiscrepanciesPVPAmount = reader.ReadDecimal("SelectedDiscrepanciesPVPAmount")
            };
            //ret.ProcessStage = (SelectionDiscrepancyStage)Enum.Parse(typeof(SelectionDiscrepancyStage), reader.ReadString("ProcessStage"));
            
            return ret;
        }

        public int GetSelectionCount(string name, string analytic, DateTime date)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(ParamSelectionId, _id),
                FormatCommandHelper.VarcharIn(ParamSelectionName, name),
                FormatCommandHelper.VarcharIn(ParamAnalytic, analytic),
                FormatCommandHelper.DateIn(ParamCreationDate, date.Date),
                FormatCommandHelper.NumericOut(CountResult)
            };

            var result = Execute(GetCountSelectionByName,
                CommandType.Text,
                parameters.ToArray());

            return DataReaderExtension.ReadInt(result.Output[CountResult]);
        }

        public void SaveFilter(Filter filter)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(ParamSelectionId, _id),
                FormatCommandHelper.ClobIn(ParamFilter, filter == null ? null : FilterHelper.Serialize(filter))
            };

            Execute(UpdateSelectionFilter,
                CommandType.Text,
                parameters.ToArray());
        }

        public long GetCountDiscrepncyNotSendSEOD()
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn("pSelectionId", _id),
                FormatCommandHelper.NumericOut(CountResult)
            };

            var result = Execute(GetCountDiscrepncyNotSendSEODSql,
                CommandType.Text,
                parameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[CountResult]);
        }

        public void SetSelectionState(int stateId, string userName)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn("pId", _id),
                FormatCommandHelper.NumericIn("statusId", stateId),
                FormatCommandHelper.VarcharIn("userName", userName)
            };

            Execute("Pac$Selections.SELECTION_SET_STATE",
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public Filter GetSelectionFilterContent()
        {
            var filters = ExecuteList(
                string.Format(FormatCommandHelper.OraCommandPattern, string.Format(FormatCommandHelper.SelectIntoCursor, ParamCursor, GetSelectionFilterContentSql) + ";"),
                CommandType.Text,
                new[]
                {
                    FormatCommandHelper.NumericIn(ParamId, _id),
                    FormatCommandHelper.Cursor(ParamCursor)
                },
                reader => FilterHelper.Deserialize(reader.ReadString("filter")).Filter);
            return filters.SingleOrDefault();
        }
    }
}