﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Server.DAL.Implementation.Selections
{
    internal class StateTransitionAdapter : BaseOracleTableAdapter, IStateTransitionAdapter
    {
        private readonly ISelectionPackageAdapter _packageAdapter;

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса SelectionStateAdapter
        /// </summary>
        /// <param name="service"></param>
        public StateTransitionAdapter(IServiceProvider service)
            : base(service)
        {
            _packageAdapter = TableAdapterCreator.SelectionPackageAdapter(service);
        }

        # endregion

        public List<SelectionTransition> GetStateTransitions()
        {
            return _packageAdapter.GetStateTransitions();
        }
    }
}
