﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class StateTransitionAdapter : BaseOracleTableAdapter, IStateTransitionAdapter
    {
        # region Константы

        private const string SearchProcedure = "GET_SELECTION_TRANSITIONS";
        private const string CursorParam = "PFAVCURSOR";
        private const string StateFromId = "STATE_FROM";
        private const string StateToId = "STATE_TO";
        private const string Operation = "OPERATION";

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса SelectionStateAdapter
        /// </summary>
        /// <param name="service"></param>
        public StateTransitionAdapter(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        public List<SelectionTransition> GetStateTransitions()
        {
            List<SelectionTransition> stateTransitions = ExecuteList<SelectionTransition>(
                FormatCommandHelper.SelectionPackageProcedure(SearchProcedure),
                CommandType.StoredProcedure,
                new OracleParameter[] 
                {
                    FormatCommandHelper.Cursor(CursorParam)
                },
                Build);

            return stateTransitions;
        }

        # region Построение объекта StateTransition

        /// <summary>
        /// Строит объект по данным из БД
        /// </summary>
        /// <param name="reader">Ссылка на OracleDataReader, содержащий данные</param>
        /// <returns>Фильтр выборки</returns>
        private SelectionTransition Build(OracleDataReader reader)
        {
            return new SelectionTransition()
            {
                STATE_FROM = (SelectionStatus)int.Parse(reader[StateFromId].ToString()),
                STATE_TO = (SelectionStatus)int.Parse(reader[StateToId].ToString()),
                OPERATION = reader[Operation].ToString(),
            };
        }

        # endregion
    }
}
