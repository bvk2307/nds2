﻿//#define USETASK_ONRELATIONSLOADING

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping.Pyramid;
using Luxoft.NDS2.Server.DAL.Pyramid.CommandBuilders;
using Luxoft.NDS2.Server.DAL.Pyramid.DataMapping;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class PyramidDataAdapter : IPyramidDataAdapter
    {
        public const string ProcedureTaxpayerRelation           = "PAC$HRZ_REPORT.TAXPAYER_RELATION";
        public const string ProcedureTaxpayerRelationFakeNode   = "PAC$HRZ_REPORT.TAXPAYER_RELATION_FAKE_ROOT";
        public const string ProcedureTaxpayerRelationReport     = "PAC$HRZ_REPORT.TAXPAYER_RELATION_REPORT";
        //private const string ProcedureTaxpayerChildRelation       = "PAC$HRZ_REPORT_EXCEL.TAXPAYER_RELATION";

        public const string ProcedureGetTaxpayersKpp            = "PAC$HRZ_REPORT.GET_TAXPAYERS_KPP";

        #region Private members

        /// <summary> A limit to depth of requested levels in graph tree. </summary>
        private const int TreeLevelDepthLimit = 64;

        private readonly IServiceProvider _service;

        //private const string SearchSQLCommandPattern =
        //    "BEGIN OPEN :pResultSet FOR {0}; END;";
        //private const string CountSQLPattern =
        //    "BEGIN SELECT COUNT(*) INTO :pResult FROM V$PYRAMID_TABLE vw WHERE vw.REQUEST_ID = :pRequestId {0}; END;";
        //private const string SearchSQLPattern =
        //    "SELECT vw.* FROM V$PYRAMID_TABLE vw WHERE vw.REQUEST_ID = :pRequestId {0} {1}";
        //private const string ViewAlias = "vw";

        //private const string FunctionGetStatus = "F$GET_REQUEST_STATUS";
        //private const string FunctionCountSummary = "F$COUNT_SUMMARY";
        //private const string ProcedureLoadSummary = "P$LOAD_SUMMARY";
        //private const string ProcedureLoadContractor = "P$LOAD_CONTRACTOR";

        #endregion Private members

        internal PyramidDataAdapter( IServiceProvider service )
        {
            _service = service;
        }

        public IReadOnlyCollection<GraphData> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            List<GraphData>         lstRelations = null;
            IEnumerable<GraphData>  relations    = null;
            if ( nodesKeyParameters.IsPurchase )
            {
                relations = LoadGraphNodesInternal( nodesKeyParameters );
            }
            else
            {
                Parallel.Invoke( new ParallelOptions { CancellationToken = CancellationToken.None },
                    () => lstRelations  = LoadGraphNodesInternal( nodesKeyParameters ),
                    () => relations     = LoadGraphNodesFakeNode( nodesKeyParameters ) );

                if ( lstRelations.Count > 0 || !nodesKeyParameters.IsRoot )
                {
                    lstRelations.AddRange( relations.Where( n => n.Level > 1 ) );
                    relations = lstRelations;
                }
            }
            return relations.ToReadOnly();
        }

        public IReadOnlyCollection<GraphData> LoadReportGraphNodes(GraphNodesKeyParameters nodesKeyParameters)
        {
            if (nodesKeyParameters.LayersLimit > TreeLevelDepthLimit)
                throw new ArgumentException(string.Format("Requested node level number is more than limit: {0}", TreeLevelDepthLimit), "nodesKeyParameters");

            return new ListCommandExecuter<GraphData>(new GraphDataDataMapper(), _service)
                .TryExecute(new ReportGraphNodesQueryBuilder(nodesKeyParameters), Constants.DB_MIRROR_CONFIG_KEY)
                .ToReadOnly();
        }

        public PageResult<GraphData> LoadGraphNodeChildrenByPage(GraphNodesKeyParameters nodesKeyParameters)
        {
            if (nodesKeyParameters.LayersLimit > TreeLevelDepthLimit)
                throw new ArgumentException(string.Format("Requested node level number is more than limit: {0}", TreeLevelDepthLimit), "nodesKeyParameters");

            var listResult = new ListCommandExecuter<GraphData>(new GraphDataDataMapper(), _service)
                .TryExecute(new GraphNodeChildrenByPageQueryBuilder(nodesKeyParameters), Constants.DB_MIRROR_CONFIG_KEY)
                .ToList();
            return new PageResult<GraphData>(listResult, listResult.Count);
        }

        public IReadOnlyCollection<DeductionDetail> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters )
        {
            if ( deductionKeyParameters.LayersLimit > TreeLevelDepthLimit )
                throw new ArgumentException( string.Format( "Requested node level number is more than limit: {0}", TreeLevelDepthLimit ), "deductionKeyParameters" );

                return new ListCommandExecuter<DeductionDetail>( new DeductionDetailDataMapper(), _service)
                    .TryExecute(new DeductionDetailsQueryBuilder(deductionKeyParameters), Constants.DB_MIRROR_CONFIG_KEY)
                    .ToReadOnly();
        }

        public IReadOnlyCollection<string> LoadTaxpayersKpp( string inn )
        {
                return new ListCommandExecuter<string>( new TaxpayersKppDataMapper(), _service)
                    .TryExecute(new TaxpayersKppQueryBuilder(inn), Constants.DB_MIRROR_CONFIG_KEY)
                    .ToReadOnly();
        }

        /// <summary> Запрашивает данные для узла дерева без декларации </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        private IReadOnlyCollection<GraphData> LoadGraphNodesFakeNode( GraphNodesKeyParameters nodesKeyParameters )
        {
            return new ListCommandExecuter<GraphData>( new RevertedGraphDataDataMapper(), _service)
                    .TryExecute(new GraphNodesFakeNodeQueryBuilder(nodesKeyParameters), Constants.DB_MIRROR_CONFIG_KEY)
                    .ToReadOnly();
        }

        private List<GraphData> LoadGraphNodesInternal(GraphNodesKeyParameters nodesKeyParameters)
        {
            if (nodesKeyParameters.LayersLimit > TreeLevelDepthLimit)
                throw new ArgumentException(string.Format("Requested node level number is more than limit: {0}", TreeLevelDepthLimit), "nodesKeyParameters");

            return new ListCommandExecuter<GraphData>(new GraphDataDataMapper(), _service)
                .TryExecute(new GraphNodesQueryBuilder(nodesKeyParameters), Constants.DB_MIRROR_CONFIG_KEY)
                .ToList();
        }

   }
}