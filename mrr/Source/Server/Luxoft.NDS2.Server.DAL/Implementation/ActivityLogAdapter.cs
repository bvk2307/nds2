﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class ActivityLogAdapter : BaseOracleTableAdapter, IActivityLogAdapter
    {
        public ActivityLogAdapter(IServiceProvider service) : base(service) { }

        public void Write(ActivityLogEntry entry, string sid)
        {
            Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, PROC_WRITE),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.VarcharIn(P_SID, sid),
                    FormatCommandHelper.NumericIn(P_ENTRY_ID, (int) entry.LogType),
                    FormatCommandHelper.NumericIn(P_ELAPS_MILSC, entry.ElapsedMilliseconds),
                    FormatCommandHelper.NumericIn(P_COUNT, entry.Count)
                });
        }

        public List<ActivityReportSummary> GetReportData(DateTime date)
        {
            Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, PROC_PREPARE_REPORT),
                CommandType.StoredProcedure,
                new[] { FormatCommandHelper.DateIn(P_DATE, date) });
            var data = ExecuteList(SELECT_REPORT, CommandType.Text, new OracleParameter[] { }, BuildReport);

            return data;
        }

        public List<long> GetUserCount(DateTime date)
        {
            var data = ExecuteList(string.Format(COUNT_USERS, date.ToString("yyyy.MM.dd")), CommandType.Text, new OracleParameter[] { }, reader => reader.ReadInt64("total"));
            return data;
        }

        private ActivityReportSummary BuildReport(OracleDataReader reader)
        {
            var item = new ActivityReportSummary();

            item.Begin = reader.ReadDateTime("begin").Value;
            item.End = reader.ReadDateTime("end").Value;
            item.Logons = reader.ReadInt("logons");
            item.Decl_open = reader.ReadInt("decl_open");
            item.Decl_size = reader.ReadInt("decl_size");
            item.Decl_ext_open = reader.ReadInt("decl_ext_open");
            item.Man_sel_call = reader.ReadInt("man_sel_call");
            item.Reports = reader.ReadInt("reports");
            item.Decl_time = reader.ReadInt("decl_time");
            item.Decl_sov_time = reader.ReadInt("decl_sov_time");
            item.Decl_mrr_time = reader.ReadInt("decl_mrr_time");
            item.Reports_time = reader.ReadInt("reports_time");
            item.Decl_time_min = reader.ReadInt("decl_time_min");
            item.Decl_time_max = reader.ReadInt("decl_time_max");
            item.Decl_time_avg = reader.ReadInt("decl_time_avg");
            item.Decl_size_min = reader.ReadInt("decl_size_min");
            item.Decl_size_max = reader.ReadInt("decl_size_max");
            item.Decl_size_avg = reader.ReadInt("decl_size_avg");
            item.Decl_sov_time_min = reader.ReadInt("decl_sov_time_min");
            item.Decl_sov_time_max = reader.ReadInt("decl_sov_time_max");
            item.Decl_sov_time_avg = reader.ReadInt("decl_sov_time_avg");
            item.Decl_mrr_time_min = reader.ReadInt("decl_mrr_time_min");
            item.Decl_mrr_time_max = reader.ReadInt("decl_mrr_time_max");
            item.Decl_mrr_time_avg = reader.ReadInt("decl_mrr_time_avg");
            item.Reports_time_min = reader.ReadInt("reports_time_min");
            item.Reports_time_max = reader.ReadInt("reports_time_max");
            item.Reports_time_avg = reader.ReadInt("reports_time_avg");
            item.Man_sel_time_min = reader.ReadInt("man_sel_time_min");
            item.Man_sel_time_max = reader.ReadInt("man_sel_time_max");
            item.Man_sel_time_avg = reader.ReadInt("man_sel_time_avg");

            item.Decl_open_uniq = reader.ReadInt("decl_open_uniq");
            item.Decl_ext_open_uniq = reader.ReadInt("decl_ext_open_uniq");
            item.Man_sel_call_uniq = reader.ReadInt("man_sel_call_uniq");
            item.Reports_uniq = reader.ReadInt("reports_uniq");
            item.MaxUserForHour = reader.ReadInt("max_user_for_hour");

            return item;
        }

        private const string PACKAGE_NAME = "PAC$ACTIVITY_LOG";
        private const string PROC_WRITE = "WRITE";
        private const string PROC_PREPARE_REPORT = "PREPARE_REPORT";
        private const string P_SID = "pSid";
        private const string P_ENTRY_ID = "pEntryId";
        private const string P_ELAPS_MILSC = "pElapsMilsc";
        private const string P_COUNT = "pCount";
        private const string P_DATE = "pDate";

        private const string SELECT_REPORT = "select * from NDS2_MRR_USER.ACTIVITY_LOG_REPORT order by BEGIN";

        private const string COUNT_USERS = "select count(distinct sid) as total from activity_log where trunc(timestamp) = TO_DATE('{0}', 'yyyy.mm.dd')";
    }
}
