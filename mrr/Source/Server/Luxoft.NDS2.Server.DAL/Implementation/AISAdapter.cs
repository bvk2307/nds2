﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    class AISAdapter : BaseOracleTableAdapter, IAISAdapter
    {
        #region Основная часть

        private IServiceProvider service;

        public AISAdapter(IServiceProvider service)
            : base(service)
        {
            this.service = service;
        }

        public ASKDekl GetDeclaration(long declId)
        {
            ASKDekl ret = null;

            List<ASKDekl> rez = ExecuteList<ASKDekl>(
                "select * from V$ASK_DECLANDJRNL where zip = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildASKDekl);

            ret = rez.FirstOrDefault();

            if (ret != null)
            {
                ret.SumOper4 = GetSumOper4List(declId);
                ret.SumOper6 = GetSumOper6(declId);
                ret.SumPer = GetSumPer(declId);
                ret.SumOper7 = GetSumOper7(declId);
                ret.SumUplNA = GetSumUplNA(declId);
                ret.SumVosUpl = GetSumVosUpl(declId);
                ret.SvedNalGodI = GetSvedNalGodI(declId);                
            }


            return ret ?? new ASKDekl();
        }

        /// <summary>
        /// Mapping ASKDekl
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private ASKDekl BuildASKDekl(OracleDataReader reader)
        {
            ASKDekl ret = new ASKDekl();

            ret.Id = reader.ReadInt64("Id");
            ret.ZIP = reader.ReadInt64("ZIP");
            ret.IdFajl = reader.ReadString("IdFajl");
            ret.VersProg = reader.ReadString("VersProg");
            ret.VersForm = reader.ReadString("VersForm");
            ret.PriznNal8_12 = reader.ReadString("PriznNal8_12");
            ret.PriznNal8 = reader.ReadString("PriznNal8");
            ret.PriznNal81 = reader.ReadString("PriznNal81");
            ret.PriznNal9 = reader.ReadString("PriznNal9");
            ret.PriznNal91 = reader.ReadString("PriznNal91");
            ret.PriznNal10 = reader.ReadString("PriznNal10");
            ret.PriznNal11 = reader.ReadString("PriznNal11");
            ret.PriznNal12 = reader.ReadString("PriznNal12");
            ret.KND = reader.ReadString("KND");
            ret.DataDok = reader.ReadDateTime("DataDok");
            ret.Period = reader.ReadString("Period");
            ret.OtchetGod = reader.ReadString("OtchetGod");
            ret.KodNO = reader.ReadString("KodNO");
            ret.NomKorr = reader.ReadString("NomKorr");
            ret.PoMestu = reader.ReadString("PoMestu");
            ret.OKVED = reader.ReadString("OKVED");
            ret.Tlf = reader.ReadString("Tlf");
            ret.NaimOrg = reader.ReadString("NaimOrg");
            ret.INNNP = reader.ReadString("INNNP");
            ret.KPPNP = reader.ReadString("KPPNP");
            ret.FormReorg = reader.ReadString("FormReorg");
            ret.INNReorg = reader.ReadString("INNReorg");
            ret.KPPReorg = reader.ReadString("KPPReorg");
            ret.FamiliyaNP = reader.ReadString("FamiliyaNP");
            ret.ImyaNP = reader.ReadString("ImyaNP");
            ret.OtchestvoNP = reader.ReadString("OtchestvoNP");
            ret.PrPodp = reader.ReadString("PrPodp");
            ret.FamiliyaPodp = reader.ReadString("FamiliyaPodp");
            ret.ImyaPodp = reader.ReadString("ImyaPodp");
            ret.OtchestvoPodp = reader.ReadString("OtchestvoPodp");
            ret.NaimDok = reader.ReadString("NaimDok");
            ret.NaimOrgPred = reader.ReadString("NaimOrgPred");
            ret.OKTMO = reader.ReadString("OKTMO");
            ret.KBK = reader.ReadString("KBK");
            ret.SumPU173_5 = reader.ReadInt64("SumPU173_5");
            ret.SumPU173_1 = reader.ReadInt64("SumPU173_1");
            ret.NomDogIT = reader.ReadString("NomDogIT");
            ret.DataDogIT = reader.ReadDateTime("DataDogIT");
            ret.DataNachDogIT = reader.ReadDateTime("DataNachDogIT");
            ret.DataKonDogIT = reader.ReadDateTime("DataKonDogIT");
            ret.NalPU164 = reader.ReadInt64("NalPU164");
            ret.NalVosstObshh = reader.ReadInt64("NalVosstObshh");
            ret.RealTov18NalBaza = reader.ReadInt64("RealTov18NalBaza");
            ret.RealTov18SumNal = reader.ReadInt64("RealTov18SumNal");
            ret.RealTov10NalBaza = reader.ReadInt64("RealTov10NalBaza");
            ret.RealTov10SumNal = reader.ReadInt64("RealTov10SumNal");
            ret.RealTov118NalBaza = reader.ReadInt64("RealTov118NalBaza");
            ret.RealTov118SumNal = reader.ReadInt64("RealTov118SumNal");
            ret.RealTov110NalBaza = reader.ReadInt64("RealTov110NalBaza");
            ret.RealTov110SumNal = reader.ReadInt64("RealTov110SumNal");
            ret.RealPredIKNalBaza = reader.ReadInt64("RealPredIKNalBaza");
            ret.RealPredIKSumNal = reader.ReadInt64("RealPredIKSumNal");
            ret.VypSMRSobNalBaza = reader.ReadInt64("VypSMRSobNalBaza");
            ret.VypSMRSobSumNal = reader.ReadInt64("VypSMRSobSumNal");
            ret.OplPredPostNalBaza = reader.ReadInt64("OplPredPostNalBaza");
            ret.OplPredPostSumNal = reader.ReadInt64("OplPredPostSumNal");
            ret.SumNalVs = reader.ReadInt64("SumNalVs");
            ret.SumNal170_3_5 = reader.ReadNullableInt64("SumNal170_3_5");
            ret.SumNal170_3_3 = reader.ReadInt64("SumNal170_3_3");
            ret.KorRealTov18NalBaza = reader.ReadInt64("KorRealTov18NalBaza");
            ret.KorRealTov18SumNal = reader.ReadInt64("KorRealTov18SumNal");
            ret.KorRealTov10NalBaza = reader.ReadInt64("KorRealTov10NalBaza");
            ret.KorRealTov10SumNal = reader.ReadInt64("KorRealTov10SumNal");
            ret.KorRealTov118NalBaza = reader.ReadInt64("KorRealTov118NalBaza");
            ret.KorRealTov118SumNal = reader.ReadInt64("KorRealTov118SumNal");
            ret.KorRealTov110NalBaza = reader.ReadInt64("KorRealTov110NalBaza");
            ret.KorRealTov110SumNal = reader.ReadInt64("KorRealTov110SumNal");
            ret.KorRealPredIKNalBaza = reader.ReadInt64("KorRealPredIKNalBaza");
            ret.KorRealPredIKSumNal = reader.ReadInt64("KorRealPredIKSumNal");
            ret.NalPredNPPriob = reader.ReadInt64("NalPredNPPriob");
            ret.NalPredNPPok = reader.ReadInt64("NalPredNPPok");
            ret.NalIschSMR = reader.ReadInt64("NalIschSMR");
            ret.NalUplTamozh = reader.ReadInt64("NalUplTamozh");
            ret.NalUplNOTovTS = reader.ReadInt64("NalUplNOTovTS");
            ret.NalIschProd = reader.ReadInt64("NalIschProd");
            ret.NalUplPokNA = reader.ReadInt64("NalUplPokNA");
            ret.NalVychObshh = reader.ReadInt64("NalVychObshh");
            ret.SumIschislItog = reader.ReadInt64("SumIschislItog");
            ret.SumVozmPdtv = reader.ReadInt64("SumVozmPdtv");
            ret.SumVozmNePdtv = reader.ReadInt64("SumVozmNePdtv");
            ret.SumNal164It = reader.ReadInt64("SumNal164It");
            ret.NalVychNePodIt = reader.ReadInt64("NalVychNePodIt");
            ret.NalIschislIt = reader.ReadInt64("NalIschislIt");
            ret.SumOper1010449KodOper = reader.ReadString("SumOper1010449KodOper");
            ret.SumOper1010449NalBaza = reader.ReadInt64("SumOper1010449NalBaza");
            ret.SumOper1010449KorIsch = reader.ReadInt64("SumOper1010449KorIsch");
            ret.SumOper1010449NalVosst = reader.ReadInt64("SumOper1010449NalVosst");
            ret.OplPostSv6Mes = reader.ReadInt64("OplPostSv6Mes");
            ret.NaimKnPok = reader.ReadString("NaimKnPok");
            ret.NaimKnPokDL = reader.ReadString("NaimKnPokDL");
            ret.NaimKnProd = reader.ReadString("NaimKnProd");
            ret.NaimKnProdDL = reader.ReadString("NaimKnProdDL");
            ret.NaimZhUchVystSchF = reader.ReadString("NaimZhUchVystSchF");
            ret.NaimZhUchPoluchSchF = reader.ReadString("NaimZhUchPoluchSchF");
            ret.NaimVystSchF173_5 = reader.ReadString("NaimVystSchF173_5");

            ret.KodOper47 = reader.ReadString("KodOper47");
            ret.NalBaza47 = reader.ReadInt64("NalBaza47");
            ret.NalVosst47 = reader.ReadInt64("NalVosst47");

            ret.KodOper48 = reader.ReadString("KodOper48");
            ret.KorNalBazaUv48 = reader.ReadInt64("KorNalBazaUv48");
            ret.KorNalBazaUm48 = reader.ReadInt64("KorNalBazaUm48");

            ret.KodOper50 = reader.ReadString("KodOper50");
            ret.KorNalBazaUv50 = reader.ReadInt64("KorNalBazaUv50");
            ret.KorIschUv50 = reader.ReadInt64("KorIschUv50");
            ret.KorNalBazaUm50 = reader.ReadInt64("KorNalBazaUm50");
            ret.KorIschUm50 = reader.ReadInt64("KorIschUm50");

            ret.RlSr118SumNal = reader.ReadNullableInt64("RlSr118SumNal");
            ret.RlSr118NalBaza = reader.ReadNullableInt64("RlSr118NalBaza");
            ret.RlSr110SumNal = reader.ReadNullableInt64("RlSr110SumNal");
            ret.RlSr110NalBaza = reader.ReadNullableInt64("RlSr110NalBaza");
            ret.UplD151SumNal = reader.ReadNullableInt64("UplD151SumNal");
            ret.UplD151NalBaza = reader.ReadNullableInt64("UplD151NalBaza");
            ret.UplD173SumNal = reader.ReadNullableInt64("UplD173SumNal");
            ret.UplD173NalBaza = reader.ReadNullableInt64("UplD173NalBaza");
            ret.NalPredNPKapStr = reader.ReadNullableInt64("NalPredNPKapStr");
            ret.NalVych171_14 = reader.ReadNullableInt64("NalVych171_14");

            return ret;
        }

        #endregion

        #region SumOper4

        private List<ASKSumOper4> GetSumOper4List(long declId)
        {
            List<ASKSumOper4> ret = ExecuteList<ASKSumOper4>(
                @"select s.* 
                  from V$ASKSUMOPER4 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper4);

            return ret;
        }

        /// <summary>
        /// Mapping SumOper4
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private ASKSumOper4 BuildSumOper4(OracleDataReader reader)
        {
            ASKSumOper4 ret = new ASKSumOper4();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.NalBaza = reader.ReadInt64("NalBaza");
            ret.NalVychPod = reader.ReadInt64("NalVychPod");
            ret.NalNePod = reader.ReadInt64("NalNePod");
            ret.NalVosst = reader.ReadInt64("NalVosst");

            return ret;
        }

        #endregion

        #region SumOper1010447

        private List<ASKSumOper1010447> GetSumOper1010447List(long declId)
        {
            List<ASKSumOper1010447> ret = ExecuteList<ASKSumOper1010447>(
                @"select s.* 
                  from V$ASKSUMOPER1010447 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper1010447);

            return ret;
        }

        private ASKSumOper1010447 BuildSumOper1010447(OracleDataReader reader)
        {
            ASKSumOper1010447 ret = new ASKSumOper1010447();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.NalBaza = reader.ReadInt64("NalBaza");
            ret.NalVosst = reader.ReadInt64("NalVosst");

            return ret;
        }

        #endregion
        
        #region SumOper1010448

        private List<ASKSumOper1010448> GetSumOper1010448List(long declId)
        {
            List<ASKSumOper1010448> ret = ExecuteList<ASKSumOper1010448>(
                @"select s.* 
                  from V$ASKSUMOPER1010448 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper1010448);

            return ret;
        }

        private ASKSumOper1010448 BuildSumOper1010448(OracleDataReader reader)
        {
            ASKSumOper1010448 ret = new ASKSumOper1010448();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.KorNalBazaUm = reader.ReadInt64("KorNalBazaUm");
            ret.KorNalBazaUv = reader.ReadInt64("KorNalBazaUv");

            return ret;
        }

        #endregion
        
        #region SumOper1010450

        private List<ASKSumOper1010450> GetSumOper1010450List(long declId)
        {
            List<ASKSumOper1010450> ret = ExecuteList<ASKSumOper1010450>(
                @"select s.* 
                  from V$ASKSUMOPER1010450 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper1010450);

            return ret;
        }

        private ASKSumOper1010450 BuildSumOper1010450(OracleDataReader reader)
        {
            ASKSumOper1010450 ret = new ASKSumOper1010450();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.KorNalBazaUm = reader.ReadInt64("KorNalBazaUm");
            ret.KorNalBazaUv = reader.ReadInt64("KorNalBazaUv");
            ret.KorIsch_164_23Um = reader.ReadInt64("KorIsch_164_23Um");
            ret.KorIsch_164_23Uv = reader.ReadInt64("KorIsch_164_23Uv");

            return ret;
        }

        #endregion
        
        #region SumOper6

        private List<ASKSumOper6> GetSumOper6(long declId)
        {
            List<ASKSumOper6> ret = ExecuteList<ASKSumOper6>(
                @"select s.* 
                  from V$ASKSUMOPER6 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper6);

            return ret;
        }

        private ASKSumOper6 BuildSumOper6(OracleDataReader reader)
        {
            ASKSumOper6 ret = new ASKSumOper6();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.NalBaza = reader.ReadInt64("NalBaza");
            ret.NalVychNePod = reader.ReadInt64("NalVychNePod");
            ret.SumNal164 = reader.ReadInt64("SumNal164");

            return ret;
        }

        #endregion
        
        #region SumPer

        private List<ASKSumPer> GetSumPer(long declId)
        {
            List<ASKSumPer> ret = ExecuteList<ASKSumPer>(
                @"select s.* 
                  from V$ASKSUMPER s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumPer);


            foreach (ASKSumPer item in ret)
            {
                item.SumOper5 = GetSumOper5(item.Id);
            }


            return ret;
        }

        private ASKSumPer BuildSumPer(OracleDataReader reader)
        {
            ASKSumPer ret = new ASKSumPer();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.OtchetGod = reader.ReadString("OtchetGod");
            ret.Period = reader.ReadString("Period");

            return ret;
        }

        private List<ASKSumOper5> GetSumOper5(decimal sumPerId)
        {
            List<ASKSumOper5> ret = ExecuteList<ASKSumOper5>(
                @"select * 
                  from V$ASKSUMOPER5 
                  where IDSUMPER = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", (long)sumPerId)
                },
                BuildSumOper5);

            return ret;
        }

        private ASKSumOper5 BuildSumOper5(OracleDataReader reader)
        {
            ASKSumOper5 ret = new ASKSumOper5();

            ret.Id = reader.ReadInt64("Id");
            ret.IdSumPer = reader.ReadInt64("IdSumPer");
            ret.KodOper = reader.ReadString("KodOper");
            ret.NalBazaNePod = reader.ReadInt64("NalBazaNePod");
            ret.NalBazaPod = reader.ReadInt64("NalBazaPod");
            ret.NalVychNePod = reader.ReadInt64("NalVychNePod");
            ret.NalVychPod = reader.ReadInt64("NalVychPod");

            return ret;
        }

        #endregion
        
        #region SumOper7

        private List<ASKSumOper7> GetSumOper7(long declId)
        {
            List<ASKSumOper7> ret = ExecuteList<ASKSumOper7>(
                @"select s.* 
                  from V$ASKSUMOPER7 s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOper7);

            return ret;
        }

        private ASKSumOper7 BuildSumOper7(OracleDataReader reader)
        {
            ASKSumOper7 ret = new ASKSumOper7();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.NalNeVych = reader.ReadInt64("NalNeVych");
            ret.StPriobTov = reader.ReadInt64("StPriobTov");
            ret.StRealTov = reader.ReadInt64("StRealTov");

            return ret;
        }

        #endregion
        
        #region SumUplNA

        private List<ASKSumUplNA> GetSumUplNA(long declId)
        {
            List<ASKSumUplNA> ret = ExecuteList<ASKSumUplNA>(
                @"select s.* 
                  from V$ASKSUMUPLNA s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", declId)
                },
                BuildSumOperSumUplNA);

            return ret;
        }

        private ASKSumUplNA BuildSumOperSumUplNA(OracleDataReader reader)
        {
            ASKSumUplNA ret = new ASKSumUplNA();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KodOper = reader.ReadString("KodOper");
            ret.Familiya = reader.ReadString("Familiya");
            ret.Imya = reader.ReadString("Imya");
            ret.INNProd = reader.ReadString("INNProd");
            ret.KBK = reader.ReadString("KBK");
            ret.KPPIno = reader.ReadString("KPPIno");
            ret.NaimProd = reader.ReadString("NaimProd");
            ret.OKTMO = reader.ReadString("OKTMO");
            ret.Otchestvo = reader.ReadString("Otchestvo");
            ret.SumIschisl = reader.ReadInt64("SumIschisl");
            ret.SumIschislNA = reader.ReadInt64("SumIschislNA");
            ret.SumIschislOpl = reader.ReadInt64("SumIschislOpl");
            ret.SumIschislOtgr = reader.ReadInt64("SumIschislOtgr");

            return ret;
        }

        #endregion
        
        #region SumVosUpl

        private List<ASKSumVosUpl> GetSumVosUpl(long declId)
        {
            List<ASKSumVosUpl> ret = ExecuteList<ASKSumVosUpl>(
                @"select s.* 
                  from V$ASKSUMVOSUPL s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", (long)declId)
                },
                BuildSumVosUpl);


            foreach (ASKSumVosUpl item in ret)
            {
                item.SvedNalGod = GetSvedNalGod(item.Id);
            }

            return ret;
        }

        private ASKSumVosUpl BuildSumVosUpl(OracleDataReader reader)
        {
            ASKSumVosUpl ret = new ASKSumVosUpl();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.DataNachAmOtch = reader.ReadDateTime("DataNachAmOtch");
            ret.DataVvodON = reader.ReadDateTime("DataVvodON");
            ret.Dom = reader.ReadString("Dom");
            ret.Gorod = reader.ReadString("Gorod");
            ret.Indeks = reader.ReadString("Indeks");
            ret.KodOpNedv = reader.ReadString("KodOpNedv");
            ret.KodRegion = reader.ReadString("KodRegion");
            ret.Korpus = reader.ReadString("Korpus");
            ret.Kvart = reader.ReadString("Kvart");
            ret.NaimNedv = reader.ReadString("NaimNedv");
            ret.NalVychON = reader.ReadInt64("NalVychON");
            ret.NaselPunkt = reader.ReadString("NaselPunkt");
            ret.Rajon = reader.ReadString("Rajon");
            ret.StVvodON = reader.ReadInt64("StVvodON");
            ret.Ulica = reader.ReadString("Ulica");
            ret.NaimOOS = reader.ReadString("NaimOOS");
            ret.KodOpOOS = reader.ReadString("KodOpOOS");
            ret.DataVvodOOC = reader.ReadDateTime("DataVvodOOC");
            ret.StVvodOOS = reader.ReadNullableInt("StVvodOOS");
            ret.NalVychOOS = reader.ReadNullableInt("NalVychOOS");
            return ret;
        }

        private List<ASKSvedNalGod> GetSvedNalGod(decimal SumVosUplId)
        {
            List<ASKSvedNalGod> ret = ExecuteList<ASKSvedNalGod>(
                @"select * 
                  from V$ASKSVEDNALGOD 
                  where IDSUMVOSUPL = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", (long)SumVosUplId)
                },
                BuildSvedNalGod);

            return ret;
        }

        private ASKSvedNalGod BuildSvedNalGod(OracleDataReader reader)
        {
            ASKSvedNalGod ret = new ASKSvedNalGod();

            ret.Id = reader.ReadInt64("Id");
            ret.IdSumVosUpl = reader.ReadInt64("IdSumVosUpl");
            ret.DataIsp170 = reader.ReadDateTime("DataIsp170");
            ret.DolyaNeObl = reader.ReadInt64("DolyaNeObl");
            ret.GodOtch = reader.ReadString("GodOtch");
            ret.NalGod = reader.ReadInt64("NalGod");

            return ret;
        }

        #endregion

        #region SvedNalGodI

        private List<ASKSvedNalGodI> GetSvedNalGodI(long declId)
        {
            List<ASKSvedNalGodI> ret = ExecuteList<ASKSvedNalGodI>(
                @"select s.* 
                  from V$ASKSVEDNALGODI s
                      left join V$ASK_DECLANDJRNL d on d.id = s.IDDEKL
                  where d.ZIP = :1",
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.NumericIn(":1", (long)declId)
                },
                BuildSvedNalGodI);

            return ret;
        }

        private ASKSvedNalGodI BuildSvedNalGodI(OracleDataReader reader)
        {
            ASKSvedNalGodI ret = new ASKSvedNalGodI();

            ret.Id = reader.ReadInt64("Id");
            ret.IdDekl = reader.ReadInt64("IdDekl");
            ret.KPPInUch = reader.ReadString("KPPInUch");
            ret.SumNalIsch = reader.ReadInt64("SumNalIsch");
            ret.SumNalVych = reader.ReadInt64("SumNalVych");

            return ret;
        }

        #endregion

    }
}
