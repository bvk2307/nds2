﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ChainViewRestrictionTableAdapter : BaseOracleTableAdapter, IChainViewRestrictionTableAdapter
    {
        public ChainViewRestrictionTableAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public IDictionary<Tuple<int, int?>, UserRestrictionConfiguration> All()
        {
            var query = _service.GetQueryText("ChainViewRestrictionConfiguration", "All");
            var data = ExecuteList(query, CommandType.Text, new[] { FormatCommandHelper.Cursor("pCursor") }, ReadLine);

            return data.ToDictionary(item => item.Key, item => item.Value);
        }

        private KeyValuePair<Tuple<int, int?>, UserRestrictionConfiguration> ReadLine(OracleDataReader reader)
        {
            var restrictionKey = Tuple.Create<int, int?>(
                    reader.ReadInt("ROLE_ID"),
                    reader.ReadNullableInt("GROUP_ID")
                );

            var ret = new KeyValuePair<Tuple<int, int?>, UserRestrictionConfiguration>(
                restrictionKey,
                new UserRestrictionConfiguration
                {
                    MaxLevelPurchase = reader.ReadInt("MAX_LEVEL_PURCHASE"),
                    MaxLevelSales = reader.ReadInt("MAX_LEVEL_SALES"),
                    MaxNavigatorChains = reader.ReadInt("MAX_NAVIGATOR_CHAINS"),
                    NotRestricted = reader.ReadBoolean("NOT_RESTRICTED"),
                    AllowPurchase = reader.ReadBoolean("ALLOW_PURCHASE")
                });

            return ret;
        }

        public IList<int> GetIspectionGroups(IList<string> list)
        {
            IList<int> ret = null;
            
            if (list.Count > 0)
            {
                string queryPattern = _service.GetQueryText("ChainViewRestrictionConfiguration", "GetInspectionGroups");
                string queryWhereCondition = String.Concat("SONO_CODE in ( '", String.Join("', '", list), "' )");
                string query = String.Format(queryPattern, queryWhereCondition);

                ret = ExecuteList(query, CommandType.Text, new[] { FormatCommandHelper.Cursor("pCursor") }, ReadInspectionGroupLine);

                if (ret.Count == 0) ret = null;
            }

            return ret;
        }

        private int ReadInspectionGroupLine(OracleDataReader reader)
        {
            return reader.ReadInt("INSPECTION_GROUP_ID");
        }
    }
}
