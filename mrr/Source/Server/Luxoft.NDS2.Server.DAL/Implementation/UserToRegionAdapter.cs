﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class UserToRegionAdapter : BaseOracleTableAdapter, IUserToRegionAdapter
    {
        # region Константы
        private const string SaveProcedure = "USER_TO_REGION_ADD_OR_REMOVE";

        private const string SelectAvaibleRegions = "GET_REGIONS_OF_USER";
        private const string SelectAvaibleInspections = "GET_INSPECTIONS_OF_USER";
        private const string UserSIDParam = "userSID";
        private const string UserTypeIdParam = "userTypeId";

        private const string RegionCodeParam = "REGIONCODE";
        private const string AddOrRemoveParam = "ADDORREMOVE";
        private const string RegionCodeField = "S_CODE";
        private const string UserSIDField = "USER_SID";
        private const string CursorParam = "PFAVCURSOR";
        private const string pRegionCode = "pRegionCode";
        private const string pCursor = "pCursor";
        private const string pContextId = "pContextId";
        private const string SearchUsers = "SELECT USER_SID FROM USER_TO_N_SSRF WHERE N_SSRF_CODE = :pRegionCode";
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string pSelectionId = "pSelectionId";

        //private const string SelectResponsibilityRegionSQL = "select Id, Code, Name, Context_id, Is_New from V$METODOLOG_REGION where context_id = :pContextId order by Code, Id";
        private const string SelectResponsibilityRegionSQL = "select Id, Code, Name, RegionFullName, Context_id, Is_New from V$METODOLOG_REGION where context_id = :pContextId";
        public const string SelectAllInspectionsSQL = "SELECT_ALL_INSPECTIONS";
        # endregion

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса UserToRegionAdapter
        /// </summary>
        /// <param name="service"></param>
        public UserToRegionAdapter(IServiceProvider service) : base(service) { }

        # endregion

        # region Реализация интерфейса IUserToRegionAdapter

        /// <summary>
        /// Добавить или удалить отвественного пользователя по регионам
        /// </summary>
        /// <param name="userSID">id пользователя</param>
        /// <param name="regionCode">код региона</param>
        /// <param name="addOrRemove">добавить или удалить</param>
        public void UserToRegionAddOrRemove(string userSID, string regionCode, bool addOrRemove)
        {
            string addOrRemovestring = "0";
            if (addOrRemove)
            {
                addOrRemovestring = "1";
            }

            Execute(
                FormatCommandHelper.MethodologistPackageProcedure(SaveProcedure),
                CommandType.StoredProcedure,
                new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(UserSIDParam, userSID),
                    FormatCommandHelper.VarcharIn(RegionCodeParam, regionCode),
                    FormatCommandHelper.VarcharIn(AddOrRemoveParam, addOrRemovestring)
                });
        }

        public List<string> GetRegionsOfUser(string userSID)
        {
            List<string> userSIDs = ExecuteList<string>(
                FormatCommandHelper.MethodologistPackageProcedure(SelectAvaibleRegions),
                CommandType.StoredProcedure,
                new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(UserSIDParam, userSID),
                    FormatCommandHelper.Cursor(CursorParam)
                },
                Build);

            return userSIDs;
        }

        public List<string> GetUsers(string regionCode)
        {
            List<string> userSIDs = ExecuteList<string>(
                string.Format(SearchSQLCommandPattern, SearchUsers),
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(pRegionCode, regionCode),
                    FormatCommandHelper.Cursor(pCursor)
                },
                (reader) =>
                {
                    return reader[UserSIDField].ToString();
                });

            return userSIDs;
        }

        # endregion

        /// <summary>
        /// Строит объект по данным из БД
        /// </summary>
        /// <param name="reader">Ссылка на OracleDataReader, содержащий данные</param>
        /// <returns>Фильтр выборки</returns>
        private string Build(OracleDataReader reader)
        {
            return reader[RegionCodeField].ToString();
        }

        private readonly string[] _ignoreOrderBy = 
        {
            TypeHelper<ResponsibilityRegion>.GetMemberName(x => x.InspectionsList),
            TypeHelper<ResponsibilityRegion>.GetMemberName(x => x.AnalystsList),
            TypeHelper<ResponsibilityRegion>.GetMemberName(x => x.ApproversList)
        };

        public List<ResponsibilityRegion> GetResponsibilityRegions(int contextId, QueryConditions conditions)
        {
            var sort = conditions.Sorting;
            var cmd = new StringBuilder(SelectResponsibilityRegionSQL);

            if (sort != null && sort.Count > 0)
            {
                int cnt = 0;
                for (var i = 0; i < sort.Count; ++i)
                {
                    if (_ignoreOrderBy.Contains(sort[i].ColumnKey))
                        continue;
                    cmd.Append(cnt > 0 ? " ," : " order by ");
                    ++cnt;
                    cmd.Append(sort[i].ColumnKey);
                    cmd.Append(sort[0].Order == ColumnSort.SortOrder.Asc ? " ASC" : " DESC");
                }
            }
            else
            {
                cmd.Append(" order by Code");
            }

            var result = ExecuteList(
               cmd.ToString(),
               CommandType.Text,
               new[] { FormatCommandHelper.NumericIn(":pContextId", contextId) },
               DataReaderExtension.ReadResponsibilityRegion);

            LoadInspections(result);
            LoadEmployees(result);

            foreach (var rRegion in result)
            {
                rRegion.RegionFullName = rRegion.Region.RegionFullName;
                if (rRegion.Region.Inspections.Count == 0)
                {
                    var predicate = result.Any(rr => rr.Region.Id != rRegion.Region.Id && rr.Region.Code == rRegion.Region.Code);
                    rRegion.InspectionsList = predicate ? "<Все остальные инспекции региона>" : "<Все>";
                }
                else
                {
                    rRegion.InspectionsList = string.Join(", ", rRegion.Region.Inspections.OrderBy(i => i.Code).Select(i => i.Code));
                }
            }

            if (sort == null || sort.Count <= 0)
                return result;
            var sorts = new List<Tuple<ColumnSort.SortOrder, Func<ResponsibilityRegion, string>>>();
            for (var i = 0; i < sort.Count; ++i)
            {
                var key = sort[i].ColumnKey;
                Func<ResponsibilityRegion, string> lambda;
                if (key == _ignoreOrderBy[0])
                    lambda = x => x.InspectionsList;
                else if (key == _ignoreOrderBy[1])
                    lambda = x => x.AnalystsList;
                else if (key == _ignoreOrderBy[2])
                    lambda = x => x.ApproversList;
                else
                    continue;
                sorts.Add(new Tuple<ColumnSort.SortOrder, Func<ResponsibilityRegion, string>>(sort[i].Order, lambda));
            }
            if (sorts.Count <= 0)
                return result;
            var ordered = sorts[0].Item1 == ColumnSort.SortOrder.Asc
                ? result.OrderBy(sorts[0].Item2)
                : result.OrderByDescending(sorts[0].Item2);
            for (var i = 1; i < sorts.Count; ++i)
            {
                ordered = sorts[i].Item1 == ColumnSort.SortOrder.Asc
                    ? ordered.ThenBy(sorts[i].Item2)
                    : ordered.ThenByDescending(sorts[i].Item2);
            }
            return ordered.ToList();
        }

        private void LoadEmployees(List<ResponsibilityRegion> rRegion)
        {
            var commandText = "select * from metodolog_check_employee";
            var commandType = CommandType.Text;

            Action<List<ResponsibilityRegion>, IDataReader> rowAction = (list, reader) =>
            {
                var reg = list.First(rr => rr.Region.Id == reader.ReadInt64("Resp_Id"));
                if (reader.ReadInt64("Type_Id") == (long)EmployeeType.Analyst)
                {
                    reg.Analitics.Sids.Add(reader.ReadString("sid"));
                }
                else if (reader.ReadInt64("Type_Id") == (long)EmployeeType.Approver)
                {
                    reg.Approvers.Sids.Add(reader.ReadString("sid"));
                }
            };

            LoadEntities(rRegion, commandText, commandType, null, rowAction);
        }

        private void LoadInspections(List<ResponsibilityRegion> rRegion)
        {
            rRegion.ForEach(rr => rr.Region.Inspections = new List<Inspection>());

            var commandText = FormatCommandHelper.MethodologistPackageProcedure(SelectAllInspectionsSQL);
            var commandType = CommandType.StoredProcedure;
            var parameters = new[]
            {
                FormatCommandHelper.Cursor(pCursor) 
            };
            Action<List<ResponsibilityRegion>, IDataReader> rowAction = (list, reader) =>
            {
                var regionResponsibility = list.FirstOrDefault(rr => rr.Region.Id == reader.ReadInt64("Id") && rr.Region.Context == (RegionContext)reader.ReadInt("context_id"));
                if (regionResponsibility != null)
                {
                    regionResponsibility.Region.Inspections.Add(reader.ReadInspection());
                }
            };

            LoadEntities(rRegion, commandText, commandType, parameters, rowAction);
        }

        private void LoadEntities(List<ResponsibilityRegion> rRegion, string commandText, CommandType commandType, OracleParameter[] parameters, Action<List<ResponsibilityRegion>, IDataReader> rowAction)
        {
            using (var connection = Connection())
            using (var command = new OracleCommand(commandText, connection))
            {
                command.CommandType = commandType;
                command.BindByName = true;
                command.Parameters.AddRange(parameters ?? new OracleParameter[0]);

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        rowAction(rRegion, reader);
                    }
                }
                connection.Close();
            }
        }


        public List<string> GetRegionsOfUser(string userSID, EmployeeType type)
        {
            return ExecuteList(
                FormatCommandHelper.MethodologistPackageProcedure(SelectAvaibleRegions),
                CommandType.StoredProcedure,
                new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(UserSIDParam, userSID),
                    FormatCommandHelper.NumericIn(UserTypeIdParam, (long)type),
                    FormatCommandHelper.Cursor(CursorParam)
                },
                reader => reader["Code"].ToString());
        }

        public List<string> GetInspectionsOfUser(string userSID, EmployeeType type)
        {
            return ExecuteList(
             FormatCommandHelper.MethodologistPackageProcedure(SelectAvaibleInspections),
             CommandType.StoredProcedure,
             new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(UserSIDParam, userSID),
                    FormatCommandHelper.NumericIn(UserTypeIdParam, (long)type),
                    FormatCommandHelper.Cursor(CursorParam)
                },
             reader => reader["Code"].ToString());
        }
    }
}