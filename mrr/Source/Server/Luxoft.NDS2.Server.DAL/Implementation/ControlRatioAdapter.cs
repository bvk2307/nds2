﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class ControlRatioAdapter : BaseOracleTableAdapter, IControlRatioAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";

        private const string SelectSqlPattern = "SELECT vw.* FROM " + TablePlaceHolder + " vw WHERE 1=1 {0} {1}";
        private const string CountSqlPattern = "BEGIN SELECT COUNT(1) INTO :pResult FROM " + TablePlaceHolder + " vw WHERE 1 = 1 {0}; END;";

        private const string DeclarationReplacement = "V$CONTROL_RATIO";
        private const string ClaimReplacement = @"(select ccr.doc_id, cr.* from V$CONTROL_RATIO cr 
inner join control_ratio_doc ccr on ccr.cr_id = cr.id 
inner join v$askkontrsoontosh ks on ks.ID = ccr.cr_id where ks.Vypoln <> 1 and ks.DeleteDate is null)";

        private const string GetClaimSql = @"SELECT 
  dc.*,
  d.inn_declarant as TaxPayerInn,
  tp.name_short   as TaxPayerName,
  dt.description  as doc_type_name,
  dst.description as statusDesc,
  d.zip           as declaration_version_id
FROM NDS2_MRR_USER.DOC dc
join NDS2_MRR_USER.declaration_history d on dc.ref_entity_id = d.reg_number and d.type_code = 0 
join NDS2_MRR_USER.control_ratio_doc ccr on ccr.doc_id = dc.doc_id
left join NDS2_MRR_USER.doc_type dt on dt.id = dc.doc_type
left join NDS2_MRR_USER.doc_status dst on dst.id = dc.status and dst.doc_type = dc.doc_type
left join NDS2_MRR_USER.TAX_PAYER tp on tp.inn = d.inn_declarant and tp.kpp_effective = d.kpp_effective
WHERE ccr.cr_id = :pId
ORDER BY CREATE_DATE desc, dc.DOC_ID desc";

        private const string UpdateUserCommentSql = @"BEGIN
merge into control_ratio_comment c
using (select :pId as Id, :pComment as User_Comment from dual) t
  on (c.id = t.id)
when matched then
  update set c.User_Comment = t.User_Comment
when not matched then
  insert (c.Id, c.User_Comment) values (t.Id, t.User_Comment); END;";

        private const string GetSingleControlRatioSql = "select * from V$CONTROL_RATIO where id = :pId";

        private const string ViewAlias = "vw";
        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";
        private const string TablePlaceHolder = "%%Table%%";

        public ControlRatioAdapter(IServiceProvider service) : base(service) { }

        public PageResult<ControlRatio> SelectDeclarationControlRatio(QueryConditions conditions)
        {
            return SelectControlRatio(conditions, DeclarationReplacement);
        }

        public PageResult<ControlRatio> SelectClaimControlRatio(QueryConditions conditions)
        {
            return SelectControlRatio(conditions, ClaimReplacement);
        }

        private PageResult<ControlRatio> SelectControlRatio(QueryConditions conditions, string replacement)
        {
            var query = conditions.ToSQL(SelectSqlPattern, ViewAlias, true, true);
            query.Text = string.Format(query.Text.Replace(TablePlaceHolder, replacement));
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<ControlRatio>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text, parameters.ToArray(), DataReaderExtension.ReadControlRatio);

            query = conditions.ToSQL(CountSqlPattern, ViewAlias, false, true);
            query.Text = string.Format(query.Text.Replace(TablePlaceHolder, replacement));
            query.Parameters.Add(new QueryParameter(false) { Name = CountResult });
            return new PageResult<ControlRatio>(data, DataReaderExtension.ReadInt(Execute(query).Output[CountResult]));
        }

        public DiscrepancyDocumentInfo GetDocumentInfo(long ratioId)
        {
            var result = ExecuteList<DiscrepancyDocumentInfo>(
               GetClaimSql,
               CommandType.Text,
               new[] { FormatCommandHelper.NumericIn(":pId", ratioId) },
               DataReaderExtension.ReadDocumentInfo);
            return result.FirstOrDefault() ?? new DiscrepancyDocumentInfo();
        }

        public void UpdateComment(long id, string comment)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn("pId", id));
            parameters.Add(FormatCommandHelper.VarcharIn("pComment", comment));

            Execute(UpdateUserCommentSql,
                CommandType.Text,
                parameters.ToArray());
        }

        public ControlRatio GetControlRatio(long id)
        {
            return ExecuteList(
                GetSingleControlRatioSql,
                CommandType.Text,
                new[] { FormatCommandHelper.NumericIn(":pId", id) },
                DataReaderExtension.ReadControlRatio).Single();
        }
    }
}