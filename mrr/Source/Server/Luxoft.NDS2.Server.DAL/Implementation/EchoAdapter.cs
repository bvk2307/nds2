﻿using System;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class EchoAdapter : BaseOracleTableAdapter, IEchoAdapter
    {
        public EchoAdapter(IServiceProvider service) : base(service) { }

        public List<string> GetDbInfo()
        {
            var result = new List<string>();

            base.ExecuteList<string>("select * from v$instance", CommandType.Text, null, 
            reader =>
            {
                var instName = reader["INSTANCE_NAME"].ToString();
                var hostName = reader["HOST_NAME"].ToString();
                var ver = reader["VERSION"].ToString();
                var startupTime = reader["STARTUP_TIME"].ToString();

                return string.Format("{0}@{1}({2}) started at {3}", instName, hostName, ver, startupTime);
            });

            return result;
        }
    }
}
