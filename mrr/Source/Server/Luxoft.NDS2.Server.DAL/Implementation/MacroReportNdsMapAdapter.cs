﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class MacroReportNdsMapAdapter : MacroReportAbstractOracleTableAdapter, IMacroReportNdsMapAdapter
    {
        public MacroReportNdsMapAdapter(IServiceProvider service) : base(service) { }

        public List<MapResponseDataNds> GetMapData(MapRequestData model)
        {
            string programName = "PAC$MACRO_REPORT.P$GET_MAP_DATA_FEDERAL";
            List<OracleParameter> dbParameters = new List<OracleParameter>();
            var submitDate = GetMapData(MacroReportType.Nds);

            dbParameters.Add(new OracleParameter("p_cursor", OracleDbType.RefCursor, ParameterDirection.Output));
            dbParameters.Add(new OracleParameter("p_calc_date", OracleDbType.Date, ParameterDirection.Input) { Value = submitDate });

            if (model.MapType == MapType.Regions)
            {
                programName = "PAC$MACRO_REPORT.P$GET_MAP_DATA_DISTRICT";
                dbParameters.Add(new OracleParameter("p_district_id", OracleDbType.Int32, ParameterDirection.Input) { Value = model.RegionId });
            }

            if (model.MapType == MapType.Sono)
            {
                programName = "PAC$MACRO_REPORT.P$GET_MAP_DATA_REGION";
                dbParameters.Add(new OracleParameter("p_region_id", OracleDbType.Varchar2, ParameterDirection.Input) { Value = model.RegionId });
            }


            if (submitDate != null)
            {
                return ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    programName,
                    CommandType.StoredProcedure,
                    dbParameters.ToArray(),
                    BuildMapResponseData);
            }
            return new List<MapResponseDataNds>();
        }

        private MapResponseDataNds BuildMapResponseData(OracleDataReader reader)
        {
            var obj = new MapResponseDataNds();

            obj.AGGREGATE_CODE = reader.ReadString(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.AGGREGATE_CODE));
            obj.AGGREGATE_NAME = reader.ReadString(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.AGGREGATE_NAME));
            obj.FISCAL_YEAR = reader.ReadInt(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.FISCAL_YEAR));
            obj.QUARTER = reader.ReadInt(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.QUARTER));
            obj.DECL_COUNT_COMPENSATION = reader.ReadInt(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.DECL_COUNT_COMPENSATION));
            obj.DECL_COUNT_PAYMENT = reader.ReadInt(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.DECL_COUNT_PAYMENT));
            obj.NDS_CALCULATED_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.NDS_CALCULATED_SUM));
            obj.NDS_DEDUCTION_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.NDS_DEDUCTION_SUM));
            obj.NDS_COMPENSATION_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.NDS_COMPENSATION_SUM));
            obj.NDS_PAYMENT_SUM = reader.ReadDecimal(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.NDS_PAYMENT_SUM));
            obj.WEIGHT_DEDUCT_TO_CALC = reader.ReadDecimal(TypeHelper<MapResponseDataNds>.GetMemberName(t => t.WEIGHT_DEDUCT_TO_CALC));
            return obj;
        }
    }

    internal abstract class MacroReportAbstractOracleTableAdapter : BaseOracleTableAdapter
    {
        protected MacroReportAbstractOracleTableAdapter(IServiceProvider service) : base(service) { }

        public DateTime? GetMapData(MacroReportType reportType)
        {
            var submitDateResponse = ExecuteList(
                Constants.DB_MIRROR_CONFIG_KEY,
                CreateNewSubmitDateRequest(reportType),
                CommandType.Text,
                new OracleParameter[] { },
                BuildSubmitDate);

            var submitDateModel = submitDateResponse.FirstOrDefault();
            if (submitDateModel != null)
            {
                if (submitDateModel.SUBMIT_DATE != null)
                {
                    return submitDateModel.SUBMIT_DATE;
                }
            }
            return null;
        }

        public class SubmitDateModel
        {
            public DateTime? SUBMIT_DATE { get; set; }
        }

        private SubmitDateModel BuildSubmitDate(OracleDataReader reader)
        {
            var obj = new SubmitDateModel();
            obj.SUBMIT_DATE = reader.ReadDateTime(TypeHelper<SubmitDateModel>.GetMemberName(t => t.SUBMIT_DATE));
            return obj;
        }

        private string CreateNewSubmitDateRequest(MacroReportType reportType)
        {
            switch (reportType)
            {
                case MacroReportType.Nds:
                    return "select max(submit_date) as submit_date  from REPORT_DECLARATION_RAW";
                case MacroReportType.Discrepancy:
                    return "select max(process_date) as submit_date from SOV_MATCH_RESULT_SUMMARY";
                default:
                    return String.Empty;
            }
        }
    }
}
