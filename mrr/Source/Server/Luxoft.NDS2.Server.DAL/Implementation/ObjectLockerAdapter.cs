﻿using System;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ObjectLockerAdapter : BaseSelectionAdapter, IObjectLockerAdapter
    {
        public ObjectLockerAdapter(IServiceProvider services)
            : base(services)
        {
        }

        public string Lock(long ObjectId, long ObjectType, string userName, string hostName)
        {
            using (var connection = Connection())
            using (var command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "PAC$OBJECTLOCKER.LOCK_OBJECT";
                command.CommandType = CommandType.StoredProcedure;

                var ret = command.Parameters.Add("v_ret", OracleDbType.NVarchar2, ParameterDirection.ReturnValue);
                ret.Size = 128;
                command.Parameters.Add("p_object_id", OracleDbType.Long, ObjectId, ParameterDirection.Input);
                command.Parameters.Add("p_object_type_id", OracleDbType.Long, ObjectType, ParameterDirection.Input);
                command.Parameters.Add("p_user_name", OracleDbType.NVarchar2, userName, ParameterDirection.Input);
                command.Parameters.Add("p_host_name", OracleDbType.NVarchar2, hostName, ParameterDirection.Input);

                connection.Open();
                command.ExecuteNonQuery();
                return ((OracleString)ret.Value).IsNull
                    ? String.Empty
                    : ret.Value.ToString();
            }
        }

        public void UnLock(string ObjectKey, string userName, string hostName)
        {
            using (var connection = Connection())
            using (var command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "PAC$OBJECTLOCKER.UNLOCK_OBJECT";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("p_object_key", OracleDbType.NVarchar2, ObjectKey, ParameterDirection.Input);
                command.Parameters.Add("p_user_name", OracleDbType.NVarchar2, userName, ParameterDirection.Input);
                command.Parameters.Add("p_host_name", OracleDbType.NVarchar2, hostName, ParameterDirection.Input);
               
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public bool CheckLock(long ObjectId, long ObjectType, string ObjectKey)
        {
            bool retVal = false;

            using (var connection = Connection())
            using (var command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "PAC$OBJECTLOCKER.CHECK_LOCK";
                command.CommandType = CommandType.StoredProcedure;

                var ret = command.Parameters.Add("v_ret", OracleDbType.Decimal, ParameterDirection.ReturnValue);
                command.Parameters.Add("p_object_id", OracleDbType.Long, ObjectId, ParameterDirection.Input);
                command.Parameters.Add("p_object_type_id", OracleDbType.Long, ObjectType, ParameterDirection.Input);
                command.Parameters.Add("p_object_key", OracleDbType.NVarchar2, ObjectKey, ParameterDirection.Input);

                connection.Open();
                command.ExecuteNonQuery();

                long sc = ((OracleDecimal)ret.Value).IsNull ? 0 : ((OracleDecimal)ret.Value).ToInt64();

                if (sc > 0)
                    retVal = true;
            }

            return retVal;
           
        }


        public string GetLockers(long ObjectId, long ObjectType)
        {
            string retVal = string.Empty;

            using (var connection = Connection())
            using (var command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "PAC$OBJECTLOCKER.GET_LOCKERS";
                command.CommandType = CommandType.StoredProcedure;

                var ret = command.Parameters.Add("v_ret", OracleDbType.NVarchar2, ParameterDirection.ReturnValue);
                ret.Size = 2048;
                command.Parameters.Add("p_object_id", OracleDbType.Long, ObjectId, ParameterDirection.Input);
                command.Parameters.Add("p_object_type_id", OracleDbType.Long, ObjectType, ParameterDirection.Input);

                connection.Open();
                command.ExecuteNonQuery();

                retVal = ((OracleString)ret.Value).IsNull ? string.Empty : ret.Value.ToString();
            }

            return retVal;
        }

    }
}