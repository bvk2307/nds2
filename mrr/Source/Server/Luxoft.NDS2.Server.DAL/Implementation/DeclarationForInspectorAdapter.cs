﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс ISelectionDeclarationAdapater
    /// </summary>
    internal class DeclarationForInspectorAdapter : BaseOracleTableAdapter, IDeclarationAdapter
    {
        # region Константы

        private const string SEARCH_SQL_COMMAND_PATTERN = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CURSOR_ALIAS = "pCursor";
        private const string COUNT_RESULT = "pResult";

        private const string QUERY_PATTERN = "SELECT vw.* FROM V$DeclarationInspect vw WHERE 1=1 {0} {1}";
        private const string COUNT_SQL_PATTERN = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$DeclarationInspect vw WHERE 1=1 {0}; END;";
        private const string VIEW_ALIAS = "vw";
        //TODO: добавить идентификатор запроса выгрузки СФ
        private const string INVOICE_SEARCH_SQL_PATTERN = @"SELECT 
                                                            distinct
                                                            vw.DECLARATION_VERSION_ID
                                                            ,vw.CHAPTER
                                                            ,vw.CREATE_DATE
                                                            ,vw.RECEIVE_DATE
                                                            ,vw.OPERATION_CODE
                                                            ,vw.INVOICE_NUM
                                                            ,vw.INVOICE_DATE
                                                            ,vw.CHANGE_NUM
                                                            ,vw.CHANGE_DATE
                                                            ,vw.CORRECTION_NUM
                                                            ,vw.CORRECTION_DATE
                                                            ,vw.CHANGE_CORRECTION_NUM
                                                            ,vw.CHANGE_CORRECTION_DATE
                                                            ,vw.RECEIPT_DOC_NUM
                                                            ,vw.RECEIPT_DOC_DATE
                                                            ,vw.BUY_ACCEPT_DATE
                                                            ,vw.BUYER_INN
                                                            ,vw.BUYER_KPP
                                                            ,vw.BUYER_NAME
                                                            ,vw.SELLER_INN
                                                            ,vw.SELLER_KPP
                                                            ,vw.SELLER_NAME
                                                            ,vw.SELLER_INVOICE_NUM
                                                            ,vw.SELLER_INVOICE_DATE
                                                            ,vw.BROKER_INN
                                                            ,vw.BROKER_KPP
                                                            ,vw.BROKER_NAME
                                                            ,vw.DEAL_KIND_CODE
                                                            ,vw.CUSTOMS_DECLARATION_NUM
                                                            ,vw.OKV_CODE
                                                            ,vw.PRICE_BUY_AMOUNT
                                                            ,vw.PRICE_BUY_NDS_AMOUNT
                                                            ,vw.PRICE_SELL
                                                            ,vw.PRICE_SELL_IN_CURR
                                                            ,vw.PRICE_SELL_18
                                                            ,vw.PRICE_SELL_10
                                                            ,vw.PRICE_SELL_0
                                                            ,vw.PRICE_NDS_18
                                                            ,vw.PRICE_NDS_10
                                                            ,vw.PRICE_TAX_FREE
                                                            ,vw.PRICE_TOTAL
                                                            ,vw.PRICE_NDS_TOTAL
                                                            ,vw.DIFF_CORRECT_DECREASE
                                                            ,vw.DIFF_CORRECT_INCREASE
                                                            ,vw.DIFF_CORRECT_NDS_DECREASE
                                                            ,vw.DIFF_CORRECT_NDS_INCREASE
                                                            ,vw.ROW_KEY
                                                            ,vw.ACTUAL_ROW_KEY
                                                            ,vw.COMPARE_ROW_KEY
                                                            ,vw.COMPARE_ALGO_ID
                                                            ,vw.FORMAT_ERRORS
                                                            ,vw.LOGICAL_ERRORS
                                                            ,vw.DECL_CORRECTION_NUM
                                                            ,vw.PRICE_NDS_BUYER
                                                            ,vw.FULL_TAX_PERIOD
                                                            ,vw.TAX_PERIOD
                                                            ,vw.FISCAL_YEAR
                                                            ,vw.PRIZNAK_DOP_LIST
                                                            ,vw.ORDINAL_NUMBER
                                                            ,vw.SELLER_AGENCY_INFO_INN
                                                            ,vw.SELLER_AGENCY_INFO_KPP
                                                            ,vw.SELLER_AGENCY_INFO_NAME
                                                            ,vw.SELLER_AGENCY_INFO_NUM
                                                            ,vw.SELLER_AGENCY_INFO_DATE
                                                            FROM v$invoice vw WHERE ACTUAL_ROW_KEY is null {0} {1}";
        private const string INVOICE_COUNT_SQL_PATTERN = "BEGIN SELECT COUNT(*) INTO :pResult FROM ({0}); END;";
        //private const string NESTED_INVOICES_SQL_PATTERN = "SELECT * FROM v$invoice vw WHERE ACTUAL_ROW_KEY IN ({0})";

        private const string SET_INSPECTOR_COMMAND_TEXT = @"merge into DECLARATION_OWNER T
                                                        using (select 1 from dual) TMP
                                                        on (:1 = T.DECLARATION_ID)
                                                        when not matched then insert(T.DECLARATION_ID, T.INSPECTOR_SID, T.INSPECTOR_NAME) values(:1, :3, :2)
                                                        when matched then update set T.INSPECTOR_SID = :3, T.INSPECTOR_NAME = :2";

        private const string GET_VERSIONS_COMMAND =
            "select declaration_version_id, correction_number from V$declaration_history where id = :1 order by DECL_DATE desc, correction_number desc";

        private const string GET_DOCUMENT_KNPS_QL_COMMAND = @"select ska.* from SEOD_KNP_ACT ska
                                                        join SEOD_KNP sk on sk.Knp_Id = ska.knp_id
                                                        join SEOD_DECLARATION sd on sd.decl_reg_num = sk.declaration_reg_num
                                                        where sd.nds2_id = :pDeclarationId";
        private const string P_DECLARATION_ID = "pDeclarationId";
        private const string SEARCH_LIST_SQL_PATTERN_FROM_HISTORY = "SELECT vw.* FROM V$declaration_history vw WHERE 1=1 {0} {1}";
        private const string COUNT_SQL_PATTERN_FROM_HISTORY = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$declaration_history vw WHERE 1=1 {0}; END;";

        private const string SEARCH_INVOICE_INNER_CHAPTER_9_10_12 = @"BEGIN
                    SELECT countChapter9, countChapter10, countChapter12 into :pCountChapter9, :pCountChapter10, :pCountChapter12 
                    FROM dual
                    left outer join 
                    (SELECT count(distinct case when vw.CHAPTER = 9 then vw.CHAPTER else 0 end) as countChapter9
                    ,count(distinct case when vw.CHAPTER = 10 then vw.CHAPTER else 0 end) as countChapter10
                    ,count(distinct case when vw.CHAPTER = 12 then vw.CHAPTER else 0 end) as countChapter12
                    FROM v$invoice vw WHERE ACTUAL_ROW_KEY is null  
                    AND vw.DECLARATION_VERSION_ID = :pDeclarationId 
                    AND (
                        (vw.CONTRACTOR_INN = :pInn AND vw.CHAPTER = 9) OR
                        (vw.CONTRACTOR_INN = :pInn AND vw.CHAPTER = 10) OR
                        (vw.CONTRACTOR_INN = :pInn AND vw.CHAPTER = 12)
                        ) 
                    GROUP BY vw.CHAPTER) r on 1 = 1; END;";

        private const string P_INN = "pInn";
        private const string P_COUNT_CHAPTER9 = "pCountChapter9";
        private const string P_COUNT_CHAPTER10 = "pCountChapter10";
        private const string P_COUNT_CHAPTER12 = "pCountChapter12";

        private const string SEARCH_INVOICE_INNER_CHAPTER_8_11 = @"BEGIN 
                    SELECT countChapter8, countChapter11 into :pCountChapter8, :pCountChapter11
                    FROM dual
                    left outer join 
                    (SELECT 
                    count(distinct case when vw.CHAPTER = 8 then vw.CHAPTER else 0 end) as countChapter8
                    ,count(distinct case when vw.CHAPTER = 11 then vw.CHAPTER else 0 end) as countChapter11
                    FROM v$invoice vw WHERE ACTUAL_ROW_KEY is null  
                    AND vw.DECLARATION_VERSION_ID = :pDeclarationId 
                    AND (
                        (vw.BUYER_INN = :pInn AND vw.CHAPTER = 8) OR
                        (vw.BUYER_INN = :pInn AND vw.CHAPTER = 11) 
                        ) 
                    GROUP BY vw.CHAPTER) r on 1 = 1; END;";
        private const string P_COUNT_CHAPTER8 = "pCountChapter8";
        private const string P_COUNT_CHAPTER11 = "pCountChapter11";

        # endregion

        # region Конструкторы

        public DeclarationForInspectorAdapter(IServiceProvider service) : base(service) { }

        # endregion

        # region Реализация интерфейса ISelectionDeclarationsAdapter

        public Dictionary<long, string> GetVersions(long declarationId)
        {
            var result = new Dictionary<long, string>();

            ExecuteList<int>(GET_VERSIONS_COMMAND, CommandType.Text,
                new [] { OraHelper.NumericIn(":1", declarationId) },
                reader =>
                {
                    result.Add(reader.ReadInt64("declaration_version_id"), reader.ReadString("correction_number"));

                    return 0;
                });

            return result;
        }

        public DeclarationSummary Search(long id)
        {
            const string sqlText = "select * from v$declaration_history where declaration_version_id = :1";

            var result = ExecuteList<DeclarationSummary>(
                sqlText,
                CommandType.Text,
                new [] { OraHelper.NumericIn(":1", id) }, BuildDeclaration);

            return result.Any() ? result.First() : null;
        }

        public PageResult<DeclarationSummary> SearchFromHistory(QueryConditions criteria)
        {
            var query = criteria.ToSQL(SEARCH_LIST_SQL_PATTERN_FROM_HISTORY, VIEW_ALIAS, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(OraHelper.Cursor(CURSOR_ALIAS));

            var data = ExecuteList<DeclarationSummary>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDeclaration);

            query = criteria.ToSQL(COUNT_SQL_PATTERN_FROM_HISTORY, VIEW_ALIAS, false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });

            var result = Execute(query);

            return new PageResult<DeclarationSummary>(
                data,
                DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]));
        }

        public PageResult<DeclarationSummary> Search(QueryConditions criteria)
        {
            var query = criteria.ToSQL(QUERY_PATTERN, VIEW_ALIAS, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();

            parameters.Add(OraHelper.Cursor(CURSOR_ALIAS));

            var data = ExecuteList<DeclarationSummary>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDeclaration);

            query = criteria.ToSQL(COUNT_SQL_PATTERN, VIEW_ALIAS, false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });

            var result = Execute(query);

            return new PageResult<DeclarationSummary>(
                data,
                DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]));
        }

        public void SetInspector(long declarationId, string nameInspector, string sidInspector)
        {
            Execute(
                SET_INSPECTOR_COMMAND_TEXT,
                CommandType.Text,
                new []
                {
                    OraHelper.NumericIn(":1", declarationId),
                    OraHelper.VarcharIn(":2", nameInspector),
                    OraHelper.VarcharIn(":3", sidInspector),
                });
        }

        public PageResult<Invoice> GetDeclarationInvoices(QueryConditions criteria)
        {
            var query = criteria.ToSQL(INVOICE_SEARCH_SQL_PATTERN, VIEW_ALIAS, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(OraHelper.Cursor(CURSOR_ALIAS));

            var invoices = ExecuteList<Invoice>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadInvoice);

            query = criteria.ToSQL(INVOICE_SEARCH_SQL_PATTERN, VIEW_ALIAS, false, true);
            query.Text = string.Format(INVOICE_COUNT_SQL_PATTERN, query.Text);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });

            var result = Execute(query);

            return new PageResult<Invoice>(
                    invoices,
                    DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]));
        }

        public DeclarationSummary SelectSingle(long declarationId)
        {
            var data = ExecuteList<DeclarationSummary>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, string.Format(QUERY_PATTERN, " and declaration_version_id = :p1 ", null)),
                CommandType.Text,
                new [] { OraHelper.NumericIn(":p1", declarationId), OraHelper.Cursor(CURSOR_ALIAS) },
                BuildDeclaration);
            return data.Single();
        }


        public DeclarationSummary Version(long declarationId)
        {
            return SelectSingle(declarationId, SEARCH_LIST_SQL_PATTERN_FROM_HISTORY);
        }

        public ActKNP GetActKNP(long declarationId)
        {
            var data = ExecuteList<ActKNP>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, GET_DOCUMENT_KNPS_QL_COMMAND),
                CommandType.Text,
                new [] { OraHelper.NumericIn(P_DECLARATION_ID, declarationId), OraHelper.Cursor(CURSOR_ALIAS) },
                BuilActKNP);
            ActKNP ret = data.FirstOrDefault();
            return ret;
        }

        public Dictionary<int, int> GetChaptersSellers(long declarationId, string Inn)
        {
            var parameters = new List<OracleParameter>();
            parameters.Add(OraHelper.NumericIn(P_DECLARATION_ID, declarationId));
            parameters.Add(OraHelper.VarcharIn(P_INN, Inn));
            parameters.Add(OraHelper.NumericOut(P_COUNT_CHAPTER9));
            parameters.Add(OraHelper.NumericOut(P_COUNT_CHAPTER10));
            parameters.Add(OraHelper.NumericOut(P_COUNT_CHAPTER12));

            var result = Execute(
                SEARCH_INVOICE_INNER_CHAPTER_9_10_12,
                CommandType.Text,
                parameters.ToArray());

            var rets = new Dictionary<int, int>();
            rets.Add(9, DataReaderExtension.ReadInt(result.Output[P_COUNT_CHAPTER9]));
            rets.Add(10, DataReaderExtension.ReadInt(result.Output[P_COUNT_CHAPTER10]));
            rets.Add(12, DataReaderExtension.ReadInt(result.Output[P_COUNT_CHAPTER12]));

            return rets;
        }

        public Dictionary<int, int> GetChaptersBuyers(long declarationId, string inn)
        {
            var parameters = new List<OracleParameter>();
            parameters.Add(OraHelper.NumericIn(P_DECLARATION_ID, declarationId));
            parameters.Add(OraHelper.VarcharIn(P_INN, inn));
            parameters.Add(OraHelper.NumericOut(P_COUNT_CHAPTER8));
            parameters.Add(OraHelper.NumericOut(P_COUNT_CHAPTER11));

            var result = Execute(
                SEARCH_INVOICE_INNER_CHAPTER_8_11,
                CommandType.Text,
                parameters.ToArray());

            var rets = new Dictionary<int, int>();
            rets.Add(8, DataReaderExtension.ReadInt(result.Output[P_COUNT_CHAPTER8]));
            rets.Add(11, DataReaderExtension.ReadInt(result.Output[P_COUNT_CHAPTER11]));

            return rets;
        }

        public long GetActual(string inn, string year, string period)
        {
            var result = Execute(
                @"
                begin
                select DECLARATION_VERSION_ID INTO :pResult from (
                  select DECLARATION_VERSION_ID 
                  from v$declaration_history 
                  where INN = :1 and FISCAL_YEAR = :2 and TAX_PERIOD = :3 
                  order by DECL_DATE desc, correction_number desc)
                where ROWNUM = 1;
                end;",
                CommandType.Text,
                new [] 
                { 
                    OraHelper.VarcharIn(":1", inn),
                    OraHelper.VarcharIn(":2", year),
                    OraHelper.VarcharIn(":3", period),
                    OraHelper.NumericOut(":pResult")
                });

            long ret = -1;

            if (result.Output.Count > 0)
                ret = DataReaderExtension.ReadInt64(result.Output[":pResult"]);

            return ret;
        }


        # endregion

        # region Потроение объекта Declaration

        private DeclarationSummary BuildDeclaration(OracleDataReader reader)
        {
            var obj = new DeclarationSummary();

            obj.ID = reader.ReadInt64("ID");
            obj.DECLARATION_VERSION_ID = reader.ReadInt64("DECLARATION_VERSION_ID");
            obj.SEOD_DECL_ID = reader.ReadNullableInt64("SEOD_DECL_ID");
            obj.ASK_DECL_ID = reader.ReadInt64("ASK_DECL_ID");
            obj.IS_ACTUAL = reader.ReadBoolean("IS_ACTIVE");
            obj.INN = reader.ReadString("INN");
            obj.KPP = reader.ReadString("KPP");
            obj.NAME = reader.ReadString("NAME");
            obj.ADDRESS1 = reader.ReadString("ADDRESS1");
            obj.ADDRESS2 = reader.ReadString("ADDRESS2");
            obj.CATEGORY = reader.ReadBoolean("CATEGORY");
            obj.CATEGORY_RU = reader.ReadString("CATEGORY_RU");
            obj.REGION = obj.CATEGORY ? "" : reader.ReadString("REGION");
            obj.REGION_ENTRY = obj.CATEGORY
                ? new CodeValueDictionaryEntry("", "")
                : new CodeValueDictionaryEntry(reader.ReadString("REGION_CODE"), reader.ReadString("REGION_NAME"));
            obj.SOUN = reader.ReadString("SOUN");
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(reader.ReadString("SOUN_CODE"), reader.ReadString("SOUN_NAME"));
            obj.REG_DATE = reader.ReadDateTime("REG_DATE");
            obj.TAX_MODE = reader.ReadString("TAX_MODE");
            obj.FULL_TAX_PERIOD = reader.ReadString("FULL_TAX_PERIOD");
            obj.OKVED_CODE = reader.ReadString("OKVED_CODE");
            obj.CAPITAL = reader.ReadDecimal("CAPITAL");
            obj.TAX_PERIOD = reader.ReadString("TAX_PERIOD");
            obj.FULL_TAX_PERIOD = reader.ReadString("FULL_TAX_PERIOD");
            obj.FISCAL_YEAR = reader.ReadString("FISCAL_YEAR");
            obj.DECL_DATE = reader.ReadDateTime("DECL_DATE");
            obj.DECL_SIGN = reader.ReadString("DECL_SIGN");
            //obj.DECL_TYPE = reader.ReadString("DECL_TYPE");
            obj.CORRECTION_NUMBER = reader.ReadString("CORRECTION_NUMBER");
            obj.COMPENSATION_AMNT = reader.ReadDecimal("COMPENSATION_AMNT");
            obj.COMPENSATION_AMNT_SIGN = reader.ReadString("COMPENSATION_AMNT_SIGN");
            obj.CH8_DEALS_AMNT_TOTAL = reader.ReadDecimal("CH8_DEALS_AMNT_TOTAL");
            obj.CH9_DEALS_AMNT_TOTAL = reader.ReadDecimal("CH9_DEALS_AMNT_TOTAL");
            obj.CH8_NDS = reader.ReadDecimal("CH8_NDS");
            obj.CH9_NDS = reader.ReadDecimal("CH9_NDS");
            obj.DISCREP_CURRENCY_AMNT = reader.ReadDecimal("DISCREP_CURRENCY_AMNT");
            obj.DISCREP_CURRENCY_COUNT = reader.ReadInt64("DISCREP_CURRENCY_COUNT");
            obj.WEAK_DISCREP_COUNT = reader.ReadInt64("WEAK_DISCREP_COUNT");
            obj.WEAK_DISCREP_AMNT = reader.ReadDecimal("WEAK_DISCREP_AMNT");
            obj.GAP_DISCREP_COUNT = reader.ReadInt64("GAP_DISCREP_COUNT");
            obj.GAP_DISCREP_AMNT = reader.ReadDecimal("GAP_DISCREP_AMNT");
            obj.NDS_INCREASE_DISCREP_COUNT = reader.ReadInt64("NDS_INCREASE_DISCREP_COUNT");
            obj.NDS_INCREASE_DISCREP_AMNT = reader.ReadDecimal("NDS_INCREASE_DISCREP_AMNT");
            obj.DISCREP_MIN_AMNT = reader.ReadDecimal("DISCREP_MIN_AMNT");
            obj.DISCREP_MAX_AMNT = reader.ReadDecimal("DISCREP_MAX_AMNT");
            obj.DISCREP_AVG_AMNT = reader.ReadDecimal("DISCREP_AVG_AMNT");
            obj.DISCREP_TOTAL_AMNT = reader.ReadDecimal("DISCREP_TOTAL_AMNT");
            obj.UPDATE_DATE = reader.ReadDateTime("UPDATE_DATE").Value;

            obj.INSPECTOR = reader.ReadString("INSPECTOR");
            obj.INSPECTORSID = reader.ReadString("INSPECTORSID");

            obj.SUR_CODE = reader.ReadInt("SUR_CODE");
            obj.CONTROL_RATIO_DATE = reader.ReadDateTime("CONTROL_RATIO_DATE");
            obj.CONTROL_RATIO_SEND_TO_SEOD = reader.ReadBoolean("CONTROL_RATIO_SEND_TO_SEOD");
            obj.CONTROL_RATIO_COUNT = reader.ReadInt64("CONTROL_RATIO_COUNT");
            obj.TOTAL_DISCREP_COUNT = reader.ReadInt64("TOTAL_DISCREP_COUNT");
            obj.STATUS = reader.ReadString("STATUS");
            obj.TOTAL_NDS_AMNT_BUYBOOK = reader.ReadDecimal("TOTAL_NDS_AMNT_BUYBOOK");
            obj.TOTAL_COST_SELL_18 = reader.ReadDecimal("TOTAL_COST_SELL_18");
            obj.TOTAL_COST_SELL_10 = reader.ReadDecimal("TOTAL_COST_SELL_10");
            obj.TOTAL_COST_SELL_0 = reader.ReadDecimal("TOTAL_COST_SELL_0");
            obj.TOTAL_TAX_AMNT_18 = reader.ReadDecimal("TOTAL_TAX_AMNT_18");
            obj.TOTAL_TAX_AMNT_10 = reader.ReadDecimal("TOTAL_TAX_AMNT_10");
            obj.TOTAL_COST_SELL_TAXFREE = reader.ReadDecimal("TOTAL_COST_SELL_TAXFREE");

            obj.DISCREP_BUY_BOOK_AMNT = reader.ReadDecimal("DISCREP_BUY_BOOK_AMNT");
            obj.DISCREP_SELL_BOOK_AMNT = reader.ReadDecimal("DISCREP_SELL_BOOK_AMNT");

            obj.TOTAL_DISCREP_COUNT = reader.ReadInt64("TOTAL_DISCREP_COUNT");
            obj.PVP_TOTAL_AMNT = reader.ReadDecimal("PVP_TOTAL_AMNT");
            obj.PVP_BUY_BOOK_AMNT = reader.ReadDecimal("PVP_BUY_BOOK_AMNT");
            obj.PVP_SELL_BOOK_AMNT = reader.ReadDecimal("PVP_SELL_BOOK_AMNT");
            obj.PVP_DISCREP_MIN_AMNT = reader.ReadDecimal("PVP_DISCREP_MIN_AMNT");
            obj.PVP_DISCREP_MAX_AMNT = reader.ReadDecimal("PVP_DISCREP_MAX_AMNT");
            obj.PVP_DISCREP_AVG_AMNT = reader.ReadDecimal("PVP_DISCREP_AVG_AMNT");

            obj.DateCloseKNP = reader.ReadDateTime("DATECLOSEKNP");

            #region BUYBOOK_*

            obj.RECORD_MARK = reader.ReadString("RECORD_MARK");
            obj.BUYBOOK_CONTRAGENT_CNT = reader.ReadInt64("BUYBOOK_CONTRAGENT_CNT");
            obj.BUYBOOK_DK_GAP_CNT = reader.ReadInt64("BUYBOOK_DK_GAP_CNT");
            obj.BUYBOOK_DK_GAP_CAGNT_CNT = reader.ReadInt64("BUYBOOK_DK_GAP_CAGNT_CNT");
            obj.BUYBOOK_DK_GAP_AMNT = reader.ReadNullableDecimal("BUYBOOK_DK_GAP_AMNT");
            obj.BUYBOOK_DK_OTHR_CNT = reader.ReadInt64("BUYBOOK_DK_OTHR_CNT");
            obj.BUYBOOK_DK_OTHR_CAGNT_CNT = reader.ReadInt64("BUYBOOK_DK_OTHR_CAGNT_CNT");
            obj.BUYBOOK_DK_OTHR_AMNT = reader.ReadNullableDecimal("BUYBOOK_DK_OTHR_AMNT");

            #endregion

            return obj;
        }

        # endregion

        # region Потроение объекта ActKNP

        private ActKNP BuilActKNP(OracleDataReader reader)
        {
            var obj = new ActKNP();

            obj.KNP_ID = reader.ReadInt64("KNP_ID");
            obj.Number = reader.ReadString("doc_num");
            obj.Date = reader.ReadDateTime("doc_data");
            obj.SumUnpaid = reader.ReadDecimal("sum_unpaid");
            obj.SumOverestimated = reader.ReadDecimal("sum_overestimated");
            obj.SumCompensation = reader.ReadDecimal("sum_compensation");

            return obj;
        }

        # endregion

        #region Другие вспомогательные методы

        private DeclarationSummary SelectSingle(long declarationId, string sqlFormat)
        {
            var data = ExecuteList<DeclarationSummary>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, string.Format(sqlFormat, " and declaration_version_id = :p1 ", null)),
                CommandType.Text,
                new [] { OraHelper.NumericIn(":p1", declarationId), OraHelper.Cursor(CURSOR_ALIAS) },
                BuildDeclaration);
            return data.Single();
        }

        private DeclarationSummary SelectSingleFromHistory(long declarationId)
        {
            var data = ExecuteList<DeclarationSummary>(
                string.Format(SEARCH_SQL_COMMAND_PATTERN, string.Format(SEARCH_LIST_SQL_PATTERN_FROM_HISTORY, " and declaration_version_id = :p1 ", null)),
                CommandType.Text,
                new [] { OraHelper.NumericIn(":p1", declarationId), OraHelper.Cursor(CURSOR_ALIAS) },
                BuildDeclaration);
            return data.Single();
        }

        #endregion
    }
}