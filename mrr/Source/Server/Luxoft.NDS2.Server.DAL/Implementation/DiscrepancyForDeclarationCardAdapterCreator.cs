﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public static class DiscrepancyForDeclarationCardAdapterCreator
    {
        public static ISearchAdapter<DeclarationDiscrepancy> AllDiscrepancyForDeclarationCardAdapter(IServiceProvider service, long requestId)
        {
            return new AllDiscrepancyForDeclarationCardAdapter(service, requestId);
        }

        public static ISearchAdapter<DeclarationDiscrepancy> KnpDiscrepancyForDeclarationCardAdapter(IServiceProvider service, string inn, string kppEffective, string fiscalYear, int periodEffective)
        {
            return new KnpDiscrepancyForDeclarationCardAdapter(service, inn, kppEffective, fiscalYear, periodEffective);
        }

        public static IHiveRequestAdapter HiveRequestAdapter(IServiceProvider service, HiveRequestBase hiveRequest)
        {
            return new HiveRequestAdapter(service, hiveRequest);
        }



    }
}
