﻿//#define USETASK_ONRELATIONSLOADING

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    [Obsolete( "Use PyramidDataAdapter instead of", error: false )]
    internal class PyramidDataAdapterOld : BaseOracleTableAdapter, IPyramidDataAdapter
    {
        #region Private members

        //private const string PackageName = "PAC$PYRAMID";

        //private const string SearchSQLCommandPattern =
        //    "BEGIN OPEN :pResultSet FOR {0}; END;";
        //private const string CountSQLPattern =
        //    "BEGIN SELECT COUNT(*) INTO :pResult FROM V$PYRAMID_TABLE vw WHERE vw.REQUEST_ID = :pRequestId {0}; END;";
        //private const string SearchSQLPattern =
        //    "SELECT vw.* FROM V$PYRAMID_TABLE vw WHERE vw.REQUEST_ID = :pRequestId {0} {1}";
        //private const string ViewAlias = "vw";

        //private const string FunctionGetStatus = "F$GET_REQUEST_STATUS";
        //private const string FunctionCountSummary = "F$COUNT_SUMMARY";
        //private const string ProcedureLoadSummary = "P$LOAD_SUMMARY";
        //private const string ProcedureLoadContractor = "P$LOAD_CONTRACTOR";

        private const string ProcedureTaxpayerRelation = PyramidDataAdapter.ProcedureTaxpayerRelation;
        private const string ProcedureTaxpayerRelationReport = PyramidDataAdapter.ProcedureTaxpayerRelationReport;
        //private const string ProcedureTaxpayerChildRelation = "PAC$HRZ_REPORT_EXCEL.TAXPAYER_RELATION";
        private const string ProcedureTaxpayerRelationFakeNode = PyramidDataAdapter.ProcedureTaxpayerRelationFakeNode;
        private const string ProcedureGetTaxpayersKpp = PyramidDataAdapter.ProcedureGetTaxpayersKpp;
        /// <summary> A limit to depth of requested levels in graph tree. </summary>
        private const int TreeLevelDepthLimit = 64;

        #endregion Private members

        public PyramidDataAdapterOld( IServiceProvider service ) : base( service ) { }

        private GraphData GetGraphDataFromReader( OracleDataReader reader )
        {
            var data = new GraphData();

            data.Id = reader.ReadNullableInt64( "ID" ) ?? 0;
            data.TaxPayerId = new TaxPayerId( reader.ReadStringNullable( "INN" ), reader.ReadStringNullable( "KPP" ) );
            data.Name = reader.ReadStringNullable( "NAME" );
            data.Level = reader.ReadNullableInt64( "LVL" ) ?? 0;
            data.ParentInn = reader.ReadStringNullable( "PARENT_INN" );
            data.ParentKpp = reader.ReadStringNullable( "parent_kpp" );
            data.TypeCode = (DeclarationTypeCode)reader.ReadInt( "DECL_TYPE_CODE" );
            data.Inspection = new CodeValueDictionaryEntry( reader.ReadString( "SOUN_CODE" ), reader.ReadString( "SOUN_NAME" ) );
            data.ProcessingStage = (DeclarationProcessignStage)reader.ReadInt( "PROCESSINGSTAGE" );
            data.ClientNumber = reader.ReadNullableInt64( "BUYERS_CNT" ) ?? 0;
            data.SellerNumber = reader.ReadNullableInt64( "SELLERS_CNT" ) ?? 0;
            data.CalcNds = reader.ReadNullableDecimal( "CALC_NDS" ) ?? 0;
            data.DeductionNds = reader.ReadNullableDecimal( "DEDUCTION_NDS" ) ?? 0;
            data.NdsPercentage = reader.ReadNullableDecimal( "SHARE_NDS_AMNT" ) ?? 0;
            data.DiscrepancyAmnt = reader.ReadNullableDecimal( "DISCREPANCY_AMNT" ) ?? 0;
            data.GapDiscrepancyTotal = reader.ReadNullableDecimal( "GAP_DISCREPANCY_TOTAL" ) ?? 0;
            data.MappedAmount = reader.ReadNullableDecimal( "MAPPED_AMOUNT" ) ?? 0;
            data.NotMappedAmnt = reader.ReadNullableDecimal( "NOT_MAPPED_AMOUNT" ) ?? 0;
            data.Sur = reader.ReadNullableInt( "SUR" );
            data.VatShare = reader.ReadDecimal( "VAT_SHARE" );
            data.VatTotal = reader.ReadDecimal( "VAT_TOTAL" );
	//apopov 27.9.2016	//TODO!!!   //uncomment when field 'SIGN_CODE' will be returned
            //data.SignType = (DeclarationType)reader.ReadInt("SIGN_CODE");

            data.SurColorArgb = reader.ReadNullableInt( "COLOR" );
            data.SurDescription = reader.ReadString( "DESCRIPTION" );
            var defaultSur = reader.ReadNullableInt("IS_DEFAULT");
            data.SurIsDefault = Convert.ToBoolean( defaultSur );
            data.Region = reader.ReadStringNullable( "REGION" );
            data.RegionCode = reader.ReadStringNullable( "REGION_CODE" );
            data.RegionName = reader.ReadStringNullable( "REGION_NAME" );
            data.DiscrepancyNdsAmnt = reader.ReadNullableDecimal( "NDS_AMOUNT" ) ?? 0;
            data.CreatedInvoiceAmnt = reader.ReadNullableDecimal( "NJSA" ) ?? 0;
            data.ReceivedInvoiceAmnt = reader.ReadNullableDecimal( "NJBA" ) ?? 0;
            data.DeductionBefore20150101Amnt = reader.ReadNullableDecimal( "BEFORE2015_AMOUNT" ) ?? 0;
            data.DeductionAfter20150101Amnt = reader.ReadNullableDecimal( "AFTER2015_AMOUNT" ) ?? 0;
            data.ClientNdsPercentage = reader.ReadNullableDecimal( "CLIENT_NDS_PRC" ) ?? 0;
            data.SellerNdsPercentage = reader.ReadNullableDecimal( "SELLER_NDS_PRC" ) ?? 0;
            data.NdsDiscrepancyTotal = reader.ReadNullableDecimal( "NDS_DISCREP_AMNT" ) ?? 0;

            return data;
        }

        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        public IReadOnlyCollection<GraphData> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
#if USETASK_ONRELATIONSLOADING

            return TaxPayerRelationsMergeHelper.Merge(
                  nodesKeyParameters,
                  LoadTaxpayerRelationsInternal,
                  LoadTaxpayerRelationsFakeNode); 

#else //!USETASK_ONRELATIONSLOADING

            List<GraphData>         lstRelations = null;
            IEnumerable<GraphData>  relations    = null;
            if ( nodesKeyParameters.IsPurchase )
            {
                relations = LoadTaxpayerRelationsInternal( nodesKeyParameters );
            }
            else
            {
                Parallel.Invoke( new ParallelOptions { CancellationToken = CancellationToken.None },
                    () => lstRelations  = LoadTaxpayerRelationsInternal( nodesKeyParameters ),
                    () => relations     = LoadTaxpayerRelationsFakeNode( nodesKeyParameters ) );

                if ( lstRelations.Count > 0 || !nodesKeyParameters.IsRoot )
                {
                    lstRelations.AddRange( relations.Where( n => n.Level > 1 ) );
                    relations = lstRelations;
                }
            }
            return relations.ToReadOnly();

#endif //USETASK_ONRELATIONSLOADING
        }

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        public IReadOnlyCollection<GraphData> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            return LoadTaxpayerReportRelationsInternal( nodesKeyParameters ).ToReadOnly();
        }

        /// <summary> Запрашивает данные для узла дерева без декларации </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        private IEnumerable<GraphData> LoadTaxpayerRelationsFakeNode( GraphNodesKeyParameters nodesKeyParameters )
        {
            List<GraphData> result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureTaxpayerRelationFakeNode,
                    CommandType.StoredProcedure,
                    new[]
                    {
                        FormatCommandHelper.VarcharIn("p_Inn", nodesKeyParameters.Inn),
                        FormatCommandHelper.VarcharIn("p_Kpp", nodesKeyParameters.Kpp),
                        FormatCommandHelper.NumericIn("p_MaxContractors", nodesKeyParameters.MostImportantContractors),
                        FormatCommandHelper.VarcharIn("p_Year", nodesKeyParameters.TaxYear.ToString()),
                        FormatCommandHelper.VarcharIn("p_Qtr", nodesKeyParameters.TaxQuarter.ToString()),
                        FormatCommandHelper.NumericIn("p_isRoot", nodesKeyParameters.IsRoot ? 1 : 0),
                        FormatCommandHelper.Cursor("p_Cursor")
                    },
                    reader =>
                    {
                        var data = this.GetGraphDataFromReader(reader);
                        data.IsReverted = true;
                        return data;
                    });

            return result;
        }

        public PageResult<GraphData> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters )
        {
            if ( nodesKeyParameters.LayersLimit > TreeLevelDepthLimit )
                throw new ArgumentException( string.Format( "Requested node level number is more than limit: {0}", TreeLevelDepthLimit ), "nodesKeyParameters" );

            var result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureTaxpayerRelation,
                    CommandType.StoredProcedure,
                    new []
                    {
                        FormatCommandHelper.VarcharIn("p_Inn", nodesKeyParameters.Inn),
                        FormatCommandHelper.VarcharIn("p_Kpp", nodesKeyParameters.Kpp),
                        FormatCommandHelper.NumericIn("p_MaxContractors", nodesKeyParameters.MostImportantContractors),
                        FormatCommandHelper.NumericIn("p_ByPurchase", nodesKeyParameters.IsPurchase ? 1 : 0),
                        FormatCommandHelper.VarcharIn("p_Year", nodesKeyParameters.TaxYear.ToString()),
                        FormatCommandHelper.VarcharIn("p_Qtr", nodesKeyParameters.TaxQuarter.ToString()),
                        FormatCommandHelper.NumericIn("p_levels", nodesKeyParameters.LayersLimit),
                        FormatCommandHelper.DecimalIn("p_sharePercentNdsCriteria", nodesKeyParameters.SharedNdsPercent),
                        FormatCommandHelper.NumericIn("p_minReturnedContractors", nodesKeyParameters.MinReturnedContractors),
                        FormatCommandHelper.Cursor("p_Cursor")
                    }, GetGraphDataFromReader);

            //apopov 20.2.2016	//DEBUG!!!  //commented because the plain tree report does not work otherwise
            //AssignParent(result, levelNumber: 1);

            return new PageResult<GraphData>( result, result.Count );
        }

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        public IReadOnlyCollection<DeductionDetail> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters )
        {
            if ( deductionKeyParameters.LayersLimit > TreeLevelDepthLimit )
                throw new ArgumentException( string.Format( "Requested node level number is more than limit: {0}", TreeLevelDepthLimit ), "deductionKeyParameters" );

            List<DeductionDetail> result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureTaxpayerRelation,
                    CommandType.StoredProcedure,
                    new []
                    {
                        FormatCommandHelper.VarcharIn("p_Inn", deductionKeyParameters.Inn),
                        FormatCommandHelper.VarcharIn("p_Kpp", deductionKeyParameters.Kpp),
                        FormatCommandHelper.NumericIn("p_MaxContractors", deductionKeyParameters.MostImportantContractors),
                        FormatCommandHelper.NumericIn("p_ByPurchase", deductionKeyParameters.IsPurchase ? 1 : 0),
                        FormatCommandHelper.VarcharIn("p_Year", deductionKeyParameters.TaxYear.ToString()),
                        FormatCommandHelper.VarcharIn("p_Qtr", deductionKeyParameters.TaxQuarter.ToString()),
                        FormatCommandHelper.NumericIn("p_levels", deductionKeyParameters.LayersLimit),
                        FormatCommandHelper.DecimalIn("p_sharePercentNdsCriteria", deductionKeyParameters.SharedNdsPercent),
                        FormatCommandHelper.NumericIn("p_minReturnedContractors", deductionKeyParameters.MinReturnedContractors),
                        FormatCommandHelper.Cursor("p_Cursor")
                    },
                    (reader) =>
                    {
                        var data = new DeductionDetail();
                        return data;
                    });

            return result.ToReadOnly();
        }

        private List<GraphData> LoadTaxpayerReportRelationsInternal( GraphNodesKeyParameters nodesKeyParameters )
        {
            if ( nodesKeyParameters.LayersLimit > TreeLevelDepthLimit )
                throw new ArgumentException( string.Format( "Requested node level number is more than limit: {0}", TreeLevelDepthLimit ), "nodesKeyParameters" );

            List<GraphData> result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureTaxpayerRelationReport,
                    CommandType.StoredProcedure,
                    new[]
                    {
                        nodesKeyParameters.TaxPayerIds.Select(i => i.Inn ?? string.Empty).ToArray().ToOracleArray("p_Inn"),
                        nodesKeyParameters.TaxPayerIds.Select(i => i.Kpp ?? string.Empty).ToArray().ToOracleArray("p_Kpp"),
                        FormatCommandHelper.NumericIn("p_MaxContractors", nodesKeyParameters.MostImportantContractors),
                        FormatCommandHelper.NumericIn("p_ByPurchase", nodesKeyParameters.IsPurchase ? 1 : 0),
                        FormatCommandHelper.VarcharIn("p_Year", nodesKeyParameters.TaxYear.ToString()),
                        FormatCommandHelper.VarcharIn("p_Qtr", nodesKeyParameters.TaxQuarter.ToString()),
                        FormatCommandHelper.DecimalIn("p_sharePercentNdsCriteria", nodesKeyParameters.SharedNdsPercent),
                        FormatCommandHelper.NumericIn("p_minReturnedContractors", nodesKeyParameters.MinReturnedContractors),
                        FormatCommandHelper.NumericIn("p_isRoot", nodesKeyParameters.IsRoot ? 1 : 0),
                        FormatCommandHelper.Cursor("p_Cursor")
                    }, GetGraphDataFromReader);

            return result;
        }

        private List<GraphData> LoadTaxpayerRelationsInternal( GraphNodesKeyParameters nodesKeyParameters )
        {
            if ( nodesKeyParameters.LayersLimit > TreeLevelDepthLimit )
                throw new ArgumentException( string.Format( "Requested node level number is more than limit: {0}", TreeLevelDepthLimit ), "nodesKeyParameters" );

            List<GraphData> result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureTaxpayerRelation,
                    CommandType.StoredProcedure,
                    new[]
                    {
                        FormatCommandHelper.VarcharIn("p_Inn", nodesKeyParameters.Inn),
                        FormatCommandHelper.VarcharIn("p_Kpp", nodesKeyParameters.Kpp),
                        FormatCommandHelper.NumericIn("p_MaxContractors", nodesKeyParameters.MostImportantContractors),
                        FormatCommandHelper.NumericIn("p_ByPurchase", nodesKeyParameters.IsPurchase ? 1 : 0),
                        FormatCommandHelper.VarcharIn("p_Year", nodesKeyParameters.TaxYear.ToString()),
                        FormatCommandHelper.VarcharIn("p_Qtr", nodesKeyParameters.TaxQuarter.ToString()),
                        FormatCommandHelper.NumericIn("p_levels", nodesKeyParameters.LayersLimit),
                        FormatCommandHelper.DecimalIn("p_sharePercentNdsCriteria", nodesKeyParameters.SharedNdsPercent),
                        FormatCommandHelper.NumericIn("p_minReturnedContractors", nodesKeyParameters.MinReturnedContractors),
                        FormatCommandHelper.Cursor("p_Cursor")
                    }, GetGraphDataFromReader);

            return result;
        }

        public IReadOnlyCollection<string> LoadTaxpayersKpp( string inn )
        {
            List<string> result =
                ExecuteList(
                    Constants.DB_MIRROR_CONFIG_KEY,
                    ProcedureGetTaxpayersKpp,
                    CommandType.StoredProcedure,
                    new[]
                    {
                        FormatCommandHelper.VarcharIn("p_Inn", inn),
                        FormatCommandHelper.Cursor("p_Cursor")
                    },
                    (reader) => reader.ReadStringNullable("KPP"));

            return result.ToReadOnly();
        }
    }
}
