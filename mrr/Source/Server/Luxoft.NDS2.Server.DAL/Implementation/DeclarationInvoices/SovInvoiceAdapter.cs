﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    internal class SovInvoiceAdapter : BaseOracleTableAdapter, ISovInvoiceAdapter
    {
        private const string ViewName = "V$INVOICE";

        private const string Alias = "vw";

        private const string CountOutputParamName = "pResult";

        private const string CursorParamName = "pResult";

        public SovInvoiceAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public CountResult Count(int chapter, FilterExpressionBase searchBy, IEnumerable<string> summaryFields = null)
        {
            var countResult = new CountResult();
            Query query;
            if (summaryFields != null && summaryFields.Any())
            {
                query =
                searchBy.ToSQL(
                ViewName.CountPattern(Alias, CountOutputParamName, summaryFields),
                    new PatternProvider(Alias));

                foreach (var sumKey in summaryFields)
                {
                    query.Parameters.Add(new QueryParameter(false) { Name = sumKey });
                }
            }
            else
            {
                query =
                searchBy.ToSQL(
                    ViewName.CountPattern(Alias, CountOutputParamName),
                    new PatternProvider(Alias));
            }

            query.Parameters.Add(new QueryParameter(false) { Name = CountOutputParamName });
            var result = Execute(query);
            if (summaryFields != null && summaryFields.Any())
            {
                countResult.InvoicesSumms = new Dictionary<string, decimal>();
                foreach (var sumKey in summaryFields)
                {
                    countResult.InvoicesSumms.Add(sumKey, DataReaderExtension.ReadDecimal(result.Output[sumKey]));
                }
            }
            countResult.Count = DataReaderExtension.ReadInt(result.Output[CountOutputParamName]);
            return countResult;
        }

        public IEnumerable<Invoice> Search(int chapter, DataQueryContext queryContext)
        {
            var query = 
                queryContext.ToSQL(
                    ViewName.ToSelectPatternOrdered(Alias), 
                    new PatternProvider(Alias));

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorParamName));

            return ExecuteList(
                query.Text.WithCursor(CursorParamName),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadInvoice);
        }

        public Dictionary<int, ImpalaOption> SearchOptions()
        {
            return new Dictionary<int, ImpalaOption>();
        }
    }
}
