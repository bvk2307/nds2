﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Impala;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.Managers;
using Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Impala;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Common.DTO;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    internal class ImpalaInvoiceAdapter : ISovInvoiceAdapter
    {
        private const string Alias = "vw";
        private readonly ServiceHelpers _helper;
        private IServiceProvider _services;

        public ImpalaInvoiceAdapter(IServiceProvider service)
        {
            _services = service;
            _helper = service as ServiceHelpers;
        }

        public CountResult Count(int chapter, FilterExpressionBase searchBy, IEnumerable<string> summaryFields = null)
        {
            CountBuilder cntBuilder = new CountBuilder(chapter, searchBy, summaryFields);    

            var scalarCommandExecuter = new ScalarCommandExecuter(_services);

            /*NOTE: FOR LOAD TEST ONLY*/
            LoggerParameterCollectionBuilder logBuilder =
                new LoggerParameterCollectionBuilder();

            searchBy.ToQuery(logBuilder, string.Empty);

            Action<string, long, string> log = (s, t, err) =>
            {
                _helper.Services.Get<TroubleshootLoger>()
                    .RegisterEvent(TryGetFirstValueEqualsInt64(logBuilder), chapter, t, DateTime.Now, s, "count", err);
            };
            /*NOTE: FOR LOAD TEST ONLY*/

            var dbResult = scalarCommandExecuter.TryExecute(cntBuilder, log);

            var result = new CountResult()
            {
                Count = Convert.ToInt32(dbResult["cnt"])
            };

            dbResult.Remove("cnt");

            Dictionary<string, decimal> summaries = new Dictionary<string, decimal>();

            foreach (var res in dbResult)
            {
                summaries.Add(res.Key, res.Value == DBNull.Value ? 0 : Convert.ToDecimal(res.Value));
            }

            result.InvoicesSumms = summaries;

            return result;

        }

        private long TryGetFirstValueEqualsInt64(LoggerParameterCollectionBuilder logBuilder)
        {
            long ret = 0;
            if (logBuilder != null && logBuilder.Values != null)
            {
                foreach (var item in logBuilder.Values)
                {
                    long result = 0;
                    if (long.TryParse(item.ToString(), out result))
                    {
                        ret = result;
                        break;
                    }
                }
            }
            return ret;
        }

        public IEnumerable<Invoice> Search(int chapter, DataQueryContext queryContext)
        {
            SetOrderByAlways(chapter, queryContext);

            var builder = new PageSearchQueryCommandBuilder(
                GetChapterQuery(chapter),
                queryContext.Where,
                queryContext.OrderBy,
                new PatternProvider(Alias))
                    .Skip(queryContext.Skip)
                    .Take(queryContext.MaxQuantity);

            /*NOTE: FOR LOAD TEST ONLY*/
            LoggerParameterCollectionBuilder logBuilder =
                new LoggerParameterCollectionBuilder();

            queryContext.Where.ToQuery(logBuilder, string.Empty);

            Action<string, long, string> log = (s, t, err) =>
            {
                _helper.Services.Get<TroubleshootLoger>()
                    .RegisterEvent(TryGetFirstValueEqualsInt64(logBuilder), chapter, t, DateTime.Now, s, "select", err);
            };
            /*NOTE: FOR LOAD TEST ONLY*/

            return new ListCommandExecuter<Invoice>(new InvoiceMapper(), _services, new ListCommandExecuterPageOptions(queryContext.Skip, queryContext.MaxQuantity)).TryExecute(builder, log);
        }

        private void SetOrderByAlways(int chapter, DataQueryContext queryContext)
        {
            if (queryContext.OrderBy == null)
            {
                queryContext.OrderBy = new List<ColumnSort>();
            }

            if (queryContext.OrderBy.Count == 0)
            {
                if (chapter == (int)DeclarationInvoicePartitionNumber.Chapter12)
                    queryContext.OrderBy.Add(new ColumnSort() { ColumnKey = TypeHelper<Invoice>.GetMemberName(t => t.ROW_KEY), Order = ColumnSort.SortOrder.Asc });
                else
                    queryContext.OrderBy.Add(new ColumnSort() { ColumnKey = TypeHelper<Invoice>.GetMemberName(t => t.ORDINAL_NUMBER), Order = ColumnSort.SortOrder.Asc });
            }
        }

        public Dictionary<int, ImpalaOption> SearchOptions()
        {
            var builder = new CommandBuilders.Oracle.PlainTextCommandBuilder(_chapterSearchOptions);
            using (var connection = new SingleConnectionFactory(_services))
            {
                SearchSettingsExecuter executer = new SearchSettingsExecuter(_services, connection);

                var result = executer.TryExecute(builder);
                //Для доп листов
                result.Add(81, result[8]);
                result.Add(91, result[9]);

                return result;
            }
        }

        private string GetChapterQuery(int chapter)
        {
            switch (chapter)
            {
                case 8:
                    return _chapter8Query;
                case 9:
                    return _chapter9Query;
                case 10:
                    return _chapter10Query;
                case 11:
                    return _chapter11Query;
                case 12:
                    return _chapter12Query;
                case 13:
                    return _chapter13Query;
                case 14:
                    return _chapter14Query;
                default:
                    return string.Empty;
            }
        }

        private const string _chapterSearchOptions = @"select 
cast(replace(impala_table, 'nds', '') as number(2)) as chapter_index,
partitions_total, usable
from v$impala_config";

        private const string  _chapter8Query= "select * from v_nds8 vw where {0} {1}";

        private const string _chapter9Query = @"select * from v_nds9 vw where {0} {1}";

        private const string _chapter10Query = @"select * from v_nds10 vw where {0} {1}";

        private const string _chapter11Query = @"select * from v_nds11 vw where {0} {1}";

        private const string _chapter12Query = @"select * from v_nds12 vw where {0} {1}";

        private const string _chapter13Query = @"select * from v_nds13 vw where {0} {1}";

        private const string _chapter14Query = @"select * from v_nds14 vw where {0} {1}";

    }
}
