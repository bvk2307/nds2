﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Commands;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    class StringValue
    {
        public string Value { get; set; }

    }

    class StringMapper : IDataMapper<StringValue>
    {
        public StringValue MapData(IDataRecord reader)
        {
            return new StringValue { Value = reader.ReadString("VALUE") };
        }
    }
    
    public class SovInvoiceCustomNumbersAdapter : IInvoiceCustomNumbersAdapter
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connectionFactory;

        public SovInvoiceCustomNumbersAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<string> SearchByInvoice(string invoiceRowKey)
        {
            return new ListCommandExecuter<StringValue>(new StringMapper(), _service, _connectionFactory)
                    .TryExecute(new GetCustomNumbersCommand(invoiceRowKey)).Select(x => x.Value).ToList();
        }
    }
}
