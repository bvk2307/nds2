﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    internal class InvoiceContractorHasDeclarationMapper : IDataMapper<KeyValuePair<long, bool>>
    {
        public KeyValuePair<long, bool> MapData(IDataRecord dataRecord)
        {
            return new KeyValuePair<long, bool>(dataRecord.GetInt64(0), dataRecord.GetBoolean("EXISTS_IN_MC"));
        }
    }

    internal class InvoiceContractorDeclarationAdapter : IInvoiceContractorDeclarationAdapter
    {
        private const string Query = "select CONTRACTOR_KEY, EXISTS_IN_MC from INVOICE_CONTRACTOR_AGGR vw where {0} {1}";

        private const string Alias = "vw";

        private IServiceProvider _loger;

        public InvoiceContractorDeclarationAdapter(IServiceProvider service)
        {
            _loger = service;
        }


        //public 
        public Dictionary<long, bool> Search(FilterExpressionBase expression)
        {
            Dictionary<long, bool> result = new Dictionary<long, bool>();
            var builder = new PageSearchQueryCommandBuilder(Query, expression, new ColumnSort[0] { }, new PatternProvider(Alias));

            using (var connection = new SingleConnectionFactory(_loger))
            {
                ListCommandExecuter<KeyValuePair<long, bool>> executer = new ListCommandExecuter<KeyValuePair<long, bool>>(new InvoiceContractorHasDeclarationMapper(), _loger, connection);

                var data = executer.TryExecute(builder);

                foreach (var keyValuePair in data)
                {
                    if (!result.ContainsKey(keyValuePair.Key))
                    {
                        result.Add(keyValuePair.Key, keyValuePair.Value);
                    }
                }

                return result;
            }


        }
    }
}
