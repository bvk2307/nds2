﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Helpers;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common
{
    internal class InvoiceMapper : IDataMapper<Invoice>
    {
        private List<string> _resultFields = null;

        public Invoice MapData(IDataRecord reader)
        {
            var data = new Invoice();

            if (_resultFields == null)
            {
                _resultFields = new List<string>(reader.FieldCount);

                for (int idx = 0; idx <= reader.FieldCount-1; idx++)
                {
                    _resultFields.Add(reader.GetName(idx).ToUpper());
                }
            }

            data.DeclarationId = reader.GetInt64OrDefault("DECLARATION_VERSION_ID", _resultFields);
            data.CHAPTER = reader.GetInt32OrDefault("CHAPTER", _resultFields);
            data.CREATE_DATE = reader.GetNullableInt32OrDefault("CREATE_DATE", _resultFields).FromShortIntFormat();
            data.RECEIVE_DATE = reader.GetNullableInt32OrDefault("RECEIVE_DATE", _resultFields).FromShortIntFormat();
            data.OPERATION_CODE = reader.GetStringOrDefault("OPERATION_CODE", _resultFields).FromZsonToCommaSeparated();
            data.INVOICE_NUM = reader.GetStringOrDefault("INVOICE_NUM", _resultFields);
            data.INVOICE_DATE = reader.GetNullableInt32OrDefault("INVOICE_DATE", _resultFields).FromShortIntFormat();
            data.CHANGE_NUM = reader.GetStringOrDefault("CHANGE_NUM", _resultFields);
            data.CHANGE_DATE = reader.GetNullableInt32OrDefault("CHANGE_DATE", _resultFields).FromShortIntFormat();
            data.CORRECTION_NUM = reader.GetStringOrDefault("CORRECTION_NUM", _resultFields);
            data.CORRECTION_DATE = reader.GetNullableInt32OrDefault("CORRECTION_DATE", _resultFields).FromShortIntFormat();
            data.CHANGE_CORRECTION_NUM = reader.GetStringOrDefault("CHANGE_CORRECTION_NUM", _resultFields);
            data.CHANGE_CORRECTION_DATE = reader.GetNullableInt32OrDefault("CHANGE_CORRECTION_DATE", _resultFields).FromShortIntFormat();

            var jsonPaymentDocResult = reader.GetStringOrDefault("RECEIPT_DOC_DATE_STR", _resultFields).FromJsonPaymentDocToKeyValueSeparated();

            data.RECEIPT_DOC_DATE_STR = jsonPaymentDocResult.DocumentNumDates;

            string docDates = reader.GetStringOrDefault("BUY_ACCEPT_DATE", _resultFields);
            data.BUY_ACCEPT_DATE = new DocDates(docDates);
            data.BUYER_INN = reader.GetStringOrDefault("BUYER_INN", _resultFields);
            data.BUYER_KPP = reader.GetStringOrDefault("BUYER_KPP", _resultFields);
            data.BUYER_NAME = reader.GetStringOrDefault("BUYER_NAME", _resultFields);
            data.SELLER_INN = reader.GetStringOrDefault("SELLER_INN", _resultFields);
            data.SELLER_KPP = reader.GetStringOrDefault("SELLER_KPP", _resultFields);
            data.SELLER_INN_RESOLVED = reader.GetStringOrDefault("SELLER_INN", _resultFields);
            //data.SELLER_KPP_RESOLVED = reader.GetStringOrDefault("SELLER_KPP", _resultFields);
            data.SELLER_NAME = reader.GetStringOrDefault("SELLER_NAME", _resultFields);
            data.SELLER_INVOICE_NUM = reader.GetStringOrDefault("SELLER_INVOICE_NUM", _resultFields);
            data.SELLER_INVOICE_DATE = reader.GetNullableInt32OrDefault("SELLER_INVOICE_DATE", _resultFields).FromShortIntFormat();
            data.BROKER_INN = reader.GetStringOrDefault("BROKER_INN", _resultFields);
            data.BROKER_KPP = reader.GetStringOrDefault("BROKER_KPP", _resultFields);
            data.BROKER_NAME = reader.GetStringOrDefault("BROKER_NAME", _resultFields);
            data.DEAL_KIND_CODE = reader.GetNullableInt32OrDefault("DEAL_KIND_CODE", _resultFields);
            data.CUSTOMS_DECLARATION_NUM = reader.GetStringOrDefault("CUSTOMS_DECLARATION_NUMBERS", _resultFields);
            data.OKV_CODE = reader.GetStringOrDefault("OKV_CODE", _resultFields);
            data.PRICE_BUY_AMOUNT = reader.GetNullableDecimalOrDefault("PRICE_BUY_AMOUNT", _resultFields);
            data.PRICE_BUY_NDS_AMOUNT = reader.GetNullableDecimalOrDefault("PRICE_BUY_NDS_AMOUNT", _resultFields);
            data.PRICE_SELL = reader.GetNullableDecimalOrDefault("PRICE_SELL", _resultFields);
            data.PRICE_SELL_IN_CURR = reader.GetNullableDecimalOrDefault("PRICE_SELL_IN_CURR", _resultFields);
            data.PRICE_SELL_18 = reader.GetNullableDecimalOrDefault("PRICE_SELL_18", _resultFields);
            data.PRICE_SELL_10 = reader.GetNullableDecimalOrDefault("PRICE_SELL_10", _resultFields);
            data.PRICE_SELL_0 = reader.GetNullableDecimalOrDefault("PRICE_SELL_0", _resultFields);
            data.PRICE_NDS_18 = reader.GetNullableDecimalOrDefault("PRICE_NDS_18", _resultFields);
            data.PRICE_NDS_10 = reader.GetNullableDecimalOrDefault("PRICE_NDS_10", _resultFields);
            data.PRICE_TAX_FREE = reader.GetNullableDecimalOrDefault("PRICE_TAX_FREE", _resultFields);
            data.PRICE_TOTAL = reader.GetNullableDecimalOrDefault("PRICE_TOTAL", _resultFields);
            data.PRICE_NDS_TOTAL = reader.GetNullableDecimalOrDefault("PRICE_NDS_TOTAL", _resultFields);
            data.DIFF_CORRECT_DECREASE = reader.GetNullableDecimalOrDefault("DIFF_CORRECT_DECREASE", _resultFields);
            data.DIFF_CORRECT_INCREASE = reader.GetNullableDecimalOrDefault("DIFF_CORRECT_INCREASE", _resultFields);
            data.DIFF_CORRECT_NDS_DECREASE = reader.GetNullableDecimalOrDefault("DIFF_CORRECT_NDS_DECREASE", _resultFields);
            data.DIFF_CORRECT_NDS_INCREASE = reader.GetNullableDecimalOrDefault("DIFF_CORRECT_NDS_INCREASE", _resultFields);
            data.DECL_CORRECTION_NUM = reader.GetStringOrDefault("DECL_CORRECTION_NUM", _resultFields);
            data.PRICE_NDS_BUYER = reader.GetNullableDecimalOrDefault("PRICE_NDS_BUYER", _resultFields);
            data.FULL_TAX_PERIOD = reader.GetStringOrDefault("FULL_TAX_PERIOD", _resultFields);
            data.TAX_PERIOD = reader.GetStringOrDefault("TAX_PERIOD", _resultFields);
            data.FISCAL_YEAR = reader.GetStringOrDefault("FISCAL_YEAR", _resultFields);
            data.ACTUAL_ROW_KEY = reader.GetStringOrDefault("Actual_Row_Key", _resultFields);
            data.ORDINAL_NUMBER = reader.GetNullableInt64OrDefault("ORDINAL_NUMBER", _resultFields);
            data.SELLER_AGENCY_INFO_INN = reader.GetStringOrDefault("SELLER_AGENCY_INFO_INN", _resultFields);
            data.SELLER_AGENCY_INFO_KPP = reader.GetStringOrDefault("SELLER_AGENCY_INFO_KPP", _resultFields);
            data.SELLER_AGENCY_INFO_NAME = reader.GetStringOrDefault("SELLER_AGENCY_INFO_NAME", _resultFields);
            data.SELLER_AGENCY_INFO_NUM = reader.GetStringOrDefault("SELLER_AGENCY_INFO_NUM", _resultFields);
            data.SELLER_AGENCY_INFO_DATE = reader.GetNullableInt32OrDefault("SELLER_AGENCY_INFO_DATE", _resultFields).FromShortIntFormat();
            data.IS_CHANGED = reader.GetNullableInt32OrDefault("IS_CHANGED", _resultFields);
            data.COMPARE_ROW_KEY = reader.GetStringOrDefault("COMPARE_ROW_KEY", _resultFields);
            data.CONTRACTOR_KEY = reader.GetNullableInt64OrDefault("CONTRACTOR_KEY", _resultFields);
            data.IS_MATCHING_ROW = reader.GetBooleanOrDefault("IS_MATCHING_ROW", _resultFields);

            int rowkey_number = reader.GetInt32OrDefault("ROWKEY_NUM", _resultFields);
            int rowkey_type = reader.GetInt32OrDefault("ROWKEY_TYPE", _resultFields);
            data.ROW_KEY = string.Format("{0}/{1}/{2}",
                        data.DeclarationId.ToString("00000000000000000000"),
                        rowkey_type,
                        rowkey_number.ToString("0000000000"));

            return data;
        }
    }

    public static class InvoiceMapperExtension
    {
        public static JsonPaymentDocResult FromJsonPaymentDocToKeyValueSeparated(this string json) 
        {
            var result = new JsonPaymentDocResult() { RawData = json };

            if (string.IsNullOrEmpty(json))
            {
                return result;
            }

            try
            {
                DataContractJsonSerializer serializer =
                    new DataContractJsonSerializer(typeof (PaymentDoc[]));
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    PaymentDoc[] docs = serializer.ReadObject(ms) as PaymentDoc[];
                    if (docs != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var paymentDoc in docs)
                        {
                            var documentNumDate = new DocumentNumDate() 
                            { 
                                Num = paymentDoc.Number, 
                                Date = ParserHelper.ParseDataTime(paymentDoc.Date.ToString()),
                                DateString = paymentDoc.Date.ToString()
                            };

                            result.DocumentNumDates.PaymentDocs.Add(documentNumDate);
                        }

                        result.DocumentNumDates.RawString = json;

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

            return result;
        }
    }

    public class JsonPaymentDocResult
    {
        public string RawData { get; set; }
        public DocumentNumDates DocumentNumDates { get; set; }

        public JsonPaymentDocResult()
        {
            DocumentNumDates = new DocumentNumDates();
        }
    }
}
