﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common
{
    [DataContract]
    public class PaymentDoc
    {
        [DataMember(Name = "pnum", Order = 1)]
        public String Number { set; get; }

        [DataMember(Name = "pdate", Order = 2)]
        public int? Date { set; get; }
    }
}
