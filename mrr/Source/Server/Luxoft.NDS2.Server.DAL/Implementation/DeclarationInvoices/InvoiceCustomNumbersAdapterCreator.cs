﻿using System;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    public enum InvoiceSource
    {
        Undefined = 0,
        Sov,
        Impala,
        Unknown
    }

    public static class InvoiceCustomNumbersAdapterCreator
    {
        /// <summary>
        /// Создает адаптер для получения спсика таможенных счетов
        /// </summary>
        /// <param name="service">Сервис-провайдер</param>
        /// <param name="invoiceSource">Тип источника данных</param>
        /// <returns>Адаптер</returns>
        public static IInvoiceCustomNumbersAdapter GetInvoiceCustomNumbersAdapter(
            this IServiceProvider service,
            InvoiceSource invoiceSource)
        {
            if (invoiceSource == InvoiceSource.Sov)
                return new SovInvoiceCustomNumbersAdapter(service, new SingleConnectionFactory(service));

            //TODO реализация для Impala
            throw new NotImplementedException(string.Format("Не реализовано для InvoiceSource = {0}", invoiceSource));
        }
    }
}
