﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Commands
{
    internal class GetCustomNumbersCommand : QueryCommandBuilder
    {
        private readonly string _invoiceRowKey;

        private const string Query = "select customs_declaration_num as value from SOV_INVOICE_CUSTOM_AGGREGATE where invoice_row_key = '{0}'";

        public GetCustomNumbersCommand(string invoiceRowKey)
        {
            _invoiceRowKey = invoiceRowKey;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            return string.Format(Query, _invoiceRowKey);
        }
    }
}
