﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using QueryCommandBuilder = Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc.QueryCommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Impala
{
    internal class CountBuilder : QueryCommandBuilder
    {
        private int _chapterType;
        private IEnumerable<string> _summaryFields;
        private FilterExpressionBase _filters;
        public CountBuilder(int chapterType, FilterExpressionBase filter, IEnumerable<string>  summaryFields)
        {
            _chapterType = chapterType;
            _summaryFields = summaryFields;
            _filters = filter;
        }

        protected override string BuildSelectStatement(IDataParameterCollection parameters)
        {
            var paramBuilder = new OdbcParameterCollectionBuilder(
                parameters as DbParameterCollection,
                "p_{0}");

            var resultFields = " count(1) as cnt ";

            if (_summaryFields != null && _summaryFields.Any())
            {
                foreach (var summaryField in _summaryFields)
                {
                    resultFields += ", sum(" + summaryField + ") as " + summaryField;
                }
            }

            return string.Format(_chapterCountQuery, resultFields, GetTableNameByChapterType(_chapterType), _filters.ToQuery(paramBuilder, new PatternProvider("vw")));
        }

        private string GetTableNameByChapterType(int chapterType)
        {
            switch (chapterType)
            {
                case 8: case 81:
                    return "v_nds8";
                case 9: case 91:
                    return "v_nds9";
                case 10:
                    return "v_nds10";
                case 11:
                    return "v_nds11";
                case 12:
                    return "v_nds12";
                case 13:
                    return "v_nds13";
                case 14:
                    return "v_nds14";
            }

            return string.Empty;
        }

        private const string _chapterCountQuery = "select {0} from {1} vw where {2};";
    }
}
