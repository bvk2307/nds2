﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.Common.DTO;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Impala
{

    internal class SearchSettingsExecuter : CommandExecuters.Oracle.CommandExecuterBase<Dictionary<int, ImpalaOption>>
    {
        public SearchSettingsExecuter(IServiceProvider logger, ConnectionFactoryBase connFactory)
            : base(logger, logger)
        {
        }

        protected override Dictionary<int, ImpalaOption> Execute(OracleCommand command)
        {
            var result = new Dictionary<int, ImpalaOption>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int chapterIndex = reader.GetInt32(0);

                    var impalaOption = new ImpalaOption();
                    impalaOption.PartitionsTotal = reader.GetInt32(1);
                    int usable = reader.GetInt32(2);
                    impalaOption.Usable = usable > 0 ? true : false;

                    result.Add(chapterIndex, impalaOption);
                }
            }

            return result;
        }
    }
}
