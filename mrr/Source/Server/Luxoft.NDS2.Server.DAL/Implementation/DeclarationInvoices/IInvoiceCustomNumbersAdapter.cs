﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices
{
    public interface IInvoiceCustomNumbersAdapter
    {
        List<string> SearchByInvoice(string invoiceRowKey);
    }
}