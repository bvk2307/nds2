﻿using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public sealed class DictionaryAnnulmentReasonAdapter : BaseOracleTableAdapter, IDictionaryAnnulmentReasonAdapter
    {
        private const string SelectSql = @"select * from v$dict_annulment_reason";

        public DictionaryAnnulmentReasonAdapter(IServiceProvider service) : base(service) { }

        public List<AnnulmentReason> GetList()
        {
            return ExecuteList(SelectSql, CommandType.Text, new OracleParameter[0], MapItem);
        }

        private AnnulmentReason MapItem(OracleDataReader reader)
        {
            return new AnnulmentReason
            {
                Id = reader.GetInt32("ID"),
                Name = reader.GetString("NAME")
            };
        }
    }
}
