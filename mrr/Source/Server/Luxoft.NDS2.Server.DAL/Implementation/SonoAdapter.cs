﻿using Luxoft.NDS2.Server.DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    class SonoAdapter : ISonoAdapter
    {
        private readonly IServiceProvider _service;

        public SonoAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public List<string> GetSonoCodes()
        {
            Func<ResultCommandHelper, List<string>> readFunc =
                reader =>
                {
                    var codes = new List<string>();
                    while (reader.Read())
                    {
                        codes.Add(reader.GetString("S_CODE"));
                    }
                    return codes;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                             {
                                 Text = "select distinct S_CODE from v$sono",
                                 Type = CommandType.Text,
                                 Parameters = new List<Oracle.DataAccess.Client.OracleParameter>()
                             }, readFunc);
        }
    }
}
