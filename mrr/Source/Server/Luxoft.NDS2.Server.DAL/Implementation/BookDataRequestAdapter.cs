﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс IBookDataRequestAdapter
    /// </summary>
    internal class BookDataRequestAdapter : BaseOracleTableAdapter, IBookDataRequestAdapter
    {
        private const string PackageName = "PAC$DECLARATION";
        private const string GetStatusProcedureName = "P$BOOK_DATA_REQUEST_STATUS";
        private const string SearchRequestId = "P$BOOK_DATA_REQUEST_EXISTS";
        private const string ParamRequestId = "pRequestId";
        private const string ParamStatusId = "pStatus";
        private const string ParamData = "pData";
        private const string ParamInn = "pInn";
        private const string ParamPeriod = "pPeriod";
        private const string ParamYear = "pYear";
        private const string ParamCorrectionNumber = "pCorrectionNumber";
        private const string ParamPartitionNumber = "pPartitionNumber";
        private const string FieldId = "ID";
        private const string FieldName = "NAME";

        private const string SearchRequestIdNdsDiscrepancy = "P$NDSDISCREPANCY_REQ_EXISTS";
        private const string ParamTypeNumber = "pType";
        private const string ParamKindNumber = "pKind";
        private const string GetStatusNdsDiscrepancyProcedureName = "P$NDSDISCREPANCY_REQ_STATUS";


        public BookDataRequestAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public DataRequestStatus Status(long requestId)
        {
            return ExecuteList<DataRequestStatus>(
                string.Format("{0}.{1}", PackageName, GetStatusProcedureName),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(ParamRequestId, requestId),
                    FormatCommandHelper.Cursor(ParamData)
                },
                (reader) =>
                    new DataRequestStatus()
                    {
                        Type = (RequestStatusType)reader.ReadInt(FieldId),
                        Name = reader.ReadString(FieldName)
                    }).First();
        }

        public KeyValuePair<long?, int>? RequestId(
            string inn,
            int period,
            int year,
            int correctionNumber,
            int partitionNumber)
        {
            KeyValuePair<long?, int>? result = null;

            var dbresult = Execute(
                string.Format("{0}.{1}", PackageName, SearchRequestId),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericOut(ParamRequestId),
                    FormatCommandHelper.NumericOut(ParamStatusId),
                    FormatCommandHelper.VarcharIn(ParamInn, inn),
                    FormatCommandHelper.NumericIn(ParamPeriod, period),
                    FormatCommandHelper.NumericIn(ParamYear, year),
                    FormatCommandHelper.NumericIn(ParamCorrectionNumber, correctionNumber),
                    FormatCommandHelper.NumericIn(ParamPartitionNumber, partitionNumber)
                });

            var paramValue = dbresult.Output[ParamRequestId];
            var currentExecutionStatus = dbresult.Output[ParamStatusId];


            if ((paramValue as INullable).IsNull)
            {
                return null;
            }

            result = new KeyValuePair<long?, int>(long.Parse(paramValue.ToString()), int.Parse(currentExecutionStatus.ToString()));

            return result;
        }

        public long? RequestIdNdsDiscrepancies(string inn,
            int period,
            int year,
            int correctionNumber,
            int type, 
            int kind)
        {
            var result = Execute(
                string.Format("{0}.{1}", PackageName, SearchRequestIdNdsDiscrepancy),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericOut(ParamRequestId),
                    FormatCommandHelper.VarcharIn(ParamInn, inn),
                    FormatCommandHelper.NumericIn(ParamPeriod, period),
                    FormatCommandHelper.NumericIn(ParamYear, year),
                    FormatCommandHelper.NumericIn(ParamCorrectionNumber, correctionNumber),
                    FormatCommandHelper.NumericIn(ParamTypeNumber, type),
                    FormatCommandHelper.NumericIn(ParamKindNumber, kind)
                });

            var paramValue = result.Output[ParamRequestId];

            if ((paramValue as INullable).IsNull)
            {
                return null;
            }

            return long.Parse(paramValue.ToString());
        }

        public DataRequestStatus StatusNdsDiscrepancy(long requestId)
        {
            return ExecuteList<DataRequestStatus>(
                string.Format("{0}.{1}", PackageName, GetStatusNdsDiscrepancyProcedureName),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(ParamRequestId, requestId),
                    FormatCommandHelper.Cursor(ParamData)
                },
                (reader) =>
                    new DataRequestStatus()
                    {
                        Type = (RequestStatusType)reader.ReadInt(FieldId),
                        Name = reader.ReadString(FieldName)
                    }).First();
        }
    }
}
