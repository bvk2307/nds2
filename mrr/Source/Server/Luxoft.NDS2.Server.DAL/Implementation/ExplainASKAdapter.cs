﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public class ExplainASKAdapter : IExplainASKAdapter
    {
        #region строковые константы

        private const string ProcedureExplainAddASKZipFajl = "NDS2_MC.P$POJASNENIE_ADD_ASKZIPFAJL";

        private const string ParamFileName = "pFileName";
        private const string ParamPathDirectory = "pPathDirectory";
        private const string ParamSounCode = "pSounCode";
        private const string ParamZip = "pZip";

        #endregion

        private readonly IServiceProvider _service;

        public ExplainASKAdapter(IServiceProvider service)
        {
            _service = service;
        }

        /// <summary>
        /// добавляет запись в таблицу AskZipФайл
        /// </summary>
        /// <param fileName="qc">название файла</param>
        /// <param pathDirectory="qc">путь к директории</param>
        /// <param sounCode="qc">код НО</param>
        /// <returns></returns>
        public long? AddAskZipFile(string fileName, string pathDirectory, string sounCode)
        {
            long? retVal = null;

            var parameters = new List<OracleParameter>()
            {
                FormatCommandHelper.VarcharIn(ParamFileName, fileName),
                FormatCommandHelper.VarcharIn(ParamPathDirectory, pathDirectory),
                FormatCommandHelper.VarcharIn(ParamSounCode, sounCode),
                FormatCommandHelper.NumericOut(ParamZip)
            };

            retVal = new CommandExecuter(_service)
                .TryExecuteAndReadLong(
                    new CommandContext()
                    {
                        Text = ProcedureExplainAddASKZipFajl,
                        Type = CommandType.StoredProcedure,
                        Parameters = parameters
                    }, ParamZip);

            return retVal;
        }
    }
}
