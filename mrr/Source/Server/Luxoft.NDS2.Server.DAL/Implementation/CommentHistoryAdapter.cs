﻿using System;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует адаптер истории комментариев объекта
    /// </summary>
    internal sealed class CommentHistoryAdapter : BaseOracleTableAdapter, ICommentHistoryAdapter
    {
        private const string SELECT_SQL = @"select * from Comment_History data where Object_Id = :pId and Object_Type_Id = :pType {0} {1}";
        private const string INSERT_SQL = @"insert into Comment_History (Object_Id, Object_Type_Id, Issue_Date, Author, Comment_Text) values (:pId, :pType, sysdate, :pAuthor, :pComment)";

        public CommentHistoryAdapter(IServiceProvider service) : base(service) { }

        public List<CommentHistory> GetCommentHistory(long id, ObjectType type, QueryConditions queryConditions)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pId", id),
                FormatCommandHelper.NumericIn(":pType", (long)type)
            };
            Func<OracleDataReader, CommentHistory> getData = reader => new CommentHistory
            {
                Author = reader.ReadString("Author"),
                Comment_Text = reader.ReadString("Comment_Text"),
                Issue_Date = reader.ReadDateTime("Issue_Date"),
                Object_Id = reader.ReadInt64("Object_Id"),
                Object_Type_Id = (ObjectType)reader.ReadInt("Object_Type_Id")
            };
            var sql = queryConditions.ToSQL(SELECT_SQL, "data", false, true);
            return ExecuteList(sql.Text, CommandType.Text, parameters, getData);
        }

        public void AddComment(CommentHistory comment)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pId", comment.Object_Id),
                FormatCommandHelper.NumericIn(":pType", (long)comment.Object_Type_Id),
                FormatCommandHelper.VarcharIn(":pAuthor", comment.Author),
                FormatCommandHelper.VarcharIn(":pComment", comment.Comment_Text)
            };
            Execute(INSERT_SQL, System.Data.CommandType.Text, parameters);
        }
    }
}