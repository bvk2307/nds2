﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Types;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class ContragentsAdapter : BaseOracleTableAdapter, IContragentsAdapter
    {
        private const string QUERY_PARAMS_PATTERN = "SELECT vw.* FROM NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT_PARAMS vw WHERE 1=1 {0} {1}";

        private const string COUNT_PARAMS_PATTERN = "BEGIN SELECT COUNT(1) INTO :pResult FROM NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT_PARAMS vw WHERE 1 = 1 {0}; END;";

        private const string SEARCH_PATTERN = "BEGIN OPEN :pCursor FOR {0}; END;";

        private const string PACKAGE_NAME = "PAC$CONTRAGENTS";
        private const string REQUEST_STATUS_ALIAS = "P$REQUEST_STATUS";
        private const string REQUEST_EXIST_ALIAS = "P$REQUEST_EXISTS";
        private const string REQUEST_PARAMS_STATUS_ALIAS = "P$REQUEST_PARAMS_STATUS";
        private const string REQUEST_PARAMS_EXIST_ALIAS = "P$REQUEST_PARAMS_EXISTS";
        private const string REQUEST_ID_ALIAS = "pRequestId";
        private const string DATA_PARAM_ALIAS = "pData";
        private const string INN_PARAM_ALIAS = "pInn";
        private const string PERIOD_PARAM_ALIAS = "pPeriod";
        private const string YEAR_PARAM_ALIAS = "pYear";
        private const string CORRECTION_NUMBER_PARAM_ALIAS = "pCorrectionNumber";
        private const string STATUS_ID_ALIAS = "ID";
        private const string STATUS_NAME_ALIAS = "NAME";

        public ContragentsAdapter(IServiceProvider service) : base(service) { }

        public long? RequestIdContragentsData(string inn, int taxPeriod, int taxYear, int correctionNum)
        {
            var result = Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, REQUEST_EXIST_ALIAS),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericOut(REQUEST_ID_ALIAS),
                    FormatCommandHelper.VarcharIn(INN_PARAM_ALIAS, inn),
                    FormatCommandHelper.NumericIn(PERIOD_PARAM_ALIAS, taxPeriod),
                    FormatCommandHelper.NumericIn(YEAR_PARAM_ALIAS, taxYear),
                    FormatCommandHelper.NumericIn(CORRECTION_NUMBER_PARAM_ALIAS, correctionNum)
                });

            var paramValue = result.Output[REQUEST_ID_ALIAS] as INullable;

            return (paramValue == null || paramValue.IsNull) ? (long?)null : long.Parse(paramValue.ToString());
        }

        public DataRequestStatus StatusContragentsData(long requestId)
        {
            return ExecuteList(
                string.Format("{0}.{1}", PACKAGE_NAME, REQUEST_STATUS_ALIAS),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(REQUEST_ID_ALIAS, requestId),
                    FormatCommandHelper.Cursor(DATA_PARAM_ALIAS)
                },
                reader =>
                    new DataRequestStatus
                    {
                        Type = (RequestStatusType)reader.ReadInt(STATUS_ID_ALIAS),
                        Name = reader.ReadString(STATUS_NAME_ALIAS)
                    }).First();
        }

        public PageResult<ContragentParamsSummary> SelectContragentParams(QueryConditions conditions)
        {
            var query = conditions.ToSQL(QUERY_PARAMS_PATTERN, "vw", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor("pCursor"));

            var data = ExecuteList(
                string.Format(SEARCH_PATTERN, query.Text),
                CommandType.Text, parameters.ToArray(),
                DataReaderExtension.ReadContragentParamsSummary);

            query = conditions.ToSQL(COUNT_PARAMS_PATTERN, "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = "pResult" });

            return new PageResult<ContragentParamsSummary>(data, DataReaderExtension.ReadInt(Execute(query).Output["pResult"]));
        }

        public long? RequestIdContragentParamsData(string inn, int taxPeriod, int taxYear, int correctionNum)
        {
            var result = Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, REQUEST_PARAMS_EXIST_ALIAS),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericOut(REQUEST_ID_ALIAS),
                    FormatCommandHelper.VarcharIn(INN_PARAM_ALIAS, inn),
                    FormatCommandHelper.NumericIn(PERIOD_PARAM_ALIAS, taxPeriod),
                    FormatCommandHelper.NumericIn(YEAR_PARAM_ALIAS, taxYear),
                    FormatCommandHelper.NumericIn(CORRECTION_NUMBER_PARAM_ALIAS, correctionNum)
                });

            var paramValue = result.Output[REQUEST_ID_ALIAS] as INullable;

            return (paramValue == null || paramValue.IsNull) ? (long?)null : long.Parse(paramValue.ToString());
        }

        public DataRequestStatus StatusContragentParamsData(long requestId)
        {
            return ExecuteList(
                string.Format("{0}.{1}", PACKAGE_NAME, REQUEST_PARAMS_STATUS_ALIAS),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(REQUEST_ID_ALIAS, requestId),
                    FormatCommandHelper.Cursor(DATA_PARAM_ALIAS)
                },
                reader =>
                    new DataRequestStatus
                    {
                        Type = (RequestStatusType)reader.ReadInt(STATUS_ID_ALIAS),
                        Name = reader.ReadString(STATUS_NAME_ALIAS)
                    }).First();
        }
    }
}
