﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.TaxPayerDetails.CommandBuilders;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class TaxPayerAdapter : BaseOracleTableAdapter, ITaxPayerAdapter
    {
        private const string SearchTaxPayer = "select * from V$TAX_PAYER_DETAIL tp where tp.inn = :pInn and tp.kpp_effective = :pKpp";
        private const string SearchTaxPayerNonEffective = "select * from V$TAX_PAYER_DETAIL tp where tp.inn = :pInn and tp.kpp_effective = F$GET_EFFECTIVE_KPP(:pInn, :pKpp)";
        private const string SearchTaxPayerInn = "select * from V$TAX_PAYER_DETAIL tp where tp.inn = :pInn";

        private readonly SingleConnectionFactory _connectionFactory;

        public TaxPayerAdapter(IServiceProvider service)
            : base(service)
        {
            _connectionFactory = new SingleConnectionFactory(service);
        }

        public TaxPayer GetByKppEffective(string inn, string kpp)
        {
            TaxPayer ret = ExecuteList(
                SearchTaxPayer,
                CommandType.Text,
                new[]
                {
                    FormatCommandHelper.VarcharIn("pInn", inn),
                    FormatCommandHelper.VarcharIn("pKpp", kpp)
                },
                BuildTaxPayer).FirstOrDefault();

            return ret;
        }

        public TaxPayer GetByKppOriginal(string inn, string kpp = null)
        {
            TaxPayer ret = ExecuteList(
                SearchTaxPayerNonEffective,
                CommandType.Text,
                new[]
                {
                    FormatCommandHelper.VarcharIn("pInn", inn),
                    FormatCommandHelper.VarcharIn("pKpp", kpp)
                },
                BuildTaxPayer).FirstOrDefault();

            return ret;
        }

        public TaxPayer GetByInn(string inn)
        {
            var ret = ExecuteList(
                SearchTaxPayerInn,
                CommandType.Text,
                new[] { FormatCommandHelper.VarcharIn("pInn", inn) },
                BuildTaxPayer);

            if (ret.Count > 1)
                throw new Exception("Переход на карточку филиала иностранной компании возможен только из списка деклараций");

            return ret.FirstOrDefault();
        }

        private TaxPayer BuildTaxPayer(OracleDataReader reader)
        {
            var obj = new TaxPayer
            {
                Id = reader.ReadInt64("ID"),

                Inn = reader.ReadString("INN"),
                Kpp = reader.ReadString("KPP"),
                KppEffective = reader.ReadString("KPP_EFFECTIVE"),
                Name = reader.ReadString("NAME_FULL"),

                InnSuccessor = reader.ReadString("INN_SUCCESSOR"),
                KppSuccessor = reader.ReadString("KPP_SUCCESSOR"),
                KppEffectiveSuccessor = reader.ReadString("KPP_EFFECTIVE_SUCCESSOR"),
                NameSuccessor = reader.ReadString("NAME_SUCCESSOR"),

                Soun = reader.ReadString("SONO_CODE"),
                OGRN = reader.ReadString("OGRN"),
                Address = reader.ReadString("ADDRESS"),
                RegistryAt = reader.ReadDateTime("DATE_ON"),
                UnregistryAt = reader.ReadDateTime("DATE_OFF"),
                SounName = reader.ReadString("SONO_NAME"),
                RegulationFund = reader.ReadDecimal("UST_CAPITAL"),
                RecordedAt = reader.ReadDateTime("DATE_REG"),
                OkvedCode = reader.ReadString("okved_code"),
                OkvedName = reader.ReadString("okved_name"),

                SurCode = reader.ReadNullableInt("SUR_CODE")
            };

            return obj;
        }

        public PageResult<BankAccount> GetULBankAccounts(QueryConditions criteria)
        {
            BankAccountQueryConditionRemapping(criteria);

            var query = criteria.ToSQL("SELECT vw.* FROM V$BSSCHET_UL vw WHERE 1=1 {0} {1}", "vw", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor("pCursor"));

            var data = ExecuteList(
                string.Format("BEGIN OPEN :pCursor FOR {0}; END;", query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildULBankAccount);

            query = criteria.ToSQL("BEGIN SELECT COUNT(*) INTO :pResult FROM V$BSSCHET_UL vw WHERE 1=1 {0}; END;", "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = "pResult" });

            var result = Execute(query);

            return new PageResult<BankAccount>(
                data,
                DataReaderExtension.ReadInt(result.Output["pResult"]));
        }

        private BankAccount BuildULBankAccount(OracleDataReader reader)
        {
            var obj = new BankAccount
            {
                BIK = reader.ReadString("BIK"),
                BankInn = reader.ReadString("INNKO"),
                BankKpp = reader.ReadString("KPPKO"),
                RegistrationBankNumber = reader.ReadString("RNKO"),
                BankBrunhNumber = reader.ReadString("NFKO"),
                BankName = reader.ReadString("NAMEKO"),
                AccountNumber = reader.ReadString("NOMSCH"),
                AccountValute = reader.ReadString("PRVALTEXT"),
                Inn = reader.ReadString("INN"),
                Kpp = reader.ReadString("KPP"),
                OpenDate = reader.ReadDateTime("DATEOPENSCH"),
                CloseDate = reader.ReadDateTime("DATECLOSESCH"),
                AccountType = reader.ReadString("VIDSCH")
            };

            return obj;

        }

        public PageResult<BankAccount> GetFLBankAccounts(QueryConditions criteria)
        {
            BankAccountQueryConditionRemapping(criteria);

            var query = criteria.ToSQL("SELECT vw.* FROM V$BSSCHET_IP vw WHERE 1=1 {0} {1}", "vw", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor("pCursor"));

            var data = ExecuteList(
                string.Format("BEGIN OPEN :pCursor FOR {0}; END;", query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildFLBankAccount);

            query = criteria.ToSQL("BEGIN SELECT COUNT(*) INTO :pResult FROM V$BSSCHET_IP vw WHERE 1=1 {0}; END;", "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = "pResult" });

            var result = Execute(query);

            return new PageResult<BankAccount>(
                data,
                DataReaderExtension.ReadInt(result.Output["pResult"]));
        }

        public List<DeclarationPeriod> GetAvailablePeriods(string inn, string kpp)
        {
            return new ListCommandExecuter<DeclarationPeriod>(
                new GenericDataMapper<DeclarationPeriod>(),
                _service, _connectionFactory)
                .TryExecute(new GetAvailablePeriodsCommand(inn, kpp)).ToList();
        }

        private BankAccount BuildFLBankAccount(OracleDataReader reader)
        {
            var obj = new BankAccount
            {
                BIK = reader.ReadString("BIK"),
                BankInn = reader.ReadString("INNKO"),
                BankKpp = reader.ReadString("KPPKO"),
                RegistrationBankNumber = reader.ReadString("RNKO"),
                BankBrunhNumber = reader.ReadString("NFKO"),
                BankName = reader.ReadString("NAMEKO"),
                AccountNumber = reader.ReadString("NOMSCH"),
                AccountValute = reader.ReadString("PRVALTEXT"),
                Inn = reader.ReadString("INN"),
                Kpp = string.Empty,
                OpenDate = reader.ReadDateTime("DATEOPENSCH"),
                CloseDate = reader.ReadDateTime("DATECLOSESCH"),
                AccountType = reader.ReadString("VIDSCH")
            };

            return obj;
        }

        public PageResult<BankAccountRequestSummary> GetBankAccountsRequest(QueryConditions criteria)
        {
            var filterBy = criteria.Filter.ToFilterGroup();
            var skip = (int)criteria.PaginationDetails.RowsToSkip;
            var take = (int?)criteria.PaginationDetails.RowsToTake;

            var result = new ListCommandExecuter<BankAccountRequestSummary>(new GenericDataMapper<BankAccountRequestSummary>(), _service, _connectionFactory)
                .TryExecute(new GetAccountRequestsCommand(filterBy, criteria.Sorting).Skip(skip).Take(take)).ToList();
            var count = new CountCommandExecuter(_service, _connectionFactory)
                .TryExecute(new CountAccountRequestsCommand(filterBy));
            return new PageResult<BankAccountRequestSummary>
            {
                Rows = result,
                TotalMatches = count
            };
        }

        public TaxPayerStatus GetTaxPayerSolvencyStatus(string inn, string kppEffective)
        {
            try
            {
                return
                    new ListCommandExecuter<TaxPayerStatus>(new GenericDataMapper<TaxPayerStatus>(), _service,
                        _connectionFactory)
                        .TryExecute(new GetTaxPayerStatusCommand(inn, kppEffective)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region AccountBrief

        public PageResult<BankAccountBrief> GetFLBankAccountBriefs(string inn, DateTime begin, DateTime end, QueryConditions criteria)
        {
            try
            {
                var result = new ListCommandExecuter<BankAccountBrief>(new GenericDataMapper<BankAccountBrief>(), _service, _connectionFactory)
                    .TryExecute(new GetFLAccountsCommand(inn, begin, end, criteria)).ToList();
                return new PageResult<BankAccountBrief>
                {
                    Rows = result
                };
            }
            catch (Exception)
            {
                return new PageResult<BankAccountBrief>
                {
                    Rows = new List<BankAccountBrief>()
                };
            }
        }

        public PageResult<BankAccountBrief> GetULBankAccountBriefs(string inn, string kpp, DateTime begin, DateTime end, QueryConditions criteria)
        {
            try
            {
                var result = new ListCommandExecuter<BankAccountBrief>(new GenericDataMapper<BankAccountBrief>(), _service, _connectionFactory)
                    .TryExecute(new GetULAccountsCommand(inn, kpp, begin, end, criteria)).ToList();
                return new PageResult<BankAccountBrief>
                {
                    Rows = result
                };
            }
            catch (Exception)
            {
                return new PageResult<BankAccountBrief>
                {
                    Rows = new List<BankAccountBrief>()
                };
            }
        }

        #endregion

        private readonly Dictionary<string, string> _mapping =
            new Dictionary<string, string>
            {
                { "BankInn", "INNKO" },
                { "BankKpp", "KPPKO" },
                { "BankName", "NAMEKO" },
                { "RegistrationBankNumber", "RNKO" },
                { "BankBrunhNumber", "NFKO" },
                { "AccountNumber", "NOMSCH" },
                { "AccountValute", "PRVALTEXT" },
                { "OpenDate", "DATEOPENSCH" },
                { "CloseDate", "DATECLOSESCH" },
                { "AccountType", "VIDSCH" }
            };

        private void BankAccountQueryConditionRemapping(QueryConditions criteria)
        {
            foreach (var sort in criteria.Sorting)
            {
                if (_mapping.ContainsKey(sort.ColumnKey))
                {
                    sort.ColumnKey = _mapping[sort.ColumnKey];
                }
            }

            foreach (var filter in criteria.Filter)
            {
                if (_mapping.ContainsKey(filter.ColumnName))
                {
                    filter.ColumnName = _mapping[filter.ColumnName];
                }
            }
        }
    }
}
