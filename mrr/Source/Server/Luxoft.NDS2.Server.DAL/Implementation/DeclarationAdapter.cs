﻿using System;
using System.Collections.Generic;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс ISelectionDeclarationAdapater
    /// </summary>

    [Obsolete]
    internal class DeclarationAdapter : BaseOracleTableAdapter, IDeclarationAdapter
    {
        # region Константы

        private const string PatternScope = "Declaration";
        private const string PatternSearchLastFullLodedDeclaration = "SearchLastFullLodedDeclaration";

        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";


        private const string SearchListSQLPattern2 = @"select vw.*, dist.DESCRIPTION as federal_region from (
        select rownum as rowid1, T1.* from (
            select
                 vw.nds2_decl_corr_id,
                 vw.decl_type_code 
            from V$DECLARATION_SEARCH vw WHERE 1=1 {0} {1}, vw.rowid asc
        ) T1 
        where rownum <= :rnNext ) T
        inner join V$DECLARATION vw on vw.nds2_decl_corr_id = T.nds2_decl_corr_id and vw.decl_type_code = T.decl_type_code {1}, T.rowid1 asc
        join NDS2_MRR_USER.FEDERAL_DISTRICT_REGION dist_reg on dist_reg.region_code = vw.region_code
        join NDS2_MRR_USER.FEDERAL_DISTRICT dist on dist.district_id = dist_reg.district_id";


        private const string SearchListSQLPattern = "SELECT vw.* FROM V$Declaration vw WHERE 1=1 {0} {1}";
        private const string CountSQLPattern = "BEGIN SELECT COUNT(1) INTO :pResult FROM V$DECLARATION_SEARCH vw WHERE 1=1 {0}; END;";
        private const string ViewAlias = "vw";
        //TODO: добавить идентификатор запроса выгрузки СФ
        private const string InvoiceSearchSQLPattern = @"SELECT * FROM V$INVOICE vw WHERE request_id = :pRequestId {0} {1}";

        private const string InvoiceCountSQLPattern = "BEGIN SELECT COUNT(*) INTO :pResult FROM ({0}); END;";

        private const string GetVersionsCommand = @"
select
  zip as declaration_version_id,
  decl.correction_number as correction_number,
  decl.correction_number_effective,
  decl.correction_number_rank,
  decl.sono_code as soun_code,
  decl.kpp,
  decl.submit_date as decl_date,
  decl.submission_date as submission_date,
  nvl2(sd.INN, 1, 0) as is_annulment_seod,
  nvl2(decl_anl.inn, 1, 0) as is_annulment
from 
  declaration_history decl
  left join (
     select inn, kpp, sono_code, fiscal_year, period_code, correction_number
     from nds2_seod.seod_declaration_annulment
     group by inn, kpp, sono_code, fiscal_year, period_code, correction_number
  ) sd on 
      decl.INN_DECLARANT = sd.INN 
      and decl.KPP = sd.KPP 
      and decl.SONO_CODE_SUBMITED = sd.SONO_CODE 
      and decl.FISCAL_YEAR = sd.FISCAL_YEAR 
      and decl.PERIOD_CODE = sd.PERIOD_CODE 
      and decl.CORRECTION_NUMBER = sd.CORRECTION_NUMBER
  left join (
    select sda.inn, sda.kpp, sda.sono_code, sda.fiscal_year, sda.period_code, sda.correction_number
    from
    (
      select annulment_id from declaration_annulment_history where status_id = 2
    ) f
    join declaration_annulment da on da.id = f.annulment_id
    join nds2_seod.seod_declaration_annulment sda on sda.id = da.seod_id
    group by sda.inn, sda.kpp, sda.sono_code, sda.fiscal_year, sda.period_code, sda.correction_number
  ) decl_anl on
      decl.INN_DECLARANT = decl_anl.INN and
      decl.KPP = decl_anl.KPP and
      decl.SONO_CODE_SUBMITED = decl_anl.SONO_CODE and
      decl.FISCAL_YEAR = decl_anl.FISCAL_YEAR and
      decl.PERIOD_CODE = decl_anl.PERIOD_CODE and
      decl.CORRECTION_NUMBER = decl_anl.CORRECTION_NUMBER
where decl.inn_declarant = :inn
  and decl.kpp_effective = :kpp
  and decl.period_effective = :periodEffective
  and decl.fiscal_year = :year
  and decl.type_code = :type
order by decl.submit_date desc, correction_number desc, correction_number_rank";

        private const string pDeclarationId = "pDeclarationId";

        private const string SearchInvoiceInnerChapter_9_10_12 = @"BEGIN
                    SELECT countChapter9, countChapter10, countChapter12 into :pCountChapter9, :pCountChapter10, :pCountChapter12 
                    FROM dual
                    left outer join 
                    (SELECT count(distinct case when vw.CHAPTER = 9 then vw.CHAPTER else 0 end) as countChapter9
                    ,count(distinct case when vw.CHAPTER = 10 then vw.CHAPTER else 0 end) as countChapter10
                    ,count(distinct case when vw.CHAPTER = 12 then vw.CHAPTER else 0 end) as countChapter12
                    FROM v$invoice vw WHERE vw.DECLARATION_VERSION_ID = :pDeclarationId 
                    AND (
                        (vw.SELLER_INN = :pInn AND vw.CHAPTER = 9) OR
                        (vw.SELLER_INN = :pInn AND vw.CHAPTER = 10) OR
                        (vw.SELLER_INN = :pInn AND vw.CHAPTER = 12)
                        ) 
                    GROUP BY vw.CHAPTER) r on 1 = 1; END;";

        private const string pInn = "pInn";
        private const string pCountChapter9 = "pCountChapter9";
        private const string pCountChapter10 = "pCountChapter10";
        private const string pCountChapter12 = "pCountChapter12";

        private const string SearchInvoiceInnerChapter_8_11 = @"BEGIN 
                    SELECT countChapter8, countChapter11 into :pCountChapter8, :pCountChapter11
                    FROM dual
                    left outer join 
                    (SELECT 
                    count(distinct case when vw.CHAPTER = 8 then vw.CHAPTER else 0 end) as countChapter8
                    ,count(distinct case when vw.CHAPTER = 11 then vw.CHAPTER else 0 end) as countChapter11
                    FROM v$invoice vw WHERE vw.DECLARATION_VERSION_ID = :pDeclarationId 
                    AND (
                        (vw.BUYER_INN = :pInn AND vw.CHAPTER = 8) OR
                        (vw.BUYER_INN = :pInn AND vw.CHAPTER = 11) 
                        ) 
                    GROUP BY vw.CHAPTER) r on 1 = 1; END;";
        private const string pCountChapter8 = "pCountChapter8";
        private const string pCountChapter11 = "pCountChapter11";

        const string SearchLastDeclaraionSql = @"select * from v$declaration_history vd where IS_ACTIVE = 1 and inn = :pInn";

        # endregion

        # region Конструкторы

        public DeclarationAdapter(IServiceProvider service) : base(service) { }

        # endregion

        # region Реализация интерфейса ISelectionDeclarationsAdapter

        public List<DeclarationVersion> GetVersions(string innDeclarant, string kppEffective, int periodEffective, string year, int typeCode)
        {
            List<DeclarationVersion> result = new List<DeclarationVersion>();

            ExecuteList<int>(GetVersionsCommand, CommandType.Text,
                new []
                {
                    FormatCommandHelper.VarcharIn(":inn", innDeclarant),
                    FormatCommandHelper.VarcharIn(":kpp", kppEffective),
                    FormatCommandHelper.NumericIn(":periodEffective", periodEffective),
                    FormatCommandHelper.VarcharIn(":year", year),
                    FormatCommandHelper.NumericIn(":type", typeCode)
                },
                reader =>
                {
                    var version = new DeclarationVersion();
                    version.DeclarationVersionId = reader.ReadInt64("declaration_version_id");
                    version.CorrectionNumber = reader.ReadString("correction_number");
                    version.CorrectionNumberEffective = reader.ReadString("correction_number_effective");
                    version.CorrectionNumberRank = reader.ReadInt("correction_number_rank");
                    version.SounCode = reader.ReadString("soun_code");
                    version.Kpp = reader.ReadString("kpp");
                    version.DeclDate = reader.ReadDateTime("decl_date");
                    version.SubmissionDate = reader.ReadDateTime("submission_date");
                    version.IsAnnulmentSeod = reader.ReadBoolean("is_annulment_seod");
                    version.IsAnnulment = reader.ReadBoolean("is_annulment");
                    version.BuildCaption();
                    result.Add(version);
                    return 0;
                });

            return result;
        }

        public DeclarationSummary SearchLastDeclaration(string inn, string kpp = null)
        {
            var data = ExecuteList<DeclarationSummary>(
                string.Format(SearchSQLCommandPattern, SearchLastDeclaraionSql),
                CommandType.Text,
                new OracleParameter[] { FormatCommandHelper.VarcharIn(":pInn", inn), FormatCommandHelper.Cursor(CursorAlias) },
                BuildDeclaration);
            return data.Single();
        }

        public DeclarationSummary SearchLastFullLoadedCorrection(string innContractor, string kppEffective, string period, string year, int typeCode)
        {
            var patternText = _service.GetQueryText(PatternScope, PatternSearchLastFullLodedDeclaration);
            
            var data = ExecuteList(
                string.Format(SearchSQLCommandPattern, patternText),
                CommandType.Text,
                new[] 
                { 
                    FormatCommandHelper.VarcharIn(":pInn", innContractor),
                    FormatCommandHelper.VarcharIn(":pKpp", kppEffective),
                    FormatCommandHelper.VarcharIn(":pPeriod", period),
                    FormatCommandHelper.VarcharIn(":pYear", year),
                    FormatCommandHelper.NumericIn(":pType", typeCode),
                    FormatCommandHelper.Cursor(CursorAlias) 
                },
                BuildDeclaration);
            return data.SingleOrDefault();
        }

        private void SetSortingDefault(QueryConditions criteria)
        {
            if (criteria.Sorting == null)
                criteria.Sorting = new List<ColumnSort>();
            if (criteria.Sorting.Count == 0)
                criteria.Sorting.Add(new ColumnSort()
                {
                    ColumnKey = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SUBMISSION_DATE),
                    Order = ColumnSort.SortOrder.Asc
                });
        }

        public PageResult<DeclarationSummary> Search(QueryConditions criteria)
        {
            SetSortingDefault(criteria);

            var query = criteria.ToSQL(SearchListSQLPattern2, ViewAlias, false, true);
            var numOfRows = 0;
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();

            var rowFrom = (int)(criteria.PageIndex - 1) * criteria.PageSize;
            var rowTo = (int)criteria.PageIndex * criteria.PageSize;

            parameters.Add(new OracleParameter("rnNext", OracleDbType.Int32, 0, rowTo, ParameterDirection.Input));

            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            Stopwatch sw1 = new Stopwatch();
            sw1.Start();

            var execSettings = ExecuteSettings.Default;
            execSettings.ExplicitSkipOptions.From = rowFrom;

            var data = ExecuteList<DeclarationSummary>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDeclarationWithChapterPresentDescription, execSettings);

            sw1.Stop();
            Console.WriteLine("Query time {0}", sw1.ElapsedMilliseconds);


            query = criteria.ToSQL(CountSQLPattern, ViewAlias, false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = CountResult });

            var cachedResult = base.GetQueryListRowCountCache(QueryListScope.AnalyticDeclarationList, query.Parameters, query.Text);

            if (cachedResult.Exists)
            {
                numOfRows = Convert.ToInt32(cachedResult.RowCount);
                Console.WriteLine("count cached");
            }
            else
            {
                Stopwatch sw = new Stopwatch();

                sw.Start();
                var result = Execute(query);
                sw.Stop();
                Console.WriteLine("count - {0}", sw.ElapsedMilliseconds);
                numOfRows = DataReaderExtension.ReadInt(result.Output[CountResult]);
                base.SetQueryListRowCountCache(QueryListScope.AnalyticDeclarationList, numOfRows, sw.ElapsedMilliseconds, cachedResult.ArgsHashInfo);
            }

            return new PageResult<DeclarationSummary>(
                    data,
                    numOfRows);
        }

        private int GetCmdHashCode(string cmdText, IEnumerable<QueryParameter> paramenters)
        {
            var sb = new StringBuilder();
            sb.Append(cmdText);
            foreach (var oracleParameter in paramenters)
            {
                sb.Append(oracleParameter.Value);
            }

            return sb.ToString().GetHashCode();
        }

        public DeclarationSummary GetDeclaration(long zip)
        {
            return DeclarationPackageAdapterCreator.Create(_service).GetDeclaration(zip);
        }

        public DeclarationSummary GetActualDeclaration(long zip)
        {
            return DeclarationPackageAdapterCreator.Create(_service).GetActualDeclaration(zip);
        }

        public PageResult<Invoice> GetDeclarationInvoices(QueryConditions criteria, long requestId)
        {
            var query = criteria.ToSQL(InvoiceSearchSQLPattern, ViewAlias, true, true);
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn("pRequestId", requestId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoices = ExecuteList<Invoice>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadInvoice);

            int totalCount = 0;
            if (!criteria.PaginationDetails.SkipTotalMatches)
            {
                criteria.Sorting.Clear();
                query = criteria.ToSQL(InvoiceSearchSQLPattern, ViewAlias, false, true);
                query.Text = string.Format(InvoiceCountSQLPattern, query.Text);
                query.Parameters.Add(new QueryParameter(true) { Name = "pRequestId", Type = OracleDbType.Int64, Value = requestId });
                query.Parameters.Add(new QueryParameter(false) { Name = CountResult, Type = OracleDbType.Int32 });

                var result = Execute(query);
                totalCount = DataReaderExtension.ReadInt(result.Output[CountResult]);
            }

            return new PageResult<Invoice>(
                    invoices,
                    totalCount);
        }

        public DeclarationSummary Version(long declarationId)
        {
            var data = ExecuteList<DeclarationSummary>(
                string.Format(SearchSQLCommandPattern, string.Format(SearchListSQLPattern, " and declaration_version_id = :p1 ", null)),
                CommandType.Text,
                new OracleParameter[] { FormatCommandHelper.NumericIn(":p1", declarationId), FormatCommandHelper.Cursor(CursorAlias) },
                BuildDeclaration);
            return data.Single();
        }

        public ActKNP GetActKnp(long declarationZip)
        {
            var data = ExecuteList<ActKNP>(
                "PAC$DECLARATION.P$GET_ACT_KNP",
                CommandType.StoredProcedure,
                new [] { 
                    FormatCommandHelper.NumericIn("pZip", declarationZip),
                    FormatCommandHelper.Cursor(CursorAlias) },
                BuilActKNP);
            ActKNP ret = data.FirstOrDefault();
            return ret;
        }

        private ActKNP BuilActKNP(OracleDataReader reader)
        {
            var obj = new ActKNP();

            obj.KNP_ID = reader.ReadInt64(TypeHelper<ActKNP>.GetMemberName(t => t.KNP_ID));
            obj.Number = reader.ReadString("doc_num");
            obj.Date = reader.ReadDateTime("doc_data");
            obj.SumUnpaid = reader.ReadDecimal("sum_unpaid");
            obj.SumOverestimated = reader.ReadDecimal("sum_overestimated");
            obj.SumCompensation = reader.ReadDecimal("sum_compensation");
            obj.Penalty = reader.ReadDecimal("sum_penalty");

            return obj;
        }

        public Dictionary<int, int> GetChaptersSellers(long declarationId, string inn)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDeclarationId, declarationId));
            parameters.Add(FormatCommandHelper.VarcharIn(pInn, inn));
            parameters.Add(FormatCommandHelper.NumericOut(pCountChapter9));
            parameters.Add(FormatCommandHelper.NumericOut(pCountChapter10));
            parameters.Add(FormatCommandHelper.NumericOut(pCountChapter12));

            var result = Execute(
                SearchInvoiceInnerChapter_9_10_12,
                CommandType.Text,
                parameters.ToArray());

            Dictionary<int, int> rets = new Dictionary<int, int>();
            rets.Add(9, DataReaderExtension.ReadInt(result.Output[pCountChapter9]));
            rets.Add(10, DataReaderExtension.ReadInt(result.Output[pCountChapter10]));
            rets.Add(12, DataReaderExtension.ReadInt(result.Output[pCountChapter12]));

            return rets;
        }

        public Dictionary<int, int> GetChaptersBuyers(long declarationId, string inn)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDeclarationId, declarationId));
            parameters.Add(FormatCommandHelper.VarcharIn(pInn, inn));
            parameters.Add(FormatCommandHelper.NumericOut(pCountChapter8));
            parameters.Add(FormatCommandHelper.NumericOut(pCountChapter11));

            var result = Execute(
                SearchInvoiceInnerChapter_8_11,
                CommandType.Text,
                parameters.ToArray());

            Dictionary<int, int> rets = new Dictionary<int, int>();
            rets.Add(8, DataReaderExtension.ReadInt(result.Output[pCountChapter8]));
            rets.Add(11, DataReaderExtension.ReadInt(result.Output[pCountChapter11]));

            return rets;
        }

        public long GetActual(string inn, string kppEffective, string year, string period, int typeCode)
        {
            var result = Execute(
                @"
                begin
                select ZIP INTO :pResult from (
                  select v.ZIP 
                  from dual
                    left join declaration_history v on v.INN_CONTRACTOR = :1 and v.KPP_EFFECTIVE = F$GET_EFFECTIVE_KPP(:1, :4) and v.FISCAL_YEAR = :2 and v.PERIOD_CODE = :3
                  order by v.submit_date desc, v.correction_number desc)
                where ROWNUM = 1;
                end;",
                CommandType.Text,
                new OracleParameter[]
                {
                    FormatCommandHelper.VarcharIn(":1", inn),
                    FormatCommandHelper.VarcharIn(":2", year),
                    FormatCommandHelper.VarcharIn(":3", period),
                    FormatCommandHelper.VarcharIn(":4", kppEffective),
                    FormatCommandHelper.NumericOut(":pResult")
                });

            long ret = -1;

            if (result.Output.Count > 0)
            {
                var rez = result.Output[":pResult"];

                if (rez.ToString().ToLower() != "null")
                    ret = DataReaderExtension.ReadInt64(rez);
                else
                    throw new ObjectNotFoundException(String.Format("Декларация ({0}/{1}/{2}) - не найденна", inn, period, year));
            }

            return ret;
        }


        # endregion

        # region Потроение объекта Declaration

        private DeclarationSummary BuildDeclaration(OracleDataReader reader)
        {
            var obj = new DeclarationSummary();

            LoadDeclarationBaseParams(reader, obj);

            return obj;
        }

        private DeclarationSummary BuildDeclarationWithChapterPresentDescription(OracleDataReader reader)
        {
            var obj = new DeclarationSummary();

            LoadDeclarationBaseParams(reader, obj);

            obj.CH8_PRESENT_DESCRIPTION =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_PRESENT_DESCRIPTION));
            obj.CH9_PRESENT_DESCRIPTION =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_PRESENT_DESCRIPTION));

            return obj;
        }


        private void LoadDeclarationBaseParams(OracleDataReader reader, DeclarationSummary obj)
        {
            obj.ID = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ID));
            obj.DECLARATION_VERSION_ID =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECLARATION_VERSION_ID));
            obj.ProcessingStage =
                (DeclarationProcessignStage)
                reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ProcessingStage));
            obj.SummaryState = (DeclarationSummaryState)reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SummaryState));
            obj.LOAD_MARK =
                (DeclarationProcessignStage) reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.LOAD_MARK));
            obj.SEOD_DECL_ID = reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SEOD_DECL_ID));
            obj.ASK_DECL_ID = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ASK_DECL_ID));
            obj.IS_ACTUAL = reader.ReadBoolean("IS_ACTIVE");
            obj.INN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN));
            obj.INN_CONTRACTOR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN_CONTRACTOR));
            obj.KPP = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP));
            obj.KPP_EFFECTIVE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP_EFFECTIVE));
            obj.NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NAME));
            obj.ADDRESS1 = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS1));
            obj.ADDRESS2 = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS2));

            obj.CATEGORY_RU = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY_RU));
            obj.REGION_NAME = (obj.CATEGORY == 1)
                                  ? ""
                                  : reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REGION_NAME));

            obj.REGION_ENTRY = (obj.CATEGORY == 1)
                                   ? new CodeValueDictionaryEntry("", "")
                                   : new CodeValueDictionaryEntry(reader.ReadString("REGION_CODE"),
                                                                  reader.ReadString("REGION_NAME"));
            obj.REGION_CODE = (obj.REGION_ENTRY != null) ? obj.REGION_ENTRY.EntryId : String.Empty;

            obj.SOUN_NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN_NAME));
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(reader.ReadString("SOUN_CODE"), reader.ReadString("SOUN_NAME"));
            obj.SOUN_CODE = (obj.SOUN_ENTRY != null) ? obj.SOUN_ENTRY.EntryId : String.Empty;
            obj.SOUN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN));
            obj.REG_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REG_DATE));
            obj.TAX_MODE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_MODE));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.OKVED_CODE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.OKVED_CODE));
            obj.CAPITAL = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CAPITAL));
            obj.TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.FISCAL_YEAR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            obj.FullTaxPeriod = new FullTaxPeriodInfo()
                                    {
                                        TaxPeriod = obj.TAX_PERIOD,
                                        FiscalYear = obj.FISCAL_YEAR,
                                        Description = obj.FULL_TAX_PERIOD
                                    };
            obj.PeriodEffective = reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PeriodEffective));
            obj.DECL_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_DATE));
            obj.SUBMISSION_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBMISSION_DATE));
            obj.DECL_TYPE_CODE = reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE_CODE));
            obj.DECL_SIGN = "";
            if (obj.DECL_TYPE_CODE == 0)
                obj.DECL_SIGN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_SIGN));
            obj.DECL_TYPE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE));
            obj.HAS_CANCELLED_CORRECTION = reader.ReadBoolean(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HAS_CANCELLED_CORRECTION));
            obj.CORRECTION_NUMBER = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER));
            obj.CORRECTION_NUMBER_EFFECTIVE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_EFFECTIVE));
            obj.CORRECTION_NUMBER_RANK =
                reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_RANK));
            obj.SUBSCRIBER_NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBSCRIBER_NAME));
            obj.PRPODP = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRPODP));
            obj.COMPENSATION_AMNT =
                reader.ReadNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT));
            obj.COMPENSATION_AMNT_SIGN =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT_SIGN));
            obj.CH8_DEALS_AMNT_TOTAL =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_DEALS_AMNT_TOTAL));
            obj.CH9_DEALS_AMNT_TOTAL =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_DEALS_AMNT_TOTAL));
            obj.CH8_NDS = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_NDS));
            obj.CH9_NDS = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_NDS));
            obj.NDS_WEIGHT = reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_WEIGHT));
            obj.DISCREP_CURRENCY_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_AMNT));
            obj.DISCREP_CURRENCY_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_COUNT));
            obj.WEAK_DISCREP_COUNT = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_COUNT));
            obj.WEAK_DISCREP_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_AMNT));
            obj.GAP_DISCREP_COUNT = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_COUNT));
            obj.GAP_DISCREP_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_AMNT));
            obj.NDS_INCREASE_DISCREP_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_COUNT));
            obj.NDS_INCREASE_DISCREP_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_AMNT));
            obj.DISCREP_MIN_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MIN_AMNT));
            obj.DISCREP_MAX_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MAX_AMNT));
            obj.DISCREP_AVG_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_AVG_AMNT));
            obj.DISCREP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_TOTAL_AMNT));
            obj.UPDATE_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.UPDATE_DATE));
            obj.INSPECTOR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INSPECTOR));
            obj.SUR_CODE = (obj.DECL_TYPE_CODE == 0)
                               ? reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUR_CODE))
                               : null;
            obj.CATEGORY = null;
            if (obj.DECL_TYPE_CODE == 0)
            {
                if (reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY)) == 1)
                {
                    obj.CATEGORY = 1;
                }
            }
            obj.CONTROL_RATIO_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DATE));
            obj.CONTROL_RATIO_SEND_TO_SEOD =
                reader.ReadBoolean(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_SEND_TO_SEOD));
            obj.CONTROL_RATIO_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_COUNT));
            obj.TOTAL_DISCREP_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TOTAL_DISCREP_COUNT));
            obj.KNP_DISCREP_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KNP_DISCREP_COUNT));
            obj.CONTROL_RATIO_DISCREP_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DISCREP_COUNT));

            obj.STATUS = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.STATUS));

            //--- Begin Сводные данные по разделу 8 и разделу 8.1
            obj.SumNDSPok_8 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_8));
            obj.SumNDSPok_81 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_81));
            obj.SumNDSPokDL_81 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPokDL_81));
            //--- End

            //--- Begin Сводные данные по разделу 9 и разделу 9.1
            obj.StProd18_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_9));
            obj.StProd10_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_9));
            obj.StProd0_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_9));
            obj.SumNDSProd18_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_9));
            obj.SumNDSProd10_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_9));
            obj.StProdOsv_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_9));
            obj.StProd18_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_91));
            obj.StProd10_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_91));
            obj.StProd0_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_91));
            obj.SumNDSProd18_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_91));
            obj.SumNDSProd10_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_91));
            obj.StProdOsv_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_91));
            obj.StProd18DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18DL_91));
            obj.StProd10DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10DL_91));
            obj.StProd0DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0DL_91));
            obj.SumNDSProd18DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18DL_91));
            obj.SumNDSProd10DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10DL_91));
            obj.StProdOsvDL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsvDL_91));
            //--- End

            obj.AKT_NOMKORR_8 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_8));
            obj.AKT_NOMKORR_81 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_81));
            obj.AKT_NOMKORR_9 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_9));
            obj.AKT_NOMKORR_91 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_91));
            obj.AKT_NOMKORR_10 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_10));
            obj.AKT_NOMKORR_11 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_11));
            obj.AKT_NOMKORR_12 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_12));

            obj.StProd = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd));

            obj.DISCREP_BUY_BOOK_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_BUY_BOOK_AMNT));
            obj.DISCREP_SELL_BOOK_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_SELL_BOOK_AMNT));

            obj.PVP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_TOTAL_AMNT));
            obj.PVP_BUY_BOOK_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_BUY_BOOK_AMNT));
            obj.PVP_SELL_BOOK_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_SELL_BOOK_AMNT));
            obj.PVP_DISCREP_MIN_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MIN_AMNT));
            obj.PVP_DISCREP_MAX_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MAX_AMNT));
            obj.PVP_DISCREP_AVG_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_AVG_AMNT));

            obj.DateCloseKNP = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DateCloseKNP));

            obj.INVOICE_COUNT8 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT8));
            obj.INVOICE_COUNT81 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT81));
            obj.INVOICE_COUNT9 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT9));
            obj.INVOICE_COUNT91 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT91));
            obj.INVOICE_COUNT10 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT10));
            obj.INVOICE_COUNT11 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT11));
            obj.INVOICE_COUNT12 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT12));

            obj.KnpClosedReasonId = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KnpClosedReasonId));
        }

        # endregion
        
        #region Priority

        private const string SelectPriority =
            @"begin open :pCursor for select m.*
                ,rp08.priority as priority_08 
                ,rp09.priority as priority_09 
                ,rp10.priority as priority_10 
                ,rp11.priority as priority_11 
                ,rp12.priority as priority_12 
            from (
                select ZIP as ZIP
                ,nvl(SUM(case when TIPFAJLA = 0 or TIPFAJLA = 2 then nvl(KolZapisey, 0) end), 0) as countInv_08 
                ,nvl(SUM(case when TIPFAJLA = 1 or TIPFAJLA = 3 then nvl(KolZapisey, 0) end), 0) as countInv_09 
                ,nvl(SUM(case when TIPFAJLA = 4 then nvl(KolZapisey, 0) end), 0) as countInv_10 
                ,nvl(SUM(case when TIPFAJLA = 5 then nvl(KolZapisey, 0) end), 0) as countInv_11 
                ,nvl(SUM(case when TIPFAJLA = 6 then nvl(KolZapisey, 0) end), 0) as countInv_12 
                from v$asksvodzap vz where ZIP = :pZip group by ZIP) m
            left outer join BOOK_DATA_REQUEST_PRIORITY rp08 on countInv_08 >= rp08.min_count and 
                ((rp08.max_count is not null and countInv_08 <= rp08.max_count) or (rp08.max_count is null))
            left outer join BOOK_DATA_REQUEST_PRIORITY rp09 on countInv_09 >= rp09.min_count and 
                ((rp09.max_count is not null and countInv_09 <= rp09.max_count) or (rp09.max_count is null))
            left outer join BOOK_DATA_REQUEST_PRIORITY rp10 on countInv_10 >= rp10.min_count and 
                ((rp10.max_count is not null and countInv_10 <= rp10.max_count) or (rp10.max_count is null))
            left outer join BOOK_DATA_REQUEST_PRIORITY rp11 on countInv_11 >= rp11.min_count and 
                ((rp11.max_count is not null and countInv_11 <= rp11.max_count) or (rp11.max_count is null))
            left outer join BOOK_DATA_REQUEST_PRIORITY rp12 on countInv_12 >= rp12.min_count and 
                ((rp12.max_count is not null and countInv_12 <= rp12.max_count) or (rp12.max_count is null))
            where m.ZIP = :pZip; end;";

        public int? GetPriority(long declarationId, int chapter)
        {
            return ExecuteList(
                SelectPriority,
                CommandType.Text,
                new[] { FormatCommandHelper.Cursor("pCursor"), FormatCommandHelper.NumericIn("pZip", declarationId) },
                (reader) => ReadPriority(reader, chapter)).FirstOrDefault();
        }

        private int? ReadPriority(OracleDataReader reader, int chapter)
        {
            //--- для журналов приоритеты брать как для 10 и 11 разделов
            if (chapter == 13) chapter = 10;
            else if (chapter == 14) chapter = 11;

            var priorityPostfix = chapter.ToString().PadLeft(2, '0');

            return reader.ReadNullableInt(string.Format("priority_{0}", priorityPostfix));
        } 

        #endregion
    }

}