﻿using System.Diagnostics;
using System.Reflection;
using System.Text;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс представляет абстрактный адаптер слоя доступа к данным
    /// </summary>
    public abstract class BaseOracleTableAdapter
    {
        # region Поля

        protected readonly IServiceProvider _service;

        # endregion

        # region Конструктор

        protected BaseOracleTableAdapter(IServiceProvider service)
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            _service = service;
        }

        # endregion

        # region Вспомогательные методы

        /// <summary>
        /// Создает экземпляр соединения с БД
        /// </summary>
        /// <returns>Ссылка на соединение с БД</returns>
        protected OracleConnection Connection()
        {
            return new OracleConnection(_service.GetConfigurationValue(Constants.DB_CONFIG_KEY));
        }

        /// <summary>
        /// Выполняет запрос к БД
        /// <param name="commandText">Текст команды к БД</param>
        /// <param name="commandType">Тип команды</param>
        /// <param name="parameters">Параметры запроса</param>
        /// </summary>
        protected CommandExecuteResult Execute(string commandText, CommandType commandType, IEnumerable<OracleParameter> parameters)
        {
            var retval = new CommandExecuteResult();
            var traceInfo = new List<KeyValuePair<string, object>>();

            traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, commandText));

            Stopwatch sw = new Stopwatch();

            using (var connection = Connection())
            {
                using (var command = new OracleCommand(commandText, connection))
                {
                    command.CommandType = commandType;
                    command.BindByName = true;

                    foreach (var p in parameters)
                    {
                        command.Parameters.Add(p);
                        traceInfo.Add(new KeyValuePair<string, object>(p.ParameterName, p.Value));
                    }

                    try
                    {
                        sw.Start();
                        connection.Open();
                        
                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CONN_OPEN_TIME, sw.ElapsedMilliseconds));

                        sw.Restart();
                        retval.AffectedRows = command.ExecuteNonQuery();

                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME, sw.ElapsedMilliseconds));
                       
                        foreach (OracleParameter parameter in command.Parameters)
                        {
                            if (parameter.Direction == ParameterDirection.Output
                                || parameter.Direction == ParameterDirection.InputOutput
                                || parameter.Direction == ParameterDirection.ReturnValue)
                            {
                                retval.Output.Add(
                                        parameter.ParameterName, 
                                        parameter.Value);
                            }
                        }

                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                    }
                    catch (OracleException oraEx)
                    {
                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx);
                        throw BuildException(oraEx);
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }

            return retval;
        }

        protected CommandExecuteResult Execute(Query query, CommandType commandType = CommandType.Text)
        {
            return Execute(
                query.Text, 
                commandType, 
                query.Parameters.Select(prm => prm.Convert()).ToArray());
        }

        protected object ExecuteScalar(CommandContext cmd)
        {
            using (var connection = Connection())
            {
                using (var command = new OracleCommand(cmd.Text, connection))
                {

                    try
                    {
                        connection.Open();

                        return  command.ExecuteScalar();

                    }
                    catch (OracleException oraEx)
                    {
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx);
                        throw BuildException(oraEx);
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected CommandExecuteResult Execute(CommandContext command)
        {
            return Execute(command.Text, command.Type, command.Parameters);
        }

        public virtual CommandExecuteResult Transaction(CommandContext[] commands)
        {
            var retval = new CommandExecuteResult();
            
            using (var connection = Connection())
            {
                try
                {
                    connection.Open();
                }
                catch (OracleException oraEx)
                {
                    _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx);
                    throw new DefaultErrorHandler().Handle(oraEx);
                }

                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var context in commands)
                    {
                        

                        using (var command = new OracleCommand(context.Text, connection))
                        {
                            var traceInfo = new List<KeyValuePair<string, object>>();

                            Stopwatch sw = new Stopwatch();

                            command.CommandType = context.Type;
                            command.BindByName = true;
                            command.Parameters.AddRange(context.Parameters.ToArray());
                            traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, command.CommandText));

                            try
                            {
                                sw.Start();
                                var result = command.ExecuteNonQuery();

                                sw.Stop();
                                traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME, sw.ElapsedMilliseconds));

                                retval.AffectedRows = result > retval.AffectedRows ? result : retval.AffectedRows;

                                foreach (OracleParameter param in command.Parameters)
                                {
                                    if ((param.Direction == ParameterDirection.Output
                                        || param.Direction == ParameterDirection.ReturnValue)
                                        && !retval.Output.ContainsKey(param.ParameterName))
                                    {
                                        retval.Output.Add(param.ParameterName, param.Value);
                                    }

                                    traceInfo.Add(new KeyValuePair<string, object>(param.ParameterName, param.Value));
                                }

                                if (context.AfterExecuted != null)
                                {
                                    context.AfterExecuted(retval);
                                }

                                _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);                           
                            }
                            catch (OracleException oraEx)
                            {
                                _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                                _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx, traceInfo);
                                transaction.Rollback();
                                connection.Close();
                                throw BuildException(oraEx);
                            }
                            catch (Exception ex)
                            {
                                _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                                _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, ex, traceInfo);
                                transaction.Rollback();
                                connection.Close();
                                throw;
                            }
                        }
                    }


                    transaction.Commit();
                    connection.Close();
                }
            }

            return retval;
        }

        /// <summary>
        /// Выполняет запрос к БД на получение курсора
        /// </summary>
        /// <param name="commandText">Текст команды</param>
        /// <param name="commandType">Тип команды</param>
        /// <param name="parameters">Коллекция параметров</param>
        /// <param name="objectBuilder">Конвертор из OracleDataReader строки в объект</param>
        /// <param name="settings">Доп настройки</param>
        /// <returns>Список объектов</returns>
        protected List<TDataObject> ExecuteList<TDataObject>(
            string commandText,
            CommandType commandType,
            OracleParameter[] parameters,
            Func<OracleDataReader, TDataObject> objectBuilder, ExecuteSettings settings = null)
        {
            var retval = new List<TDataObject>();

            var traceInfo = new List<KeyValuePair<string, object>>();

            traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, commandText));

            Stopwatch sw = new Stopwatch();

            foreach (var oracleParameter in parameters)
            {
                 traceInfo.Add(new KeyValuePair<string, object>(oracleParameter.ParameterName, oracleParameter.Value));
            }

            using (var connection = Connection())
            {
                using (var command = new OracleCommand(commandText, connection))
                {
                    command.CommandType = commandType;
                    command.BindByName = true;
                    command.Parameters.AddRange(parameters);
                    
                    try
                    {
                        sw.Start();
                        connection.Open();
                        
                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CONN_OPEN_TIME, sw.ElapsedMilliseconds));
                        
                        sw.Restart();
                        var reader = command.ExecuteReader();
                        /*var fieldInfo = reader.GetType().GetField("m_rowSize",
                                                                  BindingFlags.Instance | BindingFlags.NonPublic);
                        var rowSize = (long)fieldInfo.GetValue(reader);
                        reader.FetchSize = rowSize * 200;*/
                        
                        int skipCnt = 1;
                        while (reader.Read())
                        {
                            if (settings != null && (settings.ExplicitSkipOptions.IsSet && settings.ExplicitSkipOptions.From >= skipCnt))
                            {
                                skipCnt++;
                            }
                            else
                            {
                            retval.Add(objectBuilder(reader));
                        }
                        }

                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME, sw.ElapsedMilliseconds));

                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);                   
                    }
                    catch (OracleException oraEx)
                    {
                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx, traceInfo);

                        throw BuildException(oraEx);
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }

            return retval;
        }


        /// <summary>
        /// Выполняет запрос к БД на получение курсора
        /// </summary>
        /// <param name="commandText">Текст команды</param>
        /// <param name="commandType">Тип команды</param>
        /// <param name="parameters">Коллекция параметров</param>
        /// <param name="objectBuilder">Конвертор из OracleDataReader строки в объект</param>
        /// <returns>Список объектов</returns>
        protected List<TDataObject> ExecuteList<TDataObject>(
            string connectionKey,
            string commandText,
            CommandType commandType,
            OracleParameter[] parameters,
            Func<OracleDataReader, TDataObject> objectBuilder)
        {
            var retval = new List<TDataObject>();

            var traceInfo = new List<KeyValuePair<string, object>>();

            traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, commandText));

            Stopwatch sw = new Stopwatch();

            foreach (var oracleParameter in parameters)
            {
                traceInfo.Add(new KeyValuePair<string, object>(oracleParameter.ParameterName, oracleParameter.Value));
            }

            using (var connection = new OracleConnection(_service.GetConfigurationValue(connectionKey)))
            {
                using (var command = new OracleCommand(commandText, connection))
                {
                    command.CommandType = commandType;
                    command.BindByName = true;
                    
                    command.Parameters.AddRange(parameters);

                    try
                    {
                        sw.Start();
                        connection.Open();

                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CONN_OPEN_TIME, sw.ElapsedMilliseconds));

                        sw.Restart();
                        var reader = command.ExecuteReader();

                        sw.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME, sw.ElapsedMilliseconds));

                        while (reader.Read())
                        {
                            retval.Add(objectBuilder(reader));
                        }

                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                    }
                    catch (OracleException oraEx)
                    {
                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx, traceInfo);

                        throw BuildException(oraEx);
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }

            return retval;
        }

        protected CommandExecuter CreateExecuter()
        {
            return new CommandExecuter(_service);
        }

        protected virtual DatabaseException BuildException(OracleException oraEx)
        {
            return new DefaultErrorHandler().Handle(oraEx);
        }

        # endregion

        #region Кеширование кол-ва строк по списку

        protected QueryListRowCountCacheResult GetQueryListRowCountCache(QueryListScope scope, IEnumerable<QueryParameter> parameters, string queryText)
        {
            QueryListRowCountCacheResult result = new QueryListRowCountCacheResult(){Exists = false};
            using (var connection = Connection())
            {
                using (
                    OracleCommand cmd =
                        new OracleCommand(
                            "PAC$CACHE_MANAGER.F$GET_ROW_COUNT",
                            connection))
                {
                    connection.Open();

                    cmd.CommandType = CommandType.StoredProcedure;

                    var hashCode = this.GetQueryListArgsHashCode(parameters.ToArray(), queryText);
                    result.ArgsHashInfo = hashCode;

                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.ReturnValue, DbType = DbType.Int32, ParameterName = "retVal" });
                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int32, ParameterName = "p_scope_id", Value = (int)scope });
                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int32, ParameterName = "p_cache_id", Value = hashCode.HashCode });

                    cmd.ExecuteNonQuery();

                    var execResult = cmd.Parameters["retVal"].Value;

                    if (execResult != DBNull.Value)
                    {
                        result.Exists = true;
                        result.RowCount = Convert.ToInt32(execResult);
                    }

                    return result;
                }
            }
        }

        protected void SetQueryListRowCountCache(QueryListScope scope, int numRows, long  countElapsedMilisec,QueryParamHashInfo hashInfo)
        {
            using (var connection = Connection())
            {
                using (
                    OracleCommand cmd =
                        new OracleCommand(
                            @"PAC$CACHE_MANAGER.P$SET_ROW_COUNT",
                            connection))
                {
                    connection.Open();

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int32, ParameterName = "p_scope_id", Value = (int)scope });
                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int32, ParameterName = "p_cache_id", Value =  hashInfo.HashCode});
                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int32, ParameterName = "p_count", Value =  numRows});
                    cmd.Parameters.Add(new OracleParameter() { Direction = ParameterDirection.Input, DbType = DbType.Int64, ParameterName = "p_count_elpsd_ms", Value = countElapsedMilisec });
                    cmd.Parameters.Add(new OracleParameter("p_args", OracleDbType.Clob) { Direction = ParameterDirection.Input, Value = hashInfo.Args});

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private QueryParamHashInfo GetQueryListArgsHashCode(QueryParameter[] parameters, string queryText)
        {
            var sb = new StringBuilder(queryText);

            foreach (var paramName in parameters.Where(s=>s.Input).Select(p=>p.Name).Distinct())
            {
                foreach (var value in parameters.Where(p=>p.Name == paramName).Select(v=>v.Value == null ? "null" : v.Value.ToString()))
                {
                    sb.AppendFormat("{0},", value);
                }
            }

            return new QueryParamHashInfo()
            {
                Args = sb.ToString(),
                HashCode = sb.ToString().GetHashCode()
            };
        }

        #endregion
    }

    public enum QueryListScope
    {
        AnalyticDeclarationList = 1,
        InspectorDeclarationList = 2
    }

    public class QueryParamHashInfo
    {
        public int HashCode { get; set; }
        public string Args { get; set; }
    }

    public class QueryListRowCountCacheResult
    {
        public int RowCount { get; set; }
        public QueryParamHashInfo ArgsHashInfo { get; set; }
        public bool Exists { get; set; }
    }

    public class CommandExecuteResult
    {
        public int AffectedRows { get; set; }

        public Dictionary<string, object> Output { get; set; }

        public CommandExecuteResult()
        {
            Output = new Dictionary<string, object>();
        }
    }

    public class ExecuteSettings
    {        
        #region internals

        public class ExplicitSkipOptionsSettings
        {
            public bool IsSet
            {
                get { return From.HasValue; }
            }

            public Int32? From { get; set; }
        }

        #endregion internals

        public static ExecuteSettings Default { get { return new ExecuteSettings();} }

        ExecuteSettings()
        {
            this.ExplicitSkipOptions = new ExplicitSkipOptionsSettings();
        }

        public ExplicitSkipOptionsSettings ExplicitSkipOptions { get; private set; }
    }

    public class CommandContext
    {
        public CommandType Type { get; set; }

        public string Text { get; set; }

        //public IEnumerable<OracleParameter> Parameters { get; set; }
        public List<OracleParameter> Parameters { get; set; }

        public Action<CommandExecuteResult> AfterExecuted { get; set; }

        public CommandContext()
        {
            Type = CommandType.Text;
        }
    }
}
