﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует получение расхождений КНП для списка расхождений карточки НД
    /// </summary>
    internal class KnpDiscrepancyForDeclarationCardAdapter : ISearchAdapter<DeclarationDiscrepancy>
    {
        # region Константы

        private const string ViewAlias = "vw";
        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";
        private const string InnParam = "pInn";
        private const string KppEffectiveParam = "pKppEffective";
        private const string FiscalYearParam = "pFiscalYear";
        private const string PeriodEffectiveParam = "pPeriodEffective";

        # endregion
                
        protected readonly string _inn;
        protected readonly string _kppEffective;
        protected readonly string _fiscalYear;
        protected readonly int _periodEffective;
        
        private IServiceProvider _serviceProvider;

        public KnpDiscrepancyForDeclarationCardAdapter(IServiceProvider service, string inn, string kppEffective, string fiscalYear, int periodEffective)
        {
            _serviceProvider = service;
            _inn = inn;
            _kppEffective = kppEffective;
            _fiscalYear = fiscalYear;
            _periodEffective = periodEffective;
        }

        protected IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        private const string SearchDeclDiscrepancySQLPattern = "SELECT vw.* FROM V$DECL_KNP_DISCREPANCY_LIST vw WHERE vw.TAXPAYERINN = :pInn AND vw.KPPEFFECTIVE = :pKppEffective AND vw.FISCALYEAR = :pFiscalYear AND vw.PERIODEFFECTIVE = :pPeriodEffective AND {0} {1}";
        private const string CountDeclDiscrepancySQLPattern = "V$DECL_KNP_DISCREPANCY_LIST vw WHERE vw.TAXPAYERINN = :pInn AND vw.KPPEFFECTIVE = :pKppEffective AND vw.FISCALYEAR = :pFiscalYear AND vw.PERIODEFFECTIVE = :pPeriodEffective AND {0}";

        private class SearchCommandBuilder : PageSearchQueryCommandBuilder
        {
            private readonly string _inn;
            private readonly string _kppEffective;
            private readonly string _fiscalYear;
            private readonly int _periodEffective;

            public SearchCommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IEnumerable<ColumnSort> orderBy,
                IQueryPatternProvider queryPattern,
                string inn, 
                string kppEffective, 
                string fiscalYear, 
                int periodEffective)
                : base(selectStatementPattern, searchBy, orderBy, queryPattern)
            {
                _inn = inn;
                _kppEffective = kppEffective;
                _fiscalYear = fiscalYear;
                _periodEffective = periodEffective;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.VarcharIn(InnParam, _inn));
                parameters.Add(FormatCommandHelper.VarcharIn(KppEffectiveParam, _kppEffective));
                parameters.Add(FormatCommandHelper.VarcharIn(FiscalYearParam, _fiscalYear));
                parameters.Add(FormatCommandHelper.NumericIn(PeriodEffectiveParam, _periodEffective));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountCommandBuilder : GetCountCommandBuilder
        {
            private readonly string _inn;
            private readonly string _kppEffective;
            private readonly string _fiscalYear;
            private readonly int _periodEffective;

            public CountCommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IQueryPatternProvider queryPattern,
                string inn,
                string kppEffective,
                string fiscalYear,
                int periodEffective)
                : base(selectStatementPattern, searchBy, queryPattern)
            {
                _inn = inn;
                _kppEffective = kppEffective;
                _fiscalYear = fiscalYear;
                _periodEffective = periodEffective;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.VarcharIn(InnParam, _inn));
                parameters.Add(FormatCommandHelper.VarcharIn(KppEffectiveParam, _kppEffective));
                parameters.Add(FormatCommandHelper.VarcharIn(FiscalYearParam, _fiscalYear));
                parameters.Add(FormatCommandHelper.NumericIn(PeriodEffectiveParam, _periodEffective));
                return base.BuildSelectStatement(parameters);
            }
        }

        public IEnumerable<DeclarationDiscrepancy> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> sortBy, uint rowsToSkip, uint rowsToTake)
        {
            return new ListCommandExecuter<DeclarationDiscrepancy>(
                new GenericDataMapper<DeclarationDiscrepancy>(),
                _serviceProvider)
                .TryExecute(
                    new SearchCommandBuilder(SearchDeclDiscrepancySQLPattern,
                                                filterBy,
                                                sortBy,
                                                PatternProvider(ViewAlias),
                                                _inn,
                                                _kppEffective,
                                                _fiscalYear,
                                                _periodEffective)
                    .Take((int)rowsToTake)
                    .Skip((int)rowsToSkip)
                );
        }

        public int Count(FilterExpressionBase filterBy)
        {
            var result = new ResultCommandExecuter(_serviceProvider)
                .TryExecute(
                    new CountCommandBuilder(CountDeclDiscrepancySQLPattern,
                                            filterBy,
                                            PatternProvider(ViewAlias),
                                            _inn,
                                            _kppEffective,
                                            _fiscalYear,
                                            _periodEffective));

            return ((OracleDecimal)result[DbConstants.Result]).ToInt32();
        }

    }
}
