﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class TaxPeriodConfigurationAdapter : BaseOracleTableAdapter, ITaxPeriodConfigurationAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string SearchSQLCfgTaxPeriod= "select * from CFG_TAX_PERIOD";
        private const string pCursor = "pCursor";

        public TaxPeriodConfigurationAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public TaxPeriodConfiguration Search()
        {
            var query = string.Format(FormatCommandHelper.OraCommandPattern, string.Format(FormatCommandHelper.SelectIntoCursor + ";", pCursor, SearchSQLCfgTaxPeriod));
            var data = ExecuteList(query, CommandType.Text, new[] { FormatCommandHelper.Cursor(pCursor) }, ReadConfigTaxPeriod);
            return data.SingleOrDefault();
        }

        private TaxPeriodConfiguration ReadConfigTaxPeriod(OracleDataReader reader)
        {
            TaxPeriodConfiguration obj = new TaxPeriodConfiguration();
            obj.FiscalYearBegin = reader.ReadInt("FISCAL_YEAR_BEGIN");
            obj.TaxPeriodBegin = reader.ReadString("TAX_PERIOD_BEGIN");
            return obj;
        }
    }
}
