﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class DictionarySURAdapter : BaseOracleTableAdapter, IDictionarySURAdapter
    {
        private const string SELECT_SQL = @"select * from dict_sur";

        public DictionarySURAdapter(IServiceProvider service) : base(service) { }

        public List<SurCode> GetList()
        {
            Func<OracleDataReader, SurCode> getData = reader => new SurCode
            {
                Code = reader.ReadInt("CODE"),
                Description = reader.ReadString("DESCRIPTION"),
                ColorARGB = reader.ReadInt("COLOR_ARGB"),
                IsDefault = reader.ReadBoolean("IS_DEFAULT")
            };

            return ExecuteList(SELECT_SQL, CommandType.Text, new OracleParameter[0], getData);
        }
    }
}