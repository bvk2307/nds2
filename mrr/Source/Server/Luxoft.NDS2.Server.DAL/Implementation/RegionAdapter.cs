﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует адаптер справочника регионов
    /// </summary>
    internal class RegionAdapter : BaseOracleTableAdapter, IRegionAdapter
    {
        private const string SearchSqlCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";

        private ReportHelper _reportHelper = new ReportHelper();

        public RegionAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public List<DictionaryCommon> GetRegionsAvailable(string userSid)
        {
            return ExecuteList<DictionaryCommon>(
                FormatCommandHelper.PackageProcedure(DAL.Selections.DbConstants.SelectionPackageName, 
                DAL.Selections.DbConstants.GetRegionsProcedure),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParameterUserSid, userSid),
                    FormatCommandHelper.Cursor(DAL.Selections.DbConstants.CursorParameter)
                },
                (reader) =>
                    new DictionaryCommon()
                    {
                        S_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldCode),
                        S_NAME = reader.ReadString(DAL.Selections.DbConstants.FieldName),
                        DESCRIPTION = reader.ReadString(DAL.Selections.DbConstants.FieldDescription)
                    });
        }

        public List<DictionaryNalogOrgan> GetInspectionsAvailable(string userSid, DictionaryConditions criteria)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParameterUserSid, userSid));
            parameters.Add(FormatCommandHelper.Cursor(DAL.Selections.DbConstants.CursorParameter));

            List<DictionaryNalogOrgan> rets = ExecuteList<DictionaryNalogOrgan>(
                FormatCommandHelper.PackageProcedure(DAL.Selections.DbConstants.SelectionPackageName,
                DAL.Selections.DbConstants.GetInspectionsProcedure),
                CommandType.StoredProcedure,
                parameters.ToArray(),
                (reader) =>
                    new DictionaryNalogOrgan()
                    {
                        S_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldCode),
                        S_PARENT_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldParentCode),
                        S_NAME = reader.ReadString(DAL.Selections.DbConstants.FieldName)
                    });
            return rets;
        }

        public List<DictionaryCommon> GetRegionsAvailable(string userSid, string searchKey, int maxQuantity)
        {
            return ExecuteList<DictionaryCommon>(
                FormatCommandHelper.PackageProcedure(DAL.Selections.DbConstants.SelectionPackageName,
                DAL.Selections.DbConstants.SearchRegionsProcedure),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParameterUserSid, userSid),
                    FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParamSearchKey, "%" + searchKey + "%"),
                    FormatCommandHelper.NumericIn(DAL.Selections.DbConstants.ParamMaxQuantity, maxQuantity),
                    FormatCommandHelper.Cursor(DAL.Selections.DbConstants.CursorParameter)
                },
                (reader) =>
                    new DictionaryCommon()
                    {
                        S_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldCode),
                        S_NAME = reader.ReadString(DAL.Selections.DbConstants.FieldName),
                        DESCRIPTION = reader.ReadString(DAL.Selections.DbConstants.FieldDescription)
                    });
        }

        public List<DictionaryNalogOrgan> GetInspectionsAvailable(string userSid, string searchkey, int maxQuantity)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParameterUserSid, userSid));
            parameters.Add(FormatCommandHelper.VarcharIn(DAL.Selections.DbConstants.ParamSearchKey, "%" + searchkey + "%"));
            parameters.Add(FormatCommandHelper.NumericIn(DAL.Selections.DbConstants.ParamMaxQuantity, maxQuantity));
            parameters.Add(FormatCommandHelper.Cursor(DAL.Selections.DbConstants.CursorParameter));            

            List<DictionaryNalogOrgan> rets = ExecuteList<DictionaryNalogOrgan>(
                FormatCommandHelper.PackageProcedure(DAL.Selections.DbConstants.SelectionPackageName,
                DAL.Selections.DbConstants.SearchInspectionsProcedure),
                CommandType.StoredProcedure,
                parameters.ToArray(),
                (reader) =>
                    new DictionaryNalogOrgan()
                    {
                        S_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldCode),
                        S_PARENT_CODE = reader.ReadString(DAL.Selections.DbConstants.FieldParentCode),
                        S_NAME = reader.ReadString(DAL.Selections.DbConstants.FieldName)
                    });
            return rets;
        }

        public List<FederalDistrict> GetFederalDistricts()
        {
            return ExecuteList<FederalDistrict>(
                string.Format(SearchSqlCommandPattern, "SELECT * FROM FEDERAL_DISTRICT ORDER BY DESCRIPTION"),
                CommandType.Text,
                new OracleParameter[]
                {
                    FormatCommandHelper.Cursor(DAL.Selections.DbConstants.ParamCursor)
                },
                (reader) =>
                    new FederalDistrict()
                    {
                        Id = reader.ReadInt("district_id"),
                        Description = reader.ReadString("description")
                    });
        }

        public List<FederalDistrictToRegion> GetFederalDistrictToRegion()
        {
            return ExecuteList<FederalDistrictToRegion>(
                string.Format(SearchSqlCommandPattern, "SELECT * FROM FEDERAL_DISTRICT_REGION"),
                CommandType.Text,
                new OracleParameter[]
                {
                    FormatCommandHelper.Cursor(DAL.Selections.DbConstants.ParamCursor)
                },
                (reader) =>
                    new FederalDistrictToRegion()
                    {
                        DistrictId = reader.ReadInt("district_id"),
                        RegionCode = reader.ReadString("region_code")
                    });
        }
    }
}
