﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Server.DAL.Packages;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ConfigurationAutoselectionFilterTableAdapter : 
        BaseOracleTableAdapter, 
        IConfigurationAutoselectionFilterTableAdapter
    {
        private readonly MethodologistPackageAdapter _package;

        public ConfigurationAutoselectionFilterTableAdapter(
            IServiceProvider service)
            : base(service)
        {
            _package = new MethodologistPackageAdapter(service);
        }

        public List<AutoSelectionFilterConfiguration> All()
        {
            return _package.GetAutoselectionFilter();
        }

        public void UpdateAll(AutoSelectionFilterConfiguration data)
        {
            _package.SaveAutoselectionFilter(data.IncludeFilter, data.ExcludeFilter);
        }
    }
}
