﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class InvoiceExplainViewAdapter : BaseOracleTableAdapter, IInvoiceExplainViewAdapter
    {
        private const string Alias = "vw";

        private const string CountOutputParamName = "pResult";

        private const string CursorParamName = "pResult";

        private const string QueryPattern = "select * from V$INVOICE_EXPLAIN vw where 1=1 and {0}";

        public InvoiceExplainViewAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public List<InvoiceExplain> Search(FilterExpressionBase filter)
        {
            var query =
                filter.ToSQL(
                    QueryPattern,
                    new PatternProvider(Alias));

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorParamName));

            return ExecuteList(
                query.Text.WithCursor(CursorParamName),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadInvoiceExplainData);
        }
    }
}
