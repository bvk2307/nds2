﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    class LoadTestHelpAdapter : BaseOracleTableAdapter, ILoadTestHelpAdapter
    {
        private IServiceProvider service;

        public LoadTestHelpAdapter(IServiceProvider service)
            : base(service)
        {
            this.service = service;
        }

        #region Implementation of ILoadTestHelpAdapter

        public List<long> Get(string sonoCode, int freeChapterToLoad, int countOfData)
        {
            List<long> rez = ExecuteList<long>(
    @"select 
         dv.DECLARATION_VERSION_ID
from v$declaration dv
inner join v$askdekl d on dv.DECLARATION_VERSION_ID = d.ZIP
left join book_data_request bdr on bdr.inn = d.INNNP 
     and bdr.correctionnumber = d.NOMKORR 
     and bdr.period = d.PERIOD 
     and bdr.year = d.OTCHETGOD
    and bdr.partitionnumber = :2
where 
bdr.id is null and d.KODNO = :1  and rownum <= :3",
    CommandType.Text,
    new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(":1", sonoCode),
                    FormatCommandHelper.NumericIn(":2", freeChapterToLoad),
                    FormatCommandHelper.NumericIn(":3", countOfData)
                }, reader => long.Parse(reader[0].ToString()));

            return rez;
        }

        #endregion
    }
}
