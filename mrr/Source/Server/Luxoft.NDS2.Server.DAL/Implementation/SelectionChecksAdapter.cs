﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс ISelectionChecksAdapter
    /// </summary>
    internal class SelectionChecksAdapter : BaseOracleTableAdapter, ISelectionChecksAdapter
    {
        # region Константы

        private const string ExcludeDeclarationsSqlPattern = "BEGIN UPDATE {0}.MRR_DISCREPANCY_REF main SET ISCHECKED=0 WHERE main.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM {0}.DISCREPANCY t_disc JOIN V_DECLARATION v_decl ON v_decl.ID=t_disc.DECL_ID WHERE v_decl.REQUEST_ID=:pRequestId AND t_disc.DISCREPANCY_ID=main.DISCREPANCY_ID AND {1}); UPDATE {0}.MRR_DECLARATION_REF main SET ISCHECKED=0 WHERE main.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM V_DECLARATION v_decl WHERE v_decl.ID=main.DECLARATION_ID AND v_decl.REQUEST_ID=:pRequestId AND {1}); END;";
        private const string IncludeDeclarationsSqlPattern = "BEGIN UPDATE {0}.MRR_DISCREPANCY_REF main SET ISCHECKED=1 WHERE main.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM {0}.DISCREPANCY t_disc JOIN V_DECLARATION v_decl ON v_decl.ID=t_disc.DECL_ID WHERE v_decl.REQUEST_ID=:pRequestId AND t_disc.DISCREPANCY_ID=main.DISCREPANCY_ID AND v_decl.ISCHECKED = 0 AND {1}); UPDATE {0}.MRR_DECLARATION_REF main SET ISCHECKED=1 WHERE main.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM V_DECLARATION v_decl WHERE v_decl.ID=main.DECLARATION_ID AND v_decl.REQUEST_ID=:pRequestId AND {1}); END;";
        private const string GetDeclarationsGroupStateSqlPattern = "BEGIN SELECT SUM(v_decl.ISCHECKED),COUNT(v_decl.ID) INTO :pChecked, :pAll FROM V_DECLARATION v_decl WHERE v_decl.REQUEST_ID=:pRequestId AND {0}; END;";
        private const string ExcludeDiscrepanciesSqlPattern = "BEGIN UPDATE {0}.MRR_DISCREPANCY_REF t SET ISCHECKED=0 WHERE t.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM V_DISCREPANCY vw WHERE vw.REQUESTID=:pRequestId AND vw.DISCREPANCYID=t.DISCREPANCY_ID AND {1}); END;";
        private const string IncludeDiscrepanciesSqlPattern = "BEGIN UPDATE {0}.MRR_DISCREPANCY_REF t SET ISCHECKED=1 WHERE t.REQUEST_ID=:pRequestId AND EXISTS(SELECT 1 FROM V_DISCREPANCY vw WHERE vw.REQUESTID=:pRequestId AND vw.DISCREPANCYID=t.DISCREPANCY_ID AND {1}); END;";
        private const string GetDiscrepanciesGroupStateSqlPattern = "BEGIN SELECT SUM(vw.ISCHECKED), COUNT(vw.DISCREPANCYID) INTO :pChecked, :pAll FROM V_DISCREPANCY vw WHERE vw.REQUESTID=:pRequestId AND {0}; END;";
        private const string ViewDeclarationsAlias = "v_decl";
        private const string ViewDiscrepanciesAlias = "vw";
        private const string UpdateDeclarationCheckState = "UPDATE_DECLARATION_CHECK_STATE";
        private const string UpdateDiscrepancyCheckState = "UPDATE_DISCREPANCY_CHECK_STATE";
        private const string UpdateAllDeclarationsCheckState = "ALL_DECLARATIONS_CHECK_STATE";
        private const string UpdateAllDiscrepanciesCheckState = "ALL_DISCREPANCIES_CHECK_STATE";
        private const string CountCheckedDeclarations = "COUNT_CHECKED_DECLARATIONS";
        private const string CountCheckedDiscrepancies = "COUNT_CHECKED_DISCREPANCIES";
        private const string RequestId = "pRequestId";
        private const string DeclarationId = "pDeclarationId";
        private const string DiscrepancyId = "pDiscrepancyId";
        private const string CheckState = "pCheckState";
        private const string CheckedCount = "pChecked";
        private const string AllCount = "pAll";

        # endregion

        # region Конструторы

        public SelectionChecksAdapter(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        # region Реализация интерфейса ISelectionChecksAdapter

        public void DeclarationCheckState(long requestId, long declarationId, bool checkState)
        {
            Execute(
                OraHelper.SelectionPackageProcedure(UpdateDeclarationCheckState),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    OraHelper.NumericIn(RequestId, requestId),
                    OraHelper.NumericIn(DeclarationId, declarationId),
                    OraHelper.NumericIn(CheckState, checkState ? 1 : 0)
                });
        }

        public void DiscrepancyCheckState(long requestId, long discrepancyId, bool checkState)
        {
            Execute(
                OraHelper.SelectionPackageProcedure(UpdateDiscrepancyCheckState),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    OraHelper.NumericIn(RequestId, requestId),
                    OraHelper.NumericIn(DiscrepancyId, discrepancyId),
                    OraHelper.NumericIn(CheckState, checkState ? 1 : 0)
                });
        }

        public void ExcludeDeclarations(long requestId, QueryConditions criteria)
        {
            if (criteria == null || criteria.Filter.Count == 0)
            {
                Execute(
                    OraHelper.SelectionPackageProcedure(UpdateAllDeclarationsCheckState),
                    CommandType.StoredProcedure,
                    new OracleParameter[]
                    {
                        OraHelper.NumericIn(RequestId, requestId),
                        OraHelper.NumericIn(CheckState, 0)
                    });

            }
            else
            {
                var subQuery = criteria.Build(
                        string.Format(
                            ExcludeDeclarationsSqlPattern,
                            _service.GetConfigurationValue(Constants.MC_SCHEMA),
                            "{0}"
                        ),
                        ViewDeclarationsAlias
                );
                subQuery.Parameters.Insert(0, new QueryParameter() { Name = RequestId, Value = requestId });
                Execute(subQuery, CommandType.Text);
            }
        }

        public void IncludeDeclarations(long requestId, QueryConditions criteria)
        {
            if (criteria == null || criteria.Filter.Count == 0)
            {
                Execute(
                    OraHelper.SelectionPackageProcedure(UpdateAllDeclarationsCheckState),
                    CommandType.StoredProcedure,
                    new OracleParameter[]
                    {
                        OraHelper.NumericIn(RequestId, requestId),
                        OraHelper.NumericIn(CheckState, 1)
                    });

            }
            else
            {
                var subQuery = criteria.Build(
                        string.Format(
                            IncludeDeclarationsSqlPattern,
                            _service.GetConfigurationValue(Constants.MC_SCHEMA),
                            "{0}"
                        ),
                        ViewDeclarationsAlias
                );
                subQuery.Parameters.Insert(0, new QueryParameter() { Name = RequestId, Value = requestId });
                Execute(subQuery, CommandType.Text);
            }
        }

        public void ExcludeDiscrepancies(long requestId, QueryConditions criteria)
        {
            if (criteria == null || criteria.Filter.Count == 0)
            {
                Execute(
                    OraHelper.SelectionPackageProcedure(UpdateAllDiscrepanciesCheckState),
                    CommandType.StoredProcedure,
                    new OracleParameter[]
                    {
                        OraHelper.NumericIn(RequestId, requestId),
                        OraHelper.NumericIn(CheckState, 0)
                    });

            }
            else
            {
                var subQuery = criteria.Build(
                        string.Format(
                            ExcludeDiscrepanciesSqlPattern,
                            _service.GetConfigurationValue(Constants.MC_SCHEMA),
                            "{0}"
                        ),
                        ViewDiscrepanciesAlias
                );
                subQuery.Parameters.Insert(0, new QueryParameter() { Name = RequestId, Value = requestId });
                Execute(subQuery, CommandType.Text);
            }
        }

        public void IncludeDiscrepancies(long requestId, QueryConditions criteria)
        {
            if (criteria == null || criteria.Filter.Count == 0)
            {
                Execute(
                    OraHelper.SelectionPackageProcedure(UpdateAllDiscrepanciesCheckState),
                    CommandType.StoredProcedure,
                    new OracleParameter[]
                    {
                        OraHelper.NumericIn(RequestId, requestId),
                        OraHelper.NumericIn(CheckState, 1)
                    });

            }
            else
            {
                var subQuery = criteria.Build(
                        string.Format(
                            IncludeDiscrepanciesSqlPattern,
                            _service.GetConfigurationValue(Constants.MC_SCHEMA),
                            "{0}"
                        ),
                        ViewDiscrepanciesAlias
                );
                subQuery.Parameters.Insert(0, new QueryParameter() { Name = RequestId, Value = requestId });
                Execute(subQuery, CommandType.Text);
            }
        }

        public SelectionGroupState GetDeclarationsGroupState(long requestId, QueryConditions criteria)
        {
            CommandExecuteResult result;

            if (criteria == null || !criteria.Filter.Any())
            {
                result = Execute(
                    OraHelper.SelectionPackageProcedure(CountCheckedDeclarations),
                    CommandType.StoredProcedure,
                    new OracleParameter[] {
                        OraHelper.NumericIn(RequestId, requestId),
                        new OracleParameter(CheckedCount, OracleDbType.Decimal, ParameterDirection.Output),
                        new OracleParameter(AllCount, OracleDbType.Decimal, ParameterDirection.Output)                     
                    });
            }
            else
            {
                var query = criteria.Build(
                        GetDeclarationsGroupStateSqlPattern,
                        ViewDeclarationsAlias
                    );
                query.Parameters.Insert(0, new QueryParameter(false) { Name = CheckedCount });
                query.Parameters.Insert(1, new QueryParameter(false) { Name = AllCount });
                query.Parameters.Insert(2, new QueryParameter() { Name = RequestId, Value = requestId });

                result = Execute(query);
            }

            return State(
                GetCount(result.Output[CheckedCount]),
                GetCount(result.Output[AllCount]));
        }

        public SelectionGroupState GetDiscrepanciesGroupState(long requestId, QueryConditions criteria)
        {
            CommandExecuteResult result;

            if (criteria == null || !criteria.Filter.Any())
            {
                result = Execute(
                    OraHelper.SelectionPackageProcedure(CountCheckedDiscrepancies),
                    CommandType.StoredProcedure,
                    new OracleParameter[] {
                        OraHelper.NumericIn(RequestId, requestId),
                        new OracleParameter(CheckedCount, OracleDbType.Decimal, ParameterDirection.Output),
                        new OracleParameter(AllCount, OracleDbType.Decimal, ParameterDirection.Output)                     
                    });
            }
            else
            {
                var query = criteria.Build(
                        GetDiscrepanciesGroupStateSqlPattern,
                        ViewDiscrepanciesAlias
                    );
                query.Parameters.Insert(0, new QueryParameter(false) { Name = CheckedCount });
                query.Parameters.Insert(1, new QueryParameter(false) { Name = AllCount });
                query.Parameters.Insert(2, new QueryParameter() { Name = RequestId, Value = requestId });

                result = Execute(query);
            }

            return State(
                GetCount(result.Output[CheckedCount]),
                GetCount(result.Output[AllCount])); ;
        }

        # endregion

        private int GetCount(object value)
        {
            int retVal;

            if (!int.TryParse(value.ToString(), out retVal))
            {
                return 0;
            }

            return retVal;
        }

        private SelectionGroupState State(int included, int all)
        {
            if (all == 0 || included == 0)
            {
                return SelectionGroupState.AllExcluded;
            }

            if (all == included)
            {
                return SelectionGroupState.AllIncluded;
            }

            return SelectionGroupState.PartiallyIncluded;
        }
    }
}
