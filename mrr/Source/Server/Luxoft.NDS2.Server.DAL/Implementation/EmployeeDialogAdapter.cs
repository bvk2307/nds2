﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class EmployeeDialogAdapter : BaseOracleTableAdapter, IEmployeeDialogAdapter
    {
        private const string LoadSQL = @"select * from metodolog_check_employee where resp_id = :pId";
        private const string InsertSLQ = @"insert into metodolog_check_employee (resp_id, sid, type_id) values (:pId, :pSid, :pTypeId)";
        private const string DeleteSLQ = @"delete from metodolog_check_employee where resp_id = :pId and type_id = :pTypeId";

        public EmployeeDialogAdapter(IServiceProvider service) : base(service) { }

        public EmployeeData Load(long objectId, EmployeeType type)
        {
            var sql = LoadSQL;
            var parameters = new List<OracleParameter>
            {
                 FormatCommandHelper.NumericIn(":pId", objectId)
            };
            if (type != EmployeeType.All)
            {
                sql += " and type_id = :pTypeId";
                parameters.Add(FormatCommandHelper.NumericIn(":pTypeId", (long)type));
            }

            return new EmployeeData
            {
                Type = type,
                Sids = ExecuteList(sql, CommandType.Text, parameters.ToArray(), reader => reader.ReadString("sid"))
            };
        }

        public void Save(Region region, EmployeeData data)
        {
            var commands = new List<CommandContext>
            {
                new CommandContext
                {
                    Text = DeleteSLQ,
                    Type = CommandType.Text,
                    Parameters = new List<OracleParameter>
                    {
                        FormatCommandHelper.NumericIn(":pId", region.Id),
                        FormatCommandHelper.NumericIn(":pTypeId", (long)data.Type)
                    }
                }
            };

            Func<string, CommandContext> insert = (sid) => new CommandContext
            {
                Parameters = new List<OracleParameter>
                {
                    FormatCommandHelper.NumericIn(":pId", region.Id),
                    FormatCommandHelper.VarcharIn(":pSid", sid),
                    FormatCommandHelper.NumericIn(":pTypeId", (long)data.Type),
                },
                Text = InsertSLQ,
                Type = CommandType.Text
            };

            commands.AddRange(data.Sids.Select(item => insert(item)));
            new SounDialogAdapter(_service).SaveRegion(region);
            commands.ForEach(command => Execute(command));
        }
    }
}