﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ConfigExportInvoiceAdapter : BaseOracleTableAdapter, IConfigExportInvoiceAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string SearchSQLCfgExportInvoice = "select * from CFG_EXPORT_INVOICE order by id";

        public ConfigExportInvoiceAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public ConfigExportInvoice Search()
        {
            var query = string.Format(SearchSQLCommandPattern, SearchSQLCfgExportInvoice);
            var data = ExecuteList(query, CommandType.Text, new[] { FormatCommandHelper.Cursor("pCursor") }, ReadConfigExportInvoice);
            return data.SingleOrDefault();
        }

        private ConfigExportInvoice ReadConfigExportInvoice(OracleDataReader reader)
        {
            ConfigExportInvoice obj = new ConfigExportInvoice();
            obj.InvoiceMaxCount = reader.ReadInt64("INVOICE_MAX_COUNT");
            obj.PageSize = reader.ReadInt("PAGE_SIZE");
            obj.DelayBetweenPage = reader.ReadInt("DELAY_BETWEEN_PAGE");

            return obj;
        }
    }
}
