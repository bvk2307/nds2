﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Implementation
{

    internal sealed class DictionaryOperAdapter : BaseOracleTableAdapter, IDictionaryOperAdapter
    {
        private const string SELECT_SQL = @"select * from DICT_OPER_KIND";

        public DictionaryOperAdapter(IServiceProvider service) : base(service) { }

        public List<OperCode> GetList()
        {
            Func<OracleDataReader, OperCode> getData = reader => new OperCode
            {
                ID = reader.ReadString("ID"),
                NAME = reader.ReadString("OPER_NAME"),
            };

            return ExecuteList(SELECT_SQL, CommandType.Text, new OracleParameter[0], getData);
        }
    }
}
