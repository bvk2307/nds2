﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class ReportAdapter : BaseOracleTableAdapter, IReportAdapter
    {
        # region Константы

        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";

        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";
        private const string pReportNum = "pReportNum";
        private const string pFederalCodes = "pFederalCodes";
        private const string pRegionCodes = "pRegionCodes";
        private const string pSounCodes = "pSounCodes";
        private const string pNalogPeriod = "pNalogPeriod";
        private const string pFiscalYear = "pFiscalYear";
        private const string pDateBegin = "pDateBegin";
        private const string pDateEnd = "pDateEnd";
        private const string pDateCreateReport = "pDateCreateReport";
        private const string pTaskId = "pTaskId";
        private const string pTaskStatus = "pTaskStatus";
        private const string pRequestId = "pRequestId";
        private const string pData = "pData";
        private const string pFieldId = "ID";
        private const string pFieldName = "NAME";
        private const string pDeclStatisticType = "pDeclStatisticType";
        private const string pDeclStatisticRegim = "pDeclStatisticRegim";
        private const string pDynamicParameters = "pDynamicParameters";
        private const string pDynamicModuleType = "pDynamicModuleType";
        private const string pDynamicAggregateLevel = "pDynamicAggregateLevel";
        private const string pDynamicTimeCounting = "pDynamicTimeCounting";
        private const string pSonoCode = "pSonoCode";

        private const string ReportSaveTaskProcedure = "P$REPORT_SAVE_TASK";
        private const string ReportDynamicTechnoSaveTaskProcedure = "P$REPORT_DP_SAVE_TASK";
        private const string ReportDynamicDeclSaveTaskProcedure = "P$REPORT_DDS_SAVE_TASK";
        private const string ReportInfoResMatchSaveTaskProcedure = "P$REPORT_IRM_SAVE_TASK";
        private const string ReportMatchRuleSaveTaskProcedure = "P$REPORT_MR_SAVE_TASK";
        private const string ReportCreateProcedure = "P$REPORT_CREATE";
        private const string getTaskStatus = "BEGIN SELECT TASK_STATUS into :pTaskStatus FROM REPORT_TASK WHERE ID = :pTaskId; END;";
        private const string searchLoadingInspection = "SELECT * FROM V$REPORT_LOADING_INSPECTION vr WHERE TASK_ID = :pTaskId";
        private const string getCountLoadingInspection = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_LOADING_INSPECTION WHERE TASK_ID = :pTaskId; END;";
        private const string searchInspectorMonitoringWork = "SELECT * FROM V$REPORT_INSPEC_MONITOR_WORK vr WHERE TASK_ID = :pTaskId";
        private const string getCountInspectorMonitoringWork = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_INSPEC_MONITOR_WORK WHERE TASK_ID = :pTaskId; END;";
        private const string searchMonitoringProcessingDeclaration = "SELECT * FROM V$REPORT_MONITOR_DECLARATION vr WHERE TASK_ID = :pTaskId {0} {1}";
        private const string getCountMonitoringProcessingDeclaration = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_MONITOR_DECLARATION WHERE TASK_ID = :pTaskId; END;";
        private const string searchCheckControlRatio = "SELECT * FROM V$REPORT_CHECK_CONTROL_RATIO vr WHERE TASK_ID = :pTaskId";
        private const string getCountCheckControlRatio = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_CHECK_CONTROL_RATIO WHERE TASK_ID = :pTaskId; END;";
        private const string searchMatchingRule = "SELECT * FROM V$REPORT_MATCHING_RULE vr WHERE TASK_ID = :pTaskId";
        private const string getCountMatchingRule = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_MATCHING_RULE WHERE TASK_ID = :pTaskId; END;";
        private const string searchCheckLogicControl = "SELECT * FROM V$REPORT_CHECK_LOGIC_CONTROL vr WHERE TASK_ID = :pTaskId";
        private const string getCountCheckLogicControl = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_CHECK_LOGIC_CONTROL WHERE TASK_ID = :pTaskId; END;";
        private const string searchDeclarationStatistic = "SELECT * FROM V$REPORT_DECLARATION vr WHERE TASK_ID = :pTaskId {0} {1}";
        private const string getCountDeclarationStatistic = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_DECLARATION WHERE TASK_ID = :pTaskId; END;";

        private const string searchModuleNames = @"select module_name from TECHNO_REPORT_ITERATION group by module_name order by module_name";
        private const string searchDynamicParameters = @"select * from V$TECHNO_REPORT_PARAMETER where id >= :pIdBegin and id <= :pIdEnd";
        private const string pIdBegin = "pIdBegin";
        private const string pIdEnd = "pIdEnd";
        private const string searchReportDynamicParameters = "SELECT * FROM V$REPORT_DYNAMIC_PARAMETERS vr WHERE TASK_ID = :pTaskId";
        private const string getCountReportDynamicParameters = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_DYNAMIC_PARAMETERS WHERE TASK_ID = :pTaskId; END;";
        private const string getAllDatesTechnoReport = @"
select ri.create_date 
from TECHNO_REPORT_ITERATION ri
join TECHNO_REPORT_VALUE rv on rv.iteration_id = ri.id
group by ri.create_date
order by ri.create_date";
        private const string searchReportDynamicDeclaration = "SELECT * FROM V$REPORT_DYNAMIC_DECLARATION vr WHERE TASK_ID = :pTaskId";
        private const string getCountReportDynamicDeclaration = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_DYNAMIC_DECLARATION WHERE TASK_ID = :pTaskId; END;";
        private const string searchReportInfoResultsOfMatching = "SELECT * FROM V$REPORT_INFO_RESULT_MATCHING vr WHERE TASK_ID = :pTaskId";
        private const string getCountReportInfoResultsOfMatching = "BEGIN SELECT COUNT(*) INTO :pResult FROM V$REPORT_INFO_RESULT_MATCHING WHERE TASK_ID = :pTaskId; END;";

        private const string PackageName = "NDS2$REPORTS";
        private const string GetStatusProcedureName = "P$REPORT_LK_REQUEST_STATUS";

        private ReportHelper _reportHelper = new ReportHelper();
        private Dictionary<DynamicModuleType, string> _dictModules = new Dictionary<DynamicModuleType, string>();

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса UserToRegionAdapter
        /// </summary>
        /// <param name="service"></param>
        public ReportAdapter(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        # region Мониторинг налоговых инспекторов (1.6)

        public PageResult<InspectorMonitoringWork> SearchInspectorMonitoringWork(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchInspectorMonitoringWork, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<InspectorMonitoringWork>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildInspectorMonitoringWork);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountInspectorMonitoringWork,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<InspectorMonitoringWork>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private InspectorMonitoringWork BuildInspectorMonitoringWork(OracleDataReader reader)
        {
            return new InspectorMonitoringWork()
            {
                FederalDistrict = reader.ReadString("FederalDistrict"),
                Region = reader.ReadString("Region"),
                Inspection = reader.ReadString("Inspection"),
                Inspector = reader.ReadString("Inspector"),
                UnrepairedDiscrepancyGeneralCount = reader.ReadInt64("NotEliminated_GeneralCount"),
                UnrepairedDiscrepancyGeneralSum = reader.ReadDecimal("NotEliminated_GeneralSum"),
                UnrepairedDiscrepancyCurrencyCount = reader.ReadInt64("NotElimin_CurrencyCount"),
                UnrepairedDiscrepancyCurrencySum = reader.ReadDecimal("NotElimin_CurrencySum"),
                UnrepairedDiscrepancyNDSCount = reader.ReadInt64("NotElimin_NDSCount"),
                UnrepairedDiscrepancyNDSSum = reader.ReadDecimal("NotElimin_NDSSum"),
                UnrepairedDiscrepancyInexactComparisonCount = reader.ReadInt64("NotElimin_NotExactCount"),
                UnrepairedDiscrepancyInexactComparisonSum = reader.ReadDecimal("NotElimin_NotExactSum"),
                UnrepairedDiscrepancyBreakCount = reader.ReadInt64("NotElimin_GapCount"),
                UnrepairedDiscrepancyBreakSum = reader.ReadDecimal("NotElimin_GapSum"),
                IdentifiedAndUnrepairedDiscrepancyGeneralCount = reader.ReadInt64("IdentNotElimin_GeneralCount"),
                IdentifiedAndUnrepairedDiscrepancyGeneralSum = reader.ReadDecimal("IdentNotElimin_GeneralSum"),
                IdentifiedAndUnrepairedDiscrepancyCurrencyCount = reader.ReadInt64("IdentNotElimin_CurrencyCount"),
                IdentifiedAndUnrepairedDiscrepancyCurrencySum = reader.ReadDecimal("IdentNotElimin_CurrencySum"),
                IdentifiedAndUnrepairedDiscrepancyNDSCount = reader.ReadInt64("IdentNotElimin_NDSCount"),
                IdentifiedAndUnrepairedDiscrepancyNDSSum = reader.ReadDecimal("IdentNotElimin_NDSSum"),
                IdentifiedAndUnrepairedDiscrepancyInexactComparisonCount = reader.ReadInt64("IdentNotElimin_NotExactCount"),
                IdentifiedAndUnrepairedDiscrepancyInexactComparisonSum = reader.ReadDecimal("IdentNotElimin_NotExactSum"),
                IdentifiedAndUnrepairedDiscrepancyBreakCount = reader.ReadInt64("IdentNotElimin_GapCount"),
                IdentifiedAndUnrepairedDiscrepancyBreakSum = reader.ReadDecimal("IdentNotElimin_GapSum"),
                IdentifiedAndRepairedDiscrepancyGeneralCount = reader.ReadInt64("IdentElimin_GeneralCount"),
                IdentifiedAndRepairedDiscrepancyGeneralSum = reader.ReadDecimal("IdentElimin_GeneralSum"),
                IdentifiedAndRepairedDiscrepancyCurrencyCount = reader.ReadInt64("IdentElimin_CurrencyCount"),
                IdentifiedAndRepairedDiscrepancyCurrencySum = reader.ReadDecimal("IdentElimin_CurrencySum"),
                IdentifiedAndRepairedDiscrepancyNDSCount = reader.ReadInt64("IdentElimin_NDSCount"),
                IdentifiedAndRepairedDiscrepancyNDSSum = reader.ReadDecimal("IdentElimin_NDSSum"),
                IdentifiedAndRepairedDiscrepancyInexactComparisonCount = reader.ReadInt64("IdentElimin_NotExactCount"),
                IdentifiedAndRepairedDiscrepancyInexactComparisonSum = reader.ReadDecimal("IdentElimin_NotExactSum"),
                IdentifiedAndRepairedDiscrepancyBreakCount = reader.ReadInt64("IdentElimin_GapCount"),
                IdentifiedAndRepairedDiscrepancyBreakSum = reader.ReadDecimal("IdentElimin_GapSum")
            };
        }

        # endregion 

        # region Отчет о загруженности инспекций (2.1)

        public PageResult<LoadingInspection> SearchLoadingInspection(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchLoadingInspection, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<LoadingInspection>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildLoadingInspection);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountLoadingInspection,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<LoadingInspection>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private LoadingInspection BuildLoadingInspection(OracleDataReader reader)
        {
            LoadingInspection li = new LoadingInspection()
            {
                FederalDistrict = reader.ReadString("FederalDistrict"),
                Region = reader.ReadString("Region"),
                Inspection = reader.ReadString("Inspection"),
                InspectorCount = reader.ReadString("InspectorCount"),
                DiscrepancyGeneralCount = reader.ReadInt64("GeneralCount"),
                DiscrepancyGeneralSum = reader.ReadDecimal("GeneralSum"),
                SentToWorkDiscrepancyCount = reader.ReadInt64("SentToWorkGeneralCount"),
                SentToWorkDiscrepancySum = reader.ReadDecimal("SentToWorkGeneralSum"),
                RepairedDiscrepancyCount = reader.ReadInt64("RepairedGeneralCount"),
                RepairedDiscrepancySum = reader.ReadDecimal("RepairedGeneralSum"),
                UnrepairedDiscrepancyCount = reader.ReadInt64("UnrepairedGeneralCount"),
                UnrepairedDiscrepancySum = reader.ReadDecimal("UnrepairedGeneralSum"),
            };
            if (li.DiscrepancyGeneralCount > 0)
            {
                li.SentToWorkDiscrepancyPercentage = (int)((li.SentToWorkDiscrepancyCount * 100) / li.DiscrepancyGeneralCount); 
            }
            else { li.SentToWorkDiscrepancyPercentage = 0; }
            if (li.DiscrepancyGeneralSum > 0)
            {
                li.SentToWorkDiscrepancyPercentageSum = (int)((li.SentToWorkDiscrepancySum * 100) / li.DiscrepancyGeneralSum);
            }
            else { li.SentToWorkDiscrepancyPercentageSum = 0; }
            if (li.SentToWorkDiscrepancyCount > 0)
            {
                li.RepairedDiscrepancyPercentage = (int)((li.RepairedDiscrepancyCount * 100) / li.SentToWorkDiscrepancyCount);
            }
            else { li.RepairedDiscrepancyPercentage = 0; }
            if (li.SentToWorkDiscrepancySum > 0)
            {
                li.RepairedDiscrepancyPercentageSum = (int)((li.RepairedDiscrepancySum * 100) / li.SentToWorkDiscrepancySum);
            }
            else { li.RepairedDiscrepancyPercentageSum = 0; }

            return li;
        }

        # endregion

        # region Мониторинг обработки налоговых деклараций (1.7)

        public PageResult<MonitoringProcessingDeclaration> SearchMonitoringProcessingDeclaration(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchMonitoringProcessingDeclaration, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<MonitoringProcessingDeclaration>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildMonitoringProcessingDeclaration);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountMonitoringProcessingDeclaration,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<MonitoringProcessingDeclaration>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private MonitoringProcessingDeclaration BuildMonitoringProcessingDeclaration(OracleDataReader reader)
        {
            return new MonitoringProcessingDeclaration()
            {
                FederalDistrict = reader.ReadString("FederalDistrict"),
                Region = reader.ReadString("Region"),
                Inspection = reader.ReadString("Inspection"),
                TakeUpdatesCount = reader.ReadInt("CORRECTION_NUMBER"),
                TaxpayerINN = reader.ReadString("INN"),
                TaxpayerKPP = reader.ReadString("KPP"),
                TaxpayerName = reader.ReadString("NAME"),
                Inspector = reader.ReadString("Inspector"),

                UnrepairedDiscrepancyFromGeneralCount = reader.ReadInt64("r1totalCount"),
                UnrepairedDiscrepancyFromGeneralSum = reader.ReadDecimal("r1totalSum"),
                UnrepairedDiscrepancyFromErrorLKCount = reader.ReadInt("r1lkError"),
                UnrepairedDiscrepancyFromCurrencyCount = reader.ReadInt64("r1valuteCount"),
                UnrepairedDiscrepancyFromCurrencySum = reader.ReadDecimal("r1valuteSum"),
                UnrepairedDiscrepancyFromNDSCount = reader.ReadInt64("r1ndsCount"),
                UnrepairedDiscrepancyFromNDSSum = reader.ReadDecimal("r1ndsSum"),
                UnrepairedDiscrepancyFromInexactComparisonCount = reader.ReadInt64("r1nsCount"),
                UnrepairedDiscrepancyFromInexactComparisonSum = reader.ReadDecimal("r1nsSum"),
                UnrepairedDiscrepancyFromBreakCount = reader.ReadInt64("r1breakCount"),
                UnrepairedDiscrepancyFromBreakSum = reader.ReadDecimal("r1breakSum"),

                UnrepairedDiscrepancyToGeneralCount = reader.ReadInt64("r2totalCount"),
                UnrepairedDiscrepancyToGeneralSum = reader.ReadDecimal("r2totalSum"),
                UnrepairedDiscrepancyToErrorLKCount = reader.ReadInt("r2lkError"),
                UnrepairedDiscrepancyToCurrencyCount = reader.ReadInt64("r2valuteCount"),
                UnrepairedDiscrepancyToCurrencySum = reader.ReadDecimal("r2ndsCount"),
                UnrepairedDiscrepancyToNDSCount = reader.ReadInt64("r2ndsSum"),
                UnrepairedDiscrepancyToNDSSum = reader.ReadDecimal("r2ndsSum"),
                UnrepairedDiscrepancyToInexactComparisonCount = reader.ReadInt64("r2nsCount"),
                UnrepairedDiscrepancyToInexactComparisonSum = reader.ReadDecimal("r2nsSum"),
                UnrepairedDiscrepancyToBreakCount = reader.ReadInt64("r2breakCount"),
                UnrepairedDiscrepancyToBreakSum = reader.ReadDecimal("r2breakSum"),

                RepairedDiscrepancyGeneralCount = reader.ReadInt64("r3totalCount"),
                RepairedDiscrepancyGeneralSum = reader.ReadDecimal("r3totalSum"),
                RepairedDiscrepancyCurrencyCount = reader.ReadInt64("r3valuteCount"),
                RepairedDiscrepancyCurrencySum = reader.ReadDecimal("r3valuteSum"),
                RepairedDiscrepancyNDSCount = reader.ReadInt64("r3ndsCount"),
                RepairedDiscrepancyNDSSum = reader.ReadDecimal("r3ndsSum"),
                RepairedDiscrepancyInexactComparisonCount = reader.ReadInt64("r3nsCount"),
                RepairedDiscrepancyInexactComparisonSum = reader.ReadDecimal("r3nsSum"),
                RepairedDiscrepancyBreakCount = reader.ReadInt64("r3breakCount"),
                RepairedDiscrepancyBreakSum = reader.ReadDecimal("r3breakSum")
            };
        }

        # endregion

        # region Отчет по проверкам КС (1.4)

        public PageResult<CheckControlRatio> SearchCheckControlRatio(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchCheckControlRatio, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<CheckControlRatio>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildCheckControlRatio);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountCheckControlRatio,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<CheckControlRatio>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private CheckControlRatio BuildCheckControlRatio(OracleDataReader reader)
        {
            return new CheckControlRatio()
            {
                Num = reader.ReadString("Num"),
                Description = reader.ReadString("Description"),
                CountDiscrepancy = reader.ReadInt64("CountDiscrepancy")
            };
        }

        # endregion

        # region Отчет по правилам сопоставления (1.2)

        private PageResult<MatchingRule> SearchMatchingRuleStubs(QueryConditions criteria, long taskId)
        {
            List<MatchingRule> data = new List<MatchingRule>();
            for (int i = -1; ++i < 10; )
            {
                MatchingRule obj = new MatchingRule();

                obj.RuleGroupCaption = "1";
                obj.RuleNumberCaption = "2";
                obj.RuleNumberComment = "коммент";
                obj.MatchCount = 23;
                obj.MatchAmount = 25600;
                obj.PercentByCount = 12;
                obj.PercentByAmount = 14;
                obj.PercentInnerInvoiceErrorLC = 7;
                obj.PercentOuterInvoiceErrorLC = 6;
                obj.PercentInnerOuterInvoiceErrorLC = 2;

                data.Add(obj);
            }

            return new PageResult<MatchingRule>(
                data,
                data.Count());
        }

        public PageResult<MatchingRule> SearchMatchingRule(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchMatchingRule, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<MatchingRule>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildMatchingRule);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountMatchingRule,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<MatchingRule>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private MatchingRule BuildMatchingRule(OracleDataReader reader)
        {
            MatchingRule obj = new MatchingRule();
            obj.RuleGroupCaption = reader.ReadString("Rule_Group_Caption");
            obj.RuleNumberCaption = reader.ReadString("Rule_Number_Caption");
            obj.RuleNumberComment = reader.ReadString("Rule_Number_Comment");
            obj.MatchCount = reader.ReadInt64("MatchCount");
            obj.MatchAmount = reader.ReadDecimal("MatchAmount");
            obj.PercentByCount = reader.ReadNullableDecimal("PercentByCount");
            obj.PercentByAmount = reader.ReadNullableDecimal("PercentByAmount");
            obj.PercentInnerInvoiceErrorLC = reader.ReadNullableDecimal("PercentInnerInvoiceErrorLC");
            obj.PercentOuterInvoiceErrorLC = reader.ReadNullableDecimal("PercentOuterInvoiceErrorLC");
            obj.PercentInnerOuterInvoiceErrorLC = reader.ReadNullableDecimal("PercentInnerOuterInvErrorLC");
            return obj;
        }
        # endregion

        # region Отчет по проверкам ЛК (1.3)

        private PageResult<CheckLogicControl> SearchCheckLogicControlStubs(QueryConditions criteria, long taskId)
        {
            List<CheckLogicControl> data = new List<CheckLogicControl>();
            for (int i = -1; ++i < 10; )
            {
                CheckLogicControl obj = new CheckLogicControl();

                obj.Num = "Num";
                obj.Name = "Name";
                obj.ErrorCount = 23;
                obj.ErrorCountPercentage = 33;
                obj.ErrorFrequencyInvoiceNotExact = 21;
                obj.ErrorFrequencyInvoiceGap = 12;
                obj.ErrorFrequencyInvoiceExact = 2;
                obj.ErrorFrequencyInnerInvoice = 3;
                obj.ErrorFrequencyOuterInvoice = 4;

                data.Add(obj);
            }

            return new PageResult<CheckLogicControl>(
                data,
                data.Count());
        }

        public PageResult<CheckLogicControl> SearchCheckLogicControl(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchCheckLogicControl, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<CheckLogicControl>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildCheckLogicControl);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountCheckLogicControl,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<CheckLogicControl>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private CheckLogicControl BuildCheckLogicControl(OracleDataReader reader)
        {
            CheckLogicControl obj = new CheckLogicControl();

            obj.Num = reader.ReadString("Num");
            obj.Name = reader.ReadString("Name");
            obj.ErrorCount = reader.ReadInt64("ErrorCount");
            obj.ErrorCountPercentage = reader.ReadNullableDecimal("ErrorCountPercentage");
            obj.ErrorFrequencyInvoiceNotExact = reader.ReadNullableDecimal("ErrorFrequencyInvoiceNotExact");
            obj.ErrorFrequencyInvoiceGap = reader.ReadNullableDecimal("ErrorFrequencyInvoiceGap");
            obj.ErrorFrequencyInvoiceExact = reader.ReadNullableDecimal("ErrorFrequencyInvoiceExact");
            obj.ErrorFrequencyInnerInvoice = reader.ReadNullableDecimal("ErrorFrequencyInnerInvoice");
            obj.ErrorFrequencyOuterInvoice = reader.ReadNullableDecimal("ErrorFrequencyOuterInvoice");

            return obj;
        }

        # endregion

        # region Отчет Фоновые показатели (По поданным декларациям)

        public PageResult<DeclarationStatistic> SearchDeclarationStatistic(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchDeclarationStatistic, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<DeclarationStatistic>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDeclarationStatistic);

            var parametersForCount = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(pTaskId, taskId),
                FormatCommandHelper.NumericOut(CountResult)
            };

            var result = Execute(getCountDeclarationStatistic,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<DeclarationStatistic>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private DeclarationStatistic BuildDeclarationStatistic(OracleDataReader reader)
        {
            DeclarationStatistic obj = new DeclarationStatistic();
            
            obj.NameGroup = reader.ReadString("Name_Group");
            obj.DeclCountTotal = reader.ReadInt64("Decl_Count_Total");
            obj.DeclCountNull = reader.ReadInt64("Decl_Count_Null");
            obj.DeclCountCompensation = reader.ReadInt64("Decl_Count_Compensation");
            obj.DeclCountPayment = reader.ReadInt64("Decl_Count_Payment");
            obj.DeclCountNotSubmit = reader.ReadInt64("Decl_Count_Not_Submit");
            obj.DeclCountRevisedTotal = reader.ReadInt64("Decl_Count_Revised_Total");
            obj.TaxBaseSumCompensation = reader.ReadInt64("TaxBase_Sum_Compensation");
            obj.TaxBaseSumPayment = reader.ReadInt64("TaxBase_Sum_Payment");
            obj.NdsCalculatedSumCompensation = reader.ReadInt64("NdsCalculated_Sum_Compensation");
            obj.NdsCalculatedSumPayment = reader.ReadInt64("NdsCalculated_Sum_Payment");
            obj.NdsDeductionSumCompensation = reader.ReadInt64("NdsDeduction_Sum_Compensation");
            obj.NdsDeductionSumPayment = reader.ReadInt64("NdsDeduction_Sum_Payment");
            obj.NdsSumCompensation = reader.ReadInt64("Nds_Sum_Compensation");
            obj.NdsSumPayment = reader.ReadInt64("Nds_Sum_Payment");

            return obj;
        }

        # endregion

        # region Отчет по динамике изменения технологических параметров

        public PageResult<DynamicTechnoParameter> SearchDynamicTechnoParameter(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchReportDynamicParameters, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<DynamicTechnoParameter>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDynamicTechnoParameter);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountReportDynamicParameters,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<DynamicTechnoParameter>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private DynamicTechnoParameter BuildDynamicTechnoParameter(OracleDataReader reader)
        {
            DynamicTechnoParameter obj = new DynamicTechnoParameter();
            obj.Period = reader.ReadString("Period");
            obj.P01 = reader.ReadString("P01");
            obj.P02 = reader.ReadString("P02");
            obj.P03 = reader.ReadString("P03");
            obj.P04 = reader.ReadString("P04");
            obj.P05 = reader.ReadString("P05");
            obj.P06 = reader.ReadString("P06");
            obj.P07 = reader.ReadString("P07");
            obj.P08 = reader.ReadString("P08");
            obj.P09 = reader.ReadString("P09");
            obj.P10 = reader.ReadString("P10");
            obj.P11 = reader.ReadString("P11");
            obj.P12 = reader.ReadString("P12");
            obj.P13 = reader.ReadString("P13");
            obj.P14 = reader.ReadString("P14");
            obj.P15 = reader.ReadString("P15");
            obj.P16 = reader.ReadString("P16");
            obj.P17 = reader.ReadString("P17");
            obj.P18 = reader.ReadString("P18");
            obj.P19 = reader.ReadString("P19");
            obj.P20 = reader.ReadString("P20");
            obj.P21 = reader.ReadString("P21");
            obj.P22 = reader.ReadString("P22");
            obj.P23 = reader.ReadString("P23");
            obj.P24 = reader.ReadString("P24");
            obj.P25 = reader.ReadString("P25");
            obj.P26 = reader.ReadString("P26");
            obj.P27 = reader.ReadString("P27");
            obj.P28 = reader.ReadString("P28");
            obj.P29 = reader.ReadString("P29");
            obj.P30 = reader.ReadString("P30");
            if (!string.IsNullOrWhiteSpace(obj.Period))
            {
                obj.Period = obj.Period.Replace("\\r\\n", "\r\n"); 
            }
            return obj;
        }

        public List<DynamicParameter> SearchDictionaryDynamicParameters(DynamicModuleType moduleType)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));
            FillDynamicParameters(parameters, moduleType);

            FillDynamicModuleName();

            return ExecuteList<DynamicParameter>(
                string.Format(SearchSQLCommandPattern, searchDynamicParameters),
                CommandType.Text,
                parameters.ToArray(),
                (reader) =>
                {
                    DynamicParameter ret = new DynamicParameter();
                    ret.Id = reader.ReadInt("Id");
                    ret.Name = reader.ReadString("Name");
                    ret.ModuleType = GetDymnamicModuleType(ret.Id);
                    ret.ModuleName = GetDynamicModuleName(ret.ModuleType);
                    return ret;
                });
        }

        private void FillDynamicParameters(List<OracleParameter> parameters, DynamicModuleType moduleType)
        {
            int idBegin = 1;
            int idEnd = 100000;
            if (moduleType == DynamicModuleType.All)
            {
                idBegin = 1;
                idEnd = 100000;
            }
            else if (moduleType == DynamicModuleType.GP_3)
            {
                idBegin = 1;
                idEnd = 100;
            }
            else if (moduleType == DynamicModuleType.MC)
            {
                idBegin = 101;
                idEnd = 1000;
            }
            else if (moduleType == DynamicModuleType.SOV)
            {
                idBegin = 1001;
                idEnd = 1500;
            }
            else if (moduleType == DynamicModuleType.MRR)
            {
                idBegin = 1501;
                idEnd = 10000;
            }
            parameters.Add(FormatCommandHelper.NumericIn(pIdBegin, idBegin));
            parameters.Add(FormatCommandHelper.NumericIn(pIdEnd, idEnd));
        }

        private DynamicModuleType GetDymnamicModuleType(int id)
        {
            DynamicModuleType ret = DynamicModuleType.All;
            if (id >= 1 && id <= 100)
            {
                ret = DynamicModuleType.GP_3;
            }
            else if (id >= 101 && id <= 1000)
            {
                ret = DynamicModuleType.MC;
            }
            else if (id >= 1001 && id <= 1500)
            {
                ret = DynamicModuleType.SOV;
            }
            else if (id >= 1501)
            {
                ret = DynamicModuleType.MRR;
            }
            return ret;
        }

        private void FillDynamicModuleName()
        {
            _dictModules.Clear();
            _dictModules.Add(DynamicModuleType.GP_3, "ГП-3");
            _dictModules.Add(DynamicModuleType.MC, "МС");
            _dictModules.Add(DynamicModuleType.SOV, "СОВ");
            _dictModules.Add(DynamicModuleType.MRR, "МРР");
        }

        private string GetDynamicModuleName(DynamicModuleType moduleType)
        {
            string ret = String.Empty;
            if (_dictModules.ContainsKey(moduleType))
            {
                ret = _dictModules[moduleType];
            }
            return ret;
        }

        public List<DateTime> GetDatesTechnoReport()
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            return ExecuteList<DateTime>(
                string.Format(SearchSQLCommandPattern, getAllDatesTechnoReport),
                CommandType.Text,
                parameters.ToArray(),
                (reader) =>
                {
                    return (DateTime)reader.ReadDateTime("create_date");
                });
        }

        # endregion

        # region Отчёт по динамике изменения фоновых показателей

        public PageResult<DynamicDeclarationStatistic> SearchDynamicDeclarationStatistic(QueryConditions criteria, long taskId)
        {
            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchReportDynamicDeclaration, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<DynamicDeclarationStatistic>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDynamicDeclaration);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountReportDynamicDeclaration,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<DynamicDeclarationStatistic>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private DynamicDeclarationStatistic BuildDynamicDeclaration(OracleDataReader reader)
        {
            DynamicDeclarationStatistic obj = new DynamicDeclarationStatistic();
            obj.Period = reader.ReadString("Period");
            obj.P01 = reader.ReadString("P01");
            obj.P02 = reader.ReadString("P02");
            obj.P03 = reader.ReadString("P03");
            obj.P04 = reader.ReadString("P04");
            obj.P05 = reader.ReadString("P05");
            obj.P06 = reader.ReadString("P06");
            obj.P07 = reader.ReadString("P07");
            obj.P08 = reader.ReadString("P08");
            obj.P09 = reader.ReadString("P09");
            obj.P10 = reader.ReadString("P10");
            obj.P11 = reader.ReadString("P11");
            obj.P12 = reader.ReadString("P12");
            obj.P13 = reader.ReadString("P13");
            obj.P14 = reader.ReadString("P14");
            if (!string.IsNullOrWhiteSpace(obj.Period))
            {
                obj.Period = obj.Period.Replace("\\r\\n", "\r\n");
            }
            return obj;
        }

        # endregion

        # region Информация о результатах сопоставлений (В2)

        private PageResult<InfoResultsOfMatching> SearchInfoResultsOfMatchingStubs(QueryConditions criteria, long taskId)
        {
            List<InfoResultsOfMatching> data = new List<InfoResultsOfMatching>();
            for (int i = -1; ++i < 10; )
            {
                InfoResultsOfMatching obj = new InfoResultsOfMatching();
                obj.AggregateName = string.Format("{0}", i * 2);
                obj.SubmittingDeclarationPrimaryCount = i * 1;
                obj.SubmittingDeclarationCorrectCount = i * 2;
                obj.SubmittingDeclarationBrokeringCount = i * 3;
                obj.SubmittingJournalCount = i * 4;
                obj.CountOperationInDeclarationTotal = i * 5;
                obj.CountOperationInDeclarationR08 = i * 6;
                obj.CountOperationInDeclarationR09 = i * 7;
                obj.CountOperationInDeclarationR10 = i * 8;
                obj.CountOperationInDeclarationR11 = i * 9;
                obj.CountOperationInDeclarationR12 = i * 10;
                obj.CountExactDiscrepancy = i * 11;
                obj.TotalDeclarationDiscrepancyPrimary = i * 12;
                obj.TotalDeclarationDiscrepancyCorrection = i * 13;
                obj.TotalDeclarationDiscrepancyControlRation = i * 14;
                obj.IdentDiscrepancyTotalCount = i * 15;
                obj.IdentDiscrepancyTotalAmount = i * 16;
                obj.IdentNotExactDiscrepancyOverflowCount = i * 17;
                obj.IdentNotExactDiscrepancyOverflowAmount = i * 18;
                obj.IdentNotExactDiscrepancyNotOverflowCount = i * 19;
                obj.IdentExactDiscrepancyOverflowCount = i * 20;
                obj.IdentExactDiscrepancyOverflowAmount = i * 21;
                obj.IdentExactDiscrepancyBreakCount = i * 22;
                obj.IdentExactDiscrepancyBreakAmount = i * 23;
                obj.IdentExactDiscrepancyCurrencyCount = i * 24;
                obj.IdentExactDiscrepancyCurrencyAmount = i * 25;
                obj.SpecificWeightDiscrepancyInTotalOperation = i * 26;
                obj.DiscrepancySendToTNOCount = i * 27;
                obj.DiscrepancySendToTNOAmount = i * 28;
                data.Add(obj);
            }
            return new PageResult<InfoResultsOfMatching>(data, data.Count());
        }

        public PageResult<InfoResultsOfMatching> SearchInfoResultsOfMatching(QueryConditions criteria, long taskId)
        {
            //return SearchInfoResultsOfMatchingStubs(criteria, taskId);

            QueryConditions criteriaWitoutFilters = _reportHelper.GetEmptyFilterCriteria(criteria);
            var query = criteriaWitoutFilters.ToSQL(searchReportInfoResultsOfMatching, "vr", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<InfoResultsOfMatching>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildInfoResultsOfMatching);

            List<OracleParameter> parametersForCount = new List<OracleParameter>();
            parametersForCount.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            parametersForCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(getCountReportInfoResultsOfMatching,
                CommandType.Text,
                parametersForCount.ToArray());

            return new PageResult<InfoResultsOfMatching>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        private InfoResultsOfMatching BuildInfoResultsOfMatching(OracleDataReader reader)
        {
            InfoResultsOfMatching obj = new InfoResultsOfMatching();
            obj.AggregateName = reader.ReadString("aggregate_name");
            obj.SubmittingDeclarationPrimaryCount = reader.ReadInt64("primary_declaration_count");
            obj.SubmittingDeclarationCorrectCount = reader.ReadInt64("correction_declaration_count");
            obj.SubmittingDeclarationBrokeringCount = reader.ReadInt64("decl_with_mediator_ops_count");
            obj.SubmittingJournalCount = reader.ReadInt64("journal_count");
            obj.CountOperationInDeclarationTotal = reader.ReadInt64("invoice_record_count");
            obj.CountOperationInDeclarationR08 = reader.ReadInt64("invoice_record_count_chapter8");
            obj.CountOperationInDeclarationR09 = reader.ReadInt64("invoice_record_count_chapter9");
            obj.CountOperationInDeclarationR10 = reader.ReadInt64("invoice_record_count_chapter10");
            obj.CountOperationInDeclarationR11 = reader.ReadInt64("invoice_record_count_chapter11");
            obj.CountOperationInDeclarationR12 = reader.ReadInt64("invoice_record_count_chapter12");
            obj.CountExactDiscrepancy = reader.ReadInt64("exact_match_count");
            obj.TotalDeclarationDiscrepancyPrimary = reader.ReadInt64("prim_decl_with_mismatch_count");
            obj.TotalDeclarationDiscrepancyCorrection = reader.ReadInt64("corr_decl_with_mismatch_count");
            obj.TotalDeclarationDiscrepancyControlRation = reader.ReadInt64("decl_with_kc_mismatch_count");
            obj.IdentDiscrepancyTotalCount = reader.ReadInt64("mismatch_total_count");
            obj.IdentDiscrepancyTotalAmount = reader.ReadDecimal("mismatch_total_sum");
            obj.IdentNotExactDiscrepancyOverflowCount = reader.ReadInt64("weak_and_nds_count");
            obj.IdentNotExactDiscrepancyOverflowAmount = reader.ReadDecimal("weak_and_nds_sum");
            obj.IdentNotExactDiscrepancyNotOverflowCount = reader.ReadInt64("weak_and_no_nds_count");
            obj.IdentExactDiscrepancyOverflowCount = reader.ReadInt64("exact_and_nds_count");
            obj.IdentExactDiscrepancyOverflowAmount = reader.ReadDecimal("exact_and_nds_sum");
            obj.IdentExactDiscrepancyBreakCount = reader.ReadInt64("gap_count");
            obj.IdentExactDiscrepancyBreakAmount = reader.ReadDecimal("gap_amount");
            obj.IdentExactDiscrepancyCurrencyCount = reader.ReadInt64("currency_count");
            obj.IdentExactDiscrepancyCurrencyAmount = reader.ReadDecimal("currency_amount");
            obj.DiscrepancySendToTNOCount = reader.ReadInt64("mismatch_tno_count");
            obj.DiscrepancySendToTNOAmount = reader.ReadDecimal("mismatch_tno_sum");
            obj.SpecificWeightDiscrepancyInTotalOperation = reader.ReadDecimal("weight_mismatch_in_total_oper");
            return obj;
        }

        # endregion

        # region Заявки на создание отчетов

        public long SaveTask(QueryConditions criteria, long requestId, int reportNum)
        {
            long taskId = -1;
            var funcs = new Dictionary<int, Func<QueryConditions, long, int, long>>
            {
                {(int) ReportNum.DynamicTechnoParameter, SaveTaskDynamicTechnoReport},
                {(int) ReportNum.DynamicDeclarationStatistic, SaveTaskDynamicDeclReport},
                {(int) ReportNum.InfoResultsOfMatching, SaveTaskInfoResultsOfMatchingReport},
                {(int) ReportNum.MatchingRule, SaveTaskMatchingRuleReport}
            };

            if (funcs.ContainsKey(reportNum))
            {
                taskId = funcs[reportNum].Invoke(criteria, requestId, reportNum);
            }
            else
            {
                taskId = SaveTaskGeneric(criteria, reportNum);
            }

            RunCreateReport(reportNum, taskId, requestId);

            return taskId;
        }

        private long SaveTaskGeneric(QueryConditions criteria, int reportNum)
        {
            var oraParameters = new List<OracleParameter>();
            OracleParameter paramFederalCodes = _reportHelper.CreateArrayParameter(criteria, "FederalDistrict", pFederalCodes, string.Empty);
            OracleParameter paramRegionCodes = _reportHelper.CreateArrayParameter(criteria, "TaxPayerRegionCode", pRegionCodes, string.Empty);
            OracleParameter paramSounCodes = _reportHelper.CreateArrayParameter(criteria, "NalogOrganCode", pSounCodes, string.Empty);

            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(paramFederalCodes);
            oraParameters.Add(paramRegionCodes);
            oraParameters.Add(paramSounCodes);

            //----- Код подразделения НО
            object oSonoCode = _reportHelper.GetFilterFromCondition(criteria, "SonoCode");
            if (oSonoCode != null)
            {
                oraParameters.Add(FormatCommandHelper.VarcharIn(pSonoCode, (string)oSonoCode));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pSonoCode, DBNull.Value));
            }

            //----- Налоговый период
            object oNalogPeriod = _reportHelper.GetFilterFromCondition(criteria, "NalogPeriod");
            if (oNalogPeriod != null)
            {
                int nalogPeriod = _reportHelper.StringToInt((string)oNalogPeriod);
                oraParameters.Add(FormatCommandHelper.NumericIn(pNalogPeriod, nalogPeriod));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pNalogPeriod, DBNull.Value));
            }

            //----- Год
            object oFiscalYear = _reportHelper.GetFilterFromCondition(criteria, "Year");
            if (oFiscalYear != null)
            {
                oraParameters.Add(FormatCommandHelper.NumericIn(pFiscalYear, (int)oFiscalYear));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pFiscalYear, DBNull.Value));
            }

            //----- DateBegin
            OracleParameter paramDateBegin = _reportHelper.GetParametersByFilter(criteria, "DateBegin").FirstOrDefault();
            if (paramDateBegin == null)
            {
                paramDateBegin = new OracleParameter(pDateBegin, DBNull.Value);
            }
            paramDateBegin.ParameterName = pDateBegin;
            oraParameters.Add(paramDateBegin);

            //----- DateEnd
            OracleParameter paramDateEnd = _reportHelper.GetParametersByFilter(criteria, "DateEnd").FirstOrDefault();
            if (paramDateEnd == null)
            {
                paramDateEnd = new OracleParameter(pDateEnd, DBNull.Value);
            }
            paramDateEnd.ParameterName = pDateEnd;
            oraParameters.Add(paramDateEnd);

            //----- DateCreateReport
            OracleParameter paramDateCreateReport = _reportHelper.GetParametersByFilter(criteria, "DateCreateReport").FirstOrDefault();
            if (paramDateCreateReport == null)
            {
                paramDateCreateReport = new OracleParameter(pDateCreateReport, DBNull.Value);
            }
            paramDateCreateReport.ParameterName = pDateCreateReport;
            oraParameters.Add(paramDateCreateReport);

            //----- DeclarationStatisticType
            object declStatType = _reportHelper.GetFilterFromCondition(criteria, "DeclarationStatisticType");
            if (declStatType != null)
            {
                oraParameters.Add(FormatCommandHelper.NumericIn(pDeclStatisticType, (int)declStatType));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pDeclStatisticType, DBNull.Value));
            }

            //----- DeclarationStatisticRegim
            object declStatRegim = _reportHelper.GetFilterFromCondition(criteria, "DeclarationStatisticRegim");
            if (declStatRegim != null)
            {
                oraParameters.Add(FormatCommandHelper.NumericIn(pDeclStatisticRegim, (int)declStatRegim));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pDeclStatisticRegim, DBNull.Value));
            }

            oraParameters.Add(FormatCommandHelper.NumericOut(pTaskId));

            CommandExecuteResult result = Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportSaveTaskProcedure),
                CommandType.StoredProcedure,
                    oraParameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[pTaskId]);
        }

        private long SaveTaskDynamicTechnoReport(QueryConditions criteria, long requestId, int reportNum)
        {
            List<OracleParameter> oraParameters = new List<OracleParameter>();
            OracleParameter paramDynamicParameters = _reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.DynamicParameters, pDynamicParameters, String.Empty);

            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(paramDynamicParameters);

            object dynamicAggregateLevel = _reportHelper.GetFilterFromCondition(criteria, TaskParamCriteria.DynamicAggregateLevel);
            if (dynamicAggregateLevel != null)
            {
                oraParameters.Add(FormatCommandHelper.NumericIn(pDynamicAggregateLevel, (int)dynamicAggregateLevel));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pDynamicAggregateLevel, DBNull.Value));
            }

            object dynamicTimeCounting = _reportHelper.GetFilterFromCondition(criteria, TaskParamCriteria.DynamicTimeCounting);
            if (dynamicTimeCounting != null)
            {
                oraParameters.Add(FormatCommandHelper.NumericIn(pDynamicTimeCounting, (int)dynamicTimeCounting));
            }
            else
            {
                oraParameters.Add(new OracleParameter(pDynamicTimeCounting, DBNull.Value));
            }

            OracleParameter paramDateBegin = _reportHelper.GetParametersByFilter(criteria, TaskParamCriteria.DynamicDateBegin).FirstOrDefault();
            if (paramDateBegin == null)
            {
                paramDateBegin = new OracleParameter(pDateBegin, DBNull.Value);
            }
            paramDateBegin.ParameterName = pDateBegin;
            oraParameters.Add(paramDateBegin);

            oraParameters.Add(FormatCommandHelper.NumericOut(pTaskId));

            CommandExecuteResult result = Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportDynamicTechnoSaveTaskProcedure),
                CommandType.StoredProcedure,
                    oraParameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[pTaskId]);
        }

        private long SaveTaskDynamicDeclReport(QueryConditions criteria, long requestId, int reportNum)
        {
            List<OracleParameter> oraParameters = new List<OracleParameter>();

            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.DDS_DynamicParameters, pDynamicParameters, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.DDS_FederalDistrict, pFederalCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.DDS_TaxPayerRegionCode, pRegionCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.DDS_NalogOrganCode, pSounCodes, String.Empty));
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_AggregateByNalogOrgan, "pAggregateByNalogOrgan");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_AggregateByTime, "pAggregateByTime");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_TypeReport, "pTypeReport");
            AddOracleParam<string>(oraParameters, criteria, TaskParamCriteria.DDS_NalogPeriod, "pNalogPeriod");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_FiscalYear, "pFiscalYear");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.DDS_DateBeginDay, "pDateBeginDay");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.DDS_DateEndDay, "pDateEndDay");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.DDS_DateBeginWeek, "pDateBeginWeek");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.DDS_DateEndWeek, "pDateEndWeek");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_YearBegin, "pYearBegin");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_MonthBegin, "pMonthBegin");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_YearEnd, "pYearEnd");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.DDS_MonthEnd, "pMonthEnd");
            AddOracleParam<string>(oraParameters, criteria, TaskParamCriteria.SonoCode, "pSonoCode");
            oraParameters.Add(FormatCommandHelper.NumericOut(pTaskId));

            CommandExecuteResult result = Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportDynamicDeclSaveTaskProcedure),
                CommandType.StoredProcedure,
                    oraParameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[pTaskId]);
        }

        private long SaveTaskInfoResultsOfMatchingReport(QueryConditions criteria, long requestId, int reportNum)
        {
            List<OracleParameter> oraParameters = new List<OracleParameter>();

            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.IRM_FederalDistrict, pFederalCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.IRM_TaxPayerRegionCode, pRegionCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.IRM_NalogOrganCode, pSounCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.IRM_NalogOrganCheck, "pNalogPeriods", String.Empty));
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.IRM_AggregateByNalogOrgan, "pAggregateByNalogOrgan");
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.IRM_TimePeriod, "pTimePeriod");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.IRM_DateOneDay, "pDateOneDay");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.IRM_DateBegin, "pDateBegin");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.IRM_DateEnd, "pDateEnd");
            oraParameters.Add(FormatCommandHelper.NumericOut(pTaskId));

            CommandExecuteResult result = Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportInfoResMatchSaveTaskProcedure),
                CommandType.StoredProcedure,
                oraParameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[pTaskId]);
        }

        private long SaveTaskMatchingRuleReport(QueryConditions criteria, long requestId, int reportNum)
        {
            List<OracleParameter> oraParameters = new List<OracleParameter>();

            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.MR_FederalDistrict, pFederalCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.MR_TaxPayerRegionCode, pRegionCodes, String.Empty));
            oraParameters.Add(_reportHelper.CreateArrayParameter(criteria, TaskParamCriteria.MR_NalogOrganCode, pSounCodes, String.Empty));
            AddOracleParam<int>(oraParameters, criteria, TaskParamCriteria.IRM_AggregateByNalogOrgan, "pAggregateByNalogOrgan");
            AddOracleParam<DateTime>(oraParameters, criteria, TaskParamCriteria.IRM_DateOneDay, "pDateOneDay");
            oraParameters.Add(FormatCommandHelper.NumericOut(pTaskId));

            CommandExecuteResult result = Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportMatchRuleSaveTaskProcedure),
                CommandType.StoredProcedure,
                oraParameters.ToArray());

            return DataReaderExtension.ReadInt64(result.Output[pTaskId]);
        }

        private void AddOracleParam<T>(List<OracleParameter> oraParameters, QueryConditions criteria, string criteriaName, string paramName)
        {
            object value = _reportHelper.GetFilterFromCondition(criteria, criteriaName);
            if (value != null)
            {
                Type type = typeof(T);
                if (type == typeof(int))
                {
                    oraParameters.Add(FormatCommandHelper.NumericIn(paramName, (int)value));
                }
                else if (type == typeof(string))
                {
                    oraParameters.Add(FormatCommandHelper.VarcharIn(paramName, (string)value));
                }
                else if (type == typeof(DateTime))
                {
                    oraParameters.Add(FormatCommandHelper.DateIn(paramName, (DateTime)value));
                }
            }
            else
            {
                oraParameters.Add(new OracleParameter(paramName, DBNull.Value));
            }
        }

        public TaskStatus GetTaskStatus(long taskId)
        {
            TaskStatus ret = TaskStatus.None;
            int iStatus = -1;
            var result = Execute(
                getTaskStatus,
                CommandType.Text,
                new OracleParameter[] { 
                            FormatCommandHelper.NumericIn(pTaskId, taskId),
                            FormatCommandHelper.NumericOut(pTaskStatus)
                        }
            );
            iStatus = DataReaderExtension.ReadInt(result.Output[pTaskStatus]);
            ret = (TaskStatus)iStatus;
            return ret;
        }

        public void RunCreateReport(int reportNum, long taskId, long requestId)
        {
            List<OracleParameter> oraParameters = new List<OracleParameter>();
            oraParameters.Add(FormatCommandHelper.NumericIn(pReportNum, reportNum));
            oraParameters.Add(FormatCommandHelper.NumericIn(pTaskId, taskId));
            oraParameters.Add(FormatCommandHelper.NumericIn(pRequestId, requestId));

            Execute(
                FormatCommandHelper.ReportPackageProcedure(ReportCreateProcedure),
                CommandType.StoredProcedure,
                    oraParameters.ToArray());
        }

        # endregion

        # region Взаимодействие с СОВ

        public DataRequestStatus Status(long requestId)
        {
            return ExecuteList<DataRequestStatus>(
                string.Format("{0}.{1}", PackageName, GetStatusProcedureName),
                CommandType.StoredProcedure,
                new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(pRequestId, requestId),
                    FormatCommandHelper.Cursor(pData)
                },
                (reader) =>
                {
                    DataRequestStatus ret = new DataRequestStatus();
                    ret.Type = (RequestStatusType)reader.ReadInt(pFieldId);
                    ret.Name = reader.ReadString(pFieldName);
                    return ret;
                }).First();
        }

        # endregion
    }
}
