﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует получение всех расхождений для списка расхождений карточки НД
    /// </summary>
    internal class AllDiscrepancyForDeclarationCardAdapter : ISearchAdapter<DeclarationDiscrepancy> 
    {
        # region Константы

        private const string ViewAlias = "vw";
        private const string RequestIdParam = "pRequestId";
        
        private const string SearchDeclDiscrepancySQLPattern = "SELECT vw.* FROM V$DECL_ALL_DISCREPANCY_LIST vw WHERE vw.REQUESTID = :pRequestId AND {0} {1}";
        private const string CountDeclDiscrepancySQLPattern = "V$DECL_ALL_DISCREPANCY_LIST vw WHERE vw.REQUESTID = :pRequestId AND {0}";

        private const int RequestTypeId = 1;

        # endregion

        protected readonly long _requestId;

        private IServiceProvider _serviceProvider;

        public AllDiscrepancyForDeclarationCardAdapter(IServiceProvider service, long requestId)
        {
            _serviceProvider = service;
            _requestId = requestId;
        }

        protected IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }
        
        private class SearchCommandBuilder : PageSearchQueryCommandBuilder
        {
            private readonly long _requestId;

            public SearchCommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IEnumerable<ColumnSort> orderBy,
                IQueryPatternProvider queryPattern,
                long requestId)
                : base(selectStatementPattern, searchBy, orderBy, queryPattern)
            {
                _requestId = requestId;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericIn(RequestIdParam, _requestId));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountCommandBuilder : GetCountCommandBuilder
        {
            private readonly long _requestId;

            public CountCommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IQueryPatternProvider queryPattern,
                long requestId)
                : base(selectStatementPattern, searchBy, queryPattern)
            {
                _requestId = requestId;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericIn(RequestIdParam, _requestId));
                return base.BuildSelectStatement(parameters);
            }
        }

        public IEnumerable<DeclarationDiscrepancy> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> sortBy, uint rowsToSkip, uint rowsToTake)
        {
            return new ListCommandExecuter<DeclarationDiscrepancy>(
                new GenericDataMapper<DeclarationDiscrepancy>(),
                _serviceProvider)
                .TryExecute(
                    new SearchCommandBuilder(SearchDeclDiscrepancySQLPattern, 
                                                filterBy, 
                                                sortBy,
                                                PatternProvider(ViewAlias),
                                                _requestId)
                    .Take((int)rowsToTake)
                    .Skip((int)rowsToSkip)
                );
        }

        public int Count(FilterExpressionBase filterBy)
        {
            var result = new ResultCommandExecuter(_serviceProvider)
                .TryExecute(
                    new CountCommandBuilder(CountDeclDiscrepancySQLPattern, 
                                            filterBy, 
                                            PatternProvider(ViewAlias),
                                            _requestId));

            return ((OracleDecimal)result[DbConstants.Result]).ToInt32();
        }


    }
}
