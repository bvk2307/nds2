﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business;

using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DiscrepancyDocument.CommandBuilders;
using Luxoft.NDS2.Server.DAL.DiscrepancyDocument.DataMappers;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System.Text;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    class DocumentAdapter : BaseOracleTableAdapter, IDocumentAdapter
    {
        #region Константы

        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CountSQLCommandPattern = "BEGIN SELECT COUNT(*) INTO :pResult FROM ({0}); END;";

        private const string CountSQLPattern = @"BEGIN SELECT COUNT(*) INTO :pResult FROM DOC dc 
                                                    inner join V$Declaration d on dc.ref_entity_id = d.seod_decl_id
                                                    left outer join doc_type dt on dt.id = dc.doc_type 
                                                    left outer join doc_status dst on dst.id = dc.status and dst.doc_type = dc.doc_type
                                                    WHERE 1=1 {0}; END;";
        private const string SearchListSQLPattern = @"SELECT dc.*, d.INN as TaxPayerInn, d.name as TaxPayerName, dt.description as doc_type_name,
                                                    dst.description as statusDesc, d.declaration_version_id
                                                    FROM DOC dc 
                                                    inner join V$Declaration d on dc.ref_entity_id = d.seod_decl_id
                                                    left outer join doc_type dt on dt.id = dc.doc_type 
                                                    left outer join doc_status dst on dst.id = dc.status and dst.doc_type = dc.doc_type
                                                    WHERE 1=1 {0} {1}";

        private const string ViewAlias = "vw";
        private const string CursorAlias = "pCursor";
        private const string CountResult = "pResult";
        private const string ClaimParamName = "pClaimId";
        private const string pDocId = "pDocId";
        private const string SumResult = "pSumResult";
        private const string pStatusCode = "pStatusCode";
        private const string pStatusDesc = "pStatusDesc";
        private const string pInvoiceId = "pInvoiceId";
        private const string pUserComment = "pUserComment";
        private const string pTypeCode = "pTypeCode";
        private const string pDiscrepancyId = "pDiscrepancyId";
        private const string pStageId = "pStageId";
        private const string pInvoiceRowKey = "pInvoiceRowKey";

        private const string AggregateCalculation = @"
begin
  select
    nvl(aggregate_discrepancy.discrepancies_count, 0) as discrepancies_count,
    nvl(aggregate_invoice.invoice_count, 0) as invoice_count,
    nvl(aggregate_discrepancy.amount, 0) as amount,
    nvl(aggregate_discrepancy.amount_pvp, 0) as amount_pvp,
    nvl(aggregate_control_ratio.cr_count, 0) as cr_count
  into :p_discrepancies_count, :p_invoice_count, :p_amount, :p_amount_pvp, :p_cr_count
  from dual d
  left join
  (
    select 
       count(distinct dis.discrepancy_id) as discrepancies_count,
       sum(sov.amnt) as amount,
       sum(sov.amount_pvp) as amount_pvp
    from doc_discrepancy dis 
    left join sov_discrepancy sov on sov.id = dis.discrepancy_id
    where dis.doc_id = :pDocId
  ) aggregate_discrepancy on 1 = 1  
  left join
  (
    select
      count(distinct di.invoice_row_key) as invoice_count
    from doc d
    inner join doc_invoice di on di.doc_id = d.doc_id
    where d.doc_id = :pDocId
  ) aggregate_invoice on 1 = 1
  left join
  (
    select count(1) as cr_count
    from doc d
    inner join control_ratio_doc ccr on ccr.doc_id = d.doc_id
    inner join V$CONTROL_RATIO cr on ccr.cr_id = cr.id
    inner join v$askkontrsoontosh ask_ks on ask_ks.ID = ccr.cr_id and ask_ks.DeleteDate is null
    where d.doc_id = :pDocId and ask_ks.Vypoln <> 1
  ) aggregate_control_ratio on 1 = 1;
end;";

    private const string AggregateCalculationClaimSF = @"
    begin
        select
        nvl(d.discrepancy_count, 0) as discrepancies_count,
        nvl(aggregate_invoice.invoice_count, 0) as invoice_count,
        nvl(d.discrepancy_amount, 0) as amount,
        nvl(d.discrepancy_amount_pvp, 0) as amount_pvp
        into :p_discrepancies_count, :p_invoice_count, :p_amount, :p_amount_pvp
        from doc d
        left join
        (
        select
            d.doc_id,
            count(distinct di.invoice_row_key) as invoice_count
        from doc d
        left join doc_invoice di on di.doc_id = d.doc_id
        where d.doc_id = :pDocId
        group by d.doc_id
        ) aggregate_invoice on aggregate_invoice.doc_id = d.doc_id
        where d.doc_id = :pDocId;
    end;";

        private const string GetDocStatusHistorySql = @"select * from DOC_STATUS_HISTORY dh where dh.doc_id = :pDocId";
        private const string GetDiscrepancySql = @"select id as DiscrepancyId from sov_discrepancy sd where (sd.invoice_rk = :pInvoiceId or sd.invoice_contractor_rk = :pInvoiceId) and sd.type = :pTypeCode";
        private const string GetDiscrepancyIdByInvoiceSql = @"
begin
   select dd.discrepancy_id into :pDiscrepancyId from doc_discrepancy dd 
   join sov_discrepancy sd on sd.id = dd.discrepancy_id
   where dd.row_key = :pInvoiceId and sd.type = :pTypeCode and dd.doc_id = :pDocId;
end;";
        private const string UpdateUserCommentSql = @"BEGIN update DOC set user_comment = :pUserComment where doc_id = :pDocId; END;";
        private const string GetDocumentInfoFromDisrepancySql = @"
select dc.*, d.INN_declarant as TaxPayerInn, d.name_short as TaxPayerName, dt.description as doc_type_name,
dst.description as statusDesc, d.zip as declaration_version_id
from doc dc 
join doc_invoice di on di.doc_id = dc.doc_id and di.invoice_row_key in
(select distinct invoice_rk from sov_discrepancy where id = :pDiscrepancyId)
inner join declaration_active d on d.reg_number = dc.ref_entity_id and d.sono_code = dc.sono_code
left outer join doc_type dt on dt.id = dc.doc_type 
left outer join doc_status dst on dst.id = dc.status and dst.doc_type = dc.doc_type
where (case when (:pStageId = 2 or :pStageId = 3) and (dc.doc_type = 1 or dc.doc_type = 5) then 1 
            when (:pStageId = 4 or :pStageId = 5) and (dc.doc_type = 2 or dc.doc_type = 3) then 1 else 0 end) = 1";


        #endregion

        #region Константы Запрос списка СФ ver 02 (вьюха)

        private const string InvoiceSearchSql = @"SELECT * FROM V$DOC_INVOICE inv WHERE inv.doc_id = :pDocId {0}";
        private const string InvoiceCountSql = @"BEGIN SELECT COUNT(*) INTO :pResult FROM V$DOC_INVOICE inv WHERE inv.doc_id = :pDocId {0}; END;";
        private const string InvoiceSearchCommonSql = @"SELECT * FROM V$DOC_INVOICE_LIGHT inv WHERE inv.doc_id = :pDocId {0} {1}";
        private const string InvoiceCountCommonSql = @"BEGIN SELECT COUNT(*) INTO :pResult FROM V$DOC_INVOICE_LIGHT inv WHERE inv.doc_id = :pDocId {0}; END;";

        private const string DiscrepancyStatusSql = @"select * from v$discrepancy_status vw where {0}";

        private const int DiscrepancyNdsType = 4;
        private const int DiscrepancyGapType = 1;

        #endregion

        public DocumentAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public PageResult<DiscrepancyDocumentInfo> DocumentList(QueryConditions criteria)
        {
            var query = criteria.ToSQL(SearchListSQLPattern, "d", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<DiscrepancyDocumentInfo>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadDocumentInfo);

            query = criteria.ToSQL(CountSQLPattern, "d", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = CountResult });

            var result = Execute(query);

            return new PageResult<DiscrepancyDocumentInfo>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }

        public PageResult<DocumentKNP> DocumentKnpList(QueryConditions criteria)
        {
            var query = criteria.ToSQL("select * from V$DOC_KNP t where 1=1 {0} order by LEVEL1_DATE, LEVEL1_NUM, LEVEL2_DATE, LEVEL2_NUM, LEVEL3_DATE, LEVEL3_NUM", "t", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<DocumentKNP>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadDocumentKNP);

            query = criteria.ToSQL("BEGIN SELECT COUNT(*) INTO :pResult from V$DOC_KNP t where 1=1 {0}; END;", "t", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = CountResult });

            var result = Execute(query);

            return new PageResult<DocumentKNP>(
                data,
                DataReaderExtension.ReadInt(result.Output[CountResult]));
        }


        public DiscrepancyDocumentInfo GetDocument(long id)
        {
            DiscrepancyDocumentInfo ret = null;
            
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_service))
            {

                ret = new ListCommandExecuter<DiscrepancyDocumentInfo>(
                new DiscrepancyDocumentInfoDataMapper(),
                _service,
                connectionFactory).TryExecute(
                    new DiscrepancyDocumentInfoBuilder(id)).Single();
            }
            
            return ret;
        }

        public PageResult<DiscrepancyDocumentInvoice> GetInvoices(long docId, QueryConditions criteria)
        {
            var query = criteria.ToSQL(InvoiceSearchCommonSql, "inv", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoices = ExecuteList<DiscrepancyDocumentInvoice>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadDiscrepancyDocumentInvoice);

            FillDiscrepancyStatus(docId, invoices);

            int invoiceCount = invoices.Count;
            int pageSize = criteria.PageSize;
            uint pageIndex = criteria.PageIndex;
            if ((pageIndex > 0) || (pageIndex == 0 && invoiceCount >= pageSize))
            {
                var queryCount = criteria.ToSQL(InvoiceCountCommonSql, "inv", false, true);

                var parametersCount = queryCount.Parameters.Select(p => p.Convert()).ToList();
                parametersCount.Add(FormatCommandHelper.NumericIn(pDocId, docId));
                parametersCount.Add(FormatCommandHelper.NumericOut(CountResult));

                var result = Execute(queryCount.Text,
                    CommandType.Text,
                    parametersCount.ToArray());
                invoiceCount = DataReaderExtension.ReadInt(result.Output[CountResult]);
            }

            return new PageResult<DiscrepancyDocumentInvoice>(invoices, invoiceCount);
        }

        private void FillDiscrepancyStatus(long docId, List<DiscrepancyDocumentInvoice> invoices)
        {
            var rowKeyArray = invoices
                .Select(v => v.ROW_KEY)
                .Union(invoices.Select(v => v.ACTUAL_ROW_KEY))
                .Where(v => v != null)
                .ToArray();

            var statusInfoList = GetDiscrepancyStatus(docId, rowKeyArray);
            var statusInfoByRowKey = statusInfoList.ToLookup(v => v.RowKey);

            invoices.ForEach(item =>
            {
                var tmp = statusInfoByRowKey[item.ROW_KEY].Union(statusInfoByRowKey[item.ACTUAL_ROW_KEY]).ToArray();

                item.NDS_STATUS = tmp.Where(v => v.DiscrepancyType == DiscrepancyNdsType).Select(v => v.DiscrepancyStatus).Max();
                item.GAP_STATUS = tmp.Where(v => v.DiscrepancyType == DiscrepancyGapType).Select(v => v.DiscrepancyStatus).Max();
            });
        }

        private List<DiscrepancyStatusInfo> GetDiscrepancyStatus(long docId, string[] rowKeyArray)
        {
            var filter = FilterExpressionCreator.CreateGroup();
            filter.WithExpression(FilterExpressionCreator.Create("DocumentId", ColumnFilter.FilterComparisionOperator.Equals, docId));
            filter.WithExpression(FilterExpressionCreator.CreateInList("RowKey", rowKeyArray));

            var query = filter.ToSQL(DiscrepancyStatusSql, new PatternProvider("vw"));

            var result = ExecuteList(query.Text, CommandType.Text, query.Parameters.Select(v => v.Convert()).ToArray(), DataReaderExtension.ReadDiscrepancyStatusInfo);
            return result;
        }

        public PageResult<InvoiceRelated> GetInvoicesRelated(long docId, QueryConditions criteria)
        {
            var query = criteria.ToSQL(InvoiceSearchSql, "inv", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoices = ExecuteList(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadInvoiceRelated);

            int invoiceCount = invoices.Count;
            int pageSize = criteria.PageSize;
            uint pageIndex = criteria.PageIndex;
            if ((pageIndex > 0) || (pageIndex == 0 && invoiceCount >= pageSize))
            {
                var queryCount = criteria.ToSQL(InvoiceCountSql, "inv", false, true);

                var parametersCount = queryCount.Parameters.Select(p => p.Convert()).ToList();
                parametersCount.Add(FormatCommandHelper.NumericIn(pDocId, docId));
                parametersCount.Add(FormatCommandHelper.NumericOut(CountResult));

                var result = Execute(queryCount.Text,
                    CommandType.Text,
                    parametersCount.ToArray());
                invoiceCount = DataReaderExtension.ReadInt(result.Output[CountResult]);
            }

            return new PageResult<InvoiceRelated>(invoices, invoiceCount);
        }

        public DocumentCalculateInfo GetDocumentCalculateInfo(long docId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(pDocId, docId),
                FormatCommandHelper.NumericOut("p_discrepancies_count"),
                FormatCommandHelper.NumericOut("p_invoice_count"),
                FormatCommandHelper.NumericOut("p_amount"),
                FormatCommandHelper.NumericOut("p_amount_pvp"),
                FormatCommandHelper.NumericOut("p_cr_count")
            };

            var commandResult = Execute(AggregateCalculation, CommandType.Text, parameters);
            return new DocumentCalculateInfo
            {
                CountDiscrepancy = DataReaderExtension.ReadInt64(commandResult.Output["p_discrepancies_count"]),
                CountInvoice = DataReaderExtension.ReadInt64(commandResult.Output["p_invoice_count"]),
                CountDiscrepancyControlRatio = DataReaderExtension.ReadInt64(commandResult.Output["p_cr_count"]),
                SumAmountDiscrepancy = DataReaderExtension.ReadDecimal(commandResult.Output["p_amount"]),
                SumPVPAmountDiscrepancy = DataReaderExtension.ReadDecimal(commandResult.Output["p_amount_pvp"])
            };
        }

        public DocumentCalculateInfo GetDocumentCalculateInfoClaimSF(long docId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(pDocId, docId),
                FormatCommandHelper.NumericOut("p_discrepancies_count"),
                FormatCommandHelper.NumericOut("p_invoice_count"),
                FormatCommandHelper.NumericOut("p_amount"),
                FormatCommandHelper.NumericOut("p_amount_pvp")
            };

            var commandResult = Execute(AggregateCalculationClaimSF, CommandType.Text, parameters);
            return new DocumentCalculateInfo
            {
                CountDiscrepancy = DataReaderExtension.ReadInt64(commandResult.Output["p_discrepancies_count"]),
                CountInvoice = DataReaderExtension.ReadInt64(commandResult.Output["p_invoice_count"]),
                SumAmountDiscrepancy = DataReaderExtension.ReadDecimal(commandResult.Output["p_amount"]),
                SumPVPAmountDiscrepancy = DataReaderExtension.ReadDecimal(commandResult.Output["p_amount_pvp"])
            };
        }

        public DocumentCalculateInfo GetDocumentCalculateInfoReclaim(long docId)
        {
            return GetDocumentCalculateInfoClaimSF(docId);
        }

        public List<DocumentStatusHistory> GetStatusHistories(long docId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var docStatusHistories = ExecuteList<DocumentStatusHistory>(
                string.Format(SearchSQLCommandPattern, GetDocStatusHistorySql),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadDocumentStatusHistory);

            return docStatusHistories;
        }

        public long GetDiscrepancyId(string invoiceId, int typeCode, long docId)
        {
            var parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(pInvoiceId, invoiceId));
            parameters.Add(FormatCommandHelper.NumericIn(pTypeCode, typeCode));
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.NumericOut(pDiscrepancyId));

            var commandResult = Execute(
                GetDiscrepancyIdByInvoiceSql,
                CommandType.Text,
                parameters.ToArray());

            var discrepancyId = DataReaderExtension.ReadInt64(commandResult.Output[pDiscrepancyId]);
            return discrepancyId;
        }

        public Discrepancy GetDiscrepancy(string invoiceId, int typeCode)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn(pInvoiceId, invoiceId));
            parameters.Add(FormatCommandHelper.NumericIn(pTypeCode, typeCode));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var result = ExecuteList<Discrepancy>(
                string.Format(SearchSQLCommandPattern, GetDiscrepancySql),
                CommandType.Text,
                parameters.ToArray(),
                reader =>
                {
                    return new Discrepancy()
                    {
                        DiscrepancyId = reader.ReadInt64("DiscrepancyId"),
                    };
                });

            return result.FirstOrDefault();
        }

        public void UpdateUserComment(long docId, string userComment)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.VarcharIn(pUserComment, userComment));

            Execute(UpdateUserCommentSql,
                CommandType.Text,
                parameters.ToArray());
        }

        public DiscrepancyDocumentInfo GetDiscrepancyDocumentInfo(long discrepancyId, int stageId)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn(pDiscrepancyId, discrepancyId));
            parameters.Add(FormatCommandHelper.NumericIn(pStageId, stageId));

            var result = ExecuteList<DiscrepancyDocumentInfo>(
                GetDocumentInfoFromDisrepancySql,
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadDocumentInfo);

            return result.FirstOrDefault();
        }

        private const string CountNotRelatedInvoiceSql = "BEGIN SELECT COUNT(*) INTO :pResult FROM ({0}); END;";

        private const string NotRelatedInvoiceSearchSql = "SELECT * FROM V$NOT_REFLECTED_INVOICE vw WHERE vw.doc_id = :pDocId {0} {1}";
/*@"SELECT * FROM (SELECT 0 as DECLARATION_VERSION_ID, 
'1234567890' as INN, '3216549870' as KPP, 'КОНТРАГЕНТ' as NAME,
0 as INVOICE_NUM, sysdate as INVOICE_DATE, 0 as DISCREPANCY_STATUS, :pDocId as doc_id
FROM DUAL) inv WHERE inv.doc_id = :pDocId {0} {1}";*/

        public PageResult<NotReflectedInvoice> GetNotRefectedInvoices(long docId, QueryConditions criteria)
        {
            var query = criteria.ToSQL(NotRelatedInvoiceSearchSql, "vw", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var invoices = ExecuteList(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                DataReaderExtension.ReadNotReflectedInvoice);

            query = criteria.ToSQL(NotRelatedInvoiceSearchSql, "vw", false, true);
            var parametersCount = query.Parameters.Select(p => p.Convert()).ToList();
            parametersCount.Add(FormatCommandHelper.NumericIn(pDocId, docId));
            parametersCount.Add(FormatCommandHelper.NumericOut(CountResult));

            var result = Execute(string.Format(CountNotRelatedInvoiceSql, query.Text),
                CommandType.Text,
                parametersCount.ToArray());

            return new PageResult<NotReflectedInvoice>(
                    invoices,
                    DataReaderExtension.ReadInt(result.Output[CountResult]));            
        }
    }
}