﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс реализует интерфейс IDeclarationBriefAdapter
    /// </summary>
    internal class DeclarationBriefAdapter : BaseOracleTableAdapter, IDeclarationBriefAdapter
    {
        # region Константы

        private const string SCOPE = "DeclarationBrief";
        private const string SEARCH_PATTERN = "SEARCH_PATTERN";
        private const string CURSOR_ALIAS = "CURSOR_ALIAS";
        private const string RESULT_ALIAS = "RESULT_ALIAS";
        private const string VIEW_ALIAS = "VIEW_ALIAS";

        private const string QUERY_PATTERN = "QUERY_PATTERN";
        private const string COUNT_PATTERN = "COUNT_PATTERN";
        private const string PACKAGE_NAME = "PAC$REMARKABLE_CHANGES";
        private const string SQL_PROC_RESET_HAS_AT = "RESET_HAS_AT"; // Сброс Признака Значимых Изменений одновременно с признаком "Р"
        private const string SQL_PROC_NO_TKS_DOWN_VERSION = "NO_TKS_DOWN_VERSION";

        # endregion

        # region Конструкторы

        public DeclarationBriefAdapter(IServiceProvider service) : base(service) { }

        # endregion

        # region Реализация интерфейса IDeclarationBriefAdapter

        public List<DeclarationBrief> Search(QueryConditions criteria)
        {
            var queryPatternText = _service.GetQueryText(SCOPE, QUERY_PATTERN);
            var viewAliasText = _service.GetQueryText(SCOPE, VIEW_ALIAS);
            var cursorsAliasText = _service.GetQueryText(SCOPE, CURSOR_ALIAS);
            var searchPatternText = _service.GetQueryText(SCOPE, SEARCH_PATTERN);

            #region Принудительная сортировка
            // https://sentinel2.luxoft.com/sen/issues/browse/GNIVC_NDS2-2163

            if (criteria.Sorting == null)
                criteria.Sorting = new List<ColumnSort>();
            if (criteria.Sorting.Count == 0)
                criteria.Sorting.Add(new ColumnSort()
                {
                    ColumnKey = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_DATE),
                    Order = ColumnSort.SortOrder.Asc
                });

            #endregion

            var query = criteria.ToSQL(queryPatternText, viewAliasText, false, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();

            var rowFrom = (int)(criteria.PageIndex - 1) * criteria.PageSize;
            var rowTo = (int)criteria.PageIndex * criteria.PageSize;

            var execSettings = ExecuteSettings.Default;
            execSettings.ExplicitSkipOptions.From = rowFrom;


            parameters.Add(new OracleParameter("rnNext", OracleDbType.Int32, 0, rowTo, ParameterDirection.Input));

            parameters.Add(FormatCommandHelper.Cursor(cursorsAliasText));

            return ExecuteList(
                string.Format(searchPatternText, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildDeclaration, execSettings);
        }

        public Dictionary<string, string> GetTipsForMarks()
        {
            var result = new Dictionary<string, string>();
            ExecuteList(
                "SELECT * FROM DICT_REMARKABLE_CHANGES",
                CommandType.Text,
                new OracleParameter[] { },
                reader => result[reader.ReadString("NAME")] = reader.ReadString("DESCRIPTION"));
            return result;
        }

        public int Count(QueryConditions criteria)
        {
            var countPatternText = _service.GetQueryText(SCOPE, COUNT_PATTERN);
            var resultAliasText = _service.GetQueryText(SCOPE, RESULT_ALIAS);
            var viewAliasText = _service.GetQueryText(SCOPE, VIEW_ALIAS);
            var query = criteria.ToSQL(countPatternText, viewAliasText, false, true);
            var result = 0;

            query.Parameters.Add(new QueryParameter(false) { Name = resultAliasText });

            var cachedResult = base.GetQueryListRowCountCache(QueryListScope.InspectorDeclarationList, query.Parameters, query.Text);

            if (cachedResult.Exists)
            {
                result = cachedResult.RowCount;
            }
            else
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                result = DataReaderExtension.ReadInt(Execute(query).Output[resultAliasText]);
                sw.Stop();
                base.SetQueryListRowCountCache(QueryListScope.InspectorDeclarationList, result, sw.ElapsedMilliseconds, cachedResult.ArgsHashInfo);
            }

            return result;
        }

        /// <summary>
        /// Сброс признака значимых изменений
        /// </summary>
        /// <param name="innContractor">ИНН НП</param>
        /// <param name="kppEffective">Эффективный КПП </param>
        /// <param name="type">Тип декларации (декларация/журнал)</param>
        /// <param name="period">Налоговый период</param>
        /// <param name="year">Год</param>
        public void ResetMark(string innContractor, string kppEffective, int type, string period, string year)
        {
            var res = Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, SQL_PROC_RESET_HAS_AT),
                CommandType.StoredProcedure,
                new[]
                {
                   new OracleParameter("P_INN_CONTRACTOR",OracleDbType.Varchar2) { Value = innContractor}, 
                   new OracleParameter("P_KPP_EFFECTIVE",OracleDbType.Varchar2) { Value = kppEffective}, 
                   new OracleParameter("P_TYPE",OracleDbType.Varchar2) { Value = type}, 
                   new OracleParameter("P_PERIOD",OracleDbType.Varchar2) { Value = period}, 
                   new OracleParameter("P_YEAR",OracleDbType.Varchar2) { Value = year}
                });
        }

        /// <summary>
        /// Уменьшает на 1 счетчик необработанных неформализованных пояснений (т.е. полученных не по ТКС)
        /// </summary>
        /// <param name="zip">ZIP налоговой декларации</param>
        public void DownNoTks(long zip)
        {
            Execute(
                string.Format("{0}.{1}", PACKAGE_NAME, SQL_PROC_NO_TKS_DOWN_VERSION),
                CommandType.StoredProcedure,
                parameters: new[] { FormatCommandHelper.NumericIn("P_ZIP", zip) });
        }

        # endregion

        # region Построение объекта Declaration

        private DeclarationBrief BuildDeclaration(OracleDataReader reader)
        {
            var obj = new DeclarationBrief();

            obj.ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.ID));
            obj.DECLARATION_VERSION_ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECLARATION_VERSION_ID));
            obj.SEOD_DECL_ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SEOD_DECL_ID));
            obj.RECORD_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.RECORD_MARK));
            obj.HAS_AT = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_AT));
            obj.SLIDE_AT_COUNT = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SLIDE_AT_COUNT)).GetValueOrDefault(0);
            obj.NO_TKS_COUNT = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NO_TKS_COUNT)).GetValueOrDefault(0);
            obj.HAS_CHANGES = GetHasChanges(reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_CHANGES)));

            obj.ProcessingStage = (DeclarationProcessignStage)reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.ProcessingStage));
            obj.LOAD_MARK = (DeclarationProcessignStage)reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.LOAD_MARK));
            obj.DECL_TYPE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_TYPE));
            obj.CANCELLED = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CANCELLED));
            obj.HAS_CANCELLED_CORRECTION = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_CANCELLED_CORRECTION));
            obj.DECL_TYPE_CODE = reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_TYPE_CODE));
            obj.STATUS = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.STATUS));
            obj.STATUS_KNP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.STATUS_KNP));
            obj.SUR_CODE = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUR_CODE));
            obj.INN = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INN));
            obj.KPP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.KPP));
            obj.NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NAME));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.TAX_PERIOD));
            obj.FISCAL_YEAR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.FISCAL_YEAR));
            obj.FullTaxPeriod = new FullTaxPeriodInfo() { TaxPeriod = obj.TAX_PERIOD, FiscalYear = obj.FISCAL_YEAR, Description = obj.FULL_TAX_PERIOD };
            obj.DECL_DATE = reader.ReadDateTime(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_DATE));
            obj.SUBMISSION_DATE = reader.ReadDateTime(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUBMISSION_DATE));
            obj.CORRECTION_NUMBER = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CORRECTION_NUMBER));
            obj.SUBSCRIBER_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUBSCRIBER_NAME));
            obj.PRPODP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.PRPODP));
            obj.NDS_CHAPTER8 = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_CHAPTER8));
            obj.NDS_CHAPTER9 = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_CHAPTER9));
            obj.NDS_WEIGHT = reader.ReadNullableInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_WEIGHT));
            obj.NDS_AMOUNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_AMOUNT));
            obj.COMPENSATION_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.COMPENSATION_AMNT));
            obj.DECL_SIGN = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_SIGN));

            obj.CHAPTER8_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_MARK));
            obj.CHAPTER8_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_CONTRAGENT_CNT));
            obj.CHAPTER8_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_CNT));
            obj.CHAPTER8_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_CONTRAGENT_CNT));
            obj.CHAPTER8_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_AMNT));
            obj.CHAPTER8_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_CNT));
            obj.CHAPTER8_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER8_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_AMNT));

            obj.CHAPTER9_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_MARK));
            obj.CHAPTER9_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_CONTRAGENT_CNT));
            obj.CHAPTER9_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_CNT));
            obj.CHAPTER9_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_CONTRAGENT_CNT));
            obj.CHAPTER9_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_AMNT));
            obj.CHAPTER9_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_CNT));
            obj.CHAPTER9_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER9_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_AMNT));

            obj.CHAPTER10_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_MARK));
            obj.CHAPTER10_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_CONTRAGENT_CNT));
            obj.CHAPTER10_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_CNT));
            obj.CHAPTER10_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_CONTRAGENT_CNT));
            obj.CHAPTER10_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_AMNT));
            obj.CHAPTER10_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_CNT));
            obj.CHAPTER10_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER10_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_AMNT));

            obj.CHAPTER11_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_MARK));
            obj.CHAPTER11_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_CONTRAGENT_CNT));
            obj.CHAPTER11_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_CNT));
            obj.CHAPTER11_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_CONTRAGENT_CNT));
            obj.CHAPTER11_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_AMNT));
            obj.CHAPTER11_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_CNT));
            obj.CHAPTER11_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER11_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_AMNT));

            obj.CHAPTER12_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_MARK));
            obj.CHAPTER12_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_CONTRAGENT_CNT));
            obj.CHAPTER12_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_CNT));
            obj.CHAPTER12_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_CONTRAGENT_CNT));
            obj.CHAPTER12_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_AMNT));
            obj.CHAPTER12_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_CNT));
            obj.CHAPTER12_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER12_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_AMNT));
            if (obj.DECL_TYPE_CODE == 0 && reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CATEGORY)) == 1)
            {
                obj.CATEGORY = 1;
            }
            obj.INSPECTOR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR));
            obj.INSPECTOR_SID = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID));
            obj.SOUN_CODE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_CODE));
            obj.SOUN_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_NAME));
            obj.REGION_CODE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.REGION_CODE));
            obj.REGION_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.REGION_NAME));

            return obj;
        }

        private DeclarationHasChangesType GetHasChanges(bool hasChanges)
        {
            return ((hasChanges) ? (DeclarationHasChangesType.Changes) : (DeclarationHasChangesType.NoChanges));
        }

        # endregion
    }
}