﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    /// <summary>
    /// Этот класс представляет абстрактный адаптер слоя доступа к данным
    /// </summary>
    internal abstract class BaseOracleTableAdapter_New
    {
        private const string CommonErrorDbMessagePattern = "Ошибка на стороне БД: {0}";

        private readonly IServiceProvider _service;

        protected BaseOracleTableAdapter_New(IServiceProvider service)
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }
            _service = service;
        }

        /// <summary>
        /// Создает экземпляр соединения с БД
        /// </summary>
        /// <returns>Ссылка на соединение с БД</returns>
        private OracleConnection GetConnection()
        {
            return new OracleConnection(_service.GetConfigurationValue(Constants.DB_CONFIG_KEY));
        }


        protected CommandExecuteResult Execute(string commandText, CommandType commandType, IEnumerable<OracleParameter> parameters)
        {
            var context = new CommandContext
            {
                Text = commandText,
                Type = commandType,
                Parameters = new List<OracleParameter>(parameters)
            };
            return Execute(context);
        }

        /// <summary>
        /// Выполняет запрос к БД
        /// </summary>
        /// <param name="context">Контекст команды</param>
        protected CommandExecuteResult Execute(CommandContext context)
        {
            using (var connection = GetConnection())
            {
                return WrapDbOperation(ExecuteInternal, context, connection);
            }
        }

        protected List<T> ExecuteList<T>(string commandText, CommandType commandType, IEnumerable<OracleParameter> parameters, QueryConditions conditions = null) where T : new()
        {
            var context = new CommandContext
            {
                Text = commandText,
                Type = commandType,
                Parameters = new List<OracleParameter>(parameters)
            };
            return ExecuteList<T>(context, conditions);
        }

        protected List<T> ExecuteList<T>(CommandContext context, QueryConditions conditions = null) where T : new()
        {
            return ExecuteListBase<T>(ExecuteListInternal<T>, context, conditions);
        }

        protected List<T> ExecutePrimitiveList<T>(string commandText, CommandType commandType, IEnumerable<OracleParameter> parameters, QueryConditions conditions = null) where T : new()
        {
            var context = new CommandContext
            {
                Text = commandText,
                Type = commandType,
                Parameters = new List<OracleParameter>(parameters)
            };
            return ExecutePrimitiveList<T>(context, conditions);
        }

        protected List<T> ExecutePrimitiveList<T>(CommandContext context, QueryConditions conditions = null) where T : new()
        {
            return ExecuteListBase<T>(ExecutePrimitiveListInternal<T>, context, conditions);
        }

        private List<T> ExecuteListBase<T>(Func<CommandContext, OracleConnection, List<T>> operation, CommandContext context, QueryConditions conditions) where T : new()
        {
            if (conditions != null)
            {
                conditions.ApplyConditions(context, false);
            }
            using (var connection = GetConnection())
            {
                return WrapDbOperation(operation, context, connection);
            }
        }

        protected CommandExecuteResult[] Transaction(CommandContext[] contexts)
        {
            var result = new CommandExecuteResult[contexts.Length];
            using (var connection = GetConnection())
            {
                try
                {
                    connection.Open();
                }
                catch (OracleException oraEx)
                {
                    throw BuildException(oraEx);
                }

                using (var transaction = connection.BeginTransaction())
                {
                    Exception exception = null;
                    for (int i = 0; i < contexts.Length; i++)
                    {
                        try
                        {
                            result[i] = ExecuteInternal(contexts[i], connection);
                        }
                        catch (OracleException oraEx)
                        {
                            exception = BuildException(oraEx);
                            break;
                        }
                        catch (Exception ex)
                        {
                            exception = ex;
                            break;
                        }
                    }
                    if (exception != null)
                    {
                        transaction.Rollback();
                        connection.Close();
                        throw exception;
                    }
                    else
                    {
                        transaction.Commit();
                    }
                }
                connection.Close();
            }
            return result;
        }

        private CommandExecuteResult ExecuteInternal(CommandContext context, OracleConnection connection)
        {
            var result = new CommandExecuteResult();
            var opend = connection.State == ConnectionState.Open;
            //context.Parameters = context.Parameters ?? Enumerable.Empty<OracleParameter>();
            context.Parameters = context.Parameters ?? new List<OracleParameter>();


            var traceInfo = new List<KeyValuePair<string, object>>();

            if (!opend)
            {
                connection.Open();
            }
            using (var command = GetCommand(context, connection))
            {
                result.AffectedRows = command.ExecuteNonQuery();
                traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, command.CommandText));

                foreach (OracleParameter parameter in command.Parameters)
                {
                    traceInfo.Add(new KeyValuePair<string, object>(parameter.ParameterName, parameter.Value));

                    if (parameter.Direction == ParameterDirection.Output)
                    {
                        result.Output[parameter.ParameterName] = parameter.Value;
                    }
                }

                _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);

                if (context.AfterExecuted != null)
                {
                    context.AfterExecuted(result);
                }
            }
            if (!opend)
            {
                connection.Close();
            }
            return result;
        }

        private List<T> ExecuteListInternal<T>(CommandContext context, OracleConnection connection) where T : new()
        {
            Func<IDataReader, T> readFunc = reader => reader.Read<T>(_service);
            return ExecuteListInternalBase<T>(readFunc, context, connection);
        }

        private List<T> ExecutePrimitiveListInternal<T>(CommandContext context, OracleConnection connection) where T : new()
        {
            Func<IDataReader, T> readFunc = reader => reader.ReadPrimitive<T>(_service);
            return ExecuteListInternalBase<T>(readFunc, context, connection);
        }

        private List<T> ExecuteListInternalBase<T>(Func<IDataReader, T> readFunc, CommandContext context, OracleConnection connection) where T : new()
        {
            var result = new List<T>();
            var traceInfo = new List<KeyValuePair<string, object>>();
            using (var command = GetCommand(context, connection))
            {
                #region trace info
                connection.Open();
                foreach (OracleParameter parameter in command.Parameters)
                {
                    traceInfo.Add(new KeyValuePair<string, object>(parameter.ParameterName, parameter.Value));
                }

                traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, command.CommandText));
                #endregion trace info
                

                var sw = new Stopwatch();
                sw.Start();
                try
                {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(readFunc(reader));
                    }
                }


                    sw.Stop();
                    traceInfo.Add(new KeyValuePair<string, object>("Elapsed time", sw.ElapsedMilliseconds));
                _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
            }
                catch(Exception ex)
                {
                    sw.Stop();
                    traceInfo.Add(new KeyValuePair<string, object>("Elapsed time", sw.ElapsedMilliseconds));
                    _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, "ExecuteList", ex, traceInfo);
                }
                
            }
            return result;
        }

        private T WrapDbOperation<T>(Func<CommandContext, OracleConnection, T> operation, CommandContext context, OracleConnection connection)
        {
            try
            {
                return operation(context, connection);
            }
            catch (OracleException oraEx)
            {
                throw BuildException(oraEx);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }

        private OracleCommand GetCommand(CommandContext context, OracleConnection connection = null)
        {
            var result = new OracleCommand
            {
                CommandText = context.Text,
                Connection = connection,
                CommandType = context.Type,
                BindByName = true
            };
            result.Parameters.AddRange(context.Parameters.ToArray());
            return result;
        }

        protected virtual DatabaseException BuildException(OracleException oraEx)
        {
            return new DatabaseException(string.Format(CommonErrorDbMessagePattern, oraEx.Message), oraEx);
        }
    }
}