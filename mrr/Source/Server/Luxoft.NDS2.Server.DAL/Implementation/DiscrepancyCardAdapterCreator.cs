﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public static class DiscrepancyCardAdapterCreator
    {
        public static IHiveRequestAdapter HiveRequestAdapter(IServiceProvider service, HiveRequestDiscrepancyCard hiveRequest)
        {
            return new HiveRequestAdapter(service, hiveRequest);
        }

        
    }
}
