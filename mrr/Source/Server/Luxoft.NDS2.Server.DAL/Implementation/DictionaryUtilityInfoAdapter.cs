﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class DictionaryUtilityInfoAdapter : BaseOracleTableAdapter, IDictionaryUtilityInfoAdapter
    {
        # region Константы

        private const string UserSIDParam = "USERSID";

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса UserToRegionAdapter
        /// </summary>
        /// <param name="service"></param>
        public DictionaryUtilityInfoAdapter(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        # region Реализация интерфейса IUserToRegionAdapter

        public List<TableDictionaryColumnInfo> GetTablesDictionaryColumnInfo()
        {
            List<TableDictionaryColumnInfo> tablesDictionaryInfo = ExecuteList<TableDictionaryColumnInfo>(
                @"select c.*, utc.DATA_TYPE from DICTIONARY_COLUMN_SETTINGS c
                join DICTIONARY_TABLE_SETTINGS t on (t.name = c.TABLE_NAME)
                left outer join user_tab_columns utc on (utc.TABLE_NAME = c.TABLE_NAME) 
                     and utc.COLUMN_NAME = c.COLUMN_NAME
                WHERE t.Iseditable = 1",
                CommandType.Text,
                new OracleParameter[] { 
                },
                BuildTableDictionaryColumnInfo);

            return tablesDictionaryInfo;
        }

        public List<TableDictionaryInfo> GetTablesDictionaryInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from DICTIONARY_TABLE_SETTINGS WHERE ISEDITABLE = 1");
            List<TableDictionaryInfo> tablesDictionaryInfo = ExecuteList<TableDictionaryInfo>(
                sb.ToString(),
                CommandType.Text,
                new OracleParameter[] { 
                },
                BuildTableDictionaryInfo);

            return tablesDictionaryInfo;
        }

        private List<string> _columns;

        public List<Dictionary<string, object>> GetTableRows(string tableName, List<string> columns)
        {
            _columns = columns;
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from ");
            sb.Append(tableName);
            List<Dictionary<string, object>> tableRows = ExecuteList<Dictionary<string, object>>(
                sb.ToString(),
                CommandType.Text,
                new OracleParameter[] { 
                },
                BuildTableRows);

            return tableRows;
        }

        public void UpdateTableRow(string tableName, string key, string value, Dictionary<string, string> row)
        {
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value) && row.Count > 0)
            {
                OracleParameter[] oraParameters = new OracleParameter[row.Count + 1];

                StringBuilder sb = new StringBuilder();
                sb.Append("update ");
                sb.Append(tableName);
                sb.Append(" set ");
                bool isFirst = true;
                int i = 0;
                string parameters = String.Empty;
                foreach (KeyValuePair<string, string> item in row)
                {
                    if (!isFirst)
                    {
                        sb.Append(",");
                    }
                    sb.Append(item.Key);
                    sb.Append("=");
                    parameters = string.Format(":{0}", i + 1);
                    sb.Append(parameters);
                    oraParameters[i] = FormatCommandHelper.VarcharIn(parameters, item.Value);
                    isFirst = false;
                    i++;
                }
                sb.Append(" where ");
                sb.Append(key);
                parameters = string.Format(":{0}", i + 1);
                sb.Append("= ");
                sb.Append(parameters);
                oraParameters[i] = FormatCommandHelper.VarcharIn(parameters, value);
                Execute(
                    sb.ToString(),
                    CommandType.Text,
                    oraParameters);
            }
        }

        public void AddTableRow(string tableName, Dictionary<string, string> row)
        {
            OracleParameter[] oraParameters = new OracleParameter[row.Count];

            StringBuilder sb = new StringBuilder();
            sb.Append("insert into ");
            sb.Append(tableName);
            sb.Append(" (");
            bool isFirst = true;
            foreach (KeyValuePair<string, string> item in row)
            {
                if (!isFirst)
                {
                    sb.Append(",");
                }
                sb.Append(item.Key);
                isFirst = false;
            }
            sb.Append(") ");
            sb.Append("values(");
            isFirst = true;
            int i = 0;
            string parameters = String.Empty;
            foreach (KeyValuePair<string, string> item in row)
            {
                if (!isFirst)
                {
                    sb.Append(",");
                }
                sb.Append(" ");
                parameters = string.Format(":{0}", i + 1);
                sb.Append(parameters);
                oraParameters[i] = FormatCommandHelper.VarcharIn(parameters, item.Value);
                sb.Append(" ");
                isFirst = false;
                i++;
            }
            sb.Append(" )");
            Execute(
                sb.ToString(),
                CommandType.Text,
                oraParameters
                );
        }

        public void DeleteTableRow(string tableName, string key, string value)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("delete from ");
            sb.Append(tableName);
            sb.Append(" where ");
            sb.Append(key);
            sb.Append(" = :1");
            Execute(
                sb.ToString(),
                CommandType.Text,
                new OracleParameter[] { 
                    FormatCommandHelper.VarcharIn(":1", value)
                });
        }

        # endregion

        # region Построение объектов

        private TableDictionaryColumnInfo BuildTableDictionaryColumnInfo(OracleDataReader reader)
        {
            TableDictionaryColumnInfo ret = new TableDictionaryColumnInfo();

            ret.TableName = reader.ReadString("TABLE_NAME");
            ret.TableNameEdit = reader.ReadString("Table_Name_Edit");
            ret.ColumnName = reader.ReadString("COLUMN_NAME");
            ret.Caption = reader.ReadString("CAPTION");
            ret.Index = reader.ReadInt("RANK");
            ret.IsKey = reader.ReadBoolean("IsKey");
            ret.IsReadOnly = !reader.ReadBoolean("ISEDITABLE");
            ret.Width = reader.ReadInt("WIDTH");
            ret.TypeSort = reader.ReadString("SORTING");
            ret.ColumnDateType = reader.ReadString("DATA_TYPE");
            ret.Visible = reader.ReadBoolean("Visible");

            return ret;
        }

        private TableDictionaryInfo BuildTableDictionaryInfo(OracleDataReader reader)
        {
            TableDictionaryInfo ret = new TableDictionaryInfo()
            {
                TableName = reader.ReadString("NAME"),
                TableCaption = reader.ReadString("CAPTION"),
            };
            return ret;
        }

        private Dictionary<string, object> BuildTableRows(OracleDataReader reader)
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();
            if (_columns != null)
            {
                foreach (string columnName in _columns)
                {
                    ret.Add(columnName, reader[columnName]);
                }
            }
            return ret;
        }

        #endregion
    }
}
