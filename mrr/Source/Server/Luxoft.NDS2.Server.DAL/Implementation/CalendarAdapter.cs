﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal sealed class CalendarAdapter : BaseOracleTableAdapter, ICalendarAdapter
    {
        public CalendarAdapter(IServiceProvider service) : base(service) { }

        public OperationResult<List<DateTime>> GetRedDays(DateTime begin, DateTime end)
        {
            var dt = new DateTime(2000, 01, 01);
            var sql =
                string.Format(
                    "SELECT * FROM REDDAY WHERE RED_DATE BETWEEN TO_DATE('{0}', 'yyyymmdd') AND TO_DATE('{1}', 'yyyymmdd') ORDER BY RED_DATE",
                    begin.ToString("yyyyMMdd"), end.ToString("yyyyMMdd"));
            return new OperationResult<List<DateTime>>
            {
                Result =
                    ExecuteList(sql, CommandType.Text, new OracleParameter[] { },
                        reader => reader.ReadDateTime("RED_DATE") ?? dt)
            };
        }

        public void AddRedDay(DateTime date)
        {
            var sql = string.Format("INSERT INTO REDDAY (RED_DATE) " +
                                    "(SELECT TO_DATE('{0}', 'yyyymmdd') FROM (" +
                                    "SELECT COUNT(*) CNT FROM REDDAY WHERE RED_DATE = TO_DATE('{0}', 'yyyymmdd')) " +
                                    "T WHERE T.CNT = 0)",
                date.ToString("yyyyMMdd"));
            Execute(sql, CommandType.Text, new OracleParameter[] { });
        }

        public void RemoveRedDay(DateTime date)
        {
            var sql = string.Format("DELETE FROM REDDAY WHERE RED_DATE = TO_DATE('{0}', 'yyyymmdd')",
                date.ToString("yyyyMMdd"));
            Execute(sql, CommandType.Text, new OracleParameter[] { });
        }

        class AddDaysCommand : IOracleCommandBuilder
        {
            private readonly DateTime _date;
            private readonly int _daysToShift;

            public AddDaysCommand(DateTime date, int daysToShift)
            {
                _date = date;
                _daysToShift = daysToShift;
            }

            public OracleCommand BuildCommand(OracleConnection connection)
            {
                var command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.BindByName = true;
                command.CommandText = string.Format(DbConstants.GetDayShift, DbConstants.Result, DbConstants.Date, DbConstants.Days);

                command.Parameters
                    .With(FormatCommandHelper.DateIn(DbConstants.Date, _date))
                    .With(FormatCommandHelper.NumericIn(DbConstants.Days, _daysToShift))
                    .With(FormatCommandHelper.DateOut(DbConstants.Result));

                return command;
            }
        }

        public DateTime AddWorkingDays(DateTime date, int daysToShift)
        {
            var result = new ResultCommandExecuter(_service).TryExecute(new AddDaysCommand(date, daysToShift));
            var s = ((OracleDate) result[DbConstants.Result]);
            return new DateTime(s.Year, s.Month, s.Day);
        }
    }

    static class DbConstants
    {
        public const string GetDayShift = "begin :{0} := PAC$CALENDAR.F$ADD_WORK_DAYS(:{1}, :{2}); end;";

        public const string Result = "pResult";

        public const string Date = "pDate";

        public const string Days = "pDays";
    }
}
