﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    internal class DictionaryFilterTypeAdapter : BaseOracleTableAdapter, IDictionaryFilterTypeAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CursorAlias = "pCursor";
        private const string SearchSelectionProcessSql = @"select * from Dict_Filter_Types";

        public DictionaryFilterTypeAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public List<FilterCriteria> Search()
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<FilterCriteria>(
                string.Format(SearchSQLCommandPattern, SearchSelectionProcessSql),
                CommandType.Text,
                parameters.ToArray(),
                BuildFilterCriteria);

            return data;
        }

        private FilterCriteria BuildFilterCriteria(OracleDataReader reader)
        {
            FilterCriteria obj = new FilterCriteria();
            obj.Name = reader.ReadString("FILTER_TYPE");
            obj.ValueTypeCode = (FilterCriteria.ValueTypes)reader.ReadInt("VALUE_TYPE_CODE");
            obj.LookupTableName = reader.ReadString("LOOKUP_TABLE_NAME");
            obj.InternalName = reader.ReadString("INTERNAL_NAME");
            obj.FirInternalName = reader.ReadString("FIR_INTERNAL_NAME");
            obj.IsRequired = reader.ReadBoolean("IS_REQUIRED");

            return obj;
        }
    }
}
