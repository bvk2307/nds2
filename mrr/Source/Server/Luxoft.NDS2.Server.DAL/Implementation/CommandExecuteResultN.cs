﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Implementation
{
    public class CommandExecuteResultN
    {
        public int AffectedRows { get; set; }

        public Dictionary<string, object> Output { get; set; }

        public CommandExecuteResultN()
        {
            Output = new Dictionary<string, object>();
        }
    }
}
