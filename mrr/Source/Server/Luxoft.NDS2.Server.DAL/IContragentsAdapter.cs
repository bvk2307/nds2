﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IContragentsAdapter
    {
        long? RequestIdContragentsData(string inn, int taxPeriod, int taxYear, int correctionNum);
        DataRequestStatus StatusContragentsData(long requestId);

        PageResult<ContragentParamsSummary> SelectContragentParams(QueryConditions qc);
        long? RequestIdContragentParamsData(string inn, int taxPeriod, int taxYear, int correctionNum);
        DataRequestStatus StatusContragentParamsData(long requestId);
    }
}
