﻿using Luxoft.NDS2.Common.Contracts.DTO.ExportExcel;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    public enum ExportExcelState
    {
        Success = 0,
        Failed,
    }

    internal abstract class PackageAdapter : BaseOracleTableAdapter
    {
        protected abstract string GetPackageName();

        protected virtual string GetCommand(string procedureName)
        {
            return string.Format("{0}.{1}", GetPackageName(), procedureName);
        }
        
        protected PackageAdapter(IServiceProvider service)
            : base(service)
        {
        }
    }

    internal class ExportExcelPackageAdapter : PackageAdapter, IExportExcelPackageAdapter
    {
        private const string ProcedureFindById = "P$FIND_BY_ID";
        private const string ProcedureEnqueueSelectionDiscrepancy = "P$ENQUEUE_SELECTION_DISCREP";
        private const string ProcedureDequeue = "P$DEQUEUE";
        private const string ProcedureUpdate = "P$UPDATE";
        private const string ProcedureGetExportPath = "P$GET_EXPORT_PATH";
        private const string ProcedureGetExportThreshold = "P$GET_EXPORT_THRESHOLD";

        protected override string GetPackageName()
        {
            return "PAC$EXPORT_EXCEL";
        }

        public ExportExcelPackageAdapter(IServiceProvider service) : base(service)
        {
        }

        public ExportExcelItem FindById(long id)
        {
            return ExecuteList(
                GetCommand(ProcedureFindById),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn("pId", id),
                    FormatCommandHelper.Cursor("pResult")
                },
                Build
                ).FirstOrDefault();
        }

        public long EnqueueSelectionDiscrepancy(long selectionId, string userSid, string rowFilter)
        {
            var result = Execute(
                GetCommand(ProcedureEnqueueSelectionDiscrepancy),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericOut("pId"),
                    FormatCommandHelper.VarcharIn("pUserSid", userSid),
                    FormatCommandHelper.NumericIn("pSelectionId", selectionId),
                    FormatCommandHelper.VarcharIn("pRowFilter", rowFilter)
                });
            return long.Parse(result.Output["pId"].ToString());
        }

        public ExportExcelItem Dequeue(string processId)
        {
            return ExecuteList(
               GetCommand(ProcedureDequeue),
               CommandType.StoredProcedure,
               new[]
                {
                    FormatCommandHelper.VarcharIn("pProcessId", processId),
                    FormatCommandHelper.Cursor("pResult")
                },
                Build
                ).FirstOrDefault();
        }

        public void Update(long id, ExportExcelState state, int rowCount, string fileName, string errorMessage)
        {
            Execute(
                GetCommand(ProcedureUpdate),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn("pId", id),
                    FormatCommandHelper.NumericIn("pState", (int)state),
                    FormatCommandHelper.NumericIn("pRowCount", rowCount),
                    FormatCommandHelper.VarcharIn("pFileName", fileName),
                    FormatCommandHelper.VarcharIn("pErrorMessage", errorMessage)
                });            
        }

        public string GetExportPath()
        {
            var path = ExecuteList(
                GetCommand(ProcedureGetExportPath),
                CommandType.StoredProcedure,
                new[] { FormatCommandHelper.Cursor("pResult") },
                reader => reader.GetString("VALUE")
                ).FirstOrDefault();
            if (!string.IsNullOrEmpty(path) && path[path.Length - 1] != '\\')
                path += "\\";
            return path;
        }

        public int GetExportThreshold()
        {
            var value = ExecuteList(
                GetCommand(ProcedureGetExportThreshold),
                CommandType.StoredProcedure,
                new[] { FormatCommandHelper.Cursor("pResult") },
                reader => reader.GetNullableInt32("VALUE")
                ).FirstOrDefault();
            return value ?? 0;
        }
        
        private static ExportExcelItem Build(OracleDataReader reader)
        {
            return new ExportExcelItem
            {
                QUEUE_ITEM_ID = reader.ReadInt64(TypeHelper<ExportExcelItem>.GetMemberName(x => x.QUEUE_ITEM_ID)),
                ENQUEUED = reader.ReadDateTime(TypeHelper<ExportExcelItem>.GetMemberName(x => x.ENQUEUED)),
                STARTED = reader.ReadDateTime(TypeHelper<ExportExcelItem>.GetMemberName(x => x.STARTED)),
                COMPLETED = reader.ReadDateTime(TypeHelper<ExportExcelItem>.GetMemberName(x => x.COMPLETED)),
                USER_SID = reader.ReadStringNullable(TypeHelper<ExportExcelItem>.GetMemberName(x => x.USER_SID)),
                STARTED_BY_ID = reader.ReadStringNullable(TypeHelper<ExportExcelItem>.GetMemberName(x => x.STARTED_BY_ID)),
                SELECTION_ID = reader.ReadInt64(TypeHelper<ExportExcelItem>.GetMemberName(x => x.SELECTION_ID)),
                ROWS_QUANTITY = reader.ReadInt64(TypeHelper<ExportExcelItem>.GetMemberName(x => x.ROWS_QUANTITY)),
                ROW_FILTER = reader.ReadString(TypeHelper<ExportExcelItem>.GetMemberName(x => x.ROW_FILTER)),
                ERROR_DESCRIPTION = reader.ReadString(TypeHelper<ExportExcelItem>.GetMemberName(x => x.ERROR_DESCRIPTION)),
                FILENAME = reader.ReadString(TypeHelper<ExportExcelItem>.GetMemberName(x => x.FILENAME))
            };
        }
    }
}
