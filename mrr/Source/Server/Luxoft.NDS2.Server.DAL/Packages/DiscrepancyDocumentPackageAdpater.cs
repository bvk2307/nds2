﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    internal class DiscrepancyDocumentPackageAdpater : BaseSelectionAdapter, IDiscrepancyDocumentPackageAdapter
    {

        public DiscrepancyDocumentPackageAdpater(IServiceProvider service)
            : base(service)
        {
        }

        /// <summary>
        /// Возвращает список СФ для документа
        /// </summary>
        /// <param name="declarationId">Идентификатор документа</param>
        /// <returns>Список СФ</returns>
        public PageResult<DiscrepancyDocumentInvoice> GetInvoices(long docId, QueryConditions conditions)
        {
            return new PageResult<DiscrepancyDocumentInvoice>();
        }
    }
}
