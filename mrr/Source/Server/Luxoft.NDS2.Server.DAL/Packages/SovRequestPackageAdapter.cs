﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    internal class SovRequestPackageAdapter
    {
        private const string PackageName = "PAC$SOV_REQUEST";

        private const string ProcedureGetInvoiceRequest = "P$GET_INVOICE_REQUEST";

        private const string ProcedureSearchInvoiceRequest = "P$SEARCH_INVOICE_REQUEST";

        private const string ParamId = "pId";

        private const string ParamDeclarationId = "pDeclarationId";

        private const string ParamPartition = "pPartition";

        private const string ParamCursor = "pResult";

        private const string FieldId = "ID";

        private const string FieldStatus = "STATUS";

        #region Итоговые суммы

        private const string FieldPriceBuyAmountSum = "price_buy_amount_sum";

        private const string FieldPriceBuyNdsAmountSum = "price_buy_nds_amount_sum";

        private const string FieldPriceSellSum = "price_sell_sum";

        private const string FieldPriceSell18Sum = "price_sell_18_sum";

        private const string FieldPriceSell10Sum = "price_sell_10_sum";

        private const string FieldPriceSell0Sum = "price_sell_0_sum";

        private const string FieldPriceNds18Sum = "price_nds_18_sum";

        private const string FieldPriceNds10Sum = "price_nds_10_sum";

        private const string FieldPriceTaxFreeSum = "price_tax_free_sum";

        private const string FieldPriceTotalSum = "price_total_sum";

        private const string FieldPriceNdsTotalSum = "price_nds_total_sum";

        #endregion

        private readonly IServiceProvider _service;

        public SovRequestPackageAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public List<SovInvoiceRequest> GetInvoiceRequest(long id)
        {
            return new CommandExecuter(_service)
                .TryRead<List<SovInvoiceRequest>>(
                    new CommandContext()
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetInvoiceRequest),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                { FormatCommandHelper.NumericIn(ParamId, id) },
                                { FormatCommandHelper.Cursor(ParamCursor) }
                            }
                    },
                    ReadInvoiceRequest);
        }

        public List<SovInvoiceRequest> SearchInvoiceRequest(long declarationId, int partition)
        {
            return new CommandExecuter(_service)
                .TryRead<List<SovInvoiceRequest>>(
                    new CommandContext()
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureSearchInvoiceRequest),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                { FormatCommandHelper.NumericIn(ParamDeclarationId, declarationId) },
                                { FormatCommandHelper.NumericIn(ParamPartition, partition) },
                                { FormatCommandHelper.Cursor(ParamCursor) }
                            }
                    },
                    ReadInvoiceRequest);
        }

        private List<SovInvoiceRequest> ReadInvoiceRequest(
            ResultCommandHelper reader)
        {
            var result = new List<SovInvoiceRequest>();

            while (reader.Read())
            {
                result.Add(
                    new SovInvoiceRequest()
                    {
                        Id = reader.GetInt64(FieldId),
                        Status = (RequestStatus)reader.GetInt32(FieldStatus),
                        PriceBuyAmountSum = reader.GetDecimal(FieldPriceBuyAmountSum),
                        PriceBuyNdsAmountSum = reader.GetDecimal(FieldPriceBuyNdsAmountSum),
                        PriceSellSum = reader.GetDecimal(FieldPriceSellSum),
                        PriceSell18Sum = reader.GetDecimal(FieldPriceSell18Sum),
                        PriceSell10Sum = reader.GetDecimal(FieldPriceSell10Sum),
                        PriceSell0Sum = reader.GetDecimal(FieldPriceSell0Sum),
                        PriceNds18Sum = reader.GetDecimal(FieldPriceNds18Sum),
                        PriceNds10Sum = reader.GetDecimal(FieldPriceNds10Sum),
                        PriceTaxFreeSum = reader.GetDecimal(FieldPriceTaxFreeSum),
                        PriceTotalSum = reader.GetDecimal(FieldPriceTotalSum),
                        PriceNdsTotalSum = reader.GetDecimal(FieldPriceNdsTotalSum)
                    });
            }

            return result;
        }
    }
}
