﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Server.Common.DTO.Declaration;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    public class DeclarationPackageAdapter : IDeclarationPackageAdapter
    {
        #region строковые константы

        private const string PackageName = "PAC$DECLARATION";

        private const string ProcedureGetContractorDeclarationZip = "P$GET_CONTRACTOR_DECL_ZIP";
        private const string ProcedureGetDeclaration = "P$GET_DECLARATION";
        private const string ProcedureGetActualDeclaration = "P$GET_ACTUAL_DECLARATION";
        private const string ProcedureGetCountDocumentsKNP = "P$GET_COUNT_DOC_KNP";
        private const string ProcedureGetDeclarationOwner = "P$GET_DECLARATION_OWNER";
        private const string ProcedureGetAllDeclarationZips = "P$GET_ALL_ZIPS";
        private const string ProcedureGetChapterActualZip = "P$GET_CHAPTER_ACTUAL_ZIP";

        private const string ParamInn = "pInn";
        private const string ParamYear = "pYear";
        private const string ParamMonth = "pMonth";
        private const string ParamZip = "pZip";
        private const string ParamCursor = "pCursor";

        private const string ParamInnContractor = "P_INN_CONTRACTOR";
        private const string ParamKppEffective = "P_KPP_EFFECTIVE";
        private const string ParamType = "P_TYPE";
        private const string ParamPeriod = "P_PERIOD";
        private const string ParamFiscalYear = "P_YEAR";

        private const string FieldZip = "ZIP";
        private const string FieldRank = "Rank";

        #endregion

        private readonly IServiceProvider _service;

        public DeclarationPackageAdapter(IServiceProvider service)
        {
            _service = service;
        }

        /// <summary>
        /// Возвращает декларацию по ZIP (также фигурирует как DECLARATION_VERSION_ID)
        /// </summary>
        /// <param name="zip">Идентификатор</param>
        /// <returns>Декларация</returns>
        public DeclarationSummary GetDeclaration(long zip)
        {
            return new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetDeclaration
                                ),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                FormatCommandHelper.NumericIn(ParamZip, zip),
                                FormatCommandHelper.Cursor(ParamCursor)
                            }
                    },
                    ReadSingleDeclaration
                );
        }


        /// <summary>
        /// Возвращает актуальную коректировку декларации по ZIP (также фигурирует как DECLARATION_VERSION_ID)
        /// </summary>
        /// <param name="zip">Идентификатор</param>
        /// <returns>Декларация</returns>
        public DeclarationSummary GetActualDeclaration(long zip)
        {
            return new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetActualDeclaration
                                ),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                FormatCommandHelper.NumericIn(ParamZip, zip),
                                FormatCommandHelper.Cursor(ParamCursor)
                            }
                    },
                    ReadSingleDeclaration
                );
        }

        private DeclarationSummary ReadSingleDeclaration(ResultCommandHelper reader)
        {
            return (reader.Read()) ? BuildDeclarationWithInvoiceCount(reader) : null;
        }

        /// <summary>
        /// Возвращает список ZIPов для комбинированного ключа ИНН + год + месяц
        /// </summary>
        /// <param name="inn">ИНН подавшего НД</param>
        /// <param name="year">Отчетный год</param>
        /// <param name="month">Месяц, входящий в отчетный период</param>
        /// <returns>Список ZIP НД, ранжированный по актуальности</returns>
        public List<DeclarationRankedZip> GetContractorDeclarationZipList(long inn, int year, int month)
        {
            return new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetContractorDeclarationZip),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                FormatCommandHelper.NumericIn(ParamInn, inn),
                                FormatCommandHelper.NumericIn(ParamYear, year),
                                FormatCommandHelper.NumericIn(ParamMonth, month),
                                FormatCommandHelper.Cursor(ParamCursor)
                            }
                    },
                    ReadRankedZipList);
        }

        private List<DeclarationRankedZip> ReadRankedZipList(ResultCommandHelper reader)
        {
            var result = new List<DeclarationRankedZip>();
            while (reader.Read())
            {
                result.Add(
                    new DeclarationRankedZip
                    {
                        ZIP = reader.GetInt64(FieldZip),
                        RANK = reader.GetInt32(FieldRank)
                    });
            }
            return result;
        }

        # region Построение объекта Declaration

        private static DeclarationSummary BuildDeclaration(ResultCommandHelper reader)
        {
            var obj = new DeclarationSummary();

            obj.ID = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ID));
            obj.DECLARATION_VERSION_ID = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECLARATION_VERSION_ID));
            obj.ProcessingStage = (DeclarationProcessignStage)reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ProcessingStage));
            obj.SummaryState = (DeclarationSummaryState)reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SummaryState));
            obj.LOAD_MARK = (DeclarationProcessignStage)reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.LOAD_MARK));
            obj.SEOD_DECL_ID = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SEOD_DECL_ID));
            obj.ASK_DECL_ID = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ASK_DECL_ID));
            obj.XmlVersion = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.XmlVersion));
            obj.IS_ACTUAL = reader.GetBool("IS_ACTIVE");
            obj.INN = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN));
            obj.INN_CONTRACTOR = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN_CONTRACTOR));
            obj.KPP = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP));
            obj.KPP_EFFECTIVE = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP_EFFECTIVE));
            obj.NAME = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NAME));
            obj.ADDRESS1 = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS1));
            obj.ADDRESS2 = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS2));

            obj.CATEGORY_RU = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY_RU));
            obj.REGION_NAME = (obj.CATEGORY == 1) ? "" : reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REGION_NAME));

            obj.REGION_ENTRY = (obj.CATEGORY == 1)
                ? new CodeValueDictionaryEntry("", "")
                : new CodeValueDictionaryEntry(reader.GetString("REGION_CODE"), reader.GetString("REGION_NAME"));

            obj.SOUN_NAME = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN_NAME));
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(reader.GetString("SOUN_CODE"), reader.GetString("SOUN_NAME"));
            obj.SOUN = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN));
            obj.REG_DATE = reader.GetDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REG_DATE));
            obj.TAX_MODE = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_MODE));
            obj.OKVED_CODE = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.OKVED_CODE));
            obj.CAPITAL = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CAPITAL));
            obj.TAX_PERIOD = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD));
            obj.PeriodEffective = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PeriodEffective));
            obj.FULL_TAX_PERIOD = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.FISCAL_YEAR = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            obj.DECL_DATE = reader.GetNullableDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_DATE));
            obj.SUBMISSION_DATE = reader.GetNullableDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBMISSION_DATE));
            obj.DECL_TYPE_CODE = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE_CODE));
            obj.DECL_SIGN = "";
            if (obj.DECL_TYPE_CODE == 0) obj.DECL_SIGN = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_SIGN));
            obj.DECL_TYPE = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE));
            obj.CORRECTION_NUMBER = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER));
            obj.CORRECTION_NUMBER_RANK = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_RANK));
            obj.CORRECTION_NUMBER_EFFECTIVE = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_EFFECTIVE));
            obj.SUBSCRIBER_NAME = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBSCRIBER_NAME));
            obj.PRPODP = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRPODP));
            obj.COMPENSATION_AMNT = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT));
            obj.COMPENSATION_AMNT_SIGN = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT_SIGN));
            obj.CH8_DEALS_AMNT_TOTAL = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_DEALS_AMNT_TOTAL));
            obj.CH9_DEALS_AMNT_TOTAL = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_DEALS_AMNT_TOTAL));
            obj.CH8_NDS = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_NDS));
            obj.CH9_NDS = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_NDS));
            obj.NDS_WEIGHT = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_WEIGHT));
            obj.DISCREP_CURRENCY_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_AMNT));
            obj.DISCREP_CURRENCY_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_COUNT));
            obj.WEAK_DISCREP_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_COUNT));
            obj.WEAK_DISCREP_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_AMNT));
            obj.GAP_DISCREP_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_COUNT));
            obj.GAP_DISCREP_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_AMNT));
            obj.NDS_INCREASE_DISCREP_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_COUNT));
            obj.NDS_INCREASE_DISCREP_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_AMNT));
            obj.DISCREP_MIN_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MIN_AMNT));
            obj.DISCREP_MAX_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MAX_AMNT));
            obj.DISCREP_AVG_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_AVG_AMNT));
            obj.DISCREP_TOTAL_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_TOTAL_AMNT));
            obj.UPDATE_DATE = reader.GetDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.UPDATE_DATE));
            obj.INSPECTOR = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INSPECTOR));
            obj.SUR_CODE = (obj.DECL_TYPE_CODE == 0) ?
                     reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUR_CODE)) : null;
            obj.CATEGORY = null;
            if (obj.DECL_TYPE_CODE == 0)
            {
                if (reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY)) == 1)
                {
                    obj.CATEGORY = 1;
                }
            }
            obj.CONTROL_RATIO_DATE = reader.GetDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DATE));
            obj.CONTROL_RATIO_SEND_TO_SEOD = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_SEND_TO_SEOD));
            obj.CONTROL_RATIO_COUNT = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_COUNT));
            obj.TOTAL_DISCREP_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TOTAL_DISCREP_COUNT));
            obj.CONTROL_RATIO_DISCREP_COUNT = reader.GetInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DISCREP_COUNT));


            obj.STATUS = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.STATUS));

            //--- Begin Сводные данные по разделу 8 и разделу 8.1
            obj.SumNDSPok_8 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_8));
            obj.SumNDSPok_81 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_81));
            obj.SumNDSPokDL_81 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPokDL_81));
            //--- End

            //--- Begin Сводные данные по разделу 9 и разделу 9.1
            obj.StProd18_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_9));
            obj.StProd10_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_9));
            obj.StProd0_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_9));
            obj.SumNDSProd18_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_9));
            obj.SumNDSProd10_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_9));
            obj.StProdOsv_9 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_9));
            obj.StProd18_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_91));
            obj.StProd10_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_91));
            obj.StProd0_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_91));
            obj.SumNDSProd18_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_91));
            obj.SumNDSProd10_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_91));
            obj.StProdOsv_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_91));
            obj.StProd18DL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18DL_91));
            obj.StProd10DL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10DL_91));
            obj.StProd0DL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0DL_91));
            obj.SumNDSProd18DL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18DL_91));
            obj.SumNDSProd10DL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10DL_91));
            obj.StProdOsvDL_91 = reader.GetNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsvDL_91));
            //--- End

            obj.AKT_NOMKORR_8 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_8));
            obj.AKT_NOMKORR_81 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_81));
            obj.AKT_NOMKORR_9 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_9));
            obj.AKT_NOMKORR_91 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_91));
            obj.AKT_NOMKORR_10 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_10));
            obj.AKT_NOMKORR_11 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_11));
            obj.AKT_NOMKORR_12 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_12));

            obj.StProd = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd));

            obj.DISCREP_BUY_BOOK_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_BUY_BOOK_AMNT));
            obj.DISCREP_SELL_BOOK_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_SELL_BOOK_AMNT));

            obj.PVP_TOTAL_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_TOTAL_AMNT));
            obj.PVP_BUY_BOOK_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_BUY_BOOK_AMNT));
            obj.PVP_SELL_BOOK_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_SELL_BOOK_AMNT));
            obj.PVP_DISCREP_MIN_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MIN_AMNT));
            obj.PVP_DISCREP_MAX_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MAX_AMNT));
            obj.PVP_DISCREP_AVG_AMNT = reader.GetDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_AVG_AMNT));

            obj.DateCloseKNP = reader.GetDate(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DateCloseKNP));

            return obj;
        }

        private static DeclarationSummary BuildDeclarationWithInvoiceCount(ResultCommandHelper reader)
        {
            var obj = BuildDeclaration(reader);

            obj.INSPECTORSID = reader.GetString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INSPECTORSID));


            obj.INVOICE_COUNT8 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT8));
            obj.INVOICE_COUNT81 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT81));
            obj.INVOICE_COUNT9 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT9));
            obj.INVOICE_COUNT91 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT91));
            obj.INVOICE_COUNT10 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT10));
            obj.INVOICE_COUNT11 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT11));
            obj.INVOICE_COUNT12 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT12));

            obj.PRIORITY_08 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_08));
            obj.PRIORITY_81 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_81));
            obj.PRIORITY_09 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_09));
            obj.PRIORITY_91 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_91));
            obj.PRIORITY_10 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_10));
            obj.PRIORITY_11 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_11));
            obj.PRIORITY_12 = reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRIORITY_12));

            obj.REQUEST_KEY8 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY8));
            obj.REQUEST_KEY81 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY81));
            obj.REQUEST_KEY9 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY9));
            obj.REQUEST_KEY91 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY91));
            obj.REQUEST_KEY10 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY10));
            obj.REQUEST_KEY11 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY11));
            obj.REQUEST_KEY12 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REQUEST_KEY12));

            obj.ACTUAL_ZIP8 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP8));
            obj.ACTUAL_ZIP81 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP81));
            obj.ACTUAL_ZIP9 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP9));
            obj.ACTUAL_ZIP91 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP91));
            obj.ACTUAL_ZIP10 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP10));
            obj.ACTUAL_ZIP11 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP11));
            obj.ACTUAL_ZIP12 = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ACTUAL_ZIP12));

            obj.ActId = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ActId));
            obj.HasKnpDiscrepancy = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HasKnpDiscrepancy));
            obj.DecisionId = reader.GetNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DecisionId));
            obj.ActIsClosed = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ActIsClosed));
            obj.DecisionIsClosed = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DecisionIsClosed));
            obj.HasActDiscrepancy = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HasActDiscrepancy));
            obj.HasDecisionDiscrepancy = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HasDecisionDiscrepancy));
            obj.HasSeodDecision = reader.GetBool(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HasSeodDecision));

            return obj;
        }

        # endregion

        #region Документы КНП

        /// <summary>
        /// Возвращает общее количество документов КНП декларации
        /// </summary>
        /// <param name="innContractor"></param>
        /// <param name="kppEffective"></param>
        /// <param name="typeCode"></param>
        /// <param name="period"></param>
        /// <param name="year"></param>
        /// <returns>количество докуменнтов КНП</returns>
        private int GetCountDocumentsKNP(string innContractor, string kppEffective, int typeCode, string period, string year)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.VarcharIn("pInnContractor", innContractor),
                FormatCommandHelper.VarcharIn("pKppEffective", kppEffective),
                FormatCommandHelper.NumericIn("pType", typeCode),
                FormatCommandHelper.VarcharIn("pPeriod", period),
                FormatCommandHelper.VarcharIn("pYear", year),
                FormatCommandHelper.NumericOut("pTotalRowsCount")
            };

            new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetCountDocumentsKNP),
                        Type = CommandType.StoredProcedure,
                        Parameters = parameters
                    }, reader => 0);

            OracleParameter pTotalRowsCount = parameters.Single(p => p.ParameterName == "pTotalRowsCount");
            return DataReaderExtension.ReadInt(pTotalRowsCount.Value);
        }

        private List<DocumentKNP> ReadDocumentKNP(ResultCommandHelper reader)
        {
            var result = new List<DocumentKNP>();
            while (reader.Read())
            {
                var obj = new DocumentKNP();

                obj.Id = reader.GetNullableInt64("id");
                obj.TypeName = reader.GetString("name");
                obj.DocNum = reader.GetString("num");
                obj.Date = reader.GetNullableDate("dt");
                obj.DocStatus = reader.GetString("status");
                obj.Type = reader.GetNullableInt64("type_doc_knp");
                obj.StateExplainCanOpen = reader.GetNullableInt32("explain_can_open");

                result.Add(obj);
            }
            return result;
        }

        #endregion

        /// <summary>
        /// Получить у декларации назначенного SID инспектора
        /// </summary>
        /// <param name="innContractor">ИНН НП</param>
        /// <param name="kppEffective">Эффективный КПП </param>
        /// <param name="type">Тип декларации (декларация/журнал)</param>
        /// <param name="period">Налоговый период</param>
        /// <param name="year">Год</param>
        /// <returns>inspector sid</returns>
        public string GetDeclarationOwner(string innContractor, string kppEffective, int type, string period, string year)
        {
            var data = new List<string>();

            data = new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetDeclarationOwner),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                               FormatCommandHelper.VarcharIn(ParamInnContractor, innContractor), 
                               FormatCommandHelper.VarcharIn(ParamKppEffective, kppEffective), 
                               FormatCommandHelper.NumericIn(ParamType, type), 
                               FormatCommandHelper.VarcharIn(ParamPeriod, period), 
                               FormatCommandHelper.VarcharIn(ParamFiscalYear, year),
                               FormatCommandHelper.Cursor(ParamCursor)
                            }
                    },
                    ReadDeclarationOwners);

            return data.SingleOrDefault();
        }

        private List<string> ReadDeclarationOwners(ResultCommandHelper reader)
        {
            var result = new List<string>();
            while (reader.Read())
            {
                result.Add(reader.GetString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID)));
            }
            return result;
        }

        /// <summary>
        /// Возвращает список zip и номера корректировки всех корректировок НД
        /// </summary>
        /// <param name="zip">zip корректировки где был подан указанный раздел</param>
        /// <returns>список zip и номера корректировки всех корректировок НД</returns>
        public List<DeclarationZipCorrectionNumber> GetDeclarationZipCorrections(long zip)
        {
            return new CommandExecuter(_service)
                .TryRead(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetAllDeclarationZips),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                FormatCommandHelper.NumericIn("VP_Zip", zip),
                                FormatCommandHelper.Cursor("VP_Cursor")
                            }
                    },
                    ReadZipCorrectionNumber);
        }

        private List<DeclarationZipCorrectionNumber> ReadZipCorrectionNumber(ResultCommandHelper reader)
        {
            var result = new List<DeclarationZipCorrectionNumber>();
            while (reader.Read())
            {
                result.Add(
                    new DeclarationZipCorrectionNumber
                    {
                        Zip = reader.GetInt64("Zip"),
                        CorrectionNumber = reader.GetInt32("CorrectionNumber")
                    });
            }
            return result;
        }

        /// <summary>
        /// Возвращает zip последней корректировки, наследующей указанный раздел
        /// </summary>
        /// <param name="zip">zip корректировки где был подан указанный раздел</param>
        /// <param name="chapter">номер раздела НД</param>
        /// <returns>zip последней корректировки, наследующей указанный раздел</returns>
        public long GetDeclarationChapterActualZips(long zip, InvoiceRowKeyPartitionNumber chapter)
        {
            return new CommandExecuter(_service)
                .TryExecuteAndReadLong(
                    new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                ProcedureGetChapterActualZip),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                FormatCommandHelper.NumericIn("P_Zip", zip),
                                FormatCommandHelper.NumericIn("P_Chapter", Convert.ToInt64(chapter)),
                                FormatCommandHelper.NumericOut("P_ActualZip")
                            }
                    }, "P_ActualZip");
        }

        private List<DeclarationChapterActualZip> ReadChapterActualZip(ResultCommandHelper reader)
        {
            var result = new List<DeclarationChapterActualZip>();
            while (reader.Read())
            {
                result.Add(
                    new DeclarationChapterActualZip
                    {
                        ActualZipChapterEight = reader.GetNullableInt64("ActualZip8"),
                        ActualZipChapterEightAddSheet = reader.GetNullableInt64("ActualZip81"),
                        ActualZipChapterNine = reader.GetNullableInt64("ActualZip9"),
                        ActualZipChapterNineAddSheet = reader.GetNullableInt64("ActualZip91"),
                        ActualZipChapterTen = reader.GetNullableInt64("ActualZip10"),
                        ActualZipChapterEleven = reader.GetNullableInt64("ActualZip11"),
                        ActualZipChapterTwelve = reader.GetNullableInt64("ActualZip12"),
                    });
            }
            return result;
        }
    }
}
