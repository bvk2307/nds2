﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    internal class AutoselectionPackageAdapter : IAutoselectionInspectionLimitsFinder
    {
        private const string PackageName = "PAC$AUTOSELECTION";

        private const string ParamCursor = "pCursor";

        private const string ParamSelectionId = "pSelectionId";

        private const string FieldRegionCode = "REGION_CODE";

        private const string FieldRegion = "REGION";

        private const string FieldInspectionCode = "INSPECTION_CODE";

        private const string FieldInspection = "INSPECTION";

        private const string FieldPvp = "PVP";

        private readonly IServiceProvider _service;

        public AutoselectionPackageAdapter(IServiceProvider service)
        {
            _service = service;
        }

        private List<AutoselectionInspectionPvpLimit> GetInspectionLimits(long selectionId, string procedureName)
        {
            return new CommandExecuter(_service)
                .TryRead<List<AutoselectionInspectionPvpLimit>>(
                    new CommandContext()
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                procedureName),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                { FormatCommandHelper.NumericIn(ParamSelectionId, selectionId) },
                                { FormatCommandHelper.Cursor(ParamCursor) }
                            }
                    },
                    ReadInspectionLimit);
        }

        public List<AutoselectionInspectionPvpLimit> SearchBySelectionId(long selectionId)
        {
            return GetInspectionLimits(selectionId, "P$GET_INSPECTION_LIMITS");
        }

        public List<AutoselectionInspectionPvpLimit> SearchForOneSelectionBySelectionId(long selectionId)
        {
            return GetInspectionLimits(selectionId, "P$GET_LIMITS_FOR_ONE_SELECTION");
        }

        private List<AutoselectionInspectionPvpLimit> ReadInspectionLimit(
            ResultCommandHelper reader)
        {
            var result = new List<AutoselectionInspectionPvpLimit>();

            while (reader.Read())
            {
                result.Add(
                    new AutoselectionInspectionPvpLimit()
                    {
                        InspectionCode = reader.GetString(FieldInspectionCode),
                        InspectionName = reader.GetString(FieldInspection),
                        RegionCode = reader.GetString(FieldRegionCode),
                        RegionName = reader.GetString(FieldRegion),
                        Pvp = reader.GetDecimal(FieldPvp)
                    });
            }

            return result;
        }
    }
}
