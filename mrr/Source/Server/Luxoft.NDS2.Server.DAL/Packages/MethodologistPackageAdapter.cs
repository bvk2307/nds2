﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    internal class MethodologistPackageAdapter
    {
        private const string PackageName = "NDS2$METHODOLOGIST";

        private const string ParamCursor = "pCursor";

        private const string ParamIncludeFilter = "pIncludeFilter";

        private const string ParamExcludeFilter = "pExcludeFilter";

        private const string FieldIncludeFilter = "INCLUDE_FILTER";

        private const string FieldExcludeFilter = "EXCLUDE_FILTER";

        private const string FieldLastUpdated = "DATE_MODIFIED";

        private readonly IServiceProvider _service;

        public MethodologistPackageAdapter(IServiceProvider service)
        {
            _service = service;
        }       

        public List<AutoSelectionFilterConfiguration> GetAutoselectionFilter()
        {
            return new CommandExecuter(_service)
                .TryRead<List<AutoSelectionFilterConfiguration>>(
                    new CommandContext()
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                "P$GET_AUTOSELECTION_FILTER"),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                { FormatCommandHelper.Cursor(ParamCursor) }
                            }
                    },
                    ReadAutoSelectionFilter);
        }

        public void SaveAutoselectionFilter(string includeFilter, string excludeFilter)
        {
            new CommandExecuter(_service)
                .TryExecute(
                    new CommandContext()
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(
                                PackageName,
                                "P$UPDATE_AUTOSELECTION_FILTER"),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                            {
                                { 
                                    FormatCommandHelper.VarcharIn(
                                        ParamIncludeFilter, 
                                        includeFilter) 
                                },
                                { 
                                    FormatCommandHelper.VarcharIn(
                                        ParamExcludeFilter, 
                                        excludeFilter) 
                                }
                            }
                    });
        }

        private List<AutoSelectionFilterConfiguration> ReadAutoSelectionFilter(
            ResultCommandHelper reader)
        {
            var result = new List<AutoSelectionFilterConfiguration>();

            while (reader.Read())
            {
                result.Add(
                    new AutoSelectionFilterConfiguration()
                    {
                        IncludeFilter = reader.GetString(FieldIncludeFilter),
                        ExcludeFilter = reader.GetString(FieldExcludeFilter),
                        LastChanged = reader.GetDate(FieldLastUpdated)
                    });
            }

            return result;
        }
    }
}
