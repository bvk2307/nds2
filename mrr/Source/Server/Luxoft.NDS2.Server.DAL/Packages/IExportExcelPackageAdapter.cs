﻿using Luxoft.NDS2.Common.Contracts.DTO.ExportExcel;

namespace Luxoft.NDS2.Server.DAL.Packages
{
    public interface IExportExcelPackageAdapter
    {
        ExportExcelItem FindById(long id);
        long EnqueueSelectionDiscrepancy(long selectionId, string userSid, string rowFilter);
        ExportExcelItem Dequeue(string processId);
        void Update(long id, ExportExcelState state, int rowCount, string fileName, string errorMessage);
        string GetExportPath();
        int GetExportThreshold();
    }
}