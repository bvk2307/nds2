﻿using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public class FilterExpressionGroup : FilterExpressionBase
    {
        private readonly List<FilterExpressionBase> _expressions =
            new List<FilterExpressionBase>();

        private readonly bool _isUnion;

        internal FilterExpressionGroup(bool isUnion)
        {
            _isUnion = isUnion;
        }

        public void AddExpression(FilterExpressionBase expression)
        {
            _expressions.Add(expression);
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider)
        {
            if (!_expressions.Any())
            {
                return "0=0";
            }

            return string.Format(
                "({0})",
                string.Join(
                        string.Format(" {0} ", _isUnion.ToOperator()),
                        _expressions.Select(ex => ex.ToQuery(paramBuilder, patternProvider)).ToArray()));
        }
    }

    internal static class ExpressionGroupHelper
    {
        public static string ToOperator(this bool isUnion)
        {
            return isUnion ? "or" : "and";
        }
    }
}
