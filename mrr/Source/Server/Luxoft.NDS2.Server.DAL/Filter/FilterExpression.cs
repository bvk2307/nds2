﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public class FilterExpression : FilterExpressionBase
    {
        private readonly object _value;

        private readonly ColumnFilter.FilterComparisionOperator _type;

        private readonly string _field;

        internal FilterExpression(
            string field,
            ColumnFilter.FilterComparisionOperator type,
            object value)
        {
            _field = field;
            _type = type;
            _value = value;
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider)
        {
            return string.Format(
                patternProvider.Pattern(_field),
                string.Format(
                    OperationResolver.Resolve(_type).BuildPattern(_value),
                    patternProvider.Expression(_field),
                    string.Format(":{0}",paramBuilder.Add(_value))));
        }
    }
}
