﻿using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public abstract class FilterExpressionBase
    {
        public abstract string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider);

        public string ToQuery(
            IParameterBuilder paramBuilder,
            string viewAlias)
        {
            return ToQuery(paramBuilder, new SimpleQueryPatternProvider(viewAlias));
        }
    }
}
