﻿namespace Luxoft.NDS2.Server.DAL.Filter
{
    public interface IQueryPatternProvider
    {
        string Pattern(string field);

        string Expression(string field);
    }
}
