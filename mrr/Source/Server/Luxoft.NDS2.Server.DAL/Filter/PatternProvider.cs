﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    internal class PatternProvider : SimpleQueryPatternProvider
    {
        private readonly Dictionary<string, string> _patterns =
            new Dictionary<string, string>();

        private readonly Dictionary<string, string> _expressions =
            new Dictionary<string, string>();

        public PatternProvider(string viewAlias)
            : base(viewAlias)
        {
        }

        public PatternProvider WithPattern(string field, string pattern)
        {
            _patterns.Add(field, pattern);
            return this;
        }

        public PatternProvider WithExpression(string field, string expression)
        {
            _expressions.Add(field, expression);
            return this;
        }

        public override string Pattern(string field)
        {
            return
                _patterns.ContainsKey(field)
                ? _patterns[field] 
                : base.Pattern(field);
        }

        public override string Expression(string field)
        {
            return
                _expressions.ContainsKey(field)
                ? _expressions[field]
                : base.Expression(field);
        }
    }
}
