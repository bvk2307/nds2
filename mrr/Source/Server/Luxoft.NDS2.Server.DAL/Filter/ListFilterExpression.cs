﻿using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public class ListFilterExpression : FilterExpressionBase
    {
        private const string QueryPattern = "{0} in ({1})";

        private const string CommaSeparator = ",";

        private readonly string _field;

        private readonly List<object> _values = new List<object>();

        internal ListFilterExpression(string field, IEnumerable<object> values)
        {
            _field = field;
            _values.AddRange(values);
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider)
        {
            if (!_values.Any())
            {
                return "0=null";
            }

            var paramList = new List<string>();

            foreach (var listValue in _values)
            {
                paramList.Add(paramBuilder.Add(listValue));
            }

            return string.Format(
                patternProvider.Pattern(_field),
                string.Format(
                    QueryPattern,
                    patternProvider.Expression(_field),
                    string.Join(
                        CommaSeparator, 
                        paramList.Select(x => string.Format(":{0}",x)).ToArray())));
        }
    }
}
