﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public static class FilterExpressionCreator
    {
        public static FilterExpression Create(
            string field,
            ColumnFilter.FilterComparisionOperator comparisonOperator,
            object value)
        {
            return new FilterExpression(field, comparisonOperator, value);
        }

        public static FilterExpressionGroup CreateGroup()
        {
            return new FilterExpressionGroup(false);
        }

        public static FilterExpressionGroup CreateUnion()
        {
            return new FilterExpressionGroup(true);
        }

        public static ListFilterExpression CreateInList(string field, IEnumerable<object> listValues)
        {
            return new ListFilterExpression(field, listValues);
        }

        public static FilterExpressionNot CreateNot(FilterExpressionBase filterExpressionBase)
        {
            return new FilterExpressionNot(filterExpressionBase);
        }

        public static FilterExpressionBase ToFilterExpression(this FilterQuery filter)
        {
            if (filter.FilterOperator == FilterQuery.FilterLogicalOperator.Or
                && filter.Filtering.All(x => x.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals))
            {
                return new ListFilterExpression(filter.ColumnName, filter.Filtering.Select(x => x.Value));
            }

            var group = 
                new FilterExpressionGroup(
                    filter.FilterOperator == FilterQuery.FilterLogicalOperator.Or);

            foreach (var condition in filter.Filtering)
            {
                group.AddExpression(
                    new FilterExpression(
                        filter.ColumnName, 
                        condition.ComparisonOperator, 
                        condition.Value));
            }

            return group;
        }

        public static FilterExpressionGroup ToFilterGroup(this List<FilterQuery> filter)
        {
            var group = new FilterExpressionGroup(false);

            foreach (var columnFilter in filter)
            {
                group.AddExpression(columnFilter.ToFilterExpression());
            }

            return group;
        }
    }
}
