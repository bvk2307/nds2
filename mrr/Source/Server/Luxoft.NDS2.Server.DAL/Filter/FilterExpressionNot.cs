﻿using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Filter
{
    public class FilterExpressionNot : FilterExpressionBase
    {
        private readonly FilterExpressionBase _filterExpression;

        internal FilterExpressionNot(
            FilterExpressionBase filterExpression)
        {
            _filterExpression = filterExpression;
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            return string.Format("not ({0})", _filterExpression.ToQuery(paramBuilder, patternProvider));
        }
    }
}
