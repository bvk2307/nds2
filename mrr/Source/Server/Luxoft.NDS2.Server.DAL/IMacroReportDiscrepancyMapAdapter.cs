using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Server.DAL.Implementation;
using System;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IMacroReportDiscrepancyMapAdapter
    {
        List<MapResponseDataDiscrepancy> GetMapData(MapRequestData model);
    }
}