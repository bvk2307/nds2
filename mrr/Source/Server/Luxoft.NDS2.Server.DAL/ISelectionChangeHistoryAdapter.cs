﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы адаптера истории изменений выборки
    /// </summary>
    public interface ISelectionChangeHistoryAdapter
    {
        /// <summary>
        /// Добавляет запись об изменении выборки
        /// </summary>
        /// <param name="action">Действие, совершенное над выборкой</param>
        void Insert(ActionHistory action);
    }
}
