﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDeclarationBriefAdapter
    {
        List<DeclarationBrief> Search(QueryConditions conditions);

        Dictionary<string, string> GetTipsForMarks();

        int Count(QueryConditions conditions);

        /// <summary>
        /// Сброс признака значимых изменений
        /// </summary>
        /// <param name="innContractor">ИНН НП</param>
        /// <param name="kppEffective">Эффективный КПП </param>
        /// <param name="type">Тип декларации (декларация/журнал)</param>
        /// <param name="period">Налоговый период</param>
        /// <param name="year">Год</param>
        void ResetMark(string innContractor, string kppEffective, int type, string period, string year);

        /// <summary>
        /// Уменьшает на 1 счетчик необработанных неформализованных пояснений (т.е. полученных не по ТКС)
        /// </summary>
        /// <param name="zip"></param>
        void DownNoTks(long zip);
    }
}
