﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimQueueAdapter
    {
        /// <summary>
        /// Выполняет получения одного элемента из очереди рег.номеров
        /// </summary>
        /// <param name="queueElement">один элемент из очереди рег.номеров</param>
        /// <returns>результат успешного получения элемента</returns>
        bool TryPull(out ReclaimQueue queueElement);

        /// <summary>
        /// Выполняет наличия элементов в очереди рег.номеров готовых для обработки
        /// </summary>
        /// <returns>факт наличия элементов в очереди рег.номеров готовых для обработки</returns>
        bool ExistsInQueue();

        /// <summary>
        /// Выполняет обновление состояния одного элемента очереди рег.номеров
        /// </summary>
        /// <returns>элемент очереди</returns>
        void UpdateQueue(long queueId, ReclaimQueueState state, ReclaimLocked locked);
    }
}
