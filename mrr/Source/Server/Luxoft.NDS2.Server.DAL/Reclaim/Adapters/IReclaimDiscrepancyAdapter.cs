﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimDiscrepancyAdapter
    {
        /// <summary>
        /// Выполняет поиск расхождений для формирования автоистребования (для стороны 1)
        /// </summary>
        /// <param name="queueElemId">идентификатор элемента очереди</param>
        /// <returns>список расхождений</returns>
        IEnumerable<ReclaimDiscrepancy> SelectSideFirstAll(long queueElemId);

        /// <summary>
        /// Выполняет поиск расхождений для формирования автоистребования (для стороны 2)
        /// </summary>
        /// <param name="queueElemId">идентификатор элемента очереди</param>
        /// <returns>список расхождений</returns>
        IEnumerable<ReclaimDiscrepancy> SelectSideSecondAll(long queueElemId);

        /// <summary>
        /// Выполняет добавление расхождения в автоистребование
        /// </summary>
        /// <param name="docId">идентификатор автоитсребования</param>
        /// <param name="discrepancyId">идентификатор расхождения</param>
        /// <param name="rowKey">идентификатор счет-фактуры</param>
        /// <param name="amount">сумма расхождения</param>
        /// <param name="amountPvp">сумма расхождения ПВП</param>
        void Insert(long docId, long discrepancyId, string rowKey, decimal? amount, decimal? amountPvp);
    }
}
