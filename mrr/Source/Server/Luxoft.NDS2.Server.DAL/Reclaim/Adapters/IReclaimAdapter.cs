﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimAdapter
    {
        /// <summary>
        /// Выполняет добавление автоистребования
        /// </summary>
        /// <param name="regNumber">рег.номер корректировки</param>
        /// <param name="docType">тип документа</param>
        /// <param name="docKind">вид документа</param>
        /// <param name="sonoCode">код налогового органа</param>
        /// <param name="discrepancyCount">кол-во расхождений</param>
        /// <param name="discrepancyAmount">сумма расхождений</param>
        /// <param name="proccessed">статус обработки</param>
        /// <param name="inn">ИНН</param>
        /// <param name="kppEffective">КПП эффективный</param>
        /// <param name="queueItemId">идентификатор элемента очереди рег. номеров</param>
        /// <param name="zip">zip корректировки</param>
        /// <returns>идентификатор автоистребвания</returns>
        long Insert(
            long regNumber,
            int docType,
            int docKind,
            string sonoCode,
            long discrepancyCount,
            decimal discrepancyAmount,
            int proccessed,
            string inn,
            string kppEffective,
            long queueItemId,
            long zip,
            int quarter,
            int fiscalYear);

        /// <summary>
        /// Выполняет обновление автоистребования
        /// </summary>
        /// <param name="docId">идентификатор автоистребвания</param>
        /// <param name="discrepancyCount">кол-во расхождений</param>
        /// <param name="discrepancyAmount">сумма расхождений</param>
        /// <param name="discrepancyAmount">сумма расхождений ПВП</param>
        /// <param name="hasSchemaValidationErrors">наличие ошибок при валидации xml-документа xsd-схемой</param>
        /// <param name="hasDatabaseError">наличие ошибки при сохранения в БД</param>
        /// <param name="hasGeneralError">наличие обобщенной ошибки при сохранения</param>
        /// <param name="proccessed">статус обработки</param>
        void Update(
            long docId,
            long discrepancyCount,
            decimal discrepancyAmount,
            decimal discrepancyAmountPvp,
            bool hasSchemaValidationErrors,
            bool hasDatabaseError,
            bool hasGeneralError,
            string documentBody,
            int proccessed);
    }
}
