﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimInvoiceAdapter : IReclaimInvoiceAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimInvoiceAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет добавление всех счет-фактур в автоистребование
        /// </summary>
        /// <param name="docId">идентификатор автоитсребования</param>
        public void InsertAll(long docId)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimInsertInvocesCommandBuilder(docId));
        }
    }
}
