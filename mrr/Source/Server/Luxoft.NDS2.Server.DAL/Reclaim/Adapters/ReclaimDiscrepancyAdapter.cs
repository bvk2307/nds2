﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimDiscrepancyAdapter : IReclaimDiscrepancyAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimDiscrepancyAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет поиск расхождений для формирования автоистребования (для стороны 1)
        /// </summary>
        /// <param name="queueElemId">идентификатор элемента очереди</param>
        /// <returns>список расхождений</returns>
        public IEnumerable<ReclaimDiscrepancy> SelectSideFirstAll(long queueElemId)
        {
            return new ListCommandExecuter<ReclaimDiscrepancy>(
                new GenericDataMapper<ReclaimDiscrepancy>()
                    .WithFieldMapperFor("TYPE", new IntFieldMapper()),
                _service,
                _connectionFactory).TryExecute(
                    new ReclaimDiscrepancySearchSideOneCommandBuilder(
                        queueElemId)).ToArray();
        }

        /// <summary>
        /// Выполняет поиск расхождений для формирования автоистребования (для стороны 2)
        /// </summary>
        /// <param name="queueElemId">идентификатор элемента очереди</param>
        /// <returns>список расхождений</returns>
        public IEnumerable<ReclaimDiscrepancy> SelectSideSecondAll(long queueElemId)
        {
            return new ListCommandExecuter<ReclaimDiscrepancy>(
                new GenericDataMapper<ReclaimDiscrepancy>()
                    .WithFieldMapperFor("TYPE", new IntFieldMapper()),
                _service,
                _connectionFactory).TryExecute(
                    new ReclaimDiscrepancySearchSideTwoCommandBuilder(
                        queueElemId)).ToArray();
        }

        /// <summary>
        /// Выполняет добавление расхождения в автоистребование
        /// </summary>
        /// <param name="docId">идентификатор автоитсребования</param>
        /// <param name="discrepancyId">идентификатор расхождения</param>
        /// <param name="rowKey">идентификатор счет-фактуры</param>
        /// <param name="amount">сумма расхождения</param>
        /// <param name="amountPvp">сумма расхождения ПВП</param>
        public void Insert(long docId, long discrepancyId, string rowKey, decimal? amount, decimal? amountPvp)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimInsertDiscrepancyCommandBuilder(docId, discrepancyId, rowKey, amount, amountPvp));
        }
    }
}
