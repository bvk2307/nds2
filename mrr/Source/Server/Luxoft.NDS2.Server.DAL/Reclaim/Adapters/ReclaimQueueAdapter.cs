﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimQueueAdapter : IReclaimQueueAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimQueueAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет получения одного элемента из очереди рег.номеров
        /// </summary>
        /// <param name="queueElement">один элемент из очереди рег.номеров</param>
        /// <returns>результат успешного получения элемента</returns>
        public bool TryPull(out ReclaimQueue queueElement)
        {
            var queueElements = new ListCommandExecuter<ReclaimQueue>(
                new GenericDataMapper<ReclaimQueue>(),
                _service,
                _connectionFactory).TryExecute(
                    new ReclaimQueuePullCommandBuilder())
                    .ToList();

            queueElement = queueElements.SingleOrDefault();
            return queueElements.Any();
        }

        /// <summary>
        /// Выполняет наличия элементов в очереди рег.номеров готовых для обработки
        /// </summary>
        /// <returns>факт наличия элементов в очереди рег.номеров готовых для обработки</returns>
        public bool ExistsInQueue()
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimQueueExistsCommandBuilder());

            var valueParamExists = ((Oracle.DataAccess.Types.OracleDecimal)values[ReclaimQueueExistsCommandBuilder.ParamExists]).ToInt32();
            return (valueParamExists == 1);
        }

        /// <summary>
        /// Выполняет обновление состояния одного элемента очереди рег.номеров
        /// </summary>
        /// <returns>элемент очереди</returns>
        public void UpdateQueue(long queueId, ReclaimQueueState state, ReclaimLocked locked)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimQueueUpdateCommandBuilder(queueId, state, locked));
        }
    }
}
