﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimAdapter : IReclaimAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет добавление автоистребования
        /// </summary>
        /// <param name="regNumber">рег.номер корректировки</param>
        /// <param name="docType">тип документа</param>
        /// <param name="docKind">вид документа</param>
        /// <param name="sonoCode">код налогового органа</param>
        /// <param name="discrepancyCount">кол-во расхождений</param>
        /// <param name="discrepancyAmount">сумма расхождений</param>
        /// <param name="proccessed">статус обработки</param>
        /// <param name="inn">ИНН</param>
        /// <param name="kppEffective">КПП эффективный</param>
        /// <param name="queueItemId">идентификатор элемента очереди рег. номеров</param>
        /// <param name="zip">zip корректировки</param>
        /// <returns>идентификатор автоистребвания</returns>
        public long Insert(
            long regNumber, 
            int docType, 
            int docKind, 
            string sonoCode,
            long discrepancyCount,
            decimal discrepancyAmount,
            int proccessed,
            string inn,
            string kppEffective,
            long queueItemId,
            long zip,
            int quarter,
            int fiscalYear)
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimInsertDocCommandBuilder(
                            regNumber,
                            docType,
                            docKind, 
                            sonoCode,
                            discrepancyCount,
                            discrepancyAmount,
                            proccessed,
                            inn,
                            kppEffective,
                            queueItemId,
                            zip,
                            quarter,
                            fiscalYear));

            return ((Oracle.DataAccess.Types.OracleDecimal)values[ReclaimInsertDocCommandBuilder.ParamDocId]).ToInt64();
        }

        /// <summary>
        /// Выполняет обновление автоистребования
        /// </summary>
        /// <param name="docId">идентификатор автоистребвания</param>
        /// <param name="discrepancyCount">кол-во расхождений</param>
        /// <param name="discrepancyAmount">сумма расхождений</param>
        /// <param name="discrepancyAmountPvp">сумма расхождений ПВП</param>
        /// <param name="hasSchemaValidationErrors">наличие ошибок при валидации xml-документа xsd-схемой</param>
        /// <param name="hasDatabaseError">наличие ошибки при сохранения в БД</param>
        /// <param name="hasGeneralError">наличие обобщенной ошибки при сохранения</param>
        /// <param name="proccessed">статус обработки</param>
        public void Update(
            long docId,
            long discrepancyCount,
            decimal discrepancyAmount,
            decimal discrepancyAmountPvp,
            bool hasSchemaValidationErrors,
            bool hasDatabaseError,
            bool hasGeneralError,
            string documentBody,
            int proccessed)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimUpdateDocCommandBuilder(
                            docId,
                            discrepancyCount,
                            discrepancyAmount,
                            discrepancyAmountPvp,
                            hasSchemaValidationErrors,
                            hasDatabaseError,
                            hasGeneralError,
                            documentBody,
                            proccessed));
        }
    }
}
