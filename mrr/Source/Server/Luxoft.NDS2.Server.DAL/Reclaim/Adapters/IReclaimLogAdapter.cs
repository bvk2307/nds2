﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimLogAdapter
    {
        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - ошибка
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        void LogError(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId);

        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - предупреждение
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        void LogWarning(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId);

        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - информативное
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        void LogInformation(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId);
    }
}
