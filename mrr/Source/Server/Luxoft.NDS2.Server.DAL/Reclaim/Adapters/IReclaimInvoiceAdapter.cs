﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimInvoiceAdapter
    {
        /// <summary>
        /// Выполняет добавление всех счет-фактур в автоистребование
        /// </summary>
        /// <param name="docId">идентификатор автоитсребования</param>
        void InsertAll(long docId);
    }
}
