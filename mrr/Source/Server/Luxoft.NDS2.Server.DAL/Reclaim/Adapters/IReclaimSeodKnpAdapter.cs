﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimSeodKnpAdapter
    {
        /// <summary>
        /// Выполняет получение состояния КНП корректировки 
        /// </summary>
        /// <param name="sonoCode">код налогового органа</param>
        /// <param name="regNumber">рег.номер</param>
        /// <returns>состояние КНП корректировки</returns>
        ReclaimStateSeodKnp GetStateSeodKnp(string sonoCode, long regNumber);
    }
}
