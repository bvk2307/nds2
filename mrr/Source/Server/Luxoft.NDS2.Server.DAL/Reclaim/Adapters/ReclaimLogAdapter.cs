﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimLogAdapter : IReclaimLogAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimLogAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - ошибка
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        public void LogError(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimLogErrorCommandBuilder(title, message, zip, sonoCode, regNumber, queueId));
        }

        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - предупреждение
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        public void LogWarning(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimLogWarningCommandBuilder(title, message, zip, sonoCode, regNumber, queueId));
        }

        /// <summary>
        /// Выполняет добавление запись в лог автоистребования, тип - информативное
        /// </summary>
        /// <param name="title">заголовок сообщения</param>
        /// <param name="message">текст сообщения</param>
        /// <param name="zip">zip корректировки</param>
        /// <param name="regNumber">рег. номер корректировки</param>
        public void LogInformation(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId)
        {
            new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimLogInfoCommandBuilder(title, message, zip, sonoCode, regNumber, queueId));
        }
    }
}
