﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    internal class ReclaimSeodKnpAdapter : IReclaimSeodKnpAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ReclaimSeodKnpAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет получение состояния КНП корректировки 
        /// </summary>
        /// <param name="sonoCode">код налогового органа</param>
        /// <param name="regNumber">рег.номер</param>
        /// <returns>состояние КНП корректировки</returns>
        public ReclaimStateSeodKnp GetStateSeodKnp(string sonoCode, long regNumber)
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new ReclaimSeodKnpCommandBuilder(
                            sonoCode,
                            regNumber));

            return (ReclaimStateSeodKnp)((Oracle.DataAccess.Types.OracleDecimal)values[ReclaimSeodKnpCommandBuilder.ParamStateKnp]).ToInt32();
        }
    }
}
