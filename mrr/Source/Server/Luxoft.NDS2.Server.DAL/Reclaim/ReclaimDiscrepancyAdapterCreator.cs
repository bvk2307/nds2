﻿using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim
{
    public static class ReclaimDiscrepancyAdapterCreator
    {
        public static IReclaimDiscrepancyAdapter Create(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new ReclaimDiscrepancyAdapter(service, connectionFactory);
        }
    }
}
