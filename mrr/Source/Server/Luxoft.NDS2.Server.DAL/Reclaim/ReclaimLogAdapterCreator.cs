﻿using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim
{
    public static class ReclaimLogAdapterCreator
    {
        public static IReclaimLogAdapter Create(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new ReclaimLogAdapter(service, connectionFactory);
        }
    }
}
