﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimDiscrepancySearchCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamQueueId = "pQueueId";

        private const string ParamCursor = "pCursor";

        private long _queueId;

        public ReclaimDiscrepancySearchCommandBuilder(
            long queueId,
            string procedureName)
            : base(string.Format("{0}.{1}", ReclaimConstants.PackageName, procedureName))
        {
            _queueId = queueId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamQueueId, _queueId));
            parameters.Add(FormatCommandHelper.Cursor(ParamCursor));

            return result;
        }
    }
}
