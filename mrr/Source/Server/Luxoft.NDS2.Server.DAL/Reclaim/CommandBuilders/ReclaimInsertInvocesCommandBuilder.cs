﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimInsertInvocesCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamDocId = "pDocId";

        private readonly long _docId;

        public ReclaimInsertInvocesCommandBuilder(long docId)
            : base(string.Format("{0}.P$INSERT_INVOICES", ReclaimConstants.PackageName))
        {
            _docId = docId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamDocId, _docId));

            return result;
        }
    }
}
