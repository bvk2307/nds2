﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimQueuePullCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamQueueId = "pQueueId";

        private const string ParamCursor = "pCursor";

        public ReclaimQueuePullCommandBuilder()
            : base(string.Format("{0}.P$PULL_QUEUE", ReclaimConstants.PackageName))
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericOut(ParamQueueId));
            parameters.Add(FormatCommandHelper.Cursor(ParamCursor));

            return result;
        }
    }
}
