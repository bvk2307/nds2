﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimDiscrepancySearchSideTwoCommandBuilder: ReclaimDiscrepancySearchCommandBuilder
    {
        public ReclaimDiscrepancySearchSideTwoCommandBuilder(
            long queueId)
            : base(queueId, "P$GET_DISCREPANCIES_SIDE_TWO") { }
    }
}
