﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimInsertDocCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamRegNumber = "pRegNumber";
        private const string ParamDocType = "pDocType";
        private const string ParamDocKind = "pDocKind";
        private const string ParamSonoCode = "pSonoCode";
        private const string ParamDiscrepancyCount = "pDiscrepancyCount";
        private const string ParamDiscrepancyAmount = "pDiscrepancyAmount";
        private const string ParamProcessed = "pProcessed";
        private const string ParamInn = "pInn";
        private const string ParamKppEffective = "pKppEffective";
        private const string ParamQueueId = "pQueueId";
        private const string ParamZip = "pZip";
        private const string ParamQuarter = "pQuarter";
        private const string ParamFiscalYear = "pFiscalYear";
        public const string ParamDocId = "pDocId";

        private readonly long _regNumber;
        private readonly int _docType;
        private readonly int _docKind;
        private readonly string _sonoCode;
        private readonly long _discrepancyCount;
        private readonly decimal _discrepancyAmount;
        private readonly int _proccessed;
        private readonly string _inn;
        private readonly string _kppEffective;
        private readonly long _queueItemId;
        private readonly long _zip;
        private readonly int _quarter;
        private readonly int _fiscalYear;

        public ReclaimInsertDocCommandBuilder(
            long regNumber,
            int docType,
            int docKind,
            string sonoCode,
            long discrepancyCount,
            decimal discrepancyAmount,
            int proccessed,
            string inn,
            string kppEffective,
            long queueItemId,
            long zip,
            int quarter,
            int fiscalYear)
            : base(string.Format("{0}.P$INSERT_DOC", ReclaimConstants.PackageName))
        {
            _regNumber = regNumber;
            _docType = docType;
            _docKind = docKind;
            _sonoCode = sonoCode;
            _discrepancyCount = discrepancyCount;
            _discrepancyAmount = discrepancyAmount;
            _proccessed = proccessed;
            _inn = inn;
            _kppEffective = kppEffective;
            _queueItemId = queueItemId;
            _zip = zip;
            _quarter = quarter;
            _fiscalYear = fiscalYear;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamRegNumber, _regNumber));
            parameters.Add(FormatCommandHelper.NumericIn(ParamDocType, _docType));
            parameters.Add(FormatCommandHelper.NumericIn(ParamDocKind, _docKind));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamSonoCode, _sonoCode));
            parameters.Add(FormatCommandHelper.NumericIn(ParamDiscrepancyCount, _discrepancyCount));
            parameters.Add(FormatCommandHelper.DecimalIn(ParamDiscrepancyAmount, _discrepancyAmount));
            parameters.Add(FormatCommandHelper.NumericIn(ParamProcessed, _proccessed));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamInn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamKppEffective, _kppEffective));
            parameters.Add(FormatCommandHelper.NumericIn(ParamQueueId, _queueItemId));
            parameters.Add(FormatCommandHelper.NumericIn(ParamZip, _zip));
            parameters.Add(FormatCommandHelper.NumericIn(ParamQuarter, _quarter));
            parameters.Add(FormatCommandHelper.NumericIn(ParamFiscalYear, _fiscalYear));

            parameters.Add(FormatCommandHelper.NumericOut(ParamDocId));

            return result;
        }
    }
}
