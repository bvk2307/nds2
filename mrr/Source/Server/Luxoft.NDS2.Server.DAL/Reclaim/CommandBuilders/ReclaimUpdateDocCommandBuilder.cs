﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimUpdateDocCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamDocId = "VP_DocId";
        private const string ParamDiscrepancyCount = "VP_DiscrepancyCount";
        private const string ParamDiscrepancyAmount = "VP_DiscrepancyAmount";
        private const string ParamDiscrepancyAmountPvp = "VP_DiscrepancyAmountPvp";
        private const string ParamHasSchemaValidationErrors = "VP_HasSchemaValidationErrors";
        private const string ParamHasDatabaseError = "VP_HasDatabaseError";
        private const string ParamGeneralError = "VP_HasGeneralError";
        private const string ParamDocumentBody = "VP_DocumentBody";
        private const string ParamProcessed = "VP_Processed";

        private readonly long _docId;
        private readonly long _discrepancyCount;
        private readonly decimal _discrepancyAmount;
        private readonly decimal _discrepancyAmountPvp;
        private readonly bool _hasSchemaValidationErrors;
        private readonly bool _hasDatabaseError;
        private readonly bool _hasGeneralError;
        private readonly string _documentBody;
        private readonly int _proccessed;

        public ReclaimUpdateDocCommandBuilder(
            long docId,
            long discrepancyCount,
            decimal discrepancyAmount,
            decimal discrepancyAmountPvp,
            bool hasSchemaValidationErrors,
            bool hasDatabaseError,
            bool hasGeneralError,
            string documentBody,
            int proccessed)
            : base(string.Format("{0}.P$UPDATE_DOC", ReclaimConstants.PackageName))
        {
            _docId = docId;
            _discrepancyCount = discrepancyCount;
            _discrepancyAmount = discrepancyAmount;
            _discrepancyAmountPvp = discrepancyAmountPvp;
            _hasSchemaValidationErrors = hasSchemaValidationErrors;
            _hasDatabaseError = hasDatabaseError;
            _hasGeneralError = hasGeneralError;
            _documentBody = documentBody;
            _proccessed = proccessed;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamDocId, _docId));
            parameters.Add(FormatCommandHelper.NumericIn(ParamDiscrepancyCount, _discrepancyCount));
            parameters.Add(FormatCommandHelper.DecimalIn(ParamDiscrepancyAmount, _discrepancyAmount));
            parameters.Add(FormatCommandHelper.DecimalIn(ParamDiscrepancyAmountPvp, _discrepancyAmountPvp));
            parameters.Add(FormatCommandHelper.NumericIn(ParamHasSchemaValidationErrors, _hasSchemaValidationErrors ? 1 : 0));
            parameters.Add(FormatCommandHelper.NumericIn(ParamHasDatabaseError, _hasDatabaseError ? 1 : 0));
            parameters.Add(FormatCommandHelper.NumericIn(ParamGeneralError, _hasGeneralError ? 1 : 0));
            parameters.Add(FormatCommandHelper.ClobIn(ParamDocumentBody, _documentBody));
            parameters.Add(FormatCommandHelper.NumericIn(ParamProcessed, _proccessed));

            return result;
        }
    }
}
