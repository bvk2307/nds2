﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimQueueExistsCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public const string ParamExists = "pExists";

        public ReclaimQueueExistsCommandBuilder()
            : base(string.Format("{0}.P$EXISTS_IN_QUEUE", ReclaimConstants.PackageName))
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericOut(ParamExists));

            return result;
        }
    }
}
