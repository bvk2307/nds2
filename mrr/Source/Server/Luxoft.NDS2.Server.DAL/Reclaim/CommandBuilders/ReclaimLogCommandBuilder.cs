﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal abstract class ReclaimLogCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamTitle = "pTitle";
        private const string ParamMessage = "pMessage";
        private const string ParamZip = "pZip";
        private const string ParamSonoCode = "pSonoCode";
        private const string ParamRegNumber = "pRegNumber";
        private const string ParamQueueId = "pQueueId";

        private string _title;
        private string _message;
        private long? _zip;
        private string _sonoCode;
        private long? _regNumber;
        private long? _queueId;

        protected ReclaimLogCommandBuilder(
            string procedureName,
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId)
            : base(procedureName)
        {
            _title = title;
            _message = message;
            _zip = zip;
            _sonoCode = sonoCode;
            _regNumber = regNumber;
            _queueId = queueId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(ParamTitle, _title));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamMessage, _message));
            parameters.Add(FormatCommandHelper.NumericIn(ParamZip, _zip));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamSonoCode, _sonoCode));
            parameters.Add(FormatCommandHelper.NumericIn(ParamRegNumber, _regNumber));
            parameters.Add(FormatCommandHelper.NumericIn(ParamQueueId, _queueId));

            return result;
        }
    }
}
