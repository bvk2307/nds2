﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimInsertDiscrepancyCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamDocId = "VP_DocId";
        private const string ParamDiscrepancyId = "VP_DiscrepancyId";
        private const string ParamRowKey = "VP_RowKey";
        private const string ParamAmount = "VP_Amount";
        private const string ParamAmountPvp = "VP_AmountPvp";

        private readonly long _docId;
        private readonly long _discrepancyId;
        private readonly string _rowKey;
        private readonly decimal? _amount;
        private readonly decimal? _amountPvp;

        public ReclaimInsertDiscrepancyCommandBuilder(
            long docId, 
            long discrepancyId, 
            string rowKey,
            decimal? amount,
            decimal? amountPvp)
            : base(string.Format("{0}.P$INSERT_DISCREPANCY", ReclaimConstants.PackageName))
        {
            _docId = docId;
            _discrepancyId = discrepancyId;
            _rowKey = rowKey;
            _amount = amount;
            _amountPvp = amountPvp;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamDocId, _docId));
            parameters.Add(FormatCommandHelper.NumericIn(ParamDiscrepancyId, _discrepancyId));
            parameters.Add(FormatCommandHelper.VarcharIn(ParamRowKey, _rowKey));
            parameters.Add(FormatCommandHelper.DecimalIn(ParamAmount, _amount));
            parameters.Add(FormatCommandHelper.DecimalIn(ParamAmountPvp, _amountPvp));

            return result;
        }
    }
}
