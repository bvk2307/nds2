﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimQueueUpdateCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ParamQueueId = "pQueueId";
        private const string ParamState = "pState";
        private const string ParamIsLocked = "pIsLocked";

        private long _queueId;
        private ReclaimQueueState _state;
        private ReclaimLocked _locked;

        public ReclaimQueueUpdateCommandBuilder(
            long queueId,
            ReclaimQueueState state,
            ReclaimLocked locked)
            : base(string.Format("{0}.P$UPDATE_QUEUE", ReclaimConstants.PackageName))
        {
            _queueId = queueId;
            _state = state;
            _locked = locked;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamQueueId, _queueId));
            parameters.Add(FormatCommandHelper.NumericIn(ParamState, (int)_state));
            parameters.Add(FormatCommandHelper.NumericIn(ParamIsLocked, (int)_locked));

            return result;
        }
    }
}
