﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimSeodKnpCommandBuilder: ExecuteProcedureCommandBuilder
    {
        private const string ParamSonoCode = "pSonoCode";
        private const string ParamRegNumber = "pRegNumber";
        public const string ParamStateKnp = "pStateKnp";

        private readonly string _sonoCode;
        private readonly long _regNumber;

        public ReclaimSeodKnpCommandBuilder(string sonoCode, long regNumber)
            : base(string.Format("{0}.P$GET_STATE_SEOD_KNP", ReclaimConstants.PackageName))
        {
            _sonoCode = sonoCode;
            _regNumber = regNumber;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(ParamSonoCode, _sonoCode));
            parameters.Add(FormatCommandHelper.NumericIn(ParamRegNumber, _regNumber));
            parameters.Add(FormatCommandHelper.NumericOut(ParamStateKnp));

            return result;
        }
    }
}
