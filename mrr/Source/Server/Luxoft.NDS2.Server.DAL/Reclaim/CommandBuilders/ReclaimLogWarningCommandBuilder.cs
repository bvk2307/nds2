﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.CommandBuilders
{
    internal class ReclaimLogWarningCommandBuilder : ReclaimLogCommandBuilder
    {
        public ReclaimLogWarningCommandBuilder(
            string title,
            string message,
            long? zip,
            string sonoCode,
            long? regNumber,
            long? queueId)
            : base(
                string.Format("{0}.P$LOG_WARNING", ReclaimConstants.PackageName), 
                title, 
                message, 
                zip, 
                sonoCode, 
                regNumber,
                queueId)
        {
        }
    }
}
