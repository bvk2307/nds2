﻿using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim
{
    public static class ReclaimQueueAdapterCreator
    {
        public static IReclaimQueueAdapter Create(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new ReclaimQueueAdapter(service, connectionFactory);
        }
    }
}
