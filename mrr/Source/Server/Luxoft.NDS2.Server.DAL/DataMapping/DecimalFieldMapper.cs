﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class DecimalFieldMapper : FieldMapperBase<decimal>
    {
        protected override decimal Null()
        {
            throw new DataMappingException("Cannot convert null to decimal");
        }

        protected override decimal Data(object value)
        {
            return Convert.ToDecimal(value);
        }
    }
}
