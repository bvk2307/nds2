﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableBooleanFieldMapper : NullableFieldMapper<bool>
    {
        protected override bool? Data(object value)
        {
            return new bool?(Convert.ToBoolean(value));
        }
    }
}
