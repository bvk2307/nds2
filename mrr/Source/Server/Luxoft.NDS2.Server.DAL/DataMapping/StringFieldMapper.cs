﻿namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class StringFieldMapper : FieldMapperBase<string>
    {
        protected override string Null()
        {
            return string.Empty;
        }

        protected override string Data(object value)
        {
            return value.ToString();
        }
    }
}
