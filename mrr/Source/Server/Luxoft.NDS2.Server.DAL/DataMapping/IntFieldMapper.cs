﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class IntFieldMapper : FieldMapperBase<int>
    {
        protected override int Null()
        {
            throw new DataMappingException("Cannot convert null to int");
        }

        protected override int Data(object value)
        {
            return Convert.ToInt32(value);
        }
    }
}
