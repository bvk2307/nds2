﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class UnsignedLongFieldMapper : FieldMapperBase<ulong>
    {
        protected override ulong Null()
        {
            throw new DataMappingException("cannot convert null to ulong");
        }

        protected override ulong Data(object value)
        {
            return Convert.ToUInt64(value);
        }
    }
}
