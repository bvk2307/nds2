﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class UnsignedIntFieldMapper : FieldMapperBase<uint>
    {
        protected override uint Null()
        {
            throw new DataMappingException("cannot convert null to uint");
        }

        protected override uint Data(object value)
        {
            return Convert.ToUInt32(value);
        }
    }
}
