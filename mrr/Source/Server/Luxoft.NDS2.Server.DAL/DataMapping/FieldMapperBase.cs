﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal abstract class FieldMapperBase<TField> : IFieldMapper
    {
        public TField ConvertData(object value)
        {
            return value == DBNull.Value ? Null() : Data(value);
        }

        protected abstract TField Null();

        protected abstract TField Data(object value);

        public object MapField(object data)
        {
            return ConvertData(data);
        }
    }
}
