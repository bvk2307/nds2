﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableIntFieldMapper : NullableFieldMapper<int>
    {
        protected override int? Data(object value)
        {
            return new int?(Convert.ToInt32(value));
        }
    }
}
