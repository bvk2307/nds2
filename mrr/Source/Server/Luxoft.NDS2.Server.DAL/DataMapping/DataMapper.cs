﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal abstract class DataMapper<TData> : IDataMapper<TData>
        where TData : new()
    {
        private readonly Dictionary<Type, IFieldMapper> _fieldMappers =
            new Dictionary<Type, IFieldMapper>
            {
                { typeof(string), new StringFieldMapper() },
                { typeof(int), new IntFieldMapper() },
                { typeof(long), new LongFieldMapper() },
                { typeof(decimal), new DecimalFieldMapper() },
                { typeof(bool), new BooleanFieldMapper() },
                { typeof(DateTime), new DateTimeFieldMapper() },
                { typeof(DateTime?), new NullableDateTimeFieldMapper() },
                { typeof(decimal?), new NullableDecimalFieldMapper() },
                { typeof(int?), new NullableIntFieldMapper() },
                { typeof(long?), new NullableLongFieldMapper() },
                { typeof(ulong), new UnsignedLongFieldMapper() },
                { typeof(ulong?), new NullableULongFieldMapper() }
            };

        protected readonly Dictionary<string, PropertyInfo> Properties;
        protected readonly Dictionary<string, IFieldMapper> ColumnMappers = new Dictionary<string, IFieldMapper>();

        protected DataMapper()
        {
            Properties = typeof(TData).GetProperties().ToDictionary(p => p.Name.ToUpper(), p => p);
            foreach (var p in Properties)
            {
                IFieldMapper fm;
                if (_fieldMappers.TryGetValue(p.Value.PropertyType, out fm) && fm != null)
                    ColumnMappers[p.Key] = fm;
            }
        }

        public DataMapper<TData> WithFieldMapperFor(string columnKey, IFieldMapper fieldMapper)
        {
            ColumnMappers[columnKey.ToUpper()] = fieldMapper;
            return this;
        }

        public virtual TData MapData(IDataRecord dataRecord)
        {
            throw new NotImplementedException();
        }
    }
}
