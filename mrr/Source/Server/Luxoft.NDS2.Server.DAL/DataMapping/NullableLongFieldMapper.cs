﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableLongFieldMapper : NullableFieldMapper<long>
    {
        protected override long? Data(object value)
        {
            return Convert.ToInt64(value);
        }
    }
}
