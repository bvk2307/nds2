﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableDecimalFieldMapper : NullableFieldMapper<decimal>
    {
        protected override decimal? Data(object value)
        {
            return Convert.ToDecimal(value);
        }
    }
}
