﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class LongFieldMapper : FieldMapperBase<long>
    {
        protected override long Null()
        {
            throw new DataMappingException("Cannot convert null to long");
        }

        protected override long Data(object value)
        {
            return Convert.ToInt64(value);
        }
    }
}
