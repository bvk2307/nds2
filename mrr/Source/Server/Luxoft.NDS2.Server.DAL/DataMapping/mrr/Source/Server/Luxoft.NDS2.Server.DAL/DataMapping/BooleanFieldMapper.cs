﻿using Luxoft.NDS2.Server.Common;
using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class BooleanFieldMapper : FieldMapperBase<bool>
    {
        protected override bool Null()
        {
            throw new DataMappingException("Cannot convert null to bool");
        }

        protected override bool Data(object value)
        {
            return Convert.ToBoolean(value);
        }
    }
}
