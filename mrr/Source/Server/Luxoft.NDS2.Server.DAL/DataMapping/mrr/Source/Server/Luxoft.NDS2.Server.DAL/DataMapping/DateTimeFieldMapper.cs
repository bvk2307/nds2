﻿using System;
using Luxoft.NDS2.Server.Common;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class DateTimeFieldMapper : FieldMapperBase<DateTime>
    {
        protected override DateTime Null()
        {
            throw new DataMappingException("Cannot convert null to DateTime");
        }

        protected override DateTime Data(object value)
        {
            return Convert.ToDateTime(value);
        }
    }
}
