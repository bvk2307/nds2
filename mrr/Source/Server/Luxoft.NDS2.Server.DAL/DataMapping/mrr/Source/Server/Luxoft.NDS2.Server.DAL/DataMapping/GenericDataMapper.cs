﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class GenericDataMapper<TData> : IDataMapper<TData>
        where TData : new()
    {
        private readonly Dictionary<Type, IFieldMapper> _fieldMappers =
            new Dictionary<Type, IFieldMapper>()
            {
                { typeof(string), new StringFieldMapper() },
                { typeof(int), new IntFieldMapper() },
                { typeof(long), new LongFieldMapper() },
                { typeof(decimal), new DecimalFieldMapper() },
                { typeof(bool), new BooleanFieldMapper() },
                { typeof(DateTime), new DateTimeFieldMapper() },
                { typeof(DateTime?), new NullableDateTimeFieldMapper() },
                { typeof(decimal?), new NullableDecimalFieldMapper() },
                { typeof(int?), new NullableIntFieldMapper() },
                { typeof(long?), new NullableLongFieldMapper() }
            };

        public TData MapData(IDataRecord dataRecord)
        {
            var dataItem = new TData();

            var properties = typeof (TData).GetProperties().ToDictionary(p => p.Name.ToUpper(), p => p);

            for (var fieldIndex = 0; fieldIndex < dataRecord.FieldCount; ++fieldIndex)
            {
                var fieldName = dataRecord.GetName(fieldIndex).ToUpper();
                PropertyInfo property;

                if (properties.TryGetValue(fieldName, out property) && property != null)
                {
                    property.SetValue(
                        dataItem,
                        _fieldMappers[property.PropertyType].MapField(dataRecord[fieldIndex]),
                        null);
                }
            }

            return dataItem;
        }

    }
}
