﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class GenericDataMapper<TData> : DataMapper<TData>
        where TData : new()
    {
        private class PropertyWithMapper
        {
            public PropertyInfo Property { get; private set; }
            public IFieldMapper Mapper { get; private set; }

            public PropertyWithMapper(PropertyInfo property, IFieldMapper mapper)
            {
                Property = property;
                Mapper = mapper;
            }
        }

        private readonly Dictionary<int, PropertyWithMapper> _matchingCache = new Dictionary<int, PropertyWithMapper>();

        public override TData MapData(IDataRecord dataRecord)
        {
            return _matchingCache.Any() ? MapNext(dataRecord) : MapFirst(dataRecord);
        }

        private TData MapFirst(IDataRecord dataRecord)
        {
            var dataItem = new TData();

            for (var fieldIndex = 0; fieldIndex < dataRecord.FieldCount; ++fieldIndex)
            {
                var fieldName = dataRecord.GetName(fieldIndex).ToUpper();
                IFieldMapper fm;
                PropertyInfo property;

                if (Properties.TryGetValue(fieldName, out property) && property != null
                  && ColumnMappers.TryGetValue(fieldName, out fm) && fm != null)
                {
                    _matchingCache[fieldIndex] = new PropertyWithMapper(property, fm);
                    try
                    {
                        property.SetValue(dataItem, fm.MapField(dataRecord[fieldIndex]), null);
                    }
                    catch(Exception e)
                    {
                        throw new Exception(string.Format("Cannot convert field [{0}] to entity property [{1}]", fieldName, property.Name), e);
                    }
                }
            }

            return dataItem;
        }

        private TData MapNext(IDataRecord dataRecord)
        {
            var dataItem = new TData();

            foreach (var item in _matchingCache)
            {
                try
                {
                    item.Value.Property.SetValue(dataItem, item.Value.Mapper.MapField(dataRecord[item.Key]), null);
                }
                catch (Exception e)
                {
                    var fieldName = dataRecord.GetName(item.Key).ToUpper();
                    throw new Exception(string.Format("Cannot convert field [{0}] to entity property [{1}]", fieldName, item.Value.Property.Name), e);
                }
            }

            return dataItem;
        }

    }
}
