﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal abstract class NullableFieldMapper<T> : FieldMapperBase<Nullable<T>>
        where T : struct
    {
        protected override T? Null()
        {
            return null;
        }
    }
}
