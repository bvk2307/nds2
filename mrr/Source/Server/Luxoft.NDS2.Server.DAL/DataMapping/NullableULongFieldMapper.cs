﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableULongFieldMapper : NullableFieldMapper<ulong>
    {
        protected override ulong? Data(object value)
        {
            return Convert.ToUInt64(value);
        }
    }
}
