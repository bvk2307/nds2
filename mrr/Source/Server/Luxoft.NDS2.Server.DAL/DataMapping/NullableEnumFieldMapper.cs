﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableEnumFieldMapper : FieldMapperBase<object>
    {
        private readonly Type _enumType;

        public NullableEnumFieldMapper(Type enumType)
        {
            _enumType = enumType;
        }

        protected override object Data(object value)
        {
            return Enum.ToObject(_enumType, Convert.ToInt32(value));
        }

        protected override object Null()
        {
            return null;
        }
    }
}
