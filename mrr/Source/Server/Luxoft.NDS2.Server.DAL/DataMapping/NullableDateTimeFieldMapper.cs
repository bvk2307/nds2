﻿using System;

namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal class NullableDateTimeFieldMapper : NullableFieldMapper<DateTime>
    {
        protected override DateTime? Data(object value)
        {
            return Convert.ToDateTime(value);
        }
    }
}
