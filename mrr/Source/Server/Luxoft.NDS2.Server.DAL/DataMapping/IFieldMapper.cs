﻿namespace Luxoft.NDS2.Server.DAL.DataMapping
{
    internal interface IFieldMapper
    {
        object MapField(object data);
    }
}
