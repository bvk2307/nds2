﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Pyramid.CommandBuilders
{
    internal class TaxpayersKppQueryBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly string _inn;

        public TaxpayersKppQueryBuilder( string inn )
            : base( PyramidDataAdapter.ProcedureGetTaxpayersKpp )
        {
            _inn = inn;
        }

        protected override string BuildSelectStatement( OracleParameterCollection parameters )
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add( FormatCommandHelper.VarcharIn( "p_Inn", _inn ) );
            parameters.Add( FormatCommandHelper.Cursor( "p_Cursor" ) );

            return result;
        }
    }
}