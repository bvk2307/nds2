﻿#define DONOTUSEKPPYET

using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Pyramid.CommandBuilders
{
    internal class DeductionDetailsQueryBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly GraphNodesKeyParameters _nodesKeyParameters;

        public DeductionDetailsQueryBuilder( GraphNodesKeyParameters nodesKeyParameters )
            : base( PyramidDataAdapter.ProcedureTaxpayerRelation )
        {
            _nodesKeyParameters = nodesKeyParameters;
        }

        protected override string BuildSelectStatement( OracleParameterCollection parameters )
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add( FormatCommandHelper.VarcharIn( "p_Inn", _nodesKeyParameters.Inn ) );
#if !DONOTUSEKPPYET
            parameters.Add( FormatCommandHelper.VarcharIn( "p_Kpp", "" ) );
#endif //!DONOTUSEKPPYET
            parameters.Add( FormatCommandHelper.NumericIn( "p_MaxContractors", _nodesKeyParameters.MostImportantContractors ) );
            parameters.Add( FormatCommandHelper.NumericIn( "p_ByPurchase", _nodesKeyParameters.IsPurchase ? 1 : 0 ) );
            parameters.Add( FormatCommandHelper.VarcharIn( "p_Year", _nodesKeyParameters.TaxYear.ToString() ) );
            parameters.Add( FormatCommandHelper.VarcharIn( "p_Qtr", _nodesKeyParameters.TaxQuarter.ToString() ) );
            parameters.Add( FormatCommandHelper.NumericIn( "p_levels", _nodesKeyParameters.LayersLimit ) );
            parameters.Add( FormatCommandHelper.DecimalIn( "p_sharePercentNdsCriteria", _nodesKeyParameters.SharedNdsPercent ) );
            parameters.Add( FormatCommandHelper.NumericIn( "p_minReturnedContractors", _nodesKeyParameters.MinReturnedContractors ) );
            parameters.Add( FormatCommandHelper.Cursor( "p_Cursor" ) );

            return result;
        }
    }
}