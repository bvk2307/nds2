﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class SearchQueryBuilder : QueryCommandBuilder
    {
        private readonly long _requestId;
        private readonly QueryConditions _criteria;
         
        private const string ViewAlias = "vw";
        private const string ParamRequestId = "pRequestId";
        private const string ParamCursor = "pResultSet";

        private const string SearchSQLCommandPattern =
            "BEGIN OPEN :pResultSet FOR {0}; END;";
        private const string SearchSQLPattern =
            "SELECT vw.* FROM V$PYRAMID_TABLE vw WHERE vw.REQUEST_ID = :pRequestId {0} {1}";

        public SearchQueryBuilder(long requestId, QueryConditions criteria)
        {
            _requestId = requestId;
            _criteria = criteria;
        }

        protected override string BuildSelectStatement(OracleParameterCollection param)
        {
            _criteria.Remove(TypeHelper<ContractorRelationshipsDetail>.GetMemberName(t => t.BuyerName))
                .Remove(TypeHelper<ContractorRelationshipsDetail>.GetMemberName(t => t.Level));

            var query = _criteria.ToSQL(SearchSQLPattern, ViewAlias, false, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.NumericIn(ParamRequestId, _requestId));
            parameters.Add(FormatCommandHelper.Cursor(ParamCursor));

            return string.Format(SearchSQLCommandPattern, query.Text);
        }
    }
}