﻿using System.Data;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Pyramid.DataMapping
{
    internal class TaxpayersKppDataMapper : IDataMapper<string>
    {
        public string MapData(IDataRecord reader)
        { 
            return reader.ReadStringNullable("KPP");
        }
    }
}