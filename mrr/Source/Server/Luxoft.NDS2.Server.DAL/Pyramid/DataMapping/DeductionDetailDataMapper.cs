﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Server.DAL.DataMapping.Pyramid
{
    internal class DeductionDetailDataMapper : IDataMapper<DeductionDetail>
    {
        public DeductionDetail MapData(IDataRecord reader)
        {
            return new DeductionDetail();
        }
    }
}