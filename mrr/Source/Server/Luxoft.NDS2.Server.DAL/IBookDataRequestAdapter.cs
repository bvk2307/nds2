﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы адаптера запросов книг деклараций
    /// </summary>
    public interface IBookDataRequestAdapter
    {
        /// <summary>
        /// Запрашивает статус запроса
        /// </summary>
        /// <param name="requestId">Идентификатор запроса</param>
        /// <returns>Текущий статус запроса</returns>
        DataRequestStatus Status(long requestId);

        /// <summary>
        /// Ищет ранее созданный запрос данных о с/ф по параметрам запроса
        /// </summary>
        /// <param name="inn">ИНН НП</param>
        /// <param name="period">Период подачи декларации</param>
        /// <param name="year">Год подачи декларации</param>
        /// <param name="correctionNumber">Номер корректировки</param>
        /// <param name="partitionNumber">Номер раздела</param>
        /// <returns>Идентификатор запроса или null если запроса с такими параметрами ранее создано не было</returns>
        KeyValuePair<long?, int>? RequestId(
            string inn,
            int period,
            int year,
            int correctionNumber,
            int partitionNumber);

        /// <summary>
        /// Ищет ранее созданный запрос данных о расхождениях по декларации
        /// </summary>
        long? RequestIdNdsDiscrepancies(
            string inn,
            int period,
            int year,
            int correctionNumber,
            int type, 
            int kind);

        /// <summary>
        /// Возвращает статус запроса расхождений по декларации
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        DataRequestStatus StatusNdsDiscrepancy(long requestId);
    }
}
