using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IMacroReportNdsMapAdapter
    {
        List<MapResponseDataNds> GetMapData(MapRequestData model);
    }
}