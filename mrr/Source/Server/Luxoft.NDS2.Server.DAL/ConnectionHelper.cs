﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.Common;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL
{
    internal static class ConnectionHelper
    {
        public static OracleConnection OracleConnection(
            this IConfigurationProvider provider, 
            string configKey)
        {
            return new OracleConnection(provider.GetConfigurationValue(configKey));
        }
    }
}
