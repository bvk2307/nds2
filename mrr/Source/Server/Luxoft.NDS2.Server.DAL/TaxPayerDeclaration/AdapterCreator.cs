﻿using Luxoft.NDS2.Server.Common.Adapters.Declaration;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    public static class AdapterCreator
    {
        public static ITaxPayerDeclarationAdapter TaxPayerDeclarationAdapter(
            this IServiceProvider service)
        {
            return new TaxPayerDeclarationAdapter(service);
        }
    }
}
