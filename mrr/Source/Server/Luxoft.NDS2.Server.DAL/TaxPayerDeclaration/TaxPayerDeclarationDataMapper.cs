﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    internal class TaxPayerDeclarationDataMapper : IDataMapper<TaxPayerDeclarationVersion>
    {
        public TaxPayerDeclarationVersion MapData(IDataRecord dataRecord)
        {
            var result = new TaxPayerDeclarationVersion();

            result.CorrectionNumber = dataRecord.GetString("CorrectionNumber");
            result.DiscrepancyQuantity = dataRecord.GetNullableInt32("DiscrepancyQuantity");
            result.ControlRatioQuantity = dataRecord.GetNullableInt32("ControlRatioQuantity");
            result.InnDeclarant = dataRecord.GetString("InnDeclarant");
            result.IsLarge = dataRecord.GetNullableBoolean("IsLarge");
            result.KnpDiscrepancyQuantity = dataRecord.GetNullableInt32("KnpDiscrepancyQuantity");
            result.KnpStatus = dataRecord.GetString("KnpStatus");
            result.KppDeclarant = dataRecord.GetString("KppDeclarant");
            result.KppEffectiveDeclarant = dataRecord.GetString("KppEffectiveDeclarant");
            result.NdsTotal = dataRecord.GetNullableDecimal("NdsTotal");
            result.RegNumber = dataRecord.GetString("RegNumber");
            result.Sign = dataRecord.GetString("Sign");
            result.SurCode = dataRecord.GetNullableInt32("SurCode");
            result.TaxPeriod = dataRecord.GetString("TaxPeriod");
            result.TypeCode = dataRecord.GetInt32("TypeCode");
            result.TypeDescription = dataRecord.GetString("TypeDescription");
            result.Zip = dataRecord.GetInt64("Zip");
            result.FiscalYear = dataRecord.GetString("FiscalYear");
            result.PeriodCode = dataRecord.GetString("PeriodCode");
            result.InnReorganized = dataRecord.GetString("InnReorganized");
            result.KppReorganized = dataRecord.GetString("KppReorganized");
            result.IsActive = dataRecord.GetBoolean("IsActive");
            result.AnnulmentDate = dataRecord.GetNullableDateTime("AnnulmentDate");
            result.AnnulmentReasonId = dataRecord.GetNullableInt32("AnnulmentReasonId");

            return result;
        }
    }
}
