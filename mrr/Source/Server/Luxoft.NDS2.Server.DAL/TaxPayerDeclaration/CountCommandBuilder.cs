﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    internal class CountCommandBuilder : GetCountCommandBuilder
    {
        private const string viewAlias = "v";

        private const string viewAndAliasFormat = "V$TAXPAYER_DECLARATIONS {0}";

        public CountCommandBuilder(FilterExpressionBase filterBy)
            : base(
                string.Format(viewAndAliasFormat, viewAlias).CountPattern(),
                filterBy,
                new PatternProvider(viewAlias))
        {
        }
    }
}
