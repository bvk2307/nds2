﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    internal class SearchCommandBuilder : PageSearchQueryCommandBuilder
    {
        private const string ViewAlias = "v";

        private const string ViewAndAliasFormat = "V$TAXPAYER_DECLARATIONS {0}";

        public SearchCommandBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy)
            : base(
                string.Format(ViewAndAliasFormat, ViewAlias).SelectPattern(),
                filterBy,
                orderBy,
                new PatternProvider(ViewAlias))
        {
        }
    }
}
