﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters.Declaration;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    internal class TaxPayerDeclarationAdapter : ITaxPayerDeclarationAdapter
    {
        private readonly IServiceProvider _service;

        public TaxPayerDeclarationAdapter(
            IServiceProvider service)
        {
            _service = service;
        }

        public IEnumerable<TaxPayerDeclarationVersion> Search(
            FilterExpressionBase filterBy, 
            List<ColumnSort> orderBy)
        {
            return new ListCommandExecuter<TaxPayerDeclarationVersion>(
                new TaxPayerDeclarationDataMapper(),
                _service)
                .TryExecute(new SearchCommandBuilder(filterBy, orderBy))
                .ToList();
        }

    }
}
