﻿namespace Luxoft.NDS2.Server.DAL.TaxPayerDeclaration
{
    internal static class DbConstants
    {
        public static string TaxPayerDeclarationViewName = "V$TAXPAYER_DECLARATIONS";
    }
}
