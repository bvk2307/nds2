﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Server.DAL.OrganizationStructure
{
    internal class RegionAdapter : DictionaryTableAdapter<Region>
    {
        public RegionAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IOracleCommandBuilder CommandBuilder()
        {
            return new GetDataCommandBuilder(DbConstants.SelectRegionProcedure);
        }
    }
}
