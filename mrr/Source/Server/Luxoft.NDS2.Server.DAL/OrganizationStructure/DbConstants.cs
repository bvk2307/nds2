﻿namespace Luxoft.NDS2.Server.DAL.OrganizationStructure
{
    public static class DbConstants
    {
        public static string Package = "pac$org_structure";

        public static string SelectSonoProcedure = "p$get_sono";

        public static string SelectRegionProcedure = "p$get_regions";

        public static string SelectFederalDistrictProcedure = "p$get_federal_dictricts";

        public static string CursorName = "pData";
    }
}
