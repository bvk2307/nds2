﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.OrganizationStructure
{
    internal class GetDataCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public GetDataCommandBuilder(string procedureName)
            : base(CommandBuildHelper.PackageProcedure(DbConstants.Package, procedureName))
        {
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
 	        parameters.Add(CommandBuildHelper.Cursor(DbConstants.CursorName));
        }
    }
}
