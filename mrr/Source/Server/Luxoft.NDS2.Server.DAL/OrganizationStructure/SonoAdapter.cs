﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.DataMapping;

namespace Luxoft.NDS2.Server.DAL.OrganizationStructure
{
    internal class SonoAdapter : DictionaryTableAdapter<Sono>
    {
        public SonoAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IOracleCommandBuilder CommandBuilder()
        {
            return new GetDataCommandBuilder(DbConstants.SelectSonoProcedure);
        }

        protected override IDataMapper<Sono> DataMapper()
        {
            return new GenericDataMapper<Sono>()
                .WithFieldMapperFor(TypeHelper<Sono>.GetMemberName(x => x.SonoType), new IntFieldMapper());
        }
    }
}
