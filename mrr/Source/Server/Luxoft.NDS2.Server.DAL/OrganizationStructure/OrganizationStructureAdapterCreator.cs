﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.OrganizationStructure
{
    public static class OrganizationStructureAdapterCreator
    {
        public static IDictionaryTableAdapter<Sono> Sono(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SonoAdapter(service, connection);
        }

        public static IDictionaryTableAdapter<Region> Regions(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new RegionAdapter(service, connection);
        }

        public static IDictionaryTableAdapter<FederalDistrict> FederalDistricts(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new FederalDistrictAdapter(service, connection);
        }
    }
}
