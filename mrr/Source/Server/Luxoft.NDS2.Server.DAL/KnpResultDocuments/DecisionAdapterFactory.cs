﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public class DecisionAdapterFactory : AdapterFactoryBase
    {
        public DecisionAdapterFactory(IServiceProvider service)
            : base(service)
        {
        }

        public override Common.Adapters.KnpResultDocuments.IDocumentAdapter DocumentAdapter()
        {
            return new DecisionAdapter(_service);
        }

        public override IDocumentDiscrepancyAdapter ReadonlyDiscrepancyAdapter()
        {
            return new DecisionDiscrepancyAdapter(_service);
        }

        public override IAppendDocumentDiscrepancyAdapter AppendDicrepancyAdapter()
        {
            return new AppendDecisionDiscrepancyAdapter(_service);
        }

        public override IEditDocumentDiscrepancyAdapter EditDiscreancyAdapter()
        {
            return new EditDecisionDiscrepancyAdapter(_service);
        }
    }
}
