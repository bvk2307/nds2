﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public static class KnpResultReportAdapterCreator
    {
        public static IKnpResultReportAdapter Report(IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new ReportAdapter(service, connection);
        }

        public static IReportAggregateAdapter Agregate(IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new ReportAgregateAdapter(service, connection);
        }
    }
}
