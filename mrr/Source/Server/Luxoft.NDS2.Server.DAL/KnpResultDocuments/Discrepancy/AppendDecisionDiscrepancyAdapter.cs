﻿namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class AppendDecisionDiscrepancyAdapter : AppendDiscrepancyAdapterBase
    {
        public AppendDecisionDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string ViewName()
        {
            return DbConstants.DecisionAppendDiscrepancyViewName;
        }

        protected override string UpdateTableName()
        {
            return DbConstants.DecisionStageTableName;
        }

        protected override string CommitAllProcedureName()
        {
            return "P$DECISION_COMMIT_APPEND";
        }

        protected override string RollbackAllProcedureName()
        {
            return "P$DECISION_ROLLBACK_APPEND";
        }

        protected override string CopyAllProcedureName()
        {
            return "P$DECISION_START_APPEND";
        }
    }
}
