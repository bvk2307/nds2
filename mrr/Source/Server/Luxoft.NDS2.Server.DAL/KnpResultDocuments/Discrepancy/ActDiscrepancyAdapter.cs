﻿
namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class ActDiscrepancyAdapter : DiscrepancyAdapterBase
    {
        public ActDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string ViewName()
        {
            return DbConstants.ActDiscrepancyViewName;
        }

        protected override string UpdateTableName()
        {
            return DbConstants.ActStageTableName;
        }
    }
}
