﻿using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class EditActDiscrepancyAdapter : EditDiscrepancyAdapterBase
    {
        public EditActDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string UpdateTableName()
        {
            return DbConstants.ActDiscrepancyTableName;
        }

        protected override string ViewName()
        {
            return DbConstants.ActEditDiscrepancyViewName;
        }

        protected override string UpdateProcedureName()
        {
            return DbConstants.ActUpdateDiscrepancyProcedureName;
        }

        protected override string CloseProcedureName()
        {
            return DbConstants.ActCloseProcedureName;
        }

        protected override string RemoveDiscrepancyProcedureName()
        {
            return DbConstants.ActRemoveDiscrepancyProcedureName;
        }

        protected override string DeleteClosedProcedureName()
        {
            return DbConstants.ActDeleteClosedProcedureName;
        }

        protected override string CountAllInvalidProcedureName()
        {
            return DbConstants.ActCountAllInvalidProcedureName;
        }
    }
}
