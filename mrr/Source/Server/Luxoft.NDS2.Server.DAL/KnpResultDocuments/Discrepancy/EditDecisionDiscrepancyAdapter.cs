﻿using System;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class EditDecisionDiscrepancyAdapter : EditDiscrepancyAdapterBase
    {
        public EditDecisionDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string UpdateTableName()
        {
            return DbConstants.DecisionDiscrepancyTableName;
        }

        protected override string ViewName()
        {
            return DbConstants.DecisionEditDiscrepancyViewName;
        }

        protected override string UpdateProcedureName()
        {
            return DbConstants.DecisionUpdateProcedureName;
        }

        protected override string DeleteClosedProcedureName()
        {
            return DbConstants.DecisionDeleteClosedProcedureName;
        }

        protected override string CloseProcedureName()
        {
            return DbConstants.DecisionCloseProcedureName;
        }

        protected override string RemoveDiscrepancyProcedureName()
        {
            return DbConstants.DecisionRemoveDiscrepancyProcedureName;
        }

        protected override string CountAllInvalidProcedureName()
        {
            return DbConstants.DecisionCountAllInvalidProcedureName;
        }
    }
}
