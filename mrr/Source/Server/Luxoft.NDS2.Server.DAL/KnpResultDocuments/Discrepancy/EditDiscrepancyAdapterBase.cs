﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal abstract class EditDiscrepancyAdapterBase : DiscrepancyAdapterBase, IEditDocumentDiscrepancyAdapter
    {
        public EditDiscrepancyAdapterBase(IServiceProvider service)
            : base(service)
        {
        }

        public void UpdateAmount(long id, decimal amount)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyUpdateSqlCommandBuilder(id, amount, UpdateProcedureName()));
        }

        protected abstract string UpdateProcedureName();

        public void DeleteAllClosed(long documentId)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyDeleteSqlCommandBuilder(DeleteClosedProcedureName(), documentId));
        }

        protected abstract string DeleteClosedProcedureName();

        public int CountAllInvalid(long documentId)
        {
            var result = NonQuerySqlCommandExecuter()
                    .TryExecute(new DocumentCountAllInvalidSqlCommandBuilder(CountAllInvalidProcedureName(), documentId));

            return Int32.Parse(result[DocumentCountAllInvalidSqlCommandBuilder.ParameterCountName].ToString());
        }

        protected abstract string CountAllInvalidProcedureName();

        public void CloseDocument(long documentId)
        {
            var result = NonQuerySqlCommandExecuter()
                    .TryExecute(new DiscrepancyCommitSqlCommandBuilder(documentId, CloseProcedureName()));
        }

        protected abstract string CloseProcedureName();

        public void RemoveDiscrepancy(long documentId, long discrepancyId)
        {
            var result = NonQuerySqlCommandExecuter()
                    .TryExecute(new DiscrepancyRemoveCommandBuilder(RemoveDiscrepancyProcedureName(), documentId, discrepancyId));
        }

        protected abstract string RemoveDiscrepancyProcedureName();

    }
}
