﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal abstract class DiscrepancyAdapterBase : IDocumentDiscrepancyAdapter
    {
        # region Constants

        public const string ViewAlias = "v";

        # endregion

        # region Constructor

        private readonly IServiceProvider _service;

        protected DiscrepancyAdapterBase(IServiceProvider service)
        {
            _service = service;
        }

        # endregion

        # region IDocumentDiscrepancyAdapter

        public IEnumerable<KnpResultDiscrepancy> Search(
            FilterExpressionBase filterBy, 
            IEnumerable<ColumnSort> orderBy, 
            uint rowsToTake, 
            uint rowsToSkip)
        {
            return
                new ListCommandExecuter<KnpResultDiscrepancy>(
                    new GenericDataMapper<KnpResultDiscrepancy>()
                        .WithFieldMapperFor(
                            TypeHelper<KnpResultDiscrepancy>.GetMemberName(x => x.Status),
                            new IntFieldMapper())
                        .WithFieldMapperFor(
                            TypeHelper<KnpResultDiscrepancy>.GetMemberName(x => x.InvoiceChapter),
                            new NullableEnumFieldMapper(typeof(AskInvoiceChapterNumber))),
                    _service)
                    .TryExecute(
                        new OptimizedPageCommandBuilder(
                            ViewName().ToSelectPatternOrdered(ViewAlias),
                            filterBy,
                            orderBy,
                            new PatternProvider(ViewAlias),
                            rowsToSkip,
                            rowsToTake + rowsToSkip));
        }

        protected abstract string ViewName();

        public KnpResultDiscrepancySummary Count(FilterExpressionBase filterBy)
        {
            return
                new ListCommandExecuter<KnpResultDiscrepancySummary>(
                    new GenericDataMapper<KnpResultDiscrepancySummary>(),
                    _service)
                .TryExecute(new DiscrepancyCountSqlCommandBuilder(ViewName(), filterBy))
                .First();
        }

        public void UpdateIncluded(FilterExpressionBase filterBy, bool included)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyIncludeSqlCommandBuilder(
                        UpdateTableName(),
                        ViewName(),
                        filterBy,
                        included));
        }

        protected abstract string UpdateTableName();

        # endregion

        # region CommandExecuters

        protected CommandExecuterBase<Dictionary<string, object>> NonQuerySqlCommandExecuter()
        {
            return new ResultCommandExecuter(_service);
        }

        # endregion
    }
}
