﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal abstract class AppendDiscrepancyAdapterBase : DiscrepancyAdapterBase, IAppendDocumentDiscrepancyAdapter
    {
        protected AppendDiscrepancyAdapterBase(IServiceProvider provider)
            : base(provider)
        {
        }

        public void CopyAll(long documentId)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyCopySqlCommandBuilder(
                        documentId,
                        CopyAllProcedureName()));
        }

        protected abstract string CopyAllProcedureName();

        public void CommitAll(long documentId)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyCommitSqlCommandBuilder(
                        documentId,
                        CommitAllProcedureName()));
        }

        protected abstract string CommitAllProcedureName();

        public void RollbackAll(long documentId)
        {
            NonQuerySqlCommandExecuter()
                .TryExecute(
                    new DiscrepancyRollbackSqlCommandBuilder(
                        documentId,
                        RollbackAllProcedureName()));
        }

        protected abstract string RollbackAllProcedureName();
    }
}
