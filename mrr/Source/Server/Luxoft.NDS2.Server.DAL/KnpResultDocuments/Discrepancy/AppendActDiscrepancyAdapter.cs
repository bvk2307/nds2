﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class AppendActDiscrepancyAdapter : AppendDiscrepancyAdapterBase
    {
        public AppendActDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string ViewName()
        {
            return DbConstants.ActAppendDiscrepancyViewName;
        }

        protected override string UpdateTableName()
        {
            return DbConstants.ActStageTableName;
        }

        protected override string CommitAllProcedureName()
        {
            return "P$ACT_DISCREPANCY_APPEND";
        }

        protected override string RollbackAllProcedureName()
        {
            return "P$ACT_DIS_ROLLBACK_APPEND";
        }

        protected override string CopyAllProcedureName()
        {
            return "P$ACT_DISCREPANCY_START_APPEND";
        }
    }
}
