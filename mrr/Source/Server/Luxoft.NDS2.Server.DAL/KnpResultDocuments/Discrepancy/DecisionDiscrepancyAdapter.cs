﻿
namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy
{
    internal class DecisionDiscrepancyAdapter : DiscrepancyAdapterBase
    {
        public DecisionDiscrepancyAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string ViewName()
        {
            return DbConstants.DecisionDiscrepancyViewName;
        }

        protected override string UpdateTableName()
        {
            return DbConstants.DecisionStageTableName;
        }
    }
}
