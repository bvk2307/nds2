﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class ReportAgregateAdapter : IReportAggregateAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public ReportAgregateAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public IEnumerable<KnpResultReportSummary> Select(FilterExpressionBase filterBy)
        {
            var sqlExec =
                new ListCommandExecuter<KnpResultReportSummary>(
                    new GenericDataMapper<KnpResultReportSummary>(),
                    _service,
                    _connection);

            return sqlExec.TryExecute(new SelectReportSqlCommandBuilder(filterBy));
        }
    }
}
