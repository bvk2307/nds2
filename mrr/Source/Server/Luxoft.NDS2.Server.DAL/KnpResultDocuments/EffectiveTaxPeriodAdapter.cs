﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public static class EffectiveTaxPeriodAdapterCreator
    {
        public static IEffectiveTaxPeriodAdapter EffectiveTaxPeriodAdapter(
            this IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new EffectiveTaxPeriodAdapter(service, connection);
        }
    }

    internal class EffectiveTaxPeriodAdapter : IEffectiveTaxPeriodAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public EffectiveTaxPeriodAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public IEnumerable<EffectiveTaxPeriod> All()
        {
            var cmdBuilder = new SelectPeriodsSqlCommandBuilder();
            var dataMapper =
                new GenericDataMapper<EffectiveTaxPeriod>()
                    .WithFieldMapperFor(
                        TypeHelper<EffectiveTaxPeriod>.GetMemberName(x => x.Quarter),
                        new IntFieldMapper());
            var cmdExecuter = 
                new ListCommandExecuter<EffectiveTaxPeriod>(dataMapper, _service, _connection);

            return cmdExecuter.TryExecute(cmdBuilder);
        }
    }
}
