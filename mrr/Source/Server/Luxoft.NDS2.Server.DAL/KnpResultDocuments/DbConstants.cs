﻿namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public static class DbConstants
    {
        public static string ActAppendDiscrepancyViewName = "V$ACT_DISCREPANCY_APPEND";

        public static string ActEditDiscrepancyViewName = "V$ACT_DISCREPANCY_EDIT";

        public static string ActDiscrepancyViewName = "V$ACT_DISCREPANCY";

        public static string DecisionAppendDiscrepancyViewName = "V$DECISION_DISCREPANCY_APPEND";

        public static string DecisionEditDiscrepancyViewName = "V$DECISION_DISCREPANCY_EDIT";

        public static string DecisionDiscrepancyViewName = "V$DECISION_DISCREPANCY";

        public static string ActStageTableName = "ACT_DISCREPANCY_STAGE";

        public static string DecisionStageTableName = "DECISION_DISCREPANCY_STAGE";

        public static string ActDiscrepancyTableName = "ACT_DISCREPANCY";

        public static string DecisionDiscrepancyTableName = "DECISION_DISCREPANCY";

        public static string PackageName = "PAC$KNP_RESULT_DOCUMENTS";

        public static string ActUpdateDiscrepancyProcedureName = "P$UPDATE_ACT_DISCREPANCY";
        public static string ActCloseProcedureName = "P$ACT_CLOSE";
        public static string ActDeleteClosedProcedureName = "P$ACT_DELETE_ALL_CLOSED";
        public static string ActCountAllInvalidProcedureName = "P$ACT_DIS_INVALID_COUNT";
        public static string ActRemoveDiscrepancyProcedureName = "P$ACT_DIS_REMOVE";

        public static string DecisionUpdateProcedureName = "P$UPDATE_DECISION_DISCREPANCY";
        public static string DecisionCloseProcedureName = "P$DECISION_CLOSE";
        public static string DecisionDeleteClosedProcedureName = "P$DECISION_DELETE_ALL_CLOSED";
        public static string DecisionCountAllInvalidProcedureName = "P$DECISION_DIS_INVALID_COUNT";
        public static string DecisionRemoveDiscrepancyProcedureName = "P$DECISION_DIS_REMOVE";

        public static string KnpResultReportAgregateView = "v$knp_result_report_agregate";
        public static string ReportIdProcedureName = "P$GET_REPORT_ID";
        public static string ReportIdOutParameter = "pId";
        public static string ReportIdYearParameter = "pFiscalYear";
        public static string ReportIdQuarterParameter = "pQuarter";
        public static string ReportIdDateParameter = "pReportDate";
        public static long ReportIdNotFound = -1;

        public static string ReportPeriodsProcedureName = "P$GET_REPORT_PERIODS";
        public static string ReportPeriodsCursorName = "pResult";
    }
}
