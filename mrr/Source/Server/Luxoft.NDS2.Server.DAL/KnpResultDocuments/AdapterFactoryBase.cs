﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public abstract class AdapterFactoryBase : IDocumentAdapterFactory
    {
        protected readonly IServiceProvider _service;

        protected AdapterFactoryBase(IServiceProvider service)
        {
            _service = service;
        }

        public IEditSessionAdapter SessionAdapter()
        {
            return new EditSessionAdapter(_service);
        }

        public abstract Common.Adapters.KnpResultDocuments.IDocumentAdapter DocumentAdapter();

        public abstract IDocumentDiscrepancyAdapter ReadonlyDiscrepancyAdapter();

        public abstract IAppendDocumentDiscrepancyAdapter AppendDicrepancyAdapter();

        public abstract IEditDocumentDiscrepancyAdapter EditDiscreancyAdapter();

        public IUpdateDecisionStatusAdapter UpdateStatusAdapter()
        {
            return new DecisionUpdateStatusAdapter(_service);
        }
    }
}
