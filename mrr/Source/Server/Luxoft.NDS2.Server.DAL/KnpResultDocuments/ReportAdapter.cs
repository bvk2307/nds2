﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class ReportAdapter : IKnpResultReportAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public ReportAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public long GetId(int fiscalYear, int quarter, DateTime reportDate)
        {
            var sqlExecuter = new ResultCommandExecuter(_service, _connection);
            var commandBuilder = new GetReportIdSqlCommandBuilder(fiscalYear, quarter, reportDate);

            var result = sqlExecuter.TryExecute(commandBuilder);
            var reportId = long.Parse(result[DbConstants.ReportIdOutParameter].ToString());

            if (reportId == DbConstants.ReportIdNotFound)
            {
                var errorMessage = BuildNoDataErrorMessage(fiscalYear, quarter, reportDate);
                throw new ObjectNotFoundException(errorMessage);
            }

            return reportId;
        }

        private string BuildNoDataErrorMessage(int fiscalYear, int quarter, DateTime reportDate)
        {
            const string messageSeparator = " ";
            const string noDataMessage = "- нет данных для отчета";
            var period = new EffectiveTaxPeriod() { Quarter = (Quarter)quarter, Year = fiscalYear };

            return String.Concat(period, messageSeparator, reportDate.ToShortDateString(), messageSeparator, noDataMessage);
        }
    }
}
