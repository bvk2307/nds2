﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class SeodDecisionAdapter : ISeodDecisionAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public SeodDecisionAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public KnpDecisionType[] Search(
            string innDeclarant, 
            string inn, 
            string kppEffective, 
            string periodCode, 
            string fiscalYear)
        {
            return new ListCommandExecuter<KnpDecisionType>(new KnpDecisionTypeMapper(), _service, _connection)
                .TryExecute(new SeodDecisionSearchSqlCommandBuilder(innDeclarant, inn, kppEffective, periodCode, fiscalYear))
                .ToArray();                
        }
    }

    internal class KnpDecisionTypeMapper : IDataMapper<KnpDecisionType>
    {
        public KnpDecisionType MapData(IDataRecord dataRecord)
        {
            var code = Convert.ToInt32(dataRecord["DecisionType"]);

            return (KnpDecisionType)code;
        }
    }
}
