﻿namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class ActAdapter : DocumentAdapter
    {
        public ActAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string InsertProcedureName()
        {
            return "P$INSERT_ACT";
        }

        protected override string UpdateProcedureName()
        {
            return "P$UPDATE_ACT";
        }
    }
}
