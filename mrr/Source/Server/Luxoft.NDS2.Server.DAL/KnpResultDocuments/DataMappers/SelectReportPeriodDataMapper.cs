﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.DataMappers
{
    internal class SelectReportPeriodDataMapper : IDataMapper<EffectiveTaxPeriod>
    {
        public EffectiveTaxPeriod MapData(IDataRecord reader)
        {
            var result = new EffectiveTaxPeriod();

            result.Quarter = (Quarter)reader.GetInt32(TypeHelper<EffectiveTaxPeriod>.GetMemberName(t => t.Quarter));
            result.Year = reader.GetInt32(TypeHelper<EffectiveTaxPeriod>.GetMemberName(t => t.Year));

            return result;
        }
    }
}
