﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class DecisionUpdateStatusAdapter : IUpdateDecisionStatusAdapter
    {
        private readonly IServiceProvider _service;

        public DecisionUpdateStatusAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public int GetStatus()
        {
            var result =
                new ResultCommandExecuter(_service)
                    .TryExecute(new DecisionUpdateStatusSqlCommandBuilder());

            return Convert.ToInt32(((OracleDecimal)result[DecisionUpdateStatusSqlCommandBuilder.ParameterEnabledName]).Value);
        }
    }
}
