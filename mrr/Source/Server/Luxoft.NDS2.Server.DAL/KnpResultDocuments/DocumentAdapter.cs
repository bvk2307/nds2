﻿using def = Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal abstract class DocumentAdapter : def.IDocumentAdapter
    {
        private readonly IServiceProvider _service;

        protected DocumentAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public long Insert(string innDeclarant, string inn, string kpp, string kppEffective, int fiscalYear, string periodCode)
        {
            var commandResult =
                new ResultCommandExecuter(_service)
                    .TryExecute(
                        new DocumentInsertSqlCommandBuilder(
                            InsertProcedureName(),
                            innDeclarant, 
                            inn,
                            kpp,
                            kppEffective, 
                            periodCode, 
                            fiscalYear));

           return Int64.Parse(commandResult[DocumentInsertSqlCommandBuilder.ParameterIdName].ToString());
        }

        protected abstract string InsertProcedureName();

        public void Update(long id, DateTime? closedAt)
        {
            new ResultCommandExecuter(_service)
                .TryExecute(
                    new DocumentUpdateSqlCommandBuilder(UpdateProcedureName(), id, closedAt));
        }

        protected abstract string UpdateProcedureName();
    }
}
