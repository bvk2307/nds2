﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public static class SeodDecisionAdapterCreator
    {
        public static ISeodDecisionAdapter NewAdapter(
            IServiceProvider service, 
            ConnectionFactoryBase connection)
        {
            return new SeodDecisionAdapter(service, connection);
        }
    }
}
