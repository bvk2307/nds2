﻿namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class DecisionAdapter : DocumentAdapter
    {
        public DecisionAdapter(IServiceProvider service)
            : base(service)
        {
        }

        protected override string InsertProcedureName()
        {
            return "P$INSERT_DECISION";
        }

        protected override string UpdateProcedureName()
        {
            return "P$UPDATE_DECISION";
        }
    }
}
