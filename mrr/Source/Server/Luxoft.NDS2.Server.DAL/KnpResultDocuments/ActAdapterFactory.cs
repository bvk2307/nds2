﻿using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.Discrepancy;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    public class ActAdapterFactory : AdapterFactoryBase
    {
        public ActAdapterFactory(IServiceProvider service)
            : base(service)
        {
        }

        public override Common.Adapters.KnpResultDocuments.IDocumentAdapter DocumentAdapter()
        {
            return new ActAdapter(_service);
        }

        public override IDocumentDiscrepancyAdapter ReadonlyDiscrepancyAdapter()
        {
            return new ActDiscrepancyAdapter(_service);
        }

        public override IAppendDocumentDiscrepancyAdapter AppendDicrepancyAdapter()
        {
            return new AppendActDiscrepancyAdapter(_service);
        }

        public override IEditDocumentDiscrepancyAdapter EditDiscreancyAdapter()
        {
            return new EditActDiscrepancyAdapter(_service);
        }
    }
}
