﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyUpdateSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _discrepancyId;

        private readonly decimal _amount;

        public DiscrepancyUpdateSqlCommandBuilder(
            long discrepancyId,
            decimal amount,
            string procedureName)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _discrepancyId = discrepancyId;
            _amount = amount;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn("pDiscrepancyId", _discrepancyId))
                .With(FormatCommandHelper.DecimalIn("pAmount", _amount));
        }
    }
}
