﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class GetReportIdSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly int _fiscalYear;

        private readonly int _quarter;

        private readonly DateTime _reportDate;

        public GetReportIdSqlCommandBuilder(int fiscalYear, int quarter, DateTime reportDate)
            : base(
                FormatCommandHelper.PackageProcedure(
                    DbConstants.PackageName, 
                    DbConstants.ReportIdProcedureName))
        {
            _fiscalYear = fiscalYear;
            _quarter = quarter;
            _reportDate = reportDate;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericOut(DbConstants.ReportIdOutParameter));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ReportIdYearParameter, _fiscalYear));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ReportIdQuarterParameter, _quarter));
            parameters.With(FormatCommandHelper.DateIn(DbConstants.ReportIdDateParameter, _reportDate));
        }
    }
}
