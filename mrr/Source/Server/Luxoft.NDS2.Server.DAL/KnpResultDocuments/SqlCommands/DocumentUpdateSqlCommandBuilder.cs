﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DocumentUpdateSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _id;

        private readonly DateTime? _closedAt;
 
        public DocumentUpdateSqlCommandBuilder(string procedureName, long id, DateTime? closedAt)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _id = id;
            _closedAt = closedAt;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn("pId", _id))
                .With(FormatCommandHelper.DateIn("pCloseDate", _closedAt));
        }
    }
}
