﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SeodDecisionSearchSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public static string ParameterCursor = "pResult";

        private readonly string _innDeclarant;

        private readonly string _inn;

        private readonly string _kppEffetive;

        private readonly string _periodCode;

        private readonly string _fiscalYear;

        public SeodDecisionSearchSqlCommandBuilder(
            string innDeclarant,
            string inn,
            string kppEffective,
            string periodCode,
            string fiscalYear)
            : base(FormatCommandHelper.PackageProcedure("PAC$DECLARATION", "P$GET_SEOD_DECISION"))
        {
            _innDeclarant = innDeclarant;
            _inn = inn;
            _kppEffetive = kppEffective;
            _periodCode = periodCode;
            _fiscalYear = fiscalYear;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.VarcharIn("pInnDeclarant", _innDeclarant))
                .With(FormatCommandHelper.VarcharIn("pInn", _inn))
                .With(FormatCommandHelper.VarcharIn("pKppEffective", _kppEffetive))
                .With(FormatCommandHelper.VarcharIn("pPeriodCode", _periodCode))
                .With(FormatCommandHelper.VarcharIn("pFiscalYear", _fiscalYear))
                .With(FormatCommandHelper.Cursor("pResult"));
        }
    }
}
