﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyCopySqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _id;

        public DiscrepancyCopySqlCommandBuilder(long id, string procedureName)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _id = id;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn("pId", _id));
        }
    }
}
