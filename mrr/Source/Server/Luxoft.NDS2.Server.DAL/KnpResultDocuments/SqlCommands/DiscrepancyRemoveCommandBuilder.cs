﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyRemoveCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _documentId;
        private readonly long _discrepancyId;

        private const string _parameterDocumentIdName = "pDocumentId";
        private const string _parameterDiscrepancyIdName = "pDiscrepancyId";

        public DiscrepancyRemoveCommandBuilder(string procedureName, long documentId, long discrepancyId)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _documentId = documentId;
            _discrepancyId = discrepancyId;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn(_parameterDocumentIdName, _documentId))
                .With(FormatCommandHelper.NumericIn(_parameterDiscrepancyIdName, _discrepancyId));
        }
    }
}
