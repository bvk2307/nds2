﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DocumentCountAllInvalidSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _documentId;
        private string _parameterDocumentIdName = "pId";
        public static string ParameterCountName = "pCount";

        public DocumentCountAllInvalidSqlCommandBuilder(string procedureName, long documentId)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _documentId = documentId;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn(_parameterDocumentIdName, _documentId))
                .With(FormatCommandHelper.NumericOut(ParameterCountName));
        }
    }
}
