﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    public class DiscrepancyCountSqlCommandBuilder : QueryCommandBuilder
    {
        private const string ViewAlias = "v";

        private const string SqlQueryPattern =
            "select count(1) as Quantity, nvl(sum(case when included = 1 then amount else 0 end),0) as Amount, nvl(sum(case when included = 1 then amountbyact else 0 end),0) as ActAmount, nvl(sum(case when included = 1 then amountbydecision else 0 end),0) as DecisionAmount, nvl(sum(case when included = 1 and status = 1 then 1 else 0 end),0) as IncludedQuantity, nvl(sum(case when status = 2 then 1 else 0 end),0) as ClosedQuantity from {0} {1}";

        private const string SqlWherePattern = " where {0}";

        private readonly FilterExpressionBase _filterBy;

        private readonly string _viewName;

        public DiscrepancyCountSqlCommandBuilder(string viewName, FilterExpressionBase filterBy)
        {
            _viewName = viewName;
            _filterBy = filterBy;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var parametersBuilder = new OracleParameterCollectionBuilder(parameters, "p{0}");
            var sqlQueryText = new StringBuilder();

            sqlQueryText.AppendFormat(SqlQueryPattern, _viewName, ViewAlias);
            sqlQueryText.AppendFormat(SqlWherePattern, _filterBy.ToQuery(parametersBuilder, new PatternProvider(ViewAlias)));

            return sqlQueryText.ToString();
        }
    }
}
