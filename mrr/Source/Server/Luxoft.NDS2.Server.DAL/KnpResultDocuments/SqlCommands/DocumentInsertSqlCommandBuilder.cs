﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DocumentInsertSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        # region DbConstants

        public const string ParameterIdName = "pId";

        private static string ParameterInnName = "pInn";

        private static string ParameterKppName = "pKpp";

        private static string ParameterInnDeclarantName = "pInnDeclarant";

        private const string ParameterKppEffectiveName = "pKppEffective";

        private const string ParameterTaxPeriodName = "pPeriodCode";

        private const string ParameterFiscalYear = "pFiscalYear";

        # endregion

        # region Constructor

        private readonly string _inn;

        private readonly string _kpp;

        private readonly string _innDeclarant;

        private readonly string _kppEffective;

        private readonly string _periodCode;

        private readonly int _fiscalYear;

        public DocumentInsertSqlCommandBuilder(
            string procedureName,
            string innDeclarant,
            string inn,
            string kpp,
            string kppEffective,
            string periodCode,
            int fiscalYear)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _inn = inn;
            _kpp = kpp;
            _innDeclarant = innDeclarant;
            _kppEffective = kppEffective;
            _periodCode = periodCode;
            _fiscalYear = fiscalYear;
        }

        # endregion

        # region Sql Command Building

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericOut(ParameterIdName))
                .With(FormatCommandHelper.VarcharIn(ParameterInnDeclarantName, _innDeclarant))
                .With(FormatCommandHelper.VarcharIn(ParameterInnName, _inn))
                .With(FormatCommandHelper.VarcharIn(ParameterKppName, _kpp))
                .With(FormatCommandHelper.VarcharIn(ParameterKppEffectiveName, _kppEffective))
                .With(FormatCommandHelper.VarcharIn(ParameterTaxPeriodName, _periodCode))
                .With(FormatCommandHelper.VarcharIn(ParameterFiscalYear, _fiscalYear.ToString()));
        }

        # endregion
    }
}
