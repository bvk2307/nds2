﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SelectPeriodsSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public SelectPeriodsSqlCommandBuilder()
            : base(
                FormatCommandHelper.PackageProcedure(
                    DbConstants.PackageName,
                    DbConstants.ReportPeriodsProcedureName))
        {
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.Cursor(DbConstants.ReportPeriodsCursorName));
        }
    }
}
