﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SessionDeleteSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _documentId;

        public SessionDeleteSqlCommandBuilder(long documentId)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, "P$DELETE_SESSION"))
        {
            _documentId = documentId;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn("pDocumentId", _documentId));
        }
    }
}
