﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyDeleteSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _id;

        public DiscrepancyDeleteSqlCommandBuilder(string procedureName, long id)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _id = id;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn("pDocumentId", _id));
        }
    }
}
