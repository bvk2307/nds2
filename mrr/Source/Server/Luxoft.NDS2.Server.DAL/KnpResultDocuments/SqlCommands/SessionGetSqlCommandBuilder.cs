﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SessionGetSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        # region Constants

        private const string ProcedureName = "P$GET_SESSION";

        private const string ParameterDocumentId = "pDocumentId";

        private const string ParameterCursor = "pSession";

        # endregion

        # region Constructor

        private readonly long _documentId;

        public SessionGetSqlCommandBuilder(long documentId)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, ProcedureName))
        {
            _documentId = documentId;
        }

        # endregion

        # region Add Parameters

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn(ParameterDocumentId, _documentId))
                .With(FormatCommandHelper.Cursor(ParameterCursor));
        }

        # endregion
    }
}
