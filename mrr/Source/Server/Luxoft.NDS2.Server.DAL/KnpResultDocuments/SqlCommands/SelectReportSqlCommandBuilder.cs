﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SelectReportSqlCommandBuilder : SearchQueryCommandBuilder
    {
        private const string ViewAlias = "vw";

        public SelectReportSqlCommandBuilder(FilterExpressionBase filterBy)
            : base(
                DbConstants.KnpResultReportAgregateView.SelectPattern(ViewAlias),
                filterBy,
                Enumerable.Empty<ColumnSort>(),
                new PatternProvider(ViewAlias))
        {
        }
    }
}
