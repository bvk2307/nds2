﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyRollbackSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly long _actId;

        public DiscrepancyRollbackSqlCommandBuilder(long actId, string procedureName)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, procedureName))
        {
            _actId = actId;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn("pId", _actId));
        }
    }
}
