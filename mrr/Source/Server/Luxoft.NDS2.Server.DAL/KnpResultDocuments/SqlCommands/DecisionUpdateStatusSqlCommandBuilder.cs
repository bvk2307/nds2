﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DecisionUpdateStatusSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public static string ParameterEnabledName = "pEnabled";

        public DecisionUpdateStatusSqlCommandBuilder()
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, "P$GET_DECISION_UPDATE_STATE"))
        {
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericOut(ParameterEnabledName));
        }
    }
}
