﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SessionInsertSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly EditSession _session;

        public SessionInsertSqlCommandBuilder(EditSession session)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.PackageName, "P$INSERT_SESSION"))
        {
            _session = session;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters
                .With(FormatCommandHelper.NumericIn("pDocumentId", _session.DocumentId))
                .With(FormatCommandHelper.VarcharIn("pUserSid", _session.UserSid))
                .With(FormatCommandHelper.VarcharIn("pUserName", _session.UserName))
                .With(FormatCommandHelper.DateIn("pExpiredAt", _session.ExpiredAt));
        }
    }
}
