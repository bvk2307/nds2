﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class DiscrepancyIncludeSqlCommandBuilder : IOracleCommandBuilder
    {
        private const string ViewAlias = "v";

        private const string SqlPattern =
            "begin update {0} set included = :pIncluded where id in (select id from {1} {2} where {3}); end;";

        private readonly bool _included;

        private readonly FilterExpressionBase _filterBy;

        private readonly string _viewName;

        private readonly string _tableName;

        public DiscrepancyIncludeSqlCommandBuilder(
            string tableName, 
            string viewName, 
            FilterExpressionBase filterBy, 
            bool included)
        {
            _tableName = tableName;
            _viewName = viewName;
            _included = included;
            _filterBy = filterBy;
        }

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.BindByName = true;

            var parametersBuilder =
                new OracleParameterCollectionBuilder(
                    command.Parameters,
                    "p{0}");
            command.CommandText = 
                string.Format(
                    SqlPattern, 
                    _tableName,
                    _viewName,
                    ViewAlias,
                    _filterBy.ToQuery(parametersBuilder, new PatternProvider(ViewAlias)));
            command.Parameters.Add(FormatCommandHelper.NumericIn("pIncluded", _included ? 1 : 0));

            return command;
        }
    }
}
