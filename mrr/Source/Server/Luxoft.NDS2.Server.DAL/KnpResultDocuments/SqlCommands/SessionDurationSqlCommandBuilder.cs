﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands
{
    internal class SessionDurationSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public static string ParameterName = "pDuration";

        public SessionDurationSqlCommandBuilder()
            : base(
                FormatCommandHelper.PackageProcedure(
                    DbConstants.PackageName, 
                    "P$GET_SESSION_DURATION"))
        {
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericOut(ParameterName));
        }
    }
}
