﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments.SqlCommands;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.KnpResultDocuments
{
    internal class EditSessionAdapter : IEditSessionAdapter
    {
        private readonly IServiceProvider _service;

        public EditSessionAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public bool TryGet(long documentId, out EditSession session)
        {
            session =
                new ListCommandExecuter<EditSession>(
                    new GenericDataMapper<EditSession>(),
                    _service)
                    .TryExecute(new SessionGetSqlCommandBuilder(documentId))
                    .FirstOrDefault();

            return session != null;
        }

        public void Insert(EditSession session)
        {
            new ResultCommandExecuter(_service)
                .TryExecute(new SessionInsertSqlCommandBuilder(session));
        }

        public void Delete(long documentId)
        {
            new ResultCommandExecuter(_service)
                .TryExecute(new SessionDeleteSqlCommandBuilder(documentId));
        }

        public void Update(EditSession session)
        {
            new ResultCommandExecuter(_service)
                .TryExecute(new SessionUpdateSqlCommandBuilder(session));
        }

        public int GetDuration()
        {
            var commandResult =
                new ResultCommandExecuter(_service)
                    .TryExecute(new SessionDurationSqlCommandBuilder());

            return Int32.Parse(commandResult[SessionDurationSqlCommandBuilder.ParameterName].ToString());
        }
    }
}
