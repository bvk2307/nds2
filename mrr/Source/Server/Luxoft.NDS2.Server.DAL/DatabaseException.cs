﻿using System;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот класс представляет абстрактное исключение уровня БД
    /// </summary>
    public class DatabaseException : Exception
    {
        internal DatabaseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
