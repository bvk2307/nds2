﻿namespace Luxoft.NDS2.Server.DAL.SystemSettings
{
    public static class DbConstants
    {
        public static string SystemSettingsPackage = "PAC$SYSTEM_SETTINGS";
        public static string SystemSettingsListsPackage = "PAC$TAX_PAYER_RESTRICTION";
        public static string LoadSettingsProcedure = "P$LOAD";
        public static string SaveSettingsProcedure = "P$SAVE";
        public static string LoadInnListProcedure = "P$LOAD_INN_LIST";
        public static string UpdateListProcedure = "P$UPDATE";
        public static string AllListsProcedure = "P$ALL";
        public static string ActiveWhiteListsProcedure = "P$ACTIVE_WHITE_LISTS";


        public static string CursorParameter = "pCursor";
        public static string ClaimResendingTimeoutParameter = "pClaimResendingTimeout";
        public static string ResendingAttemptsParameter = "pResendingAttempts";
        public static string ClaimDeliveryTimeoutParameter = "pClaimDeliveryTimeout";
        public static string ClaimExplainTimeoutParameter = "pClaimExplainTimeout";
        public static string ReclaimReplyTimeoutParameter = "pReclaimReplyTimeout";
        public static string ReplyEntryParameter = "pReplyEntryTimeout";
        public static string NameParameter = "pName";
        public static string CreatedByParameter = "pCreatedBy";
        public static string IsActiveParameter = "pIsActive";
        public static string InnListParameter = "pInnList";
        public static string IdParameter = "pId";
        public static string TypeParameter = "pType";

        public static string InnDb = "INN";

        public static string WhiteListsViewName = "V$WHITE_LISTS";
    }
}
