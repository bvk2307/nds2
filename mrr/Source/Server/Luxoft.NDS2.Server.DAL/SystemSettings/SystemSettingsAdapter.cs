﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.Common.Adapters.SystemSettings;
using Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.SystemSettings
{
    internal class SystemSettingsAdapter : ISystemSettingsAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public SystemSettingsAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        /// <summary>
        /// Загружает параметров АСК НДС-2
        /// </summary>
        /// <returns></returns>
        public NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings Load()
        {
            var sqlExecuter = new ListCommandExecuter<NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings>(
                new GenericDataMapper<NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings>(), 
                _service,
                _connection);
            var commandBuilder = new LoadSettingsCommandBuilder();
            return sqlExecuter.TryExecute(commandBuilder).FirstOrDefault();
        }

        /// <summary>
        /// Сохраняет параметры АСК НДС-2
        /// </summary>
        /// <param name="claimResendingTimeout">Время ожидания ответа от СЭОД</param>
        /// <param name="resendingAttempts">количество повторных отправок</param>
        /// <param name="claimDeliveryTimeout">Время ожидания вручения Ат/АИ</param>
        /// <param name="claimExplainTimeout">Время ожидания ответа на АТ </param>
        /// <param name="reclaimReplyTimeout">Время ожидания ответа на АИ</param>
        /// <param name="replyEntry">Время ожидания ввода ответа на АИ</param>
        public void Save(int claimResendingTimeout, int resendingAttempts, int claimDeliveryTimeout, int claimExplainTimeout,
            int reclaimReplyTimeout, int replyEntry)
        {
            new ResultCommandExecuter(_service, _connection)
                .TryExecute(new SaveSettingsCommandBuilder(claimResendingTimeout, 
                resendingAttempts,
                claimDeliveryTimeout,
                claimExplainTimeout,
                reclaimReplyTimeout,
                replyEntry));
        }
    }
}