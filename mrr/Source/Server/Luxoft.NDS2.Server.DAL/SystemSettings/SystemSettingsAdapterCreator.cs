﻿using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.Adapters.SystemSettings;

namespace Luxoft.NDS2.Server.DAL.SystemSettings
{
    /// <summary>
    ///     Создает адаптеры для использования методов, работающих с Параметрами АСК НДС-2
    /// </summary>
    public static class SystemSettingsAdapterCreator
    {
        public static ISystemSettingsAdapter SystemSettingsAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new SystemSettingsAdapter(serviceProvider, connection);
        }

        public static ISystemSettingsListsAdapter SystemSettingsListsAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new SystemSettingsListsAdapter(serviceProvider, connection);
        }

        public static ISearchAdapter<SystemSettingsList> WhiteListsAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new WhiteListsAdapter(serviceProvider, connection);
        }
    }
}