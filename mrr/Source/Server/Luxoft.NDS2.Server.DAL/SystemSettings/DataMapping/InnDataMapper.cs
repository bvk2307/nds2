﻿using Luxoft.NDS2.Server.DAL.Helpers;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.DataMapping
{
    internal class InnDataMapper : IDataMapper<string>
    {
        public string MapData(IDataRecord reader)
        { 
            return reader.ReadStringNullable(DbConstants.InnDb);
        }
    }
}