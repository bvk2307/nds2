﻿using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Server.Common.Adapters.SystemSettings;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.DAL.SystemSettings.DataMapping;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Server.DAL.Explain.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.SystemSettings
{
    internal class SystemSettingsListsAdapter : ISystemSettingsListsAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public SystemSettingsListsAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <param name="activate">активировать или деактивировать</param>
        /// <returns></returns>
        public void Update(long id, bool activate)
        {
            new ResultCommandExecuter(_service, _connection)
                .TryExecute(new UpdateListCommandBuilder(id, activate));
        }

        /// <summary>
        /// Загружает спиок ИНН для конкретного списка настроек
        /// </summary>
        /// <param name="id">идентификатор списка</param>
        /// <returns></returns>
        public string[] LoadInnList(long id)
        {
            var sqlExecuter = new ListCommandExecuter<string>(new InnDataMapper(), _service, _connection);
            var commandBuilder = new LoadInnListCommandBuilder(id);

            var result = sqlExecuter.TryExecute(commandBuilder);
            return result.ToArray();
        }
        
        /// <summary>
        /// Сохраняет список
        /// </summary>
        /// <param name="list"></param>
        public void Save(SystemSettingsList list)
        {
            new ResultCommandExecuter(_service, _connection)
                        .TryExecute(new SaveListCommandBuilder(list));
        }

        /// <summary>
        /// Возвращает списки заданного типа
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<SystemSettingsList> All(SettingsListType type)
        {
            var sqlExecuter = new ListCommandExecuter<SystemSettingsList>(
                 new GenericDataMapper<SystemSettingsList>(),
                 _service,
                 _connection);
            var commandBuilder = new AllListsCommandBuilder((int)type);
            return sqlExecuter.TryExecute(commandBuilder).ToList();

        }
    }
}