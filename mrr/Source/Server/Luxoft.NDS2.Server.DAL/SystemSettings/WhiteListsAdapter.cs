﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Server.DAL.SystemSettings
{
    internal class WhiteListsAdapter : SearchDataAdapterBase<SystemSettingsList>
    {
        public WhiteListsAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<SystemSettingsList> DataMapper()
        {
            return new GenericDataMapper<SystemSettingsList>();
        }

        protected override string ViewName()
        {
            return DbConstants.WhiteListsViewName;
        }
    }
}
