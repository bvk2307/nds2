﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая сохранять Параметры АСК НДС-2
    /// </summary>
    class SaveSettingsCommandBuilder : SystemSettingsPackageCommand
    {

        private readonly int _claimResendingTimeout;
        private readonly int _resendingAttempts;
        private readonly int _claimDeliveryTimeout;
        private readonly int _claimExplainTimeout;
        private readonly int _reclaimReplyTimeout;
        private readonly int _replyEntry;

        /// <summary>
        /// Сохраняет параметры АСК НДС-2
        /// </summary>
        /// <param name="claimResendingTimeout">Время ожидания ответа от СЭОД</param>
        /// <param name="resendingAttempts">количество повторных отправок</param>
        /// <param name="claimDeliveryTimeout">Время ожидания вручения Ат/АИ</param>
        /// <param name="claimExplainTimeout">Время ожидания ответа на АТ </param>
        /// <param name="reclaimReplyTimeout">Время ожидания ответа на АИ</param>
        /// <param name="replyEntry">Время ожидания ввода ответа на АИ</param>
        public SaveSettingsCommandBuilder(int claimResendingTimeout,
                                          int resendingAttempts,
                                          int claimDeliveryTimeout,
                                          int claimExplainTimeout,
                                          int reclaimReplyTimeout,
                                          int replyEntry) : base(DbConstants.SaveSettingsProcedure)
        {
            _claimResendingTimeout = claimResendingTimeout;
            _resendingAttempts = resendingAttempts;
            _claimDeliveryTimeout = claimDeliveryTimeout;
            _claimExplainTimeout = claimExplainTimeout;
            _reclaimReplyTimeout = reclaimReplyTimeout;
            _replyEntry = replyEntry;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ClaimResendingTimeoutParameter, _claimResendingTimeout));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ResendingAttemptsParameter, _resendingAttempts));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ClaimDeliveryTimeoutParameter, _claimDeliveryTimeout));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ClaimExplainTimeoutParameter, _claimExplainTimeout));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ReclaimReplyTimeoutParameter, _reclaimReplyTimeout));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ReplyEntryParameter, _replyEntry));

        }
    }
}
