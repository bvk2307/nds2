﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, работающая с запросами из пакета PAC$SYSTEM_SETTINGS 
    /// </summary>
    class SystemSettingsPackageCommand : ExecuteProcedureCommandBuilder
    {
        public SystemSettingsPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.SystemSettingsPackage, procedureName))
        {
        }
    }
}
