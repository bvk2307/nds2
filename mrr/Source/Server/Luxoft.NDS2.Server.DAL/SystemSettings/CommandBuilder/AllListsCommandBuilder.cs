﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая загружать Список 
    /// </summary>
    class AllListsCommandBuilder : SystemSettingsListsPackageCommand
    {
        private readonly int _type;
        public AllListsCommandBuilder(int type) : base(DbConstants.AllListsProcedure)
        {
            _type = type;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.TypeParameter, _type));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.CursorParameter));
        }
    }
}
