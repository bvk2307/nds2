﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая загружать Список ИНН
    /// </summary>
    class LoadInnListCommandBuilder : SystemSettingsListsPackageCommand
    {
        private readonly long _id;
        public LoadInnListCommandBuilder(long id) : base(DbConstants.LoadInnListProcedure)
        {
            _id = id;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IdParameter, _id));

            parameters.With(FormatCommandHelper.Cursor(DbConstants.CursorParameter));
        }
    }
}
