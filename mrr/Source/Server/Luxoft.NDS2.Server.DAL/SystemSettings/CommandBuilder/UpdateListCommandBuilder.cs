﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая активировать БС
    /// </summary>
    class UpdateListCommandBuilder : SystemSettingsListsPackageCommand
    {
        private readonly long _id;
        private readonly bool _activate;

        /// <summary>
        /// Конструктор для команды активирования БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <param name="activate">активировать/деактивировать</param>
        public UpdateListCommandBuilder(long id, bool activate ) : base(DbConstants.UpdateListProcedure)
        {
            _id = id;
            _activate = activate;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IdParameter, _id));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IsActiveParameter, _activate ? 1 : 0));
        }
    }
}
