﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, работающая с запросами из пакета PAC$TAX_PAYER_RESTRICTION 
    /// </summary>
    class SystemSettingsListsPackageCommand : ExecuteProcedureCommandBuilder
    {
        public SystemSettingsListsPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.SystemSettingsListsPackage, procedureName))
        {
        }
    }
}
