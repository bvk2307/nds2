﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая сохранять список
    /// </summary>
    class SaveListCommandBuilder : SystemSettingsListsPackageCommand
    {
        private readonly SystemSettingsList _list;

        /// <summary>
        /// Конструктор для команды построения ИВ
        /// </summary>
        public SaveListCommandBuilder(SystemSettingsList list ) : base(DbConstants.SaveSettingsProcedure)
        {
            _list = list;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.TypeParameter, _list.ListType));
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.NameParameter, _list.Name));
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.CreatedByParameter, _list.CreatedBy));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IsActiveParameter, _list.IsActive ? 1 : 0));
            parameters.With(FormatCommandHelper.ToOracleArray(
                _list.InnList == null || _list.InnList.Length == 0 ? new string[1] { string.Empty } : _list.InnList,
                DbConstants.InnListParameter));
        }
    }
}
