﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая загружать Парметры АСК НДС-2
    /// </summary>
    class LoadSettingsCommandBuilder : SystemSettingsPackageCommand
    {
        public LoadSettingsCommandBuilder() : base(DbConstants.LoadSettingsProcedure)
        {
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.Cursor(DbConstants.CursorParameter));
        }
    }
}
