﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder
{
    /// <summary>
    /// Команда, позволяющая загружать активные белые списки
    /// </summary>
    class ActiveWhiteListsCommandBuilder : SystemSettingsListsPackageCommand
    {
        public ActiveWhiteListsCommandBuilder() : base(DbConstants.ActiveWhiteListsProcedure)
        {
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.Cursor(DbConstants.CursorParameter));
        }
    }
}
