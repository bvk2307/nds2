﻿using System.Data;

namespace Luxoft.NDS2.Server.DAL
{
    internal interface IDataMapper<TData>
    {
        TData MapData(IDataRecord dataRecord);
    }
}
