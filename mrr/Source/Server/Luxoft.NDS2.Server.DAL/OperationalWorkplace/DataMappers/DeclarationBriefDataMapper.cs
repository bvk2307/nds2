﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Data;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.DataMappers
{
    internal class DeclarationBriefDataMapper : IDataMapper<DeclarationBrief>
    {
        public DeclarationBrief MapData(IDataRecord reader)
        {
            var obj = new DeclarationBrief();

            obj.ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.ID));
            obj.DECLARATION_VERSION_ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECLARATION_VERSION_ID));
            obj.SEOD_DECL_ID = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SEOD_DECL_ID));
            obj.RECORD_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.RECORD_MARK));
            obj.HAS_AT = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_AT));
            obj.SLIDE_AT_COUNT = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SLIDE_AT_COUNT)).GetValueOrDefault(0);
            obj.NO_TKS_COUNT = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NO_TKS_COUNT)).GetValueOrDefault(0);
            obj.HAS_CHANGES = GetHasChanges(reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_CHANGES)));

            obj.ProcessingStage = (DeclarationProcessignStage)reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.ProcessingStage));
            obj.LOAD_MARK = (DeclarationProcessignStage)reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.LOAD_MARK));
            obj.DECL_TYPE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_TYPE));
            obj.CANCELLED = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CANCELLED));
            obj.HAS_CANCELLED_CORRECTION = reader.ReadBoolean(TypeHelper<DeclarationBrief>.GetMemberName(t => t.HAS_CANCELLED_CORRECTION));
            obj.DECL_TYPE_CODE = reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_TYPE_CODE));
            obj.STATUS = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.STATUS));
            obj.STATUS_KNP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.STATUS_KNP));
            obj.SUR_CODE = reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUR_CODE));
            obj.INN = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INN));
            obj.INN_CONTRACTOR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INN_CONTRACTOR));
            obj.KPP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.KPP));
            obj.KPP_EFFECTIVE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.KPP_EFFECTIVE));
            obj.KPP_EFFECTIVE_CONTRACTOR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.KPP_EFFECTIVE_CONTRACTOR));
            obj.NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NAME));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.TAX_PERIOD));
            obj.PERIOD_EFFECTIVE = reader.ReadInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.PERIOD_EFFECTIVE));
            obj.FISCAL_YEAR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.FISCAL_YEAR));
            obj.FullTaxPeriod = new FullTaxPeriodInfo() { TaxPeriod = obj.TAX_PERIOD, FiscalYear = obj.FISCAL_YEAR, Description = obj.FULL_TAX_PERIOD };
            obj.DECL_DATE = reader.ReadDateTime(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_DATE));
            obj.SUBMISSION_DATE = reader.ReadDateTime(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUBMISSION_DATE));
            obj.CORRECTION_NUMBER = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CORRECTION_NUMBER));
            obj.CORRECTION_PROCESSED = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CORRECTION_PROCESSED));
            obj.SUBSCRIBER_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SUBSCRIBER_NAME));
            obj.PRPODP = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.PRPODP));
            obj.NDS_CHAPTER8 = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_CHAPTER8));
            obj.NDS_CHAPTER9 = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_CHAPTER9));
            obj.NDS_WEIGHT = reader.ReadNullableInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_WEIGHT));
            obj.NDS_AMOUNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.NDS_AMOUNT));
            obj.COMPENSATION_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.COMPENSATION_AMNT));
            obj.DECL_SIGN = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.DECL_SIGN));

            obj.CHAPTER8_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_MARK));
            obj.CHAPTER8_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_CONTRAGENT_CNT));
            obj.CHAPTER8_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_CNT));
            obj.CHAPTER8_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_CONTRAGENT_CNT));
            obj.CHAPTER8_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_GAP_AMNT));
            obj.CHAPTER8_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_CNT));
            obj.CHAPTER8_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER8_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER8_OTHER_AMNT));

            obj.CHAPTER9_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_MARK));
            obj.CHAPTER9_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_CONTRAGENT_CNT));
            obj.CHAPTER9_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_CNT));
            obj.CHAPTER9_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_CONTRAGENT_CNT));
            obj.CHAPTER9_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_GAP_AMNT));
            obj.CHAPTER9_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_CNT));
            obj.CHAPTER9_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER9_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER9_OTHER_AMNT));

            obj.CHAPTER10_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_MARK));
            obj.CHAPTER10_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_CONTRAGENT_CNT));
            obj.CHAPTER10_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_CNT));
            obj.CHAPTER10_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_CONTRAGENT_CNT));
            obj.CHAPTER10_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_GAP_AMNT));
            obj.CHAPTER10_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_CNT));
            obj.CHAPTER10_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER10_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER10_OTHER_AMNT));

            obj.CHAPTER11_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_MARK));
            obj.CHAPTER11_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_CONTRAGENT_CNT));
            obj.CHAPTER11_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_CNT));
            obj.CHAPTER11_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_CONTRAGENT_CNT));
            obj.CHAPTER11_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_GAP_AMNT));
            obj.CHAPTER11_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_CNT));
            obj.CHAPTER11_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER11_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER11_OTHER_AMNT));

            obj.CHAPTER12_MARK = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_MARK));
            obj.CHAPTER12_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_CONTRAGENT_CNT));
            obj.CHAPTER12_GAP_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_CNT));
            obj.CHAPTER12_GAP_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_CONTRAGENT_CNT));
            obj.CHAPTER12_GAP_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_GAP_AMNT));
            obj.CHAPTER12_OTHER_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_CNT));
            obj.CHAPTER12_OTHER_CONTRAGENT_CNT = reader.ReadInt64(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_CONTRAGENT_CNT));
            obj.CHAPTER12_OTHER_AMNT = reader.ReadNullableDecimal(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CHAPTER12_OTHER_AMNT));

            obj.INSPECTOR = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR));
            obj.INSPECTOR_SID = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID));
            if (obj.DECL_TYPE_CODE == 0 && reader.ReadNullableInt(TypeHelper<DeclarationBrief>.GetMemberName(t => t.CATEGORY)) == 1)
            {
                obj.CATEGORY = 1;
            }
            obj.SOUN_CODE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_CODE));
            obj.SOUN_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_NAME));
            obj.REGION_CODE = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.REGION_CODE));
            obj.REGION_NAME = reader.ReadString(TypeHelper<DeclarationBrief>.GetMemberName(t => t.REGION_NAME));
            
            return obj;
        }

        private DeclarationHasChangesType GetHasChanges(bool? hasChanges)
        {
            DeclarationHasChangesType ret = DeclarationHasChangesType.NoChanges;

            if (hasChanges.HasValue)
                ret = hasChanges.Value ? DeclarationHasChangesType.Changes : DeclarationHasChangesType.NoChanges;
            
            return ret;
        }

    }
}
