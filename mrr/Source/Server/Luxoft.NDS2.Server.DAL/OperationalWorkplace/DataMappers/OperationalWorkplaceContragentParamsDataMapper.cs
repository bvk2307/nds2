﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.DataMappers
{
    internal class OperationalWorkplaceContragentParamsDataMapper : IDataMapper<ContragentParamsSummary>
    {
        public ContragentParamsSummary MapData(IDataRecord reader)
        {
            var ret = new ContragentParamsSummary();
            
            ret.DECLARATION_VERSION_ID = reader.GetInt64("DECLARATION_VERSION_ID");
            ret.INN = reader.GetString("INN");

            ret.INN_DECLARANT = reader.GetString("INN_DECLARANT");
            ret.INN_REORGANIZED = reader.GetString("INN_REORGANIZED");
            ret.KPP_EFFECTIVE = reader.GetString("KPP_EFFECTIVE");
            ret.FISCAL_YEAR = reader.GetString("FISCAL_YEAR");
            ret.PERIOD_CODE = reader.GetString("PERIOD_CODE"); ;
            ret.DOC_TYPE_CODE = reader.GetInt32("DOC_TYPE_CODE");

            ret.CHAPTER = reader.GetInt32("CHAPTER");
            ret.MARK = reader.GetString("MARK");
            ret.DIS_STATUS = reader.GetNullableInt32("DIS_STATUS");
            int r;
            ret.SUR_CODE = Int32.TryParse(reader.GetString("SUR_CODE"), out r) ? r : 0;
            ret.DOC_TYPE = reader.GetString("DOC_TYPE");
            ret.DOC_TYPE_CODE = reader.GetInt32("DOC_TYPE_CODE");
            ret.CONTRACTOR_INN = reader.GetString("CONTRACTOR_INN");
            ret.CONTRACTOR_KPP = reader.GetString("CONTRACTOR_KPP");
            ret.CONTRACTOR_NAME = reader.GetString("CONTRACTOR_NAME");
            ret.INVOICE_NUM = reader.GetString("INVOICE_NUM");
            ret.INVOICE_DATE = reader.GetDateTime("INVOICE_DATE");
            ret.AMOUNT_TOTAL = reader.GetNullableDecimal("AMOUNT_TOTAL");
            ret.AMOUNT_NDS = reader.GetNullableDecimal("AMOUNT_NDS");
            ret.DIS_TYPE_NAME = reader.GetString("DIS_TYPE_NAME");
            ret.DIS_STATUS_NAME = reader.GetString("DIS_STATUS_NAME");
            ret.DIS_ID = reader.GetNullableInt64("DIS_ID");
            ret.DIS_TYPE = reader.GetNullableInt32("DIS_TYPE");
            ret.DIS_AMOUNT = reader.GetNullableDecimal("DIS_AMOUNT");

            return ret;
        }
    }
}
