﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.DataMappers
{
    internal class OperationalWorkplaceContragentDataMapper : IDataMapper<ContragentSummary>
    {
        public ContragentSummary MapData(IDataRecord reader)
        {
            var ret = new ContragentSummary();

            ret.CHAPTER8_COUNT = reader.GetInt32("CHAPTER8_COUNT");
            ret.CHAPTER9_COUNT = reader.GetInt32("CHAPTER9_COUNT");
            ret.CHAPTER10_COUNT = reader.GetInt32("CHAPTER10_COUNT");
            ret.CHAPTER11_COUNT = reader.GetInt32("CHAPTER11_COUNT");
            ret.CHAPTER12_COUNT = reader.GetInt32("CHAPTER12_COUNT");
            ret.MARK = reader.GetString("MARK");
            ret.DECLARATION_VERSION_ID = reader.GetInt64("DECLARATION_VERSION_ID");
            ret.INN_DECLARANT = reader.GetString("INN_DECLARANT");
            ret.INN_REORGANIZED = reader.GetString("INN_REORGANIZED");
            ret.KPP_EFFECTIVE = reader.GetString("KPP_EFFECTIVE");
            ret.PERIOD_CODE = reader.GetString("PERIOD_CODE");
            ret.FISCAL_YEAR = reader.GetString("FISCAL_YEAR");
            ret.DOC_TYPE = reader.GetString("DOC_TYPE");
            ret.DOC_TYPE_CODE = reader.GetString("DOC_TYPE_CODE");
            ret.SUR_CODE = reader.GetNullableInt32("SUR_CODE");
            ret.CONTRACTOR_INN = reader.GetString("CONTRACTOR_INN");
            ret.CONTRACTOR_KPP = reader.GetString("CONTRACTOR_KPP");
            ret.CONTRACTOR_NAME = reader.GetString("CONTRACTOR_NAME");
            ret.IS_IMPORT = reader.GetNullableBoolean("IS_IMPORT");
            ret.CHAPTER8_AMOUNT = reader.GetNullableDecimal("CHAPTER8_AMOUNT");
            ret.CHAPTER9_AMOUNT = reader.GetNullableDecimal("CHAPTER9_AMOUNT");
            ret.CHAPTER10_AMOUNT = reader.GetNullableDecimal("CHAPTER10_AMOUNT");
            ret.CHAPTER11_AMOUNT = reader.GetNullableDecimal("CHAPTER11_AMOUNT");
            ret.DECLARED_TAX_AMNT = reader.GetNullableDecimal("DECLARED_TAX_AMNT");
            ret.CALCULATED_TAX_AMNT_R9 = reader.GetNullableDecimal("CALCULATED_TAX_AMNT_R9");
            ret.CALCULATED_TAX_AMNT_R12 = reader.GetNullableDecimal("CALCULATED_TAX_AMNT_R12");
            ret.CHAPTER10_AMOUNT_NDS = reader.GetNullableDecimal("CHAPTER10_AMOUNT_NDS");
            ret.CHAPTER11_AMOUNT_NDS = reader.GetNullableDecimal("CHAPTER11_AMOUNT_NDS");
            ret.DECLARED_TAX_AMNT_TOTAL = reader.GetNullableDecimal("DECLARED_TAX_AMNT_TOTAL");
            ret.GAP_QUANTITY_8 = reader.GetNullableDecimal("GAP_QUANTITY_8");
            ret.GAP_QUANTITY_9 = reader.GetNullableDecimal("GAP_QUANTITY_9");
            ret.GAP_QUANTITY_10 = reader.GetNullableDecimal("GAP_QUANTITY_10");
            ret.GAP_QUANTITY_11 = reader.GetNullableDecimal("GAP_QUANTITY_11");
            ret.GAP_QUANTITY_12 = reader.GetNullableDecimal("GAP_QUANTITY_12");
            ret.GAP_AMOUNT_8 = reader.GetNullableDecimal("GAP_AMOUNT_8");
            ret.GAP_AMOUNT_9 = reader.GetNullableDecimal("GAP_AMOUNT_9");
            ret.GAP_AMOUNT_10 = reader.GetNullableDecimal("GAP_AMOUNT_10");
            ret.GAP_AMOUNT_11 = reader.GetNullableDecimal("GAP_AMOUNT_11");
            ret.GAP_AMOUNT_12 = reader.GetNullableDecimal("GAP_AMOUNT_12");
            ret.OTHER_QUANTITY_8 = reader.GetNullableDecimal("OTHER_QUANTITY_8");
            ret.OTHER_QUANTITY_9 = reader.GetNullableDecimal("OTHER_QUANTITY_9");
            ret.OTHER_QUANTITY_10 = reader.GetNullableDecimal("OTHER_QUANTITY_10");
            ret.OTHER_QUANTITY_11 = reader.GetNullableDecimal("OTHER_QUANTITY_11");
            ret.OTHER_QUANTITY_12 = reader.GetNullableDecimal("OTHER_QUANTITY_12");
            ret.OTHER_AMOUNT_8 = reader.GetNullableDecimal("OTHER_AMOUNT_8");
            ret.OTHER_AMOUNT_9 = reader.GetNullableDecimal("OTHER_AMOUNT_9");
            ret.OTHER_AMOUNT_10 = reader.GetNullableDecimal("OTHER_AMOUNT_10");
            ret.OTHER_AMOUNT_11 = reader.GetNullableDecimal("OTHER_AMOUNT_11");
            ret.OTHER_AMOUNT_12 = reader.GetNullableDecimal("OTHER_AMOUNT_12");

            return ret;
        }
    }
}
