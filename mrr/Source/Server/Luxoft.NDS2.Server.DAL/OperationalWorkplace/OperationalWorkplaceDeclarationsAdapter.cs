﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace.DataMappers;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public class OperationalWorkplaceDeclarationsAdapter : IOperationalWorkplaceDeclarationsAdapter
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connectionFactory;

        public OperationalWorkplaceDeclarationsAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<DeclarationBrief> Search(FilterExpressionGroup searchCriteria, List<ColumnSort> sorting, int rowsToSkip, int? rowsToTake)
        {
            sorting.Add(new ColumnSort() { ColumnKey = "ID", Order = ColumnSort.SortOrder.Asc });
            return new ExplicitListCommandExecuter<DeclarationBrief>(
                new DeclarationBriefDataMapper(),
                _service,
                _connectionFactory,
                rowsToSkip).TryExecute(
                    new OperationalWorplaceDeclarationSearchBuilder(
                        searchCriteria,
                        sorting,
                        _service,
                        rowsToTake)).ToList();
        }

        public int Count(FilterExpressionGroup searchCriteria)
        {
            return new CountCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new OperationalWorplaceDeclarationCountBuilder(searchCriteria, _service));
        }
    }
}
