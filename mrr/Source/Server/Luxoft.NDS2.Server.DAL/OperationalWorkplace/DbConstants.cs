﻿
namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public static class DbConstants
    {
        public static readonly string OperationalWorkplaceContractorList = "V$INSPECTOR_CONTRAGENT";
        public static readonly string OperationalWorkplaceContractorParams = "V$INSPECTOR_CONTRAGENT_PARAMS";
    }
}
