﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders
{
    internal class OperationalWorkplaceContractorParamsSearchBuilder : PageSearchQueryCommandBuilder
    {
        public static string Alias = "vw";

        public OperationalWorkplaceContractorParamsSearchBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryTemplateProvider queryTemplate)
            : base(
                string.Format("{0} {1}", DbConstants.OperationalWorkplaceContractorParams, Alias).SelectPattern(),
                filterBy,
                orderBy,
                new PatternProvider(Alias))
        {
        }
    }
}
