﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders
{
    internal class OperationalWorplaceDeclarationCountBuilder : GetCountTemplateCommandBuilder
    {
        public OperationalWorplaceDeclarationCountBuilder(
            FilterExpressionBase filterBy,
            IQueryTemplateProvider queryTemplate)
            : base(
                queryTemplate.GetQueryText("DeclarationBrief", "COUNT_PATTERN_FOR_NEW_ENGINE"),
                filterBy,
                new PatternProvider("vw"))
        {
        }
    }
}
