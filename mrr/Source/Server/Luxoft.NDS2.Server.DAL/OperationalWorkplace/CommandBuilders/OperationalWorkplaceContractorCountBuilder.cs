﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders
{
    internal class OperationalWorkplaceContractorCountBuilder : GetCountCommandBuilder
    {
        public static string Alias = "vw";

        public OperationalWorkplaceContractorCountBuilder(
            FilterExpressionBase filterBy,
            IQueryTemplateProvider queryTemplate)
            : base(
                string.Format("{0} {1}", DbConstants.OperationalWorkplaceContractorList, Alias).CountPattern(),
                filterBy,
                new PatternProvider(Alias))
        {
        }
    }
}
