﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders
{
    internal class OperationalWorplaceDeclarationSearchBuilder : SearchQueryCommandBuilder
    {
        private readonly int? _rowsToTake;

        public OperationalWorplaceDeclarationSearchBuilder(FilterExpressionGroup searchCriteria, 
                                                            List<ColumnSort> sorting,
                                                            IQueryTemplateProvider queryTemplate,
                                                            int? rowsToTake)
            : base(
                queryTemplate.GetQueryText("DeclarationBrief", "QUERY_PATTERN_FOR_NEW_ENGINE"),
                searchCriteria,
                sorting,
                new PatternProvider("vw"))

        {
            _rowsToTake = rowsToTake;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add("rnNext", _rowsToTake);

            return result;
        }


    }
}
