﻿
namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public static class OperationalWorkplaceAdaptersCreator
    {
        public static IOperationalWorkplaceDeclarationsAdapter InspectorWorkplaceDeclarationsAdapter(
            this IServiceProvider service, 
            ConnectionFactoryBase connectionFactory)
        {
            return new OperationalWorkplaceDeclarationsAdapter(service, connectionFactory);
        }
    }
}
