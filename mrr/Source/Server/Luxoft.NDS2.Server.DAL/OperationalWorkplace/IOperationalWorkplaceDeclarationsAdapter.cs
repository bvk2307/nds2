using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public interface IOperationalWorkplaceDeclarationsAdapter
    {
        List<DeclarationBrief> Search(FilterExpressionGroup searchCriteria, List<ColumnSort> order, int page, int? pageSeek);

        int Count(FilterExpressionGroup searchCriteria);
    }
}