﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public static class OperationalWorkplaceContractorAdapterCreator
    {
        public static ISearchAdapter<ContragentSummary> OperationalWorkplaceContractorAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new OperationalWorkplaceContractorAdapter(service, connectionFactory);
        }

        public static ISearchAdapter<ContragentParamsSummary> OperationalWorkplaceContractorParamsAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new OperationalWorkplaceContractorParamsAdapter(service, connectionFactory);
        }
    }
}
