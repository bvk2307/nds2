﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace.CommandBuilders;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace.DataMappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.OperationalWorkplace
{
    public class OperationalWorkplaceContractorParamsAdapter : ISearchAdapter<ContragentParamsSummary>
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public OperationalWorkplaceContractorParamsAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Выполняет поиск данных о контрагенте, удовлетворяющих заданным критериям
        /// </summary>
        /// <param name="searchCriteria">Критерии отбора контрагентов</param>
        /// <param name="orderBy">параметры сортировки</param>
        /// <param name="rowsToSkip">кол-во пропущенных строк</param>
        /// <param name="rowsToTake">кол-во необходимых строк</param>
        /// <returns>список контрагентов</returns>
        public IEnumerable<ContragentParamsSummary> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            uint rowsToSkip,
            uint rowsToTake)
        {
            return new ListCommandExecuter<ContragentParamsSummary>(
                new OperationalWorkplaceContragentParamsDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new OperationalWorkplaceContractorParamsSearchBuilder(searchCriteria, orderBy, _service)
                    .Skip((int)rowsToSkip)
                    .Take((int)rowsToTake)).ToList();
        }

        // Не предполагается к использованию
        public int Count(FilterExpressionBase searchCriteria)
        {
            throw new NotImplementedException();
        }
    }
}

