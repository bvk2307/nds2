﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.TaxPayerDetails.CommandBuilders
{
    class GetTaxPayerStatusCommand : ExecuteProcedureCommandBuilder
    {
        private const string PackageName = "PAC$USER_TASK";
        private const string ProcedureName = "P$GET_TAX_PAYER_STATUS";

        private readonly string _inn;
        private readonly string _kppEffective;

        private const string Cursor = "p_cursor";
        private const string Inn = "p_inn";
        private const string KppEffective = "p_kpp_effective";

        public GetTaxPayerStatusCommand(string inn, string kppEffective)
            : base(string.Format("{0}.{1}", PackageName, ProcedureName))
        {
            _inn = inn;
            _kppEffective = kppEffective;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(KppEffective, _kppEffective));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }

    }

}

