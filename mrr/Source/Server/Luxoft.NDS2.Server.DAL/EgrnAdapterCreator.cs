﻿using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.Egrn;

namespace Luxoft.NDS2.Server.DAL
{
    public static class EgrnAdapterCreator
    {
        public static IULTableAdapter EgrnULAdapter(this IServiceProvider service)
        {
            return new ULTableAdapter(service);
        }

        public static IIPTableAdapter EgrnIPAdapter(this IServiceProvider service)
        {
            return new IPTableAdapater(service);
        }
    }
}
