﻿using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL
{
    public abstract class ConnectionFactoryBase : IDisposable
    {
        protected ConnectionFactoryBase(IServiceProvider configurationProvider)
        {
            ConfigurationProvider = configurationProvider;
        }

        protected IServiceProvider ConfigurationProvider
        {
            get;
            private set;
        }

        public virtual void Dispose()
        {
            DisposeDependencies();
            GC.SuppressFinalize(this);
        }

        internal OracleConnection CreateOracleConnection()
        {
            return ChangeGlobalizationSettings(CreateOracleConnectionInternal());
        }

        protected abstract OracleConnection CreateOracleConnectionInternal();

        protected abstract void DisposeDependencies();

        protected OracleConnection ChangeGlobalizationSettings(OracleConnection connection)
        {
            return connection;
        }
    }
}
