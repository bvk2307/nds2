﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает функционал адптера доступа к данным отчета "Пирамида"
    /// </summary>
    public interface IPyramidDataAdapter
    {
        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        IReadOnlyCollection<GraphData> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        IReadOnlyCollection<GraphData> LoadReportGraphNodes(GraphNodesKeyParameters nodesKeyParameters);

        /// <summary>
        /// Запрашивает данные для узла дерева
        /// </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП на один уровень вниз без родителя</returns>
        PageResult<GraphData> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        IReadOnlyCollection<DeductionDetail> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters );

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        IReadOnlyCollection<string> LoadTaxpayersKpp(string inn);
    }
}
