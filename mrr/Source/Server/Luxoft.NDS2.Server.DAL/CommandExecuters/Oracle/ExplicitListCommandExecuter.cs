﻿using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    internal class ExplicitListCommandExecuter<TData> : ListCommandExecuter<TData>
        where TData : new()
    {
        private readonly int _rowsToSkip;
        
        [System.Obsolete]
        public ExplicitListCommandExecuter(
            IDataMapper<TData> mapper, 
            IServiceProvider provider, 
            ConnectionFactoryBase connection, 
            int rowsToSkip)
            : this(mapper, provider, rowsToSkip)
        {
        }

        public ExplicitListCommandExecuter(
            IDataMapper<TData> mapper, 
            IServiceProvider provider, 
            int rowsToSkip)
            : base(mapper, provider)
        {
            _rowsToSkip = rowsToSkip;
        }

        protected override IEnumerable<TData> Execute(OracleCommand command)
        {
            OracleDataReader reader = command.ExecuteReader();

            if (_rowsToSkip > 0)
            {
                int sc = 0;
                bool next = false;

                do
                {
                    next = reader.Read();
                    sc++;
                } while (next && sc < _rowsToSkip);
            }

            return base.Execute(reader);
        }
    }
}
