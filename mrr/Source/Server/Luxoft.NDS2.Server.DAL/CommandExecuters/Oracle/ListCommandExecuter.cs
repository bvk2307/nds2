﻿using System;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    internal class ListCommandExecuter<TData> : CommandExecuterBase<IEnumerable<TData>>
    {
        private readonly IDataMapper<TData> _mapper;

        [Obsolete]
        public ListCommandExecuter(IDataMapper<TData> mapper, IServiceProvider provider, ConnectionFactoryBase connection)
            : this(mapper, provider)
        {           
        }

        public ListCommandExecuter(IDataMapper<TData> mapper, IServiceProvider provider)
            : base(provider, provider)
        {
            _mapper = mapper;
        }

        protected override IEnumerable<TData> Execute(OracleCommand command)
        {
            return Execute(command.ExecuteReader());
        }

        protected IEnumerable<TData> Execute(OracleDataReader reader)
        {
            return new DbDataFetcher<TData>(reader, _mapper).ToList();
        }
    }
}
