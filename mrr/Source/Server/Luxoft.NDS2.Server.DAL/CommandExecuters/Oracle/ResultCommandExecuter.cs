﻿using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    internal class ResultCommandExecuter : CommandExecuterBase<Dictionary<string, object>>
    {
        [System.Obsolete]
        public ResultCommandExecuter(IServiceProvider provider, ConnectionFactoryBase connection)
            : this(provider)
        {
        }

        public ResultCommandExecuter(IServiceProvider provider)
            : base(provider, provider)
        {
        }

        protected override Dictionary<string, object> Execute(OracleCommand command)
        {
            command.ExecuteNonQuery();

            var result = new Dictionary<string, object>();

            foreach (OracleParameter parameter in command.Parameters)
            {
                if (parameter.Direction == ParameterDirection.Output
                    || parameter.Direction == ParameterDirection.InputOutput
                    || parameter.Direction == ParameterDirection.ReturnValue)
                {
                    result.Add(parameter.ParameterName, parameter.Value);
                }
            }

            return result;
        }
    }
}
