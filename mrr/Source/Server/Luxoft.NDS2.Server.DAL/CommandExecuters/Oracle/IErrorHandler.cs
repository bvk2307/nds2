﻿using System.Data.Common;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    public interface IErrorHandler
    {
        DatabaseException Handle(DbException oraError);
    }
}
