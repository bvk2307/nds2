﻿using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    internal class ListCommandFetcher<TData> : CommandExecuterBase<long>
    {
        private readonly Func<TData, bool> _processAction;
        private readonly IDataMapper<TData> _mapper;

        public ListCommandFetcher(IDataMapper<TData> mapper, IServiceProvider provider, Func<TData, bool> processAction)
            : base(provider, provider)
        {
            _processAction = processAction;
            _mapper = mapper;
        }

        protected override long Execute(OracleCommand command)
        {
            var cnt = 0;

            var fetcher = new DbDataFetcher<TData>(command.ExecuteReader(), _mapper);

            foreach (var record in fetcher)
            {
                ++cnt;
                if (!_processAction(record))
                    return cnt;
            }

            return cnt;
        }

    }

}
