﻿using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle
{
    [Obsolete("Используем ResultCommandExecuter")]
    internal class CountCommandExecuter : CommandExecuterBase<int>
    {
        private readonly string ResultParameterName = "pResult";

        [System.Obsolete]
        public CountCommandExecuter(IServiceProvider provider, ConnectionFactoryBase connection)
            : this(provider)
        {
        }

        public CountCommandExecuter(IServiceProvider provider)
            : base(provider, provider)
        {
        }


        protected override int Execute(OracleCommand command)
        {
            command.ExecuteNonQuery();
            return Convert.ToInt32(command.Parameters[ResultParameterName].Value.ToString());
        }
    }
}
