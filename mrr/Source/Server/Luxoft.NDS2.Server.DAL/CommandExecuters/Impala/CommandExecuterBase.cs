﻿using System.Data;
using System.Data.Common;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL.CommandBuilders;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Providers.Odbc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Luxoft.NDS2.Server.Common.Managers;
using Luxoft.NDS2.Server.DAL.CommandBuilders.ImpalaOdbc;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Impala
{
    internal abstract class CommandExecuterBase<TResult>
    {
        private readonly ILogProvider _logger;

        private IErrorHandler _errorHandler;

        public CommandExecuterBase(ILogProvider logger)
        {
            _logger = logger;
            _errorHandler = new DefaultErrorHandler();
        }

        public CommandExecuterBase<TResult> WithErrorHandler(IErrorHandler errorHandler)
        {
            if (errorHandler == null)
            {
                throw new ArgumentNullException("errorHandler");
            }

            _errorHandler = errorHandler;

            return this;
        }

//        private string GetQueryText(DbCommand cmd)
//        {
//            string cmdText = cmd.CommandText;
//            
//            for (int i = cmd.Parameters.Count-1; i >= 0; i--)
//            {
//                cmdText = cmdText.Replace("@"+cmd.Parameters[i].ParameterName, cmd.Parameters[i].Value.ToString());
//            }
//
//            return cmdText;
//        }

        public TResult TryExecute(IDbCommandBuilder commandBuilder, Action<string, long, string> debugCallback = null)
        {
            var traceInfo = new List<KeyValuePair<string, object>>();
            var watcher = new Stopwatch();
            IDbConnectionProvider provider = ConnectionPool.Current;
            TResult retVal = default(TResult);
            IDbConnection conn = null;
            string sourceHost = null;
            try
            {
                if (!provider.TryGetConnection(out conn))
                {
                    throw new ApplicationException("Все соединения с источником данных заняты.");
                }
                
                var command = commandBuilder.BuildCommand(conn);

                traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT,
                    command.CommandText));

                foreach (DbParameter oracleParameter in command.Parameters)
                {
                    traceInfo.Add(new KeyValuePair<string, object>(oracleParameter.ParameterName, oracleParameter.Value));
                }

                sourceHost = command.Connection.ConnectionString;

                watcher.Start();
                retVal = Execute(command);
                watcher.Stop();

                if (debugCallback != null)
                    debugCallback(command.Connection.ConnectionString, watcher.ElapsedMilliseconds, string.Empty);

                traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME,
                    watcher.ElapsedMilliseconds));

                _logger.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
            }
            catch (DbException dbEx)
            {
                _logger.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, dbEx, traceInfo);

                if (debugCallback != null)
                    debugCallback(sourceHost, watcher.ElapsedMilliseconds, dbEx.ToString());

                throw _errorHandler.Handle(dbEx);
            }
            catch (Exception ex)
            {
                _logger.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, ex, traceInfo);
                if (debugCallback != null)
                    debugCallback(sourceHost, watcher.ElapsedMilliseconds, ex.ToString());

                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return retVal;
        }

        protected abstract TResult Execute(IDbCommand command);
    }
}
