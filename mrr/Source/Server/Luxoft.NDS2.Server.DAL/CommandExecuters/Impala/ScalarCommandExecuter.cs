﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Impala
{

    internal class ScalarCommandExecuter : CommandExecuterBase<Dictionary<String, Object>>
    {
        public ScalarCommandExecuter(ILogProvider logger) : base(logger)
        {
        }

        protected override Dictionary<String, Object> Execute(IDbCommand command)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    for (int fieldIdx = 0; fieldIdx <= reader.FieldCount - 1; fieldIdx++)
                    {
                        result.Add(reader.GetName(fieldIdx), reader[fieldIdx]);
                    }
                    
                    return result;
                }
            }

            return result;
        }
    }
}
