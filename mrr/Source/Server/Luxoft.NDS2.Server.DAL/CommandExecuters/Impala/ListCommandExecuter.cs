﻿using System.Data;
using System.Data.Common;
using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data.Odbc;

namespace Luxoft.NDS2.Server.DAL.CommandExecuters.Impala
{
    internal class ListCommandExecuterPageOptions
    {
        public ListCommandExecuterPageOptions(int skip, int take)
        {
            SkipRows = skip;
            TakeRows = take;
        }

        public int SkipRows { get; private set; }

        public int TakeRows { get; private set; }
    }

    internal class ListCommandExecuter<TData> : CommandExecuterBase<IEnumerable<TData>>
        where TData : new()
    {
        private readonly IDataMapper<TData> _mapper;

        private ListCommandExecuterPageOptions _pageOptions;

        public ListCommandExecuter(
            IDataMapper<TData> mapper,
            ILogProvider logger, 
            ListCommandExecuterPageOptions pageOptions = null)
            : base(logger)
        {
            _mapper = mapper;
            _pageOptions = pageOptions;
        }

        protected override IEnumerable<TData> Execute(IDbCommand command)
        {
            List<TData> result = new List<TData>();

            using (var reader = command.ExecuteReader() as OdbcDataReader)
            {
                while (reader.Read())
                {
                    result.Add(_mapper.MapData(reader));
                }
            }

            return result;
        }
    }
}
