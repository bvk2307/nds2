﻿using System.Data.Common;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IErrorHandler
    {
        DatabaseException Handle(DbException oraError);
    }
}
