﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IConnectionFactoryCreator
    {
        ConnectionFactoryBase CreateSingleConnectionFactory(IServiceProvider _serviceHelpers);
    }
}
