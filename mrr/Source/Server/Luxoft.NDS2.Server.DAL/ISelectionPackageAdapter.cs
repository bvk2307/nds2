﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ISelectionPackageAdapter
    {

        /// <summary>
        /// Синхронизирует статус выборки со статусом обработки запроса расхождений
        /// </summary>
        /// <returns>Статус выборки после синхронизации</returns>
        List<SelectionRequest> SyncWithRequestProcessing(long id, SelectionType type);
        /// <summary>
        /// Загружает конкретную выборку
        /// </summary>
        /// <returns>Выборка</returns>
        Selection GetSelection(long id);

        /// <summary>
        /// Устанавливает статус выборки
        /// </summary>
        /// <param name="id"></param>
        /// <param name="stateId"></param>
        /// <param name="userName"></param>
        void SetSelectionState(long id, SelectionType type, int stateId, string userName, ActionType action);

        /// <summary>
        /// Устанавливает статус выборки
        /// </summary>
        /// <param name="id"></param>
        /// <param name="stateId"></param>
        /// <param name="userName"></param>
        void UpdateSelectionStateAfterHiveLoad(long id, SelectionType type, string userName, ActionType action);

        /// <summary>
        /// Сохранение выборки
        /// </summary>
        /// <param name="data"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        Selection SaveSelection(Selection data, ActionType actionType);

        /// <summary>
        /// Устанавливает статус Согласована
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userSid"></param>
        void Approve(long id, string userSid);

        /// <summary>
        /// Устанавливает статус Черновик при действии возврат на доработку
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userSid"></param>
        void SendForCorrection(long id, string userSid);

        /// <summary>
        /// Генерация имени выборки
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <param name="type">Тип выборки(ручная или шаблон)</param>
        /// <returns>Имя выборки</returns>
        string GenerateUniqueName(string userSid, SelectionType type);

        /// <summary>
        /// Проверка уникальности имени выборки
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <param name="selectionName">Имя выборки</param>
        /// <param name="selectionId">ID выборки</param>
        /// <param name="type">Тип выборки(ручная или шаблон)</param>
        /// <returns>Новое имя выборки</returns>
        string VerifyUniqueName(string userSid, string selectionName, long selectionId, SelectionType type);

        /// <summary>
        /// Список записий истории
        /// </summary>
        /// <param name="selectionId">ID выборки</param>
        /// <returns></returns>
        List<ActionHistory> GetActionsHistory(long selectionId);

        /// <summary>
        /// Получение объекта перехода (?)
        /// </summary>
        /// <returns></returns>
        List<SelectionTransition> GetStateTransitions();

        long CreateRequest(long selectionId, string query);

        Selection UpdateSelectionAggregateData(Selection selection);

        void UpdateSelectionAggregate(long selectionId, SelectionType type);

        /// <summary>
        /// Обноялвяет поля Регионы в выборке, если оно изменилось после применеия фильтра
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        void UpdateSelectionRegions(long selectionId, string regions);

        /// <summary>
        /// Возвращает True - если все выборки в шаблоне в статусе не Загружается, Формирование АТ и Сформировано АТ
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        bool CanDeleteTemplate(long templateId);

        /// <summary>
        /// Возвращает информацию по расхождениям в выборке
        /// Общее количество расхождений и к-во расхождений включенных в выборку
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        List<SelectionDiscrepancyStatistic> GetDiscrepancyStatistic(long selectionId, SelectionType type);

    }
}
