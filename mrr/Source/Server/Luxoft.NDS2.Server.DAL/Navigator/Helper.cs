﻿using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Navigator
{
    public static class Helper
    {
        public static int ToParam(this Period period)
        {
            return period.Year * 100 + period.Code;
        }

        private static string InnSQLPattern = ":pInn{0}";

        private static string InnParamPattern = "pInn{0}";

        public static string ToSQLExpression(this string[] innList)
        {
            var result = new StringBuilder();

            for (var index = 0; index < innList.Count(); index++)
            {
                result.AppendFormat(
                    index == 0 ? InnSQLPattern : "," + InnSQLPattern, 
                    index + 1);
            }

            return result.ToString();
        }

        public static IEnumerable<OracleParameter> ToOracleParameters(this string[] innList)
        {
            var result = new List<OracleParameter>();

            for (var index = 0; index < innList.Count(); index++)
            {
                result.Add(
                    new OracleParameter(
                        string.Format(InnParamPattern, index + 1), 
                        innList[index]));
            }

            return result;
        }
    }
}
