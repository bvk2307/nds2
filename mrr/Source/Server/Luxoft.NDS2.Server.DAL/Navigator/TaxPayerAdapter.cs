﻿using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Navigator
{
    internal class TaxPayerAdapter : BaseOracleTableAdapter, ITaxPayerAdapter
    {
        private const string LoadTaxPayersFull = "LoadTaxPayersFull";

        private const string LoadTaxPayersShort = "LoadTaxPayersShort";

        private const string CountTaxPayersFull = "CountTaxPayersFull";

        private const string CountTaxPayersShort = "CountTaxPayersShort";

        private const string HasPurchaseDeclarationsQuery = "HasPurchaseDeclarations";

        private const string ViewPrefix = "vw";

        private const string ParamPeriodFrom = "pFrom";

        private const string ParamPeriodTo = "pTo";

        private const string ParamInn = "pInn";

        public TaxPayerAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public List<TaxPayer> All(QueryConditions criteria, PeriodRange range = null)
        {
            var query = criteria.ToSQL(
                _service.GetQueryText(Constants.Scope, range == null ? LoadTaxPayersShort : LoadTaxPayersFull),
                ViewPrefix,
                false,
                true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();

            if (range != null)
            {
                parameters.Add(
                    new OracleParameter(ParamPeriodFrom, range.From.ToParam()));
                parameters.Add(
                    new OracleParameter(ParamPeriodTo, range.To.ToParam()));
            }
            parameters.Add(
                FormatCommandHelper.Cursor(Constants.ParamCursor));

            return ExecuteList(
                string.Format(Constants.PageQueryPattern,query.Text),
                CommandType.Text,
                parameters.ToArray(),
                Build);
        }

        public List<TaxPayer> Page(QueryConditions criteria, PeriodRange range = null)
        {
            var query = criteria.ToSQL(
                _service.GetQueryText(Constants.Scope, range == null ? LoadTaxPayersShort : LoadTaxPayersFull),
                ViewPrefix,
                true,
                true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();

            if (range != null)
            {
                parameters.Add(
                    new OracleParameter(ParamPeriodFrom, range.From.ToParam()));
                parameters.Add(
                    new OracleParameter(ParamPeriodTo, range.To.ToParam()));
            }

            parameters.Add(
                FormatCommandHelper.Cursor(Constants.ParamCursor));

            return ExecuteList(
                string.Format(Constants.PageQueryPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                Build);
        }

        public int Count(QueryConditions criteria, PeriodRange range = null)
        {
            var query = criteria.ToSQL(
                _service.GetQueryText(Constants.Scope, range == null ? CountTaxPayersShort : CountTaxPayersFull),
                ViewPrefix,
                false,
                true);

            query.Parameters.Add(new QueryParameter(false) { Name = Constants.ParamResult });

            return DataReaderExtension.ReadInt(Execute(query).Output[Constants.ParamResult]);
        }

        public IEnumerable<TaxPayerPurchaseStatus> PurchaseStatus(
            string[] innList, 
            PeriodRange range)
        {
            var query =
                string.Format(
                    Constants.PageQueryPattern,
                    string.Format(
                        _service.GetQueryText(Constants.Scope, HasPurchaseDeclarationsQuery), 
                        innList.ToSQLExpression()));

            var parameters = new List<OracleParameter>();
            parameters.AddRange(innList.ToOracleParameters());
            parameters.Add(FormatCommandHelper.Cursor(Constants.ParamCursor));
            parameters.Add(FormatCommandHelper.NumericIn(ParamPeriodFrom, range.From.ToParam()));
            parameters.Add(FormatCommandHelper.NumericIn(ParamPeriodTo, range.To.ToParam()));

            return ExecuteList(
                query,
                CommandType.Text,
                parameters.ToArray(),
                TaxPayerPurchaseStatus);
        }

        public int CountPurchase(string inn, PeriodRange range)
        {
            var result =
                Execute(
                    _service.GetQueryText(Constants.Scope, HasPurchaseDeclarationsQuery),
                    CommandType.Text,
                    new[]
                    {
                        FormatCommandHelper.VarcharIn(ParamInn, inn),
                        FormatCommandHelper.NumericIn(ParamPeriodFrom, range.From.ToParam()),
                        FormatCommandHelper.NumericIn(ParamPeriodTo, range.To.ToParam()),
                        FormatCommandHelper.NumericRet(Constants.ParamResult)
                    });

            return int.Parse(result.Output[Constants.ParamResult].ToString());
        }

        private TaxPayer Build(OracleDataReader reader)
        {
            return new TaxPayer
            {
                Inn = reader.ReadString("inn"),
                Inspection = reader.ReadString("inspection"),
                Kpp = reader.ReadString("kpp"),
                Name = reader.ReadString("name"),
                Region = reader.ReadString("region"),
                FederalDistrict = reader.ReadString("federaldistrict"),
                SurCode = reader.ReadInt("surcode"),
                SalesAmount = reader.ReadNullableDecimal("salesamount"),
                PurchaseAmount = reader.ReadNullableDecimal("purchaseamount"),
                NdsCalculated = reader.ReadNullableDecimal("ndscalculated"),
                NdsDeduction = reader.ReadNullableDecimal("ndsdeduction"),
                RegionCode = reader.ReadString("regioncode"),
                InspectionCode = reader.ReadString("inspectioncode")
            };
        }

        private TaxPayerPurchaseStatus TaxPayerPurchaseStatus(OracleDataReader reader)
        {
            return new TaxPayerPurchaseStatus
            {
                Inn = reader.ReadString("innnp"),
                NdsCompensationQuantity = reader.ReadInt("decl_quantity")
            };
        }
    }
}
