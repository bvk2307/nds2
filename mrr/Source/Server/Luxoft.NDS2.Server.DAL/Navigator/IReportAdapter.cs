﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Navigator
{
    public interface IReportAdapter
    {
        RequestStatus GetStatus(long requestId);

        List<ChainSummary> GetPairs(long requestId);

        List<ChainContractorData> GetData(long? pairId, long? chainId, long requestId);
    }
}
