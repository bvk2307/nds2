﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Navigator
{
    internal class ReportAdapter : BaseOracleTableAdapter, IReportAdapter
    {
        private const string PackageName = "PAC$NAVIGATOR";

        private const string ParamRequestId = "pRequestId";

        private const string ParamCursor = "pResult";

        private const string ParamStatus = "pStatus";

        public ReportAdapter(IServiceProvider provider)
            : base(provider)
        {
        }

        public RequestStatus GetStatus(long requestId)
        {
 	       var result = Execute(
                FormatCommandHelper.PackageProcedure(PackageName, "P$GET_STATUS"),
                CommandType.StoredProcedure,
                new []
                {
                    FormatCommandHelper.NumericIn(ParamRequestId, requestId),
                    FormatCommandHelper.NumericOut(ParamStatus)
                });

            return (RequestStatus)int.Parse(result.Output[ParamStatus].ToString());
        }

        public List<ChainSummary> GetPairs(long requestId)
        {
            return ExecuteList(
                FormatCommandHelper.PackageProcedure(PackageName, "P$GET_PAIRS"),
                CommandType.StoredProcedure,
                new [] 
                {
                    FormatCommandHelper.NumericIn(ParamRequestId, requestId),
                    FormatCommandHelper.Cursor(ParamCursor)
                },
                ToChainSummary);
        }

        public List<ChainContractorData> GetData(long? pairId, long? chainId, long requestId)
        {
            return ExecuteList(
                FormatCommandHelper.PackageProcedure(PackageName, "P$GET_DATA"),
                CommandType.StoredProcedure,
                new[]
                {
                    new OracleParameter("pPairId", pairId.HasValue ? (object)pairId.Value : DBNull.Value),
                    new OracleParameter("pChainId", chainId.HasValue ? (object)chainId.Value : DBNull.Value),
                    new OracleParameter("pRequestId", requestId),
                    FormatCommandHelper.Cursor(ParamCursor)
                },
                ToChainContractor);
        }

        private ChainSummary ToChainSummary(OracleDataReader reader)
        {
            return new ChainSummary
            {
                PairId = reader.ReadInt64("pair_id"),
                ChainId = reader.ReadInt64("chain_id"),
                Number = reader.ReadInt("chain_number"),
                HeadTaxPayerInn = reader.ReadString("head_taxpayer_inn"),
                HeadTaxPayerName = reader.ReadString("head_taxpayer_name"),
                TailTaxPayerInn = reader.ReadString("tail_taxpayer_inn"),
                TailTaxPayerName = reader.ReadString("tail_taxpayer_name"),
                TargetTaxPayersQuantity = reader.ReadInt("chain_targets_quantity"),
                LinksQuantity = reader.ReadInt("chain_links")
            };
        }

        private ChainContractorData ToChainContractor(OracleDataReader reader)
        {
            return new ChainContractorData
            {
                BuyerAmountAsBuyer = reader.ReadDecimal("amount_per_buyer_as_buyer"),
                BuyerAmountAsSeller = reader.ReadDecimal("amount_per_buyer_as_seller"),
                ChainId = reader.ReadInt64("chain_id"),
                ChainMaxMappedAmount = reader.ReadNullableDecimal("max_mapped_amount"),
                ChainMaxNotMappedAmount = reader.ReadNullableDecimal("max_not_mapped_amount"),
                ChainMinMappedAmount = reader.ReadNullableDecimal("min_mapped_amount"),
                ChainMinNotMappedAmount = reader.ReadNullableDecimal("min_not_mapped_amount"),
                ChainNumber = reader.ReadInt("chain_number"),
                Head = reader.ReadBoolean("head"),
                Id = reader.ReadInt64("id"),
                Index = reader.ReadInt("order_number"),
                Inn = reader.ReadString("inn"),
                Kpp = reader.ReadString("kpp"),
                MappedAmountAsBuyer = reader.ReadDecimal("mapped_amount_as_buyer"),
                MappedAmountAsSeller = reader.ReadDecimal("mapped_amount_as_seller"),
                Name = reader.ReadString("name"),
                NdsCalculated = reader.ReadNullableDecimal("nds_calculated"),
                NdsDeduction = reader.ReadNullableDecimal("nds_deduction"),
                NdsPurchase = reader.ReadNullableDecimal("nds_purchase"),
                NdsSales = reader.ReadNullableDecimal("nds_sales"),
                SellerAmountAsBuyer = reader.ReadDecimal("amount_per_seller_as_buyer"),
                SellerAmountAsSeller = reader.ReadDecimal("amount_per_seller_as_seller"),
                SurCode = reader.ReadInt("sur_code"),
                Tail = reader.ReadBoolean("tail")
            };
        }
    }
}
