﻿namespace Luxoft.NDS2.Server.DAL.Navigator
{
    internal static class Constants
    {
        public static string Scope = "navigator";

        public static string PageQueryPattern = "begin open :pCursor for {0}; end;";

        public static string ParamResult = "pResult";

        public static string ParamCursor = "pCursor";
    }
}
