﻿using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Navigator
{
    public interface ITaxPayerAdapter
    {
        List<TaxPayer> All(QueryConditions criteria, PeriodRange range = null);

        List<TaxPayer> Page(QueryConditions criteria, PeriodRange range = null);

        int Count(QueryConditions criteria, PeriodRange range = null);

        IEnumerable<TaxPayerPurchaseStatus> PurchaseStatus(string[] innList, PeriodRange range);
    }
}
