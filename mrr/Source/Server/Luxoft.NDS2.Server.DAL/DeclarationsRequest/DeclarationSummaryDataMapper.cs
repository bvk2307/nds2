﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.DataMapping;
using System;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.DeclarationsRequest
{
    internal class DeclarationSummaryDataMapper<TData> : DataMapper<TData>
        where TData : DeclarationSummary, new()
    {
        public override TData MapData(IDataRecord dataRecord)
        {
            return LoadDeclarationBaseParams(dataRecord);
        }

        private static TData LoadDeclarationBaseParams(IDataRecord reader)
        {
            DeclarationSummary obj = new TData();
            obj.ID = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ID));
            obj.DECLARATION_VERSION_ID =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECLARATION_VERSION_ID));
            obj.ProcessingStage =
                (DeclarationProcessignStage)
                reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ProcessingStage));
            obj.SummaryState = (DeclarationSummaryState)reader.GetInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SummaryState));
            obj.LOAD_MARK =
                (DeclarationProcessignStage)reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.LOAD_MARK));
            obj.SEOD_DECL_ID = reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SEOD_DECL_ID));
            obj.ASK_DECL_ID = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ASK_DECL_ID));
            obj.IS_ACTUAL = reader.ReadBoolean("IS_ACTIVE");
            obj.INN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN));
            obj.INN_CONTRACTOR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN_CONTRACTOR));
            obj.KPP = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP));
            obj.KPP_EFFECTIVE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KPP_EFFECTIVE));
            obj.NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NAME));
            obj.ADDRESS1 = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS1));
            obj.ADDRESS2 = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.ADDRESS2));

            obj.CATEGORY_RU = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY_RU));
            obj.REGION_NAME = (obj.CATEGORY == 1)
                                  ? ""
                                  : reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REGION_NAME));

            obj.REGION_ENTRY = (obj.CATEGORY == 1)
                                   ? new CodeValueDictionaryEntry("", "")
                                   : new CodeValueDictionaryEntry(reader.ReadString("REGION_CODE"),
                                                                  reader.ReadString("REGION_NAME"));
            obj.REGION_CODE = (obj.REGION_ENTRY != null) ? obj.REGION_ENTRY.EntryId : String.Empty;

            obj.SOUN_NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN_NAME));
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(reader.ReadString("SOUN_CODE"), reader.ReadString("SOUN_NAME"));
            obj.SOUN_CODE = (obj.SOUN_ENTRY != null) ? obj.SOUN_ENTRY.EntryId : String.Empty;
            obj.SOUN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SOUN));
            obj.REG_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.REG_DATE));
            obj.TAX_MODE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_MODE));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.OKVED_CODE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.OKVED_CODE));
            obj.CAPITAL = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CAPITAL));
            obj.TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TAX_PERIOD));
            obj.FULL_TAX_PERIOD = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FULL_TAX_PERIOD));
            obj.FISCAL_YEAR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.FISCAL_YEAR));
            obj.FullTaxPeriod = new FullTaxPeriodInfo()
            {
                TaxPeriod = obj.TAX_PERIOD,
                FiscalYear = obj.FISCAL_YEAR,
                Description = obj.FULL_TAX_PERIOD
            };
            obj.PeriodEffective = reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PeriodEffective));
            obj.DECL_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_DATE));
            obj.SUBMISSION_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBMISSION_DATE));
            obj.DECL_TYPE_CODE = reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE_CODE));
            obj.DECL_SIGN = "";
            if (obj.DECL_TYPE_CODE == 0)
                obj.DECL_SIGN = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_SIGN));
            obj.DECL_TYPE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DECL_TYPE));
            obj.HAS_CANCELLED_CORRECTION =
                reader.ReadBoolean(TypeHelper<DeclarationSummary>.GetMemberName(t => t.HAS_CANCELLED_CORRECTION));
            obj.CORRECTION_NUMBER = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER));
            obj.CORRECTION_NUMBER_EFFECTIVE = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_EFFECTIVE));
            obj.CORRECTION_NUMBER_RANK =
                reader.ReadInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CORRECTION_NUMBER_RANK));
            obj.SUBSCRIBER_NAME = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUBSCRIBER_NAME));
            obj.PRPODP = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PRPODP));
            obj.COMPENSATION_AMNT =
                reader.ReadNullableDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT));
            obj.COMPENSATION_AMNT_SIGN =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.COMPENSATION_AMNT_SIGN));
            obj.CH8_DEALS_AMNT_TOTAL =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_DEALS_AMNT_TOTAL));
            obj.CH9_DEALS_AMNT_TOTAL =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_DEALS_AMNT_TOTAL));
            obj.CH8_NDS = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_NDS));
            obj.CH9_NDS = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_NDS));
            obj.NDS_WEIGHT = reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_WEIGHT));
            obj.DISCREP_CURRENCY_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_AMNT));
            obj.DISCREP_CURRENCY_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_CURRENCY_COUNT));
            obj.WEAK_DISCREP_COUNT = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_COUNT));
            obj.WEAK_DISCREP_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.WEAK_DISCREP_AMNT));
            obj.GAP_DISCREP_COUNT = reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_COUNT));
            obj.GAP_DISCREP_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.GAP_DISCREP_AMNT));
            obj.NDS_INCREASE_DISCREP_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_COUNT));
            obj.NDS_INCREASE_DISCREP_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.NDS_INCREASE_DISCREP_AMNT));
            obj.DISCREP_MIN_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MIN_AMNT));
            obj.DISCREP_MAX_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_MAX_AMNT));
            obj.DISCREP_AVG_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_AVG_AMNT));
            obj.DISCREP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_TOTAL_AMNT));
            obj.UPDATE_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.UPDATE_DATE));
            obj.INSPECTOR = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INSPECTOR));
            obj.SUR_CODE = (obj.DECL_TYPE_CODE == 0)
                               ? reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUR_CODE))
                               : null;
            obj.CATEGORY = null;
            if (obj.DECL_TYPE_CODE == 0)
            {
                if (reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CATEGORY)) == 1)
                {
                    obj.CATEGORY = 1;
                }
            }
            obj.CONTROL_RATIO_DATE = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DATE));
            obj.CONTROL_RATIO_SEND_TO_SEOD =
                reader.ReadBoolean(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_SEND_TO_SEOD));
            obj.CONTROL_RATIO_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_COUNT));
            obj.TOTAL_DISCREP_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.TOTAL_DISCREP_COUNT));
            obj.KNP_DISCREP_COUNT =
                reader.ReadNullableInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KNP_DISCREP_COUNT));
            obj.CONTROL_RATIO_DISCREP_COUNT =
                reader.ReadInt64(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CONTROL_RATIO_DISCREP_COUNT));

            obj.STATUS = reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.STATUS));

            //--- Begin Сводные данные по разделу 8 и разделу 8.1
            obj.SumNDSPok_8 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_8));
            obj.SumNDSPok_81 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPok_81));
            obj.SumNDSPokDL_81 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSPokDL_81));
            //--- End

            //--- Begin Сводные данные по разделу 9 и разделу 9.1
            obj.StProd18_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_9));
            obj.StProd10_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_9));
            obj.StProd0_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_9));
            obj.SumNDSProd18_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_9));
            obj.SumNDSProd10_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_9));
            obj.StProdOsv_9 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_9));
            obj.StProd18_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18_91));
            obj.StProd10_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10_91));
            obj.StProd0_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0_91));
            obj.SumNDSProd18_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18_91));
            obj.SumNDSProd10_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10_91));
            obj.StProdOsv_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsv_91));
            obj.StProd18DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd18DL_91));
            obj.StProd10DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd10DL_91));
            obj.StProd0DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd0DL_91));
            obj.SumNDSProd18DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd18DL_91));
            obj.SumNDSProd10DL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SumNDSProd10DL_91));
            obj.StProdOsvDL_91 = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProdOsvDL_91));
            //--- End

            obj.AKT_NOMKORR_8 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_8));
            obj.AKT_NOMKORR_81 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_81));
            obj.AKT_NOMKORR_9 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_9));
            obj.AKT_NOMKORR_91 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_91));
            obj.AKT_NOMKORR_10 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_10));
            obj.AKT_NOMKORR_11 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_11));
            obj.AKT_NOMKORR_12 = reader.ReadNullableInt(TypeHelper<DeclarationSummary>.GetMemberName(t => t.AKT_NOMKORR_12));

            obj.StProd = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.StProd));

            obj.DISCREP_BUY_BOOK_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_BUY_BOOK_AMNT));
            obj.DISCREP_SELL_BOOK_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DISCREP_SELL_BOOK_AMNT));

            obj.PVP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_TOTAL_AMNT));
            obj.PVP_BUY_BOOK_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_BUY_BOOK_AMNT));
            obj.PVP_SELL_BOOK_AMNT = reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_SELL_BOOK_AMNT));
            obj.PVP_DISCREP_MIN_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MIN_AMNT));
            obj.PVP_DISCREP_MAX_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_MAX_AMNT));
            obj.PVP_DISCREP_AVG_AMNT =
                reader.ReadDecimal(TypeHelper<DeclarationSummary>.GetMemberName(t => t.PVP_DISCREP_AVG_AMNT));

            obj.DateCloseKNP = reader.ReadDateTime(TypeHelper<DeclarationSummary>.GetMemberName(t => t.DateCloseKNP));

            obj.INVOICE_COUNT8 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT8));
            obj.INVOICE_COUNT81 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT81));
            obj.INVOICE_COUNT9 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT9));
            obj.INVOICE_COUNT91 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT91));
            obj.INVOICE_COUNT10 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT10));
            obj.INVOICE_COUNT11 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT11));
            obj.INVOICE_COUNT12 = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.INVOICE_COUNT12));

            obj.CH8_PRESENT_DESCRIPTION =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH8_PRESENT_DESCRIPTION));
            obj.CH9_PRESENT_DESCRIPTION =
                reader.ReadString(TypeHelper<DeclarationSummary>.GetMemberName(t => t.CH9_PRESENT_DESCRIPTION));

            obj.KnpClosedReasonId = reader.GetNullableInt32(TypeHelper<DeclarationSummary>.GetMemberName(t => t.KnpClosedReasonId));

            return (TData) obj;
        }

    }
}
