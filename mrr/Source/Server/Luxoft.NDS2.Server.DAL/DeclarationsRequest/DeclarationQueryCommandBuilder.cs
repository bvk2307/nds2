﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.DeclarationsRequest
{
    internal class DeclarationQueryCommandBuilder : SearchQueryCommandBuilder
    {
        private const string IndexedPrefix = "indexed_query";

        private const string IndexKey = "row_number";

        private const string PageQueryPattern =
            "WITH indexed_query AS (SELECT ROWNUM as row_number, q.* FROM ({0}, v.ID asc) q where ROWNUM<=:pTo) " +
            "SELECT vw.* FROM indexed_query T " +
            "inner join V$Declaration vw on vw.join_k = T.join_k " +
            "WHERE (T.row_number>:pFrom AND T.row_number<=:pTo) order by T.row_number asc";

        private const string SkipQueryPattern =
            "WITH indexed_query AS (SELECT ROWNUM as row_number, q.* FROM ({0}) q) SELECT * FROM indexed_query WHERE (indexed_query.row_number>:pFrom)";

        private int _rowsToSkip;

        private int? _rowsToTake;

        public DeclarationQueryCommandBuilder(
            string selectStatementPattern,
            FilterExpressionBase searchBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider queryPattern)
            : base(selectStatementPattern, searchBy, orderBy, queryPattern)
        {
        }

        public DeclarationQueryCommandBuilder Skip(int rowsToSkip)
        {
            _rowsToSkip = rowsToSkip;

            return this;
        }

        public DeclarationQueryCommandBuilder Take(int? rowsToTake)
        {
            _rowsToTake = rowsToTake;

            return this;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var innerStatement = base.BuildSelectStatement(parameters);

            if (_rowsToTake.HasValue)
            {
                parameters.Add("pFrom", _rowsToSkip);
                parameters.Add("pTo", _rowsToTake.Value + _rowsToSkip);

                return string.Format(
                    PageQueryPattern,
                    innerStatement,
                    _rowsToSkip,
                    _rowsToTake.Value + _rowsToSkip);
            }

            if (_rowsToSkip > 0)
            {
                parameters.Add("pFrom", _rowsToSkip);

                return string.Format(
                    SkipQueryPattern,
                    innerStatement,
                    _rowsToSkip);
            }

            return innerStatement;
        }
    }
}
