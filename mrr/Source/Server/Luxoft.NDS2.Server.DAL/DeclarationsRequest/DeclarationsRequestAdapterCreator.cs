﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.DeclarationsRequest
{
    public static class DeclarationsRequestAdapterCreator
    {
        public static ISearchAdapter<DeclarationSummary> GetDeclarationsRequestAdapter(this IServiceProvider service)
        {
            return new DeclarationsRequestAdapter(service);
        }
    }
}
