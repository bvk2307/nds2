﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.DeclarationsRequest
{
    internal sealed class DeclarationsRequestAdapter : SearchDataAdapterBase<DeclarationSummary>
    {
        private readonly IServiceProvider _service;

        public DeclarationsRequestAdapter(IServiceProvider service) : base(service)
        {
            _service = service;
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        public override IEnumerable<DeclarationSummary> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> sortBy, uint rowsToSkip, uint rowsToTake)
        {
            return new ListCommandExecuter<DeclarationSummary>(
                DataMapper(),
                _service)
                .TryExecute(
                    new DeclarationQueryCommandBuilder(
                        string.Format("select v.join_k from {0} {1} where {2} {3}", ViewName(), ViewAlias, "{0}", "{1}"),
                        filterBy,
                        sortBy,
                        PatternProvider(ViewAlias))
                    .Take((int)rowsToTake)
                    .Skip((int)rowsToSkip));
        }

        public override int Count(FilterExpressionBase filterBy)
        {
            return new CountCommandExecuter(_service)
                .TryExecute(
                    new GetCountCommandBuilder(
                            string.Format("{0} {1} where {2}", @"V$DECLARATION_SEARCH", ViewAlias, "{0}"),
                            filterBy,
                            PatternProvider(ViewAlias)));
        }

        protected override IDataMapper<DeclarationSummary> DataMapper()
        {
            return new DeclarationSummaryDataMapper<DeclarationSummary>();
        }

        protected override string ViewName()
        {
            return @"V$DECLARATION_SEARCH";
        }
    }
}
