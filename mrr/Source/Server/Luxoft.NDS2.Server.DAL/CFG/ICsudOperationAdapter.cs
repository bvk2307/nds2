using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.DTO.CFG;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public interface ICsudOperationAdapter
    {
        List<CsudOperation> Search(string baseOperation);
    }
}