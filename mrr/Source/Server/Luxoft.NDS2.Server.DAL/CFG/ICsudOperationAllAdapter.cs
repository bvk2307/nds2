﻿using Luxoft.NDS2.Server.Common.DTO.CFG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public interface ICsudOperationAllAdapter
    {
        List<CsudOperation> All();
    }
}
