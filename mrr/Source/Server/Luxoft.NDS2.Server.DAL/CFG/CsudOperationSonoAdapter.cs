﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.DAL.CFG.CommandBuilders;
using Luxoft.NDS2.Server.DAL.CFG.DataMappers;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.DataMapping;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public class CsudOperationSonoAdapter : ICsudOperationSonoAdapter, ICsudOperationSonoAllAdapter
    {
        private IServiceProvider _service;
        private ConnectionFactoryBase _connectionFactory;

        internal CsudOperationSonoAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<string> Search(long id, string sononCode)
        {
            return new ListCommandExecuter<Object>(
                new CsudOperationSonoDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new CsudOperationSonoQueryBuilder(_service, id, sononCode)).Select(x => x.ToString()).ToList();
        }

        public List<CsudOperationSono> All()
        {
            return new ListCommandExecuter<CsudOperationSono>(
                new CsudOperationSonoAllDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new CsudOperationSonoAllQueryBuilder(_service)).ToList();
        }
    }
}
