﻿using System;
using System.Data;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class CsudOperationSonoDataMapper : IDataMapper<Object>
    {
        public Object MapData(IDataRecord reader)
        {
            string ret = reader.ReadString("ChildSONO");
            
            return ret;
        }
    }
}
