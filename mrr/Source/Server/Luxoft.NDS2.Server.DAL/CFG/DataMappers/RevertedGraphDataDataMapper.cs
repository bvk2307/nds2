﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class RevertedGraphDataDataMapper : GraphDataDataMapper
    {
        public override GraphData MapData(IDataRecord reader)
        {
            var graphData = base.MapData(reader);
            graphData.IsReverted = true;

            return graphData;
        }
    }
}