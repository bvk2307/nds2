﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class CsudOperationDataMapper : IDataMapper<CsudOperation>
    {
        public CsudOperation MapData(IDataRecord reader)
        {
            var obj = new CsudOperation();

            obj.ID = reader.ReadInt64("CSUDOperationId");
            obj.Name = reader.ReadString("CSUDOperationName");
            obj.MasterName = reader.ReadString("MasterName");
            
            return obj;
        }
    }
}
