﻿using System;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class GraphDataDataMapper : IDataMapper<GraphData>
    {
        public virtual GraphData MapData(IDataRecord reader)
        {
            var data = new GraphData();

            data.Id = reader.ReadNullableInt64("ID") ?? 0;
            data.TaxPayerId = new TaxPayerId(reader.ReadStringNullable("INN"), reader.ReadStringNullable("KPP"));
            data.Name = reader.ReadStringNullable("NAME");
            data.Level = reader.ReadNullableInt64("LVL") ?? 0;
            data.ParentInn = reader.ReadStringNullable("PARENT_INN");
            data.TypeCode = (DeclarationTypeCode)reader.ReadInt("DECL_TYPE_CODE");
            data.Inspection = new CodeValueDictionaryEntry(reader.ReadString("SOUN_CODE"), reader.ReadString("SOUN_NAME"));
            data.ProcessingStage = (DeclarationProcessignStage)reader.ReadInt("PROCESSINGSTAGE");
            data.ClientNumber = reader.ReadNullableInt64("BUYERS_CNT") ?? 0;
            data.SellerNumber = reader.ReadNullableInt64("SELLERS_CNT") ?? 0;
            data.CalcNds = reader.ReadNullableDecimal("CALC_NDS") ?? 0;
            data.DeductionNds = reader.ReadNullableDecimal("DEDUCTION_NDS") ?? 0;
            data.NdsPercentage = reader.ReadNullableDecimal("SHARE_NDS_AMNT") ?? 0;
            data.DiscrepancyAmnt = reader.ReadNullableDecimal("DISCREPANCY_AMNT") ?? 0;
            data.GapDiscrepancyTotal = reader.ReadNullableDecimal("GAP_DISCREPANCY_TOTAL") ?? 0;
            data.MappedAmount = reader.ReadNullableDecimal("MAPPED_AMOUNT") ?? 0;
            data.NotMappedAmnt = reader.ReadNullableDecimal("NOT_MAPPED_AMOUNT") ?? 0;
            data.Sur = reader.ReadNullableInt("SUR");
            data.VatShare = reader.ReadDecimal("VAT_SHARE");
            data.VatTotal = reader.ReadDecimal("VAT_TOTAL");
            data.SurColorArgb = reader.ReadNullableInt("COLOR");
            data.SurDescription = reader.ReadString("DESCRIPTION");
            var defaultSur = reader.ReadNullableInt("IS_DEFAULT");
            data.SurIsDefault = Convert.ToBoolean(defaultSur);
            data.Region = reader.ReadStringNullable("REGION");
            data.RegionCode = reader.ReadStringNullable("REGION_CODE");
            data.RegionName = reader.ReadStringNullable("REGION_NAME");
            data.DiscrepancyNdsAmnt = reader.ReadNullableDecimal("NDS_AMOUNT") ?? 0;
            data.CreatedInvoiceAmnt = reader.ReadNullableDecimal("NJSA") ?? 0;
            data.ReceivedInvoiceAmnt = reader.ReadNullableDecimal("NJBA") ?? 0;
            data.DeductionBefore20150101Amnt = reader.ReadNullableDecimal("BEFORE2015_AMOUNT") ?? 0;
            data.DeductionAfter20150101Amnt = reader.ReadNullableDecimal("AFTER2015_AMOUNT") ?? 0;
            data.ClientNdsPercentage = reader.ReadNullableDecimal("CLIENT_NDS_PRC") ?? 0;
            data.SellerNdsPercentage = reader.ReadNullableDecimal("SELLER_NDS_PRC") ?? 0;
            data.NdsDiscrepancyTotal = reader.ReadNullableDecimal("NDS_DISCREP_AMNT") ?? 0;

            return data;
        }
    }
}