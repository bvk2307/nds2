﻿using System;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class InspectionGroupDataMapper : IDataMapper<InspectionGroup>
    {
        public InspectionGroup MapData(IDataRecord reader)
        {
            int rez = reader.ReadInt("INSPECTION_GROUP_ID");
            return (InspectionGroup)rez;
        }

    }
}
