﻿using System;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class RestrictionChainsDataMapper : IDataMapper<KeyValuePair<UserRestrictionKey, ConfigurationRestrictionsForChains>>
    {
        public KeyValuePair<UserRestrictionKey, ConfigurationRestrictionsForChains> MapData(IDataRecord reader)
        {
            var restrictionKey = new UserRestrictionKey(
                (UserRole)reader.ReadInt("ROLE_ID"),
                (InspectionGroup?)reader.ReadNullableInt("GROUP_ID")
            );

            var ret = new KeyValuePair<UserRestrictionKey, ConfigurationRestrictionsForChains>(
                restrictionKey,
                new ConfigurationRestrictionsForChains
                {
                    MaxLevelPurchase = reader.ReadInt("MAX_LEVEL_PURCHASE"),
                    MaxLevelSales = reader.ReadInt("MAX_LEVEL_SALES"),
                    MaxNavigatorChains = reader.ReadInt("MAX_NAVIGATOR_CHAINS"),
                    NotRestricted = reader.ReadBoolean("NOT_RESTRICTED"),
                    AllowPurchase = reader.ReadBoolean("ALLOW_PURCHASE")
                });

            return ret;
        }

    }
}
