﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.DataMappers
{
    internal class CsudOperationSonoAllDataMapper : IDataMapper<CsudOperationSono>
    {
        public CsudOperationSono MapData(IDataRecord reader)
        {
            var obj = new CsudOperationSono();

            obj.OpearationId = reader.GetInt32("operation_csud_id");
            obj.GroupSono = reader.ReadString("GroupSONO");
            obj.ChildSono = reader.ReadString("ChildSONO");

            return obj;
        }
    }
}
