using Luxoft.NDS2.Server.Common.DTO.CFG;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public interface ICsudOperationSonoAdapter
    {
        List<string> Search(long id, string sononCode);
    }
}