﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.CFG.CommandBuilders;
using Luxoft.NDS2.Server.DAL.CFG.DataMappers;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public class RestrictionChainsAdapter : IRestrictionChainsAdapter
    {
        private IServiceProvider _service;
        private ConnectionFactoryBase _connectionFactory;

        internal RestrictionChainsAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }


        public IDictionary<UserRestrictionKey, ConfigurationRestrictionsForChains> All()
        {
            return new ListCommandExecuter<KeyValuePair<UserRestrictionKey, ConfigurationRestrictionsForChains>>(
                new RestrictionChainsDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new RestrictionChainsQueryBuilder(_service)).ToDictionary(item => item.Key, item => item.Value);
        }

    }
}
