﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.CFG.CommandBuilders;
using Luxoft.NDS2.Server.DAL.CFG.DataMappers;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public class CsudOperationAdapter : ICsudOperationAdapter, ICsudOperationAllAdapter
    {
        private IServiceProvider _service;
        private ConnectionFactoryBase _connectionFactory;

        internal CsudOperationAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<CsudOperation> Search(string baseOperation)
        {
            return new ListCommandExecuter<CsudOperation>(
                new CsudOperationDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new CsudOperationQueryBuilder(_service, baseOperation)).ToList();
        }

        public List<CsudOperation> All()
        {
            return new ListCommandExecuter<CsudOperation>(
                new CsudOperationDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new CsudOperationAllQueryBuilder(_service)).ToList();
        }
    }
}
