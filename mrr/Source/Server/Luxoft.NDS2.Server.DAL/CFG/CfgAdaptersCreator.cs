﻿namespace Luxoft.NDS2.Server.DAL.CFG
{
    public static class CfgAdaptersCreator
    {
        public static ICsudOperationAdapter CsudOperationAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new CsudOperationAdapter(service, connectionFactory);
        }

        public static ICsudOperationSonoAdapter CsudOperationSonoAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new CsudOperationSonoAdapter(service, connectionFactory);
        }

        public static ICsudOperationSonoAllAdapter CsudOperationSonoAllAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new CsudOperationSonoAdapter(service, connectionFactory);
        }

        public static ICsudOperationAllAdapter CsudOperationAllAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new CsudOperationAdapter(service, connectionFactory);
        }

        public static IInspectionGroupAdapter InspectionGroupAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new InspectionGroupAdapter(service, connectionFactory);
        }

        public static IRestrictionChainsAdapter RestrictionChainsAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new RestrictionChainsAdapter(service, connectionFactory);
        }

    }
}
