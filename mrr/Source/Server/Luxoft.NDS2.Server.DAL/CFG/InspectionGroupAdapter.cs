﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.CFG.CommandBuilders;
using Luxoft.NDS2.Server.DAL.CFG.DataMappers;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public class InspectionGroupAdapter : IInspectionGroupAdapter
    {
        private IServiceProvider _service;
        private ConnectionFactoryBase _connectionFactory;

        internal InspectionGroupAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }


        public List<InspectionGroup> Search(IList<string> list)
        {
            return new ListCommandExecuter<InspectionGroup>(
                new InspectionGroupDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new InspectionGroupAdapterQueryBuilder(_service, list)).ToList();
        }

    }
}
