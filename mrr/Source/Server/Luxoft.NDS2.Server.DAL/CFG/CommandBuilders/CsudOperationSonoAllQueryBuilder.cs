﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class CsudOperationSonoAllQueryBuilder : QueryCommandBuilder
    {
        private string _requestPattern;

        public CsudOperationSonoAllQueryBuilder(IQueryTemplateProvider queryTemplate)
        {
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "GetOperationsSONOAll");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            return _requestPattern;
        }
    }
}
