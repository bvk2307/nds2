﻿using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class CsudOperationQueryBuilder : QueryCommandBuilder
    {
        private string _baseOperation;
        private string _requestPattern;

        public CsudOperationQueryBuilder(IQueryTemplateProvider queryTemplate, string oprerationBase)
        {
            _baseOperation = oprerationBase;
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "GetCSUDOperations");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.VarcharIn("OperationName", _baseOperation));

            return _requestPattern;
        }
    }
}
