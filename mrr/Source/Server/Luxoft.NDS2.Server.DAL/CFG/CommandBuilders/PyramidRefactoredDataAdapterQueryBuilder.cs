﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class PyramidRefactoredDataAdapterQueryBuilder : ExecuteProcedureCommandBuilder
    {
        private const string ProcedureCountCheckedDeclarations = "Pac$Selections.COUNT_CHECKED_DECLARATIONS";

        private const string ParamSelectionId = "pSelectionId";
        public const string ParamCheckedCount = "pChecked";
        public const string ParamAllCount = "pAll";

        private long _selectionId;

        public PyramidRefactoredDataAdapterQueryBuilder(long selectionId)
            : base(ProcedureCountCheckedDeclarations)
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamSelectionId, _selectionId));
            parameters.Add(FormatCommandHelper.NumericOut(ParamCheckedCount));
            parameters.Add(FormatCommandHelper.NumericOut(ParamAllCount));

            return result;
        }
    }
}