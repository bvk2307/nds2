﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class InspectionGroupAdapterQueryBuilder : QueryCommandBuilder
    {
        private IList<string> _listCodes;
        private string _requestPattern;

        public InspectionGroupAdapterQueryBuilder(IQueryTemplateProvider queryTemplate, IList<string> list)
        {
            _listCodes = list;
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "GetInspectionGroups");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var paramBuilder = new OracleParameterCollectionBuilder(
                parameters,
                "p_{0}");

            var sonoFilter = FilterExpressionCreator.CreateInList("SONO_CODE", _listCodes);

            var group = FilterExpressionCreator.CreateGroup().WithExpression(sonoFilter);

            return String.Format(_requestPattern, group.ToQuery(paramBuilder, "vw"));
        }

    }
}
