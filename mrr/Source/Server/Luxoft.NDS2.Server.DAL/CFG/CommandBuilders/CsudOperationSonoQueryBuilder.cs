﻿using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class CsudOperationSonoQueryBuilder : QueryCommandBuilder
    {
        private string _requestPattern;
        private string _baseSonoCode;
        private long _idCsudOperation;

        public CsudOperationSonoQueryBuilder(IQueryTemplateProvider queryTemplate, long idCsudOperation, string sonoBase)
        {
            _idCsudOperation = idCsudOperation;
            _baseSonoCode = sonoBase;
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "GetOperationsSONO");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn("pOperationId", _idCsudOperation));
            parameters.Add(FormatCommandHelper.VarcharIn("pSONOCode", _baseSonoCode));

            return _requestPattern;
        }
    }
}
