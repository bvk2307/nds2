﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class CsudOperationAllQueryBuilder : QueryCommandBuilder
    {
        private string _requestPattern;

        public CsudOperationAllQueryBuilder(IQueryTemplateProvider queryTemplate)
        {
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "GetCSUDOperationsAll");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            return _requestPattern;
        }
    }
}
