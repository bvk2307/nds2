﻿using Oracle.DataAccess.Client;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.CFG.CommandBuilders
{
    internal class RestrictionChainsQueryBuilder : QueryCommandBuilder
    {
        private string _requestPattern;

        public RestrictionChainsQueryBuilder(IQueryTemplateProvider queryTemplate)
        {
            _requestPattern = queryTemplate.GetQueryText("UserRestrictionsConfiguration", "Chains");
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            return _requestPattern;
        }

    }
}
