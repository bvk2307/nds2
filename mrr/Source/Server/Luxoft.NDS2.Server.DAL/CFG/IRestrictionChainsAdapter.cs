using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Server.DAL.CFG
{
    public interface IRestrictionChainsAdapter
    {
        IDictionary<UserRestrictionKey, ConfigurationRestrictionsForChains> All();
    }
}