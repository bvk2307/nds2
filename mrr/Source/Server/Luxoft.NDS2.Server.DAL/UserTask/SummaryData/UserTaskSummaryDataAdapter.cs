﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Selections.DataMappers;

namespace Luxoft.NDS2.Server.DAL.UserTask.SummaryData
{
    internal class UserTaskSummaryDataAdapter : IUserTaskSummaryDataAdapter
    {
        private const string GetSummaryDataProc = "PAC$USER_TASK.P$GET_SUMMARY_STATISTIC";

        private readonly ConnectionFactoryBase _connectionFactory;

        private readonly IServiceProvider _service;

        public UserTaskSummaryDataAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<Summary> GetSummaryBySono(string sonoCode)
        {
            var builder = new CommandBuilder(GetSummaryDataProc, null, sonoCode);

            return new ListCommandExecuter<Summary>(new GenericDataMapper<Summary>(), _service, _connectionFactory).TryExecute(builder).ToList();
        }
        public List<Summary> GetSummaryBySid(string inspectorSid, string sonoCode)
        {
            var builder = new CommandBuilder(GetSummaryDataProc, inspectorSid, sonoCode);

            return new ListCommandExecuter<Summary>(new GenericDataMapper<Summary>(), _service, _connectionFactory).TryExecute(builder).ToList();
        }
    }
}
