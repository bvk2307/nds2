﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.UserTask.SummaryData
{
    internal class CommandBuilder : ExecuteProcedureCommandBuilder
    {
        private const string PSid = "p_sid";
        private const string PSonoCode = "p_sono_code";
        private const string PCursor = "p_cursor";

        private string _inspectoSid;
        private string _sonoCode;

        public CommandBuilder(string procedureName, string inspectorSid, string sonoCode):base(procedureName)
        {
            _inspectoSid = inspectorSid;
            _sonoCode = sonoCode;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(PSid, _inspectoSid));
            parameters.Add(FormatCommandHelper.VarcharIn(PSonoCode, _sonoCode));
            parameters.Add(FormatCommandHelper.Cursor(PCursor));

            return result;
        }
    }
}
