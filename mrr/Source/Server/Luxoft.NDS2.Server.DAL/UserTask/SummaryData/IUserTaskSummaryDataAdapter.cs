﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Server.DAL.UserTask.SummaryData
{
    public interface IUserTaskSummaryDataAdapter
    {
        List<Summary> GetSummaryBySono(string sonoCode);
        List<Summary> GetSummaryBySid(string inspectorSid, string sonoCode);
    }
}
