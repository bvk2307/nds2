﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal abstract class UserTaskAgregateAdapter<TData> : IUserTaskAgregateAdapter<TData>
        where TData : UserTaskSummary, new()
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        protected UserTaskAgregateAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public TData[] Select(FilterExpressionBase filterBy)
        {
            var executer = 
                new ListCommandExecuter<TData>(
                    new GenericDataMapper<TData>(), 
                    _service, 
                    _connection);

            return executer.TryExecute(Command(filterBy)).ToArray();
        }

        protected abstract IOracleCommandBuilder Command(FilterExpressionBase filterBy);
    }
}
