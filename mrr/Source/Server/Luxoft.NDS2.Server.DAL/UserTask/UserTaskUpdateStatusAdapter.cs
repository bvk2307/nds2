﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder;


namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal class UserTaskUpdateStatusAdapter : BaseOracleTableAdapter, IUserTaskUpdateStatusAdapter
    {
        private readonly ConnectionFactoryBase _connectionFactory;

        private readonly IServiceProvider _service;
      
        public UserTaskUpdateStatusAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
            : base(service)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }
       
        /// <summary>
        /// Выставляет статус "Выполнено" ПЗ Ввод ответа на истребование
        /// </summary>
        public void CompleteReclaimReplyTask(long explainId)
        {
            new ResultCommandExecuter(_service, _connectionFactory)
                .TryExecute(new UserTaskCompleteReclaimReplyCommand(explainId));
        }
    }
}
