﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal class UserTaskTypeAdapter : IDictionaryCommonAdapter
    {
        private readonly ConnectionFactoryBase _connectionFactory;

        private readonly IServiceProvider _service;

        public UserTaskTypeAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }
        public List<DictionaryItem> All()
        {
            var builder = new UserTaskDictionaryCommand(DbConstants.UserTaskTypeDictionaryProcedureName);
            return new ListCommandExecuter<DictionaryItem>(new GenericDataMapper<DictionaryItem>(), _service, _connectionFactory).TryExecute(builder).ToList();
        }
    }
}
