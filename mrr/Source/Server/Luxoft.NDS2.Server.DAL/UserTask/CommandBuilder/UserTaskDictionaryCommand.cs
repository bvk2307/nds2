﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder
{
    class UserTaskDictionaryCommand : ExecuteProcedureCommandBuilder
    {
        public UserTaskDictionaryCommand(string procName)
            : base(FormatCommandHelper.PackageProcedure(DbConstants.UserTaskPackageName, procName))
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParameterCursorName));

            return result;
        }
    
    }
}
