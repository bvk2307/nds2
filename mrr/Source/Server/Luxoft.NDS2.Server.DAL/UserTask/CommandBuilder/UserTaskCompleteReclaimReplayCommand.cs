﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder
{
    class UserTaskCompleteReclaimReplyCommand : UserTaskPackageCommand
    {
        private readonly long _explaiId;
        private const string ExplainId = "ExplainId";

        public UserTaskCompleteReclaimReplyCommand(long explainId)
            : base("P$COMPLETE_RECLAIM_REPLY_TASK")
        {
            _explaiId = explainId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ExplainId, _explaiId));

            return result;
        }
    }
}
