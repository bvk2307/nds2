﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder
{
    class UserTaskPackageCommand : ExecuteProcedureCommandBuilder
    {
        protected const string PackageName = "PAC$USER_TASK";
        
        public UserTaskPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", PackageName, procedureName))
        {
        }
    }
}
