﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder
{
    internal class GetReportSqlCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private readonly DateTime _reportDate;

        public GetReportSqlCommandBuilder(DateTime reportDate)
            : base(
                FormatCommandHelper.PackageProcedure(
                    DbConstants.UserTaskPackageName, 
                    DbConstants.UserTaskReportGetIdProcedureName))
        {
            _reportDate = reportDate;
        }


        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericOut(DbConstants.ParameterReportId));
            parameters.With(FormatCommandHelper.NumericOut(DbConstants.ParameterStatus));
            parameters.With(FormatCommandHelper.DateIn(DbConstants.ParameterReportDate, _reportDate));
        }
    }
}
