﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder
{
    internal class UserTaskReportByUserCommand : SearchQueryCommandBuilder
    {
        private const string Alias = "vw";

        public UserTaskReportByUserCommand(FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy)
            : base(
                DbConstants.UserTaskReportUserViewName.SelectPattern(Alias),
                filterBy,
                orderBy,
                new PatternProvider(Alias))
        {
        }
    }
}