﻿using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder;
using System;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal class UserTaskReportAdapter : IUserTaskReportAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public UserTaskReportAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public ReportIdSearchResult FindByDate(DateTime reportDate)
        {
            var commandExecuter = new ResultCommandExecuter(_service, _connection);

            var result = commandExecuter.TryExecute(
                new GetReportSqlCommandBuilder(reportDate));

            long resultId;
            long.TryParse(result[DbConstants.ParameterReportId] == null ? "0" : result[DbConstants.ParameterReportId].ToString(), out resultId);
            return new ReportIdSearchResult
            {
                Id = resultId,
                Status = (SearchStatus) Convert.ToInt32(result[DbConstants.ParameterStatus].ToString())
            };
        }
    }
}
