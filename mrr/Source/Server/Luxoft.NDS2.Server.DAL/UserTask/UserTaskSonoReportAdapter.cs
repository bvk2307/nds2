﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.UserTask.CommandBuilder;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal class UserTaskSonoReportAdapter<T> : UserTaskAgregateAdapter<T>
        where T : UserTaskSummary, new()
    {

        public UserTaskSonoReportAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
            : base(service, connectionFactory)
        {
        }

        protected override IOracleCommandBuilder Command(FilterExpressionBase filterBy)
        {
            return new UserTaskReportByRegionCommand(filterBy, Enumerable.Empty<ColumnSort>());
        }
    }
}
