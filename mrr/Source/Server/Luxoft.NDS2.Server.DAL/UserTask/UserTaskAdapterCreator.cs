﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.DAL.UserTask.SummaryData;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    public static class UserTaskAdapterCreator
    {
        public static IUserTasksAdapter Create(
            IServiceProvider serviceProvider, 
            ConnectionFactoryBase connectionFactoryBase)
        {
            return new UserTaskAdapter(serviceProvider, connectionFactoryBase);
        }

        public static IUserTaskSummaryDataAdapter CreateSummaryDataAdapter(IServiceProvider serviceProvider, ConnectionFactoryBase connectionFactoryBase)
        {
            return new UserTaskSummaryDataAdapter(serviceProvider, connectionFactoryBase);
        }

        public static IDictionaryCommonAdapter CreateUserTaskStatusAdapter(IServiceProvider serviceProvider, ConnectionFactoryBase connectionFactoryBase)
        {
            return new UserTaskStatusAdapter(serviceProvider, connectionFactoryBase);
        }
        public static IDictionaryCommonAdapter CreateUserTaskTypeAdapter(IServiceProvider serviceProvider, ConnectionFactoryBase connectionFactoryBase)
        {
            return new UserTaskTypeAdapter(serviceProvider, connectionFactoryBase);
        }

        public static IUserTaskAgregateAdapter<UserTaskInspectorSummary> ReportByInspectorAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new UserTaskInspectorReportAdapter(service, connection);
        }

        public static IUserTaskAgregateAdapter<UserTaskSonoSummary> ReportBySonoAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new UserTaskSonoReportAdapter<UserTaskSonoSummary>(service, connection);
        }

        public static IUserTaskAgregateAdapter<UserTaskRegionSummary> ReportByRegionAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new UserTaskSonoReportAdapter<UserTaskRegionSummary>(service, connection);
        }

        public static IUserTaskReportAdapter ReportAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new UserTaskReportAdapter(service, connection);
        }

        public static IUserTaskUpdateStatusAdapter UpdateStatusAdapter(
           IServiceProvider service,
           ConnectionFactoryBase connection)
        {
            return new UserTaskUpdateStatusAdapter(service, connection);
        }
    }
}
