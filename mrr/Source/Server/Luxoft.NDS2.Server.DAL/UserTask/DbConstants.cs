﻿namespace Luxoft.NDS2.Server.DAL.UserTask
{
    public static class DbConstants
    {
        public const string UserTaskPackageName = "PAC$USER_TASK";
        public const string UserTaskTypeDictionaryProcedureName = "P$GET_DICT_TASK_TYPE";
        public const string UserTaskStatusDictionaryProcedureName = "P$GET_DICT_TASK_STATUS";

        public const string UserTaskReportUserViewName = "V$USER_TASK_REPORT_USER";
        public const string UserTaskReportSonoViewName = "V$USER_TASK_REPORT_SONO";
        public const string UserTaskReportGetIdProcedureName = "P$GET_REPORT_ID";

        public const string ParameterCursorName = "p_cursor";
        public const string ParameterReportId = "pId";
        public const string ParameterStatus = "pStatus";
        public const string ParameterReportDate = "pDate";

        public const long NoReportFoundResult = -1;
    }
}
