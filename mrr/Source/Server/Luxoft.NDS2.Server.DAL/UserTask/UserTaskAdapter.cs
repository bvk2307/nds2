﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.UserTask;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.UserTask
{
    internal class UserTaskAdapter : BaseOracleTableAdapter, IUserTasksAdapter
    {
        private const string ListViewName = "V$USER_TASK_LIST";
        private const string BriefListViewName = "V$USER_TASK_LIST_BRIEF";

        private const string ViewAlias = "vw";

        private const string GetDetailsSp = "P$GET_DETAILS_DATA";
        private const string GetActorListSp = "P$GET_ACTOR_LIST";
        private const string GetTaxPayerSolvencySp = "P$GET_DICT_TAX_PAYER_SOLVENCY";
        private const string PackageName = "PAC$USER_TASK";
        private const string PSetSolvencySp = "P$SET_SOLVENCY";
        private const string PCloseTaxpayerAlalyzeTask = "P$CLOSE_TAXPAYER_ALALYZE_TASK";

        private readonly ConnectionFactoryBase _connectionFactory;

        private readonly IServiceProvider _service;
        private string CursorName = "p_cursor";


        public UserTaskAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
            : base(service)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }
        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        public List<UserTaskInList> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> orderBy, uint rowsFrom, uint rowsTo)
        {
                return new ListCommandExecuter<UserTaskInList>(
                    new GenericDataMapper<UserTaskInList>(), 
                    _service,
                    _connectionFactory).TryExecute(
                        new OptimizedPageCommandBuilder(
                            BriefListViewName.ToSelectPatternOrdered(ViewAlias), 
                            filterBy, 
                            orderBy, 
                            new PatternProvider(ViewAlias), 
                            rowsFrom, 
                            rowsTo))
                        .ToList();
        }


        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        public List<UserTaskInList> SearchFullData(FilterExpressionBase filterBy, IEnumerable<ColumnSort> orderBy, uint rowsFrom, uint rowsTo)
        {
            return new ListCommandExecuter<UserTaskInList>(
                new GenericDataMapper<UserTaskInList>(),
                _service,
                _connectionFactory).TryExecute(
                    new OptimizedPageCommandBuilder(
                        ListViewName.ToSelectPatternOrdered(ViewAlias),
                        filterBy,
                        orderBy,
                        new PatternProvider(ViewAlias),
                        rowsFrom,
                        rowsTo))
                    .ToList();
        }

        /// <summary>
        /// поиск количества пз по заданным условиям
        /// </summary>
        public int Count(FilterExpressionBase filterBy)
        {
            return new CountCommandExecuter(
                _service,
                _connectionFactory)
                .TryExecute(
                    new GetCountCommandBuilder(
                        string.Format("{0} {1} where {2}", ListViewName, ViewAlias, "{0}"),
                        filterBy, 
                        new PatternProvider(ViewAlias)));
        }

        /// <summary>
        /// поиск общего количества пз по заданным условиям
        /// </summary>
        public int Total(FilterExpressionBase filterBy)
        {
            return new CountCommandExecuter(
                _service,
                _connectionFactory)
                .TryExecute(
                    new GetCountCommandBuilder(
                        string.Format("{0} {1} where {2}", BriefListViewName, ViewAlias, "{0}"),
                        filterBy,
                        new PatternProvider(ViewAlias)));
        }
        /// <summary>
        /// Сведения о пз - для карточки
        /// </summary>
        public UserTaskInDetails GetUserTaskDetails(
            long userTaskId,
            int taskTypeId,
            long declarationZip
            )
        {
            Func<ResultCommandHelper, UserTaskInDetails> readFunc =
                reader =>
                {
                    if (reader.Read())
                    {
                        var userTaskInDetails = new UserTaskInDetails
                        {
                            Id = reader.GetInt64("task_id"),
                            Status = reader.GetInt32("status"),
                            StatusName = reader.GetString("status_name"),
                            ActualCompleteDate = reader.GetNullableDate("actual_complete_date"),
                            PlannedCompleteDate = reader.GetNullableDate("planning_complete_date"),
                            DaysAfterExpire = reader.GetInt32("days_after_expire"),
                            DuePeriod = reader.GetInt32("days_to_process"),

                            SurCode = reader.GetNullableInt32("sur_code"),
                            KnpStatus = reader.GetString("status_knp"),
                            Sign = reader.GetString("sign"),
                            NdsTotal = reader.GetNullableDecimal("nds_total"),

									 SeodNumber = reader.GetString("external_doc_num"),
                            ClaimdocDate = reader.GetNullableDate("claimdoc_date"),
                            ClaimdocCreateNumber = reader.GetNullableInt32("claimdoc_id"),
                            ClaimdocTypeName = reader.GetString("claimdoc_type_name"),

                            ExplainId = reader.GetNullableInt64("explain_id"),
                            AnswerDate = reader.GetNullableDate("answer_incoming_date"),
                            AnswerStatusCode = reader.GetNullableInt32("answer_status_id"),
                            AnswerStatus = reader.GetString("answer_status"),
                            OtherAnswerNotExists = reader.GetInt32("other_answer_notexists"),

                            TaxPayerSolvency = reader.GetNullableInt32("v_tax_payer_solvency"),
                            Comment = reader.GetString("v_comment"),

                            KnpCloseReasonId = reader.GetNullableInt32("v_knp_close_reason_id")
                        };
                        return userTaskInDetails;
                    }
                    return null;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text =
                        FormatCommandHelper.PackageProcedure(PackageName, GetDetailsSp),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter>
                                                  {
                                                      FormatCommandHelper.NumericIn("p_task_id", userTaskId),
                                                      FormatCommandHelper.NumericIn("p_task_type_id", taskTypeId),
                                                      FormatCommandHelper.NumericIn("p_declaraton_zip", declarationZip),
                                                      FormatCommandHelper.Cursor(CursorName)
                                                  }
                }, readFunc);
        }

        /// <summary>
        ///     пз - Список исполнителей
        /// </summary>
        /// <param name="userTaskId">userTask Id</param>
        public List<Actor> GetActorList(long userTaskId)
        {
            Func<ResultCommandHelper, List<Actor>> readFunc =
                reader =>
                {
                    var result = new List<Actor>();
                    while (reader.Read())
                    {
                        result.Add(new Actor
                        {
                            AssignedName = reader.GetString("user_name"),
                            AssignDate = reader.GetNullableDate("assign_date")
                        });
                    }
                    return result;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text =
                        FormatCommandHelper.PackageProcedure(PackageName, GetActorListSp),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter>
                    {
                        FormatCommandHelper.NumericIn("p_task_id", userTaskId),
                        FormatCommandHelper.Cursor(CursorName)
                    }
                }, readFunc);
        }

        /// <summary>
        /// список статусов нп
        /// </summary>
        public List<TaxPayerSolvency> GetTaxPayerSolvencyList()
        {
            Func<ResultCommandHelper, List<TaxPayerSolvency>> readFunc =
                reader =>
                {
                    var result = new List<TaxPayerSolvency>();
                    while (reader.Read())
                        result.Add(new TaxPayerSolvency
                        {
                            Id = reader.GetInt64("solvency_id"),
                            Description = reader.GetString("description"),
                        });
                    return result;
                };

            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetTaxPayerSolvencySp),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter>
                            {
                                FormatCommandHelper.Cursor(CursorName)
                            }
                }, readFunc);
        }

        /// <summary>
        /// Выставить статус платежеспособности
        /// </summary>
        public void SetSolvency(long taskId, long solvencyStatusId, string comment)
        {
            var commands = new List<CommandContext>();
            //Выставить статус платежеспособности
            commands.Add(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, PSetSolvencySp),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new List<OracleParameter>
                                {
                                    FormatCommandHelper.NumericIn("p_task_id", taskId),
                                     FormatCommandHelper.NumericIn("p_solvency_status_id", solvencyStatusId),
                                    FormatCommandHelper.VarcharIn("p_comment",comment)
                                }
                });
            // Завершение ПЗ "Анализ налогоплательщика" 
            commands.Add(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, PCloseTaxpayerAlalyzeTask),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new List<OracleParameter>
                                {
                                    FormatCommandHelper.NumericIn("p_task_id", taskId)
                                }
                });
            Transaction(commands.ToArray());
        }
    }
}
