﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceOperationAdapterCreator
    {
        public static IExplainInvoiceOperationAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceOperationAdapter(serviceProvider);
        }
    }
}
