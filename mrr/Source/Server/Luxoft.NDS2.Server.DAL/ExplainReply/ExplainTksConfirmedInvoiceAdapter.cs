﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainTksConfirmedInvoiceAdapter : SearchDataAdapterBase<ExplainTksInvoiceConfirmed>
    {
        public ExplainTksConfirmedInvoiceAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<ExplainTksInvoiceConfirmed> DataMapper()
        {
            return new GenericDataMapper<ExplainTksInvoiceConfirmed>()
                .WithFieldMapperFor(
                    TypeHelper<ExplainTksInvoiceConfirmed>.GetMemberName(x => x.Chapter),
                    new IntFieldMapper())
                .WithFieldMapperFor(
                    TypeHelper<ExplainTksInvoiceConfirmed>.GetMemberName(x => x.Status),
                    new IntFieldMapper());
        }

        protected override string ViewName()
        {
            return DbConstants.ViewConfirmedInvoices;
        }
    }
}
