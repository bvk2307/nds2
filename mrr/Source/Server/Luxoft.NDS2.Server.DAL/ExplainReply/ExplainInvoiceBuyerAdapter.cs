﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceBuyerAdapter : 
        InvoiceContractorAdapter, IExplainInvoiceContractorAdapter
    {
        public ExplainInvoiceBuyerAdapter(IServiceProvider serviceProvider)
            : base("DOC_INVOICE_BUYER", serviceProvider)
        {

        }

        protected override string InnColumnName
        {
            get { return "BUYER_INN"; }
        }

        protected override string KppColumnName
        {
            get { return "BUYER_KPP"; }
        }
    }
}
