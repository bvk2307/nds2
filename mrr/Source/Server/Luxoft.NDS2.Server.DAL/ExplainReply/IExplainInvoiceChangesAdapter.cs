﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public interface IExplainInvoiceChangesAdapter
    {
        /// <summary>
        /// Выполняет поиск расхождений в выборке, удовлетворяющих заданным критериям
        /// </summary>
        /// <param name="searchCriteria">Критерии выборки расхождений в выборке</param>
        /// <param name="orderBy">параметры сортировки</param>
        /// <param name="docId">id автотребования</param>
        /// <returns>список изменений СФ</returns>
        IEnumerable<ExplainTksInvoiceChanges> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long docId);
    }
}
