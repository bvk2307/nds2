﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.Adapters.Explain;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainTksAdaptersCreator
    {
        public static IExplainTksInvoiceAdapter<ExplainTksInvoiceChapter8> Chapter8(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksInvoiceAdapter<ExplainTksInvoiceChapter8>(
                service,
                connection,
                DbConstants.ViewChapter8);
        }

        public static IExplainTksInvoiceAdapter<ExplainTksInvoiceChapter9> Chapter9(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksInvoiceAdapter<ExplainTksInvoiceChapter9>(
                service,
                connection,
                DbConstants.ViewChapter9);
        }

        public static IExplainTksInvoiceAdapter<ExplainTksInvoiceChapter10> Chapter10(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksInvoiceAdapter<ExplainTksInvoiceChapter10>(
                service,
                connection,
                DbConstants.ViewChapter10);
        }

        public static IExplainTksInvoiceAdapter<ExplainTksInvoiceChapter11> Chapter11(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksInvoiceAdapter<ExplainTksInvoiceChapter11>(
                service,
                connection,
                DbConstants.ViewChapter11);
        }

        public static IExplainTksInvoiceAdapter<ExplainTksInvoiceChapter12> Chapter12(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksInvoiceAdapter<ExplainTksInvoiceChapter12>(
                service,
                connection,
                DbConstants.ViewChapter12);
        }

        public static ISearchAdapter<ExplainTksInvoiceConfirmed> ConfirmedInvoices(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksConfirmedInvoiceAdapter(service, connection);
        }

        public static ISearchAdapter<ExplainTksInvoiceUnconfirmed> UnconfirmedInvoices(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainTksUnconfirmedInvoiceAdapter(service, connection);
        }

        public static IExplainInvoiceAttributeAdapter<InvoiceReceiptDocument> ReceiptDocuments(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainInvoiceReceiptDocumentAdapter(service, connection);
        }

        public static IExplainInvoiceAttributeAdapter<InvoiceReceiveDate> ReceiveDates(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainInvoiceReceiveDateAdapter(service, connection);
        }

        public static IExplainInvoiceAttributeAdapter<InvoiceContractor> Contractors(
            IServiceProvider service,
            ConnectionFactoryBase connection)
        {
            return new ExplainInvoiceContractorAdapter(service, connection);
        }

    }
}
