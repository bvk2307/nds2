﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceAdapterCreator
    {
        public static IExplainInvoiceAdapter AllInvoices(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceAdapter(serviceProvider);
        }

        public static IExplainInvoiceAdapter ModifiedInvoices(IServiceProvider serviceProvider)
        {
            return new ModifiedExplainInvoiceAdapter(serviceProvider);
        }
    }
}
