﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoicePaymentDocumentAdapter :
       SingleTableAdapterBase<InvoicePaymentDocument>, IExplainInvoicePaymentDocumentAdapter
    {
        private const string InvoiceKeyColumnName = "ROW_KEY_ID";
        private const string InvoicePaymentDocNumColumnName = "DOC_NUM";
        private const string InvoicePaymentDocDateColumnName = "DOC_DATE";

        public ExplainInvoicePaymentDocumentAdapter(IServiceProvider serviceProvider)
            : base("DOC_INVOICE_PAYMENT_DOC", serviceProvider)
        {
        }


        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<InvoicePaymentDocument>.GetMemberName(x => x.InvoiceKey),
                   InvoiceKeyColumnName);
        }

        protected override List<InvoicePaymentDocument> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<InvoicePaymentDocument>();

            while (reader.Read())
            {
                dataList.Add(new InvoicePaymentDocument
                {
                    InvoiceKey = reader.GetString(InvoiceKeyColumnName),
                    Number = reader.GetString(InvoicePaymentDocNumColumnName),
                    Date = reader.GetNullableDate(InvoicePaymentDocDateColumnName)
                });
            }

            return dataList;
        }
    }
}
