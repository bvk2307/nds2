﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class DbConstants
    {
        public static string Package = "PAC$SOV_EXPLAIN_REQUEST";

        public static string ProcedureGetById = "P$SELECT_REQUEST";

        public static string ProcedureFindByExplain = "P$SEARCH_REQUEST_BY_EXPLAIN";

        public static string ParameterId = "pId";

        public static string ParameterCursor = "pResult";

        public static string ParameterExplainId = "pExplainId";

        public static string ParameterChapter = "pChapter";

        public static string ViewChapter8 = "v$explain_tks_invoice_8";

        public static string ViewChapter9 = "v$explain_tks_invoice_9";

        public static string ViewChapter10 = "v$explain_tks_invoice_10";

        public static string ViewChapter11 = "v$explain_tks_invoice_11";

        public static string ViewChapter12 = "v$explain_tks_invoice_12";

        public static string ViewConfirmedInvoices = "v$explain_invoice_confirmed";

        public static string ViewUnconfirmedInvoices = "v$explain_invoice_unconfirmed";
    }
}
