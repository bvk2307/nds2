﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceAdapter : ExplainInvoiceAdapterBase
    {
        public ExplainInvoiceAdapter(IServiceProvider service)
            : base("V$EXPLAIN_INVOICE_EDIT", service)
        {

        }
    }
}
