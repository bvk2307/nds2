﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceOperationAdapter :
       SingleTableAdapterBase<InvoiceOperation>, IExplainInvoiceOperationAdapter
    {
        private const string InvoiceKeyColumnName = "ROW_KEY_ID";
        private const string InvoiceOperationCodeColumnName = "OPERATION_CODE";
        
        public ExplainInvoiceOperationAdapter(IServiceProvider serviceProvider)
            : base("DOC_INVOICE_OPERATION", serviceProvider)
        {
        }


        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<InvoiceOperation>.GetMemberName(x => x.InvoiceKey),
                   InvoiceKeyColumnName);
        }

        protected override List<InvoiceOperation> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<InvoiceOperation>();

            while (reader.Read())
            {
                dataList.Add(new InvoiceOperation
                {
                    InvoiceKey = reader.GetString(InvoiceKeyColumnName),
                    Code = reader.GetString(InvoiceOperationCodeColumnName),
                });
            }

            return dataList;
        }
    }
}
