﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceAttributeChangeAdapterCreator
    {
        public static IExplainInvoiceAttributeChangeAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceAttributeChangeAdapter(serviceProvider);
        }
    }
}
