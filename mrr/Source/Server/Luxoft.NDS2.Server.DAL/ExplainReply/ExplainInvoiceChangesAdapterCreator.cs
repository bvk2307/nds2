﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceChangesAdapterCreator
    {
        public static IExplainInvoiceChangesAdapter Create(this IServiceProvider serviceProvider, ConnectionFactoryBase connectionFactory)
        {
            return new ExplainInvoiceChangesAdapter(serviceProvider, connectionFactory);
        }
    }
}
