﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceBuyAcceptAdapterCreator
    {
        public static IExplainInvoiceBuyAcceptAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceBuyAcceptAdapter(serviceProvider);
        }
    }
}
