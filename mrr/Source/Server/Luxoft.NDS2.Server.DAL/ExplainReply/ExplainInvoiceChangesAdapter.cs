﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.ExplainReply.CommandBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceChangesAdapter : IExplainInvoiceChangesAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public ExplainInvoiceChangesAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<ExplainTksInvoiceChanges> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long docId)
        {
            return new ListCommandExecuter<ExplainTksInvoiceChanges>(
                new GenericDataMapper<ExplainTksInvoiceChanges>(),
                _service,
                _connectionFactory).TryExecute(
                    new ExplainTksInvoiceChangesSearchBuilder(searchCriteria, orderBy, _service,
                        docId));
        }
    }
}
