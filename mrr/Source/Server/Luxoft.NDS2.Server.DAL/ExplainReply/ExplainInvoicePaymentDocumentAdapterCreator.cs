﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoicePaymentDocumentAdapterCreator
    {
        public static IExplainInvoicePaymentDocumentAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoicePaymentDocumentAdapter(serviceProvider);
        }
    }
}
