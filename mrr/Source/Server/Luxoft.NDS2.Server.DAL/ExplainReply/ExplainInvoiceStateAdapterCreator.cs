﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceStateAdapterCreator
    {
        public static IExplainInvoiceStateAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceStateAdapter(serviceProvider);
        }
    }
}
