﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    /// <summary>
    /// Этот интерфейс описывает функции загрузки/сохранения данных СФ в рамках ответов\пояснений по АТ
    /// </summary>
    public interface IExplainInvoiceAdapter
    {
        List<ExplainInvoice> Search(
            FilterExpressionBase filterBy, 
            List<ColumnSort> orderBy, 
            uint take, 
            uint skip);

        int Count(FilterExpressionBase filterBy);
    }
}
