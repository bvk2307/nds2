﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ModifiedExplainInvoiceAdapter : ExplainInvoiceAdapterBase
    {
        public ModifiedExplainInvoiceAdapter(IServiceProvider serviceProvider)
            : base("V$EXPLAIN_INVOICE_VIEW", serviceProvider)
        {

        }
    }
}
