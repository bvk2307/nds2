﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceBuyerAdapterCreator
    {
        public static IExplainInvoiceContractorAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceBuyerAdapter(serviceProvider);
        }
    }
}
