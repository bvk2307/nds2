﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainTksInvoiceAdapter<TChapter> : SearchDataAdapterBase<TChapter>, IExplainTksInvoiceAdapter<TChapter>
        where TChapter : class, IExplainTksInvoiceBase, new()
    {
        private readonly string _viewName;

        public ExplainTksInvoiceAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection,
            string viewName)
            : base(service, connection)
        {
            _viewName = viewName;
        }

        protected override string ViewName()
        {
            return _viewName;
        }

        protected override IDataMapper<TChapter> DataMapper()
        {
            return new GenericDataMapper<TChapter>()
                    .WithFieldMapperFor(
                        TypeHelper<TChapter>.GetMemberName(x => x.Status),
                        new IntFieldMapper())
                    .WithFieldMapperFor(
                        TypeHelper<TChapter>.GetMemberName(x => x.Chapter),
                        new IntFieldMapper());
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }
    }
}
