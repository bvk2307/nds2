﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceSellerAdapter : 
       InvoiceContractorAdapter, IExplainInvoiceContractorAdapter
    {
        public ExplainInvoiceSellerAdapter(IServiceProvider serviceProvider)
            : base("DOC_INVOICE_SELLER", serviceProvider)
        {
        }

        protected override string InnColumnName
        {
            get { return "SELLER_INN"; }
        }

        protected override string KppColumnName
        {
            get { return "SELLER_KPP"; }
        }
    }
}
