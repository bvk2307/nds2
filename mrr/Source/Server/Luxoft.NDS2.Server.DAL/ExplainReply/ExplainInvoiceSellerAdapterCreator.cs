﻿namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public static class ExplainInvoiceSellerAdapterCreator
    {
        public static IExplainInvoiceContractorAdapter Create(IServiceProvider serviceProvider)
        {
            return new ExplainInvoiceSellerAdapter(serviceProvider);
        }
    }
}
