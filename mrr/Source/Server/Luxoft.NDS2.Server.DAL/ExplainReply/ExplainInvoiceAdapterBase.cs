﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal abstract class ExplainInvoiceAdapterBase
        : SingleTableAdapterBase<ExplainInvoice>, IExplainInvoiceAdapter
    {
        # region Конструктор

        protected ExplainInvoiceAdapterBase(string tableName, IServiceProvider provider)
            : base(tableName, provider)
        {
        }

        # endregion

        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName);
        }

        protected override List<ExplainInvoice> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<ExplainInvoice>();

            while (reader.Read())
            {
                var explainInvoice = new ExplainInvoice();

                explainInvoice.DocId = reader.GetInt64("DOC_ID");
                explainInvoice.DeclarationId = reader.GetInt64("DECLARATION_VERSION_ID");
                explainInvoice.CHAPTER = reader.GetInt32("INVOICE_CHAPTER");
                explainInvoice.CREATE_DATE_DT = reader.GetNullableDate("CREATE_DATE");
                explainInvoice.RECEIVE_DATE_DT = reader.GetNullableDate("RECEIVE_DATE");
                explainInvoice.INVOICE_NUM = reader.GetString("INVOICE_NUM");
                explainInvoice.INVOICE_DATE_DT = reader.GetNullableDate("INVOICE_DATE");
                explainInvoice.CHANGE_NUM_I = reader.GetNullableInt32("CHANGE_NUM");
                explainInvoice.CHANGE_DATE_DT = reader.GetNullableDate("CHANGE_DATE");
                explainInvoice.CORRECTION_NUM = reader.GetString("CORRECTION_NUM");
                explainInvoice.CORRECTION_DATE_DT = reader.GetNullableDate("CORRECTION_DATE");
                explainInvoice.CHANGE_CORRECTION_NUM_I = reader.GetNullableInt32("CHANGE_CORRECTION_NUM");
                explainInvoice.CHANGE_CORRECTION_DATE_DT = reader.GetNullableDate("CHANGE_CORRECTION_DATE");
                explainInvoice.SELLER_INVOICE_NUM = reader.GetString("SELLER_INVOICE_NUM");
                explainInvoice.SELLER_INVOICE_DATE_DT = reader.GetNullableDate("SELLER_INVOICE_DATE");
                explainInvoice.BROKER_INN = reader.GetString("BROKER_INN");
                explainInvoice.BROKER_KPP = reader.GetString("BROKER_KPP");
                explainInvoice.DEAL_KIND_CODE_I = reader.GetNullableInt32("DEAL_KIND_CODE");
                explainInvoice.CUSTOMS_DECLARATION_NUM = reader.GetString("CUSTOMS_DECLARATION_NUM");
                explainInvoice.OKV_CODE = reader.GetString("OKV_CODE");
                explainInvoice.PRICE_BUY_AMOUNT_DC = reader.GetNullableDecimal("PRICE_BUY_AMOUNT");
                explainInvoice.PRICE_BUY_NDS_AMOUNT = reader.GetNullableDecimal("PRICE_BUY_NDS_AMOUNT");
                explainInvoice.PRICE_SELL_DC = reader.GetNullableDecimal("PRICE_SELL");
                explainInvoice.PRICE_SELL_IN_CURR_DC = reader.GetNullableDecimal("PRICE_SELL_IN_CURR");
                explainInvoice.PRICE_SELL_18_DC = reader.GetNullableDecimal("PRICE_SELL_18");
                explainInvoice.PRICE_SELL_10_DC = reader.GetNullableDecimal("PRICE_SELL_10");
                explainInvoice.PRICE_SELL_0_DC = reader.GetNullableDecimal("PRICE_SELL_0");
                explainInvoice.PRICE_NDS_18 = reader.GetNullableDecimal("PRICE_NDS_18");
                explainInvoice.PRICE_NDS_10 = reader.GetNullableDecimal("PRICE_NDS_10");
                explainInvoice.PRICE_TAX_FREE_DC = reader.GetNullableDecimal("PRICE_TAX_FREE");
                explainInvoice.PRICE_TOTAL_DC = reader.GetNullableDecimal("PRICE_TOTAL");
                explainInvoice.PRICE_NDS_TOTAL = reader.GetNullableDecimal("PRICE_NDS_TOTAL");
                explainInvoice.DIFF_CORRECT_DECREASE = reader.GetNullableDecimal("DIFF_CORRECT_DECREASE");
                explainInvoice.DIFF_CORRECT_INCREASE = reader.GetNullableDecimal("DIFF_CORRECT_INCREASE");
                explainInvoice.DIFF_CORRECT_NDS_DECREASE_DC =
                    reader.GetNullableDecimal("DIFF_CORRECT_NDS_DECREASE");
                explainInvoice.DIFF_CORRECT_NDS_INCREASE_DC =
                    reader.GetNullableDecimal("DIFF_CORRECT_NDS_INCREASE");
                explainInvoice.DECL_CORRECTION_NUM = reader.GetString("DECL_CORRECTION_NUM");
                explainInvoice.PRICE_NDS_BUYER = reader.GetNullableDecimal("PRICE_NDS_BUYER");
                explainInvoice.DISPLAY_FULL_TAX_PERIOD = reader.GetString("DISPLAY_FULL_TAX_PERIOD");
                explainInvoice.TAX_PERIOD = reader.GetString("TAX_PERIOD");
                explainInvoice.FISCAL_YEAR = reader.GetString("FISCAL_YEAR");
                explainInvoice.ROW_KEY = reader.GetString("INVOICE_ROW_KEY");
                explainInvoice. ACTUAL_ROW_KEY = reader.GetString("Actual_Row_Key");
                explainInvoice.PRIZNAK_DOP_LIST = reader.GetBoolExtended("IS_DOP_LIST");
                explainInvoice.ORDINAL_NUMBER = reader.GetNullableInt64("ORDINAL_NUMBER");
                explainInvoice.SELLER_AGENCY_INFO_INN = reader.GetString("SELLER_AGENCY_INFO_INN");
                explainInvoice.SELLER_AGENCY_INFO_KPP = reader.GetString("SELLER_AGENCY_INFO_KPP");
                explainInvoice.SELLER_AGENCY_INFO_NAME = reader.GetString("SELLER_AGENCY_INFO_NAME");
                explainInvoice.SELLER_AGENCY_INFO_NUM = reader.GetString("SELLER_AGENCY_INFO_NUM");
                explainInvoice.SELLER_AGENCY_INFO_DATE_DT = reader.GetNullableDate("SELLER_AGENCY_INFO_DATE");
                explainInvoice.Type = (ExplainInvoiceType) reader.GetInt32("Type");

                explainInvoice.AddState((ExplainInvoiceState) reader.GetInt32("State"));

                dataList.Add(explainInvoice);
            }

            return dataList;
        }
    }
}
