﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public interface IExplainInvoicePaymentDocumentAdapter
    {
        List<InvoicePaymentDocument> Search(FilterExpressionBase filterBy);
    }
}
