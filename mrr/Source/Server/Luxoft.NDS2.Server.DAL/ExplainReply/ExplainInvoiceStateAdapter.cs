﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceStateAdapter 
        : SingleTableAdapterBase<ExplainInvoiceCorrectState>, IExplainInvoiceStateAdapter
    {

        private const string ColumnInvoceKey = "INVOICE_ORIGINAL_ID";

        public ExplainInvoiceStateAdapter(IServiceProvider serviceProvider)
            : base("V$EXPLAIN_INVOICE_STATE", serviceProvider)
        {
        }

        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<ExplainInvoiceCorrectState>.GetMemberName(x => x.InvoiceId),
                    ColumnInvoceKey);
        }

        protected override List<ExplainInvoiceCorrectState> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<ExplainInvoiceCorrectState>();

            while(reader.Read())
            {
                var entity = new ExplainInvoiceCorrectState();

                entity.ExplainId = reader.GetInt64("explain_Id");
                entity.InvoiceId = reader.GetString("invoice_original_id");
                entity.State = (ExplainInvoiceStateInThis)reader.GetInt32("invoice_state");
                entity.ExplainIncomingDate = reader.GetDate("incoming_date");
                var nullableBoolValue = reader.GetNullableBool("RECEIVE_BY_TKS");
                entity.RECEIVE_BY_TKS = nullableBoolValue.HasValue ? nullableBoolValue.Value : false; 

                dataList.Add(entity);
            }

            return dataList;
        }
    }
}
