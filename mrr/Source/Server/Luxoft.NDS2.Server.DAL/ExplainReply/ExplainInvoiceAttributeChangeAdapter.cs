﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceAttributeChangeAdapter 
        : SingleTableAdapterBase<ExplainInvoiceCorrect>, IExplainInvoiceAttributeChangeAdapter
    {
        private const string ColumnInvoceKey = "INVOICE_ORIGINAL_ID";

        public ExplainInvoiceAttributeChangeAdapter(IServiceProvider serviceProvider)
            : base("V$EXPLAIN_INVOICE_ATTR_CHANGE", serviceProvider)
        {

        }

        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<ExplainInvoiceCorrect>.GetMemberName(x => x.InvoiceId),
                    ColumnInvoceKey);
        }

        protected override List<ExplainInvoiceCorrect> BuildDataList(ResultCommandHelper reader)
        {
            var result = new List<ExplainInvoiceCorrect>();

            while(reader.Read())
            {
                result.Add(
                    new ExplainInvoiceCorrect
                        {
                            ExplainId = reader.GetInt64("explain_Id"),
                            ExplainIncomingDate = reader.GetDate("incoming_date"),
                            FieldName = reader.GetString("FIELD_NAME"),
                            FieldValue = reader.GetString("FIELD_VALUE"),
                            InvoiceId = reader.GetString("invoice_original_id")
                        });
            }

            return result;
        }
    }
}
