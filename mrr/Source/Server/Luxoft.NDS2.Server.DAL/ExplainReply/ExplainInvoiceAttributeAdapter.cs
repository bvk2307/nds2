﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal abstract class ExplainInvoiceAttributeAdapter<T> : IExplainInvoiceAttributeAdapter<T>
        where T : InvoiceAttribute, new()
    {
        private static string SqlQueryPattern =
            "select * from {0} {1} where {2}";

        private static string ViewAlias = "v";

        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        protected ExplainInvoiceAttributeAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public IEnumerable<T> Search(FilterExpressionBase filterBy)
        {
            var sqlCommandBuilder =
                new SearchQueryCommandBuilder(
                    string.Format(SqlQueryPattern, ViewName(), ViewAlias, "{0} {1}"),
                    filterBy,
                    new ColumnSort[0],
                    new PatternProvider(ViewAlias));

            return new ListCommandExecuter<T>(
                new GenericDataMapper<T>()
                    .WithFieldMapperFor(
                        TypeHelper<T>.GetMemberName(x => x.State),
                        new IntFieldMapper()),
                    _service,
                    _connection)
                .TryExecute(sqlCommandBuilder);
        }

        protected abstract string ViewName();
    }

    internal class ExplainInvoiceReceiptDocumentAdapter : ExplainInvoiceAttributeAdapter<InvoiceReceiptDocument>
    {
        public ExplainInvoiceReceiptDocumentAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override string ViewName()
        {
            return "V$EXPLAIN_TKS_RECEIPT_DOCUMENT";
        }
    }

    internal class ExplainInvoiceReceiveDateAdapter : ExplainInvoiceAttributeAdapter<InvoiceReceiveDate>
    {
        public ExplainInvoiceReceiveDateAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override string ViewName()
        {
            return "V$EXPLAIN_TKS_RECEIVE_DATE";
        }
    }

    internal class ExplainInvoiceContractorAdapter : ExplainInvoiceAttributeAdapter<InvoiceContractor>
    {
        public ExplainInvoiceContractorAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override string ViewName()
        {
            return "V$EXPLAIN_TKS_CONTRACTOR";
        }
    }
}
