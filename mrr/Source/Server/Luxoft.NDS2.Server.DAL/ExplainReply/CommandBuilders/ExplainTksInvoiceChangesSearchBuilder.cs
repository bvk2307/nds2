﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.ExplainReply.CommandBuilders
{
    internal class ExplainTksInvoiceChangesSearchBuilder : SearchQueryCommandBuilder
    {
        private const string ExplainTksInvoiceScope = "ExplainTksInvoice";

        private const string InvoiceChangesTemplateName = @"InvoiceChanges";

        private const string DocId = "pDocId";

        private long _docId;

        public ExplainTksInvoiceChangesSearchBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryTemplateProvider queryTemplate,
            long docId)
            : base(
                queryTemplate.GetQueryText(ExplainTksInvoiceScope, InvoiceChangesTemplateName),
                filterBy,
                orderBy,
                new PatternProvider("tks"))
        {
            _docId = docId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DocId, _docId));

            return result;
        }
    }
}
