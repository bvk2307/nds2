﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainTksUnconfirmedInvoiceAdapter : SearchDataAdapterBase<ExplainTksInvoiceUnconfirmed>
    {
        public ExplainTksUnconfirmedInvoiceAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<ExplainTksInvoiceUnconfirmed> DataMapper()
        {
            return new GenericDataMapper<ExplainTksInvoiceUnconfirmed>();
        }

        protected override string ViewName()
        {
            return DbConstants.ViewUnconfirmedInvoices;
        }
    }
}
