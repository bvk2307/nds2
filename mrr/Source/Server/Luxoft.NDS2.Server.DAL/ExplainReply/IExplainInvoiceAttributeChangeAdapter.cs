﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    public interface IExplainInvoiceAttributeChangeAdapter
    {
        List<ExplainInvoiceCorrect> Search(FilterExpressionBase filterBy);
    }
}
