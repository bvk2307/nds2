﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.ExplainReply
{
    internal class ExplainInvoiceBuyAcceptAdapter :
       SingleTableAdapterBase<InvoiceBuyAccept>, IExplainInvoiceBuyAcceptAdapter
    {
        private const string InvoiceKeyColumnName = "ROW_KEY_ID";
        private const string InvoiceBuyAcceptDateColumnName = "BUY_ACCEPT_DATE";

        public ExplainInvoiceBuyAcceptAdapter(IServiceProvider serviceProvider)
            : base("DOC_INVOICE_BUY_ACCEPT", serviceProvider)
        {
        }


        protected override PatternProvider GetPatternProvider(string tableName)
        {
            return new PatternProvider(tableName)
                .WithExpression(
                    TypeHelper<InvoiceBuyAccept>.GetMemberName(x => x.InvoiceKey),
                   InvoiceKeyColumnName);
        }

        protected override List<InvoiceBuyAccept> BuildDataList(ResultCommandHelper reader)
        {
            var dataList = new List<InvoiceBuyAccept>();

            while (reader.Read())
            {
                dataList.Add(new InvoiceBuyAccept
                {
                    InvoiceKey = reader.GetString(InvoiceKeyColumnName),
                    AcceptedAt = reader.GetNullableDate(InvoiceBuyAcceptDateColumnName)
                });
            }

            return dataList;
        }
    }
}
