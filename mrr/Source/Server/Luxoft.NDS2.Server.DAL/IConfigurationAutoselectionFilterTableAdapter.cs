﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IConfigurationAutoselectionFilterTableAdapter
    {
        List<AutoSelectionFilterConfiguration> All();

        void UpdateAll(AutoSelectionFilterConfiguration data);
    }
}
