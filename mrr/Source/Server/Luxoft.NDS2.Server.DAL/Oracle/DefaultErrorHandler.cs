﻿using System.Data.Common;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal class DefaultErrorHandler : IErrorHandler
    {
        private const string CommonErrorDbMessagePattern = "Ошибка запроса при обращении к источнику данных";

        public virtual DatabaseException Handle(DbException oraError)
        {
            return new DatabaseException(
                CommonErrorDbMessagePattern,
                oraError);
        }
    }
}
