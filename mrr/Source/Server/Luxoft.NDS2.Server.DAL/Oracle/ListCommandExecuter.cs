﻿using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal class ListCommandExecuter<TData> : CommandExecuterBase<IEnumerable<TData>>
    {
        private readonly IDataMapper<TData> _mapper;

        public ListCommandExecuter(
            IDataMapper<TData> mapper,
            ILogProvider logger,
            ConnectionFactoryBase connectionFactory)
            : base(logger, connectionFactory)
        {
            _mapper = mapper;
        }

        protected override IEnumerable<TData> Execute(OracleCommand command)
        {
            return new DbDataFetcher<TData>(command.ExecuteReader(), _mapper);
        }

        protected IEnumerable<TData> Execute(OracleDataReader reader)
        {
            return new DbDataFetcher<TData>(reader, _mapper);
        }
    }
}
