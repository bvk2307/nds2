﻿using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal class CountCommandExecuter : CommandExecuterBase<int>
    {
        private readonly string ResultParameterName = "pResult";

        public CountCommandExecuter(
            ILogProvider logger,
            ConnectionFactoryBase connectionFactory)
            : base(logger, connectionFactory)
        {
        }

        protected override int Execute(OracleCommand command)
        {
            command.ExecuteNonQuery();
            return TryReadInt(command.Parameters[ResultParameterName].Value);
        }

        private static int TryReadInt(object value)
        {
            int retVal = 0;
            if (value != null)
            {
                int valTemp = 0;
                if (int.TryParse(value.ToString(), out valTemp))
                    retVal = valTemp;
            }
            return retVal;
        }
    }
}
