﻿using System.Data.Common;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal class UniqueKeyViolationErrorHandler : DefaultErrorHandler
    {
        private const string ExceptionMessagePattern = "FAVORITE_FILTERS_UK";
        private const string ErrorMessageText = "Фильтр с таким именем уже существует у данного пользователя";

        public override DatabaseException Handle(DbException oraError)
        {
            if (oraError.Message.ToUpper().Contains(ExceptionMessagePattern))
            {
                return new UniqueKeyViolationException(
                    UniqueKey.FavoriteFilter,
                    ErrorMessageText,
                    oraError);
            }
            else
            {
                return base.Handle(oraError);
            }            
        }
    }
}
