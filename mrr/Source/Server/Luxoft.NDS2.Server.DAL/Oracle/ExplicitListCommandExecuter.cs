﻿using Luxoft.NDS2.Common.Contracts.Services;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal class ExplicitListCommandExecuter<TData> : ListCommandExecuter<TData>
        where TData : new()
    {
        private readonly int _rowsToSkip;

        public ExplicitListCommandExecuter(
            IDataMapper<TData> mapper,
            ILogProvider logger,
            ConnectionFactoryBase connectionFactory,
            int rowsToSkip)
            : base(mapper, logger, connectionFactory)
        {
            _rowsToSkip = rowsToSkip;
        }

        protected override IEnumerable<TData> Execute(OracleCommand command)
        {
            OracleDataReader reader = command.ExecuteReader();

            if (_rowsToSkip > 0)
            {
                int sc = 0;
                bool next = false;

                do
                {
                    next = reader.Read();
                    sc++;
                } while (next && sc < _rowsToSkip);
            }

            return base.Execute(reader);
        }
    }
}
