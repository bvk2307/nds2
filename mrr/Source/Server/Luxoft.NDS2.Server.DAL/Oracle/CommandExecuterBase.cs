﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Luxoft.NDS2.Server.DAL.Oracle
{
    internal abstract class CommandExecuterBase<TResult>
    {
        private readonly ILogProvider _logger;

        private readonly ConnectionFactoryBase _connectionFactory;

        private IErrorHandler _errorHandler;

        public CommandExecuterBase(
            ILogProvider logger,
            ConnectionFactoryBase connectionFactory)
        {
            _logger = logger;
            _connectionFactory = connectionFactory;
            _errorHandler = new DefaultErrorHandler();
        }

        public CommandExecuterBase<TResult> WithErrorHandler(IErrorHandler errorHandler)
        {
            if (errorHandler == null)
            {
                throw new ArgumentNullException("errorHandler");
            }

            _errorHandler = errorHandler;

            return this;
        }

        [Obsolete]
        public TResult TryExecute(IOracleCommandBuilder commandBuilder)
        {
            TryExecute(commandBuilder.BuildCommand(_connectionFactory.CreateOracleConnection());
        }

        public TResult TryExecute(OracleCommand command)
        {
            var traceInfo = new List<KeyValuePair<string, object>>();
            var watcher = new Stopwatch();
            TResult retVal = default(TResult);

            traceInfo.Add(
                new KeyValuePair<string, object>(
                    Constants.CommonMessages.TRACE_DB_CMD_TEXT,
                    command.CommandText));

            foreach (OracleParameter oracleParameter in command.Parameters)
            {
                traceInfo.Add(
                    new KeyValuePair<string, object>(
                        oracleParameter.ParameterName,
                        oracleParameter.Value));
            }

            try
            {
                watcher.Start();
                retVal = Execute(command);
                watcher.Stop();
                traceInfo.Add(
                    new KeyValuePair<string, object>(
                        Constants.CommonMessages.TRACE_DB_EXEC_TIME,
                        watcher.ElapsedMilliseconds));

                _logger.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
            }
            catch (OracleException oraEx)
            {
                _logger.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx, traceInfo);
                throw _errorHandler.Handle(oraEx);
            }
            catch (Exception ex)
            {
                _logger.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, ex, traceInfo);
                throw;
            }

            return retVal;
        }

        protected abstract TResult Execute(OracleCommand command);
    }
}
