﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring
{
    public interface ICommonAdapter
    {
        List<TnoRatingInfo> GetAllQuartersRatingsRf(int fiscalYear, int quarter, DateTime calcDate);
        List<TnoRatingInfo> GetAllQuartersRatingsUfns(int fiscalYear, int quarter, DateTime calcDate, string tnoCode);
        List<TnoRatingInfo> GetAllQuartersRatingsIfns(int fiscalYear, int quarter, DateTime calcDate, string tnoCode);
        List<TnoRatingInfo> GetAllQuartersRatingsDistrict(int fiscalYear, int quarter, DateTime calcDate, string districtCode);

        List<IfnsRatingByDiscrepancyShareInDeductionData> GetIfnsRatingByDiscrepancyShareInDeduction(int fiscalYear, int quarter, DateTime calcDate);

        Dictionary<QuarterInfo, List<ReportCalculationInfo>> SearchQuarterCalculationDates();

        List<TnoData> SearchRatingDetails(CalculationData requestData);
    }
}
