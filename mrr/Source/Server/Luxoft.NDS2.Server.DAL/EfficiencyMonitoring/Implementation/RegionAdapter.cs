﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation
{
    internal class RegionAdapter : BaseOracleTableAdapter, IRegionAdapter
    {
        private class NameReplaceInstruction
        {
            internal int OrderNumber;
            internal string TextForTruncate;
            internal string TextForReplace;
        }

        private static readonly NameReplaceInstruction[] ReplaceInstructions = {
            new NameReplaceInstruction(){OrderNumber = 1, TextForTruncate = "межрегиональная инспекция федеральной налоговой службы по крупнейшим налогоплательщикам", TextForReplace = "МРИ ФНС России по КН"},
            new NameReplaceInstruction(){OrderNumber = 2, TextForTruncate = "управление федеральной налоговой службы", TextForReplace = "УФНС России"}
        };

        private static int ReplaceInstructionCount = 2;

        private const string SQL_TABLE_REGION = "v$efficiency_monitor_by_ufns";


        // Все запросы нужно перевести в пакеты и оптимизировать (особенно запрос при выборе топ худших)
        private const int TOP_COUNT = 5;
        private const string SQL_SEARCH =
              "select * from "
            + "(select vw.* "
            + "from {0} vw "
            + "where FISCAL_YEAR = :pYear and QTR = :pQtr and CALCULATION_DATE = :pDay "
            + "and {1}) "
            + "where {2}";
        private const string SQL_SEARCH_DATES =
              "select CALCULATION_DATE "
            + "from {0} vw "
            + "where vw.FISCAL_YEAR = :pYear and vw.QTR = :pQtr "
            + "group by CALCULATION_DATE";

        private const string SQL_SEARCH_REGION_DATES =
              "select CALCULATION_DATE "
            + "from {0} vw "
            + "where vw.UFNS_CODE = :pRegionId and vw.FISCAL_YEAR = :pYear and vw.QTR = :pQtr "
            + "group by CALCULATION_DATE";
        
        private const string SQL_MAX_RANK = "select max({0}) from {1} vw where FISCAL_YEAR = :pYear and QTR = :pQtr";

        private const String SQL_SEARCH_FOR_COMPARISON =
            "select vw.* " +
            "from {0} vw " +
            "where FISCAL_YEAR = :pYear AND QTR = :pQtr and CALCULATION_DATE = :pDay " +
            "and {1}";

        private const String SQL_LAST_PERIOD_FOR_REGION =
            "select * from " +
            "(select vw.* " +
            "from {0} vw " +
            "where vw.UFNS_CODE = :pRegionId and vw.FISCAL_YEAR = :pYear AND vw.QTR = :pQtr " +
            "order by vw.CALCULATION_DATE DESC) " +
            "where rownum < 2 ";

        private const String SQL_GET_REGION_DATA =
            "select * from " +
            "(select vw.* " +
            "from {0} vw " +
            "where vw.UFNS_CODE = :pRegionId " +
                "and vw.FISCAL_YEAR = :pYear and vw.QTR = :pQtr and vw.CALCULATION_DATE = :pDay " +
            "order by vw.FISCAL_YEAR DESC, vw.QTR DESC) " +
            "where rownum < 2 ";

        private const String SQL_GET_ALL_REGION_DATA =
            "select vw.* " +
            "from {0} vw " +
            "where vw.UFNS_CODE = :pRegionId " +
            "order by vw.FISCAL_YEAR DESC, vw.QTR DESC ";

        public RegionAdapter(IServiceProvider service)
            :base(service)
        { }

        public IEnumerable<RegionData> SearchEfficiencyRaiting(RaitingRequestData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day)
            };
            
            #region Условия

            var condition = String.Empty;
            var additionCondition = "1=1";
            var conditions = new List<string>();
            if (requestData.District != "0")
            {
                conditions.Add(String.Format("DISTRICT_CODE = {0}", requestData.District));
            }

            if (requestData.IDs == null || !requestData.IDs.Any())
            {
                // Если нужно получить топ записей
                if (requestData.District == "0" && !String.IsNullOrEmpty(requestData.District))
                {
                    // По РФ
                    conditions.Add(requestData.IsTopBest ?
                        // лучших
                        String.Format("{0} >= {1} and {0} <= {2}", requestData.RankName, 1, TOP_COUNT)
                        // худших
                        : String.Format("{0} >= (({1}) - {2}) and {0} <= ({1})", requestData.RankName, String.Format(SQL_MAX_RANK, requestData.RankName, SQL_TABLE_REGION), TOP_COUNT - 1));
                }
                else
                {
                    condition = String.Format("order by {0} {1}", requestData.RankName, requestData.IsTopBest ? "ASC" : "DESC");
                    additionCondition = String.Format("rownum <= {0}", TOP_COUNT);
                }
            }
            else if ( requestData.IDs.Any() )
            {
                conditions.Add(String.Format("UFNS_CODE IN ({0})", String.Join(", ", requestData.IDs)));
            }

            condition = String.Format("{0} {1}", conditions.Any() ? String.Join(" and ", conditions) : "1=1", condition);

            #endregion
            
            var sql = String.Format(SQL_SEARCH, SQL_TABLE_REGION, condition, additionCondition);

            return ExecuteList(sql, CommandType.Text, parameters, RegionBuilder);
        }

        public IEnumerable<RegionData> SearchEfficiencyRaitingForComparison(RaitingRequestData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day)
            };

            var condition = "";
            var conditions = new List<string>();
            if (requestData.District != "0" && !String.IsNullOrEmpty(requestData.District))
            {
                conditions.Add(String.Format("DISTRICT_CODE = {0}", requestData.District));
            }

            if (null != requestData.IDs && 0 != requestData.IDs.Count)
            {
                conditions.Add(String.Format("UFNS_CODE IN ({0}) ", String.Join(", ", requestData.IDs)));
            }
            else
            {
                condition = String.Format("ORDER BY {0} {1} ", requestData.RankName, requestData.IsTopBest ? "ASC" : "DESC");
            }

            condition = String.Format("{0} {1}", conditions.Any() ? String.Join(" and ", conditions) : "1=1", condition);

            var sql = String.Format(SQL_SEARCH_FOR_COMPARISON, SQL_TABLE_REGION, condition);

            return ExecuteList(sql, CommandType.Text, parameters, RegionBuilder);
        }

        public IEnumerable<RegionData> SearchDispatchRaiting(RaitingRequestData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day)
            };

            #region Условия

            var condition = String.Empty;
            var additionCondition = "1=1";
            var conditions = new List<String>();
            if (requestData.District != "0" && !String.IsNullOrEmpty(requestData.District))
            {
                conditions.Add(String.Format("DISTRICT_CODE = {0}", requestData.District));
            }

            if (requestData.IDs == null)
            {
                condition = String.Format("order by {0} {1}", requestData.RankName, requestData.IsTopBest ? "ASC" : "DESC");
                additionCondition = String.Format("rownum <= {0}", TOP_COUNT);
            }
            else if (requestData.IDs.Count > 0)
            {
                conditions.Add(String.Format("UFNS_CODE IN ({0})", String.Join(", ", requestData.IDs)));
            }
            
            condition = String.Format("{0} {1}", conditions.Any() ? String.Join(" and ", conditions) : "1=1", condition);

            #endregion

            var sql = String.Format(SQL_SEARCH, SQL_TABLE_REGION, condition, additionCondition);

            return ExecuteList(sql, CommandType.Text, parameters, RegionBuilder);
        }

        public IEnumerable<DateTime> SearchDates(int fiscalYear, int quarter)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn(":pYear", fiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", quarter)
            };
            return ExecuteList<DateTime>(
                String.Format(SQL_SEARCH_DATES, SQL_TABLE_REGION), 
                CommandType.Text, 
                parameters,
                (reader) => { return (DateTime)reader.ReadDateTime("CALCULATION_DATE"); }
            );
        }

        /// <summary>
        /// Получает список доступных дат в квартале для данного региона
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="fiscalYear"></param>
        /// <param name="quarterNumber"></param>
        /// <returns>Перечисление с датами, полученными из БД выборкой для указанных региона, года и квартала</returns>
        /// <remarks>
        /// Реальная ситуация, когда для данного региона нет каких-то дат, имеющихся для других регионов.
        /// В этой ситуации реально получение ошибки в работе системы. 
        /// При этом причину отсутствия даты (ошибка при выгрузке из HIVE) никто рассматривать не будет.
        /// </remarks>
        public IEnumerable<DateTime> SearchRegionDates(string regionId, int fiscalYear, int quarterNumber)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.VarcharIn(":pRegionId", regionId ),
                FormatCommandHelper.NumericIn(":pYear", fiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", quarterNumber)
            };
            return ExecuteList<DateTime>(
                string.Format(SQL_SEARCH_REGION_DATES, SQL_TABLE_REGION),
                CommandType.Text,
                parameters,
                (reader) => reader.ReadDateTime("CALCULATION_DATE").Value);
        }

        public IEnumerable<RegionData> GetRegions(string districtId)
        {
            return ExecuteList<RegionData>(
                String.Format(
                "SELECT DISTINCT DISTRICT_CODE, UFNS_CODE, UFNS_NAME FROM v$efficiency_monitor_by_ufns " +
                "WHERE DISTRICT_CODE = {0} " +
                "ORDER BY DISTRICT_CODE ASC, UFNS_CODE ASC",
                districtId
                ),
                CommandType.Text,
                new OracleParameter[0],
                (reader) =>
                {
                    RegionData reg = new RegionData();
                    reg.DISTRICT_CODE = reader.ReadInt("DISTRICT_CODE");
                    reg.UFNS_CODE = reader.ReadString("UFNS_CODE");
                    reg.UFNS_NAME = reader.ReadString("UFNS_NAME");
                    return reg;
                }
            );
        }

        // mrdekk
        public RegionData GetLastRaitingForRegion(string regionId, int fiscalYear, int quarterNumber)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pRegionId", regionId),
                FormatCommandHelper.NumericIn(":pYear", fiscalYear ),
                FormatCommandHelper.NumericIn(":pQtr", quarterNumber)
            };

            List<RegionData> regions = ExecuteList(String.Format(SQL_LAST_PERIOD_FOR_REGION, SQL_TABLE_REGION)
                , CommandType.Text, parameters, RegionBuilder);
            if (null != regions && 0 != regions.Count)
                return regions[0];

            return null;
        }

        public RegionData GetRegionData(string regionId, int fiscalYear, int quarter, DateTime date)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pRegionId", regionId ),
                FormatCommandHelper.NumericIn(":pYear", fiscalYear ),
                FormatCommandHelper.NumericIn(":pQtr", quarter),
                FormatCommandHelper.DateIn(":pDay", date)
            };

            List<RegionData> regions = ExecuteList(String.Format(SQL_GET_REGION_DATA, SQL_TABLE_REGION)
                , CommandType.Text, parameters, RegionBuilder);
            if (null != regions && 0 != regions.Count)
                return regions[0];

            return null;
        }

        public IEnumerable<RegionData> GetAllRegionData(string regionId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pRegionId", regionId)
            };

            return ExecuteList(String.Format(SQL_GET_ALL_REGION_DATA, SQL_TABLE_REGION), CommandType.Text, parameters, RegionBuilder);
        }

        private RegionData RegionBuilder(OracleDataReader reader)
        {
            var obj = new RegionData();

            obj.DISTRICT_CODE = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DISTRICT_CODE));
            obj.UFNS_CODE = reader.ReadString(TypeHelper<RegionData>.GetMemberName(t => t.UFNS_CODE));
            obj.UFNS_NAME = TruncateRegionName(reader.ReadString(TypeHelper<RegionData>.GetMemberName(t => t.UFNS_NAME)));
            obj.FISCAL_YEAR = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.FISCAL_YEAR));
            obj.QTR = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.QTR));
            obj.CALCULATION_DATE = reader.ReadDateTime(TypeHelper<RegionData>.GetMemberName(t => t.CALCULATION_DATE));
            obj.DEDUCTION_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DEDUCTION_AMNT));
            obj.CALCULATION_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.CALCULATION_AMNT));
            obj.EFF_IDX_1 = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.EFF_IDX_1));
            obj.EFF_IDX_2 = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.EFF_IDX_2));
            obj.EFF_IDX_3 = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.EFF_IDX_3));
            obj.EFF_IDX_4 = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.EFF_IDX_4));
            obj.EFF_IDX_AVG = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.EFF_IDX_AVG));
            obj.RNK_FEDERAL_EFF_IDX_1 = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_1));
            obj.RNK_FEDERAL_EFF_IDX_2 = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2));
            obj.RNK_FEDERAL_EFF_IDX_3 = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_3));
            obj.RNK_FEDERAL_EFF_IDX_4 = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_4));
            obj.RNK_FEDERAL_EFF_IDX_AVG = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_AVG));

            obj.DIS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_TOTAL_AMNT));
            obj.DIS_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_TOTAL_CNT));
            obj.DIS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_GAP_TOTAL_AMNT));
            obj.DIS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_GAP_TOTAL_CNT));
            obj.DIS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_NDS_TOTAL_AMNT));
            obj.DIS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_NDS_TOTAL_CNT));

            obj.DIS_CLS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_TOTAL_AMNT));
            obj.DIS_CLS_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_TOTAL_CNT));
            obj.DIS_CLS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_AMNT));
            obj.DIS_CLS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_CNT));
            obj.DIS_CLS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_AMNT));
            obj.DIS_CLS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_CNT));

            obj.DIS_OPN_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_TOTAL_AMNT));
            obj.DIS_OPN_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_TOTAL_CNT));
            obj.DIS_OPN_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_AMNT));
            obj.DIS_OPN_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_CNT));
            obj.DIS_OPN_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_AMNT));
            obj.DIS_OPN_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_CNT));

            obj.SENT_IDX = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.SENT_IDX));
            obj.SENT_IDX_GAP = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.SENT_IDX_GAP));
            obj.SENT_IDX_NDS = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.SENT_IDX_NDS));
            obj.CLOSED_IDX = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.CLOSED_IDX));
            obj.CLOSED_IDX_GAP = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.CLOSED_IDX_GAP));
            obj.CLOSED_IDX_NDS = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.CLOSED_IDX_NDS));

            obj.DECL_ALL_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_ALL_CNT));
            obj.DECL_ALL_NDS_SUM = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DECL_ALL_NDS_SUM));
            obj.DECL_OPERATION_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_OPERATION_CNT));
            obj.DECL_DISCREPANCY_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_DISCREPANCY_CNT));
            obj.DECL_PAY_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_PAY_CNT));
            obj.DECL_PAY_NDS_SUM = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DECL_PAY_NDS_SUM));
            obj.DECL_COMPENSATE_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_COMPENSATE_CNT));
            obj.DECL_COMPENSATE_NDS_SUM = reader.ReadDecimal(TypeHelper<RegionData>.GetMemberName(t => t.DECL_COMPENSATE_NDS_SUM));
            obj.DECL_ZERO_CNT = reader.ReadInt(TypeHelper<RegionData>.GetMemberName(t => t.DECL_ZERO_CNT));


            return obj;
        }

        public static string TruncateRegionName(string regionName)
        {
            var resultValue = regionName;
            var lowCaseName = regionName.ToLower();

            for (var i = 1; i <= ReplaceInstructionCount; ++i)
            {
                var instruction = ReplaceInstructions.FirstOrDefault(r => r.OrderNumber == i);
                if (instruction != null)
                {
                    int pos = lowCaseName.IndexOf(instruction.TextForTruncate, StringComparison.Ordinal);
                    if (pos != -1)
                    {
                        resultValue = String.Concat(instruction.TextForReplace,
                            resultValue.Substring(pos + instruction.TextForTruncate.Length));
                        break;
                    }
                }
            }
            return resultValue;
        }
    }
}
