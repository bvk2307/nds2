﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation
{
    internal class CommonAdapter : BaseOracleTableAdapter, ICommonAdapter
    {
        private const string SQL_SEARCH_CALCULATE_INFO = "PAC$EFFICIENCY_MONITOR.GET_CALC_DATES";

        private const string SQL_CHART_DATA_RF = "PAC$EFFICIENCY_MONITOR.CHART_DATA_RF";
        private const string SQL_CHART_DATA_UFNS = "PAC$EFFICIENCY_MONITOR.CHART_DATA_UFNS";
        private const string SQL_CHART_DATA_IFNS = "PAC$EFFICIENCY_MONITOR.CHART_DATA_IFNS";
        private const string SQL_CHART_DATA_DISTRICT = "PAC$EFFICIENCY_MONITOR.CHART_DATA_DISTRICT";


        private const string SQL_SEARCH_CALCULATED_DETAILS_CA_UFNS = "PAC$EFFICIENCY_MONITOR.GET_CA_UFNS_LIST";
        private const string SQL_SEARCH_CALCULATED_DETAILS_CA_DISTRICT = "PAC$EFFICIENCY_MONITOR.GET_CA_DISTRICT_UFNS_LIST";
        private const string SQL_SEARCH_CALCULATED_DETAILS_UFNS_IFNS = "PAC$EFFICIENCY_MONITOR.GET_UFNS_IFNS_LIST";
        private const string SQL_SEARCH_CALCULATED_DETAILS_MIKN_IFNS = "PAC$EFFICIENCY_MONITOR.GET_CA_MIKN_UFNS_LIST";

        private const string SQL_SEARCH_CALCULATED_DETAILS_IFNS = "PAC$EFFICIENCY_MONITOR.GET_IFNS_DATA";

        private const string SQL_GET_IFNS_RATING_BY_DISCREPANCY_SHARE_IN_DEDUCTION = "PAC$EFFICIENCY_MONITOR.P$GET_IFNS_RATING_BY_INDEX_24";

        private const string SQL_SEARCH_TNO_RATING_TIME_STAT = "PAC$EFFICIENCY_MONITOR.GET_EFF_TIME_STAT";

        private Dictionary<QuarterInfo, List<ReportCalculationInfo>> _result;

        public CommonAdapter(IServiceProvider service)
            : base(service)
        { }

        public List<TnoRatingInfo> GetAllQuartersRatingsRf(int fiscalYear, int quarter, DateTime calcDate)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("p_fiscal_year", fiscalYear),
                FormatCommandHelper.NumericIn("p_quarter", quarter),
                FormatCommandHelper.DateIn("p_date", calcDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoRatingInfo>(SQL_CHART_DATA_RF, CommandType.StoredProcedure, parameters, TnoRatingHistoryBuilder);
        }

        public List<IfnsRatingByDiscrepancyShareInDeductionData> GetIfnsRatingByDiscrepancyShareInDeduction(int fiscalYear, int quarter, DateTime calcDate)
        {
            var parameters = new OracleParameter[] 
            {   
                FormatCommandHelper.NumericIn("pYear", fiscalYear),
                FormatCommandHelper.NumericIn("pQtr", quarter),
                FormatCommandHelper.DateIn("pCalcDate", calcDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return
                ExecuteList<IfnsRatingByDiscrepancyShareInDeductionData>(
                    SQL_GET_IFNS_RATING_BY_DISCREPANCY_SHARE_IN_DEDUCTION, CommandType.StoredProcedure, parameters,
                    IfnsRatingByDiscrepancyShareInDeductionBuilder);
        }

        public List<TnoRatingInfo> GetAllQuartersRatingsUfns(int fiscalYear, int quarter, DateTime calcDate, string tnoCode)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("p_fiscal_year", fiscalYear),
                FormatCommandHelper.NumericIn("p_quarter", quarter),
                FormatCommandHelper.DateIn("p_date", calcDate),
                FormatCommandHelper.VarcharIn("pTnoCode", tnoCode),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoRatingInfo>(SQL_CHART_DATA_UFNS, CommandType.StoredProcedure, parameters, TnoRatingHistoryBuilder);
        }

        public List<TnoRatingInfo> GetAllQuartersRatingsIfns(int fiscalYear, int quarter, DateTime calcDate, string tnoCode)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("p_fiscal_year", fiscalYear),
                FormatCommandHelper.NumericIn("p_quarter", quarter),
                FormatCommandHelper.DateIn("p_date", calcDate),
                FormatCommandHelper.VarcharIn("pTnoCode", tnoCode),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoRatingInfo>(SQL_CHART_DATA_IFNS, CommandType.StoredProcedure, parameters, TnoRatingHistoryBuilder);
        }

        public List<TnoRatingInfo> GetAllQuartersRatingsDistrict(int fiscalYear, int quarter, DateTime calcDate, string districtCode)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("p_fiscal_year", fiscalYear),
                FormatCommandHelper.NumericIn("p_quarter", quarter),
                FormatCommandHelper.DateIn("p_date", calcDate),
                FormatCommandHelper.VarcharIn("pDistrictCode", districtCode),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoRatingInfo>(SQL_CHART_DATA_DISTRICT, CommandType.StoredProcedure, parameters, TnoRatingHistoryBuilder);
        }


        public Dictionary<QuarterInfo, List<ReportCalculationInfo>> SearchQuarterCalculationDates()
        {
            _result = new Dictionary<QuarterInfo, List<ReportCalculationInfo>>();

            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.Cursor("pRefCursor")
            };

            ExecuteList<object>(SQL_SEARCH_CALCULATE_INFO, CommandType.StoredProcedure, parameters, CalculationDataBuilder);

            return _result;
        }

        public List<TnoData> SearchRatingDetails(CalculationData requestData)
        {

            switch (requestData.DetailLevel)
            {
                case ScopeEnum.RF_DISTRICTS:
                    return SearchRatingDetailsCaDistrict(requestData);

                case ScopeEnum.RF_UFNS:
                    return SearchRatingDetailsCaUfns(requestData);

                case ScopeEnum.UFNS:
                    return SearchRatingDetailsUfns(requestData);

                case ScopeEnum.MIKN:
                    return SearchRatingDetailsMiKnIfns(requestData);

                case ScopeEnum.IFNS:
                    return SearchRatingDetailsIfns(requestData);
                default:
                    return new List<TnoData>();
            }
        }


        private List<TnoData> SearchRatingDetailsCaUfns(CalculationData requestData)
        {

            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("pYear", requestData.Quarter.Year),
                FormatCommandHelper.NumericIn("pQtr", requestData.Quarter.Quarter),
                FormatCommandHelper.DateIn("pCalcDate", requestData.CalculationDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoData>(SQL_SEARCH_CALCULATED_DETAILS_CA_UFNS, CommandType.StoredProcedure, parameters, CalculatedDataBuilder);
        }

        private List<TnoData> SearchRatingDetailsCaDistrict(CalculationData requestData)
        {

            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.VarcharIn("pDistrictCode", requestData.SelectedTnoCode),
                FormatCommandHelper.NumericIn("pYear", requestData.Quarter.Year),
                FormatCommandHelper.NumericIn("pQtr", requestData.Quarter.Quarter),
                FormatCommandHelper.DateIn("pCalcDate", requestData.CalculationDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoData>(SQL_SEARCH_CALCULATED_DETAILS_CA_DISTRICT, CommandType.StoredProcedure, parameters, CalculatedDataBuilder);
        }

        private List<TnoData> SearchRatingDetailsUfns(CalculationData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.VarcharIn("pSelectedTno", requestData.SelectedTnoCode),
                FormatCommandHelper.NumericIn("pYear", requestData.Quarter.Year),
                FormatCommandHelper.NumericIn("pQtr", requestData.Quarter.Quarter),
                FormatCommandHelper.DateIn("pCalcDate", requestData.CalculationDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoData>(SQL_SEARCH_CALCULATED_DETAILS_UFNS_IFNS, CommandType.StoredProcedure, parameters, CalculatedDataBuilder);
        }

        private List<TnoData> SearchRatingDetailsMiKnIfns(CalculationData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.NumericIn("pYear", requestData.Quarter.Year),
                FormatCommandHelper.NumericIn("pQtr", requestData.Quarter.Quarter),
                FormatCommandHelper.DateIn("pCalcDate", requestData.CalculationDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoData>(SQL_SEARCH_CALCULATED_DETAILS_MIKN_IFNS, CommandType.StoredProcedure, parameters, CalculatedDataBuilder);
        }

        private List<TnoData> SearchRatingDetailsIfns(CalculationData requestData)
        {
            var parameters = new OracleParameter[] 
            { 
                FormatCommandHelper.VarcharIn("pTnoCode", requestData.SelectedTnoCode),
                FormatCommandHelper.NumericIn("pYear", requestData.Quarter.Year),
                FormatCommandHelper.NumericIn("pQtr", requestData.Quarter.Quarter),
                FormatCommandHelper.DateIn("pCalcDate", requestData.CalculationDate),
                FormatCommandHelper.Cursor("pRefCursor")
            };

            return ExecuteList<TnoData>(SQL_SEARCH_CALCULATED_DETAILS_IFNS, CommandType.StoredProcedure, parameters, CalculatedDataBuilder);
        }


        private object CalculationDataBuilder(OracleDataReader reader)
        {
            QuarterInfo q = new QuarterInfo()
            {
                Year = reader.ReadInt("fiscal_year"),
                Quarter = reader.ReadInt("period")
            };

            if (!_result.ContainsKey(q))
            {
                _result.Add(q, new List<ReportCalculationInfo>());
            }
            var calcInfo = new ReportCalculationInfo()
            {
                Date = reader.ReadDateTime("calc_date").Value,
                IsBasedOnEfficiencyAgregate = reader.ReadInt("Is_Eff_Agr_Calc")
            };

            _result[q].Add(calcInfo);

            return null;
        }

        private TnoRatingInfo TnoRatingHistoryBuilder(OracleDataReader reader)
        {
            var obj = new TnoRatingInfo();

            obj.TnoCode = reader.ReadString("TNO_CODE");
            obj.Quarter = reader.ReadInt("QTR");
            obj.FiscalYear = reader.ReadInt("FISCAL_YEAR");
            obj.Rating_2_1_Value = reader.ReadDecimal("eff_idx_2_1");
            obj.Rating_2_2_Value = reader.ReadDecimal("eff_idx_2_2");
            obj.Rating_2_3_Value = reader.ReadDecimal("eff_idx_2_3");
            obj.Rating_2_4_Value = reader.ReadDecimal("eff_idx_2_4");

            return obj;
        }


        private IfnsRatingByDiscrepancyShareInDeductionData IfnsRatingByDiscrepancyShareInDeductionBuilder(
            OracleDataReader reader)
        {
            var obj = new IfnsRatingByDiscrepancyShareInDeductionData();

            obj.Rating = reader.ReadInt("Rating");
            obj.SonoCode = reader.ReadString("SonoCode");
            obj.SonoName = reader.ReadString("SonoName");
            obj.RegionCode = reader.ReadString("RegionCode");
            obj.RegionName = reader.ReadString("RegionName");
            obj.DiscrepancyAmount = reader.ReadDecimal("DiscrepancyAmount");
            obj.DeductionAmount = reader.ReadDecimal("DeductionAmount");
            obj.ShareValue = reader.ReadDecimal("ShareValue");
            obj.RatingPrev = reader.ReadNullableInt("RatingPrev");
            return obj;
        }

        private TnoData CalculatedDataBuilder(OracleDataReader reader)
        {
            var obj = new TnoData();


            obj.RATING_TYPE = reader.ReadString(TypeHelper<TnoData>.GetMemberName(t => t.RATING_TYPE));
            obj.TNO_CODE = reader.ReadString(TypeHelper<TnoData>.GetMemberName(t => t.TNO_CODE));
            obj.TNO_NAME = reader.ReadString(TypeHelper<TnoData>.GetMemberName(t => t.TNO_NAME));

            obj.DEDUCTION_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DEDUCTION_AMNT));
            obj.CALCULATION_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.CALCULATION_AMNT));

            obj.DIS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_TOTAL_AMNT));
            obj.DIS_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_TOTAL_CNT));
            obj.DIS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_GAP_TOTAL_AMNT));
            obj.DIS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_GAP_TOTAL_CNT));
            obj.DIS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_NDS_TOTAL_AMNT));
            obj.DIS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_NDS_TOTAL_CNT));

            obj.DIS_CLS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_TOTAL_AMNT));
            obj.DIS_CLS_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_TOTAL_CNT));
            obj.DIS_CLS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_AMNT));
            obj.DIS_CLS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_CNT));
            obj.DIS_CLS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_AMNT));
            obj.DIS_CLS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_CNT));

            obj.DIS_OPN_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_TOTAL_AMNT));
            obj.DIS_OPN_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_TOTAL_CNT));
            obj.DIS_OPN_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_AMNT));
            obj.DIS_OPN_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_CNT));
            obj.DIS_OPN_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_AMNT));
            obj.DIS_OPN_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_CNT));


            obj.DECL_ALL_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_ALL_CNT));
            obj.DECL_ALL_NDS_SUM = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DECL_ALL_NDS_SUM));
            obj.DECL_OPERATION_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_OPERATION_CNT));
            obj.DECL_DISCREPANCY_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_DISCREPANCY_CNT));
            obj.DECL_PAY_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_PAY_CNT));
            obj.DECL_PAY_NDS_SUM = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DECL_PAY_NDS_SUM));
            obj.DECL_COMPENSATE_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_COMPENSATE_CNT));
            obj.DECL_COMPENSATE_NDS_SUM = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.DECL_COMPENSATE_NDS_SUM));
            obj.DECL_ZERO_CNT = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.DECL_ZERO_CNT));

            obj.EFF_IDX_2_1 = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.EFF_IDX_2_1));
            obj.EFF_IDX_2_2 = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.EFF_IDX_2_2));
            obj.EFF_IDX_2_3 = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.EFF_IDX_2_3));
            obj.EFF_IDX_2_4 = reader.ReadDecimal(TypeHelper<TnoData>.GetMemberName(t => t.EFF_IDX_2_4));

            obj.RNK_EFF_IDX_2_1 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_1));
            obj.RNK_EFF_IDX_2_2 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_2));
            obj.RNK_EFF_IDX_2_3 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_3));
            obj.RNK_EFF_IDX_2_4 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_4));

            obj.RNK_EFF_IDX_2_1_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_1_PREV));
            obj.RNK_EFF_IDX_2_2_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_2_PREV));
            obj.RNK_EFF_IDX_2_3_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_3_PREV));
            obj.RNK_EFF_IDX_2_4_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_EFF_IDX_2_4_PREV));

            obj.RNK_FEDERAL_EFF_IDX_2_1 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_1));
            obj.RNK_FEDERAL_EFF_IDX_2_2 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_2));
            obj.RNK_FEDERAL_EFF_IDX_2_3 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_3));
            obj.RNK_FEDERAL_EFF_IDX_2_4 = reader.ReadInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_4));

            obj.RNK_FEDERAL_EFF_IDX_2_1_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_1_PREV));
            obj.RNK_FEDERAL_EFF_IDX_2_2_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_2_PREV));
            obj.RNK_FEDERAL_EFF_IDX_2_3_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_3_PREV));
            obj.RNK_FEDERAL_EFF_IDX_2_4_PREV = reader.ReadNullableInt(TypeHelper<TnoData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2_4_PREV));

            return obj;
        }


        Dictionary<QuarterInfo, List<ReportCalculationInfo>> ICommonAdapter.SearchQuarterCalculationDates()
        {
            return SearchQuarterCalculationDates();
        }
    }
}
