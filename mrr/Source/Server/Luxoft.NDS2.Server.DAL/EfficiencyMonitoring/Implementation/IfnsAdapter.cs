﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;

using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Helpers;

using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.Helpers;


namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation
{
    internal class IfnsAdapter : BaseOracleTableAdapter, IIfnsAdapter
    {
        private class NameReplaceInstruction
        {
            internal int OrderNumber;
            internal string TextForTruncate;
            internal string TextForReplace;
        }

        private static readonly NameReplaceInstruction[] ReplaceInstructions =
        {
            new NameReplaceInstruction()
            {
                OrderNumber = 1,
                TextForTruncate = "инспекция федеральной налоговой службы",
                TextForReplace = "ИФНС России"
            }
        };

        private static int ReplaceInstructionCount = 1;

        private const string SQL_TABLE_IFNS = "v$efficiency_monitor_by_ifns";

        private const int TOP_COUNT = 5;

        private const string SQL_SEARCH =
            "select vw.* " +
            "from {0} vw " +
            "where FISCAL_YEAR = :pYear and QTR = :pQtr and CALCULATION_DATE = :pDay " +
            "and REGION_CODE = :pRegionId " +
            "order by {1}";

        private const string SQL_SEARCH_NO_REGION =
            "select vw.* " +
            "from {0} vw " +
            "where FISCAL_YEAR = :pYear and QTR = :pQtr and CALCULATION_DATE = :pDay " +
            "order by {1}";

        private const string SQL_SEARCH_IN_CODES =
            "select vw.* " +
            "from {0} vw " +
            "where FISCAL_YEAR = :pYear and QTR = :pQtr and CALCULATION_DATE = :pDay " +
            "and REGION_CODE = :pRegionId " +
            "{1} " +
            "order by {2}";

        private const string SQL_SEARCH_IN_CODES_NO_REGION =
            "select vw.* " +
            "from {0} vw " +
            "where FISCAL_YEAR = :pYear and QTR = :pQtr and CALCULATION_DATE = :pDay " +
            "{1} " +
            "order by {2}";

        private const string SQL_SEARCH_DATES =
            "select CALCULATION_DATE " +
            "from {0} " +
            "where FISCAL_YEAR = :pYear and QTR = :pQtr " +
            "and REGION_CODE = :pRegionId " +
            "group by CALCULATION_DATE";

        private const string SQL_MAX_RANK =
            "select max({0}) from  {1} vw where FISCAL_YEAR = :pYear and QTR = :pQtr and REGION_CODE = :pRegionId";

        private const String SQL_LAST_PERIOD_FOR_INSPECTION =
            "select * from " +
            "(select vw.* " +
            "from {0} vw " +
            "where vw.SONO_CODE = :pSonoCode and FISCAL_YEAR = :pYear and QTR = :pQtr " +
            "order by vw.CALCULATION_DATE DESC) " +
            "where rownum < 2 ";

        private const String SQL_GET_IFNS_DATA =
            "select * from " +
            "(select vw.* " +
            "from {0} vw " +
            "where vw.SONO_CODE = :pSonoCode " +
            "and vw.FISCAL_YEAR = :pYear and vw.QTR = :pQtr and vw.CALCULATION_DATE = :pDay " +
            "order by vw.FISCAL_YEAR DESC, vw.QTR DESC) " +
            "where rownum < 2 ";

        private const String SQL_ALL_IFNS_FOR_INSPECTION =
            "select vw.* " +
            "from {0} vw " +
            "where vw.SONO_CODE = :pSonoCode " +
            "order by vw.FISCAL_YEAR DESC, vw.QTR DESC ";

        public IfnsAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public IEnumerable<IfnsData> SearchEfficiencyRaiting(RaitingRequestData requestData,
            string regionId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day),
                FormatCommandHelper.VarcharIn(":pRegionId", regionId)
            };
            if (regionId == "-1")
            {
                parameters = new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                    FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                    FormatCommandHelper.DateIn(":pDay", requestData.Day)
                };
            }

            var condition2 =
                requestData.IsTopBest
                    ? String.Format("{0} ASC", requestData.RankName)
                    : String.Format("{0} DESC", requestData.RankName);

            var sql = String.Format("-1" != regionId ? SQL_SEARCH : SQL_SEARCH_NO_REGION, SQL_TABLE_IFNS,
                condition2);

            return ExecuteList(sql, CommandType.Text, parameters, IfnsBuilder);
        }

        public IEnumerable<IfnsData> SearchEfficiencyRaiting(RaitingRequestData requestData
            , string regionId, List<string> codes)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day),
                FormatCommandHelper.VarcharIn(":pRegionId", regionId),
            };

            if ("-1" == regionId)
            {
                parameters = new OracleParameter[]
                {
                    FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                    FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                    FormatCommandHelper.DateIn(":pDay", requestData.Day)
                };
            }

            var condition = (null != codes && 0 != codes.Count)
                ? String.Format("and SONO_CODE IN ({0}) ", String.Join(", ", codes))
                : "";

            var condition2 =
                requestData.IsTopBest
                    ? String.Format("{0} ASC", requestData.RankName)
                    : String.Format("{0} DESC", requestData.RankName);



            var sql = String.Format("-1" != regionId ? SQL_SEARCH_IN_CODES : SQL_SEARCH_IN_CODES_NO_REGION
                , SQL_TABLE_IFNS, condition, condition2);

            return ExecuteList(sql, CommandType.Text, parameters, IfnsBuilder);
        }

        public IEnumerable<IfnsData> SearchEfficiencyRaitingForComparison(
            RaitingRequestData requestData, string regionId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pYear", requestData.FiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", requestData.Quarter),
                FormatCommandHelper.DateIn(":pDay", requestData.Day),
                FormatCommandHelper.VarcharIn(":pRegionId", regionId)
            };
            var conditions = new List<string>();

            string orderCondition = String.Empty;
            if (requestData.IDs != null && requestData.IDs.Count != 0)
            {
                conditions.Add(String.Format("SONO_CODE IN ({0}) ", String.Join(", ", requestData.IDs)));
            }

            orderCondition = String.Format("{0} {1} ", requestData.RankName, requestData.IsTopBest ? "ASC" : "DESC");

            var sql = String.Format(SQL_SEARCH, SQL_TABLE_IFNS,
                conditions.Any() ? String.Join(" and ", conditions) : "1=1", orderCondition);

            return ExecuteList(sql, CommandType.Text, parameters, IfnsBuilder);
        }

        public IEnumerable<DateTime> SearchDates(int fiscalYear, int quarter, string regionId)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.NumericIn(":pYear", fiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", quarter),
                FormatCommandHelper.VarcharIn(":pRegionId", regionId)
            };

            return ExecuteList<DateTime>(
                String.Format(SQL_SEARCH_DATES, SQL_TABLE_IFNS),
                CommandType.Text,
                parameters,
                (reader) => { return (DateTime) reader.ReadDateTime("CALCULATION_DATE"); }
                );
        }

        public IfnsData GetLastRatingForIfns(string sonoCode, int fiscalYear, int quarterNumber)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pSonoCode", sonoCode),
                FormatCommandHelper.NumericIn(":pYear", fiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", quarterNumber)
            };

            List<IfnsData> inspections =
                ExecuteList(String.Format(SQL_LAST_PERIOD_FOR_INSPECTION, SQL_TABLE_IFNS)
                    , CommandType.Text, parameters, IfnsBuilder);
            if (null != inspections && 0 != inspections.Count)
                return inspections[0];

            return null;
        }

        public IfnsData GetIfnsData(string sonoCode, int fiscalYear, int quarter, DateTime date)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pSonoCode", sonoCode),
                FormatCommandHelper.NumericIn(":pYear", fiscalYear),
                FormatCommandHelper.NumericIn(":pQtr", quarter),
                FormatCommandHelper.DateIn(":pDay", date)
            };

            List<IfnsData> inspections = ExecuteList(String.Format(SQL_GET_IFNS_DATA, SQL_TABLE_IFNS)
                , CommandType.Text, parameters, IfnsBuilder);
            if (null != inspections && 0 != inspections.Count)
                return inspections[0];

            return null;
        }

        public IEnumerable<IfnsData> GetAllIfnsData(string sonoCode)
        {
            var parameters = new OracleParameter[]
            {
                FormatCommandHelper.VarcharIn(":pSonoCode", sonoCode)
            };

            return ExecuteList(String.Format(SQL_ALL_IFNS_FOR_INSPECTION, SQL_TABLE_IFNS)
                , CommandType.Text, parameters, IfnsBuilder);
        }

        private IfnsData IfnsBuilder(OracleDataReader reader)
        {
            var obj = new IfnsData();

            obj.REGION_CODE = reader.ReadString(TypeHelper<IfnsData>.GetMemberName(t => t.REGION_CODE));
            obj.REGION_NAME =
                RegionAdapter.TruncateRegionName(
                    reader.ReadString(TypeHelper<IfnsData>.GetMemberName(t => t.REGION_NAME)));
            obj.SONO_NAME =
                TruncateSonoName(reader.ReadString(TypeHelper<IfnsData>.GetMemberName(t => t.SONO_NAME)));
            obj.FISCAL_YEAR = reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.FISCAL_YEAR));
            obj.QTR = reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.QTR));
            obj.SONO_CODE = reader.ReadString(TypeHelper<IfnsData>.GetMemberName(t => t.SONO_CODE));
            obj.CALCULATION_DATE =
                reader.ReadDateTime(TypeHelper<IfnsData>.GetMemberName(t => t.CALCULATION_DATE));
            obj.DEDUCTION_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.DEDUCTION_AMNT));
            obj.CALCULATION_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.CALCULATION_AMNT));
            obj.EFF_IDX_1 = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.EFF_IDX_1));
            obj.EFF_IDX_2 = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.EFF_IDX_2));
            obj.EFF_IDX_3 = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.EFF_IDX_3));
            obj.EFF_IDX_4 = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.EFF_IDX_4));
            obj.EFF_IDX_AVG = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.EFF_IDX_AVG));
            obj.RNK_REGION_EFF_IDX_1 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_REGION_EFF_IDX_1));
            obj.RNK_REGION_EFF_IDX_2 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_REGION_EFF_IDX_2));
            obj.RNK_REGION_EFF_IDX_3 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_REGION_EFF_IDX_3));
            obj.RNK_REGION_EFF_IDX_4 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_REGION_EFF_IDX_4));
            obj.RNK_REGION_EFF_IDX_AVG =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_REGION_EFF_IDX_AVG));
            obj.RNK_FEDERAL_EFF_IDX_1 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_1));
            obj.RNK_FEDERAL_EFF_IDX_2 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_2));
            obj.RNK_FEDERAL_EFF_IDX_3 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_3));
            obj.RNK_FEDERAL_EFF_IDX_4 =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_4));
            obj.RNK_FEDERAL_EFF_IDX_AVG =
                reader.ReadInt(TypeHelper<IfnsData>.GetMemberName(t => t.RNK_FEDERAL_EFF_IDX_AVG));
            obj.BUYER_DIS_OPN_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_AMNT));
            obj.BUYER_DIS_OPN_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_KNP_AMNT));
            obj.BUYER_DIS_OPN_GAP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_GAP_AMNT));
            obj.BUYER_DIS_OPN_GAP_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_GAP_KNP_AMNT));
            obj.BUYER_DIS_OPN_NDS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_NDS_AMNT));
            obj.BUYER_DIS_OPN_NDS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_OPN_NDS_KNP_AMNT));
            obj.BUYER_DIS_CLS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_AMNT));
            obj.BUYER_DIS_CLS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_KNP_AMNT));
            obj.BUYER_DIS_CLS_GAP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_GAP_AMNT));
            obj.BUYER_DIS_CLS_GAP_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_GAP_KNP_AMNT));
            obj.BUYER_DIS_CLS_NDS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_NDS_AMNT));
            obj.BUYER_DIS_CLS_NDS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_CLS_NDS_KNP_AMNT));
            obj.BUYER_DIS_ALL_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.BUYER_DIS_ALL_KNP_AMNT));
            obj.SELLER_DIS_OPN_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_AMNT));
            obj.SELLER_DIS_OPN_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_KNP_AMNT));
            obj.SELLER_DIS_OPN_GAP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_GAP_AMNT));
            obj.SELLER_DIS_OPN_GAP_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_GAP_KNP_AMNT));
            obj.SELLER_DIS_OPN_NDS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_NDS_AMNT));
            obj.SELLER_DIS_OPN_NDS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_OPN_NDS_KNP_AMNT));
            obj.SELLER_DIS_CLS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_AMNT));
            obj.SELLER_DIS_CLS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_KNP_AMNT));
            obj.SELLER_DIS_CLS_GAP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_GAP_AMNT));
            obj.SELLER_DIS_CLS_GAP_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_GAP_KNP_AMNT));
            obj.SELLER_DIS_CLS_NDS_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_NDS_AMNT));
            obj.SELLER_DIS_CLS_NDS_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_CLS_NDS_KNP_AMNT));
            obj.SELLER_DIS_ALL_KNP_AMNT =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SELLER_DIS_ALL_KNP_AMNT));
            obj.SENT_IDX = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SENT_IDX));
            obj.SENT_IDX_GAP =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SENT_IDX_GAP));
            obj.SENT_IDX_NDS =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.SENT_IDX_NDS));
            obj.CLOSED_IDX = reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.CLOSED_IDX));
            obj.CLOSED_IDX_GAP =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.CLOSED_IDX_GAP));
            obj.CLOSED_IDX_NDS =
                reader.ReadDecimal(TypeHelper<IfnsData>.GetMemberName(t => t.CLOSED_IDX_NDS));
            return obj;
        }

        private static string TruncateSonoName(string regionName)
        {
            var resultValue = regionName;
            var lowCaseName = regionName.ToLower();

            for (var i = 1; i <= ReplaceInstructionCount; ++i)
            {
                var instruction = ReplaceInstructions.FirstOrDefault(r => r.OrderNumber == i);
                if (instruction != null)
                {
                    int pos = lowCaseName.IndexOf(instruction.TextForTruncate, StringComparison.Ordinal);
                    if (pos != -1)
                    {
                        resultValue = String.Concat(instruction.TextForReplace,
                            resultValue.Substring(pos + instruction.TextForTruncate.Length));
                        break;
                    }
                }
            }
            return resultValue;
        }
    }
}
