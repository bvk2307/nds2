﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation
{
    internal class FederalAdapter: BaseOracleTableAdapter, IFederalAdapter
    {
        

        private const string SQL_SEARCH_ALL = "select * from v$effectiveness_monitor_rf";

        public FederalAdapter(IServiceProvider service)
            :base(service)
        { }

        public IEnumerable<EffectivenessMonitorFederal> SearchAll()
        {
            return ExecuteList(SQL_SEARCH_ALL, CommandType.Text, new OracleParameter[]{}, FederalBuilder);
        }


        private EffectivenessMonitorFederal FederalBuilder(OracleDataReader reader)
        {
            var obj = new EffectivenessMonitorFederal();
            
            obj.FISCAL_YEAR = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.FISCAL_YEAR));
            obj.QTR = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.QTR));
            obj.DISTRICT_CODE = reader.ReadString(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DISTRICT_CODE));
            obj.CALCULATION_DATE = reader.ReadDateTime(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.CALCULATION_DATE));
            obj.DEDUCTION_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DEDUCTION_AMNT));
            obj.CALCULATION_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.CALCULATION_AMNT));
//            obj.EFF_IDX_1 = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.EFF_IDX_1));
//            obj.EFF_IDX_2 = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.EFF_IDX_2));
//            obj.EFF_IDX_3 = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.EFF_IDX_3));
//            obj.EFF_IDX_4 = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.EFF_IDX_4));
//            obj.EFF_IDX_AVG = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.EFF_IDX_AVG));
//            obj.BUYER_DIS_OPN_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_AMNT));
//            obj.BUYER_DIS_CLS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_AMNT));
//            obj.BUYER_DIS_OPN_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_KNP_AMNT));
//            obj.BUYER_DIS_CLS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_KNP_AMNT));
//            obj.BUYER_DIS_OPN_GAP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_GAP_AMNT));
//            obj.BUYER_DIS_CLS_GAP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_GAP_AMNT));
//            obj.BUYER_DIS_OPN_GAP_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_GAP_KNP_AMNT));
//            obj.BUYER_DIS_CLS_GAP_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_GAP_KNP_AMNT));
//            obj.BUYER_DIS_OPN_NDS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_NDS_AMNT));
//            obj.BUYER_DIS_CLS_NDS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_NDS_AMNT));
//            obj.BUYER_DIS_OPN_NDS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_OPN_NDS_KNP_AMNT));
//            obj.BUYER_DIS_CLS_NDS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_CLS_NDS_KNP_AMNT));
//            obj.BUYER_DIS_ALL_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.BUYER_DIS_ALL_KNP_AMNT));
//            obj.SELLER_DIS_OPN_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_AMNT));
//            obj.SELLER_DIS_CLS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_AMNT));
//            obj.SELLER_DIS_OPN_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_KNP_AMNT));
//            obj.SELLER_DIS_CLS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_KNP_AMNT));
//            obj.SELLER_DIS_OPN_GAP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_GAP_AMNT));
//            obj.SELLER_DIS_CLS_GAP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_GAP_AMNT));
//            obj.SELLER_DIS_OPN_GAP_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_GAP_KNP_AMNT));
//            obj.SELLER_DIS_CLS_GAP_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_GAP_KNP_AMNT));
//            obj.SELLER_DIS_OPN_NDS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_NDS_AMNT));
//            obj.SELLER_DIS_CLS_NDS_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_NDS_AMNT));
//            obj.SELLER_DIS_OPN_NDS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_OPN_NDS_KNP_AMNT));
//            obj.SELLER_DIS_CLS_NDS_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_CLS_NDS_KNP_AMNT));
//            obj.SELLER_DIS_ALL_KNP_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SELLER_DIS_ALL_KNP_AMNT));
//            obj.SENT_IDX = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SENT_IDX));
//            obj.SENT_IDX_GAP = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SENT_IDX_GAP));
//            obj.SENT_IDX_NDS = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.SENT_IDX_NDS));
//            obj.CLOSED_IDX = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.CLOSED_IDX));
//            obj.CLOSED_IDX_GAP = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.CLOSED_IDX_GAP));
//            obj.CLOSED_IDX_NDS = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.CLOSED_IDX_NDS));


            obj.DIS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_TOTAL_AMNT));
            obj.DIS_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_TOTAL_CNT));
            obj.DIS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_GAP_TOTAL_AMNT));
            obj.DIS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_GAP_TOTAL_CNT));
            obj.DIS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_NDS_TOTAL_AMNT));
            obj.DIS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_NDS_TOTAL_CNT));

            obj.DIS_CLS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_TOTAL_AMNT));
            obj.DIS_CLS_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_TOTAL_CNT));
            obj.DIS_CLS_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_AMNT));
            obj.DIS_CLS_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_GAP_TOTAL_CNT));
            obj.DIS_CLS_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_AMNT));
            obj.DIS_CLS_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_CLS_NDS_TOTAL_CNT));

            obj.DIS_OPN_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_TOTAL_AMNT));
            obj.DIS_OPN_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_TOTAL_CNT));
            obj.DIS_OPN_GAP_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_AMNT));
            obj.DIS_OPN_GAP_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_GAP_TOTAL_CNT));
            obj.DIS_OPN_NDS_TOTAL_AMNT = reader.ReadDecimal(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_AMNT));
            obj.DIS_OPN_NDS_TOTAL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorRegion>.GetMemberName(t => t.DIS_OPN_NDS_TOTAL_CNT));


            obj.DECL_ALL_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_ALL_CNT));
            obj.DECL_ALL_NDS_SUM = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_ALL_NDS_SUM));
            obj.DECL_OPERATION_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_OPERATION_CNT));
            obj.DECL_DISCREPANCY_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_DISCREPANCY_CNT));
            obj.DECL_PAY_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_PAY_CNT));
            obj.DECL_PAY_NDS_SUM = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_PAY_NDS_SUM));
            obj.DECL_COMPENSATE_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_COMPENSATE_CNT));
            obj.DECL_COMPENSATE_NDS_SUM = reader.ReadDecimal(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_COMPENSATE_NDS_SUM));
            obj.DECL_ZERO_CNT = reader.ReadInt(TypeHelper<EffectivenessMonitorFederal>.GetMemberName(t => t.DECL_ZERO_CNT));


            return obj;
        }

        public IEnumerable<EffectivenessMonitorFederal> Search(string districtCode)
        {
            throw new NotImplementedException();
        }
    }
}
