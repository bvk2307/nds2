﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilders;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation
{
    public class DiscrepancyDetailsAdapter
    {
        private string SEARCH_QUERY_TEMPLATE = @"
select 
* 
from (
      select (rownum - 1) rn, t.* from 
        (
          select 
           *
          from V$EFFICIENCY_MONITOR_DETAIL vw where {0} {1}
        ) T 
      where (rownum - 1) < {3}
     ) R where rn >= {2}";


        private ConnectionFactoryBase _connectionFactory;
        private readonly IServiceProvider _service;

        public DiscrepancyDetailsAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Поиск детализации по расхождениям
        /// </summary>
        /// <param name="searchCriteria">критерии поиска. год, квартал, дата рачсета должны быть обязательными</param>
        /// <param name="orderBy">критерий соритровки</param>
        /// <returns>список детализации по расхождениям</returns>
        public List<DiscrepancyDetail> Search(FilterExpressionBase searchCriteria, IEnumerable<ColumnSort> orderBy, uint rowsFrom, uint rowsTo)
            {
                return new ListCommandExecuter<DiscrepancyDetail>(
                    new GenericDataMapper<DiscrepancyDetail>(),
                    _service,
                    _connectionFactory).TryExecute(
                        new SearchQueryCommandBuilder(string.Format(SEARCH_QUERY_TEMPLATE, "{0}", "{1}", rowsFrom, rowsTo),
                            searchCriteria,
                            orderBy, new PatternProvider("vw"))).ToList();
        }


        public int Count(FilterExpressionBase searchCriteria)
        {
            return new CountCommandExecuter(_service,
                _connectionFactory).TryExecute(new GetCountCommandBuilder("V$EFFICIENCY_MONITOR_DETAIL vw where {0}", searchCriteria, new PatternProvider("vw")));

        }

    }
}
