﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring
{
    public interface IFederalAdapter
    {
        IEnumerable<EffectivenessMonitorFederal> Search(String districtCode);

        IEnumerable<EffectivenessMonitorFederal> SearchAll();
    }
}
