﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring
{
    public interface IIfnsAdapter
    {
        IEnumerable<IfnsData> SearchEfficiencyRaiting(RaitingRequestData requestData, string regionId);
        IEnumerable<IfnsData> SearchEfficiencyRaiting(RaitingRequestData requestData, string regionId, List<string> codes);
        IEnumerable<DateTime> SearchDates(int fiscalYear, int quarterNumber, string regionId);
        IfnsData GetLastRatingForIfns(string sonoCode, int fiscalYear, int quarterNumber);
        IfnsData GetIfnsData(string sonoCode, int fiscalYear, int quarterNumber, DateTime date);
        IEnumerable<IfnsData> GetAllIfnsData(string sonoCode);

        IEnumerable<IfnsData> SearchEfficiencyRaitingForComparison(RaitingRequestData requestData, string regionId);
    }
}
