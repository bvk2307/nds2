﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.EfficiencyMonitoring
{
    public interface IRegionAdapter
    {
        /// <summary>
        /// Отбирает топ записей для рейтинга эффективности
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        IEnumerable<RegionData> SearchEfficiencyRaiting(RaitingRequestData requestData);

        IEnumerable<RegionData> SearchEfficiencyRaitingForComparison( RaitingRequestData requestData );

        /// <summary>
        /// Отбирает топ записей для ретинга отправки
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        IEnumerable<RegionData> SearchDispatchRaiting(RaitingRequestData requestData);

        /// <summary>
        /// Получает список доступных дат в квартале
        /// </summary>
        /// <returns></returns>
        IEnumerable<DateTime> SearchDates(int fiscalYear, int quarter);

        /// <summary>
        /// Получает список доступных дат в квартале для данного региона
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="fiscalYear"></param>
        /// <param name="quarterNumber"></param>
        /// <returns>Перечисление с датами, полученными из БД выборкой для указанных региона, года и квартала</returns>
        /// <remarks>
        /// Реальная ситуация, когда для данного региона нет каких-то дат, имеющихся для других регионов.
        /// В этой ситуации реально получение ошибки в работе системы. 
        /// При этом причину отсутствия даты (ошибка при выгрузке из HIVE) никто рассматривать не будет.
        /// </remarks>
        IEnumerable<DateTime> SearchRegionDates(string regionId, int fiscalYear, int quarterNumber);

        // mrdekk

        /// <summary>
        /// [smg] : Получает самый свежий рейтинг УФНС за указанный квартал (для шапки)
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="fiscalYear"></param>
        /// <param name="quarterNumber"></param>
        /// <returns></returns>
        RegionData GetLastRaitingForRegion(string regionId, int fiscalYear, int quarterNumber);

        /// <summary>
        /// Получить данные по конкретному расчету
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="fiscalYear"></param>
        /// <param name="quarter"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        RegionData GetRegionData(string regionId, int fiscalYear, int quarter, DateTime date);

        /// <summary>
        /// Получить данные за все периоды по региону
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        IEnumerable<RegionData> GetAllRegionData(string regionId);

        /// <summary>
        /// Получить список регионов
        /// </summary>
        /// <returns></returns>
        IEnumerable<RegionData> GetRegions(string districtId);
    }
}
