﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder
{
    class KnpResultPackageCommand : ExecuteProcedureCommandBuilder
    {
        public KnpResultPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.KnpResultPackage, procedureName))
        {
        }
    }
}
