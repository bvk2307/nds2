﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder
{
    class DocumentPackageCommand : ExecuteProcedureCommandBuilder
    {
        public DocumentPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.DocumentPackage, procedureName))
        {
        }
    }
}
