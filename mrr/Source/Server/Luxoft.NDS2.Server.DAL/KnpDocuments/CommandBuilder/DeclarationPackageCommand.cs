﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder
{
    class DeclarationPackageCommand : ExecuteProcedureCommandBuilder
    {
        public DeclarationPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.DeclarationPackage, procedureName))
        {
        }
    }
}
