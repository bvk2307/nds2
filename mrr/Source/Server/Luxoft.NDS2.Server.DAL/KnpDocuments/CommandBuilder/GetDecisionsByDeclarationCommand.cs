﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder
{
    class GetDecisionsByDeclarationCommand : KnpResultPackageCommand
    {
        private readonly string _innDeclarant;
        private readonly string _innContractor;
        private readonly string _kppEffective;
        private readonly int _periodEffective;
        private readonly int _fiscalYear;


        public GetDecisionsByDeclarationCommand(
            string innDeclarant,
            string innContractor,
            string kppEffective,
            int periodEffective,
            int fiscalYear)
            : base(DbConstants.GetDecisionsProcedure)
        {
            _innDeclarant = innDeclarant;
            _innContractor = innContractor;
            _kppEffective = kppEffective;
            _periodEffective = periodEffective;
            _fiscalYear = fiscalYear;
        }
        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.InnDeclarantParameter, _innDeclarant));
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.InnContractorParameter, _innContractor));
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.KppEffectiveParameter, _kppEffective));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.PeriodEffectiveParameter, _periodEffective));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.FiscalYearParameter, _fiscalYear));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.CursorParameter));
        }
    }
}
