﻿namespace Luxoft.NDS2.Server.DAL.KnpDocuments
{
    public static class DbConstants
    {
        public static string GetDeclarationVersionsProcedure = "P$GET_VERSIONS";
        public static string GetClaimsByDeclarationProcedure = "P$GET_CLAIMS_BY_DECLARATION";
        public static string GetExplainsByDeclarationProcedure = "P$GET_EXPLAINS_BY_DECLARATION";
        public static string GetActsProcedure = "P$GET_ACTS_BY_DECLARATION";
        public static string GetDecisionsProcedure = "P$GET_DECISIONS_BY_DECLARATION";

        public static string InnDeclarantParameter = "pInnDeclarant";
        public static string InnContractorParameter = "pInnContractor";
        public static string KppEffectiveParameter = "pKppEffective";
        public static string PeriodEffectiveParameter = "pPeriodEffective";
        public static string FiscalYearParameter = "pYear";
        public static string CursorParameter = "pCursor";

        public static string DeclarationPackage = "PAC$DECLARATION";
        public static string DocumentPackage = "PAC$DOCUMENT";
        public static string KnpResultPackage = "PAC$KNP_RESULT_DOCUMENTS";
    }
}
