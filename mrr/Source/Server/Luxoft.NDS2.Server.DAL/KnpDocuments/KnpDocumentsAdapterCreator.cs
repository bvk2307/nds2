﻿using Luxoft.NDS2.Server.Common.Adapters.KnpDocuments;

namespace Luxoft.NDS2.Server.DAL.KnpDocuments
{
    public static class KnpDocumentsAdapterCreator
    {
        public static IKnpDocumentsDeclarationAdapter KnpDocumentsDeclarationAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new KnpDocumentsDeclarationAdapter(serviceProvider,  connection);
        }

        public static IKnpDocumentsClaimAdapter KnpDocumentsClaimAdapter(
           IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new KnpDocumentsClaimAdapter(serviceProvider, connection);
        }

        public static IKnpDocumentsExplainAdapter KnpDocumentsExplainAdapter(
         IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new KnpDocumentsExplainAdapter(serviceProvider, connection);
        }

        public static IKnpDocumentsActAdapter KnpDocumentsActAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new KnpDocumentsActAdapter(serviceProvider, connection);
        }

        public static IKnpDocumentsDecisionAdapter KnpDocumentsDecisionAdapter(
          IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new KnpDocumentsDecisionAdapter(serviceProvider, connection);
        }
    }
}
