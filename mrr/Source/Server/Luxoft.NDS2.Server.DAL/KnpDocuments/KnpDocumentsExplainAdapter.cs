﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters.KnpDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder;
using System.Collections.Generic;
using System.Linq;


namespace Luxoft.NDS2.Server.DAL.KnpDocuments
{
    internal class KnpDocumentsExplainAdapter : IKnpDocumentsExplainAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public KnpDocumentsExplainAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public List<ClaimExplain>SelectByDeclaration(
          string innDeclarant,
          string innContractor,
          string kppEffective,
          int periodEffective,
          int fiscalYear)
        {
            var sqlExecuter =
              new ListCommandExecuter<ClaimExplain>(
                  new GenericDataMapper<ClaimExplain>(),
                  _service,
                  _connection);
            var commandBuilder = new GetExplainsByDeclarationCommand(innDeclarant, innContractor, kppEffective, periodEffective, fiscalYear);
            return sqlExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}