﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Server.Common.Adapters.KnpDocuments;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.KnpDocuments.CommandBuilder;
using System.Collections.Generic;
using System.Linq;


namespace Luxoft.NDS2.Server.DAL.KnpDocuments
{
    internal class KnpDocumentsDecisionAdapter : IKnpDocumentsDecisionAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public KnpDocumentsDecisionAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public List<SeodKnpDocument> SelectByDeclaration(
            string innDeclarant,
            string innContractor,
            string kppEffective,
            int periodEffective,
            int fiscalYear)
        {
            var sqlExecuter =
             new ListCommandExecuter<SeodKnpDocument>(
                 new GenericDataMapper<SeodKnpDocument>(),
                 _service,
                 _connection);
            var commandBuilder = new GetDecisionsByDeclarationCommand(innDeclarant, innContractor, kppEffective, periodEffective, fiscalYear);
            return sqlExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}