﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    internal abstract class SearchDataAdapterBase<TDto> : ISearchAdapter<TDto>
        where TDto : class
    {
        private readonly IServiceProvider _service;

        protected const string ViewAlias = "v";

        [Obsolete("Используем конструктор без ConnectionFactoryBase")]
        protected SearchDataAdapterBase(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
        }

        protected SearchDataAdapterBase(IServiceProvider service)
        {
            _service = service;
        }

        public virtual IEnumerable<TDto> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> sortBy, uint rowsToSkip, uint rowsToTake)
        {
            return new ListCommandExecuter<TDto>(
                DataMapper(),
                _service)
                .TryExecute(
                    new PageSearchQueryCommandBuilder(
                        string.Format("select * from {0} {1} where {2} {3}", ViewName(), ViewAlias, "{0}", "{1}"),
                        filterBy,
                        sortBy,
                        PatternProvider(ViewAlias))
                    .Take((int)rowsToTake)
                    .Skip((int)rowsToSkip));
        }

        public virtual int Count(FilterExpressionBase filterBy)
        {
            return new CountCommandExecuter(_service)
                .TryExecute(
                    new GetCountCommandBuilder(
                            string.Format("{0} {1} where {2}", ViewName(), ViewAlias, "{0}"),
                            filterBy,
                            PatternProvider(ViewAlias)));
        }

        protected abstract IQueryPatternProvider PatternProvider(string viewAlias);

        protected abstract IDataMapper<TDto> DataMapper();

        protected abstract string ViewName();
    }
}
