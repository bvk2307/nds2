﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection
{
    public interface IDiscrepancySelectionAdpater
    {
        Dictionary<string, object> ExecuteSqlQuery(string queryText, List<OracleParameter> parameters);
        OperationResult ExecuteQuery(string queryText, List<OracleParameter> parameters);
        int GetJobStatusCode(string jobName, DateTime dtStartJob);
        List<SelectionProcess> GetSelectionsForProcess();
        IEnumerable<long> GetSelectionsIdentifiers(SelectionStatus status);
        List<SelectionProcess> GetAllSelections();
        int CountSelectionsInProcess();
        SelectionProcess GetSelection(long id);
    }
}
