﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Implementation
{
    internal class DiscrepancySelectionAdpater : BaseOracleTableAdapter, IDiscrepancySelectionAdpater
    {
        # region Константы

        private const string getJobStatusSql = @"
BEGIN SELECT r.status_code into :pStatusCode FROM dual
left outer join 
(
    SELECT rownum as row_index, z.* FROM 
    (
        SELECT DECODE(STATUS, 'SUCCEEDED', 1, 'FAILED', 2, 0) as status_code FROM USER_SCHEDULER_JOB_LOG
        WHERE JOB_NAME = :pJobName and LOG_DATE >= :pJobStart and Operation = 'RUN' order by LOG_ID desc
    ) z
) r on r.row_index = 1; END;";

        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CursorAlias = "pCursor";
        private const string SearchSelectionProcessSql = @"
select s.id, s.status_date 
       ,(case when sf.selection_id is not null then 1 else 0 end) as is_manual
       ,sf.filter as filter_manual
       ,'' as include_filter_auto
       ,'' as exclude_filter_auto
       ,s.status as status
from selection s
left join selection_filter sf on sf.selection_id = s.id
where status = 8
order by status_date";

        private const string SearchSelectionIdentifiersSql = @"select s.id from selection s where s.status = :pSelectionStatus";

        private const string SearchSelectionAllSql = @"
select s.id, s.status_date 
       ,(case when sf.selection_id is not null then 1 else 0 end) as is_manual
       ,sf.filter as filter_manual
       ,af.include_filter as include_filter_auto
       ,af.exclude_filter as exclude_filter_auto
       ,s.status as status
from selection s
left join selection_filter sf on sf.selection_id = s.id
left join autoselection_filter af on af.id = s.id
order by id";

        private const string SearchSelectionInProcessSql = @"
begin
    select 
    count(1) into :pCount
    from selection
    where status = 14; /*В процессе загрузки*/
end;";

        private const string SearchSelectionProcessOneSql = @"
select s.id, s.status_date 
       ,(case when sf.selection_id is not null then 1 else 0 end) as is_manual
       ,sf.filter as filter_manual
       ,asf.include_filter as include_filter_auto
       ,asf.exclude_filter as exclude_filter_auto
       ,s.status as status
from selection s
left join selection_filter sf on sf.selection_id = s.id
left join AUTOSELECTION_REGION asr on asr.selection_id = s.id
left join AUTOSELECTION_FILTER asf on asf.id = asr.filter_id
where s.id = :pSelectionId";
        # endregion

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса UserToRegionAdapter
        /// </summary>
        /// <param name="service"></param>
        public DiscrepancySelectionAdpater(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        public OperationResult ExecuteQuery(string queryText, List<OracleParameter> parameters)
        {
            var result = new OperationResult();

            try
            {
                Execute(queryText, CommandType.Text, parameters.ToArray());
                result.Status = ResultStatus.Success;
            }
            catch(Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.Message = ex.Message;
            }


            return result;
        }

        public Dictionary<string, object> ExecuteSqlQuery(string queryText, List<OracleParameter> parameters)
        {
            var result = Execute(queryText,
                CommandType.Text,
                parameters.ToArray());

            Dictionary<string, object> rets = new Dictionary<string, object>();
            foreach (OracleParameter item in parameters.Where(p=>p.Direction == ParameterDirection.Output))
            {
                rets.Add(item.ParameterName, result.Output[item.ParameterName]);
            }
            return rets;
        }

        public int GetJobStatusCode(string jobName, DateTime dtStartJob)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.VarcharIn("pJobName", jobName));
            parameters.Add(FormatCommandHelper.DateIn("pJobStart", dtStartJob));
            parameters.Add(FormatCommandHelper.NumericOut("pStatusCode"));

            var result = Execute(getJobStatusSql,
                CommandType.Text,
                parameters.ToArray());

            int statusCode = DataReaderExtension.ReadInt(result.Output["pStatusCode"]);
            return statusCode;
        }

        public List<SelectionProcess> GetSelectionsForProcess()
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<SelectionProcess>(
                string.Format(SearchSQLCommandPattern, SearchSelectionProcessSql),
                CommandType.Text,
                parameters.ToArray(),
                BuildSelectionProcess);

            return data;
        }

        public IEnumerable<long> GetSelectionsIdentifiers(SelectionStatus status)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.DecimalIn("pSelectionStatus", (int)status));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<long>(
                string.Format(SearchSQLCommandPattern, SearchSelectionIdentifiersSql),
                CommandType.Text,
                parameters.ToArray(),
                (reader) => { return reader.ReadInt64("id"); }
            );

            return data;
        }

        public List<SelectionProcess> GetAllSelections()
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<SelectionProcess>(
                string.Format(SearchSQLCommandPattern, SearchSelectionAllSql),
                CommandType.Text,
                parameters.ToArray(),
                BuildSelectionProcess);

            return data;
        }

        public int CountSelectionsInProcess()
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericOut("pCount"));
            var outs = ExecuteSqlQuery(SearchSelectionInProcessSql, parameters);

            return Int32.Parse(outs["pCount"].ToString());
        }

        public SelectionProcess GetSelection(long id)
        {
            List<OracleParameter> parameters = new List<OracleParameter>();
            parameters.Add(FormatCommandHelper.NumericIn("pSelectionId", id));
            parameters.Add(FormatCommandHelper.Cursor(CursorAlias));

            var data = ExecuteList<SelectionProcess>(
                string.Format(SearchSQLCommandPattern, SearchSelectionProcessOneSql),
                CommandType.Text,
                parameters.ToArray(),
                BuildSelectionProcess);

            return data.Single();
        }

        private SelectionProcess BuildSelectionProcess(OracleDataReader reader)
        {
            SelectionProcess obj = new SelectionProcess();
            obj.Id = reader.ReadInt64("id");
            obj.StatusDate = reader.ReadDateTime("status_date");

            // фильтр ручной выборки
            obj.FilterManualContent = reader.ReadString("filter_manual");
            //TODO этот устаревший филльтр нужен только для конвертации (Build <- All <- Converter)
            var selectionFilterResult = SelectionFilter.Deserialize(obj.FilterManualContent);
            obj.FilterManualVersionOne = selectionFilterResult.SelectionFilter;
            var filterResult = FilterHelper.Deserialize(obj.FilterManualContent);
            obj.FilterManual = filterResult.Filter;

            obj.FilterManualVersion = SelectionFilterVersion.Unknow;
            if (selectionFilterResult.IsSuccess)
                obj.FilterManualVersion = SelectionFilterVersion.VersionOne;
            if (filterResult.IsSuccess)
                obj.FilterManualVersion = SelectionFilterVersion.VersionTwo;

            // фильтры авто-выборок
            string includeFilterRaw = reader.ReadString("include_filter_auto");
            if (!string.IsNullOrWhiteSpace(includeFilterRaw))
            {
                obj.IncludeFilterAutoVersionOne = DeserializeAutoFilterVersionOne(includeFilterRaw);
                obj.FilterAutoIncludeVersion = SelectionFilterVersion.Unknow;

                if (obj.IncludeFilterAutoVersionOne != null)
                    obj.FilterAutoIncludeVersion = SelectionFilterVersion.VersionOne;

                var filterAutoIncludeResult = FilterHelper.Deserialize(includeFilterRaw);
                obj.IncludeFilterAuto = filterAutoIncludeResult.Filter;

                if (filterAutoIncludeResult.IsSuccess)
                    obj.FilterAutoIncludeVersion = SelectionFilterVersion.VersionTwo;
            }

            string excludeFilterRaw = reader.ReadString("exclude_filter_auto");
            if (!string.IsNullOrWhiteSpace(includeFilterRaw))
            {
                obj.ExcludeFilterAutoVersionOne = DeserializeAutoFilterVersionOne(excludeFilterRaw);
                obj.FilterAutoExcludeVersion = SelectionFilterVersion.Unknow;

                if (obj.ExcludeFilterAutoVersionOne != null)
                    obj.FilterAutoExcludeVersion = SelectionFilterVersion.VersionOne;

                var filterAutoExcludeResult = FilterHelper.Deserialize(excludeFilterRaw);
                obj.ExcludeFilterAuto = filterAutoExcludeResult.Filter;

                if (filterAutoExcludeResult.IsSuccess)
                    obj.FilterAutoExcludeVersion = SelectionFilterVersion.VersionTwo;
            }

            obj.IsManual = reader.ReadBoolean("is_manual");
            obj.Status = (SelectionStatus)reader.ReadInt("status");
            return obj;
        }

        private List<GroupFilter> DeserializeAutoFilterVersionOne(string filterRaw)
        {
            List<GroupFilter> ret = null;
            if (!string.IsNullOrWhiteSpace(filterRaw))
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<GroupFilter>));
                try
                {
                    using (TextReader textReader = new StringReader(filterRaw))
                    {
                       ret = ser.Deserialize(textReader) as List<GroupFilter>;
                    }
                }
                catch (Exception) { }
            }
            return ret;
        }
    }
}
