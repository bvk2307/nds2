﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterVersionCreator
    {
        public static Parameter CreateParameter(FilterVersionCreateParameters creationParameter)
        {
            var parameter = new Parameter();
            var parameterValues = new List<ParameterValue>();
            if (creationParameter.Operator == ColumnFilterOperator.Beetween)
            {
                parameterValues.Add(new ParameterValue
                {
                    ValueFrom = creationParameter.ValueOne,
                    ValueTo = creationParameter.ValueTwo
                });
            }
            else
                parameterValues.Add(new ParameterValue() { Value = creationParameter.ValueOne });
        
            parameter.Operator = creationParameter.Operator;
            parameter.Values = parameterValues.ToArray();
            parameter.IsEnabled = creationParameter.IsEnabled;
            parameter.AttributeId = creationParameter.AttributeId;

            return parameter;
        }

        public static Parameter CreateParameterList(FilterVersionCreateParameters creationParameter)
        {
            var parameter = new Parameter();
            var parameterValues = new List<ParameterValue>();
            foreach (var item in creationParameter.Values)
                parameterValues.Add(new ParameterValue() { Value = item });
            parameter.Operator = creationParameter.Operator;
            parameter.Values = parameterValues.ToArray();
            parameter.IsEnabled = creationParameter.IsEnabled;
            parameter.AttributeId = creationParameter.AttributeId;

            return parameter;
        }

        public static Filter CreateFilter()
        {
            return new Filter();
        }

        public static ParameterGroup CreateParameterGroup()
        {
            return new ParameterGroup();
        }
    }
}
