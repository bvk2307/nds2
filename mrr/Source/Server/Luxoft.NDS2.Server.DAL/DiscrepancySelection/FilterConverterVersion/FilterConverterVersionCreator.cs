﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterConverterVersionCreator
    {
        public static FilterConverterVersionManual CreateFilterConverterManual(List<EditorParameter> editorParameters)
        {
            return new FilterConverterVersionManual(editorParameters);
        }

        public static FilterConverterVersionAuto CreateFilterConverterAuto(List<EditorParameter> editorParameters)
        {
            return new FilterConverterVersionAuto(editorParameters);
        }
    }
}
