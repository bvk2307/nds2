﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;
using DiscrepancyAggregateTableColumns = Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression.DiscrepancyAggregateTableColumns;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterConverterVersionManual : FilterConverterVersionBase
    {
        private readonly List<string> _excludeList = new List<string>
        {
            DiscrepancyAggregateTableColumns.ExcludeSide1Inn,
            DiscrepancyAggregateTableColumns.ExcludeSide2Inn
        };

        private readonly List<string> _listWithOperator = new List<string>
        {
            DiscrepancyAggregateTableColumns.Side1OperationCodes,
            DiscrepancyAggregateTableColumns.Side2OperationCodes
        };

        public FilterConverterVersionManual(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        public Filter Convert(SelectionFilter selectionFilter)
        {
            return ConvertGroup(selectionFilter.GroupsFilters);
        }

        protected Filter ConvertGroup(List<GroupFilter> groupFilters)
        {
            var filter = FilterVersionCreator.CreateFilter();
            var parameterGroups = new List<ParameterGroup>();

            foreach (var groupFilter in groupFilters)
                parameterGroups.Add(ConvertOneGroup(groupFilter));

            filter.Groups = parameterGroups.ToArray();
            return filter;
        }

        protected ParameterGroup ConvertOneGroup(GroupFilter groupFilter)
        {
            var parameterGroup = FilterVersionCreator.CreateParameterGroup();
            var parameters = new List<Parameter>();

            var aggregateSumNds = new List<string>() 
            { 
                 DiscrepancyAggregateTableColumns.Side1MarkRule,
                 DiscrepancyAggregateTableColumns.Side1NDSAmount,
                 DiscrepancyAggregateTableColumns.Side2MarkRule,
                 DiscrepancyAggregateTableColumns.Side2NDSAmount
            };

            var criteriesSumNds = groupFilter.Filters
                .Where(p => aggregateSumNds.Contains(p.FirInternalName)).ToList();
            var criteriesOthers = groupFilter.Filters
                .Where(p => !aggregateSumNds.Contains(p.FirInternalName)).ToList();

            FillExpressionSumNds(parameters, criteriesSumNds, DiscrepancyAggregateTableColumns.Side1MarkRule, DiscrepancyAggregateTableColumns.Side1NDSAmount);
            FillExpressionSumNds(parameters, criteriesSumNds, DiscrepancyAggregateTableColumns.Side2MarkRule, DiscrepancyAggregateTableColumns.Side2NDSAmount);

            foreach (var filterCriteria in criteriesOthers)
            {
                if (string.IsNullOrWhiteSpace(filterCriteria.FirInternalName) || filterCriteria.Values.Count == 0)
                    continue;

                if (filterCriteria.Values.Count == 1)
                    FillExpressionForSingle(parameters, filterCriteria);
                else
                    FillExpressionForList(parameters, filterCriteria);
            }
            parameterGroup.IsEnabled = groupFilter.IsActive;
            parameterGroup.Parameters = parameters.ToArray();
            return parameterGroup;
        }

        private void FillExpressionSumNds(List<Parameter> parameters, List<FilterCriteria> criteries, string nameMarkRule, string nameSumNds)
        {
            var criteriaMarkRule = criteries.Where(p => p.FirInternalName == nameMarkRule).FirstOrDefault();
            var criteriaSumNds = criteries.Where(p => p.FirInternalName == nameSumNds).SingleOrDefault();
            if (criteriaMarkRule != null && criteriaSumNds != null)
            {
                var filterElementSumNds = criteriaSumNds.Values.First();
                var filterElementMarkRule = criteriaMarkRule.Values.First();
                var parameter = CreateParameter(
                    nameSumNds,
                    ConvertComparisionOperator(filterElementSumNds.Operator),
                    criteriaSumNds.IsActive && criteriaMarkRule.IsActive,
                    filterElementSumNds.ValueOne,
                    filterElementSumNds.ValueTwo,
                    filterElementMarkRule.ValueOne);
                parameters.Add(parameter);
            }
            if (criteriaMarkRule == null && criteriaSumNds != null)
            {
                var filterElementSumNds = criteriaSumNds.Values.First();
                var parameter = CreateParameter(
                    nameSumNds,
                    ConvertComparisionOperator(filterElementSumNds.Operator),
                    criteriaSumNds.IsActive,
                    filterElementSumNds.ValueOne,
                    filterElementSumNds.ValueTwo,
                    ((int)DeclarationType.ToPay).ToString());
                parameters.Add(parameter);
                parameter = CreateParameter(
                    nameSumNds,
                    ConvertComparisionOperator(filterElementSumNds.Operator),
                    criteriaSumNds.IsActive,
                    filterElementSumNds.ValueOne,
                    filterElementSumNds.ValueTwo,
                    ((int)DeclarationType.ToCharge).ToString());
                parameters.Add(parameter);
            }
            if (criteriaMarkRule != null && criteriaSumNds == null)
            {
                var filterElementMarkRule = criteriaMarkRule.Values.First();
                var parameter = CreateParameter(
                    nameSumNds,
                    ConvertComparisionOperator(filterElementMarkRule.Operator),
                    criteriaMarkRule.IsActive,
                    filterElementMarkRule.ValueOne,
                    null);
                parameters.Add(parameter);
            }
        }

        private void FillExpressionForList(List<Parameter> parameters, FilterCriteria filterCriteria)
        {
            var listValues =
                filterCriteria.Values.Select<FilterElement, string>(
                    filterElement => ConvertValue(filterCriteria.InternalName, filterElement.ValueOne))
                    .ToList();

            Parameter parameter;
            if (_listWithOperator.Contains(filterCriteria.InternalName))
            {
                var op = filterCriteria.Values.First().Operator;
                parameter = CreateParameterList(filterCriteria.FirInternalName, ConvertComparisionOperator(op), filterCriteria.IsActive, listValues);
            }
            else
            {
                parameter = _excludeList.Contains(filterCriteria.InternalName)
                    ? CreateParameterNotInList(filterCriteria.FirInternalName, filterCriteria.IsActive, listValues)
                    : CreateParameterInList(filterCriteria.FirInternalName, filterCriteria.IsActive, listValues);
            }

            parameters.Add(parameter);
        }

        private string ConvertValue(string name, string value)
        {
            if (name == "side1operationCodes" || name == "side2operationCodes")
            {
                long n;
                if (long.TryParse(value, out n))
                    value = ((long)Math.Log(n, 2) + 1).ToString();
            }
            return value;
        }

        private void FillExpressionForSingle(List<Parameter> parameters, FilterCriteria filterCriteria)
        {
            var filterElement = filterCriteria.Values.First();

            var parameter = _excludeList.Contains(filterCriteria.InternalName)
                ? CreateParameter(
                    filterCriteria.FirInternalName,
                    ColumnFilterOperator.NotEquals,
                    filterCriteria.IsActive,
                    ConvertValue(filterCriteria.FirInternalName, filterElement.ValueOne),
                    ConvertValue(filterCriteria.FirInternalName, filterElement.ValueTwo))
                : CreateParameter(
                    filterCriteria.FirInternalName,
                    ConvertComparisionOperator(filterElement.Operator),
                    filterCriteria.IsActive,
                    ConvertValue(filterCriteria.FirInternalName, filterElement.ValueOne),
                    ConvertValue(filterCriteria.FirInternalName, filterElement.ValueTwo));

            parameters.Add(parameter);
        }
    }
}
