﻿using System.Collections.Generic;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterVersionCreateParameters
    {
        public string AggregateName { get; set; }
        public ColumnFilterOperator Operator { get; set; }
        public string ValueOne { get; set; }
        public string ValueTwo { get; set; }
        public string Value { get; set; }
        public List<string> Values { get; set; }
        public bool IsEnabled { get; set; }
        public int AttributeId { get; set; }
    }
}
