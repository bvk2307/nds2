﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class EditorParameterExtend
    {
        public EditorParameter EditorParameter { get; set; }
        public int Rank { get; set; }
    }
}
