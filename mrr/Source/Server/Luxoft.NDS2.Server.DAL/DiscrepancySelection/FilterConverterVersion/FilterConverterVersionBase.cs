﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;
using SelectionFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.FilterElement.ComparisonOperations;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterConverterVersionBase
    {
        private readonly Dictionary<SelectionFilterOperator, ColumnFilterOperator> _dictConvertOperators =
            new Dictionary<SelectionFilterOperator, ColumnFilterOperator>();

        protected readonly List<EditorParameter> _editorParameters = new List<EditorParameter>();

        protected Dictionary<string, string> _aggregateNameMapping = new Dictionary<string, string>();

        public FilterConverterVersionBase(List<EditorParameter> editorParameters)
        {
            CreateDictConvertOperator();
            CreateAggregateNameMapping();
            _editorParameters.AddRange(editorParameters);
        }

        private void CreateDictConvertOperator()
        {
            _dictConvertOperators.Clear();
            _dictConvertOperators.Add(SelectionFilterOperator.Contains, ColumnFilterOperator.Contains);
            _dictConvertOperators.Add(SelectionFilterOperator.NotContains, ColumnFilterOperator.NotContains);
            _dictConvertOperators.Add(SelectionFilterOperator.OneOf, ColumnFilterOperator.OneOf);
            _dictConvertOperators.Add(SelectionFilterOperator.EndWith, ColumnFilterOperator.EndWith);
            _dictConvertOperators.Add(SelectionFilterOperator.Equals, ColumnFilterOperator.Equals);
            _dictConvertOperators.Add(SelectionFilterOperator.GE, ColumnFilterOperator.GE);
            _dictConvertOperators.Add(SelectionFilterOperator.GT, ColumnFilterOperator.GT);
            _dictConvertOperators.Add(SelectionFilterOperator.LE, ColumnFilterOperator.LE);
            _dictConvertOperators.Add(SelectionFilterOperator.LT, ColumnFilterOperator.LT);
            _dictConvertOperators.Add(SelectionFilterOperator.NotEquals, ColumnFilterOperator.NotEquals);
            _dictConvertOperators.Add(SelectionFilterOperator.StartWith, ColumnFilterOperator.StartWith);
            _dictConvertOperators.Add(SelectionFilterOperator.Between, ColumnFilterOperator.Beetween);
            _dictConvertOperators.Add(SelectionFilterOperator.InSet, ColumnFilterOperator.InList);
        }

        private void CreateAggregateNameMapping()
        {
            _aggregateNameMapping.Add("inn", "decl_inn");
            _aggregateNameMapping.Add("period", "period_full");
            _aggregateNameMapping.Add("REGION", "region_code");
            _aggregateNameMapping.Add("IFNS_NUMBER", "sono_code");
        }

        private string GetAggregateNameForConvert(string aggregateName)
        {
            string ret = aggregateName;
            if (_aggregateNameMapping.ContainsKey(aggregateName))
                ret = _aggregateNameMapping[aggregateName];
            return ret;
        }

        private int GetEditorParameterId(string aggregateName)
        {
            string aggregateNameForConvert = GetAggregateNameForConvert(aggregateName);
            var editorParameterExtend = _editorParameters
                .Where(p => p.AggregateName == aggregateNameForConvert)
                .Select(p => new EditorParameterExtend()
                {
                    EditorParameter = p,
                    Rank = p.Area == EditorArea.BuyerSide ? 1 : p.Area == EditorArea.SellerSide ? 2 : p.Area == EditorArea.DiscrepancyParameters ? 3 : 4
                })
                .OrderBy(p => p.Rank)
                .FirstOrDefault();
            if (editorParameterExtend != null)
                return editorParameterExtend.EditorParameter.Id;
            else
                throw new KeyNotFoundException();
        }

        protected Parameter CreateParameter(string name, ColumnFilterOperator filterOperator, bool isEnabled, string valueOne, string valueTwo)
        {
            var creationParameter = new FilterVersionCreateParameters
            {
                AggregateName = name,
                Operator = filterOperator,
                ValueOne = valueOne,
                ValueTwo = valueTwo,
                IsEnabled = isEnabled,
                AttributeId = GetEditorParameterId(name)
            };
            if (name == "period")
                creationParameter.ValueOne = "2015" + creationParameter.ValueOne;
            return FilterVersionCreator.CreateParameter(creationParameter);
        }

        protected Parameter CreateParameter(string name, ColumnFilterOperator filterOperator, bool isEnabled, string valueOne, string valueTwo, string value)
        {
            var creationParameter = new FilterVersionCreateParameters
            {
                AggregateName = name,
                Operator = filterOperator,
                ValueOne = valueOne,
                ValueTwo = valueTwo,
                Value = value,
                IsEnabled = isEnabled,
                AttributeId = GetEditorParameterId(name)
            };
            if (name == "period")
                creationParameter.Value = "2015" + creationParameter.Value;
            return FilterVersionCreator.CreateParameter(creationParameter);
        }

        protected Parameter CreateParameterInList(string name, bool isEnabled, List<string> values)
        {
            return CreateParameterList(name, ColumnFilterOperator.InList, isEnabled, values);
        }

        protected Parameter CreateParameterNotInList(string name, bool isEnabled, List<string> values)
        {
            return CreateParameterList(name, ColumnFilterOperator.NotInList, isEnabled, values);
        }

        protected Parameter CreateParameterList(string name, ColumnFilterOperator filterOperator, bool isEnabled, List<string> values)
        {
            var creationParameter = new FilterVersionCreateParameters
            {
                AggregateName = name,
                Operator = filterOperator,
                Values = values,
                IsEnabled = isEnabled,
                AttributeId = GetEditorParameterId(name)
            };
            if (name == "period")
                creationParameter.Values = creationParameter.Values.Select(x => "2015" + x).ToList();
            return FilterVersionCreator.CreateParameterList(creationParameter);
        }

        protected ColumnFilterOperator ConvertComparisionOperator(
            SelectionFilterOperator comparisionOperator)
        {
            return _dictConvertOperators.ContainsKey(comparisionOperator)
                ? _dictConvertOperators[comparisionOperator]
                : ColumnFilterOperator.NotDefinedOperator;
        }
    }
}
