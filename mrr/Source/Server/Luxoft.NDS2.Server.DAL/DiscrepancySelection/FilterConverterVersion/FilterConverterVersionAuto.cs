﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion
{
    public class FilterConverterVersionAuto : FilterConverterVersionBase
    {
        public FilterConverterVersionAuto(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        public Filter Convert(List<GroupFilter> groupFilters)
        {
            return ConvertGroup(groupFilters);
        }

        protected Filter ConvertGroup(List<GroupFilter> groupFilters)
        {
            var filter = FilterVersionCreator.CreateFilter();
            var parameterGroups = new List<ParameterGroup>();

            foreach (var groupFilter in groupFilters)
                parameterGroups.Add(ConvertOneGroup(groupFilter));

            filter.Groups = parameterGroups.ToArray();
            return filter;
        }

        protected ParameterGroup ConvertOneGroup(GroupFilter groupFilter)
        {
            var parameterGroup = FilterVersionCreator.CreateParameterGroup();
            var parameters = new List<Parameter>();

            foreach (var filterCriteria in groupFilter.Filters)
            {
                if (string.IsNullOrWhiteSpace(filterCriteria.InternalName) || filterCriteria.Values.Count == 0)
                    continue;

                if (filterCriteria.Values.Count == 1)
                    FillExpressionForSingle(parameters, filterCriteria);
                else
                    FillExpressionForList(parameters, filterCriteria);
            }
            parameterGroup.IsEnabled = groupFilter.IsActive;
            parameterGroup.Parameters = parameters.ToArray();
            return parameterGroup;
        }

        private void FillExpressionForList(List<Parameter> parameters, FilterCriteria filterCriteria)
        {
            var listValues =
                filterCriteria.Values.Select<FilterElement, string>(filterElement => filterElement.ValueOne)
                    .ToList();

            var parameter = CreateParameterInList(filterCriteria.InternalName, filterCriteria.IsActive, listValues);

            parameters.Add(parameter);
        }

        private void FillExpressionForSingle(List<Parameter> parameters, FilterCriteria filterCriteria)
        {
            var filterElement = filterCriteria.Values.First();

            var parameter = CreateParameter(filterCriteria.InternalName, ConvertComparisionOperator(filterElement.Operator), filterCriteria.IsActive, filterElement.ValueOne, filterElement.ValueTwo);

            parameters.Add(parameter);
        }
    }
}
