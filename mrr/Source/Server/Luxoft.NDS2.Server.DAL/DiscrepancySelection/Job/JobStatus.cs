﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job
{
    public enum JobStatus
    {
        Unknonw = 0,
        Succeeded = 1,
        Failed = 2
    }
}
