﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job
{
    public class JobStatusResult
    {
        public JobStatus Status { get; set; }
        public string Operation { get; set; }
    }
}
