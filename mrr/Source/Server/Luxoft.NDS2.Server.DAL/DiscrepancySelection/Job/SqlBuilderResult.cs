﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job
{
    public class SqlJobBuilderResult
    {
        public string SqlQueryText { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class SqlJobBodyResult
    {
        public string SqlQueryText { get; set; }
        public string SqlVariablesText { get; set; }
        public List<OracleParameter> Parameters { get; set; }
    }
}
