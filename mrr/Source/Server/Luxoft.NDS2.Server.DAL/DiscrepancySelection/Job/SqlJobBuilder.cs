﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job
{
    public class SqlJobBuilder
    {
        public const string PDtJobStart = "pDtJobStart";

        public static SqlJobBuilderResult CreatePlSqlBlock(long Id, string sqlBody)
        {
            StringBuilder plSqlBlockBuilder = new StringBuilder();

            plSqlBlockBuilder.AppendLine("DECLARE");
            plSqlBlockBuilder.AppendLine("p_rowsCount number;");
            plSqlBlockBuilder.AppendLine("BEGIN");
            plSqlBlockBuilder.AppendLine(sqlBody);
            plSqlBlockBuilder.AppendLine("END;");

            string procedureShortName = string.Format("P$SELECTION_MANAGE_{0}", Id);
            string procedureName = string.Format("NDS2_MRR_USER.P$SELECTION_MANAGE_{0}", Id);


            return new SqlJobBuilderResult() { Name = procedureName, ShortName = procedureShortName, SqlQueryText = plSqlBlockBuilder.ToString() };
        }


        public static SqlJobBuilderResult CreateProcedure(long Id, string sqlBody, string queryVariables, List<OracleParameter> oraParameters)
        {
            string procedureShortName = string.Format("P$SELECTION_MANAGE_{0}", Id);
            string procedureName = string.Format("NDS2_MRR_USER.P$SELECTION_MANAGE_{0}", Id);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("CREATE OR REPLACE PROCEDURE {0}", procedureName));
            sb.AppendLine("(");
            bool isFirst = true;
            foreach (OracleParameter item in oraParameters)
            {
                if (isFirst)
                    isFirst = false;
                else
                    sb.Append(",");
                sb.AppendLine(string.Format("{0} {1} {2}", item.ParameterName, GetDirectionString(item), GetOracleTypeString(item)));
            }
            sb.AppendLine(")");
            sb.AppendLine("IS");
            sb.AppendLine(queryVariables);
            sb.AppendLine("BEGIN");
            sb.AppendLine(sqlBody);
            sb.AppendLine(string.Format("END {0};", procedureShortName));

            return new SqlJobBuilderResult() { Name = procedureName, ShortName = procedureShortName, SqlQueryText = sb.ToString() };
        }

        public static SqlJobBuilderResult CreateProgramm(long Id, string procedureName, List<OracleParameter> oraParameters)
        {
            string programmShortName = string.Format("PG$SELECTION_MANAGE_{0}", Id);
            string programmName = string.Format("NDS2_MRR_USER.{0}", programmShortName);

            string createProgrammTemplate = @"
DBMS_SCHEDULER.CREATE_PROGRAM (
   program_name          => '{0}',
   program_type          => 'STORED_PROCEDURE',
   program_action        => 'NDS2_MRR_USER.{1}',
   number_of_arguments   => {2},
   enabled               => false,
   comments              => 'программа формирования выборки');";

            string argumentTemplate = @"
dbms_scheduler.DEFINE_PROGRAM_ARGUMENT(
program_name=>'{0}',
argument_position=>{2},
argument_type=>'{1}',
DEFAULT_VALUE=>'');";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BEGIN");
            sb.AppendLine(string.Format(createProgrammTemplate, programmName, procedureName, oraParameters.Count()));
            sb.AppendLine();
            int arg_position = 1;
            foreach (OracleParameter item in oraParameters)
            {
                sb.AppendLine(string.Format(argumentTemplate, programmName, GetOracleTypeString(item), arg_position));
                sb.AppendLine();
                arg_position++;
            }
            sb.AppendLine(string.Format("dbms_scheduler.enable('{0}');", programmName));
            sb.AppendLine("END;");

            return new SqlJobBuilderResult() { Name = programmName, ShortName = programmShortName, SqlQueryText = sb.ToString() };
        }

        public static SqlJobBuilderResult CreateJob(long Id, string programmName, List<OracleParameter> oraParameters)
        {
            string jobShortName = string.Format("J$SELECTION_MANAGE_{0}", Id);
            string jobName = string.Format("NDS2_MRR_USER.{0}", jobShortName);

            string createJobTemplate = @"
    sys.dbms_scheduler.create_job(job_name => '{0}', 
    program_name => '{1}', 
    start_date => sysdate + INTERVAL '1' SECOND, 
    repeat_interval => null, 
    end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
    job_class => 'DEFAULT_JOB_CLASS', 
    enabled => false, 
    auto_drop => true, 
    comments => 'джоб формирования выборки');";

            string argumentTemplate = @"
    sys.dbms_scheduler.SET_JOB_ANYDATA_VALUE(
    job_name                => '{0}',
    argument_position       => {3},
    argument_value          => {2}(:{1}));";

            string jobEnabledTemplate = @"sys.dbms_scheduler.enable('{0}');";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("begin");
            sb.AppendLine(string.Format(createJobTemplate, jobName, programmName));
            sb.AppendLine();
            int arg_position = 1;
            foreach (OracleParameter item in oraParameters)
            {
                sb.AppendLine(string.Format(argumentTemplate, jobName, item.ParameterName, GetAnyDataConvertor(item), arg_position));
                sb.AppendLine();
                arg_position++;
            }

            sb.AppendLine(string.Format("select sysdate into :{0} from dual;", PDtJobStart));
            oraParameters.Add(FormatCommandHelper.DateOut(PDtJobStart));

            sb.AppendLine(string.Format(jobEnabledTemplate, jobName));
            sb.AppendLine("end;");
            return new SqlJobBuilderResult() { Name = jobName, ShortName = jobShortName, SqlQueryText = sb.ToString() };
        }

        public static SqlJobBuilderResult CreateLightWeightJob(long Id, string programmName, List<OracleParameter> oraParameters)
        {
            string jobShortName = string.Format("J$SELECTION_MANAGE_{0}", Id);
            string jobName = string.Format("NDS2_MRR_USER.{0}", jobShortName);

            string createJobTemplate = @"
    sys.dbms_scheduler.create_job (
      job_name        => '{0}',
      program_name    => '{1}',
      job_style       => 'LIGHTWEIGHT',
      enabled         => false);";

            string argumentTemplate = @"
    sys.dbms_scheduler.SET_JOB_ANYDATA_VALUE(
    job_name                => '{0}',
    argument_position       => {3},
    argument_value          => {2}(:{1}));";

            string jobEnabledTemplate = @"sys.dbms_scheduler.enable('{0}');";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("begin");
            sb.AppendLine();
            sb.AppendLine(string.Format(createJobTemplate, jobName, programmName));
            sb.AppendLine();
            int arg_position = 1;
            foreach (OracleParameter item in oraParameters)
            {
                sb.AppendLine(string.Format(argumentTemplate, jobName, item.ParameterName, GetAnyDataConvertor(item), arg_position));
                sb.AppendLine();
                arg_position++;
            }

            sb.AppendLine(string.Format("select sysdate into :{0} from dual;", PDtJobStart));
            oraParameters.Add(FormatCommandHelper.DateOut(PDtJobStart));

            sb.AppendLine(string.Format(jobEnabledTemplate, jobName));
            sb.AppendLine("end;");
            return new SqlJobBuilderResult() { Name = jobName, ShortName = jobShortName, SqlQueryText = sb.ToString() };
        }

        private static string GetOracleTypeString(OracleParameter parameter)
        {
            Dictionary<DbType, string> dictOraDbTypes = new Dictionary<DbType, string>();
            dictOraDbTypes.Add(DbType.Decimal, "NUMBER");
            dictOraDbTypes.Add(DbType.Int16, "NUMBER");
            dictOraDbTypes.Add(DbType.Int32, "NUMBER");
            dictOraDbTypes.Add(DbType.Int64, "NUMBER");
            dictOraDbTypes.Add(DbType.UInt16, "NUMBER");
            dictOraDbTypes.Add(DbType.UInt32, "NUMBER");
            dictOraDbTypes.Add(DbType.UInt64, "NUMBER");
            dictOraDbTypes.Add(DbType.String, "VARCHAR2");
            dictOraDbTypes.Add(DbType.AnsiString, "VARCHAR2");
            dictOraDbTypes.Add(DbType.StringFixedLength, "VARCHAR2");
            dictOraDbTypes.Add(DbType.Date, "DATE");
            dictOraDbTypes.Add(DbType.DateTime, "DATE");
            dictOraDbTypes.Add(DbType.DateTime2, "DATE");
            dictOraDbTypes.Add(DbType.Time, "DATE");

            string ret = String.Empty;
            if (dictOraDbTypes.ContainsKey(parameter.DbType))
            {
                ret = dictOraDbTypes[parameter.DbType];
            }
            return ret;
        }

        private static string GetDirectionString(OracleParameter parameter)
        {
            string ret = String.Empty;

            Dictionary<System.Data.ParameterDirection, string> dictOraDirections = new Dictionary<System.Data.ParameterDirection, string>();
            dictOraDirections.Add(System.Data.ParameterDirection.Input, "IN");
            dictOraDirections.Add(System.Data.ParameterDirection.Output, "OUT");
            dictOraDirections.Add(System.Data.ParameterDirection.InputOutput, "IN");

            if (dictOraDirections.ContainsKey(parameter.Direction))
            {
                ret = dictOraDirections[parameter.Direction];
            }
            return ret;
        }

        private static string GetAnyDataConvertor(OracleParameter parameter)
        {
            Dictionary<DbType, string> dictOraDbTypes = new Dictionary<DbType, string>();
            dictOraDbTypes.Add(DbType.Decimal, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.Int16, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.Int32, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.Int64, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.UInt16, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.UInt32, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.UInt64, "ANYDATA.ConvertNumber");
            dictOraDbTypes.Add(DbType.String, "ANYDATA.ConvertVarchar");
            dictOraDbTypes.Add(DbType.AnsiString, "ANYDATA.ConvertVarchar");
            dictOraDbTypes.Add(DbType.StringFixedLength, "ANYDATA.ConvertVarchar");
            dictOraDbTypes.Add(DbType.Date, "ANYDATA.ConvertDate");
            dictOraDbTypes.Add(DbType.DateTime, "ANYDATA.ConvertDate");
            dictOraDbTypes.Add(DbType.DateTime2, "ANYDATA.ConvertDate");
            dictOraDbTypes.Add(DbType.Time, "ANYDATA.ConvertDate");

            string ret = String.Empty;
            if (dictOraDbTypes.ContainsKey(parameter.DbType))
            {
                ret = dictOraDbTypes[parameter.DbType];
            }
            return ret;
        }

        public static SqlJobBuilderResult DropJob(string jobName)
        {
            string dropJobTemplate = @"
            begin
            sys.dbms_scheduler.drop_job('{0}');
            exception when others then null;
            end;";

            string dropJobsql = string.Format(dropJobTemplate, jobName);

            return new SqlJobBuilderResult() { Name = jobName, SqlQueryText = dropJobsql };
        }

        public static SqlJobBuilderResult DropProgramm(string programmName)
        {
            string dropProgrammTemplate = @"
            begin
            DBMS_SCHEDULER.DROP_PROGRAM('{0}');
            exception when others then null;
            end;";

            string dropProgrammSql = string.Format(dropProgrammTemplate, programmName);

            return new SqlJobBuilderResult() { Name = programmName, SqlQueryText = dropProgrammSql };
        }

        public static SqlJobBuilderResult DropProcedure(string procedureName)
        {
            
            string dropProcedureTemplate = @"
            begin
            NDS2_MRR_USER.NDS2$SYS.Drop_object_if_exist('{0}'); 
            exception when others then null;
            end;";

            string dropProcedureSql = string.Format(dropProcedureTemplate, procedureName);

            return new SqlJobBuilderResult() { Name = procedureName, SqlQueryText = dropProcedureSql };
        }
    }
}
