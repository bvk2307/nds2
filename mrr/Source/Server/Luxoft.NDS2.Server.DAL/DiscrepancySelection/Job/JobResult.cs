﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job
{
    public class JobResult
    {
        public string ProcedureName { get; set; }
        public string ProcedureShortName { get; set; }
        public string ProgrammName { get; set; }
        public string ProgrammShortName { get; set; }
        public string JobName { get; set; }
        public string JobShortName { get; set; }
        public DateTime DtJobStart { get; set; }
    }
}
