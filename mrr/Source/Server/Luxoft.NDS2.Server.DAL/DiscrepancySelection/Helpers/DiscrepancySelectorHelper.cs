﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.Helpers
{
    public class DiscrepancySelectorHelper
    {
        public static object GetParameterValue(Dictionary<string, object> retParams, string parameterName)
        {
            object ret = null;
            if (retParams.ContainsKey(parameterName))
            {
                ret = retParams[parameterName];
            }
            return ret;
        }

        public static long ReadInt64(object value)
        {
            long ret = 0;
            long temp = 0;
            if (value != null && long.TryParse(value.ToString(), out temp))
            {
                ret = temp;
            }
            return ret;
        }

        public static DateTime ReadDateTime(object value)
        {
            DateTime ret = DateTime.MinValue;
            if (value != null)
            {
                Oracle.DataAccess.Types.OracleDate oracleDateTime = (Oracle.DataAccess.Types.OracleDate)value;
                ret = (DateTime)oracleDateTime.Value;
            }
            return ret;
        }

        public static string ReadString(object value)
        {
            string ret = String.Empty;
            if (value != null)
            {
                ret = value.ToString();
            }
            return ret;
        }
    }
}
