﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;
using System.Linq;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator;
using SelectionFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public class FilterConverterExpressionAuto : FilterConverterExpressionBase
    {
        public FilterConverterExpressionAuto(List<EditorParameter> editorParameters)
            : base(editorParameters) 
        {
        }

        public FilterExpressionBase Convert(Filter includeFilter, Filter excludeFilter,
            List<FilterCriteria> filterCriteries, List<AutoselectionInspectionPvpLimit> pvpLimits)
        {
            FilterExpressionBase filterExpressionInclude = null;
            FilterExpressionBase filterExpressionExclude = null;
            if (includeFilter != null)
                filterExpressionInclude = CreateOneFilterAutoSelection(includeFilter);
            else if (excludeFilter != null)
                filterExpressionExclude = CreateOneFilterAutoSelection(excludeFilter);

            var filterExpressionGroupUnion = FilterExpressionCreator.CreateUnion();

            var pvpGroups = pvpLimits.GroupBy(p => p.Pvp).ToList();
            foreach (IGrouping<decimal, AutoselectionInspectionPvpLimit> pvpGroupItem in pvpGroups)
            {
                var filterExpressionGroup = FilterExpressionCreator.CreateGroup();

                //------ значение PVP
                var fePvp = CreateFilterExpressionBase("pvp", ColumnFilterOperator.GreaterThan, pvpGroupItem.Key);
                filterExpressionGroup.WithExpression(fePvp);

                //------ список инспекций
                var listValues = pvpGroupItem.Select(pvpItem => pvpItem.InspectionCode).Cast<object>().ToList();

                var feInsp = FilterExpressionCreator.CreateInList("sono_code", listValues);
                filterExpressionGroup.WithExpression(feInsp);

                //------ Filter
                if (filterExpressionInclude != null)
                    filterExpressionGroup.WithExpression(filterExpressionInclude);
                else if (filterExpressionExclude != null)
                    filterExpressionGroup.WithExpression(FilterExpressionCreator.CreateNot(filterExpressionExclude));

                filterExpressionGroupUnion.WithExpression(filterExpressionGroup);
            }
            return filterExpressionGroupUnion;
        }

        private FilterExpressionBase CreateOneFilterAutoSelection(Filter filter)
        {
            var filterMainGroup = FilterExpressionCreator.CreateGroup();
            if (filter == null)
                return filterMainGroup;

            foreach (var parameterGroup in filter.Groups.Where(p => p.IsEnabled))
            {
                var filterExpressionGroup = FilterExpressionCreator.CreateGroup();
                foreach (var parameter in parameterGroup.Parameters.Where(p => p.IsEnabled))
                {
                    var editorParameter = GetEditorParametr(parameter.AttributeId);
                    if (editorParameter == null)
                        continue;
                    if (editorParameter != null &&
                        (string.IsNullOrWhiteSpace(editorParameter.AggregateName) || parameter.Values.Count() == 0))
                        continue;

                    if (parameter.Values.Count() == 1)
                        FillExpressionForSingle(filterExpressionGroup, parameter, editorParameter);
                    else if (parameter.Values.Count() > 1)
                        FillExpressionForList(filterExpressionGroup, parameter, editorParameter);
                }
                filterMainGroup.WithExpression(filterExpressionGroup);
            }
            return filterMainGroup;
        }

        private void FillExpressionForSingle(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            var parameterValue = parameter.Values.First();

            if (!FiltersAggregateNames.ContainsKey(editorParameter.AggregateName))
                return;

            if (parameter.Operator == SelectionFilterOperator.Beetween)
            {
                var filterExpressionFrom = CreateFilterExpressionBase(editorParameter.AggregateName, ColumnFilterOperator.GreaterThanOrEqualTo, parameterValue.ValueFrom);
                filterExpressionGroup.WithExpression(filterExpressionFrom);
                var filterExpressionTo = CreateFilterExpressionBase(editorParameter.AggregateName, ColumnFilterOperator.LessThanOrEqualTo, parameterValue.ValueTo);
                filterExpressionGroup.WithExpression(filterExpressionTo);
            }
            else
            {
                var filterExpressionOne = CreateFilterExpressionBase(editorParameter.AggregateName, ConvertComparisionOperator(parameter.Operator), parameterValue.Value);
                filterExpressionGroup.WithExpression(filterExpressionOne);
            }
        }

        private void FillExpressionForList(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            var listValues = parameter.Values.Select<ParameterValue, object>(parameterValue => parameterValue.Value).ToList();

            var filterExpressionList = FilterExpressionCreator.CreateInList(editorParameter.AggregateName, listValues);
            filterExpressionGroup.WithExpression(filterExpressionList);
        }
    }
}
