﻿using System.Collections.Generic;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public class FilterExpressionCreateParameters
    {
        public string FirInternalName { get; set; }
        public ColumnFilterOperator Operator { get; set; }
        public object Value { get; set; }
        public IEnumerable<object> ListValues { get; set; }
    }
}
