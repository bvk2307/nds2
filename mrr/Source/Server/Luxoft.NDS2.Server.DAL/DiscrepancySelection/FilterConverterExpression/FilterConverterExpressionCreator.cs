﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public class FilterConverterExpressionCreator
    {
        public static FilterConverterExpressionManual CreateFilterConverterManual(List<EditorParameter> editorParameters)
        {
            return new FilterConverterExpressionManual(editorParameters);
        }

        public static FilterConverterExpressionAuto CreateFilterConverterAuto(List<EditorParameter> editorParameters)
        {
            return new FilterConverterExpressionAuto(editorParameters);
        }
    }
}
