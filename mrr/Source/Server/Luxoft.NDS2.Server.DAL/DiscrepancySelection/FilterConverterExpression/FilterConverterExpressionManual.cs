﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;
using System.Linq;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator;
using SelectionFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public class FilterConverterExpressionManual : FilterConverterExpressionBase
    {
        private readonly List<string> _excludeList = new List<string>
        {
            DiscrepancyAggregateTableColumns.ExcludeSide1Inn,
            DiscrepancyAggregateTableColumns.ExcludeSide2Inn,
        };

        private readonly List<string> _bitFieldList = new List<string>
        {
            DiscrepancyAggregateTableColumns.Side1OperationCodes,
            DiscrepancyAggregateTableColumns.Side2OperationCodes
        };

        public FilterConverterExpressionManual(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        public FilterExpressionBase Convert(Filter selectionFilter)
        {
            var filterExpressionGroupUnion = FilterExpressionCreator.CreateUnion();

            foreach (var groupFilter in selectionFilter.Groups.Where(p => p.IsEnabled))
            {
                var filterExpressionGroup = FilterExpressionCreator.CreateGroup();
                foreach (var parameter in groupFilter.Parameters.Where(p => p.IsEnabled))
                {
                    var editorParameter = GetEditorParametr(parameter.AttributeId);
                    if (editorParameter == null)
                        continue;
                    if (editorParameter != null &&
                        (string.IsNullOrWhiteSpace(editorParameter.AggregateName) || parameter.Values.Count() == 0))
                        continue;

                    if (parameter.Operator != SelectionFilterOperator.InList &&
                        parameter.Operator != SelectionFilterOperator.NotInList &&
                        parameter.Values.Count() == 1)
                    {
                        FillExpressionForSingle(filterExpressionGroup, parameter, editorParameter);
                    }
                    else
                    {
                        FillExpressionForList(filterExpressionGroup, parameter, editorParameter);
                    }
                }
                filterExpressionGroupUnion.WithExpression(filterExpressionGroup);
            }
            return filterExpressionGroupUnion;
        }

        private void FillExpressionForSingle(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            if (!FiltersAggregateNames.ContainsKey(editorParameter.AggregateName))
                return;

            if (FillExpressionSumNds(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionInOtherSelection(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionContractorDeclarationId(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionBitFieldList(filterExpressionGroup, parameter, editorParameter))
                return;

            var parameterValue = parameter.Values.First();

            if (parameter.Operator == SelectionFilterOperator.Beetween)
            {
                FillOperatorBeetween(filterExpressionGroup, editorParameter.AggregateName, parameterValue.ValueFrom, parameterValue.ValueTo);
                return;
            }

            var filterExpression = _excludeList.Contains(editorParameter.AggregateName)
                ? CreateFilterExpressionBase(editorParameter.AggregateName, ColumnFilterOperator.NotEquals, parameterValue.Value)
                : CreateFilterExpressionBase(editorParameter.AggregateName, ConvertComparisionOperator(parameter.Operator), parameterValue.Value);
            filterExpressionGroup.WithExpression(filterExpression);
        }

        private void FillExpressionForList(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            if (FillExpressionSumNds(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionInOtherSelection(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionContractorDeclarationId(filterExpressionGroup, parameter, editorParameter))
                return;

            if (FillExpressionBitFieldList(filterExpressionGroup, parameter, editorParameter))
                return;

            if (parameter.Operator == SelectionFilterOperator.Beetween)
            {
                var filterBeetweenUnioun = FilterExpressionCreator.CreateUnion();
                foreach (var parameterValue in parameter.Values)
                {
                    var filterBeetweenGroup = FilterExpressionCreator.CreateGroup();
                    FillOperatorBeetween(filterBeetweenGroup, editorParameter.AggregateName, parameterValue.ValueFrom, parameterValue.ValueTo);
                    filterBeetweenUnioun.WithExpression(filterBeetweenGroup);
                }
                filterExpressionGroup.WithExpression(filterBeetweenUnioun);
                return;
            }

            var listValues =
                parameter.Values.Select<ParameterValue, object>(parameterValue => parameterValue.Value)
                    .ToList();

            var filterExpression = _excludeList.Contains(editorParameter.AggregateName) || parameter.Operator == ComparisonOperator.NotInList
                ? FilterExpressionCreator.CreateNotInList(editorParameter.AggregateName, listValues)
                : FilterExpressionCreator.CreateInList(editorParameter.AggregateName, listValues);

            filterExpressionGroup.WithExpression(filterExpression);
        }

        private bool FillExpressionSumNds(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            bool ret = false;
            if (editorParameter.AggregateName == DiscrepancyAggregateTableColumns.Side1NDSAmount ||
                editorParameter.AggregateName == DiscrepancyAggregateTableColumns.Side2NDSAmount)
            {
                var ndsAmountMapper = new Dictionary<string, string>();
                ndsAmountMapper.Add(DiscrepancyAggregateTableColumns.Side1NDSAmount, DiscrepancyAggregateTableColumns.Side1MarkRule);
                ndsAmountMapper.Add(DiscrepancyAggregateTableColumns.Side2NDSAmount, DiscrepancyAggregateTableColumns.Side2MarkRule);

                var filterExpressionMain = FilterExpressionCreator.CreateUnion();
                foreach (var parameterValue in parameter.Values)
                {
                    var filterExpressionItemGroup = FilterExpressionCreator.CreateGroup();

                    string declarationType = ((int)DeclarationType.Empty).ToString();
                    if (!string.IsNullOrWhiteSpace(parameterValue.Value))
                        declarationType = parameterValue.Value;

                    if (declarationType == ((int)DeclarationType.ToCharge).ToString() ||
                        declarationType == ((int)DeclarationType.ToPay).ToString() ||
                        declarationType == ((int)DeclarationType.Zero).ToString())
                    {
                        if (ndsAmountMapper.ContainsKey(editorParameter.AggregateName))
                        {
                            var filterExpression = CreateFilterExpressionBase(ndsAmountMapper[editorParameter.AggregateName], ColumnFilterOperator.Equals, parameterValue.Value);
                            filterExpressionItemGroup.WithExpression(filterExpression);
                        }
                    }

                    if (parameter.Operator == SelectionFilterOperator.Beetween)
                    {
                        if (declarationType == ((int)DeclarationType.ToPay).ToString())
                            FillOperatorBeetween(filterExpressionItemGroup, editorParameter.AggregateName, parameterValue.ValueFrom, parameterValue.ValueTo);

                        if (declarationType == ((int)DeclarationType.Empty).ToString())
                        {
                            if (ndsAmountMapper.ContainsKey(editorParameter.AggregateName))
                            {
                                var filterExpressionGroupUnion = FilterExpressionCreator.CreateUnion();

                                var filterExpressionGroupPay = FilterExpressionCreator.CreateGroup();
                                var filterExpressionPay = CreateFilterExpressionBase(ndsAmountMapper[editorParameter.AggregateName], ColumnFilterOperator.Equals, ((int)DeclarationType.ToPay).ToString());
                                filterExpressionGroupPay.WithExpression(filterExpressionPay);
                                FillOperatorBeetween(filterExpressionGroupPay, editorParameter.AggregateName, parameterValue.ValueFrom, parameterValue.ValueTo);
                                filterExpressionGroupUnion.WithExpression(filterExpressionGroupPay);

                                var filterExpressionGroupCharge = FilterExpressionCreator.CreateGroup();
                                var filterExpressionCharge = CreateFilterExpressionBase(ndsAmountMapper[editorParameter.AggregateName], ColumnFilterOperator.Equals, ((int)DeclarationType.ToCharge).ToString());
                                filterExpressionGroupCharge.WithExpression(filterExpressionCharge);
                                if (!string.IsNullOrWhiteSpace(parameterValue.ValueFrom))
                                    parameterValue.ValueFrom = "-" + parameterValue.ValueFrom;
                                if (!string.IsNullOrWhiteSpace(parameterValue.ValueFrom))
                                    parameterValue.ValueTo = "-" + parameterValue.ValueTo;
                                FillOperatorBeetween(filterExpressionGroupCharge, editorParameter.AggregateName, parameterValue.ValueTo, parameterValue.ValueFrom);
                                filterExpressionGroupUnion.WithExpression(filterExpressionGroupCharge);

                                filterExpressionItemGroup.WithExpression(filterExpressionGroupUnion);
                            }
                        }
                    }

                    filterExpressionMain.WithExpression(filterExpressionItemGroup);
                }
                filterExpressionGroup.WithExpression(filterExpressionMain);
                ret = true;
            }

            return ret;
        }

        private void FillOperatorBeetween(FilterExpressionGroup filterExpressionGroup, string aggregateName, string valueFrom, string valueTo)
        {
            if (!string.IsNullOrWhiteSpace(valueFrom))
            {
                var filterExpressionFrom = CreateFilterExpressionBase(aggregateName, ColumnFilterOperator.GreaterThanOrEqualTo, valueFrom);
                filterExpressionGroup.WithExpression(filterExpressionFrom);
            }
            if (!string.IsNullOrWhiteSpace(valueTo))
            {
                var filterExpressionTo = CreateFilterExpressionBase(aggregateName, ColumnFilterOperator.LessThanOrEqualTo, valueTo);
                filterExpressionGroup.WithExpression(filterExpressionTo);
            }
        }

        private bool FillExpressionContractorDeclarationId(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            bool ret = false;
            if (editorParameter.AggregateName == DiscrepancyAggregateTableColumns.ContractorDeclId)
            {
                filterExpressionGroup.WithExpression(CreateFilterExpressionBase(editorParameter.AggregateName, ColumnFilterOperator.Equals, null));
                ret = true;
            }
            return ret;
        }

        private bool FillExpressionInOtherSelection(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            if (editorParameter.AggregateName != DiscrepancyAggregateTableColumns.IsInOtherSelections)
                return false;

            var filterExpressionOtherSel = CreatePatternGroupExpression(editorParameter.AggregateName, parameter.Values.Select(v => v.Value));
            filterExpressionGroup.WithExpression(filterExpressionOtherSel);
            return true;
        }

        private bool FillExpressionBitFieldList(FilterExpressionGroup filterExpressionGroup, Parameter parameter, EditorParameter editorParameter)
        {
            if (!_bitFieldList.Contains(editorParameter.AggregateName))
                return false;

            var filterExpressionBit = CreateBitFieldFilter(parameter, editorParameter);
            if (filterExpressionBit != null)
                filterExpressionGroup.WithExpression(filterExpressionBit);

            return true;
        }
    }
}
