﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using ColumnFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator;
using SelectionFilterOperator = Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.ComparisonOperator;

namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public class FilterConverterExpressionBase
    {
        protected readonly Dictionary<string, Func<FilterExpressionCreateParameters, FilterExpressionBase>> FiltersAggregateNames =
            new Dictionary<string, Func<FilterExpressionCreateParameters, FilterExpressionBase>>();

        private readonly Dictionary<SelectionFilterOperator, ColumnFilterOperator> _dictConvertOperators = new Dictionary<SelectionFilterOperator, ColumnFilterOperator>();

        private readonly Dictionary<string, Func<object, object>> _filterObjectConverts = new Dictionary<string, Func<object, object>>();

        private readonly List<EditorParameter> _editorParameters = new List<EditorParameter>();

        public FilterConverterExpressionBase(List<EditorParameter> editorParameters)
        {
            _editorParameters.AddRange(editorParameters);
            CreateUniqueExpressionCreator();
            CreateDictConvertOperator();
            CreateFilterObjectConverts();
        }

        private void CreateUniqueExpressionCreator()
        {
            Func<FilterExpressionCreateParameters, FilterExpressionBase> funcSimple = p => FilterExpressionCreator.Create(p.FirInternalName, p.Operator, p.Value);
            Func<FilterExpressionCreateParameters, FilterExpressionBase> funcIsInOtherSelections = p => FilterExpressionCreator.Create(p.FirInternalName, p.Operator, p.Value);

            FiltersAggregateNames.Clear();
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.RegionCode, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.SonoCode, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclInn, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Inn, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Name, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.SurCode, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorSurCode, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Period, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.PeriodYear, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.PeriodFull, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.SubmitDate, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclSign, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclTotalAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclMinAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclMaxAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclAvgAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclTotalCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclPurchaseCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclSalesCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclTotalPVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclMinPVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclMaxPVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclPurchasePVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.DeclSalesPVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.TypeCode, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Amount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.PVP, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.IsInOtherSelections, funcIsInOtherSelections);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.CreateDate, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.CompareRule, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.InvoiceChapter, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.InvoiceContractorChapter, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side1MarkRule, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side2MarkRule, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side1NDSAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side2NDSAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ExcludeBuyerInn, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ExcludeSellerInn, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side1OperationCodes, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.Side2OperationCodes, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorSubmitDate, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalPvp, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclMinAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclMaxAmount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclMinPvp, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclMaxPvp, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclPurchaseCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclSalesCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalCount, funcSimple);
            FiltersAggregateNames.Add(DiscrepancyAggregateTableColumns.ContractorDeclId, funcSimple);
        }

        private void CreateFilterObjectConverts()
        {
            Func<object, object> funcString = p => p;

            Func<object, object> funcDate = p =>
            {
                var s = p as string;
                if (s == null)
                    return p;
                DateTime dt;
                return DateTime.TryParse(s, out dt) ? dt : p;
            };

            Func<object, object> funcDecimal = p =>
            {
                var s = p as string;
                if (s == null)
                    return p;
                decimal temp;
                var val = s;
                if (decimal.TryParse(val, out temp))
                {
                    return temp;
                }
                if (decimal.TryParse(val.Replace(",", "."), out temp))
                {
                    return temp;
                }
                return p;
            };

            Func<object, object> funcLong = p =>
            {
                var s = p as string;
                if (s == null)
                    return p;
                long temp;
                return long.TryParse(s, out temp) ? temp : p;
            };

            _filterObjectConverts.Clear();
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.RegionCode, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.SonoCode, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclInn, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Inn, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Name, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.SurCode, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorSurCode, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Period, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.PeriodYear, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.PeriodFull, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.SubmitDate, funcDate);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclSign, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclTotalAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclMinAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclMaxAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclAvgAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclTotalCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclPurchaseCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclSalesCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclTotalPVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclMinPVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclMaxPVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclPurchasePVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.DeclSalesPVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.TypeCode, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Amount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.PVP, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.IsInOtherSelections, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.CreateDate, funcDate);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.CompareRule, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.InvoiceChapter, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.InvoiceContractorChapter, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side1MarkRule, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side2MarkRule, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side1NDSAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side2NDSAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ExcludeBuyerInn, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ExcludeSellerInn, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side1OperationCodes, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.Side2OperationCodes, funcString);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorSubmitDate, funcDate);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalPvp, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclMinAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclMaxAmount, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclMinPvp, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclMaxPvp, funcDecimal);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclPurchaseCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclSalesCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclTotalCount, funcLong);
            _filterObjectConverts.Add(DiscrepancyAggregateTableColumns.ContractorDeclId, funcLong);
        }

        private void CreateDictConvertOperator()
        {
            _dictConvertOperators.Clear();
            _dictConvertOperators.Add(SelectionFilterOperator.Contains, ColumnFilterOperator.Contains);
            _dictConvertOperators.Add(SelectionFilterOperator.NotContains, ColumnFilterOperator.DoesNotContain);
            _dictConvertOperators.Add(SelectionFilterOperator.Equals, ColumnFilterOperator.Equals);
            _dictConvertOperators.Add(SelectionFilterOperator.NotEquals, ColumnFilterOperator.NotEquals);
            _dictConvertOperators.Add(SelectionFilterOperator.GE, ColumnFilterOperator.GreaterThan);
            _dictConvertOperators.Add(SelectionFilterOperator.GT, ColumnFilterOperator.GreaterThanOrEqualTo);
            _dictConvertOperators.Add(SelectionFilterOperator.LE, ColumnFilterOperator.LessThan);
            _dictConvertOperators.Add(SelectionFilterOperator.LT, ColumnFilterOperator.LessThanOrEqualTo);
            _dictConvertOperators.Add(SelectionFilterOperator.StartWith, ColumnFilterOperator.StartsWith);
            _dictConvertOperators.Add(SelectionFilterOperator.EndWith, ColumnFilterOperator.EndsWith);
            _dictConvertOperators.Add(SelectionFilterOperator.InList, ColumnFilterOperator.Contains);
            _dictConvertOperators.Add(SelectionFilterOperator.NotInList, ColumnFilterOperator.DoesNotContain);
            _dictConvertOperators.Add(SelectionFilterOperator.OneOf, ColumnFilterOperator.OneOf);

            _dictConvertOperators.Add(SelectionFilterOperator.Beetween, ColumnFilterOperator.NotDefinedOperator);
            _dictConvertOperators.Add(SelectionFilterOperator.IsNull, ColumnFilterOperator.NotDefinedOperator);
            _dictConvertOperators.Add(SelectionFilterOperator.IsNotNull, ColumnFilterOperator.NotDefinedOperator);
        }

        private FilterExpressionBase GetFilterExpression(string name, FilterExpressionCreateParameters p)
        {
            return FiltersAggregateNames.ContainsKey(name)
                ? FiltersAggregateNames[name].Invoke(p)
                : FilterExpressionCreator.Create(p.FirInternalName, p.Operator, p.Value);
        }

        protected FilterExpressionBase CreateFilterExpressionBase(string name, ColumnFilterOperator filterOperator, object value)
        {
            var p = new FilterExpressionCreateParameters
            {
                FirInternalName = name,
                Operator = filterOperator,
                Value = ConvertFilterObject(name, value)
            };
            return GetFilterExpression(name, p);
        }

        protected FilterExpressionBase CreatePatternGroupExpression(string name, IEnumerable<object> values)
        {
            return new PatternGroupExpression(name, values);
        }

        protected ColumnFilterOperator ConvertComparisionOperator(
            SelectionFilterOperator comparisionOperator)
        {
            return _dictConvertOperators.ContainsKey(comparisionOperator)
                ? _dictConvertOperators[comparisionOperator]
                : ColumnFilterOperator.NotDefinedOperator;
        }

        private object ConvertFilterObject(string name, object value)
        {
            return _filterObjectConverts.ContainsKey(name)
                ? _filterObjectConverts[name].Invoke(value)
                : value;
        }

        protected EditorParameter GetEditorParametr(int attributeId)
        {
            return _editorParameters.SingleOrDefault(p => p.Id == attributeId);
        }

        protected FilterExpressionBase CreateBitFieldFilter(Parameter parameter, EditorParameter editorParameter)
        {
            var listValues =
                parameter.Values.Select(parameterValue => FilterExpressionCreator.ToBit(parameterValue.Value))
                    .ToList();

            return FilterExpressionCreator.CreateBitFieldFilter(editorParameter.AggregateName, listValues, ConvertComparisionOperator(parameter.Operator));
        }
    }
}
