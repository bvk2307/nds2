﻿
namespace Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression
{
    public static class DiscrepancyAggregateTableColumns
    {
        /// <summary>
        /// Код региона
        /// </summary>
        public const string RegionCode = "region_code";

        /// <summary>
        /// Код инспекции
        /// </summary>
        public const string SonoCode = "sono_code";

        /// <summary>
        /// ИНН НП
        /// </summary>
        public const string DeclInn = "decl_inn";

        /// <summary>
        /// ИНН покупателя
        /// </summary>
        public const string Inn = "inn";

        /// <summary>
        /// ФИО/Название НП
        /// </summary>
        public const string Name = "name";

        /// <summary>
        /// СУР НП
        /// </summary>
        public const string SurCode = "sur_code";

        /// <summary>
        /// СУР контрагента
        /// </summary>
        public const string ContractorSurCode = "contractor_sur_code";

        /// <summary>
        /// Отчетный период
        /// </summary>
        public const string Period = "period";

        /// <summary>
        /// Отчетный период с годом
        /// </summary>
        public const string PeriodYear = "period;year";

        /// <summary>
        /// Отчетный период с годом
        /// </summary>
        public const string PeriodFull = "period_full";

        /// <summary>
        /// Дата подачи декларации
        /// </summary>
        public const string SubmitDate = "submit_date";

        /// <summary>
        /// Признак НД
        /// </summary>
        public const string DeclSign = "decl_sign";

        /// <summary>
        /// Сумма к уплате/вычету
        /// </summary>
        public const string DeclTotalAmount = "decl_total_amount";

        /// <summary>
        /// Статистические значения сумм
        /// </summary>
        public const string DeclMinAmount = "decl_min_amount";
        public const string DeclMaxAmount = "decl_max_amount";
        public const string DeclAvgAmount = "decl_avg_amount";

        /// <summary>
        /// Число деклараций
        /// </summary>
        public const string DeclTotalCount = "decl_total_count";

        /// <summary>
        /// 
        /// </summary>
        public const string DeclPurchaseCount = "decl_purchase_count";

        /// <summary>
        /// Кол-во продаж
        /// </summary>
        public const string DeclSalesCount = "decl_sales_count";
        public const string DeclTotalPVP = "decl_total_pvp";
        public const string DeclMinPVP = "decl_min_pvp";
        public const string DeclMaxPVP = "decl_max_pvp";
        public const string DeclPurchasePVP = "decl_purchase_pvp";
        public const string DeclSalesPVP = "decl_sales_pvp";
        public const string TypeCode = "type_code";
        public const string Amount = "amount";
        public const string PVP = "pvp";
        public const string IsInOtherSelections = "is_in_other_selections";
        public const string CreateDate = "create_date";
        
        /// <summary>
        /// Номеро правила сопоставления
        /// </summary>
        public const string CompareRule = "compare_rule";

        /// <summary>
        /// Раздел НД
        /// </summary>
        public const string InvoiceChapter = "invoice_chapter";

        /// <summary>
        /// Раздел НД контрагента
        /// </summary>
        public const string InvoiceContractorChapter = "invoice_contractor_chapter";

        /// <summary>
        /// Признак НД покупателя
        /// </summary>
        public const string Side1MarkRule = "side1mark_rule";

        /// <summary>
        /// Признак НД продавца
        /// </summary>
        public const string Side2MarkRule = "side2mark_rule";

        /// <summary>
        /// Сумма по НД покупателя
        /// </summary>
        public const string Side1NDSAmount = "side1nds_amount";

        /// <summary>
        /// Сумма по НД продавца
        /// </summary>
        public const string Side2NDSAmount = "side2nds_amount";

        /// <summary>
        /// Исключенные ИНН покупателя
        /// </summary>
        public const string ExcludeBuyerInn = "exclude_buyer_inn";

        /// <summary>
        /// Исключенные ИНН продавца
        /// </summary>
        public const string ExcludeSellerInn = "exclude_seller_inn";

        /// <summary>
        /// КВО покупателя
        /// </summary>
        public const string Side1OperationCodes = "side1operationCodes";

        /// <summary>
        /// КВО продавца
        /// </summary>
        public const string Side2OperationCodes = "side2operationCodes";

        /// <summary>
        /// Исключенные ИНН покупателя
        /// </summary>
        public const string ExcludeSide1Inn = "ExcludeSide1Inn";

        /// <summary>
        /// Исключенные ИНН продавца
        /// </summary>
        public const string ExcludeSide2Inn = "ExcludeSide2Inn";

        /// <summary>
        /// Дата подачи декларации в НО для контрагента
        /// </summary>
        public const string ContractorSubmitDate = "contractor_submit_date";

        /// <summary>
        /// Сумма к уплате/вычету для контрагента
        /// </summary>
        public const string ContractorDeclTotalAmount = "contractor_decl_total_amount";

        public const string ContractorDeclTotalPvp = "contractor_decl_total_pvp";
        public const string ContractorDeclMinAmount = "contractor_decl_min_amount";
        public const string ContractorDeclMaxAmount = "contractor_decl_max_amount";
        public const string ContractorDeclMinPvp = "contractor_decl_min_pvp";
        public const string ContractorDeclMaxPvp = "contractor_decl_max_pvp";
        public const string ContractorDeclPurchaseCount = "contractor_decl_purchase_count";
        public const string ContractorDeclSalesCount = "contractor_decl_sales_count";
        public const string ContractorDeclTotalCount = "contractor_decl_total_count";
        public const string ContractorDeclId = "contractor_decl_id";
    }
}
