﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IAISAdapter
    {
        ASKDekl GetDeclaration(long declId);
    }
}
