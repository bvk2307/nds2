﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDiscrepancyDocumentPackageAdapter
    {
        /// <summary>
        /// Возвращает список СФ для документа
        /// </summary>
        /// <param name="declarationId">Идентификатор документа</param>
        /// <returns>Список СФ</returns>
        PageResult<DiscrepancyDocumentInvoice> GetInvoices(long docId, QueryConditions conditions);
    }
}
