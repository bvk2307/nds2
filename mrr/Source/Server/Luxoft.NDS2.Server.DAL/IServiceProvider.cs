﻿using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы сервисного слоя необходимые для работы DAL
    /// </summary>
    public interface IServiceProvider : ILogProvider
    {
        /// <summary>
        /// Вычитывает параметр конфигурации по ключу
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Значение параметра конфигурации</returns>
        string GetConfigurationValue(string key);

        string GetQueryText(string scope, string qName);
    }
}
