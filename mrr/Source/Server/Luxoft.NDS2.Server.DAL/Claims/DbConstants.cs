﻿
namespace Luxoft.NDS2.Server.DAL.Claims
{
    static class DbConstants
    {
        public const string PatternScope = "InvoiceClaimReports";

        public const string QueryInvoiceClaimListPattern = "InvoiceClaimList";

        public const string QueryInvoiceClaimListByStatusesPattern = "InvoiceClaimListByStatuses";

        public const string QueryDatesPattern =
            "select t.create_date as Value from v$doc_create_dates t where t.doc_type = :pDocType order by t.create_date asc";

        public const string QueryDocSendStatusPattern =
            @"select :pDocType, t.SendDate, t.SendTime, t.Username, case when t.SendDate is not null then 1 else 0 end as WasSent from DUAL
left join DOC_SEND_DATA t on t.DocType = :pDocType and SendDate = trunc(:pDate)";

        public const string QueryStatusDictionaryPattern =
            "select t.id, t.Description from doc_status t where t.doc_type = :pDocType order by t.id";

        public const string QueryDocsByDatePattern =
            "select * from V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {1}";

        public const string CountDocsByDatePattern =
            "V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {1}";

        public const string QueryDocsByDateAndStatusPattern =
            "select * from V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {0}.StatusCode in ({1}) and {2}";

        public const string CountDocsByDateAndStatusPattern =
            "V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {0}.StatusCode in ({1}) and {2}";

        public const string QueryDocsToSendPattern =
            "select * from V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {0}.StatusCode = -1 and {0}.Excluded is null and {1}";

        public const string CountDocsToSendPattern =
            "V$INVOICE_CLAIM_LIST {0} where {0}.CreateDate = :pDate and {0}.StatusCode = -1 and {0}.Excluded is null and {1}";

        public const string QueryReportSelectionStatsPattern =
            "select * from V$SELECTION_STATS v where v.CreateDate = :{0}";

        public const string QueryReportTaxMonitoringPattern = @"select rownum as LineNumber, v.* from V$TAX_MONITORING v where v.DocId = :{0} and v.chapter = :{1} and v.IsUnconfirmed = 0";

        public const string QueryReportTaxMonitoringUnconfirmedPattern = @"select * from V$TAX_MONITORING v where v.DocId = :{0} and v.IsUnconfirmed = 1";

        public const string DocManagePackage = "PAC$DOC_MANAGE";

        public const string IncludeProcedure = "P$INCLUDE";

        public const string ExcludeProcedure = "P$EXCLUDE";

        public const string SendProcedure = "P$SEND";

        public const string GetBaseDateProcedure = "P$GET_BASE_DATE";

        public const string DocId = "pDocId";

        public const string User = "pUser";

        public const string DocType = "pDocType";

        public const string Date = "pDate";

        public const string StatusCode = "pStatusCode";

        public const string StatusCodeQPattern = ":pStatusCode{0}";

        public const string StatusCodePattern = "pStatusCode{0}";

        public const string Result = "pResult";

        public const string Chapter = "pChapter";
    }
}
