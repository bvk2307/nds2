﻿using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class DateTimeContainer
    {
        public DateTime Value { get; set; }
    }

    class GetCreateDatesCommandBuilder : QueryCommandBuilder
    {
        private readonly ClaimType _claimType;

        public GetCreateDatesCommandBuilder(ClaimType claimType)
        {
            _claimType = claimType;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.DocType, (int)_claimType));

            return DbConstants.QueryDatesPattern;
        }
    }

    class CreateDatesAdapter : BaseOracleTableAdapter, ICreateDatesAdapter
    {
        public CreateDatesAdapter(IServiceProvider service) : base(service)
        {
        }

        public List<DateTime> All(ClaimType type)
        {
            return new ListCommandExecuter<DateTimeContainer>(new GenericDataMapper<DateTimeContainer>(), _service)
                    .TryExecute(new GetCreateDatesCommandBuilder(type)).Select(x => x.Value).ToList();
        }
    }
}
