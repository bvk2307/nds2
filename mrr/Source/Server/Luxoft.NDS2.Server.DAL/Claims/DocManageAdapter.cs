﻿using System;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class DocManageCommand : ExecuteProcedureCommandBuilder
    {
        private readonly OracleParameter[] _parameters;

        public DocManageCommand(string procedure, OracleParameter[] parameters)
            : base(string.Format("{0}.{1}", DbConstants.DocManagePackage, procedure))
        {
            _parameters = parameters;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            foreach (var parameter in _parameters)
            {
                parameters.With(parameter);
            }
        }
    }

    class DocManageAdapter : IDocManageAdapter
    {
        private readonly IServiceProvider _service;

        public DocManageAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public void Include(long docId, string username)
        {
            new ResultCommandExecuter(_service).TryExecute(new DocManageCommand(DbConstants.IncludeProcedure, new[]
            {
                FormatCommandHelper.NumericIn(DbConstants.DocId, docId),
                FormatCommandHelper.VarcharIn(DbConstants.User, username)
            }));
        }

        public void Exclude(long docId, string username)
        {
            new ResultCommandExecuter(_service).TryExecute(new DocManageCommand(DbConstants.ExcludeProcedure, new[]
            {
                FormatCommandHelper.NumericIn(DbConstants.DocId, docId),
                FormatCommandHelper.VarcharIn(DbConstants.User, username)
            }));
        }

        public void Send(DateTime date, string username)
        {
            new ResultCommandExecuter(_service).TryExecute(new DocManageCommand(DbConstants.SendProcedure, new[]
            {
                FormatCommandHelper.DateIn(DbConstants.Date, date),
                FormatCommandHelper.VarcharIn(DbConstants.User, username)
            }));
        }

        public DateTime GetBaseDate()
        {
            var result = new ResultCommandExecuter(_service)
                .TryExecute(new DocManageCommand(DbConstants.GetBaseDateProcedure, new[]
                {
                    FormatCommandHelper.DateOut(DbConstants.Date)
                }));
            var s = ((OracleDate) result[DbConstants.Date]);
            return new DateTime(s.Year, s.Month, s.Day);
        }
    }
}
