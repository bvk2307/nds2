﻿using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class ReportInvoiceClaimListAdapter : IReportInvoiceClaimListAdapter
    {
        private readonly IServiceProvider _service;
        private readonly string _query;

        public ReportInvoiceClaimListAdapter(IServiceProvider service)
        {
            _service = service;
            _query = _service.GetQueryText(DbConstants.PatternScope, DbConstants.QueryInvoiceClaimListPattern);
        }

        class QueryCommand : QueryCommandBuilder
        {
            private readonly string _selectStatementPattern;
            private readonly DateTime _date;
            private readonly FilterExpressionBase _searchBy;

            public QueryCommand(string selectStatementPattern, DateTime date,
            FilterExpressionBase searchBy)
            {
                _selectStatementPattern = selectStatementPattern;
                _date = date;
                _searchBy = searchBy;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.With(FormatCommandHelper.DateIn(DbConstants.Date, _date));

                var paramBuilder = new OracleParameterCollectionBuilder(
                    parameters,
                    "p_{0}");

                return string.Format(
                    _selectStatementPattern,
                    _searchBy.ToQuery(paramBuilder, new PatternProvider("v")));
            }
        }

        protected virtual QueryCommandBuilder GetQuery(DateTime date, FilterExpressionBase filter)
        {
            return new QueryCommand(_query, date, filter);
        }

        public List<InvoiceClaimReportItem> Search(DateTime date, List<FilterQuery> filter)
        {
            return new ListCommandExecuter<InvoiceClaimReportItem>(new GenericDataMapper<InvoiceClaimReportItem>(),
                _service).TryExecute(GetQuery(date, filter.ToFilterGroup())).ToList();
        }
    }

    sealed class ReportInvoiceClaimListByStatusAdapter : ReportInvoiceClaimListAdapter
    {
        private readonly int[] _statuses;
        private readonly string _query;
        public ReportInvoiceClaimListByStatusAdapter(IServiceProvider service, int[] statuses)
            : base(service)
        {
            _statuses = statuses;
            _query = service.GetQueryText(DbConstants.PatternScope, DbConstants.QueryInvoiceClaimListByStatusesPattern);
        }

        class QueryCommand : QueryCommandBuilder
        {
            private readonly string _selectStatementPattern;
            private readonly DateTime _date;
            private readonly FilterExpressionBase _searchBy;
            private readonly int[] _statuses;
            private readonly string _statusLine;

            public QueryCommand(string selectStatementPattern, DateTime date, FilterExpressionBase searchBy, int[] statuses)
            {
                _selectStatementPattern = selectStatementPattern;
                _date = date;
                _searchBy = searchBy;
                _statuses = statuses;
                _statusLine = string.Join(",", _statuses.Select(s => string.Format(DbConstants.StatusCodeQPattern, s))).Replace("-", "_");
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.With(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                foreach (var st in _statuses)
                {
                    parameters.Add(FormatCommandHelper.NumericIn(string.Format(DbConstants.StatusCodePattern, st).Replace("-", "_"), st));
                }

                var paramBuilder = new OracleParameterCollectionBuilder(
                    parameters,
                    "p_{0}");

                return string.Format(
                    _selectStatementPattern,
                    _searchBy.ToQuery(paramBuilder, new PatternProvider("v")), _statusLine);
            }
        }

        protected override QueryCommandBuilder GetQuery(DateTime date, FilterExpressionBase filter)
        {
            return new QueryCommand(_query, date, filter, _statuses);
        }
    }
}
