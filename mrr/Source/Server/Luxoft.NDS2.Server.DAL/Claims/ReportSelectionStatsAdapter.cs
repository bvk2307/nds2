﻿using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class ReportSelectionStatsAdapter : IReportSelectionStatsAdapter
    {
        private readonly IServiceProvider _service;

        public ReportSelectionStatsAdapter(IServiceProvider service)
        {
            _service = service;
        }

        class GetReportCommand : QueryCommandBuilder
        {
            private readonly DateTime _date;

            public GetReportCommand(DateTime date)
            {
                _date = date;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));

                return string.Format(DbConstants.QueryReportSelectionStatsPattern, DbConstants.Date);
            }
        }

        public List<SelectionClaimCompare> All(DateTime date)
        {
            return new ListCommandExecuter<SelectionClaimCompare>(new GenericDataMapper<SelectionClaimCompare>(),
                _service)
                .TryExecute(new GetReportCommand(date)).ToList();
        }
    }
}
