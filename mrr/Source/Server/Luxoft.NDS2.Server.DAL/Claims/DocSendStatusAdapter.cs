﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class GetDocSendStatusCommandBuilder : QueryCommandBuilder
    {
        private readonly ClaimType _claimType;
        private readonly DateTime _date;


        public GetDocSendStatusCommandBuilder(ClaimType claimType, DateTime date)
        {
            _claimType = claimType;
            _date = date;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.DocType, (int)_claimType));
            parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));

            return DbConstants.QueryDocSendStatusPattern;
        }

    }
    
    class DocSendStatusAdapter : BaseOracleTableAdapter, IDocSendStatusAdapter
    {
        public DocSendStatusAdapter(IServiceProvider service) : base(service)
        {
        }

        public DocSendStatus Get(ClaimType type, DateTime date)
        {
            return new ListCommandExecuter<DocSendStatus>(new GenericDataMapper<DocSendStatus>(), _service)
                   .TryExecute(new GetDocSendStatusCommandBuilder(type, date)).First();
        }
    }
}
