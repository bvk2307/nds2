﻿using System.Linq;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class GetStatusListCommandBuilder : QueryCommandBuilder
    {
        private readonly ClaimType _claimType;

        public GetStatusListCommandBuilder(ClaimType claimType)
        {
            _claimType = claimType;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.DocType, (int)_claimType));

            return DbConstants.QueryStatusDictionaryPattern;
        }
    }
    
    class StatusListAdapter : BaseOracleTableAdapter, IStatusListAdapter
    {
        public StatusListAdapter(IServiceProvider service) : base(service)
        {
        }

        public List<SimpleStatusDto> All(ClaimType type)
        {
            return new ListCommandExecuter<SimpleStatusDto>(new GenericDataMapper<SimpleStatusDto>(), _service)
                    .TryExecute(new GetStatusListCommandBuilder(type)).ToList();
        }
    }
}
