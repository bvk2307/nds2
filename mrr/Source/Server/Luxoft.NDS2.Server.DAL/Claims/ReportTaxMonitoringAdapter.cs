﻿using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class ReportTaxMonitoringAdapter : IReportTaxMonitoringAdapter
    {
        private readonly IServiceProvider _service;

        public ReportTaxMonitoringAdapter(IServiceProvider service)
        {
            _service = service;
        }

        class GetReportCommand : QueryCommandBuilder
        {
            private readonly long _docId;
            private readonly int _chapter;

            public GetReportCommand(long docId, int chapter)
            {
                _docId = docId;
                _chapter = chapter;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericIn(DbConstants.DocId, _docId));
                parameters.Add(FormatCommandHelper.NumericIn(DbConstants.Chapter, _chapter));

                return string.Format(DbConstants.QueryReportTaxMonitoringPattern, DbConstants.DocId, DbConstants.Chapter);
            }
        }

        class GetReportUncofirmedCommand : QueryCommandBuilder
        {
            private readonly long _docId;

            public GetReportUncofirmedCommand(long docId)
            {
                _docId = docId;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.NumericIn(DbConstants.DocId, _docId));

                return string.Format(DbConstants.QueryReportTaxMonitoringUnconfirmedPattern, DbConstants.DocId);
            }
        }

        public List<TaxMonitoringReportItem> GetForChapter(long docId, int chapter)
        {
            return new ListCommandExecuter<TaxMonitoringReportItem>(
                    new GenericDataMapper<TaxMonitoringReportItem>(),
                    _service)
                    .TryExecute(new GetReportCommand(docId, chapter)).ToList();
        }

        public List<TaxMonitoringReportItem> GetUnconfirmed(long docId)
        {
            return new ListCommandExecuter<TaxMonitoringReportItem>(
                    new GenericDataMapper<TaxMonitoringReportItem>(),
                    _service)
                    .TryExecute(new GetReportUncofirmedCommand(docId)).ToList();            
        }
    }
}
