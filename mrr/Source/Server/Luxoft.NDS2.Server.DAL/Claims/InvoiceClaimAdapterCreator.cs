﻿using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using System;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    public static class InvoiceClaimAdapterCreator
    {
        public static ICreateDatesAdapter CreateDatesAdapter(IServiceProvider service)
        {
            return new CreateDatesAdapter(service);
        }

        public static ISearchAdapter<InvoiceClaim> InvoiceClaimAdapter(IServiceProvider service, DateTime date, int[] status)
        {
            return new InvoiceClaimByDateAndStatusAdapter(service, date, status);
        }

        public static IStatusListAdapter CreateStatusListAdapter(IServiceProvider service)
        {
            return new StatusListAdapter(service);
        }

        public static IDocSendStatusAdapter CreateDocSendStatusAdapter(IServiceProvider service)
        {
            return new DocSendStatusAdapter(service);
        }

        public static ISearchAdapter<InvoiceClaim> InvoiceClaimToSendAdapter(IServiceProvider service, DateTime date)
        {
            return new InvoiceClaimToSendAdapter(service, date);
        }

        public static IDocManageAdapter CreateDocManageAdapter(IServiceProvider service)
        {
            return new DocManageAdapter(service);
        }

        public static IReportSelectionStatsAdapter CreateReportSelectionStatsAdapter(IServiceProvider service)
        {
            return new ReportSelectionStatsAdapter(service);
        }

        public static IReportInvoiceClaimListAdapter CreateReportInvoiceClaimListAdapter(IServiceProvider service)
        {
            return new ReportInvoiceClaimListAdapter(service);
        }

        public static IReportInvoiceClaimListAdapter CreateReportInvoiceClaimListByStatusAdapter(IServiceProvider service, int[] statuses)
        {
            return new ReportInvoiceClaimListByStatusAdapter(service, statuses);
        }

        public static IReportTaxMonitoringAdapter CreateReportTaxMonitoringAdapter(IServiceProvider service)
        {
            return new ReportTaxMonitoringAdapter(service);
        }
    }
}
