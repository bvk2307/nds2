﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Claims
{
    class InvoiceClaimByDateAdapter : ISearchAdapter<InvoiceClaim>
    {
        private readonly IServiceProvider _service;
        protected readonly DateTime Date;
        protected string ViewAlias = "v";

        public InvoiceClaimByDateAdapter(IServiceProvider service, DateTime date)
        {
            _service = service;
            Date = date;
        }

        protected IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected virtual string GetSearchQueryPattern()
        {
            return string.Format(DbConstants.QueryDocsByDatePattern, ViewAlias, "{0} {1}");
        }

        protected virtual string GetCountQueryPattern()
        {
            return string.Format(DbConstants.CountDocsByDatePattern, ViewAlias, "{0}");
        }

        private class CommandBuilder : PageSearchQueryCommandBuilder
        {
            private readonly DateTime _date;

            public CommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IEnumerable<ColumnSort> orderBy,
                IQueryPatternProvider queryPattern,
                DateTime date)
                : base(selectStatementPattern, searchBy, orderBy, queryPattern)
            {
                _date = date;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountMatchesCommandBulider : GetCountCommandBuilder
        {
            private readonly DateTime _date;

            public CountMatchesCommandBulider(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IQueryPatternProvider queryPattern,
                DateTime date)
                : base(selectStatementPattern, searchBy, queryPattern)
            {
                _date = date;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                return base.BuildSelectStatement(parameters);
            }
        }

        protected virtual PageSearchQueryCommandBuilder GetSearchCommandBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> sortBy)
        {
            return new CommandBuilder(
                GetSearchQueryPattern(),
                filterBy,
                sortBy,
                PatternProvider(ViewAlias),
                Date);
        }

        protected virtual GetCountCommandBuilder GetCountCommandBuilder(
            FilterExpressionBase filterBy)
        {
            return new CountMatchesCommandBulider(
                GetCountQueryPattern(),
                filterBy,
                PatternProvider(ViewAlias), Date);
        }

        public IEnumerable<InvoiceClaim> Search(FilterExpressionBase filterBy, IEnumerable<ColumnSort> sortBy, uint rowsToSkip, uint rowsToTake)
        {
            return new ListCommandExecuter<InvoiceClaim>(
                new GenericDataMapper<InvoiceClaim>(),
                _service)
                .TryExecute(GetSearchCommandBuilder(filterBy, sortBy)
                    .Take((int) rowsToTake)
                    .Skip((int) rowsToSkip));
        }

        public int Count(FilterExpressionBase filterBy)
        {
            var result = new ResultCommandExecuter(_service)
                .TryExecute(GetCountCommandBuilder(filterBy));
            return ((OracleDecimal)result[DbConstants.Result]).ToInt32();
        }
    }

    sealed class InvoiceClaimByDateAndStatusAdapter : InvoiceClaimByDateAdapter
    {
        private readonly int[] _status;

        private readonly string _statusLine;

        public InvoiceClaimByDateAndStatusAdapter(IServiceProvider service, DateTime date, int[] status)
            : base(service, date)
        {
            _status = status;
            _statusLine = string.Join(",", _status.Select(s => string.Format(DbConstants.StatusCodeQPattern, s))).Replace("-","_");
        }

        protected override string GetSearchQueryPattern()
        {
            return string.Format(DbConstants.QueryDocsByDateAndStatusPattern, ViewAlias, _statusLine, "{0} {1}");
        }

        protected override string GetCountQueryPattern()
        {
            return string.Format(DbConstants.CountDocsByDateAndStatusPattern, ViewAlias, _statusLine, "{0}");
        }

        private class CommandBuilder : PageSearchQueryCommandBuilder
        {
            private readonly DateTime _date;
            private readonly int[] _status;

            public CommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IEnumerable<ColumnSort> orderBy,
                IQueryPatternProvider queryPattern,
                DateTime date,
                int[] status)
                : base(selectStatementPattern, searchBy, orderBy, queryPattern)
            {
                _date = date;
                _status = status;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                foreach (var st in _status)
                {
                    parameters.Add(FormatCommandHelper.NumericIn(string.Format(DbConstants.StatusCodePattern, st).Replace("-", "_"), st));
                }
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountMatchesCommandBulider : GetCountCommandBuilder
        {
            private readonly DateTime _date;
            private readonly int[] _status;

            public CountMatchesCommandBulider(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IQueryPatternProvider queryPattern,
                DateTime date,
                int[] status)
                : base(selectStatementPattern, searchBy, queryPattern)
            {
                _date = date;
                _status = status;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                foreach (var st in _status)
                {
                    parameters.Add(FormatCommandHelper.NumericIn(string.Format(DbConstants.StatusCodePattern, st).Replace("-", "_"), st));
                }
                return base.BuildSelectStatement(parameters);
            }
        }

        protected override PageSearchQueryCommandBuilder GetSearchCommandBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> sortBy)
        {
            return new CommandBuilder(
                GetSearchQueryPattern(),
                filterBy,
                sortBy,
                PatternProvider(ViewAlias),
                Date,
                _status);
        }

        protected override GetCountCommandBuilder GetCountCommandBuilder(
            FilterExpressionBase filterBy)
        {
            return new CountMatchesCommandBulider(
                GetCountQueryPattern(),
                filterBy,
                PatternProvider(ViewAlias),
                Date,
                _status);
        }
    }

    sealed class InvoiceClaimToSendAdapter : InvoiceClaimByDateAdapter
    {
        public InvoiceClaimToSendAdapter(IServiceProvider service, DateTime date)
            : base(service, date)
        {
        }

        protected override string GetSearchQueryPattern()
        {
            return string.Format(DbConstants.QueryDocsToSendPattern, ViewAlias, "{0} {1}");
        }

        protected override string GetCountQueryPattern()
        {
            return string.Format(DbConstants.CountDocsToSendPattern, ViewAlias, "{0}");
        }

        private class CommandBuilder : PageSearchQueryCommandBuilder
        {
            private readonly DateTime _date;

            public CommandBuilder(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IEnumerable<ColumnSort> orderBy,
                IQueryPatternProvider queryPattern,
                DateTime date)
                : base(selectStatementPattern, searchBy, orderBy, queryPattern)
            {
                _date = date;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                return base.BuildSelectStatement(parameters);
            }
        }

        private class CountMatchesCommandBulider : GetCountCommandBuilder
        {
            private readonly DateTime _date;

            public CountMatchesCommandBulider(
                string selectStatementPattern,
                FilterExpressionBase searchBy,
                IQueryPatternProvider queryPattern,
                DateTime date)
                : base(selectStatementPattern, searchBy, queryPattern)
            {
                _date = date;
            }

            protected override string BuildSelectStatement(OracleParameterCollection parameters)
            {
                parameters.Add(FormatCommandHelper.DateIn(DbConstants.Date, _date));
                return base.BuildSelectStatement(parameters);
            }
        }

        protected override PageSearchQueryCommandBuilder GetSearchCommandBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> sortBy)
        {
            return new CommandBuilder(
                GetSearchQueryPattern(),
                filterBy,
                sortBy,
                PatternProvider(ViewAlias),
                Date);
        }

        protected override GetCountCommandBuilder GetCountCommandBuilder(
            FilterExpressionBase filterBy)
        {
            return new CountMatchesCommandBulider(
                GetCountQueryPattern(),
                filterBy,
                PatternProvider(ViewAlias),
                Date);
        }
    }
}
