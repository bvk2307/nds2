﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IEmployeeDialogAdapter
    {
        EmployeeData Load(long objectId, EmployeeType type);

        void Save(Region region, EmployeeData data);
    }
}