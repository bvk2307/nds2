﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDictionaryCommonAdapter
    {
        List<DictionaryItem> All();
    }
}
