﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IChainViewRestrictionTableAdapter
    {
        IDictionary<Tuple<int, int?>, UserRestrictionConfiguration> All();

        IList<int> GetIspectionGroups(IList<string> list);
    }
}
