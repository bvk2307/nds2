﻿using System.Security.Cryptography.X509Certificates;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Загружает параметров список выборок
    /// </summary>
    internal class SelectionAggregateAdapter : SearchDataAdapterBase<SelectionListItemData>
    {
        public SelectionAggregateAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<SelectionListItemData> DataMapper()
        {
            return new GenericDataMapper<SelectionListItemData>().
                WithFieldMapperFor(TypeHelper<SelectionListItemData>.GetMemberName(x => x.StatusId),
                    new NullableEnumFieldMapper(typeof(SelectionStatus)))
                .WithFieldMapperFor(TypeHelper<SelectionListItemData>.GetMemberName(x => x.TypeCode),
                    new IntFieldMapper());
        }

        protected override string ViewName()
        {
            return DbConstants.SelectionListViewName;
        }
    }
}