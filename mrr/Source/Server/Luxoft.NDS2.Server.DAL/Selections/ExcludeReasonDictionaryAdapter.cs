﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public class ExcludeReasonDictionaryAdapter : IDictionaryCommonAdapter
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connectionFactory;

        public ExcludeReasonDictionaryAdapter(IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }
        public List<DictionaryItem> All()
        {
            var builder = new ExcludeReasonsDictionaryCommand();
            return new ListCommandExecuter<DictionaryItem>(
                new GenericDataMapper<DictionaryItem>(),
                _service,
                _connectionFactory).TryExecute(builder).ToList();
        }
    }
}
