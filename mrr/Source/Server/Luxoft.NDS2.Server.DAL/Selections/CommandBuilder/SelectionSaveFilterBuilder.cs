﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionSaveFilterBuilder : SelectionPackageCommand
    {
        private readonly long _selectionId;
        private readonly Filter _filter;

        public SelectionSaveFilterBuilder(long selectionId, Filter filter)
            : base(DbConstants.SaveFilterProc)
        {
            _selectionId = selectionId;
            _filter = filter;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));
            parameters.Add(FormatCommandHelper.ClobIn(DbConstants.ParamFilter, _filter == null ? null : FilterHelper.Serialize(_filter)));

            return result;
        }
    }
}
