﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class DeleteSelectionsByTemplateBuilder : SelectionPackageCommand
    {
        private readonly long _templateId;
        private readonly string _userSid;

        public DeleteSelectionsByTemplateBuilder(long templateId)
            : base(DbConstants.DeleteSelectionsByTemplateProcedure)
        {
            _templateId = templateId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionTemplateId, _templateId));

            return result;
        }
    }
}