﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionGetFilterEditorParameterBuilder : SelectionPackageCommand
    {
        private readonly SelectionType _selectionType;

        public SelectionGetFilterEditorParameterBuilder(SelectionType selectionType)
            : base(DbConstants.GetFilterEditorParametersProcedure)
        {
            _selectionType = selectionType;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamIsManual, _selectionType == SelectionType.Hand ? 1 : 0));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamCursor));

            return result;
        }
    }
}