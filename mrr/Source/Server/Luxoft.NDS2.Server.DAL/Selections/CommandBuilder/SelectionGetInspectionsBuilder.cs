﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionGetInspectionsBuilder : SelectionPackageCommand
    {
        private readonly string _userSid;

        public SelectionGetInspectionsBuilder(string userSid)
            : base(DbConstants.GetInspectionsProcedure)
        {
            _userSid = userSid;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, _userSid));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.CursorParameter));

            return result;
        }
    }
}