﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionGetRegionBuilder : SelectionPackageCommand
    {
        private readonly string _userSid;

        public SelectionGetRegionBuilder(string userSid)
            : base(DbConstants.GetRegionsProcedure)
        {
            _userSid = userSid;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, _userSid));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.CursorParameter));

            return result;
        }
    }
}
