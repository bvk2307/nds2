﻿using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionUpdateRegionBuilder : SelectionPackageCommand
    {
        private readonly SelectionRegionalBoundsDataItem _regionalItem;
        private readonly long _templateId;

        public SelectionUpdateRegionBuilder(long templateId, SelectionRegionalBoundsDataItem item)
            : base(DbConstants.UpdateRegionalBoundsProcedure)
        {
            _templateId = templateId;
            _regionalItem = item;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionTemplateId, _templateId));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamInclude, _regionalItem.Include));
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParamRegionCode, _regionalItem.RegionCode));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamDiscrTotal, _regionalItem.DiscrTotalAmt));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamDiscrMin, _regionalItem.DiscrMinAmt));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamDiscrGapTotal, _regionalItem.DiscrGapTotalAmt));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamDiscrGapMin, _regionalItem.DiscrGapMinAmt));

            return result;
        }
    }
}