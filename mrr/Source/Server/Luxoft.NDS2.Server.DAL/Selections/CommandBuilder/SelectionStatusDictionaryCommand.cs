﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    class SelectionStatusDictionaryCommand : SelectionPackageCommand
    {
        public SelectionStatusDictionaryCommand()
            : base(DbConstants.SelectionStatusDictionaryProc)
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(DbConstants.CursorParameter));

            return result;
        }
    
    }
}
