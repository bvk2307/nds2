﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionFilterPeriodBuilder : SelectionPackageCommand
    {
        public SelectionFilterPeriodBuilder() :
            base(DbConstants.GetPeriodsTemplateProc)
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamCursor));
            return result;
        }
    }
}
