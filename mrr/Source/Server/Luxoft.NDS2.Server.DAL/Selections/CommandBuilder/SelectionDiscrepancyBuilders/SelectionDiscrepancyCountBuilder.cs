﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDiscrepancyBuilders
{
    internal class SelectionDiscrepancyCountBuilder : GetCountTemplateCommandBuilder
    {
        private const string SelectionScope = "Selection.Discrepancy";

        private const string SearchDiscrepancyCountSqlPatternBase = @"Count";

        private readonly long _selectionId;

        public SelectionDiscrepancyCountBuilder(
            FilterExpressionBase filterBy,
            long selectionId,
            IQueryTemplateProvider queryTemplate)
            : base(
                queryTemplate.GetQueryText(SelectionScope, SearchDiscrepancyCountSqlPatternBase),
                filterBy,
                new PatternProvider("vw"))
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));

            return result;
        }
    }
}