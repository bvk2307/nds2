﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDiscrepancyBuilders
{
    internal class SelectionDiscrepancySearchBuilder : SearchQueryCommandBuilder
    {
        private const string Scope = "Selection.Discrepancy";

        private const string PatternName = @"Search";

        private const string SelectionId = "pSelectionId";

        private const string RowsSkip = "rowsSkip";

        private const string RowsTop = "rowsTop";

        private readonly int _rowsToSkip;

        private int? _rowsToTake;

        private readonly long _selectionId;

        public SelectionDiscrepancySearchBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryTemplateProvider queryTemplate,
            long selectionId,
            int rowsToSkip,
            int rowsToTake)
            : base(
                queryTemplate.GetQueryText(Scope, PatternName),
                filterBy,
                orderBy,
                new PatternProvider("vw"))
        {
            _selectionId = selectionId;
            _rowsToSkip = rowsToSkip;
            _rowsToTake = rowsToTake;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(RowsSkip, _rowsToSkip);
            parameters.Add(RowsTop, _rowsToTake.Value + _rowsToSkip);
            parameters.Add(FormatCommandHelper.NumericIn(SelectionId, _selectionId));

            return result;
        }
    }
}