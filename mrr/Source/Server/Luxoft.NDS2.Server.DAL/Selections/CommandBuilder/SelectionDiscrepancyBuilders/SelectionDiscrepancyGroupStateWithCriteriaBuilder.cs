﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilders
{
    internal class SelectionDiscrepancyGroupStateWithCriteriaBuilder : GetTemplateCommandBuilder
    {
        private const string SelectionDiscrepancyScope = "Selection.Discrepancy";

        private const string GetGroupStateSqlTemplateName = "GroupStateSqlTemplate";

        private readonly long _selectionId;

        public SelectionDiscrepancyGroupStateWithCriteriaBuilder(
            FilterExpressionBase filterBy,
            long selectionId,
            IQueryTemplateProvider queryTemplate)
            : base(
                queryTemplate.GetQueryText(SelectionDiscrepancyScope, GetGroupStateSqlTemplateName),
                filterBy,
                new PatternProvider("vw"))
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamDisabledCount));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamCheckedCount));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamAllCount));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamInWorkedCount));

            return result;
        }
    }
}