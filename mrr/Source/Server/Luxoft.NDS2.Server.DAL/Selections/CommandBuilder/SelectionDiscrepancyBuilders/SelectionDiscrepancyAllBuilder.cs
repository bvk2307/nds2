﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDiscrepancyBuilders
{
    internal class SelectionDiscrepancyAllBuilder : SearchQueryCommandBuilder
    {
        private const string SelectionScope = "Selection.Discrepancy";

        private const string SearchPattern = @"All";

        private readonly long _selectionId;

        public SelectionDiscrepancyAllBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryTemplateProvider queryTemplate,
            long selectionId)
            : base(
                queryTemplate.GetQueryText(SelectionScope, SearchPattern),
                filterBy,
                orderBy,
                new PatternProvider("vw"))
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));

            return result;
        }
    }
}
