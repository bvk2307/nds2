﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionLoadBuilder : SelectionPackageCommand
    {
        private readonly long _selectionId;

        public SelectionLoadBuilder(long selectionId)
            : base(DbConstants.ProcedureLoadSelection)
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamId, _selectionId));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamCursor));

            return result;
        }
    }
}
