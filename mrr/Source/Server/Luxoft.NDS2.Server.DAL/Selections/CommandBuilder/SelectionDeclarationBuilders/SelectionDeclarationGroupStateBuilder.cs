﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders
{
    internal class SelectionDeclarationGroupStateBuilder : SelectionPackageCommand
    {
        private readonly long _selectionId;

        public SelectionDeclarationGroupStateBuilder(long selectionId)
            : base(DbConstants.ProcedureCountCheckedDeclarations)
        {
            _selectionId = selectionId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamCheckedCount));
            parameters.Add(FormatCommandHelper.NumericOut(DbConstants.ParamAllCount));

            return result;
        }
    }
}