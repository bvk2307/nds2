﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders
{
    internal class SelectionDeclarationSetStateBuilder : SelectionPackageCommand
    {
        private readonly long _declarationId;

        private readonly long _selectionId;
        private readonly bool _state;
        private readonly string _userSid;


        public SelectionDeclarationSetStateBuilder(long selectionId, long declarationId, bool state, string userSid)
            : base(DbConstants.ProcedureUpdateCheckState)
        {
            _selectionId = selectionId;
            _declarationId = declarationId;
            _state = state;
            _userSid = userSid;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, _selectionId));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamDeclarationId, _declarationId));
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, _userSid));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamCheckState, _state ? 1 : 0));

            return result;
        }
    }
}