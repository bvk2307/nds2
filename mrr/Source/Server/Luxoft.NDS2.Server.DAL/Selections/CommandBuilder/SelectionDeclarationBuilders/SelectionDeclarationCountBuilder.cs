﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders
{
    internal class SelectionDeclarationCountBuilder : GetCountCommandBuilder
    {
        public static string SelectionDeclarationListViewName = "V$SELECTION_DECLARATION vw";

        public SelectionDeclarationCountBuilder(
            FilterExpressionBase filterBy,
            IQueryTemplateProvider queryTemplate)
            : base(
                SelectionDeclarationListViewName.CountPattern(),
                filterBy,
                new PatternProvider("vw"))
        {
        }
    }
}
