﻿using System.Collections.Generic;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders
{
    internal class SelectionSetStateBuilder : SelectionPackageCommand
    {

        private readonly long _selectionId;
        private readonly bool _state;
        private readonly long[] _zipList;
        private readonly string _userSid;


        public SelectionSetStateBuilder(
            long selectionId,
            bool state,
            string userSid, 
            long[] zipList)
            : base(DbConstants.ProcedureUpdateAllDeclarationsCheckState)
        {
            _selectionId = selectionId;
            _state = state;
            _userSid = userSid;
            _zipList = zipList;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId , _selectionId));
            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamCheckState, _state ? 1 : 0));
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, _userSid));
            parameters.Add(FormatCommandHelper.LongArrayIn(DbConstants.ParamZipList, _zipList == null || _zipList.Length == 0 ? new long[1] { 0 } : _zipList));
       
            return result;
        }
    }
}
