﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders
{
    internal class SelectionDeclarationSearchBuilder : PageSearchQueryCommandBuilder
    {
        public static string SelectionDeclarationListViewName = "V$SELECTION_DECLARATION vw";

        public SelectionDeclarationSearchBuilder(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            IQueryTemplateProvider queryTemplate)
            : base(
                SelectionDeclarationListViewName.SelectPattern(),
                filterBy,
                orderBy,
                new PatternProvider("vw"))
        {
        }
    }
}
