﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionGetFilterParameterOptionsBuilder : SelectionPackageCommand
    {
        private readonly int _parameterId;

        public SelectionGetFilterParameterOptionsBuilder(int parameterId)
            : base(DbConstants.GetFilterEditorParameterOptionsProcedure)
        {
            _parameterId = parameterId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamAttributeId, _parameterId));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamCursor));

            return result;
        }
    }
}