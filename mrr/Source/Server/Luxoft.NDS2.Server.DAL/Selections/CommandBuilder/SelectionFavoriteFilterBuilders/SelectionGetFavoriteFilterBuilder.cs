﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders
{
    internal class SelectionGetFavoriteFilterBuilder : SelectionPackageCommand
    {
        private readonly long _favoriteId;

        public SelectionGetFavoriteFilterBuilder(long favoriteId)
            : base(DbConstants.ProcedureGetFavoriteFilter)
        {
            _favoriteId = favoriteId;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamFavoriteFilterId, _favoriteId));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamFavoriteFilterCursor));

            return result;
        }
    }
}