﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders
{
    internal class SelectionAllFavoriteFilterBuilder : SelectionPackageCommand
    {
        public SelectionAllFavoriteFilterBuilder()
            : base(DbConstants.ProcedureGetAllFavoriteFilters)
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamFavoriteFilterCursor));

            return result;
        }
    }
}