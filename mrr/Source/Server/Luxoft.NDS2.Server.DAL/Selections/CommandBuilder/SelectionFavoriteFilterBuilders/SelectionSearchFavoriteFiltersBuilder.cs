﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders
{
    internal class SelectionSearchFavoriteFiltersBuilder : SelectionPackageCommand
    {
        private readonly string _owner;

        public SelectionSearchFavoriteFiltersBuilder(string owner)
            : base(DbConstants.ProcedureSearchFavoriteFilters)
        {
            _owner = owner;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParamFavoriteFilterPAnalyst, _owner));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamFavoriteFilterCursor));

            return result;
        }
    }
}