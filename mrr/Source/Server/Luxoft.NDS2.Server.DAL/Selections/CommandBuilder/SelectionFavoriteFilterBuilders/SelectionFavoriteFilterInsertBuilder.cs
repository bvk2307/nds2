﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders
{
    internal class SelectionFavoriteFilterInsertBuilder : SelectionPackageCommand
    {
        private readonly FavoriteFilter _filter;

        public SelectionFavoriteFilterInsertBuilder(FavoriteFilter filter)
            : base(DbConstants.ProcedureSaveFavoriteFilter)
        {
            _filter = filter;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.IdInOut());
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParamFavoriteFilterPName, _filter.Name));
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParamFavoriteFilterPAnalyst, _filter.Analyst));
            parameters.Add(FormatCommandHelper.NClobIn(DbConstants.ParamFavoriteFilterPFilter,
                FilterHelper.Serialize(_filter.FilterVersionTwo)));

            return result;
        }
    }
}