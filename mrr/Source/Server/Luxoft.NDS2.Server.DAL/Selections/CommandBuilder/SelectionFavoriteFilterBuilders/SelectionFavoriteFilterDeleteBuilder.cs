﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders
{
    internal class SelectionFavoriteFilterDeleteBuilder : SelectionPackageCommand
    {
        private readonly FavoriteFilter _filter;

        public SelectionFavoriteFilterDeleteBuilder(FavoriteFilter filter)
            : base(DbConstants.ProcedureDeleteFavoriteFilter)
        {
            _filter = filter;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.IdIn(_filter.Id));

            return result;
        }
    }
}