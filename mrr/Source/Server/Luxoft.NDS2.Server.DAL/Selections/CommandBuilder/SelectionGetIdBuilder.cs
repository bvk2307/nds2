﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    internal class SelectionGetIdBuilder : SelectionPackageCommand
    {
        private readonly long _templateId;
        private readonly string _region;
        public SelectionGetIdBuilder(long templateId, string region)
            : base(DbConstants.GetSelectionIdProcedure)
        {
            _templateId = templateId;
            _region = region;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DbConstants.ParamSelectionTemplateId, _templateId));
            parameters.Add(FormatCommandHelper.VarcharIn(DbConstants.ParamRegions, _region));
            parameters.Add(FormatCommandHelper.Cursor(DbConstants.ParamCursor));

            return result;
        }
    }
}