﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.Selections.CommandBuilder
{
    class SelectionPackageCommand : ExecuteProcedureCommandBuilder
    {
        public SelectionPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", DbConstants.SelectionPackageName, procedureName))
        {
        }
    }
}
