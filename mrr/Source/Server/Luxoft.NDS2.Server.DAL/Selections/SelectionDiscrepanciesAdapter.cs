﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilders;
using Luxoft.NDS2.Server.DAL.Selections.DataMappers;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDiscrepancyBuilders;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    internal class SelectionDiscrepanciesAdapter : ISelectionDiscrepanciesAdapter
    {
        private readonly IServiceProvider _service;

        public SelectionDiscrepanciesAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public IEnumerable<SelectionDiscrepancy> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId,
            int rowsToSkip,
            int rowsToTake)
        {
            return new ListCommandExecuter<SelectionDiscrepancy>(
                new GenericDataMapper<SelectionDiscrepancy>(),
                _service
                ).TryExecute(
                    new SelectionDiscrepancySearchBuilder(
                        searchCriteria,
                        orderBy,
                        _service,
                        selectionId,
                        rowsToSkip,
                        rowsToTake)).ToList();
        }

        public IEnumerable<SelectionDiscrepancy> All(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId)
        {
            return new ListCommandExecuter<SelectionDiscrepancy>(
                new GenericDataMapper<SelectionDiscrepancy>(),
                _service
                ).TryExecute(
                    new SelectionDiscrepancyAllBuilder(searchCriteria, orderBy, _service,
                        selectionId));
        }

        public void ForAll(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId,
            Func<SelectionDiscrepancy, bool> processAction)
        {
            new ListCommandFetcher<SelectionDiscrepancy>(
                new GenericDataMapper<SelectionDiscrepancy>(),
                _service,
                processAction
                )
                .TryExecute(
                    new SelectionDiscrepancyAllBuilder(searchCriteria, orderBy, _service,
                        selectionId));
        }


        public int Count(FilterExpressionBase searchCriteria, long selectionId)
        {
            return new CountCommandExecuter(_service)
                .TryExecute(new SelectionDiscrepancyCountBuilder(searchCriteria, selectionId, _service));
        }
    }
}
