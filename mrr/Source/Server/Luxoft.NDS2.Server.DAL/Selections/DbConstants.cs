﻿namespace Luxoft.NDS2.Server.DAL.Selections
{
    public static class DbConstants
    {
        public static string SelectionPackageName = "PAC$SELECTION";
        public static string SelectionListViewName = "V$SELECTION_LIST";
        public static string SelectionDeclarartionViewName = "V$SELECTION_DECLARATION";
        public static string SelectionDiscrepancyViewName = "V$SELECTION_DISCREPANCY";
        public static string SelectionRegionalBoundsViewName = "V$SELECTION_REGIONAL_BOUNDS";
        
        public static string SelectionStatusDictionaryProc = "P$SELECT_STATUS_DICTIONARY";
        public static string ExcludeReasonDictionaryProc = "P$EXCLUDE_REASON_DICTIONARY";
        public static string GetFilterProc = "P$GET_FILTER";
        public static string SaveFilterProc = "P$SAVE_FILTER";
        public static string GetPeriodsTemplateProc = "P$GET_PERIODS_TEMPLATE";
        public static string ProcedureGetSelectionStatus = "P$GET_SELECTION_STATUS";
        public static string ProcedureGetSelectionRequestStatus = "P$GET_SEL_REQUEST_STATUS";
        public static string ProcedureLoadSelection = "P$LOAD_SELECTION";
        public static string ProcedureSelectionSetState = "P$SELECTION_SET_STATE";
        public static string ProcedureApprove = "P$APPROVE";
        public static string ProcedureSendForCorrection = "P$SEND_FOR_CORRECTION";
        public static string ProcedureSaveSelection = "P$SAVE";
        public static string FunctionGenerateUniqueName = "F$GenerateUniqueName";
        public static string FunctionVerifySelectionName = "F$VERIFY_SELECTION_NAME";
        public static string ProcedureGetActionsHistry = "P$GET_ACTION_HISTORY";
        public static string SearchProcedure = "P$GET_SELECTION_TRANSITIONS";
        public static string CreateRequestProcedure = "P$CREATE_REQUEST";
        public static string GetRegionsProcedure = "P$GET_REGIONS";
        public static string GetInspectionsProcedure = "P$GET_INSPECTIONS";
        public static string GetFilterEditorParametersProcedure = "P$GET_FILTER_EDITOR_PARAMETERS";
        public static string GetFilterEditorParameterOptionsProcedure = "P$GET_FILTER_PARAMETER_OPTIONS";
        public static string ProcedureGetFilterParameters = "P$GET_FILTER_PARAMETERS";
        public static string ProcedureCountCheckedDeclarations = "P$COUNT_CHECKED_DECLARATIONS";
        public static string ProcedureUpdateCheckState = "P$UPDATE_DECL_CHECK_STATE";
        public static string ProcedureUpdateAllDeclarationsCheckState = "p$UPDATE_ALL_DECL_CHECK_STATE";
        public static string ProcedureCountCheckDiscrepancies = "P$COUNT_CHECKED_DISCREPANCIES";
        public static string ProcedureGetAllFavoriteFilters = "P$GET_ALL_FAVORITE_FILTERS";
        public static string ProcedureDeleteFavoriteFilter = "P$DELETE_FAVORITE_FILTER";
        public static string ProcedureSaveFavoriteFilter = "P$SAVE_FAVORITE_FILTER";
        public static string ProcedureSearchFavoriteFilters = "P$GET_FAVORITE_FILTERS";
        public static string ProcedureGetFavoriteFilter = "P$GET_FAVORITE_FILTER";
        public static string SearchRegionsProcedure = "P$SEARCH_REGIONS";
        public static string SearchInspectionsProcedure = "P$SEARCH_INSPECTIONS";
        public static string UpdateRegionalBoundsProcedure = "P$UPDATE_REGIONAL_BOUNDS";
        public static string DeleteSelectionsByTemplateProcedure = "P$DELETE_SEL_BY_TEMPLATE";
        public static string GetSelectionIdProcedure = "P$GET_SEL_BY_TEMPL_ID";
        public static string UpdateSelectionAggregateProcedure = "P$UPDATE_AGGREGATE";
        public static string GetRegionalBoundsProcedure = "P$GET_REGIONAL_BOUND";
        public static string CanDeleteTemplateProcedure = "P$CAN_DELETE_TEMPLATE";
        public static string UpdateSelectionRegionsProcedure = "P$UPDATE_SEL_REGIONS";


        public static string CursorParameter = "pData";
        public static string ParamFilter = "pFilter";
        public static string ParamId = "pId";
        public static string ParamZipList = "pZipList";
        public static string ParamSelectionId = "pSelectionId";
        public static string ParamSelectionType = "pSelectionType";
        public static string ParamSelectionTemplateId = "pSelectionTemplateId";
        public static string ParamCursor = "pCursor";
        public static string ParameterStatusId = "pStatusId";
        public static string ParameterStatus = "pStatus";
        public static string ParameterStatusDate = "pStatusDate";
        public static string ParameterUserSid = "pUserSid";
        public static string ParameterUser = "pUser";
        public static string ParameterName = "pName";
        public static string ParameterTypeName = "pType";
        public static string ParameterCreatedAt = "pCreatedAt";
        public static string ParameterLastStatusChangedAt = "pLastStatusChangedAt";
        public static string ParameterDeclQty = "pDeclQty";
        public static string ParameterDiscrQty = "pDiscrQty";
        public static string ParameterDiscrAmt = "pDiscrAmt";
        public static string ParameterLastEditedBy = "pLastEditedBy";
        public static string ParameterApprovedBy = "pApprovedBy";
        public static string ParameterLastEditedBySid = "pLastEditedBySid";
        public static string ParameterApprovedBySid = "pApprovedBySid";
        public static string ParamActionType = "pAction";
        public static string ParamQueryText = "pQuery";
        public static string ParamIsManual = "pIsManual";
        public static string ParamAttributeId = "pAttributeId";
        public static string ParamCheckedCount = "pChecked";
        public static string ParamAllCount = "pAll";
        public static string ParamDeclarationId = "pDeclarationId";
        public static string ParamDiscrepancyId = "pDiscrepancyId";
        public static string ParamCheckState = "pCheckState";
        public static string ParamDisabledCount = "pDisabled";
        public static string ParamInWorkedCount = "pInWorked";
        public static string ParamSearchKey = "pSearchKey";
        public static string ParamMaxQuantity = "pMaxQuantity";
        public static string ParamInclude = "pInclude";
        public static string ParamRegionCode = "pRegionCode";
        public static string ParamRegions = "pRegions";
        public static string ParamDiscrTotal = "pDiscrTotal";
        public static string ParamDiscrMin = "pDiscrMin";
        public static string ParamDiscrGapTotal = "pDiscrGapTotal";
        public static string ParamDiscrGapMin = "pDiscrGapMin";
        public static string ParamCanDelete = "pCanDelete";

        public static string ParamFavoriteFilterId = "ID";
        public static string ParamFavoriteFilterCursor = "PFAVCURSOR";
        public static string ParamFavoriteFilterPName = "PNAME";
        public static string ParamFavoriteFilterPFilter = "PFILTER";
        public static string ParamFavoriteFilterPAnalyst = "PANALYST";
        public static string ParamTransitionsStateFromId = "STATE_FROM";
        public static string ParamTransitionsStateToId = "STATE_TO";
        public static string ParamTransitionsOperation = "OPERATION";

        public static string StatusField = "STATUS";
        public static string RequestIdField = "REQUEST_ID";
        public static string SelectionIdField = "SELECTION_ID";
        public static string FieldCode = "S_CODE";
        public static string FieldParentCode = "S_PARENT_CODE";
        public static string FieldName = "S_NAME";
        public static string FieldDescription = "DESCRIPTION";

        public static string SelectionId = "SelectionId";
        public static string SelectionName = "SelectionName";
        public static string InProcessQty = "InProcessQty";
        public static string TotalQty = "TotalQty";


    }
}
