﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    class LongMapper : IDataMapper<long>
    {
        public long MapData(IDataRecord reader)
        {
            return reader.ReadInt64("value");
        }
    }

    public class SelectionAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public SelectionAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public Selection FindByTemplateId(long templateId, string region)
        {
            return new ListCommandExecuter<Selection>(
             new GenericDataMapper<Selection>()
             .WithFieldMapperFor(TypeHelper<Selection>.GetMemberName(x => x.Status),
                            new NullableEnumFieldMapper(typeof(SelectionStatus)))
                             .WithFieldMapperFor(TypeHelper<Selection>.GetMemberName(x => x.TypeCode),
                            new IntFieldMapper()), 
             _service,
             _connection).TryExecute(
                 new SelectionGetIdBuilder(templateId, region)).FirstOrDefault();
        }

        public SelectionRegionalBoundsDataItem GetRegionalBounds(long templateId, string region)
        {
            return new ListCommandExecuter<SelectionRegionalBoundsDataItem>(
             new GenericDataMapper<SelectionRegionalBoundsDataItem>()
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrTotalAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrMinAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrGapTotalAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrGapMinAmt),
                            new UnsignedIntFieldMapper()),
             _service,
             _connection).TryExecute(
                 new GetRegionalBoundsBuilder(templateId, region)).FirstOrDefault();
        }
    }
}