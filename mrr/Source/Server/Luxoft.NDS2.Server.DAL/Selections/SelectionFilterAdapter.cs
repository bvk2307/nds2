﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Selections.DataMappers;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.SystemSettings.CommandBuilder;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    internal class SelectionFilterAdapter : ISelectionFilterAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public SelectionFilterAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<SelectionParameter> GetFilterParameters()
        {
            return new ListCommandExecuter<SelectionParameter>(
                new SelectionFilterParameterDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetFilterParametersBuilder());
        }

        public IEnumerable<EditorParameter> GetFilterEditorParameters(SelectionType selectionType)
        {
            return new ListCommandExecuter<EditorParameter>(
                new SelectionFilterEditorParameterDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetFilterEditorParameterBuilder(selectionType));
        }

        public IEnumerable<KeyValuePair<string, string>> GetFilterParameterOptions(int parameterId)
        {
            return new ListCommandExecuter<FilterOption>(
                new SelectionFilterParamterOptionsDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetFilterParameterOptionsBuilder(parameterId))
                    .Select(p => new KeyValuePair<string, string>(p.Value, p.Name)); 
        }

        public IEnumerable<KeyValuePair<string, string>> GetPeriods()
        {
            return new ListCommandExecuter<FilterOption>(
                new SelectionFilterPeriodMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionFilterPeriodBuilder())
                    .Select(p => new KeyValuePair<string, string>(p.Value, p.Name));
        }
        
        public IEnumerable<Region> GetRegions(string userSid)
        {
            var regions = new ListCommandExecuter<Region>(
                new SelectionGetRegionsDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetRegionBuilder(userSid)).ToList();

            var inspections = new ListCommandExecuter<Inspection>(
                new SelectionGetInspectionsDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetInspectionsBuilder(userSid)).ToList();

            foreach (var inspection in inspections)
            {
                var region = regions.FirstOrDefault(
                    p => p != null && p.Code != null && 
                    inspection != null && inspection.Code != null && 
                    p.Code == inspection.Code.Substring(0, 2));
                if (region != null)
                    region.Inspections.Add(inspection);
            }

            return regions.ToArray();
        }

        /// <summary>
        /// Возвращает только активные списки
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, string>> GetWhiteLists()
        {
            var sqlExecuter = new ListCommandExecuter<SystemSettingsList>(
                   new GenericDataMapper<SystemSettingsList>(),
                   _service,
                   _connectionFactory);
            var commandBuilder = new ActiveWhiteListsCommandBuilder();
            return
                sqlExecuter.TryExecute(commandBuilder)
                    .Select(x => new KeyValuePair<string, string>(x.Id.ToString(), x.Name));
        }
   
        public void SaveFilter(long selectionId, Filter filter)
        {
            new ResultCommandExecuter(
                _service,
                _connectionFactory).TryExecute(
                    new SelectionSaveFilterBuilder(selectionId, filter));
        }

        public Filter GetSelectionFilterContent(long selectionId)
        {
            return new ListCommandExecuter<Filter>(
                new SelectionGetFilterContentDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetFilterContentBuilder(selectionId)).SingleOrDefault();
        }
    }
}
