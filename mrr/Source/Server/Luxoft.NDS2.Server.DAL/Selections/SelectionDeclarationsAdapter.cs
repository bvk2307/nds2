﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilders;
using Luxoft.NDS2.Server.DAL.Selections.DataMappers;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionDeclarationBuilders;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    internal class SelectionDeclarationsAdapter : ISelectionDeclarationsAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public SelectionDeclarationsAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<SelectionDeclaration> Search(
            FilterExpressionBase searchCriteria, 
            IEnumerable<ColumnSort> orderBy, 
            int rowsToSkip, 
            int? rowsToTake)
        {
            return new ListCommandExecuter<SelectionDeclaration>(
                new SelectionDeclarationDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionDeclarationSearchBuilder(searchCriteria, orderBy, _service)
                    .Skip(rowsToSkip)
                    .Take(rowsToTake)).ToList();
        }

        public int Count(FilterExpressionBase searchCriteria)
        {
            return new CountCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new SelectionDeclarationCountBuilder(searchCriteria, _service));
        }

        public SelectionGroupState GetDeclarationState(long selectionId)
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new SelectionDeclarationGroupStateBuilder(selectionId));

            return SelectionGroupStateData.State(
                    SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamCheckedCount]),
                    SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamAllCount]));
        }

        public void SetDeclarationState(long selectionId, long declarationId, bool state, string userSid)
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new SelectionDeclarationSetStateBuilder(selectionId, declarationId, state, userSid));
        }

        public void SetState(long selectionId, bool state, string userSid, long[] zipList )
        {
            var values = new ResultCommandExecuter(
                    _service,
                    _connectionFactory).TryExecute(
                        new SelectionSetStateBuilder(selectionId, state, userSid, zipList));
        }
    }
}
