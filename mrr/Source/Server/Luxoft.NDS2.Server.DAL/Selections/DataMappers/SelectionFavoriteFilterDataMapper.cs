﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionFavoriteFilterDataMapper : IDataMapper<FavoriteFilter>
    {
        private const string ParamAnalyst = "ANALYST";
        private const string ParamFavoriteFilterId = "ID";
        private const string ParamFavoriteFilterName = "NAME";
        private const string ParamFavoriteFilterFilter = "FILTER";

        public FavoriteFilter MapData(IDataRecord reader)
        {
            var result = new FavoriteFilter();

            result.Analyst = reader[ParamAnalyst].ToString();
            result.Id = long.Parse(reader[ParamFavoriteFilterId].ToString());
            result.Name = reader[ParamFavoriteFilterName].ToString();
            //TODO этот устаревший филльтр нужен только для конвертации (Build <- All <- Converter)
            var filterResultVersionOne = SelectionFilter.Deserialize(reader[ParamFavoriteFilterFilter].ToString());
            result.Filter = filterResultVersionOne.SelectionFilter;
            var filterResultVersionTwo = FilterHelper.Deserialize(reader[ParamFavoriteFilterFilter].ToString());
            result.FilterVersionTwo = filterResultVersionTwo.Filter;

            if (filterResultVersionOne.IsSuccess)
                result.FilterVersion = SelectionFilterVersion.VersionOne;
            if (filterResultVersionTwo.IsSuccess)
                result.FilterVersion = SelectionFilterVersion.VersionTwo;

            return result;
        }
    }
}
