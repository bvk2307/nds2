﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionGetRegionsDataMapper : IDataMapper<Region>
    {
        public Region MapData(IDataRecord reader)
        {
            var result = new Region();

            result.Code = reader.GetString("S_CODE");
            result.Name = reader.GetString("S_NAME");
            result.RegionFullName = reader.GetString("DESCRIPTION");
            result.Inspections = new List<Inspection>();

            return result;
        }
    }
}
