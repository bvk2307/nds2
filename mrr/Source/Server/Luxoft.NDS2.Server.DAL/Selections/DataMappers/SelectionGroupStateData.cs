﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilders;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal static class SelectionGroupStateData
    {
        public static SelectionGroupStateResult CreateSelectionGroupStateResult(Dictionary<string, object> values)
        {
            var groupStateResult = new SelectionGroupStateResult
            {
                SelectionGroupState = State(
                    SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamCheckedCount]),
                    SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamAllCount]),
                    SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamDisabledCount])),
                DiscrepancyInWorkedCount = SelectionDataMapperHelper.ReadInt(values[DbConstants.ParamInWorkedCount])
            };
            return groupStateResult;
        }

        public static SelectionGroupState State(int included, int all, int disabled = 0)
        {
            if (all == 0 || (all == disabled && included == 0))
            {
                return SelectionGroupState.AllExcludedAndDisabled;
            }

            if (included == 0)
            {
                return SelectionGroupState.AllExcluded;
            }

            if (all == included)
            {
                return SelectionGroupState.AllIncluded;
            }

            return SelectionGroupState.PartiallyIncluded;
        }
    }
}
