﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionGetInspectionsDataMapper : IDataMapper<Inspection>
    {
        public Inspection MapData(IDataRecord reader)
        {
            var result = new Inspection();

            result.Code = reader.GetString("S_CODE");
            result.Name = reader.GetString("S_NAME");

            return result;
        }
    }
}
