﻿using Luxoft.NDS2.Server.DAL.Packages;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionFilterPeriodMapper : IDataMapper<FilterOption>
    {
        public FilterOption MapData(IDataRecord reader)
        {
            var r = new FilterOption();
            r.Name = reader.GetString("PERIOD");
            r.Value = reader.GetString("PERIOD_CODE");
            return r;
        }
    }
}
