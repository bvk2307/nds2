﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionDataMapper : IDataMapper<Selection>
    {
        public Selection MapData(IDataRecord reader)
        {
            var result = new Selection();
            result.Id = reader.GetInt64(reader.GetOrdinal("Id"));
            result.Analytic = reader.GetString("Analytic");
            result.AnalyticSid = reader.GetString("AnalyticSid");
            result.Manager = reader.GetString("Manager");
            result.ManagerSid = reader.GetString("ManagerSid");
            result.CreationDate = reader.GetDateTime("CreationDate");
            result.ModificationDate = reader.GetDateTime("ModificationDate");
            result.Name = reader.GetString("Name");
            result.Status = (SelectionStatus?)reader.GetNullableInt32("Status");
            result.DeclarationCount = reader.GetInt32("DeclarationCount");
            result.DiscrepanciesCount = reader.GetInt32("DiscrepanciesCount");
            result.StatusDate = reader.GetDateTime("StatusDate");
            result.Type = reader.GetString("Type");
            result.TypeCode = (SelectionType)reader.GetInt32("TypeCode");
            result.TotalAmount = reader.GetDecimal("TotalAmount");
            result.Regions = reader.GetString("Regions");
            result.TemplateId = reader.GetNullableInt64("TemplateId");
            return result;
        }
    }
}
