﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionFilterParameterDataMapper : IDataMapper<SelectionParameter>
    {
        public SelectionParameter MapData(IDataRecord reader)
        {
            var result = new SelectionParameter();

            result.Id = reader.GetInt32(reader.GetOrdinal("Id"));
            result.Name = reader.GetString("Name");

            return result;
        }
    }
}
