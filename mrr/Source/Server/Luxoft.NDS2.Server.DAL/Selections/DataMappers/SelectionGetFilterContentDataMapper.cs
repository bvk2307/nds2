﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionGetFilterContentDataMapper : IDataMapper<Filter>
    {
        public Filter MapData(IDataRecord reader)
        {
            var result = new Filter();

            result = FilterHelper.Deserialize(reader.GetString("filter")).Filter;

            return result;
        }
    }
}
