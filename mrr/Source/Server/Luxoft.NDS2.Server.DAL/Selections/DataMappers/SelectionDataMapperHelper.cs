﻿
namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal static class SelectionDataMapperHelper
    {
        public static int ReadInt(object value)
        {
            int retVal;

            if (!int.TryParse(value.ToString(), out retVal))
            {
                return 0;
            }

            return retVal;
        }
    }
}
