﻿using Luxoft.NDS2.Server.DAL.Packages;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionFilterParamterOptionsDataMapper  : IDataMapper<FilterOption>
    {
        public FilterOption MapData(IDataRecord reader)
        {
            var result = new FilterOption();

            result.Name = reader.GetString("Name");
            result.Value = reader.GetString("Value");

            return result;
        }
    }
}
