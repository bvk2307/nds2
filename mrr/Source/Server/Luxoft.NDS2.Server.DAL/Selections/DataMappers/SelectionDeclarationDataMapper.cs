﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionDeclarationDataMapper : IDataMapper<SelectionDeclaration>
    {
        public SelectionDeclaration MapData(IDataRecord reader)
        {
            var result = new SelectionDeclaration();

            result.Zip = reader.GetInt64(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Zip));

            result.Included = reader.GetBoolean(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Included));

            result.IsActual = reader.GetBoolean(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.IsActual));

            result.TypeId = reader.GetInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TypeId));

            result.Sur = reader.GetNullableInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Sur));

            result.Inn = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Inn));
            
            result.Kpp = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Kpp));
            
            result.Name = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Name));

            result.InnReorganized = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.InnReorganized));

            result.RegNumber = reader.GetInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.RegNumber));

            result.DeclarationSign = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DeclarationSign));

            result.NdsAmount =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.NdsAmount));

            result.DiscrepancyCount =
                reader.GetNullableInt64(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyCount));

            result.DiscrepancyAmountChapter8 =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountChapter8));

            result.DiscrepancyAmountChapter9 =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountChapter9));

            result.DiscrepancyAmountTotal =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountTotal));

            result.DiscrepancyAmountMin =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountMin));

            result.DiscrepancyAmountMax =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountMax));

            result.DiscrepancyAmountAvg =
                reader.GetNullableDecimal(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DiscrepancyAmountAvg));

            result.TaxPeriod = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TaxPeriod));

            result.DeclarationVersion = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.DeclarationVersion));

            result.SubmitDate = reader.GetNullableDateTime(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.SubmitDate));

            result.SonoCode = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.SonoCode));

            result.SonoName = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.SonoName));

            result.RegionCode = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.RegionCode));

            result.RegionName = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.RegionName));
            
            result.Inspector = reader.GetString(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Inspector));

            result.StatusCode = reader.GetInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.StatusCode));

            result.SubmitDate = reader.GetNullableDateTime(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.SubmitDate));

            result.CalcStatus = reader.GetInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.CalcStatus));

            result.Category = reader.GetBoolean(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.IsActual));

            result.ExcludeFromClaimType = reader.GetInt32(TypeHelper<SelectionDeclaration>.GetMemberName(t => t.ExcludeFromClaimType));

            return result;
        }
    }
}
