﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Selections.DataMappers
{
    internal class SelectionFilterEditorParameterDataMapper : IDataMapper<EditorParameter>
    {
        public EditorParameter MapData(IDataRecord reader)
        {
            var result = new EditorParameter();

            result.Id = reader.GetInt32("Id");
            result.Name = reader.GetString("Name");
            result.AggregateName = reader.GetString("aggregate_name");
            result.AreaId = reader.GetInt32("AreaId");
            result.TypeId = reader.GetInt32("TypeId");
            result.ModelId = reader.GetInt32("ModelId");

            result.Area = (EditorArea)result.AreaId;
            result.ViewType = (EditorType)result.TypeId;
            result.ModelType = (ParameterType)result.ModelId;

            return result;
        }
    }
}
