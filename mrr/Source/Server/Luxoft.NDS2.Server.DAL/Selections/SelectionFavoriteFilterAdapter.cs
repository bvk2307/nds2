﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Selections.DataMappers;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder.SelectionFavoriteFilterBuilders;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    internal class SelectionFavoriteFilterAdapter : ISelectionFavoriteFilterAdpater
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connectionFactory;

        public SelectionFavoriteFilterAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public List<FavoriteFilter> Search(string owner)
        {
            return new ListCommandExecuter<FavoriteFilter>(
                new SelectionFavoriteFilterDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionSearchFavoriteFiltersBuilder(owner)).ToList();
        }

        public FavoriteFilter Get(long favoriteId)
        {
            return new ListCommandExecuter<FavoriteFilter>(
                new SelectionFavoriteFilterDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionGetFavoriteFilterBuilder(favoriteId)).SingleOrDefault();
        }

        public List<FavoriteFilter> All()
        {
            return new ListCommandExecuter<FavoriteFilter>(
                new SelectionFavoriteFilterDataMapper(),
                _service,
                _connectionFactory).TryExecute(
                    new SelectionAllFavoriteFilterBuilder()).ToList();
        }

        public void Insert(FavoriteFilter filter)
        {
            new ResultCommandExecuter(_service, _connectionFactory)
                .WithErrorHandler(new UniqueKeyViolationErrorHandler())
                .TryExecute(new SelectionFavoriteFilterInsertBuilder(filter));
        }

        public void Update(FavoriteFilter filter)
        {
            new ResultCommandExecuter(
                _service,
                _connectionFactory).TryExecute(
                    new SelectionFavoriteFilterUpdateBuilder(filter));
        }

        public void Delete(FavoriteFilter filter)
        {
            new ResultCommandExecuter(
                _service,
                _connectionFactory).TryExecute(
                    new SelectionFavoriteFilterDeleteBuilder(filter));
        }
    }
}
