﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    class DiscrepAmountExpressionWriter : FilterExpressionWriter
    {
        public DiscrepAmountExpressionWriter(List<EditorParameter> editorParameters) : base(editorParameters)
        {
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParamater = GetEditorParameter(filter.AttributeId);

            if (editorParamater.AggregateName != AggregateColumnNames.TypeCode)
            {
                return false;
            }

            int i = 0;
            foreach (var filterVal in filter.Values)
            {
                if (filter.Values.Length > 1 && i > 0)
                    writer.Append(" or ");

                writer.AppendFormat("(dis.{0}={1} and dis.{2}>={3} and dis.{4}>={5})",
                editorParamater.AggregateName,
                filterVal.Value,
                int.Parse(filterVal.Value) == (int)DiscrepancyType.Break ? AggregateColumnNames.DeclTotalAmountGap : AggregateColumnNames.DeclTotalAmountNds,
                filterVal.ValueFrom??"0",
                AggregateColumnNames.DeclMinAmount,
                filterVal.ValueTo??"0");
                i++;
            }

            return true;
        }
    }
}
