﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    class ContractorZipExpressionWriter : FilterExpressionWriter
    {
        public ContractorZipExpressionWriter(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParamater = GetEditorParameter(filter.AttributeId);

            if (editorParamater.AggregateName != AggregateColumnNames.SellerZip)
            {
                return false;
            }

            writer.AppendFormat("dis.{0} is {1}null", editorParamater.AggregateName, filter.Values.First().Value == "0" ? "not " : string.Empty);

            return true;
        }
    }
}
