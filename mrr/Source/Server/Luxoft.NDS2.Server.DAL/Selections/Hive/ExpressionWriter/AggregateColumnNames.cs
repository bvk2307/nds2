﻿
namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public static class AggregateColumnNames
    {
        #region Сторона покупателя

        /// <summary>
        /// Код региона
        /// </summary>
        public const string RegionCode = "region_code";

        /// <summary>
        /// Код инспекции
        /// </summary>
        public const string SonoCode = "sono_code";

        /// <summary>
        /// ИНН покупателя
        /// </summary>
        public const string BuyerInn = "decl_inn";

        /// <summary>
        /// СУР НП
        /// </summary>
        public const string SurCode = "sur_code";

        /// <summary>
        /// Крупнейший
        /// </summary>
        public const string Category = "category";

        /// <summary>
        /// Отчетный период с годом
        /// </summary>
        public const string PeriodFull = "period_full";

        /// <summary>
        /// Дата подачи декларации
        /// </summary>
        public const string SubmitDate = "submit_date";

        /// <summary>
        /// Признак НД
        /// </summary>
        public const string DeclSign = "decl_sign";

        /// <summary>
        /// Сумма по НД покупателя
        /// </summary>
        public const string Side1NdsAmount = "decl_nds";

        #endregion

        #region Сторона продавца



        /// <summary>
        /// Регион продавцв
        /// </summary>
        public const string SellerRegionCode = "contractor_region_code";

        /// <summary>
        /// Код инспекции
        /// </summary>
        public const string SellerSonoCode = "contractor_sono_code";

        /// <summary>
        /// ИНН продавца
        /// </summary>
        public const string SellerInn = "contractor_inn";

        /// <summary>
        /// СУР НП
        /// </summary>
        public const string SellerSurCode = "contractor_sur_code";

        /// <summary>
        /// Крупнейший
        /// </summary>
        public const string SellerCategory = "contractor_category";

        /// <summary>
        /// Отчетный период с годом
        /// </summary>
        public const string SellerPeriodFull = "contractor_period_full";

        /// <summary>
        /// Дата подачи декларации
        /// </summary>
        public const string SellerSubmitDate = "contractor_submit_date";

        /// <summary>
        /// Сумма по НД продавца
        /// </summary>
        public const string Side2NdsAmount = "contractor_decl_nds";

        /// <summary>
        /// 'Не представлена НД / Журнал
        /// </summary>
        public const string SellerZip = "contractor_zip";

        /// <summary>
        /// Признак НД
        /// </summary>
        public const string SellerDeclSign = "contractor_decl_sign";

        #endregion

        #region Область Расхождений

        /// <summary>
        /// Тип расхождения
        /// </summary>
        public const string TypeCode = "type_code";


        /// <summary>
        /// Вид расхождения
        /// </summary>
        public const string KindCode = "multiple";

        /// <summary>
        /// Номер правила сопоставления
        /// </summary>
        public const string CompareRule = "compare_rule";
        #endregion

        /// <summary>
        /// Общая сумма расхождений по типу НДС
        /// </summary>
        public const string DeclTotalAmountNds = "discrepancy_nds_amt";

        /// <summary>
        /// Общая сумма расхождений по типу Расхождение
        /// </summary>
        public const string DeclTotalAmountGap = "discrepancy_gap_amt";

        /// <summary>
        /// Минимальная сумма расхождения 
        /// </summary>
        public const string DeclMinAmount = "amount";

        /// <summary>
        /// Источник расхождения по стороне покупателя
        /// </summary>
        public const string InvoiceChapter = "invoice_chapter";

        /// <summary>
        /// Источник расхождения по стороне продавца
        /// </summary>
        public const string InvoiceContractorChapter = "contractor_invoice_chapter";

        /// <summary>
        /// КВО покупателя
        /// </summary>
        public const string Side1OperationCodes = "kvo";

        /// <summary>
        /// КВО продавца
        /// </summary>
        public const string Side2OperationCodes = "contractor_kvo";
    }
}
