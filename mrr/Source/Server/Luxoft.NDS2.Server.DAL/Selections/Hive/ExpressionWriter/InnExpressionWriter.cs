﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public class InnExpressionWriter : FilterExpressionWriter
    {
        public InnExpressionWriter(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        private bool IsSingleValueFilter(Parameter filter, EditorParameter editorParameter)
        {
            return (editorParameter.AggregateName == AggregateColumnNames.BuyerInn &&
                    filter.Operator != ComparisonOperator.NotDefinedOperator) ||
                   (editorParameter.AggregateName == AggregateColumnNames.SellerInn &&
                    filter.Operator != ComparisonOperator.NotDefinedOperator);
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParameter = GetEditorParameter(filter.AttributeId);

            if (!IsSingleValueFilter(filter, editorParameter))
            {
                return false;
            }

            if (filter.Values.Length < 1)
                return true;

            bool isFirstFilterValue = true;

            if (filter.Values.Length > 1)
            {
                writer.AppendFormat("dis.{0} {2}in ({1})", editorParameter.AggregateName,
                    string.Join(", ", filter.Values.Select(x=> string.Format("'{0}'", x.Value))), 
                    filter.Operator == ComparisonOperator.NotInList ? "not " : "");
                return true;
            }

            foreach (var value in filter.Values)
            {
                if (isFirstFilterValue)
                {
                    isFirstFilterValue = false;
                }
                else
                {
                    writer.Append(" and ");
                }

                writer.Append(GetExpression(filter.Operator, editorParameter.AggregateName, value.Value));
            }

            return true;
        }

        protected string GetExpression(ComparisonOperator compareOperator, string fieldName, string value)
        {
            switch (compareOperator)
            {
                case ComparisonOperator.Equals:
                case ComparisonOperator.InList:
                    return string.Format("dis.{0} = '{1}'", fieldName, value);
                case ComparisonOperator.NotEquals:
                case ComparisonOperator.NotInList:
                    return string.Format("dis.{0} <> '{1}'", fieldName, value);
                default:
                    return string.Empty;
            }
        }
    }
}
