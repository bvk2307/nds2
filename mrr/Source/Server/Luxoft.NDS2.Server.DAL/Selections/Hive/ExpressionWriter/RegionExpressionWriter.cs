﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public class RegionExpressionWriter :FilterExpressionWriter
    {
        /// <summary>
        /// Границы отбора по регионам - ИФНС 9901 должна относиться к региону 50
        /// </summary>
        private const string RegionCode99 = "99";
        private const string RegionCode50 = "50";
        private const string SonoCode9901 = "9901";

        public RegionExpressionWriter(List<EditorParameter> editorParameters) : base(editorParameters)
        {
        }

        private bool IsRegionFilter(Parameter filter, EditorParameter editorParameter)
        {
            return editorParameter.AggregateName == AggregateColumnNames.RegionCode ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerRegionCode;
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParameter = GetEditorParameter(filter.AttributeId);

            if (!IsRegionFilter(filter, editorParameter))
            {
                return false;
            }

            if (filter.Values.Length < 1)
                return true;

            bool bAdded = false;
            if (filter.Values.Select(x=>x.Value).Contains(RegionCode50))
            {
                writer.AppendFormat(string.Format("(dis.{0} = '{1}' or dis.{2} = '{3}')",
                    editorParameter.AggregateName,
                    RegionCode50,
                    editorParameter.AggregateName == AggregateColumnNames.RegionCode ? AggregateColumnNames.SonoCode : AggregateColumnNames.SellerSonoCode,
                    SonoCode9901));
                bAdded = true;
            }

            if (filter.Values.Select(x => x.Value).Contains(RegionCode99))
            {
                if(bAdded)
                    writer.Append(" or ");

                writer.AppendFormat(string.Format("(dis.{0} = '{1}' and dis.{2} <> '{3}')",
                    editorParameter.AggregateName,
                    RegionCode99,
                    editorParameter.AggregateName == AggregateColumnNames.RegionCode ? AggregateColumnNames.SonoCode : AggregateColumnNames.SellerSonoCode,
                    SonoCode9901));
                bAdded = true;
            }

            if (filter.Values.Any(x => !(x.Value == RegionCode50 || x.Value == RegionCode99)))
            {
                if(bAdded)
                    writer.Append(" or ");

                writer.AppendFormat("dis.{0} in ({1})", editorParameter.AggregateName, string.Join(",",
                    filter.Values.Where(x => !(x.Value == RegionCode50 || x.Value == RegionCode99)).Select(x => string.Format("'{0}'", x.Value))));
                
            }

            return true;

        }
      
    }
}
