﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public class DateRangeValueExpressionWriter : FilterExpressionWriter
    {
        public DateRangeValueExpressionWriter(List<EditorParameter> editorParameters) : base(editorParameters)
        {
        }

        private bool IsRangeFilter(Parameter filter, EditorParameter editorParameter)
        {
            return editorParameter.AggregateName == AggregateColumnNames.SubmitDate || 
                editorParameter.AggregateName == AggregateColumnNames.SellerSubmitDate;
        }

        const string HiveDateFormat = "yyyy-MM-dd";

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParameter = GetEditorParameter(filter.AttributeId);

            if (!IsRangeFilter(filter, editorParameter))
            {
                return false;
            }

            var value1 = filter.Values[0].ValueFrom;
            var value2 = filter.Values[0].ValueTo;

            if (value1 == null && value2 == null)
            {
                return true;
            }

            if (value1 == null)
            {
                var val = DateTime.Parse(value2).Date.ToString(HiveDateFormat);
                writer.AppendFormat("dis.{0} < '{1}'", editorParameter.AggregateName, val);
                return true;
            }

            if (value2 == null)
            {
                var val = DateTime.Parse(value1).Date.ToString(HiveDateFormat);
                writer.AppendFormat("dis.{0} > '{1}'", editorParameter.AggregateName, val);
                return true;
            }
            var val1 = DateTime.Parse(value1).Date.ToString(HiveDateFormat);
            var val2 = DateTime.Parse(value2).Date.ToString(HiveDateFormat);

            writer.AppendFormat("dis.{0} between '{1}' and '{2}'", editorParameter.AggregateName, val1, val2);


            return true;
        }

    }
}
