﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    class ExternalSourceExpressionWriter : FilterExpressionWriter
    {
        private readonly ISelectionService _service;
        public ExternalSourceExpressionWriter(List<EditorParameter> editorParameters, ISelectionService service)
            : base(editorParameters)
        {
            _service = service;
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParamater = GetEditorParameter(filter.AttributeId);

            if (!((editorParamater.AggregateName == AggregateColumnNames.BuyerInn || 
                editorParamater.AggregateName == AggregateColumnNames.SellerInn) &&
                filter.Operator == ComparisonOperator.NotDefinedOperator))
            {
                return false;
            }

            if (filter.Values.Length != 1)
                return false;

            long id;
            long.TryParse(filter.Values.First().Value, out id);

            var innList = _service.GetInnFromWhiteList(id).Result;

            if (innList.Count > 0 )
            {
                writer.AppendFormat("dis.{0} not in ({1}) or dis.{0} is null",
                    editorParamater.AggregateName,
                    string.Join(", ", innList.Select(x => string.Format("'{0}'", x))));
            }
            return true;
        }
    }
}
