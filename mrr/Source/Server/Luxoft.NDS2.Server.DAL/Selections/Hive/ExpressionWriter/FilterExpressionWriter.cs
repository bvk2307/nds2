﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public abstract class FilterExpressionWriter
    {
        protected List<EditorParameter> _editorParameters;

        public FilterExpressionWriter(List<EditorParameter> editorParameters)
        {
            _editorParameters = editorParameters;
        }

        public EditorParameter GetEditorParameter(int id)
        {
            return _editorParameters.SingleOrDefault(x => x.Id == id);
        }

        public abstract bool ProcessFilterExpression(Parameter filter, StringBuilder writer);
       
    }
}
