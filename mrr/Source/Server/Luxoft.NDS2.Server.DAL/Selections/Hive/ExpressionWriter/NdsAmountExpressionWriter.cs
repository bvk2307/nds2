﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    class NdsAmountExpressionWriter : FilterExpressionWriter
    {
        public NdsAmountExpressionWriter(List<EditorParameter> editorParameters)
            : base(editorParameters)
        {
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParamater = GetEditorParameter(filter.AttributeId);

            if (!(editorParamater.AggregateName == AggregateColumnNames.Side1NdsAmount ||
                editorParamater.AggregateName == AggregateColumnNames.Side2NdsAmount))
            {
                return false;
            }

            int i = 0;
            foreach (var filterVal in filter.Values)
            {
                if (filter.Values.Length > 1 && i > 0)
                    writer.Append(" or ");

                if (filterVal.Value == "3")
                {
                    writer.AppendFormat("(dis.{0}={1})",
                    editorParamater.Area == EditorArea.BuyerSide ? AggregateColumnNames.DeclSign : AggregateColumnNames.SellerDeclSign,
                    filterVal.Value);
                }
                else
                {
                    writer.AppendFormat("(dis.{0}={1} and dis.{2}>={3})",
                    editorParamater.Area == EditorArea.BuyerSide ? AggregateColumnNames.DeclSign : AggregateColumnNames.SellerDeclSign,
                    filterVal.Value,
                    editorParamater.AggregateName,
                    filterVal.ValueFrom ?? "0");
                }
              
                i++;
            }

            return true;
        }
    }
}
