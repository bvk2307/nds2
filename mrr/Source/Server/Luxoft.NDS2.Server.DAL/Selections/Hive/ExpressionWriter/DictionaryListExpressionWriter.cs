﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter
{
    public class DictionaryListExpressionWriter :FilterExpressionWriter
    {
        public DictionaryListExpressionWriter(List<EditorParameter> editorParameters) : base(editorParameters)
        {
        }

        private bool IsDictionaryFilter(Parameter filter, EditorParameter editorParameter)
        {
            return editorParameter.AggregateName == AggregateColumnNames.SonoCode ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerSonoCode ||
                   editorParameter.AggregateName == AggregateColumnNames.SurCode ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerSurCode ||
                   editorParameter.AggregateName == AggregateColumnNames.Category ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerCategory ||
                   editorParameter.AggregateName == AggregateColumnNames.PeriodFull ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerPeriodFull ||
                   editorParameter.AggregateName == AggregateColumnNames.KindCode ||
                   editorParameter.AggregateName == AggregateColumnNames.CompareRule ||
                   editorParameter.AggregateName == AggregateColumnNames.InvoiceChapter ||
                   editorParameter.AggregateName == AggregateColumnNames.InvoiceContractorChapter ||
                   editorParameter.AggregateName == AggregateColumnNames.Side1OperationCodes ||
                   editorParameter.AggregateName == AggregateColumnNames.Side2OperationCodes;
        }

        private bool IsNumericValue(EditorParameter editorParameter)
        {
            return editorParameter.AggregateName == AggregateColumnNames.SurCode ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerSurCode ||
                   editorParameter.AggregateName == AggregateColumnNames.Category ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerCategory ||
                   editorParameter.AggregateName == AggregateColumnNames.PeriodFull ||
                   editorParameter.AggregateName == AggregateColumnNames.SellerPeriodFull || 
                   editorParameter.AggregateName == AggregateColumnNames.KindCode ||
                   editorParameter.AggregateName == AggregateColumnNames.CompareRule ||
                   editorParameter.AggregateName == AggregateColumnNames.InvoiceChapter ||
                   editorParameter.AggregateName == AggregateColumnNames.InvoiceContractorChapter ||
                   editorParameter.AggregateName == AggregateColumnNames.Side1OperationCodes ||
                   editorParameter.AggregateName == AggregateColumnNames.Side2OperationCodes;
        }

        public override bool ProcessFilterExpression(Parameter filter, StringBuilder writer)
        {
            var editorParameter = GetEditorParameter(filter.AttributeId);

            if (!IsDictionaryFilter(filter, editorParameter))
            {
                return false;
            }

            if (filter.Values.Length < 1)
                return true;

            if (editorParameter.AggregateName == AggregateColumnNames.Side1OperationCodes ||
                editorParameter.AggregateName == AggregateColumnNames.Side2OperationCodes)
            {
                switch (filter.Operator)
                {
                    case ComparisonOperator.NotInList:
                    case ComparisonOperator.NotContains:
                        WriteBitwiseNotContains(filter, editorParameter, writer);
                        break;
                    case ComparisonOperator.NotEquals:
                        WriteBitwiseNotEquals(filter, editorParameter, writer);
                        break;
                    case ComparisonOperator.InList:
                        WriteBitwiseInList(filter, editorParameter, writer);
                        break;
                    case ComparisonOperator.Contains:
                        WriteBitwiseContains(filter, editorParameter, writer);
                        break;
                    case ComparisonOperator.Equals:
                        WriteBitwiseEquals(filter, editorParameter, writer);
                        break;
                    case ComparisonOperator.OneOf:
                        WriteBitwiseOneOf(filter, editorParameter, writer);
                        break;
                }
                return true;
            }

            if (filter.Operator == ComparisonOperator.InList  || filter.Operator == ComparisonOperator.Equals)
            {
                WriteInList(filter, editorParameter, writer);
                return true;
            }

            if (filter.Operator == ComparisonOperator.NotInList || filter.Operator == ComparisonOperator.NotEquals)
            {
                WriteNotInList(filter, editorParameter, writer);
                return true;
            }

            

            throw new InvalidOperationException(string.Format("Некорректные условия фильтра типа DictionaryList: {0}", filter.Operator));
        }

        private void WriteInList(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            var format = IsNumericValue(editorParameter) ? "{0}" : "'{0}'";

            writer.AppendFormat("dis.{0} in ({1})", editorParameter.AggregateName, string.Join(",", 
                filter.Values.Select(x => string.Format(format, x.Value))));
        }

        private void WriteNotInList(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat("dis.{0} not in ({1})", editorParameter.AggregateName, string.Join(",", filter.Values.Select(x=>string.Format("'{0}'",x.Value))));
        }

        private void WriteBitwiseContains(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat(string.Format("dis.{0} & {1} = {1}", editorParameter.AggregateName,
                            filter.Values.Sum(x => PowerValue(Convert.ToInt32(x.Value)))));
        }

        private void WriteBitwiseNotContains(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat(string.Format("dis.{0} & {1} != {1}", editorParameter.AggregateName,
                            filter.Values.Sum(x => PowerValue(Convert.ToInt32(x.Value)))));
        }

        private void WriteBitwiseInList(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat("{0}",
              string.Join(" or ",
                  filter.Values.Select(x => x.Value).Select(
                      i => string.Format("dis.{0} & {1} = dis.{0}", editorParameter.AggregateName, PowerValue(Convert.ToInt32(i)))))
          );
        }

        private void WriteBitwiseNotEquals(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat(string.Format("dis.{0} != {1}", editorParameter.AggregateName, 
                filter.Values.Sum(x => PowerValue(Convert.ToInt32(x.Value)))));
        }

        private void WriteBitwiseEquals(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat(string.Format("dis.{0} = {1}", editorParameter.AggregateName,
                filter.Values.Sum(x => PowerValue(Convert.ToInt32(x.Value)))));
        }

        private void WriteBitwiseOneOf(Parameter filter, EditorParameter editorParameter, StringBuilder writer)
        {
            writer.AppendFormat(string.Format("dis.{0} & {1} > 0", editorParameter.AggregateName,
                 filter.Values.Sum(x => PowerValue(Convert.ToInt32(x.Value)))));
        }

        private long PowerValue(int value)
        {
            if (value == 1) return value;

            return (long)Math.Pow(2, value-1);
        }
    }
}
