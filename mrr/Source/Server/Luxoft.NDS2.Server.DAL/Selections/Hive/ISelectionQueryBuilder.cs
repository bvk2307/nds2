﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive
{
    public interface ISelectionQueryBuilder
    {
        ISelectionQuery BuildSelectionQuery(Filter filter);
    }
}
