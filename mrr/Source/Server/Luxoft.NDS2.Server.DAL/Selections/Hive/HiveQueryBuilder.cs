﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive
{
    public class HiveQueryBuilder : ISelectionQueryBuilder
    {
        private const string QueryScope = "Selection";

        private const string HiveBuyerQueryPattern = "HIVE_QUERY_BUYER";

        private const int SellerZipParameterId = 122;

        private readonly List<FilterExpressionWriter> _expressionWriters;

        readonly StringBuilder _builder;

        private readonly string _queryBuyerText;

        public HiveQueryBuilder(
            IQueryTemplateProvider queryService,
            ISelectionService selectionService,
            List<EditorParameter> listEditorParameters)
        {
            _queryBuyerText = queryService.GetQueryText(QueryScope, HiveBuyerQueryPattern);
            _builder = new StringBuilder();
            _expressionWriters = new List<FilterExpressionWriter>
            {
                new InnExpressionWriter(listEditorParameters),
                new DateRangeValueExpressionWriter(listEditorParameters),
                new DictionaryListExpressionWriter(listEditorParameters),
                new ExternalSourceExpressionWriter(listEditorParameters, selectionService),
                new DiscrepAmountExpressionWriter(listEditorParameters),
                new NdsAmountExpressionWriter(listEditorParameters),
                new ContractorZipExpressionWriter(listEditorParameters),
                new RegionExpressionWriter(listEditorParameters)
            };
        }

        public ISelectionQuery BuildSelectionQuery(Filter filter)
        {

            var isFirstGroup = true;
            var result = new SelectionQuery();
            ValidateFilter(filter);
            foreach (var selectionFilterGroup in filter.Groups)
            {
                if(!selectionFilterGroup.IsEnabled)
                    continue;
                
                var firstFilter = true;

                var queryTextLocal = _queryBuyerText;

                if (isFirstGroup)
                {
                    _builder.AppendLine(queryTextLocal);
                    isFirstGroup = false;
                }
                else
                {
                    _builder.AppendLine();
                    _builder.AppendLine(" union ");
                    _builder.AppendLine(queryTextLocal);
                }

                foreach (var selectionFilterCriteria in selectionFilterGroup.Parameters)
                {
                    if (firstFilter)
                    {
                        firstFilter = false;
                        _builder.Append(" and (");
                    }
                    else
                    {
                        _builder.Append("\n and (");
                    }

                    ProcessWriteFilterExpression(selectionFilterCriteria);

                    _builder.Append(")");
                }
            }

            result.QueryText = _builder.ToString();

            return result;
        }

        void ValidateFilter(Filter filter)
        {
            foreach (var selectionFilterGroup in filter.Groups)
            {
                if (!selectionFilterGroup.IsEnabled)
                    continue;

                var parameters = selectionFilterGroup.Parameters
                    .Where(x => x.Values.Length > 0).ToArray();

                var resultParameters = new List<Parameter>();
                foreach (var parameter in parameters)
                {
                    var editorParameter = _expressionWriters.First().GetEditorParameter(parameter.AttributeId);

                    parameter.Values =
                        parameter.Values.Where(
                            x =>
                                !string.IsNullOrEmpty(x.Value) || !string.IsNullOrEmpty(x.ValueFrom) ||
                                !string.IsNullOrEmpty(x.ValueTo)).ToArray();
                    if (parameter.Values.Length <= 0)
                        continue;

                    if (editorParameter.AggregateName == AggregateColumnNames.TypeCode)
                    {
                        parameter.Values = parameter.Values.Select(
                            x =>
                            {
                                x.ValueFrom = string.IsNullOrEmpty(x.ValueFrom) ? "0" : x.ValueFrom;
                                x.ValueTo = string.IsNullOrEmpty(x.ValueTo) ? "0" : x.ValueTo;
                                return x;
                            }).ToArray();

                    }
                    if (editorParameter.AggregateName == AggregateColumnNames.Side1NdsAmount || 
                        editorParameter.AggregateName == AggregateColumnNames.Side2NdsAmount
                        )
                    {
                        parameter.Values = parameter.Values.Where(
                            x => !string.IsNullOrEmpty(x.Value) && x.Value != "0" ).ToArray();

                    }
                    if(parameter.Values.Length > 0)
                        resultParameters.Add(parameter);
                }
              
                selectionFilterGroup.Parameters = resultParameters.ToArray();
            }
        }

        private void ProcessWriteFilterExpression(Parameter filter)
        {
            foreach (var filterExpressionWriter in _expressionWriters)
            {
                if (filterExpressionWriter.ProcessFilterExpression(filter, _builder))
                {
                    break;
                }
            }
        }

    }

}
