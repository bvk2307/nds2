﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive
{
    public class SelectionQuery : ISelectionQuery
    {
        public string QueryText { get; set; }
    }
}
