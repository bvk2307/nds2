﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Selections.Hive
{
    public interface ISelectionQuery
    {
        string QueryText { get; }
    }
}
