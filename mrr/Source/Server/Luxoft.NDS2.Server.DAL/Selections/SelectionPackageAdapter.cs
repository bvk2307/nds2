﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    internal class SelectionPackageAdapter : BaseSelectionAdapter, ISelectionPackageAdapter
    {
        #region строковые константы

        #endregion

        public SelectionPackageAdapter(IServiceProvider services)
            : base(services)
        {
        }

        #region Из SelectionFilterAdapter

        public List<SelectionRequest> SyncWithRequestProcessing(long id, SelectionType type)
        {
            return ExecuteList(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureGetSelectionRequestStatus),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, id),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type),
                    FormatCommandHelper.Cursor(DbConstants.ParamCursor)
                }, ReadSelectionRequest);
        }

        private static SelectionRequest ReadSelectionRequest(OracleDataReader reader)
        {
            var ret = new SelectionRequest
            {
                RequestId = reader.ReadInt64(DbConstants.RequestIdField),
                SelectionId = reader.ReadInt64(DbConstants.SelectionIdField),
                RequestStatus = (SelectionRequestStatus)reader.ReadInt(DbConstants.StatusField)
            };

            return ret;
        }

        public Selection GetSelection(long id)
        {
            List<Selection> selections = ExecuteList(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureLoadSelection),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(DbConstants.ParamId, id),
                    FormatCommandHelper.Cursor(DbConstants.ParamCursor)
                },
                BuildSelection);
            return selections.FirstOrDefault();
        }

        private static Selection BuildSelection(OracleDataReader reader)
        {
            var ret = new Selection
            {
                Id = reader.ReadInt64("Id"),
                Analytic = reader.ReadString("Analytic"),
                AnalyticSid = reader.ReadString("AnalyticSid"),
                Manager = reader.ReadString("Manager"),
                ManagerSid = reader.ReadString("ManagerSid"),
                CreationDate = reader.ReadDateTime("CreationDate"),
                ModificationDate = reader.ReadDateTime("ModificationDate"),
                Name = reader.ReadString("Name"),
                Status = (SelectionStatus?)reader.ReadNullableInt("Status"),
                DeclarationCount = reader.ReadInt("DeclarationCount"),
                DiscrepanciesCount = reader.ReadInt("DiscrepanciesCount"),
                StatusDate = reader.ReadDateTime("StatusDate"),
                Type = reader.ReadString("Type"),
                TypeCode = (SelectionType)reader.ReadInt("TypeCode"),
                TotalAmount = reader.ReadDecimal("TotalAmount"),
                Regions = reader.ReadString("Regions"),
                TemplateId = reader.ReadNullableInt64("TemplateId")
            };

            return ret;
        }

        public void SetSelectionState(long id, SelectionType type, int ststusId, string userSid, ActionType action)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, id),
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type),
                FormatCommandHelper.NumericIn(DbConstants.ParameterStatusId, ststusId),
                FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
                FormatCommandHelper.NumericIn(DbConstants.ParamActionType, (int)action)
            };

            Execute(FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureSelectionSetState),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void UpdateSelectionStateAfterHiveLoad(long id, SelectionType type, string userSid, ActionType action)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, id),
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type),
                FormatCommandHelper.NumericIn(DbConstants.ParameterStatusId, -1),
                FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
                FormatCommandHelper.NumericIn(DbConstants.ParamActionType, (int)action)
            };

            Execute(FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureSelectionSetState),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void Approve(long id, string userSid)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, id),
                FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
            };

            Execute(FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureApprove),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }

        public void SendForCorrection(long id, string userSid)
        {
            var parameters = new List<OracleParameter>
            {
                FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, id),
                FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
            };

            Execute(FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureSendForCorrection),
                CommandType.StoredProcedure,
                parameters.ToArray());
        }
        #endregion

        #region Из SelectionChangeHistoryAdapter
        #endregion

        #region Из DalManager


        public Selection SaveSelection(Selection data, ActionType actionType)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureSaveSelection),
                CommandType.StoredProcedure,
                new[]
                {
                  new OracleParameter(DbConstants.ParamId, OracleDbType.Int64,  data.Id, ParameterDirection.InputOutput),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionTemplateId, data.TemplateId),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterName, data.Name),
                    FormatCommandHelper.NumericIn(DbConstants.ParameterTypeName, (int)data.TypeCode),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterLastEditedBySid, data.AnalyticSid),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterLastEditedBy, data.Analytic),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterApprovedBySid, data.ManagerSid),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterApprovedBy, data.Manager),
                    FormatCommandHelper.NumericIn(DbConstants.ParameterStatusId,  (int?)data.Status),
                    FormatCommandHelper.VarcharIn(DbConstants.ParamRegions, data.Regions),
                    FormatCommandHelper.NumericIn(DbConstants.ParamActionType, (int)actionType)
                });
            data.Id = DataReaderExtension.ReadInt(result.Output[DbConstants.ParamId]);
            return data;
        }

        public string GenerateUniqueName(string userSid, SelectionType type)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.FunctionGenerateUniqueName),
                CommandType.StoredProcedure,
                new[]
                {
                    new OracleParameter("v_ret", OracleDbType.Varchar2, ParameterDirection.ReturnValue) {Size = 128},
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type)
                });

            return result.Output["v_ret"].ToString();
        }

        public string VerifyUniqueName(string userSid, string selectionName, long selectionId, SelectionType type)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.FunctionVerifySelectionName),
                CommandType.StoredProcedure,
                new[]
                {
                    new OracleParameter("v_ret", OracleDbType.Varchar2, ParameterDirection.ReturnValue) {Size = 1024},
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterUserSid, userSid),
                    FormatCommandHelper.VarcharIn(DbConstants.ParameterName, selectionName),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, selectionId),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type)
                });

            return result.Output["v_ret"].ToString();
        }

        public List<ActionHistory> GetActionsHistory(long selectionId)
        {
            var result = ExecuteList(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureGetActionsHistry),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, selectionId),
                    FormatCommandHelper.Cursor(DbConstants.ParamCursor)
                }, reader =>
                {
                    var currId = Convert.ToInt32(reader[0]);
                    var currSelectionId = Convert.ToInt32(reader[1]);
                    var currStatus = Convert.ToString(reader[2]);
                    var currChangeDate = Convert.ToDateTime(reader[3]);
                    var currUserSid = Convert.ToString(reader[4]);
                    var currAction = Convert.ToString(reader[5]);
                    var currUserName = Convert.ToString(reader[6]);

                    return new ActionHistory
                    {
                        Id = currId,
                        ChangeAt = currChangeDate,
                        SelectionId = currSelectionId,
                        Status = currStatus,
                        ChangedBy = currUserName,
                        ChangedBySid = currUserSid,
                        Action = currAction
                    };
                });
            return result;
        }

        #endregion

        #region Из StateTransitionAdapter

       
        public List<SelectionTransition> GetStateTransitions()
        {
            return ExecuteList(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.SearchProcedure),
                CommandType.StoredProcedure,
                new [] 
                {
                    FormatCommandHelper.Cursor(DbConstants.CursorParameter)
                },
                BuildSelectionTransition);
        }

        private static SelectionTransition BuildSelectionTransition(OracleDataReader reader)
        {
            return new SelectionTransition
            {
                STATE_FROM = (SelectionStatus)int.Parse(reader[DbConstants.ParamTransitionsStateFromId].ToString()),
                STATE_TO = (SelectionStatus)int.Parse(reader[DbConstants.ParamTransitionsStateToId].ToString()),
                OPERATION = reader[DbConstants.ParamTransitionsOperation].ToString(),
            };
        }

        public long CreateRequest(long selectionId, string query)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.CreateRequestProcedure),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericOut(DbConstants.ParamId),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId,selectionId),
                    FormatCommandHelper.ClobIn(DbConstants.ParamQueryText, query)

                });
            return DataReaderExtension.ReadInt(result.Output[DbConstants.ParamId]);
        }

        public Selection UpdateSelectionAggregateData(Selection data)
        {
            var result = Execute(
               FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.UpdateSelectionAggregateProcedure),
               CommandType.StoredProcedure,
               new[]
               {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, data.Id),
                    FormatCommandHelper.NumericIn(DbConstants.ParameterTypeName, (int)data.TypeCode),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDeclQty),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDiscrQty),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDiscrAmt)
               });
            data.DeclarationCount = DataReaderExtension.ReadInt(result.Output[DbConstants.ParameterDeclQty]);
            data.DiscrepanciesCount = DataReaderExtension.ReadInt(result.Output[DbConstants.ParameterDiscrQty]);
            data.TotalAmount = DataReaderExtension.ReadDecimal(result.Output[DbConstants.ParameterDiscrAmt]);
            return data;
        }

        public void UpdateSelectionAggregate(long selectionId, SelectionType type)
        {
            var result = Execute(
               FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.UpdateSelectionAggregateProcedure),
               CommandType.StoredProcedure,
               new[]
               {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, selectionId),
                    FormatCommandHelper.NumericIn(DbConstants.ParameterTypeName, (int)type),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDeclQty),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDiscrQty),
                    FormatCommandHelper.NumericOut(DbConstants.ParameterDiscrAmt)
               });
        }

        /// <summary>
        /// Обноялвяет поля Регионы в выборке, если оно изменилось после применеия фильтра
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        public void UpdateSelectionRegions(long selectionId, string regions)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.UpdateSelectionRegionsProcedure),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, selectionId),
                    FormatCommandHelper.VarcharIn(DbConstants.ParamRegions, regions)
                });
        }
        #endregion


        #region Из SelectionDiscrepanciesAdapter

        public List<SelectionDiscrepancyStatistic> GetDiscrepancyStatistic(long selectionId, SelectionType type)
        {
            return ExecuteList(
               FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.ProcedureCountCheckDiscrepancies),
               CommandType.StoredProcedure,
               new[]
               {
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionId, selectionId),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionType, (int)type),
                    FormatCommandHelper.Cursor(DbConstants.ParamCursor)
               }, ReadDiscrepancyStatistic);
        }

        private static SelectionDiscrepancyStatistic ReadDiscrepancyStatistic(OracleDataReader reader)
        {
            var ret = new SelectionDiscrepancyStatistic
            {
                SelectionId = reader.ReadInt64(DbConstants.SelectionId),
                SelectionName = reader.ReadString(DbConstants.SelectionName),
                InProcessQty = reader.ReadInt64(DbConstants.InProcessQty),
                TotalQty = reader.ReadInt64(DbConstants.InProcessQty)
            };

            return ret;
        }

        #endregion
        /// <summary>
        /// Определяет возможность удаления шаблона в зависимости от статусов выборок в нем
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public bool CanDeleteTemplate(long templateId)
        {
            var result = Execute(
                FormatCommandHelper.PackageProcedure(DbConstants.SelectionPackageName, DbConstants.CanDeleteTemplateProcedure),
                CommandType.StoredProcedure,
                new[]
                {
                    FormatCommandHelper.NumericOut(DbConstants.ParamCanDelete),
                    FormatCommandHelper.NumericIn(DbConstants.ParamSelectionTemplateId, templateId),

                });
            return DataReaderExtension.ReadInt(result.Output[DbConstants.ParamCanDelete]) == 1;
        }
    }

    internal class FilterOption
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
