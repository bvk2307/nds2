﻿using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Сохраняет границы отбора по регионам
    /// </summary>
    public class SelectionRegionalBoundsAdapter 
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connection;
        public SelectionRegionalBoundsAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public void UpdateSelectionRegionalBounds(long templateId, SelectionRegionalBoundsDataItem item)
        {
            new ResultCommandExecuter(
               _service,
               _connection).TryExecute(
                   new SelectionUpdateRegionBuilder(templateId, item));
        }
    }
}