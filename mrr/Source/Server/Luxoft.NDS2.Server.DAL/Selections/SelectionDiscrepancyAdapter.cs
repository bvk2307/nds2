﻿using System.Security.Cryptography.X509Certificates;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Загружает параметров список расхождений выборки
    /// </summary>
    internal class SelectionDiscrepancyAdapter : SearchDataAdapterBase<SelectionDiscrepancy>
    {
        public SelectionDiscrepancyAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<SelectionDiscrepancy> DataMapper()
        {
            return new GenericDataMapper<SelectionDiscrepancy>();
            //WithFieldMapperFor(TypeHelper<SelectionListItemData>.GetMemberName(x => x.StatusId),
            //                     new NullableEnumFieldMapper(typeof(SelectionStatus)));
        }

        protected override string ViewName()
        {
            return DbConstants.SelectionDiscrepancyViewName;
        }
    }
}