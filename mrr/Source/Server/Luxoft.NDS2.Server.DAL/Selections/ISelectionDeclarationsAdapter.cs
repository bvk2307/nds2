﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public interface ISelectionDeclarationsAdapter
    {
        /// <summary>
        /// Ищет декларации в выборки по заданному критерию
        /// </summary>
        /// <param name="criteria">Критейрий поиска деклараций в выборке</param>
        /// <param name="orderBy">параметры сортировки</param>
        /// <param name="selectionId">id выборки</param>
        /// <param name="rowsToSkip">кол-во пропущенных строк</param>
        /// <param name="rowsToTake">кол-во необходимых строк</param>
        /// <returns>список декларций</returns>
        IEnumerable<SelectionDeclaration> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            int rowsToSkip,
            int? rowsToTake);

        /// <summary>
        /// Запрашивает кол-во деклараций в выборки по заданному критерию
        /// </summary>
        /// <param name="criteria">Критейрий поиска деклараций в выборке</param>
        /// <param name="selectionId">id выборки</param>
        /// <returns>кол-во декларций</returns>
        int Count(FilterExpressionBase searchCriteria);

        /// <summary>
        /// Получает состояние включения группы деклараций в выборку
        /// </summary>
        /// <param name="selectionId"></param>
        /// <returns>Состояние включения группы деклараций в выборку</returns>
        SelectionGroupState GetDeclarationState(long selectionId);

        /// <summary>
        /// Устанавливает/снимает флаг выбора декларации
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="declarationId"></param>
        /// <param name="state"></param>
        /// <param name="userSid"></param>
        void SetDeclarationState(long selectionId, long declarationId, bool state, string userSid);

        /// <summary>
        /// Включает/выключает все декларации в обработку в данной выборке
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="state"></param>
        /// <param name="userSid"></param>
        /// <param name="zipList"></param>
        void SetState(long selectionId, bool state, string userSid, long[] zipList);
    }
}
