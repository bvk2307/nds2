﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Работает со статусами выборок
    /// </summary>
    public class SelectionStatusAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public SelectionStatusAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public void DeleteSelectionsByTemplate(long templateId)
        {
            new ResultCommandExecuter(
                _service,
                _connection).TryExecute(
                new DeleteSelectionsByTemplateBuilder(templateId));
        }
    }
}