﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public interface ISelectionDiscrepanciesAdapter
    {
        /// <summary>
        /// Выполняет поиск расхождений в выборке, удовлетворяющих заданным критериям
        /// </summary>
        /// <param name="searchCriteria">Критерии выборки расхождений в выборке</param>
        /// <param name="orderBy">параметры сортировки</param>
        /// <param name="selectionId">id выборки</param>
        /// <param name="rowsToSkip">кол-во пропущенных строк</param>
        /// <param name="rowsToTake">кол-во необходимых строк</param>
        /// <returns>список расхождений</returns>
        IEnumerable<SelectionDiscrepancy> Search(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId,
            int rowsToSkip,
            int rowsToTake);

        IEnumerable<SelectionDiscrepancy> All(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId);

        /// <summary>
        /// Выполняет processAction для каждой записи
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="orderBy"></param>
        /// <param name="selectionId"></param>
        /// <param name="processAction"></param>
        void ForAll(
            FilterExpressionBase searchCriteria,
            IEnumerable<ColumnSort> orderBy,
            long selectionId,
            Func<SelectionDiscrepancy, bool> processAction);

        /// <summary>
        /// Выполняет запрос кол-ва расхождений в выборке, удовлетворяющих заданным критериям
        /// </summary>
        /// <param name="searchCriteria">Критерии выборки расхождений в выборке</param>
        /// <param name="selectionId">id выборки</param>
        /// <returns>кол-во расхождений</returns>
        int Count(FilterExpressionBase searchCriteria, long selectionId);
    }
}
