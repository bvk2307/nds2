﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public interface ISelectionFilterAdapter
    {
        IEnumerable<SelectionParameter> GetFilterParameters();

        IEnumerable<EditorParameter> GetFilterEditorParameters(SelectionType selectionType);

        IEnumerable<KeyValuePair<string, string>> GetFilterParameterOptions(int parameterId);

        IEnumerable<KeyValuePair<string, string>> GetPeriods();

        IEnumerable<Region> GetRegions(string userSid);

        void SaveFilter(long selectionId, Filter filter);

        Filter GetSelectionFilterContent(long selectionId);

        IEnumerable<KeyValuePair<string, string>> GetWhiteLists();
    }
}
