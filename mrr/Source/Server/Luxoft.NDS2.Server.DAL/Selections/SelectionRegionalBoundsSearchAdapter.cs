﻿using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Selections.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Загружает параметры шаблона - Границы отбора по регионам
    /// </summary>
    internal class SelectionRegionalBoundsSearchAdapter : SearchDataAdapterBase<SelectionRegionalBoundsDataItem>
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connection;
        public SelectionRegionalBoundsSearchAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
            _service = service;
            _connection = connection;
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<SelectionRegionalBoundsDataItem> DataMapper()
        {
            return new GenericDataMapper<SelectionRegionalBoundsDataItem>()
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrTotalAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrMinAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrGapTotalAmt),
                            new UnsignedIntFieldMapper())
             .WithFieldMapperFor(TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(x => x.DiscrGapMinAmt),
                            new UnsignedIntFieldMapper());
        }

        protected override string ViewName()
        {
            return DbConstants.SelectionRegionalBoundsViewName;
        }
    }
}