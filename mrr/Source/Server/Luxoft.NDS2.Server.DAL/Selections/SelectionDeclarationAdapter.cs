﻿using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    /// <summary>
    ///     Загружает параметров список выборок
    /// </summary>
    internal class SelectionDeclarationAdapter : SearchDataAdapterBase<SelectionDeclaration>
    {
        public SelectionDeclarationAdapter(IServiceProvider service, ConnectionFactoryBase connection)
            : base(service, connection)
        {
        }

        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<SelectionDeclaration> DataMapper()
        {
            return new GenericDataMapper<SelectionDeclaration>();
        }

        protected override string ViewName()
        {
            return DbConstants.SelectionDeclarartionViewName;
        }
    }
}