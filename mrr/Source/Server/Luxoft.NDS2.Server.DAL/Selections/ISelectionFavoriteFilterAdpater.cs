﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public interface ISelectionFavoriteFilterAdpater
    {
        List<FavoriteFilter> Search(string owner);

        FavoriteFilter Get(long favoriteId);

        List<FavoriteFilter> All();

        void Insert(FavoriteFilter filter);

        void Update(FavoriteFilter filter);

        void Delete(FavoriteFilter filter);
    }
}
