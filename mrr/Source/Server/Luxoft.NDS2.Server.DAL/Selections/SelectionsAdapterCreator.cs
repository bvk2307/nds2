﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.Selections
{
    public static class SelectionsAdapterCreator
    {
        public static ISearchAdapter<SelectionListItemData> SelectionAgregateAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new SelectionAggregateAdapter(serviceProvider, connection);
        }

        public static IDictionaryCommonAdapter SelectionStatusDictionaryAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SelectionStatusDictionaryAdapter(service, connection);
        }

        public static IDictionaryCommonAdapter ExcludeReasonDictionaryAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new ExcludeReasonDictionaryAdapter(service, connection);
        }

        public static ISearchAdapter<SelectionDeclaration> SelectionDeclarationAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SelectionDeclarationAdapter(service, connection);
        }

        public static ISearchAdapter<SelectionDiscrepancy> SelectionDiscrepancyAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SelectionDiscrepancyAdapter(service, connection);
        }

        public static ISearchAdapter<SelectionRegionalBoundsDataItem> SelectionRegionalBoundsSearchAdapterAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SelectionRegionalBoundsSearchAdapter(service, connection);
        }

        public static SelectionRegionalBoundsAdapter SelectionRegionalBoundsAdapterAdapter(
            IServiceProvider service, ConnectionFactoryBase connection)
        {
            return new SelectionRegionalBoundsAdapter(service, connection);
        }

        public static ISelectionDeclarationsAdapter SelectionDeclarationsAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new SelectionDeclarationsAdapter(service, connectionFactory);
        }

        public static ISelectionDiscrepanciesAdapter SelectionDiscrepanciesAdapter(this IServiceProvider service)
        {
            return new SelectionDiscrepanciesAdapter(service);
        }

        public static ISelectionFilterAdapter SelectionFilterAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new SelectionFilterAdapter(service, connectionFactory);
        }

        public static ISelectionFavoriteFilterAdpater SelectionFavoriteFilterAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new SelectionFavoriteFilterAdapter(service, connectionFactory);
        }

        public static SelectionStatusAdapter SelectionStatusAdapter(this IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            return new SelectionStatusAdapter(service, connectionFactory);
        }

        public static SelectionAdapter SelectionAdapter(this IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            return new SelectionAdapter(service, connectionFactory);
        }
    }
}