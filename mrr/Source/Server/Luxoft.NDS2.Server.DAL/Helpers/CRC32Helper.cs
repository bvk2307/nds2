﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    internal static class CRC32Helper
    {
        public static uint GalcCRC32 ( string initialString )
        {
            const int BufferSize = 1024;
            const uint Polynomial = 0xEDB88320;

            uint result = 0xFFFFFFFF;
            uint crc32;
            uint[] tableCRC32 = new uint[256];

            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(initialString ?? "")); 
            
            // Инициалиазация таблицы
            for (uint i = 0; i < 256; i++)
            {
                crc32 = i;

                for (uint j = 8; j > 0; j--)
                {
                    if ((crc32 & 1) == 1)
                        crc32 = (crc32 >> 1) ^ Polynomial;
                    else
                        crc32 >>= 1;
                }

                tableCRC32[i] = crc32;
            }

            // Чтение из буфера

            byte[] buffer = new byte[BufferSize];
            int count = stream.Read(buffer, 0, BufferSize);

            //
            // Вычисление CRC
            //
            while (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    result = ((result) >> 8) ^ tableCRC32[(buffer[i]) ^ ((result) & 0x000000FF)];
                }

                count = stream.Read(buffer, 0, BufferSize);
            }

            return result;
        }
                 
    }
}
