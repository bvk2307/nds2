﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    /// <summary>
    /// Этот класс позволяет строить SQL выражения
    /// </summary>
    internal static class QueryBuilder
    {
        public const string SQLOrderBy = "ORDER BY";
        public const string SQLOrderByAscending = "ASC";
        public const string SQLOrderByDescending = "DESC";

        private static Dictionary<FilterQuery.FilterLogicalOperator, string> operationsCode = new Dictionary<FilterQuery.FilterLogicalOperator, string>()
            {
                { FilterQuery.FilterLogicalOperator.NotDefinedOperator, string.Empty },
                { FilterQuery.FilterLogicalOperator.And, "AND" },
                { FilterQuery.FilterLogicalOperator.Or, "OR" }
            };

        # region Build Where

        public static string ToSQL(
            this List<FilterQuery> filter,
            IQualifiedFieldNameBuilder fieldNameBuilder,
            IParameterBuilder paramBuilder
            )
        {
            string ret = string.Empty;
            bool firstLck = true;

            // string.Join() emulation
            foreach (var item in filter)
            {
                if (!firstLck)
                {
                    string groupOperator;
                    if (operationsCode.TryGetValue(item.GroupOperator, out groupOperator))
                    {
                        ret = string.Concat(ret, " ", groupOperator, " ");
                    }
                }

                var condition = item.ToSQL(fieldNameBuilder, paramBuilder);
                if (!string.IsNullOrWhiteSpace(condition))
                    ret = string.Concat(ret, string.Format(" ({0}) ", condition));

                firstLck = false;
            }

            return ret;
        }

        public static string ToSQL(
            this ColumnFilter filter,
            IQualifiedFieldNameBuilder fieldNameBuilder,
            IParameterBuilder parameterBuilder)
        {
            return OperationResolver
                .Resolve(filter.ComparisonOperator)
                .Build(parameterBuilder, fieldNameBuilder, filter.Value);
        }

        public static string ToSQL(
            this FilterQuery filter,
            IQualifiedFieldNameBuilder fieldNameBuilder,
            IParameterBuilder parameterBuilder)
        {
            fieldNameBuilder.FieldName = filter.ColumnName;
            fieldNameBuilder.IsComplex = filter.IsComplex;

            return string.Join(
                filter.FilterOperator.ToString(),
                filter.Filtering.Select(
                    flt => string.Format(" ({0}) ", flt.ToSQL(fieldNameBuilder, parameterBuilder))).ToArray());
        }

        # endregion

        # region Build Order By

        public static string ToSQL(
            this IEnumerable<ColumnSort> sorting,
            IQualifiedFieldNameBuilder fieldNameBuilder
            )
        {
            if (!sorting.Any())
            {
                return string.Empty;
            }

            return string.Format(
                "{0} {1}",
                SQLOrderBy,
                string.Join(",", sorting.Select(sort => sort.ToSQL(fieldNameBuilder))));
        }

        public static string ToSQL(
            this ColumnSort sort,
            IQualifiedFieldNameBuilder fieldNameBuilder
            )
        {
            fieldNameBuilder.FieldName = sort.ColumnKey;

            return string.Format(
                sort.Order == ColumnSort.SortOrder.None ? "{0}" : "{0} {1}",
                fieldNameBuilder.Build(),
                sort.Order == ColumnSort.SortOrder.Desc ? SQLOrderByDescending : SQLOrderByAscending);
        }

        public static string ToSQL(
            this IEnumerable<ColumnSort> orderBy,
            IQueryPatternProvider expressionBuilder)
        {
            if (!orderBy.Any())
            {
                return string.Empty;
            }

            return string.Format(
                "{0} {1}",
                SQLOrderBy,
                string.Join(",", orderBy.Select(sort => sort.ToSQL(expressionBuilder))));
        }

        public static string ToSQL(
            this ColumnSort orderBy,
            IQueryPatternProvider expressionBuilder)
        {
            var template = orderBy.Order == ColumnSort.SortOrder.None ? "{0}" : (orderBy.NullAsZero ? "NVL({0}, 0) {1}" : "{0} {1}");
            return string.Format(
                template,
                expressionBuilder.Expression(orderBy.ColumnKey),
                orderBy.Order == ColumnSort.SortOrder.Desc ? SQLOrderByDescending : SQLOrderByAscending);
        }
        
        # endregion

        # region Build Paging

        private const string IndexedPrefix = "indexed_query";
        private const string IndexKey = "row_number";
        private const string PageQueryPattern = "WITH {0} AS (SELECT ROWNUM as {1}, q.* FROM ({2}) q) SELECT * FROM {0} WHERE ({0}.{1}>{3} AND {0}.{1}<={4})";
        private const string SkipQueryPattern = "WITH {0} AS (SELECT ROWNUM as {1}, q.* FROM ({2}) q) SELECT * FROM {0} WHERE ({0}.{1}>{3})";

        public static string WithPaging(
            this QueryConditions conditions,
            string innerStatement,
            IParameterBuilder paramBuilder)
        {
            var indexFrom = paramBuilder.Add((int)(conditions.PageIndex - 1) * conditions.PageSize);
            var indexTo = paramBuilder.Add((int)conditions.PageIndex * conditions.PageSize);

            return WithPaging(indexFrom, indexTo, innerStatement);
        }

        public static string WithPaging(
            string indexFrom,
            string indexTo,
            string innerStatement)
        {
            return string.Format(
                PageQueryPattern,
                IndexedPrefix,
                IndexKey,
                innerStatement,
                indexFrom,
                indexTo
                );
        }

        # endregion

        public static Query ToSQL(
            this QueryConditions conditions,
            string sqlStatementPattern,
            string filterTablePrefix,
            bool includePaging = false,
            bool includeLeftAnd = false)
        {
            var fieldNameBuilder = new PrefixBasedFieldNameBuilder(filterTablePrefix);
            var paramBuilder = new IncrementalParameterBuilder("param_{0}");
            var conditionsStr = conditions.Filter.Any()
                ? conditions.Filter.ToSQL(fieldNameBuilder, paramBuilder)
                : string.Empty;
            var query = string.Format(
                    sqlStatementPattern,
                    string.Format("{0} {1}",
                        includeLeftAnd && !string.IsNullOrEmpty(conditionsStr) ? " AND " : string.Empty,
                        conditionsStr
                        ),
                    conditions.Sorting.ToSQL(fieldNameBuilder));

            if (includePaging)
            {
                query = conditions.WithPaging(query, paramBuilder);
            }
            query.Replace("\r\n", " ");

            return new Query()
            {
                Text = query,
                Parameters = paramBuilder.Parameters
            };
        }

        public static Query ToSQL(
            this DataQueryContext filter,
            string sqlStatementPattern,
            IQueryPatternProvider fieldPatternProvider)
        {
            var paramBuilder = new IncrementalParameterBuilder("param_{0}");

            var queryText =
                string.Format(
                    sqlStatementPattern,
                    filter.Where.ToQuery(paramBuilder, fieldPatternProvider),
                    filter.OrderBy.ToSQL(fieldPatternProvider));

                queryText =
                    WithPaging(
                        paramBuilder.Add(filter.Skip),
                        paramBuilder.Add(filter.Skip + filter.MaxQuantity),
                        queryText);

            queryText.Replace("\r\n", " ");

            return new Query()
            {
                Text = queryText,
                Parameters = paramBuilder.Parameters
            };
        }

        public static Query ToSQL(
            this FilterExpressionBase filter,
            string sqlStatementPattern,
            IQueryPatternProvider fieldPatternProvider)
        {
            var paramBuilder = new IncrementalParameterBuilder("param_{0}");
            var queryText =
                string.Format(
                    sqlStatementPattern,
                    filter.ToQuery(paramBuilder, fieldPatternProvider));

            return new Query
            {
                Text = queryText,
                Parameters = paramBuilder.Parameters
            };
        }

        internal static void ApplyConditions(this QueryConditions conditions, CommandContext context, bool includePaging)
        {
            var parameterBuilder = new IncrementalParameterBuilder("param_{0}");
            var result = new StringBuilder(ApplyWhereString(conditions, context.Text, parameterBuilder, includePaging));
            
            var order = GetOrderByString(conditions, parameterBuilder);
            if (order != string.Empty)
            {
                result.AppendFormat(" {0} ({1})", SQLOrderBy, order);
            }
            context.Text = result.ToString();
        }

        private static string ApplyWhereString(QueryConditions conditions, string commandText, IParameterBuilder parameterBuilder, bool includePaging)
        {
            var result = new StringBuilder(commandText);
            
            for (int i = 0; i < conditions.Filter.Count; i++)
            {
                var filter = conditions.Filter[i];
                if (i > 0 && operationsCode.ContainsKey(filter.FilterOperator))
                {
                    result.AppendFormat(" {0} ", operationsCode[filter.FilterOperator]);
                }
                else
                {
                    result.AppendFormat(" {0} ", FilterQuery.FilterLogicalOperator.And);
                }

                Func<ColumnFilter, string> resolveFilter = columnFilter => OperationResolver
                    .Resolve(columnFilter.ComparisonOperator)
                    .Build(parameterBuilder, new PrefixBasedFieldNameBuilder { FieldName = filter.ColumnName }, columnFilter.Value);

                var sql = string.Join(string.Format(" {0} ", filter.FilterOperator),
                    filter.Filtering.Select(flt => string.Format("({0})", resolveFilter(flt))));

                result.AppendFormat("({0})", sql);
            }

            if (includePaging)
            {
                var query = conditions.WithPaging(result.ToString(), parameterBuilder);
                result.Clear();
                result.Append(query);
            }
            return result.ToString();
        }

        private static string GetOrderByString(QueryConditions conditions, IParameterBuilder parameterBuilder)
        {
            if (!conditions.Sorting.Any())
            {
                return string.Empty;
            }

            var sortColumns =
                from column in conditions.Sorting
                where column.Order != ColumnSort.SortOrder.None
                let order = column.Order == ColumnSort.SortOrder.Desc ? SQLOrderByDescending : SQLOrderByAscending
                select string.Format("{0} {1}", column.ColumnKey, order);

            return string.Join(", ", sortColumns);
        }
    }

    public class Query
    {
        public string Text
        {
            get;
            set;
        }

        public List<QueryParameter> Parameters
        {
            get;
            set;
        }

        internal CommandContext ToContext()
        {
            return new CommandContext()
            {
                Text = Text,
                Type = CommandType.Text,
                Parameters = Parameters.Select(p => p.Convert()).ToList()
            };
        }
    }

    public class QueryParameter
    {
        public QueryParameter(bool input = true)
        {
            Input = input;            
        }

        public string Name
        {
            get;
            set;
        }

        public object Value
        {
            get;
            set;
        }

        public bool Input
        {
            get;
            set;
        }

        public OracleDbType Type
        {
            get;
            set;
        }

        private OracleParameter ConvertSafe()
        {
            try
            {
                OracleParameter ret;
                
                if (Value is Enum)
                    ret = new OracleParameter(Name, (int) Value);
                else if (Value is Boolean)
                    ret = new OracleParameter(Name, (bool) Value ? 1 : 0);
                else
                    ret = new OracleParameter(Name, Value);

                ret.Direction = ParameterDirection.InputOutput;

                return ret;
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Не удалось создать OracleParameter {0} со значением \"{1}\" ({2})",
                    Name, Value == null ? "Null" : Value.ToString(), Value == null ? "Null" : Value.GetType().Name), e);
            }
        }


        public OracleParameter Convert()
        {
            return Input 
                ? ConvertSafe()  
                : new OracleParameter(Name, OracleDbType.Decimal, ParameterDirection.Output);
        }
    }
}
