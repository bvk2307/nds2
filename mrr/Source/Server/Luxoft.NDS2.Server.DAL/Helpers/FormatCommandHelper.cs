﻿using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    /// <summary>
    /// Этот класс реализует вспомогательные методы постороения Oracle ADO.NET обектов
    /// </summary>
    public static class FormatCommandHelper
    {
        /// <summary>
        /// Имя пакета БД Оракл спроцедурами работы с выборками
        /// </summary>

        public const string MethodologistPackage = "NDS2$METHODOLOGIST";

        public const string ReportPackage = "NDS2$REPORTS";

        public const string DiscrepanciesPackage = "NDS2$Discrepancies";

        public static string OraCommandPattern = "BEGIN {0} END;";

        public static string SelectIntoCursor = "OPEN :{0} FOR {1}";

        private const string IdParam = "PID";

        private const string ProcedureFormat = "{0}.{1}";

        public static string PackageProcedure(string packageName, string procedureName)
        {
            return string.Format(ProcedureFormat, packageName, procedureName);
        }

        public static string OraCommand(string commandText)
        {
            return string.Format(OraCommandPattern, commandText);
        }

        public static string SelectCommand(string selectStatement, string cursorAlias)
        {
            return OraCommand(string.Format(SelectIntoCursor, cursorAlias, selectStatement));
        }

        /// <summary>
        /// Строит полное имя процедуры пакета работы с методами методолога
        /// </summary>
        /// <param name="name">Имя процедуры</param>
        /// <returns>Полное имя процедуры</returns>
        public static string MethodologistPackageProcedure(string name)
        {
            return string.Format("{0}.{1}", MethodologistPackage, name);
        }

        /// <summary>
        /// Строит полное имя процедуры пакета работы с отчетами
        /// </summary>
        /// <param name="name">Имя процедуры</param>
        /// <returns>Полное имя процедуры</returns>
        public static string ReportPackageProcedure(string name)
        {
            return string.Format("{0}.{1}", ReportPackage, name);
        }

        /// <summary>
        /// Строит полное имя процедуры пакета работы с расхождениями
        /// </summary>
        /// <param name="name">Имя процедуры</param>
        /// <returns>Полное имя процедуры</returns>
        public static string DiscrepanciesPackageProcedure(string name)
        {
            return string.Format("{0}.{1}", DiscrepanciesPackage, name);
        }

        /// <summary>
        /// Строит параметр запроса типа курсор
        /// </summary>
        /// <param name="name">Имя курсора</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter Cursor(string name)
        {
            return new OracleParameter(name, OracleDbType.RefCursor, ParameterDirection.Output);
        }

        public static OracleParameterCollection With(this OracleParameterCollection parameters, OracleParameter parameter)
        {
            parameters.Add(parameter);

            return parameters;
        }

        /// <summary>
        /// Строит входящий строковый параметр запроса
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter VarcharIn(string name, string value)
        {
            return new OracleParameter(
                name,
                OracleDbType.Varchar2,
                string.IsNullOrWhiteSpace(value) ? (object)DBNull.Value : value,
                ParameterDirection.Input);
        }

        /// <summary>
        /// Строит входящий параметр Id
        /// </summary>
        /// <param name="value">Значение Id</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter IdIn(long value)
        {
            return new OracleParameter(
                IdParam,
                OracleDbType.Int64,
                value,
                ParameterDirection.Input
                );
        }

        /// <summary>
        /// Строит входящий/выходящий параметр Id
        /// </summary>
        /// <param name="value">Значение Id</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter IdInOut(long value)
        {
            return new OracleParameter(
                IdParam,
                OracleDbType.Int64,
                value,
                ParameterDirection.InputOutput
                );
        }

        /// <summary>
        /// Строит параметр Id входящий/выходящий
        /// </summary>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter IdInOut()
        {
            return new OracleParameter(
                IdParam,
                OracleDbType.Int64,
                ParameterDirection.InputOutput);
        }

        /// <summary>
        /// Строит входящий параметр типа NClob
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="content">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter NClobIn(string name, string content)
        {
            return new OracleParameter(
                name,
                OracleDbType.NClob,
                content,
                ParameterDirection.Input);
        }

        /// <summary>
        /// Строит входящий параметр типа Clob
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="content">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter ClobIn(string name, string content)
        {
            return new OracleParameter(
                name,
                OracleDbType.Clob,
                content,
                ParameterDirection.Input);
        }

        /// <summary>
        /// Строит параметр типа Number
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter NumericIn(string name, long value)
        {
            return NumericIn(name, new long?(value));
        }

        /// <summary>
        /// Строит параметр типа Number
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        /// <returns>Ссылка на параметр</returns>
        public static OracleParameter NumericIn(string name, long? value)
        {
            return new OracleParameter(
                name,
                OracleDbType.Int64,
                value.HasValue ? (object)value.Value : DBNull.Value,
                ParameterDirection.Input
                );
        }

        public static OracleParameter NumericOut(string name)
        {
            return new OracleParameter(
                    name,
                    OracleDbType.Decimal,
                    ParameterDirection.Output
                );
        }

        public static OracleParameter NumericRet(string name)
        {
            return new OracleParameter(
                name,
                OracleDbType.Int32,
                ParameterDirection.ReturnValue
                );
        }

        public static OracleParameter DateIn(string name, DateTime? value)
        {
            return new OracleParameter(
                name,
                OracleDbType.Date,
                value,
                ParameterDirection.Input
                );
        }

        public static OracleParameter DateOut(string name)
        {
            return new OracleParameter(
                    name,
                    OracleDbType.Date,
                    ParameterDirection.Output
                );
        }

        public static OracleParameter DecimalIn(string name, decimal? value)
        {
            return new OracleParameter(
                    name,
                    OracleDbType.Decimal,
                    value,
                    ParameterDirection.Input
                );
        }

        public static OracleParameter IntResult(string name)
        {
            return new OracleParameter(name, OracleDbType.Decimal, ParameterDirection.ReturnValue);
        }

        /// <summary>
        /// Создает параметр запроса к БД Oracle для передачи массива строк
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="values">Массив значений параметра</param>
        /// <param name="direction">Направление параметра</param>
        /// <returns>Ссылка на экземпляр параметра</returns>
        public static OracleParameter ToOracleArray(
            this string[] values, 
            string name,
            ParameterDirection direction = ParameterDirection.Input)
        {
            return new OracleParameter
            {
                ParameterName = name,
                OracleDbType = OracleDbType.Varchar2,
                CollectionType = OracleCollectionType.PLSQLAssociativeArray,
                Direction = direction,
                Value = values,
                Size = values.Length,
                ArrayBindSize = values.Select(item => item.Length).ToArray()
            };
        }

        public static OracleParameter LongArrayIn(string name, long[] values) 
        {
            return new OracleParameter(name, OracleDbType.Int64)
            {
                Direction = ParameterDirection.Input,
                Value = values,
                CollectionType = OracleCollectionType.PLSQLAssociativeArray,
                Size = values.Count()
            };
        }

        # region Шаблоны построение SQL запросов

        private static string SelectPatternFormat = "select * from {0} {1} where {2}";

        private static string CountPatternFormat = 
            "begin select count(*) into :{2} from {0} {1} where {3}; end;";

        private static string CountAndSumPatternFormat =
            "begin select count(*), {0} into :{3} from {1} {2} where {4}; end;";

        private static string CursorPatternFormat = "begin open :{0} for {1}; end;";

        public static string ToSelectPattern(this string tableOrViewName, string alias)
        {
            return string.Format(SelectPatternFormat, tableOrViewName, alias, "{0}");
        }

        public static string ToSelectPatternOrdered(this string tableOrViewName, string alias)
        {
            return string.Format(SelectPatternFormat, tableOrViewName, alias, "{0} {1}");
        }

        public static string CountPattern(
            this string tableOrViewName, 
            string alias, 
            string outputParameter)
        {
            return string.Format(
                CountPatternFormat, 
                tableOrViewName, 
                alias, 
                outputParameter, 
                "{0}");
        }

        public static string CountPattern(
            this string tableOrViewName,
            string alias,
            string outputParameter,
            IEnumerable<string> summaryFields)
        {
            if (summaryFields == null || !summaryFields.Any())
            {
                return CountPattern(tableOrViewName, alias, outputParameter);
            }

            var summs = String.Join(",", summaryFields.Select(d => String.Format("SUM({0})", d)));
            var outputParameters = String.Format("{0}, {1}", outputParameter, String.Join(",", summaryFields.Select(d => ":" + d)));

            return string.Format(
                CountAndSumPatternFormat,
                summs,
                tableOrViewName,
                alias,                
                outputParameters,
                "{0}");
        }


        public static string WithCursor(this string queryText, string cursorAlias)
        {
            return string.Format(
                CursorPatternFormat,
                cursorAlias,
                queryText);
        }

        # endregion
    }
}