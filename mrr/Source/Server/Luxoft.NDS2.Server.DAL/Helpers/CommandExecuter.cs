﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    public class CommandExecuter
    {
        private readonly IServiceProvider _service;

        private IErrorHandler _errorHandler;

        public CommandExecuter(IServiceProvider service)
        {
            _service = service;
            _errorHandler = new DefaultErrorHandler();
        }

        public CommandExecuter WithErrorHandler(IErrorHandler errorHandler)
        {
            if (errorHandler == null)
            {
                throw new ArgumentNullException("errorHandler");
            }

            _errorHandler = errorHandler;

            return this;
        }

        public TResult TryRead<TResult>(
            CommandContext commandContext, 
            Func<ResultCommandHelper, TResult> resultBuilder)
        {
            TResult result = default(TResult);

            TryExecute(
                commandContext,
                (command) =>
                {
                    string commandText = command.CommandText;
                    result =
                        resultBuilder(
                            new ResultCommandHelper(
                                command.ExecuteReader(), 
                                commandText));
                });

            return result;
        }

        public void TryExecute(CommandContext commandContext)
        {
            TryExecute(commandContext, (command) => command.ExecuteNonQuery());
        }

        public int TryReadCount(CommandContext commandContext, string outputParamName)
        {
            int retVal = 0;

            TryExecute(
                commandContext, 
                command =>
                    {
                        command.ExecuteNonQuery();
                        retVal = int.Parse(command.Parameters[outputParamName].Value.ToString());
                    });

            return retVal;
        }

        public long TryExecuteAndReadLong(CommandContext commandContext, string outputParamName)
        {
            long retVal = 0;

            TryExecute(
                commandContext,
                command =>
                {
                    command.ExecuteNonQuery();
                    retVal = long.Parse(command.Parameters[outputParamName].Value.ToString());
                });

            return retVal;
        }

        private void TryExecute(CommandContext commandContext, Action<OracleCommand> action)
        {
            var traceInfo = new List<KeyValuePair<string, object>>();
            var watcher = new Stopwatch();

            traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CMD_TEXT, commandContext.Text));

            foreach (var oracleParameter in commandContext.Parameters)
            {
                traceInfo.Add(new KeyValuePair<string, object>(oracleParameter.ParameterName, oracleParameter.Value));
            }

            using (var connection = NewConnection())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = commandContext.Text;
                    command.CommandType = commandContext.Type;
                    command.BindByName = true;
                    command.Parameters.AddRange(commandContext.Parameters.ToArray());

                    try
                    {
                        watcher.Start();
                        connection.Open();

                        watcher.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_CONN_OPEN_TIME, watcher.ElapsedMilliseconds));

                        watcher.Restart();
                        action(command);

                        watcher.Stop();
                        traceInfo.Add(new KeyValuePair<string, object>(Constants.CommonMessages.TRACE_DB_EXEC_TIME, watcher.ElapsedMilliseconds));

                        _service.LogNotification(Constants.CommonMessages.TRACE_DB_CMD_EXECUTED, null, traceInfo);
                    }
                    catch (OracleException oraEx)
                    {
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, oraEx, traceInfo);
                        throw _errorHandler.Handle(oraEx);
                    }
                    catch (Exception ex)
                    {
                        _service.LogError(Constants.CommonMessages.TRACE_DB_CMD_ERROR, null, ex, traceInfo);
                        throw;
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }
        }

        public OracleConnection NewConnection()
        {
            return  new OracleConnection(_service.GetConfigurationValue(Constants.DB_CONFIG_KEY));
        }
    }
}
