﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    public enum ReadError { FieldMissing, DataTypeError };

    public class ResultCommandHelper
    {
        # region Константы

        private const string ErrorFormat = "Ошибка источника данных {0}. {1}";

        private const string ErrorFieldIsMissing = "поле {0} не найдено";

        private const string ErrorFieldConvertionError = "поле {0} некорректный тип данных в строке {1}";

        # endregion

        # region Поля

        private readonly OracleDataReader _reader;

        private readonly string _objectName;

        private int _rowIndex;

        private readonly Dictionary<string, ReadError> _errors = new Dictionary<string,ReadError>();

        # endregion

        # region Конструктор

        public ResultCommandHelper(OracleDataReader reader, string objectName)
        {
            _reader = reader;
            _objectName = objectName;
        }

        # endregion

        # region Чтение строк

        public bool Read()
        {
            Validate();

            if (_reader.Read())
            {
                _rowIndex++;
                return true;
            }

            return false;
        }

        # endregion

        # region Конвертация данных

        public string GetString(string fieldName)
        {
            object val;
            var retVal = string.Empty;

            if (TryGetObject(fieldName, out val))
            {
                retVal = val == null ? string.Empty : val.ToString();
            }

            return retVal;
        }

        public DateTime GetDate(string fieldName)
        {
            object val;
            DateTime result;

            if (TryGetObject(fieldName, out val))
            {
                if (!IsDBNull(val) && val != null && DateTime.TryParse(val.ToString(), out result))
                {
                    return result;
                }

                AddFormatError(fieldName);
            }

            return default(DateTime);
        }

        public DateTime? GetNullableDate(string fieldName)
        {
            object val;
            DateTime? retVal = null;

            if (TryGetObject(fieldName, out val))
            {
                if (IsDBNull(val) || val == null)
                    return null;

                retVal = GetDate(val, fieldName);
            }

            return retVal;
        }

        public bool GetBool(string fieldName)
        {
            return GetInt32(fieldName) > 0;
        }

        public bool? GetNullableBool(string fieldName)
        {
            var intValue = GetNullableInt32(fieldName);
            return intValue.HasValue ? (bool?)(intValue.Value > 0) : null;
        }

        public bool GetBoolExtended(string fieldName)
        {
            bool ret = false;
            int? val = GetNullableInt32(fieldName);
            if (val != null && val > 0)
                ret = true;
            return ret;
        }

        public int GetInt32(string fieldName)
        {
            object val;
            var retVal = default(int);

            if (TryGetObject(fieldName, out val))
            {
                retVal = GetInt32(val, fieldName);
            }

            return retVal;
        }

        public long GetInt64(string fieldName)
        {
            object val;
            var retVal = default(long);

            if (TryGetObject(fieldName, out val))
            {
                retVal = GetInt64(val, fieldName);
            }

            return retVal;
        }

        public decimal GetDecimal(string fieldName)
        {
            object val;
            var retVal = default(decimal);

            if (TryGetObject(fieldName, out val) && val != null && !IsDBNull(val))
            {
                retVal = GetDecimal(val, fieldName);
            }

            return retVal;
        }

        public int? GetNullableInt32(string fieldName)
        {
            object val;
            int? retVal = null;

            if (TryGetObject(fieldName, out val))
            {
                if (IsDBNull(val) || val == null)
                    return null;

                retVal = GetInt32(val, fieldName);
            }

            return retVal;
        }

        public long? GetNullableInt64(string fieldName)
        {
            object val;
            long? retVal = null;

            if (TryGetObject(fieldName, out val))
            {
                if (IsDBNull(val) || val == null)
                    return null;

                retVal = GetInt64(val, fieldName);
            }

            return retVal;
        }

        public decimal? GetNullableDecimal(string fieldName)
        {
            object val;
            decimal? retVal = null;

            if (TryGetObject(fieldName, out val))
            {
                if (IsDBNull(val) || val == null)
                    return null;
                retVal = GetDecimal(val, fieldName);
            }

            return retVal;
        }

        public bool TryGetObject(string fieldName, out object val)
        {
            var result = true;

            try
            {
                val = _reader[fieldName];
            }
            catch (IndexOutOfRangeException)
            {
                if (!_errors.ContainsKey(fieldName))
                {
                    _errors.Add(fieldName, ReadError.FieldMissing);
                }

                val = null;
                result = false;
            }

            return result;
        }
        
        private bool IsDBNull(object value)
        {
            return value == DBNull.Value;
        }

        # endregion

        # region Валидация

        public void Validate()
        {
            if (_errors.Any())
            {
                throw ReaderError();
            }
        }

        private void AddFormatError(string fieldName)
        {
            if (_errors.ContainsKey(fieldName))
            {
                return;
            }

            _errors.Add(fieldName, ReadError.DataTypeError);
        }

        private DatabaseException ReaderError()
        {
            throw new DatabaseException(
                string.Format(
                    ErrorFormat,
                    _objectName,
                    string.Join(
                        ",", 
                        _errors.Select(
                            err => string.Format(
                                    err.Value == ReadError.DataTypeError 
                                        ? ErrorFieldConvertionError 
                                        : ErrorFieldIsMissing, 
                                    err.Key, 
                                    _rowIndex)).ToArray())), null);
        }

        # endregion

        #region приватные геттеры

        private long GetInt64(object val, string fieldName)
        {
            var retVal = default(long);
            try
            {
                retVal = long.Parse(val.ToString());
            }
            catch (NullReferenceException)
            {
                AddFormatError(fieldName);
            }
            catch (ArgumentNullException)
            {
                AddFormatError(fieldName);
            }
            catch (OverflowException)
            {
                AddFormatError(fieldName);
            }
            catch (FormatException)
            {
                AddFormatError(fieldName);
            }
            return retVal;
        }

        private int GetInt32(object val, string fieldName)
        {
            var retVal = default(int);
            try
            {
                retVal = int.Parse(val.ToString());
            }
            catch (NullReferenceException)
            {
                AddFormatError(fieldName);
            }
            catch (ArgumentNullException)
            {
                AddFormatError(fieldName);
            }
            catch (OverflowException)
            {
                AddFormatError(fieldName);
            }
            catch (FormatException)
            {
                AddFormatError(fieldName);
            }
            return retVal;
        }

        private decimal GetDecimal(object val, string fieldName)
        {
            var retVal = default(decimal);
            try
            {
                retVal = Convert.ToDecimal(val);
            }
            catch (NullReferenceException)
            {
                AddFormatError(fieldName);
            }
            catch (ArgumentNullException)
            {
                AddFormatError(fieldName);
            }
            catch (OverflowException)
            {
                AddFormatError(fieldName);
            }
            catch (FormatException)
            {
                AddFormatError(fieldName);
            }
            return retVal;
        }

        private DateTime GetDate(object val, string fieldName)
        {
            var retVal = default(DateTime);
            try
            {
                retVal = DateTime.Parse(val.ToString());
            }
            catch (NullReferenceException)
            {
                AddFormatError(fieldName);
            }
            catch (ArgumentNullException)
            {
                AddFormatError(fieldName);
            }
            catch (OverflowException)
            {
                AddFormatError(fieldName);
            }
            catch (FormatException)
            {
                AddFormatError(fieldName);
            }
            return retVal;
        }

        #endregion
    }
}
