﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    internal static class DataReaderExtension
    {
        public static bool HasField(this IDataRecord reader, string name)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }


            return false;
        }

        #region String

        public static string ReadString(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsString() : default(string);
        }

        public static string ReadStringNullable(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsStringNullable() : string.Empty;
        }

        private static bool AsBoolean(this object value)
        {
            return IsDbNull(value) ? false : Convert.ToInt32(value) > 0;
        }

        private static string AsString(this object value)
        {
            return IsDbNull(value) ? string.Empty : Convert.ToString(value);
        }

        private static string AsStringNullable(this object value)
        {
            return IsDbNull(value) ? null : Convert.ToString(value);
        }
        #endregion        

        #region Int64
        public static Int64 ReadInt64(this IDataRecord reader, string name, Int64 defaultValue = 0)
        {
            return reader.HasField(name) ? reader.ReadNullableInt64(name).GetValueOrDefault(defaultValue) : defaultValue;
        }

        public static Int64? ReadNullableInt64(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsNullableInt64() : null;
        }

        private static Int64? AsNullableInt64(this object value)
        {
            Int64 result;
            return IsDbNull(value) || !Int64.TryParse(Convert.ToString(value), out result) ? (Int64?)null : result;
        }
        #endregion

        #region Int32
        public static Int32 ReadInt(this IDataRecord reader, string name, Int32 defaultValue = 0)
        {
            return reader.HasField(name) ? reader[name].AsNullableInt32().GetValueOrDefault(defaultValue) : defaultValue;
        }

        public static Int32? ReadNullableInt(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsNullableInt32() : null;
        }

        private static Int32? AsNullableInt32(this object value)
        {
            Int32 result;
            return IsDbNull(value) || !Int32.TryParse(Convert.ToString(value), out result) ? (Int32?)null : result;
        }
        #endregion

        #region DateTime
        public static DateTime? ReadDateTime(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsNullableDateTime() : null;
        }

        public static DateTime? ReadDateTimeRemoveMax(this IDataRecord reader, string name, DateTime maxDate)
        {
            if (!reader.HasField(name)) return null;

            var result = reader[name].AsNullableDateTime();
            return result.HasValue && result.Value != maxDate ? result : null;
        }

        private static DateTime? AsNullableDateTime(this object value)
        {

            DateTime result;
            return IsDbNull(value) || !DateTime.TryParse(Convert.ToString(value), out result) ? (DateTime?)null : result;
        }
        #endregion

        #region Decimal
        public static Decimal ReadDecimal(this IDataRecord reader, string name, Decimal defaultValue = 0)
        {
            return reader.HasField(name) ? reader[name].AsNullableDecimal().GetValueOrDefault(defaultValue) : defaultValue;
        }

        public static Decimal? ReadNullableDecimal(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader[name].AsNullableDecimal() : null;
        }

        private static Decimal? AsNullableDecimal(this object value)
        {

            if (value is OracleDecimal)
            {
                return ((OracleDecimal) value).IsNull ? (Decimal?) null : ((OracleDecimal) value).Value;
            }
            else
            {
                Decimal result;
                return IsDbNull(value) || !Decimal.TryParse(Convert.ToString(value), out result) ? (Decimal?) null : result;
            }
        }
        #endregion

        public static bool ReadBoolean(this IDataRecord reader, string name)
        {
            return reader.HasField(name) ? reader.ReadInt(name, 0) > 0 : false;
        }

        private static bool IsDbNull(object data)
        {
            return data == DBNull.Value;
        }

        public static SelectionGroupState State(int included, int all, int disabled = 0)
        {
            if (all == 0 || (all == disabled && included == 0))
            {
                return SelectionGroupState.AllExcludedAndDisabled;
            }

            if (included == 0)
            {
                return SelectionGroupState.AllExcluded;
            }

            if (all == included)
            {
                return SelectionGroupState.AllIncluded;
            }

            return SelectionGroupState.PartiallyIncluded;
        }

        public static int ReadInt(object value)
        {
            int retVal;

            if (!int.TryParse(value.ToString(), out retVal))
            {
                return 0;
            }

            return retVal;
        }

        public static long ReadInt64(object value)
        {
            long retVal;

            if (!long.TryParse(value.ToString(), out retVal))
            {
                return 0;
            }

            return retVal;
        }

        public static decimal ReadDecimal(object value, decimal defaultValue = 0)
        {
            return value.AsNullableDecimal().GetValueOrDefault(defaultValue);
        }

        #region Entities

        public static DeclarationDiscrepancy ReadDiscrepancy(this IDataRecord reader)
        {
            var result = new DeclarationDiscrepancy();

            result.Amount = reader.ReadDecimal(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.Amount));
            result.AmountPvp = reader.ReadDecimal(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.AmountPvp));
            result.ContractorDeclSign = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorDeclSign));
            result.ContractorDocType = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorDocType));
            result.ContractorInn = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorInn));
            result.ContractorInvoiceKey = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorInvoiceKey));
            result.ContractorKpp = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorKpp));
            result.ContractorName = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorName));
            result.ContractorNdsAmount = reader.ReadDecimal(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorNdsAmount));
            result.ContractorInvoiceSource = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorInvoiceSource));
            result.ContractorZip = reader.ReadNullableInt64(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorZip));
            result.DiscrepancyId = reader.ReadInt64(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.DiscrepancyId));
            result.FoundDate = reader.ReadDateTime(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.FoundDate));
            result.KnpStatus = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.KnpStatus));
            result.StatusCode = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.StatusCode));
            result.StatusName = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.StatusName));
            result.TaxPayerDeclSign = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TaxPayerDeclSign));
            result.TaxPayerInvoiceKey = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TaxPayerInvoiceKey));
            result.TaxPayerInvoiceSource = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TaxPayerInvoiceSource));
            result.TaxPayerZip = reader.ReadInt64(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TaxPayerZip));
            result.TypeCode = reader.ReadInt(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TypeCode));
            result.TypeName = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.TypeName));
            result.ContractorInnReorganized =
                reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ContractorInnReorganized));
            result.ActInn = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ActInn));
            result.ActKpp = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.ActKpp));
            result.KnpDocType = reader.ReadString(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.KnpDocType));
            result.KnpDocAmount = reader.ReadNullableDecimal(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.KnpDocAmount));
            result.DecisionAmountDifference =
                reader.ReadNullableDecimal(TypeHelper<DeclarationDiscrepancy>.GetMemberName(x => x.DecisionAmountDifference));

            return result;
        }

        public static ControlRatio ReadControlRatio(this IDataRecord reader)
        {
            return new ControlRatio
            {
                Id = reader.ReadInt64("Id"),
                Decl_Version_Id = reader.ReadInt64("Decl_Version_Id"),
                HasDiscrepancy = reader.ReadBoolean("HasDiscrepancy"),
                HasDiscrepancyString = reader.ReadString("HasDiscrepancyString"),
                DisparityLeft = reader.ReadDecimal("DisparityLeft"),
                DisparityRight = reader.ReadDecimal("DisparityRight"),
                UserComment = reader.ReadString("UserComment"),
                CalculationDate = reader.ReadDateTime("CalculationDate"),
                Type = new ControlRatioType
                {
                    Code = reader.ReadString("TypeCode"),
                    Formulation = reader.ReadString("TypeFormulation"),
                    Description = reader.ReadString("TypeDescription"),
                    CalculationCondition = reader.ReadString("TypeCalculationCondition"),
                    CalculationOperator = reader.ReadString("TypeCalculationOperator"),
                    CalculationLeftSide = reader.ReadString("TypeCalculationLeftSide"),
                    CalculationRightSide = reader.ReadString("TypeCalculationRightSide")
                }
            };
        }

        public static ContragentSummary ReadContragentSummary(this IDataRecord reader)
        {
            var ret = new ContragentSummary();
            ret.CHAPTER8_COUNT = reader.ReadInt("CHAPTER8_COUNT");
            ret.CHAPTER9_COUNT = reader.ReadInt("CHAPTER9_COUNT");
            ret.CHAPTER10_COUNT = reader.ReadInt("CHAPTER10_COUNT");
            ret.CHAPTER11_COUNT = reader.ReadInt("CHAPTER11_COUNT");
            ret.CHAPTER12_COUNT = reader.ReadInt("CHAPTER12_COUNT");
            ret.MARK = reader.ReadString("MARK");
            ret.DECLARATION_VERSION_ID = reader.ReadInt64("DECLARATION_VERSION_ID");
            ret.INN_DECLARANT = reader.ReadString("INN_DECLARANT");
            ret.INN_REORGANIZED = reader.ReadString("INN_REORGANIZED");
            ret.KPP_EFFECTIVE = reader.ReadString("KPP_EFFECTIVE");
            ret.PERIOD_CODE = reader.ReadString("PERIOD_CODE");
            ret.FISCAL_YEAR = reader.ReadString("FISCAL_YEAR");
            ret.DOC_TYPE = reader.ReadString("DOC_TYPE");
            ret.DOC_TYPE_CODE = reader.ReadString("DOC_TYPE_CODE");
            ret.SUR_CODE = reader.ReadInt("SUR_CODE");
            ret.CONTRACTOR_INN = reader.ReadString("CONTRACTOR_INN");
            ret.CONTRACTOR_KPP = reader.ReadString("CONTRACTOR_KPP");
            ret.CONTRACTOR_NAME = reader.ReadString("CONTRACTOR_NAME");
            ret.CHAPTER8_AMOUNT = reader.ReadNullableDecimal("CHAPTER8_AMOUNT");
            ret.CHAPTER9_AMOUNT = reader.ReadNullableDecimal("CHAPTER9_AMOUNT");
            ret.CHAPTER10_AMOUNT = reader.ReadNullableDecimal("CHAPTER10_AMOUNT");
            ret.CHAPTER11_AMOUNT = reader.ReadNullableDecimal("CHAPTER11_AMOUNT");
            ret.DECLARED_TAX_AMNT = reader.ReadNullableDecimal("DECLARED_TAX_AMNT");
            ret.CALCULATED_TAX_AMNT_R9 = reader.ReadNullableDecimal("CALCULATED_TAX_AMNT_R9");
            ret.CALCULATED_TAX_AMNT_R12 = reader.ReadNullableDecimal("CALCULATED_TAX_AMNT_R12");
            ret.CHAPTER10_AMOUNT_NDS = reader.ReadNullableDecimal("CHAPTER10_AMOUNT_NDS");
            ret.CHAPTER11_AMOUNT_NDS = reader.ReadNullableDecimal("CHAPTER11_AMOUNT_NDS");
            ret.DECLARED_TAX_AMNT_TOTAL = reader.ReadNullableDecimal("DECLARED_TAX_AMNT_TOTAL");
            ret.GAP_QUANTITY_8 = reader.ReadNullableDecimal("GAP_QUANTITY_8");
            ret.GAP_QUANTITY_9 = reader.ReadNullableDecimal("GAP_QUANTITY_9");
            ret.GAP_QUANTITY_10 = reader.ReadNullableDecimal("GAP_QUANTITY_10");
            ret.GAP_QUANTITY_11 = reader.ReadNullableDecimal("GAP_QUANTITY_11");
            ret.GAP_QUANTITY_12 = reader.ReadNullableDecimal("GAP_QUANTITY_12");
            ret.GAP_AMOUNT_8 = reader.ReadNullableDecimal("GAP_AMOUNT_8");
            ret.GAP_AMOUNT_9 = reader.ReadNullableDecimal("GAP_AMOUNT_9");
            ret.GAP_AMOUNT_10 = reader.ReadNullableDecimal("GAP_AMOUNT_10");
            ret.GAP_AMOUNT_11 = reader.ReadNullableDecimal("GAP_AMOUNT_11");
            ret.GAP_AMOUNT_12 = reader.ReadNullableDecimal("GAP_AMOUNT_12");
            ret.OTHER_QUANTITY_8 = reader.ReadNullableDecimal("OTHER_QUANTITY_8");
            ret.OTHER_QUANTITY_9 = reader.ReadNullableDecimal("OTHER_QUANTITY_9");
            ret.OTHER_QUANTITY_10 = reader.ReadNullableDecimal("OTHER_QUANTITY_10");
            ret.OTHER_QUANTITY_11 = reader.ReadNullableDecimal("OTHER_QUANTITY_11");
            ret.OTHER_QUANTITY_12 = reader.ReadNullableDecimal("OTHER_QUANTITY_12");
            ret.OTHER_AMOUNT_8 = reader.ReadNullableDecimal("OTHER_AMOUNT_8");
            ret.OTHER_AMOUNT_9 = reader.ReadNullableDecimal("OTHER_AMOUNT_9");
            ret.OTHER_AMOUNT_10 = reader.ReadNullableDecimal("OTHER_AMOUNT_10");
            ret.OTHER_AMOUNT_11 = reader.ReadNullableDecimal("OTHER_AMOUNT_11");
            ret.OTHER_AMOUNT_12 = reader.ReadNullableDecimal("OTHER_AMOUNT_12");
            return ret;
        }

        public static ContragentParamsSummary ReadContragentParamsSummary(this IDataRecord reader)
        {
            var obj = new ContragentParamsSummary
            {
                DECLARATION_VERSION_ID = reader.ReadInt64("DECLARATION_VERSION_ID"),
                INN = reader.ReadString("INN"),
                CHAPTER = reader.ReadInt("CHAPTER"),
                MARK = reader.ReadString("MARK"),
                DIS_STATUS = reader.ReadNullableInt("DIS_STATUS"),
                SUR_CODE = reader.ReadInt("SUR_CODE"),
                DOC_TYPE = reader.ReadString("DOC_TYPE"),
                DOC_TYPE_CODE = reader.ReadInt("DOC_TYPE_CODE"),
                CONTRACTOR_INN = reader.ReadString("CONTRACTOR_INN"),
                CONTRACTOR_KPP = reader.ReadString("CONTRACTOR_KPP"),
                CONTRACTOR_NAME = reader.ReadString("CONTRACTOR_NAME"),
                INVOICE_NUM = reader.ReadString("INVOICE_NUM"),
                INVOICE_DATE = reader.ReadDateTime("INVOICE_DATE"),
                AMOUNT_TOTAL = reader.ReadNullableDecimal("AMOUNT_TOTAL"),
                AMOUNT_NDS = reader.ReadNullableDecimal("AMOUNT_NDS"),
                DIS_TYPE_NAME = reader.ReadString("DIS_TYPE_NAME"),
                DIS_STATUS_NAME = reader.ReadString("DIS_STATUS_NAME"),
                DIS_ID = reader.ReadNullableInt64("DIS_ID"),
                DIS_TYPE = reader.ReadNullableInt("DIS_TYPE"),
                DIS_AMOUNT = reader.ReadNullableDecimal("DIS_AMOUNT"),
            };
            return obj;
        }

        public static DiscrepancyDocumentInfo ReadDocumentInfo(this IDataRecord reader)
        {
            var claimInfo = new DiscrepancyDocumentInfo();

            claimInfo.Id = reader.ReadInt64("doc_id");
            claimInfo.DeclarationId = reader.ReadInt64("ref_entity_id");
            claimInfo.DeclarationVersionId = reader.ReadInt64("DECLARATION_VERSION_ID");
            claimInfo.CreateDate = reader.ReadDateTime("create_date");
            claimInfo.SendDate = reader.ReadDateTime("tax_payer_send_date");
            claimInfo.DeliveryDate = reader.ReadDateTime("tax_payer_delivery_date");
            claimInfo.ReplyDate = reader.ReadDateTime("tax_payer_reply_date");
            claimInfo.StatusChangeDate = reader.ReadDateTime("status_date");
            claimInfo.Status = new DictionaryEntry() { EntryId = reader.ReadInt("status"), EntryValue = reader.ReadString("statusDesc") };
            claimInfo.EodId = reader.ReadString("external_doc_num");
            claimInfo.EodDate = reader.ReadDateTime("external_doc_date");
            claimInfo.DiscrepancyAmount = reader.ReadDecimal("discrepancy_amount");
            claimInfo.DiscrepancyCount = reader.ReadInt("discrepancy_count");
            claimInfo.TaxPayerInn = reader.ReadString("TaxPayerInn");
            claimInfo.TaxPayerName = reader.ReadString("TaxPayerName");
            claimInfo.Soun = new CodeValueDictionaryEntry(reader.ReadString("sono_code"), "");
            claimInfo.SeodSentDate = reader.ReadDateTime("external_doc_date");
            claimInfo.SeodAccepted = reader.ReadInt("seod_accepted");
            claimInfo.SeodAcceptDate = reader.ReadDateTime("seod_accept_date");
            claimInfo.CloseDate = reader.ReadDateTime("close_date");
            claimInfo.DocType = new DictionaryEntry() { EntryId = reader.ReadInt("doc_type"), EntryValue = reader.ReadString("doc_type_name") };
            claimInfo.UserComment = reader.ReadString("user_comment");
            claimInfo.ProlongAnswerDate = reader.ReadDateTime("prolong_answer_date");
            claimInfo.KnpSyncStatus = reader.ReadInt("knp_sync_status");

            return claimInfo;
        }

        public static DocumentKNP ReadDocumentKNP(this IDataRecord reader)
        {
            var ret = new DocumentKNP();

            ret.Id = reader.ReadNullableInt64("id");
            ret.TypeName = reader.ReadString("name");
            ret.DocNum = reader.ReadString("num");
            ret.Date = reader.ReadDateTime("dt");
            ret.DocStatus = reader.ReadString("status");
            ret.Type = reader.ReadNullableInt64("type_doc_knp");
            ret.StateExplainCanOpen = reader.ReadNullableInt("explain_can_open");

            return ret;
        }



        public static Inspection ReadInspection(this IDataRecord reader)
        {
            return new Inspection
            {
                Code = reader.ReadString("Code"),
                Name = reader.ReadString("Name"),
                Selected = reader.ReadBoolean("IS_IN_PROCESS")
            };
        }

        public static ResponsibilityRegion ReadResponsibilityRegion(this IDataRecord reader)
        {
            return new ResponsibilityRegion
            {
                Region = reader.ReadMetodologRegion()
            };
        }

        public static Region ReadMetodologRegion(this IDataRecord reader)
        {
            return new Region
            {
                Id = reader.ReadInt64("Id"),
                Code = reader.ReadString("Code"),
                Name = reader.ReadString("Name"),
                RegionFullName = reader.ReadString("RegionFullName"),
                IsNew = reader.ReadBoolean("Is_New"),
                Context = (RegionContext)reader.ReadInt64("Context_Id")
            };
        }

        private static void FillInvoice(this IDataRecord reader, Invoice ret)
        {
            ret.DeclarationId = reader.ReadInt64("DECLARATION_VERSION_ID");
            ret.CHAPTER = reader.ReadInt("CHAPTER");
            ret.CREATE_DATE = reader.ReadDateTime("CREATE_DATE");
            ret.RECEIVE_DATE = reader.ReadDateTime("RECEIVE_DATE");
            ret.OPERATION_CODE = reader.ReadString("OPERATION_CODE");
            ret.INVOICE_NUM = reader.ReadString("INVOICE_NUM");
            ret.INVOICE_DATE = reader.ReadDateTime("INVOICE_DATE");
            ret.CHANGE_NUM = reader.ReadString("CHANGE_NUM");
            ret.CHANGE_DATE = reader.ReadDateTime("CHANGE_DATE");
            ret.CORRECTION_NUM = reader.ReadString("CORRECTION_NUM");
            ret.CORRECTION_DATE = reader.ReadDateTime("CORRECTION_DATE");
            ret.CHANGE_CORRECTION_NUM = reader.ReadString("CHANGE_CORRECTION_NUM");
            ret.CHANGE_CORRECTION_DATE = reader.ReadDateTime("CHANGE_CORRECTION_DATE");

            ret.RECEIPT_DOC_NUM = reader.ReadStringNullable("RECEIPT_DOC_NUM");
            string RECEIPT_DOC_DATE_STR = reader.ReadStringNullable("RECEIPT_DOC_DATE");
            ret.RECEIPT_DOC_DATE = null;
            DateTime dtParse = DateTime.Now;
            if (ret.RECEIPT_DOC_DATE_STR != null && DateTime.TryParse(RECEIPT_DOC_DATE_STR, out dtParse))
                ret.RECEIPT_DOC_DATE = dtParse;

            ret.RECEIPT_DOC_DATE_STR = new DocumentNumDates();
            ret.RECEIPT_DOC_DATE_STR.PaymentDocs.Add(new DocumentNumDate() { Num = ret.RECEIPT_DOC_NUM, Date = ret.RECEIPT_DOC_DATE, DateString = RECEIPT_DOC_DATE_STR });

            string docDates = reader.ReadStringNullable("BUY_ACCEPT_DATE");
            ret.BUY_ACCEPT_DATE = new DocDates(docDates);
            ret.BUYER_INN = reader.ReadString("BUYER_INN");
            ret.BUYER_KPP = reader.ReadString("BUYER_KPP");
            ret.BUYER_NAME = reader.ReadString("BUYER_NAME");
            ret.SELLER_INN = reader.ReadString("SELLER_INN");
            ret.SELLER_KPP = reader.ReadString("SELLER_KPP");
            ret.SELLER_INN_RESOLVED = reader.ReadString("SELLER_INN");
            ret.SELLER_KPP_RESOLVED = reader.ReadString("SELLER_KPP");
            ret.SELLER_NAME = reader.ReadString("SELLER_NAME");
            ret.SELLER_INVOICE_NUM = reader.ReadString("SELLER_INVOICE_NUM");
            ret.SELLER_INVOICE_DATE = reader.ReadDateTime("SELLER_INVOICE_DATE");
            ret.BROKER_INN = reader.ReadString("BROKER_INN");
            ret.BROKER_KPP = reader.ReadString("BROKER_KPP");
            ret.BROKER_NAME = reader.ReadString("BROKER_NAME");
            ret.DEAL_KIND_CODE = reader.ReadInt("DEAL_KIND_CODE");
            ret.CUSTOMS_DECLARATION_NUM = reader.ReadString("CUSTOMS_DECLARATION_NUM");
            ret.OKV_CODE = reader.ReadString("OKV_CODE");
            ret.PRICE_BUY_AMOUNT = reader.ReadNullableDecimal("PRICE_BUY_AMOUNT");
            ret.PRICE_BUY_NDS_AMOUNT = reader.ReadNullableDecimal("PRICE_BUY_NDS_AMOUNT");
            ret.PRICE_SELL = reader.ReadNullableDecimal("PRICE_SELL");
            ret.PRICE_SELL_IN_CURR = reader.ReadNullableDecimal("PRICE_SELL_IN_CURR");
            ret.PRICE_SELL_18 = reader.ReadNullableDecimal("PRICE_SELL_18");
            ret.PRICE_SELL_10 = reader.ReadNullableDecimal("PRICE_SELL_10");
            ret.PRICE_SELL_0 = reader.ReadNullableDecimal("PRICE_SELL_0");
            ret.PRICE_NDS_18 = reader.ReadNullableDecimal("PRICE_NDS_18");
            ret.PRICE_NDS_10 = reader.ReadNullableDecimal("PRICE_NDS_10");
            ret.PRICE_TAX_FREE = reader.ReadNullableDecimal("PRICE_TAX_FREE");
            ret.PRICE_TOTAL = reader.ReadNullableDecimal("PRICE_TOTAL");
            ret.PRICE_NDS_TOTAL = reader.ReadNullableDecimal("PRICE_NDS_TOTAL");
            ret.DIFF_CORRECT_DECREASE = reader.ReadNullableDecimal("DIFF_CORRECT_DECREASE");
            ret.DIFF_CORRECT_INCREASE = reader.ReadNullableDecimal("DIFF_CORRECT_INCREASE");
            ret.DIFF_CORRECT_NDS_DECREASE = reader.ReadNullableDecimal("DIFF_CORRECT_NDS_DECREASE");
            ret.DIFF_CORRECT_NDS_INCREASE = reader.ReadNullableDecimal("DIFF_CORRECT_NDS_INCREASE");
            ret.DECL_CORRECTION_NUM = reader.ReadString("DECL_CORRECTION_NUM");
            ret.PRICE_NDS_BUYER = reader.ReadNullableDecimal("PRICE_NDS_BUYER");
            ret.FULL_TAX_PERIOD = reader.ReadString("FULL_TAX_PERIOD");
            ret.TAX_PERIOD = reader.ReadString("TAX_PERIOD");
            ret.FISCAL_YEAR = reader.ReadString("FISCAL_YEAR");
            ret.ROW_KEY = reader.ReadString("Row_Key");
            ret.ACTUAL_ROW_KEY = reader.ReadString("Actual_Row_Key");
            ret.ORDINAL_NUMBER = reader.ReadInt64("ORDINAL_NUMBER");
            ret.SELLER_AGENCY_INFO_INN = reader.ReadString("SELLER_AGENCY_INFO_INN");
            ret.SELLER_AGENCY_INFO_KPP = reader.ReadString("SELLER_AGENCY_INFO_KPP");
            ret.SELLER_AGENCY_INFO_NAME = reader.ReadString("SELLER_AGENCY_INFO_NAME");
            ret.SELLER_AGENCY_INFO_NUM = reader.ReadString("SELLER_AGENCY_INFO_NUM");
            ret.SELLER_AGENCY_INFO_DATE = reader.ReadDateTime("SELLER_AGENCY_INFO_DATE");
            ret.IS_CHANGED = reader.ReadNullableInt("IS_CHANGED");
            ret.COMPARE_ROW_KEY = reader.ReadString("COMPARE_ROW_KEY");
        }

        public static Invoice ReadInvoice(this IDataRecord reader)
        {
            var ret = new Invoice();

            FillInvoice(reader, ret);

            ret.CONTRACTOR_DECL_IN_MC = reader.ReadNullableInt("CONTRACTOR_DECL_IN_MC") == 1;

            ret.CONTRACTOR_KEY = reader.ReadNullableInt64("CONTRACTOR_KEY");

            ret.IS_MATCHING_ROW = reader.ReadInt("IS_MATCHING_ROW") == 1;

            ret.REQUEST_ID = reader.ReadInt64("REQUEST_ID");

            return ret;
        }

        public static InvoiceRelated ReadInvoiceRelated(this IDataRecord reader)
        {
            var ret = new InvoiceRelated();

            FillInvoice(reader, ret);

            ret.DISPLAY_FULL_TAX_PERIOD = reader.ReadString("DISPLAY_FULL_TAX_PERIOD");
            ret.DocId = (long)reader.ReadDecimal("DOC_ID");

            return ret;
        }

        public static InvoiceCorrection ReadInvoiceCorrection(this IDataRecord reader)
        {
            var ret = new InvoiceCorrection
            {
                EXPLAIN_ID = reader.ReadInt64("EXPLAIN_ID"),
                INVOICE_ORIGINAL_ID = reader.ReadString("INVOICE_ORIGINAL_ID"),
                FIELD_NAME = reader.ReadString("FIELD_NAME"),
                FIELD_VALUE = reader.ReadString("FIELD_VALUE")
            };

            return ret;
        }

        public static DiscrepancyDocumentInvoice ReadDiscrepancyDocumentInvoice(this IDataRecord reader)
        {
            var ret = new DiscrepancyDocumentInvoice();

            ret.IS_CHANGED = reader.ReadNullableInt("IS_CHANGED");
            ret.CHAPTER = reader.ReadInt("CHAPTER");
            ret.INVOICE_NUM = reader.ReadString("INVOICE_NUM");
            ret.INVOICE_DATE = reader.ReadDateTime("INVOICE_DATE");
            ret.ROW_KEY = reader.ReadString("ROW_KEY");
            ret.CALC_PRICE_WITH_NDS = reader.ReadNullableDecimal("CALC_PRICE_WITH_NDS");
            ret.CALC_PRICE_NDS = reader.ReadNullableDecimal("CALC_PRICE_NDS");
            ret.DISPLAY_FULL_TAX_PERIOD = reader.ReadString("DISPLAY_FULL_TAX_PERIOD");
            ret.GAP_AMOUNT = reader.ReadNullableDecimal("GAP_AMOUNT");
            ret.NDS_AMOUNT = reader.ReadNullableDecimal("NDS_AMOUNT");

            return ret;
        }

        public static DiscrepancyStatusInfo ReadDiscrepancyStatusInfo(this IDataRecord reader)
        {
            var ret = new DiscrepancyStatusInfo();
            ret.DocumentId = reader.ReadInt64("DocumentId");
            ret.RowKey = reader.ReadString("RowKey");
            ret.DiscrepancyType = reader.GetNullableInt32("DiscrepancyType");
            ret.DiscrepancyStatus = reader.GetNullableInt32("DiscrepancyStatus");
            ret.Count = reader.GetInt32("Count");
            return ret;
        }

        //TODO: Deprecated
        public static DiscrepancyDocumentInvoice ReadDiscrepancyDocumentInvoiceCalculate(this IDataRecord reader)
        {
            var ret = new DiscrepancyDocumentInvoice();

            ret.ROW_KEY = reader.ReadString("ROW_KEY");

            ret.GAP_AMOUNT = reader.ReadNullableDecimal("GAP_AMOUNT");
            ret.NDS_AMOUNT = reader.ReadNullableDecimal("NDS_AMOUNT");
            ret.GAP_STATUS = reader.ReadNullableInt("GAP_STATUS");
            ret.NDS_STATUS = reader.ReadNullableInt("NDS_STATUS");

            return ret;
        }

        public static ExplainInvoice ReadExplainInvoice(this IDataRecord reader)
        {
            ExplainInvoice ret = new ExplainInvoice();

            ret.DeclarationId = reader.ReadInt64("DECLARATION_VERSION_ID");
            ret.CHAPTER = reader.ReadInt("INVOICE_CHAPTER");
            ret.CREATE_DATE_DT = reader.ReadDateTime("CREATE_DATE");
            ret.RECEIVE_DATE_DT = reader.ReadDateTime("RECEIVE_DATE");
            ret.OPERATION_CODE = string.Empty;//reader.ReadString("OPERATION_CODE");
            ret.INVOICE_NUM = reader.ReadString("INVOICE_NUM");
            ret.INVOICE_DATE_DT = reader.ReadDateTime("INVOICE_DATE");
            ret.CHANGE_NUM_I = reader.ReadNullableInt("CHANGE_NUM");
            ret.CHANGE_DATE_DT = reader.ReadDateTime("CHANGE_DATE");
            ret.CORRECTION_NUM = reader.ReadString("CORRECTION_NUM");
            ret.CORRECTION_DATE_DT = reader.ReadDateTime("CORRECTION_DATE");
            ret.CHANGE_CORRECTION_NUM = reader.ReadString("CHANGE_CORRECTION_NUM");
            ret.CHANGE_CORRECTION_DATE_DT = reader.ReadDateTime("CHANGE_CORRECTION_DATE");
            ret.RECEIPT_DOC_NUM = string.Empty;//reader.ReadString("RECEIPT_DOC_NUM");
            ret.BUY_ACCEPT_DATE_DT = null;// reader.ReadDateTime("BUY_ACCEPT_DATE");
            ret.BUYER_INN = null;//reader.ReadString("BUYER_INN");
            ret.BUYER_KPP = null;//reader.ReadString("BUYER_KPP");
            ret.SELLER_INN = null; // reader.ReadString("SELLER_INN");
            ret.SELLER_KPP = null; // reader.ReadString("SELLER_KPP");
            ret.SELLER_INVOICE_NUM = reader.ReadString("SELLER_INVOICE_NUM");
            ret.SELLER_INVOICE_DATE_DT = reader.ReadDateTime("SELLER_INVOICE_DATE");
            ret.BROKER_INN = reader.ReadString("BROKER_INN");
            ret.BROKER_KPP = reader.ReadString("BROKER_KPP");
            ret.DEAL_KIND_CODE_I = reader.ReadNullableInt("DEAL_KIND_CODE");
            ret.CUSTOMS_DECLARATION_NUM = reader.ReadString("CUSTOMS_DECLARATION_NUM");
            ret.OKV_CODE = reader.ReadString("OKV_CODE");
            ret.PRICE_BUY_AMOUNT_DC = reader.ReadNullableDecimal("PRICE_BUY_AMOUNT");
            ret.PRICE_BUY_NDS_AMOUNT = reader.ReadNullableDecimal("PRICE_BUY_NDS_AMOUNT");
            ret.PRICE_SELL_DC = reader.ReadNullableDecimal("PRICE_SELL");
            ret.PRICE_SELL_IN_CURR_DC = reader.ReadNullableDecimal("PRICE_SELL_IN_CURR");
            ret.PRICE_SELL_18_DC = reader.ReadNullableDecimal("PRICE_SELL_18");
            ret.PRICE_SELL_10_DC = reader.ReadNullableDecimal("PRICE_SELL_10");
            ret.PRICE_SELL_0_DC = reader.ReadNullableDecimal("PRICE_SELL_0");
            ret.PRICE_NDS_18 = reader.ReadNullableDecimal("PRICE_NDS_18");
            ret.PRICE_NDS_10 = reader.ReadNullableDecimal("PRICE_NDS_10");
            ret.PRICE_TAX_FREE_DC = reader.ReadNullableDecimal("PRICE_TAX_FREE");
            ret.PRICE_TOTAL_DC = reader.ReadNullableDecimal("PRICE_TOTAL");
            ret.PRICE_NDS_TOTAL = reader.ReadNullableDecimal("PRICE_NDS_TOTAL");
            ret.DIFF_CORRECT_DECREASE = reader.ReadNullableDecimal("DIFF_CORRECT_DECREASE");
            ret.DIFF_CORRECT_INCREASE = reader.ReadNullableDecimal("DIFF_CORRECT_INCREASE");
            ret.DIFF_CORRECT_NDS_DECREASE_DC = reader.ReadNullableDecimal("DIFF_CORRECT_NDS_DECREASE");
            ret.DIFF_CORRECT_NDS_INCREASE_DC = reader.ReadNullableDecimal("DIFF_CORRECT_NDS_INCREASE");
            ret.DECL_CORRECTION_NUM = reader.ReadString("DECL_CORRECTION_NUM");
            ret.PRICE_NDS_BUYER = reader.ReadNullableDecimal("PRICE_NDS_BUYER");
            ret.DISPLAY_FULL_TAX_PERIOD = reader.ReadString("DISPLAY_FULL_TAX_PERIOD");
            ret.TAX_PERIOD = reader.ReadString("TAX_PERIOD");
            ret.FISCAL_YEAR = reader.ReadString("FISCAL_YEAR");
            ret.ROW_KEY = reader.ReadString("INVOICE_ROW_KEY");
            ret.ACTUAL_ROW_KEY = reader.ReadString("Actual_Row_Key");
            ret.PRIZNAK_DOP_LIST = reader.ReadBoolean("IS_DOP_LIST");
            ret.ORDINAL_NUMBER = reader.ReadInt64("ORDINAL_NUMBER");
            ret.SELLER_AGENCY_INFO_INN = reader.ReadString("SELLER_AGENCY_INFO_INN");
            ret.SELLER_AGENCY_INFO_KPP = reader.ReadString("SELLER_AGENCY_INFO_KPP");
            ret.SELLER_AGENCY_INFO_NAME = reader.ReadString("SELLER_AGENCY_INFO_NAME");
            ret.SELLER_AGENCY_INFO_NUM = reader.ReadString("SELLER_AGENCY_INFO_NUM");
            ret.SELLER_AGENCY_INFO_DATE_DT = reader.ReadDateTime("SELLER_AGENCY_INFO_DATE");
            ret.DocId = reader.ReadInt64("DOC_ID");

            ret.Type = (ExplainInvoiceType)reader.ReadInt("Type");
            ret.AddState((ExplainInvoiceState)reader.ReadInt("State"));

            ret.CopyToStringProperties();

            return ret;
        }

        public static ExplainInvoiceCorrect ReadExplainInvoiceCorrect(this IDataRecord reader)
        {
            ExplainInvoiceCorrect ret = new ExplainInvoiceCorrect();
            ret.ExplainId = reader.ReadInt64("explain_Id");
            ret.ExplainIncomingDate = (DateTime)reader.ReadDateTime("incoming_date");
            ret.FieldName = reader.ReadString("FIELD_NAME");
            ret.FieldValue = reader.ReadString("FIELD_VALUE");
            ret.InvoiceId = reader.ReadString("invoice_original_id");
            return ret;
        }

        public static ExplainInvoiceCorrectState ReadExplainInvoiceCorrectState(this IDataRecord reader)
        {
            ExplainInvoiceCorrectState ret = new ExplainInvoiceCorrectState();
            ret.ExplainId = reader.ReadInt64("explain_Id");
            ret.InvoiceId = reader.ReadString("invoice_original_id");
            ret.State = (ExplainInvoiceStateInThis)reader.ReadInt("invoice_state");
            ret.ExplainIncomingDate = (DateTime)reader.ReadDateTime("incoming_date");
            ret.RECEIVE_BY_TKS = reader.ReadBoolean("RECEIVE_BY_TKS");
            return ret;
        }

        public static DocumentStatusHistory ReadDocumentStatusHistory(this IDataRecord reader)
        {
            DocumentStatusHistory ret = new DocumentStatusHistory();
            ret.StatusCode = reader.ReadInt("status");
            ret.StatusDate = reader.ReadDateTime("status_date").Value;
            return ret;
        }

        public static ExplainReplyInfo ReadExplainReplyInfo(this IDataRecord reader)
        {
            var obj = new ExplainReplyInfo();

            obj.EXPLAIN_ID = reader.ReadInt64("explain_id");
            obj.ZIP = reader.ReadInt64("zip");
            obj.DOC_ID = reader.ReadInt64("doc_id");
            obj.DECLARATION_VERSION_ID = reader.ReadInt64("declaration_version_id");
            obj.TYPE_ID = reader.ReadInt("type_id");
            obj.TYPE_NAME = reader.ReadString("type_name");
            obj.INCOMING_NUM = reader.ReadString("incoming_num");
            obj.INCOMING_DATE = (DateTime)reader.ReadDateTime("incoming_date");
            obj.executor_receive_date = reader.ReadDateTime("executor_receive_date");
            obj.doc_num = reader.ReadString("doc_num");
            obj.doc_date = reader.ReadDateTime("doc_date");
            obj.send_date_to_iniciator = reader.ReadDateTime("send_date_to_iniciator");
            obj.RECEIVE_BY_TKS = reader.ReadBoolean("receive_by_tks");
            obj.status_id = reader.ReadInt("status_id");
            obj.status_name = reader.ReadString("status_name");
            obj.status_set_date = reader.ReadDateTime("status_set_date");
            obj.user_comment = reader.ReadString("user_comment");
            obj.FILE_ID = reader.ReadString("file_id");
            obj.ExplainOtherReason = reader.ReadString("ExplainOtherReason");
            return obj;
        }

        public static ExplainReplyHistoryStatus ReadExplainReplyHistoryStatus(this IDataRecord reader)
        {
            var obj = new ExplainReplyHistoryStatus();
            obj.Status_id = reader.ReadInt("status_id");
            obj.Submit_date = (DateTime)reader.ReadDateTime("submit_date");
            return obj;
        }

        public static InvoiceExplain ReadInvoiceExplainData(this IDataRecord reader)
        {
            var ret = new InvoiceExplain();

            ret.InvoiceKey = reader.ReadString("ROW_KEY");
            ret.DocumentType = reader.ReadInt("DOC_TYPE");
            ret.DocumentNumber = reader.ReadString("INCOMING_NUM");
            ret.DocumentDate = reader.ReadDateTime("INCOMING_DATE");
            ret.Rank = reader.ReadInt("RANK");

            return ret;
        }

        #endregion

        private const string MESSAGE_FORMAT_FIELD_NOT_FOUND = "Поле {0}.{1} не найдено в БД";
        private const string MESSAGE_FORMAT_CAST_ERROR = "Ошибка при преобразовании типа дынных {0} -> {1}.{2}";

        public static T Read<T>(this IDataRecord reader, ILogProvider logger) where T : new()
        {
            var result = new T();
            var returnedColumns = new List<string>(reader.FieldCount);

            for (int i = 0; i < reader.FieldCount; i++)
            {
                returnedColumns.Add(reader.GetName(i));
            }

            foreach (var property in typeof(T).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(IgnoreLoadAttribute), false).Length == 0 && returnedColumns.Any(i => i.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    object obj, value;
                    obj = reader[property.Name];  //присутствует в результате запроса
                    value = GetCastMethod(property.PropertyType)(obj); //правильный тип
                    property.SetValue(result, value, null);
                }
            }
            return result;
        }

        public static T ReadPrimitive<T>(this IDataRecord reader, ILogProvider logger) where T : new()
        {
            var result = new T();
            object obj, value;
            obj = reader[0];  //присутствует в результате запроса
            value = GetCastMethod(typeof(T))(obj); //правильный тип
            return (T)value;
        }

        private static Func<object, object> Cast<T>(Func<object, T> convert)
        {
            return obj => obj == DBNull.Value ? default(T) : obj.GetType() == typeof(T) ? obj : convert(obj);
        }

        private static Func<object, object> GetCastMethod(Type type)
        {
            if (type == typeof(bool))
            {
                return Cast(obj => obj.AsBoolean());
            }
            if (type == typeof(int))
            {
                return Cast(obj => obj.AsNullableInt32().GetValueOrDefault());
            }
            if (type == typeof(int?))
            {
                return Cast(obj => obj.AsNullableInt32());
            }
            if (type == typeof(long))
            {
                return Cast(obj => obj.AsNullableInt64().GetValueOrDefault());
            }
            if (type == typeof(long?))
            {
                return Cast(obj => obj.AsNullableInt64());
            }
            if (type == typeof(decimal))
            {
                return Cast(obj => obj.AsNullableDecimal().GetValueOrDefault());
            }
            if (type == typeof(decimal?))
            {
                return Cast(obj => obj.AsNullableDecimal());
            }
            if (type == typeof(DateTime?))
            {
                return Cast(obj => obj.AsNullableDateTime());
            }
            if (type == typeof(string))
            {
                return Cast(obj => obj.AsString());
            }
            if (type == typeof(DiscrepancyStatus))
            {
                return Cast(obj => obj.AsNullableInt32());
            }
            if (type == typeof(DiscrepancyStage))
            {
                return Cast(obj => obj.AsNullableInt32());
            }
            throw new NotSupportedException();
        }

        public static NotReflectedInvoice ReadNotReflectedInvoice(this IDataRecord reader)
        {
            return new NotReflectedInvoice
            {
                DECLARATION_VERSION_ID = reader.ReadString("DECLARATION_VERSION_ID"),
                INN = reader.ReadString("INN"),
                KPP = reader.ReadString("KPP"),
                NAME = reader.ReadString("NAME"),
                INVOICE_NUM = reader.ReadString("INVOICE_NUM"),
                INVOICE_DATE = reader.ReadDateTime("INVOICE_DATE"),
                DISCREPANCY_STATUS = reader.ReadInt("DISCREPANCY_STATUS"),
                COMPARE_ROW_KEY = reader.ReadString("COMPARE_ROW_KEY"),
                CHAPTER = reader.ReadInt("CHAPTER")
            };
        }
    }
}