﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Helpers
{
    public class ReportHelper
    {
        public OracleParameter CreateArrayParameter(QueryConditions criteria, string columnName, string parameterName, string defaultValue = null)
        {
            return CreateArrayParameter(criteria.Filter, columnName, parameterName, defaultValue);
        }

        public OracleParameter CreateArrayParameter(DictionaryConditions criteria, string columnName, string parameterName)
        {
            return CreateArrayParameter(criteria.Filter, columnName, parameterName);
        }

        private OracleParameter CreateArrayParameter(List<FilterQuery> filter, string columnName, string parameterName, string defaultValue = null)
        {
            List<string> valuesParam = new List<string>();
            FilterQuery filterQuery = filter.Where(p => p.ColumnName == columnName).FirstOrDefault();
            if (filterQuery != null)
            {
                foreach (ColumnFilter item in filterQuery.Filtering)
                {
                    valuesParam.Add(item.Value.ToString());
                }
            }
            if (valuesParam.Count == 0 && defaultValue != null)
            {
                valuesParam.Add(defaultValue);
            }

            OracleParameter paramArraible = new OracleParameter();
            paramArraible.ParameterName = parameterName;
            paramArraible.OracleDbType = OracleDbType.Varchar2;
            paramArraible.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
            paramArraible.Direction = ParameterDirection.Input;
            paramArraible.Value = valuesParam.ToArray();
            paramArraible.Size = valuesParam.Count();
            List<int> sizes = new List<int>();
            foreach (string item in valuesParam)
            {
                sizes.Add(item.Length);
            }
            paramArraible.ArrayBindSize = sizes.ToArray();

            return paramArraible;
        }

        public List<string> GetParamListCriteria(QueryConditions criteria, string columnName)
        {
            List<string> valuesParam = new List<string>();
            FilterQuery filterQuery = criteria.Filter.Where(p => p.ColumnName == "NalogOrganCode").FirstOrDefault();
            if (filterQuery != null)
            {
                foreach (ColumnFilter item in filterQuery.Filtering)
                {
                    valuesParam.Add(item.Value.ToString());
                }
            }
            return valuesParam;
        }

        public object GetFilterFromCondition(QueryConditions criteria, string columnName)
        {
            object ret = null;
            FilterQuery filterQuery = criteria.Filter.Where(p => p.ColumnName == columnName).FirstOrDefault();
            if (filterQuery != null)
            {
                ColumnFilter columnFilter = filterQuery.Filtering.FirstOrDefault();
                if (columnFilter != null)
                {
                    ret = columnFilter.Value;
                }
            }
            return ret;
        }

        public int StringToInt(string value)
        {
            int ret = 0;
            int temp = 0;
            if (int.TryParse(value, out temp))
            {
                ret = temp;
            }
            return ret;
        }

        public long StringToInt64(string value)
        {
            long ret = 0;
            long temp = 0;
            if (long.TryParse(value, out temp))
            {
                ret = temp;
            }
            return ret;
        }

        public List<OracleParameter> GetParametersByFilter(QueryConditions criteria, string columnName)
        {
            QueryConditions criteriaClone = (QueryConditions)criteria.Clone();
            criteriaClone.Filter = criteriaClone.Filter.Where(p => p.ColumnName == columnName).ToList();
            var query = criteriaClone.ToSQL("{0}", "t", false, true);
            return query.Parameters.Select(p => p.Convert()).ToList();
        }

        public QueryConditions GetEmptyFilterCriteria(QueryConditions criteria)
        {
            QueryConditions criteriaWitoutFilters = (QueryConditions)criteria.Clone();
            criteriaWitoutFilters.Filter = new List<FilterQuery>();
            return criteriaWitoutFilters;
        }
    }
}
