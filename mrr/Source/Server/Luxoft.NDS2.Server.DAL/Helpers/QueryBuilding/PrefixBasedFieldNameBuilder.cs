﻿using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding
{
    public class PrefixBasedFieldNameBuilder : IQualifiedFieldNameBuilder
    {
        private readonly string _prefix;

        public string FieldName { get; set; }

        public bool IsComplex { get; set; }

        public PrefixBasedFieldNameBuilder() { }

        public PrefixBasedFieldNameBuilder(string prefix)
        {
            _prefix = prefix;
            IsComplex = false;
        }

        public string Build()
        {
            return IsComplex
                ? string.Format(FieldName, _prefix)
                : _prefix == null
                  ? FieldName
                  : string.Format("{0}.{1}", _prefix, FieldName);
        }
    }
}