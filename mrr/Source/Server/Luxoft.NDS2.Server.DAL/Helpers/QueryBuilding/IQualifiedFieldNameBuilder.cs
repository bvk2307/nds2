﻿namespace Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding
{
    internal interface IQualifiedFieldNameBuilder
    {
        string Build();

        string FieldName { get; set; }

        bool IsComplex { get; set; }
    }
}
