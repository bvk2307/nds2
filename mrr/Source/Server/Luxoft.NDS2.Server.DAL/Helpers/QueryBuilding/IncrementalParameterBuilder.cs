﻿using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding
{
    internal class IncrementalParameterBuilder : IParameterBuilder<QueryParameter>
    {
        private readonly string _pattern;
        private int _counter = 0;

        public List<QueryParameter> Parameters
        {
            get;
            private set;
        }

        public IncrementalParameterBuilder(string pattern)
        {
            _pattern = pattern;
            Parameters = new List<QueryParameter>();
        }

        public string Add(object value)
        {
            _counter++;
            var newParam = new QueryParameter()
            {
                Name = string.Format(_pattern, _counter),
                Value = value
            };

            Parameters.Add(newParam);
            return ":"+newParam.Name;
        }
    }
}
