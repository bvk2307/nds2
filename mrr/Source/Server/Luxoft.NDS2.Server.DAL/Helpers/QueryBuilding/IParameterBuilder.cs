﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Helpers.QueryBuilding
{
    public interface IParameterBuilder
    {
        /// <summary>
        /// Добавляет параметр в коллекцию
        /// </summary>
        /// <param name="value">Значение параметра</param>
        /// <returns>Имя нового параметра</returns>
        string Add(object value);

        List<QueryParameter> Parameters { get; }
    }
}
