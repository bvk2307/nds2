﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface ISonoAdapter
    {
        List<string> GetSonoCodes();
    }
}
