﻿using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.DiscrepancyDocument.DataMappers
{
    internal class DiscrepancyDocumentInfoDataMapper : IDataMapper<DiscrepancyDocumentInfo>
    {
        public DiscrepancyDocumentInfo MapData(IDataRecord reader)
        {
            var claimInfo = new DiscrepancyDocumentInfo();

            claimInfo.Id = reader.ReadInt64("doc_id");
            claimInfo.DeclarationId = reader.ReadInt64("ref_entity_id");
            claimInfo.DeclarationVersionId = reader.ReadInt64("DECLARATION_VERSION_ID");
            claimInfo.CreateDate = reader.ReadDateTime("create_date");
            claimInfo.SendDate = reader.ReadDateTime("tax_payer_send_date");
            claimInfo.DeliveryDate = reader.ReadDateTime("tax_payer_delivery_date");
            claimInfo.ReplyDate = reader.ReadDateTime("tax_payer_reply_date");
            claimInfo.StatusChangeDate = reader.ReadDateTime("status_date");
            claimInfo.Status = new DictionaryEntry()
                                   {EntryId = reader.ReadInt("status"), EntryValue = reader.ReadString("statusDesc")};
            claimInfo.EodId = reader.ReadString("external_doc_num");
            claimInfo.EodDate = reader.ReadDateTime("external_doc_date");
            claimInfo.DiscrepancyAmount = reader.ReadDecimal("discrepancy_amount");
            claimInfo.DiscrepancyCount = reader.ReadInt("discrepancy_count");
            claimInfo.TaxPayerInn = reader.ReadString("TaxPayerInn");
            claimInfo.TaxPayerName = reader.ReadString("TaxPayerName");
            claimInfo.Soun = new CodeValueDictionaryEntry(reader.ReadString("sono_code"), "");
            claimInfo.SeodSentDate = reader.ReadDateTime("external_doc_date");
            claimInfo.SeodAccepted = reader.ReadInt("seod_accepted");
            claimInfo.SeodAcceptDate = reader.ReadDateTime("seod_accept_date");
            claimInfo.CloseDate = reader.ReadDateTime("close_date");
            claimInfo.CloseReason = reader.ReadInt("close_reason");
            claimInfo.DocType = new DictionaryEntry()
                                    {
                                        EntryId = reader.ReadInt("doc_type"),
                                        EntryValue = reader.ReadString("doc_type_name")
                                    };
            claimInfo.UserComment = reader.ReadString("user_comment");
            claimInfo.ProlongAnswerDate = reader.ReadDateTime("prolong_answer_date");
            claimInfo.KnpSyncStatus = reader.ReadInt("knp_sync_status");

            return claimInfo;
        }
    }
}
