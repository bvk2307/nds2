﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.DiscrepancyDocument.CommandBuilders
{
    class DiscrepancyDocumentInfoBuilder: ExecuteProcedureCommandBuilder
    {
        private const string ProcedureSearchFavoriteFilters = "PAC$DOCUMENT.GET_DOC_INFO";

        private const string ParamFavoriteFilterPAnalyst = "pDocId";
        private const string ParamFavoriteFilterCursor = "pRezCursor";

        private long id;


        public DiscrepancyDocumentInfoBuilder(long id)
            : base(ProcedureSearchFavoriteFilters)
        {
            this.id = id;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(ParamFavoriteFilterPAnalyst, id));
            parameters.Add(FormatCommandHelper.Cursor(ParamFavoriteFilterCursor));

            return result;
        }
    }
}
