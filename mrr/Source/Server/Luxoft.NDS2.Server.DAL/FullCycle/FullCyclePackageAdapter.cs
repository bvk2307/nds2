﻿using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.FullCycle.CommandBuilders;
using Luxoft.NDS2.Server.DAL.FullCycle.DataMappers;

namespace Luxoft.NDS2.Server.DAL.FullCycle
{
    public class FullCyclePackageAdapter : IFullCyclePackageAdapter
    {
        private readonly IServiceProvider _service;

        private readonly ConnectionFactoryBase _connection;

        public FullCyclePackageAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public long GetComparisonDataVersion()
        {
            var values = new ResultCommandExecuter(
                _service,
                _connection).TryExecute(
                    new FullCycleGetSequenceCurrentValCommandBuilder());

            return FullCycleDataMapperHelper.ReadLong(values[FullCycleGetSequenceCurrentValCommandBuilder.ParamCount]);
        }
    }
}
