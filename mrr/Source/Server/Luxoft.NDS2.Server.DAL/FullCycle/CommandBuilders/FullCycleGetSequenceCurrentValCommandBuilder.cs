﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.FullCycle.CommandBuilders
{
    internal class FullCycleGetSequenceCurrentValCommandBuilder : ExecuteProcedureCommandBuilder
    {
        public const string ParamCount = "pCount";
        private const string PackageName = "PAC$NDS2_FULL_CYCLE";
        private const string ProcedureName = "P$GET_COMPARISON_DATA_VERSION";

        public FullCycleGetSequenceCurrentValCommandBuilder()
            : base(string.Concat(PackageName, ".", ProcedureName))
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericOut(ParamCount));

            return result;
        }
    }
}
