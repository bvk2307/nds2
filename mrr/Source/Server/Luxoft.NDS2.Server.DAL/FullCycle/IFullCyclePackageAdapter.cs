namespace Luxoft.NDS2.Server.DAL.FullCycle
{
    public interface IFullCyclePackageAdapter
    {
        long GetComparisonDataVersion();
    }
}