﻿
namespace Luxoft.NDS2.Server.DAL.FullCycle.DataMappers
{
    internal static class FullCycleDataMapperHelper
    {
        public static long ReadLong(object value)
        {
            long retVal;

            if (!long.TryParse(value.ToString(), out retVal))
            {
                return 0;
            }

            return retVal;
        }
    }
}
