﻿using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.BankAccounts
{
    internal class BankAccountsAdapter : IBankAccountsAdapter
    {
        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connectionFactory;

        private const string XmlSchemaName = "TAX3EXCH_NDS2_CAM_05_01.xsd";

        class LongMapper : IDataMapper<long>
        {
            public long MapData(IDataRecord reader)
            {
                return reader.ReadInt64("VALUE");
            }
        }

        private class StringValue
        {
            public string Value { get; set; }

        }

        private class StringMapper : IDataMapper<StringValue>
        {
            public StringValue MapData(IDataRecord reader)
            {
                return new StringValue { Value = reader.ReadString("VALUE") };
            }
        }

        public BankAccountsAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        public void LogToDb(MessageType type, string link, string message)
        {
            new ResultCommandExecuter(_service, _connectionFactory)
                .TryExecute(new LogToDbCommand((int)type, link, message));
        }

        private class GetSonoCommandBuilder : IOracleCommandBuilder
        {
            private const string CursorParameter = "pCursor";

            private const string CommandPattern = "begin open :{0} for select sono_code as value from SONO_BANK_LIMIT; end;";

            public OracleCommand BuildCommand(OracleConnection connection)
            {
                var command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.BindByName = true;
                command.CommandText =
                    string.Format(CommandPattern, CursorParameter);

                command.Parameters.Add(FormatCommandHelper.Cursor(CursorParameter));

                return command;
            }
        }

        private bool AllowedForSono(IEnumerable<string> allowedInspections)
        {
            var list = new ListCommandExecuter<StringValue>(new StringMapper(), _service, _connectionFactory)
                .TryExecute(new GetSonoCommandBuilder());
            return list.Any(sono => allowedInspections.Any(s => sono.Value == s));
        }

        public bool PushRequest(BankAccountManualRequest request, List<string> allowedInspections)
        {
            if (!AllowedForSono(allowedInspections))
            {
                throw new Exception("Данный функционал доступен только пользователям, участвующим в опытной эксплуатации");
            }

            foreach (var group in request.Accounts.GroupBy(a => new {a.Bik, a.InnBank, a.KppBank, a.FilialNumber, a.BankNumber}))
            {
                var bank = group.First();

                var docId = GetDocId();

                var xml = XmlProducer.Generate(docId, request.ToBankAccountRequest());
                
                var schema = GetXmlSchema(XmlSchemaName);
                var xsd = XmlProducer.GetSchema(schema);
                var isValid = true;
                XmlProducer.Validate(xsd, xml, (o, args) =>
                {
                    isValid = false;
                    var validationArg = args as ValidationEventArgs;
                    if (validationArg != null)
                    {
                        LogToDb(MessageType.Error, new System.Diagnostics.StackFrame(0).GetMethod().Name, validationArg.Message);
                        return;
                    }
                    var errorArg = args as ErrorEventArgs;
                    if (errorArg != null)
                    {
                        LogToDb(MessageType.Error, new System.Diagnostics.StackFrame(0).GetMethod().Name, errorArg.GetException().Message);
                    }
                });

                var result = new ResultCommandExecuter(_service)
                    .TryExecute(new PushCommand(docId, request.Inn, request.KppEffective, request.IsReorginized, request.Period, request.Year,
                        request.SonoCode, request.UserName, request.UserSid,
                        bank.Bik, bank.NameBank, bank.InnBank, bank.KppBank,
                        bank.BankNumber, bank.FilialNumber,
                       // string.Join(", ", group.Select(x => x.Account)),
                        group.Select(x => x.Account).ToArray(),
                        BankAccountManualRequest.GetAccountsXml(group.ToArray()),
                        isValid ? (int)BankAccountRequestStatus.XmlGenerated : (int)BankAccountRequestStatus.XmlValidationError,
                        xml));
                if (((OracleDecimal) result[ResultParameterName]).ToInt64() == -1)
                    return false;
            }
            return true;
        }

        public List<long> FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear)
        {
            return new ListCommandExecuter<long>(
                new LongMapper(),
                _service,
                _connectionFactory)
                .TryExecute(new FindSentRequestCommand(inn, kppEffective, periodCode, fiscalYear)).ToList();
        }

        public long GetPullLimit()
        {
            var value = new ListCommandExecuter<long>(
                new LongMapper(),
                _service, _connectionFactory).TryExecute(new GetPullLimitCommand()).FirstOrDefault();
            return value;
        }

        public List<DeclarationPeriod> GetAvailablePeriods(string inn, string kpp)
        {
            return new ListCommandExecuter<DeclarationPeriod>(
                new GenericDataMapper<DeclarationPeriod>(),
                _service, _connectionFactory)
                .TryExecute(new GetAvailablePeriodsCommand(inn, kpp)).ToList();
        }

        public BankAccountRequest PullRequest()
        {
            return new ListCommandExecuter<BankAccountRequest>(
                new GenericDataMapper<BankAccountRequest>()
                  .WithFieldMapperFor("RequestActor", new IntFieldMapper())
                  .WithFieldMapperFor("ACCOUNTS", new AccountsFieldMapper()),
                _service, _connectionFactory).TryExecute(new PullCommand()).FirstOrDefault();
        }

        public long GetDocId()
        {
            var value = new ListCommandExecuter<long>(
                new LongMapper(),
                _service, _connectionFactory).TryExecute(new GetDocIdCommand()).FirstOrDefault();
            return value;
        }

        public void DropRequest(long id)
        {
            new ResultCommandExecuter(_service, _connectionFactory)
                .TryExecute(new DropRequestCommand(id));
        }

        public List<BankAccountBrief> GetAccounts(string inn, string kppEffective,
            string bik, string innBank, string kppBank,
            string bankNumber, string filialNumber,
            DateTime begin, DateTime end)
        {
            return new ListCommandExecuter<BankAccountBrief>(
                new GenericDataMapper<BankAccountBrief>(),
                _service, _connectionFactory).TryExecute(new SearchAccountsCommand(inn, kppEffective, bik, innBank, kppBank, bankNumber, filialNumber, begin, end)).ToList();
        }

        public List<string> GetRequestedAccounts(string bik, DateTime begin, DateTime end)
        {
            return new ListCommandExecuter<StringValue>(
                new StringMapper(),
                _service, _connectionFactory).TryExecute(new SearchRequestedAccountsCommand(bik, begin.Date, end.Date)).Select(x => x.Value).ToList();
        }

        private const string ResultParameterName = "pResult";

        public bool UpdateXml(long id, long docId, string[] accounts, string xml)
        {
            var result = new ResultCommandExecuter(_service)
                .TryExecute(new UpdateXmlCommand(id, docId, accounts, xml));
            return ((OracleDecimal)result[ResultParameterName]).ToInt64() == 1;
        }

        public void SetStatus(long id, BankAccountRequestStatus status)
        {
            new ResultCommandExecuter(_service, _connectionFactory)
                .TryExecute(new SetStatusCommand(id, (int)status)).FirstOrDefault();
        }

        public string GetXmlSchema(string url)
        {
            var result = new ListCommandExecuter<StringValue>(
                new StringMapper(),
                _service, _connectionFactory).TryExecute(new GetXmlSchemaCommand(url)).FirstOrDefault();
            return result != null ? result.Value : string.Empty;
        }
    }

    class AccountsFieldMapper : FieldMapperBase<BankAccountBrief[]>
    {
        protected override BankAccountBrief[] Null()
        {
            return new BankAccountBrief[] { };
        }

        protected override BankAccountBrief[] Data(object value)
        {
            var xmlSerializer =
                new XmlSerializer(typeof(BankAccountBrief[]));

            return xmlSerializer.Deserialize(new StringReader(value.ToString())) as BankAccountBrief[];
        }
    }
}
