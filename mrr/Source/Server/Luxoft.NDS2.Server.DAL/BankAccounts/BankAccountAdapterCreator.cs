﻿using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.BankAccounts
{
    public static class BankAccountAdapterCreator
    {
        public static IBankAccountsAdapter GetBankAccountsAdapter(this IServiceProvider service, ConnectionFactoryBase connectionFactory)
        {
            return new BankAccountsAdapter(service, connectionFactory);
        }

        public static ISearchAdapter<BankAccountRequestSummary> GetBankAccountRequestAdapter(this IServiceProvider service)
        {
            return new BankAccountRequestAdapter(service);
        }
    }
}
