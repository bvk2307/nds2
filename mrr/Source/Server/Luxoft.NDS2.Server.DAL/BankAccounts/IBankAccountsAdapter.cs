﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Server.DAL.BankAccounts
{
    public enum MessageType
    {
        Info = 0,
        Warning,
        Error
    }
    
    public interface IBankAccountsAdapter
    {
        void LogToDb(MessageType type, string link, string message);

        bool PushRequest(BankAccountManualRequest request, List<string> allowedInspections);

        List<long> FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear);

        long GetPullLimit();

        BankAccountRequest PullRequest();

        List<DeclarationPeriod> GetAvailablePeriods(string inn, string kpp);

        long GetDocId();

        void DropRequest(long id);

        List<BankAccountBrief> GetAccounts(string inn, string kppEffective,
            string bik, string innBank, string kppBank,
            string bankNumber, string filialNumber,
            DateTime begin, DateTime end);

        List<string> GetRequestedAccounts(string bik, DateTime begin, DateTime end);

        bool UpdateXml(long id, long docId, string[] accounts, string xml);

        void SetStatus(long id, BankAccountRequestStatus status);

        string GetXmlSchema(string url);
    }
}