﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetFLAccountsCommand : IOracleCommandBuilder
    {
        private readonly string _inn;
        private readonly DateTime _begin;
        private readonly DateTime _end;
        private readonly QueryConditions _criteria;

        private const string CommandPattern = @"begin
  open :{0} for select vw.* from (
    select
      bs.INN,
      '' as KPP,
      bs.BIK,
      bs.NAMEKO as NameBank,
      bs.INNKO as INNBANK,
      bs.KPPKO as KPPBANK,
      bs.RNKO as BANKNUMBER,
      bs.NFKO as FILIALNUMBER,
      bs.NOMSCH as ACCOUNT,
      case bs.PRVAL when '0' then 'рубли' else 'валюта' end as CURRENCY,
      bs.VIDSCH as ACCOUNTTYPE,
      bs.DATECLOSESCH as CLOSEDATE,
      bs.DATEOPENSCH as OPENDATE
    from BSSCHET_IP bs
    left join REQUESTED_ACCOUNTS ra on ra.bik = bs.bik and ra.account = bs.nomsch and ra.periodbegin <= :{1} and ra.periodend >= :{2}
    where bs.inn = :{3}
      and (bs.dateclosesch >= :{1} or bs.dateclosesch is null)
      and bs.dateopensch <= :{2}
      and ra.bik is null) vw
    where 0 = 0 {4}
    {5};
end;";

        private const string Inn = "pInn";

        private const string Begin = "pBegin";

        private const string End = "pEnd";

        private const string Cursor = "pCursor";

        public GetFLAccountsCommand(string inn, DateTime begin, DateTime end, QueryConditions criteria)
        {
            _inn = inn;
            _begin = begin;
            _end = end;
            _criteria = criteria;
        }

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.BindByName = true;
            command.CommandText = _criteria.ToSQL(string.Format(CommandPattern, Cursor, Begin, End, Inn, "{0}", "{1}"), "vw", false, true).Text;

            command.Parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            command.Parameters.Add(FormatCommandHelper.DateIn(Begin, _begin));
            command.Parameters.Add(FormatCommandHelper.DateIn(End, _end));
            command.Parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return command;

        }
    }

    class GetULAccountsCommand : IOracleCommandBuilder
    {
        private readonly string _inn;
        private readonly string _kpp;
        private readonly DateTime _begin;
        private readonly DateTime _end;
        private readonly QueryConditions _criteria;

        private const string CommandPattern = @"begin
  open :{0} for select vw.* from (
    select
      bs.INN,
      bs.KPP,
      bs.BIK,
      bs.NAMEKO as NameBank,
      bs.INNKO as INNBANK,
      bs.KPPKO as KPPBANK,
      bs.RNKO as BANKNUMBER,
      bs.NFKO as FILIALNUMBER,
      bs.NOMSCH as ACCOUNT,
      case bs.PRVAL when '0' then 'рубли' else 'валюта' end as CURRENCY,
      bs.VIDSCH as ACCOUNTTYPE,
      bs.DATECLOSESCH as CLOSEDATE,
      bs.DATEOPENSCH as OPENDATE
    from BSSCHET_UL bs
    left join REQUESTED_ACCOUNTS ra on ra.bik = bs.bik and ra.account = bs.nomsch and ra.periodbegin <= :{1} and ra.periodend >= :{2}
    where bs.inn = :{3} and bs.kpp = :{4}
      and (bs.dateclosesch >= :{1} or bs.dateclosesch is null)
      and bs.dateopensch <= :{2}
      and ra.bik is null) vw
    where 0 = 0 {5}
    {6};
end;";

        private const string Inn = "pInn";

        private const string Kpp = "pKpp";

        private const string Begin = "pBegin";

        private const string End = "pEnd";

        private const string Cursor = "pCursor";

        public GetULAccountsCommand(string inn, string kpp, DateTime begin, DateTime end, QueryConditions criteria)
        {
            _inn = inn;
            _kpp = kpp;
            _begin = begin;
            _end = end;
            _criteria = criteria;
        }

        public OracleCommand BuildCommand(OracleConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.BindByName = true;
            command.CommandText = _criteria.ToSQL(string.Format(CommandPattern, Cursor, Begin, End, Inn, Kpp, "{0}", "{1}"), "vw", false, true).Text;

            command.Parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            command.Parameters.Add(FormatCommandHelper.VarcharIn(Kpp, _kpp));
            command.Parameters.Add(FormatCommandHelper.DateIn(Begin, _begin));
            command.Parameters.Add(FormatCommandHelper.DateIn(End, _end));
            command.Parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return command;

        }
    }

}
