﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class LogToDbCommand : BankAccauntsPackageCommand
    {
        private readonly int _type;
        private readonly string _link;
        private readonly string _message;

        private const string Type = "pType";
        private const string Link = "pLink";
        private const string Message = "pMessage";

        public LogToDbCommand(int type, string link, string message)
            : base("P$LOG")
        {
            _type = type;
            _link = link;
            _message = message;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(Type, _type));
            parameters.Add(FormatCommandHelper.VarcharIn(Link, _link));
            parameters.Add(FormatCommandHelper.VarcharIn(Message, _message));

            return result;
        }
    }
}
