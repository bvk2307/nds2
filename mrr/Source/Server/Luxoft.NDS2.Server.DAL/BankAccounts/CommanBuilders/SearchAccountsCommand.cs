﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class SearchAccountsCommand : BankAccauntsPackageCommand
    {
        private readonly string _inn;
        private readonly string _kppEffective;
        private readonly string _bik;
        private readonly string _innBank;
        private readonly string _kppBank;
        private readonly string _bankNumber;
        private readonly string _filialNumber;
        private readonly DateTime _beginDate;
        private readonly DateTime _endDate;

        private const string Inn = "pInn";

        private const string Kpp = "pKpp";

        private const string Bik = "pBik";

        private const string InnBank = "pInnBank";

        private const string KppBank = "pKppBank";

        private const string BankNumber = "pBankNumber";

        private const string FilialNumber = "pFilialNumber";

        private const string BeginDate = "pBeginDate";

        private const string EndDate = "pEndDate";

        private const string Cursor = "pCursor";

        public SearchAccountsCommand(string inn, string kpp, string bik, string innBank, string kppBank, string bankNumber, string filialNumber, DateTime beginDate, DateTime endDate)
            : base("P$SEARCH_ACCOUNTS")
        {
            _inn = inn;
            _kppEffective = kpp;
            _bik = bik;
            _innBank = innBank;
            _kppBank = kppBank;
            _bankNumber = bankNumber;
            _filialNumber = filialNumber;
            _beginDate = beginDate;
            _endDate = endDate;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(Kpp, _kppEffective));
            parameters.Add(FormatCommandHelper.VarcharIn(Bik, _bik));
            parameters.Add(FormatCommandHelper.VarcharIn(InnBank, _innBank));
            parameters.Add(FormatCommandHelper.VarcharIn(KppBank, _kppBank));
            parameters.Add(FormatCommandHelper.VarcharIn(BankNumber, _bankNumber));
            parameters.Add(FormatCommandHelper.VarcharIn(FilialNumber, _filialNumber));
            parameters.Add(FormatCommandHelper.DateIn(BeginDate, _beginDate));
            parameters.Add(FormatCommandHelper.DateIn(EndDate, _endDate));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }

    class SearchRequestedAccountsCommand : BankAccauntsPackageCommand
    {
        private readonly string _bik;
        private readonly DateTime _beginDate;
        private readonly DateTime _endDate;

        private const string Bik = "pBik";

        private const string BeginDate = "pBegin";

        private const string EndDate = "pEnd";

        private const string Cursor = "pCursor";

        public SearchRequestedAccountsCommand(string bik, DateTime beginDate, DateTime endDate)
            : base("P$GET_REQUESTED_ACCOUNTS")
        {
            _bik = bik;
            _beginDate = beginDate;
            _endDate = endDate;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Bik, _bik));
            parameters.Add(FormatCommandHelper.DateIn(BeginDate, _beginDate));
            parameters.Add(FormatCommandHelper.DateIn(EndDate, _endDate));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }
}
