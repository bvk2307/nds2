﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetDocIdCommand : BankAccauntsPackageCommand
    {
        private const string Cursor = "pCursor";

        public GetDocIdCommand() : base("P$GET_DOC_ID")
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    
    }
}
