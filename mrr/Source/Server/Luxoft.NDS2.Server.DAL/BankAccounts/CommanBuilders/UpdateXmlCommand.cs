﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    sealed class UpdateXmlCommand : BankAccauntsPackageCommand
    {
        private readonly long _id;
        private readonly long _docId;
        private readonly string[] _accounts;
        private readonly string _text;

        private const string Id = "pId";

        private const string DocId = "pDocId";

        private const string Accounts = "pAccountsText";

        private const string Text = "pText";

        private const string Result = "pResult";

        public UpdateXmlCommand(long id, long docId, string[] accounts, string text) : base("P$UPDATE_XML")
        {
            _id = id;
            _docId = docId;
            _accounts = accounts;
            _text = text;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(Id, _id));
            parameters.Add(FormatCommandHelper.NumericIn(DocId, _docId));
            parameters.Add(_accounts.ToOracleArray(Accounts));
            parameters.Add(FormatCommandHelper.ClobIn(Text, _text));
            parameters.Add(FormatCommandHelper.NumericOut(Result));

            return result;
        }
    }

}
