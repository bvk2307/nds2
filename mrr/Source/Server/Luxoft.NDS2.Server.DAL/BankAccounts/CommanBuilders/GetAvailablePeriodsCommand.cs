﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    sealed class GetAvailablePeriodsCommand : BankAccauntsPackageCommand
    {
        private readonly string _inn;
        private readonly string _kpp;


        private const string Inn = "pInn";

        private const string Kpp = "pKpp";
        
        private const string Cursor = "pCursor";

        public GetAvailablePeriodsCommand(string inn, string kpp)
            : base("P$GET_AVAILABLE_PERIODS")
        {
            _inn = inn;
            _kpp = kpp;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(Kpp, _kpp));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }
}
