﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class BankAccauntsPackageCommand : ExecuteProcedureCommandBuilder
    {
        protected const string PackageName = "PAC$BANK_ACCOUNTS";
        
        public BankAccauntsPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", PackageName, procedureName))
        {
        }
    }
}
