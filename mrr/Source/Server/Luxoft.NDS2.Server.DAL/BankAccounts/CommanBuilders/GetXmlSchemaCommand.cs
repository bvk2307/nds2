﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetXmlSchemaCommand : BankAccauntsPackageCommand
    {
        private readonly string _url;

        private const string Name = "pName";
        private const string Cursor = "pCursor";

        public GetXmlSchemaCommand(string url) : base("P$GET_SCHEMA")
        {
            _url = url;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Name, _url));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }
}
