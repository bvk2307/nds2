﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetAccountRequestsCommand : PageSearchQueryCommandBuilder
    {
        private const string Query = "select vw.* from V$BANK_ACCOUNT_REQUEST vw WHERE {0} {1}";

        public GetAccountRequestsCommand(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy
            )
            : base(
                Query,
                filterBy,
                orderBy,
                new PatternProvider("vw"))
        {
        }
    }

    class CountAccountRequestsCommand : GetCountCommandBuilder
    {
        private const string Query = "V$BANK_ACCOUNT_REQUEST vw WHERE {0}";

        public CountAccountRequestsCommand(FilterExpressionBase searchBy)
            : base(Query, searchBy, new PatternProvider("vw"))
        {
        }
    }
}
