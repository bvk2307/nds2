﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class FindSentRequestCommand : BankAccauntsPackageCommand
    {
        private readonly string _inn;
        private readonly string _kppEffective;
        private readonly string _period;
        private readonly string _year;

        private const string Inn = "pInn";

        private const string KppEffective = "pKppEffective";

        private const string TypeCode = "pTypeCode";

        private const string PeriodCode = "pPeriodCode";

        private const string FiscalYear = "pFiscalYear"; private const string Cursor = "pCursor";

        public FindSentRequestCommand(string inn, string kppEffective, string period, string year)
            : base("P$FIND")
        {
            _inn = inn;
            _kppEffective = kppEffective;
            _period = period;
            _year = year;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(KppEffective, _kppEffective));
            parameters.Add(FormatCommandHelper.NumericIn(TypeCode, 0));
            parameters.Add(FormatCommandHelper.VarcharIn(PeriodCode, _period));
            parameters.Add(FormatCommandHelper.VarcharIn(FiscalYear, _year)); 
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }

    }
}
