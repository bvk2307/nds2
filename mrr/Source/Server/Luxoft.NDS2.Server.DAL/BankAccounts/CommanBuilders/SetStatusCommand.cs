﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class SetStatusCommand : BankAccauntsPackageCommand
    {
        private readonly long _id;
        private readonly int _status;
        private const string Id = "pId";
        private const string Status = "pStatus";

        public SetStatusCommand(long id, int status)
            : base("P$SET_STATUS")
        {
            _id = id;
            _status = status;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(Id, _id));
            parameters.Add(FormatCommandHelper.NumericIn(Status, _status));

            return result;
        }
    }
}
