﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetRequestAccountsCommand : BankAccauntsPackageCommand
    {
        private const string Id = "pId";
        private const string Cursor = "pCursor";
        
        private readonly long _id;

        public GetRequestAccountsCommand(long id)
            : base("P$GET_ACCOUNTS")
        {
            _id = id;
        }
        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(Id, _id));
            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }
}
