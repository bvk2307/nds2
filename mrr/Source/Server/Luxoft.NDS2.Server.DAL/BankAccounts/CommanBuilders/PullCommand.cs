﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    sealed class PullCommand : BankAccauntsPackageCommand
    {
        private const string Cursor = "pCursor";
        
        public PullCommand() : base("P$PULL")
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    }
}
