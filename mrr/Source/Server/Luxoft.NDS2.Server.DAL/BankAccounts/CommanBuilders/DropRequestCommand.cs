﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{

    sealed class DropRequestCommand : BankAccauntsPackageCommand
    {
        private readonly long _id;

        private const string Id = "pId";

        public DropRequestCommand(long id)
            : base("P$DROP_REQUEST")
        {
            _id = id;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(Id, _id));

            return result;
        }
    }
}
