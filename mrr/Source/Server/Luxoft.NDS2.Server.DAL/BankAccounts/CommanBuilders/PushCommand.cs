﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class PushCommand : BankAccauntsPackageCommand
    {
        private readonly long _docId;
        private readonly string _inn;
        private readonly string _kppEffective;
        private readonly bool _isReorganized;
        private readonly string _period;
        private readonly string _year;
        private readonly string _sonoCode;
        private readonly string _userName;
        private readonly string _userSid;
        private readonly string _bik;
        private readonly string _bankName;
        private readonly string _bankInn;
        private readonly string _bankKpp;
        private readonly string _bankNumber;
        private readonly string _bankFilial;
        private readonly string[] _accounts;
        private readonly string _accountsLob;
        private readonly int _status;
        private readonly string _docBody;

        private const string DocId = "pDocId";

        private const string Inn = "pInn";

        private const string KppEffective = "pKppEffective";

        private const string Reorganized = "pReorganized";

        private const string TypeCode = "pTypeCode";

        private const string PeriodCode = "pPeriodCode";

        private const string FiscalYear = "pFiscalYear";

        private const string SonoCode = "pSonoCode";

        private const string UserSid = "pUserSid";

        private const string UserName = "pUserName";

        private const string Bik = "pBik";

        private const string BankInn = "pBankInn";

        private const string BankKpp = "pBankKpp";

        private const string BankName = "pBankName";

        private const string BankNumber = "pBankNumber";

        private const string BankFilial = "pBankFilial";

        private const string AccountsText = "pAccountsText";

        private const string Accounts = "pAccounts";

        private const string Status = "pStatus";

        private const string DocBody = "pXml";

        private const string Result = "pResult";

        public PushCommand(long docId, string inn, string kppEffective, bool isReorganized, string period, string year, string sonoCode,
            string userName, string userSid,
            string bik, string bankName,
            string bankInn, string bankKpp,
            string bankNumber, string bankFilial,
            string[] accounts, string accountsLob,
            int status,
            string docBody)
            : base("P$PUSH")
        {
            _docId = docId;
            _inn = inn;
            _kppEffective = kppEffective;
            _isReorganized = isReorganized;
            _period = period;
            _year = year;
            _sonoCode = sonoCode;
            _userName = userName;
            _userSid = userSid;
            _bik = bik;
            _bankName = bankName;
            _bankInn = bankInn;
            _bankKpp = bankKpp;
            _bankNumber = bankNumber;
            _bankFilial = bankFilial;
            _accounts = accounts;
            _accountsLob = accountsLob;
            _status = status;
            _docBody = docBody;
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.NumericIn(DocId, _docId));
            parameters.Add(FormatCommandHelper.VarcharIn(Inn, _inn));
            parameters.Add(FormatCommandHelper.VarcharIn(KppEffective, _kppEffective));
            parameters.Add(FormatCommandHelper.NumericIn(Reorganized, _isReorganized ? 1 : 0));
            parameters.Add(FormatCommandHelper.NumericIn(TypeCode, 0));
            parameters.Add(FormatCommandHelper.VarcharIn(PeriodCode, _period));
            parameters.Add(FormatCommandHelper.VarcharIn(FiscalYear, _year));
            parameters.Add(FormatCommandHelper.VarcharIn(SonoCode, _sonoCode));
            parameters.Add(FormatCommandHelper.VarcharIn(UserSid, _userSid));
            parameters.Add(FormatCommandHelper.VarcharIn(UserName, _userName));
            parameters.Add(FormatCommandHelper.VarcharIn(Bik, _bik));
            parameters.Add(FormatCommandHelper.VarcharIn(BankInn, _bankInn));
            parameters.Add(FormatCommandHelper.VarcharIn(BankKpp, _bankKpp));
            parameters.Add(FormatCommandHelper.VarcharIn(BankName, _bankName));
            parameters.Add(FormatCommandHelper.VarcharIn(BankNumber, _bankNumber));
            parameters.Add(FormatCommandHelper.VarcharIn(BankFilial, _bankFilial));
            parameters.Add(_accounts.ToOracleArray(AccountsText));
            parameters.Add(FormatCommandHelper.ClobIn(Accounts, _accountsLob));
            parameters.Add(FormatCommandHelper.NumericIn(Status, _status));
            parameters.Add(FormatCommandHelper.ClobIn(DocBody, _docBody));
            parameters.Add(FormatCommandHelper.NumericOut(Result));

            return result;
        }
        
    }

}
