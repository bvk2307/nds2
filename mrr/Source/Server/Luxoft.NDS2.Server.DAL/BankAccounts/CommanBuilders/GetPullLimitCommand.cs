﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.BankAccounts.CommanBuilders
{
    class GetPullLimitCommand : BankAccauntsPackageCommand
    {
        private const string Cursor = "pCursor";

        public GetPullLimitCommand() : base("P$GET_PULL_LIMIT")
        {
        }

        protected override string BuildSelectStatement(OracleParameterCollection parameters)
        {
            var result = base.BuildSelectStatement(parameters);

            parameters.Add(FormatCommandHelper.Cursor(Cursor));

            return result;
        }
    
    }
}
