﻿using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.BankAccounts
{
    internal class BankAccountRequestAdapter : SearchDataAdapterBase<BankAccountRequestSummary>
    {
        public BankAccountRequestAdapter(IServiceProvider service) : base(service)
        {
        }
        protected override IQueryPatternProvider PatternProvider(string viewAlias)
        {
            return new PatternProvider(viewAlias);
        }

        protected override IDataMapper<BankAccountRequestSummary> DataMapper()
        {
            return new GenericDataMapper<BankAccountRequestSummary>();
        }

        protected override string ViewName()
        {
            return DbConstants.ViewBankAccountRequest;
        }
    }
}
