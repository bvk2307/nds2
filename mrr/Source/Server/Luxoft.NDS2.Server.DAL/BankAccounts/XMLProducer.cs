﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;

namespace Luxoft.NDS2.Server.DAL.BankAccounts
{
    #region Константы для xml

    static class ElementStrings
    {
        public const string Request = "ЗАПНОВЫПИС";

        public const string IdDoc = "ИдДок";

        public const string SonoCode = "КодНО";

        public const string DateBegin = "ДатаНач";

        public const string DateEnd = "ДатаКон";

        public const string BankInn = "ИННКО";

        public const string BankKpp = "КППКО";

        public const string Bik = "БИК";

        public const string BankNumber = "НомБ";

        public const string FilialNumber = "НомФ";

        public const string InnUl = "ИННЮЛ";

        public const string InnFl = "ИННФЛ";

        public const string Kpp = "КПП";

        public const string RequestKind = "ВидЗапр";

        public const string RequestType = "ТипЗапр";

        public const string AccountNumber = "НомСч";
    }

    #endregion

    public sealed class StringWriterWithEncoding : StringWriter
    {
        private readonly Encoding _encoding;

        public StringWriterWithEncoding(Encoding encoding)
        {
            _encoding = encoding;
        }

        public override Encoding Encoding
        {
            get { return _encoding; }
        }
    }

    public class XmlProducer
    {
        public static string Generate(long docId, BankAccountRequest request)
        {
            var encoding = Encoding.GetEncoding("windows-1251");
            var sb = new StringWriterWithEncoding(encoding);

            var xml = XmlWriter.Create(sb, new XmlWriterSettings { Encoding = encoding });

            xml.WriteStartElement(ElementStrings.Request);

            xml.WriteElementString(ElementStrings.IdDoc, docId.ToString(CultureInfo.InvariantCulture));
            xml.WriteElementString(ElementStrings.SonoCode, request.SonoCode);
            xml.WriteElementString(ElementStrings.DateBegin, request.PeriodBegin.ToString("dd.MM.yyyy"));
            xml.WriteElementString(ElementStrings.DateEnd, request.PeriodEnd.ToString("dd.MM.yyyy"));
            xml.WriteElementString(ElementStrings.BankInn, request.InnBank);
            xml.WriteElementString(ElementStrings.BankKpp, request.KppBank);
            xml.WriteElementString(ElementStrings.Bik, request.Bik);
            xml.WriteElementString(ElementStrings.BankNumber, request.BankNumber);
            xml.WriteElementString(ElementStrings.FilialNumber, request.FilialNumber);
            if (request.Inn.Length == 10)
            {
                xml.WriteElementString(ElementStrings.InnUl, request.Inn);
                xml.WriteElementString(ElementStrings.Kpp, request.Kpp);
            }
            else
            {
                xml.WriteElementString(ElementStrings.InnFl, request.Inn);
            }
            xml.WriteElementString(ElementStrings.RequestKind, request.RequestType);
            xml.WriteElementString(ElementStrings.RequestType, request.RequestActor == RequestActor.Auto ? "1" : "2");

            foreach (var acc in request.Accounts)
            {
                xml.WriteElementString(ElementStrings.AccountNumber, acc.Account);
            }

            xml.WriteEndElement();
            xml.Close();

            return sb.ToString();
        }

        public static void Validate(
            XmlSchemaSet xsd, string xmlString,
            Action<object, EventArgs> validationHandler)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(new StringReader(xmlString));
                doc.Schemas.Add(xsd);
                doc.Validate(new ValidationEventHandler(validationHandler));
            }
            catch (Exception e)
            {
                validationHandler(null, new ErrorEventArgs(e));
            }
        }

        public static XmlSchemaSet GetSchema(string xsdString)
        {
            var xsd = new XmlSchemaSet();
            xsd.Add(null, new XmlTextReader(new StringReader(xsdString)));
            return xsd;
        }
    }
}
