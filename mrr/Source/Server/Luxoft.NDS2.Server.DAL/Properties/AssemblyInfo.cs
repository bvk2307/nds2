﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("Luxoft.NDS2.Server.DAL")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

// Параметр ComVisible со значением FALSE делает типы в сборке невидимыми 
// для COM-компонентов.  Если требуется обратиться к типу в этой сборке через 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("1d3b3f49-da2b-4d59-b0e7-13c0dd0a47f8")]
[assembly: InternalsVisibleTo("Luxoft.NDS2.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100adc229bee2ef27529ae2339115d83e6bc01f922d4eacacf740cacc083ce61c7d6871902d1113d43eda55044b1711fffca528d9da3a1c844622ede842f2ed3ba410145b690890b54acda2a4f582e42d5bf80571d227762a121ce6973379b5413dc19259364d94329d4c9e14b5b08cb38cd258ffdf25aeda7990539dae58754acf")]