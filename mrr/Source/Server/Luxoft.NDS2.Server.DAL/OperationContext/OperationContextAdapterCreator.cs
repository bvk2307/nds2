﻿using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.OperationContext
{
    public static class OperationContextAdapterCreator
    {
        public static IOperationContextAdapter Create(IServiceProvider serviceProvider,
            ConnectionFactoryBase connectionFactoryBase)
        {
            return new OperationContextAdapter(serviceProvider, connectionFactoryBase);
        }
    }
}
