﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.OperationContext;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.OperationContext
{
    public static class OperationAdapterCreator
    {
        public static IOperationAdapter OperationAdapter(this IServiceProvider service)
        {
            return new OperationAdapter(service);
        }
    }

    internal class OperationAdapter : IOperationAdapter
    {
        private readonly IServiceProvider _service;

        public OperationAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public List<OperationData> Select(FilterExpressionBase filterBy)
        {
            var commandBuilder =
                new SearchQueryCommandBuilder(
                        DbConstants.OperationView.SelectPattern(DbConstants.ViewAlias),
                        filterBy,
                        new ColumnSort[] { },
                        new PatternProvider(DbConstants.ViewAlias));
            var commandExecuter =
                new ListCommandExecuter<OperationData>(
                    new GenericDataMapper<OperationData>(),
                    _service);

            return commandExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}
