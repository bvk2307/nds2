﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.OperationContext;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.OperationContext
{
    public static class OperationContextAdapterCreator
    {
        public static IOperationContextAdapter OperationContextAdapter(this IServiceProvider service)
        {
            return new OperationContextAdapter(service);
        }
    }

    internal class OperationContextAdapter : IOperationContextAdapter
    {
        private readonly IServiceProvider _service;

        public OperationContextAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public List<OperationAccessData> Select(FilterExpressionBase filterBy)
        {
            var commandBuilder = 
                new SearchQueryCommandBuilder(
                        DbConstants.OperationRestrictionView.SelectPattern(DbConstants.ViewAlias),
                        filterBy,
                        new ColumnSort[] {},
                        new PatternProvider(DbConstants.ViewAlias));
            var commandExecuter =
                new ListCommandExecuter<OperationAccessData>(
                    new GenericDataMapper<OperationAccessData>()
                        .WithFieldMapperFor(
                            TypeHelper<OperationAccessData>.GetMemberName(x => x.LimitType),
                            new IntFieldMapper()),
                    _service);

            return commandExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}
