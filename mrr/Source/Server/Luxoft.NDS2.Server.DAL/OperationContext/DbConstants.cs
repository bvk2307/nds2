﻿namespace Luxoft.NDS2.Server.DAL.OperationContext
{
    public static class DbConstants
    {
        public const string OperationRestrictionView = "V$RM_ACCESS_CONTEXT";

        public const string OperationView = "V$RM_ACCESS_CONTEXT_LIGHT";

        public const string ViewAlias = "vw";
    }
}
