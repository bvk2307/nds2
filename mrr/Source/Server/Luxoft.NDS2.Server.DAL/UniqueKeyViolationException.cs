﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Перечесление уникальных ключей БД
    /// </summary>
    public enum UniqueKey
    {
        FavoriteFilter
    }
    /// <summary>
    /// Этот класс представляет исключение при нарушении уникального ключа при попытке выполнить запрос к БД
    /// </summary>
    public class UniqueKeyViolationException : DatabaseException
    {
        /// <summary>
        /// Создает экземпляр класса UniqueViolationException 
        /// </summary>
        /// <param name="key">Уникальный ключ</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="innerException">Внутреннее исключение</param>
        public UniqueKeyViolationException(
            UniqueKey key,
            string message, 
            Exception innerException)
            : base(message, innerException)
        {
            Key = key;
        }

        /// <summary>
        /// Возвращает тип уникального ключа
        /// </summary>
        public UniqueKey Key
        {
            get;
            private set;
        }
    }
}
