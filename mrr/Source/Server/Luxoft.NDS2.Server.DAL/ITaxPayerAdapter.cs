﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы работы с данными налогоплательщика
    /// </summary>
    public interface ITaxPayerAdapter
    {
        /// <summary>
        /// Загружает данные налогоплательщика из БД
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="kpp">КПП эффективный</param>
        /// <returns>Данные налогоплательщика</returns>
        TaxPayer GetByKppEffective(string inn, string kpp);

        /// <summary>
        /// Загружает данные налогоплательщика из БД
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="kpp">КПП</param>
        /// <returns>Данные налогоплательщика</returns>
        TaxPayer GetByKppOriginal(string inn, string kpp = null);

        /// <summary>
        /// Загружает данные налогоплательщика из БД
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <returns>Данные налогоплательщика</returns>
        TaxPayer GetByInn(string inn);

        /// <summary>
        /// Получает список банковских счетов юридического лица в соответствии с критериями
        /// </summary>
        /// <param name="criteriaSearh">Критерий отбора</param>
        /// <returns></returns>
        PageResult<BankAccount> GetULBankAccounts(QueryConditions criteriaSearh);

        /// <summary>
        /// Получает список банковских счетов физического лица в соответствии с критериями
        /// </summary>
        /// <param name="criteriaSearh">Критерий отбора</param>
        /// <returns></returns>
        PageResult<BankAccount> GetFLBankAccounts(QueryConditions criteriaSearh);

        PageResult<BankAccountBrief> GetULBankAccountBriefs(string inn, string kpp, DateTime begin, DateTime end, QueryConditions criteria);

        PageResult<BankAccountBrief> GetFLBankAccountBriefs(string inn, DateTime begin, DateTime end, QueryConditions criteria);

        PageResult<BankAccountRequestSummary> GetBankAccountsRequest(QueryConditions criteria);

        List<DeclarationPeriod> GetAvailablePeriods(string inn, string kpp);

        /// <summary>
        /// Статус платёжеспособности НП
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="kppEffective">КПП эффективный</param>
        /// <returns>Статус</returns>
        TaxPayerStatus GetTaxPayerSolvencyStatus(string inn, string kppEffective);
    }
}
