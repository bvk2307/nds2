﻿using Luxoft.NDS2.Server.DAL.Helpers;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Explain.DataMapping
{
    internal class CommentDataMapper : IDataMapper<string>
    {
        public string MapData(IDataRecord reader)
        { 
            return reader.ReadStringNullable(DbConstants.DbUserCommentName);
        }
    }
}