﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Explain.CommandBuilder;
using Luxoft.NDS2.Server.DAL.Explain.DataMapping;

namespace Luxoft.NDS2.Server.DAL.Explain
{
    internal class ExplainControlRatioAdapter : IExplainControlRatioAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public ExplainControlRatioAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public List<ExplainControlRatio> SelectByExplain(long explainZip)
        {
            var sqlExecuter = new ListCommandExecuter<ExplainControlRatio>(new GenericDataMapper<ExplainControlRatio>(),
                _service, _connection);
            var commandBuilder = new GetControlRatioListCommand(explainZip);
            return sqlExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}