﻿namespace Luxoft.NDS2.Server.DAL.Explain
{
    public static class DbConstants
    {
        public static string UpdateCommentProcedure = "P$UPDATE_COMMENT";
        public static string GetCommentProcedure = "P$GET_COMMENT";
        public static string GetExplainDetailsProcedure = "P$GET";
        public static string GetControlRatioListProcedure = "P$GET_CONTROL_RATIO_LIST";
        public static string SearchExplainsByClaimProcedure = "P$SEARCH_BY_CLAIM";


        public static string IdParameter = "pId";
        public static string DocIdParameter = "pDocId";
        public static string TypeParameter = "pType";
        public static string CommentParameter = "pComment";
        public static string ResultCursorParameter = "pResult";
        public static string ZipParameter = "pZip";

        public static string DbUserCommentName = "user_comment";
    }
}
