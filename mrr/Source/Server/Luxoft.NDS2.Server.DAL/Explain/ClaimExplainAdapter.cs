﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Explain.CommandBuilder;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Explain
{
    internal class ClaimExplainAdapter : IClaimExplainAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public ClaimExplainAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public List<ClaimExplain> SearchExplainsByClaim(long claimId)
        {
            var sqlExecuter = new ListCommandExecuter<ClaimExplain>(new GenericDataMapper<ClaimExplain>(),
                _service, _connection);
            var commandBuilder = new SearchExplainsByClaimCommand(claimId);
            return sqlExecuter.TryExecute(commandBuilder).ToList();
        }
    }
}