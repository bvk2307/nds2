﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class UpdateCommentCommand : ExplainPackageCommand
    {
        private readonly long _id;
        private readonly int _type;
        private readonly string _comment;

        public UpdateCommentCommand(long id, int type, string comment)
            : base(DbConstants.UpdateCommentProcedure)
        {
            _id = id;
            _type = type;
            _comment = comment;
        }

        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IdParameter, _id));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.TypeParameter, _type));
            parameters.With(FormatCommandHelper.VarcharIn(DbConstants.CommentParameter, _comment));
        }
    }
}
