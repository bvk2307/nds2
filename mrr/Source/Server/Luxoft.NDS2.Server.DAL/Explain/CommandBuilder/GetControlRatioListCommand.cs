﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class GetControlRatioListCommand : ExplainPackageCommand
    {
        private readonly long _zip;

        public GetControlRatioListCommand(long explainZip)
            : base(DbConstants.GetControlRatioListProcedure)
        {
            _zip = explainZip;
        }
        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.ZipParameter, _zip));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.ResultCursorParameter));
        }
    }
}
