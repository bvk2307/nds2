﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class GetExplainDetailsCommand : ExplainPackageCommand
    {
        private readonly long _id;

        public GetExplainDetailsCommand(long id)
            : base(DbConstants.GetExplainDetailsProcedure)
        {
            _id = id;
        }
        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IdParameter, _id));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.ResultCursorParameter));
        }
    }
}
