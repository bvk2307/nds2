﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class GetCommentCommand : ExplainPackageCommand
    {
        private readonly long _id;
        private readonly int _type;

        public GetCommentCommand(long id, int type)
            : base(DbConstants.GetCommentProcedure)
        {
            _id = id;
            _type = type;
        }
        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.IdParameter, _id));
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.TypeParameter, _type));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.CommentParameter));
        }
    }
}
