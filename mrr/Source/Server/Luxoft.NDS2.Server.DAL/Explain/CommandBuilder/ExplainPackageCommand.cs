﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class ExplainPackageCommand : ExecuteProcedureCommandBuilder
    {
        protected const string PackageName = "PAC$EXPLAIN";
        
        public ExplainPackageCommand(string procedureName)
            : base(string.Format("{0}.{1}", PackageName, procedureName))
        {
        }
    }
}
