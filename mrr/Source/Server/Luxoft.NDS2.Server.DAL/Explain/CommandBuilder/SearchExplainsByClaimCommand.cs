﻿using Luxoft.NDS2.Server.DAL.Helpers;

namespace Luxoft.NDS2.Server.DAL.Explain.CommandBuilder
{
    class SearchExplainsByClaimCommand : ExplainPackageCommand
    {
        private readonly long _id;

        public SearchExplainsByClaimCommand(long id)
            : base(DbConstants.SearchExplainsByClaimProcedure)
        {
            _id = id;
        }
        protected override void DefineParameters(Oracle.DataAccess.Client.OracleParameterCollection parameters)
        {
            parameters.With(FormatCommandHelper.NumericIn(DbConstants.DocIdParameter, _id));
            parameters.With(FormatCommandHelper.Cursor(DbConstants.ResultCursorParameter));
        }
    }
}
