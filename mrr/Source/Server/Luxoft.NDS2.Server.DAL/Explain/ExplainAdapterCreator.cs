﻿using Luxoft.NDS2.Server.Common.Adapters.Explain;


namespace Luxoft.NDS2.Server.DAL.Explain
{
    public static class ExplainAdapterCreator
    {
        public static IExplainCommentAdapter ExplainCommentAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new ExplainCommentAdapter(serviceProvider,  connection);
        }

        public static IExplainAdapter ExplainAdapter(
           IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new ExplainAdapter(serviceProvider, connection);
        }

        public static IExplainControlRatioAdapter ExplainControlRatioAdapter(
         IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new ExplainControlRatioAdapter(serviceProvider, connection);
        }

        public static IClaimExplainAdapter ClaimExplainAdapter(
            IServiceProvider serviceProvider, ConnectionFactoryBase connection)
        {
            return new ClaimExplainAdapter(serviceProvider, connection);
        }
    }
}
