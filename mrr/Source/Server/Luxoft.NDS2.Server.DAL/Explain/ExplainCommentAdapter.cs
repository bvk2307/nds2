﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Explain.CommandBuilder;
using Luxoft.NDS2.Server.DAL.Explain.DataMapping;

namespace Luxoft.NDS2.Server.DAL.Explain
{
    internal class ExplainCommentAdapter : IExplainCommentAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public ExplainCommentAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public string Search(long explainId, ExplainType explainType)
        {
            var sqlExecuter = new ListCommandExecuter<string>(new CommentDataMapper(), _service, _connection);
            var commandBuilder = new GetCommentCommand(explainId, (int) explainType);

            var result = sqlExecuter.TryExecute(commandBuilder).FirstOrDefault();
            return result;
        }

        public void Update(long id, ExplainType type, string text)
        {
            new ResultCommandExecuter(_service, _connection)
                .TryExecute(new UpdateCommentCommand(id, (int) type, text));
        }
    }
}