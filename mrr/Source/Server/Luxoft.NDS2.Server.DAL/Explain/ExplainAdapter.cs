﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Explain.CommandBuilder;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.Explain
{
    internal class ExplainAdapter : IExplainAdapter
    {
        private readonly ConnectionFactoryBase _connection;
        private readonly IServiceProvider _service;

        public ExplainAdapter(IServiceProvider service, ConnectionFactoryBase connection)
        {
            _service = service;
            _connection = connection;
        }

        public ExplainDetailsData Search(long explainId)
        {
            var sqlExecuter = 
                new ListCommandExecuter<ExplainDetailsData>(
                    new GenericDataMapper<ExplainDetailsData>(),
                    _service, 
                    _connection);
            var commandBuilder = new GetExplainDetailsCommand(explainId);
            return sqlExecuter.TryExecute(commandBuilder).FirstOrDefault();
        }
    }
}