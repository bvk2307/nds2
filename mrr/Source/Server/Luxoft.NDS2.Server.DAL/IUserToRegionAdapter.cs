﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы DAL адаптера, для загрузки и управления отвественных пользователей по регионам
    /// </summary>
    public interface IUserToRegionAdapter
    {
        #region Old
        void UserToRegionAddOrRemove(string userSID, string regionCode, bool addOrRemove);
        List<string> GetUsers(string regionCode);
        #endregion

        List<ResponsibilityRegion> GetResponsibilityRegions(int contextId, QueryConditions conditions);
        List<string> GetRegionsOfUser(string userSID, EmployeeType type);
        List<string> GetInspectionsOfUser(string userSID, EmployeeType type);
    }
}