﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.MacroReport
{
    public interface IMacroReportNpAdapter
    {
        /// <summary>
        /// Ищет декларации по заданному критерию
        /// </summary>
        /// <param name="searchCriteria">Критерий поиска деклараций</param>
        /// <returns>список деклараций для отчета</returns>
        List<NpResponseData> Search(FilterExpressionBase searchCriteria, IEnumerable<ColumnSort> orderBy, uint rowsFrom,
            uint rowsTo);

        /// <summary>
        /// Возвращает кол-во записей по указанным критериям поиска
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        int Count(FilterExpressionBase searchCriteria);
    }
}
