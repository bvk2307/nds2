﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DataMapping;
using Luxoft.NDS2.Server.DAL.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.MacroReport
{
    internal class MacroReportNpAdapter : IMacroReportNpAdapter
    {

        private string SEARCH_QUERY_TEMPLATE = @"
select 
* 
from (
      select (rownum - 1) rn, t.* from 
        (
          select 
           *
          from V$MACROREPORT_TAXPAYER_DETAIL vw where {0} {1}
        ) T 
      where (rownum - 1) < {3}
     ) R where rn >= {2}";

        private readonly IServiceProvider _service;
        private readonly ConnectionFactoryBase _connectionFactory;

        public MacroReportNpAdapter(
            IServiceProvider service,
            ConnectionFactoryBase connectionFactory)
        {
            _service = service;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Ищет декларации по заданному критерию
        /// </summary>
        /// <param name="searchCriteria">критерии поиска. год, квартал должны быть обязательными</param>
        /// <param name="orderBy">критерий соритровки</param>
        /// <returns>список детализации по расхождениям</returns>
        public List<NpResponseData> Search(FilterExpressionBase searchCriteria, IEnumerable<ColumnSort> orderBy, uint rowsFrom, uint rowsTo)
        {
            return new ListCommandExecuter<NpResponseData>(
                new GenericDataMapper<NpResponseData>(),
                _service,
                _connectionFactory).TryExecute(
                    new SearchQueryCommandBuilder(string.Format(SEARCH_QUERY_TEMPLATE, "{0}", "{1}", rowsFrom, rowsTo),
                        searchCriteria,
                        orderBy, new PatternProvider("vw"))).ToList();
        }


        public int Count(FilterExpressionBase searchCriteria)
        {
            return new CountCommandExecuter(_service,
                _connectionFactory).TryExecute(new GetCountCommandBuilder("V$MACROREPORT_TAXPAYER_DETAIL vw where {0}", searchCriteria, new PatternProvider("vw")));

        }

    }
}
