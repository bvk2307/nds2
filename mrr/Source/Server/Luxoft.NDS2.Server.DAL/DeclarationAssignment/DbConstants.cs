﻿
namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    public static class DbConstants
    {
        public static readonly string UsersPackageName = "PAC$USERS";
        public static readonly string UsersMergeProcedureName = "P$MERGE";
        public static readonly string UsersMergeParamUserId = "retval";
        public static readonly string UsersMergeParamName = "p_user_name";
        public static readonly string UsersMergeParamUserSid = "p_user_sid";

        public static readonly string DeclarationAssignPackageName = "PAC$DECLARATION";
        public static readonly string DeclarationlAssignProcedureName = "P$ASSIGN_INSPECTOR";
        public static readonly string DeclarationAssignParamInnDeclarant = "p_inn_declarant";
        public static readonly string DeclarationAssignParamKppEff = "p_kpp_effective";
        public static readonly string DeclarationlAssignParamPeriodEff = "p_period_effective";
        public static readonly string DeclarationlAssignParamFiscalYear = "p_fiscal_year";
        public static readonly string DeclarationAssignParamTypeCode = "p_type_code";
        public static readonly string DeclarationAssignParamAssignedTo = "p_assigned_to_id";
        public static readonly string DeclarationAssignParamAssignedBy = "p_assigned_by_id";
    }
}
