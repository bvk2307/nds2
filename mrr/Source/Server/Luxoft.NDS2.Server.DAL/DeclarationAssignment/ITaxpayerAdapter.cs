﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    /// <summary>
    /// Объект доступа к данным для функционала
    /// закреплений налогоплательщиков за инспекторами
    /// </summary>
    public interface ITaxpayerAdapter
    {
        /// <summary>
        /// Получает список налогоплательщиков
        /// </summary>
        /// <param name="sCode">СОНО-код</param>
        PageResult<Taxpayer> GetTaxpayerList(QueryConditions criteria);
    }
}
