﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment.CommandBuilder;
using Oracle.DataAccess.Types;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal class UserAdapter : IUserAdapter
    {
        private readonly IServiceProvider _serviceProvider;

        public UserAdapter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Выполняет вставку или обновление пользователя
        /// </summary>
        /// <param name="userInfo">Информация о пользователе</param>
        /// <returns></returns>
        public long InsertOrUpdate(UserInformation userInfo)
        {
            var retValues = new ResultCommandExecuter(_serviceProvider)
                                .TryExecute(new MergeUserCommandBuilder(userInfo.Name, userInfo.Sid));

            return ((OracleDecimal)retValues[DbConstants.UsersMergeParamUserId]).ToInt64();
        }
    }
}
