﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal class TaxpayerAdapter: BaseOracleTableAdapter, ITaxpayerAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string SEARCH_SQL_GET_TAX_PAYER_LIST = 
              "select vw.* "
            + "from IDA_TAX_PAYER_WHITE_LIST vw "
            + "left join IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia on tpia.{1} = vw.{2} and tpia.{0} = 1 "
            + "where tpia.{1} is null ";
        private const string COUNT_SQL_TAX_PAYERS = 
              "BEGIN SELECT COUNT(1) INTO :pResult FROM IDA_TAX_PAYER_WHITE_LIST vw "
            + "left join IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia on tpia.{1} = vw.{2} and tpia.{0} = 1 "
            + "where tpia.{0} is null ";
        private const string PARAM_SONO_CODE = "p_s_code";
        private const string PARAM_TAXPAYER_CURSOR = "p_taxpayer_cursor";
        private const string COUNT_RESULT = "pResult";
        private const string CURSOR_ALIAS = "pCursor";

        public TaxpayerAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public PageResult<Taxpayer> GetTaxpayerList(QueryConditions criteria)
        {
            var searchSql = String.Format(SEARCH_SQL_GET_TAX_PAYER_LIST,
                TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.IS_ACTIVE),
                TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.INN),
                TypeHelper<Taxpayer>.GetMemberName(t => t.INN)) + "{0} {1}";
            var query = criteria.ToSQL(searchSql, "vw", true, true);
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CURSOR_ALIAS));

            var data = ExecuteList<Taxpayer>(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildTaxpayer);

            return new PageResult<Taxpayer>(data, GetCount(criteria));
        }

        public int GetCount(QueryConditions criteria)
        {
            var countSql = String.Format(COUNT_SQL_TAX_PAYERS,
                TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.IS_ACTIVE),
                TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.INN),
                TypeHelper<Taxpayer>.GetMemberName(t => t.INN)) + "{0}; END;";
            var query = criteria.ToSQL(countSql, "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });

            var result = Execute(query);

            return DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]);
        }

        private Taxpayer BuildTaxpayer(OracleDataReader reader)
        {
            var taxpayer = new Taxpayer
            {
                SONO_CODE = reader.GetString(TypeHelper<Taxpayer>.GetMemberName(t => t.SONO_CODE)),
                INN = reader.GetString(TypeHelper<Taxpayer>.GetMemberName(t => t.INN)),
                KPP = reader.GetString(TypeHelper<Taxpayer>.GetMemberName(t => t.KPP)),
                NAME = reader.GetString(TypeHelper<Taxpayer>.GetMemberName(t => t.NAME))
            };
            return taxpayer;
        }
    }
}
