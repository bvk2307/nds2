﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    /// <summary>
    /// Объект доступа к данным для функционала
    /// назначений ответсвенных инспекторов на декларации
    /// </summary>
    public interface IInspectorDeclarationAssingmentAdapter
    {
        /// <summary>
        /// Возвращает список всех активных инспекторов 
        /// </summary>
        /// <returns>
        /// </returns>
        List<InspectorSonoAssignment> GetActiveInspectors();

        /// <summary>
        /// Возвращает список активных инспекторов или не активных инспекторов с
        /// у которых есть назначенные декларации по коду ИФНС
        /// </summary>
        /// <param name="sCode">
        /// код инспекции
        /// </param>
        /// <returns>
        /// Список инспекторов для заданной ИФНС
        /// </returns>
        List<InspectorSonoAssignment> GetFreeInspectors(string sCode);

        /// <summary>
        /// Пометить существующего инспектора как неактивного
        /// </summary>
        /// <param name="inspector"></param>
        void DeactivateAssignment(InspectorSonoAssignment inspector);

        /// <summary>
        /// Добавить нового инспектора
        /// </summary>
        /// <param name="inspector"></param>
        void AddAssignment(InspectorSonoAssignment inspector);

        /// <summary>
        /// Снимает нагрузку по автоматическим назначению деклараций на инспектора
        /// </summary>
        /// <param name="inspector">
        /// Инспектор, с которого надо снять нагрузку
        /// </param>
        /// <param name="sCode">
        /// Код ИФНС
        /// </param>
        /// <param name="updatedBy">
        /// Кем обновляется запись
        /// </param>
        /// <returns>
        /// Нагрузка успешно снята с инспектора
        /// </returns>
        bool ClearInspectorWorkLoad(InspectorSonoAssignment inspector);


        /// <summary>
        /// Возвращает список настроек нагрузки на активных инспекторов
        /// </summary>
        /// <param name="sCode">
        /// код инспекции
        /// </param>
        /// <returns>
        /// Список настроек нагрузки
        /// </returns>
        List<InspectorWorkload> GetAvailableInspectorsWorkload(string sCode);


        /// <summary>
        /// Возвращает загрузку по умолчанию на инспекцию
        /// </summary>
        /// <param name="sCode">
        /// Код ИФНС из СОНО
        /// </param>
        /// <returns>
        /// Загрузку на инспектора в заданной инспекции по умолчанию
        /// </returns>
        DefaultInspectorWorkLoad GetDefaultInspectorsWorkload(string sCode);

        /// <summary>
        /// Устанавливает нагрузку по автоматическим назначению деклараций на инспектора
        /// </summary>
        /// <param name="workload">
        /// Параметры нагрузки
        /// </param>
        /// <returns>
        /// true запись успешно обработана
        /// false инспектор не активен для этой ИФНС
        /// </returns>
        bool SetInspectorWorkLoad(InspectorWorkload workload);


        /// <summary>
        /// Сохраняет нагрузку по умолчанию
        /// на инспекторов в заданной инспекции
        /// </summary>
        /// <param name="defaultInspectorWorkLoad">
        /// Нагрузка по умолчанию на инспектора
        /// </param>
        void SaveDefaultInspectorsWorkload(DefaultInspectorWorkLoad defaultInspectorWorkLoad);

        List<ActualInspectorWorkload> GetActualInspectorsWorkload(string sonoCode);

        List<DeclarationForAutoDistribution> GetDeclarationsForAutoDistribution(string sonoCode);

        void AssignDeclaration(long declarationId, string sid);

        void AddNonAssignedDeclaration(long declarationId);

        void DistributeIndividualDeclarations(string sonoCode);

        InspectorSonoAssignment GetInspector(string sonoCode, string sid);

        void ClearNonDistributedDeclarations(string sonoCode);
    }

    public class ActualInspectorWorkload
    {
        public bool HasPaymentPermission { get; set; }

        public bool HasCompensationPermission { get; set; }

        public string SID { get; set; }

        public decimal Payment { get; set; }

        public decimal Compensation { get; set; }
    }


    public class DeclarationForAutoDistribution
    {
        public long DeclarationId { get; set; }

        public bool IsPayment { get; set; }

        public bool IsCompensation { get; set; }

        public decimal Amount { get; set; }
    }
}
