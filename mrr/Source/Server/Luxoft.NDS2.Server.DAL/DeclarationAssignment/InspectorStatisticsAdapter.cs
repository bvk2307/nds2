﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal class InspectorStatisticsAdapter : BaseOracleTableAdapter, IInspectorStatisticsAdapter
    {
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string CountFullSQLPattern = "BEGIN select COUNT(*) INTO :pResult from V$INSPECTOR_ASSIGNMENT vw where vw.sono_code = :p_sono_code {0}; END;";
        private const string PARAM_SONO_CODE = "p_sono_code";
        private const string COUNT_RESULT = "pResult";
        private const string CURSOR_ALIAS = "pCursor";
        private const string SEARCH_SQL_ACTIVE = "select vw.* from V$INSPECTOR_ASSIGNMENT vw where vw.IS_ACTIVE = 1 and vw.WL_IS_ACTIVE = 1 and vw.sono_code = :p_sono_code ";
        /// <summary>
        /// Шаблон запроса выборки нагрузки на инспектора. Выбираются все активные инспектора + не активные инспектора у котрых есть хотя бы одна декларация 
        /// </summary>
        private const string SEARCH_SQL_FULL = "select vw.* from V$INSPECTOR_ASSIGNMENT vw where vw.sono_code = :p_sono_code and (vw.COMPENSATION_CNT <> 0 or vw.PAY_CNT <> 0 or vw.WL_IS_ACTIVE = 1) {0} {1}";

        public InspectorStatisticsAdapter(IServiceProvider service)
            :base(service)
        {
        }

        public List<InspectorStatistics> GetActiveInspectorsStatistics(string sono_code)
        {
            var result = ExecuteList(
               SEARCH_SQL_ACTIVE,
               CommandType.Text,
               new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(PARAM_SONO_CODE, sono_code)
                                }.ToArray(),
                BuildTaxpayerInspectorAssignment
                );

            return result;
        }

        public PageResult<InspectorStatistics> GetFullInspectorStatistics(string sono_code, QueryConditions qc)
        {
            var query = qc.ToSQL(SEARCH_SQL_FULL, "vw", true, true);
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(new OracleParameter(PARAM_SONO_CODE, sono_code));
            parameters.Add(FormatCommandHelper.Cursor(CURSOR_ALIAS));
            var data = ExecuteList(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildTaxpayerInspectorAssignment);

            query = qc.ToSQL(CountFullSQLPattern, "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });
            query.Parameters.Add(new QueryParameter { Name = PARAM_SONO_CODE, Value = sono_code });
            var result = Execute(query);


            return new PageResult<InspectorStatistics>(
                data,
                DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]));
        }

        private InspectorStatistics BuildTaxpayerInspectorAssignment(OracleDataReader reader)
        {
            var obj = new InspectorStatistics
                          {
                              SID = reader.ReadString(TypeHelper<InspectorStatistics>.GetMemberName(t => t.SID)),
                              NAME = reader.ReadString(TypeHelper<InspectorStatistics>.GetMemberName(t => t.NAME)),
                              EMPLOYEE_NUM =
                                  reader.ReadString(TypeHelper<InspectorStatistics>.GetMemberName(t => t.EMPLOYEE_NUM)),
                              COMPENSATION_CNT =
                                  reader.ReadDecimal(TypeHelper<InspectorStatistics>.GetMemberName(t => t.COMPENSATION_CNT)),
                              NDS_COMPENSATION_AMOUNT =
                                  reader.ReadDecimal(
                                      TypeHelper<InspectorStatistics>.GetMemberName(t => t.NDS_COMPENSATION_AMOUNT)),
                              PAY_CNT = reader.ReadDecimal(TypeHelper<InspectorStatistics>.GetMemberName(t => t.PAY_CNT)),
                              CLAIM_AMOUNT =
                                  reader.ReadDecimal(TypeHelper<InspectorStatistics>.GetMemberName(t => t.CLAIM_AMOUNT)),
                              IS_ACTIVE =
                                  reader.ReadBoolean(TypeHelper<InspectorStatistics>.GetMemberName(t => t.IS_ACTIVE))
                          };
            ;
            return obj;
        }
    }
}
