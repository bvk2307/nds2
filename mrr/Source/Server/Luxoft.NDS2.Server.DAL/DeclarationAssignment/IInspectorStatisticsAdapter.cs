﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    public interface IInspectorStatisticsAdapter
    {
        List<InspectorStatistics> GetActiveInspectorsStatistics(string sono_code);
        PageResult<InspectorStatistics> GetFullInspectorStatistics(string sono_code, QueryConditions qc);
    }
}
