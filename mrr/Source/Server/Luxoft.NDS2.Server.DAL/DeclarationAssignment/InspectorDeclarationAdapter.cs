﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    class InspectorDeclarationAdapter : BaseOracleTableAdapter, IInspectorDeclarationAdapter
    {
        private const string QueryScope = "DeclarationAssignment";
        private const string DeclarationDistributionQueryPattern = "DECL_DISTRIB_QUERY_PATTERN";
        private const string DeclarationDistributionCountPattern = "DECL_DISTRIB_COUNT_PATTERN";
        private const string DeclarationNotDistributionQueryPattern = "DECL_NOT_DISTRIB_QUERY_PATTERN";
        private const string DeclarationNotDistributionCountPattern = "DECL_NOT_DISTRIB_COUNT_PATTERN";
        private const string ViewAlias = "VIEW_ALIAS";
        private const string CursorAlias = "CURSOR_ALIAS";
        private const string SearchPattern = "SEARCH_PATTERN";
        private const string ResultAlias = "RESULT_ALIAS";

        public InspectorDeclarationAdapter(IServiceProvider service)
            : base(service)
        {
        }

        private static void ReplacePeriodSortingAndFiltering(QueryConditions qc)
        {
            var periodSorting = qc.Sorting.SingleOrDefault(s => s.ColumnKey.ToUpper() == "PERIOD");
            if (periodSorting != null)
            {
                qc.Sorting.Remove(periodSorting);
                qc.Sorting.Add(new ColumnSort
                                   {
                                       ColumnKey = "FISCALYEAR",
                                       Order = periodSorting.Order,
                                       Inverted = periodSorting.Inverted,
                                       SortKind = periodSorting.SortKind
                                   });
                qc.Sorting.Add(new ColumnSort
                                   {
                                       ColumnKey = "PERIODORDER",
                                       Order = periodSorting.Order,
                                       Inverted = periodSorting.Inverted,
                                       SortKind = periodSorting.SortKind
                                   });
            }
            var periodFilter = qc.Filter.SingleOrDefault(f => f.ColumnName.ToUpper() == "PERIOD");
            if(periodFilter != null)
            {
                var periodFiltering =  periodFilter.Filtering.FirstOrDefault();
                if( periodFiltering != null)
                {
                    if (periodFiltering.Value.ToString().ToUpper() == "ПРЕДЫДУЩИЙ")
                    {
                        qc.Filter.Remove(periodFilter);
                        var previousPeriod = FisalPeriod.GetPrevious();
                        qc.Filter.Add(new FilterQuery { ColumnName = "FISCALYEAR", FilterOperator = FilterQuery.FilterLogicalOperator.And ,Filtering = new List<ColumnFilter> { new ColumnFilter { Value = previousPeriod.FiscalYear, ComparisonOperator= ColumnFilter.FilterComparisionOperator.Equals } } });
                        qc.Filter.Add(new FilterQuery { ColumnName = "QUARTER", FilterOperator = FilterQuery.FilterLogicalOperator.And, Filtering = new List<ColumnFilter> { new ColumnFilter { Value = previousPeriod.Quarter, ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals } } });
                    }
                }
            }
        }



        public PageResult<InspetorDeclarationDistribution> GetInspectorDeclarationsDistribution(string sCode, QueryConditions qc)
        {
            ReplacePeriodSortingAndFiltering(qc);
            var queryText = _service.GetQueryText(QueryScope, DeclarationDistributionQueryPattern);
            var viewAlias = _service.GetQueryText(QueryScope, ViewAlias);
            var cursorAlias = _service.GetQueryText(QueryScope, CursorAlias);
            var searcPattern = _service.GetQueryText(QueryScope, SearchPattern);

            var query = qc.ToSQL(queryText, viewAlias, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(new OracleParameter("p_sono_code", sCode));
            parameters.Add(FormatCommandHelper.Cursor(cursorAlias));
            var sql = string.Format(searcPattern, query.Text);
            Func<ResultCommandHelper, List<InspetorDeclarationDistribution>> readFunc = reader =>
            {
                var result = new List<InspetorDeclarationDistribution>();
                while (reader.Read())
                {
                    var inspDecl = new Declaration
                    {
                        DeclarationVersionId = reader.GetNullableInt64("DECLARATIONVERSIONID"),
                        Id = reader.GetInt64("ID"),
                        DeclSign = reader.GetString("DECLSIGN"),
                        Inn =
                            reader.
                            GetString
                            ("INN"),
                        Kpp =
                            reader.
                            GetString
                            ("KPP"),
                        Name =
                            reader.
                            GetString
                            ("NAME"),
                        Period =
                            reader.
                            GetString
                            ("PERIOD"),
                        IsTaxPayerAttached
                            =
                            reader.
                            GetBool
                            ("ISTAXPAYERATTACHED"),
                        CompensationAmount = reader.
                        GetDecimal("COMPENSATIONAMOUNT"),
                        KNPStatus = reader.GetString("KNPSTATUS"),
                        ClaimAmount = reader.GetNullableDecimal("CLAIMAMOUNT")
                    };
                    var insp = new InspectorSonoAssignment
                                   {
                                       SID = reader.GetString("SID"),
                                       Name = reader.GetString("INSPECTORNAME"),
                                       EmployeeNum = reader.GetString("EMPLOYEENUM"),
                                       InsertDate = reader.GetDate("INSERT_DATE"),
                                       InsertedBy = reader.GetString("INSERTED_BY"),
                                       UpdatedDate = reader.GetDate("UPDATE_DATE"),
                                       UpdatedBy = reader.GetString("UPDATED_BY"),
                                       IsActive = reader.GetBool("IS_ACTIVE"),
                                       SonoCode = reader.GetString("S_CODE")
                                   };
                    result.Add(new InspetorDeclarationDistribution {Inspector = insp, Declaration = inspDecl});
                }
                return result;
            };
            return new PageResult<InspetorDeclarationDistribution>(new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = sql,
                    Type = CommandType.Text,
                    Parameters = parameters
                }, readFunc), GetInspetorDeclarationsCount(sCode, qc));
        }



        private int GetInspetorDeclarationsCount(string sCode, QueryConditions criteria)
        {
            var queryText = _service.GetQueryText(QueryScope, DeclarationDistributionCountPattern);
            var viewAlias = _service.GetQueryText(QueryScope, ViewAlias);
            var resultAlias = _service.GetQueryText(QueryScope, ResultAlias);
            var query = criteria.ToSQL(queryText, viewAlias, false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = resultAlias });
            query.Parameters.Add(new QueryParameter
            {
                Name = "p_sono_code",
                Value = sCode
            });
            var result = Execute(query);
            return DataReaderExtension.ReadInt(result.Output[resultAlias]);
        }

        public PageResult<InspectorDeclarationNotDistribution> GetInspectorDeclarationsNotDistribution(
            string inscpectionCode, QueryConditions searchCriteria)
        {
            ReplacePeriodSortingAndFiltering(searchCriteria);

            var queryText = _service.GetQueryText(QueryScope, DeclarationNotDistributionQueryPattern);
            var viewAlias = _service.GetQueryText(QueryScope, ViewAlias);
            var cursorAlias = _service.GetQueryText(QueryScope, CursorAlias);
            var searcPattern = _service.GetQueryText(QueryScope, SearchPattern);

            var query = searchCriteria.ToSQL(queryText, viewAlias, true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(new OracleParameter("p_sono_code", inscpectionCode));
            parameters.Add(FormatCommandHelper.Cursor(cursorAlias));
            var sql = string.Format(searcPattern, query.Text);

            return new PageResult<InspectorDeclarationNotDistribution>(
                new CommandExecuter(_service).TryRead(new CommandContext
                {
                    Text = sql,
                    Type = CommandType.Text,
                    Parameters = parameters
                }, 
                ReadInspectorDeclarationNotDistribution),
                GetInspectorDeclarationsNotDistributionCount(inscpectionCode, searchCriteria));
        }

        private List<InspectorDeclarationNotDistribution> ReadInspectorDeclarationNotDistribution(ResultCommandHelper reader)
        {
            var result = new List<InspectorDeclarationNotDistribution>();
            while (reader.Read())
            {
                result.Add(BuildInspectorDeclarationNotDistribution(reader));
            }
            return result;
        }

        private InspectorDeclarationNotDistribution BuildInspectorDeclarationNotDistribution(ResultCommandHelper reader)
        {
            var declaration = new Declaration();
            declaration.DeclarationVersionId = reader.GetNullableInt64("DECLARATIONVERSIONID");
            declaration.Id = reader.GetInt64("ID");
            declaration.DeclSign = reader.GetString("DECLSIGN");
            declaration.Inn = reader.GetString("INN");
            declaration.Kpp = reader.GetString("KPP");
            declaration.Name = reader.GetString("NAME");
            declaration.Period = reader.GetString("PERIOD");
            declaration.IsTaxPayerAttached = reader.GetBool("ISTAXPAYERATTACHED");
            declaration.CompensationAmount = reader.GetDecimal("COMPENSATIONAMOUNT");
            declaration.ClaimAmount = reader.GetNullableDecimal("CLAIMAMOUNT");
            
            var inspector = new InspectorDeclarationLastAssignment();
            inspector.SID = reader.GetString("SID");
            inspector.Name = reader.GetString("INSPECTORNAME");
            inspector.EmployeeNum = reader.GetString("EMPLOYEENUM");

            return new InspectorDeclarationNotDistribution 
            { 
                Declaration = declaration,
                InspectorLastAssignment = inspector 
            };
        }

        private int GetInspectorDeclarationsNotDistributionCount(string inscpectionCode, QueryConditions searchCriteria)
        {
            ReplacePeriodSortingAndFiltering(searchCriteria);

            var queryText = _service.GetQueryText(QueryScope, DeclarationNotDistributionCountPattern);
            var viewAlias = _service.GetQueryText(QueryScope, ViewAlias);
            var resultAlias = _service.GetQueryText(QueryScope, ResultAlias);
            var query = searchCriteria.ToSQL(queryText, viewAlias, false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = resultAlias });
            query.Parameters.Add(new QueryParameter
            {
                Name = "p_sono_code",
                Value = inscpectionCode
            });
            var result = Execute(query);
            return DataReaderExtension.ReadInt(result.Output[resultAlias]);
        }
    }
}
