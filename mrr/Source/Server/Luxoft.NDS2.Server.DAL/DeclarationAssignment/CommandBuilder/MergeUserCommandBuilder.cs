﻿using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment.CommandBuilder
{
    /// <summary>
    /// Строит команду вызова хранимой процедуры осуществляющей вставку или обновление вользователя.
    /// Все названия параметров и наименование хранимой процедуры в DbConstants
    /// </summary>
    internal class MergeUserCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private string _userName;
        private string _userSid;
        public MergeUserCommandBuilder(string userName, string userSid)
            : base(CommandBuildHelper.PackageProcedure(DbConstants.UsersPackageName, DbConstants.UsersMergeProcedureName))
        {
            _userName = userName;
            _userSid = userSid;
        }
        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(DbConstants.UsersMergeParamUserId, OracleDbType.Decimal, System.Data.ParameterDirection.ReturnValue);
            parameters.Add(DbConstants.UsersMergeParamName, _userName);
            parameters.Add(DbConstants.UsersMergeParamUserSid, _userSid);
        }
    }
}
