﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.CommandBuilders.Oracle;
using Oracle.DataAccess.Client;
using System;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment.CommandBuilder
{
    /// <summary>
    /// Строит команду вызова хранимой процедуры назначения декларации на пользователя.
    /// </summary>
    internal class InsertDeclarationAssignmentCommandBuilder : ExecuteProcedureCommandBuilder
    {
        private long _assignedBy;
        private long _assignedTo;
        private DeclarationSummaryKey _declarationKey;

        public InsertDeclarationAssignmentCommandBuilder(DeclarationSummaryKey declarationKey, long assignedBy, long assignedTo)
            : base(CommandBuildHelper.PackageProcedure(DbConstants.DeclarationAssignPackageName, DbConstants.DeclarationlAssignProcedureName))
        {
            if (declarationKey == null)
                throw new ArgumentNullException("declarationKey");
            _declarationKey = declarationKey;
            _assignedBy = assignedBy;
            _assignedTo = assignedTo;
        }

        protected override void DefineParameters(OracleParameterCollection parameters)
        {
            parameters.Add(DbConstants.DeclarationAssignParamInnDeclarant, _declarationKey.InnDeclarant);
            parameters.Add(DbConstants.DeclarationAssignParamKppEff, _declarationKey.KppEffective);
            parameters.Add(DbConstants.DeclarationlAssignParamPeriodEff, _declarationKey.PeriodEffective);
            parameters.Add(DbConstants.DeclarationlAssignParamFiscalYear, _declarationKey.FiscalYear);
            parameters.Add(DbConstants.DeclarationAssignParamTypeCode, _declarationKey.IsJournal ? 1 : 0);
            parameters.Add(DbConstants.DeclarationAssignParamAssignedTo,_assignedTo);
            parameters.Add(DbConstants.DeclarationAssignParamAssignedBy, _assignedBy);
        }
    }
}
