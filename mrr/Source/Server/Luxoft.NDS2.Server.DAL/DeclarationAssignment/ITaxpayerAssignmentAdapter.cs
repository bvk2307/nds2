﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    /// <summary>
    /// Объект доступа к данным для функционала
    /// закреплений налогоплательщиков за инспекторами
    /// </summary>
    public interface ITaxpayerAssignmentAdapter
    {
        /// <summary>
        /// Возвращает набор данных для списка налогоплательщиков, не подлежащих автоматическому назначению
        /// </summary>
        /// <param name="qc">Критерий</param>
        /// <returns></returns>
        PageResult<TaxpayerAssignment> GetTaxpayersAssignmentsList(QueryConditions qc);

        /// <summary>
        /// Перемещает НП в список НП, не подлежащих автоматической обработке
        /// </summary>
        /// <param name="ta">Список НП</param>
        /// <returns></returns>
        bool AddTaxpayerAssignment(Taxpayer t, string userId);

        /// <summary>
        /// Удаляет НП из списка НП, не подлежащих автоматической обработке 
        /// </summary>
        /// <param name="ta"></param>
        /// <returns></returns>
        bool RemoveTaxpayerAssignment(TaxpayerAssignment ta, string userId);

        /// <summary>
        /// Назначает инспектора на НП
        /// </summary>
        /// <param name="ta"></param>
        /// <returns></returns>
        bool AddInspectorAssignment(TaxpayerAssignment ta, string userId);

        /// <summary>
        /// Отменяет назначение
        /// </summary>
        /// <param name="ta"></param>
        /// <returns></returns>
        bool CancelInspectorAssignment(TaxpayerAssignment ta, string userId);
    }
}
