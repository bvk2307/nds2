using System;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    struct FisalPeriod
    {
        public int FiscalYear { get; set; }
        public int Quarter { get; set; }

        public static FisalPeriod GetPrevious()
        {
            var now = DateTime.UtcNow;
            var fiscalYear = now.Year;
            var quater = (now.Month + 2) / 3;
            if (quater == 1)
            {
                quater = 4;
                fiscalYear--;
            }
            else
            {
                quater--;
            }
            return new FisalPeriod { FiscalYear = fiscalYear, Quarter = quater };
        }
    }
}