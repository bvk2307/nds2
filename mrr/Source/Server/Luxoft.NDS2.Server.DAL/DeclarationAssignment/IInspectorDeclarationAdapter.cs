﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    public interface IInspectorDeclarationAdapter
    {
        PageResult<InspetorDeclarationDistribution> GetInspectorDeclarationsDistribution(string sCode, QueryConditions qc);

        PageResult<InspectorDeclarationNotDistribution> GetInspectorDeclarationsNotDistribution(string inscpectionCode, QueryConditions searchCriteria);
    }
}
