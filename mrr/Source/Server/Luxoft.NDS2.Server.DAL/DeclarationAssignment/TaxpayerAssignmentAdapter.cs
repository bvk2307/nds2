﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal class TaxpayerAssignmentAdapter : BaseOracleTableAdapter, ITaxpayerAssignmentAdapter
    {
        private const string PACKAGE_NAME = "NDS2_MRR_USER.PAC$TAX_PAYER_SETTINGS";
        private const string PROCEDURE_ADD_TAX_PAYER_ASSIGNMENT = "ADD_TAX_PAYER_ASSIGNMENT";
        private const string PROCEDURE_REMOVE_TAX_PAYER_ASSIGNMENT = "REMOVE_TAX_PAYER_ASSIGNMENT";
        private const string PROCEDURE_ADD_INSPECTOR_ASSIGNMENT = "ADD_INSPECTOR_ASSIGNMENT";
        private const string PROCEDURE_CANCEL_ASSIGNMENT = "CANCEL_ASSIGNMENT";
        private const string SearchSQLCommandPattern = "BEGIN OPEN :pCursor FOR {0}; END;";
        private const string PARAM_SONO_CODE = "p_sono_code";
        private const string PARAM_TAXPAYER_ASSIGNMENT_CURSOR = "p_taxpayer_cursor";
        private const string PARAM_INN = "p_inn";
        private const string PARAM_SID = "p_sid";
        private const string PARAM_UPDATE_BY = "p_updated_by";
        private const string PARAM_IS_ACTIVE = "p_is_active";
        private const string COUNT_RESULT = "pResult";
        private const string CURSOR_ALIAS = "pCursor";
        private const string SEARCH_SQL_TAXPAYERS_ASSGNMENTS = "select vw.* from V$TAXPAYER_ASSIGNMENT vw where vw.{0} = 1 ";
        private const string COUNT_SQL_TAXPAYERS_ASSIGNMENTS = "BEGIN SELECT COUNT(1) INTO :pResult FROM V$TAXPAYER_ASSIGNMENT vw WHERE 1=1 {0}; END;";
        private const string MERGE_TO_TAXPAYER_ASSIGNMENT = "";

        public TaxpayerAssignmentAdapter(IServiceProvider service)
            :base(service)
        {
        }


        public PageResult<TaxpayerAssignment> GetTaxpayersAssignmentsList(QueryConditions criteria)
        {
            var searchSql = String.Format(SEARCH_SQL_TAXPAYERS_ASSGNMENTS, TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.IS_ACTIVE)) + "{0} {1}";
            var query = criteria.ToSQL(searchSql, "vw", true, true);

            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(CURSOR_ALIAS));

            var data = ExecuteList(
                string.Format(SearchSQLCommandPattern, query.Text),
                CommandType.Text,
                parameters.ToArray(),
                BuildTaxpayerAssignment);

            return new PageResult<TaxpayerAssignment>(data, GetCount(criteria));
        }

        public int GetCount(QueryConditions criteria)
        {
            var query = criteria.ToSQL(COUNT_SQL_TAXPAYERS_ASSIGNMENTS, "vw", false, true);
            query.Parameters.Add(new QueryParameter(false) { Name = COUNT_RESULT });

            var result = Execute(query);

            return DataReaderExtension.ReadInt(result.Output[COUNT_RESULT]);
        }

        private TaxpayerAssignment BuildTaxpayerAssignment(OracleDataReader reader)
        {
            var obj = new TaxpayerAssignment()
            {
                INN = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.INN)),
                KPP = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.KPP)),
                TAX_PAYER_NAME = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.TAX_PAYER_NAME)),

                SID = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.SID)),
                EMPLOYEE_NUM = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.EMPLOYEE_NUM)),
                INSPECTOR_NAME = reader.GetString(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.INSPECTOR_NAME)),
                IS_ACTIVE = reader.GetBoolean(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.IS_ACTIVE)),
                IS_INSPECTOR_ACTIVE = reader.GetBoolean(TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.IS_INSPECTOR_ACTIVE))
         
            };

            return obj;
        }

        public bool AddTaxpayerAssignment(Taxpayer taxpayer, string userId)
        {
            var isActiveParam = FormatCommandHelper.NumericOut(PARAM_IS_ACTIVE);
            var result = Execute(
                String.Format("{0}.{1}", PACKAGE_NAME, PROCEDURE_ADD_TAX_PAYER_ASSIGNMENT),
                CommandType.StoredProcedure,
                new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(PARAM_SONO_CODE, taxpayer.SONO_CODE),
                                    FormatCommandHelper.VarcharIn(PARAM_INN, taxpayer.INN),
                                    FormatCommandHelper.VarcharIn(PARAM_UPDATE_BY, userId),
                                    isActiveParam
                                });
            return ((OracleDecimal)isActiveParam.Value).Value == 1;
        }

        public bool RemoveTaxpayerAssignment(TaxpayerAssignment ta, string userId)
        {
            var isActiveParam = FormatCommandHelper.NumericOut(PARAM_IS_ACTIVE);
            var result = Execute(
                String.Format("{0}.{1}", PACKAGE_NAME, PROCEDURE_REMOVE_TAX_PAYER_ASSIGNMENT),
                CommandType.StoredProcedure,
                new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(PARAM_INN, ta.INN),
                                    FormatCommandHelper.VarcharIn(PARAM_UPDATE_BY, userId),
                                    isActiveParam
                                });
            return ((OracleDecimal)isActiveParam.Value).Value == 1;
        }

        public bool AddInspectorAssignment(TaxpayerAssignment ta, string userId)
        {
            var result = Execute(
                String.Format("{0}.{1}", PACKAGE_NAME, PROCEDURE_ADD_INSPECTOR_ASSIGNMENT),
                CommandType.StoredProcedure,
                new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(PARAM_INN, ta.INN),
                                    FormatCommandHelper.VarcharIn(PARAM_SID, ta.SID),
                                    FormatCommandHelper.VarcharIn(PARAM_UPDATE_BY, userId)
                                });
            return true;
        }

        public bool CancelInspectorAssignment(TaxpayerAssignment ta, string userId)
        {
            var result = Execute(
                String.Format("{0}.{1}", PACKAGE_NAME, PROCEDURE_CANCEL_ASSIGNMENT),
                CommandType.StoredProcedure,
                new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(PARAM_INN, ta.INN),
                                    FormatCommandHelper.VarcharIn(PARAM_UPDATE_BY, userId)
                                });
            return true;
        }
    }
}
