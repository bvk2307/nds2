﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal sealed class InspectorDeclarationAssignmentAdapter : IInspectorDeclarationAssingmentAdapter
    {
        private const string PackageName = "NDS2_MRR_USER.PAC$INSPECTOR_DECL_ASSG";

        private const string GetInspectorsSonoAssignmentsProcedureName = "GET_INSPECTOR_SONO_ASSG";
        private const string GetInspectorProcedureName = "GET_INSPECTOR";
        private const string GetActualInspectorWorkloadProcedureName = "GET_ACTUAL_INSPECTOR_WORKLOADS";
        private const string GetFreeInspectorAssignmentsProcedureName = "GET_FREE_ASSIGNMENTS";
        private const string SaveDefaultInspectorsWorkloadProcedureName = "SET_DEFAULT_WORKLOAD";
        private const string DeactivateInspectorProcedureName = "DEACTIVATE_INSPECTOR";
        private const string AddInspectorProcedureName = "ADD_INSPECTOR";
        private const string GetInspectorWorkLoadProcedureName = "GET_INSPECTORS_WORKLOAD";
        private const string GetDefaultWorkLoadProcedureName = "GET_DEFAULT_WORKLOAD";
        private const string SetInspectorWorkloadProcedureName = "SET_INSPECTOR_WORKLOAD";
        private const string ClearInspectorWorkloadProcedureName = "CLEAR_INSPECTOR_WORK_LOAD";
        private const string DistributeIndividualDeclarationsProcedureName = "DISTRIBUTE_INDIVIDUAL_DECL";
        private const string GetDeclarationsForAutoDistributionProcedureName = "GET_DECLS_FOR_AUTO_DISTR";
        private const string AddNonAssignedDeclarationsProcedureName = "ADD_NON_ASSIGNED_DECL";
        private const string AssignDeclarationProcedureName = "ASSIGN_DECLARATION";
        private const string ClearNonDistributedDeclarationsProcedureName = "CLEAR_NON_DISTRIB_DECLS";

        private const string ParamSid = "p_sid";
        private const string ParamIsActive = "p_is_active";
        private const string ParamIsSonoAssgActive = "p_is_sono_assg_active";
        private const string ParamUpdBy = "p_updated_by";
        private const string ParamInspectorCursor = "p_inspector_cursor";
        private const string ParamSONOCodeCursor = "p_s_code_cursor";
        private const string ParamWorkloadCursor = "p_workload_cursor";
        private const string ParamSONOCode = "p_s_code";
        private const string ParamInpectorName = "p_name";
        private const string ParamEmpNum = "p_employee_num";
        private const string ParamMaxPaymentCapacity = "p_max_payment_cap";
        private const string ParamMaxCompenationCapacity = "p_max_comp_cap";
        private const string ParamHasPaymentPermission = "p_has_payment_perm";
        private const string ParamHasCompensationPermission = "p_has_compensation_perm";
        private const string ParamResult = "p_result";
        private const string ParamIsPaused = "p_is_paused";
        private const string ParamActualWorkloadCusor = "p_act_wl_cursor";
        private const string ParamDeclCursor = "p_decl_cursor";
        private const string ParamDeclarationId = "p_id";
        private readonly IServiceProvider _service;

        public InspectorDeclarationAssignmentAdapter(IServiceProvider service)
        {
            _service = service;
        }

        public List<InspectorSonoAssignment> GetActiveInspectors()
        {
            Func<ResultCommandHelper, List<InspectorSonoAssignment>> readFunc =
                reader =>
                {
                    var inspectors = new List<InspectorSonoAssignment>();
                    while (reader.Read())
                    {
                        var inspector = LoadAssignmentInspector(reader);
                        inspectors.Add(inspector);
                    }
                    return inspectors;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetInspectorsSonoAssignmentsProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter> { FormatCommandHelper.Cursor(ParamInspectorCursor) }
                }, readFunc);
        }

        public List<InspectorSonoAssignment> GetFreeInspectors(string sCode)
        {
            Func<ResultCommandHelper, List<InspectorSonoAssignment>> readFunc =
                reader =>
                {
                    var inspectors = new List<InspectorSonoAssignment>();
                    while (reader.Read())
                    {
                        var inspector = LoadAssignmentInspector(reader);
                        inspectors.Add(inspector);
                    }
                    return inspectors;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetFreeInspectorAssignmentsProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter>
                                     {
                                         FormatCommandHelper.VarcharIn(ParamSONOCode,sCode),
                                         FormatCommandHelper.Cursor(ParamInspectorCursor)
                                     }
                }, readFunc);
        }

        public void DeactivateAssignment(InspectorSonoAssignment inspector)
        {
            _service.LogNotification(
              string.Format("IDA: удаление инспектора: sid {0}, имя {1}", inspector.SID, inspector.Name),
              "InspectorDeclarationAssignmentAdapter.DeactivateAssignment");
            var executer = new CommandExecuter(_service);
            executer.TryExecute(
                new CommandContext
                    {
                        Text = FormatCommandHelper.PackageProcedure(PackageName, DeactivateInspectorProcedureName),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(ParamSid, inspector.SID),
                                     FormatCommandHelper.VarcharIn(ParamSONOCode, inspector.SonoCode),
                                    FormatCommandHelper.VarcharIn(ParamUpdBy,inspector.UpdatedBy)
                                }
                    });
        }

        public void AddAssignment(InspectorSonoAssignment inspector)
        {
            _service.LogNotification(
                string.Format("IDA: добавление инспектора: sid {0}, имя {1}", inspector.SID, inspector.Name),
                "InspectorDeclarationAssignmentAdapter.AddAssignment");
            var executer = new CommandExecuter(_service);
            executer.TryExecute(
                new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, AddInspectorProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(ParamSid, inspector.SID),
                                    FormatCommandHelper.VarcharIn(ParamInpectorName, inspector.Name),
                                    FormatCommandHelper.VarcharIn(ParamEmpNum, inspector.EmployeeNum),
                                    FormatCommandHelper.VarcharIn(ParamSONOCode,inspector.SonoCode),
                                    FormatCommandHelper.VarcharIn(ParamUpdBy, inspector.UpdatedBy)
                                }
                });
        }

        public List<InspectorWorkload> GetAvailableInspectorsWorkload(string sCode)
        {
            Func<ResultCommandHelper, List<InspectorWorkload>> readFunc =
                reader =>
                {
                    var inspectorWorkloads = new List<InspectorWorkload>();
                    while (reader.Read())
                    {
                        var inspector = LoadAssignmentInspector(reader);
                        var inspectorWorkload = LoadWorkLoad(reader);
                        inspectorWorkload.Inspector = inspector;
                        inspectorWorkloads.Add(inspectorWorkload);
                    }
                    return inspectorWorkloads;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                             {
                                 Text =
                                     FormatCommandHelper.PackageProcedure(PackageName, GetInspectorWorkLoadProcedureName),
                                 Type = CommandType.StoredProcedure,
                                 Parameters = new List<OracleParameter>
                                                  {
                                                      FormatCommandHelper.VarcharIn(ParamSONOCode, sCode),
                                                      FormatCommandHelper.Cursor(ParamInspectorCursor)
                                                  }
                             }, readFunc);
        }

        public DefaultInspectorWorkLoad GetDefaultInspectorsWorkload(string sCode)
        {
            Func<ResultCommandHelper, DefaultInspectorWorkLoad> readFunc =
                reader =>
                {
                    if (reader.Read())
                    {
                        var dfltLoad = new DefaultInspectorWorkLoad
                                           {
                                               SonoCode = reader.GetString("S_CODE"),
                                               HasCompensationPermission = reader.GetBool("COMPENSATION_DECL_PERM"),
                                               HasPaymentPermission = reader.GetBool("PAYMENT_DECL_PERM"),
                                               MaxPaymentCapacity = reader.GetDecimal("MAX_PAYMENT_CAPACITY"),
                                               MaxCompenationCapacity =
                                                   reader.GetDecimal("MAX_COMPENSATION_CAPACITY"),
                                               InsertDate = reader.GetDate("INSERT_DATE"),
                                               InsertedBy = reader.GetString("INSERTED_BY"),
                                               UpdatedDate = reader.GetDate("UPDATE_DATE"),
                                               UpdatedBy = reader.GetString("UPDATED_BY"),
                                               IsActive = reader.GetBool("IS_ACTIVE")
                                           };
                        return dfltLoad;
                    }
                    return null;
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                             {
                                 Text =
                                     FormatCommandHelper.PackageProcedure(PackageName, GetDefaultWorkLoadProcedureName),
                                 Type = CommandType.StoredProcedure,
                                 Parameters = new List<OracleParameter>
                                                  {
                                                      FormatCommandHelper.VarcharIn(ParamSONOCode, sCode),
                                                      FormatCommandHelper.Cursor(ParamWorkloadCursor)
                                                  }
                             }, readFunc);
        }

        public bool SetInspectorWorkLoad(InspectorWorkload workload)
        {
            _service.LogNotification(
                                        string.Format("IDA: установление параметров нагрузки на инспектора, доступного для автоматического назначения деклараций : sid {0}, имя {1}," +
                                                      "к уплате {2}, к возмещению {3}",
                                                       workload.Inspector.SID, workload.Inspector.Name, workload.MaxPaymentCapacity, workload.MaxCompenationCapacity),
                                        "InspectorDeclarationAssignmentAdapter.SetInspectorWorkLoad");
            var isActiveParam = FormatCommandHelper.NumericOut(ParamIsActive);
            var executer = new CommandExecuter(_service);
            executer.TryExecute(
                new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, SetInspectorWorkloadProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(ParamSid, workload.Inspector.SID),
                                    FormatCommandHelper.VarcharIn(ParamSONOCode,workload.SonoCode),
                                    FormatCommandHelper.DecimalIn(ParamMaxPaymentCapacity, workload.MaxPaymentCapacity),
                                    FormatCommandHelper.DecimalIn(ParamMaxCompenationCapacity, workload.MaxCompenationCapacity),
                                    FormatCommandHelper.NumericIn(ParamHasPaymentPermission, workload.HasPaymentPermission? 1:0),
                                    FormatCommandHelper.NumericIn(ParamHasCompensationPermission, workload.HasCompensationPermission?1:0),
                                    FormatCommandHelper.VarcharIn(ParamUpdBy, workload.UpdatedBy),
                                    FormatCommandHelper.NumericIn(ParamIsPaused, workload.IsPaused? 1:0),
                                    isActiveParam
                                }
                });
            bool isActive = ((OracleDecimal)isActiveParam.Value).Value == 1;
            workload.IsActive = isActive;
            workload.Inspector.IsActive = workload.IsActive;
            return isActive;
        }

        public bool ClearInspectorWorkLoad(InspectorSonoAssignment inspector)
        {
            _service.LogNotification(
                                  string.Format("IDA: удаление параметров нагрузки на инспектора, доступного для автоматического назначения деклараций : sid {0}, имя {1},",
                                                 inspector.SID, inspector.Name),
                                  "InspectorDeclarationAssignmentAdapter.ClearInspectorWorkLoad");
            var resultParam = FormatCommandHelper.NumericOut(ParamResult);
            var isSonoAssgActiveParam = FormatCommandHelper.NumericOut(ParamIsSonoAssgActive);
            var executer = new CommandExecuter(_service);
            executer.TryExecute(
                new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, ClearInspectorWorkloadProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(ParamSid, inspector.SID),
                                    FormatCommandHelper.VarcharIn(ParamSONOCode,inspector.SonoCode),
                                    FormatCommandHelper.VarcharIn(ParamUpdBy,inspector.UpdatedBy),
                                    isSonoAssgActiveParam,
                                    resultParam
                                }
                });
            bool deactivated = ((OracleDecimal)resultParam.Value).Value == 0;
            bool isSonoAssgActive = ((OracleDecimal)isSonoAssgActiveParam.Value).Value == 1;
            inspector.IsActive = isSonoAssgActive;
            return deactivated;
        }

        public void SaveDefaultInspectorsWorkload(DefaultInspectorWorkLoad defaultInspectorWorkLoad)
        {
            _service.LogNotification(
                string.Format("IDA: изменения нагрузки по умолчанию на инспектора ИФНС. Код СОНО: {0}, " +
                              "максимальная сумма к возмещению: {1}, максимальная сумма к уплате: {2}",
                              defaultInspectorWorkLoad.SonoCode,
                              defaultInspectorWorkLoad.MaxCompenationCapacity,
                              defaultInspectorWorkLoad.MaxPaymentCapacity),
                "InspectorDeclarationAssignmentAdapter.SaveDefaultInspectorsWorkload");
            var executer = new CommandExecuter(_service);
            executer.TryExecute(
                new CommandContext
                    {
                        Text =
                            FormatCommandHelper.PackageProcedure(PackageName, SaveDefaultInspectorsWorkloadProcedureName),
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new List<OracleParameter>
                                {
                                    FormatCommandHelper.VarcharIn(ParamSONOCode, defaultInspectorWorkLoad.SonoCode),
                                    FormatCommandHelper.NumericIn(ParamHasCompensationPermission,
                                                                  defaultInspectorWorkLoad.HasCompensationPermission
                                                                      ? 1
                                                                      : 0),
                                    FormatCommandHelper.DecimalIn(ParamMaxCompenationCapacity,
                                                                  defaultInspectorWorkLoad.MaxCompenationCapacity),
                                    FormatCommandHelper.NumericIn(ParamHasPaymentPermission,
                                                                  defaultInspectorWorkLoad.HasPaymentPermission ? 1 : 0),
                                    FormatCommandHelper.DecimalIn(ParamMaxPaymentCapacity,
                                                                  defaultInspectorWorkLoad.MaxPaymentCapacity),
                                    FormatCommandHelper.VarcharIn(ParamUpdBy, defaultInspectorWorkLoad.UpdatedBy),
                                    FormatCommandHelper.NumericIn(ParamIsActive,
                                                                  defaultInspectorWorkLoad.IsActive ? 1 : 0)
                                }
                    });
        }


        public void DistributeIndividualDeclarations(string sonoCode)
        {
            new CommandExecuter(_service).TryExecute(new CommandContext
                                                         {
                                                             Text =
                                                                 FormatCommandHelper.PackageProcedure(PackageName,
                                                                                                      DistributeIndividualDeclarationsProcedureName),
                                                             Type = CommandType.StoredProcedure,
                                                             Parameters = new List<OracleParameter>
                                                                              {
                                                                                  FormatCommandHelper.VarcharIn(
                                                                                      ParamSONOCode, sonoCode)
                                                                              }
                                                         });
        }

        public InspectorSonoAssignment GetInspector(string sonoCode, string sid)
        {
            Func<ResultCommandHelper, InspectorSonoAssignment> readFunc =
                reader =>
                {
                    if (reader.Read())
                    {
                        return LoadAssignmentInspector(reader);
                    }
                    return new InspectorSonoAssignment();
                };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetInspectorProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter> { 
                        FormatCommandHelper.VarcharIn(ParamSONOCode, sonoCode),
                        FormatCommandHelper.VarcharIn(ParamSid, sid),
                        FormatCommandHelper.Cursor(ParamInspectorCursor)
                    }
                }, readFunc);
        }


        public List<ActualInspectorWorkload> GetActualInspectorsWorkload(string sonoCode)
        {
            Func<ResultCommandHelper, List<ActualInspectorWorkload>> readFunc =
              reader =>
              {
                  var actualInspectorWorkloads = new List<ActualInspectorWorkload>();
                  while (reader.Read())
                  {
                      var workload = new ActualInspectorWorkload
                                         {
                                             SID = reader.GetString("INSPECTOR_SID"),
                                             Payment = reader.GetDecimal("PAYMENT"),
                                             Compensation = reader.GetDecimal("COMPENSATION"),
                                             HasCompensationPermission = reader.GetBool("COMPENSATION_DECL_PERM"),
                                             HasPaymentPermission = reader.GetBool("PAYMENT_DECL_PERM")
                                         };
                      actualInspectorWorkloads.Add(workload);
                  }
                  return actualInspectorWorkloads;
              };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetActualInspectorWorkloadProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter> 
                    { 
                        FormatCommandHelper.VarcharIn(ParamSONOCode, sonoCode),
                        FormatCommandHelper.Cursor(ParamActualWorkloadCusor) 
                    }
                }, readFunc);
        }

        public List<DeclarationForAutoDistribution> GetDeclarationsForAutoDistribution(string sonoCode)
        {
            Func<ResultCommandHelper, List<DeclarationForAutoDistribution>> readFunc = reader =>
            {
                var actualInspectorWorkloads = new List<DeclarationForAutoDistribution>();
                while (reader.Read())
                {
                    var workload = new DeclarationForAutoDistribution
                    {
                        DeclarationId = reader.GetInt64("ID"),
                        Amount = reader.GetDecimal("AMOUNT"),
                        IsCompensation = reader.GetBool("IS_COMPENSATION"),
                        IsPayment = reader.GetBool("IS_PAYMENT"),
                    };
                    actualInspectorWorkloads.Add(workload);
                }
                return actualInspectorWorkloads;
            };
            return new CommandExecuter(_service)
                .TryRead(new CommandContext
                {
                    Text = FormatCommandHelper.PackageProcedure(PackageName, GetDeclarationsForAutoDistributionProcedureName),
                    Type = CommandType.StoredProcedure,
                    Parameters = new List<OracleParameter> 
                    { 
                        FormatCommandHelper.VarcharIn(ParamSONOCode, sonoCode),
                        FormatCommandHelper.Cursor(ParamDeclCursor) 
                    }
                }, readFunc);
        }

        public void AddNonAssignedDeclaration(long declarationId)
        {
            new CommandExecuter(_service).TryExecute(new CommandContext
            {
                Text =
                    FormatCommandHelper.PackageProcedure(PackageName,
                                                         AddNonAssignedDeclarationsProcedureName),
                Type = CommandType.StoredProcedure,
                Parameters = new List<OracleParameter>{ 
                                                         FormatCommandHelper.NumericIn(ParamDeclarationId,declarationId)
                                                      }
            });
        }

        public void AssignDeclaration(long declarationId, string sid)
        {
            new CommandExecuter(_service).TryExecute(new CommandContext
                                                         {
                                                             Text =
                                                                 FormatCommandHelper.PackageProcedure(PackageName,
                                                                                                      AssignDeclarationProcedureName),
                                                             Type = CommandType.StoredProcedure,
                                                             Parameters = new List<OracleParameter>
                                                                              {
                                                                                  FormatCommandHelper.NumericIn(
                                                                                      ParamDeclarationId, declarationId),
                                                                                  FormatCommandHelper.VarcharIn(
                                                                                      ParamSid, sid)
                                                                              }
                                                         });
        }


        private static InspectorSonoAssignment LoadAssignmentInspector(ResultCommandHelper reader)
        {
            var inspector = new InspectorSonoAssignment
            {
                SID = reader.GetString("SID"),
                Name = reader.GetString("NAME"),
                EmployeeNum = reader.GetString("EMPLOYEE_NUM"),
                InsertDate = reader.GetDate("INSERT_DATE"),
                InsertedBy = reader.GetString("INSERTED_BY"),
                UpdatedDate = reader.GetDate("UPDATE_DATE"),
                UpdatedBy = reader.GetString("UPDATED_BY"),
                IsActive = reader.GetBool("IS_ACTIVE"),
                SonoCode = reader.GetString("S_CODE")
            };
            return inspector;
        }


        private static InspectorWorkload LoadWorkLoad(ResultCommandHelper reader)
        {
            var inspectorWorkload = new InspectorWorkload
                                        {
                                            SonoCode = reader.GetString("S_CODE"),
                                            HasCompensationPermission = reader.GetBool("COMPENSATION_DECL_PERM"),
                                            HasPaymentPermission = reader.GetBool("PAYMENT_DECL_PERM"),
                                            MaxPaymentCapacity = reader.GetDecimal("MAX_PAYMENT_CAPACITY"),
                                            MaxCompenationCapacity = reader.GetDecimal("MAX_COMPENSATION_CAPACITY"),
                                            IsPaused = reader.GetBool("IS_PAUSED"),
                                            InsertDate = reader.GetDate("WL_INSERT_DATE"),
                                            InsertedBy = reader.GetString("WL_INSERTED_BY"),
                                            UpdatedDate = reader.GetDate("WL_UPDATE_DATE"),
                                            UpdatedBy = reader.GetString("WL_UPDATED_BY"),
                                            IsActive = reader.GetBool("WL_IS_ACTIVE")
                                        };
            return inspectorWorkload;
        }


        public void ClearNonDistributedDeclarations(string sonoCode)
        {
            new CommandExecuter(_service).TryExecute(new CommandContext
            {
                Text =
                    FormatCommandHelper.PackageProcedure(PackageName,
                                                         ClearNonDistributedDeclarationsProcedureName),
                Type = CommandType.StoredProcedure,
                Parameters = new List<OracleParameter>
                                                                              {
                                                                                  FormatCommandHelper.VarcharIn(
                                                                                      ParamSONOCode, sonoCode)
                                                                              }
            });
        }
    }
}