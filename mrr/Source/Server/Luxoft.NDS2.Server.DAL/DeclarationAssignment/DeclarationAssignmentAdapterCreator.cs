﻿using Luxoft.NDS2.Server.Common.Adapters.Declaration;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    public static class DeclarationAssignmentAdapterCreator
    {
        public static IDeclarationAssignmentAdapter DeclarationAssignmentAdapter(
            this IServiceProvider serviceProvider)
        {
            return new DeclarationAssignmentAdapter(serviceProvider);
        }
    }
}
