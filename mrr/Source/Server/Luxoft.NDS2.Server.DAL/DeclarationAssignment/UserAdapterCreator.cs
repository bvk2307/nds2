﻿using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    public static class UserAdapterCreator
    {
        public static IUserAdapter UserAdapter(this IServiceProvider serviceProvider)
        {
            return new UserAdapter(serviceProvider);
        }
    }
}
