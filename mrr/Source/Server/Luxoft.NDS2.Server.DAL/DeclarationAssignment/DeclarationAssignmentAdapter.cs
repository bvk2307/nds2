﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.Adapters.Declaration;
using Luxoft.NDS2.Server.DAL.CommandExecuters.Oracle;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment.CommandBuilder;

namespace Luxoft.NDS2.Server.DAL.DeclarationAssignment
{
    internal class DeclarationAssignmentAdapter : IDeclarationAssignmentAdapter
    {
        private readonly IServiceProvider _serviceProvider;

        public DeclarationAssignmentAdapter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Выплняет вставку назначения (декларация на инспектора) в БД
        /// </summary>
        /// <param name="declarationKey">Ключ декларации</param>
        /// <param name="assignedBy">Идентификатор администратора назначившего декларацию</param>
        /// <param name="assignedTo">Идентификатор инспектора на которого назначена декларация</param>
        public void Insert(DeclarationSummaryKey declarationKey, long assignedBy, long assignedTo)
        {
            new ResultCommandExecuter(_serviceProvider)
                       .TryExecute(new InsertDeclarationAssignmentCommandBuilder(declarationKey, assignedBy, assignedTo));
        }
    }
}
