﻿using Luxoft.NDS2.Common.Contracts;
using Oracle.DataAccess.Client;
using System;
using System.Data;

namespace Luxoft.NDS2.Server.DAL
{
    public class SingleConnectionFactory : ConnectionFactoryBase
    {
        private OracleConnection _oraConnection;

        private string _connectionConfigKey = null;

        public SingleConnectionFactory(IServiceProvider configurationProvider)
            : base(configurationProvider)
        {
            _connectionConfigKey = Constants.DB_CONFIG_KEY;
        }

        public SingleConnectionFactory(IServiceProvider configurationProvider, string connectionConfigKey)
            : base(configurationProvider)
        {
            _connectionConfigKey = connectionConfigKey;
        }

        protected override OracleConnection CreateOracleConnectionInternal()
        {
            if (_oraConnection == null)
            {
                _oraConnection =
                    new OracleConnection(
                        ConfigurationProvider.GetConfigurationValue(_connectionConfigKey));
                _oraConnection.Open();
            }

            return _oraConnection;
        }

        protected override void DisposeDependencies()
        {
            if (_oraConnection != null)
            {
                if (_oraConnection.State != ConnectionState.Closed)
                {
                    _oraConnection.Close();
                }
                _oraConnection.Dispose();
                _oraConnection = null;
            }
        }
    }
}
