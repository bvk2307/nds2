﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы движения выборки по процессу
    /// </summary>
    public interface ISelectionWorkflowAdapter
    {
       
       /// <summary>
        /// Проверяет возможность перехода из статуса в статус
        /// </summary>
        /// <param name="fromTransition"></param>
        /// <param name="toTransition"></param>
        /// <returns></returns>
        bool HasTransition(int fromTransition, int toTransition);
    }
}
