﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы DAL адаптера, позволяющего
    /// включать/выключать декларации/расхождения в рамках конкретной выборки
    /// </summary>
    public interface ISelectionChecksAdapter
    {
        /// <summary>
        /// Меняет состояние включения декларации и ее расхождений в выборку
        /// </summary>
        /// <param name="requestId">Идентификатор запроса выборки</param>
        /// <param name="declarationId">Идентификатор декларации</param>
        /// <param name="checkState">Признак включения декларации в выборку</param>
        void DeclarationCheckState(long requestId, long declarationId, bool checkState);

        /// <summary>
        /// Меняет состояние включения расхождения и декларации, в котором оно обнаружено, в выборку
        /// </summary>
        /// <param name="requestId">Идентификатор запроса выборки</param>
        /// <param name="discrepancyId">Идентификатор расхождения</param>
        /// <param name="checkState">Признак включения расхождения в выборку</param>
        void DiscrepancyCheckState(long requestId, long discrepancyId, bool checkState);

        void ExcludeDeclarations(long requestId, QueryConditions criteria);

        void IncludeDeclarations(long requestId, QueryConditions criteria);

        void ExcludeDiscrepancies(long requestId, QueryConditions criteria);

        void IncludeDiscrepancies(long requestId, QueryConditions criteria);

        SelectionGroupState GetDeclarationsGroupState(long requestId, QueryConditions criteria);

        SelectionGroupState GetDiscrepanciesGroupState(long requestId, QueryConditions criteria);
    }
}
