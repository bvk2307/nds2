﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Egrn
{
    internal class ULTableAdapter : BaseOracleTableAdapter, IULTableAdapter
    {
        private const string ParamCursor = "pResult";

        private const string QueryPattern = "select * from MV$TAX_PAYER_NAME t where 1=1 and {0}";

        public ULTableAdapter(IServiceProvider service)
            : base(service)
        {
        }

        public List<EgrnULTableRow> Search(FilterExpressionBase filter)
        {
            var query = filter.ToSQL(QueryPattern, PatternProvider());
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(ParamCursor));

            return ExecuteList(
                query.Text.WithCursor(ParamCursor),
                CommandType.Text,
                parameters.ToArray(),
                Read);   
        }

        private static EgrnULTableRow Read(OracleDataReader reader)
        {
            return new EgrnULTableRow
            {
                INN = reader.ReadString(TypeHelper<EgrnULTableRow>.GetMemberName(dto => dto.INN)),
                NP_NAME = reader.ReadString(TypeHelper<EgrnULTableRow>.GetMemberName(dto => dto.NP_NAME))
            };
        }

        private static IQueryPatternProvider PatternProvider()
        {
            return new SimpleQueryPatternProvider("t");
        }
    }
}
