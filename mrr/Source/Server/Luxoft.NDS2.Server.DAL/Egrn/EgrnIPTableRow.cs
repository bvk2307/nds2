﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Filter;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Egrn
{
    public class EgrnIPTableRow
    {
        internal static string QueryPattern = "select * from V_EGRN_IP t where 1=1 and {0}";

        internal static IQueryPatternProvider PatternProvider()
        {
            return new SimpleQueryPatternProvider("t");
        }

        public string INNFL
        {
            get;
            set;
        }

        public string LAST_NAME
        {
            get;
            set;
        }

        public string FIRST_NAME
        {
            get;
            set;
        }

        public string PATRONYMIC
        {
            get;
            set;
        }

        internal static EgrnIPTableRow Read(OracleDataReader reader)
        {
            return new EgrnIPTableRow
            {
                INNFL = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.INNFL)),
                LAST_NAME = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.LAST_NAME)),
                FIRST_NAME = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.FIRST_NAME)),
                PATRONYMIC = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.PATRONYMIC))
            };
        }
    }
}
