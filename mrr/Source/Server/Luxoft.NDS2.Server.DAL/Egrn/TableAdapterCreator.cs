﻿namespace Luxoft.NDS2.Server.DAL.Egrn
{
    public static class TableAdapterCreator
    {
        public static IULTableAdapter ULAdapter(IServiceProvider service)
        {
            return new ULTableAdapter(service);
        }

        public static IIPTableAdapter IPAdapter(IServiceProvider service)
        {
            return new IPTableAdapater(service);
        }
    }
}
