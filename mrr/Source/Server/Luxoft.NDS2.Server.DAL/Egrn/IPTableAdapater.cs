﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Luxoft.NDS2.Server.DAL.Egrn
{
    internal class IPTableAdapater : BaseOracleTableAdapter, IIPTableAdapter
    {
        private const string QueryPattern = "select * from V_EGRN_IP t where 1=1 and {0}";

        private const string ParamCursor = "pResult";

        public IPTableAdapater(IServiceProvider service)
            : base(service)
        {
        }

        public List<EgrnIPTableRow> Search(FilterExpressionBase filter)
        {
            var query = filter.ToSQL(QueryPattern, PatternProvider());
            var parameters = query.Parameters.Select(p => p.Convert()).ToList();
            parameters.Add(FormatCommandHelper.Cursor(ParamCursor));

            return ExecuteList(
                query.Text.WithCursor(ParamCursor),
                CommandType.Text,
                parameters.ToArray(),
                Read);
        }        

        private static IQueryPatternProvider PatternProvider()
        {
            return new SimpleQueryPatternProvider("t");
        }

        private static EgrnIPTableRow Read(OracleDataReader reader)
        {
            return new EgrnIPTableRow
            {
                INNFL = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.INNFL)),
                LAST_NAME = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.LAST_NAME)),
                FIRST_NAME = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.FIRST_NAME)),
                PATRONYMIC = reader.ReadString(TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.PATRONYMIC))
            };
        }
    }
}
