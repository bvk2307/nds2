﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.DAL.Filter;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL.Egrn
{
    public interface IULTableAdapter
    {
        List<EgrnULTableRow> Search(FilterExpressionBase filter);
    }
}
