﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Filter;
using Luxoft.NDS2.Server.DAL.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL.Egrn
{
    public class EgrnULTableRow
    {
        internal static string QueryPattern = "select * from V_EGRN_UL t where 1=1 and {0}";

        internal static IQueryPatternProvider PatternProvider()
        {
            return new SimpleQueryPatternProvider("t");
        }

        public string INN
        {
            get;
            set;
        }

        public string NAME_FULL
        {
            get;
            set;
        }

        internal static EgrnULTableRow Read(OracleDataReader reader)
        {
            return new EgrnULTableRow
            {
                INN = reader.ReadString(TypeHelper<EgrnULTableRow>.GetMemberName(dto => dto.INN)),
                NAME_FULL = reader.ReadString(TypeHelper<EgrnULTableRow>.GetMemberName(dto => dto.NAME_FULL))
            };
        }
    }
}
