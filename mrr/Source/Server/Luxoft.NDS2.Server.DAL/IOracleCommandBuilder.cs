﻿using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.DAL
{
    internal interface IOracleCommandBuilder
    {
        OracleCommand BuildCommand(OracleConnection connection);
    }
}
