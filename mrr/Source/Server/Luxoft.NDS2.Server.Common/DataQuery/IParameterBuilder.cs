﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public interface IParameterBuilder
    {
        /// <summary>
        /// Добавляет параметр в коллекцию
        /// </summary>
        /// <param name="value">Значение параметра</param>
        /// <returns>Имя нового параметра</returns>
        string Add(object value);
    }

    public interface IParameterBuilder<TParam> : IParameterBuilder
    {
        List<TParam> Parameters { get; }
    }
}
