﻿using System;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    /// <summary>
    /// Оборачивает выражение запроса в функцию Upper()
    /// </summary>
    public class UpperCaseNestedQueryPatternProvider : IQueryPatternProvider
    {
        IQueryPatternProvider _queryPatternProvider;

        public UpperCaseNestedQueryPatternProvider(IQueryPatternProvider queryPatternProvider)
        {
            if (queryPatternProvider == null)
                throw new ArgumentNullException("IQueryPatternProvider");
            _queryPatternProvider = queryPatternProvider;
        }
        public string Expression(string field)
        {
            return String.Format("UPPER({0})", _queryPatternProvider.Expression(field));
        }

        public string Pattern(string field)
        {
            return _queryPatternProvider.Pattern(field);
        }
    }
}
