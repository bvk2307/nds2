﻿namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public interface IQualifiedFieldNameBuilder
    {
        string Build();

        string FieldName { get; set; }

        bool IsComplex { get; set; }
    }
}
