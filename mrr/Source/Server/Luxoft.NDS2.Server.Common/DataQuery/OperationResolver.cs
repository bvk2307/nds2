﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    internal abstract class OperationResolver
    {
        public string Build(
            IParameterBuilder paramBuilder,
            IQualifiedFieldNameBuilder fieldNameBuilder,
            object value)
        {
            return string.Format(
                Pattern(value),
                fieldNameBuilder.Build(),
                string.Format("{0}", ParamNeeded(value) ? (paramBuilder.Add(value == null ? DBNull.Value : value)) : string.Empty));
        }

        public string BuildPattern(object value)
        {
            return Pattern(value);
        }

        public bool IsParamNeeded(object value)
        {
            return ParamNeeded(value);
        }

        protected abstract string Pattern(object value = null);

        protected virtual bool ParamNeeded(object value)
        {
            return true;
        }

        public static OperationResolver Resolve(ColumnFilter.FilterComparisionOperator operation)
        {
            switch (operation)
            {
                case ColumnFilter.FilterComparisionOperator.Contains:
                    return new ContainsResolver();
                case ColumnFilter.FilterComparisionOperator.DoesNotContain:
                    return new DoesNotContainResolver();
                case ColumnFilter.FilterComparisionOperator.DoesNotEndWith:
                    return new DoesNotEndWithResolver();
                case ColumnFilter.FilterComparisionOperator.DoesNotMatch:
                    return new DoesNotMatchResolver();
                case ColumnFilter.FilterComparisionOperator.DoesNotStartWith:
                    return new DoesNotStartWithResolver();
                case ColumnFilter.FilterComparisionOperator.EndsWith:
                    return new EndsWithResolver();
                case ColumnFilter.FilterComparisionOperator.Equals:
                    return new EqualsResolver();
                case ColumnFilter.FilterComparisionOperator.GreaterThan:
                    return new GreaterResolver();
                case ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo:
                    return new GreaterOrEqualResolver();
                case ColumnFilter.FilterComparisionOperator.LessThan:
                    return new LesserResolver();
                case ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo:
                    return new LesserOrEqualResolver();
                case ColumnFilter.FilterComparisionOperator.Like:
                    return new ContainsResolver();
                case ColumnFilter.FilterComparisionOperator.Match:
                    return new EqualsResolver();
                case ColumnFilter.FilterComparisionOperator.NotDefinedOperator:
                    return new NotDefinedResolver();
                case ColumnFilter.FilterComparisionOperator.NotEquals:
                    return new NotEqualResolver();
                case ColumnFilter.FilterComparisionOperator.NotLike:
                    return new DoesNotContainResolver();
                case ColumnFilter.FilterComparisionOperator.StartsWith:
                    return new StartsWithResolver();
                case ColumnFilter.FilterComparisionOperator.OneOf:
                    return new OneOfResolver();
            };

            throw new NotSupportedException();
        }
    }

    # region Реализация OperationResolver

    internal class OneOfResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} in ({1})";
        }
    }

    internal class ContainsResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} LIKE '%'||{1}||'%'";
        }
    }

    internal class DoesNotContainResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "( ({0} NOT LIKE '%'||{1}||'%') or ({0} is null) )";
        }
    }

    internal class DoesNotEndWithResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "( ({0} NOT LIKE '%'||{1}) or ({0} is null) )";
        }
    }

    internal class DoesNotMatchResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "( ({0} != '%'||{1}) or ({0} is null) )";
        }
    }

    internal class DoesNotStartWithResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "( ({0} NOT LIKE ''||{1}||'%') or ({0} is null) )";
        }
    }

    internal class EndsWithResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} LIKE '%'||{1}";
        }
    }

    internal class EqualsResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            if (value == null || value == DBNull.Value)
            {
                return "{0} IS NULL";
            }

            return "{0} = {1}";
        }

        protected override bool ParamNeeded(object value)
        {
            return value != null && value != DBNull.Value;
        }
    }

    internal class GreaterResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} > {1}";
        }
    }

    internal class GreaterOrEqualResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} >= {1}";
        }
    }

    internal class LesserResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} < {1}";
        }
    }

    internal class LesserOrEqualResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} <= {1}";
        }
    }

    internal class MatchResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} = '%'||{1}";
        }
    }

    internal class NotDefinedResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} IS NULL";
        }
    }

    internal class NotEqualResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            if (value == null || value == DBNull.Value)
            {
                return "{0} IS NOT NULL";
            }

            return "( {0} != {1} OR {0} IS NULL )";
        }

        protected override bool ParamNeeded(object value)
        {
            return value != null && value != DBNull.Value;
        }
    }

    internal class StartsWithResolver : OperationResolver
    {
        protected override string Pattern(object value = null)
        {
            return "{0} LIKE ''||{1}||'%'";
        }
    }

    # endregion
}
