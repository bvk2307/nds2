﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public abstract class BitFilterExpressionBase : FilterExpressionBase
    {
        protected abstract string GetQueryPattern();
        
        private readonly string _field;

        private readonly List<object> _values = new List<object>();

        internal BitFilterExpressionBase(string field, IEnumerable<object> values)
        {
            _field = field;
            _values.AddRange(values);
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            if (!_values.Any())
            {
                return "0=null";
            }

            var bit = _values.Sum(x => long.Parse(x.ToString()));

            return string.Format(
                patternProvider.Pattern(_field),
                string.Format(
                    GetQueryPattern(),
                    patternProvider.Expression(_field),
                    bit));
        }
    }

    public sealed class BitFilterExpression : BitFilterExpressionBase
    {
        public BitFilterExpression(string field, IEnumerable<object> values) : base(field, values) {}

        protected override string GetQueryPattern()
        {
            return "{0} = {1}";
        }
    }

    public sealed class BitNegativeFilterExpression : BitFilterExpressionBase
    {
        public BitNegativeFilterExpression(string field, IEnumerable<object> values) : base(field, values) { }

        protected override string GetQueryPattern()
        {
            return "{0} != {1}";
        }
    }

    public sealed class BitOneOfFilterExpression : BitFilterExpressionBase
    {
        public BitOneOfFilterExpression(string field, IEnumerable<object> values) : base(field, values) { }

        protected override string GetQueryPattern()
        {
            return "NVL(BITAND({0}, {1}), 0) > 0";
        }
    }

    public sealed class BitInListFilterExpression : BitFilterExpressionBase
    {
        public BitInListFilterExpression(string field, IEnumerable<object> values) : base(field, values) { }

        protected override string GetQueryPattern()
        {
            return "NVL(BITAND({0}, {1}), 0) = {0}";
        }
    }

    public sealed class BitNotInListFilterExpression : BitFilterExpressionBase
    {
        public BitNotInListFilterExpression(string field, IEnumerable<object> values) : base(field, values) { }

        protected override string GetQueryPattern()
        {
            return "NVL(BITAND({0}, {1}), 0) != {1}";
        }
    }
}
