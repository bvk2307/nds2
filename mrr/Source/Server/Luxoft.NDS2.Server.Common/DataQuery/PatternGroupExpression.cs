﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class PatternGroupExpression : FilterExpressionBase
    {
        private readonly string _field;

        private readonly List<object> _values = new List<object>();

        public PatternGroupExpression(string field, IEnumerable<object> values)
        {
            _field = field;
            _values.AddRange(values);
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            if (!_values.Any())
            {
                return "0=null";
            }

            var result = string.Join(" OR", _values.Select(x => patternProvider.Pattern(string.Format("{0}_value_{1}", _field, x))));

            return string.Format("({0})", result);
        }
    }
}
