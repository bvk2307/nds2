﻿namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public interface IQueryPatternProvider
    {
        /// <summary>
        /// Возвращает строку-шаблон в которую должен быть включено выражение WHERE для конкретного поля
        /// </summary>
        /// <param name="field">Имя поля</param>
        /// <returns>Шаблон</returns>
        string Pattern(string field);

        /// <summary>
        /// Возвращает выражение для конкретного поля, с которым оно используется в WHERE условии
        /// </summary>
        /// <param name="field"Имя поля></param>
        /// <returns>Выражение</returns>
        string Expression(string field);
    }
}
