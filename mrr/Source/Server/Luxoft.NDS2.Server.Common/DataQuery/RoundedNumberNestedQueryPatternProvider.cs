﻿using System;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    /// <summary>
    /// Оборачивает выражение запроса в функцию округления с заданным коэффициентом округления ROUND(). 
    /// </summary>
    public class RoundedNumberNestedQueryPatternProvider : IQueryPatternProvider
    {
        private readonly int _roundFactor;
        private readonly IQueryPatternProvider _queryPatternProvider;

        public RoundedNumberNestedQueryPatternProvider(IQueryPatternProvider queryPatternProvider, int roundFactor)
        {
            if (queryPatternProvider == null)
                throw new ArgumentNullException("QueryPatternProvider");

            _queryPatternProvider = queryPatternProvider;
            _roundFactor = roundFactor;
        }

        public string Expression(string field)
        {
            return String.Format("ROUND({0},{1})", _queryPatternProvider.Expression(field), _roundFactor);
        }

        public string Pattern(string field)
        {
            return _queryPatternProvider.Pattern(field);
        }
    }
}
