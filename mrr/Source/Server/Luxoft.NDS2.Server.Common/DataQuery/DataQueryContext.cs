﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class DataQueryContext
    {
        public FilterExpressionBase Where
        {
            get;
            set;
        }

        public List<ColumnSort> OrderBy
        {
            get;
            set;
        }

        public int Skip
        {
            get;
            set;
        }

        public int MaxQuantity
        {
            get;
            set;
        }
    }
}
