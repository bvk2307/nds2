﻿namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class SimpleQueryPatternProvider : IQueryPatternProvider
    {
        private const string ExpressionPattern = "{0}.{1}";

        public SimpleQueryPatternProvider(string viewAlias)
        {
            ViewAlias = viewAlias;
        }

        protected string ViewAlias
        {
            get;
            private set;
        }

        public virtual string Pattern(string field)
        {
            return "{0}";
        }

        public virtual string Expression(string field)
        {
            return string.Format(ExpressionPattern, ViewAlias, field);
        }
    }
}
