﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public abstract class ListFilterExpressionBase<T> : FilterExpressionBase
    {
        protected abstract string GetQueryPattern();

        private const string CommaSeparator = ",";

        private readonly string _field;

        private readonly List<T> _values = new List<T>();

        internal ListFilterExpressionBase(string field, IEnumerable<T> values)
        {
            _field = field;
            _values.AddRange(values);
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            if (!_values.Any())
            {
                return "0=null";
            }

            var paramList = _values.Select(x => paramBuilder.Add(x)).ToList();

            return string.Format(
                patternProvider.Pattern(_field),
                string.Format(
                    GetQueryPattern(),
                    patternProvider.Expression(_field),
                    string.Join(
                        CommaSeparator,
                        paramList.Select(x => string.Format("{0}", x)).ToArray())));
        }
    }

    public sealed class IntInListFilterExpression : ListFilterExpressionBase<int>
    {
        public IntInListFilterExpression(string field, IEnumerable<int> values)
            : base(field, values)
        {
        }

        protected override string GetQueryPattern()
        {
            return "{0} in ({1})";
        }
    }

    public sealed class InListFilterExpression<T> : ListFilterExpressionBase<T>
    {
        public InListFilterExpression(string field, IEnumerable<T> values) : base(field, values) {}

        protected override string GetQueryPattern()
        {
            return "{0} in ({1})";
        }
    }

    public sealed class NotInListFilterExpression<T> : ListFilterExpressionBase<T>
    {
        public NotInListFilterExpression(string field, IEnumerable<T> values) : base(field, values) { }

        protected override string GetQueryPattern()
        {
            return "( ({0} not in ({1})) or ({0} is null) )";
        }
    }
}
