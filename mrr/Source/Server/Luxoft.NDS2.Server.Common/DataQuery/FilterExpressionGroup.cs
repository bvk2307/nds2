﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class FilterExpressionGroup : FilterExpressionBase
    {
        private readonly List<FilterExpressionBase> _expressions =
            new List<FilterExpressionBase>();

        private readonly bool _isUnion;

        internal FilterExpressionGroup(bool isUnion)
        {
            _isUnion = isUnion;
        }

        public FilterExpressionGroup WithExpression(FilterExpressionBase expression)
        {
            _expressions.Add(expression);

            return this;
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider)
        {
            if (!_expressions.Any())
            {
                return "0=0";
            }

            return string.Format(
                "({0})",
                string.Join(
                        string.Format(" {0} ", _isUnion.ToOperator()),
                        _expressions.Select(ex => ex.ToQuery(paramBuilder, patternProvider)).ToArray()));
        }
    }

    internal static class ExpressionGroupHelper
    {
        public static string ToOperator(this bool isUnion)
        {
            return isUnion ? "or" : "and";
        }
    }
}
