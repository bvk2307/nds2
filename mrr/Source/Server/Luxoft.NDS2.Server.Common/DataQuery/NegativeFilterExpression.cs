﻿namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class NegativeFilterExpression : FilterExpressionBase
    {
        private readonly FilterExpressionBase _filterExpression;

        internal NegativeFilterExpression(
            FilterExpressionBase filterExpression)
        {
            _filterExpression = filterExpression;
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            return string.Format("not ({0})", _filterExpression.ToQuery(paramBuilder, patternProvider));
        }
    }
}
