﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public static class FilterExpressionCreator
    {
        public static FilterExpression Create(
            string field,
            ColumnFilter.FilterComparisionOperator comparisonOperator,
            object value)
        {
            return new FilterExpression(field, comparisonOperator, value);
        }

        public static FilterExpressionGroup CreateGroup()
        {
            return new FilterExpressionGroup(false);
        }

        public static FilterExpressionGroup CreateUnion()
        {
            return new FilterExpressionGroup(true);
        }

        public static FilterExpressionBase CreateInList<T>(string field, IEnumerable<T> listValues)
        {
            return new InListFilterExpression<T>(field, listValues);
        }

        public static FilterExpressionBase CreateNotInList<T>(string field, IEnumerable<T> listValues)
        {
            return new NotInListFilterExpression<T>(field, listValues);
        }

        public static NegativeFilterExpression CreateNot(FilterExpressionBase filterExpressionBase)
        {
            return new NegativeFilterExpression(filterExpressionBase);
        }

        public static FilterExpressionBase CreateBitand(string field, IEnumerable<object> listValues)
        {
            return new BitFilterExpression(field, listValues);
        }

        public static FilterExpressionBase CreateNegativeBitand(string field, IEnumerable<object> listValues)
        {
            return new BitNegativeFilterExpression(field, listValues);
        }

        public static FilterExpressionBase CreateOneOfBitand(string field, IEnumerable<object> listValues)
        {
            return new BitOneOfFilterExpression(field, listValues);
        }

        public static FilterExpressionBase CreateInListBitand(string field, IEnumerable<object> listValues)
        {
            return new BitInListFilterExpression(field, listValues);
        }

        public static FilterExpressionBase CreateNotInListBitand(string field, IEnumerable<object> listValues)
        {
            return new BitNotInListFilterExpression(field, listValues);
        }

        public static FilterExpressionBase CreateBitFieldFilter(string field, IEnumerable<object> listValues, ColumnFilter.FilterComparisionOperator filterOperator)
        {
            FilterExpressionBase filterExpression = null;

            switch (filterOperator)
            { 
                case ColumnFilter.FilterComparisionOperator.Equals:
                    filterExpression = CreateBitand(field, listValues);
                    break;
                case ColumnFilter.FilterComparisionOperator.NotEquals:
                    filterExpression = CreateNegativeBitand(field, listValues);
                    break;
                case ColumnFilter.FilterComparisionOperator.Contains:
                case ColumnFilter.FilterComparisionOperator.Like:
                    filterExpression = CreateInListBitand(field, listValues);
                    break;
                case ColumnFilter.FilterComparisionOperator.DoesNotContain:
                case ColumnFilter.FilterComparisionOperator.NotLike:
                    filterExpression = CreateNotInListBitand(field, listValues);
                    break;
                case ColumnFilter.FilterComparisionOperator.OneOf:
                    filterExpression = CreateOneOfBitand(field, listValues);
                    break;
            }
            return filterExpression;
        }

        private static readonly string[] BitColumns = { "OperationCodesBit" };

        public static object ToBit(string value)
        {
            int n;
            if (!int.TryParse(value, out n))
                return 0;
            return (ulong)Math.Pow(2, n - 1);
        }

        public static FilterExpressionBase ToFilterExpression(this FilterQuery filter)
        {
            if (BitColumns.Contains(filter.ColumnName) && (filter.Filtering.Count > 0
                && filter.Filtering.All(x => x.Value != DBNull.Value)))
            {
                return CreateBitFieldFilter(filter.ColumnName, filter.Filtering.Select(x => ToBit(x.Value.ToString())),
                    filter.Filtering.First().ComparisonOperator);
            }

            if ((filter.FilterOperator == FilterQuery.FilterLogicalOperator.Or) 
                && (filter.Filtering.Count > 1)
                && filter.Filtering.All(x => x.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                && filter.Filtering.All(x => x.IsCaseSensitive == true) 
                && filter.Filtering.All(x => x.HasRoundFactor == false)
                && (filter.Filtering.All(x => x.Value != DBNull.Value)))
            {
                return new InListFilterExpression<object>(filter.ColumnName, filter.Filtering.Select(x => x.Value));
            }

            var group =
                new FilterExpressionGroup(
                    filter.FilterOperator == FilterQuery.FilterLogicalOperator.Or);

            foreach (var condition in filter.Filtering)
            {
                FilterExpression fe = null;
                
                //Проверки на признаки специальных фильтров
                if (condition.HasRoundFactor)
                    fe = new RoundedDecimalFilterExpression(filter.ColumnName, condition.ComparisonOperator, Convert.ToDecimal(condition.Value), condition.RoundFactor);
                else if (!condition.IsCaseSensitive)
                    fe = new CaseInsensitiveStringFilterExpression(filter.ColumnName, condition.ComparisonOperator, Convert.ToString(condition.Value));

                //Дефолтный фильтр
                if (fe == null)
                    fe = new FilterExpression(filter.ColumnName, condition.ComparisonOperator, condition.Value);

                group.WithExpression(fe);
            }

            return group;
        }

        public static FilterExpressionGroup ToFilterGroup(this IEnumerable<FilterQuery> filter)
        {
            var group = new FilterExpressionGroup(false);
            foreach (var columnFilter in filter)
            {
                group.WithExpression(columnFilter.ToFilterExpression());
            }

            return group;
        }

        public static FilterExpressionGroup ToFilterWithGroupOperator(this IEnumerable<FilterQuery> filter)
        {
            if (ExistsOnlyOneFilter(filter))
                return ToFilterGroup(filter);

            var groupResult = CreateGroup();

            foreach (var itemGroup in filter.GroupBy(p => p.ColumnName))
            {
                var filterOneNameGroup = filter.Where(p => p.ColumnName == itemGroup.Key);
                if (filterOneNameGroup.Count() == 1)
                {
                    groupResult.WithExpression(filterOneNameGroup.First().ToFilterExpression());
                }
                else
                {
                    var groupAnd = CreateGroup();
                    var filterAnds = filterOneNameGroup.Where(p => p.GroupOperator == FilterQuery.FilterLogicalOperator.And);
                    foreach (var itemFilterAnd in filterAnds)
                    {
                        groupAnd.WithExpression(itemFilterAnd.ToFilterExpression());
                    }

                    var groupOr = CreateUnion();
                    var filterOrs = filterOneNameGroup.Where(p => p.GroupOperator == FilterQuery.FilterLogicalOperator.Or);
                    foreach (var itemFilterOr in filterOrs)
                    {
                        groupOr.WithExpression(itemFilterOr.ToFilterExpression());
                    }

                    if (filterAnds.Any() && !filterOrs.Any())
                        groupResult.WithExpression(groupAnd);
                    if (!filterAnds.Any() && filterOrs.Any())
                        groupResult.WithExpression(groupOr);

                    if (filterAnds.Any() && filterOrs.Any())
                    {
                        var groupOneColumn = CreateUnion();
                        groupOneColumn.WithExpression(groupAnd);
                        groupOneColumn.WithExpression(groupOr);
                        groupResult.WithExpression(groupOneColumn);
                    }
                }
            }

            return groupResult;
        }

        private static bool ExistsOnlyOneFilter(IEnumerable<FilterQuery> filter)
        {
            bool existsOnlyOneFilters = true;
            foreach (var itemGroup in filter.GroupBy(p => p.ColumnName))
            {
                var filterOneNameGroup = filter.Where(p => p.ColumnName == itemGroup.Key);
                if (filterOneNameGroup.Count() > 1)
                {
                    existsOnlyOneFilters = false;
                    break;
                }
            }
            return existsOnlyOneFilters;
        }

        public static DataQueryContext ToDataQueryContext(this QueryConditions queryContext)
        {
            return new DataQueryContext
            {
                MaxQuantity = (int)queryContext.PaginationDetails.RowsToTake.Value,
                OrderBy = queryContext.Sorting,
                Skip = (int)queryContext.PaginationDetails.RowsToSkip,
                Where = queryContext.Filter.ToFilterGroup()
            };
        }
    }
}
