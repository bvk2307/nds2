﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    /// <summary>
    /// Выражение фильтра учитывающее регистр содержимого строки при фильтрации
    /// </summary>
    public class CaseInsensitiveStringFilterExpression : FilterExpression
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="field">Название поля в таблице по которому осуществляется фильтр</param>
        /// <param name="type">Тип операции фильтрации</param>
        /// <param name="value">Значение фильтра</param>
        internal CaseInsensitiveStringFilterExpression(string field, ColumnFilter.FilterComparisionOperator type, string value)
            : base(field, type, value.ToUpper()) { }

        public override string ToQuery(IParameterBuilder paramBuilder, IQueryPatternProvider patternProvider)
        {
            return base.ToQuery(paramBuilder, new UpperCaseNestedQueryPatternProvider(patternProvider));
        }
    }
}
