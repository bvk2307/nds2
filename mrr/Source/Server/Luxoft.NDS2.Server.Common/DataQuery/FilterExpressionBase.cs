﻿namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public abstract class FilterExpressionBase
    {
        public abstract string ToQuery(
            IParameterBuilder paramBuilder, 
            IQueryPatternProvider patternProvider);

        public string ToQuery(
            IParameterBuilder paramBuilder,
            string viewAlias)
        {
            return ToQuery(paramBuilder, new SimpleQueryPatternProvider(viewAlias));
        }
    }
}
