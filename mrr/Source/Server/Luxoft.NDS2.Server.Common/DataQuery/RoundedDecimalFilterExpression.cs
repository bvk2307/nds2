﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    /// <summary>
    /// Выражение фильтра, содержащее округление параметра.
    /// </summary>
    public class RoundedDecimalFilterExpression : FilterExpression
    {
        private readonly int _roundFactor;
        /// <summary>
        /// Контруктор
        /// </summary>
        /// <param name="field">Название поля в таблице по которому осуществляется фильтр</param>
        /// <param name="type">Тип операции фильтрации</param>
        /// <param name="value">Значение фильтра</param>
        /// <param name="roundFactor">Значение признака округления</param>
        internal RoundedDecimalFilterExpression(string field, ColumnFilter.FilterComparisionOperator type, decimal value, int roundFactor) : base(field, type, value)
        {
            _roundFactor = roundFactor;
        }

        public int RoundFactor
        {
            get { return _roundFactor; }
        }

        public override string ToQuery(IParameterBuilder paramBuilder, IQueryPatternProvider patternProvider)
        {
            return base.ToQuery(paramBuilder, new RoundedNumberNestedQueryPatternProvider(patternProvider, _roundFactor));
        }
    }
}
