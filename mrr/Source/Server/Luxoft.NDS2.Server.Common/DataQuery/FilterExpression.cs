﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.Common.DataQuery
{
    public class FilterExpression : FilterExpressionBase
    {
        private readonly object _value;
        private readonly ColumnFilter.FilterComparisionOperator _type;
        private readonly string _field;

        internal FilterExpression(string field, ColumnFilter.FilterComparisionOperator type, object value)
        {
            _field = field;
            _type = type;
            _value = value;
        }

        public override string ToQuery(
            IParameterBuilder paramBuilder,
            IQueryPatternProvider patternProvider)
        {
            var operationResolver = OperationResolver.Resolve(_type);

            string ret = string.Format(
                patternProvider.Pattern(_field),
                string.Format(
                    operationResolver.BuildPattern(_value),
                    patternProvider.Expression(_field),
                    operationResolver.IsParamNeeded(_value) ? string.Format("{0}", paramBuilder.Add(_value)) : string.Empty));
            return ret;
        }

    }
}
