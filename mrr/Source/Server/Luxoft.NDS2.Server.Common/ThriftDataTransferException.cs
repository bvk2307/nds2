﻿using System;

namespace Luxoft.NDS2.Server.Common
{
    public class ThriftDataTransferException : Exception
    {
        public ThriftDataTransferException(Exception innerException)
            : base("Thrift data transfer error", innerException)
        {
        }
    }
}
