﻿using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы сервисного слоя необходимые для работы DAL
    /// </summary>
    public interface IServiceProvider : ILogProvider, IConfigurationProvider, IQueryTemplateProvider
    {
        string CurrentUserSID { get;  }
    }
}
