﻿using System;

namespace Luxoft.NDS2.Server.Common
{
    /// <summary>
    /// Исключение для сигнализации о нарушении прав доступа
    /// </summary>
    public class AccessDeniedException : Exception
    {
        public AccessDeniedException(string message) 
            : base(message)
        {
        }
    }
}
