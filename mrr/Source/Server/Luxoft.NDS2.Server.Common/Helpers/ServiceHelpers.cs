﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Security.Principal;
using System.Threading;
using CommonComponents.Configuration;
using CommonComponents.Directory;
using CommonComponents.Instrumentation;
using CommonComponents.Shared;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.Services.Managers.Query;
using dal = Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Common;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ServiceHelpers : dal.IServiceProvider//, ILogProvider
    {
        public IReadOnlyServiceCollection Services { get; private set; }

        private readonly IInstrumentationService _logService;

        public IIdentity User
        {
            get
            {
                return Thread.CurrentPrincipal.Identity;
            }
        }

#if DEBUG
        /// <summary>
        /// Возвращает ФИО пользователя в домене или в рабочей группе
        /// </summary>
        public string UserDisplayName
        {
            get
            {
                string userName = string.Empty;
                try
                {
                    var userInfoService = Services.Get<IUserInfoService>();
                    if (userInfoService == null)
                        return User.Name;
                    var ap = Services.Get<IAuthorizationProvider>();
                    var userInfo = userInfoService.GetUserInfoBySID(ap.CurrentUserSID);
                    userName = userInfo != null ? userInfo.DisplayName : User.Name;
                }
                catch (ApplicationException) //Ошибка работы с доменом
                {
                    userName = WindowsIdentity.GetCurrent().Name;
                }

                return userName;
            }
        }
#else
        /// <summary>
        /// Возвращает ФИО пользователя в домене
        /// </summary>
        public string UserDisplayName
        {
            get
            {
                var userInfoService = Services.Get<IUserInfoService>();
                if (userInfoService == null)
                    return User.Name;
                var ap = Services.Get<IAuthorizationProvider>();
                var userInfo = userInfoService.GetUserInfoBySID(ap.CurrentUserSID);
                return userInfo != null ? userInfo.DisplayName : User.Name;
            }
        }
#endif

        public ServiceHelpers(IReadOnlyServiceCollection services)
        {
            Services = services;
            _logService = services.Get<IInstrumentationService>();
        }

        public IClassifierService ClassifierService
        {
            get
            {
                string thesAccessConfigProfileName = this.GetConfigurationValue(Constants.THES_ACESS_CONFIG_PROFILE_NAME_KEY);
                if (thesAccessConfigProfileName == null)
                    throw new InvalidOperationException(string.Format("Key: '{0}' is not found in <appSettings> in configuration", Constants.THES_ACESS_CONFIG_PROFILE_NAME_KEY));

                IClassifierServiceFactory classifierServiceFactory = Services.Get<IClassifierServiceFactory>();
                return classifierServiceFactory.GetClassifierService(Services, thesAccessConfigProfileName);

            }
        }

        public string GetQueryText(string scope, string qName)
        {
            return Services.Get<IQueryProvider>().GetQueryText(scope, qName);
        }

        public void LogDebug(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
            Log(message, TraceEventType.Information, InstrumentationLevel.Debug, methodName, null, additionalProperties);
        }

        public void LogNotification(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
            Log(message, TraceEventType.Information, InstrumentationLevel.Troubleshooting, methodName, null, additionalProperties);
        }

        public void LogError(string message, string methodName = null, Exception ex = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
            Log(message, TraceEventType.Error, InstrumentationLevel.Normal, methodName, ex, additionalProperties);
        }

        public void LogWarning(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
            Log(message, TraceEventType.Warning, InstrumentationLevel.Troubleshooting, methodName, null, additionalProperties);
        }

        public void LogTrace(string methodName)
        {
            Log(string.Format("Diagnostic({0})", methodName), TraceEventType.Warning, InstrumentationLevel.Troubleshooting, methodName);
        }

        public string GetConfigurationValue(string key)
        {
            ProfileInfo prof = ProfileInfo.Default;
            AppSettingsSection apps;
            if (Services.Get<IConfigurationDataService>().TryGetSection(prof, out apps))
            {
                return apps.Settings[key].Value;
            }

            return null;
        }

        private void Log(string message, TraceEventType traceEventType, InstrumentationLevel instrumentationLevel, string methodName, Exception ex = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
            IProfilingEntry logEntry = _logService.CreateProfilingEntry(traceEventType, (int)instrumentationLevel, Constants.Instrumentation.SERVER_PROFILE_TYPE_NAME, 100);

            logEntry.MessageText = message;

            AddException(logEntry, ex);

             logEntry.AddMonitoringItem("Method name", methodName);
             if (additionalProperties != null)
             {
                foreach (var o in additionalProperties)
                {
                       logEntry.AddMonitoringItem(o.Key, o.Value == null ? "null" : o.Value.ToString());
                }
             }

             _logService.Write(logEntry);               
        }


        public static void AddException(IProfilingEntry rec, Exception ex, int num = 0)
        {
            if (ex != null)
            {
                rec.AddMonitoringItem(string.Format("Exception{0}:", num), ex);
                AddException(rec, ex.InnerException, num + 1);
            }
        }

        public OperationResult<T> Do<T>(Func<T> work, IEnumerable<string> permissions = null) where T : class
        {
            var result = new OperationResult<T>();

            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    if (!Services.Get<IAuthorizationProvider>().IsOperationEligible(permission))
                    {
                        DenyWork(result, permission);
                        return result;
                    }
                }
            }

	//apopov 25.2.2016	//DEBUG!!!
            //IProfilingEntry profillingEntry = StartTracing( Constants.Instrumentation.SERVICECALL_PROFILING_PROFILE_TYPE_NAME );

            try
            {
                result.Result = work();
                result.Status = ResultStatus.Success;

                //TracePoint( profillingEntry, "ServiceHelpers.Do", new { Mark = "Service call is completed successfully" } );
                //FinishTracing( profillingEntry );
            }
            catch (ThriftDataTransferException thriftEx)
            {
                LogError(thriftEx.Message, "Do", thriftEx);
                result.Status = ResultStatus.Error;
                result.Message = "Ошибка обмена данными с сервисом взаимодействия";
            }
            catch (SovRequestProcessingException sovEx)
            {
                LogError(sovEx.Message, "Do", sovEx);
                result.Status = ResultStatus.Error;
                result.Message = "Ошибка загрузки данных сервиса взаимодействия";
            }
            catch (DatabaseException dbEx)
            {
                string message = dbEx.Message;
                LogError(message, "Do DB", dbEx);
                result.Status = ResultStatus.Error;
                result.Message = message;
            }
            catch (ObjectNotFoundException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.NoDataFound;
                result.Message = exception.Message;
            }
            catch (AccessDeniedException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Denied;
                result.Message = exception.Message;
            }
            catch (Exception exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Error;
                result.Message = exception.Message;
            }

            return result;
        }

        public OperationResult<T> Do<T>(Func<T> work, string permission) where T : class, new()
        {
            return Do(work, new string[] { permission });
        }

        public void DenyWork(IOperationResult result, string permission)
        {
            result.Status = ResultStatus.Denied;
            result.Message = "Отсутствуют права на выполнение функции";
            LogError(string.Format("Отсутствуют права на выполнение функции {0} для пользователя {1}", permission, User.Name));
        }

        public OperationResult Do(Action work, IEnumerable<string> permissions = null)
        {
            var result = new OperationResult();

            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    if (!Services.Get<IAuthorizationProvider>().IsOperationEligible(permission))
                    {
                        DenyWork(result, permission);
                        return result;
                    }
                }
            }

            try
            {
                work();
                result.Status = ResultStatus.Success;
            }
            catch (DatabaseException dbEx)
            {
                LogError(dbEx.InnerException.Message, "Do DB", dbEx.InnerException);
                result.Status = ResultStatus.Error;
                result.Message = dbEx.InnerException.Message;
            }
            catch (ObjectNotFoundException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.NoDataFound;
                result.Message = exception.Message;
            }
            catch (Exception exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Error;
                result.Message = exception.Message;
            }

            return result;
        }

        public OperationResult Do(Action work, string permission)
        {
            return Do(work, new string[] { permission });
        }

        public OperationResult<T> DoEx<T>(Func<T> work, IEnumerable<string> permissions = null) where T : struct
        {
            var result = new OperationResult<T>();

            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    if (!Services.Get<IAuthorizationProvider>().IsOperationEligible(permission))
                    {
                        DenyWork(result, permission);
                        return result;
                    }
                }
            }

            try
            {
                result.Result = work();
                result.Status = ResultStatus.Success;
            }
            catch (DatabaseException dbEx)
            {
                LogError(dbEx.InnerException.Message, "Do DB", dbEx.InnerException);
                result.Status = ResultStatus.Error;
                result.Message = dbEx.InnerException.Message;
            }
            catch (ObjectNotFoundException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.NoDataFound;
                result.Message = exception.Message;
            }
            catch (Exception exception)
            {
                LogError(exception.Message, "DoEx", exception);
                result.Status = ResultStatus.Error;
                result.Message = exception.Message;
            }

            return result;
        }

        public OperationResult<T> DoEx<T>(Func<T> work, string permission) where T : struct
        {
            return DoEx(work, new string[] { permission });
        }

        public void GenerateException(ResultStatus status, string message)
        {
            if (status == ResultStatus.NoDataFound)
            {
                throw new ObjectNotFoundException(message);
            }
            else if (status == ResultStatus.Denied)
            {
                throw new AccessDeniedException(message);
            }
            else if (status == ResultStatus.Error)
            {
                throw new Exception(message);
            }
        }

	//apopov 26.2.2016	//DEBUG!!!
/*
        public IProfilingEntry StartTracing( string profilingMessageTypeName )
        {
            Contract.Ensures( Contract.Result<IProfilingEntry>() != null );

            IProfilingEntry profilingEntry = _logService.CreateProfilingEntry( TraceEventType.Verbose,  //Information, 
                //(int)InstrumentationLevel.Troubleshooting,
                (int)InstrumentationLevel.Debug, 
                profilingMessageTypeName, messagePriority: 0 );

            return profilingEntry;
        }

        public void TracePoint( IProfilingEntry profilingEntry, string pointName, object pointDetails, string pointDeclaration = null )
        {
            Contract.Requires( profilingEntry != null );
            Contract.Requires( !string.IsNullOrWhiteSpace( pointName ) );
            Contract.Requires( pointDetails != null );

            ProfilingEntryHelper.CreateCheckpoint( profilingEntry, pointName, TraceEventType.Information, pointDeclaration, pointDetails );
        }

        public void FinishTracing( IProfilingEntry profilingEntry )
        {
            Contract.Requires( profilingEntry != null );

            _logService.Write( profilingEntry );
        }
*/

        public string CurrentUserSID
        {
            get
            {
                return Services.Get<IAuthorizationProvider>().CurrentUserSID;
            }
        }
    }
}