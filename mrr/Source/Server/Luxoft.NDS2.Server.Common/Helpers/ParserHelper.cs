﻿using System;

namespace Luxoft.NDS2.Server.Common.Helpers
{
    public static class ParserHelper
    {
        public static bool TryParseDataTime(string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;

            DateTime dtParse = DateTime.Now;
            if (DateTime.TryParse(value, out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }
            else
            {
                string[] formats = { "dd.MM.yyyy", "yyyyMMdd", "dd/MM/yyyy" };

                if (DateTime.TryParseExact(
                    value,
                    formats,
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.DateTimeStyles.None,
                    out dtParse))
                {
                    resultValue = dtParse;
                    ret = true;
                }
            }

            return ret;
        }

        public static DateTime? ParseDataTime(string value)
        {
            DateTime? result = null;
            DateTime dtParse = DateTime.Now;
            if (ParserHelper.TryParseDataTime(value, out dtParse))
            {
                result = dtParse;
            }
            return result;
        }

        public static int? ParseDataTimeToInteger(string value)
        {
            int? result = null;
            DateTime dtParse = DateTime.Now;
            if (ParserHelper.TryParseDataTime(value, out dtParse))
            {
                result = dtParse.Year * 10000 + dtParse.Month * 100 + dtParse.Day;
            }
            return result;
        }
    }
}
