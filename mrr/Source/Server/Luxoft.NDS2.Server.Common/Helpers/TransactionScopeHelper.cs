﻿using System.Transactions;

namespace Luxoft.NDS2.Server.Common.Helpers
{
    public static class TransactionScopeHelper
    {
        public static TransactionScope DefaultTransactionScope()
        {
            return new TransactionScope(
                TransactionScopeOption.RequiresNew,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted });
        }
    }
}
