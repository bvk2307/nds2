﻿using System.Collections.Generic;
using CommonComponents.ThesAccess;

namespace Luxoft.NDS2.Server.Common.Cache.Extensions
{
    /// <summary> Extension methods for <see cref="IDataItem"/> adding to methods in <see cref="Utils"/>. </summary>
    public static class DataItemExtensions
    {
        /// <summary> Gets POCO instance from <paramref name="dataItem"/> or throws <see cref="KeyNotFoundException"/> if <paramref name="dataItem"/> is not POCO data item (see in <see cref="PocoClassifierProvider{TItem}"/>. </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        public static TItem GetPocoItemValue<TItem>( this IDataItem dataItem )
        {
            TItem poco = dataItem.GetValue<TItem>( PocoClassifierProvider<TItem>.PocoPropertyName );

            return poco;
        }
    }
}