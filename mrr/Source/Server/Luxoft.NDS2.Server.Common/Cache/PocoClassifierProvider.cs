﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using CommonComponents.ThesAccess;
using CommonComponents.ThesAccess.Providers;

namespace Luxoft.NDS2.Server.Common.Cache
{
    /// <summary> A server cache data provider for data items of type <typeparamref name="TItem"/> only. </summary>
    /// <typeparam name="TItem"></typeparam>
    public abstract class PocoClassifierProvider<TItem> : ProviderAbstractImpl
    {
        internal const string PocoPropertyName = "Poco";

        /// <summary> Закрытый конструктор. </summary>
        /// <param name="serviceProvider"> Коллекция доступных сервисов. </param>
        protected PocoClassifierProvider( IServiceProvider serviceProvider ) : base( serviceProvider ) { }

        /*
                #region Override Members

                #region Public
        */
        /// <summary>
        /// Проверяет есть ли у текущего пользователя права для работы
        /// с указанным классификатором.
        /// </summary>
        /// <param name="context">Контекст выполнения запроса.</param>
        /// <param name="classifierName">
        /// Наименование классификатора, права для доступа к которому требуется проверить.
        /// </param>
        /// <returns>
        /// Если у пользователя есть права для работы с указанным классификтором - true.
        /// Если у пользователя нет прав для работы с указанным классификтором  - false.
        /// </returns>
        public override bool CheckPermission( object context, string classifierName )
        {
            Contract.Requires( context != null );
            Contract.Requires( !String.IsNullOrEmpty( classifierName ) );

            return true;

	//apopov 7.11.2016	//TODO!!!   //remove or use
            /*
                        string namedConfig = this.ContextToNamedConfig(context);

                        if (String.IsNullOrEmpty(namedConfig))
                            throw new InvalidOperationException();


                        // получаем метаданные классификатора
                        SqlUniproviderClassifierElement meta = this.GetClassifierMeta(namedConfig, classifierName);

                        string permission = meta.Permission;   //Авторизационный контекст классификатора

                        if (String.IsNullOrEmpty(permission))
                        {//Авторизационный контекст именованной конфигурации

                            SqlUniproviderNamedConfigElement namedConfigElement =
                                this.GetSqlUniproviderNamedConfigElement(namedConfig);

                            permission = namedConfigElement.Permission;
                        }

                        if (String.IsNullOrEmpty(permission))
                        {
                            throw new InvalidOperationException(
                                String.Format(
                                    "В конфигурациом профиле не указан авторизационный контекст для классификатора \"{0}\" (NamedConfig: \"{1}\")",
                                    classifierName,
                                    namedConfig));
                        }

                        return this.Authorize(permission);
            */
        }

        /// <summary>
        /// Проверить наличие данных в справочнике.
        /// </summary>
        /// <param name="request">Объект, определяющий запрос к справочнику.</param>
        /// <param name="namedConfig">Наименование применяемой конфигурации.</param>
        /// <returns>Если в справочнике присутствуют данные, отвечающие запросу - true.</returns>
        protected override bool Exist( IRequest request, string namedConfig )
        {
            Contract.Requires( !String.IsNullOrEmpty( namedConfig ) );
            Contract.Requires( request != null );

            throw new NotImplementedException( string.Format( "Exist() is not implemented yet in {0}. Implement it like LookUp() if it is needed.", GetType().FullName ) );
        }

        /// <summary> Возвращает выборку данных по запросу. </summary>
        /// <param name="request">Объект, определяющий запрос к справочнику.</param>
        /// <param name="namedConfig">Наименование применяемой конфигурации.</param>
        /// <returns>Выборка данных, соответствующая указанному запросу.</returns>
        protected override ICollection<IDataItem> LookUp( IRequest request, string namedConfig )
        {
            Contract.Requires( !String.IsNullOrEmpty( namedConfig ) );
            Contract.Requires( request != null );
            Contract.Ensures( Contract.Result<ICollection<IDataItem>>() != null );

            var items    = OnLookUp( request, namedConfig );
            DataItem[] dataItems                = new DataItem[items.Count];
            Property<TItem> propertyItem;
            DataItem dataItem;
            int nI = -1;
            foreach ( TItem item in items )
            {
                propertyItem = new Property<TItem>( PocoPropertyName, item );
                dataItem = new DataItem { ID = ++nI, PropertyLists = new Dictionary<string, IProperty>( 1 ) };
                dataItem.PropertyLists.Add( propertyItem.Name, propertyItem );
                dataItems[nI] =  dataItem;
            }

            return dataItems;
        }

        protected abstract IReadOnlyCollection<TItem> OnLookUp( IRequest request, string namedConfig );

        /// <summary> Получить доступ к потоковым данным. </summary>
        /// <param name="request">Объект, определяющий запрос к справочнику.</param>
        /// <param name="namedConfig">Наименование применяемой конфигурации.</param>
        /// <returns>Считыватель потоковых данных.</returns>
        protected override IItemReader<IDataItem> LookUpReader( IRequest request, string namedConfig )
        {
            throw new NotImplementedException( string.Concat( "Method ProviderAbstractImpl.LookUpReader() is not supported by: ", this.GetType().FullName ) );
        }

        /// <summary> Инициализировать провайдер данных. </summary>
        /// <param name="parameters">
        /// Параметры инициализации провайдера.
        ///     Key - наименование параметра.
        ///     Value - значение параметра.
        /// </param>
        protected override void Init( Dictionary<string, string> parameters )
        {
            Contract.Requires( parameters != null );
        }
    }
}