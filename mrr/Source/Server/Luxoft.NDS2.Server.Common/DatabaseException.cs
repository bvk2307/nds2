﻿using System;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот класс представляет абстрактное исключение уровня БД
    /// </summary>
    public class DatabaseException : Exception
    {
        public DatabaseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
