﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public class ReclaimDeclaration
    {
        /// <summary>
        /// Идентификатор корректировки
        /// </summary>
        public long Zip { get; set; }

        /// <summary>
        /// рег.номер
        /// </summary>
        public long RegNumber { get; set; }

        /// <summary>
        /// Код налогового органа
        /// </summary>
        public string SonoCode { get; set; }

        /// <summary>
        /// Код отчетного периода
        /// </summary>
        public string PeriodCode { get; set; }

        /// <summary>
        /// Год отчетного периода
        /// </summary>
        public string FiscalYear { get; set; }

        /// <summary>
        /// ИНН налогаплательщика
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// КПП налогаплательщика
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// КПП эффективный налогаплательщика
        /// </summary>
        public string KppEffective { get; set; }
    }
}
