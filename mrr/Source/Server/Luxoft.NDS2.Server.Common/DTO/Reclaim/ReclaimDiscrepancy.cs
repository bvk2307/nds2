﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public class ReclaimDiscrepancy
    {
        #region Атрибуты Декларации


        /// <summary>
        /// Идентификатор корректировки
        /// </summary>
        public long Zip { get; set; }

        /// <summary>
        /// рег.номер
        /// </summary>
        public long? RegNumber { get; set; }

        /// <summary>
        /// Код налогового органа
        /// </summary>
        public string SonoCode { get; set; }

        /// <summary>
        /// ИНН налогаплательщика
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// КПП налогаплательщика
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// КПП эффективный налогаплательщика
        /// </summary>
        public string KppEffective { get; set; }

        #endregion

        #region Атрибуты расхождения

        /// <summary>
        /// Идентификатор расхождения
        /// </summary>
        public long DiscrepancyId { get; set; }

        /// <summary>
        /// Тип расхождения
        /// </summary>
        public DiscrepancyType Type { get; set; }

        /// <summary>
        /// Сумма расхождения
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Сумма расхождения ПВП
        /// </summary>
        public decimal? AmountPvp { get; set; }

        /// <summary>
        /// Номер правила сопоставления
        /// </summary>
        public int? RuleNum { get; set; }

        /// <summary>
        /// ИНН покупателя
        /// </summary>
        public string BuyerInn { get; set; }

        /// <summary>
        /// ИНН продавца
        /// </summary>
        public string SellerInn { get; set; }

        /// <summary>
        /// Код отчетного периода
        /// </summary>
        public string PeriodCode { get; set; }

        /// <summary>
        /// год отчетного периода
        /// </summary>
        public string FiscalYear { get; set; }

        /// <summary>
        /// Эффективный КПП покупателя
        /// </summary>
        public string BuyerKppEffective { get; set; }

        /// <summary>
        /// Эффективный КПП продавца
        /// </summary>
        public string SellerKppEffective { get; set; }

        /// <summary>
        /// ИНН контрагента
        /// </summary>
        public string ContractorInn { get; set; }

        /// <summary>
        /// Эффективный КПП контрагента
        /// </summary>
        public string ContractorKppEffective { get; set; }

        #endregion

        #region Атрибуты СФ

        /// <summary>
        /// Номер СФ
        /// </summary>
        public string InvoiceNum { get; set; }

        /// <summary>
        /// Дата СФ
        /// </summary>
        public DateTime? InvoiceDate { get; set; }

        /// <summary>
        /// Номер корректировочного счета-фактуры продавца
        /// </summary>
        public string CorrectionNum { get; set; }

        /// <summary>
        /// Дата корректировочного счета-фактуры продавца
        /// </summary>
        public DateTime? CorrectionDate { get; set; }

        /// <summary>
        /// Идентификатор счет-фактуры
        /// </summary>
        public string RowKey { get; set; }

        /// <summary>
        /// Идентификатор агрегирующей записи в Hbase
        /// </summary>
        public string ActualRowKey { get; set; }

        #endregion
    }
}
