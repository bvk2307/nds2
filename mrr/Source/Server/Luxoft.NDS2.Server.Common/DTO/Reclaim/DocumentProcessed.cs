﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public enum DocumentProcessed
    {
        NotProcessed = 0,
        Processed = 1
    }
}
