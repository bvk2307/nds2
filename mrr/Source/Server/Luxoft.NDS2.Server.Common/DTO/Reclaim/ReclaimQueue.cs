﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public class ReclaimQueue
    {
        /// <summary>
        /// Идентификатор элемента очереди
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор АТ или АИ
        /// </summary>
        public long DocId { get; set; }

        /// <summary>
        /// Сторона отработки
        /// </summary>
        public int Side { get; set; }

        /// <summary>
        /// Состояние элемента очереди
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Состояние захвата элемента очереди
        /// </summary>
        public int IsLocked { get; set; }

        /// <summary>
        /// Дата создания элемента очереди
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
