﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public enum ReclaimLocked
    {
        NotLocked = 0,
        Locked = 1,
        Processed = 2
    }
}
