﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public class ReclaimDocument
    {
        /// <summary>
        /// Идентификатор документа истребования
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Рег.номер истребования
        /// </summary>
        public long RegNumber { get; set; }

        /// <summary>
        /// Тип документа истребования
        /// </summary>
        public int DocType { get; set; }

        /// <summary>
        /// Вид документа истребования
        /// </summary>
        public int DocKind { get; set; }

        /// <summary>
        /// Код налоговаого органа
        /// </summary>
        public int SonoCode { get; set; }

        /// <summary>
        /// Кол-во расхождений включенных в документ
        /// </summary>
        public long DiscrepancyCount { get; set; }

        /// <summary>
        /// Сумма расхождений включенных в документ
        /// </summary>
        public decimal DiscrepancyAmount { get; set; }

        /// <summary>
        /// Этап документа
        /// </summary>
        public int Stage { get; set; }

        /// <summary>
        /// Комментарий пользователя
        /// </summary>
        public string userComment { get; set; }

        /// <summary>
        /// Статус отработки
        /// </summary>
        public int Proccessed { get; set; }

        /// <summary>
        /// ИНН налогоплателщика
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// КПП эффективный
        /// </summary>
        public string KppEffective { get; set; }
    }
}
