﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Reclaim
{
    public enum ReclaimQueueState
    {
        NotProcessed = 1, 
        InProcess = 2, 
        Processed = 3
    }
}
