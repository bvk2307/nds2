﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.CFG
{
    public class CsudOperationSono
    {
        public int OpearationId { get;set; }
        public string GroupSono { get; set; }
        public string ChildSono { get; set; }
    }
}
