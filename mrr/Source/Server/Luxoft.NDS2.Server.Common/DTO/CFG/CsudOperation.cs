﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.CFG
{
    public class CsudOperation
    {
        public long ID;
        public string Name;
        public string MasterName;
    }
}
