﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Declaration
{
    public class DeclarationZipCorrectionNumber
    {
        /// <summary>
        /// Zip декларации
        /// </summary>
        public long Zip { get; set; }

        /// <summary>
        /// Номер корректировки
        /// </summary>
        public int CorrectionNumber { get; set; }
    }
}
