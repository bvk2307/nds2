﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO.Declaration
{
    public class DeclarationChapterActualZip
    {
        /// <summary>
        /// Активный Zip декларации для раздела 8
        /// </summary>
        public long? ActualZipChapterEight { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 8.1
        /// </summary>
        public long? ActualZipChapterEightAddSheet { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 9
        /// </summary>
        public long? ActualZipChapterNine { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 9.1
        /// </summary>
        public long? ActualZipChapterNineAddSheet { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 10
        /// </summary>
        public long? ActualZipChapterTen { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 11
        /// Zip декларации
        /// </summary>
        public long? ActualZipChapterEleven { get; set; }

        /// <summary>
        /// Активный Zip декларации для раздела 12
        /// </summary>
        public long? ActualZipChapterTwelve { get; set; }
    }
}
