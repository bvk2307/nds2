﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Common.DTO
{
    public class ImpalaOption
    {
        public int PartitionsTotal { get; set; }
        public bool Usable { get; set; }
    }
}
