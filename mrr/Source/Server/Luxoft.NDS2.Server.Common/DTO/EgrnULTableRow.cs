﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.Common.DTO
{
    public class EgrnULTableRow
    {
        public string INN
        {
            get;
            set;
        }

        public string NP_NAME
        {
            get;
            set;
        }
    }
}
