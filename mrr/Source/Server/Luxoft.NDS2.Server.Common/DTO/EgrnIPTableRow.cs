﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.Common.DTO
{
    public class EgrnIPTableRow
    {
        public string INNFL
        {
            get;
            set;
        }

        public string LAST_NAME
        {
            get;
            set;
        }

        public string FIRST_NAME
        {
            get;
            set;
        }

        public string PATRONYMIC
        {
            get;
            set;
        }
    }
}
