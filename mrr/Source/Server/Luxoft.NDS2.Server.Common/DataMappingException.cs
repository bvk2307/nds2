﻿using System;

namespace Luxoft.NDS2.Server.Common
{
    public class DataMappingException : Exception
    {
        public DataMappingException(string message)
            : base(message)
        {
        }

        public DataMappingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
