﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface IHiveRequestAdapter 
    {
        long GetRequestId(out int status);

        int GetRequestStatus(long requestId);
    }
}
