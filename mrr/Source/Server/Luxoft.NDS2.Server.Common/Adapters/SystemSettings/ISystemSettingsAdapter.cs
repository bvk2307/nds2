﻿
namespace Luxoft.NDS2.Server.Common.Adapters.SystemSettings
{
    /// <summary>
    /// Этот интрефейс описывает методы работы с параметрами АСК НДС-2
    /// </summary>
    public interface ISystemSettingsAdapter
    {
       /// <summary>
       /// Загружает параметров АСК НДС-2
       /// </summary>
       /// <returns></returns>
        NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings Load();

        /// <summary>
        /// Сохраняет параметры АСК НДС-2
        /// </summary>
        /// <param name="claimResendingTimeout">Время ожидания ответа от СЭОД</param>
        /// <param name="resendingAttempts">количество повторных отправок</param>
        /// <param name="claimDeliveryTimeout">Время ожидания вручения Ат/АИ</param>
        /// <param name="claimExplainTimeout">Время ожидания ответа на АТ </param>
        /// <param name="reclaimReplyTimeout">Время ожидания ответа на АИ</param>
        /// <param name="replyEntry">Время ожидания ввода ответа на АИ</param>
        void Save(int claimResendingTimeout,
            int resendingAttempts,
            int claimDeliveryTimeout,
            int claimExplainTimeout,
            int reclaimReplyTimeout,
            int replyEntry);
    }
}
