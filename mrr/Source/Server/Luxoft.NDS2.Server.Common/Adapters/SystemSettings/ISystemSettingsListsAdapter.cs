﻿
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;

namespace Luxoft.NDS2.Server.Common.Adapters.SystemSettings
{
    /// <summary>
    /// Этот интрефейс описывает методы работы с параметрами АСК НДС-2
    /// </summary>
    public interface ISystemSettingsListsAdapter
    {
        /// <summary>
        /// Загружает список ИНН
        /// </summary>
        /// <returns></returns>
        string[] LoadInnList(long id);

        /// <summary>
        /// Сохраняет список 
        /// </summary>
        /// <param name="list">список</param>
        void Save(NDS2.Common.Contracts.DTO.SystemSettings.SystemSettingsList list);
        
        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <param name="activate">активировать или деактивировать</param>
        /// <returns></returns>
        void Update(long id, bool activate);

        /// <summary>
        /// Возвращает все системные списки
        /// </summary>
        /// <returns></returns>
        List<NDS2.Common.Contracts.DTO.SystemSettings.SystemSettingsList> All(SettingsListType type);
    }
}
