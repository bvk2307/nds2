﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface ISovInvoiceRequestAdapter
    {
        SovInvoiceRequest SearchById(long requestId);

        List<SovInvoiceRequest> SearchByDeclaration(long declarationId, int partition);
    }
}
