﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IContragentViewAdapter
    {
        /// <summary>
        /// Возвращает постраничный список контрагентов
        /// </summary>
        /// <param name="qc">объект фильтров и сортировки</param>
        /// <returns>страницу списка контрагентов</returns>
        PageResult<ContragentSummary> SearchData(QueryConditions qc);

        PageResult<ContragentSummary> SearchCount(QueryConditions qc);
    }
}
