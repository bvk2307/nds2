﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDiscrepancyDocumentInvoiceCalculateAdapter
    {
        List<DiscrepancyDocumentInvoice> Search(DataQueryContext queryContext);
    }
}
