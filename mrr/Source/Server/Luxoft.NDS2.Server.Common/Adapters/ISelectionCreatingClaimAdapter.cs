﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface ISelectionCreatingClaimAdapter
    {
        /// <summary>
        /// Проверяет наличие выборок в статусе "Согласовано" в списке выборок для активирования кнопки "Сформриовать АТ"
        /// </summary>
        bool ExistApprovedSelections();

        /// <summary>
        /// Подсчитывает количество выборок в статусах "Согласовано" и "На согласовании" в списке выборок для заполнения всплывающего сообщения
        /// </summary>
        SelectionByStatusesCount CountApprovedSelections();
        
        /// <summary>
        /// Переводит выборки из статуса "Согласовано" в статус "Формирование АТ" 
        /// </summary>
        void SetClaimCreatingStatus();
    }
}
