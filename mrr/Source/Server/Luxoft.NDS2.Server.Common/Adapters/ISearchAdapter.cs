﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface ISearchAdapter<TDto>
    {
        IEnumerable<TDto> Search(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> sortBy,
            uint rowsToSkip,
            uint rowsToTake);

        int Count(FilterExpressionBase filterBy);
    }
}
