﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface IIPTableAdapter
    {
        List<EgrnIPTableRow> Search(FilterExpressionBase filter);
    }
}
