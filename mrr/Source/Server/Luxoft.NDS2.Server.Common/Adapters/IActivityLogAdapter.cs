﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IActivityLogAdapter
    {
        void Write(ActivityLogEntry entry, string sid);
        List<ActivityReportSummary> GetReportData(DateTime date);
        List<long> GetUserCount(DateTime date);
    }
}