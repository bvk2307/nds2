﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IAutoselectionInspectionLimitsFinder
    {
        List<AutoselectionInspectionPvpLimit> SearchBySelectionId(long selectionId);
        List<AutoselectionInspectionPvpLimit> SearchForOneSelectionBySelectionId(long selectionId);
    }
}
