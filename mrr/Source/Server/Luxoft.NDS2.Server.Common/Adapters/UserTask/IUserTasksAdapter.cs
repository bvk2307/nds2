﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Common.Adapters.UserTask
{
    public interface IUserTasksAdapter
    {
        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        List<UserTaskInList> Search(
            FilterExpressionBase filterBy, 
            IEnumerable<ColumnSort> orderBy, 
            uint rowsFrom, 
            uint rowsTo);

        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        List<UserTaskInList> SearchFullData(
            FilterExpressionBase filterBy,
            IEnumerable<ColumnSort> orderBy,
            uint rowsFrom,
            uint rowsTo);

        /// <summary>
        /// поиск количества пз по заданным условиям
        /// </summary>
        int Count(FilterExpressionBase filterBy);

        /// <summary>
        /// поиск общего количества пз по заданным условиям
        /// </summary>
        int Total(FilterExpressionBase filterBy);

        /// <summary>
        /// Сведения о пользовательском задании - для карточки
        /// </summary>
        UserTaskInDetails GetUserTaskDetails(
            long userTaskId,
            int taskTypeId,
            long declarationZip
            );

        /// <summary>
        /// пз - Список исполнителей
        /// </summary>
        /// <param name="userTaskId">userTask Id</param>
        List<Actor> GetActorList(long userTaskId);

        /// <summary>
        /// список статусов нп
        /// </summary>
        List<TaxPayerSolvency> GetTaxPayerSolvencyList();

        /// <summary>
        /// Выставить статус платежеспособности
        /// </summary>
        void SetSolvency(long taskId, long solvencyStatusId, string comment);
    }
}
