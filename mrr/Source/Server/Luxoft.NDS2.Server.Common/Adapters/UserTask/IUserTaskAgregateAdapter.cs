﻿using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Common.Adapters.UserTask
{
    public interface IUserTaskAgregateAdapter<T> where T : UserTaskSummary
    {
        T[] Select(FilterExpressionBase filterBy);
    }
}
