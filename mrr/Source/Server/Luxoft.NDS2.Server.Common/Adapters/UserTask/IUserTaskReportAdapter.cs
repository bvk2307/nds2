﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;

namespace Luxoft.NDS2.Server.Common.Adapters.UserTask
{
    public interface IUserTaskReportAdapter
    {
        ReportIdSearchResult FindByDate(DateTime reportDate);
    }
}
