﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Common.Adapters.UserTask
{
    public interface IUserTaskUpdateStatusAdapter
    {
        /// <summary>
        /// Выставляет статус "Выполнено" ПЗ Ввод ответа на истребование
        /// </summary>
        void CompleteReclaimReplyTask(long explainId);
    }
}
