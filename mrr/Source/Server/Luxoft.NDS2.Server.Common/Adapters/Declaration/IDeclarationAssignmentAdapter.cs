﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.Common.Adapters.Declaration
{
    public interface IDeclarationAssignmentAdapter
    {
        void Insert(DeclarationSummaryKey declarationKey, long assignedBy, long assignedTo);
    }
}
