﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.Declaration
{
    public interface ITaxPayerDeclarationAdapter
    {
        IEnumerable<TaxPayerDeclarationVersion> Search(FilterExpressionBase filterBy, List<ColumnSort> orderBy);
    }
}
