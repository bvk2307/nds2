﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDocumentAdapter
    {
        PageResult<DiscrepancyDocumentInfo> DocumentList(QueryConditions criteria);
        PageResult<DocumentKNP> DocumentKnpList(QueryConditions criteria);

        DiscrepancyDocumentInfo GetDocument(long id);

        PageResult<DiscrepancyDocumentInvoice> GetInvoices(long docId, QueryConditions criteria);
        PageResult<InvoiceRelated> GetInvoicesRelated(long docId, QueryConditions criteria);

        DocumentCalculateInfo GetDocumentCalculateInfo(long docId);
        DocumentCalculateInfo GetDocumentCalculateInfoClaimSF(long docId);
        List<DocumentStatusHistory> GetStatusHistories(long docId);
        long GetDiscrepancyId(string invoiceId, int typeCode, long docId);
        Discrepancy GetDiscrepancy(string invoiceId, int typeCode);
        void UpdateUserComment(long docId, string userComment);
        DiscrepancyDocumentInfo GetDiscrepancyDocumentInfo(long discrepancyId, int stageId);
        PageResult<NotReflectedInvoice> GetNotRefectedInvoices(long docId, QueryConditions criteria);
    }
}
