﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Server.Common.Adapters.Explain
{
    public interface IExplainCommentAdapter
    {
        string Search(long explainId, ExplainType explainType);

        void Update(long id, ExplainType type, string text);
    }
}
