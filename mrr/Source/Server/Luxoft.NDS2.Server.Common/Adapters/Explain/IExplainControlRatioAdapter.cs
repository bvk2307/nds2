﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.Explain
{
    public interface IExplainControlRatioAdapter
    {
        List<ExplainControlRatio> SelectByExplain(long explainZip);
    }
}
