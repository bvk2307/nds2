﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Server.Common.Adapters.Explain
{
    public interface IExplainAdapter
    {
        ExplainDetailsData Search(long id);
    }
}
