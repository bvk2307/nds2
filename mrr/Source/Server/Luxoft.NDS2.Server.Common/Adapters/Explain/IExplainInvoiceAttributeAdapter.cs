﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.Explain
{
    public interface IExplainInvoiceAttributeAdapter<T>
    {
        IEnumerable<T> Search(FilterExpressionBase filterBy);
    }
}
