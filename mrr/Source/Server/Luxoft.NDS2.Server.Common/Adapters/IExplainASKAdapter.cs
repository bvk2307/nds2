﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IExplainASKAdapter
    {
        /// <summary>
        /// добавляет запись в таблицу AskZipФайл
        /// </summary>
        /// <param fileName="qc">название файла</param>
        /// <param pathDirectory="qc">путь к директории</param>
        /// <param sounCode="qc">код НО</param>
        /// <returns></returns>
        long? AddAskZipFile(string fileName, string pathDirectory, string sounCode);
    }
}
