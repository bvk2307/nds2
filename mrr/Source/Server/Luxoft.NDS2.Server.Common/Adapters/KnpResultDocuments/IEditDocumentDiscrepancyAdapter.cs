﻿
namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IEditDocumentDiscrepancyAdapter : IDocumentDiscrepancyAdapter
    {
        void UpdateAmount(long id, decimal amount);

        void DeleteAllClosed(long documentId);

        int CountAllInvalid(long documentId);

        void CloseDocument(long documentId);

        void RemoveDiscrepancy(long documentId, long discrepancyId);
    }
}
