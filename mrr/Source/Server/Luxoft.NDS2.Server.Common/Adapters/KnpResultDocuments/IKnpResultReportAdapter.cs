﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IKnpResultReportAdapter
    {
        long GetId(int fiscalYear, int quarter, DateTime reportDate);
    }
}
