﻿namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IUpdateDecisionStatusAdapter
    {
        int GetStatus();
    }
}
