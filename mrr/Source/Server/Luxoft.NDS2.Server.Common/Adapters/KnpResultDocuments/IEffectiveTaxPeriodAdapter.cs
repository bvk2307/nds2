﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IEffectiveTaxPeriodAdapter
    {
        IEnumerable<EffectiveTaxPeriod> All();
    }
}
