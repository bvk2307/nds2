﻿using System;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IDocumentAdapter
    {
        long Insert(string innDeclarant, string inn, string kpp, string kppEffective, int year, string period);

        void Update(long id, DateTime? closedAt);
    }
}
