﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IAppendDocumentDiscrepancyAdapter : IDocumentDiscrepancyAdapter
    {
        void CopyAll(long documentId);

        void CommitAll(long documentId);

        void RollbackAll(long documentId);
    }
}
