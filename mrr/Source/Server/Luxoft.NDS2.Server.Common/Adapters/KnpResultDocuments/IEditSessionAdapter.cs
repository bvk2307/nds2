﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IEditSessionAdapter
    {
        bool TryGet(long documentId, out EditSession session);

        void Insert(EditSession session);

        void Update(EditSession session);

        void Delete(long documentId);

        int GetDuration();
    }
}
