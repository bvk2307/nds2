﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface ISeodDecisionAdapter
    {
        KnpDecisionType[] Search(
            string innDeclarant,
            string inn,
            string kppEffective,
            string periodCode,
            string fiscalYear);
    }
}
