﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IDocumentAdapterFactory
    {
        IEditSessionAdapter SessionAdapter();

        IDocumentAdapter DocumentAdapter();

        IDocumentDiscrepancyAdapter ReadonlyDiscrepancyAdapter();

        IAppendDocumentDiscrepancyAdapter AppendDicrepancyAdapter();

        IEditDocumentDiscrepancyAdapter EditDiscreancyAdapter();

        IUpdateDecisionStatusAdapter UpdateStatusAdapter();
    }
}
