﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IDocumentDiscrepancyAdapter
    {
        IEnumerable<KnpResultDiscrepancy> Search(
            FilterExpressionBase filterBy, 
            IEnumerable<ColumnSort> orderBy, 
            uint rowsToTake, 
            uint rowsToSkip);

        KnpResultDiscrepancySummary Count(FilterExpressionBase filterBy);

        void UpdateIncluded(FilterExpressionBase filterBy, bool included);
    }
}
