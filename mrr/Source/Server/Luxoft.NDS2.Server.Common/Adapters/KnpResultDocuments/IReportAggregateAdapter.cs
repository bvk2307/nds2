﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments
{
    public interface IReportAggregateAdapter
    {
        IEnumerable<KnpResultReportSummary> Select(FilterExpressionBase filterBy);
    }
}
