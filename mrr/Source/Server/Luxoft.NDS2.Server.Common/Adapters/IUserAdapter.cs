﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface IUserAdapter
    {
        /// <summary>
        /// Ищет пользователя по Sid и добавляет нового либо обновляет данные существующего
        /// </summary>
        /// <param name="userInfo">Данные пользователя</param>
        /// <returns>ИД пользователя</returns>
        long InsertOrUpdate(UserInformation userInfo);
    }
}
