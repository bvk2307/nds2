﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface ISovInvoiceAdapter
    {        
        CountResult Count(int chapter, FilterExpressionBase searchBy, IEnumerable<string> summaryFields = null);

        IEnumerable<Invoice> Search(int chapter, DataQueryContext queryContext);

        Dictionary<int, ImpalaOption> SearchOptions();
    }

    public struct CountResult
    {
        public int Count;
        public Dictionary<string, decimal> InvoicesSumms;
    }
}
