﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IControlRatioAdapter
    {
        PageResult<ControlRatio> SelectDeclarationControlRatio(QueryConditions conditions);

        PageResult<ControlRatio> SelectClaimControlRatio(QueryConditions conditions);

        DiscrepancyDocumentInfo GetDocumentInfo(long ratioId);

        void UpdateComment(long id, string comment);

        ControlRatio GetControlRatio(long id);
    }
}