﻿using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.Claims
{
    public interface IReportSelectionStatsAdapter
    {
        List<SelectionClaimCompare> All(DateTime date);
    }

    public interface IReportInvoiceClaimListAdapter
    {
        List<InvoiceClaimReportItem> Search(DateTime date, List<FilterQuery> filter);
    }

    public interface IReportTaxMonitoringAdapter
    {
        List<TaxMonitoringReportItem> GetForChapter(long docId, int chapter);

        List<TaxMonitoringReportItem> GetUnconfirmed(long docId);
    }
}
