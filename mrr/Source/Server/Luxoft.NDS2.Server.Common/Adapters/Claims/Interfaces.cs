﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.Common.Adapters.Claims
{
    public enum ClaimType
    {
        Invoice = 1
    }

    public interface ICreateDatesAdapter
    {
        List<DateTime> All(ClaimType type);
    }

    public interface IStatusListAdapter
    {
        List<SimpleStatusDto> All(ClaimType type);
    }

    public interface IDocSendStatusAdapter
    {
        DocSendStatus Get(ClaimType type, DateTime date);
    }

    public interface IDocManageAdapter
    {
        void Include(long docId, string username);

        void Exclude(long docId, string username);
        
        void Send(DateTime date, string username);
        
        DateTime GetBaseDate();
    }
}
