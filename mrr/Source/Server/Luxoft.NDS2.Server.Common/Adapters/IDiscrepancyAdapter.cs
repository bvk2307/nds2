﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.DAL
{
    public interface IDiscrepancyAdapter
    {
        DiscrepancySummaryInfo GetSummaryInfo();

        RequestStatusType GetInvoiceRequestStatus(long requestId);

        bool DiscrepancyIsClosed();
        List<DiscrepancySide> GetDiscrepancySides();

        void SaveComment(DetailsBase details);
        void SaveDiscrepancyUserComment(string comment);

        /// <summary>
        /// Поиск данных расхождений в агрегате расхождений для НД
        /// </summary>
        /// <param name="criteria">Условия поиска</param>
        /// <returns>Страница данных расхождений</returns>
        PageResult<DeclarationDiscrepancy> SearchDeclarationDiscrepancy(QueryConditions criteria);
    }
}