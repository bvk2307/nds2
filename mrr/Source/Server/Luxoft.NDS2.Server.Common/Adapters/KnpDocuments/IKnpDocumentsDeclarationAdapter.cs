﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpDocuments
{
    public interface IKnpDocumentsDeclarationAdapter
    {
        List<KnpDocumentDeclaration> SelectVersions(
            string innDeclarant,
            string innContractor,
            string kppEffective,
            int periodEffective,
            int fiscalYear);
    }
}
