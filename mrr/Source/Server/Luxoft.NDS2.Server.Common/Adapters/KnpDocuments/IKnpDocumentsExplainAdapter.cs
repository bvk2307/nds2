﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpDocuments
{
    public interface IKnpDocumentsExplainAdapter
    {
        List<ClaimExplain> SelectByDeclaration(
            string innDeclarant,
            string innContractor,
            string kppEffective,
            int periodEffective,
            int fiscalYear);
    }
}
