﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Server.Common.Adapters.KnpDocuments
{
    public interface IKnpDocumentsActAdapter
    {
        List<SeodKnpDocument> SelectByDeclaration(
            string innDeclarant,
            string innContractor,
            string kppEffective,
            int periodEffective,
            int fiscalYear);
    }
}
