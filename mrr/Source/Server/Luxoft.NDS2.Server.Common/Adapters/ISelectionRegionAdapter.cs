﻿
namespace Luxoft.NDS2.Server.DAL
{
    public interface ISelectionRegionAdapter
    {
        void UpdateBySelection(long selectionId, string[] regionCodes);
    }
}
