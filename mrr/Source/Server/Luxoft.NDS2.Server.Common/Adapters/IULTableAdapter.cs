﻿using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface IULTableAdapter
    {
        List<EgrnULTableRow> Search(FilterExpressionBase filter);
    }
}
