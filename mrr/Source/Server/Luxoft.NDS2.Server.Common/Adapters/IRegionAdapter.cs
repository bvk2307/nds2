﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс описывает методы работы со справочником регионов
    /// </summary>
    public interface IRegionAdapter
    {
        /// <summary>
        /// Получает список регионов, декларации которыз доступны пользователю
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <returns>Список регионов</returns>
        List<DictionaryCommon> GetRegionsAvailable(string userSid);

        List<DictionaryCommon> GetRegionsAvailable(
            string userSid,
            string searchKey,
            int maxQuantity);

        /// <summary>
        /// Получает список инспекций, которые доступны пользователю
        /// </summary>
        /// <param name="userSid">SID пользователя</param>
        /// <param name="criteria">Дополнительные условия отбора</param>
        /// <returns>Список инспекций</returns>
        List<DictionaryNalogOrgan> GetInspectionsAvailable(string userSid, DictionaryConditions criteria);

        List<DictionaryNalogOrgan> GetInspectionsAvailable(
            string userSid,
            string searchkey,
            int maxQuantity);

        /// <summary>
        /// Получает список федеральных округов
        /// </summary>
        /// <returns>Список федеральных округов</returns>
        List<FederalDistrict> GetFederalDistricts();

        /// <summary>
        /// Получает список соотвествия федеральных округов регионам
        /// </summary>
        /// <returns>Список соотвествия федеральных округов регионам</returns>
        List<FederalDistrictToRegion> GetFederalDistrictToRegion();
    }
}
