﻿
namespace Luxoft.NDS2.Server.DAL
{
    /// <summary>
    /// Этот интерфейс болкиратора объектов
    /// </summary>
    public interface IObjectLockerAdapter
    {
        string Lock(long ObjectId, long ObjectType, string userName, string hostName);

        void UnLock(string ObjectKey, string userName, string hostName);

        bool CheckLock(long ObjectId, long ObjectType, string ObjectKey);

        string GetLockers(long ObjectId, long ObjectType);
    }
}
