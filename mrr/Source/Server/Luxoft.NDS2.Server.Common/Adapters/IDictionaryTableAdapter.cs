﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters
{
    public interface IDictionaryTableAdapter<TData>
    {
        IEnumerable<TData> All();
    }
}
