﻿using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Common.Adapters.OperationContext
{
    public interface IOperationContextAdapter
    {
        List<OperationAccessData> Select(FilterExpressionBase filterBy);
    }
}
