﻿namespace Luxoft.NDS2.Server.Common.Adapters.OperationContext
{
    public enum AccessLimitType
    {
        NotRestricted = 1,
        SelfInspection,
        FederalDistrict,
        Region,
        Inspection
    }
}
