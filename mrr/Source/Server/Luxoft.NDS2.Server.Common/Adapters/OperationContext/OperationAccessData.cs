﻿namespace Luxoft.NDS2.Server.Common.Adapters.OperationContext
{
    public class OperationAccessData : OperationData
    {
        public string Description { get; set; }

        public string AccessToRegionCode { get; set; }

        public string AccessToSonoCode { get; set; }

        public int? AccessToDictrictId { get; set; }
    }
}
