﻿
namespace Luxoft.NDS2.Server.Common.Adapters.OperationContext
{
    public class OperationData
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string SonoCode { get; set; }

        public AccessLimitType LimitType { get; set; }

        public string RoleName { get; set; }
    }
}
