﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Server.Services.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Server.Common.Managers
{
    public class TroubleshootLoger
    {
        private object _syncLock;
        private const int _objectToFlush = 50;

        List<Tuple<long, int, long, DateTime, string, string, string>> _queries = new List<Tuple<long, int, long, DateTime, string, string, string>>();  

        public TroubleshootLoger(IReadOnlyServiceCollection services)
        {
            _syncLock = new object();
        }

        public void RegisterEvent(long zip, int chapter, long elapsedMs, DateTime ts, string source, string queryType, string error)
        {
#if DEBUG
            if (_queries.Count > _objectToFlush)
            {
                lock (_syncLock)
                {
                    if (_queries.Count > _objectToFlush)
                    {
                        FlushDataToDatabase();
                        _queries.Clear();
                    }
                }
            }

            _queries.Add(new Tuple<long, int, long, DateTime, string, string, string>(zip, chapter, elapsedMs, ts, source, queryType, error));
#endif
        }

        private void FlushDataToDatabase()
        {
            using (OracleConnection conn = new OracleConnection("Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = m9965-opz256)(PORT = 1521))) (CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = NDS2TEST)));User Id=NDS2_MRR_USER;Password=NDS2_MRR_USER;Connection Timeout=120;"))
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    try
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.BindByName = true;
                        BuildCommadText(cmd);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("TroubleshootLoger: Ошибка записи в БД - {0}", ex.Message);
                    }
                }
            }
        }

        private void BuildCommadText(OracleCommand cmd)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("begin");
            int i = 0;
            int j = 0;
            foreach (var query in _queries)
            {
                sb.AppendFormat("insert into impala_lt_log values(:pZip{0}, :pChapter{1}, :pElapsedMs{2}, :pTimeStamp{3}, :pSourceHost{4}, :pQueryType{5}, :pHost{6}, :pStatus{7}, :pErrorText{8}); \n", i++, i++, i++, i++, i++, i++, i++, i++, i++);

                cmd.Parameters.Add(string.Format(":pZip{0}", j++), OracleDbType.Int64, ParameterDirection.Input).Value = query.Item1;
                cmd.Parameters.Add(string.Format(":pChapter{0}", j++), OracleDbType.Int32, ParameterDirection.Input).Value = query.Item2;
                cmd.Parameters.Add(string.Format(":pElapsedMs{0}", j++), OracleDbType.Int64, ParameterDirection.Input).Value = query.Item3;
                cmd.Parameters.Add(string.Format(":pTimeStamp{0}", j++), OracleDbType.Date, ParameterDirection.Input).Value = query.Item4;
                cmd.Parameters.Add(string.Format(":pSourceHost{0}", j++), OracleDbType.Varchar2, ParameterDirection.Input).Value = query.Item5;
                cmd.Parameters.Add(string.Format(":pQueryType{0}", j++), OracleDbType.Varchar2, ParameterDirection.Input).Value = query.Item6;
                cmd.Parameters.Add(string.Format(":pHost{0}", j++), OracleDbType.Varchar2, ParameterDirection.Input).Value = Environment.MachineName;
                cmd.Parameters.Add(string.Format(":pStatus{0}", j++), OracleDbType.Int32, ParameterDirection.Input).Value = string.IsNullOrEmpty(query.Item7) ? 1 : 3;
                cmd.Parameters.Add(string.Format(":pErrorText{0}", j++), OracleDbType.Clob, ParameterDirection.Input).Value = query.Item7;

            }

            sb.AppendLine("end;");

            cmd.CommandText = sb.ToString();
        }
    }
}
