﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Server.Services.Managers.Authorization
{
    public interface IAuthorizationProvider
    {
        string CurrentUserName { get; }

        List<AccessRight> GetUserPermissions();

        List<AccessRight> GetUserPermissions(string SID);

        void ChangeUserRole(string roleName);

        bool IsOperationEligible(string operationName);

        bool IsUserInRole(string roleName);

        bool IsUserInRole(string userSID, string roleName);

        string CurrentUserSID { get; }

        List<string> GetUserSIDs();

        List<UserStructContextRight> GetStructContextUserRigths(string ifns);

        IList<string> GetUserStructContexts(PermissionType permType, string objectName);

        /// <summary>
        /// Возвращает список сотрудников ИФНС принадлежащих к указанной роли
        /// </summary>
        /// <param name="ifns">
        /// Код ИФНС из СОНО
        /// </param>
        /// <param name="roleOperation">
        /// Наименование операции
        /// </param>
        /// <returns>
        /// Список идентификаторов сотрудников по указанной ИФНС
        /// </returns>
        List<UserStructContextRight> GetStructContextUsersByRole(string ifns, string roleOperation);
    }
}
