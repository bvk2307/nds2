﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Xml.Linq;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.Managers.Authorization
{
    public class AzManAuthorizationProvider : IAuthorizationProvider
    {
        private IAuthorizationService _commonAuthService;
        private string _cacheKeyPrefix = "nds.auth.";
        private MemoryCache _cache = MemoryCache.Default;
        private ServiceHelpers _helper = null;

        public AzManAuthorizationProvider(IReadOnlyServiceCollection services)
        {
            _commonAuthService = services.Get<IAuthorizationService>();
            _helper = new ServiceHelpers(services);
        }

        public string CurrentUserName { get; private set; }

        public List<AccessRight> GetUserPermissions()
        {
            return GetAccessRightsInternal();
        }

        public List<AccessRight> GetUserPermissions(string SID)
        {
            return GetAccessRightsInternal(SID);
        }

        private List<AccessRight> GetAccessRightsInternal()
        {
            return GetAccessRightsInternal(CurrentUserSID);
        }

        private List<AccessRight> GetAccessRightsInternal(string SID)
        {
            var op = new Constants.SystemPermissions.Operations();
            var result = new List<AccessRight>();
            var operationAuthCtx = typeof(Constants.SystemPermissions.Operations)
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                .Select(f => new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, f.GetValue(op).ToString()))
                .ToList();

            var availableContexts = _commonAuthService.GetStructContexts(SID,
                new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, Constants.SystemPermissions.Operations.SystemLogon)
                    .GetFullName());

            foreach (var ctx in availableContexts)
            {
                operationAuthCtx.ForEach(ct => ct.StructContext = ctx);

                var authResult = _commonAuthService.Authorize(_helper.User, operationAuthCtx.Select(c => c.GetFullName()).ToList());

                for (int i = 0; i < operationAuthCtx.Count(); i++)
                {
                    if (authResult[i] && !result.Any(ar => ar.Name.Equals(operationAuthCtx[i].Name)))
                    {
                        result.Add(new AccessRight() { StructContext = operationAuthCtx[i].StructContext, Name = operationAuthCtx[i].Name, PermType = PermissionType.Operation });
                    }
                }
            }

            return result;
        }

        public void ChangeUserRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public List<string> GetSubsystemRoles()
        {
            return GetUserPermissions().Where(n => n.Name.StartsWith("Роль.")).Select(r => r.Name).ToList();
        }

        public bool IsOperationEligible(string operationName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(operationName));
        }

        public bool IsUserInRole(string roleName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(roleName));
        }

        public bool IsUserInRole(string userSID, string roleName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(roleName));
        }

        public string CurrentUserSID
        {
            get
            {
                if (Thread.CurrentPrincipal.Identity is WindowsIdentity)
                {
                    return ((WindowsIdentity)Thread.CurrentPrincipal.Identity).User.Value;
                }

                return string.Empty;
            }
        }

        public List<string> GetUserSIDs()
        {
            throw new NotImplementedException();
        }

        public List<UserStructContextRight> GetStructContextUserRigths(string ifns)
        {
            var result = new List<UserStructContextRight>();

            List<string> roleOperatons = new List<string>()
            {
                Constants.SystemPermissions.Operations.RoleAnalyst,
                Constants.SystemPermissions.Operations.RoleApprover,
                Constants.SystemPermissions.Operations.RoleDeveloper,
                Constants.SystemPermissions.Operations.RoleInspector,
                Constants.SystemPermissions.Operations.RoleManager,
                Constants.SystemPermissions.Operations.RoleMedodologist,
                Constants.SystemPermissions.Operations.RoleSender

            };

            foreach (var roleOperation in roleOperatons)
            {
                var ctx = new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, ifns, roleOperation).GetFullName();
                foreach (var assignment in _commonAuthService.GetAssignments(ctx))
                {
                    result.Add(new UserStructContextRight()
                    {
                        UserSid = assignment,
                        Role = roleOperation,
                        StructContext = ifns
                    });
                }
            }

            return result;
        }

        public IList<string> GetUserStructContexts(PermissionType permType, string objectName)
        {
            var ctx = new AuthorizationContext(permType, Constants.SubsystemName, objectName).GetFullName();

            return _commonAuthService.GetStructContexts(ctx);
        }


        public List<UserStructContextRight> GetStructContextUsersByRole(string ifns, string roleOperation)
        {
            var ctx = new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, ifns, roleOperation).GetFullName();
            return _commonAuthService.GetAssignments(ctx).Select(assignment => new UserStructContextRight
            {
                UserSid = assignment,
                Role = roleOperation,
                StructContext = ifns
            }).ToList();
        }
    }
}