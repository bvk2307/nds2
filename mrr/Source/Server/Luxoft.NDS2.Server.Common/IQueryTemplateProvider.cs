﻿namespace Luxoft.NDS2.Server.Common
{
    /// <summary>
    /// Этот интерфейс описывает метод получения текста шаблона SQL запроса из XML
    /// </summary>
    public interface IQueryTemplateProvider
    {
        string GetQueryText(string scope, string qName);
    }
}
