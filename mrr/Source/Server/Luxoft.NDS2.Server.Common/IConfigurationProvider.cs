﻿namespace Luxoft.NDS2.Server.Common
{
    public interface IConfigurationProvider
    {
        /// <summary>
        /// Вычитывает параметр конфигурации по ключу
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Значение параметра конфигурации</returns>
        string GetConfigurationValue(string key);
    }
}
