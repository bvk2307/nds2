﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Common
{
    public class TaxPayerNamesLoader
    {
        private static int IPInnLength = 12;

        private static int InListMaximumCapacity = 1000;

        private readonly IIPTableAdapter _ipAdapter;

        private readonly IULTableAdapter _ulAdapter;

        public TaxPayerNamesLoader(IIPTableAdapter ipAdapter, IULTableAdapter ulAdapter)
        {
            _ipAdapter = ipAdapter;
            _ulAdapter = ulAdapter;
        }

        public IEnumerable<EgrnName> Load(string[] innList)
        {
            var result = new List<EgrnName>();

            result.AddRange(LoadIpNames(innList.Where(inn => inn.Length == IPInnLength).ToArray()));
            result.AddRange(LoadUlNames(innList.Where(inn => inn.Length < IPInnLength).ToArray()));

            return result;
        }

        private IEnumerable<EgrnName> LoadIpNames(string[] innList)
        {
            List<EgrnName> result = new List<EgrnName>();

            if (!innList.Any())
            {
                return new EgrnName[0];
            }

            List<string> inns = new List<string>(innList);
            if (inns.Count > InListMaximumCapacity)
            {
                int i = 0;
                var lastPieceDataCount = inns.Count % InListMaximumCapacity;
                int maxDataBlock = inns.Count / InListMaximumCapacity;

                for (; i < maxDataBlock; i = i + 1)
                {
                    result.AddRange(LoadIPNamesPartial(inns.GetRange(i * InListMaximumCapacity, InListMaximumCapacity).ToArray()));
                }

                if (lastPieceDataCount > 0)
                {
                    result.AddRange(LoadIPNamesPartial(inns.GetRange(i * InListMaximumCapacity, lastPieceDataCount).ToArray()));
                }

                return result;
            }
            else
            {
                return LoadUlNamesPartial(innList);
            }
        }

        private IEnumerable<EgrnName> LoadIPNamesPartial(string[] innList)
        {
            return
                _ipAdapter
                .Search(
                    FilterExpressionCreator.CreateInList(
                        TypeHelper<EgrnIPTableRow>.GetMemberName(dto => dto.INNFL),
                        innList))
                .Select(
                    dto =>
                        new EgrnName
                        {
                            Inn = dto.INNFL,
                            Name =
                                string.Join(
                                    " ",
                                    dto.LAST_NAME ?? string.Empty,
                                    dto.FIRST_NAME ?? string.Empty,
                                    dto.PATRONYMIC ?? string.Empty)
                        });
        }

        private IEnumerable<EgrnName> LoadUlNames(string[] innList)
        {
            List<EgrnName> result = new List<EgrnName>();

            if (!innList.Any())
            {
                return result;
            }

            List<string> inns = new List<string>(innList/*.Distinct()*/);

            /*NOTE: Ограничения Оракла на 1000 элементов в in списке*/
            if (inns.Count > InListMaximumCapacity)
            {
                int i = 0;
                var lastPieceDataCount = inns.Count % InListMaximumCapacity;
                int maxDataBlock = inns.Count / InListMaximumCapacity;

                for (; i < maxDataBlock; i = i + 1)
                {
                    result.AddRange(LoadUlNamesPartial(inns.GetRange(i * InListMaximumCapacity, InListMaximumCapacity).ToArray()));
                }

                if (lastPieceDataCount > 0)
                {
                    result.AddRange(LoadUlNamesPartial(inns.GetRange(i * InListMaximumCapacity, lastPieceDataCount).ToArray()));
                }

                return result;
            }
            else
            {
                return LoadUlNamesPartial(innList);
            }
        }

        private IEnumerable<EgrnName> LoadUlNamesPartial(string[] innList)
        {
            return _ulAdapter
                    .Search(
                        FilterExpressionCreator.CreateInList(
                           TypeHelper<EgrnULTableRow>.GetMemberName(dto => dto.INN), 
                            innList))
                    .Select(
                        dto =>
                            new EgrnName
                            {
                                Inn = dto.INN,
                                Name = dto.NP_NAME
                            });
        }
    }
}
