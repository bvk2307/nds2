﻿using System;

namespace Luxoft.NDS2.Server.Common
{
    public class SovRequestProcessingException : Exception
    {
        public SovRequestProcessingException()
            : base("SOV data feed error")
        {
        }
    }
}
