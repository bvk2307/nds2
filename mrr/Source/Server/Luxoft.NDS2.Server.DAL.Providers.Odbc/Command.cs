﻿//using System;
//using System.Data;
//using System.Data.Common;
//using System.Data.Odbc;
//
//namespace Luxoft.NDS2.Server.DAL.Providers.Odbc
//{
//    /// <summary>
//    /// Обертка над ODBC command
//    /// </summary>
//    public class Command : IDisposable
//    {
//        private OdbcCommand _command;
//
//        public string CommandText
//        {
//            get { return _command.CommandText; }
//            set { _command.CommandText = value; }
//        }
//
//        public Command(Connection conn, string commandText)
//        {
//            _command = new OdbcCommand(commandText, conn.RealConnection);
//        }
//
//        public object ExecuteScalar()
//        {
//            return _command.ExecuteScalar();
//        }
//
//        public int ExecuteNonQuery()
//        {
//            return _command.ExecuteNonQuery();
//        }
//
//        public IDataReader ExecuReader()
//        {
//            return _command.ExecuteReader();
//        }
//
//        public OdbcParameterCollection Parameters
//        {
//            get { return _command.Parameters; }
//        }
//
//        public void Dispose()
//        {
//            _command.Dispose();
//        }
//    }
//}
