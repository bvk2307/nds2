﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using Luxoft.NDS2.Server.DAL.Providers.Odbc.Configuration;

namespace Luxoft.NDS2.Server.DAL.Providers.Odbc
{
    /// <summary>
    /// Пул соединений по нодам кластера.
    /// </summary>
    public sealed class ConnectionPool : IDbConnectionProvider, IDisposable
    {
        private Dictionary<short, List<Connection>> _pool = new Dictionary<short, List<Connection>>();
        private ulong _missfires = 0;
        private short _currentPoolIndex = 0;
        private int _poolsCount = 0;
        private int _poolSize = 10;

        private static Lazy<ConnectionPool> _lzConnectionPool = new Lazy<ConnectionPool>( () => new ConnectionPool(), isThreadSafe: true );
        private static object _syncLock = new object();

        private static DataSourcePoolConfigurationSection _configuration = null;

        /// <summary> Uses a configuration of data sources from <see cref="_configuration"/> if it has been initialized by <see cref="Initialize"/>() already. </summary>
        private ConnectionPool()
        {
            if ( _configuration == null )
                ConnectionPool.Initialize( System.Configuration.ConfigurationManager.GetSection("DataSourcePoolSection") as DataSourcePoolConfigurationSection );

            InitPool();
        }

        /// <summary> Must been called only one time before the first request of <see cref="Current"/> or must been uncalled at all (it will be call by the constructor internally in this case). </summary>
        /// <param name="dataSourcePoolConfigurationSection"> A configuration of data sources to use. </param>
        public static void Initialize( DataSourcePoolConfigurationSection dataSourcePoolConfigurationSection )
        {
            if ( dataSourcePoolConfigurationSection == null )
                throw new ArgumentNullException( "dataSourcePoolConfigurationSection" );
            Contract.Ensures( _configuration != null );
            Contract.EndContractBlock();
            if ( _configuration != null )
                throw new InvalidOperationException( "The repeat call of Initialize() is not allowed" );

            _configuration = dataSourcePoolConfigurationSection;
        }

        public static ConnectionPool Current { get { return _lzConnectionPool.Value; } }

        /// <summary>
        /// Инициализация пула соединений по заданной конфигурации
        /// </summary>
        private void InitPool()
        {
#if DEBUG
            Stopwatch sw = new Stopwatch();
            sw.Start();
#endif
            var dataSources = _configuration.ConfigElements.Cast<DataSourceElement>();

            _poolsCount = dataSources.Count();
            _poolSize = Convert.ToInt32(_configuration.ConfigElements.PoolSize);
            short poolId = 0;

            foreach (var connString in dataSources)
            {
                var pool = new List<Connection>();
                for (int i = 0; i < _poolSize; i++)
                {
                    pool.Add(new Connection(connString.ConnectionDsn) { PoolName = connString.Name, Id = i, IsAvailable = true });
                }

                _pool.Add(poolId, pool);
                poolId++;
            }
#if DEBUG
            sw.Stop();
            Debug.WriteLine("Пул соединений импалы проинициализирован за {0}, Кол-во пулов:{1} Кол-во соединений в пуле:{2}", sw.ElapsedMilliseconds, _poolsCount, _poolSize);
#endif
        }

        /// <summary>
        /// Попытка получить свободное соединение.
        /// </summary>
        /// <param name="conn">Соединение</param>
        /// <param name="attempts">Кол-во попыток получения соединения</param>
        /// <param name="waitTimeoutInMs">Таймаут между попытками получить соединение</param>
        /// <returns></returns>
        public bool TryGetConnection(out IDbConnection conn, int attempts = 3, int waitTimeoutInMs = 2000)
        {
            bool success = false;
            conn = null;
            while (!success && attempts > 0)
            {
                lock (_syncLock)
                {
                    short iteration = 0;

                    while (iteration <= _poolsCount && !success)
                    {
                        if (_currentPoolIndex == _poolsCount - 1)
                        {
                            _currentPoolIndex = 0;
                        }
                        else
                        {
                            _currentPoolIndex++;
                        }

                        conn = _pool[_currentPoolIndex].FirstOrDefault(c => c.IsAvailable);

                        success = conn != null;

                        if (success)
                        {
                            Connection connection = (Connection)conn;
                            connection.IsAvailable = false;
                            connection.Hits++;
                            Debug.WriteLine(string.Format("Found free connection. Pool:{0}  Connection:{1}", connection.PoolName, connection.Id));
                        }
                        iteration++;
                    }
                }

                if (!success)
                {
                    _missfires++;
                    attempts--;
                    Thread.Sleep(waitTimeoutInMs);
                }    
            }

            return success;
        }


        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;

                foreach (var pool in _pool)
                {
                    foreach (var connection in pool.Value)
                    {
                        connection.DisposeInternal();
                    }
                }                
            }
        }
    }
}
