﻿using System.Data;

namespace Luxoft.NDS2.Server.DAL.Providers.Odbc
{
    /// <summary> A simple provider of <see cref="IDbConnection"/> </summary>
    public interface IDbConnectionProvider
    {
        /// <summary>
        /// Попытка получить свободное соединение.
        /// </summary>
        /// <param name="conn">Соединение</param>
        /// <param name="attempts">Кол-во попыток получения соединения</param>
        /// <param name="waitTimeoutInMs">Таймаут между попытками получить соединение</param>
        /// <returns></returns>
        bool TryGetConnection(out IDbConnection conn, int attempts = 3, int waitTimeoutInMs = 2000);
    }
}