﻿
using System.Configuration;

namespace Luxoft.NDS2.Server.DAL.Providers.Odbc.Configuration
{
    public class DataSourcePoolConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("DataSources", IsRequired = true)]
        public DataSourceCollection ConfigElements
        {
            get
            {
                return base["DataSources"] as DataSourceCollection;
            }
        }
    }

    [ConfigurationCollection(typeof(DataSourceElement), AddItemName = "DataSource")]
    public class DataSourceCollection : ConfigurationElementCollection
    {
        [ConfigurationProperty("poolSize")]
        public string PoolSize { get { return this["poolSize"].ToString(); } }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DataSourceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var elm = element as DataSourceElement;
            if (elm != null)
                return elm.Name;
            else
                return null;
        }
    }

    public class DataSourceElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return base["name"] as string; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("dsn", IsRequired = true)]
        public string ConnectionDsn
        {
            get { return base["dsn"] as string; }
            set { base["dsn"] = value; }
        }
    }
}
