﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;

namespace Luxoft.NDS2.Server.DAL.Providers.Odbc
{
    /// <summary>
    /// Обертка над реальным соединением Impala ODBC
    /// </summary>
    internal sealed class Connection : IDbConnection
    {
        private OdbcConnection _connection;

        internal bool IsAvailable { get; set; }

        internal string PoolName { get; set; }

        internal int Id { get; set; }

        internal ulong Hits { get; set; }

        internal OdbcConnection RealConnection { get { return _connection; } }

        public Connection(string connectionDsn)
        {
            _connection = new OdbcConnection(connectionDsn);
            _connection.Open();
        }

        public IDbTransaction BeginTransaction()
        {
            return _connection.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return _connection.BeginTransaction(il);
        }

        public void Close()
        {
            IsAvailable = true;
            Trace.WriteLine(string.Format("Соединение '{0}' возвращено обратно в пул '{1}'", Id, PoolName));
        }

        public void ChangeDatabase(string databaseName)
        {
            throw new NotImplementedException();
        }

        public IDbCommand CreateCommand()
        {
            var cmd = new OdbcCommand();
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
            Console.WriteLine(_connection.State);
            cmd.Connection = _connection;

            return cmd;
        }

        public string ConnectionString { get; set; }
        public int ConnectionTimeout { get; private set; }
        public string Database { get; private set; }
        public ConnectionState State { get; private set; }

        public void Open()
        {
            if (_connection == null)
            {
                throw new NullReferenceException("connection null");
            }

            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
        }

        public void Dispose()
        {
            this.Close();
        }

        internal void DisposeInternal()
        {
            if (_connection != null)
            {
                _connection.Dispose();
            }
        }
    }
}
