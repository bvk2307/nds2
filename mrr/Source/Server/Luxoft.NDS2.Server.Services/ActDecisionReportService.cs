﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.UserAccess;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IActDecisionReportService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ActDecisionReportService : IActDecisionReportService
    {
        private readonly ServiceHelpers _helper;


        private static string[] Operations =
            new string[]
            {
                MrrOperations.ActAndDecision.ActDecisionReportByCountry,
                MrrOperations.ActAndDecision.ActDecisionReportByFederalDistrict,
                MrrOperations.ActAndDecision.ActDecisionReportByRegion,
                MrrOperations.ActAndDecision.ActDecisionReportBySono
            };

        public ActDecisionReportService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<long> ReportId(DateTime reportDate, int fiscalYear, int quarter)
        {
            return _helper.DoEx(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return KnpResultReportAdapterCreator
                            .Report(_helper, connection)
                            .GetId(fiscalYear, quarter, reportDate);
                    }
                });
        }

        public OperationResult<KnpResultReportSummary[]> DataByRegion(long reportId)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = KnpResultReportAdapterCreator.Agregate(_helper, connection);
                        var group = FilterByReportId(reportId);

                        group.WithExpression(
                            FilterExpressionCreator.Create(
                                TypeHelper<KnpResultReportSummary>.GetMemberName(x => x.SonoCode),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                null));

                        return adapter.Select(group).ToArray();
                    }
                });
        }

        public OperationResult<KnpResultReportSummary[]> DataBySono(long reportId, string[] regionCodes, string[] sonoCodes)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = KnpResultReportAdapterCreator.Agregate(_helper, connection);
                        var group = FilterByReportId(reportId);

                        group.WithExpression(
                            FilterExpressionCreator.Create(
                                TypeHelper<KnpResultReportSummary>.GetMemberName(x => x.SonoCode),
                                ColumnFilter.FilterComparisionOperator.NotEquals,
                                null));

                        if (sonoCodes != null && sonoCodes.Any())
                        {
                            group.WithExpression(
                                FilterExpressionCreator.CreateInList(
                                    TypeHelper<KnpResultReportSummary>.GetMemberName(x => x.SonoCode),
                                    sonoCodes));
                        }
                        else
                        {
                            group.WithExpression(
                                FilterExpressionCreator.CreateInList(
                                    TypeHelper<KnpResultReportSummary>.GetMemberName(x => x.RegionCode),
                                    regionCodes));
                        }

                        return adapter.Select(group).ToArray();
                    }
                });
        }

        public OperationResult<AccessContext> GetAccesContext()
        {
            return _helper.Do(() => 
            {
                return _helper.GetAccessContext(Operations);
            });
        }

        private FilterExpressionGroup FilterByReportId(long reportId)
        {
            return FilterExpressionCreator.CreateGroup()
                .WithExpression(
                    FilterExpressionCreator.Create(
                        TypeHelper<KnpResultReportSummary>.GetMemberName(x => x.ReportId),
                        ColumnFilter.FilterComparisionOperator.Equals,
                        reportId));               
        }

        public OperationResult<EffectiveTaxPeriod[]> GetAvailableTaxPeriods()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return _helper.EffectiveTaxPeriodAdapter(connection).All().ToArray();
                    }
                });
        }
    }
}
