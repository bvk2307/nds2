﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Communication;
using CommonComponents.Shared;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDiscrepancyForDeclarationCardService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    class DiscrepancyForDeclarationCardService : IDiscrepancyForDeclarationCardService
    {
        private const int RequestTicking = 1000;
        private const int RequestTimeout = 600000;

        private int _requestTiming;

        private readonly ServiceHelpers _helper;
        private readonly IAuthorizationProvider _localAuthProvider;

        public DiscrepancyForDeclarationCardService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<PageResult<DeclarationDiscrepancy>> GetAllDiscrepancyList(QueryConditions qc, HiveRequestDiscrepanciesList hiveRequest)
        {
            var rez = _helper.Do(
             () =>
             {

                var allQuery = (QueryConditions)qc.Clone();
                allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                IHiveRequestAdapter hiveRequestAdapter = DiscrepancyForDeclarationCardAdapterCreator.HiveRequestAdapter(_helper, hiveRequest);
                
                int status;
                long requestId = hiveRequestAdapter.GetRequestId(out status);

                while (_requestTiming < RequestTimeout)
                {
                    if (status == (int)HiveRequestStatus.FinishLoading)
                    {
                        break;
                    }

                    if (status == (int)HiveRequestStatus.Error)
                    {
                        throw new Exception(String.Format(Constants.HiveRequestDiscrepanciesListLoadFailed, hiveRequest.Inn, hiveRequest.KppEffective, hiveRequest.FiscalYear, hiveRequest.PeriodEffective, hiveRequest.CorrectionNumber));
                    }

                    System.Threading.Thread.Sleep(RequestTicking);

                    status = hiveRequestAdapter.GetRequestStatus(requestId);

                    _requestTiming += RequestTicking;
                }

                if (status == (int)HiveRequestStatus.FinishLoading)
                {
                    return DiscrepancyForDeclarationCardAdapterCreator.AllDiscrepancyForDeclarationCardAdapter(_helper, requestId)
                            .Page(
                            allQuery.Filter.ToFilterGroup(),
                            qc.Filter.ToFilterGroup(),
                            qc);
                }
                else
                {
                    throw new Exception(String.Format(Constants.HiveRequestDiscrepanciesListTimeout, hiveRequest.Inn, hiveRequest.KppEffective, hiveRequest.FiscalYear, hiveRequest.PeriodEffective, hiveRequest.CorrectionNumber));
                }

             });

            if (rez.Result == null)
            {
                rez.Result = new PageResult<DeclarationDiscrepancy>();
            }

            return rez;
        }

        public OperationResult<PageResult<DeclarationDiscrepancy>> GetKnpDiscrepancyList(QueryConditions qc, string inn, string kppEffective, string fiscalYear, int periodEffective)
        {
            return _helper.Do(
             () =>
             {
                 var allQuery = (QueryConditions)qc.Clone();
                 allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                 var result = DiscrepancyForDeclarationCardAdapterCreator.KnpDiscrepancyForDeclarationCardAdapter(_helper, inn, kppEffective, fiscalYear, periodEffective)
                             .Page(
                                 allQuery.Filter.ToFilterGroup(),
                                 qc.Filter.ToFilterGroup(),
                                 qc);

                 return result;

             });
        }

        public OperationResult<List<string>> GetUserOperations()
        {
            return _helper.Do(() => _localAuthProvider.GetUserPermissions().Where(x => x.PermType == PermissionType.Operation).Select(x => x.Name).ToList());
        }
    }
}
