﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Server.Services.InvoiceData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.Services.InvoiceData.SOV;
using explainTks = Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Services.ExplainTks;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDiscrepancyDocumentService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class DiscrepancyDocumentDataService : ServiceBase, IDiscrepancyDocumentService
    {
        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public DiscrepancyDocumentDataService(IReadOnlyServiceCollection services)
            : base(services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }


        public OperationResult<PageResult<DiscrepancyDocumentInfo>> GetDocumentInfo(QueryConditions qc)
        {
            return DalHelper.Execute<OperationResult<PageResult<DiscrepancyDocumentInfo>>>(
                            () => new OperationResult<PageResult<DiscrepancyDocumentInfo>>()
                            {
                                Result = TableAdapterCreator.DiscrepancyDocument(_helper).DocumentList(qc),
                                Status = ResultStatus.Success
                            },
                            _helper
                            );
        }

        public OperationResult<DiscrepancyDocumentInfo> GetDocument(long id)
        {
            return DalHelper.Execute<OperationResult<DiscrepancyDocumentInfo>>(
                            () => new OperationResult<DiscrepancyDocumentInfo>()
                            {
                                Result = TableAdapterCreator.DiscrepancyDocument(_helper).GetDocument(id),
                                Status = ResultStatus.Success
                            },
                            _helper
                            );
        }



        public OperationResult<PageResult<ControlRatio>> GetClaimControlRatio(long claimId, QueryConditions conditions)
        {
            conditions.Filter.Add(new FilterQuery
            {
                ColumnName = "doc_id",
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>
                {
                    new ColumnFilter
                    {
                        Value = claimId,
                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                    }
                }
            });
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.ControlRatio(_helper).SelectClaimControlRatio(conditions), _helper);
        }

        public OperationResult<PageResult<DiscrepancyDocumentInvoice>> GetInvoices(long docId, QueryConditions allConditions)
        {
            return _helper.Do<PageResult<DiscrepancyDocumentInvoice>>(
                () =>
                {
                    QueryConditions invoicesConditions = (QueryConditions)allConditions.Clone();

                    var result = TableAdapterCreator.DiscrepancyDocument(_helper).GetInvoices(docId, invoicesConditions);

                    var rowKeys = new List<string>();
                    rowKeys.AddRange(
                        result
                            .Rows
                            .Where(dto => !string.IsNullOrWhiteSpace(dto.ROW_KEY))
                            .Select(dto => dto.ROW_KEY));

                    var invoiceExplainDatas = LoadInvoiceExplainDatas(rowKeys.ToArray());

                    var explainTKSChanges = LoadExplainInvoiceChanges(docId, rowKeys.ToArray());

                    var invoiceChangesTks = new ExplainTksFieldConverter()
                        .ConvertToExplainInvoiceChanges(explainTKSChanges);

                    foreach (var invoice in result.Rows)
                    {
                        var invoiceKey = invoice.ROW_KEY;
                        var explainData = invoiceExplainDatas.Where(p => p.InvoiceKey == invoiceKey).OrderBy(p => p.Rank).FirstOrDefault();
                        if (explainData != null)
                        {
                            invoice.ExplainNum = explainData.DocumentNumber;
                            invoice.ExplainDate = explainData.DocumentDate;
                            invoice.ExplainType = explainData.DocumentType;
                        }
                        if (invoiceChangesTks.Any(p => p.InvoiceId == invoice.ROW_KEY))
                        {
                            invoice.IS_CHANGED = 1;
                        }
                        else
                        {
                            if (explainTKSChanges.Any(p => p.RowKey == invoice.ROW_KEY))
                            {
                                invoice.IS_CHANGED = 0;
                            }
                        }
                    }
                    return result;
                }
          );
        }

        public OperationResult<PageResult<InvoiceRelated>> GetInvoicesRelated(long docId, QueryConditions conditions)
        {
            return _helper.Do(() =>
            {
                var result = TableAdapterCreator.DiscrepancyDocument(_helper).GetInvoicesRelated(docId, conditions);

                //--- Множественные значения
                var buyers = ExplainInvoiceBuyerAdapterCreator
                    .Create(_helper)
                    .GetContractors(result.Rows);

                var sellers = ExplainInvoiceSellerAdapterCreator
                    .Create(_helper)
                    .GetContractors(result.Rows);

                var operations = ExplainInvoiceOperationAdapterCreator
                    .Create(_helper)
                    .GetOperation(result.Rows);

                var buyaccepts = ExplainInvoiceBuyAcceptAdapterCreator
                    .Create(_helper)
                    .GetBuyAccepts(result.Rows);

                var paymentdocuments = ExplainInvoicePaymentDocumentAdapterCreator
                    .Create(_helper)
                    .GetPaymentDocuments(result.Rows);

                var invoiceChanges = ExplainInvoiceAttributeChangeAdapterCreator
                    .Create(_helper)
                    .GetChanges(result.Rows);

                result.Rows
                    .ForEach(invoice =>
                    {
                        invoice.Attributes.Buyers.AddRange(
                            buyers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                        invoice.Attributes.Sellers.AddRange(
                            sellers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                        invoice.Attributes.Operations.AddRange(
                            operations.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                        invoice.Attributes.BuyAccepts.AddRange(
                            buyaccepts.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                        invoice.Attributes.PaymentDocuments.AddRange(
                            paymentdocuments.Where(x => x.InvoiceKey == invoice.ROW_KEY));

                        invoice.SetMultipleAttributes();
                    }
                );

                var rowKeys = new List<string>();
                rowKeys.AddRange(
                    result
                        .Rows
                        .Where(dto => !string.IsNullOrWhiteSpace(dto.ROW_KEY))
                        .Select(dto => dto.ROW_KEY));

                var explainTKSChanges = LoadExplainInvoiceChanges(docId, rowKeys.ToArray());

                var invoiceChangesTks = new ExplainTksFieldConverter()
                    .ConvertToExplainInvoiceChanges(explainTKSChanges);

                invoiceChanges.AddRange(invoiceChangesTks);

                //--- Изменения в СФ
                foreach (var invoice in result.Rows)
                {
                    var corrections = invoiceChanges.Where(p => p.InvoiceId == invoice.ROW_KEY)
                        .Select(p => new InvoiceCorrection()
                        {
                            EXPLAIN_ID = p.ExplainId,
                            INVOICE_ORIGINAL_ID = p.InvoiceId,
                            FIELD_NAME = p.FieldName,
                            FIELD_VALUE = p.FieldValue
                        }).ToList();

                    var orderedExplains = invoiceChanges
                        .Where(p => p.InvoiceId == invoice.ROW_KEY)
                        .OrderBy(p => p.ExplainIncomingDate)
                        .ThenBy(p => p.ExplainId)
                        .Select(p => p.ExplainId)
                        .Distinct();

                    foreach (var explainId in orderedExplains)
                    {
                        var correct = corrections.Where(p => p.EXPLAIN_ID == explainId);
                        var child = invoice.BornChild();
                        child.Correct(correct);
                    }

                    if (invoiceChangesTks.Any(p => p.InvoiceId == invoice.ROW_KEY))
                    {
                        invoice.IS_CHANGED = 1;
                    }
                    else
                    {
                        if (explainTKSChanges.Any(p => p.RowKey == invoice.ROW_KEY))
                        {
                            invoice.IS_CHANGED = 0;
                        }
                    }
                }

                //--- Тул-типы для признака редактирования в пояснении
                var invoiceExplainDatas = LoadInvoiceExplainDatas(rowKeys.ToArray());
                foreach (var invoice in result.Rows)
                {
                    var explainData = invoiceExplainDatas.Where(p => p.InvoiceKey == invoice.ROW_KEY).OrderBy(p => p.Rank).FirstOrDefault();
                    if (explainData != null)
                    {
                        invoice.ExplainNum = explainData.DocumentNumber;
                        invoice.ExplainDate = explainData.DocumentDate;
                        invoice.ExplainType = explainData.DocumentType;
                    }
                }
                return result;
            });
        }

        private void FillEgrnName(List<InvoiceRelated> invoices)
        {
            var contractorNamesBuilder =
                new DiscrepancyDocumentInvoiceBuilder(
                    new TaxPayerNamesLoader(
                        DataAccessService.EgrnIPAdapter(),
                        DataAccessService.EgrnULAdapter()));

            var chapterData = new DiscrepancyDocumentChapterData() { Invoices = invoices };
            chapterData = contractorNamesBuilder.WithEgrn(chapterData);
        }

        private List<InvoiceExplain> LoadInvoiceExplainDatas(string[] rowKeys)
        {
            var filter = rowKeys.ToFilter(TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.ROW_KEY));

            var result = InvoiceExplainViewAdapterCreator.InvoiceExplainViewAdapter(_helper)
                .Search(filter).OrderByDescending(p => p.Rank).ToList();

            return result;
        }
        
        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АТ по КС
        /// </summary>
        /// <param name="docId">Идентификатор АТ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АТ по КС</returns>
        public OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfo(long docId)
        {
            return _helper.Do<DocumentCalculateInfo>(() => { return TableAdapterCreator.DiscrepancyDocument(_helper).GetDocumentCalculateInfo(docId); });
        }

        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АТ по СФ
        /// </summary>
        /// <param name="docId">Идентификатор АТ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АТ по СФ</returns>
        public OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfoClaimSF(long docId)
        {
            return _helper.Do<DocumentCalculateInfo>(() => { return TableAdapterCreator.DiscrepancyDocument(_helper).GetDocumentCalculateInfoClaimSF(docId); });
        }

        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АИ
        /// </summary>
        /// <param name="docId">Идентификатор АИ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АИ</returns>
        public OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfoReclaim(long docId)
        {
            return _helper.Do<DocumentCalculateInfo>(() => { return TableAdapterCreator.DiscrepancyDocument(_helper).GetDocumentCalculateInfoReclaim(docId); });
        }

        public OperationResult<DeclarationSummary> GetDeclaration(long id)
        {
            return _helper.Do<DeclarationSummary>(() => { return TableAdapterCreator.Declaration(_helper).GetDeclaration(id); });
        }

        public OperationResult<List<DocumentStatusHistory>> GetStatusHistories(long docId)
        {
            return _helper.Do<List<DocumentStatusHistory>>(() => { return TableAdapterCreator.DiscrepancyDocument(_helper).GetStatusHistories(docId); });
        }

        public OperationResult<long> GetDiscrepancyId(string invoiceId, int typeCode, long docId)
        {
            return _helper.DoEx<long>(() => { return TableAdapterCreator.DiscrepancyDocument(_helper).GetDiscrepancyId(invoiceId, typeCode, docId); });
        }

        public OperationResult UpdateUserComment(long docId, string userComment)
        {
            return _helper.Do(() => { TableAdapterCreator.DiscrepancyDocument(_helper).UpdateUserComment(docId, userComment); });
        }

        public OperationResult<PageResult<NotReflectedInvoice>> GetNotReflectedInvoices(long docId, QueryConditions qc)
        {
            return _helper.Do(() => TableAdapterCreator.DiscrepancyDocument(_helper).GetNotRefectedInvoices(docId, qc));
        }

        private List<explainTks.ExplainTksInvoiceChanges> LoadExplainInvoiceChanges(
            long docId, string[] rowKeys)
        {
            var filter = FilterExpressionCreator
                            .CreateInList(
                                TypeHelper<explainTks.ExplainTksInvoiceChanges>.GetMemberName(dto => dto.RowKey),
                                rowKeys);

            var result = new List<explainTks.ExplainTksInvoiceChanges>();

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                result = ExplainInvoiceChangesAdapterCreator
                    .Create(_helper, connectionFactory)
                    .Search(filter, new List<ColumnSort>(), docId)
                    .ToList();

                if (result.Any())
                {
                    result.FillAttributesForChanges(_helper, connectionFactory);
                }
            }

            return result;
        }
    }

    public static class DiscrepancyDocumentDataServiceHelper
    {
        public static List<ExplainInvoiceCorrect> GetChanges(
            this IExplainInvoiceAttributeChangeAdapter adapter,
            IEnumerable<InvoiceRelated> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyList(
                        TypeHelper<ExplainInvoiceCorrect>
                            .GetMemberName(x => x.InvoiceId)));
        }

        private static FilterExpressionBase ToInvoiceKeyList(
            this IEnumerable<InvoiceRelated> invoices,
            string invoiceKeyField)
        {
            return FilterExpressionCreator
                .CreateInList(
                    invoiceKeyField,
                    invoices.Select(x => x.ROW_KEY).ToArray());
        }

        private static FilterExpressionBase ToInvoiceKeyListWithDocId(
            this IEnumerable<InvoiceRelated> invoices,
            string invoiceKeyField)
        {
            var rowKeyFilter = FilterExpressionCreator.
                CreateInList(invoiceKeyField,
                invoices.Select(x => x.ROW_KEY).ToArray());

            var docId = invoices.Any() ? invoices.First().DocId : Int64.MinValue;

            return FilterExpressionCreator
                .CreateGroup().WithExpression(docId.ToDocIDFilter()).WithExpression(rowKeyFilter);
        }



        public static List<InvoiceContractor> GetContractors(
            this IExplainInvoiceContractorAdapter adapter,
            IEnumerable<InvoiceRelated> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceContractor>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<InvoiceOperation> GetOperation(
            this IExplainInvoiceOperationAdapter adapter,
            IEnumerable<InvoiceRelated> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceOperation>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<InvoiceBuyAccept> GetBuyAccepts(
            this IExplainInvoiceBuyAcceptAdapter adapter,
            IEnumerable<InvoiceRelated> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceBuyAccept>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<InvoicePaymentDocument> GetPaymentDocuments(
            this IExplainInvoicePaymentDocumentAdapter adapter,
            IEnumerable<InvoiceRelated> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoicePaymentDocument>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        private static FilterExpressionBase SearchAttibutesByForChanges<T>(this IEnumerable<T> invoices)
            where T : explainTks.ExplainTksInvoiceChanges
        {
            return FilterExpressionCreator
                                    .CreateGroup()
                                    .WithExpression(
                                        FilterExpressionCreator.Create(
                                            TypeHelper<explainTks.InvoiceAttribute>
                                                .GetMemberName(x => x.ExplainZip),
                                            ColumnFilter.FilterComparisionOperator.Equals,
                                            invoices.First().ExplainZip))
                                    .WithExpression(
                                        FilterExpressionCreator.CreateInList(
                                            TypeHelper<explainTks.InvoiceAttribute>
                                                .GetMemberName(x => x.InvoiceRowKey),
                                            invoices.Select(x => x.RowKey)));
        }

        public static void FillAttributesForChanges<T>(
            this IEnumerable<T> invoices,
            Luxoft.NDS2.Server.DAL.IServiceProvider service,
            ConnectionFactoryBase connection)
            where T : explainTks.ExplainTksInvoiceChanges
        {
            var filterBy = invoices.SearchAttibutesByForChanges();
            var receiptDocs =
                new InvoiceAttributeLoader<explainTks.InvoiceReceiptDocument>(
                    ExplainTksAdaptersCreator.ReceiptDocuments(service, connection));
            var receiveDates =
                new InvoiceAttributeLoader<explainTks.InvoiceReceiveDate>(
                    ExplainTksAdaptersCreator.ReceiveDates(service, connection));
            var contractors =
                new InvoiceAttributeLoader<explainTks.InvoiceContractor>(
                    ExplainTksAdaptersCreator.Contractors(service, connection));
            receiptDocs.Load(filterBy);
            receiveDates.Load(filterBy);
            contractors.Load(filterBy);

            foreach (var invoice in invoices)
            {
                invoice.ReceiptDocuments = receiptDocs.GetAttributes(invoice.RowKey);
                invoice.ReceiveDates = receiveDates.GetAttributes(invoice.RowKey);
                invoice.Contractors = contractors.GetAttributes(invoice.RowKey);
            }
        }
    }
}
