﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services
{
    internal static class PageResultHelper
    {
        public static PageResult<T> Page<T>(
            this ISearchAdapter<T> adapter,
            Func<int> allAvailableCounter,
            FilterExpressionGroup allMatches,
            QueryConditions query)
        {
            var result = new PageResult<T>();

            if (!query.PaginationDetails.SkipTotalAvailable)
            {
                result.TotalAvailable = allAvailableCounter();
            }

            if (!query.PaginationDetails.SkipTotalMatches)
            {
                result.TotalMatches =
                    query.Filter.Any()
                    ? adapter.Count(allMatches)
                    : (query.PaginationDetails.SkipTotalAvailable ? allAvailableCounter() : result.TotalAvailable);
            }

            result.Rows = 
                adapter.Search(
                    allMatches, 
                    query.Sorting, 
                    query.PaginationDetails.RowsToSkip, 
                    query.PaginationDetails.RowsToTake.Value)
                 .ToList();

            return result;
        }

        public static PageResult<T> Page<T>(
            this ISearchAdapter<T> adapter,
            FilterExpressionBase allAvailable,
            FilterExpressionGroup allMatches,
            QueryConditions query)
        {
            return adapter.Page(() => adapter.Count(allAvailable), allMatches, query);
        }
    }
}
