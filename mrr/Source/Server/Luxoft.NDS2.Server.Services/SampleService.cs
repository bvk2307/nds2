﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Communication;
using CommonComponents.Configuration;
using CommonComponents.ExceptionHandling;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(ISampleService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SampleService : ISampleService
    {
        private ServiceHelpers _helper;
        private IConfigurationDataService _configuration;

        public SampleService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        #region Implementation of ISampleService

        public string PingAdmin(string input)
        {
            return Ping(input, Constants.SystemPermissions.Operations.SystemLogon);
        }

        public string PingInspector(string input)
        {
            return Ping(input, Constants.SystemPermissions.Operations.SystemLogon);
        }

        public string PingManager(string input)
        {
            return Ping(input, Constants.SystemPermissions.Operations.SystemLogon);
        }

        public int PingDatabase()
        {
            DalManager mgr = new DalManager(_helper);
            return mgr.TestConnection();
        }

        public void RaiseExceptionOnServer()
        {
            try
            {
                throw new ApplicationException("тестовая исключительная ситуация");
            }
            catch (Exception ex)
            {
                _helper.LogError(ex.Message, "RaiseExceptionOnServer", ex);   
                throw ex;
            }
        }

        #endregion

        private string  Ping(string input, string roleContext)
        {
//            var authService = _helper.Services.Get<CommonComponents.Security.Authorization.IAuthorizationService>();
//            var perms = authService.GetSubsystemPermissions(System.Threading.Thread.CurrentPrincipal.Identity, Constants.SubsystemName);
//
//            if (_helper.Authorize(roleContext))
//            {
//                _helper.LogWarning(string.Format("authirized as {0}", roleContext), "Ping");
//                return string.Format("pong {0}", input);
//            }
//            else
//            {
//                _helper.LogWarning(string.Format("unauthirized as {0}", roleContext), "Ping");
//            }

            return "unauthorized";
        }


    }
}
