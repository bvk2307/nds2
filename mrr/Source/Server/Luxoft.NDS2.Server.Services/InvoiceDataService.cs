﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.InvoiceData;
using Luxoft.NDS2.Server.Services.InvoiceData.SOV;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Collections.Generic;
using System.Text;
using Luxoft.NDS2.Server.Common;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IInvoiceDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InvoiceDataService : ServiceBase, IInvoiceDataService
    {
        private readonly ServiceHelpers _helper;
        private readonly IAuthorizationProvider _localAuthProvider;

        public InvoiceDataService(IReadOnlyServiceCollection services)
            : base(services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        #region Invoice source

        private InvoiceSource _invoiceSource = InvoiceSource.Undefined;

        private InvoiceSource InvoiceSource
        {
            get
            {
                if (_invoiceSource != InvoiceSource.Undefined)
                    return _invoiceSource;

                var value = _helper.GetConfigurationValue("NDS2.Server.InvoiceSource");
                if (value == null || value.Equals("sov", StringComparison.InvariantCultureIgnoreCase))
                    _invoiceSource = InvoiceSource.Sov;
                else if (value.Equals("impala", StringComparison.InvariantCultureIgnoreCase))
                    _invoiceSource = InvoiceSource.Impala;
                else
                 _invoiceSource = InvoiceSource.Unknown;

                return _invoiceSource;
            }
        }

        #endregion

        #region Получение данных счетов-фактур из раздела декларации

        public OperationResult<DeclarationChapterData> SearchInDeclarationChapter(DeclarationChapterDataRequest request)
        {
            if (InvoiceSource == InvoiceSource.Sov)
            {
                return SearchInDeclarationChapterSovInternal(request);
            }

            if (InvoiceSource == InvoiceSource.Impala)
            {
                return SearchInDeclarationChapterImpalaInternal(request);
            }
            
            return 
                new OperationResult<DeclarationChapterData>
                {
                    Message = "Не удалось определить источник данных", 
                    Status = ResultStatus.Error
                };

        }

        /// <summary>
        /// Поиск через Импалу
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private OperationResult<DeclarationChapterData> SearchInDeclarationChapterImpalaInternal(DeclarationChapterDataRequest request)
        {
            var builder =
                new InvoiceData.Impala.DeclarationChapterDataBuilder(DataAccessService.ImpalaInvoiceAdapter());

            var contractorNamesBuilder =
                new InvoiceBuilder(
                    new TaxPayerNamesLoader(
                        DataAccessService.EgrnIPAdapter(),
                        DataAccessService.EgrnULAdapter()),
                    DataAccessService.InvoiceExplainViewAdapter(),
                    DataAccessService.InvoiceContractorDeclarationAdapter());

            return Helper.Do(
                () =>
                    {
                        LogNotificationBegin(request, "Загрузка СФ (Импала)");

                        var result = contractorNamesBuilder.WithContragentSign(
                            contractorNamesBuilder.WithInvoiceExplain(
                                contractorNamesBuilder.WithEgrn(
                                    builder.Build(request))));

                        LogNotificationEnd(request, result, "Загрузка СФ (Импала) завершена");

                        return result;
                    });
        }


        /// <summary>
        /// Поиск через SOV
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private OperationResult<DeclarationChapterData> SearchInDeclarationChapterSovInternal(DeclarationChapterDataRequest request)
        {
            var builder =
                new DeclarationChapterDataBuilder(
                    DataAccessService.SovInvoiceRequestAdapter(),
                    DataAccessService.SovInvoiceAdapter(),
                    ThriftClient());

            var contractorNamesBuilder =
                new InvoiceBuilder(
                    new TaxPayerNamesLoader(
                        DataAccessService.EgrnIPAdapter(),
                        DataAccessService.EgrnULAdapter()),
                    DataAccessService.InvoiceExplainViewAdapter(),
                    DataAccessService.InvoiceContractorDeclarationAdapter());

            return Helper.Do(
                () =>
                {
                    LogNotificationBegin(request, "Загрузка СФ (СОВ)");

                    var result = contractorNamesBuilder.WithContragentSign(
                        contractorNamesBuilder.WithInvoiceExplain(
                            contractorNamesBuilder.WithEgrn(
                                builder.Build(request))));

                    LogNotificationEnd(request, result, "Загрузка СФ (СОВ) завершена");

                    return result;
                });
        }

        #endregion

        #region Получение набора номеров таможенных деклараций по ключу СФ

        public OperationResult<List<string>> SearchCustomDeclarationNumbers(string invoiceRowKey)
        {
            if (InvoiceSource == InvoiceSource.Unknown)
                return
                    new OperationResult<List<string>>
                    {
                        Message = "Не удалось определить источник данных",
                        Status = ResultStatus.Error
                    };

            return Helper.Do(
                () => Helper.GetInvoiceCustomNumbersAdapter(InvoiceSource).SearchByInvoice(invoiceRowKey)
                );
        }

        #endregion

        #region Logging

        private void LogNotificationBegin(DeclarationChapterDataRequest request, string caption)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("{0}, id декларации(журнала): \"{1}\", номер раздела: \"{2}\", SID пользователя: \"{3}\", имя пользователя: \"{4}\"");
            stringBuilder.Append(", PageIndex = {5}, PageSize = {6}, RowsToSkip = {7}, RowsToTake = {8}");

            string message = string.Format(
                    stringBuilder.ToString(),
                    caption,
                    request.DeclarationId,
                    request.Chapter,
                    _localAuthProvider.CurrentUserSID,
                    _localAuthProvider.CurrentUserName,
                    request.SearchContext.PageIndex,
                    request.SearchContext.PageSize,
                    request.SearchContext.PaginationDetails.RowsToSkip,
                    request.SearchContext.PaginationDetails.RowsToTake
                    );

            _helper.LogNotification(message);
        }

        private void LogNotificationEnd(DeclarationChapterDataRequest request, DeclarationChapterData chapterData, string caption)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("{0}, id декларации(журнала): \"{1}\", номер раздела: \"{2}\", SID пользователя: \"{3}\", имя пользователя: \"{4}\"");
            stringBuilder.Append(", PageIndex = {5}, PageSize = {6}, RowsToSkip = {7}, RowsToTake = {8}");
            stringBuilder.Append(", MatchesQuantity = {9}, Invoices.Count = {10}, UsableData = {11}");

            string message = string.Format(
                    stringBuilder.ToString(),
                    caption,
                    request.DeclarationId,
                    request.Chapter,
                    _localAuthProvider.CurrentUserSID,
                    _localAuthProvider.CurrentUserName,
                    request.SearchContext.PageIndex,
                    request.SearchContext.PageSize,
                    request.SearchContext.PaginationDetails.RowsToSkip,
                    request.SearchContext.PaginationDetails.RowsToTake,
                    chapterData.MatchesQuantity,
                    chapterData.Invoices != null ? chapterData.Invoices.Count : 0,
                    chapterData.UsableData
                    );

            _helper.LogNotification(message);
        }

        #endregion
    }
}
