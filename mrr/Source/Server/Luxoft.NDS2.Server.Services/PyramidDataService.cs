﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract( typeof( IPyramidDataService ) )]
    [CommunicationService( ServiceInstanceMode.PerCall )]
    public class PyramidDataService : IPyramidDataService
    {
        private readonly ServiceHelpers _helper;

        public PyramidDataService( IReadOnlyServiceCollection services )
        {
            Contract.Assert( null != services.Get<IAuthorizationProvider>() );  //it is needed for '_heleper' to check permissions of an operation

            _helper = new ServiceHelpers( services );
        }

        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            OperationResult<IReadOnlyCollection<GraphData>> result = _helper.Do(
                () => TableAdapterCreator.Pyramid( _helper ).LoadGraphNodes( nodesKeyParameters ) );

            return result;
        }

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            OperationResult<IReadOnlyCollection<GraphData>> result = _helper.Do(
                () => TableAdapterCreator.Pyramid( _helper ).LoadReportGraphNodes( nodesKeyParameters ) );

            return result;
        }

        public OperationResult<PageResult<GraphData>> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters )
        {
            OperationResult<PageResult<GraphData>> result = _helper.Do(
                () => TableAdapterCreator.Pyramid( _helper ).LoadGraphNodeChildrenByPage(nodesKeyParameters) );

            return result;
        }

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<DeductionDetail>> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters )
        {
            OperationResult<IReadOnlyCollection<DeductionDetail>> result = _helper.Do(
                () => TableAdapterCreator.Pyramid( _helper ).LoadDeductionDetails( deductionKeyParameters ) );

            return result;
        }

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<string>> LoadTaxpayersKpp( string inn )
        {
            OperationResult<IReadOnlyCollection<string>> result = _helper.Do(
                () => TableAdapterCreator.Pyramid( _helper ).LoadTaxpayersKpp( inn ) );

            return result;
        }
    }
}
