﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL.Selections;

namespace Luxoft.NDS2.Server.Services.Implementation
{
    /// <summary>
    /// Этот класс реализует сервисные методы работы с избранными фильтрами
    /// </summary>
    public class FavoriteFilterServiceDelegate
    {
        # region Поля

        private IIdentityProvider _identityProvider;
        private ISelectionFavoriteFilterAdpater _adapter;
        private ILogProvider _logger;

        # endregion

        # region Конструкторы

        public FavoriteFilterServiceDelegate(
            IIdentityProvider identityProvider,
            ISelectionFavoriteFilterAdpater adapter,
            ILogProvider logger)
        {
            if (identityProvider == null)
            {
                throw new ArgumentNullException("identityProvider");
            }

            if (adapter == null)
            {
                throw new ArgumentNullException("adapter");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            _identityProvider = identityProvider;
            _adapter = adapter;
            _logger = logger;
        }

        # endregion

        # region Реализация сервисных методов

        /// <summary>
        /// Запрашивает избранные фильры текущего пользователя      
        /// </summary>
        /// <returns>Результат выполнения операции</returns>
        public OperationResult<List<KeyValuePair<long, string>>> GetFavoriteFilters()
        {
            return DalHelper.Execute<OperationResult<List<KeyValuePair<long, string>>>>(
                () => new OperationResult<List<KeyValuePair<long, string>>>()
                    {
                        Result = _adapter
                            .Search(_identityProvider.Current())
                            .Select(fav => new KeyValuePair<long, string>(fav.Id, fav.Name))
                            .ToList(),
                        Status = ResultStatus.Success
                    },
                _logger);
        }

        private OperationResult<KeyValuePair<long, string>> CreateAction(string name, Filter filter)
        {
            try
            {
                var obj = new FavoriteFilter()
                {
                    Analyst = _identityProvider.Current(),
                    Name = name.Trim(),
                    FilterVersionTwo = filter
                };

                _adapter.Insert(obj);

                return new OperationResult<KeyValuePair<long, string>>()
                {
                    Status = ResultStatus.Success,
                    Result = new KeyValuePair<long, string>(0, name)
                };
            }
            catch (UniqueKeyViolationException)
            {
                return new OperationResult<KeyValuePair<long, string>>()
                {
                    Status = ResultStatus.ConstraintViolation
                };
            }
        }

        /// <summary>
        /// Добавляет фильтр в избранное.
        /// </summary>
        /// <param name="name">Имя фильтра</param>
        /// <param name="filter">Настройки фильтра</param>
        /// <returns>Результат выполнения операции</returns>
        public OperationResult<KeyValuePair<long, string>> Create(string name, Filter filter)
        {
            return DalHelper.Execute<OperationResult<KeyValuePair<long, string>>>(
                    () => CreateAction(name, filter),
                    _logger
                );
        }

        private OperationResult<Filter> GetFavoriteSelectionFilterVersionTwoAction(long favoriteId)
        {
            var myFavorites = _adapter.Search(_identityProvider.Current());

            if (myFavorites.All(fav => fav.Id != favoriteId))
            {
                return new OperationResult<Filter>()
                {
                    Status = ResultStatus.NoDataFound
                };
            }

            return new OperationResult<Filter>()
            {
                Status = ResultStatus.Success,
                Result = myFavorites.Single(fav => fav.Id == favoriteId).FilterVersionTwo
            };
        }

        /// <summary>
        /// Ищет фильтр по идентификатору
        /// </summary>
        /// <param name="favoriteId">Идентификатор фильтра</param>
        /// <returns>Результат выполнения операции</returns>
        public OperationResult<Filter> GetFavoriteSelectionFilterVersionTwo(long favoriteId)
        {
            return DalHelper.Execute<OperationResult<Filter>>(
                () => GetFavoriteSelectionFilterVersionTwoAction(favoriteId),
                _logger);
        }

        private OperationResult OverwriteAction(string name, Filter filter)
        {
            var myFavorites = _adapter.Search(_identityProvider.Current());

            if (myFavorites.All(fav => fav.Name != name.Trim()))
            {
                return new OperationResult()
                {
                    Status = ResultStatus.NoDataFound
                };
            }

            var filterToUpdate = myFavorites.Single(fav => fav.Name == name.Trim());
            filterToUpdate.FilterVersionTwo = filter;
            _adapter.Update(filterToUpdate);

            return new OperationResult()
            {
                Status = ResultStatus.Success
            };
        }

        /// <summary>
        /// Меняет настройки фильтра в избранном
        /// </summary>
        /// <param name="name">Имя фильтра в избранном</param>
        /// <param name="filter">Новые настройки фильтра</param>
        /// <returns>Результат выполнения операции</returns>
        public OperationResult Overwrite(string name, Filter filter)
        {
            return DalHelper.Execute<OperationResult>(() => OverwriteAction(name, filter), _logger);
        }

        private OperationResult DeleteAction(long id)
        {
            var myFavorites = _adapter.Search(_identityProvider.Current());

            if (myFavorites.All(fav => fav.Id != id))
            {
                return new OperationResult()
                {
                    Status = ResultStatus.NoDataFound
                };
            }

            _adapter.Delete(myFavorites.Single(fav => fav.Id == id));

            return new OperationResult()
            {
                Status = ResultStatus.Success
            };
        }

        /// <summary>
        /// Удаляет фильтр из избранного
        /// </summary>
        /// <param name="id">Идентификатор фильтра в избранном</param>
        /// <returns>Результат выполнения операции</returns>
        public OperationResult Delete(long id)
        {
            return DalHelper.Execute<OperationResult>(() => DeleteAction(id), _logger);
        }

        # endregion
    }
}
