﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Server.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Server.Services.Implementation
{
    public class StateSelectionServiceDelegate
    {
        # region Поля

        private IIdentityProvider _identityProvider;
        private IStateTransitionAdapter _adapter;
        private ILogProvider _logger;

        # endregion

        # region Конструкторы

        public StateSelectionServiceDelegate(
            IIdentityProvider identityProvider,
            IStateTransitionAdapter adapter,
            ILogProvider logger)
        {
            if (identityProvider == null)
            {
                throw new ArgumentNullException("identityProvider");
            }

            if (adapter == null)
            {
                throw new ArgumentNullException("adapter");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            _identityProvider = identityProvider;
            _adapter = adapter;
            _logger = logger;
        }

        # endregion

        public List<SelectionTransition> GetStateTransitions()
        {
            return _adapter.GetStateTransitions();
        }
    }
}
