﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System;

namespace Luxoft.NDS2.Server.Services.Implementation
{
    public class SelectionChecksServiceDelegate
    {
        private ISelectionChecksAdapter _adapter;
        private ILogProvider _logger;

        public SelectionChecksServiceDelegate(
            ISelectionChecksAdapter adapter,
            ILogProvider logger
            )
        {
            if (adapter == null)
            {
                throw new ArgumentNullException("adapter");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            _adapter = adapter;
            _logger = logger;
        }

        public OperationResult IncludeDeclaration(long requestId, long declarationId)
        {
            return DalHelper.Execute(
                () => _adapter.DeclarationCheckState(requestId, declarationId, true),
                _logger);
        }

        public OperationResult ExcludeDeclaration(long requestId, long declarationId)
        {
            return DalHelper.Execute(
                () => _adapter.DeclarationCheckState(requestId, declarationId, false),
                _logger);
        }

        public OperationResult IncludeDiscrepancy(long requestId, long discrepancyId)
        {
            return DalHelper.Execute(
                () => _adapter.DiscrepancyCheckState(requestId, discrepancyId, true),
                _logger);
        }

        public OperationResult ExcludeDiscrepancy(long requestId, long discrepancyId)
        {
            return DalHelper.Execute(
                () => _adapter.DiscrepancyCheckState(requestId, discrepancyId, false),
                _logger);
        }

        public PrimitiveOperationResult<SelectionGroupState> DeclarationsState(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute<PrimitiveOperationResult<SelectionGroupState>>(
                () => new PrimitiveOperationResult<SelectionGroupState>()
                        {
                            Result = _adapter.GetDeclarationsGroupState(requestId, criteria),
                            Status = ResultStatus.Success
                        },
                _logger);
        }

        public OperationResult IncludeDeclarations(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute(
                () => _adapter.IncludeDeclarations(requestId, criteria),
                _logger);
                
        }

        public OperationResult ExcludeDeclarations(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute(
                () => _adapter.ExcludeDeclarations(requestId, criteria),
                _logger);
        }

        public PrimitiveOperationResult<SelectionGroupState> DiscrepanciesState(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute<PrimitiveOperationResult<SelectionGroupState>>(
                () => new PrimitiveOperationResult<SelectionGroupState>()
                {
                    Result = _adapter.GetDiscrepanciesGroupState(requestId, criteria),
                    Status = ResultStatus.Success
                },
                _logger);
        }

        public OperationResult IncludeDiscrepancies(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute(
                () => _adapter.IncludeDiscrepancies(requestId, criteria),
                _logger);

        }

        public OperationResult ExcludeDiscrepancies(long requestId, QueryConditions criteria = null)
        {
            return DalHelper.Execute(
                () => _adapter.ExcludeDiscrepancies(requestId, criteria),
                _logger);
        }
    }
}
