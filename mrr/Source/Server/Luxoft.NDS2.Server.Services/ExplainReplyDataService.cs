﻿using System;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Explain;
using Luxoft.NDS2.Server.DAL.UserTask;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.ExplainProcessedMC;
using Luxoft.NDS2.Server.Services.UserAccess;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IExplainReplyDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ExplainReplyDataService : IExplainReplyDataService
    {
        private readonly ServiceHelpers _helper;

        public ExplainReplyDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<ExplainReplyInfo> GetExplainReplyInfo(long id)
        {
            return DalHelper.Execute(
                () => new OperationResult<ExplainReplyInfo>
                {
                    Result = TableAdapterCreator.ExplainReply(_helper).GetExplainReplyInfo(id),
                    Status = ResultStatus.Success
                },
                _helper
            );
        }

        public OperationResult<List<ExplainReplyInfo>> GetExplainReplyList(long docId, QueryConditions conditions)
        {
            return DalHelper.Execute(
                () => new OperationResult<List<ExplainReplyInfo>>
                {
                    Result = TableAdapterCreator.ExplainReply(_helper).GetExplainReplyList(docId, conditions),
                    Status = ResultStatus.Success
                },
                _helper
            );
        }

        public OperationResult<List<ExplainReplyInfo>> GetListExplainsOfDocument(long docId)
        {
            return DalHelper.Execute(
                () => new OperationResult<List<ExplainReplyInfo>>
                {
                    Result = TableAdapterCreator.ExplainReply(_helper).GetListExplainsOfDocument(docId),
                    Status = ResultStatus.Success
                },
                _helper
            );
        }

        public OperationResult<bool> IsPreviousExplainsOfDocumentInWorked(ExplainReplyInfo explainReplyInfo)
        {
            return _helper.DoEx(
                () =>
                {
                    var ret = false;

                    var explainsOfDocument = TableAdapterCreator
                        .ExplainReply(_helper)
                        .GetListExplainsOfDocument(explainReplyInfo.DOC_ID);

                    var countPreviousExplain = explainsOfDocument
                        .Where(p => (p.EXPLAIN_ID != explainReplyInfo.EXPLAIN_ID) &&
                                    ((p.INCOMING_DATE < explainReplyInfo.INCOMING_DATE) ||
                                     ((p.INCOMING_DATE == explainReplyInfo.INCOMING_DATE) &&
                                      (p.EXPLAIN_ID < explainReplyInfo.EXPLAIN_ID))))
                        .Count();

                    if (countPreviousExplain == 0)
                    {
                        ret = true;
                    }
                    else
                    {
                        var countPreviousExplainNotProcessed = explainsOfDocument
                            .Where(p => (p.EXPLAIN_ID != explainReplyInfo.EXPLAIN_ID) &&
                                        ((p.INCOMING_DATE < explainReplyInfo.INCOMING_DATE) ||
                                         ((p.INCOMING_DATE == explainReplyInfo.INCOMING_DATE) &&
                                          (p.EXPLAIN_ID < explainReplyInfo.EXPLAIN_ID))) &&
                                        (p.status_id != ExplainReplyStatus.NotProcessed))
                            .Count();

                        if (countPreviousExplainNotProcessed > 0) ret = true;
                    }
                    return ret;
                });
        }

        [Obsolete]
        public OperationResult UpdateUserComment(long explainId, string userComment)
        {
            return UpdateComment(explainId, ExplainType.Invoice, userComment);
        }

        public OperationResult<List<ExplainReplyHistoryStatus>> GetExplainReplyHistoryStatues(long explain_id)
        {
            return DalHelper.Execute(
                () => new OperationResult<List<ExplainReplyHistoryStatus>>
                {
                    Result = TableAdapterCreator.ExplainReply(_helper).GetExplainReplyHistoryStatues(explain_id),
                    Status = ResultStatus.Success
                },
                _helper
            );
        }

        public OperationResult<DeclarationSummary> GetDeclaration(long id)
        {
            return _helper.Do(() => { return TableAdapterCreator.Declaration(_helper).GetDeclaration(id); });
        }

        public OperationResult UpdateInvoiceCorrect(long explainId, ExplainInvoiceCorrect correct,
            InvoiceCorrectTypeOperation typeOperation)
        {
            return
                _helper.Do(
                    () =>
                    {
                        TableAdapterCreator.ExplainReply(_helper)
                            .UpdateInvoiceCorrect(explainId, correct, typeOperation);
                    });
        }

        public OperationResult UpdateExplainStatus(long explainId, int explainStatusId)
        {
            return
                _helper.Do(
                    () => { TableAdapterCreator.ExplainReply(_helper).UpdateExplainStatus(explainId, explainStatusId); });
        }

        public OperationResult UpdateInvoiceCorrectState(long explainId, string invoiceId,
            ExplainInvoiceStateInThis state)
        {
            return
                _helper.Do(
                    () =>
                    {
                        TableAdapterCreator.ExplainReply(_helper).UpdateInvoiceCorrectState(explainId, invoiceId, state);
                    });
        }

        public OperationResult DeleteInvoiceCorrectState(long explainId, string invoiceId)
        {
            return
                _helper.Do(
                    () => { TableAdapterCreator.ExplainReply(_helper).DeleteInvoiceCorrectState(explainId, invoiceId); });
        }

        public OperationResult DeleteAllInvoiceCorrectState(long explainId)
        {
            return
                _helper.Do(() => { TableAdapterCreator.ExplainReply(_helper).DeleteAllInvoiceCorrectState(explainId); });
        }

        public OperationResult<bool> ExistsInvoiceChangeOfExplain(long explainId)
        {
            return _helper.DoEx(
                () => { return TableAdapterCreator.ExplainReply(_helper).ExistsInvoiceChangeOfExplain(explainId); });
        }

        /// <summary>
        ///     Отправка пояснения в МС
        /// </summary>
        /// <param name="explainId"></param>
        /// <param name="declarationId"></param>
        /// <returns></returns>
        public OperationResult SendExplainXMLtoMC(long explainId, long declarationId)
        {
            var result = _helper.Do(
                () =>
                {
                    var zipContainerManager = new ExplainZipContainerManager(_helper);
                    zipContainerManager.SendToMC(explainId, declarationId);
                    TableAdapterCreator.DeclarationForInspector(_helper).DownNoTks(declarationId);
                });
            if (result.Status == ResultStatus.Success)
                _helper.Do(() =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        UserTaskAdapterCreator.UpdateStatusAdapter(_helper, connection)
                            .CompleteReclaimReplyTask(explainId);
                    }
                });
            return result;
        }

        /// <summary>
        ///     Делает апдейт комментария
        /// </summary>
        /// <param name="explainId">id пояснения</param>
        /// <param name="type">тип пояснения</param>
        /// <param name="text">комментарий</param>
        /// <returns></returns>
        public OperationResult UpdateComment(long explainId, ExplainType type, string text)
        {
            return _helper.Do(() =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        ExplainAdapterCreator.ExplainCommentAdapter(_helper, connection)
                            .Update(explainId, type, text);
                    }
                }
            );
        }

        public OperationResult<List<ExplainControlRatio>> GetExplainControlRatio(long zip)
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    return ExplainAdapterCreator.ExplainControlRatioAdapter(_helper, connection)
                        .SelectByExplain(zip);
                }
            }
            );
        }


        public OperationResult<AccessContext> GetAccessContext()
        {
            return _helper.Do(
                () => _helper.GetAccessContext(
                    new[]
                    {
                        MrrOperations.ExplainReply.ExplainEdit,
                        MrrOperations.ExplainReply.ExplainView
                    }));
        }

        /// <summary>
        /// Возвращает пояснения по идентификатору АТ
        /// </summary>
        /// <param name="claimId">Идентификатор АТ</param>
        /// <returns>Массив данных пояснений</returns>
        public OperationResult<List<ClaimExplain>> SearchByClaim(long claimId)
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    return ExplainAdapterCreator.ClaimExplainAdapter(_helper, connection).SearchExplainsByClaim(claimId);
                }
            }
           );
        }

        /// <summary>
        /// Возвращает данные пояснения
        /// </summary>
        /// <param name="id"></param>
        /// <param name="explainType"></param>
        /// <returns></returns>
        public OperationResult<ExplainDetailsData> GetExplainDetails(long id, ExplainType explainType)
        {
            return _helper.Do(() =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var comment = ExplainAdapterCreator.ExplainCommentAdapter(_helper, connection)
                            .Search(id, explainType);
                        var details = ExplainAdapterCreator.ExplainAdapter(_helper, connection).Search(id);
                        details.Comment = comment;
                        return details;
                    }
                }
            );
        }
    }
}