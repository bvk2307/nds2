﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.KnpResultDocuments
{
    public class DiscrepancyPageLoader
    {
        private readonly IDocumentDiscrepancyAdapter _adapter;

        private readonly TaxPayerNamesLoader _namesLoader;

        public DiscrepancyPageLoader(IDocumentDiscrepancyAdapter adapter, TaxPayerNamesLoader namesLoader)
        {
            _adapter = adapter;
            _namesLoader = namesLoader;
        }

        public KnpResultDiscrepancyPage Load(long documentId, QueryConditions queryParameters)
        {
            var result = new KnpResultDiscrepancyPage();
            var filterByDocument = KnpResultDiscrepancyServiceHelper.FilterByDocumentId(documentId);
            var filterBy =
                FilterExpressionCreator
                    .CreateGroup()
                    .WithExpression(filterByDocument)
                    .WithExpression(queryParameters.Filter.ToFilterGroup());

            result.TotalSummary =
                queryParameters.PaginationDetails.SkipTotalAvailable
                    ? new KnpResultDiscrepancySummary()
                    : _adapter.Count(filterByDocument);

            result.MatchesSummary =
                queryParameters.PaginationDetails.SkipTotalMatches
                    ? new KnpResultDiscrepancySummary()
                    : (!queryParameters.PaginationDetails.SkipTotalAvailable 
                            && !queryParameters.Filter.Any()) 
                        ? result.TotalSummary 
                        : _adapter.Count(filterBy);

            result.Discrepancies =
                _adapter
                    .Search(
                        filterBy,
                        queryParameters.Sorting,
                        queryParameters.PaginationDetails.RowsToTake.Value,
                        queryParameters.PaginationDetails.RowsToSkip)
                    .ToArray();

            var taxPayerNames =
                _namesLoader
                    .Load(result.Discrepancies.Select(x => x.ContractorInn).Distinct().ToArray())
                    .GroupBy(x => x.Inn)
                    .Select(x => x.First())
                    .ToDictionary(x => x.Inn, x => x.Name);

            foreach (var discrepancy in result.Discrepancies)
            {
                if (taxPayerNames.ContainsKey(discrepancy.ContractorInn))
                {
                    discrepancy.ContractorName = taxPayerNames[discrepancy.ContractorInn];
                }
            }

            return result;
        }
    }    
}
