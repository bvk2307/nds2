﻿using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL;
using System;
using System.Transactions;
using TransactionScopeHelper = Luxoft.NDS2.Server.Common.Helpers.TransactionScopeHelper;

namespace Luxoft.NDS2.Server.Services.KnpResultDocuments
{
    public abstract class AppendDocumentDiscrepancyService : DocumentDiscrepancyServiceBase, IAppendDocumentDiscrepancyService
    {
        protected AppendDocumentDiscrepancyService(IReadOnlyServiceCollection services)
            : base(services)
        {
        }

        public OperationResult<int> StartSession(long documentId)
        {
            return _helper.DoEx(() =>
                {
                    if (!CheckSonoRestrictions())
                        throw new Exception(SONORestrictionsExeptionText);

                    using (var transaction = TransactionScopeHelper.DefaultTransactionScope())
                    {
                        var adapterFactory = AdapterFactory();
                        var sessionAdapter = adapterFactory.SessionAdapter();
                        var discrepancyAdapter = adapterFactory.AppendDicrepancyAdapter();
                        var sessionDurationSeconds = sessionAdapter.GetDuration();
                        EditSession session;

                        if (sessionAdapter.TryGet(documentId, out session))
                        {
                            return sessionDurationSeconds;
                        }

                        session =
                            new EditSession
                                {
                                    DocumentId = documentId,
                                    ExpiredAt = DateTime.Now.AddSeconds(sessionDurationSeconds),
                                    UserName = _localAuthProvider.CurrentUserName,
                                    UserSid = _localAuthProvider.CurrentUserSID
                                };

                        if (!UpdateStatusEnabled(adapterFactory.UpdateStatusAdapter()))
                            RaiseOperationUnavailableException();

                        discrepancyAdapter.CopyAll(documentId);
                        sessionAdapter.Insert(session);


                        if (!UpdateStatusEnabled(adapterFactory.UpdateStatusAdapter()))
                        {
                            RaiseOperationUnavailableException();
                        }
                        else
                        {
                            transaction.Complete();
                        }

                        return sessionDurationSeconds;
                    }
                });
        }

        public OperationResult<int> ExtendSession(long documentId)
        {
            return _helper.DoEx(() =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var sessionAdapter = AdapterFactory().SessionAdapter();
                        var sessionDurationSeconds = sessionAdapter.GetDuration();
                        EditSession session;

                        if (sessionAdapter.TryGet(documentId, out session))
                        {
                            session.ExpiredAt = session.ExpiredAt.AddSeconds(sessionDurationSeconds);
                            sessionAdapter.Update(session);

                            return sessionDurationSeconds;
                        }

                        throw new Exception("Сессия устарела и была удалена");
                    }
                });
        }

        public OperationResult Commit(long documentId)
        {
            return CommitOrRollback(documentId, adapter => adapter.CommitAll(documentId));
        }

        public OperationResult Rollback(long documentId)
        {
            return CommitOrRollback(documentId, adapter => adapter.RollbackAll(documentId));
        }

        protected virtual OperationResult CommitOrRollback(long documentId, Action<IAppendDocumentDiscrepancyAdapter> action)
        {
            return _helper.Do(() => 
            {
                using (var transaction = new TransactionScope())
                {
                    var adapterFactory = AdapterFactory();
                    var sessionAdapter = adapterFactory.SessionAdapter();
                    var discrepancyAdapter = adapterFactory.AppendDicrepancyAdapter();

                    action(adapterFactory.AppendDicrepancyAdapter());
                    sessionAdapter.Delete(documentId);
                    transaction.Complete();
                }
            });
        }

        protected override IDocumentDiscrepancyAdapter DiscrepancyAdapter(ConnectionFactoryBase connection)
        {
            return AdapterFactory().AppendDicrepancyAdapter();
        }
    }
}
