﻿using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL;
using System;
using System.Transactions;
using TransactionScopeHelper = Luxoft.NDS2.Server.Common.Helpers.TransactionScopeHelper;

namespace Luxoft.NDS2.Server.Services.KnpResultDocuments
{
    public abstract class EditDocumentDiscrepancyService : DocumentDiscrepancyServiceBase, IEditDocumentDiscrepancyService
    {
        protected EditDocumentDiscrepancyService(IReadOnlyServiceCollection services)
            : base(services)
        {
        }

        public OperationResult CloseDocument(long documentId)
        {
            return _helper.Do(() =>
                {
                    using (var transaction = TransactionScopeHelper.DefaultTransactionScope())
                    {
                        var adapterFactory = AdapterFactory();
                        var statusAdapter = adapterFactory.UpdateStatusAdapter();
                        var adapter = adapterFactory.EditDiscreancyAdapter();

                        if (ExistInvalidDiscrepancy(adapter, documentId))
                            RaiseExistInvalidDiscrepancyException();

                        if (!CloseDocumentEnabled(statusAdapter))
                            RaiseOperationUnavailableException();

                        adapterFactory.DocumentAdapter().Update(documentId, DateTime.Now);
                        adapter.DeleteAllClosed(documentId);
                        adapter.CloseDocument(documentId);

                        if (!CloseDocumentEnabled(statusAdapter))
                        {
                            RaiseOperationUnavailableException();
                        }
                        else
                        {
                            transaction.Complete();
                        }
                    }
                });
        }

        protected abstract bool CloseDocumentEnabled(IUpdateDecisionStatusAdapter statusAdapter);

        public OperationResult Update(long id, decimal newAmount)
        {
            return _helper.Do(() =>
            {
                AdapterFactory().EditDiscreancyAdapter().UpdateAmount(id, newAmount);
            });
        }

        public OperationResult<int> CountAllInvalid(long documentId)
        {
            return _helper.DoEx(() =>
            {
                return AdapterFactory().EditDiscreancyAdapter().CountAllInvalid(documentId);
            });
        }

        protected override IDocumentDiscrepancyAdapter DiscrepancyAdapter(ConnectionFactoryBase connection)
        {
            return AdapterFactory().EditDiscreancyAdapter();
        }

        protected override OperationResult IncludeOrExclude(long documentId, long discrepancyId, bool newState)
        {
            return _helper.Do(() =>
            {
                if (!newState)
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = (IEditDocumentDiscrepancyAdapter)DiscrepancyAdapter(connection);

                        adapter.RemoveDiscrepancy(documentId, discrepancyId);
                    }
                }
                else
                    throw new NotImplementedException("For edit, include is not available");
            });
        }

        # region Проверка наличия невалидных расхождений

        protected bool ExistInvalidDiscrepancy(IEditDocumentDiscrepancyAdapter adapter, long documentId)
        {
            return adapter.CountAllInvalid(documentId) > 0;
        }

        protected void RaiseExistInvalidDiscrepancyException()
        {
            throw new Exception("Операция недоступна, в документе присутствуют невалидные расхождения. Обновите данные.");
        }

        # endregion

    }
}
