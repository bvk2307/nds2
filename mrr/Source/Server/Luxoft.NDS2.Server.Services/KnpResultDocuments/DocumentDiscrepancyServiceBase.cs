﻿using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Entities;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.KnpResultDocuments
{
    public abstract class DocumentDiscrepancyServiceBase : IDocumentDiscrepancyService
    {
        # region Константы

        private const bool Included = true;

        private const bool Excluded = false;

        # endregion

        # region Конструктор

        protected readonly ServiceHelpers _helper;

        protected readonly IAuthorizationProvider _localAuthProvider;

        protected DocumentDiscrepancyServiceBase(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        # endregion 

        # region Фабрика адаптеров

        protected abstract IDocumentAdapterFactory AdapterFactory();

        # endregion

        # region Реализация IDocumentDiscrepancyService

        public OperationResult<long> CreateDocument(string innDeclarant, string inn, string kpp, string kppEffective, int year, string periodCode)
        {
            return _helper.DoEx<long>(() =>
            {
                if (!CheckSonoRestrictions())
                    throw new Exception(SONORestrictionsExeptionText);
                
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var adapter = AdapterFactory().DocumentAdapter();

                    return adapter.Insert(innDeclarant, inn, kpp, kppEffective, year, periodCode);
                }
            });
        }

        public OperationResult<KnpResultDiscrepancyPage> Search(long documentId, QueryConditions queryConditions)
        {
            return _helper.Do<KnpResultDiscrepancyPage>(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return new DiscrepancyPageLoader(
                            AdapterFactory().ReadonlyDiscrepancyAdapter(),
                            NamesLoader())
                            .Load(documentId, queryConditions);
                    }
                });
        }

        public OperationResult<KnpResultDiscrepancyPage> SearchUncommited(long documentId, QueryConditions queryConditions)
        {
            return _helper.Do<KnpResultDiscrepancyPage>(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return new DiscrepancyPageLoader(
                            DiscrepancyAdapter(connection),
                            NamesLoader())
                            .Load(documentId, queryConditions);
                    }
                });
        }

        private TaxPayerNamesLoader NamesLoader()
        {
            return new TaxPayerNamesLoader(
                EgrnAdapterCreator.EgrnIPAdapter(_helper),
                EgrnAdapterCreator.EgrnULAdapter(_helper));
        }

        public OperationResult Include(long documentId, long id)
        {
            return IncludeOrExclude(documentId, id, Included);
        }

        public OperationResult Exclude(long documentId, long id)
        {
            return IncludeOrExclude(documentId, id, Excluded);
        }

        protected virtual OperationResult IncludeOrExclude(long documentId, long discrepancyId, bool newState)
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    DiscrepancyAdapter(connection)
                        .UpdateIncluded(
                            KnpResultDiscrepancyServiceHelper.FilterByDiscrepancyId(discrepancyId),
                            newState);
                }
            });
        }

        public OperationResult<KnpResultDiscrepancySummary> IncludeAll(long documentId, FilterQuery[] criteria)
        {
            return IncludeOrExcludeAll(documentId, Included, criteria ?? new FilterQuery[0]);
        }

        public OperationResult<KnpResultDiscrepancySummary> ExcludeAll(long documentId, FilterQuery[] criteria)
        {
            return IncludeOrExcludeAll(documentId, Excluded, criteria ?? new FilterQuery[0]);
        }

        private OperationResult<KnpResultDiscrepancySummary> IncludeOrExcludeAll(long documentId, bool newState, FilterQuery[] filterBy)
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var filterByDocument = KnpResultDiscrepancyServiceHelper.FilterByDocumentId(documentId);
                    var filterByAllCriterias =
                        !filterBy.Any()
                            ? filterByDocument
                            : FilterExpressionCreator
                                .CreateGroup()
                                .WithExpression(filterByDocument)
                                .WithExpression(filterBy.ToFilterGroup());
                    var adapter = DiscrepancyAdapter(connection);

                    adapter.UpdateIncluded(filterByAllCriterias, newState);

                    return adapter.Count(filterByDocument);
                }
            });
        }

        /// <summary>
        /// Возвращает адаптер расхождений в составе документа
        /// </summary>
        /// <returns>Ссылка на адаптер</returns>
        protected abstract IDocumentDiscrepancyAdapter DiscrepancyAdapter(ConnectionFactoryBase connection);

        # endregion

        # region Проверка доступности функционала

        protected bool UpdateStatusEnabled(IUpdateDecisionStatusAdapter adapter)
        {
            return adapter.GetStatus() == 1;
        }

        protected void RaiseOperationUnavailableException()
        {
            throw new Exception("Операция временно недоступа по причине проведения технических работ. Повторите попытку позднее.");
        }

        protected bool CheckSonoRestrictions()
        {
            bool ret = false;

            var dalManager = new DalManager(_helper);

            var allowedSONO = dalManager.GetTableData<SonoLimitAIR>("SONO_LIMIT_AIR").Select(x => x.SONO_CODE).ToList();

            if (allowedSONO.Any())
            {
                var userSONO = _localAuthProvider.GetUserStructContexts(PermissionType.Operation, Constants.SystemPermissions.Operations.SystemLogon).ToList();

                if (userSONO.Intersect(allowedSONO).Any())
                    ret = true;
            }
            else
                ret = true;

            return ret;
        }

        protected const string SONORestrictionsExeptionText = "Данный функционал доступен только пользователям, участвующим в опытной эксплуатации";

        # endregion
    }
}
