﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Services.KnpResultDocuments
{
    internal static class KnpResultDiscrepancyServiceHelper
    {
        public static FilterExpressionBase FilterByDocumentId(long documentId)
        {
            return FilterExpressionCreator.Create(
                TypeHelper<KnpResultDiscrepancy>.GetMemberName(x => x.DocumentId),
                ColumnFilter.FilterComparisionOperator.Equals,
                documentId);
        }

        public static FilterExpressionBase FilterByDiscrepancyId(long discrepancyId)
        {
            return FilterExpressionCreator.Create(
                TypeHelper<KnpResultDiscrepancy>.GetMemberName(x => x.Id),
                ColumnFilter.FilterComparisionOperator.Equals,
                discrepancyId);
        }
    }
}
