﻿using System.Threading;

namespace Luxoft.NDS2.Server.Services
{
    /// <summary>
    /// Этот класс реализует интерфейс IIdentityProvider
    /// </summary>
    internal class IdentityProvider : IIdentityProvider
    {
        public string Current()
        {
            return Thread.CurrentPrincipal.Identity.Name;
        }
    }
}
