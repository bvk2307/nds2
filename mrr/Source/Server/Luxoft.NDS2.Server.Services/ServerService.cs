﻿using CommonComponents.Shared;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Luxoft.NDS2.Server.Services
{
    public class ServerService
    {
        private const string Address = "http://localhost:9601/";

        private readonly IReadOnlyServiceCollection _serviceCollection;

        private ServiceHost _host;

        private ServiceHelpers _helper;

        public ServerService(IReadOnlyServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
        }

        public void Start()
        {
            _helper = new ServiceHelpers(_serviceCollection);

            _helper.Do(() =>
            {
                var url = new Uri(Address);

                var service = new HrzAuthService(_serviceCollection);

                _host = new ServiceHost(service, url);
                var behavior = new WebHttpBehavior();
                var binding = new WebHttpBinding();
                var ep = _host.AddServiceEndpoint(typeof(NDS2.Common.Contracts.Services.IHrzAuthService), binding,
                    Address);

                ep.Behaviors.Add(behavior);
                _host.Open();
            });
        }

        public void Shutdown()
        {
            _helper.Do(() => _host.Close());
        }
    }

}
