﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.ExplainReply;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.DAL;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IExplainInvoiceService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ExplainInvoiceService : IExplainInvoiceService
    {
        private readonly ServiceHelpers _serviceContext;

        public ExplainInvoiceService(IReadOnlyServiceCollection serviceLocator)
        {
            _serviceContext = new ServiceHelpers(serviceLocator);
        }

        public OperationResult<PageResult<ExplainInvoice>> GetAllClaimInvoices(long explainId, int chapter, QueryConditions query)
        {
            return _serviceContext.Do(() =>
            {
                var result = ExplainInvoiceAdapterCreator.AllInvoices(_serviceContext).GetInvoices(explainId, chapter, query);
                FillInvoices(explainId, result.Rows, ExplainRegim.Edit);
                return result;
            });
        }

        public OperationResult<PageResult<ExplainInvoice>> GetModifiedInvoices(long explainId, int chapter, QueryConditions query)
        {
            return _serviceContext.Do(() =>
            {
                var result = ExplainInvoiceAdapterCreator.ModifiedInvoices(_serviceContext).GetInvoices(explainId, chapter, query);
                FillInvoices(explainId, result.Rows, ExplainRegim.View);
                return result;
            });
        }

        public OperationResult<ExplainInvoice> GetOneExplainInvoice(ExplainReplyInfo explainInfo, ExplainRegim regim, string invoiceId)
        {
            return _serviceContext.Do(() =>
            {
                var invoice = TableAdapterCreator.ExplainReply(_serviceContext).GetOneExplainInvoice(explainInfo, regim, invoiceId);
                var invoices = new List<ExplainInvoice>
                {
                    invoice
                };
                FillInvoices(explainInfo.EXPLAIN_ID, invoices, ExplainRegim.Edit);
                return invoice;
            });
        }

        private void FillInvoices(long explainId, List<ExplainInvoice> invoices, ExplainRegim regim)
        {
            var invoiceStateHistory = ExplainInvoiceStateAdapterCreator.Create(_serviceContext).GetStates(invoices);

            var invoiceChanges = ExplainInvoiceAttributeChangeAdapterCreator.Create(_serviceContext).GetChanges(invoices);

            var buyers = ExplainInvoiceBuyerAdapterCreator.Create(_serviceContext).GetContractors(invoices);

            var sellers = ExplainInvoiceSellerAdapterCreator.Create(_serviceContext).GetContractors(invoices);

            var operations = ExplainInvoiceOperationAdapterCreator.Create(_serviceContext).GetOperation(invoices);

            var buyaccepts = ExplainInvoiceBuyAcceptAdapterCreator.Create(_serviceContext).GetBuyAccepts(invoices);

            var paymentdocuments = ExplainInvoicePaymentDocumentAdapterCreator.Create(_serviceContext).GetPaymentDocuments(invoices);


            invoices.ForEach(invoice =>
            {
                invoice.Attributes.Buyers.AddRange(buyers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                invoice.Attributes.Sellers.AddRange(sellers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                invoice.Attributes.Operations.AddRange(operations.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                invoice.Attributes.BuyAccepts.AddRange(buyaccepts.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                invoice.Attributes.PaymentDocuments.AddRange(paymentdocuments.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                invoice.RefreshProperties();
                FillInvoiceState(explainId, regim, invoice, invoiceStateHistory);
                FillInvoiceRelated(invoice, invoiceChanges);
                FillInvoicePreviousInvoiceChange(explainId, invoice, invoiceChanges);
            });
        }

        private void FillInvoiceState(long explainId, ExplainRegim regim, ExplainInvoice invoice, List<ExplainInvoiceCorrectState> allStates)
        {
            var invoiceStates = allStates.Where(v => v.InvoiceId == invoice.ROW_KEY).ToList();
            var lastState = invoiceStates.SingleOrDefault(v => v.ExplainId == explainId);
            if (lastState != null)
            {
                SetStateOfThisInvoice(invoice, lastState);
            } 
            else if (regim == ExplainRegim.Edit)
            {
                var previousStates = invoiceStates.Where(v => v.ExplainId < explainId).ToList();
                if (previousStates.Count > 0)
                {
                    SetStateOfPreviousInvoice(invoice, previousStates);
                }
                else
                {
                    var tksStates = allStates.Where(v => v.ExplainId > explainId && v.RECEIVE_BY_TKS).ToList();
                    SetStateOfPreviousInvoice(invoice, tksStates);
                }
            }
        }

        private void FillInvoiceRelated(ExplainInvoice invoice, List<ExplainInvoiceCorrect> allChanges)
        {
            invoice.RelatedInvoices = new List<ExplainInvoice>();
            if (invoice.State == ExplainInvoiceState.ChangingInThisExplain || invoice.State == ExplainInvoiceState.ChangingInPreviousExplain)
            {
                ExplainInvoice relatedInvoice = BuildLastModifiedInvoice(invoice, allChanges);
                relatedInvoice.Parent = invoice;
                invoice.RelatedInvoices.Add(relatedInvoice);
            }
        }

        private void FillInvoicePreviousInvoiceChange(long explainId, ExplainInvoice invoice, List<ExplainInvoiceCorrect> allChanges)
        {
            if (invoice.State == ExplainInvoiceState.ChangingInThisExplain || invoice.State == ExplainInvoiceState.ChangingInPreviousExplain)
            {
                invoice.PreviousInvoiceChange = BuildPreviousModifiedInvoice(explainId, invoice, allChanges);
            }
            else
            {
                invoice.PreviousInvoiceChange = (ExplainInvoice) invoice.Clone();
            }
        }

        private ExplainInvoice BuildLastModifiedInvoice(ExplainInvoice originalInvoice, List<ExplainInvoiceCorrect> allChanges)
        {
            var invoiceChanges = allChanges.Where(x => x.InvoiceId == originalInvoice.ROW_KEY).ToList();
            List<ExplainInvoiceCorrect> changes = invoiceChanges.OrderBy(v => v.ExplainId).ThenBy(v => v.ExplainIncomingDate).ToList();
            return BuildModifiedInvoice(originalInvoice, changes);
        }

        private ExplainInvoice BuildPreviousModifiedInvoice(long explainId, ExplainInvoice originalInvoice, List<ExplainInvoiceCorrect> allChanges)
        {
            var invoiceChanges = allChanges.Where(x => x.InvoiceId == originalInvoice.ROW_KEY).ToList();
            List<ExplainInvoiceCorrect> changes = invoiceChanges.Where(v => v.ExplainId < explainId).OrderBy(v => v.ExplainId).ThenBy(v => v.ExplainIncomingDate).ToList();
            return BuildModifiedInvoice(originalInvoice, changes);
        }

        private ExplainInvoice BuildModifiedInvoice(ExplainInvoice originalInvoice, List<ExplainInvoiceCorrect> invoiceChanges)
        {
            ExplainInvoice result = (ExplainInvoice)originalInvoice.Clone();
            result.Type = ExplainInvoiceType.Correct;
            result.SetState(ExplainInvoiceState.NotUsing);
            result.RefreshProperties();
            FillInvoiceFromChanges(result, invoiceChanges);
            return result;
        }
        
        private void SetStateOfThisInvoice(ExplainInvoice invoice, ExplainInvoiceCorrectState state)
        {
            if (state.State == ExplainInvoiceStateInThis.Changing)
            {
                invoice.AddState(ExplainInvoiceState.ChangingInThisExplain);
            }
            else if (state.State == ExplainInvoiceStateInThis.Processed)
            {
                invoice.AddState(ExplainInvoiceState.ProcessedInThisExplain);
            }
        }

        private void SetStateOfPreviousInvoice(ExplainInvoice invoice, List<ExplainInvoiceCorrectState> previousStates)
        {
            if (previousStates.Count > 0)
            {
                if (previousStates.Count(p => p.State == ExplainInvoiceStateInThis.Processed) > 0)
                {
                    invoice.AddState(ExplainInvoiceState.ProcessedInPreviousExplain);
                }
                else if (previousStates.Count(p => p.State == ExplainInvoiceStateInThis.Changing) > 0)
                {
                    invoice.AddState(ExplainInvoiceState.ChangingInPreviousExplain);
                }
            }
        }

        private void FillInvoiceFromChanges(
            ExplainInvoice invoice, 
            List<ExplainInvoiceCorrect> changes)
        {
            PropertyInfo[] properties = typeof (ExplainInvoice).GetProperties();
            foreach (ExplainInvoiceCorrect item in changes)
            {
                var ru = new CultureInfo("ru-RU");
                var itemProp = properties.SingleOrDefault(p => p.Name == item.FieldName);

                if (itemProp != null)
                {
                    string type = itemProp.PropertyType.FullName;
                    if (type.Contains("System.Decimal") && item.FieldValue != "")
                    {
                        string s = item.FieldValue;
                        decimal temp = Decimal.Parse(s, NumberStyles.AllowDecimalPoint);
                        itemProp.SetValue(invoice, temp, null);
                    }
                    if (type.Contains("System.DateTime") && item.FieldValue != "")
                    {
                        string s = item.FieldValue;
                        DateTime temp = DateTime.ParseExact(s, "dd.MM.yyyy", ru);
                        itemProp.SetValue(invoice, temp, null);
                    }
                    if (!type.Contains("System.Decimal") && !type.Contains("System.DateTime"))
                        itemProp.SetValue(invoice, Convert.ChangeType(item.FieldValue, itemProp.PropertyType), null);
                }
            }
        }
    }

    public static class ExplainInvoiceServiceHelper
    {
        public static PageResult<ExplainInvoice> GetInvoices(
            this IExplainInvoiceAdapter adapter,
            long explainId,
            int chapter,
            QueryConditions query)
        {
            var result = new PageResult<ExplainInvoice>();

            if (!query.PaginationDetails.SkipTotalAvailable)
            {
                result.TotalAvailable =
                    adapter.Count(
                        FilterExpressionCreator.CreateGroup()
                            .WithExpression(explainId.ToExplainFilter())
                            .WithExpression(chapter.ToChapterFilter()));
            }

            var filterBy = query
                .Filter
                .ToFilterGroup()
                .WithExpression(explainId.ToExplainFilter())
                .WithExpression(chapter.ToChapterFilter());

            if (!query.PaginationDetails.SkipTotalMatches)
            {
                result.TotalMatches = adapter.Count(filterBy);
            }

            result.Rows =
                adapter.Search(
                    filterBy,
                    query.Sorting,
                    query.PaginationDetails.RowsToTake.Value,
                    query.PaginationDetails.RowsToSkip);


            return result;
        }

        public static List<ExplainInvoiceCorrectState> GetStates(
            this IExplainInvoiceStateAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyList(
                        TypeHelper<ExplainInvoiceCorrectState>
                            .GetMemberName(x => x.InvoiceId)));
        }

        public static List<InvoiceContractor> GetContractors(
            this IExplainInvoiceContractorAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceContractor>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<ExplainInvoiceCorrect> GetChanges(
            this IExplainInvoiceAttributeChangeAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyList(
                        TypeHelper<ExplainInvoiceCorrect>
                            .GetMemberName(x => x.InvoiceId)));
        }

        public static FilterExpressionBase ToExplainFilter(this long explainId)
        {
            return FilterExpressionCreator.Create(
                "EXPLAIN_ID",
                ColumnFilter.FilterComparisionOperator.Equals,
                explainId);
        }
        public static FilterExpressionBase ToDocIDFilter(this long docId)
        {
            return FilterExpressionCreator.Create(
                "DOC_ID",
                ColumnFilter.FilterComparisionOperator.Equals,
                docId);
        }

        public static FilterExpressionBase ToChapterFilter(this int chapter)
        {
            return FilterExpressionCreator.Create(
                "INVOICE_CHAPTER",
                ColumnFilter.FilterComparisionOperator.Equals,
                chapter);
        }

        private static FilterExpressionBase ToInvoiceKeyList(
            this IEnumerable<ExplainInvoice> invoices,
            string invoiceKeyField)
        {
            var rowKeyFilter = FilterExpressionCreator.
                CreateInList(invoiceKeyField,
                invoices.Select(x => x.ROW_KEY).ToArray());

            return FilterExpressionCreator.CreateGroup().WithExpression(rowKeyFilter);
        }

        private static FilterExpressionBase ToInvoiceKeyListWithDocId(
        this IEnumerable<ExplainInvoice> invoices,
        string invoiceKeyField)
        {
            var rowKeyFilter = FilterExpressionCreator.
                CreateInList(invoiceKeyField,
                invoices.Select(x => x.ROW_KEY).ToArray());

            var docId = invoices.Any() ? invoices.First().DocId : Int64.MinValue;

            return FilterExpressionCreator
                .CreateGroup().WithExpression(docId.ToDocIDFilter()).WithExpression(rowKeyFilter);
        }

        public static List<InvoiceOperation> GetOperation(
            this IExplainInvoiceOperationAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceOperation>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<InvoiceBuyAccept> GetBuyAccepts(
            this IExplainInvoiceBuyAcceptAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoiceBuyAccept>
                            .GetMemberName(x => x.InvoiceKey)));
        }

        public static List<InvoicePaymentDocument> GetPaymentDocuments(
            this IExplainInvoicePaymentDocumentAdapter adapter,
            IEnumerable<ExplainInvoice> invoices)
        {
            return adapter
                .Search(
                    invoices.ToInvoiceKeyListWithDocId(
                        TypeHelper<InvoicePaymentDocument>
                            .GetMemberName(x => x.InvoiceKey)));
        }

    }
}
