﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class DataService : IDataService
    {
        private ServiceHelpers _helper;
        private DalManager _dalManager;
        private IAuthorizationProvider _localAuthProvider;


        public DataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _dalManager = new DalManager(_helper);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        #region Implementation of IDataService

        public OperationResult<List<RegionDetails>> GetRegions()
        {
            return _helper.Do(() =>
            {
                List<DictionaryRegions> regions = _dalManager.GetTableData<DictionaryRegions>("V$SSRF").OrderBy(p => p.S_CODE).ToList();
                List<UserToRegion> userToRegions = _dalManager.GetTableData<UserToRegion>("USER_TO_N_SSRF");
                List<RegionDetails> regionDetailses = new List<RegionDetails>();
                RegionDetails regionDetails;
                foreach (DictionaryRegions region in regions)
                {
                    regionDetails = new RegionDetails() { Code = region.S_CODE, Name = region.S_NAME };
                    regionDetails.ClearUsers();
                    foreach (UserToRegion itemUserToregion in userToRegions.Where(p => p.N_SSRF_CODE == region.S_CODE))
                    {
                        regionDetails.UserSIDs.Add(itemUserToregion.USER_SID);
                    }
                    regionDetailses.Add(regionDetails);
                }
                return regionDetailses;
            });
        }

        public OperationResult<List<ShortClarification>> GetShortClarifications()
        {
            return _helper.Do(() =>
            {
                List<ShortClarification> clarifications = _dalManager.GetTableData<ShortClarification>("EXPLAIN_FOR_RESEND").OrderBy(p => p.ZIP).ToList();
                return clarifications;
            });
        }

        public OperationResult<List<FederalDistrict>> GetFederalDistricts()
        {
            return _helper.Do(() =>
            {
                List<FederalDistrict> federalDistricts = TableAdapterCreator.Region(_helper).GetFederalDistricts();
                List<FederalDistrictToRegion> federalDistrictToRegion = TableAdapterCreator.Region(_helper).GetFederalDistrictToRegion();
                foreach (FederalDistrict fd in federalDistricts)
                {
                    fd.RegionCodes = federalDistrictToRegion.Where(p => p.DistrictId == fd.Id).Select(p => p.RegionCode).ToList();
                }
                return federalDistricts;
            });
        }

        public OperationResult<List<DictionaryNalogOrgan>> GetNalogOrgans()
        {
            return _helper.Do(() => _dalManager.GetTableData<DictionaryNalogOrgan>("v$sono").OrderBy(p => p.S_CODE).ToList());
        }

        public OperationResult<List<DictionaryNalogOrgan>> GetNalogOrgans(string nalogOrganParent)
        {
            return _helper.Do(() => _dalManager.GetTableData<DictionaryNalogOrgan>("v$sono").Where(p => p.S_PARENT_CODE != null && p.S_PARENT_CODE == nalogOrganParent).OrderBy(p => p.S_CODE).ToList());
        }

        public OperationResult<List<DictionaryNalogPeriods>> GetNalogPeriods()
        {
            return _helper.Do(() =>
            {
                List<DictionaryNalogPeriods> periods = _dalManager.GetTableData<DictionaryNalogPeriods>("DICT_NALOG_PERIODS").ToList();
                NalogPeriodsAddBoundary(periods);
                return periods;
            });
        }

        private void NalogPeriodsAddBoundary(List<DictionaryNalogPeriods> periods)
        {
            int year = DateTime.Now.Year;
            foreach (DictionaryNalogPeriods item in periods)
            {
                item.GenerateBoundary(year);
            }
        }

        private IConfigurationAdapter Configuration()
        {
            return TableAdapterCreator.Configuration(_helper);
        }

        private IConfigurationAutoselectionFilterTableAdapter AutoselectionFilter()
        {
            return TableAdapterCreator.ConfigurationAutoselectionFilter(_helper);
        }

        public OperationResult<Configuration> ReadConfiguration()
        {
            var c = ConfigurationCache.Instance();

            if (c.Cache == null)
            {
                var r = DalHelper.Execute<OperationResult<Configuration>>(
                    () =>
                    {
                        var result = Configuration().Read();
                        return new OperationResult<Configuration>()
                        {
                            Result = result ?? new Configuration(),
                            Status = ResultStatus.Success
                        };
                    },
                    _helper);
                c.Cache = r.Result;
            }
            return new OperationResult<Configuration>(c.Cache) {Status = ResultStatus.Success};
        }

        public OperationResult SaveConfiguration(ConfigurationParamName param, string value)
        {
            return DalHelper.Execute(() => Configuration().SaveParam(param, value), _helper);
        }


        public OperationResult<Dictionary<string, string>> GetDictionaryData(string nameDictionary, DictionaryConditions criteria)
        {
            return DalHelper.Execute(() =>
                {
                    var ret = new OperationResult<Dictionary<string, string>>(new Dictionary<string, string>(0));

                    switch (nameDictionary)
                    {
                        case "DICT_NALOG_PERIODS":
                        {
                            ret.Result = Dictionaries.GetPeriodDictionary();
                            break;
                        }
                        case "dict_discrep_filter_status":
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.ID.ToString(), x => x.NAME);
                            break;
                        }
                        case "v$ssrf":
                        {
                            ret.Result =
                                TableAdapterCreator.Region(_helper)
                                    .GetRegionsAvailable(_localAuthProvider.CurrentUserSID)
                                    .ToDictionary(x => x.S_CODE, x => x.S_NAME);
                            break;
                        }
                        case "v$sono":
                        {
                            ret.Result =
                                TableAdapterCreator.Region(_helper)
                                    .GetInspectionsAvailable(_localAuthProvider.CurrentUserSID, criteria)
                                    .ToDictionary(x => x.S_CODE, x => x.S_NAME);
                            break;
                        }
                        case "DICT_SUR":
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.CODE, x => x.DESCRIPTION);
                            break;
                        }
                        case "DICT_DISCREPANCY_KIND":
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.CODE, x => x.NAME);
                            break;
                        }
                        case "DICT_DISCREPANCY_TYPE":
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.S_CODE, x => x.DESCRIPTION);
                            break;
                        }
                        case "DICT_SELECTED_STATE":
                        case "DICT_COMPARE_RULES":
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.CODE, x => x.DESCRIPTION);
                            break;
                        }
                        case "DICT_CHAPTER":
                        {
                            ret.Result = Dictionaries.GetDictChapter();
                            break;
                        }
                        case "DICT_MARK":
                        {
                            ret.Result = Dictionaries.GetDictMark();
                            break;
                        }
                        case "DICT_OPERATION_CODES":
                        {
                            var dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.BIT_CODE, x => string.Concat(x.CODE, ". ", x.NAME));
                            break;
                        }
                        default:
                        {
                            List<DictionaryCommon> dictData = _dalManager.GetTableData<DictionaryCommon>(nameDictionary);
                            ret.Result = dictData.ToDictionary(x => x.S_CODE, x => x.S_NAME);
                            break;
                        }
                    }

                    return ret;
                },
                _helper);
        }

        public OperationResult<Dictionary<string, string>> SearchRegions(string searchKey, int maxQuantity)
        {
            return DalHelper.Execute<OperationResult<Dictionary<string, string>>>(
                () =>
                {
                    var ret = new OperationResult<Dictionary<string, string>>(new Dictionary<string, string>(0));
                    ret.Result =
                        TableAdapterCreator
                        .Region(_helper)
                        .GetRegionsAvailable(
                            _localAuthProvider.CurrentUserSID,
                            searchKey,
                            maxQuantity)
                        .ToDictionary(x => x.S_CODE, x => x.DESCRIPTION);

                    return ret;
                }, _helper);
        }

        public OperationResult<Dictionary<string, string>> SearchInspections(string searchKey, int maxQuantity)
        {
            return DalHelper.Execute<OperationResult<Dictionary<string, string>>>(
                () =>
                {
                    var ret = new OperationResult<Dictionary<string, string>>(new Dictionary<string, string>(0));
                    ret.Result =
                        TableAdapterCreator
                        .Region(_helper)
                    .GetInspectionsAvailable(
                        _localAuthProvider.CurrentUserSID,
                        searchKey,
                        maxQuantity)
                    .ToDictionary(x => x.S_CODE, x => x.S_NAME);

                    return ret;
                }, _helper);
        }

        public OperationResult<List<T>> GetTableData<T>(string tblName) where T : class , new()
        {
            OperationResult<List<T>> res = new OperationResult<List<T>>();
            res.Result = _dalManager.GetTableData<T>(tblName);
            return res;
        }

        public OperationResult<List<FilterCriteria>> GetAutoselectionFilterCriteria()
        {
            return DalHelper
                .Execute(
                    () =>
                        Configuration().GetAutoselectionFilterCriteria(), _helper);
        }

        public OperationResult SaveAutoselectionFilter(List<GroupFilter> IncludeFilter, List<GroupFilter> ExcludeFilter)
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<GroupFilter>));

            StringBuilder sb = new StringBuilder();
            var dataItem = new AutoSelectionFilterConfiguration();
            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, IncludeFilter);
            }

            dataItem.IncludeFilter = sb.ToString();

            sb.Clear();

            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, ExcludeFilter);
            }

            dataItem.ExcludeFilter = sb.ToString();


            return DalHelper
                .Execute<OperationResult>(
                    () =>
                    {
                        AutoselectionFilter().UpdateAll(dataItem);
                        return new OperationResult
                        {
                            Status = ResultStatus.Success
                        };
                    },
                    _helper);
        }

        public OperationResult SaveAutoselectionFilter(Filter includeFilter, Filter excludeFilter)
        {
            var dataItem = new AutoSelectionFilterConfiguration();
            dataItem.IncludeFilter = FilterHelper.Serialize(includeFilter);
            dataItem.ExcludeFilter = FilterHelper.Serialize(excludeFilter);

            return DalHelper
                .Execute<OperationResult>(
                    () =>
                    {
                        AutoselectionFilter().UpdateAll(dataItem);
                        return new OperationResult
                        {
                            Status = ResultStatus.Success
                        };
                    },
                    _helper);
        }

        public OperationResult LoadAutoselectionFilter(out List<GroupFilter> IncludeFilter, out List<GroupFilter> ExcludeFilter)
        {
            string incl = string.Empty;
            string excl = string.Empty;

            IncludeFilter = null; //TODO: Если что-либо из этого стереть, то экран методолога перестанет работать. Такой зависимости быть не должно
            ExcludeFilter = null;

            var rez = DalHelper.Execute<OperationResult>(
                () =>
                {
                    var data = AutoselectionFilter().All();

                    if (data != null && data.Any())
                    {
                        incl = data.First().IncludeFilter;
                        excl = data.First().ExcludeFilter;
                    }

                    return new OperationResult
                        {
                            Status = ResultStatus.Success
                        };
                },
                _helper);

            if (rez.Status == ResultStatus.Success)
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<GroupFilter>));

                try
                {
                    using (TextReader reader = new StringReader(incl))
                    {
                        IncludeFilter = ser.Deserialize(reader) as List<GroupFilter>;
                    }
                }
                catch (Exception) { }

                try
                {
                    using (TextReader reader = new StringReader(excl))
                    {
                        ExcludeFilter = ser.Deserialize(reader) as List<GroupFilter>;
                    }
                }
                catch (Exception) { }
            }

            return rez;
        }

        public OperationResult LoadAutoselectionFilter(out Filter includeFilter, out Filter excludeFilter)
        {
            string includeFilterRaw = string.Empty;
            string excludeFilterRaw = string.Empty;

            includeFilter = new Filter();
            excludeFilter = new Filter();

            var rez = DalHelper.Execute<OperationResult>(
                () =>
                {
                    var data = AutoselectionFilter().All();

                    if (data != null && data.Any())
                    {
                        includeFilterRaw = data.First().IncludeFilter;
                        excludeFilterRaw = data.First().ExcludeFilter;
                    }

                    return new OperationResult
                    {
                        Status = ResultStatus.Success
                    };
                },
                _helper);

            if (!string.IsNullOrWhiteSpace(includeFilterRaw))
            {
                var filterAutoIncludeResult = FilterHelper.Deserialize(includeFilterRaw);
                includeFilter = filterAutoIncludeResult.Filter;
            }

            if (!string.IsNullOrWhiteSpace(includeFilterRaw))
            {
                var filterAutoExcludeResult = FilterHelper.Deserialize(excludeFilterRaw);
                excludeFilter = filterAutoExcludeResult.Filter;
            }

            return rez;
        }

        public OperationResult<List<CFGRegion>> GetLoadingInspection()
        {
            return DalHelper.Execute<OperationResult<List<CFGRegion>>>(() => Configuration().GetRegionParam(CFGParam.DiscrepancyPVP), _helper);
        }

        public OperationResult<CFGRegion> LoadingInspectionSetValue(CFGRegion param)
        {
            return DalHelper.Execute<OperationResult<CFGRegion>>(() => Configuration().SetRegionParamValue(param), _helper);
        }

        public OperationResult<TaxPeriodConfiguration> GetTaxPeriodConfiguration()
        {
            return DalHelper.Execute<OperationResult<TaxPeriodConfiguration>>(() =>
            {
                var result = TableAdapterCreator.TaxPeriodConfigurationAdapter(_helper).Search();
                return new OperationResult<TaxPeriodConfiguration>()
                {
                    Result = result ?? new TaxPeriodConfiguration(),
                    Status = ResultStatus.Success
                };
            }, _helper);
        }

        #endregion

    }
}
