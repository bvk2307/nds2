﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IUserToRegionService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class UserToRegionService : IUserToRegionService
    {
        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public UserToRegionService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<List<string>> GetUsers(string regionCode)
        {
            return _helper.Do(() => TableAdapterCreator.UserToRegion(_helper).GetUsers(regionCode));
        }

        public OperationResult<SounDataContainer> GetSounDialogData(Region region, QueryConditions conditions)
        {
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.SounDialog(_helper).Load(region, conditions), _helper);
        }

        public OperationResult SaveSounDialogData(Region region)
        {
            return DalHelper.Execute(() => TableAdapterCreator.SounDialog(_helper).Save(region), _helper);
        }

        public OperationResult<List<ResponsibilityRegion>> GetResponsibilityRegionData(int contextId, QueryConditions conditions)
        {
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.UserToRegion(_helper).GetResponsibilityRegions(contextId, conditions), _helper);
        }
    }
}