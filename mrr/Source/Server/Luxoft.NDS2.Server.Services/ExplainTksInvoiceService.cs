﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.Adapters.Explain;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.ExplainReply;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.SortOptimization;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IExplainTksInvoiceService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ExplainTksInvoiceService : IExplainTksInvoiceService
    {
        private readonly ServiceHelpers _helper;

        public ExplainTksInvoiceService(IReadOnlyServiceCollection service)
        {
            _helper = new ServiceHelpers(service);
        }

        public OperationResult<PageResult<ExplainTksInvoiceChapter8>> FindInChapter8(
            long explainId, 
            QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        if (!queryConditions.Sorting.Any())
                        {
                            queryConditions.Sorting.AddRange(ExplainTksServiceHelper.DefaultOrder());
                        }
                        
                        var adapter = ExplainTksAdaptersCreator.Chapter8(_helper, connection);
                        var filterByExplainId =
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceChapter8>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainId);
                        var result = adapter
                            .Page<ExplainTksInvoiceChapter8>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);

                        if (result.Rows.Any())
                        {
                            result.Rows.FillAttributes(_helper, connection);
                        }

                        return result;
                    }
                });
        }

        public OperationResult<PageResult<ExplainTksInvoiceChapter9>> FindInChapter9(long explainId, QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        if (!queryConditions.Sorting.Any())
                        {
                            queryConditions.Sorting.AddRange(ExplainTksServiceHelper.DefaultOrder());
                        }

                        var adapter = ExplainTksAdaptersCreator.Chapter9(_helper, connection);
                        var filterByExplainId =
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceChapter9>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainId);

                        var result = adapter
                            .Page<ExplainTksInvoiceChapter9>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);

                        if (result.Rows.Any())
                        {
                            result.Rows.FillAttributes(_helper, connection);
                        }

                        return result;
                    }
                });
        }

        public OperationResult<PageResult<ExplainTksInvoiceChapter10>> FindInChapter10(long explainId, QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = ExplainTksAdaptersCreator.Chapter10(_helper, connection);
                        var filterByExplainId =
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceChapter10>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainId);

                        return adapter
                            .Page<ExplainTksInvoiceChapter10>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);
                    }
                });
        }

        public OperationResult<PageResult<ExplainTksInvoiceChapter11>> FindInChapter11(long explainId, QueryConditions queryConditions)
        {
            return _helper.Do(
                 () =>
                 {
                     using (var connection = new SingleConnectionFactory(_helper))
                     {
                         var adapter = ExplainTksAdaptersCreator.Chapter11(_helper, connection);
                         var filterByExplainId =
                             FilterExpressionCreator.Create(
                                 TypeHelper<ExplainTksInvoiceChapter11>.GetMemberName(x => x.ExplainZip),
                                 ColumnFilter.FilterComparisionOperator.Equals,
                                 explainId);

                         return adapter
                             .Page<ExplainTksInvoiceChapter11>(
                                 filterByExplainId,
                                 queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                 queryConditions);
                     }
                 });
        }

        public OperationResult<PageResult<ExplainTksInvoiceChapter12>> FindInChapter12(long explainId, QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = ExplainTksAdaptersCreator.Chapter12(_helper, connection);
                        var filterByExplainId =
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceChapter12>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainId);

                        return adapter
                            .Page<ExplainTksInvoiceChapter12>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);
                    }
                });
        }

        public OperationResult<PageResult<ExplainTksInvoiceConfirmed>> FindConfirmedInvoices(long explainZip, QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = ExplainTksAdaptersCreator.ConfirmedInvoices(_helper, connection);
                        var filterByExplainId = 
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceConfirmed>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainZip);

                        return adapter
                            .Page<ExplainTksInvoiceConfirmed>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);
                    }
                });
        }

        public OperationResult<PageResult<ExplainTksInvoiceUnconfirmed>> FindUnconfirmedInvoices(long explainZip, QueryConditions queryConditions)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = ExplainTksAdaptersCreator.UnconfirmedInvoices(_helper, connection);
                        var filterByExplainId =
                            FilterExpressionCreator.Create(
                                TypeHelper<ExplainTksInvoiceUnconfirmed>.GetMemberName(x => x.ExplainZip),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                explainZip);

                        return adapter
                            .Page<ExplainTksInvoiceUnconfirmed>(
                                filterByExplainId,
                                queryConditions.Filter.ToFilterGroup().WithExpression(filterByExplainId),
                                queryConditions);
                    }
                });
        }
    }

    public static class ExplainTksServiceHelper
    {
        public static FilterExpressionBase SearchAttibutesBy<T>(this IEnumerable<T> invoices)
            where T : IExplainTksInvoiceBase
        {
            return FilterExpressionCreator
                                    .CreateGroup()
                                    .WithExpression(
                                        FilterExpressionCreator.Create(
                                            TypeHelper<InvoiceAttribute>
                                                .GetMemberName(x => x.ExplainZip),
                                            ColumnFilter.FilterComparisionOperator.Equals,
                                            invoices.First().ExplainZip))
                                    .WithExpression(
                                        FilterExpressionCreator.CreateInList(
                                            TypeHelper<InvoiceAttribute>
                                                .GetMemberName(x => x.InvoiceRowKey),
                                            invoices.Select(x => x.RowKey)));
        }

        public static void FillAttributes<T>(
            this IEnumerable<T> invoices,
            IServiceProvider service,
            ConnectionFactoryBase connection)
            where T : IExplainTksBookInvoice
        {
            var filterBy = invoices.SearchAttibutesBy();
            var receiptDocs =
                new InvoiceAttributeLoader<InvoiceReceiptDocument>(
                    ExplainTksAdaptersCreator.ReceiptDocuments(service, connection));
            var receiveDates =
                new InvoiceAttributeLoader<InvoiceReceiveDate>(
                    ExplainTksAdaptersCreator.ReceiveDates(service, connection));
            var contractors =
                new InvoiceAttributeLoader<InvoiceContractor>(
                    ExplainTksAdaptersCreator.Contractors(service, connection));
            receiptDocs.Load(filterBy);
            receiveDates.Load(filterBy);
            contractors.Load(filterBy);

            foreach (var invoice in invoices)
            {
                invoice.ReceiptDocuments = receiptDocs.GetAttributes(invoice.RowKey);
                invoice.ReceiveDates = receiveDates.GetAttributes(invoice.RowKey);
                invoice.Contractors = contractors.GetAttributes(invoice.RowKey);
            }
        }

        public static IEnumerable<ColumnSort> DefaultOrder()
        {
            yield return new ColumnSort
            {
                ColumnKey = TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.Chapter),
                Order = ColumnSort.SortOrder.Asc
            };

            yield return new ColumnSort
            {
                ColumnKey = TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.OrdinalNumber),
                Order = ColumnSort.SortOrder.Asc
            };
        }
    }

    public class InvoiceAttributeLoader<T>
        where T : InvoiceAttribute
    {
        private readonly IExplainInvoiceAttributeAdapter<T> _adapter;

        private bool _loaded;

        private readonly Dictionary<string, List<T>> _values = new Dictionary<string, List<T>>();

        public InvoiceAttributeLoader(IExplainInvoiceAttributeAdapter<T> adapter)           
        {
            _adapter = adapter;
        }

        public void Load(FilterExpressionBase filterBy)
        {
            foreach (var invoiceAttribute in _adapter.Search(filterBy))
            {
                if (_values.ContainsKey(invoiceAttribute.InvoiceRowKey))
                {
                    _values[invoiceAttribute.InvoiceRowKey].Add(invoiceAttribute);
                }
                else
                {
                    _values.Add(invoiceAttribute.InvoiceRowKey, new List<T> { invoiceAttribute });
                }
            }
        }

        public T[] GetAttributes(string rowKey)
        {
            if (!_values.ContainsKey(rowKey))
            {
                return new T[0];
            }

            return _values[rowKey].ToArray();
        }
    }
}
