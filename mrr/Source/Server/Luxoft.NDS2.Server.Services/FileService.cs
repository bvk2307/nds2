﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Collections.Generic;
using System.IO;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IFileService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class FileService : IFileService
    {
        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public FileService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public long GetSize(string path)
        {
            try
            {
                long result = 0;

                if (!File.Exists(path))
                    result =- 1;

                result = new FileInfo(path).Length;

                _helper.LogNotification(
                    string.Format(
                        "Получение размера файла (GetSize), SID пользователя: \"{0}\", имя пользователя: \"{1}\", путь к файлу: \"{2}\", return size: \"{3}\"",
                        _localAuthProvider.CurrentUserSID,
                        _localAuthProvider.CurrentUserName,
                        path,
                        result)
                    );

                return result;
            }
            catch (Exception exception)
            {
                _helper.LogError(string.Format("Получение размера файла (GetSize), ошибка = \"{0}\"", exception.Message),
                    "FileService::GetSize",
                    exception,
                    new List<KeyValuePair<string, object>>());

                return -1;
            }
        }

        public byte[] GetChunk(string path, int offset, int bufferSize)
        {
            try
            {
                _helper.LogNotification(
                    string.Format(
                        "Получение части файла (GetChunk), SID пользователя: \"{0}\", имя пользователя: \"{1}\", путь к файлу: \"{2}\", offset = {3}, bufferSize = {4}",
                        _localAuthProvider.CurrentUserSID,
                        _localAuthProvider.CurrentUserName,
                        path, offset, bufferSize)
                    );

                if (!File.Exists(path))
                    return null;

                var size = new FileInfo(path).Length;
                if (offset > size)
                    return null;

                byte[] buffer;
                int bytesRead;

                using (var fileStream
                    = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    fileStream.Seek(offset, SeekOrigin.Begin);
                    buffer = new byte[bufferSize];
                    bytesRead = fileStream.Read(buffer, 0, bufferSize);
                }

                if (bytesRead == bufferSize)
                    return buffer;

                var trimmedBuffer = new byte[bytesRead];
                Array.Copy(buffer, trimmedBuffer, bytesRead);
                return trimmedBuffer;
            }
            catch (Exception exception)
            {
                _helper.LogError(string.Format("Получение части файла (GetChunk), ошибка = \"{0}\"", exception.Message), 
                    "FileService::GetChunk", 
                    exception, 
                    new List<KeyValuePair<string, object>>());

                return new byte[0];
            }
        }
    }
}
