﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using DocumentFormat.OpenXml.Office2013.Drawing.ChartStyle;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{

    [CommunicationContract(typeof(IEchoService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class EchoService : IEchoService
    {
        private readonly ServiceHelpers _helper;

        public EchoService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public string PingDatabase()
        {
            string res = null;
            try
            {
                res = TableAdapterCreator.EchoAdapter(_helper).GetDbInfo().FirstOrDefault();
            }
            catch (Exception ex)
            {
                res = string.Format("Error = {0}", ex.Message);
            }

            return res;
        }

        public string PingAuthService()
        {
            return "not implemented";
        }

        public string PingSOV()
        {
            return "not implemented";
        }
    }
}
