﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Security.Cryptography;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using System.Text;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO.Horizon;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class HrzAuthService : IHrzAuthService
    {
        private IAuthorizationProvider _authProvider;

        #region RSAKey

        private string _rsaParam = @"<RSAKeyValue>
	<Modulus>pVRveaUABCdYDRbXFMvi0zBFdi+FessftPx1VRVQ9akkGpS1nMlBqJ3Cs6Qu/4F+JgvHp8ebOn7j32B3pfvOYbVA+wR8NSt2Y6VNVDFxU6HItdRElz65meAVa3Vv1TvAO0rvSC2XlAQ47/zXgCbXn7gQEwIgQVXOzWJCyMT6ZKM=</Modulus>
	<Exponent>AQAB</Exponent>
	<P>56bNB4Vy57IDttrxnasey0N8T+fc1gTMRJN1FIqDqN4G5DhUXdk3ChwArh3ruSRR0ijieawIcSIU5Uh+jvKkew==</P>
	<Q>trUSis8ud1j0r+9kRoUCZknESTAUvCIws5X+FEwdZHoWrj09ib4H2sEVyFjdKmTzkJa2jHphcRRohIWxYdtr+Q==</Q>
	<DP>VOgBXFFgdWX9EIedabHv3obEyLDuc49co4H8tuOyMDH5CqtO2PslvlHXOfqDjN+wEbykjf4tNfJNAGEa6iArgw==</DP>
	<DQ>G+XkMevWxYSAwEiGgDoZFrNW1HISCHQUCafyhvjkWA5IXkydk+W/X0iyYcgn5SxLAJ3yu9qH/HNITkv3vOB7SQ==</DQ>
	<InverseQ>uN/xelCaLXxhsHPoG3Q/37xHQqOv7oBPjaCJkJi7cQQGl5Vt9uaXPIKACDP/lUPcVC/mwCpWVdSwU35a4Xa56w==</InverseQ>
	<D>HS9fkli3PQR4/a6Hf0tSmjdOO8O9y2RqD3f1enSN2x/lmK+hs7CvX4Wh5jZYeZAxZaP0tcB9vYDC1x7fYDSoRLwl64dmRyfx2nephhy7Ip0/Ub2FbyWcqAgk1qsm7Fg41u5Gxg9rZZtoimi3UAKYpUekMjRx8T+dpphUdNoXjTE=</D>
</RSAKeyValue>";

        #endregion

        public HrzAuthService(IReadOnlyServiceCollection collection)
        {
            _authProvider = collection.Get<IAuthorizationProvider>();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "auth/?d={domain}&u={login}&p={pass}")]
        public HorizonAuthorizationResult GetUserRights(string domain, string login, string pass)
        {
            var result = new HorizonAuthorizationResult();

            try
            {
                var prov = new RSACryptoServiceProvider(1024);
                prov.FromXmlString(_rsaParam);
                var domainDecryptedBytes = prov.Decrypt(Convert.FromBase64String(domain), false);
                var loginDecryptedBytes = prov.Decrypt(Convert.FromBase64String(login), false);
                var passDecryptedBytes = prov.Decrypt(Convert.FromBase64String(pass), false);

                var domainDecrypted = Encoding.Default.GetString(domainDecryptedBytes);
                var loginDecrypted = Encoding.Default.GetString(loginDecryptedBytes);
                var passDecrypted = Encoding.Default.GetString(passDecryptedBytes);

                TraceInputData(loginDecrypted, passDecrypted, domainDecrypted);

                //AuthorizeAcrossWorkgroup(loginDecrypted, passDecrypted, domainDecrypted);
                AuthorizeAcrossDomain(loginDecrypted, passDecrypted, domainDecrypted);

                NTAccount acc = new NTAccount(loginDecrypted);
                var s = (SecurityIdentifier) acc.Translate(typeof (SecurityIdentifier));

                foreach (var item in _authProvider.GetUserPermissions(s.ToString()))
                {
                    if (!result.AccessRigths.ContainsKey(item.StructContext))
                    {
                        result.AccessRigths.Add(item.StructContext, new List<string>());
                    }

                    result.AccessRigths[item.StructContext].Add(item.Name);
                }

                TraceData(result);

                result.Status = HorizonAuthorizationStatus.Success;
            }
            catch (SecurityAccessDeniedException ex)
            {
                result.Status = HorizonAuthorizationStatus.Unauthorized;
                result.Message = ex.Message;
            }
            catch(Exception ex)
            {
                TraceError(ex);
                result.Status = HorizonAuthorizationStatus.Error;
                result.Message = ex.Message;
            }

            return result;
        }

        private void AuthorizeAcrossDomain(string login, string pass, string domain)
        {
            using (var ctx = new PrincipalContext(ContextType.Domain, domain))
            {
                if (!ctx.ValidateCredentials(login, pass))
                {
                    throw new SecurityAccessDeniedException("Логин или пароль указаны не верно");
                }
            }
        }

        private void AuthorizeAcrossWorkgroup(string login, string pass, string domain)
        {
            using (var ctx = new PrincipalContext(ContextType.Machine, domain))
            {
                if (!ctx.ValidateCredentials(login, pass))
                {
                    throw new SecurityAccessDeniedException("Логин или пароль указаны не верно");
                }
            }
        }


        #region TraceInfo

        private void TraceInputData(string login, string pass, string domain)
        {
            #if DEBUG
            Console.WriteLine("Запрос прав доступа - Domain:{0} Login:{1} Pass:{2}", domain, login, pass);
            #endif
        }

        private void TraceData(HorizonAuthorizationResult data)
        {
            #if DEBUG
            Console.WriteLine("Перечень доступных операций");
            Console.WriteLine("Кол-во контекстов {0}", data.AccessRigths.Keys.Count);
            Console.WriteLine("Кол-во операций {0}", data.AccessRigths.Values.Count);

            foreach (var value in data.AccessRigths.Values)
            {
                foreach (var item in value)
                {
                    Console.WriteLine("{0}", item);
                }
            }

            Console.WriteLine("Конец запроса");
            #endif
        }

        private void TraceError(Exception ex)
        {
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(ex.Message);
            Console.WriteLine("---------------------------------------");
        }

        #endregion TraceInfo
    }
}
