﻿using CommonComponents.Communication;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.Cache.Extensions;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.Services.UserAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(ISecurityService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SecurityService : ISecurityService
    {
        private readonly IReadOnlyServiceCollection _services;
        private readonly ServiceHelpers _helper;
        private readonly IAuthorizationProvider _localAuthProvider;

        private string counterName = "Server.AuthService";
        private string counterGroup = "NDS2";
        private PerformanceCounter _cntr;

        public SecurityService(IReadOnlyServiceCollection services)
        {
            Contract.Requires(services != null);

            _services = services;
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<List<AccessRight>> GetUserPermissions()
        {
            _helper.LogTrace("Security-GetUserPermissions");
            return _helper.Do(() => _localAuthProvider.GetUserPermissions());
        }

        public OperationResult ChangeUserRole(string roleName)
        {
            return _helper.Do(() => _localAuthProvider.ChangeUserRole(roleName));
        }

        private static List<string> roleOperations = 
            new List<string>
            {
                { Constants.SystemPermissions.Operations.RoleAdministrator },
                { Constants.SystemPermissions.Operations.RoleAnalyst },
                { Constants.SystemPermissions.Operations.RoleApprover },
                { Constants.SystemPermissions.Operations.RoleDeveloper },
                { Constants.SystemPermissions.Operations.RoleInspector },
                { Constants.SystemPermissions.Operations.RoleManager },
                { Constants.SystemPermissions.Operations.RoleMedodologist },
                { Constants.SystemPermissions.Operations.RoleObserver },
                { Constants.SystemPermissions.Operations.RoleSender }
            };

        [Obsolete]
        public OperationResult<List<string>> GetSubsystemRoles()
        {
            _helper.LogTrace("Security-GetSubsystemRoles");
            return _helper.Do(() => 
                _localAuthProvider
                    .GetUserPermissions()
                    .Where(permission => 
                        permission.PermType == PermissionType.Operation
                            && roleOperations.Contains(permission.Name))
                    .Select(operation => operation.Name)
                    .ToList());
        }

        public OperationResult<List<string>> GetSubsystemUsers()
        {
            _helper.LogTrace("Security-GetSubsystemUsers");
            return _helper.Do(() => _localAuthProvider.GetUserSIDs());
        }

        public OperationResult<List<UserStructContextRight>> GetStructContextUsers(string structContext)
        {
            _helper.LogTrace("Security-GetStructContextUsers");
            return _helper.Do(() => 
                _localAuthProvider.GetStructContextUserRigths(structContext)
                    .Where(sc=>sc.Role == Constants.SystemPermissions.Operations.RoleAnalyst || sc.Role == Constants.SystemPermissions.Operations.RoleApprover).ToList());
        }

	    //TODO "Переходим на GetUserAccess(string operationIdentifier), IUserPermissionRequestService и что-то вокруг"
        public OperationResult<ContractorDataPolicy> GetUserRestrictionConfiguration()
        {
            return _helper.Do(
                () =>
                {
                    var ret = new ContractorDataPolicy();

                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        var creator = new ContractorDataPolicyProvider( _localAuthProvider,
                                                                        _helper.InspectionGroupAdapter(connectionFactory),
                                                                        _helper.RestrictionChainsAdapter(connectionFactory));
                        ret = creator.GetPermissions();
                    }

                    return ret;
                });
        }
        
	    //apopov 8.11.2016	//TODO!!!   //test only code. It should be removed when a custom cache data provider (a descendant of PocoClassifierProvider) will be used anywhere
        private static bool s_onceChecked = true;	//assign 'false' to once call per process session by the first call of GetUserAccess()

        public OperationResult<UserAccessPolicy> GetUserAccess(string operationIdentifier)
        {
            Contract.Requires(!string.IsNullOrEmpty(operationIdentifier));
            return _helper.Do(
                () =>
                    {
	                    //apopov 8.11.2016	//TODO!!!   //test only code. It should be removed when a custom cache data provider (a descendant of PocoClassifierProvider) will be used anywhere
                        if ( !s_onceChecked )
                        {
                            s_onceChecked = true;

                            IClassifierServiceFactory classifierServiceFactory = _services.Get<IClassifierServiceFactory>();

                            Contract.Assume( classifierServiceFactory != null );

                            IClassifierService classifierService = classifierServiceFactory.GetClassifierService( _services, "NDS2Server.ThesAccess.config" );

                            Contract.Assume( classifierService != null );

                            IFetch<IDataItem> rigths = classifierService.LookUp( new CommonComponents.ThesAccess.Request( "OperatioUserRigths" ), "NDS2Classifiers.UserAccess" );
                            foreach ( IDataItem dataItem in rigths.Items )
                            {
                                var sb = new StringBuilder( 64 ).Append( "~~~ " );
                                bool isNotFirstPassage = false;
                                foreach ( KeyValuePair<string, IProperty> keyValuePair in dataItem.PropertyLists )
                                {
                                    if ( isNotFirstPassage )
                                        sb.Append( ", " );
                                    else isNotFirstPassage = true;

                                    UserAccessMapEntry poco = dataItem.GetPocoItemValue<UserAccessMapEntry>();
                                    sb.Append( keyValuePair.Key ).Append( ": '" ).Append( poco ).Append( "' ID: " ).Append( poco.Id );
                                }
                                Debug.WriteLine( sb.ToString() );
                            }
                        }
            
                        var ret = new UserAccessPolicy();
                        
                        using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var uacCache = UserAccessOperationCache.Instance(_helper, connectionFactory);

                            var creator = new UserAccessPolicyProvider(_localAuthProvider, uacCache, uacCache);

                            ret = creator.GetPermissions(operationIdentifier);
                        }

                        return ret;
                    });
        }

        /// <summary> Is operation eligible for "soun" code <paramref name="sounCode"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        public OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, string sounCode)
        {
            Contract.Requires(!string.IsNullOrEmpty(operationIdentifier));
            Contract.Requires(!string.IsNullOrEmpty(sounCode));

            return _helper.DoEx(
                () =>
                    {
                        UserPermissionRequestResult permissionResult;
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var userAccessPolicyAnalyzer = NewUserAccessPolicyAnalyzer(connectionFactory);
                            permissionResult = userAccessPolicyAnalyzer.IsOperationEligibleForSounCodes(operationIdentifier, sounCode)
                                               ? UserPermissionRequestResult.Granted : UserPermissionRequestResult.Denied;
                        }
                        return permissionResult;
                    });
        }

        /// <summary> Is operation eligible for some "soun" code from defined <paramref name="sounCodes"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCodes"></param>
        /// <returns></returns>
        public OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, IEnumerable<string> sounCodes)
        {
            Contract.Requires(!string.IsNullOrEmpty(operationIdentifier));
            Contract.Requires(Contract.ForAll(sounCodes, code => !string.IsNullOrEmpty(code)));

            return _helper.DoEx(
                () =>
                    {
                        UserPermissionRequestResult permissionResult;
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var userAccessPolicyAnalyzer = NewUserAccessPolicyAnalyzer(connectionFactory);
                            permissionResult = userAccessPolicyAnalyzer.IsOperationEligibleForSounCodes(operationIdentifier, sounCodes);
                        }
                        return permissionResult;
                    });
        }

        /// <summary> Is operation eligible for a taxpayer? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="taxpayerId"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        public OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, TaxPayerId taxpayerId, Period period = null)
        {
            Contract.Requires(!string.IsNullOrEmpty(operationIdentifier));
            Contract.Requires(taxpayerId != null && !string.IsNullOrEmpty(taxpayerId.Inn));
            Contract.Requires(operationIdentifier != Constants.SystemPermissions.Operations.DeclarationTreeRelation);
            if ( operationIdentifier != Constants.SystemPermissions.Operations.DeclarationTreeRelation )
                throw new NotSupportedException(string.Format("{0}.IsOperationEligibleByUserAccess(string, TaxPayerId, Period) does not support operation '{1}' yet", this.GetType().FullName, operationIdentifier));
            Contract.EndContractBlock();

            return _helper.DoEx(
                () =>
                    {
                        var declarationsDataService = new DeclarationsDataService(_services);
                        UserPermissionRequestResult permissionResult = UserPermissionRequestResult.NoData;

                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            IEnumerable<string> sounCodeIterator = 
                                GetDeclarationSounCodesIterator(declarationsDataService, taxpayerId.Inn, period, connectionFactory);    //deffered request to DeclarationsDataService is used only if it will be needed
                            permissionResult = NewUserAccessPolicyAnalyzer(connectionFactory).IsOperationEligibleForSounCodes(operationIdentifier, sounCodeIterator);
                        }
                        return permissionResult;
                    });
        }

        /// <summary> Iterates <see cref="DeclarationBrief.SOUN_CODE"/> of declarations returned by <see cref="DeclarationsDataService.Search"/>(). </summary>
        /// <param name="declarationsDataService"></param>
        /// <param name="innCode"></param>
        /// <param name="period"></param>
        /// <param name="connectionFactory"></param>
        /// <returns></returns>
        private IEnumerable<string> GetDeclarationSounCodesIterator(
            DeclarationsDataService declarationsDataService, string innCode, Period period, ConnectionFactoryBase connectionFactory)
        {
            Contract.Requires(connectionFactory != null);

            foreach (DeclarationBrief declarationBrief in declarationsDataService.Search(innCode, period, connectionFactory))
            {
                yield return declarationBrief.SOUN_CODE;
            }
        }

        private UserAccessPolicyAnalyzer NewUserAccessPolicyAnalyzer(ConnectionFactoryBase connectionFactory)
        {
            var uacCache = UserAccessOperationCache.Instance(_helper, connectionFactory);

            return new UserAccessPolicyAnalyzer(_localAuthProvider, uacCache, uacCache);
        }
        [Obsolete]
        public OperationResult<string[]> ValidateAccess(string[] operations)
        {
            return _helper.Do(() => _helper.ValidateAccess(operations));
        }

        public OperationResult<string[]> ValidateAccessCsud(string[] operations)
        {
            List<string> result = new List<string>();

            return _helper.Do(
                () =>
                {
                    using (var connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        foreach (var operation in operations)
                        {
                            var userAccess = GetUserAccess(operation);
                            if (userAccess != null && userAccess.Result != null)
                            {
                                if(userAccess.Result.FullAccess)
                                    result.Add(operation);
                            }
                        }
                        return result.ToArray();
                    }
                });
        }

        public OperationResult<AccessContext> GetAccessContext(string[] operations)
        {
            return _helper.Do(() => _helper.GetAccessContext(operations));
        }
    }
}
