﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.ThriftProxy;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(INavigatorReportDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class NavigatorReportDataService : INavigatorReportDataService
    {
        private readonly ServiceHelpers _helper;

        private readonly IAuthorizationProvider _localAuthProvider;

        public NavigatorReportDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<long> SubmitRequest(Request request)
        {
            var sovProxy =
                ClientProxyCreator.Create(
                    _helper.GetConfigurationValue(Constants.MC_SERVICE_HOST),
                    int.Parse(_helper.GetConfigurationValue(Constants.MC_SERVICE_PORT)),
                    _helper);

            return _helper.DoEx<long>(
                () =>
                {
                    CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                    return sovProxy.RequestNavigatorData(request);
                });
        }

        public OperationResult<RequestStatus> GetRequestStatus(long requestId)
        {
            return _helper.DoEx<RequestStatus>(
                () =>
                {
                    CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                    return TableAdapterCreator.NavigatorReport(_helper).GetStatus(requestId);
                });
        }

        public OperationResult<List<ChainSummary>> GetPairs(long requestId)
        {
            return _helper.Do<List<ChainSummary>>(
                () =>
                {
                    CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                    return TableAdapterCreator.NavigatorReport(_helper).GetPairs(requestId);
                });
        }

        public OperationResult<List<ChainContractorData>> GetChainContractors(long pairId, long requestId)
        {
            return _helper.Do(
                () =>
                {
                    CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                    return TableAdapterCreator.NavigatorReport(_helper).GetData(pairId, null, requestId);
                });
        }

        public OperationResult<List<ChainContractorData>> GetChain(long chainId, long requestId)
        {
            return _helper.Do(
                () => 
                    {
                        CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                        return TableAdapterCreator.NavigatorReport(_helper).GetData(null, chainId, requestId);
                    });
        }

        public OperationResult<List<TaxPayer>> GetTaxPayers(LoadTaxPayersContract contract)
        {
            var adapter = TableAdapterCreator.NavigatorTaxPayer(_helper);

            return _helper.Do(
                () => 
                    {
                        CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                        return adapter.Page(
                            WithRestrictions(
                                new QueryConditions
                                {
                                    Filter = contract.Filter,
                                    Sorting = contract.Sort,
                                    PageIndex = contract.PageIndex,
                                    PageSize = contract.PageSize
                                }), contract.Range);
                    });
        }

        public OperationResult<int> CountTaxPayers(CountTaxPayersContract contract)
        {
            var adapter = TableAdapterCreator.NavigatorTaxPayer(_helper);

            return _helper.DoEx(
                () =>
                    {
                        CheckPermissions(Constants.SystemPermissions.Operations.NavigatorView);

                        return adapter.Count(
                            WithRestrictions(
                                new QueryConditions
                                {
                                    Filter = contract.Filter,
                                    Sorting = new List<ColumnSort>()
                                }));
                    });
        }

        private void CheckPermissions(string operation)
        {
            if (!GetOperationPermissions(operation))
            {
                throw new AccessDeniedException("Отсутствуют права на выполнение функции");
            }
        }

        private bool GetOperationPermissions(string operation)
        {
            bool ret = false;

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                var policyProvider = new UserAccessPolicyProvider(_localAuthProvider,
                                          UserAccessOperationCache.Instance(_helper, connectionFactory),
                                          UserAccessOperationCache.Instance(_helper, connectionFactory));
                var restrictions = policyProvider.GetPermissions(operation);

                if (restrictions.FullAccess)
                    ret = true;
                else
                {
                    ret = restrictions.AllowedInspections.Any();
                }
            }

            return ret;
        }

        private QueryConditions WithRestrictions(QueryConditions conditions)
        {
            var filterHelper = new QueryConditionsHelper(_localAuthProvider, _helper);
            filterHelper.AddRestriction(
                conditions,
                TypeHelper<TaxPayer>.GetMemberName(x => x.RegionCode),
                TypeHelper<TaxPayer>.GetMemberName(x => x.InspectionCode));

            return conditions;
        }

        public OperationResult<Dictionary<string, bool>> GetTaxPayersPurchaseStatus(string[] taxPayersInn, PeriodRange range)
        {
            return _helper.Do(
                () => 
                    {
                        var buyers = 
                            TableAdapterCreator
                                .NavigatorTaxPayer(_helper)
                                .PurchaseStatus(taxPayersInn, range)
                                .ToDictionary(
                                    (data) => data.Inn,
                                    (data) => data.NdsCompensationQuantity > 0);

                        foreach (var inn in taxPayersInn)
                        {
                            if (!buyers.ContainsKey(inn))
                            {
                                buyers.Add(inn, false);
                            }
                        }

                        return buyers;
                    });
        }
    }
}
