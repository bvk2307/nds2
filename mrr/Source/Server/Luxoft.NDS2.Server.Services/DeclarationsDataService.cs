﻿using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Results;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO.Declaration;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.BankAccounts;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL.DeclarationsRequest;
using Luxoft.NDS2.Server.DAL.KnpDocuments;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace;
using Luxoft.NDS2.Server.DAL.OperationContext;
using Luxoft.NDS2.Server.DAL.TaxPayerDeclaration;
using Luxoft.NDS2.Server.Services.EntitiesConverters;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Helpers.Declarations;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess;
using Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.Services.SortOptimization;
using Luxoft.NDS2.Server.Services.UserAccess;
using Luxoft.NDS2.Server.ThriftProxy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using DeclarationKey = Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.DeclarationKey;
using TransactionScopeHelper = Luxoft.NDS2.Server.Common.Helpers.TransactionScopeHelper;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDeclarationsDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class DeclarationsDataService : IDeclarationsDataService
    {
        # region .ctor

        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public DeclarationsDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        # endregion

        # region Назначение деклараций\журналов на инспектора

        public OperationResult AssignDeclaration(DeclarationSummaryKey declarationKey, string assignToSid)
        {
            return _helper.Do(
                () =>
                {
                    var userInfoService = _helper.Services.Get<IUserInfoService>();
                    var assignedBy = userInfoService.GetCurrentUserInfo();
                    var assignedTo = userInfoService.GetUserInfoBySID(assignToSid);

                    var userAdapter = _helper.UserAdapter();
                    var assignedById = userAdapter.InsertOrUpdate(assignedBy.ToContract());
                    var assignedToId = userAdapter.InsertOrUpdate(assignedTo.ToContract());

                    _helper
                        .DeclarationAssignmentAdapter()
                        .Insert(declarationKey, assignedById, assignedToId);
                });
        }

        # endregion

        private bool SovCacheEnabled
        {
            get
            {
                var keyVal = _helper.GetConfigurationValue(Constants.SERVER_SOV_CACHE_ENABLED_KEY);
                return bool.Parse(keyVal);
            }
        }

        private string[] GetDeclarationCardViewPermissions()
        {
            var loader = new UserAccessContextLoader(
                _helper.OperationContextAdapter(),
                _helper.Services.Get<IAuthorizationProvider>());

            var accessContext = loader.GetAccessContext(new[] { MrrOperations.Declaration.DeclarationContractor });

            bool checkUserPermission = !accessContext.Operations.Any();

            return checkUserPermission
                ? new[] { Constants.SystemPermissions.Operations.TaxPayerViewCard }
                : new string[] { };
        }

        #region Запросы из списка и из карточки деклараций

        public OperationResult<Object> GetUserDisplayName()
        {
            return new OperationResult<object>(new object()) { Result = _helper.UserDisplayName };
        }

        #endregion

        #region Запросы из списка деклараций


        public OperationResult<PageResult<DeclarationSummary>> SelectDeclarations(QueryConditions conditions)
        {
            conditions = DeclarationListPartitioningHelper
                .TransformConditions(conditions, RegionGroupCache.Instance(_helper));

            conditions.PaginationDetails.SkipTotalAvailable = true;

            return _helper.Do(
                () =>
                {
                    var adapter = _helper.GetDeclarationsRequestAdapter();
                    return adapter.Page(
                        FilterExpressionCreator.CreateGroup(),
                        conditions.Filter.ToFilterGroup(),
                        conditions
                        );
                },
                new[] { Constants.SystemPermissions.Operations.DeclarationViewList }
                );
        }

        public OperationResult<PageResult<TaxPayerDeclarationVersion>> SelectTaxPayerDeclarations(QueryConditions query)
        {
            return _helper.Do(
                () =>
                {
                    var tableAdapter = _helper.TaxPayerDeclarationAdapter();
                    var filterBy = query.Filter.ToFilterWithGroupOperator();

                    var rows = tableAdapter.Search(
                        filterBy,
                        query.Sorting)
                        .ToList();

                    return new PageResult<TaxPayerDeclarationVersion>
                    {
                        Rows = rows,
                        TotalMatches = rows.Count
                    };
                },
                new[] { Constants.SystemPermissions.Operations.DeclarationViewList }
                );
        }

        private Dictionary<string, OperationAccessContext> _operationCache = new Dictionary<string, OperationAccessContext>();

        public OperationResult<PageResult<DeclarationBrief>> SelectDeclarationsForInspector(QueryConditions conditions)
        {
            return _helper.Do(
                () =>
                {
                    var result = new PageResult<DeclarationBrief>();

                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        #region OperationAccessContext operation = { Спасаем базу от наплыва запросов к правам }

                        var authProvider = _helper.Services.Get<IAuthorizationProvider>();
                        var operation = InspectorAccessContextCache.Instance().Get(authProvider.CurrentUserSID);
                        if (operation == null)
                        {
                            var loader = new UserAccessContextLoader(
                                _helper.OperationContextAdapter(),
                                authProvider);

                            var accessContext = loader.GetAccessContext(new[] { MrrOperations.InspectorWorkPlace });

                            if (!accessContext.Operations.Any())
                            {
                                return new PageResult<DeclarationBrief>();
                            }

                            operation = accessContext.Operations.First();
                            InspectorAccessContextCache.Instance().Set(authProvider.CurrentUserSID, operation);
                        }

                        #endregion

                        #region накидываем ограничения по правам

                        if (operation.AvailableRegions.Any())
                        {
                            var fc = new FilterQuery
                            {
                                ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.REGION_CODE),
                                ColumnType = typeof(string),
                                FilterOperator = FilterQuery.FilterLogicalOperator.Or
                            };
                            fc.Filtering.AddRange(operation.AvailableRegions.Select(r => new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = r
                            }));
                            conditions.Filter.Add(fc);
                        }

                        if (operation.AvailableInspections.Any())
                        {
                            var fc = new FilterQuery
                            {
                                ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SOUN_CODE),
                                ColumnType = typeof(string),
                                FilterOperator = FilterQuery.FilterLogicalOperator.Or
                            };
                            fc.Filtering.AddRange(operation.AvailableInspections.Select(i => new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = i
                            }));
                            conditions.Filter.Add(fc);
                        }

                        #endregion

                        conditions = DeclarationListPartitioningHelper
                            .TransformConditions(conditions, RegionGroupCache.Instance(_helper));

                        var searchCriteria = conditions.Filter.ToFilterGroup();

                        var inspectorDeclarationAdpater = _helper.InspectorWorkplaceDeclarationsAdapter(connection);

                        if (!conditions.PaginationDetails.SkipTotalAvailable
                            && conditions.Filter.Any(x => x.UserDefined))
                        {
                            var allQuery = conditions.Clone() as QueryConditions;
                            allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                            FilterExpressionGroup searchCriteriaClear = allQuery.Filter.ToFilterGroup();

                            result.TotalAvailable = inspectorDeclarationAdpater.Count(searchCriteriaClear);
                        }

                        if (!conditions.PaginationDetails.SkipTotalMatches)
                        {
                            result.TotalMatches = inspectorDeclarationAdpater.Count(searchCriteria);

                            result.TotalAvailable =
                                result.TotalAvailable > result.TotalMatches
                                ? result.TotalAvailable
                                : result.TotalMatches;
                        }

                        var rowFrom = (int)(conditions.PageIndex - 1) * conditions.PageSize;
                        var rowTo = (int)conditions.PageIndex * conditions.PageSize;


                        result.Rows = inspectorDeclarationAdpater.Search(searchCriteria,
                                                                          conditions.Sorting,
                                                                          rowFrom,
                                                                          rowTo);
                    }

                    return result;
                });
        }

        public OperationResult<Dictionary<string, string>> GetTipsForMarks()
        {
            var c = SignificantChagesDictionaryCache.Instance();
            if (c.Cache == null)
            {
                var result = _helper.Do(() => TableAdapterCreator.DeclarationForInspector(_helper).GetTipsForMarks());
                c.Cache = result.Result;
            }
            return new OperationResult<Dictionary<string, string>>(c.Cache) {Status = ResultStatus.Success};
        }

        private void InspectorAttachRestrictionsToList(QueryConditions conditions)
        {
            bool isOr = false;

            var list = _localAuthProvider.GetUserStructContexts(PermissionType.Operation, Constants.SystemPermissions.Operations.RoleInspector);
            var inspections = new FilterQuery
            {
                ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.SOUN_CODE),
                GroupOperator = isOr ? FilterQuery.FilterLogicalOperator.Or : FilterQuery.FilterLogicalOperator.And,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or
            };

            if (list.Any())
            {
                if (!list.Contains(Constants.SystemPermissions.CentralDepartmentCode))
                {
                    foreach (var ifns in list)
                    {
                        if (ifns.Length >= 4 && ifns.Substring(2, 2) == "00")
                        {
                            string regionCode = ifns.Substring(0, 2);
                            inspections.Filtering.Add(new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.StartsWith, Value = regionCode });
                        }
                        else
                            inspections.Filtering.Add(new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = ifns });
                    }

                    conditions.Filter.Add(inspections);
                }
            }
            else
            {
                inspections.Filtering.Add(new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "-1" });
                conditions.Filter.Add(inspections);
            }

            isOr = true;
        }

        public OperationResult<List<string>> GetUserStructContexts(string context)
        {
            return _helper.Do(
                () =>
                {
                    return _localAuthProvider.GetUserStructContexts(PermissionType.Operation, context).ToList();
                },
                new string[] { }
                );
        }

        #endregion

        #region Запросы из карточки декларации

        public OperationResult<DeclarationSummary> GetDeclaration(string inn, string kppEffective, string year, string period, int typeCode)
        {
            OperationResult<DeclarationSummary> ret = null;

            var rez = _helper.DoEx<long>(
                () => TableAdapterCreator.Declaration(_helper).GetActual(inn, kppEffective, year, period, typeCode),
                new string[] { Constants.SystemPermissions.Operations.DeclarationViewCard });

            if (rez.Status == ResultStatus.Success)
            {
                ret = GetDeclaration(rez.Result);
            }
            else
                ret = new OperationResult<DeclarationSummary>() { Status = rez.Status, Message = rez.Message };

            return ret;
        }

        public OperationResult<DeclarationSummary> GetDeclaration(long id)
        {
            return GetDeclaration(id, () => { return TableAdapterCreator.Declaration(_helper).GetDeclaration(id); });
        }

        public OperationResult<DeclarationSummary> GetActualDeclaration(long id)
        {
            return GetDeclaration(id, () => { return TableAdapterCreator.Declaration(_helper).GetActualDeclaration(id); });
        }

        public OperationResult<DeclarationSummary> GetDeclaration(long id, Func<DeclarationSummary> getDeclaration)
        {
            return _helper.Do(
                () =>
                {
                    var ret = getDeclaration();

                    if (ret != null)
                    {
                        try
                        {
                            ret.Revisions = TableAdapterCreator.Declaration(_helper).GetVersions(
                                ret.INN,
                                ret.KPP_EFFECTIVE,
                                ret.PeriodEffective,
                                ret.FISCAL_YEAR,
                                ret.DECL_TYPE_CODE);
                        }
                        catch (Exception ex)
                        {
                            _helper.LogError(ex.Message, "GetDeclaration", ex);
                            throw new Exception(string.Format("Список корректировок: {0}", ex.Message));
                        }

                        using (var connection = new SingleConnectionFactory(_helper))
                        {
                            ret.ACTKNP = TableAdapterCreator.Declaration(_helper).GetActKnp(ret.DECLARATION_VERSION_ID);
                            if (ret.ACTKNP != null)
                            {
                                ret.SeodDecisionDocuments
                                    = SeodDecisionAdapterCreator.NewAdapter(_helper, connection)
                                        .Search(
                                            ret.INN,
                                            ret.INN_CONTRACTOR,
                                            ret.KPP_EFFECTIVE,
                                            ret.TAX_PERIOD,
                                            ret.FISCAL_YEAR);
                            }

                            ret.ComparisonDataVersion = FullCycleAdapterCreator.NewAdapter(_helper, connection).GetComparisonDataVersion();
                        }

                        ASKDekl decl = TableAdapterCreator.AIS(_helper).GetDeclaration(id);
                        if (decl != null)
                        {
                            FillDetailData(decl, ret);
                        }
                    }
                    else
                    {
                        throw new ObjectNotFoundException(string.Format("Декларация ID = {0} не найдена", id));
                    }

                    return ret;
                },
                new string[] { Constants.SystemPermissions.Operations.DeclarationViewCard }
                );
        }

        public OperationResult<bool> VerifyComparasionDataVersionActuality(long count)
        {
            return _helper.DoEx(
                () =>
                {
                    var ret = true;

                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var currentFullCycleCount = FullCycleAdapterCreator.NewAdapter(_helper, connection).GetComparisonDataVersion();

                        ret = currentFullCycleCount == count;
                    }

                    return ret;
                });
        }

        public OperationResult<bool> VerifySystemSupportInProcess()
        {
            return _helper.DoEx(
                () =>
                {
                    var ret = false;

                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapterFactory = new DecisionAdapterFactory(_helper);
                        var adapter = adapterFactory.UpdateStatusAdapter();
                        var currentStatus = adapter.GetStatus();

                        ret = currentStatus == 1;
                    }

                    return ret;
                });
        }


        private void FillDetailData(ASKDekl decl, DeclarationSummary summary)
        {
            //TODO создать иерархию из автогенеренных классов или хотя бы общий интерфейс из них выделить
            var version = new TwoLeveledVersion(decl.VersForm);
            if (version.OlderThan(new TwoLeveledVersion(DeclarationFormatVersionConst.FIVE_POINT_ZERO_FIVE)))
            {
                var tmp = EntitiesConverter.ASKDeklToФайл(decl);
                tmp.Документ.РегНом = summary.SEOD_DECL_ID.ToString();
                summary.DetailData = tmp.SerializeXML();
                #region Кириллица! Кириллица!!!

                summary.Chapter8xml = tmp.Документ.НДС.КнигаПокуп.НаимКнПок;
                summary.Chapter81xml = tmp.Документ.НДС.КнигаПокупДЛ.НаимКнПокДЛ;
                summary.Chapter9xml = tmp.Документ.НДС.КнигаПрод.НаимКнПрод;
                summary.Chapter91xml = tmp.Документ.НДС.КнигаПродДЛ.НаимКнПродДЛ;
                summary.Chapter10xml = tmp.Документ.НДС.ЖУчВыстСчФ.НаимЖУчВыстСчФ;
                summary.Chapter11xml = tmp.Документ.НДС.ЖУчПолучСчФ.НаимЖУчПолучСчФ;
                summary.Chapter12xml = tmp.Документ.НДС.ВыстСчФ_1735.НаимВыстСчФ_1735;

                #endregion
            }
            if (decl.VersForm == DeclarationFormatVersionConst.FIVE_POINT_ZERO_FIVE)
            {
                var tmp = EntitiesConverter.ASKDeklToФайл505(decl);
                tmp.Документ.РегНом = summary.SEOD_DECL_ID.ToString();
                summary.DetailData = tmp.SerializeXML();
                #region Кириллица! Кириллица!!!

                summary.Chapter8xml = tmp.Документ.НДС.КнигаПокуп.НаимКнПок;
                summary.Chapter81xml = tmp.Документ.НДС.КнигаПокупДЛ.НаимКнПокДЛ;
                summary.Chapter9xml = tmp.Документ.НДС.КнигаПрод.НаимКнПрод;
                summary.Chapter91xml = tmp.Документ.НДС.КнигаПродДЛ.НаимКнПродДЛ;
                summary.Chapter10xml = tmp.Документ.НДС.ЖУчВыстСчФ.НаимЖУчВыстСчФ;
                summary.Chapter11xml = tmp.Документ.НДС.ЖУчПолучСчФ.НаимЖУчПолучСчФ;
                summary.Chapter12xml = tmp.Документ.НДС.ВыстСчФ_1735.НаимВыстСчФ_1735;

                #endregion
            }
            summary.FormatVersion = decl.VersForm;
        }

        public OperationResult<DeclarationSummary> GetDeclaration(DeclarationKey key)
        {
            return key.Id.HasValue
                ? GetDeclaration(key.Id.Value)
                : GetDeclaration(key.Inn, key.Kpp, key.Year, key.Period, key.TypeCode);
        }

        public OperationResult<DeclarationSummary> GetActualDeclaration(DeclarationKey key)
        {
            return GetActualDeclaration(key.Id.Value);
        }

        public OperationResult<long> GetDeclarationLastZipInheritChapter(long zip, InvoiceRowKeyPartitionNumber chapter)
        {
            return _helper.DoEx(
                () =>
                {
                    var declarationPackageAdapter = DeclarationPackageAdapterCreator
                        .Create(_helper);

                    return declarationPackageAdapter.GetDeclarationChapterActualZips(zip, chapter);
                });
        }

        private Dictionary<InvoiceRowKeyPartitionNumber, long?> ConvertToDictionaryChapterZip(DeclarationChapterActualZip declaratioChapterActualZip)
        {
            var dictonary = new Dictionary<InvoiceRowKeyPartitionNumber, long?>()
            {
                { InvoiceRowKeyPartitionNumber.Chapter8, declaratioChapterActualZip.ActualZipChapterEight },
                { InvoiceRowKeyPartitionNumber.Chapter81, declaratioChapterActualZip.ActualZipChapterEightAddSheet },
                { InvoiceRowKeyPartitionNumber.Chapter9, declaratioChapterActualZip.ActualZipChapterNine },
                { InvoiceRowKeyPartitionNumber.Chapter91, declaratioChapterActualZip.ActualZipChapterNineAddSheet },
                { InvoiceRowKeyPartitionNumber.Chapter10, declaratioChapterActualZip.ActualZipChapterTen },
                { InvoiceRowKeyPartitionNumber.Chapter11, declaratioChapterActualZip.ActualZipChapterEleven },
                { InvoiceRowKeyPartitionNumber.Chapter12, declaratioChapterActualZip.ActualZipChapterTwelve }
            };

            return dictonary;
        }

        public OperationResult<PageResult<ControlRatio>> GetDeclarationControlRatio(long declarationVersionId, QueryConditions conditions)
        {
            conditions.Filter.Add(new FilterQuery
            {
                ColumnName = "Decl_Version_Id",
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>
                {
                    new ColumnFilter
                    {
                        Value = declarationVersionId,
                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                    }
                }
            });
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.ControlRatio(_helper).SelectDeclarationControlRatio(conditions), _helper);
        }

        public OperationResult<DeclarationRankedZip> GetContractorDeclarationZip(long inn, int year, int month)
        {
            return _helper.Do(() =>
            {
                var list = DeclarationPackageAdapterCreator.Create(_helper)
                    .GetContractorDeclarationZipList(inn, year, month);
                DeclarationRankedZip ret = null;
                if (list.Count() > 0)
                {
                    var maxRank = list.Max(x => x.RANK);
                    ret = list.First(x => x.RANK == maxRank);
                }
                return ret;
            });
        }

        #region Окно оперативной работы

        public OperationResult<PageResult<ContragentSummary>> GetContragentsList(string inn, string innReorganized,
                    string kppEffective, string year, string period, int typeCode, int chapter, QueryConditions conditions, bool needCount)
        {
            return _helper.Do(
                    () =>
                    {
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var searchCriteria = DeclarationContractorExpressionHelper
                                .CreateExpressionForContractorList(inn, innReorganized, kppEffective, year, period, typeCode, chapter, conditions);

                            var operationalWorkplaceContractorAdapter = OperationalWorkplaceContractorAdapterCreator
                                .OperationalWorkplaceContractorAdapter(_helper, connectionFactory);

                            int resultTotalMatches = 0;
                            List<ContragentSummary> resultRows = null;

                            if (needCount)
                                resultTotalMatches = operationalWorkplaceContractorAdapter.Count(searchCriteria);
                            else
                                resultRows = operationalWorkplaceContractorAdapter.Search(
                                        searchCriteria,
                                        conditions.Sorting,
                                        conditions.PaginationDetails.RowsToSkip,
                                        (uint)conditions.PaginationDetails.RowsToTake).ToList();

                            return new PageResult<ContragentSummary>
                            {
                                TotalMatches = resultTotalMatches,
                                Rows = resultRows,
                            };
                        }
                    }
            );
        }


        public OperationResult<PageResult<ContragentParamsSummary>> GetContragentParams(string inn, string innReorganized,
                    string kppEffective, string year, string period, int typeCode, string contragentInn, int chapter, QueryConditions conditions)
        {
            return _helper.Do(
                    () =>
                    {
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var searchCriteria = DeclarationContractorExpressionHelper
                                .CreateExpressionForContractorParams(inn, innReorganized, kppEffective, year, period, typeCode, contragentInn, chapter, conditions);

                            var operationalWorkplaceContractorAdapter = OperationalWorkplaceContractorAdapterCreator
                                .OperationalWorkplaceContractorParamsAdapter(_helper, connectionFactory);

                            List<ContragentParamsSummary> resultRows = null;

                            resultRows = operationalWorkplaceContractorAdapter.Search(
                                    searchCriteria,
                                    conditions.Sorting,
                                    conditions.PaginationDetails.RowsToSkip,
                                    (uint)conditions.PaginationDetails.RowsToTake).ToList();

                            return new PageResult<ContragentParamsSummary>
                            {
                                TotalMatches = resultRows.Count,
                                Rows = resultRows,
                            };
                        }
                    }
            );
        }

        public OperationResult<ContragentsSovOperation> GetContragentsDataSov(ContragentsSovOperation param)
        {
            return _helper.Do(
                () =>
                {
                    var point = "create adapter";
                    try
                    {
                        var adapter = TableAdapterCreator.ContragentsList(_helper);
                        point = "check param.RequestId.HasValue";
                        if (!param.RequestId.HasValue)
                        {
                            #region Lookup for in-process request

                            point = string.Format("in-process request, parse values: taxPeriod = {0}, taxYear = {1}, correctionNum = {2}",
                                param.Declaration.TAX_PERIOD, param.Declaration.FISCAL_YEAR, param.Declaration.CORRECTION_NUMBER);

                            var taxPeriod = int.Parse(param.Declaration.TAX_PERIOD);
                            var taxYear = int.Parse(param.Declaration.FISCAL_YEAR);
                            var correctionNum = int.Parse(param.Declaration.CORRECTION_NUMBER);

                            point += "... RequestIdContragentsData";

                            var rqId = adapter.RequestIdContragentsData(param.Declaration.INN, taxPeriod, taxYear,
                                correctionNum);

                            point = "check result";

                            if (!rqId.HasValue)
                            {
                                point = "create proxy";
                                var mcProxy = ClientProxyCreator.Create(
                                    _helper.GetConfigurationValue(Constants.MC_SERVICE_HOST),
                                    int.Parse(_helper.GetConfigurationValue(Constants.MC_SERVICE_PORT)));

                                point = "RequestContragentData";
                                rqId = mcProxy.RequestContragentData(param.Declaration.INN, taxPeriod, taxYear,
                                    correctionNum);
                            }

                            param.RequestId = rqId;
                            param.ExecutionStatus = RequestStatusType.InProcess;

                            #endregion
                        }
                        else
                        {
                            point = "StatusContragentsData";
                            var rez = adapter.StatusContragentsData(param.RequestId.Value);
                            point = "get ExecutionStatus";
                            param.ExecutionStatus = rez.Type;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + " @ " + point, e);
                    }

                    return param;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<ContragentsSovOperation> GetContragentParamsDataSov(ContragentsSovOperation param)
        {
            return _helper.Do(
                 () =>
                 {
                     var adapter = TableAdapterCreator.ContragentsList(_helper);
                     if (!param.RequestId.HasValue)
                     {
                         #region Lookup for in-process request

                         var taxPeriod = int.Parse(param.Declaration.TAX_PERIOD);
                         var taxYear = int.Parse(param.Declaration.FISCAL_YEAR);
                         var correctionNum = int.Parse(param.Declaration.CORRECTION_NUMBER);

                         var rqId = adapter.RequestIdContragentParamsData(param.Declaration.INN, taxPeriod, taxYear, correctionNum);
                         if (!rqId.HasValue)
                         {
                             var mcProxy = ClientProxyCreator.Create(
                                 _helper.GetConfigurationValue(Constants.MC_SERVICE_HOST),
                                 int.Parse(_helper.GetConfigurationValue(Constants.MC_SERVICE_PORT)));

                             rqId = mcProxy.RequestContragentParamsData(param.Declaration.INN, "", taxPeriod, taxYear, correctionNum);
                         }

                         param.RequestId = rqId;
                         param.ExecutionStatus = RequestStatusType.InProcess;

                         #endregion
                     }
                     else
                     {
                         var rez = adapter.StatusContragentParamsData(param.RequestId.Value);
                         param.ExecutionStatus = rez.Type;
                     }

                     return param;
                 },
                 GetDeclarationCardViewPermissions()
                 );
        }

        public OperationResult<DeclarationSummary> SearchLastFullLoadedCorrection(string innContractor, string kppEffective, string period, string year, int typeCode)
        {
            return _helper.Do(
                () =>
                {
                    return TableAdapterCreator.Declaration(_helper).SearchLastFullLoadedCorrection(innContractor, kppEffective, period, year, typeCode);
                });
        }

        #endregion


        public OperationResult<Dictionary<int, int>> GetChaptersSellers(long declId, string Inn)
        {
            return _helper.Do(() => TableAdapterCreator.Declaration(_helper).GetChaptersSellers(declId, Inn));
        }
        public OperationResult<Dictionary<int, int>> GetChaptersBuyers(long declId, string Inn)
        {
            return _helper.Do(() => TableAdapterCreator.Declaration(_helper).GetChaptersBuyers(declId, Inn));
        }

        public OperationResult<ConfigExportInvoice> GetConfigExportInvoice()
        {
            return _helper.Do<ConfigExportInvoice>(
                () =>
                {
                    return TableAdapterCreator.ConfigExportInvoice(_helper).Search();
                },
                new string[] { }
                );
        }

        #endregion

        #region Запросы из карточки НП

        public OperationResult<TaxPayer> GetTaxPayerByKppEffective(string inn, string kpp)
        {
            return _helper.Do(
                () =>
                {
                    var taxPayer = TableAdapterCreator.TaxPayer(_helper).GetByKppEffective(inn, kpp);

                    if (taxPayer == null)
                    {
                        if (string.IsNullOrWhiteSpace(kpp))
                        {
                            throw new ObjectNotFoundException(string.Format("Налогоплательщик с ИНН {0} не найден.", inn));
                        }
                        else
                        {
                            throw new ObjectNotFoundException(string.Format("Налогоплательщик с ИНН/КПП {0}/{1} не найден.", inn, kpp));
                        }
                    }

                    return taxPayer;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<TaxPayer> GetTaxPayerByKppOriginal(string inn, string kpp)
        {
            return _helper.Do(
                () =>
                {
                    var taxPayer = TableAdapterCreator.TaxPayer(_helper).GetByKppOriginal(inn, kpp);

                    if (taxPayer == null)
                    {
                        if (string.IsNullOrWhiteSpace(kpp))
                        {
                            throw new ObjectNotFoundException(string.Format("Налогоплательщик с ИНН {0} не найден.", inn));
                        }
                        else
                        {
                            throw new ObjectNotFoundException(string.Format("Налогоплательщик с ИНН/КПП {0}/{1} не найден.", inn, kpp));
                        }
                    }

                    return taxPayer;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<TaxPayer> GetTaxPayerByInn(string inn)
        {
            return _helper.Do(
                () =>
                {
                    var taxPayer = TableAdapterCreator.TaxPayer(_helper).GetByInn(inn);

                    if (taxPayer == null)
                    {
                        throw new ObjectNotFoundException(string.Format("Налогоплательщик с ИНН {0} не найден.", inn));
                    }

                    return taxPayer;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<DeclarationSummary> SearchLastDeclaration(string inn, string kpp)
        {
            return _helper.Do(
                () =>
                {
                    var declSummary = TableAdapterCreator.Declaration(_helper).SearchLastDeclaration(inn);
                    if (declSummary == null)
                    {
                        throw new ObjectNotFoundException(string.Format("Декларация с ИНН {0} не найдена.", inn));
                    }
                    return declSummary;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<Object> GetTaxPayerSUR(string inn, string kpp)
        {
            return _helper.Do<Object>(
                () =>
                {
                    var qc = new QueryConditions();

                    qc.Filter.Add(new FilterQuery()
                    {
                        ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x1 => x1.INN),
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering = new List<ColumnFilter>()
                                                               {
                                                                   new ColumnFilter()
                                                                       {
                                                                           Value = inn,
                                                                           ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                                                                       }
                                                               }
                    });

                    qc.Filter.Add(new FilterQuery()
                    {
                        ColumnName = TypeHelper<DeclarationSummary>.GetMemberName(x => x.KPP),
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering = new List<ColumnFilter>()
                                                               {
                                                                   new ColumnFilter()
                                                                       {
                                                                           Value = kpp,
                                                                           ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                                                                       }
                                                               }
                    });

                    qc.Sorting.Add(new ColumnSort()
                    {
                        ColumnKey = TypeHelper<DeclarationSummary>.GetMemberName(x2 => x2.SUBMISSION_DATE),
                        Order = ColumnSort.SortOrder.Desc
                    });

                    qc.Sorting.Add(new ColumnSort()
                    {
                        ColumnKey = TypeHelper<DeclarationSummary>.GetMemberName(x3 => x3.CORRECTION_NUMBER),
                        Order = ColumnSort.SortOrder.Desc
                    });

                    var item = TableAdapterCreator.Declaration(_helper).Search(qc).Rows.FirstOrDefault();

                    int? surCode = null;
                    if (item != null)
                    {
                        surCode = item.SUR_CODE;
                    }

                    return surCode;
                },
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<PageResult<BankAccount>> SelectULBankAccounts(QueryConditions criteriaSearch)
        {
            return _helper.Do(
                () => TableAdapterCreator.TaxPayer(_helper).GetULBankAccounts(criteriaSearch),
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<PageResult<BankAccount>> SelectFLBankAccounts(QueryConditions criteriaSearch)
        {
            return _helper.Do(
                () => TableAdapterCreator.TaxPayer(_helper).GetFLBankAccounts(criteriaSearch),
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<List<DeclarationPeriod>> GetAvailablePeriods(string inn, string kpp)
        {
            return _helper.Do(
                () => TableAdapterCreator.TaxPayer(_helper).GetAvailablePeriods(inn, kpp),
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<PageResult<BankAccountRequestSummary>> SelectBankAccountRequests(
            QueryConditions criteriaSearch, string inn, string kpp)
        {
            return _helper.Do(
                () =>
                {
                    var adapter = _helper.GetBankAccountRequestAdapter();
                    var filterByInnKpp = FilterExpressionCreator.CreateGroup()
                            .WithExpression(
                                FilterExpressionCreator.Create(
                                    TypeHelper<BankAccountRequestSummary>.GetMemberName(x => x.Inn),
                                    ColumnFilter.FilterComparisionOperator.Equals,
                                    inn))
                            .WithExpression(
                                FilterExpressionCreator.Create(
                                    TypeHelper<BankAccountRequestSummary>.GetMemberName(x => x.KppEffective),
                                    ColumnFilter.FilterComparisionOperator.Equals,
                                    kpp));

                    var columnReplacer = new SortColumnReplacer<BankAccountRequestSummary>();
                    columnReplacer
                        .WhenSortingBy(n => n.FullTaxPeriod)
                        .ReplaceWith(new List<Expression<Func<BankAccountRequestSummary, int>>>
                            {
                                n => n.FiscalYear,
                                n => n.SortOrderPeriod
                            });
                    columnReplacer.Optimize(criteriaSearch.Sorting);

                    return adapter
                        .Page(
                         filterByInnKpp,
                         criteriaSearch.Filter.ToFilterGroup(),
                         criteriaSearch);
                });
        }

        public OperationResult<PageResult<BankAccountBrief>> SelectULBankAccountBriefs(string inn, string kpp, DateTime begin, DateTime end, QueryConditions criteria)
        {

            return _helper.Do(
                () => TableAdapterCreator.TaxPayer(_helper).GetULBankAccountBriefs(inn, kpp, begin, end, criteria),
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<PageResult<BankAccountBrief>> SelectFLBankAccountBriefs(string inn, DateTime begin, DateTime end, QueryConditions criteria)
        {
            return _helper.Do(
                () => TableAdapterCreator.TaxPayer(_helper).GetFLBankAccountBriefs(inn, begin, end, criteria),
                GetDeclarationCardViewPermissions()
                );
        }

        public OperationResult<bool> PushBankAccountRequest(BankAccountManualRequest request)
        {
            return _helper.DoEx(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        var uacCache = UserAccessOperationCache.Instance(_helper, connectionFactory);

                        var creator = new UserAccessPolicyProvider(_localAuthProvider, uacCache, uacCache);

                        var permissions = creator.GetPermissions(Constants.SystemPermissions.Operations.TaxPayerRequestBankAccounts);

                        using (var scope = TransactionScopeHelper.DefaultTransactionScope())
                        {
                            var result = _helper.GetBankAccountsAdapter(connectionFactory)
                                .PushRequest(request, permissions.AllowedInspections);
                            if (result)
                            {
                                scope.Complete();
                            }
                            return result;
                        }
                    }
                }
                );
        }

        public OperationResult<List<long>> FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear)
        {
            return _helper.Do(
                () => _helper.GetBankAccountsAdapter(new SingleConnectionFactory(_helper))
                    .FindSentBankAccountRequest(inn, kppEffective, periodCode, fiscalYear)
                );
        }

        public OperationResult<List<string>> GetRequestedAccounts(string bik, DateTime begin, DateTime end)
        {
            return _helper.Do(
                () => _helper.GetBankAccountsAdapter(new SingleConnectionFactory(_helper))
                        .GetRequestedAccounts(bik, begin, end)
                );
        }

        public OperationResult<TaxPayerStatus> GetTaxPayerSolvencyStatus(string inn, string kppEffective)
        {
            return _helper.Do(
               () => TableAdapterCreator.TaxPayer(_helper).GetTaxPayerSolvencyStatus(inn, kppEffective)
               );
        }

        public OperationResult<ResetMarkResult> ResetMark(string innContractor, string kppEffective, int type, string period, string year)
        {
            return _helper.Do(
                () =>
                {
                    var resetMarkResult = new ResetMarkResult();

                    var inspectorSid = DeclarationPackageAdapterCreator.Create(_helper).GetDeclarationOwner(innContractor, kppEffective, type, period, year);

                    if (!string.IsNullOrWhiteSpace(inspectorSid))
                    {
                        if (inspectorSid == _localAuthProvider.CurrentUserSID)
                        {
                            TableAdapterCreator.DeclarationForInspector(_helper).ResetMark(innContractor, kppEffective, type, period, year);
                            resetMarkResult.Success = true;
                            resetMarkResult.DeclarationOwnerType = DeclarationOwnershipType.AssignedToMe;
                        }
                        else
                        {
                            resetMarkResult.Success = false;
                            resetMarkResult.DeclarationOwnerType = DeclarationOwnershipType.AssignedToOther;
                        }
                    }
                    else
                    {
                        resetMarkResult.Success = false;
                        resetMarkResult.DeclarationOwnerType = DeclarationOwnershipType.None;
                    }

                    return resetMarkResult;
                });
        }

        #endregion

        public List<DeclarationBrief> Search(string innCode, Period period, ConnectionFactoryBase connectionFactory)
        {
            IOperationalWorkplaceDeclarationsAdapter declarationsAdapter =
                OperationalWorkplaceAdaptersCreator.InspectorWorkplaceDeclarationsAdapter(_helper, connectionFactory);
            FilterExpressionGroup query = DeclarationFilterHelper.GetFilter(innCode, period).Filter.ToFilterGroup();

            List<DeclarationBrief> declarations = declarationsAdapter.Search(
                                                    query,
                                                    new List<ColumnSort>
                                                    {
                                                        new ColumnSort
                                                        {
                                                            ColumnKey = "INN",
                                                            Order = ColumnSort.SortOrder.Asc
                                                        }
                                                    },
                                                    0,
                                                    1);
            return declarations;
        }

        public OperationResult<KnpDocumentsData> KnpDocuments(
             string innDeclarant,
             string innReorganized,
             string kppEffective,
             int fiscalYear,
             int periodEffective)
        {
            return _helper.Do(
                 () =>
                 {
                     using (var connectionFactory = new SingleConnectionFactory(_helper))
                     {
                         var declarations = KnpDocumentsAdapterCreator.KnpDocumentsDeclarationAdapter(_helper,
                                 connectionFactory)
                             .SelectVersions(innDeclarant, innReorganized, kppEffective, periodEffective, fiscalYear);
                         var claims = KnpDocumentsAdapterCreator.KnpDocumentsClaimAdapter(_helper, connectionFactory)
                             .SelectByDeclaration(innDeclarant, innReorganized, kppEffective, periodEffective,
                                 fiscalYear);
                         var explains = KnpDocumentsAdapterCreator.KnpDocumentsExplainAdapter(_helper, connectionFactory)
                             .SelectByDeclaration(innDeclarant, innReorganized, kppEffective, periodEffective,
                                 fiscalYear);
                         var acts = KnpDocumentsAdapterCreator.KnpDocumentsActAdapter(_helper, connectionFactory)
                             .SelectByDeclaration(innDeclarant, innReorganized, kppEffective, periodEffective, fiscalYear);
                         var decisions = KnpDocumentsAdapterCreator.KnpDocumentsDecisionAdapter(_helper,
                                 connectionFactory)
                             .SelectByDeclaration(innDeclarant, innReorganized, kppEffective, periodEffective,
                                 fiscalYear);
                         return new KnpDocumentsData
                          {
                              Decisions = decisions,
                              Acts = acts,
                              Claims = claims,
                              Declarations = declarations,
                              Explains = explains
                          };
                     }
                 });
        }
    }
}
