﻿using CommonComponents.Shared;
using com = Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.ThriftProxy;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Server.Services
{
    public abstract class ServiceBase
    {
        protected ServiceHelpers Helper
        {
            get;
            private set;
        }

        protected com.IServiceProvider DataAccessService
        {
            get
            {
                return Helper;
            }
        }

        protected ServiceBase(IReadOnlyServiceCollection services)
        {
            Helper = new ServiceHelpers(services);
        }

        protected IClientProxy ThriftClient()
        {
            return ClientProxyCreator.Create(
                Helper.GetConfigurationValue(Constants.MC_SERVICE_HOST),
                int.Parse(Helper.GetConfigurationValue(Constants.MC_SERVICE_PORT)));
        }
    }
}
