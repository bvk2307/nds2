﻿namespace Luxoft.NDS2.Server.Services
{
    /// <summary>
    /// Этот интерфейс описывает операции работы с текущим пользователем
    /// </summary>
    public interface IIdentityProvider
    {
        /// <summary>
        /// Получает данные текущего пользователя
        /// </summary>
        /// <returns>Имя текущего пользователя</returns>
        string Current();
    }
}
