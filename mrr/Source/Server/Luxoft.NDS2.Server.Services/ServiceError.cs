﻿using System;

namespace Luxoft.NDS2.Server.Services
{
    /// <summary>
    /// Этот класс представляет ошибку уровня сервера приложения
    /// </summary>
    internal class ServiceError
    {
        private const string TextFormat = "Ошибка на сервере - {0}";

        private Guid _id = Guid.NewGuid();

        /// <summary>
        /// Возвращает текст ошибки
        /// </summary>
        public string Text
        {
            get { return string.Format(TextFormat, _id); }
        }
    }
}
