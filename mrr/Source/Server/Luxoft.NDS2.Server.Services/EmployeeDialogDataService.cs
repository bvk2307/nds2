﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IEmployeeDialogDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class EmployeeDialogDataService : IEmployeeDialogDataService
    {
        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public EmployeeDialogDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<EmployeeData> Load(long objectId, EmployeeType type)
        {
            return DalHelper.ExecuteOperation<EmployeeData>(() => TableAdapterCreator.EmployeeDialog(_helper).Load(objectId, type), _helper);
        }

        public OperationResult Save(Region region, EmployeeData data)
        {
            return DalHelper.Execute(() => TableAdapterCreator.EmployeeDialog(_helper).Save(region, data), _helper);
        }

        public OperationResult<List<string>> GetSIDsByCode(EmployeeType type, string code)
        {
            var result = GetStructContextUsers(code);
            if (result.Status != ResultStatus.Success)
            {
                return new OperationResult<List<string>>
                {
                    Message = result.Message,
                    Status = result.Status,
                    Result = new List<string>()
                };
            }
            Func<string, bool> rolePredicate = role =>
            {
                switch (type)
                {
                    case EmployeeType.Analyst:
                        return role == Constants.SystemPermissions.Operations.RoleAnalyst;
                    case EmployeeType.Approver:
                        return role == Constants.SystemPermissions.Operations.RoleApprover;
                    default:
                        return false;
                }
            };
            return new OperationResult<List<string>>
            {
                Status = ResultStatus.Success,
                Result = result.Result.Where(item => rolePredicate(item.Role)).Select(item => item.UserSid).ToList()
            };
        }

        private OperationResult<List<UserStructContextRight>> GetStructContextUsers(string structContext)
        {
            return _helper.Do(() =>
                _localAuthProvider.GetStructContextUserRigths(structContext)
                    .Where(sc => sc.Role == Constants.SystemPermissions.Operations.RoleAnalyst || sc.Role == Constants.SystemPermissions.Operations.RoleApprover).ToList());
        }
    }
}