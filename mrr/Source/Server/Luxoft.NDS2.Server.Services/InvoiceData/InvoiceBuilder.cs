﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using Luxoft.NDS2.Server.DAL;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.InvoiceData
{
    public class InvoiceBuilder
    {
        private readonly TaxPayerNamesLoader _taxPayerNamesLoader;

        private readonly IInvoiceExplainViewAdapter _invoiceExplainAdapter;

        private readonly IInvoiceContractorDeclarationAdapter _additionalAttributerAdapter;

        public InvoiceBuilder(
            TaxPayerNamesLoader taxPayerNamesLoader,
            IInvoiceExplainViewAdapter invoiceExplainAdapter,
            IInvoiceContractorDeclarationAdapter additionalAttributerAdapter)
        {
            _taxPayerNamesLoader = taxPayerNamesLoader;
            _invoiceExplainAdapter = invoiceExplainAdapter;
            _additionalAttributerAdapter = additionalAttributerAdapter;
        }

        private bool CheckNotData(DeclarationChapterData data)
        {
            return !data.PartDataList.All(x => x.DataReady) || data.Invoices == null || !data.Invoices.Any();
        }

        public DeclarationChapterData WithInvoiceExplain(DeclarationChapterData data)
        {
            if (CheckNotData(data))
                return data;

            var rowKeys = new List<string>();
            rowKeys.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.ROW_KEY))
                    .Select(dto => dto.ROW_KEY));

            var invoiceExplainDatas = LoadInvoiceExplainDatas(rowKeys);

            foreach (var invoice in data.Invoices)
            {
                var explainData = invoiceExplainDatas.Where(p => p.InvoiceKey == invoice.ROW_KEY).OrderBy(p => p.Rank).FirstOrDefault();
                if (explainData != null)
                {
                    invoice.ExplainNum = explainData.DocumentNumber;
                    invoice.ExplainDate = explainData.DocumentDate;
                    invoice.ExplainType = explainData.DocumentType;
                }
            }

            return data;
        }

        public DeclarationChapterData WithEgrn(DeclarationChapterData data)
        {
            if (CheckNotData(data))
                return data;

            var innList = new List<string>();

            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.BUYER_INN))
                    .Select(dto => dto.BUYER_INN));
            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.BROKER_INN))
                    .Select(dto => dto.BROKER_INN));
            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.SELLER_INN))
                    .Select(dto => dto.SELLER_INN));

            var innDictionary =
                _taxPayerNamesLoader
                    .Load(innList.ToArray())
                    .GroupBy(p => p.Inn)
                    .Select(p => p.First())
                    .ToDictionary((x) => x.Inn);


            foreach (var invoice in data.Invoices)
            {
                invoice.BUYER_NAME =
                    string.IsNullOrWhiteSpace(invoice.BUYER_INN)
                        || !innDictionary.ContainsKey(invoice.BUYER_INN)
                   ? string.Empty
                   : innDictionary[invoice.BUYER_INN].Name;
                invoice.BROKER_NAME =
                    string.IsNullOrWhiteSpace(invoice.BROKER_INN)
                        || !innDictionary.ContainsKey(invoice.BROKER_INN)
                   ? string.Empty
                   : innDictionary[invoice.BROKER_INN].Name;
                invoice.SELLER_NAME =
                    string.IsNullOrWhiteSpace(invoice.SELLER_INN)
                        || !innDictionary.ContainsKey(invoice.SELLER_INN)
                   ? string.Empty
                   : innDictionary[invoice.SELLER_INN].Name;
            }

            return data;
        }

        public DeclarationChapterData WithContragentSign(DeclarationChapterData data)
        {
            if (CheckNotData(data))
                return data;

            foreach (var invoice in data.Invoices.Where(p => p.IS_MATCHING_ROW))
            {
                invoice.CONTRACTOR_DECL_IN_MC = invoice.IS_MATCHING_ROW;
            }

            var contragentKeys = new List<object>();
            contragentKeys.AddRange(
                data
                    .Invoices
                    .Where(dto => dto.CONTRACTOR_KEY.HasValue && !dto.IS_MATCHING_ROW)
                    .Select(dto => (object)dto.CONTRACTOR_KEY.Value)
                    .Distinct());

            var contragentKeysResult = LoadContragentDecls(contragentKeys);

            foreach (var invoice in data.Invoices.Where(i => i.CONTRACTOR_KEY.HasValue && !i.IS_MATCHING_ROW))
            {
                if (contragentKeysResult.ContainsKey(invoice.CONTRACTOR_KEY.Value))
                {
                    invoice.CONTRACTOR_DECL_IN_MC = contragentKeysResult[invoice.CONTRACTOR_KEY.Value];
                }
                else
                {
                    invoice.CONTRACTOR_KEY = null;
                }
            }

            return data;
        }

        #region LoadInvoiceExplain 

        private static int MaxInListCapacity = 1000;

        private List<InvoiceExplain> LoadInvoiceExplainDatas(List<string> rowKeys)
        {
            List<InvoiceExplain> result = new List<InvoiceExplain>();

            if (rowKeys.Count > MaxInListCapacity)
            {
                var lastPieceOfDataCount = rowKeys.Count % MaxInListCapacity;
                int i = 0;
                for (; i < rowKeys.Count / MaxInListCapacity; i += MaxInListCapacity)
                {
                    result.AddRange(LoadInvoiceExplainDatasPartial(rowKeys.GetRange(i, MaxInListCapacity))); 
                }

                if (lastPieceOfDataCount > 0)
                {
                    result.AddRange(LoadInvoiceExplainDatasPartial(rowKeys.GetRange(i, lastPieceOfDataCount))); 
                }

                return result;
            }
            else
            {
                return LoadInvoiceExplainDatasPartial(rowKeys);
            }
           
        }

        private List<InvoiceExplain> LoadInvoiceExplainDatasPartial(List<string> rowKeys)
        {
            var filter = FilterExpressionCreator
                        .CreateGroup()
                        .WithExpression(rowKeys.ToArray().ToFilter(TypeHelper<Invoice>.GetMemberName(dto => dto.ROW_KEY)));

            return _invoiceExplainAdapter.Search(filter).OrderByDescending(p => p.Rank).ToList();    
        }

        #endregion LoadInvoiceExplain

        #region LoadContragentDecls

        private Dictionary<long, bool> LoadContragentDecls(List<object> contragentKeys)
        {
            if (contragentKeys.Count > MaxInListCapacity)
            {
                Dictionary<long, bool> result = new Dictionary<long, bool>();
                int i = 0;
                var lastPieceOfdataCount = contragentKeys.Count % MaxInListCapacity;

                for (; i < contragentKeys.Count / MaxInListCapacity; i += MaxInListCapacity)
                {
                    foreach (var res in LoadContragentDeclsPartial(contragentKeys.GetRange(i, MaxInListCapacity)))
                    {
                        if (!result.ContainsKey(res.Key))
                        {
                            result.Add(res.Key, res.Value);
                        }
                    }
                }

                if (lastPieceOfdataCount > 0)
                {
                    foreach (var res in LoadContragentDeclsPartial(contragentKeys.GetRange(i, lastPieceOfdataCount)))
                    {
                        if (!result.ContainsKey(res.Key))
                        {
                            result.Add(res.Key, res.Value);
                        }
                    }
                }

                return result;
            }
            else
            {
                return LoadContragentDeclsPartial(contragentKeys);
            }

        }

        private Dictionary<long, bool> LoadContragentDeclsPartial(List<object> contragentKeys)
        {
            var filter = FilterExpressionCreator.CreateGroup().WithExpression(contragentKeys.ToArray().ToFilter("CONTRACTOR_KEY"));

            return _additionalAttributerAdapter.Search(filter);
        }

        #endregion LoadContragentDecls
    }
}
