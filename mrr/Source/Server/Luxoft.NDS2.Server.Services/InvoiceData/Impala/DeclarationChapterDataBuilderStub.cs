﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.Impala
{
    public class DeclarationChapterDataBuilderStub
    {
        public DeclarationChapterData Build(DeclarationChapterDataRequest request)
        {
            if (request == null || request.ClientCache == null || !request.ClientCache.Any())
            {
                throw new ArgumentException("Некорректный запрос на раздел декларации. Отсутствует клиентский кеш в запросе.", "request");
            }

            var response = new DeclarationChapterData(request);
            response.UsableData = true;

            var customsJsonValue = new Dictionary<int, string>();
            customsJsonValue.Add(0, "[ { 10100001 }, { 1234234234 }, { 1231231 }, { 5555555 }, { 6666666 }, { 7777777 }, { 88888888} ]" );
            customsJsonValue.Add(1, "[ { 555555555 }, { 1234234234 }, { 1231231 }, { 111111111 }, { 332323232 }, { 454545454 }, { 565656565 } ]");
            customsJsonValue.Add(2, "[ { 10100001 }, { 1234234234 }, { 1231231 }, { 22222222 }, { 78787878788 }, { 8989898989 }, { 323232323 } ]");

            var list = new List<Invoice>();

            for (int i = -1; ++i < 3; )
            {
                list.Add(
                new Invoice()
                {
                    IS_CHANGED = null,
                    ExplainNum = null,
                    ExplainDate = null,
                    ExplainType = 0,
                    IS_MATCHING_ROW = false,
                    CONTRACTOR_KEY = null,
                    CONTRACTOR_DECL_IN_MC = false,
                    ORDINAL_NUMBER = null,
                    OPERATION_CODE = null,
                    INVOICE_NUM = string.Format("invoice num {0}", i),
                    INVOICE_DATE = DateTime.Now,
                    CHANGE_NUM = string.Format("change_num {0}", i),
                    CHANGE_DATE = DateTime.Now.AddDays(-1),
                    CORRECTION_NUM = string.Format("correction_num {0}", i),
                    CORRECTION_DATE = DateTime.Now.AddDays(-2),
                    CHANGE_CORRECTION_NUM = string.Format("CHANGE_CORRECTION_NUM {0}", i),
                    CHANGE_CORRECTION_DATE = DateTime.Now.AddDays(-3),
                    RECEIPT_DOC_NUM = null,
                    RECEIPT_DOC_DATE = null,
                    RECEIPT_DOC_DATE_STR = null,
                    BUY_ACCEPT_DATE = null,
                    SELLER_INN = null,
                    SELLER_KPP = null,
                    SELLER_INN_RESOLVED = null,
                    SELLER_KPP_RESOLVED = null,
                    SELLER_NAME = null,
                    BROKER_INN = null,
                    BROKER_KPP = null,
                    BROKER_NAME = null,
                    CREATE_DATE = null,
                    RECEIVE_DATE = null,
                    DeclarationId = request.DeclarationId,
                    CHAPTER = request.Chapter,
                    BUYER_INN = null,
                    BUYER_KPP = null,
                    BUYER_NAME = null,
                    CUSTOMS_DECLARATION_NUM = customsJsonValue[i % 3],
                    CUSTOMS_DECLARATION_NUMBERS = null,
                    OKV_CODE = null,
                    PRICE_BUY_AMOUNT = null,
                    PRICE_BUY_NDS_AMOUNT = null,
                    SELLER_INVOICE_NUM = null,
                    SELLER_INVOICE_DATE = null,
                    DEAL_KIND_CODE = null,
                    PRICE_SELL = null,
                    PRICE_SELL_IN_CURR = null,
                    PRICE_SELL_18 = null,
                    PRICE_SELL_10 = null,
                    PRICE_SELL_0 = null,
                    PRICE_NDS_18 = null,
                    PRICE_NDS_10 = null,
                    PRICE_TAX_FREE = null,
                    PRICE_TOTAL = null,
                    PRICE_NDS_TOTAL = null,
                    DIFF_CORRECT_DECREASE = null,
                    DIFF_CORRECT_INCREASE = null,
                    DIFF_CORRECT_NDS_DECREASE = null,
                    DIFF_CORRECT_NDS_INCREASE = null,
                    DECL_CORRECTION_NUM = null,
                    PRICE_NDS_BUYER = null,
                    FULL_TAX_PERIOD = null,
                    TAX_PERIOD = null,
                    FISCAL_YEAR = null,
                    IsAdditionalSheet = false,
                    SELLER_AGENCY_INFO_INN = null,
                    SELLER_AGENCY_INFO_KPP = null,
                    SELLER_AGENCY_INFO_NAME = null,
                    SELLER_AGENCY_INFO_NUM = null,
                    SELLER_AGENCY_INFO_DATE = null,
                    ROW_KEY = string.Format("{0}", request.DeclarationId, request.Chapter),
                    ACTUAL_ROW_KEY = null,
                    COMPARE_ROW_KEY = null,
                    COMPARE_ALGO_ID = 0,
                    FORMAT_ERRORS = null,
                    LOGICAL_ERRORS = null,
                    REQUEST_ID = 0
                });
            }

            response.MatchesQuantity = list.Count;
            response.Summaries = new Dictionary<string, decimal>();
            response.Invoices = list;

            ConvertInvoiceImpalaValuesToBusiness(response.Invoices);

            response.PartDataList = new List<DeclarationChapterSummary>()
            {
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter8 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter81 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter9 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter91 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter10 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter11 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter12 },
            };

            return response;
        }

        private void ConvertInvoiceImpalaValuesToBusiness(List<Invoice> invoices)
        {
            foreach (var item in invoices)
            {
                item.CHAPTER = (int)DeclarationInvoiceChapterNumberConverter.ToBusinessNumber((ImpalaInvoiceChapterNumber)item.CHAPTER);
                item.CUSTOMS_DECLARATION_NUMBERS = item.CUSTOMS_DECLARATION_NUM.FromJsonToStringArray();
                item.CUSTOMS_DECLARATION_NUM = item.CUSTOMS_DECLARATION_NUMBERS.GenerateShortCustomsDeclarationNum();
            }
        }
    }
}
