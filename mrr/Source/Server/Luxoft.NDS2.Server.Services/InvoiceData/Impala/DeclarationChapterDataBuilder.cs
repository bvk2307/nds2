﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;

namespace Luxoft.NDS2.Server.Services.InvoiceData.Impala
{
    public class DeclarationChapterDataBuilder
    {
        private readonly ISovInvoiceAdapter _invoiceFinder;

        public DeclarationChapterDataBuilder(ISovInvoiceAdapter invoiceFinder)
        {
            _invoiceFinder = invoiceFinder;
        }

        public DeclarationChapterData Build(DeclarationChapterDataRequest request)
        {
            if (request == null || request.ClientCache == null || !request.ClientCache.Any())
            {
                throw new ArgumentException("Некорректный запрос на раздел декларации. Отсутствует клиентский кеш в запросе.", "request");
            }

            var response = new DeclarationChapterData(request);
            var partitionCodes = _invoiceFinder.SearchOptions();

            #region Проверка доступности разделов

            response.UsableData = GetChapterUsable(request, partitionCodes);
            if (!response.UsableData)
            {
                response.Invoices = new List<Invoice>();
                response.Summaries = new Dictionary<string, decimal>();
                response.MatchesQuantity = 0;

                return response;
            }

            #endregion

            #region Запрос СФ

            request.SearchContext = new TransformSearchContextImpala().Transform(request.SearchContext);
            ConvertSearchContextToImpalaValues(request.SearchContext);
            var useAskData = !request.SearchContext.Filter.Any();
            var searchBy = request.SearchContext.Filter.ToFilterWithGroupOperator();

            if (request.ClientCache.Select(r => r.Zip).Distinct().Count() > 1)
            {
                var chaptersSubGroup = FilterExpressionCreator.CreateUnion();

                var impalaHint = FilterExpressionCreator.CreateInList("PARTZIP",
                request.ClientCache.Select(
                r => (object)(r.Zip % partitionCodes[(int)r.Part.ToBusinessNumber()].PartitionsTotal)));
                searchBy.WithExpression(impalaHint);

                foreach (var chapterInfo in request.ClientCache)
                {
                    chaptersSubGroup.WithExpression(
                        FilterExpressionCreator.CreateGroup()
                            .WithExpression(FilterExpressionCreator
                                .Create("DECLARATION_VERSION_ID", ColumnFilter.FilterComparisionOperator.Equals,
                                    chapterInfo.Zip))
                            .WithExpression(FilterExpressionCreator
                                .Create(TypeHelper<Invoice>.GetMemberName(t => t.CHAPTER),
                                    ColumnFilter.FilterComparisionOperator.Equals,
                                    ConvertChapterToImpalaChapter((int)chapterInfo.Part.ToBusinessNumber())
                                    ))
                            .WithExpression(FilterExpressionCreator.Create("PARTZIP", ColumnFilter.FilterComparisionOperator.Equals, chapterInfo.Zip % partitionCodes[(int)chapterInfo.Part.ToBusinessNumber()].PartitionsTotal))
                            );
                }

                searchBy.WithExpression(chaptersSubGroup);
            }
            else
            {
                var zip = request.ClientCache.Select(ch => ch.Zip).Distinct().FirstOrDefault();
                searchBy.WithExpression(FilterExpressionCreator
                        .Create("DECLARATION_VERSION_ID", ColumnFilter.FilterComparisionOperator.Equals,
                            zip));

                searchBy.WithExpression(FilterExpressionCreator
                        .Create("PARTZIP", ColumnFilter.FilterComparisionOperator.Equals,
                            zip % partitionCodes[request.Chapter].PartitionsTotal));

                searchBy.WithExpression(
                    FilterExpressionCreator.CreateInList(TypeHelper<Invoice>.GetMemberName(t => t.CHAPTER),
                    request.Chapter > 12 ? new object[] { ConvertChapterToImpalaChapter(request.Chapter) } :
                        request.ClientCache.Select(
                            ch =>
                            (object)(ConvertChapterToImpalaChapter((int)ch.Part.ToBusinessNumber()))
                            )));
            }

            var summaryFields = GetSummaryFields(request.Chapter);

            var countResult = _invoiceFinder.Count(request.Chapter, searchBy, summaryFields);
            response.MatchesQuantity = countResult.Count;
            response.Summaries = countResult.InvoicesSumms;

            var context = request.SearchContext.ToDataQueryContext();
            context.Where = searchBy;

            response.Invoices = _invoiceFinder.Search(request.Chapter, context).ToList();
            ConvertInvoiceImpalaValuesToBusiness(response.Invoices);

            #endregion

            response.PartDataList = new List<DeclarationChapterSummary>()
            {
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter8 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter81 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter9 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter91 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter10 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter11 },
                new DeclarationChapterSummary() { DataReady = true, Part = AskInvoiceChapterNumber.Chapter12 },
            };

            return response;
        }

        private bool CheckAllowableChapterNumber(int chapter)
        {
            int[] allowChapters = 
            { 
                (int)DeclarationInvoiceChapterNumber.Chapter8, 
                (int)DeclarationInvoiceChapterNumber.Chapter9, 
                (int)DeclarationInvoiceChapterNumber.Chapter10, 
                (int)DeclarationInvoiceChapterNumber.Chapter11, 
                (int)DeclarationInvoiceChapterNumber.Chapter12, 
                (int)DeclarationInvoiceChapterNumber.JournalChapter1, 
                (int)DeclarationInvoiceChapterNumber.JournalChapter2 
            };
            return allowChapters.Contains(chapter);
        }

        private IEnumerable<string> GetSummaryFields(int chapter)
        {
            if (!CheckAllowableChapterNumber(chapter)) { return new List<string>().ToArray(); }
            var result = new List<string>();
            switch (chapter)
            {
                case (int)DeclarationInvoiceChapterNumber.Chapter8:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_AMOUNT));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_NDS_AMOUNT));
                    break;
                case (int)DeclarationInvoiceChapterNumber.Chapter9:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_18));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_10));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_0));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_18));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_10));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TAX_FREE));
                    break;
                case (int)DeclarationInvoiceChapterNumber.Chapter10:
                case (int)DeclarationInvoiceChapterNumber.JournalChapter1:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TOTAL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_TOTAL));
                    break;
                case (int)DeclarationInvoiceChapterNumber.Chapter11:
                case (int)DeclarationInvoiceChapterNumber.JournalChapter2:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TOTAL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_TOTAL));
                    break;
                case (int)DeclarationInvoiceChapterNumber.Chapter12:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TAX_FREE));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_AMOUNT));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_NDS_AMOUNT));
                    break;
            }
            return result;
        }

        private int ConvertChapterToImpalaChapter(int originalChapterNumber)
        {
            return (int)DeclarationInvoiceChapterNumberConverter.ToImpalaNumber((DeclarationInvoicePartitionNumber)originalChapterNumber);
        }

        private void ConvertSearchContextToImpalaValues(QueryConditions searchContext)
        {
            foreach (var itemFilterQuery in searchContext.Filter)
            {
                if (itemFilterQuery.ColumnName == TypeHelper<Invoice>.GetMemberName(t => t.CHAPTER))
                {
                    foreach (var itemColumnFilter in itemFilterQuery.Filtering)
                    {
                        itemColumnFilter.Value = ConvertToImpalaChapterNumber(itemColumnFilter.Value);
                    }
                }
            }
        }

        private int ConvertToImpalaChapterNumber(object chapterValue)
        {
            int chapter = -1;
            if (chapterValue != null)
            {
                int result = -1;
                if (int.TryParse(chapterValue.ToString(), out result))
                {
                    chapter = result;
                }
            }
            return ConvertChapterToImpalaChapter(chapter);
        }

        private void ConvertInvoiceImpalaValuesToBusiness(List<Invoice> invoices)
        {
            foreach (var item in invoices)
            {
                item.CHAPTER = (int)DeclarationInvoiceChapterNumberConverter.ToBusinessNumber((ImpalaInvoiceChapterNumber)item.CHAPTER);

                item.CUSTOMS_DECLARATION_NUMBERS = item.CUSTOMS_DECLARATION_NUM.FromJsonToStringArray();
                item.CUSTOMS_DECLARATION_NUM = item.CUSTOMS_DECLARATION_NUMBERS.GenerateShortCustomsDeclarationNum();
            }
        }

        private bool GetChapterUsable(DeclarationChapterDataRequest request, Dictionary<int, ImpalaOption> partitionCodes)
        {
            bool usableData = true;
            foreach (var chapterInfo in request.ClientCache)
            {
                if (!partitionCodes[(int)chapterInfo.Part.ToBusinessNumber()].Usable)
                {
                    usableData = false;
                    break;
                }
            }
            return usableData;
        }
    }

    static class InvoiceHelper
    {
        private const int NumberThreshold = 3;

        public static string GenerateShortCustomsDeclarationNum(this string[] customsDeclarationNumbers)
        {
            if (customsDeclarationNumbers == null && customsDeclarationNumbers.Length == 0)
                return string.Empty;

            if (customsDeclarationNumbers.Length <= NumberThreshold)
                return string.Join(",", customsDeclarationNumbers);

            return string.Format("{0}...", string.Join(", ", customsDeclarationNumbers.Take(NumberThreshold)));
        }
    }
}
