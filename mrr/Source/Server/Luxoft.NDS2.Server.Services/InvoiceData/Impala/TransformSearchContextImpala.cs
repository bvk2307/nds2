﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.InvoiceData.Impala
{
    public class TransformSearchContextImpala
    {
        public QueryConditions Transform(QueryConditions queryConditionIn)
        {
            QueryConditions queryConditionsTransform = null;
            if (queryConditionIn != null)
            {
                queryConditionsTransform = (QueryConditions)queryConditionIn.Clone();

                TransformFilteringStringEmpty(queryConditionsTransform);

                TransformFilteringJson(queryConditionsTransform);

                TransformFiledBuyAcceptDate(queryConditionsTransform);

                TransformFieldStringToInt(queryConditionsTransform);
            }

            return queryConditionsTransform;
        }

        private void TransformFilteringStringEmpty(QueryConditions queryConditionsTransform)
        {
            var fieldNamesOfStringEmpty = new List<string>()
                {
                    TypeHelper<Invoice>.GetMemberName(t => t.OPERATION_CODE),
                    TypeHelper<Invoice>.GetMemberName(t => t.INVOICE_NUM),
                    TypeHelper<Invoice>.GetMemberName(t => t.SELLER_INN),
                    TypeHelper<Invoice>.GetMemberName(t => t.CORRECTION_NUM),
                    TypeHelper<Invoice>.GetMemberName(t => t.SELLER_KPP),
                    TypeHelper<Invoice>.GetMemberName(t => t.BROKER_INN),
                    TypeHelper<Invoice>.GetMemberName(t => t.BUYER_INN),
                    TypeHelper<Invoice>.GetMemberName(t => t.BUYER_KPP),
                    TypeHelper<Invoice>.GetMemberName(t => t.BROKER_KPP),
                    TypeHelper<Invoice>.GetMemberName(t => t.CUSTOMS_DECLARATION_NUM),
                    TypeHelper<Invoice>.GetMemberName(t => t.OKV_CODE),
                    TypeHelper<Invoice>.GetMemberName(t => t.DECL_CORRECTION_NUM),
                    TypeHelper<Invoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_INN),
                    TypeHelper<Invoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_KPP),
                    TypeHelper<Invoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_NUM)
                };

            new TransformStringEmpty(queryConditionsTransform, fieldNamesOfStringEmpty)
                .TransformFiltering();
        }

        private void TransformFilteringJson(QueryConditions queryConditionsTransform)
        {
            new TransformJsonOperationCode(queryConditionsTransform)
                .TransformFiltering();

            new TransformJsonPaymentDoc(queryConditionsTransform)
                .TransformFiltering();
        }

        private void TransformFiledBuyAcceptDate(QueryConditions queryConditionsTransform)
        {
            new TransformBuyAcceptDate(queryConditionsTransform)
                .TransformFiltering();
        }

        private void TransformFieldStringToInt(QueryConditions queryConditionsTransform)
        {
            var fieldNames = new List<string>()
                {
                    TypeHelper<Invoice>.GetMemberName(t => t.CHANGE_NUM),
                    TypeHelper<Invoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM)
                };

            new TransformStringToInt(queryConditionsTransform, fieldNames)
                .TransformFiltering();
        }
    }
}
