﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Server.Services.InvoiceData.SOV
{
    public class DeclarationInvoiceChapterInfoSov
    {
        /// <summary>
        /// Номер раздела 8, 8.1 и т.д.
        /// </summary>
        public AskInvoiceChapterNumber Part
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает ZIP актуальной корректировки для раздела
        /// </summary>
        public long Zip
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает идентификаторы запросов данных счетов-фактур
        /// </summary>
        public long? RequestKey
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает статус запросов данных счетов-фактур
        /// </summary>
        public RequestStatus RequestStatus
        {
            get;
            set;
        }

        #region Итоговые суммы

        public decimal PriceBuyAmountSum { get; set; }

        public decimal PriceBuyNdsAmountSum { get; set; }

        public decimal PriceSellSum { get; set; }

        public decimal PriceSell18Sum { get; set; }

        public decimal PriceSell10Sum { get; set; }

        public decimal PriceSell0Sum { get; set; }

        public decimal PriceNds18Sum { get; set; }

        public decimal PriceNds10Sum { get; set; }

        public decimal PriceTaxFreeSum { get; set; }

        public decimal PriceTotalSum { get; set; }

        public decimal PriceNdsTotalSum { get; set; }

        #endregion
    }
}
