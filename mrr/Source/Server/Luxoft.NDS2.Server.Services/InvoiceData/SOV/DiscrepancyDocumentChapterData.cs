﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.SOV
{
    public class DiscrepancyDocumentChapterData
    {
        public List<InvoiceRelated> Invoices { get; set; }
    }
}
