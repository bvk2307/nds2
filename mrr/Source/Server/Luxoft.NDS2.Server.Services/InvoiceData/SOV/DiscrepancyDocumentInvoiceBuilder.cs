﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.SOV
{
    public class DiscrepancyDocumentInvoiceBuilder
    {
        private readonly TaxPayerNamesLoader _taxPayerNamesLoader;

        public DiscrepancyDocumentInvoiceBuilder(TaxPayerNamesLoader taxPayerNamesLoader)
        {
            _taxPayerNamesLoader = taxPayerNamesLoader;
        }

        public DiscrepancyDocumentChapterData WithEgrn(DiscrepancyDocumentChapterData data)
        {
            if (!data.Invoices.Any())
            {
                return data;
            }

            var innList = new List<string>();

            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.BUYER_INN))
                    .Select(dto => dto.BUYER_INN));
            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.BROKER_INN))
                    .Select(dto => dto.BROKER_INN));
            innList.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.SELLER_INN))
                    .Select(dto => dto.SELLER_INN));

            var innDictionary =
                _taxPayerNamesLoader.Load(innList.ToArray())
                    .GroupBy(p => p.Inn)
                    .Select(p => p.First())
                    .ToDictionary((x) => x.Inn);

            var rowKeys = new List<string>();
            rowKeys.AddRange(
                data
                    .Invoices
                    .Where(dto => !string.IsNullOrWhiteSpace(dto.ROW_KEY))
                    .Select(dto => dto.ROW_KEY));

            foreach (var invoice in data.Invoices)
            {
                invoice.BUYER_NAME =
                    string.IsNullOrWhiteSpace(invoice.BUYER_INN)
                        || !innDictionary.ContainsKey(invoice.BUYER_INN)
                   ? string.Empty
                   : innDictionary[invoice.BUYER_INN].Name;
                invoice.BROKER_NAME =
                    string.IsNullOrWhiteSpace(invoice.BROKER_INN)
                        || !innDictionary.ContainsKey(invoice.BROKER_INN)
                   ? string.Empty
                   : innDictionary[invoice.BROKER_INN].Name;
                invoice.SELLER_NAME =
                    string.IsNullOrWhiteSpace(invoice.SELLER_INN)
                        || !innDictionary.ContainsKey(invoice.SELLER_INN)
                   ? string.Empty
                   : innDictionary[invoice.SELLER_INN].Name;
            }

            return data;
        }
    }
}
