﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Services.InvoiceData.SOV
{
    public class DeclarationChapterDataBuilder
    {
        private readonly ISovInvoiceRequestAdapter _requestFinder;

        private readonly ISovInvoiceAdapter _invoiceFinder;

        private readonly IClientProxy _requestProxy;

        public DeclarationChapterDataBuilder(
            ISovInvoiceRequestAdapter requestFinder,
            ISovInvoiceAdapter invoiceFinder,
            IClientProxy requestProxy)
        {
            _requestFinder = requestFinder;
            _invoiceFinder = invoiceFinder;
            _requestProxy = requestProxy;
        }

        public DeclarationChapterData Build(DeclarationChapterDataRequest request)
        {
            if (request == null || request.ClientCache == null || !request.ClientCache.Any())
            {
                throw new ArgumentException("Invalid declaration chapter request data", "request");
            }

            var response = new DeclarationChapterData(request);

            var chaptersToRequest = new List<DeclarationInvoiceChapterInfo>();

            var chaptersRequestSov = new List<DeclarationInvoiceChapterInfoSov>();

            #region Проверка доступности разделов

            response.UsableData = true;

            #endregion

            foreach (var cache in request.ClientCache)
            {
                #region Поиск запросов СОВ

                var partition = request.Chapter > 12 ? request.Chapter : (int)cache.Part.ToBusinessNumber();

                var dataCache =
                    _requestFinder.SearchByDeclaration(
                        cache.Zip,
                        partition);

                foreach (var itemRequest in dataCache)
                {
                    if (itemRequest != null)
                    {
                        var itemChapterRequestSov = new DeclarationInvoiceChapterInfoSov()
                        {
                            Part = cache.Part,
                            Zip = cache.Zip,
                            RequestKey = itemRequest.Id,
                            RequestStatus = itemRequest.Status,
                            PriceBuyAmountSum = itemRequest.PriceBuyAmountSum,
                            PriceBuyNdsAmountSum = itemRequest.PriceBuyNdsAmountSum,
                            PriceSellSum = itemRequest.PriceSellSum,
                            PriceSell18Sum = itemRequest.PriceSell18Sum,
                            PriceSell10Sum = itemRequest.PriceSell10Sum,
                            PriceSell0Sum = itemRequest.PriceSell0Sum,
                            PriceNds18Sum = itemRequest.PriceNds18Sum,
                            PriceNds10Sum = itemRequest.PriceNds10Sum,
                            PriceTaxFreeSum = itemRequest.PriceTaxFreeSum,
                            PriceTotalSum = itemRequest.PriceTotalSum,
                            PriceNdsTotalSum = itemRequest.PriceNdsTotalSum
                        };
                        chaptersRequestSov.Add(itemChapterRequestSov);
                    }
                }

                #endregion

                #region Подготовка необходимых запросов для СОВ

                var sovRequest = dataCache.FirstOrDefault();

                if (sovRequest == null)
                {
                    chaptersToRequest.Add(cache);
                }
                else if (!cache.DataReady)
                {
                    #region Данные уже запрошены, но еще не пришли

                    if (sovRequest.Status == RequestStatus.Failed ||
                        sovRequest.Status == RequestStatus.NotSuccessInNightMode)
                    {
                        throw new SovRequestProcessingException();
                    }
                    response.PartDataList.Find(cache.Part).DataReady =
                        (dataCache.Where(p=>p.Status == RequestStatus.Completed).Count() > 0) ? true : false;

                    #endregion
                }

                #endregion
            }

            #region Запрос данных из СОВа

            foreach (var item in chaptersToRequest)
            {
                var cacheSummary = chaptersRequestSov
                    .Where(p => p.Part == item.Part && p.Zip == item.Zip && 
                          (p.RequestStatus == RequestStatus.InProcess || p.RequestStatus == RequestStatus.Completed))
                    .FirstOrDefault();

                var data = response.PartDataList.Find(item.Part);

                if (cacheSummary != null)
                {
                    data.DataReady = cacheSummary.RequestStatus == RequestStatus.Completed;
                }
                else
                {
                    var partition = request.Chapter > 12 ? request.Chapter : (int)item.Part.ToBusinessNumber();
                    _requestProxy.RequestDeclarationChapter(
                            item.Zip,
                            partition,
                            item.Priority);
                    data.DataReady = false;
                }
            }

            #endregion

            #region Запрос СФ

            request.SearchContext = new TransformSearchContextSOV().Transform(request.SearchContext);

            if (response.PartDataList.All(x => x.DataReady))
            {
                var useAskData = !request.SearchContext.Filter.Any();
                var searchBy = request.SearchContext.Filter.ToFilterGroup();


                var requestedChapters = chaptersRequestSov.Where(x => x.RequestKey.HasValue && x.RequestStatus == RequestStatus.Completed);

                if (requestedChapters.Select(ch=>ch.Zip).Distinct().Count() > 1)
                {
                    var chaptersSubGroup = FilterExpressionCreator.CreateUnion();

                    foreach (
                        var chapterInfo in
                            chaptersRequestSov.Where(
                                x => x.RequestKey.HasValue && x.RequestStatus == RequestStatus.Completed))
                    {
                        chaptersSubGroup.WithExpression(
                            FilterExpressionCreator.CreateGroup()
                                .WithExpression(FilterExpressionCreator
                                    .Create("DECLARATION_VERSION_ID", ColumnFilter.FilterComparisionOperator.Equals,
                                        chapterInfo.Zip))
                                .WithExpression(FilterExpressionCreator
                                    .Create(TypeHelper<Invoice>.GetMemberName(t => t.CHAPTER),
                                        ColumnFilter.FilterComparisionOperator.Equals,
                                        request.Chapter > 12 ? (object)request.Chapter : (object)chapterInfo.Part.ToBusinessNumber())));
                    }

                    searchBy.WithExpression(chaptersSubGroup);
                }
                else
                {
                    searchBy.WithExpression(FilterExpressionCreator
                        .Create("DECLARATION_VERSION_ID", ColumnFilter.FilterComparisionOperator.Equals,
                            requestedChapters.Select(ch => ch.Zip).Distinct().FirstOrDefault()));


                    searchBy.WithExpression(
                        FilterExpressionCreator.CreateInList(TypeHelper<Invoice>.GetMemberName(t => t.CHAPTER),
                            requestedChapters.Select(ch => request.Chapter > 12 ? (object)request.Chapter : (object)ch.Part.ToBusinessNumber())));
                }

                var summaryFields = GetSummaryFields(request.Chapter);

                if (!request.SearchContext.PaginationDetails.SkipTotalMatches)
                {                    
                    if (useAskData)
                    {
                        response.MatchesQuantity = request.ClientCache.Sum(x => x.TotalQuantity);
                        response.Summaries = GetInvoicesSummsFromRequest(summaryFields, requestedChapters.ToList());
                    }
                    else
                    {
                        var countResult = _invoiceFinder.Count(request.Chapter, searchBy, summaryFields);
                        response.MatchesQuantity = countResult.Count;
                        response.Summaries = countResult.InvoicesSumms;
                    }
                }
                else 
                {
                    if (request.SearchContext.Filter.Count == 0)
                        response.Summaries = GetInvoicesSummsFromRequest(summaryFields, requestedChapters.ToList());
                    else
                        response.Summaries = GetInvoicesSummsFromCacheSummaries(request.ClientCacheSummaries);
                }
                var context = request.SearchContext.ToDataQueryContext();
                context.Where = searchBy;

                response.Invoices = _invoiceFinder.Search(request.Chapter, context).ToList();
            }

            #endregion

            return response;
        }

        private IEnumerable<string> GetSummaryFields(int chapter)
        {
            var result = new List<string>();
            switch (chapter)
            {
                case 8:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_AMOUNT));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_NDS_AMOUNT));
                    break;
                case 9:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_18));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_10));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_0));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_18));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_10));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TAX_FREE));
                    break;
                case 10:
                case 11:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TOTAL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_TOTAL));
                    break;
                case 12:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TAX_FREE));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_AMOUNT));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_NDS_AMOUNT));
                    break;
                case 13:
                case 14:
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TOTAL));
                    result.Add(TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_TOTAL));
                    break;
            }
            return result;
        }

        private Dictionary<string, string> CreateFieldMappingSummariesSum()
        {
            return new Dictionary<string, string>()
            {
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TAX_FREE), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceTaxFreeSum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceSellSum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_18), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceSell18Sum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_10), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceSell10Sum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_SELL_0), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceSell0Sum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_18), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceNds18Sum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_10), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceNds10Sum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_AMOUNT), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceBuyAmountSum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_BUY_NDS_AMOUNT), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceBuyNdsAmountSum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_TOTAL), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceTotalSum) },
                { TypeHelper<Invoice>.GetMemberName(c => c.PRICE_NDS_TOTAL), TypeHelper<DeclarationInvoiceChapterInfoSov>.GetMemberName(c => c.PriceNdsTotalSum) }
            };
        }

        /// <summary>
        /// Достает данные для Суммарной строки из запросов к СОВ 
        /// </summary>
        /// <param name="summaryFields">список полей</param>
        /// <param name="chaptersRequestSov">запросы к СОВ</param>
        /// <returns></returns>
        public Dictionary<string, decimal> GetInvoicesSummsFromRequest(IEnumerable<string> summaryFields, List<DeclarationInvoiceChapterInfoSov> chaptersRequestSov)
        {
            var invoicesSumms = new Dictionary<string, decimal>();
            var fieldMapping = CreateFieldMappingSummariesSum();
            foreach (var field in summaryFields)
            {
                if (fieldMapping.ContainsKey(field))
                {
                    var propertyField = typeof(DeclarationInvoiceChapterInfoSov).GetProperty(fieldMapping[field]);
                    decimal sum = 0;
                    foreach (var itemChapterRequest in chaptersRequestSov)
                    {
                        sum += (decimal)propertyField.GetValue(itemChapterRequest, null);
                    }
                    invoicesSumms.Add(field, sum);
                }
            }
            return invoicesSumms;
        }

        /// <summary>
        /// Достает данные для Суммарной строки из клиентского кэша итоговых сумм, запрошенных ранее из БД
        /// </summary>
        /// <param name="ClentCacheSummaries">кэш клиента итоговых сумм</param>
        /// <returns></returns>
        public Dictionary<string, decimal> GetInvoicesSummsFromCacheSummaries(Dictionary<string, decimal> ClentCacheSummaries)
        {
            var invoicesSumms = new Dictionary<string, decimal>();
            foreach (var item in ClentCacheSummaries)
            {
                invoicesSumms.Add(item.Key, item.Value);
            }
            return invoicesSumms;
        }
    }
}
