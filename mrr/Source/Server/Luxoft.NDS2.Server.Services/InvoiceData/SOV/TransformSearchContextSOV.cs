﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition;

namespace Luxoft.NDS2.Server.Services.InvoiceData.SOV
{
    public class TransformSearchContextSOV
    {
        public QueryConditions Transform(QueryConditions queryConditionIn)
        {
            QueryConditions queryConditionsTransform = null;
            if (queryConditionIn != null)
            {
                queryConditionsTransform = (QueryConditions)queryConditionIn.Clone();

                TransformFilteringPaymentDoc(queryConditionsTransform);

                TransformFiledBuyAcceptDate(queryConditionsTransform);
            }

            return queryConditionsTransform;
        }

        private void TransformFilteringPaymentDoc(QueryConditions queryConditionsTransform)
        {
            var transformer = new TransformJsonPaymentDocSOV(queryConditionsTransform);

            transformer.TransformFiltering();

            transformer.TransformSorting();
        }

        private void TransformFiledBuyAcceptDate(QueryConditions queryConditionsTransform)
        {
            new TransformBuyAcceptDateSOV(queryConditionsTransform)
                .TransformFiltering();
        }
    }
}
