﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Helpers;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformBuyAcceptDate : TransformFieldBase
    {
        private string _fieldName = TypeHelper<Invoice>.GetMemberName(t => t.BUY_ACCEPT_DATE);

        public TransformBuyAcceptDate(QueryConditions conditions)
            : base(conditions)
        {
        }

        public override void TransformFiltering()
        {
            var filterConditionClone = (QueryConditions)_conditions.Clone();

            _conditions.Filter =
                _conditions.Filter.Where(p => _fieldName != p.ColumnName).ToList();

            var filterTransforming =
                filterConditionClone.Filter.Where(p => _fieldName == p.ColumnName).ToList();

            foreach (FilterQuery filterQuery in filterTransforming)
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    if (columnFilter.Value == DBNull.Value ||
                        columnFilter.Value == null)
                    {
                        SearchContextHelper.AddFilter(
                            _conditions,
                            _fieldName,
                            filterQuery.FilterOperator,
                            columnFilter.Value,
                            columnFilter.ComparisonOperator);
                    }
                    else
                    {
                        string jsonValue = String.Empty;
                        var docDates = columnFilter.Value as DocDates;
                        if (docDates != null)
                        {
                            foreach(var item in docDates.Dates)
                            {
                                var dateOfIntegerView = ConvertDateToIntegerView(item);

                                SearchContextHelper.AddFilter(
                                    _conditions,
                                    _fieldName,
                                    filterQuery.FilterOperator,
                                    dateOfIntegerView,
                                    columnFilter.ComparisonOperator);
                            }
                        }
                        else
                        {
                            string docDatesString = columnFilter.Value as string;
                            if (docDatesString != null)
                            {
                                var dateOfIntegerView = ParserHelper.ParseDataTimeToInteger(docDatesString);

                                SearchContextHelper.AddFilter(
                                    _conditions,
                                    _fieldName,
                                    filterQuery.FilterOperator,
                                    dateOfIntegerView,
                                    columnFilter.ComparisonOperator);
                            }
                        }
                    }
                }
            }
        }

        private int ConvertDateToIntegerView(DateTime dt)
        {
            return dt.Year * 10000 + dt.Month * 100 + dt.Day;
        }
    }
}
