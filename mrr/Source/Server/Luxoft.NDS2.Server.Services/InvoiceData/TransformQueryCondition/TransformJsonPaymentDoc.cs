﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformJsonPaymentDoc : TransformJsonBase
    {
        protected List<ColumnFilter.FilterComparisionOperator> _operators = new List<ColumnFilter.FilterComparisionOperator>()
        {
            ColumnFilter.FilterComparisionOperator.Equals,
            ColumnFilter.FilterComparisionOperator.NotEquals,
        };

        public TransformJsonPaymentDoc(QueryConditions conditions)
            : base(conditions, new List<string>() { TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR) })
        {
        }

        protected override void TransformOneColumnFilter(FilterQuery filterQuery, ColumnFilter columnFilter)
        {
            if (columnFilter.Value == DBNull.Value || columnFilter.Value == null)
            {
                SearchContextHelper.AddFilter(
                    _conditions,
                    filterQuery.ColumnName,
                    filterQuery.FilterOperator,
                    "[]",
                    columnFilter.ComparisonOperator);
            }
            else
            {
                if (_operators.Contains(columnFilter.ComparisonOperator))
                {
                    string jsonValue = String.Empty;
                    var dates = columnFilter.Value as DocumentNumDates;
                    if (dates != null)
                    {
                        jsonValue = dates.RawString;
                    }
                    else
                    {
                        var paymentDocs = new PaymentDocParser().Parse(columnFilter.Value);
                        jsonValue = ConvertToJsonValue(paymentDocs);
                    }

                    SearchContextHelper.AddFilter(
                        _conditions,
                        TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR),
                        filterQuery.FilterOperator,
                        jsonValue,
                        columnFilter.ComparisonOperator);
                }
            }
        }

        public string ConvertToJsonValue(IEnumerable<PaymentDoc> values)
        {
            var stringBuilder = new StringBuilder();
            if (values.Count() > 0)
            {
                stringBuilder.Append("[");
                bool isFirst = true;
                foreach (var item in values)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        stringBuilder.Append(",");
                    stringBuilder.Append("{");
                    stringBuilder.AppendFormat("\"pnum\":\"{0}\", ", item.Number);
                    stringBuilder.AppendFormat("\"pdate\":\"{0}\"", item.Date);
                    stringBuilder.Append("}");
                }
                stringBuilder.Append("]");
            }
            return stringBuilder.ToString();
        }
    }
}
