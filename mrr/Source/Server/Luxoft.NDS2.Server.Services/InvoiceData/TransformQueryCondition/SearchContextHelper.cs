﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class SearchContextHelper
    {
        public static void AddFilter(
            QueryConditions queryCondition, 
            string columnName, 
            FilterQuery.FilterLogicalOperator filterQueryOperator,
            object value, 
            ColumnFilter.FilterComparisionOperator comparisionOperator)
        {
            FilterQuery filterQuery = queryCondition.Filter.Where(p => p.ColumnName == columnName).SingleOrDefault();
            if (filterQuery == null)
            {
                filterQuery = new FilterQuery();
                filterQuery.ColumnName = columnName;
                filterQuery.FilterOperator = filterQueryOperator;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = comparisionOperator;
                columnFilter.Value = value;
                filterQuery.Filtering.Add(columnFilter);
                queryCondition.Filter.Add(filterQuery);
            }
            else
            {
                if (filterQuery.Filtering.Where(p => ((p.Value != null && value != null && p.Value.Equals(value)) || (p.Value == null && value == null)) &&
                    p.ComparisonOperator == comparisionOperator).Count() == 0)
                {
                    filterQuery.FilterOperator = filterQueryOperator;
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = comparisionOperator;
                    columnFilter.Value = value;
                    filterQuery.Filtering.Add(columnFilter);
                }
            }
        }

        public static FilterQuery AddFilter(
            QueryConditions queryCondition,
            string columnName,
            FilterQuery.FilterLogicalOperator filterQueryOperator,
            FilterQuery.FilterLogicalOperator filterQueryGroupOperator,
            object value,
            ColumnFilter.FilterComparisionOperator comparisionOperator,
            FilterQuery filterQueryAddon)
        {
            FilterQuery result = filterQueryAddon;
            if (filterQueryAddon == null)
            {
                var filterQuery = new FilterQuery();
                filterQuery.ColumnName = columnName;
                filterQuery.FilterOperator = filterQueryOperator;
                filterQuery.GroupOperator = filterQueryGroupOperator;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = comparisionOperator;
                columnFilter.Value = value;
                filterQuery.Filtering.Add(columnFilter);
                queryCondition.Filter.Add(filterQuery);
                result = filterQuery;
            }
            else
            {
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = comparisionOperator;
                columnFilter.Value = value;
                filterQueryAddon.Filtering.Add(columnFilter);
            }
            return result;
        }
    }
}
