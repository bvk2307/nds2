﻿using Luxoft.NDS2.Server.Common.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation.DeclarationInvoices.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class PaymentDocParser
    {
        public List<PaymentDoc> Parse(object value)
        {
            string values = String.Empty;
            if (value != null)
                values = value.ToString();

            List<string> words = values.Split(',').ToList();
            var valueStrings = words.Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();

            var paymentDocs = new List<PaymentDoc>();
            if (valueStrings.Count > 0)
            {
                foreach (var itemValue in valueStrings)
                {
                    string valueOneDoc = RemoveWrapperPaymentDoc(itemValue);
                    var paymentDoc = ParsePaymentDoc(valueOneDoc);
                    if (paymentDoc != null)
                        paymentDocs.Add(paymentDoc);
                }
            }
            return paymentDocs;
        }

        private string RemoveWrapperPaymentDoc(string valueOneDoc)
        {
            if (valueOneDoc.StartsWith("'"))
                valueOneDoc = valueOneDoc.Substring(1, valueOneDoc.Length - 1);
            if (valueOneDoc.EndsWith("'"))
                valueOneDoc = valueOneDoc.Substring(0, valueOneDoc.Length - 1);

            return valueOneDoc;
        }

        private PaymentDoc ParsePaymentDoc(string value)
        {
            PaymentDoc result = null;

            var words = value.Trim().Split(' ').Where(p => !string.IsNullOrEmpty(p.Trim())).ToList();
            string docNum = null;
            string docPrefix = String.Empty;
            string docDate = null;

            int idxNum;
            int idxPrefix;
            int idxDate;
            GetIndexDocNumDate(words, out idxNum, out idxPrefix, out idxDate);

            if (idxNum > -1 && words.Count > idxNum)
                docNum = words[idxNum].Trim();
            if (idxPrefix > -1 && words.Count > idxPrefix)
                docPrefix = words[idxPrefix].Trim();
            if (idxDate > -1 && words.Count > idxDate)
                docDate = words[idxDate].Trim();

            if ((idxNum > -1 && idxPrefix > -1 && idxDate > -1 && words.Count == 3) ||
                (idxNum > -1 && idxPrefix == -1 && idxDate == -1 && words.Count == 1) ||
                (idxNum == -1 && idxPrefix > -1 && idxDate > -1 && words.Count == 2))
            {
                int? Date = ParserHelper.ParseDataTimeToInteger(docDate);

                result = new PaymentDoc() { Number = docNum, Date = Date };
            }

            return result;
        }

        private void GetIndexDocNumDate(List<string> words, out int idxNum, out int idxPrefix, out int idxDate)
        {
            idxPrefix = -1;
            idxNum = -1;
            idxDate = -1;
            int idx = 0;
            foreach (string item in words)
            {
                if (item.ToLower() == "от")
                {
                    idxPrefix = idx;
                    break;
                }
                idx++;
            }

            if (idxPrefix > -1)
            {
                idxNum = idxPrefix - 1;
                if (idxPrefix < (words.Count - 1))
                {
                    idxDate = idxPrefix + 1;
                }
            }
            else if (words.Count > 0)
            {
                idxNum = 0;
            }
        }
    }
}
