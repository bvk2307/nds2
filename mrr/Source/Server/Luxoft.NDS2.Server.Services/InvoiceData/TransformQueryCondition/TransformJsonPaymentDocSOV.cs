﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformJsonPaymentDocSOV : TransformFieldBase
    {
        public TransformJsonPaymentDocSOV(QueryConditions conditions)
            : base(conditions)
        {
        }

        public override void TransformFiltering()
        {
            var filterConditionClone = (QueryConditions)_conditions.Clone();

            var filterDocNumDates =
                filterConditionClone.Filter.Where(p => p.ColumnName == TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR)).ToList();
            _conditions.Filter =
                _conditions.Filter.Where(p => p.ColumnName != TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR)).ToList();

            foreach (FilterQuery filterQuery in filterDocNumDates)
            {
                foreach (Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    if (columnFilter.Value == DBNull.Value || columnFilter.Value == null)
                    {
                        SearchContextHelper.AddFilter(_conditions,
                            TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_NUM),
                            filterQuery.FilterOperator, columnFilter.Value, columnFilter.ComparisonOperator);
                        SearchContextHelper.AddFilter(_conditions,
                            TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE),
                            filterQuery.FilterOperator, columnFilter.Value, columnFilter.ComparisonOperator);
                    }
                    else
                    {
                        var dates = columnFilter.Value as DocumentNumDates;
                        if (dates != null)
                        {
                            #region для DocumentNumDates

                            var paymentdoc = dates.PaymentDocs.FirstOrDefault();
                            if (paymentdoc != null)
                            {
                                SearchContextHelper.AddFilter(_conditions, TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_NUM),
                                    filterQuery.FilterOperator, paymentdoc.Num, columnFilter.ComparisonOperator);
                                SearchContextHelper.AddFilter(_conditions, TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE),
                                    filterQuery.FilterOperator, paymentdoc.DateString, columnFilter.ComparisonOperator);
                            }

                            #endregion
                        }
                        else
                        {
                            #region для строки

                            var stringValue = columnFilter.Value as string;
                            if (stringValue == null)
                                continue;

                            stringValue = RemoveWrapperPaymentDoc(stringValue);

                            string docNumDate = stringValue;
                            var words = docNumDate.Trim().Split(' ').Where(p => !string.IsNullOrEmpty(p.Trim())).ToList();
                            string docNum = null;
                            string docPrefix = String.Empty;
                            string docDate = null;

                            int idxNum;
                            int idxPrefix;
                            int idxDate;
                            GetIndexDocNumDate(words, out idxNum, out idxPrefix, out idxDate);

                            if (idxNum > -1 && words.Count > idxNum)
                                docNum = words[idxNum].Trim();
                            if (idxPrefix > -1 && words.Count > idxPrefix)
                                docPrefix = words[idxPrefix].Trim();
                            if (idxDate > -1 && words.Count > idxDate)
                                docDate = words[idxDate].Trim();

                            if ((idxNum > -1 && idxPrefix > -1 && idxDate > -1 && words.Count == 3) ||
                                (idxNum > -1 && idxPrefix == -1 && idxDate == -1 && words.Count == 1) ||
                                (idxNum == -1 && idxPrefix > -1 && idxDate > -1 && words.Count == 2))
                            {
                                SearchContextHelper.AddFilter(_conditions,
                                    TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_NUM),
                                    filterQuery.FilterOperator, docNum, columnFilter.ComparisonOperator);
                                SearchContextHelper.AddFilter(_conditions,
                                    TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE),
                                    filterQuery.FilterOperator, docDate, columnFilter.ComparisonOperator);
                            }

                            #endregion
                        }
                    }
                }
            }
        }

        private void GetIndexDocNumDate(List<string> words, out int idxNum, out int idxPrefix, out int idxDate)
        {
            idxPrefix = -1;
            idxNum = -1;
            idxDate = -1;
            int idx = 0;
            foreach (string item in words)
            {
                if (item.ToLower() == "от")
                {
                    idxPrefix = idx;
                    break;
                }
                idx++;
            }

            if (idxPrefix > -1)
            {
                idxNum = idxPrefix - 1;
                if (idxPrefix < (words.Count - 1))
                {
                    idxDate = idxPrefix + 1;
                }
            }
            else if (words.Count > 0)
            {
                idxNum = 0;
            }
        }

        private string RemoveWrapperPaymentDoc(string valueOneDoc)
        {
            if (valueOneDoc.StartsWith("'"))
                valueOneDoc = valueOneDoc.Substring(1, valueOneDoc.Length - 1);
            if (valueOneDoc.EndsWith("'"))
                valueOneDoc = valueOneDoc.Substring(0, valueOneDoc.Length - 1);

            return valueOneDoc;
        }

        public override void TransformSorting()
        {
            var existsTransformField = _conditions.Sorting.Any(p => p.ColumnKey == TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR));

            if (!existsTransformField)
                return;

            var newSortList = new List<ColumnSort>();

            foreach (var columnSort in _conditions.Sorting)
            {
                if (columnSort.ColumnKey == TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE_STR))
                {
                    var col = (ColumnSort)columnSort.Clone();
                    col.ColumnKey = TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_NUM);
                    newSortList.Add(col);
                    col = (ColumnSort)columnSort.Clone();
                    col.ColumnKey = TypeHelper<Invoice>.GetMemberName(t => t.RECEIPT_DOC_DATE);
                    newSortList.Add(col);
                }
                else
                {
                    newSortList.Add(columnSort);
                }
            }

            _conditions.Sorting = newSortList;
        }
    }
}
