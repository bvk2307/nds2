﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformStringToInt : TransformFieldBase
    {
        protected List<string> _fieldNames= new List<string>();

        public TransformStringToInt(QueryConditions conditions, List<string> fieldNames)
            : base(conditions)
        {
            _fieldNames.AddRange(fieldNames);
        }

        public override void TransformFiltering()
        {
            foreach (FilterQuery filterQuery in _conditions.Filter.Where(p=> _fieldNames.Contains(p.ColumnName)))
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    if (columnFilter.Value != DBNull.Value && columnFilter.Value != null)
                    {
                        var stringValue = columnFilter.Value as string;
                        if (stringValue != null)
                        {
                            int? intValue = ParseToInt(stringValue);
                            if (intValue.HasValue)
                            {
                                columnFilter.Value = intValue.Value;
                            }
                        }
                    }
                }
            }
        }

        private int? ParseToInt(string value)
        {
            int? result = null;

            int intParse = 0;
            if (int.TryParse(value, out intParse))
            {
                result = intParse;
            }

            return result;
        }
    }
}
