﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformJsonOperationCode : TransformJsonBase
    {
        public TransformJsonOperationCode(QueryConditions conditions)
            : base(conditions, new List<string>() { TypeHelper<Invoice>.GetMemberName(t => t.OPERATION_CODE) })
        {
        }

        protected override void TransformOneColumnFilter(FilterQuery filterQuery, ColumnFilter columnFilter)
        {
            if (columnFilter.Value == DBNull.Value || columnFilter.Value == null)
            {
                SearchContextHelper.AddFilter(
                    _conditions,
                    filterQuery.ColumnName,
                    filterQuery.FilterOperator,
                    "[]",
                    columnFilter.ComparisonOperator);
            }                    
            else
            {
                if (columnFilter.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals ||
                    columnFilter.ComparisonOperator == ColumnFilter.FilterComparisionOperator.NotEquals)
                {
                    string jsonValue = ConvertToJsonValueForEquals(columnFilter.Value);
                    SearchContextHelper.AddFilter(
                            _conditions,
                            filterQuery.ColumnName,
                            filterQuery.FilterOperator,
                            jsonValue,
                            columnFilter.ComparisonOperator);
                }
                if (columnFilter.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Contains ||
                    columnFilter.ComparisonOperator == ColumnFilter.FilterComparisionOperator.DoesNotContain)
                {
                    var values = ConvertToJsonValuesForContains(columnFilter.Value);

                    FilterQuery filterQueryAddon = null;
                    Luxoft.NDS2.Common.Contracts.DTO.Query.FilterQuery.FilterLogicalOperator groupOperator = filterQuery.FilterOperator;
                    foreach (var item in values)
                    {
                        filterQueryAddon = SearchContextHelper.AddFilter(
                                _conditions,
                                filterQuery.ColumnName,
                                item.FilterOperator,
                                groupOperator,
                                item.Value,
                                columnFilter.ComparisonOperator,
                                filterQueryAddon);
                    }
                }
            }
        }

        private string ConvertToJsonValueForEquals(object value)
        {
            string values = value.ToString();
            List<string> words = values.Split(',').ToList();
            var valueStrings = words.Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();

            var stringBuilder = new StringBuilder();
            if (valueStrings.Count > 0)
            {
                stringBuilder.Append("[");
                bool isFirst = true;
                foreach (var itemValue in valueStrings)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        stringBuilder.Append(",");
                    stringBuilder.Append("{");
                    stringBuilder.AppendFormat("{0}", itemValue);
                    stringBuilder.Append("}");
                }
                stringBuilder.Append("]");
            }
            return stringBuilder.ToString();
        }

        private List<ValueWrapper> ConvertToJsonValuesForContains(object value)
        {
            var result = new List<ValueWrapper>();

            string values = value.ToString();
            List<string> words = values.Split(',').ToList();
            var valueStrings = words.Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();

            if (valueStrings.Count > 0)
            {
                var stringBuilder = new StringBuilder();
                foreach (var itemValue in valueStrings)
                {
                    bool useDefault = true;
                    if (itemValue.Length == 1 && valueStrings.Count() == 1)
                    {
                        int? intValue = TryParseInt(itemValue);
                        if (intValue != null && intValue >= 0 && intValue < 10)
                        {
                            useDefault = false;

                            var valueWrapper = new ValueWrapper();

                            stringBuilder.Clear();
                            stringBuilder.Append("{");
                            stringBuilder.AppendFormat("{0}", intValue);
                            valueWrapper.Value = stringBuilder.ToString();
                            valueWrapper.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                            result.Add(valueWrapper);

                            stringBuilder.Clear();
                            stringBuilder.AppendFormat("{0}", intValue);
                            stringBuilder.Append("}");
                            valueWrapper.Value = stringBuilder.ToString();
                            valueWrapper.FilterOperator = FilterQuery.FilterLogicalOperator.Or;
                            result.Add(valueWrapper);
                        }
                    }
                    if (useDefault)
                    {
                        var valueWrapper = new ValueWrapper();

                        stringBuilder.Clear();
                        stringBuilder.Append("{");
                        stringBuilder.AppendFormat("{0}", itemValue);
                        stringBuilder.Append("}");
                        valueWrapper.Value = stringBuilder.ToString();
                        valueWrapper.FilterOperator = FilterQuery.FilterLogicalOperator.And;
                        result.Add(valueWrapper);
                    }
                }
            }

            return result;
        }

        private int? TryParseInt(string value)
        {
            int? result = null;
            int valueParse = 0;
            if (int.TryParse(value, out valueParse))
            {
                result = valueParse;
            }
            return result;
        }
    }

    class ValueWrapper
    {
        public Luxoft.NDS2.Common.Contracts.DTO.Query.FilterQuery.FilterLogicalOperator FilterOperator  { get; set; }

        public string Value { get; set; }

        public ValueWrapper()
        {
        }
    }
}
