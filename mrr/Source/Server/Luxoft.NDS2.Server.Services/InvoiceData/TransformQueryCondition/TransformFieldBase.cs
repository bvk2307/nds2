﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public abstract class TransformFieldBase
    {
        protected QueryConditions _conditions;

        public TransformFieldBase(QueryConditions conditions)
        {
            _conditions = conditions;
        }

        public virtual void TransformFiltering()
        {
        }

        public virtual void TransformSorting()
        {
        }
    }
}
