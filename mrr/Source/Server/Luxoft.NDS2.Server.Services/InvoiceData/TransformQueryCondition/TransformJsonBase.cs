﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.InvoiceData.TransformQueryCondition
{
    public class TransformJsonBase : TransformFieldBase
    {
        protected List<string> _fieldNames = new List<string>();

        public TransformJsonBase(QueryConditions conditions, List<string> fieldNames)
            : base(conditions)
        {
            _fieldNames.AddRange(fieldNames);
        }

        public override void TransformFiltering()
        {
            var filterConditionClone = (QueryConditions)_conditions.Clone();

            _conditions.Filter =
                _conditions.Filter.Where(p => !_fieldNames.Contains(p.ColumnName)).ToList();

            var filterTransforming =
                filterConditionClone.Filter.Where(p => _fieldNames.Contains(p.ColumnName)).ToList();

            foreach (FilterQuery filterQuery in filterTransforming)
            {
                foreach (ColumnFilter columnFilter in filterQuery.Filtering)
                {
                    TransformOneColumnFilter(filterQuery, columnFilter);
                }
            }
        }

        protected virtual void TransformOneColumnFilter(FilterQuery filterQuery, ColumnFilter columnFilter)
        {
        }
    }
}
