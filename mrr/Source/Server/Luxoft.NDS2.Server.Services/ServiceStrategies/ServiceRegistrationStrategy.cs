﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Communication;
using CommonComponents.GenericHost;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using Luxoft.NDS2.Server.Services.Claims;
using Luxoft.NDS2.Server.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.Services.EfficiencyMonitoring;
using Luxoft.NDS2.Server.Services.Selections;
using Luxoft.NDS2.Server.Services.SystemSettings;
using Luxoft.NDS2.Server.Services.UserTasks;

namespace Luxoft.NDS2.Server.Services.ServiceStrategies
{
    public class ServiceRegistrationStrategy
    {
        [PreHandler(ApplicationHostEvents.Activate)]
        public void OnActivate(IStrategyContext context)
        {
            var serviceHost = context.ServiceCollection.Get<IServiceHost>();
            serviceHost.Add(typeof(IActDecisionReportService), typeof(ActDecisionReportService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IAppendActDiscrepancyService), typeof(ActDiscrepancyAppendService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IEditActDiscrepancyService), typeof(ActDiscrepancyEditService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IActivityLogService), typeof(ActivityLogService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ICalendarDataService), typeof(CalendarDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IAppendDecisionDiscrepancyService), typeof(DecisionDiscrepancyAppendService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IEditDecisionDiscrepancyService), typeof(DecisionDiscrepancyEditService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IExplainInvoiceService), typeof(ExplainInvoiceService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IExplainTksInvoiceService), typeof(ExplainTksInvoiceService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IFileService), typeof(FileService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IEchoService), typeof(EchoService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInvoiceDataService), typeof(InvoiceDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IMacroReportDataService), typeof(MacroReportDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(INavigatorReportDataService), typeof(NavigatorReportDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IObjectLocker), typeof(ObjectLockerService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ILoadTestHelpService), typeof(LoadTestHelpService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDiscrepancyDocumentService), typeof(DiscrepancyDocumentDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IControlRatioService), typeof(ControlRatioService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDictionaryDataService), typeof(DictionaryDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDiscrepancyDataService), typeof(DiscrepancyDataServer), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDataService), typeof(DataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDeclarationsDataService), typeof(DeclarationsDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IEmployeeDialogDataService), typeof(EmployeeDialogDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IPyramidDataService), typeof(PyramidDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IReportService), typeof(ReportService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IExplainReplyDataService), typeof(ExplainReplyDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISelectionsDataService), typeof(SelectionsDataService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISampleService), typeof(SampleService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISecurityService), typeof(SecurityService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IUserInformationService), typeof(UserInformationService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IUserToRegionService), typeof(UserToRegionService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IVersionInfoService), typeof(VersionInfoService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IUserTaskService), typeof(UserTaskService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IUserTaskReportService), typeof(UserTaskReportService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInspectorDeclarationService), typeof(InspectorDeclarationService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInspectorStatisticsService), typeof(InspectorStaisticsService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInspectorService), typeof(InspectorService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ITaxpayerAssignmentService), typeof(TaxpayerAssignmentService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ITaxpayerService), typeof(TaxpayerService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IWorkloadDefaultsService), typeof(WorkloadDefaultsService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IWorkloadService), typeof(InspectorDecarationAssignmentService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IEfficiencyMonitoringService), typeof(EfficiencyMonitoringService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISystemSettingsService), typeof(SystemSettingsService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISettingsListsService), typeof(SettingsListsService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(ISelectionService), typeof(SelectionService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInvoiceClaimService), typeof(InvoiceClaimService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IInvoiceClaimReportService), typeof(InvoiceClaimReportService), ServiceInstanceMode.PerCall);
            serviceHost.Add(typeof(IDiscrepancyForDeclarationCardService), typeof(DiscrepancyForDeclarationCardService), ServiceInstanceMode.PerCall);
        }

        [PreHandler(ApplicationHostEvents.Deactivate)]
        public void OnDeactivate(IStrategyContext context)
        {

        }
    }
}
