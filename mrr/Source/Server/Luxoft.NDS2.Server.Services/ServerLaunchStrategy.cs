﻿using CommonComponents.GenericHost;
using CommonComponents.Shared;
using System;

namespace Luxoft.NDS2.Server.Services
{
    public class ServerLaunchStrategy
    {

        [PreHandler(ApplicationHostEvents.Activate)]
        public void OnActivate(IStrategyContext context)
        {
            var serv = context.ServiceCollection.Get<ServerService>();
            serv.Start();
            Console.WriteLine(@"ServerLaunchStrategy - Activated");
        }

        [PreHandler(ApplicationHostEvents.Deactivate)]
        public void OnDeactivate(IStrategyContext context)
        {
            var serv = context.ServiceCollection.Get<ServerService>();
            serv.Shutdown();
            Console.WriteLine(@"ServerLaunchStrategy - Deactivated");
        }

    }

}
