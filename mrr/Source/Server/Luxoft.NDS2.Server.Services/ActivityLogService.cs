﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IActivityLogService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ActivityLogService : IActivityLogService
    {
        private readonly ServiceHelpers _helper;

        public ActivityLogService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult Write(string sid, ActivityLogEntry entry)
        {
            return DalHelper.Execute(() => TableAdapterCreator.ActivityLog(_helper).Write(entry, sid), _helper);
        }
    }
}
