﻿using System;
using System.Collections.Generic;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Query;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(ILoadTestHelpService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public sealed class LoadTestHelpService : ILoadTestHelpService
    {
        private readonly ServiceHelpers _helper;
        private readonly IQueryProvider _qProvider;
        public LoadTestHelpService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _qProvider = services.Get<IQueryProvider>();
        }



        #region Implementation of ILoadTestHelpService

        public OperationResult<List<long>> GetDeclarationForBookLoad(string sonoCode, int chapterToLoad, int countOfData)
        {
            var res = _helper.Do<List<long>>(() =>
                {
                    var data = TableAdapterCreator.LoadTestHelper(_helper).Get(sonoCode, chapterToLoad, countOfData);

                    return data;
                });


            return res;
        }

        public OperationResult<List<string>> GetQueryText(string scope, string qName)
        {
            var ret =  new OperationResult<List<string>>(new List<string>(0));
            try
            {
                ret.Result.Add(_qProvider.GetQueryText(scope, qName));
            }
            catch (Exception ex)
            {
                ret.Message = ex.ToString();
                ret.Status = ResultStatus.Error;
            }

            return ret;
        }

        #endregion
    }
}