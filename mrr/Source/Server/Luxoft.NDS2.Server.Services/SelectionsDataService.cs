﻿using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Selections;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(ISelectionsDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SelectionsDataService : ISelectionsDataService
    {
        private const string DECLARATION_STATUS_KNP_OPEN = "Открыта";
        private readonly ServiceHelpers _helper;

        public SelectionsDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<DeclarationPageResult> SearchDeclarations(long selectionId, QueryConditions criteria)
        {
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        var selectionDeclarationAdpater = _helper
                            .SelectionDeclarationsAdapter(connectionFactory);

                        QueryConditionsHelper.AddFilter(
                            criteria,
                            "SelectionId",
                            FilterQuery.FilterLogicalOperator.And,
                            selectionId,
                            ColumnFilter.FilterComparisionOperator.Equals);

                        QueryConditionsHelper.AddFilter(
                            criteria,
                            TypeHelper<DeclarationSummary>.GetMemberName(p => p.STATUS),
                            FilterQuery.FilterLogicalOperator.And,
                            DECLARATION_STATUS_KNP_OPEN,
                            ColumnFilter.FilterComparisionOperator.Equals);

                        var searchCriteria = criteria.Filter.ToFilterGroup();

                        var knpStatusKey = TypeHelper<SelectionDeclaration>.GetMemberName(p => p.StatusCode);
                        var knpStatusSorting = criteria.Sorting.Find(v => v.ColumnKey == knpStatusKey);
                        if (knpStatusSorting != null)
                            knpStatusSorting.ColumnKey =
                                TypeHelper<SelectionDeclaration>.GetMemberName(p => p.StatusCode);

                        var resultRows = selectionDeclarationAdpater.Search(
                            searchCriteria,
                            criteria.Sorting,
                            (int) criteria.PaginationDetails.RowsToSkip,
                            (int?) criteria.PaginationDetails.RowsToTake);

                        var resultTotalMatches = selectionDeclarationAdpater.Count(searchCriteria);

                        return new DeclarationPageResult
                        {
                            TotalMatches = resultTotalMatches,
                            Rows = resultRows.ToList(),
                            State = selectionDeclarationAdpater.GetDeclarationState(selectionId)
                        };
                    }
                }
            );
        }
    }
}