﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Luxoft.NDS2.Server.Services.Managers.Common
{
    public static class Excel
    {
        public static string ConvertToExcel2007(string path2003)
        {
            var path2007 = new FileInfo(path2003).Directory.FullName;
            path2007 = Path.Combine(path2007, Path.GetFileNameWithoutExtension(path2003) + ".xlsx");
            if (path2007.Equals(path2003, StringComparison.InvariantCultureIgnoreCase))
            {
                return path2007;
            }

            var book = Infragistics.Excel.Workbook.Load(path2003);
            if (book.CurrentFormat == Infragistics.Excel.WorkbookFormat.Excel97To2003)
            {
                book.SetCurrentFormat(Infragistics.Excel.WorkbookFormat.Excel2007);
            }
            book.Save(path2007);
            return path2007;
        }

        public static void SetValue(this Cell cell, string value)
        {
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
            cell.CellValue = new CellValue(value);
        }

        public static void SetValue(this Cell cell, int value)
        {
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
            cell.CellValue = new CellValue(value.ToString(CultureInfo.InvariantCulture));
        }

        public static void SetValue(this Cell cell, long value)
        {
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
            cell.CellValue = new CellValue(value.ToString(CultureInfo.InvariantCulture));
        }

        public static Cell GetCell(this Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            return row.Elements<Cell>().First(c => String.Compare(c.CellReference.Value, columnName + rowIndex, StringComparison.OrdinalIgnoreCase) == 0);
        }

        public static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().Elements<Row>().First(r => r.RowIndex == rowIndex);
        }

        public static string GetValueSafe(this Dictionary<int, string> dictionary, int key, string defaultValue = null)
        {
#if DEBUG
            return (dictionary.ContainsKey(key) ? dictionary[key] : defaultValue) ?? string.Format("#[{0}]", key);
#else
            return (dictionary.ContainsKey(key) ? dictionary[key] : defaultValue) ?? string.Empty;
#endif
        }

        public static long GetNumberSafe(this Dictionary<int, string> dictionary, int key, int defaultValue = 0)
        {
            var value = dictionary.GetValueSafe(key, defaultValue.ToString());
            long number;
            return long.TryParse(value, out number) ? number : defaultValue;
        }

        public static TimeSpan GetTimeSpanSafe(this Dictionary<int, string> dictionary, int key)
        {
            var value = dictionary.GetValueSafe(key);
            TimeSpan ts;
            return TimeSpan.TryParse(value, out ts) ? ts : TimeSpan.FromSeconds(0);
        }

        public static string ColumnAddressByNumber(int number)
        {
            var columnName = new StringBuilder();
            int div = number;
            while (div > 0)
            {
                int mod = (div - 1) % 26;
                columnName.Insert(0, Convert.ToChar(65 + mod));
                div = (int)((div - mod) / 26);
            }
            return columnName.ToString();
        }
    }
}