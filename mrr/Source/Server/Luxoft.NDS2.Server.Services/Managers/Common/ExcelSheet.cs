﻿using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Luxoft.NDS2.Server.Services.Managers.Common
{
    class ExcelSheet
    {
        public Worksheet Sheet { get; set; }

        public SheetData Data { get; set; }

        public string Name { get; set; }

        public static List<ExcelSheet> GetSheets(SpreadsheetDocument document)
        {
            var result =
                from sheet in document.WorkbookPart.Workbook.Sheets.OfType<Sheet>()
                let name = sheet.Name
                let worksheet = ((WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet
                from data in worksheet.Elements<SheetData>()
                select new ExcelSheet { Sheet = worksheet, Data = data, Name = name };

            return result.ToList();
        }
    }
}