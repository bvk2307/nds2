﻿namespace Luxoft.NDS2.Server.Services.Managers.Query
{
    public interface IQueryProvider
    {
        string GetQueryText(string scope, string queryName);
    }
}
