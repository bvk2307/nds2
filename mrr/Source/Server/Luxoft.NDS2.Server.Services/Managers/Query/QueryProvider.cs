﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using CommonComponents.Catalog;
using CommonComponents.Shared;

namespace Luxoft.NDS2.Server.Services.Managers.Query
{
    public class QueryProvider : IQueryProvider
    {
        private ICatalogFactoryService _catalogService;
        private const string _catalogBasePath = "logicalCatalog://RoleCatalog/NDS2Subsystem/FileSystems/QueryTemplates/";
        private const string _templateExtention = ".xml";

        public QueryProvider(IReadOnlyServiceCollection runtime)
        {
            _catalogService = runtime.Get<ICatalogFactoryService>();
        }

        #region Implementation of IQueryProvider

        public string GetQueryText(string scope, string queryName)
        {
            return GetQueryTextInternal(scope, queryName);
        }

        #endregion
        //TODO: реазизовать кеширование шаблонов, загружаемых с диска
        private string GetQueryTextInternal(string scope, string queryName)
        {
            var path = string.Concat(_catalogBasePath, scope, _templateExtention);

            using(var contentStream = _catalogService.CreateInstance<Stream>(new Uri(path)))
            {
                XDocument d = XDocument.Load(contentStream);

                var foundQueryNode = d.FirstNode.XPathSelectElement(string.Format("/Template/Query[@name='{0}']", queryName));

                if(foundQueryNode == null)
                {
                    return string.Empty;
                }

                return foundQueryNode.Value;
            }
        }
    }
}
