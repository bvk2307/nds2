﻿using System.Collections.Generic;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System.Linq;
using Luxoft.NDS2.Server.Services.UserAccess;

namespace Luxoft.NDS2.Server.Services.Managers
{
    internal class SelectionUser : IUser
    {
        private readonly IAuthorizationProvider _provider;

        public SelectionUser(IAuthorizationProvider provider, ServiceHelpers helper)
        {
            _provider = provider;
            _accessContext = helper.GetAccessContext(_ruleMap.Values.Distinct());
        }

        public string Sid
        {
            get
            {
                return _provider.CurrentUserSID;
            }
        }

        public bool IsInRole(string role)
        {
            return _provider.IsUserInRole(role);
        }

        private readonly Dictionary<string, string> _ruleMap = new Dictionary<string, string>
        {
            {Constants.SystemPermissions.Operations.SelectionChange, MrrOperations.Selection.Edit},
            {Constants.SystemPermissions.Operations.SelectionTemplateChange, MrrOperations.Selection.EditTemplate},
            {Constants.SystemPermissions.Operations.SelectionRemove, MrrOperations.Selection.DeleteSelectionDraft},
            {Constants.SystemPermissions.Operations.SelectionTemplateRemove, MrrOperations.Selection.DeleteTemplate},
            {Constants.SystemPermissions.Operations.SelectionToApprove, MrrOperations.Selection.SelectionToApprove},
            {Constants.SystemPermissions.Operations.SelectionToCorrect, MrrOperations.Selection.SelectionToRework},
            {Constants.SystemPermissions.Operations.SelectionApproved, MrrOperations.Selection.SelectionApprove},
            {Constants.SystemPermissions.Operations.SelectionUpdate, MrrOperations.Selection.ViewSelection},
            {Constants.SystemPermissions.Operations.SelectionView, MrrOperations.Selection.ViewSelection},
            {Constants.SystemPermissions.Operations.SelectionTemplateView, MrrOperations.Selection.ViewTemplate},
            {Constants.SystemPermissions.Operations.SelectionTakeInWork, MrrOperations.Selection.Edit}

        };

        private readonly AccessContext _accessContext;

        public bool HasOperation(string operation)
        {
            if (_accessContext.Operations.Any(x => x.Name == operation))
                return true;
            
            string rmOperation;
            if (_ruleMap.TryGetValue(operation, out rmOperation))
            {
                return _accessContext.Operations.Any(x => x.Name == rmOperation);
            }
            return _provider
                .GetUserPermissions()
                .Any(x => x.PermType == PermissionType.Operation && x.Name == operation);
        }
    }
}
