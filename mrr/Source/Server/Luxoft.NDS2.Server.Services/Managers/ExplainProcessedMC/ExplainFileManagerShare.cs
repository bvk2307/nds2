﻿using System;
using System.IO;

namespace Luxoft.NDS2.Server.Services.Managers.ExplainProcessedMC
{
    public class ExplainFileManagerShare : IExplainFileManager
    {
        private string _mcDirectoryIn = String.Empty;
        private string _fileNameZip = String.Empty;
        private string _filePathZipTemp = String.Empty;

        public ExplainFileManagerShare(string mcDirectoryIn, string fileNameZip, string filePathZipTemp)
        {
            _mcDirectoryIn = mcDirectoryIn;
            _fileNameZip = fileNameZip;
            _filePathZipTemp = filePathZipTemp;
        }

        public void CopyFile()
        {
            if (!_mcDirectoryIn.EndsWith("\\"))
                _mcDirectoryIn += "\\";
            string filePathMC = string.Concat(_mcDirectoryIn, _fileNameZip);
            File.Copy(_filePathZipTemp, filePathMC, true);
        }
    }
}
