﻿using System;
using System.Net;

namespace Luxoft.NDS2.Server.Services.Managers.ExplainProcessedMC
{
    public class ExplainFileManagerFtp : IExplainFileManager
    {
        private string _mcDirectoryIn = String.Empty;
        private string _fileNameZip = String.Empty;
        private string _filePathZipTemp = String.Empty;
        private string _ftpusername = String.Empty;
        private string _ftppassword = String.Empty;

        public ExplainFileManagerFtp(string mcDirectoryIn, string fileNameZip,
            string filePathZipTemp, string ftpusername, string ftppassword)
        {
            _mcDirectoryIn = mcDirectoryIn;
            _fileNameZip = fileNameZip;
            _filePathZipTemp = filePathZipTemp;
            _ftpusername = ftpusername;
            _ftppassword = ftppassword;
        }

        public void CopyFile()
        {
            string fileUrl = _mcDirectoryIn + _fileNameZip;
            CopyToFtp(_filePathZipTemp, fileUrl, _ftpusername, _ftppassword);
        }

        private void CopyToFtp(string fileurl, string fileUrl, string ftpusername, string ftppassword)
        {
            FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(fileUrl);
            ftpClient.Credentials = new System.Net.NetworkCredential(ftpusername, ftppassword);
            ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            ftpClient.UseBinary = true;
            ftpClient.KeepAlive = true;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileurl);
            ftpClient.ContentLength = fi.Length;
            byte[] buffer = new byte[4097];
            int bytes = 0;
            int total_bytes = (int)fi.Length;
            System.IO.FileStream fs = fi.OpenRead();
            System.IO.Stream rs = ftpClient.GetRequestStream();
            while (total_bytes > 0)
            {
                bytes = fs.Read(buffer, 0, buffer.Length);
                rs.Write(buffer, 0, bytes);
                total_bytes = total_bytes - bytes;
            }
            fs.Close();
            rs.Close();
            FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
            string value = uploadResponse.StatusDescription;
            uploadResponse.Close();
        }
    }
}
