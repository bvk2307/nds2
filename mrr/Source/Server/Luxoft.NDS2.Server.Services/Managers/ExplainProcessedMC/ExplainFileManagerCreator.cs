﻿using System;

namespace Luxoft.NDS2.Server.Services.Managers.ExplainProcessedMC
{
    public static class ExplainFileManagerCreator
    {
        public static IExplainFileManager CreateExplainFileManagerShare(string mcDirectoryIn, string fileNameZip, string filePathZipTemp)
        {
            return new ExplainFileManagerShare(mcDirectoryIn, fileNameZip, filePathZipTemp);
        }

        public static IExplainFileManager CreateExplainFileManagerFtp(string mcDirectoryIn, string fileNameZip,
            string filePathZipTemp, string ftpusername, string ftppassword)
        {
            return new ExplainFileManagerFtp(mcDirectoryIn, fileNameZip, filePathZipTemp, ftpusername, ftppassword);
        }

        public static IExplainFileManager CreateExplainFileManager(string typeShare, string mcDirectoryIn, string fileNameZip,
            string filePathZipTemp, string ftpusername, string ftppassword)
        {
            if (typeShare != null && typeShare.ToLower() == "file")
            {
                return CreateExplainFileManagerShare(mcDirectoryIn, fileNameZip, filePathZipTemp);
            }
            else if (typeShare != null && typeShare.ToLower() == "ftp")
            {
                return CreateExplainFileManagerFtp(mcDirectoryIn, fileNameZip, filePathZipTemp, ftpusername, ftppassword);
            }
            else throw new Exception("Указан неизвестный тип копирования zip-контейнера");
        }
    }
}
