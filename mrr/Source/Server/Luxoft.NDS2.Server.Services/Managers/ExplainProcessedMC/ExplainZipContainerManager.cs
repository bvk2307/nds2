﻿using CommonComponents.Catalog;
using CommonComponents.Configuration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.ExplainReply;
using Luxoft.NDS2.Server.Services.Entities.Parameters;
using Luxoft.NDS2.Server.Services.EntitiesConverters;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.Services.Managers.ExplainProcessedMC
{
    public class ExplainZipContainerManager
    {
        #region Переменные

        private ServiceHelpers _helper;

        private string appVersion = String.Empty;
        private string mcDirectoryIn = String.Empty;
        private string mcDirectoryIntoMC = String.Empty;
        private string xsdExplainPath = String.Empty;
        private string xsdExplainInventoryPath = String.Empty;
        private string xsdPackageDescriptionPath = String.Empty;
        private string pathDirectoryXSDFiles = String.Empty;
        private string ftpusername = String.Empty;
        private string ftppassword = String.Empty;
        private string typeShare = String.Empty;

        private ExplainParameters parameters;

        private string nameFile = String.Empty;
        private string filePathMC = String.Empty;
        private string fileExtension = ".xml";
        private string filePathExplain = String.Empty;
        private string filePathInventory = String.Empty;
        private string filePathPackageDescription = String.Empty;
        private string filePathDescription = String.Empty;
        private string basePath = String.Empty;

        private string filePathInventoryBin = String.Empty;
        private string filePathExlainBin = String.Empty;
        private string filePathDescriptionBin = String.Empty;
        private string fileZipExtension = ".zip";
        private string filePathZipTemp = String.Empty;
        private string fileNameZip = String.Empty;

        #endregion

        public ExplainZipContainerManager(ServiceHelpers helper)
        {
            _helper = helper;
        }

        public void SendToMC(long explainId, long declId)
        {
            ReadConfiguration();
            ReadData(explainId, declId);
            CreateBaseTempDirectory();
            CreateExplain();
            CreateDescription();
            CreateInventory();
            CreatePackageDescription();
            CreateZipContainer();
            CopyFile(explainId);
            UpdateExplainFileName(explainId);
            AddAskZipFile(explainId);
        }

        #region Методы

        private void ReadConfiguration()
        {
            var _configuration = _helper.Services.Get<IConfigurationDataService>();

            AppSettingsSection apps;
            if (_configuration.TryGetSection<AppSettingsSection>(ProfileInfo.Default, out apps))
            {
                appVersion = apps.Settings["Version"].Value;
                mcDirectoryIn = apps.Settings["MC.Directory"].Value;
                mcDirectoryIntoMC = apps.Settings["MC.DirectoryIntoMC"].Value;
                typeShare = apps.Settings["MC.typeShare"].Value;
                ftpusername = apps.Settings["MC.ftpusername"].Value;
                ftppassword = apps.Settings["MC.ftppassword"].Value;
                pathDirectoryXSDFiles = apps.Settings["NDS2.XSD.Directory"].Value;
                xsdExplainPath = String.Concat(apps.Settings["NDS2.XSD.Directory"].Value,
                                               apps.Settings["NDS2.XSD.Explain"].Value);
                xsdExplainInventoryPath = String.Concat(apps.Settings["NDS2.XSD.Directory"].Value,
                                                        apps.Settings["NDS2.XSD.ExplainInventory"].Value);
                xsdPackageDescriptionPath = String.Concat(apps.Settings["NDS2.XSD.Directory"].Value,
                                                        apps.Settings["NDS2.XSD.ExplainPackageDescription"].Value);
            }
            else
            {
                throw new ConfigurationErrorsException("NDS2Server - configuration appsettings not found");
            }

            if (string.IsNullOrWhiteSpace(mcDirectoryIntoMC))
                mcDirectoryIntoMC = mcDirectoryIn;
        }

        private void ReadData(long explainId, long declId)
        {
            parameters = new ExplainParameters();
            parameters.ExplainReplyInfo = TableAdapterCreator.ExplainReply(_helper).GetExplainReplyInfo(explainId);
            parameters.Invoices = TableAdapterCreator.ExplainReply(_helper).GetInvoicesOnlyCorrectInThisExplain(parameters.ExplainReplyInfo);
            FillExplainInvoicesAttributes(parameters.Invoices);
            parameters.InvoicesCorrectes = TableAdapterCreator.ExplainReply(_helper).GetExplainAllInvoiceCorrect(explainId);
            parameters.Decl = TableAdapterCreator.Declaration(_helper).GetDeclaration(declId);
            parameters.DocInfo = TableAdapterCreator.DiscrepancyDocument(_helper).GetDocument(parameters.ExplainReplyInfo.DOC_ID);
            parameters.AskFile = null;
            parameters.AskDecl = TableAdapterCreator.AIS(_helper).GetDeclaration(declId);
            if (parameters.AskDecl != null)
            {
                parameters.AskFile = EntitiesConverter.ASKDeklToФайл(parameters.AskDecl);
            }
            parameters.Version = appVersion;
            parameters.FullNameVersion = string.Format("АСК НДС-2 {0}", appVersion);
            parameters.CodeSoun = parameters.AskFile.Документ.КодНО;
            parameters.IdFrom = parameters.AskFile.Документ.КодНО;
            parameters.IdTo = parameters.AskFile.Документ.КодНО;
        }

        private void FillExplainInvoicesAttributes(List<ExplainInvoice> invoices)
        {
            var buyers = ExplainInvoiceBuyerAdapterCreator
                .Create(_helper)
                .GetContractors(invoices);

            var sellers = ExplainInvoiceSellerAdapterCreator
                .Create(_helper)
                .GetContractors(invoices);

            var operations = ExplainInvoiceOperationAdapterCreator
                .Create(_helper)
                .GetOperation(invoices);

            var buyaccepts = ExplainInvoiceBuyAcceptAdapterCreator
                .Create(_helper)
                .GetBuyAccepts(invoices);

            var paymentdocuments = ExplainInvoicePaymentDocumentAdapterCreator
                .Create(_helper)
                .GetPaymentDocuments(invoices);

            invoices
                .ForEach(invoice =>
                {
                    invoice.Attributes.Buyers.AddRange(
                        buyers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                    invoice.Attributes.Sellers.AddRange(
                        sellers.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                    invoice.Attributes.Operations.AddRange(
                        operations.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                    invoice.Attributes.BuyAccepts.AddRange(
                        buyaccepts.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                    invoice.Attributes.PaymentDocuments.AddRange(
                        paymentdocuments.Where(x => x.InvoiceKey == invoice.ROW_KEY));
                    invoice.SetMultipleAttributes();
                }
                );
        }

        private void CreateBaseTempDirectory()
        {
            string baseMainPath = Path.GetTempPath();
            basePath = string.Format("{0}{1}\\", baseMainPath, Guid.NewGuid());
            Directory.CreateDirectory(basePath);
        }

        private void CreateExplain()
        {
            var explainXmlManager = new ExplainXMLManager();
            var entityOfExlain = explainXmlManager.CreateExplainXMLEntity(parameters, out nameFile);
            parameters.NameFileOfExplain = nameFile;
            filePathExplain = string.Concat(basePath, nameFile, fileExtension);
            WriteXML(filePathExplain, entityOfExlain, xsdExplainPath);
            filePathExlainBin = string.Concat(basePath, ExplainHelpers.GenerateUnikFileName(), ".bin");
            ZipHelper.CreateZip(new ZipParameter() { filePath = filePathExplain, fileNameIntoZip = "file.xml" }, filePathExlainBin);
            parameters.NameFileIntoZipOfExplain = Path.GetFileName(filePathExlainBin);
        }

        private void CreateDescription()
        {
            var descriptionyManager = new ExplainDescriptionManager();
            var entityDescription = descriptionyManager.CreateEntity(parameters, out nameFile);
            parameters.NameFileOfDescription = nameFile;
            filePathDescription = string.Concat(basePath, nameFile, fileExtension);
            WriteXML(filePathDescription, entityDescription);
            filePathDescriptionBin = string.Concat(basePath, ExplainHelpers.GenerateUnikFileName(), ".bin");
            ZipHelper.CreateZip(new ZipParameter() { filePath = filePathDescription, fileNameIntoZip = "file.xml" }, filePathDescriptionBin);
            parameters.NameFileIntoZipOfDescription = Path.GetFileName(filePathDescriptionBin);
        }

        private void CreateInventory()
        {
            var inventoryManager = new ExplainInventoryManager();
            var entityOfInventory = inventoryManager.CreateXMLEntity(parameters, out nameFile);
            parameters.NameFileOfInventory = nameFile;
            filePathInventory = string.Concat(basePath, nameFile, fileExtension);
            WriteXML(filePathInventory, entityOfInventory, xsdExplainInventoryPath);
            filePathInventoryBin = string.Concat(basePath, ExplainHelpers.GenerateUnikFileName(), ".bin");
            ZipHelper.CreateZip(new ZipParameter() { filePath =  filePathInventory, fileNameIntoZip = "file.xml" }, filePathInventoryBin);
            parameters.NameFileIntoZipOfInventory = Path.GetFileName(filePathInventoryBin);
        }

        private void CreatePackageDescription()
        {
            var packageDescriptionManager = new ExplainPackageDescriptionManager();
            var entityOfPackageDescription = packageDescriptionManager.CreateXML(parameters, out nameFile);
            parameters.NameFileOfPackageDescription = nameFile;
            filePathPackageDescription = string.Concat(basePath, nameFile, fileExtension);
            WriteXML(filePathPackageDescription, entityOfPackageDescription, xsdPackageDescriptionPath);
            parameters.NameFileIntoZipOfPackageDescription = "packageDescription.xml";
        }

        private void CreateZipContainer()
        {
            string fileNameZipTemp = "fileExplainZipTemp";
            filePathZipTemp = string.Concat(basePath, fileNameZipTemp, fileZipExtension);
            var zipParameters = new List<ZipParameter>();
            zipParameters.Add(new ZipParameter() { filePath = filePathExlainBin });
            zipParameters.Add(new ZipParameter() { filePath = filePathDescriptionBin });
            zipParameters.Add(new ZipParameter() { filePath = filePathInventoryBin });
            zipParameters.Add(new ZipParameter() { filePath = filePathPackageDescription, fileNameIntoZip = parameters.NameFileIntoZipOfPackageDescription });

            ZipHelper.CreateZip(zipParameters, filePathZipTemp);
        }

        private void CopyFile(long explainId)
        {
            fileNameZip = string.Format("{0}{1}", parameters.NameFileOfExplain, fileZipExtension);

            var fileManager = ExplainFileManagerCreator.CreateExplainFileManager(typeShare, mcDirectoryIn, fileNameZip, filePathZipTemp, ftpusername, ftppassword);
            fileManager.CopyFile();
        }

        private void UpdateExplainFileName(long explainId)
        {
            string fileNameOutput = parameters.NameFileOfExplain + fileExtension;
            TableAdapterCreator.ExplainReply(_helper).UpdateExplainFileName(explainId, fileNameOutput);
        }

        private void AddAskZipFile(long explainId)
        {
            long? zip = ExplainASKAdapterCreator.ExplainASKAdapter(_helper).AddAskZipFile(fileNameZip, mcDirectoryIntoMC, parameters.CodeSoun);
            if (zip != null)
                TableAdapterCreator.ExplainReply(_helper).UpdateExplainZip(explainId, zip.Value);
        }

        private void WriteXML<T>(string fileSource, T entity)
        {
            WriteXML(fileSource, entity, null, false);
        }

        private void WriteXML<T>(string fileSource, T entity, string fileForValidating)
        {
            WriteXML(fileSource, entity, fileForValidating, true);
        }

        private void WriteXML<T>(string fileSource, T entity, string fileForValidating, bool isValidate)
        {
            using (FileStream saveFile = new FileStream(fileSource, FileMode.Create, FileAccess.Write))
            {
                var xns = new XmlSerializerNamespaces();
                XmlSerializer xs = new XmlSerializer(entity.GetType());
                xns.Add(String.Empty, String.Empty);
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(saveFile, Encoding.GetEncoding("windows-1251")))
                    xs.Serialize(xmlTextWriter, entity, xns);
            }
            if (isValidate)
                ValidateXML(fileSource, fileForValidating);
        }

        private void ValidateXML(string fileSource, string fileForValidating)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(fileSource);

            var csv = _helper.Services.Get<ICatalogFactoryService>();
            var contentStream = csv.CreateInstance<Stream>(new Uri(fileForValidating));
            XmlReader reader = XmlReader.Create(contentStream);

            xml.Schemas.Add(null, reader);

            xml.Validate(null);
        }

        #endregion
    }
}
