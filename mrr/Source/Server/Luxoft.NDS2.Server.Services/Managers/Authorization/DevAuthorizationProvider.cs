﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Xml.Linq;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.Managers.Authorization
{
    public class AzOperation : PermissionObject
    {
        public Guid Id { get; set; }

        public override PermissionType PermissionType
        {
            get { return PermissionType.Operation; }
        }
    }

    public class AzRole : PermissionObject
    {
        public AzRole()
        {
            Tasks = new List<AzTask>();
        }
        public Guid Id { get; set; }

        public List<AzTask> Tasks { get; set; }

        public override PermissionType PermissionType
        {
            get { return PermissionType.Operation; }
        }
    }

    public class AzTask : PermissionObject
    {
        public AzTask()
        {
            Operations = new List<AzOperation>();
        }

        public Guid Id { get; set; }
        public List<AzOperation> Operations { get; set; }

        public override PermissionType PermissionType
        {
            get { return PermissionType.Task; }
        }
    }

    public class AzMember
    {
        public string Sid { get; set; }
    }

    public class DevAuthorizationProvider : IAuthorizationProvider
    {
        public List<AzOperation> Operations { get; private set; }
        public List<AzTask> Tasks { get; private set; }
        public List<AzRole> Roles { get; private set; }
        public List<AzMember> Members { get; private set; }
        public Dictionary<string, List<AzRole>> UserMappings { get; set; }

        private string AzManFilePath
        {
            get { return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "INIAS.AzManStore.xml"); }
        }

        private string CurrentUserSid
        {
            get
            {
                var userIdentifier = Thread.CurrentPrincipal.Identity.Name;

                WindowsIdentity identity = null;

                if (Thread.CurrentPrincipal.Identity is WindowsIdentity)
                {
                    identity = (WindowsIdentity)Thread.CurrentPrincipal.Identity;
                }

                if (identity == null)
                {
                    try
                    {
                        identity = ConvertToWindowsIdentity(userIdentifier);
                    }
                    catch (Exception) { }
                }

#if DEBUG //Для тех кто работает не в домене
                if (identity == null)
                {
                    identity = WindowsIdentity.GetCurrent();
                }
#endif

                return identity == null ? string.Empty : identity.User.Value;
            }
        }

        private WindowsIdentity ConvertToWindowsIdentity(string userIdentifier)
        {
            ActiveDs.IADsNameTranslate translator = this.CreateTranslator();

            try
            {
                translator.Set((int)ActiveDs.ADS_NAME_TYPE_ENUM.ADS_NAME_TYPE_SID_OR_SID_HISTORY_NAME, userIdentifier);

                var userPrincipalName = translator.Get((int)ActiveDs.ADS_NAME_TYPE_ENUM.ADS_NAME_TYPE_USER_PRINCIPAL_NAME);

                return new WindowsIdentity(userPrincipalName);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Некорректный идентификатор пользователя '{0}'", userIdentifier), "userIdentifier", ex);
            }
            finally
            {
                Marshal.ReleaseComObject(translator);
            }
        }

        #region private ActiveDs.IADsNameTranslate CreateTranslator()
        [SecurityCritical]
        private ActiveDs.IADsNameTranslate CreateTranslator()
        {
            string logonServer = null;

            //            if (this._useLogonServer)
            //                logonServer = Environment.GetEnvironmentVariable("LOGONSERVER").Trim('\\');

            ActiveDs.NameTranslateClass translator = new ActiveDs.NameTranslateClass();

            try
            {
                if (!string.IsNullOrEmpty(logonServer))
                    translator.Init((int)ActiveDs.ADS_NAME_INITTYPE_ENUM.ADS_NAME_INITTYPE_SERVER, logonServer);
                else
                    translator.Init((int)ActiveDs.ADS_NAME_INITTYPE_ENUM.ADS_NAME_INITTYPE_GC, null);
            }
            catch
            {
                Marshal.ReleaseComObject(translator);
                throw;
            }

            return translator;
        }
        #endregion

        private ServiceHelpers _helper;

        public DevAuthorizationProvider(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            Operations = new List<AzOperation>();
            Roles = new List<AzRole>();
            Tasks = new List<AzTask>();
            Members = new List<AzMember>();
            UserMappings = new Dictionary<string, List<AzRole>>();

            ParseAzManFile();
        }

        public string CurrentUserName { get { return Thread.CurrentPrincipal.Identity.Name; } }

        public List<AccessRight> GetUserPermissions()
        {
            return GetUserPermissions(CurrentUserSid);
        }

        public List<AccessRight> GetUserPermissions(string sid)
        {


            if (string.IsNullOrEmpty(sid))
            {
                throw new AuthorizationException("Ошибка определения текущего пользователя");
            }

            var profileData = new List<KeyValuePair<string, object>>();

            profileData.Add(new KeyValuePair<string, object>("sid", CurrentUserSID));
            profileData.Add(new KeyValuePair<string, object>("uname", CurrentUserName));

            var result = new List<AccessRight>();

            _helper.LogDebug("ЦСУД", "GetUserPermissions", profileData);

            if (!UserMappings.ContainsKey(sid))
            {
                return new List<AccessRight>();
            }

            foreach (var role in UserMappings[sid])
            {
                result.Add(new AccessRight() { Name = role.Name, PermType = PermissionType.Operation });
                foreach (var task in role.Tasks)
                {
                    result.Add(new AccessRight()
                    {
                        Name = task.Name,
                        PermType = PermissionType.Task
                    });

                    foreach (var operation in task.Operations)
                    {
                        result.Add(new AccessRight()
                        {
                            Name = operation.Name,
                            PermType = PermissionType.Operation
                        });
                    }
                }
            }
            return result;
        }

        public void ChangeUserRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName)) throw new ArgumentException("roleName");

            var role = Roles.FirstOrDefault(r => r.Name == roleName);
            if (role == null) throw new InvalidOperationException(string.Format("Роль {0} не найдена", roleName));

            var sid = CurrentUserSid;
            if (string.IsNullOrEmpty(sid)) throw new InvalidOperationException("Пользователь не найден");

            UserMappings[sid].Clear();
            UserMappings[sid].Add(role);
        }

        public List<string> GetSubsystemRoles()
        {
            return Roles.Select(r => r.Name).Distinct().ToList();
        }

        public bool IsOperationEligible(string operationName)
        {
            var sid = CurrentUserSid;
            if (string.IsNullOrEmpty(sid)) return false;
            if (!UserMappings.ContainsKey(sid)) return false;

            return
                GetUserPermissions()
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == operationName);

        }

        public bool IsUserInRole(string roleName)
        {
            var sid = CurrentUserSid;
            if (string.IsNullOrEmpty(sid)) return false;
            if (!UserMappings.ContainsKey(sid)) return false;

            return
                GetUserPermissions()
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == roleName);
        }

        public bool IsUserInRole(string userSID, string roleName)
        {
            if (string.IsNullOrEmpty(userSID)) return false;
            if (!UserMappings.ContainsKey(userSID)) return false;

            return
                GetUserPermissions(userSID)
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == roleName);
        }

        public string CurrentUserSID { get { return CurrentUserSid; } }

        public List<string> GetUserSIDs()
        {
            return Members.Select(m => m.Sid).ToList();
        }

        public List<UserStructContextRight> GetStructContextUserRigths(string ifns)
        {
            var result = new List<UserStructContextRight>();
            foreach (var map in UserMappings)
            {
                foreach (var role in map.Value)
                {
                    result.Add(new UserStructContextRight() { Role = role.Name, StructContext = ifns, UserSid = map.Key });
                }
            }

            return result;
        }

        public IList<string> GetUserStructContexts(PermissionType permType, string objectName)
        {
            return new List<string>()
                {                    
                    Constants.SystemPermissions.CentralDepartmentCode,
                    "25"
                };
        }

        private void ParseAzManFile()
        {
            if (!File.Exists(AzManFilePath)) throw new FileNotFoundException(AzManFilePath);

            XDocument doc = XDocument.Load(AzManFilePath);
            var nds2ApplicationNode =
                doc.Descendants("AzApplication")
                    .Where(n => n.Attribute("Name").Value.Equals(Constants.SubsystemName))
                    .First();

            ParseOperations(nds2ApplicationNode);
            ParseTasks(nds2ApplicationNode);
            ParseRoles(nds2ApplicationNode);
        }

        private void ParseOperations(XElement appElement)
        {
            foreach (var operationNode in appElement.Descendants("AzOperation"))
            {
                Operations.Add(new AzOperation()
                {
                    Id = Guid.Parse(operationNode.Attribute("Guid").Value),
                    StructContext = "0000",
                    Name = operationNode.Attribute("Name").Value
                });
            }
        }

        private void ParseTasks(XElement appElement)
        {
            foreach (var taskElement in appElement.Descendants("AzTask").Where(t => t.Attribute("RoleDefinition") == null))
            {
                AzTask task = new AzTask()
                {
                    Id = Guid.Parse(taskElement.Attribute("Guid").Value),
                    Name = taskElement.Attribute("Name").Value,
                };

                foreach (var operationLink in taskElement.Descendants("OperationLink"))
                {
                    task.Operations.Add(Operations.First(o => o.Id.Equals(Guid.Parse(operationLink.Value))));
                }

                Tasks.Add(task);
            }
        }

        private void ParseRoles(XElement appElement)
        {
            var scope = appElement.Descendants("AzScope").Where(s => s.Attribute("Name").Value.Equals("0000")).First();
            foreach (var roleElm in scope.Descendants("AzRole"))
            {
                AzRole role = new AzRole()
                {
                    Id = Guid.Parse(roleElm.Attribute("Guid").Value),
                    Name = roleElm.Attribute("Name").Value
                };
                Roles.Add(role);
                foreach (var tLinks in roleElm.Descendants("TaskLink"))
                {
                    var roleDefinition = appElement.Descendants("AzTask")
                        .Where(t => t.Attribute("Guid").Value.Equals(tLinks.Value))
                        .First();

                    foreach (var roleTasks in roleDefinition.Descendants("TaskLink"))
                    {
                        role.Tasks.Add(Tasks.First(tsk => tsk.Id.Equals(Guid.Parse(roleTasks.Value))));
                    }

                }

                foreach (var member in roleElm.Descendants("Member"))
                {
                    var userSid = member.Value;
                    if (!UserMappings.ContainsKey(userSid))
                    {
                        UserMappings.Add(userSid, new List<AzRole>());
                    }

                    UserMappings[userSid].Add(role);
                }
            }
        }
    }
}