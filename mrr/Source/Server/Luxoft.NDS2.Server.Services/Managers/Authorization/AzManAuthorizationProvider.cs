﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Xml.Linq;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.Managers.Authorization
{
    public class AzManAuthorizationProvider : IAuthorizationProvider
    {
        private IAuthorizationService _commonAuthService;
        private string _cacheKeyPrefix = "nds.auth.";
        private MemoryCache _cache = MemoryCache.Default;
        private ServiceHelpers _helper = null;

        public AzManAuthorizationProvider(IReadOnlyServiceCollection services)
        {
            _commonAuthService = services.Get<IAuthorizationService>();
            _helper = new ServiceHelpers(services);
        }

        public string CurrentUserName { get; private set; }

        public List<AccessRight> GetUserPermissions()
        {
            return GetAccessRightsInternal();
        }

        private List<AccessRight> GetAccessRightsInternal()
        {
            var op = new Constants.SystemPermissions.Operations();
            var result = new List<AccessRight>();
            var operationAuthCtx = typeof(Constants.SystemPermissions.Operations)
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                .Select(f => new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, f.GetValue(op).ToString()))
                .ToList();

            var availableContexts = _commonAuthService.GetStructContexts(CurrentUserSID,
                new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, Constants.SystemPermissions.Operations.SystemLogon)
                    .GetFullName());

            foreach (var ctx in availableContexts)
            {
                operationAuthCtx.ForEach(ct => ct.StructContext = ctx);

                var authResult = _commonAuthService.Authorize(_helper.User, operationAuthCtx.Select(c => c.GetFullName()).ToList());

                for (int i = 0; i < operationAuthCtx.Count(); i++)
                {
                    if (authResult[i] && !result.Any(ar => ar.Name.Equals(operationAuthCtx[i].Name)))
                    {
                        result.Add(new AccessRight() { StructContext = operationAuthCtx[i].GetFullName(), Name = operationAuthCtx[i].Name, PermType = PermissionType.Operation });
                    }
                }
            }

            return result;
        }

        public void ChangeUserRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public List<string> GetSubsystemRoles()
        {
            return GetUserPermissions().Where(n => n.Name.StartsWith("Роль.")).Select(r => r.Name).ToList();
        }

        public bool IsOperationEligible(string operationName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(operationName));
        }

        public bool IsUserInRole(string roleName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(roleName));
        }

        public bool IsUserInRole(string userSID, string roleName)
        {
            return GetUserPermissions().Any(ar => ar.Name.Equals(roleName));
        }

        public string CurrentUserSID
        {
            get
            {
                if (Thread.CurrentPrincipal.Identity is WindowsIdentity)
                {
                    return ((WindowsIdentity)Thread.CurrentPrincipal.Identity).User.Value;
                }

                return string.Empty;
            }
        }

        public List<string> GetUserSIDs()
        {
            throw new NotImplementedException();
        }

        public List<UserStructContextRight> GetStructContextUserRigths(string ifns)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetUserStructContexts(PermissionType permType, string objectName)
        {
            throw new NotImplementedException();
        }
    }
}