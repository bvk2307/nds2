﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Security.Principal;
using System.Text;
using System.Threading;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.Managers.Authorization
{
    public class AuthorizationProvider : IAuthorizationProvider
    {
        private IAuthorizationService _commonAuthService;
        private string _cacheKeyPrefix = "nds.auth.";
        private MemoryCache _cache = MemoryCache.Default;
        private ServiceHelpers _helper = null;

        private PerformanceCounter _ldapTotalCalls;
        private PerformanceCounter _ldapCacheCalls;
        private PerformanceCounter _ldapRequestTime;

        public AuthorizationProvider(IReadOnlyServiceCollection services)
        {
            _commonAuthService = services.Get<IAuthorizationService>();
            _helper = new ServiceHelpers(services);
            InitCounters();
        }

        public string CurrentUserName { get { return Thread.CurrentPrincipal.Identity.Name; }}

        private void InitCounters()
        {
            if(PerformanceCounterCategory.Exists(PerformanceCounters.Server.CategoryName))
            {
                _ldapCacheCalls = new PerformanceCounter(PerformanceCounters.Server.CategoryName, PerformanceCounters.Server.Security.RequestsCacheTotalName);
                _ldapRequestTime = new PerformanceCounter(PerformanceCounters.Server.CategoryName, PerformanceCounters.Server.Security.LdapRequestTimeName);
                _ldapTotalCalls = new PerformanceCounter(PerformanceCounters.Server.CategoryName, PerformanceCounters.Server.Security.RequestsTotalName);
            }
        }

        public List<AccessRight> GetUserPermissions()
        {
            List<AccessRight> result = new List<AccessRight>();
            var cacheKey = string.Concat(_cacheKeyPrefix, CurrentUserSID);
            var profileData = new List<KeyValuePair<string, object>>();

            profileData.Add(new KeyValuePair<string, object>("sid", CurrentUserSID));
            profileData.Add(new KeyValuePair<string, object>("uname", CurrentUserName));

            var sw = new Stopwatch();

            if(GetFromCache(cacheKey, out result))
            {
                if(result == null)
                {
                    sw.Start();

                    result = GetAccessRightsInternal();

                    _helper.LogDebug("ЦСУД", "GetUserPermissions", profileData);
                    
                    sw.Stop();

                    LdapTotalCallsIncrement();
                    LdapRequestTimeSet(sw.ElapsedMilliseconds);

                    CacheItem(cacheKey, result);

                    return result;
                }

                LdapTotalCacheCallsIncrement();

                _helper.LogDebug("ЦСУД(кэш)", "GetUserPermissions", profileData);

                return result;
            }

            _helper.LogDebug("ЦСУД", "GetUserPermissions", profileData);

            sw.Start();
            result = GetAccessRightsInternal();

            sw.Stop();

            LdapTotalCallsIncrement();
            LdapRequestTimeSet(sw.ElapsedMilliseconds);

            CacheItem(cacheKey, result);

            return result;
        }

        private List<AccessRight> GetAccessRightsInternal()
        {
            var op = new Constants.SystemPermissions.Operations();
            var result = new List<AccessRight>();
            var operationAuthCtx = typeof(Constants.SystemPermissions.Operations)
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                .Select(f => new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, f.GetValue(op).ToString()))
                .ToList();

            var availableContexts = _commonAuthService.GetStructContexts(CurrentUserSID,
                new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, Constants.SystemPermissions.Operations.SystemLogon)
                    .GetFullName());

            foreach (var ctx in availableContexts)
            {
                operationAuthCtx.ForEach(ct => ct.StructContext = ctx);

                var authResult = _commonAuthService.Authorize(_helper.User, operationAuthCtx.Select(c => c.GetFullName()).ToList());

                for (int i = 0; i < operationAuthCtx.Count(); i++)
                {
                    if (authResult[i] && !result.Any(ar => ar.Name.Equals(operationAuthCtx[i].Name)))
                    {
                        result.Add(new AccessRight() { StructContext = operationAuthCtx[i].GetFullName(), Name = operationAuthCtx[i].Name, PermType = PermissionType.Operation });
                    }
                }
            }

            return result;
        }


        public void ChangeUserRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public List<string> GetSubsystemRoles()
        {
            throw new NotImplementedException();
        }

        public bool IsOperationEligible(string operationName)
        {
            if (string.IsNullOrEmpty(operationName)) return false;

            return
                GetUserPermissions()
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == operationName);
        }

        public bool IsUserInRole(string roleName)
        {
            return
                GetUserPermissions()
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == roleName);
        }

        public bool IsUserInRole(string userSID, string roleName)
        {
            return
                GetUserPermissions()
                    .Any(perm => perm.PermType == PermissionType.Operation && perm.Name == roleName);
        }

        public string CurrentUserSID
        {
            get
            {
                if (Thread.CurrentPrincipal.Identity is WindowsIdentity)
                {
                    return ((WindowsIdentity)Thread.CurrentPrincipal.Identity).User.Value;
                }

                return string.Empty;
            }
        }


        public List<string> GetUserSIDs()
        {
            List<string> result = new List<string>();
            return result;
        }

        public List<UserStructContextRight> GetStructContextUserRigths(string ifns)
        {
            var result = new List<UserStructContextRight>();

            List<string> roleOperatons = new List<string>()
            {
                Constants.SystemPermissions.Operations.RoleAnalyst,
                Constants.SystemPermissions.Operations.RoleApprover,
                Constants.SystemPermissions.Operations.RoleDeveloper,
                Constants.SystemPermissions.Operations.RoleInspector,
                Constants.SystemPermissions.Operations.RoleManager,
                Constants.SystemPermissions.Operations.RoleMedodologist,
                Constants.SystemPermissions.Operations.RoleSender

            };

            foreach (var roleOperation in roleOperatons)
            {
                var ctx = new AuthorizationContext(PermissionType.Operation, Constants.SubsystemName, ifns, roleOperation).GetFullName();
                foreach (var assignment in _commonAuthService.GetAssignments(ctx))
                {
                    result.Add(new UserStructContextRight()
                    {
                        UserSid = assignment,
                        Role = roleOperation,
                        StructContext = ifns
                    });
                }
            }

            return result;
        }

        public IList<string> GetUserStructContexts(PermissionType permType, string objectName)
        {
            var ctx = new AuthorizationContext(permType, Constants.SubsystemName, objectName).GetFullName();

            return _commonAuthService.GetStructContexts(ctx);

        }


        private void CacheItem(string key, object value)
        {
            _cache.Add(new CacheItem(key, value),
                       new CacheItemPolicy() {AbsoluteExpiration = DateTime.Now.AddMinutes(60)});
        }

        private bool GetFromCache<T>(string key, out T data) where T : class
        {
            if(_cache.Contains(key))
            {
                data = _cache.Get(key) as T;
                return true;
            }

            data = null;

            return false;
        }

        #region Helpers

        private void LdapTotalCallsIncrement()
        {
            if(_ldapTotalCalls != null)
            {
                _ldapTotalCalls.Increment();
            }
        }

        private void LdapTotalCacheCallsIncrement()
        {
            if (_ldapCacheCalls != null)
            {
                _ldapCacheCalls.Increment();
            }
        }

        private void LdapRequestTimeSet(Int64 millisec)
        {
            if (_ldapRequestTime != null)
            {
                _ldapRequestTime.RawValue = millisec;
            }
        }

        #endregion
    }
}
