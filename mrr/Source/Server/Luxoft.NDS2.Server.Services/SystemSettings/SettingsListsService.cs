﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.SystemSettings;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.SystemSettings
{
    [CommunicationContract(typeof(ISystemSettingsService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SettingsListsService : ISettingsListsService
    {
        private readonly ServiceHelpers _helper;
        public SettingsListsService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        /// <summary>
        /// Сохраняет список БС
        /// </summary>
        /// <param name="name">наименование списка</param>
        /// <param name="createdBy">кем создан</param>
        /// <param name="isActive">активен</param>
        /// <param name="innList">список инн</param>
        /// <returns></returns>
        public OperationResult SaveWhiteList(SystemSettingsList list)
        {
            return _helper.Do(
              () =>
              {
                  using (var connection = new SingleConnectionFactory(_helper))
                  {
                      SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection)
                      .Save(list);
                  }
              });
        }

        /// <summary>
        /// Синхранизирует БС в Hive и Oracle
        /// Использует сервис СВК
        /// </summary>
        /// <returns></returns>
        public OperationResult HiveSyncWhiteList()
        {
            return _helper.Do(
                () =>
                {
                    var url = _helper.GetConfigurationValue(Constants.SvkSyncWhiteListsUrl);
                    var method = _helper.GetConfigurationValue(Constants.SvkSyncWhiteListsMethod);
                    var timeOut = int.Parse(_helper.GetConfigurationValue(Constants.SvkSyncWhiteListsTimeout));

                    var request = WebRequest.Create(url);
                    request.Method = method;
                    request.Timeout = timeOut;
                    using (var webResponse = request.GetResponse())
                    {
                        webResponse.Close();
                    }
                });
        }

        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        public OperationResult Activate(long id)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection).Update(id, true);
                 }
             });
        }

        /// <summary>
        /// Деактивирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        public OperationResult Deactivate(long id)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection).Update(id, false);
                 }
             });
        }

        /// <summary>
        /// Возвращает список INN
        /// </summary>
        public OperationResult<string[]> LoadInnList(long id)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     return SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection).LoadInnList(id);
                 }
             });
        }

        /// <summary>
        /// Возвращает список ИВ
        /// </summary>
        public OperationResult<SystemSettingsList> LoadExcludeList()
        {
            return _helper.Do(
              () =>
              {
                  using (var connection = new SingleConnectionFactory(_helper))
                  {
                      return SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection)
                      .All(SettingsListType.ExcludeList).FirstOrDefault();
                  }
              });
        }

        /// <summary>
        /// Возвращает список НМ
        /// </summary>
        public OperationResult<SystemSettingsList> LoadTaxMonitorList()
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     return SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection)
                     .All(SettingsListType.TaxMonitorList).FirstOrDefault();
                 }
             });
        }

        /// <summary>
        /// Возвращает список БС
        /// </summary>
        public OperationResult<List<SystemSettingsList>> All(QueryConditions qc)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     return SystemSettingsAdapterCreator
                     .WhiteListsAdapter(_helper, connection).Search(qc.Filter.ToFilterGroup(), qc.Sorting, 0, int.MaxValue)
                     .ToList();
                 }
             });
        }


        /// <summary>
        /// Сохраняет список ИВ
        /// </summary>
        /// <param name="createdBy">кем создан/изменен</param>
        /// <param name="innList">список инн</param>
        public OperationResult SaveExcludeList(SystemSettingsList list)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                      SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection).Save(list);
                 }
             });
        }

        /// <summary>
        /// Сохраняет список НМ
        /// </summary>
        /// <param name="createdBy">кем создан/изменен</param>
        /// <param name="innList">список инн</param>
        public OperationResult SaveTaxMonitorList(SystemSettingsList list)
        {
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     SystemSettingsAdapterCreator.SystemSettingsListsAdapter(_helper, connection).Save(list);
                 }
             });
        }

        /// <summary>
        /// Возвращает имя текущего юзера
        /// </summary>
        /// <returns></returns>
        public OperationResult<Object> GetUserDisplayName()
        {
            return new OperationResult<object>(new object()) { Result = _helper.UserDisplayName };
        }
    }
}
