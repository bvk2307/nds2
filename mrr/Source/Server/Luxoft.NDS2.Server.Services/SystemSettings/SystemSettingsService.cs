﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.SystemSettings;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.SystemSettings
{
    [CommunicationContract(typeof(ISystemSettingsService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SystemSettingsService : ISystemSettingsService
    {
        private readonly ServiceHelpers _helper;
        public SystemSettingsService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }
        public OperationResult<NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings> Load()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return SystemSettingsAdapterCreator.SystemSettingsAdapter(_helper, connection).Load();
                    }
                });
        }

        public OperationResult Save(NDS2.Common.Contracts.DTO.SystemSettings.SystemSettings settingsData)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        SystemSettingsAdapterCreator.SystemSettingsAdapter(_helper, connection)
                            .Save(settingsData.ClaimResendingTimeout,
                                settingsData.ResendingAttempts, settingsData.ClaimDeliveryTimeout,
                                settingsData.ClaimExplainTimeout, settingsData.ReclaimReplyTimeout,
                                settingsData.ReplyEntryTimeout);
                    }
                });
        }
    }
}
