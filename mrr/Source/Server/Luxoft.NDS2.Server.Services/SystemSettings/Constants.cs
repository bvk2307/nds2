﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.SystemSettings
{
    public static class Constants
    {
        public static string SvkSyncWhiteListsUrl = "SVK.SyncWhiteListsUrl";
        public static string SvkSyncWhiteListsMethod = "SVK.SyncWhiteListsMethod";
        public static string SvkSyncWhiteListsTimeout = "SVK.SyncWhiteListsTimeout";
    }
}
