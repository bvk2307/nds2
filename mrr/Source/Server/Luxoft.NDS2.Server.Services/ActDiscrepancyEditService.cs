﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments;
using Luxoft.NDS2.Server.Common.Adapters.KnpResultDocuments;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.KnpResultDocuments;
using Luxoft.NDS2.Server.Services.KnpResultDocuments;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IEditActDiscrepancyService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ActDiscrepancyEditService : EditDocumentDiscrepancyService, IEditActDiscrepancyService
    {
        public ActDiscrepancyEditService(IReadOnlyServiceCollection service)
            : base(service)
        {
        }

        protected override IDocumentAdapterFactory AdapterFactory()
        {
            return new ActAdapterFactory(_helper);
        }

        protected override bool CloseDocumentEnabled(IUpdateDecisionStatusAdapter statusAdapter)
        {
            return true;
        }
    }
}
