﻿using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.Services.Entities
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Файл
    {
        private ФайлДокумент документField;

        private string идФайлField;

        private string версПрогField;

        private ФайлВерсФорм версФормField;

        private ФайлПризнНал812 признНал812Field;

        private ФайлПризнНал8 признНал8Field;

        private bool признНал8FieldSpecified;

        private ФайлПризнНал81 признНал81Field;

        private bool признНал81FieldSpecified;

        private ФайлПризнНал9 признНал9Field;

        private bool признНал9FieldSpecified;

        private ФайлПризнНал91 признНал91Field;

        private bool признНал91FieldSpecified;

        private ФайлПризнНал10 признНал10Field;

        private bool признНал10FieldSpecified;

        private ФайлПризнНал11 признНал11Field;

        private bool признНал11FieldSpecified;

        private ФайлПризнНал12 признНал12Field;

        private bool признНал12FieldSpecified;

        

        /// <remarks/>
        public ФайлДокумент Документ
        {
            get
            {
                return this.документField;
            }
            set
            {
                this.документField = value;
            }
        }
        

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИдФайл
        {
            get
            {
                return this.идФайлField;
            }
            set
            {
                this.идФайлField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ВерсПрог
        {
            get
            {
                return this.версПрогField;
            }
            set
            {
                this.версПрогField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлВерсФорм ВерсФорм
        {
            get
            {
                return this.версФормField;
            }
            set
            {
                this.версФормField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("ПризнНал8-12")]
        public ФайлПризнНал812 ПризнНал812
        {
            get
            {
                return this.признНал812Field;
            }
            set
            {
                this.признНал812Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал8 ПризнНал8
        {
            get
            {
                return this.признНал8Field;
            }
            set
            {
                this.признНал8Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал8Specified
        {
            get
            {
                return this.признНал8FieldSpecified;
            }
            set
            {
                this.признНал8FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал81 ПризнНал81
        {
            get
            {
                return this.признНал81Field;
            }
            set
            {
                this.признНал81Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал81Specified
        {
            get
            {
                return this.признНал81FieldSpecified;
            }
            set
            {
                this.признНал81FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал9 ПризнНал9
        {
            get
            {
                return this.признНал9Field;
            }
            set
            {
                this.признНал9Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал9Specified
        {
            get
            {
                return this.признНал9FieldSpecified;
            }
            set
            {
                this.признНал9FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал91 ПризнНал91
        {
            get
            {
                return this.признНал91Field;
            }
            set
            {
                this.признНал91Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал91Specified
        {
            get
            {
                return this.признНал91FieldSpecified;
            }
            set
            {
                this.признНал91FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал10 ПризнНал10
        {
            get
            {
                return this.признНал10Field;
            }
            set
            {
                this.признНал10Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал10Specified
        {
            get
            {
                return this.признНал10FieldSpecified;
            }
            set
            {
                this.признНал10FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал11 ПризнНал11
        {
            get
            {
                return this.признНал11Field;
            }
            set
            {
                this.признНал11Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал11Specified
        {
            get
            {
                return this.признНал11FieldSpecified;
            }
            set
            {
                this.признНал11FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал12 ПризнНал12
        {
            get
            {
                return this.признНал12Field;
            }
            set
            {
                this.признНал12Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал12Specified
        {
            get
            {
                return this.признНал12FieldSpecified;
            }
            set
            {
                this.признНал12FieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокумент
    {

        private ФайлДокументСвНП свНПField;

        private ФайлДокументПодписант подписантField;

        private ФайлДокументНДС нДСField;

        private ФайлДокументКНД кНДField;

        private string датаДокField;

        private ФайлДокументПериод периодField;

        private string отчетГодField;

        private string кодНОField;

        private string номКоррField;

        private ФайлДокументПоМесту поМестуField;

        private string регНомField;

        /// <remarks/>
        public ФайлДокументСвНП СвНП
        {
            get
            {
                return this.свНПField;
            }
            set
            {
                this.свНПField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументПодписант Подписант
        {
            get
            {
                return this.подписантField;
            }
            set
            {
                this.подписантField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДС НДС
        {
            get
            {
                return this.нДСField;
            }
            set
            {
                this.нДСField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументКНД КНД
        {
            get
            {
                return this.кНДField;
            }
            set
            {
                this.кНДField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаДок
        {
            get
            {
                return this.датаДокField;
            }
            set
            {
                this.датаДокField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументПериод Период
        {
            get
            {
                return this.периодField;
            }
            set
            {
                this.периодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "gYear")]
        public string ОтчетГод
        {
            get
            {
                return this.отчетГодField;
            }
            set
            {
                this.отчетГодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодНО
        {
            get
            {
                return this.кодНОField;
            }
            set
            {
                this.кодНОField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string РегНом
        {
            get { return регНомField; }
            set { регНомField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НомКорр
        {
            get
            {
                return this.номКоррField;
            }
            set
            {
                this.номКоррField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументПоМесту ПоМесту
        {
            get
            {
                return this.поМестуField;
            }
            set
            {
                this.поМестуField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументСвНП
    {

        private object itemField;

        private string оКВЭДField;

        private string тлфField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("НПФЛ", typeof(ФайлДокументСвНПНПФЛ))]
        [System.Xml.Serialization.XmlElementAttribute("НПЮЛ", typeof(ФайлДокументСвНПНПЮЛ))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ОКВЭД
        {
            get
            {
                return this.оКВЭДField;
            }
            set
            {
                this.оКВЭДField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Тлф
        {
            get
            {
                return this.тлфField;
            }
            set
            {
                this.тлфField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументСвНПНПФЛ
    {

        private ФИОТип фИОField;

        private string иННФЛField;

        /// <remarks/>
        public ФИОТип ФИО
        {
            get
            {
                return this.фИОField;
            }
            set
            {
                this.фИОField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИННФЛ
        {
            get
            {
                return this.иННФЛField;
            }
            set
            {
                this.иННФЛField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ФИОТип
    {

        private string фамилияField;

        private string имяField;

        private string отчествоField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Фамилия
        {
            get
            {
                return this.фамилияField;
            }
            set
            {
                this.фамилияField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Имя
        {
            get
            {
                return this.имяField;
            }
            set
            {
                this.имяField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Отчество
        {
            get
            {
                return this.отчествоField;
            }
            set
            {
                this.отчествоField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class АдрРФТип
    {

        private string индексField;

        private string кодРегионField;

        private string районField;

        private string городField;

        private string населПунктField;

        private string улицаField;

        private string домField;

        private string корпусField;

        private string квартField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Индекс
        {
            get
            {
                return this.индексField;
            }
            set
            {
                this.индексField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодРегион
        {
            get
            {
                return this.кодРегионField;
            }
            set
            {
                this.кодРегионField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Район
        {
            get
            {
                return this.районField;
            }
            set
            {
                this.районField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Город
        {
            get
            {
                return this.городField;
            }
            set
            {
                this.городField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаселПункт
        {
            get
            {
                return this.населПунктField;
            }
            set
            {
                this.населПунктField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Улица
        {
            get
            {
                return this.улицаField;
            }
            set
            {
                this.улицаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Дом
        {
            get
            {
                return this.домField;
            }
            set
            {
                this.домField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Корпус
        {
            get
            {
                return this.корпусField;
            }
            set
            {
                this.корпусField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Кварт
        {
            get
            {
                return this.квартField;
            }
            set
            {
                this.квартField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class СведСумНал
    {

        private string налБазаField;

        private string сумНалField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБаза
        {
            get
            {
                return this.налБазаField;
            }
            set
            {
                this.налБазаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНал
        {
            get
            {
                return this.сумНалField;
            }
            set
            {
                this.сумНалField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументСвНПНПЮЛ
    {

        private ФайлДокументСвНПНПЮЛСвРеоргЮЛ свРеоргЮЛField;

        private string наимОргField;

        private string иННЮЛField;

        private string кППField;

        /// <remarks/>
        public ФайлДокументСвНПНПЮЛСвРеоргЮЛ СвРеоргЮЛ
        {
            get
            {
                return this.свРеоргЮЛField;
            }
            set
            {
                this.свРеоргЮЛField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимОрг
        {
            get
            {
                return this.наимОргField;
            }
            set
            {
                this.наимОргField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИННЮЛ
        {
            get
            {
                return this.иННЮЛField;
            }
            set
            {
                this.иННЮЛField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КПП
        {
            get
            {
                return this.кППField;
            }
            set
            {
                this.кППField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументСвНПНПЮЛСвРеоргЮЛ
    {

        private ФайлДокументСвНПНПЮЛСвРеоргЮЛФормРеорг формРеоргField;

        private string иННЮЛField;

        private string кППField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументСвНПНПЮЛСвРеоргЮЛФормРеорг ФормРеорг
        {
            get
            {
                return this.формРеоргField;
            }
            set
            {
                this.формРеоргField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИННЮЛ
        {
            get
            {
                return this.иННЮЛField;
            }
            set
            {
                this.иННЮЛField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КПП
        {
            get
            {
                return this.кППField;
            }
            set
            {
                this.кППField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументСвНПНПЮЛСвРеоргЮЛФормРеорг
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2 = 2,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("3")]
        Item3 = 3,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("5")]
        Item5 = 5,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("6")]
        Item6 = 6,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументПодписант
    {

        private ФИОТип фИОField;

        private ФайлДокументПодписантСвПред свПредField;

        private ФайлДокументПодписантПрПодп прПодпField;

        /// <remarks/>
        public ФИОТип ФИО
        {
            get
            {
                return this.фИОField;
            }
            set
            {
                this.фИОField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументПодписантСвПред СвПред
        {
            get
            {
                return this.свПредField;
            }
            set
            {
                this.свПредField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументПодписантПрПодп ПрПодп
        {
            get
            {
                return this.прПодпField;
            }
            set
            {
                this.прПодпField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументПодписантСвПред
    {

        private string наимДокField;

        private string наимОргField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимДок
        {
            get
            {
                return this.наимДокField;
            }
            set
            {
                this.наимДокField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимОрг
        {
            get
            {
                return this.наимОргField;
            }
            set
            {
                this.наимОргField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументПодписантПрПодп
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2 = 2,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДС
    {

        private ФайлДокументНДССумУплНП сумУплНПField;

        private ФайлДокументНДССумУплНА[] сумУплНАField;

        private ФайлДокументНДССумУпл164 сумУпл164Field;

        private ФайлДокументНДСНалПодтв0 налПодтв0Field;

        private ФайлДокументНДСНалВычПред0 налВычПред0Field;

        private ФайлДокументНДСНалНеПодтв0 налНеПодтв0Field;

        private ФайлДокументНДСОперНеНал оперНеНалField;

        private ФайлДокументНДСКнигаПокуп книгаПокупField;

        private ФайлДокументНДСКнигаПокупДЛ книгаПокупДЛField;

        private ФайлДокументНДСКнигаПрод книгаПродField;

        private ФайлДокументНДСКнигаПродДЛ книгаПродДЛField;

        private ФайлДокументНДСЖУчВыстСчФ жУчВыстСчФField;

        private ФайлДокументНДСЖУчПолучСчФ жУчПолучСчФField;

        private ФайлДокументНДСВыстСчФ_1735 выстСчФ_1735Field;

        /// <remarks/>
        public ФайлДокументНДССумУплНП СумУплНП
        {
            get
            {
                return this.сумУплНПField;
            }
            set
            {
                this.сумУплНПField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумУплНА")]
        public ФайлДокументНДССумУплНА[] СумУплНА
        {
            get
            {
                return this.сумУплНАField;
            }
            set
            {
                this.сумУплНАField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДССумУпл164 СумУпл164
        {
            get
            {
                return this.сумУпл164Field;
            }
            set
            {
                this.сумУпл164Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалПодтв0 НалПодтв0
        {
            get
            {
                return this.налПодтв0Field;
            }
            set
            {
                this.налПодтв0Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалВычПред0 НалВычПред0
        {
            get
            {
                return this.налВычПред0Field;
            }
            set
            {
                this.налВычПред0Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалНеПодтв0 НалНеПодтв0
        {
            get
            {
                return this.налНеПодтв0Field;
            }
            set
            {
                this.налНеПодтв0Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСОперНеНал ОперНеНал
        {
            get
            {
                return this.оперНеНалField;
            }
            set
            {
                this.оперНеНалField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСКнигаПокуп КнигаПокуп
        {
            get
            {
                return this.книгаПокупField;
            }
            set
            {
                this.книгаПокупField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСКнигаПокупДЛ КнигаПокупДЛ
        {
            get
            {
                return this.книгаПокупДЛField;
            }
            set
            {
                this.книгаПокупДЛField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСКнигаПрод КнигаПрод
        {
            get
            {
                return this.книгаПродField;
            }
            set
            {
                this.книгаПродField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСКнигаПродДЛ КнигаПродДЛ
        {
            get
            {
                return this.книгаПродДЛField;
            }
            set
            {
                this.книгаПродДЛField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСЖУчВыстСчФ ЖУчВыстСчФ
        {
            get
            {
                return this.жУчВыстСчФField;
            }
            set
            {
                this.жУчВыстСчФField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСЖУчПолучСчФ ЖУчПолучСчФ
        {
            get
            {
                return this.жУчПолучСчФField;
            }
            set
            {
                this.жУчПолучСчФField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ВыстСчФ_173.5")]
        public ФайлДокументНДСВыстСчФ_1735 ВыстСчФ_1735
        {
            get
            {
                return this.выстСчФ_1735Field;
            }
            set
            {
                this.выстСчФ_1735Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУплНП
    {

        private string оКТМОField;

        private string кБКField;

        private string сумПУ_1735Field;

        private string сумПУ_1731Field;

        private string номДогИТField;

        private string датаНачДогИТField;

        private string датаКонДогИТField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ОКТМО
        {
            get
            {
                return this.оКТМОField;
            }
            set
            {
                this.оКТМОField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КБК
        {
            get
            {
                return this.кБКField;
            }
            set
            {
                this.кБКField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("СумПУ_173.5", DataType = "integer")]
        public string СумПУ_1735
        {
            get
            {
                return this.сумПУ_1735Field;
            }
            set
            {
                this.сумПУ_1735Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("СумПУ_173.1", DataType = "integer")]
        public string СумПУ_1731
        {
            get
            {
                return this.сумПУ_1731Field;
            }
            set
            {
                this.сумПУ_1731Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НомДогИТ
        {
            get
            {
                return this.номДогИТField;
            }
            set
            {
                this.номДогИТField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаНачДогИТ
        {
            get
            {
                return this.датаНачДогИТField;
            }
            set
            {
                this.датаНачДогИТField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаКонДогИТ
        {
            get
            {
                return this.датаКонДогИТField;
            }
            set
            {
                this.датаКонДогИТField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУплНА
    {

        private object itemField;

        private string кППИноField;

        private string кБКField;

        private string оКТМОField;

        private string сумИсчислField;

        private string кодОперField;

        private string сумИсчислОтгрField;

        private string сумИсчислОплField;

        private string сумИсчислНАField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СведПродФЛ", typeof(ФайлДокументНДССумУплНАСведПродФЛ))]
        [System.Xml.Serialization.XmlElementAttribute("СведПродЮЛ", typeof(ФайлДокументНДССумУплНАСведПродЮЛ))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КППИно
        {
            get
            {
                return this.кППИноField;
            }
            set
            {
                this.кППИноField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КБК
        {
            get
            {
                return this.кБКField;
            }
            set
            {
                this.кБКField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ОКТМО
        {
            get
            {
                return this.оКТМОField;
            }
            set
            {
                this.оКТМОField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумИсчисл
        {
            get
            {
                return this.сумИсчислField;
            }
            set
            {
                this.сумИсчислField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумИсчислОтгр
        {
            get
            {
                return this.сумИсчислОтгрField;
            }
            set
            {
                this.сумИсчислОтгрField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумИсчислОпл
        {
            get
            {
                return this.сумИсчислОплField;
            }
            set
            {
                this.сумИсчислОплField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумИсчислНА
        {
            get
            {
                return this.сумИсчислНАField;
            }
            set
            {
                this.сумИсчислНАField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУплНАСведПродФЛ
    {

        private ФИОТип фИОПродField;

        private string иННФЛПродField;

        /// <remarks/>
        public ФИОТип ФИОПрод
        {
            get
            {
                return this.фИОПродField;
            }
            set
            {
                this.фИОПродField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИННФЛПрод
        {
            get
            {
                return this.иННФЛПродField;
            }
            set
            {
                this.иННФЛПродField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУплНАСведПродЮЛ
    {

        private string наимПродField;

        private string иННЮЛПродField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимПрод
        {
            get
            {
                return this.наимПродField;
            }
            set
            {
                this.наимПродField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИННЮЛПрод
        {
            get
            {
                return this.иННЮЛПродField;
            }
            set
            {
                this.иННЮЛПродField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164
    {

        private ФайлДокументНДССумУпл164СумНалОб сумНалОбField;

        private ФайлДокументНДССумУпл164СумНалВыч сумНалВычField;

        private ФайлДокументНДССумУпл164СумВосУпл[] сумВосУплField;

        private ФайлДокументНДССумУпл164СведНалГодИ[] сумВычИнField;

        private string налПУ164Field;

        /// <remarks/>
        public ФайлДокументНДССумУпл164СумНалОб СумНалОб
        {
            get
            {
                return this.сумНалОбField;
            }
            set
            {
                this.сумНалОбField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДССумУпл164СумНалВыч СумНалВыч
        {
            get
            {
                return this.сумНалВычField;
            }
            set
            {
                this.сумНалВычField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумВосУпл")]
        public ФайлДокументНДССумУпл164СумВосУпл[] СумВосУпл
        {
            get
            {
                return this.сумВосУплField;
            }
            set
            {
                this.сумВосУплField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("СведНалГодИ", IsNullable = false)]
        public ФайлДокументНДССумУпл164СведНалГодИ[] СумВычИн
        {
            get
            {
                return this.сумВычИнField;
            }
            set
            {
                this.сумВычИнField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалПУ164
        {
            get
            {
                return this.налПУ164Field;
            }
            set
            {
                this.налПУ164Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СумНалОб
    {

        private СведСумНал реалТов18Field;

        private СведСумНал реалТов10Field;

        private СведСумНал реалТов118Field;

        private СведСумНал реалТов110Field;

        private СведСумНал реалПредИКField;

        private СведСумНал выпСМРСобField;

        private СведСумНал оплПредПостField;

        private ФайлДокументНДССумУпл164СумНалОбСумНалВосст сумНалВосстField;

        private СведСумНал корРеалТов18Field;

        private СведСумНал корРеалТов10Field;

        private СведСумНал корРеалТов118Field;

        private СведСумНал корРеалТов110Field;

        private СведСумНал корРеалПредИКField;

        private string налВосстОбщField;

        private СведСумНал _реалСрок151_1_118Field;

        private СведСумНал _реалСрок151_1_110Field;

        private СведСумНал _уплДеклар151_1Field;

        private СведСумНал _уплДеклар173_6Field;

        /// <remarks/>
        public СведСумНал РеалТов18
        {
            get
            {
                return this.реалТов18Field;
            }
            set
            {
                this.реалТов18Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал РеалТов10
        {
            get
            {
                return this.реалТов10Field;
            }
            set
            {
                this.реалТов10Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал РеалТов118
        {
            get
            {
                return this.реалТов118Field;
            }
            set
            {
                this.реалТов118Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал РеалТов110
        {
            get
            {
                return this.реалТов110Field;
            }
            set
            {
                this.реалТов110Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал РеалПредИК
        {
            get
            {
                return this.реалПредИКField;
            }
            set
            {
                this.реалПредИКField = value;
            }
        }

        /// <remarks/>
        public СведСумНал ВыпСМРСоб
        {
            get
            {
                return this.выпСМРСобField;
            }
            set
            {
                this.выпСМРСобField = value;
            }
        }

        /// <remarks/>
        public СведСумНал ОплПредПост
        {
            get
            {
                return this.оплПредПостField;
            }
            set
            {
                this.оплПредПостField = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДССумУпл164СумНалОбСумНалВосст СумНалВосст
        {
            get
            {
                return this.сумНалВосстField;
            }
            set
            {
                this.сумНалВосстField = value;
            }
        }

        /// <remarks/>
        public СведСумНал КорРеалТов18
        {
            get
            {
                return this.корРеалТов18Field;
            }
            set
            {
                this.корРеалТов18Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал КорРеалТов10
        {
            get
            {
                return this.корРеалТов10Field;
            }
            set
            {
                this.корРеалТов10Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал КорРеалТов118
        {
            get
            {
                return this.корРеалТов118Field;
            }
            set
            {
                this.корРеалТов118Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал КорРеалТов110
        {
            get
            {
                return this.корРеалТов110Field;
            }
            set
            {
                this.корРеалТов110Field = value;
            }
        }

        /// <remarks/>
        public СведСумНал КорРеалПредИК
        {
            get
            {
                return this.корРеалПредИКField;
            }
            set
            {
                this.корРеалПредИКField = value;
            }
        }

        /// <remarks/>
        [XmlElementAttribute("РеалСрок151.1_118")]
        public СведСумНал РеалСрок151_1_118
        {
            get
            {
                return this._реалСрок151_1_118Field;
            }
            set
            {
                this._реалСрок151_1_118Field = value;
            }
        }

        /// <remarks/>
        [XmlElementAttribute("РеалСрок151.1_110")]
        public СведСумНал РеалСрок151_1_110
        {
            get
            {
                return this._реалСрок151_1_110Field;
            }
            set
            {
                this._реалСрок151_1_110Field = value;
            }
        }

        /// <remarks/>
        [XmlElementAttribute("УплДеклар151.1")]
        public СведСумНал УплДеклар151_1
        {
            get
            {
                return this._уплДеклар151_1Field;
            }
            set
            {
                this._уплДеклар151_1Field = value;
            }
        }

        /// <remarks/>
        [XmlElementAttribute("УплДеклар173.6")]
        public СведСумНал УплДеклар173_6
        {
            get
            {
                return this._уплДеклар173_6Field;
            }
            set
            {
                this._уплДеклар173_6Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВосстОбщ
        {
            get
            {
                return this.налВосстОбщField;
            }
            set
            {
                this.налВосстОбщField = value;
            }
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СумНалОбСумНалВосст
    {

        private string сумНалВсField;

        private string сумНал17033Field;

        private string сумНалОперСт0Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНалВс
        {
            get
            {
                return this.сумНалВсField;
            }
            set
            {
                this.сумНалВсField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("СумНал170.3.3", DataType = "integer")]
        public string СумНал17033
        {
            get
            {
                return this.сумНал17033Field;
            }
            set
            {
                this.сумНал17033Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНалОперСт0
        {
            get
            {
                return this.сумНалОперСт0Field;
            }
            set
            {
                this.сумНалОперСт0Field = value;
            }
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СумНалВыч
    {

        private string налПредНППриобField;

        private string налПредНППокField;

        private string налИсчСМРField;

        private string налУплТаможField;

        private string налУплНОТовТСField;

        private string налИсчПродField;

        private string налУплПокНАField;

        private string налВычОбщField;

        private string _налПредНПКапСтрField;

        private string _налВыч171_14Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалПредНППриоб
        {
            get
            {
                return this.налПредНППриобField;
            }
            set
            {
                this.налПредНППриобField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалПредНППок
        {
            get
            {
                return this.налПредНППокField;
            }
            set
            {
                this.налПредНППокField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалИсчСМР
        {
            get
            {
                return this.налИсчСМРField;
            }
            set
            {
                this.налИсчСМРField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалУплТамож
        {
            get
            {
                return this.налУплТаможField;
            }
            set
            {
                this.налУплТаможField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалУплНОТовТС
        {
            get
            {
                return this.налУплНОТовТСField;
            }
            set
            {
                this.налУплНОТовТСField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалИсчПрод
        {
            get
            {
                return this.налИсчПродField;
            }
            set
            {
                this.налИсчПродField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалУплПокНА
        {
            get
            {
                return this.налУплПокНАField;
            }
            set
            {
                this.налУплПокНАField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычОбщ
        {
            get
            {
                return this.налВычОбщField;
            }
            set
            {
                this.налВычОбщField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute(DataType = "integer")]
        public string НалПредНПКапСтр
        {
            get
            {
                return this._налПредНПКапСтрField;
            }
            set
            {
                this._налПредНПКапСтрField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "НалВыч171.14", DataType = "integer")]
        public string НалВыч171_14
        {
            get
            {
                return this._налВыч171_14Field;
            }
            set
            {
                this._налВыч171_14Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СумВосУпл
    {

        private АдрРФТип адрМННедField;

        private ФайлДокументНДССумУпл164СумВосУплСведНалГод[] сведНалГодField;

        private string наимНедвField;

        private string кодОпНедвField;

        private string датаВводОНField;

        private string датаНачАмОтчField;

        private string стВводОНField;

        private string налВычОНField;

        private string наимООСField;

        private string кодОпООСField;

        private string налВычООСField;

        private string датаВводООСField;

        private string стВводООСField;

        /// <remarks/>
        public АдрРФТип АдрМННед
        {
            get
            {
                return this.адрМННедField;
            }
            set
            {
                this.адрМННедField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СведНалГод")]
        public ФайлДокументНДССумУпл164СумВосУплСведНалГод[] СведНалГод
        {
            get
            {
                return this.сведНалГодField;
            }
            set
            {
                this.сведНалГодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимНедв
        {
            get
            {
                return this.наимНедвField;
            }
            set
            {
                this.наимНедвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпНедв
        {
            get
            {
                return this.кодОпНедвField;
            }
            set
            {
                this.кодОпНедвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаВводОН
        {
            get
            {
                return this.датаВводОНField;
            }
            set
            {
                this.датаВводОНField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаНачАмОтч
        {
            get
            {
                return this.датаНачАмОтчField;
            }
            set
            {
                this.датаНачАмОтчField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СтВводОН
        {
            get
            {
                return this.стВводОНField;
            }
            set
            {
                this.стВводОНField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычОН
        {
            get
            {
                return this.налВычОНField;
            }
            set
            {
                this.налВычОНField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string НаимООС
        {
            get
            {
                return this.наимООСField;
            }
            set
            {
                this.наимООСField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string КодОпООС
        {
            get
            {
                return this.кодОпООСField;
            }
            set
            {
                this.кодОпООСField = value;
            }
        }


        /// <remarks/>
        [XmlAttributeAttribute()]
        public string ДатаВводООС
        {
            get
            {
                return this.датаВводООСField;
            }
            set
            {
                this.датаВводООСField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string СтВводООС
        {
            get
            {
                return this.стВводООСField;
            }
            set
            {
                this.стВводООСField = value;
            }
        }

        /// <remarks/>
        [XmlAttributeAttribute()]
        public string НалВычООС
        {
            get
            {
                return this.налВычООСField;
            }
            set
            {
                this.налВычООСField = value;
            }
        }        
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СумВосУплСведНалГод
    {

        private string годОтчField;

        private string датаИсп170Field;

        private decimal доляНеОблField;

        private string налГодField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "gYear")]
        public string ГодОтч
        {
            get
            {
                return this.годОтчField;
            }
            set
            {
                this.годОтчField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ДатаИсп170
        {
            get
            {
                return this.датаИсп170Field;
            }
            set
            {
                this.датаИсп170Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal ДоляНеОбл
        {
            get
            {
                return this.доляНеОблField;
            }
            set
            {
                this.доляНеОблField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалГод
        {
            get
            {
                return this.налГодField;
            }
            set
            {
                this.налГодField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДССумУпл164СведНалГодИ
    {

        private string кППИнУчField;

        private string сумНалИсчField;

        private string сумНалВычField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КППИнУч
        {
            get
            {
                return this.кППИнУчField;
            }
            set
            {
                this.кППИнУчField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНалИсч
        {
            get
            {
                return this.сумНалИсчField;
            }
            set
            {
                this.сумНалИсчField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНалВыч
        {
            get
            {
                return this.сумНалВычField;
            }
            set
            {
                this.сумНалВычField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалПодтв0
    {

        private ФайлДокументНДСНалПодтв0СумОпер4[] сумОпер4Field;

        private ФайлДокументНДСНалПодтв0СумОпер1010447 сумОпер1010447Field;

        private ФайлДокументНДСНалПодтв0СумОпер1010448 сумОпер1010448Field;

        private string сумИсчислИтогField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумОпер4")]
        public ФайлДокументНДСНалПодтв0СумОпер4[] СумОпер4
        {
            get
            {
                return this.сумОпер4Field;
            }
            set
            {
                this.сумОпер4Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалПодтв0СумОпер1010447 СумОпер1010447
        {
            get
            {
                return this.сумОпер1010447Field;
            }
            set
            {
                this.сумОпер1010447Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалПодтв0СумОпер1010448 СумОпер1010448
        {
            get
            {
                return this.сумОпер1010448Field;
            }
            set
            {
                this.сумОпер1010448Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумИсчислИтог
        {
            get
            {
                return this.сумИсчислИтогField;
            }
            set
            {
                this.сумИсчислИтогField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалПодтв0СумОпер4
    {

        private string кодОперField;

        private string налБазаField;

        private string налВычПодField;

        private string налНеПодField;

        private string налВосстField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБаза
        {
            get
            {
                return this.налБазаField;
            }
            set
            {
                this.налБазаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычПод
        {
            get
            {
                return this.налВычПодField;
            }
            set
            {
                this.налВычПодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалНеПод
        {
            get
            {
                return this.налНеПодField;
            }
            set
            {
                this.налНеПодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВосст
        {
            get
            {
                return this.налВосстField;
            }
            set
            {
                this.налВосстField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалПодтв0СумОпер1010447
    {

        private ФайлДокументНДСНалПодтв0СумОпер1010447КодОпер кодОперField;

        private string налБазаField;

        private string налВосстField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументНДСНалПодтв0СумОпер1010447КодОпер КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБаза
        {
            get
            {
                return this.налБазаField;
            }
            set
            {
                this.налБазаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВосст
        {
            get
            {
                return this.налВосстField;
            }
            set
            {
                this.налВосстField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументНДСНалПодтв0СумОпер1010447КодОпер
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1010447")]
        Item1010447 = 1010447,
        Item0 = 0,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалПодтв0СумОпер1010448
    {

        private ФайлДокументНДСНалПодтв0СумОпер1010448КодОпер кодОперField;

        private string корНалБазаУвField;

        private string корНалБазаУмField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументНДСНалПодтв0СумОпер1010448КодОпер КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string КорНалБазаУв
        {
            get
            {
                return this.корНалБазаУвField;
            }
            set
            {
                this.корНалБазаУвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string КорНалБазаУм
        {
            get
            {
                return this.корНалБазаУмField;
            }
            set
            {
                this.корНалБазаУмField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументНДСНалПодтв0СумОпер1010448КодОпер
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1010448")]
        Item1010448 = 1010448,
        Item0 = 0,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалВычПред0
    {

        private ФайлДокументНДСНалВычПред0СумПер[] сумПерField;

        private string сумВозмПдтвField;

        private string сумВозмНеПдтвField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумПер")]
        public ФайлДокументНДСНалВычПред0СумПер[] СумПер
        {
            get
            {
                return this.сумПерField;
            }
            set
            {
                this.сумПерField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумВозмПдтв
        {
            get
            {
                return this.сумВозмПдтвField;
            }
            set
            {
                this.сумВозмПдтвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумВозмНеПдтв
        {
            get
            {
                return this.сумВозмНеПдтвField;
            }
            set
            {
                this.сумВозмНеПдтвField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалВычПред0СумПер
    {

        private ФайлДокументНДСНалВычПред0СумПерСумОпер5[] сумОпер5Field;

        private string отчетГодField;

        private ФайлДокументНДСНалВычПред0СумПерПериод периодField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумОпер5")]
        public ФайлДокументНДСНалВычПред0СумПерСумОпер5[] СумОпер5
        {
            get
            {
                return this.сумОпер5Field;
            }
            set
            {
                this.сумОпер5Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "gYear")]
        public string ОтчетГод
        {
            get
            {
                return this.отчетГодField;
            }
            set
            {
                this.отчетГодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументНДСНалВычПред0СумПерПериод Период
        {
            get
            {
                return this.периодField;
            }
            set
            {
                this.периодField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалВычПред0СумПерСумОпер5
    {

        private string кодОперField;

        private string налБазаПодField;

        private string налВычПодField;

        private string налБазаНеПодField;

        private string налВычНеПодField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБазаПод
        {
            get
            {
                return this.налБазаПодField;
            }
            set
            {
                this.налБазаПодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычПод
        {
            get
            {
                return this.налВычПодField;
            }
            set
            {
                this.налВычПодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБазаНеПод
        {
            get
            {
                return this.налБазаНеПодField;
            }
            set
            {
                this.налБазаНеПодField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычНеПод
        {
            get
            {
                return this.налВычНеПодField;
            }
            set
            {
                this.налВычНеПодField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументНДСНалВычПред0СумПерПериод
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("01")]
        Item01 = 1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("02")]
        Item02 = 2,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("03")]
        Item03 = 3,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("04")]
        Item04 = 4,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("05")]
        Item05 = 5,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("06")]
        Item06 = 6,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("07")]
        Item07 = 7,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("08")]
        Item08 = 8,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("09")]
        Item09 = 9,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("10")]
        Item10 = 10,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        Item11 = 11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        Item12 = 12,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("21")]
        Item21 = 21,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("22")]
        Item22 = 22,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("23")]
        Item23 = 23,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("24")]
        Item24 = 24,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалНеПодтв0
    {

        private ФайлДокументНДСНалНеПодтв0СумОпер6[] сумОпер6Field;

        private ФайлДокументНДСНалНеПодтв0СумОпер1010449 сумОпер1010449Field;

        private ФайлДокументНДСНалНеПодтв0СумОпер1010450 сумОпер1010450Field;

        private string сумНал164ИтField;

        private string налВычНеПодИтField;

        private string налИсчислИтField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумОпер6")]
        public ФайлДокументНДСНалНеПодтв0СумОпер6[] СумОпер6
        {
            get
            {
                return this.сумОпер6Field;
            }
            set
            {
                this.сумОпер6Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалНеПодтв0СумОпер1010449 СумОпер1010449
        {
            get
            {
                return this.сумОпер1010449Field;
            }
            set
            {
                this.сумОпер1010449Field = value;
            }
        }

        /// <remarks/>
        public ФайлДокументНДСНалНеПодтв0СумОпер1010450 СумОпер1010450
        {
            get
            {
                return this.сумОпер1010450Field;
            }
            set
            {
                this.сумОпер1010450Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНал164Ит
        {
            get
            {
                return this.сумНал164ИтField;
            }
            set
            {
                this.сумНал164ИтField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычНеПодИт
        {
            get
            {
                return this.налВычНеПодИтField;
            }
            set
            {
                this.налВычНеПодИтField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалИсчислИт
        {
            get
            {
                return this.налИсчислИтField;
            }
            set
            {
                this.налИсчислИтField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалНеПодтв0СумОпер6
    {

        private string кодОперField;

        private string налБазаField;

        private string сумНал164Field;

        private string налВычНеПодField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБаза
        {
            get
            {
                return this.налБазаField;
            }
            set
            {
                this.налБазаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СумНал164
        {
            get
            {
                return this.сумНал164Field;
            }
            set
            {
                this.сумНал164Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВычНеПод
        {
            get
            {
                return this.налВычНеПодField;
            }
            set
            {
                this.налВычНеПодField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалНеПодтв0СумОпер1010449
    {

        private ФайлДокументНДСНалНеПодтв0СумОпер1010449КодОпер кодОперField;

        private string налБазаField;

        private string корИсч16423Field;

        private string налВосстField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументНДСНалНеПодтв0СумОпер1010449КодОпер КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалБаза
        {
            get
            {
                return this.налБазаField;
            }
            set
            {
                this.налБазаField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("КорИсч.164.23", DataType = "integer")]
        public string КорИсч16423
        {
            get
            {
                return this.корИсч16423Field;
            }
            set
            {
                this.корИсч16423Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалВосст
        {
            get
            {
                return this.налВосстField;
            }
            set
            {
                this.налВосстField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументНДСНалНеПодтв0СумОпер1010449КодОпер
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1010449")]
        Item1010449 = 1010449,
        Item0 = 0,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСНалНеПодтв0СумОпер1010450
    {

        private ФайлДокументНДСНалНеПодтв0СумОпер1010450КодОпер кодОперField;

        private string корНалБазаУвField;

        private string корИсч16423УвField;

        private string корНалБазаУмField;

        private string корИсч16423УмField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлДокументНДСНалНеПодтв0СумОпер1010450КодОпер КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string КорНалБазаУв
        {
            get
            {
                return this.корНалБазаУвField;
            }
            set
            {
                this.корНалБазаУвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("КорИсч.164.23Ув", DataType = "integer")]
        public string КорИсч16423Ув
        {
            get
            {
                return this.корИсч16423УвField;
            }
            set
            {
                this.корИсч16423УвField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string КорНалБазаУм
        {
            get
            {
                return this.корНалБазаУмField;
            }
            set
            {
                this.корНалБазаУмField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("КорИсч.164.23Ум", DataType = "integer")]
        public string КорИсч16423Ум
        {
            get
            {
                return this.корИсч16423УмField;
            }
            set
            {
                this.корИсч16423УмField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументНДСНалНеПодтв0СумОпер1010450КодОпер
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1010450")]
        Item1010450 = 1010450,
        Item0 = 0,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСОперНеНал
    {

        private ФайлДокументНДСОперНеНалСумОпер7[] сумОпер7Field;

        private string оплПостСв6МесField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("СумОпер7")]
        public ФайлДокументНДСОперНеНалСумОпер7[] СумОпер7
        {
            get
            {
                return this.сумОпер7Field;
            }
            set
            {
                this.сумОпер7Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string ОплПостСв6Мес
        {
            get
            {
                return this.оплПостСв6МесField;
            }
            set
            {
                this.оплПостСв6МесField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСОперНеНалСумОпер7
    {

        private string кодОперField;

        private string стРеалТовField;

        private string стПриобТовField;

        private string налНеВычField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string КодОпер
        {
            get
            {
                return this.кодОперField;
            }
            set
            {
                this.кодОперField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СтРеалТов
        {
            get
            {
                return this.стРеалТовField;
            }
            set
            {
                this.стРеалТовField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string СтПриобТов
        {
            get
            {
                return this.стПриобТовField;
            }
            set
            {
                this.стПриобТовField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string НалНеВыч
        {
            get
            {
                return this.налНеВычField;
            }
            set
            {
                this.налНеВычField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСКнигаПокуп
    {

        private string наимКнПокField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимКнПок
        {
            get
            {
                return this.наимКнПокField;
            }
            set
            {
                this.наимКнПокField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСКнигаПокупДЛ
    {

        private string наимКнПокДЛField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимКнПокДЛ
        {
            get
            {
                return this.наимКнПокДЛField;
            }
            set
            {
                this.наимКнПокДЛField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСКнигаПрод
    {

        private string наимКнПродField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимКнПрод
        {
            get
            {
                return this.наимКнПродField;
            }
            set
            {
                this.наимКнПродField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСКнигаПродДЛ
    {

        private string наимКнПродДЛField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимКнПродДЛ
        {
            get
            {
                return this.наимКнПродДЛField;
            }
            set
            {
                this.наимКнПродДЛField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСЖУчВыстСчФ
    {

        private string наимЖУчВыстСчФField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимЖУчВыстСчФ
        {
            get
            {
                return this.наимЖУчВыстСчФField;
            }
            set
            {
                this.наимЖУчВыстСчФField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСЖУчПолучСчФ
    {

        private string наимЖУчПолучСчФField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string НаимЖУчПолучСчФ
        {
            get
            {
                return this.наимЖУчПолучСчФField;
            }
            set
            {
                this.наимЖУчПолучСчФField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ФайлДокументНДСВыстСчФ_1735
    {

        private string наимВыстСчФ_1735Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("НаимВыстСчФ_173.5")]
        public string НаимВыстСчФ_1735
        {
            get
            {
                return this.наимВыстСчФ_1735Field;
            }
            set
            {
                this.наимВыстСчФ_1735Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументКНД
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1151001")]
        Item1151001 = 1151001,

        [System.Xml.Serialization.XmlEnumAttribute("1115104")]
        Item1115104 = 1115104
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументПериод
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("01")]
        Item01 = 1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("02")]
        Item02 = 2,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("03")]
        Item03 = 3,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("04")]
        Item04 = 4,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("05")]
        Item05 = 5,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("06")]
        Item06 = 6,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("07")]
        Item07 = 7,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("08")]
        Item08 = 8,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("09")]
        Item09 = 9,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("10")]
        Item10 = 10,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        Item11 = 11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        Item12 = 12,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("21")]
        Item21 = 21,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("22")]
        Item22 = 22,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("23")]
        Item23 = 23,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("24")]
        Item24 = 24,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("51")]
        Item51 = 51,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("54")]
        Item54 = 54,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("55")]
        Item55 = 55,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("56")]
        Item56 = 56,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("71")]
        Item71 = 71,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("72")]
        Item72 = 72,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("73")]
        Item73 = 73,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("74")]
        Item74 = 74,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("75")]
        Item75 = 75,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("76")]
        Item76 = 76,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("77")]
        Item77 = 77,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("78")]
        Item78 = 78,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("79")]
        Item79 = 79,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("80")]
        Item80 = 80,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("81")]
        Item81 = 81,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("82")]
        Item82 = 82,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлДокументПоМесту
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("116")]
        Item116 = 116,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("213")]
        Item213 = 213,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("214")]
        Item214 = 214,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("215")]
        Item215 = 215,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("216")]
        Item216 = 216,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("227")]
        Item227 = 227,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("231")]
        Item231 = 231,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("250")]
        Item250 = 250,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("331")]
        Item331 = 331,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлВерсФорм
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("5.04")]
        Item504 = 0,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал812
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал8
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал81
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал9
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал91
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал10
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал11
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum ФайлПризнНал12
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0 = 0,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1 = 1,
    }
}