﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.Services.Entities.ExplainDescription
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class описание
    {
        private string КНДField;

        private string КодНОField;

        public string КНД
        {
            get
            {
                return this.КНДField;
            }
            set
            {
                this.КНДField = value;
            }
        }

        public string КодНО
        {
            get
            {
                return this.КодНОField;
            }
            set
            {
                this.КодНОField = value;
            }
        }
    }
}
