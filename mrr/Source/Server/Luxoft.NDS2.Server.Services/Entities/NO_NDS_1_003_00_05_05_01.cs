﻿
namespace Luxoft.NDS2.Server.Services.Entities
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Файл505
    {

        private ФайлДокумент документField;

        private string идФайлField;

        private string версПрогField;

        private ФайлВерсФорм версФормField;

        private ФайлПризнНал812 признНал812Field;

        private ФайлПризнНал8 признНал8Field;

        private bool признНал8FieldSpecified;

        private ФайлПризнНал81 признНал81Field;

        private bool признНал81FieldSpecified;

        private ФайлПризнНал9 признНал9Field;

        private bool признНал9FieldSpecified;

        private ФайлПризнНал91 признНал91Field;

        private bool признНал91FieldSpecified;

        private ФайлПризнНал10 признНал10Field;

        private bool признНал10FieldSpecified;

        private ФайлПризнНал11 признНал11Field;

        private bool признНал11FieldSpecified;

        private ФайлПризнНал12 признНал12Field;

        private bool признНал12FieldSpecified;

        /// <remarks/>
        public ФайлДокумент Документ
        {
            get
            {
                return this.документField;
            }
            set
            {
                this.документField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ИдФайл
        {
            get
            {
                return this.идФайлField;
            }
            set
            {
                this.идФайлField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ВерсПрог
        {
            get
            {
                return this.версПрогField;
            }
            set
            {
                this.версПрогField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлВерсФорм ВерсФорм
        {
            get
            {
                return this.версФормField;
            }
            set
            {
                this.версФормField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("ПризнНал8-12")]
        public ФайлПризнНал812 ПризнНал812
        {
            get
            {
                return this.признНал812Field;
            }
            set
            {
                this.признНал812Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал8 ПризнНал8
        {
            get
            {
                return this.признНал8Field;
            }
            set
            {
                this.признНал8Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал8Specified
        {
            get
            {
                return this.признНал8FieldSpecified;
            }
            set
            {
                this.признНал8FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал81 ПризнНал81
        {
            get
            {
                return this.признНал81Field;
            }
            set
            {
                this.признНал81Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал81Specified
        {
            get
            {
                return this.признНал81FieldSpecified;
            }
            set
            {
                this.признНал81FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал9 ПризнНал9
        {
            get
            {
                return this.признНал9Field;
            }
            set
            {
                this.признНал9Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал9Specified
        {
            get
            {
                return this.признНал9FieldSpecified;
            }
            set
            {
                this.признНал9FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал91 ПризнНал91
        {
            get
            {
                return this.признНал91Field;
            }
            set
            {
                this.признНал91Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал91Specified
        {
            get
            {
                return this.признНал91FieldSpecified;
            }
            set
            {
                this.признНал91FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал10 ПризнНал10
        {
            get
            {
                return this.признНал10Field;
            }
            set
            {
                this.признНал10Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал10Specified
        {
            get
            {
                return this.признНал10FieldSpecified;
            }
            set
            {
                this.признНал10FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал11 ПризнНал11
        {
            get
            {
                return this.признНал11Field;
            }
            set
            {
                this.признНал11Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал11Specified
        {
            get
            {
                return this.признНал11FieldSpecified;
            }
            set
            {
                this.признНал11FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ФайлПризнНал12 ПризнНал12
        {
            get
            {
                return this.признНал12Field;
            }
            set
            {
                this.признНал12Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ПризнНал12Specified
        {
            get
            {
                return this.признНал12FieldSpecified;
            }
            set
            {
                this.признНал12FieldSpecified = value;
            }
        }
    }

}
