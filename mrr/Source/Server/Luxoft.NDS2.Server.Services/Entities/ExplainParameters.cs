﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.Entities.Parameters
{
    public class ExplainParameters
    {
        public ExplainReplyInfo ExplainReplyInfo { get; set; }
        public DeclarationSummary Decl { get; set; } 
        public DiscrepancyDocumentInfo DocInfo { get; set; } 
        public ASKDekl AskDecl { get; set; } 
        public Entities.Файл AskFile { get; set; }
        public List<ExplainInvoice> Invoices { get; set; } 
        public List<ExplainInvoiceCorrect> InvoicesCorrectes { get; set; }
        public string FullNameVersion { get; set; }
        public string Version { get; set; }
        public string CodeSoun { get; set; }
        public string IdFrom { get; set; }
        public string IdTo { get; set; }
        public string NameFileOfExplain { get; set; }
        public string NameFileOfInventory { get; set; }
        public string NameFileOfPackageDescription { get; set; }
        public string NameFileOfDescription { get; set; }
        public string NameFileIntoZipOfExplain { get; set; }
        public string NameFileIntoZipOfInventory { get; set; }
        public string NameFileIntoZipOfPackageDescription { get; set; }
        public string NameFileIntoZipOfDescription { get; set; }
    }
}
