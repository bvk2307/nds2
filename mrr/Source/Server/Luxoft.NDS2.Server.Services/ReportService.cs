﻿using System;
using System.Collections.Generic;
using System.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.DAL;
using CommonComponents.Shared;
using CommonComponents.Communication;
using Luxoft.NDS2.Server.ThriftProxy;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IReportService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ReportService : IReportService
    {
        private ServiceHelpers _helper;

        public ReportService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }
        
        #region Implementation of IReportService

        public ReportDocumentResult CreateReport(string templateName, DataSet data)
        {
            return null;
        }

        public OperationResult<PageResult<InspectorMonitoringWork>> SearchInspectorMonitoringWork(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchInspectorMonitoringWork(criteria, taskId));
        }

        public OperationResult<PageResult<LoadingInspection>> SearchLoadingInspection(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchLoadingInspection(criteria, taskId));
        }

        public OperationResult<PageResult<MonitoringProcessingDeclaration>> SearchMonitoringProcessingDeclaration(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchMonitoringProcessingDeclaration(criteria, taskId));
        }

        public OperationResult<PageResult<CheckControlRatio>> SearchCheckControlRatio(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchCheckControlRatio(criteria, taskId));
        }

        public OperationResult<PageResult<MatchingRule>> SearchMatchingRule(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchMatchingRule(criteria, taskId));
        }

        public OperationResult<PageResult<CheckLogicControl>> SearchCheckLogicControl(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchCheckLogicControl(criteria, taskId));
        }

        public OperationResult<PageResult<DeclarationStatistic>> SearchDeclarationStatistic(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchDeclarationStatistic(criteria, taskId));
        }

        public OperationResult<PageResult<DynamicTechnoParameter>> SearchDynamicTechnoParameter(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchDynamicTechnoParameter(criteria, taskId));
        }

        public OperationResult<List<DynamicParameter>> SearchDictionaryDynamicParameters(DynamicModuleType moduleType)
        {
            return _helper.Do(() => 
                {
                    IReportAdapter reportAdapter = TableAdapterCreator.Report(_helper);
                    return reportAdapter.SearchDictionaryDynamicParameters(moduleType);
                });
        }

        public OperationResult<List<DateTime>> GetDatesTechnoReport()
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).GetDatesTechnoReport());
        }

        public OperationResult<PageResult<DynamicDeclarationStatistic>> SearchDynamicDeclarationStatistic(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchDynamicDeclarationStatistic(criteria, taskId));
        }

        public OperationResult<PageResult<InfoResultsOfMatching>> SearchInfoResultsOfMatching(QueryConditions criteria, long taskId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).SearchInfoResultsOfMatching(criteria, taskId));
        }

        public OperationResult<long> SaveTask(QueryConditions criteria, long requestId, int reportNum)
        {
            return _helper.DoEx(() => TableAdapterCreator.Report(_helper).SaveTask(criteria, requestId, reportNum));
        }

        public OperationResult<TaskStatus> GetTaskStatus(long taskId)
        {
            return _helper.DoEx(() => TableAdapterCreator.Report(_helper).GetTaskStatus(taskId));
        }

        public OperationResult RunCreateReport(int reportNum, long taskId, long requestId)
        {
            return _helper.Do(() => TableAdapterCreator.Report(_helper).RunCreateReport(reportNum, taskId, requestId));
        }

        public OperationResult<long> StartBuildReport(QueryConditions criteria, int reportNum)
        {
            return _helper.DoEx<long>(
                () =>
                {
                    var mcProxy = ClientProxyCreator.Create(
                        _helper.GetConfigurationValue(Constants.MC_SERVICE_HOST),
                        int.Parse(_helper.GetConfigurationValue(Constants.MC_SERVICE_PORT)));

                    ReportHelper _reportHelper = new ReportHelper();

                    //--- год
                    int? yearPeriod = (int)_reportHelper.GetFilterFromCondition(criteria, "Year");
                    int year = 0;
                    if (yearPeriod != null)
                    {
                        year = (int)yearPeriod;
                    }

                    //--- Налоговый период
                    List<string> periods = new List<string>();
                    string onePeriod = (string)_reportHelper.GetFilterFromCondition(criteria, "NalogPeriod");
                    if (!string.IsNullOrEmpty(onePeriod))
                    {
                        periods.Add(onePeriod);
                    }

                    //--- Дата
                    string date = String.Empty;
                    DateTime? dtCreateReport = (DateTime?)_reportHelper.GetFilterFromCondition(criteria, "DateCreateReport");
                    if (dtCreateReport != null)
                    {
                        date = string.Format("{0:00}.{1:00}.{2:0000}", dtCreateReport.Value.Day, dtCreateReport.Value.Month, dtCreateReport.Value.Year);
                    }

                    //--- Инспекции
                    List<string> inspections = _reportHelper.GetParamListCriteria(criteria, "NalogOrganCode");

                    long retRequstId = 0;
                    long? requstId = null;
                    //-------------------- проверка, что запрос с такими парметрами уже был ???????
                    if (!requstId.HasValue)
                    {
                        if (reportNum == (int)ReportNum.CheckLogicControl)
                        {
                            requstId = mcProxy.RequestReportLogicalChecks(year, periods, date, inspections);
                        }
                    }
                    if (requstId != null)
                    {
                        retRequstId = (long)requstId;
                    }

                    return retRequstId;
                },
                new string[] { }
            );
        }

        public OperationResult<DataRequestStatusWitnInitialize> CheckBuildReport(long requestId)
        {
            return _helper.Do<DataRequestStatusWitnInitialize>(
                () =>
                {
                    DataRequestStatusWitnInitialize st = new DataRequestStatusWitnInitialize() { IsIninializing = false };

                    try
                    {
                        DataRequestStatus rez;
                        rez = TableAdapterCreator.Report(_helper).Status(requestId);
                        st.Type = rez.Type;
                    }
                    catch (InvalidOperationException ex)
                    {
                        st.Type = RequestStatusType.Error;
                    }

                    return st;
                },
                new string[] { }
            );
        }

        #endregion
    }
}
