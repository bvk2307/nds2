﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Exceptions;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.ThriftProxy;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Server.Services.Managers;
using Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IObjectLocker))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class ObjectLockerService : IObjectLocker
    {
        private ServiceHelpers _helper;
        private DalManager _dalManager;
        private IAuthorizationProvider _localAuthProvider;

        public ObjectLockerService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _dalManager = new DalManager(_helper);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<Object> Lock(long objectId, long objectType, string hostName)
        {
            return DalHelper.Execute(() => new OperationResult<Object>
            {
                Result = TableAdapterCreator.ObjectLocker(_helper).Lock(objectId, objectType, _helper.User.Name, hostName),
                Status = ResultStatus.Success
            }, _helper);
        }

        public OperationResult Unlock(string lockKey, string hostName)
        {
            OperationResult ret = new OperationResult {Status = ResultStatus.Success};
            
            if (!String.IsNullOrEmpty(lockKey) && !String.IsNullOrEmpty(hostName))
                ret = DalHelper.Execute(() => TableAdapterCreator.ObjectLocker(_helper).UnLock(lockKey, _helper.User.Name, hostName), _helper);
            
            return ret;
        }

        public OperationResult<Object> CheckLock(long objectId, long objectType)
        {
            return DalHelper.Execute(() => new OperationResult<Object>
            {
                Result = TableAdapterCreator.ObjectLocker(_helper).GetLockers(objectId, objectType),
                Status = ResultStatus.Success
            }, _helper);
        }
    }
}