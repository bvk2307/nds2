﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Luxoft.NDS2.Server.Services.ExplainTks
{
    public class ExplainTksFieldConverter
    {
        private ExplainFieldFormatter explainFieldFormatter = new ExplainFieldFormatter();

        private Dictionary<string, string> mappingFromTksToClaimChapterGeneral = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewInvoiceNumber), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.INVOICE_NUM) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewInvoiceDate), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.INVOICE_DATE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeNumber), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CHANGE_NUM) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeDate), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CHANGE_DATE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewCorrectionNumber), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CORRECTION_NUM) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewCorrectionDate), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CORRECTION_DATE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeCorrectionNumber), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CHANGE_CORRECTION_NUM) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeCorrectionDate), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.CHANGE_CORRECTION_DATE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewOkvCode), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.OKV_CODE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAgencyInvoiceNumber), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_AGENCY_INFO_NUM) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAgencyInvoiceDate), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_AGENCY_INFO_DATE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewDealCode), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.DEAL_KIND_CODE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewOperationCodesBit), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.OPERATION_CODE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.ReceiveDates), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUY_ACCEPT_DATE_VIEW) },
            { TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_NUM_VIEW), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_NUM_VIEW) },
            { TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_DATE_VIEW), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_DATE_VIEW) }
        };

        private Dictionary<string, string> mappingFromTksToClaimChapterEight = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_KPP) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_BUY_AMOUNT) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_KPP) }
        };

        private Dictionary<string, string> mappingFromTksToClaimChapterNine = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_KPP) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_SELL_IN_CURR) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount2), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_SELL) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_KPP) }
        };

        private Dictionary<string, string> mappingFromTksToClaimChapterTen = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_KPP) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_TOTAL) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_AGENCY_INFO_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_AGENCY_INFO_KPP) }
        };

        private Dictionary<string, string> mappingFromTksToClaimChapterEleven = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.SELLER_KPP) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_TOTAL) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BROKER_KPP) }
        };

        private Dictionary<string, string> mappingFromTksToClaimChapterTwelve = new Dictionary<string, string>()
        {
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_INN) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.BUYER_KPP) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_TAX_FREE) },
            { TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount2), TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.PRICE_BUY_AMOUNT) }
        };

        public string ConvertFieldNameToInvoice(int chapter, string fieldNameTks)
        {
            if (mappingFromTksToClaimChapterGeneral.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterGeneral[fieldNameTks];

            if (chapter == 8 && mappingFromTksToClaimChapterEight.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterEight[fieldNameTks];

            if (chapter == 9 && mappingFromTksToClaimChapterNine.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterNine[fieldNameTks];

            if (chapter == 10 && mappingFromTksToClaimChapterTen.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterTen[fieldNameTks];

            if (chapter == 11 && mappingFromTksToClaimChapterEleven.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterEleven[fieldNameTks];

            if (chapter == 12 && mappingFromTksToClaimChapterTwelve.ContainsKey(fieldNameTks))
                return mappingFromTksToClaimChapterTwelve[fieldNameTks];

            return fieldNameTks;
        }

        public List<ExplainInvoiceCorrect> ConvertToExplainInvoiceChanges(
            List<ExplainTksInvoiceChanges> changes)
        {
            var result = new List<ExplainInvoiceCorrect>();

            foreach (var item in changes)
            {
                if (item.NewAmount.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), item.NewAmount);

                if (!string.IsNullOrEmpty(item.NewInvoiceNumber))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewInvoiceNumber), item.NewInvoiceNumber);

                if (item.NewInvoiceDate.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewInvoiceDate), item.NewInvoiceDate);

                if (!string.IsNullOrEmpty(item.NewChangeNumber))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeNumber), item.NewChangeNumber);

                if (item.NewChangeDate.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeDate), item.NewChangeDate);

                if (!string.IsNullOrEmpty(item.NewCorrectionNumber))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewCorrectionNumber), item.NewCorrectionNumber);

                if (item.NewCorrectionDate.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewCorrectionDate), item.NewCorrectionDate);

                if (!string.IsNullOrEmpty(item.NewChangeCorrectionNumber))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeCorrectionNumber), item.NewChangeCorrectionNumber);

                if (item.NewChangeCorrectionDate.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewChangeCorrectionDate), item.NewChangeCorrectionDate);

                if (!string.IsNullOrEmpty(item.NewOkvCode))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewOkvCode), item.NewOkvCode);

                if (!string.IsNullOrEmpty(item.NewAgencyInvoiceNumber))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAgencyInvoiceNumber), item.NewAgencyInvoiceNumber);

                if (item.NewAgencyInvoiceDate.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAgencyInvoiceDate), item.NewAgencyInvoiceDate);

                if (!string.IsNullOrEmpty(item.NewDealCode))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewDealCode), item.NewDealCode);

                if (!string.IsNullOrEmpty(item.NewBrokerInn))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerInn), item.NewBrokerInn);

                if (!string.IsNullOrEmpty(item.NewBrokerKpp))
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewBrokerKpp), item.NewBrokerKpp);

                if (item.NewAmount.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount), item.NewAmount);

                if (item.NewAmount2.HasValue)
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewAmount2), item.NewAmount2);

                if (item.NewOperationCodesBit.HasValue)
                {
                    var operationCodes = item.NewOperationCodesBit.Value.ToOperationCodes();
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewOperationCodesBit), operationCodes);
                }

                if (item.ReceiveDates.Any())
                {
                    var receiveDates = item.ReceiveDates.New().Format();
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.ReceiveDates), receiveDates);
                }

                if (item.ReceiptDocuments.Any())
                {
                    var receiptDocumentNums = item.ReceiptDocuments.New().FormatDocumentNum();
                    var receiptDocumentDates = item.ReceiptDocuments.New().FormatDocumentDate();
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_NUM_VIEW), receiptDocumentNums);
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<InvoiceRelated>.GetMemberName(dto => dto.RECEIPT_DOC_DATE_VIEW), receiptDocumentDates);
                }

                if (item.Contractors.Any())
                {
                    var contractorInns = item.Contractors.New().FormatInn();
                    var contractorKpps = item.Contractors.New().FormatKpp();
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorInn), contractorInns);
                    CreateExplainInvoiceCorrect(result, item, TypeHelper<ExplainTksInvoiceChanges>.GetMemberName(dto => dto.NewContractorKpp), contractorKpps);
                }
            }
            return result;
        }

        private void CreateExplainInvoiceCorrect(List<ExplainInvoiceCorrect> result, ExplainTksInvoiceChanges item, string fieldName, object fieldValue)
        {
            var formatting = new Dictionary<Type, Func<object, string>>()
            {
                { typeof(decimal), (value) => { return explainFieldFormatter.FormatDecimal((decimal)fieldValue); } },
                { typeof(DateTime), (value) => { return explainFieldFormatter.FormatDateTime((DateTime)fieldValue); } },
                { typeof(string), (value) => { return (string)fieldValue; } }
            };

            result.Add(new ExplainInvoiceCorrect()
            {
                ExplainId = item.ExplainId,
                InvoiceId = item.RowKey,
                ExplainIncomingDate = item.ExplainIncomingDate,
                FieldName = ConvertFieldNameToInvoice(item.Chapter, fieldName),
                FieldValue = formatting[fieldValue.GetType()].Invoke(fieldValue)
            });
        }
    }
}
