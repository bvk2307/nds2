﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using CommonComponents.Utils;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.UserTask;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;


namespace Luxoft.NDS2.Server.Services.UserTasks
{
    [CommunicationContract(typeof(IUserTaskService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class UserTaskService : IUserTaskService
    {

        private readonly ServiceHelpers _helper;
        private readonly UserRights _userRights;
        private const int StatusCreatedId = 3;
        private const int StatusOverdueId = 4;

        public UserTaskService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            var userPermissions = _helper.Services.Get<IAuthorizationProvider>().GetUserPermissions();
            _userRights = new UserRights(userPermissions);
        }

        /// <summary>
        /// права текущего user
        /// </summary>
        public OperationResult<UserRights> GetUserRights()
        {
            return new OperationResult<UserRights>(_userRights);
        }

        /// <summary>
        /// статистика по пз текущего user
        /// </summary>
        public OperationResult<List<Summary>> GetSummaryList()
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var dataAdapter = UserTaskAdapterCreator.CreateSummaryDataAdapter(_helper, connection);

                    if (_userRights.UserTaskRole == UserTaskRole.Administrator)
                        return dataAdapter.GetSummaryBySono(_userRights.SonoCode);
                    if (_userRights.UserTaskRole == UserTaskRole.Inspector)
                        return dataAdapter.GetSummaryBySid(_helper.CurrentUserSID, _userRights.SonoCode);
                    
                    return new List<Summary>();
                }
            });
        }

        /// <summary>
        /// фильтр по отделу-сотруднику
        /// </summary>
        private FilterExpressionGroup GetUserRightsFilter()
        {
            var result = FilterExpressionCreator.CreateUnion();
            switch (_userRights.UserTaskRole)
            {
                case UserTaskRole.Administrator:
                    // фильтр по отделу

                    if (_userRights.IsCentral) // доп фильтра SonoCode нет
                        return result;
                    if (_userRights.IsManagement)
                        result.WithExpression(FilterExpressionCreator.Create(
                            TypeHelper<UserTaskInList>.GetMemberName(x => x.RegionCode),
                            ColumnFilter.FilterComparisionOperator.Equals,
                            _userRights.RegionCode));
                    else
                        result.WithExpression(FilterExpressionCreator.Create(
                            TypeHelper<UserTaskInList>.GetMemberName(x => x.SonoCode),
                            ColumnFilter.FilterComparisionOperator.Equals,
                            _userRights.SonoCode));
                    break;
                case UserTaskRole.Inspector:
                    // фильтр по сотруднику
                    result.WithExpression(FilterExpressionCreator.Create(
                        TypeHelper<UserTaskInList>.GetMemberName(x => x.AssignedSid),
                        ColumnFilter.FilterComparisionOperator.Equals,
                        _helper.CurrentUserSID));
                    break;
            }

            return result;
        }

        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        /// <param name="queryContext">условия поиска</param>
        public OperationResult<PageResult<UserTaskInList>> Search(QueryConditions queryContext, bool fullData)
        {
            FieldSubstitution(queryContext);
            AdditionalStatusConditions(queryContext);
            SetDefaultSort(queryContext);

            var searchCriteria = queryContext.Filter.ToFilterGroup();
            var userRightsFilter = GetUserRightsFilter();
            searchCriteria.WithExpression(userRightsFilter);

            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var pageResult = new PageResult<UserTaskInList>();
                        

                        var dataAdapter = UserTaskAdapterCreator.Create(_helper, connection);

                        pageResult.Rows = fullData ?
                         dataAdapter
                                .SearchFullData(searchCriteria,
                                    queryContext.Sorting,
                                    queryContext.PaginationDetails.RowsToSkip,
                                    queryContext.PaginationDetails.RowsToSkip +
                                    queryContext.PaginationDetails.RowsToTake.Value
                                ) :
                            dataAdapter
                                .Search(searchCriteria,
                                    queryContext.Sorting,
                                    queryContext.PaginationDetails.RowsToSkip,
                                    queryContext.PaginationDetails.RowsToSkip +
                                    queryContext.PaginationDetails.RowsToTake.Value
                                );

                        if (!queryContext.PaginationDetails.SkipTotalMatches)
                        {
                            pageResult.TotalMatches = dataAdapter.Count(searchCriteria);
                            pageResult.TotalAvailable = dataAdapter.Total(userRightsFilter);
                        }

                        return pageResult;
                    }
                });
        }

        private static void SetDefaultSort(QueryConditions queryContext)
        {
            //неназн задания - в конец списка вместо клиент фильтра
            if (queryContext.Sorting.IsNullOrEmpty())
                queryContext.Sorting =
                    new List<ColumnSort>
                    {
                        new ColumnSort
                        {
                            ColumnKey = TypeHelper<UserTaskInList>.GetMemberName(t => t.CurrentProcessState),
                            Order = ColumnSort.SortOrder.Asc
                        },
                        new ColumnSort
                        {
                            ColumnKey = TypeHelper<UserTaskInList>.GetMemberName(t => t.DaysAfterExpire),
                            Order = ColumnSort.SortOrder.Desc
                        },
                        new ColumnSort
                        {
                            ColumnKey = TypeHelper<UserTaskInList>.GetMemberName(t => t.ActualCompleteDate),
                            Order = ColumnSort.SortOrder.Asc
                        }
                    };
        }

        /// <summary>
        /// заменить Column DocumentPeriodName -> DocumentPeriodId
        /// </summary>
        /// <param name="queryContext"></param>
        private static void FieldSubstitution(QueryConditions queryContext)
        {
            queryContext.Filter.SingleOrDefault(query =>
                query.ColumnName == TypeHelper<UserTaskInList>.GetMemberName(t => t.DocumentPeriodName)).
                IfNotNull(query =>
                {
                    query.ColumnName = TypeHelper<UserTaskInList>.GetMemberName(t => t.DocumentPeriodId);
                    //query.ColumnType = typeof(int);
                });


            queryContext.Sorting.SingleOrDefault(query =>
                query.ColumnKey == TypeHelper<UserTaskInList>.GetMemberName(t => t.DocumentPeriodName)).
                IfNotNull(query =>
                    query.ColumnKey = TypeHelper<UserTaskInList>.GetMemberName(t => t.DocumentPeriodId));
        }

        /// <summary>
        /// Добавляет необходимые условия для сортировки.фильтрации
        /// по статусу "Просрочено"
        /// </summary>
        /// <param name="queryContext"></param>
        private static void AdditionalStatusConditions(QueryConditions queryContext)
        {
            var ststusSorting = queryContext.Sorting.SingleOrDefault(query =>
                    query.ColumnKey == TypeHelper<UserTaskInList>.GetMemberName(t => t.Status));

            if (ststusSorting != null)
            {
                queryContext.Sorting.Add(new ColumnSort()
                {
                    ColumnKey = TypeHelper<UserTaskInList>.GetMemberName(t => t.PlannedCompleteDate),
                    Order = ststusSorting.Order == ColumnSort.SortOrder.Asc ? ColumnSort.SortOrder.Asc : ColumnSort.SortOrder.Desc
                });
            }

            var statusFilter = queryContext.Filter.SingleOrDefault(query =>
                    query.ColumnName == TypeHelper<UserTaskInList>.GetMemberName(t => t.Status));

            if (statusFilter != null)
            {
                var filteringConditionOverdue = statusFilter.Filtering.SingleOrDefault(v => (int) v.Value == StatusOverdueId);
                var filteringConditionCreated = statusFilter.Filtering.SingleOrDefault(v => (int)v.Value == StatusCreatedId);
                if (filteringConditionOverdue != null)
                {
                    filteringConditionOverdue.Value = StatusCreatedId;
                    queryContext.Filter.Add(
                    new FilterQuery
                    {
                        ColumnName = TypeHelper<UserTaskInList>.GetMemberName(t => t.PlannedCompleteDate),
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering =
                            new List<ColumnFilter>
                            {
                                new ColumnFilter
                                {
                                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThan,
                                    Value = DateTime.UtcNow.Date
                                }
                            }
                    });
                }
                if (filteringConditionCreated != null)
                {
                   queryContext.Filter.Add(
                   new FilterQuery
                   {
                       ColumnName = TypeHelper<UserTaskInList>.GetMemberName(t => t.PlannedCompleteDate),
                       FilterOperator = FilterQuery.FilterLogicalOperator.And,
                       Filtering =
                           new List<ColumnFilter>
                           {
                                new ColumnFilter
                                {
                                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                                    Value = DateTime.UtcNow.Date
                                }
                           }
                   });
                }
            }
        }

        /// <summary>
        /// данные по карточке пз
        /// </summary>
        /// <returns></returns>
        public OperationResult<UserTaskInDetails> GetCardData(
            long userTaskId,
            int taskTypeId,
            long declarationZip
            )
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var dataAdapter = UserTaskAdapterCreator.Create(_helper, connection);

                        var userTaskInDetails = dataAdapter.GetUserTaskDetails(userTaskId, taskTypeId, declarationZip);
                        userTaskInDetails.ActorList = dataAdapter.GetActorList(userTaskId);
                        userTaskInDetails.TaxPayerSolvencyList = dataAdapter.GetTaxPayerSolvencyList();

                        return userTaskInDetails;
                    }
                });
        }

        /// <summary>
        ///     Выставить статус платежеспособности
        /// </summary>
        public OperationResult SetSolvency(long taskId, long solvencyStatusId, string comment)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var dataAdapter = UserTaskAdapterCreator.Create(_helper, connection);
                        dataAdapter.SetSolvency(taskId, solvencyStatusId, comment);
                    }
                });
        }


        /// <summary>
        /// Список всех возможных типов ПЗ
        /// </summary>
        /// <returns></returns>
        public OperationResult<List<DictionaryItem>> GetUserTaskTypeDictionary()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var dataAdapter = UserTaskAdapterCreator.CreateUserTaskTypeAdapter(_helper, connection);

                        return dataAdapter.All();
                    }
                });
        }

        /// <summary>
        /// Список всех возможных статусов ПЗ
        /// </summary>
        /// <returns></returns>
        public OperationResult<List<DictionaryItem>> GetUserTaskStatusDictionary()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var dataAdapter = UserTaskAdapterCreator.CreateUserTaskStatusAdapter(_helper, connection);

                        return dataAdapter.All();
                    }
                });
        }

        public OperationResult CompleteReclaimReplyTask(long explainId)
        {
            return _helper.Do(
                () =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var dataAdapter = UserTaskAdapterCreator.UpdateStatusAdapter(_helper, connection);

                     dataAdapter.CompleteReclaimReplyTask(explainId);
                }
            });
        }
    }
}
