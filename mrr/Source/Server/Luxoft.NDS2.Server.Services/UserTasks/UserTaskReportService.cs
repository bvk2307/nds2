﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.UserTask;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.OperationContext;
using Luxoft.NDS2.Server.DAL.UserTask;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Server.Services.UserAccess;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.UserTasks
{
    [CommunicationContract(typeof(IUserTaskReportService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class UserTaskReportService : IUserTaskReportService
    {
        private readonly ServiceHelpers _helper;

        private static string[] Operations = new string[]
        {
            MrrOperations.UserTask.UserTaskReportByCountry,
            MrrOperations.UserTask.UserTaskReportByInspections,
            MrrOperations.UserTask.UserTaskReportByInspector,
            MrrOperations.UserTask.UserTaskReportByRegion,
            MrrOperations.UserTask.UserTaskReportByRegionInspection
        };

        public UserTaskReportService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<AccessContext> GetAccesContext()
        {
            return _helper.Do(
                () => 
                {
                    return _helper.GetAccessContext(Operations);
                });
        }

        public OperationResult<UserTaskInspectorSummary[]> ReportByUsers(
            long reportId, 
            string[] userSids,
            int[] taskTypes)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var filter =
                            UserReportServiceHelper.FilterExpression(reportId, taskTypes)
                                .WithExpression(
                                    FilterExpressionCreator.CreateInList(
                                        TypeHelper<UserTaskInspectorSummary>.GetMemberName(x => x.UserSid),
                                        userSids));
                                
                        var dataAdapter = UserTaskAdapterCreator.ReportByInspectorAdapter(_helper, connection);

                        return dataAdapter.Select(filter);
                    }
                });
        }

        public OperationResult<UserTaskRegionSummary[]> ReportByRegion(
            long reportId, 
            string[] regionCodes,
            int[] taskTypes)
        {

            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var filter = UserReportServiceHelper.FilterExpression(reportId, taskTypes);

                        if (regionCodes.Any())
                        {
                            filter.WithExpression(
                                FilterExpressionCreator.CreateInList(
                                    TypeHelper<UserTaskRegionSummary>.GetMemberName(x => x.RegionCode),
                                    regionCodes));
                        }

                        var dataAdapter = UserTaskAdapterCreator.ReportByRegionAdapter(_helper, connection);
                        return dataAdapter.Select(filter);
                    }
                });
        }

        public OperationResult<UserTaskSonoSummary[]> ReportBySono(
            long reportId, 
            string[] regionCodes, 
            int[] taskTypes)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var filter = UserReportServiceHelper.FilterExpression(reportId, taskTypes);

                        if (regionCodes.Any())
                        {
                            filter.WithExpression(
                                FilterExpressionCreator.CreateInList(
                                    TypeHelper<UserTaskSonoSummary>.GetMemberName(x => x.RegionCode),
                                    regionCodes));
                        }

                        var dataAdapter = UserTaskAdapterCreator.ReportBySonoAdapter(_helper, connection);

                        return dataAdapter.Select(filter);
                    }
                });
        }

        public OperationResult<ReportIdSearchResult> GetReportId(DateTime reportDate)
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return UserTaskAdapterCreator
                            .ReportAdapter(_helper, connection)
                            .FindByDate(reportDate);
                    }
                });
        }
    }

    public static class UserReportServiceHelper
    {
        public static FilterExpressionGroup FilterExpression(long reportId, int[] taskTypes)
        {
            var group =
                FilterExpressionCreator.CreateGroup()
                    .WithExpression(
                        FilterExpressionCreator.Create(
                            TypeHelper<UserTaskSummary>.GetMemberName(x => x.ReportId),
                            ColumnFilter.FilterComparisionOperator.Equals,
                            reportId));

            if (taskTypes.Any())
            {
                group.WithExpression(
                    FilterExpressionCreator.CreateInList(
                        TypeHelper<UserTaskInspectorSummary>.GetMemberName(x => x.TaskType),
                        taskTypes));
            }

            return group;
        }
    }
}
