﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Linq.Expressions;

namespace Luxoft.NDS2.Server.Services.SortOptimization
{
    public class ColumnPair<T> where T : class
    {
        public ColumnPair(string initialColumn)
        {
            InitialColumn = initialColumn;
        }

        public string InitialColumn
        {
            get;
            private set;
        }

        public IList<string> ColumnsToReplace {
            get;
            private set;
        }

        public void ReplaceWith<TV>(IEnumerable<Expression<Func<T, TV>>> expressions)
        {
            ColumnsToReplace = new List<string>(expressions.Count());
            foreach (var expression in expressions)
            {
                ColumnsToReplace.Add(TypeHelper<T>.GetMemberName(expression));
            }
        }
    }

    public class SortColumnReplacer<T> : ISortOptimizer
        where T : class
    {
        private readonly List<ColumnPair<T>> _pairs = new List<ColumnPair<T>>();

        public void Optimize(List<ColumnSort> originalColumnsList)
        {
            var originalColumnsListTemp = new List<ColumnSort>(originalColumnsList);

            foreach (var columnSort in originalColumnsListTemp)
            {
                var pair = _pairs.FirstOrDefault(p => p.InitialColumn == columnSort.ColumnKey);

                if (pair != null)
                {
                    columnSort.ColumnKey = pair.ColumnsToReplace[0];
                    if (pair.ColumnsToReplace.Count > 1)
                    {
                        for (var columnCounter = 1; columnCounter < pair.ColumnsToReplace.Count(); columnCounter++)
                        {
                            var columnSortNew = columnSort.Clone() as ColumnSort;
                            columnSortNew.ColumnKey = pair.ColumnsToReplace[columnCounter];
                            originalColumnsList.Add(columnSortNew);
                        }
                    }
                    
                }
            }
        }

        public ColumnPair<T> WhenSortingBy<TN>(Expression<Func<T, TN>> expression)
        {
            var pair = new ColumnPair<T>(TypeHelper<T>.GetMemberName(expression));
            _pairs.Add(pair);
            return pair;
        }
    }
}
