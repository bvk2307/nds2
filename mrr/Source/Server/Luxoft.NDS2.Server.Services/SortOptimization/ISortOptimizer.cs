﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.SortOptimization
{
    public interface ISortOptimizer
    {
        void Optimize(List<ColumnSort> originalColumnsList);
    }
}
