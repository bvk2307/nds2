﻿using System;
using System.Collections.Generic;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IControlRatioService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public sealed class ControlRatioService : IControlRatioService
    {
        private readonly ServiceHelpers _helper;
        public ControlRatioService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<DiscrepancyDocumentInfo> GetDocument(long ratioId)
        {
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.ControlRatio(_helper).GetDocumentInfo(ratioId), _helper);
        }


        public OperationResult<DeclarationSummary> GetDeclarationVersion(long? declarationId)
        {
            return GetDeclaration(declarationId, () => TableAdapterCreator.Declaration(_helper).Version(declarationId.Value));
        }

        private OperationResult<DeclarationSummary> GetDeclaration(long? id, Func<DeclarationSummary> func)
        {
            if (!id.HasValue)
            {
                return new OperationResult<DeclarationSummary> { Result = new DeclarationSummary(), Status = ResultStatus.Success };
            }
            return DalHelper.ExecuteOperation(func, _helper);
        }

        public OperationResult UpdateComment(long id, string comment)
        {
            return DalHelper.Execute(() => TableAdapterCreator.ControlRatio(_helper).UpdateComment(id, comment), _helper);
        }

        public OperationResult<ControlRatio> GetControlRatio(long id)
        {
            return DalHelper.ExecuteOperation(() => TableAdapterCreator.ControlRatio(_helper).GetControlRatio(id), _helper);
        }
    }
}