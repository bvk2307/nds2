﻿using System;
using System.Collections.Generic;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(ICalendarDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class CalendarDataService : ICalendarDataService
    {
        private readonly ServiceHelpers _helper;

        public CalendarDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<List<DateTime>> GetRedDays(DateTime begin, DateTime end)
        {
            return DalHelper.Execute(() => TableAdapterCreator.Calendar(_helper).GetRedDays(begin, end), _helper);
        }

        public void AddRedDay(DateTime date)
        {
            DalHelper.Execute(() => TableAdapterCreator.Calendar(_helper).AddRedDay(date), _helper);
        }

        public void RemoveRedDay(DateTime date)
        {
            DalHelper.Execute(() => TableAdapterCreator.Calendar(_helper).RemoveRedDay(date), _helper);
        }

        public OperationResult<DateTime> AddWorkingDays(DateTime date, int daysToShift)
        {
            return _helper.DoEx(() => TableAdapterCreator.Calendar(_helper).AddWorkingDays(date, daysToShift));
        }
    }
}
