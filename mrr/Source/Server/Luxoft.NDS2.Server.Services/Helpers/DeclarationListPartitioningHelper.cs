﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Server.Services.Helpers.Declarations;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    static class DeclarationListPartitioningHelper
    {
        #region Map period_code to quarter

        public static readonly Dictionary<string, int> Periods = new Dictionary<string, int>
            {
                {"1", 1},
                {"2", 2},
                {"3", 3},
                {"4", 4},

                {"21", 1},
                {"22", 2},
                {"23", 3},
                {"24", 4},

                {"51", 1},
                {"54", 2},
                {"55", 3},
                {"56", 4},

                {"01", 1},
                {"02", 1},
                {"03", 1},
                {"04", 2},
                {"05", 2},
                {"06", 2},
                {"07", 3},
                {"08", 3},
                {"09", 3},
                {"10", 4},
                {"11", 4},
                {"12", 4},

                {"71", 1},
                {"72", 1},
                {"73", 1},
                {"74", 2},
                {"75", 2},
                {"76", 2},
                {"77", 3},
                {"78", 3},
                {"79", 3},
                {"80", 4},
                {"81", 4},
                {"82", 4},
            };

        #endregion
        
        public static QueryConditions TransformConditions(QueryConditions conditions, RegionGroupCache cache)
        {
            
            #region period partition

            var yearKey = TypeHelper<DeclarationSummary>.GetMemberName(x => x.FISCAL_YEAR);
            var periodKey = TypeHelper<DeclarationSummary>.GetMemberName(x => x.TAX_PERIOD);

            var yf = conditions.Filter
                .Where(
                q => q.ColumnName == yearKey &&
                    q.Filtering.Any(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                    ).ToArray();
            var pf = conditions.Filter
                .Where(
                q => q.ColumnName == periodKey &&
                    q.Filtering.Any(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                    ).ToArray();

            var years = yf
                .SelectMany(q => q.Filtering.Where(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                        .Select(f => f.Value)).ToArray();
            var periodCodes = pf
                .SelectMany(q => q.Filtering.Where(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                        .Select(f => f.Value)).ToArray();

            var periods = new List<int>();
            foreach (var code in periodCodes)
            {
                int p;
                if (Periods.TryGetValue(code.ToString(), out p) && !periods.Contains(p))
                {
                    periods.Add(p);
                }
            }

            foreach (var y in years)
            {
                foreach (var p in periods)
                {
                    var v = long.Parse(string.Format("{0}{1}", y, p));
                    var fc = new FilterQuery
                    {
                        ColumnName = "partition_id",
                        ColumnType = typeof(long)
                    };
                    fc.Filtering.Add(new ColumnFilter
                    {
                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                        Value = v
                    });
                    conditions.Filter.Add(fc);
                }
            }

            foreach (var f in yf)
            {
                conditions.Filter.Remove(f);
            }
            foreach (var p in pf)
            {
                conditions.Filter.Remove(p);
            }

            #endregion

            var regionKey = TypeHelper<DeclarationSummary>.GetMemberName(x => x.REGION_CODE);
            var sonoKey = TypeHelper<DeclarationSummary>.GetMemberName(x => x.SOUN_CODE);

            var rf = conditions.Filter
                .Where(
                q => q.ColumnName == regionKey &&
                    q.Filtering.Any(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                    ).ToArray();
            var sf = conditions.Filter
                .Where(
                q => q.ColumnName == sonoKey &&
                    q.Filtering.Any(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                    ).ToArray();

            var regions = rf
                .SelectMany(q => q.Filtering.Where(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                        .Select(f => f.Value.ToString())).ToList();
            regions.AddRange(sf
                .SelectMany(q => q.Filtering.Where(f => f.ComparisonOperator == ColumnFilter.FilterComparisionOperator.Equals)
                        .Select(f => f.Value.ToString().Substring(0, 2)))
                );

            foreach (var region in regions.Distinct())
            {
                var fc = new FilterQuery
                {
                    ColumnName = "subpartition_id",
                    ColumnType = typeof(long)
                };
                fc.Filtering.Add(new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = cache.Get(region)
                });
                conditions.Filter.Add(fc);
            }

            return conditions;
        }
    }
}
