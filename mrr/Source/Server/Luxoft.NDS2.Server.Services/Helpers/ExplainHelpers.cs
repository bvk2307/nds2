﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainHelpers
    {
        public static string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                ret = string.Format("{0:00}.{1:00}.{2:0000}", dt.Value.Day, dt.Value.Month, dt.Value.Year);
            }
            return ret;
        }

        public static string GenerateUnikFileName()
        {
            return Guid.NewGuid().ToString().Replace("-", String.Empty);
        }

        public static string GenerateUnikFileId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
