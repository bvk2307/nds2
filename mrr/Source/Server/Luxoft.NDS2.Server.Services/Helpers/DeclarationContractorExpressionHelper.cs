﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class DeclarationContractorExpressionHelper
    {
        public static FilterExpressionGroup CreateExpressionForContractorList(
            string inn,
            string innReorganized,
            string kppEffective,
            string year,
            string period,
            int typeCode,
            int chapter,
            QueryConditions conditions)
        {
            CheckArguments(inn, year, period, chapter, conditions);

            var chapterNumber = (ContractorChapterNumber)chapter;

            var mappingQuantity = new Dictionary<ContractorChapterNumber, string>()
            {
                { ContractorChapterNumber.Chapter8, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER8_COUNT) },
                { ContractorChapterNumber.Chapter9, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER9_COUNT_EFFECTIVE) },
                { ContractorChapterNumber.Chapter10, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER10_COUNT) },
                { ContractorChapterNumber.Chapter11, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER11_COUNT) },
                { ContractorChapterNumber.Chapter12, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER12_COUNT) }
            };

            var searchCriteria = conditions.Filter.ToFilterGroup();


            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    mappingQuantity[chapterNumber],
                    ColumnFilter.FilterComparisionOperator.GreaterThan,
                    0)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.INN_DECLARANT),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    inn)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.INN_REORGANIZED),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    innReorganized)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.KPP_EFFECTIVE),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    kppEffective)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.FISCAL_YEAR),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    year)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.PERIOD_CODE),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    period)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.DOC_TYPE_CODE),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    typeCode)
                    );

            return searchCriteria;
        }



        public static FilterExpressionGroup CreateExpressionForContractorParams(
           string inn,
           string innReorganized,
           string kppEffective,
           string year,
           string period,
           int typeCode,
           string contractorInn,
           int chapter,
           QueryConditions conditions)
        {
            CheckArguments(inn, year, period, chapter, conditions);

            var searchCriteria = conditions.Filter.ToFilterGroup();

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.INN_DECLARANT),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    inn)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.INN_REORGANIZED),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    innReorganized)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.KPP_EFFECTIVE),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    kppEffective)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.FISCAL_YEAR),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    year)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.PERIOD_CODE),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    period)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.CONTRACTOR_INN),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    contractorInn)
                    );

            searchCriteria.WithExpression(
                FilterExpressionCreator.Create(
                    TypeHelper<ContragentParamsSummary>.GetMemberName(x => x.CHAPTER),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    chapter)
                    );

            return searchCriteria;
        }

        private static void CheckArguments(string inn, string year, string period, int chapter, QueryConditions conditions)
        {
            if (String.IsNullOrWhiteSpace(inn))
                throw new ArgumentNullException("inn", "ИНН не может быть пустым.");
            if (String.IsNullOrWhiteSpace(year))
                throw new ArgumentNullException("year", "Год отчетного периода не может быть пустым.");
            if (String.IsNullOrWhiteSpace(period))
                throw new ArgumentNullException("period", "Код отчетного периода не может быть пустым.");
            if (!Enum.IsDefined(typeof(ContractorChapterNumber), chapter))
                throw new ArgumentOutOfRangeException("chapter", "Значение не входит в диапазон ContractorChapterNumber");
            if (conditions == null)
                throw new ArgumentNullException("conditions", "Фильтр для поиска пустой");
        }
    }
}
