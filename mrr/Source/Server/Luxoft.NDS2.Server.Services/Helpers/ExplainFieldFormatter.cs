﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainFieldFormatter
    {
        private List<string> _fieldOfDates = new List<string>();

        public ExplainFieldFormatter()
        {
            Init();
        }

        private void Init()
        {
            _fieldOfDates.Clear();
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CREATE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIVE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INVOICE_DATE));
        }

        public string GetValue(string fieldName, string value)
        {
            string retValue = value;

            if (_fieldOfDates.Any(p => p == fieldName))
            {
                var dateTimeValue = ExplainParser.ParseDataTime(value);
                if (dateTimeValue != null)
                    retValue = FormatDateTime(dateTimeValue);
            }

            return retValue;
        }

        public string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                ret = string.Format("{0:00}.{1:00}.{2:0000}", dt.Value.Day, dt.Value.Month, dt.Value.Year);
            }
            return ret;
        }

        public string FormatDecimal(decimal? value)
        {
            string ret = String.Empty;
            if (value != null)
            {
                ret = value.Value.ToString(CultureInfo.GetCultureInfo("Ru-ru"));
            }
            return ret;
        }
    }
}
