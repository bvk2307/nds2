﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class ZipHelper
    {
        public static void CreateZip(ZipParameter zipParameter, string filePathZip)
        {
            var zipParameters = new List<ZipParameter>();
            zipParameters.Add(zipParameter);
            CreateZip(zipParameters, filePathZip);
        }

        public static void CreateZip(List<ZipParameter> zipParameters, string filePathZip)
        {
            using (ZipOutputStream zipOutStream = new ZipOutputStream(File.Create(filePathZip)))
            {
                byte[] buffer = new byte[4096];
                foreach (var itemZipParameter in zipParameters)
                {
                    string fileNameIntoZip = itemZipParameter.fileNameIntoZip;
                    if (string.IsNullOrWhiteSpace(fileNameIntoZip))
                        fileNameIntoZip = Path.GetFileName(itemZipParameter.filePath);
                    ZipEntry entry = new ZipEntry(fileNameIntoZip);
                    zipOutStream.PutNextEntry(entry);
                    using (FileStream fileStreamBody = File.OpenRead(itemZipParameter.filePath))
                    {
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fileStreamBody.Read(buffer, 0, buffer.Length);
                            zipOutStream.Write(buffer, 0, sourceBytes);
                        }
                        while (sourceBytes > 0);
                    }
                }
                zipOutStream.Finish();
                zipOutStream.Close();
            }
        }
    }

    public class ZipParameter
    {
        public string filePath { get; set; }
        public string fileNameIntoZip { get; set; }
    }
}
