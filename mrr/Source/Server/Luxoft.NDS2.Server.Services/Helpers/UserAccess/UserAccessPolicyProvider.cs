﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess
{
    /// <summary>
    /// Клас - создатель объекта ограничений прав для текущего пользователя
    /// </summary>
    public class UserAccessPolicyProvider
    {
        #region Private DTO struct

        private struct CSUDOperationSonoCodes
        {
            public long OperationID;
            public string OperationName;
            public IList<string> SONOCodes;
        }

        #endregion

        #region Private fields

        private IAuthorizationProvider _authProvider;
        private ICsudOperationAdapter _csudOperationAdapter;
        private ICsudOperationSonoAdapter _csudOperationSonoAdapter;
        private string _fullAccessMark = "****";

        #endregion

        public UserAccessPolicyProvider(IAuthorizationProvider AuthProvider,
                                 ICsudOperationAdapter csudOperationAdapter,
                                 ICsudOperationSonoAdapter csudOperationSonoAdapter)
        {
            Contract.Assert(AuthProvider != null);
            Contract.Assert(csudOperationAdapter != null);
            Contract.Assert(csudOperationSonoAdapter != null);

            _authProvider = AuthProvider;
            _csudOperationAdapter = csudOperationAdapter;
            _csudOperationSonoAdapter = csudOperationSonoAdapter;
        }

        public UserAccessPolicy GetPermissions(string Operation)
        {
            var ret = new UserAccessPolicy();
            
            //1. получить список операций CSUD из БД
            var operationsCSUD = _csudOperationAdapter.Search(Operation);

            if (!operationsCSUD.IsNullOrEmpty()) // если операция зарегистрирована в бд
            {
                ret.FullAccess = false;

                //2. по списку CSUD получаем их соно-коды из CSUD
                var CSUDSONO = new List<CSUDOperationSonoCodes>();
                foreach (var oprationCSUDItem in operationsCSUD)
                {
                    var sonoCodes = _authProvider.GetUserStructContexts(PermissionType.Operation, oprationCSUDItem.Name);

                    if (!sonoCodes.IsNullOrEmpty())
                        CSUDSONO.Add(new CSUDOperationSonoCodes()
                        {
                            OperationID = oprationCSUDItem.ID,
                            OperationName = oprationCSUDItem.Name,
                            SONOCodes = sonoCodes
                        });
                }

                //3. по списку оперция-сонокод - подгружаем подгружаем ограничения по входящим в них инспекциям
                var resultSONOCodes = new List<string>();
                foreach (var CSUDSONOItem in CSUDSONO)
                {
                    foreach (var SONOCodesItem in CSUDSONOItem.SONOCodes)
                    {
                        var childSONO =
                            _csudOperationSonoAdapter.Search(CSUDSONOItem.OperationID, SONOCodesItem);

                        if (childSONO.Contains(_fullAccessMark))
                        {
                            ret.FullAccess = true;
                            resultSONOCodes.Clear();
                            break;
                        }
                        else
                        {
                            resultSONOCodes.AddRange(childSONO);
                        }
                    }

                    if (ret.FullAccess)
                        break;
                }

                ret.AllowedInspections.AddRange(resultSONOCodes.Distinct().ToList());
            }
            else // если операция незарегистрирована в бд
            {
                ret.FullAccess = _authProvider.GetUserStructContexts(PermissionType.Operation, Operation).Any();
            }


            return ret;
        }
    }
}
