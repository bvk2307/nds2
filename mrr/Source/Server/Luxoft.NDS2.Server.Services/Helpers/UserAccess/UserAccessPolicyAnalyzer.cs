﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess
{
    /// <summary> A wrapper of <see cref="UserAccessPolicyProvider.GetPermissions"/>(). </summary>
    internal sealed class UserAccessPolicyAnalyzer
    {
        private UserAccessPolicyProvider _userAccessPolicyProvider;

        internal UserAccessPolicyAnalyzer( 
            IAuthorizationProvider authorizationProvider, ICsudOperationAdapter csudOperationAdapter, ICsudOperationSonoAdapter csudOperationSonoAdapter )
        {
            Contract.Assert( authorizationProvider != null );
            Contract.Assert( csudOperationAdapter != null );
            Contract.Assert( csudOperationSonoAdapter != null );
            Contract.Ensures( _userAccessPolicyProvider != null );

            _userAccessPolicyProvider = new UserAccessPolicyProvider( authorizationProvider, csudOperationAdapter, csudOperationSonoAdapter );
        }

        /// <summary> Is operation eligible for "soun" code <paramref name="sounCode"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        public bool IsOperationEligibleForSounCodes( string operationIdentifier, string sounCode )
        {
            Contract.Requires( !string.IsNullOrEmpty( operationIdentifier ) );

            UserAccessPolicy restrictions = _userAccessPolicyProvider.GetPermissions( operationIdentifier );
            bool isEligible = restrictions.FullAccess;
            if ( !isEligible )
            {
                Contract.Assume( !string.IsNullOrEmpty( sounCode ) );
                
                isEligible = restrictions.AllowedInspections.Contains( sounCode );
            }

            return isEligible;
        }

        /// <summary> Is operation eligible for some "soun" code from defined <paramref name="sounCodes"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCodes"></param>
        /// <returns></returns>
        public UserPermissionRequestResult IsOperationEligibleForSounCodes( string operationIdentifier, IEnumerable<string> sounCodes )
        {
            Contract.Requires( !string.IsNullOrEmpty( operationIdentifier ) );

            UserAccessPolicy restrictions = _userAccessPolicyProvider.GetPermissions( operationIdentifier );
            UserPermissionRequestResult permissionResult = restrictions.FullAccess 
                                        ? UserPermissionRequestResult.Granted : UserPermissionRequestResult.NoData;
            if ( permissionResult != UserPermissionRequestResult.Granted )
            {
                foreach ( string sounCode in sounCodes )
                {
                    Contract.Assert( !string.IsNullOrEmpty( sounCode ) );

                    if ( restrictions.AllowedInspections.Contains( sounCode ) )
                    {
                        permissionResult = UserPermissionRequestResult.Granted;
                        break;
                    }
                    if ( permissionResult == UserPermissionRequestResult.NoData )
                        permissionResult = UserPermissionRequestResult.Denied;
                }
            }
            return permissionResult;
        }
    }
}