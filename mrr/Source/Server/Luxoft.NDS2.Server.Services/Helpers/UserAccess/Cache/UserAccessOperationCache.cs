﻿using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.CFG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache
{
    public class UserAccessOperationCache : ICsudOperationAdapter, ICsudOperationSonoAdapter 
    {
        private static UserAccessOperationCache _instance;
        private Dictionary<string, List<CsudOperation>> _csudOperations = null;
        private Dictionary<long, List<CsudOperationSono>> _csudOperationsSono = null;
        private DateTime? _csudOperationLastLoading;
        private DateTime? _csudOperationSonoLastLoading;
        private int MAX_LIFE_CACHE_DAYS = 8;
        private ICsudOperationSonoAllAdapter _csudOperationSonoAdapter;
        private ICsudOperationAllAdapter _csudOperationAdapter;

        private UserAccessOperationCache(Luxoft.NDS2.Server.DAL.IServiceProvider service, 
            ConnectionFactoryBase connectionFactory)
        {
            _csudOperations = new Dictionary<string, List<CsudOperation>>();
            _csudOperationsSono = new Dictionary<long, List<CsudOperationSono>>();
            _csudOperationSonoAdapter = CfgAdaptersCreator.CsudOperationSonoAllAdapter(service, connectionFactory);
            _csudOperationAdapter = CfgAdaptersCreator.CsudOperationAllAdapter(service, connectionFactory);
        }

        public static UserAccessOperationCache Instance(
            Luxoft.NDS2.Server.DAL.IServiceProvider service, 
            ConnectionFactoryBase connectionFactory)
        {
            if (_instance == null)
            {
                _instance = new UserAccessOperationCache(service, connectionFactory);
            }
            return _instance;
        }
    
        public List<string> Search(long id, string sononCode)
        {
            if (!_csudOperationSonoLastLoading.HasValue ||
                (_csudOperationSonoLastLoading.HasValue &&
                (DateTime.Now - _csudOperationSonoLastLoading.Value).TotalHours >= MAX_LIFE_CACHE_DAYS))
            {
                _csudOperationsSono.Clear();
                foreach (var item in _csudOperationSonoAdapter.All().GroupBy(p => p.OpearationId))
                {
                    _csudOperationsSono.Add(item.Key, item.ToList());
                }
                _csudOperationSonoLastLoading = DateTime.Now;
            }

            var values = new List<string>();
            if (_csudOperationsSono.ContainsKey(id))
            {
                foreach (var item in _csudOperationsSono[id].Where(p => p.GroupSono == sononCode))
                {
                    values.Add(item.ChildSono);
                }
            }

            return values;
        }
    
        public List<CsudOperation> Search(string baseOperation)
        {
            if (!_csudOperationLastLoading.HasValue || 
                (_csudOperationLastLoading.HasValue &&
                (DateTime.Now - _csudOperationLastLoading.Value).TotalHours >= MAX_LIFE_CACHE_DAYS))
            {
                _csudOperations.Clear();
                foreach (var item in _csudOperationAdapter.All().GroupBy(p => p.MasterName))
                {
                    _csudOperations.Add(item.Key, item.ToList());
                }
                _csudOperationLastLoading = DateTime.Now;
            }

            var values = new List<CsudOperation>();
            if (_csudOperations.ContainsKey(baseOperation))
                values = _csudOperations[baseOperation];

            return values;
        }
    }
}
