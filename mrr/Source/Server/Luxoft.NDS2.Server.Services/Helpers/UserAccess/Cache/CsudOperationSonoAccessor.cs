﻿using System;
using System.Collections.Generic;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Server.Common.DTO.CFG;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache
{
    public class CsudOperationSonoAccessor : CsudOperationSono, IAccessorInitializer
    {


        public void Init(IDataItem row)
        {
            foreach (KeyValuePair<string, IProperty> keyValuePair in row.PropertyLists)
            {
                string propertyName = keyValuePair.Value.Name;
                switch (propertyName)
                {
                    case "OpearationId":
                        int? opearationId = row.GetValue<int?>( propertyName );
                        if ( opearationId.HasValue )
                            OpearationId = opearationId.Value;
                        break;
                    case "GroupSono":
                        GroupSono = row.GetValue<string>( propertyName );
                        break;
                    case "ChildSono":
                        ChildSono = row.GetValue<string>( propertyName );
                        break;
                    default:
                        throw new NotSupportedException(string.Format("Unknown property name: '{0}' for type: {1}", propertyName, GetType().FullName));
                }
            }
        }
    }
}
