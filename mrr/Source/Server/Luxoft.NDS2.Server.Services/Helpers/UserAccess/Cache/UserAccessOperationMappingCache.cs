﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.Common.DTO.CFG;
using Luxoft.NDS2.Server.DAL.CFG;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache
{
    internal class UserAccessOperationMappingCache : ICsudOperationAdapter, ICsudOperationSonoAdapter
    {
        private ICsudOperationAdapter _csudOperationAdapter;
        private ICsudOperationSonoAdapter _csudOperationSonoAdapter;
        private ServiceHelpers _helper;
        private bool _cacheDisabled;

        public UserAccessOperationMappingCache(
            ServiceHelpers serviceHelper,
            ICsudOperationAdapter csudOperationAdapter, 
            ICsudOperationSonoAdapter csudOperationSonoAdapter)
        {
            _csudOperationAdapter = csudOperationAdapter;
            _csudOperationSonoAdapter = csudOperationSonoAdapter;
            _helper = serviceHelper;

            string isClassifiersCacheDisabled = _helper.GetConfigurationValue(Constants.SERVER_CLASSIFIERS_CACHE_DISABLED_KEY);
            _cacheDisabled = isClassifiersCacheDisabled != null &&
                             bool.TryParse(isClassifiersCacheDisabled, out _cacheDisabled) && _cacheDisabled;

        }

        public List<CsudOperation> Search(string baseOperation)
        {
            if (_cacheDisabled)
                return _csudOperationAdapter.Search(baseOperation);

            IFetch<CsudOperation> cachedResult = _helper.ClassifierService.LookUp<CsudOperation, CsudOperationAccessor>(new Request("MrrCsudOperations"), "NDS2Classifiers.UAC");

            if (cachedResult.RecordCount == 0)
            {
                throw new InvalidOperationException("Данные MrrCsudOperations обязаны быть");
            }

            return new List<CsudOperation>(cachedResult.Items.Where(i => i.MasterName.Equals(baseOperation)));
        }

        public List<string> Search(long id, string sononCode)
        {
            if (_cacheDisabled)
                return _csudOperationSonoAdapter.Search(id, sononCode);

            var cachedResult = _helper.ClassifierService.LookUp<CsudOperationSono, CsudOperationSonoAccessor>(new Request("MrrCsudOperationSono"), "NDS2Classifiers.UAC");

            if (cachedResult.RecordCount == 0)
            {
                throw new InvalidOperationException("Данные MrrCsudOperationSono обязаны быть");
            }

            return new List<string>(cachedResult.Items.Where(i => i.OpearationId == id && i.GroupSono.Equals(sononCode)).Select(r=> r.ChildSono));
        }
    }
}
