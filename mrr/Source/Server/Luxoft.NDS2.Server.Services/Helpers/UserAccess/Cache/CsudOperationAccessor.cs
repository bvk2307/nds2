﻿using System;
using System.Collections.Generic;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Server.Common.DTO.CFG;

namespace Luxoft.NDS2.Server.Services.Helpers.UserAccess.Cache
{
    public class CsudOperationAccessor : CsudOperation, IAccessorInitializer
    {
        public void Init(IDataItem row)
        {
            foreach (KeyValuePair<string, IProperty> keyValuePair in row.PropertyLists)
            {
                string propertyName = keyValuePair.Value.Name;
                switch (propertyName)
                {
                    case "ID":
                        int? id = row.GetValue<int?>( propertyName );
                        if ( id.HasValue )
                            ID = id.Value;
                        break;
                    case "Name":
                        Name = row.GetValue<string>( propertyName );
                        break;
                    case "MasterName":
                        MasterName = row.GetValue<string>( propertyName );
                        break;
                    default:
                        throw new NotSupportedException(string.Format("Unknown property name: '{0}' for type: {1}", propertyName, GetType().FullName));
                }
            }
        }
    }
}
