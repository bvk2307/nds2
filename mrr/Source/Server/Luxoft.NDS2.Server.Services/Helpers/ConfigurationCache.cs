﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    class ConfigurationCache
    {
        private ConfigurationCache()
        {
            
        }

        public Configuration Cache { get; set; }

        #region Singleton

        private static volatile ConfigurationCache _instance;

        private static readonly object Lock = new object();

        public static ConfigurationCache Instance()
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                        _instance = new ConfigurationCache();
                }
            }
            return _instance;
        }

        #endregion
    }
}
