﻿using Luxoft.NDS2.Server.Services.Entities.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Server.Services.Entities.PackageDescription;
using System.Xml.Serialization;
using System.IO;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainPackageDescriptionManager
    {
        public string GenerateXML(ExplainParameters parameters, out string nameFile)
        {
            string nameFileInner = String.Empty;
            string xmlSerialize = SerializeEntity(CreateXML(parameters, out nameFileInner));
            nameFile = nameFileInner;
            return xmlSerialize;
        }

        public ТрансИнф CreateXML(ExplainParameters parameters, out string nameFile)
        {
            var entityXML = new ТрансИнф();
            nameFile = "packageDescription";

            entityXML.версияФормата = ТрансИнфВерсияФормата.ФНС10;
            entityXML.типДокументооборота = "Представление";
            entityXML.типТранзакции = "ПредставлениеНП";
            entityXML.идентификаторДокументооборота = ExplainHelpers.GenerateUnikFileName();
            entityXML.кодТипаДокументооборота = "12";
            entityXML.кодТипаТранзакции = "01";
            entityXML.ВерсПрог = parameters.FullNameVersion; 
            entityXML.отправитель = new ТрансИнфОтправитель();
            entityXML.отправитель.идентификаторСубъекта = parameters.CodeSoun;
            entityXML.отправитель.типСубъекта = "инспектор";
            entityXML.спецоператор = new ТрансИнфСпецоператор();
            entityXML.спецоператор.идентификаторСубъекта = "000";
            entityXML.спецоператор.типСубъекта = "АСК НДС-2";
            entityXML.получатель = new ТрансИнфПолучатель();
            entityXML.получатель.идентификаторСубъекта = "9900"; 
            entityXML.получатель.типСубъекта = "налоговыйОрган";

            var documents = new List<ТрансИнфДокумент>();

            var docInventory = new ТрансИнфДокумент();
            docInventory.типДокумента = "представление";
            docInventory.идентификаторДокумента = ExplainHelpers.GenerateUnikFileName();
            docInventory.типСодержимого = "xml";
            docInventory.сжат = true;
            docInventory.зашифрован = false;
            docInventory.исходноеИмяФайла = parameters.NameFileOfInventory + ".xml"; 
            docInventory.кодТипаДокумента = "01";
            docInventory.содержимое = new ТрансИнфДокументСодержимое();
            docInventory.содержимое.имяФайла = parameters.NameFileIntoZipOfInventory;
            documents.Add(docInventory);

            var docDescription = new ТрансИнфДокумент();
            docDescription.типДокумента = "описание";
            docDescription.идентификаторДокумента = ExplainHelpers.GenerateUnikFileName();
            docDescription.типСодержимого = "xml";
            docDescription.сжат = true;
            docDescription.зашифрован = false;
            docDescription.исходноеИмяФайла = parameters.NameFileOfDescription; 
            docDescription.кодТипаДокумента = "02";
            docDescription.содержимое = new ТрансИнфДокументСодержимое();
            docDescription.содержимое.имяФайла = parameters.NameFileIntoZipOfDescription;
            documents.Add(docDescription);

            var docExplain = new ТрансИнфДокумент();
            docExplain.типДокумента = "приложение";
            docExplain.идентификаторДокумента = ExplainHelpers.GenerateUnikFileName();
            docExplain.типСодержимого = "xml";
            docExplain.сжат = true;
            docExplain.зашифрован = false;
            docExplain.исходноеИмяФайла = parameters.NameFileOfExplain + ".xml"; 
            docExplain.кодТипаДокумента = "03";
            docExplain.содержимое = new ТрансИнфДокументСодержимое();
            docExplain.содержимое.имяФайла = parameters.NameFileIntoZipOfExplain;
            documents.Add(docExplain);

            entityXML.документ = documents.ToArray();

            return entityXML;
        }

        private string SerializeEntity(ТрансИнф entityXML)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ТрансИнф));
            StringBuilder sb = new StringBuilder();
            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, entityXML);
            }
            return sb.ToString();
        }
    }
}
