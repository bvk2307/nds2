﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Luxoft.NDS2.Server.Services.Entities.Inventory;
using Luxoft.NDS2.Server.Services.Entities.Parameters;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainInventoryManager
    {
        public string GenerateXMLEntity(ExplainParameters x, out string nameFile)
        {
            string nameFileInner = String.Empty;
            string xmlSerialize = SerializeEntity(CreateXMLEntity(x, out nameFileInner));
            nameFile = nameFileInner;
            return xmlSerialize;
        }

        public Entities.Inventory.Файл CreateXMLEntity(ExplainParameters x, out string nameFile)
        {
            Entities.Inventory.Файл entityXML = new Entities.Inventory.Файл();
            nameFile = String.Empty;

            DateTime docDT = DateTime.Now;
            string A = x.IdFrom; //идентификатор получателя, которому направляется файл обмена
            string K = x.IdTo; //идентификатор конечного получателя, для которого предназначена информация из данного файла обмена
            string O = string.Format("{0}{1}", x.Decl.INN, x.Decl.KPP);
            string unikFileId = ExplainHelpers.GenerateUnikFileId();
            string nameFileWithoutExt = string.Format("ON_OPDOCNO_{0}_{1}_{2}_{3:0000}{4:00}{5:00}_{6}", A, K, O, docDT.Year, docDT.Month, docDT.Day, unikFileId);
            nameFile = nameFileWithoutExt;

            entityXML.ИдФайл = nameFileWithoutExt;
            entityXML.ВерсПрог = x.Version;
            entityXML.ВерсФорм = Entities.Inventory.ФайлВерсФорм.Item503;

            ФайлДокумент Документ = new ФайлДокумент();
            entityXML.Документ = Документ;

            //----------- Документ --------------------------
            Документ.ДатаДок = ExplainHelpers.FormatDateTime(docDT);
            Документ.КНД = ФайлДокументКНД.Item1165034;

            //----- Сведения об отправителе документа
            Документ.СвОтпр = new ФайлДокументСвОтпр();
            Документ.СвОтпр.ПризОтпрУП = ФайлДокументСвОтпрПризОтпрУП.Item1;
            object СвОтпр = null;
            if (x.AskFile.Документ.СвНП.Item is Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ)
            {
                Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ item = (Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ)x.AskFile.Документ.СвНП.Item;
                СвФЛТип СвФЛТип1 = new СвФЛТип();
                СвФЛТип1.ИННФЛ = item.ИННФЛ;
                СвФЛТип1.ФИО = new ФИОТип();
                СвФЛТип1.ФИО.Фамилия = item.ФИО.Фамилия;
                СвФЛТип1.ФИО.Имя = item.ФИО.Имя;
                СвФЛТип1.ФИО.Отчество = item.ФИО.Отчество;
                СвОтпр = СвФЛТип1;
            }
            else if (x.AskFile.Документ.СвНП.Item is Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ)
            {
                Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ item = (Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ)x.AskFile.Документ.СвНП.Item;
                СвЮЛТип СвЮЛТип1 = new СвЮЛТип();
                СвЮЛТип1.ИННЮЛ = item.ИННЮЛ;
                СвЮЛТип1.КПП = item.КПП;
                СвЮЛТип1.НаимОрг = item.НаимОрг;
                СвОтпр = СвЮЛТип1;
            }
            Документ.СвОтпр.Item = СвОтпр;

            //----- Сведения о получателе документа
            Документ.СвПолуч = new ФайлДокументСвПолуч();
            Документ.СвПолуч.КодНО = x.AskFile.Документ.КодНО;

            //----- Сведения о налогоплательщике
            Документ.СвНП = new ФайлДокументСвНП();
            object СвНП = null;
            if (x.AskFile.Документ.СвНП.Item is Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ)
            {
                Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ item = (Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПФЛ)x.AskFile.Документ.СвНП.Item;
                СвФЛТип СвФЛТип1 = new СвФЛТип();
                СвФЛТип1.ИННФЛ = item.ИННФЛ;
                СвФЛТип1.ФИО = new ФИОТип();
                СвФЛТип1.ФИО.Фамилия = item.ФИО.Фамилия;
                СвФЛТип1.ФИО.Имя = item.ФИО.Имя;
                СвФЛТип1.ФИО.Отчество = item.ФИО.Отчество;
                СвНП = СвФЛТип1;
            }
            else if (x.AskFile.Документ.СвНП.Item is Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ)
            {
                Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ item = (Luxoft.NDS2.Server.Services.Entities.ФайлДокументСвНПНПЮЛ)x.AskFile.Документ.СвНП.Item;
                СвЮЛТип СвЮЛТип1 = new СвЮЛТип();
                СвЮЛТип1.ИННЮЛ = item.ИННЮЛ;
                СвЮЛТип1.КПП = item.КПП;
                СвЮЛТип1.НаимОрг = item.НаимОрг;
                СвНП = СвЮЛТип1;
            }
            Документ.СвНП.Item = СвНП;

            //----- Идентификатор файла документа, к которому формируется опись
            Документ.ИдФайлОсн = new ФайлДокументИдФайлОсн();
            Документ.ИдФайлОсн.ИмяФайлТребПояс = x.NameFileOfExplain;
            Документ.ИдФайлОсн.ItemElementName = ItemChoiceType.ИмяФайлТреб;
            Документ.ИдФайлОсн.Item = x.NameFileOfExplain;

            //----- Документы, направляемые в налоговый орган
            Документ.ДокНапрНО = new ФайлДокументДокНапрНО();
            Документ.ДокНапрНО.КолФайл = "01";
            List<ФайлДокументДокНапрНОДокФорм> ДокФормы = new List<ФайлДокументДокНапрНОДокФорм>();
            ФайлДокументДокНапрНОДокФорм ФайлДокументДокНапрНОДокФорм1 = new ФайлДокументДокНапрНОДокФорм();
            ФайлДокументДокНапрНОДокФорм1.КодДок = ФайлДокументДокНапрНОДокФормКодДок.Item8888;
            ФайлДокументДокНапрНОДокФорм1.КНД_Док = "1160200";
            //Указывается порядковый номер запрашиваемого документа, согласно требованию о представлении документов (информации).
            //Имеет вид 1.ХХ (2.ХХ), где ХХ, порядковый номер
            ФайлДокументДокНапрНОДокФорм1.ПорНомДок = "1.00";
            ФайлДокументДокНапрНОДокФорм1.ИмяФайл = x.NameFileOfExplain;
            ФайлДокументДокНапрНОДокФорм1.ИмяФайлЭЦП = "NO_SIGNATURE_FILE";
            ДокФормы.Add(ФайлДокументДокНапрНОДокФорм1);
            Документ.ДокНапрНО.ДокФорм = ДокФормы.ToArray();
            //-----------------------------------------------

            //--- Подписант
            ФайлДокументПодписант Подписант = new ФайлДокументПодписант();
            Подписант.ДолжнПодп = String.Empty;
            if (!String.IsNullOrEmpty(x.AskFile.Документ.СвНП.Тлф))
                Подписант.Тлф = x.AskFile.Документ.СвНП.Тлф;
            Подписант.ПрПодп = ФайлДокументПодписантПрПодп.Item4;
            
            ФайлДокументПодписантСвПред СвПред = new ФайлДокументПодписантСвПред();
            СвПред.НаимДок = string.IsNullOrEmpty(x.AskFile.Документ.Подписант.СвПред.НаимДок) ? "UNSIGNED" : x.AskFile.Документ.Подписант.СвПред.НаимДок;
            Подписант.СвПред = СвПред;
            
            ФИОТип ФИО = new ФИОТип();
            ФИО.Фамилия = string.IsNullOrEmpty(x.AskFile.Документ.Подписант.ФИО.Фамилия) ? "UNSIGNED" : x.AskFile.Документ.Подписант.ФИО.Фамилия;
            ФИО.Имя = string.IsNullOrEmpty(x.AskFile.Документ.Подписант.ФИО.Имя) ? "UNSIGNED" : x.AskFile.Документ.Подписант.ФИО.Имя;
            if (!string.IsNullOrEmpty(x.AskFile.Документ.Подписант.ФИО.Отчество))
                ФИО.Отчество =  x.AskFile.Документ.Подписант.ФИО.Отчество;
            Подписант.ФИО = ФИО;

            Документ.Подписант = Подписант;

            return entityXML;
        }

        private string SerializeEntity(Entities.Inventory.Файл entityXML)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Entities.Inventory.Файл));
            StringBuilder sb = new StringBuilder();
            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, entityXML);
            }
            return sb.ToString();
        }
    }
}
