﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Services.EntitiesExplain;
using Luxoft.NDS2.Server.Services.Entities.Parameters;
using ФайлВерсФорм = Luxoft.NDS2.Server.Services.EntitiesExplain.ФайлВерсФорм;
using ФайлДокумент = Luxoft.NDS2.Server.Services.EntitiesExplain.ФайлДокумент;
using ФайлДокументКНД = Luxoft.NDS2.Server.Services.EntitiesExplain.ФайлДокументКНД;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainXMLManager
    {
        private ExplainFieldFormatter _explainFieldFormatter = new ExplainFieldFormatter();

        public ExplainXMLManager()
        {
            InitData();
        }

        private void InitData()
        {
            FillOperartionBuyCode(_operationBuyCodes);
            FillOperartionSaleCode(_operationSaleCodes);
            FillOperartionR10Code(_operationR10Codes);
            FillOperartionR11Code(_operationR11Codes);
            FillDealKindR11Code(_dealKindR11Codes);
        }

        private void FillChapter08(bool priznakDL, ExplainInvoice inv,
            List<ExplainInvoiceCorrect> invoicesCorrectes,
            List<ФайлДокументСведТребКнигаПокупСоотвСтр> СведТребКнигаПокупСведСоотв,
            List<ФайлДокументСведТребКнигаПокупРасхСтр> СведТребКнигаПокупСведРасх,
            List<ФайлДокументСведТребКнигаПокупДЛСоотвСтр> СведТребКнигаПокупСведСоотвДЛ,
            List<ФайлДокументСведТребКнигаПокупДЛРасхСтр> СведТребКнигаПокупСведРасхДЛ)
        {
            //--- Подтвержденная СФ
            if (inv.State == ExplainInvoiceState.ProcessedInThisExplain)
            {
                //--- Подтверждение (исходная СФ)
                КнПокСтрТип КнПокСтрТипПодтв = GenerateInvoiceOriginalR08(inv);

                if (!priznakDL)
                {
                    ФайлДокументСведТребКнигаПокупСоотвСтр файлДокументСведТребКнигаПокупСоотвСтр = new ФайлДокументСведТребКнигаПокупСоотвСтр();
                    файлДокументСведТребКнигаПокупСоотвСтр.КнПокСтр = КнПокСтрТипПодтв;
                    файлДокументСведТребКнигаПокупСоотвСтр.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПокупСведСоотв.Add(файлДокументСведТребКнигаПокупСоотвСтр);
                }
                else
                {
                    ФайлДокументСведТребКнигаПокупДЛСоотвСтр файлДокументСведТребКнигаПокупДЛСоотвСтр = new ФайлДокументСведТребКнигаПокупДЛСоотвСтр();
                    файлДокументСведТребКнигаПокупДЛСоотвСтр.КнПокДЛСтр = КнПокСтрТипПодтв;
                    файлДокументСведТребКнигаПокупДЛСоотвСтр.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПокупСведСоотвДЛ.Add(файлДокументСведТребКнигаПокупДЛСоотвСтр);
                }
            }
            //--- Изменная СФ
            else if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                КнПокСтрТип ПоясненКнПокСтр = new КнПокСтрТип();

                //--- Расхождение (исходная СФ)
                КнПокСтрТип РасхождКнПокСтр = GenerateInvoiceOriginalR08(inv);

                if (!priznakDL)
                {
                    ФайлДокументСведТребКнигаПокупРасхСтр СведТребКнигаПокупСведРасх1 = new ФайлДокументСведТребКнигаПокупРасхСтр();
                    СведТребКнигаПокупСведРасх1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПокупСведРасх.Add(СведТребКнигаПокупСведРасх1);
                    ФайлДокументСведТребКнигаПокупРасхСтрПояснен Пояснен = new ФайлДокументСведТребКнигаПокупРасхСтрПояснен();
                    Пояснен.ТипИнф = ФайлДокументСведТребКнигаПокупРасхСтрПоясненТипИнф.Item2;
                    Пояснен.КнПокСтр = ПоясненКнПокСтр;
                    СведТребКнигаПокупСведРасх1.Пояснен = Пояснен;
                    ФайлДокументСведТребКнигаПокупРасхСтрРасхожд Расхожд1 = new ФайлДокументСведТребКнигаПокупРасхСтрРасхожд();
                    Расхожд1.ТипИнф = ФайлДокументСведТребКнигаПокупРасхСтрРасхождТипИнф.Item1;
                    Расхожд1.КнПокСтр = РасхождКнПокСтр;
                    СведТребКнигаПокупСведРасх1.Расхожд = Расхожд1;
                }
                else
                {
                    ФайлДокументСведТребКнигаПокупДЛРасхСтр СведТребКнигаПокупСведРасхДЛ1 = new ФайлДокументСведТребКнигаПокупДЛРасхСтр();
                    СведТребКнигаПокупСведРасхДЛ1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПокупСведРасхДЛ.Add(СведТребКнигаПокупСведРасхДЛ1);
                    ФайлДокументСведТребКнигаПокупДЛРасхСтрПояснен Пояснен = new ФайлДокументСведТребКнигаПокупДЛРасхСтрПояснен();
                    Пояснен.ТипИнф = ФайлДокументСведТребКнигаПокупДЛРасхСтрПоясненТипИнф.Item2;
                    Пояснен.КнПокДЛСтр = ПоясненКнПокСтр;
                    СведТребКнигаПокупСведРасхДЛ1.Пояснен = Пояснен;
                    ФайлДокументСведТребКнигаПокупДЛРасхСтрРасхожд РасхождДЛ1 = new ФайлДокументСведТребКнигаПокупДЛРасхСтрРасхожд();
                    РасхождДЛ1.ТипИнф = ФайлДокументСведТребКнигаПокупДЛРасхСтрРасхождТипИнф.Item1;
                    РасхождДЛ1.КнПокДЛСтр = РасхождКнПокСтр;
                    СведТребКнигаПокупСведРасхДЛ1.Расхожд = РасхождДЛ1;
                }

                //--- Пояснение (измененная СФ)
                ПоясненКнПокСтр.НомСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), inv.INVOICE_NUM);
                ПоясненКнПокСтр.ДатаСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), FormatDateTime(inv.INVOICE_DATE_DT));
                ПоясненКнПокСтр.НомИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_NUM), inv.CHANGE_NUM);
                ПоясненКнПокСтр.ДатаИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE), FormatDateTime(inv.CHANGE_DATE_DT));
                ПоясненКнПокСтр.НомКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_NUM), inv.CORRECTION_NUM);
                ПоясненКнПокСтр.ДатаКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE), FormatDateTime(inv.CORRECTION_DATE_DT));
                ПоясненКнПокСтр.НомИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM), inv.CHANGE_CORRECTION_NUM);
                ПоясненКнПокСтр.ДатаИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE), FormatDateTime(inv.CHANGE_CORRECTION_DATE_DT));
                ПоясненКнПокСтр.НомТД = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CUSTOMS_DECLARATION_NUM), inv.CUSTOMS_DECLARATION_NUM);
                ПоясненКнПокСтр.ОКВ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), inv.OKV_CODE);
                ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal priceBuyAmount = TryParseDecimal(invCorr.FieldValue);
                    ПоясненКнПокСтр.СтоимПокупВ = priceBuyAmount;
                    ПоясненКнПокСтр.СтоимПокупВSpecified = true;
                }
                string operationCodes = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OPERATION_CODE), inv.OPERATION_CODE);
                ПоясненКнПокСтр.КодВидОпер = GetOperationBuyCode(operationCodes);
                string receiptNumbers = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_NUM), inv.RECEIPT_DOC_NUM);
                string receiptDates = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_DATE), inv.RECEIPT_DOC_DATE);
                ПоясненКнПокСтр.ДокПдтвОпл = GenerateRecieptDocuments(receiptNumbers, receiptDates);
                string buyAcceptDates = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUY_ACCEPT_DATE), FormatDateTime(inv.BUY_ACCEPT_DATE_DT));
                ПоясненКнПокСтр.ДатаУчТов = GetAccepptDates(buyAcceptDates);
                string sellerInns = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INN), inv.SELLER_INN);
                ПоясненКнПокСтр.СвПрод = GenerateTaxpayerInfoArray(sellerInns);
                string brokerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BROKER_INN), inv.BROKER_INN);
                ПоясненКнПокСтр.СвПос = GenerateTaxpayerInfo(brokerInn);
            }
        }

        private КнПокСтрТип GenerateInvoiceOriginalR08(ExplainInvoice inv)
        {
            КнПокСтрТип КнПокСтр = new КнПокСтрТип();
            КнПокСтр.НомСчФПрод = GetNullAndNotEmptyStringValue(inv.INVOICE_NUM);
            КнПокСтр.ДатаСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.INVOICE_DATE_DT);
            КнПокСтр.НомИспрСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_NUM);
            КнПокСтр.ДатаИспрСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_DATE_DT);
            КнПокСтр.НомКСчФПрод = GetNullAndNotEmptyStringValue(inv.CORRECTION_NUM);
            КнПокСтр.ДатаКСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.CORRECTION_DATE_DT);
            КнПокСтр.НомИспрКСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_CORRECTION_NUM);
            КнПокСтр.ДатаИспрКСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_CORRECTION_DATE_DT);
            КнПокСтр.НомТД = GetNullAndNotEmptyStringValue(inv.CUSTOMS_DECLARATION_NUM);
            КнПокСтр.ОКВ = GetNullAndNotEmptyStringValue(inv.OKV_CODE);
            if (inv.PRICE_BUY_AMOUNT_DC != null)
            {
                КнПокСтр.СтоимПокупВ = (decimal)inv.PRICE_BUY_AMOUNT_DC;
                КнПокСтр.СтоимПокупВSpecified = true;
            }
            КнПокСтр.КодВидОпер = GetOperationBuyCode(inv.OPERATION_CODE);
            КнПокСтр.ДокПдтвОпл = GenerateRecieptDocuments(inv.Attributes.PaymentDocuments);
            КнПокСтр.ДатаУчТов = GetAccepptDatesOriginal(inv);
            КнПокСтр.СвПрод = GenerateTaxpayerInfoArray(inv.Attributes.Sellers);
            КнПокСтр.СвПос = GenerateTaxpayerInfo(inv.BROKER_INN);

            return КнПокСтр;
        }

        private void FillChapter09(bool priznakDL, ExplainInvoice inv,
            List<ExplainInvoiceCorrect> invoicesCorrectes,
            List<ФайлДокументСведТребКнигаПродСоотвСтр> СведТребКнигаПродСведСоотв,
            List<ФайлДокументСведТребКнигаПродРасхСтр> СведТребКнигаПродСведРасх,
            List<ФайлДокументСведТребКнигаПродДЛСоотвСтр> СведТребКнигаПродСведСоотвДЛ,
            List<ФайлДокументСведТребКнигаПродДЛРасхСтр> СведТребКнигаПродСведРасхДЛ)
        {
            //--- Подтвержденная СФ
            if (inv.State == ExplainInvoiceState.ProcessedInThisExplain)
            {
                //--- Подтверждение (исходная СФ)
                КнПродСтрТип КнПродСтрTипПодтв = GenerateInvoiceOriginalR09(inv);

                if (!priznakDL)
                {
                    ФайлДокументСведТребКнигаПродСоотвСтр файлДокументСведТребКнигаПродСоотвСтр = new ФайлДокументСведТребКнигаПродСоотвСтр();
                    файлДокументСведТребКнигаПродСоотвСтр.КнПродСтр = КнПродСтрTипПодтв;
                    файлДокументСведТребКнигаПродСоотвСтр.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПродСведСоотв.Add(файлДокументСведТребКнигаПродСоотвСтр);
                }
                else
                {
                    ФайлДокументСведТребКнигаПродДЛСоотвСтр файлДокументСведТребКнигаПродДЛСоотвСтр = new ФайлДокументСведТребКнигаПродДЛСоотвСтр();
                    файлДокументСведТребКнигаПродДЛСоотвСтр.КнПродДЛСтр = КнПродСтрTипПодтв;
                    файлДокументСведТребКнигаПродДЛСоотвСтр.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПродСведСоотвДЛ.Add(файлДокументСведТребКнигаПродДЛСоотвСтр);
                }
            }
            //--- Изменная СФ
            else if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                КнПродСтрТип ПоясненКнПродСтр = new КнПродСтрТип();

                //--- Расхождение (исходная СФ)
                КнПродСтрТип РасхождКнПродСтр = GenerateInvoiceOriginalR09(inv);

                //--- Сведения, поясняющие расхождения (ошибки, противоречия, несоответствия)
                if (!priznakDL)
                {
                    ФайлДокументСведТребКнигаПродРасхСтр СведТребКнигаПродСведРасх1 = new ФайлДокументСведТребКнигаПродРасхСтр();
                    СведТребКнигаПродСведРасх1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПродСведРасх.Add(СведТребКнигаПродСведРасх1);
                    ФайлДокументСведТребКнигаПродРасхСтрПояснен Пояснен = new ФайлДокументСведТребКнигаПродРасхСтрПояснен();
                    Пояснен.ТипИнф = ФайлДокументСведТребКнигаПродРасхСтрПоясненТипИнф.Item2;
                    Пояснен.КнПродСтр = ПоясненКнПродСтр;
                    СведТребКнигаПродСведРасх1.Пояснен = Пояснен;
                    ФайлДокументСведТребКнигаПродРасхСтрРасхожд Расхожд = new ФайлДокументСведТребКнигаПродРасхСтрРасхожд();
                    Расхожд.ТипИнф = ФайлДокументСведТребКнигаПродРасхСтрРасхождТипИнф.Item1;
                    Расхожд.КнПродСтр = РасхождКнПродСтр;
                    СведТребКнигаПродСведРасх1.Расхожд = Расхожд;
                }
                else
                {
                    ФайлДокументСведТребКнигаПродДЛРасхСтр СведТребКнигаПродСведРасхДЛ1 = new ФайлДокументСведТребКнигаПродДЛРасхСтр();
                    СведТребКнигаПродСведРасхДЛ1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                    СведТребКнигаПродСведРасхДЛ.Add(СведТребКнигаПродСведРасхДЛ1);
                    ФайлДокументСведТребКнигаПродДЛРасхСтрПояснен Пояснен = new ФайлДокументСведТребКнигаПродДЛРасхСтрПояснен();
                    Пояснен.ТипИнф = ФайлДокументСведТребКнигаПродДЛРасхСтрПоясненТипИнф.Item2;
                    Пояснен.КнПродДЛСтр = ПоясненКнПродСтр;
                    СведТребКнигаПродСведРасхДЛ1.Пояснен = Пояснен;
                    ФайлДокументСведТребКнигаПродДЛРасхСтрРасхожд Расхожд = new ФайлДокументСведТребКнигаПродДЛРасхСтрРасхожд();
                    Расхожд.ТипИнф = ФайлДокументСведТребКнигаПродДЛРасхСтрРасхождТипИнф.Item1;
                    Расхожд.КнПродДЛСтр = РасхождКнПродСтр;
                    СведТребКнигаПродСведРасхДЛ1.Расхожд = Расхожд;
                }

                //--- Пояснение (измененная СФ)
                ПоясненКнПродСтр.НомСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), inv.INVOICE_NUM); ;
                ПоясненКнПродСтр.ДатаСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), FormatDateTime(inv.INVOICE_DATE_DT));
                ПоясненКнПродСтр.НомИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_NUM), inv.CHANGE_NUM);
                ПоясненКнПродСтр.ДатаИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE), FormatDateTime(inv.CHANGE_DATE_DT));
                ПоясненКнПродСтр.НомКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_NUM), inv.CORRECTION_NUM);
                ПоясненКнПродСтр.ДатаКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE), FormatDateTime(inv.CORRECTION_DATE_DT));
                ПоясненКнПродСтр.НомИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM), inv.CHANGE_CORRECTION_NUM);
                ПоясненКнПродСтр.ДатаИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE), FormatDateTime(inv.CHANGE_CORRECTION_DATE_DT));
                ПоясненКнПродСтр.ОКВ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), inv.OKV_CODE);
                ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_IN_CURR)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal price = TryParseDecimal(invCorr.FieldValue);
                    ПоясненКнПродСтр.СтоимПродСФВ = price;
                    ПоясненКнПродСтр.СтоимПродСФВSpecified = true;
                }
                string receiptNumbers = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_NUM), inv.RECEIPT_DOC_NUM);
                string receiptDates = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_DATE), inv.RECEIPT_DOC_DATE);
                ПоясненКнПродСтр.ДокПдтвОпл = GenerateRecieptDocuments(receiptNumbers, receiptDates);
                string operationCodes = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OPERATION_CODE), inv.OPERATION_CODE);
                ПоясненКнПродСтр.КодВидОпер = GetOperationSaleCode(operationCodes);
                string buyerInns = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_INN), inv.BUYER_INN);
                ПоясненКнПродСтр.СвПокуп = GenerateTaxpayerInfoArray(buyerInns);
                string brokerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BROKER_INN), inv.BROKER_INN);
                ПоясненКнПродСтр.СвПос = GenerateTaxpayerInfo(brokerInn);
            }
        }

        private КнПродСтрТип GenerateInvoiceOriginalR09(ExplainInvoice inv)
        {
            КнПродСтрТип КнПродСтр = new КнПродСтрТип();
            КнПродСтр.НомСчФПрод = GetNullAndNotEmptyStringValue(inv.INVOICE_NUM);
            КнПродСтр.ДатаСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.INVOICE_DATE_DT);
            КнПродСтр.НомИспрСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_NUM);
            КнПродСтр.ДатаИспрСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_DATE_DT);
            КнПродСтр.НомКСчФПрод = GetNullAndNotEmptyStringValue(inv.CORRECTION_NUM);
            КнПродСтр.ДатаКСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.CORRECTION_DATE_DT);
            КнПродСтр.НомИспрКСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_CORRECTION_NUM);
            КнПродСтр.ДатаИспрКСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_CORRECTION_DATE_DT);
            КнПродСтр.ОКВ = GetNullAndNotEmptyStringValue(inv.OKV_CODE);
            if (inv.PRICE_SELL_IN_CURR_DC != null)
            {
                КнПродСтр.СтоимПродСФВ = (decimal)inv.PRICE_SELL_IN_CURR_DC;
                КнПродСтр.СтоимПродСФВSpecified = true;
            }
            КнПродСтр.ДокПдтвОпл = GenerateRecieptDocuments(inv.Attributes.PaymentDocuments);
            КнПродСтр.КодВидОпер = GetOperationSaleCode(inv.OPERATION_CODE);
            КнПродСтр.СвПокуп = GenerateTaxpayerInfoArray(inv.Attributes.Buyers);
            КнПродСтр.СвПос = GenerateTaxpayerInfo(inv.BROKER_INN);
            return КнПродСтр;
        }

        private void FillChapter10(ExplainInvoice inv,
            List<ExplainInvoiceCorrect> invoicesCorrectes,
            List<ФайлДокументСведТребЖУчВыстСчФСоотвСтр> ЖУчВыстСчФСведСоотв,
            List<ФайлДокументСведТребЖУчВыстСчФРасхСтр> ЖУчВыстСчФСведРасх)
        {
            //--- Подтвержденная СФ
            if (inv.State == ExplainInvoiceState.ProcessedInThisExplain)
            {
                ФайлДокументСведТребЖУчВыстСчФСоотвСтр ЖУчВыстСчФСведСоотв1 = new ФайлДокументСведТребЖУчВыстСчФСоотвСтр();
                ЖУчВыстСчФСведСоотв1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                ЖУчВыстСчФСведСоотв1.ЖУчВСтр = GenerateInvoiceOriginalR10(inv);
                ЖУчВыстСчФСведСоотв.Add(ЖУчВыстСчФСведСоотв1);
            }
            //--- Изменная СФ
            else if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                ФайлДокументСведТребЖУчВыстСчФРасхСтр ЖУчВыстСчФСведРасх1 = new ФайлДокументСведТребЖУчВыстСчФРасхСтр();
                ЖУчВыстСчФСведРасх1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                ЖУчВыстСчФСведРасх.Add(ЖУчВыстСчФСведРасх1);

                //--- Расхождение (исходная СФ)
                ЖУчВыстСчФСведРасх1.Расхожд = new ФайлДокументСведТребЖУчВыстСчФРасхСтрРасхожд();
                ЖУчВыстСчФСведРасх1.Расхожд.ТипИнф = ФайлДокументСведТребЖУчВыстСчФРасхСтрРасхождТипИнф.Item1;
                ЖУчВыстСчФСведРасх1.Расхожд.ЖУчВСтр = GenerateInvoiceOriginalR10(inv);

                //--- Пояснение (измененная СФ)
                ЖУчВыстСчФСведРасх1.Пояснен = new ФайлДокументСведТребЖУчВыстСчФРасхСтрПояснен();
                ЖУчВыстСчФСведРасх1.Пояснен.ТипИнф = ФайлДокументСведТребЖУчВыстСчФРасхСтрПоясненТипИнф.Item2;
                ЖУчВСтрТип ЖУчВСтр = new ЖУчВСтрТип();
                ЖУчВыстСчФСведРасх1.Пояснен.ЖУчВСтр = ЖУчВСтр;
                ЖУчВСтр.НомСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), inv.INVOICE_NUM); ;
                ЖУчВСтр.ДатаСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), FormatDateTime(inv.INVOICE_DATE_DT)); ;
                ЖУчВСтр.НомИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_NUM), inv.CHANGE_NUM); ;
                ЖУчВСтр.ДатаИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE), FormatDateTime(inv.CHANGE_DATE_DT)); ;
                ЖУчВСтр.НомКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_NUM), inv.CORRECTION_NUM); ;
                ЖУчВСтр.ДатаКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE), FormatDateTime(inv.CORRECTION_DATE_DT)); ;
                ЖУчВСтр.НомИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM), inv.CHANGE_CORRECTION_NUM); ;
                ЖУчВСтр.ДатаИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE), FormatDateTime(inv.CHANGE_CORRECTION_DATE_DT)); ;
                string operationCodes = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OPERATION_CODE), inv.OPERATION_CODE);
                ЖУчВСтр.КодВидОпер = GetOperationR10Code(operationCodes);
                string buyerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_INN), inv.BUYER_INN);
                ЖУчВСтр.СвПокуп = GenerateTaxpayerInfo(buyerInn);
                List<ЖУчВСтрТипСвПосрДеят> СвПосрДеят = new List<ЖУчВСтрТипСвПосрДеят>();
                ЖУчВСтрТипСвПосрДеят ЖУчВСтрТипСвПосрДеят1 = new ЖУчВСтрТипСвПосрДеят();
                string SELLER_AGENCY_INFO_DATE = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_DATE), FormatDateTime(inv.SELLER_AGENCY_INFO_DATE_DT));
                ЖУчВСтрТипСвПосрДеят1.ДатаСчФОтПрод = GetNullAndNotEmptyDateStringValue(SELLER_AGENCY_INFO_DATE);
                string SELLER_AGENCY_INFO_NUM = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_NUM), inv.SELLER_AGENCY_INFO_NUM);
                ЖУчВСтрТипСвПосрДеят1.НомСчФОтПрод = GetNullAndNotEmptyStringValue(SELLER_AGENCY_INFO_NUM);
                ЖУчВСтрТипСвПосрДеят1.ОКВ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), inv.OKV_CODE);
                string sellerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_INN), inv.SELLER_AGENCY_INFO_INN);
                ЖУчВСтрТипСвПосрДеят1.СвПрод = GenerateTaxpayerInfo(sellerInn);
                ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TOTAL)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal price = TryParseDecimal(invCorr.FieldValue);
                    ЖУчВСтрТипСвПосрДеят1.СтоимТовСчФВс = price;
                    ЖУчВСтрТипСвПосрДеят1.СтоимТовСчФВсSpecified = true;
                }
                else if (inv.PRICE_TOTAL_DC != null)
                {
                    ЖУчВСтрТипСвПосрДеят1.СтоимТовСчФВс = (decimal)inv.PRICE_TOTAL_DC;
                }
                СвПосрДеят.Add(ЖУчВСтрТипСвПосрДеят1);
                ЖУчВСтр.СвПосрДеят = СвПосрДеят.Any() ? СвПосрДеят.ToArray() : null;
            }
        }

        private ЖУчВСтрТип GenerateInvoiceOriginalR10(ExplainInvoice inv)
        {
            ЖУчВСтрТип ЖУчВСтр = new ЖУчВСтрТип();
            ЖУчВСтр.НомСчФПрод = GetNullAndNotEmptyStringValue(inv.INVOICE_NUM);
            ЖУчВСтр.ДатаСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.INVOICE_DATE_DT);
            ЖУчВСтр.НомИспрСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_NUM);
            ЖУчВСтр.ДатаИспрСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_DATE_DT);
            ЖУчВСтр.НомКСчФПрод = GetNullAndNotEmptyStringValue(inv.CORRECTION_NUM);
            ЖУчВСтр.ДатаКСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.CORRECTION_DATE_DT);
            ЖУчВСтр.НомИспрКСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_CORRECTION_NUM);
            ЖУчВСтр.ДатаИспрКСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_CORRECTION_DATE_DT);
            ЖУчВСтр.КодВидОпер = GetOperationR10Code(inv.OPERATION_CODE);
            ЖУчВСтр.СвПокуп = GenerateTaxpayerInfo(inv.BUYER_INN);
            List<ЖУчВСтрТипСвПосрДеят> ЖУчВыстСчФСведСоотвСвПосрДеят = new List<ЖУчВСтрТипСвПосрДеят>();
            ЖУчВСтрТипСвПосрДеят ЖУчВыстСчФСведСоотвСвПосрДеят1 = new ЖУчВСтрТипСвПосрДеят();
            ЖУчВыстСчФСведСоотвСвПосрДеят1.СвПрод = GenerateTaxpayerInfo(inv.SELLER_AGENCY_INFO_INN);
            ЖУчВыстСчФСведСоотвСвПосрДеят1.НомСчФОтПрод = GetNullAndNotEmptyStringValue(inv.SELLER_AGENCY_INFO_NUM);
            ЖУчВыстСчФСведСоотвСвПосрДеят1.ДатаСчФОтПрод = GetNullAndNotEmptyDateTimeValue(inv.SELLER_AGENCY_INFO_DATE_DT);
            ЖУчВыстСчФСведСоотвСвПосрДеят1.ОКВ = GetNullAndNotEmptyStringValue(inv.OKV_CODE);
            if (inv.PRICE_TOTAL_DC != null)
            {
                ЖУчВыстСчФСведСоотвСвПосрДеят1.СтоимТовСчФВс = (decimal)inv.PRICE_TOTAL_DC;
                ЖУчВыстСчФСведСоотвСвПосрДеят1.СтоимТовСчФВсSpecified = true;
            }
            ЖУчВыстСчФСведСоотвСвПосрДеят.Add(ЖУчВыстСчФСведСоотвСвПосрДеят1);
            ЖУчВСтр.СвПосрДеят = ЖУчВыстСчФСведСоотвСвПосрДеят.Any() ? ЖУчВыстСчФСведСоотвСвПосрДеят.ToArray() : null;
            return ЖУчВСтр;
        }

        private void FillChapter11(ExplainInvoice inv,
            List<ExplainInvoiceCorrect> invoicesCorrectes,
            List<ФайлДокументСведТребЖУчПолучСчФСоотвСтр> СведТребЖУчПолучСчФСведСоотв,
            List<ФайлДокументСведТребЖУчПолучСчФРасхСтр> СведТребЖУчПолучСчФСведРасх)
        {
            //--- Подтвержденная СФ
            if (inv.State == ExplainInvoiceState.ProcessedInThisExplain)
            {
                ФайлДокументСведТребЖУчПолучСчФСоотвСтр СведТребЖУчПолучСчФСведСоотв1 = new ФайлДокументСведТребЖУчПолучСчФСоотвСтр();
                СведТребЖУчПолучСчФСведСоотв1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                СведТребЖУчПолучСчФСведСоотв1.ЖУчПСтр = GenerateInvoiceOriginalR11(inv);
                СведТребЖУчПолучСчФСведСоотв.Add(СведТребЖУчПолучСчФСведСоотв1);
            }
            //--- Изменная СФ
            else if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                ФайлДокументСведТребЖУчПолучСчФРасхСтр СведТребЖУчПолучСчФСведРасх1 = new ФайлДокументСведТребЖУчПолучСчФРасхСтр();
                СведТребЖУчПолучСчФСведРасх1.НомерПор = GetNullAndNotEmptyDecimalStringValue(inv.ORDINAL_NUMBER);
                СведТребЖУчПолучСчФСведРасх.Add(СведТребЖУчПолучСчФСведРасх1);

                //--- Расхождение (исходная СФ)
                СведТребЖУчПолучСчФСведРасх1.Расхожд = new ФайлДокументСведТребЖУчПолучСчФРасхСтрРасхожд();
                СведТребЖУчПолучСчФСведРасх1.Расхожд.ТипИнф = ФайлДокументСведТребЖУчПолучСчФРасхСтрРасхождТипИнф.Item1;
                СведТребЖУчПолучСчФСведРасх1.Расхожд.ЖУчПСтр = GenerateInvoiceOriginalR11(inv);

                //--- Пояснение (измененная СФ)
                СведТребЖУчПолучСчФСведРасх1.Пояснен = new ФайлДокументСведТребЖУчПолучСчФРасхСтрПояснен();
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр = new ЖУчПСтрТип();
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.НомСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), inv.INVOICE_NUM);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.ДатаСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), FormatDateTime(inv.INVOICE_DATE_DT));
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.НомИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_NUM), inv.CHANGE_NUM);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.ДатаИспрСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE), FormatDateTime(inv.CHANGE_DATE_DT));
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.НомКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_NUM), inv.CORRECTION_NUM);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.ДатаКСчФПрод = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE), FormatDateTime(inv.CORRECTION_DATE_DT));
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.НомИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM), inv.CHANGE_CORRECTION_NUM);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.ДатаИспрКСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE), FormatDateTime(inv.CHANGE_CORRECTION_DATE_DT));
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.КодВидСд = GetDealKindR11Code(GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.DEAL_KIND_CODE), inv.DEAL_KIND_CODE));
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.ОКВ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), inv.OKV_CODE);
                ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TOTAL)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal price = TryParseDecimal(invCorr.FieldValue);
                    СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СтоимТовСчФВс = price;
                    СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СтоимТовСчФВсSpecified = true;
                }
                else if (inv.PRICE_TOTAL_DC != null)
                {
                    СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СтоимТовСчФВс = (decimal)inv.PRICE_TOTAL_DC;
                    СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СтоимТовСчФВсSpecified = true;
                }
                string operationCodes = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OPERATION_CODE), inv.OPERATION_CODE);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.КодВидОпер = GetOperationR11Code(operationCodes);
                string sellerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INN), inv.SELLER_INN);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СвПрод = GenerateTaxpayerInfo(sellerInn);
                string brokerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BROKER_INN), inv.BROKER_INN);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ЖУчПСтр.СвКомис = GenerateTaxpayerInfo(brokerInn);
                СведТребЖУчПолучСчФСведРасх1.Пояснен.ТипИнф = ФайлДокументСведТребЖУчПолучСчФРасхСтрПоясненТипИнф.Item2;
            }
        }

        private ЖУчПСтрТип GenerateInvoiceOriginalR11(ExplainInvoice inv)
        {
            ЖУчПСтрТип ЖУчПСтр = new ЖУчПСтрТип();
            ЖУчПСтр.НомСчФПрод = GetNullAndNotEmptyStringValue(inv.INVOICE_NUM);
            ЖУчПСтр.ДатаСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.INVOICE_DATE_DT);
            ЖУчПСтр.НомИспрСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_NUM);
            ЖУчПСтр.ДатаИспрСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_DATE_DT);
            ЖУчПСтр.НомКСчФПрод = GetNullAndNotEmptyStringValue(inv.CORRECTION_NUM);
            ЖУчПСтр.ДатаКСчФПрод = GetNullAndNotEmptyDateTimeValue(inv.CORRECTION_DATE_DT);
            ЖУчПСтр.НомИспрКСчФ = GetNullAndNotEmptyStringValue(inv.CHANGE_CORRECTION_NUM);
            ЖУчПСтр.ДатаИспрКСчФ = GetNullAndNotEmptyDateTimeValue(inv.CHANGE_CORRECTION_DATE_DT);
            ЖУчПСтр.КодВидСд = GetDealKindR11Code(inv.DEAL_KIND_CODE);
            ЖУчПСтр.ОКВ = GetNullAndNotEmptyStringValue(inv.OKV_CODE);
            if (inv.PRICE_TOTAL_DC != null)
            {
                ЖУчПСтр.СтоимТовСчФВс = (decimal)inv.PRICE_TOTAL_DC;
                ЖУчПСтр.СтоимТовСчФВсSpecified = true;
            }
            ЖУчПСтр.КодВидОпер = GetOperationR11Code(inv.OPERATION_CODE);
            ЖУчПСтр.СвПрод = GenerateTaxpayerInfo(inv.SELLER_INN);
            ЖУчПСтр.СвКомис = GenerateTaxpayerInfo(inv.BROKER_INN);
            return ЖУчПСтр;
        }
    
        private void FillChapter12(ExplainInvoice inv,
            List<ExplainInvoiceCorrect> invoicesCorrectes,
            List<ВСчФ_1735Тип> СведСоотвR12,
            List<ФайлДокументСведТребВыстСчФ_1735СведРасхСчФ> СведРасхR12)
        {
            //--- Подтвержденная СФ
            if (inv.State == ExplainInvoiceState.ProcessedInThisExplain)
            {
                СведСоотвR12.Add(GenerateInvoiceOriginalR12(inv));
            }
            //--- Изменная СФ
            else if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                ФайлДокументСведТребВыстСчФ_1735СведРасхСчФ СведРасх1 = new ФайлДокументСведТребВыстСчФ_1735СведРасхСчФ();
                СведРасхR12.Add(СведРасх1);

                //--- Расхождение (исходная СФ)
                СведРасх1.Расхожд = new ФайлДокументСведТребВыстСчФ_1735СведРасхСчФРасхожд();
                СведРасх1.Расхожд.ТипИнф = ФайлДокументСведТребВыстСчФ_1735СведРасхСчФРасхождТипИнф.Item1; //--- Item1 (1 – расхождение)
                СведРасх1.Расхожд.ВСчФ_1735 = GenerateInvoiceOriginalR12(inv);

                //--- Пояснение (измененная СФ)
                ФайлДокументСведТребВыстСчФ_1735СведРасхСчФПояснен Пояснен = new ФайлДокументСведТребВыстСчФ_1735СведРасхСчФПояснен();
                СведРасх1.Пояснен = Пояснен;
                Пояснен.ТипИнф = ФайлДокументСведТребВыстСчФ_1735СведРасхСчФПоясненТипИнф.Item2; //--- Item2 (2 – пояснение)
                Пояснен.ВСчФ_1735 = new ВСчФ_1735Тип();
                Пояснен.ВСчФ_1735.НомСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), inv.INVOICE_NUM);
                Пояснен.ВСчФ_1735.ДатаСчФ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), FormatDateTime(inv.INVOICE_DATE_DT));
                Пояснен.ВСчФ_1735.ОКВ = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), inv.OKV_CODE);
                string buyerInn = GetInvoiceCorrectValue(invoicesCorrectes, inv.ROW_KEY, TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_INN), inv.BUYER_INN);
                Пояснен.ВСчФ_1735.СвПокуп = GenerateTaxpayerInfo(buyerInn);
                //---
                ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TAX_FREE)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal price = TryParseDecimal(invCorr.FieldValue);
                    Пояснен.ВСчФ_1735.СтоимТовБНалВс = price;
                    Пояснен.ВСчФ_1735.СтоимТовБНалВсSpecified = true;
                }
                else if (inv.PRICE_TAX_FREE_DC != null)
                {
                    Пояснен.ВСчФ_1735.СтоимТовБНалВс = (decimal)inv.PRICE_TAX_FREE_DC;
                    Пояснен.ВСчФ_1735.СтоимТовБНалВсSpecified = true;
                }
                //---
                invCorr = invoicesCorrectes.Where(p => p.InvoiceId == inv.ROW_KEY && p.FieldName == TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT)).SingleOrDefault();
                if (invCorr != null)
                {
                    decimal price = TryParseDecimal(invCorr.FieldValue);
                    Пояснен.ВСчФ_1735.СтоимТовСНалВс = price;
                    Пояснен.ВСчФ_1735.СтоимТовСНалВсSpecified = true;
                }
                else if (inv.PRICE_BUY_AMOUNT_DC != null)
                {
                    Пояснен.ВСчФ_1735.СтоимТовСНалВс = (decimal)inv.PRICE_BUY_AMOUNT_DC;
                    Пояснен.ВСчФ_1735.СтоимТовСНалВсSpecified = true;
                }
            }
        }

        private ВСчФ_1735Тип GenerateInvoiceOriginalR12(ExplainInvoice inv)
        {
            ВСчФ_1735Тип ВСчФ_1735Тип1 = new ВСчФ_1735Тип();
            ВСчФ_1735Тип1.НомСчФ = GetNullAndNotEmptyStringValue(inv.INVOICE_NUM);
            ВСчФ_1735Тип1.ДатаСчФ = GetNullAndNotEmptyDateTimeValue(inv.INVOICE_DATE_DT);
            ВСчФ_1735Тип1.ОКВ = GetNullAndNotEmptyStringValue(inv.OKV_CODE);
            ВСчФ_1735Тип1.СвПокуп = GenerateTaxpayerInfo(inv.BUYER_INN);
            if (inv.PRICE_TAX_FREE_DC != null)
            {
                ВСчФ_1735Тип1.СтоимТовБНалВс = (decimal)inv.PRICE_TAX_FREE_DC;
                ВСчФ_1735Тип1.СтоимТовБНалВсSpecified = true;
            }
            if (inv.PRICE_BUY_AMOUNT_DC != null)
            {
                ВСчФ_1735Тип1.СтоимТовСНалВс = (decimal)inv.PRICE_BUY_AMOUNT_DC;
                ВСчФ_1735Тип1.СтоимТовСНалВсSpecified = true;
            }
            return ВСчФ_1735Тип1;
        }

        private void GenerateCommonParamters(EntitiesExplain.Файл entityXML, ExplainReplyInfo explainReplyInfo, DeclarationSummary decl, ASKDekl askDecl, Entities.Файл askFile,
            string version, string IdFrom, string IdTo, out string nameFile)
        {
            DateTime docDT = DateTime.Now;
            string A = IdFrom;
            string K = IdTo;
            string O = string.Format("{0}{1}", decl.INN, decl.KPP);
            string unikFileId = ExplainHelpers.GenerateUnikFileId();
            string nameFileWithoutExt = string.Format("ON_OTTRNDS_{0}_{1}_{2}_{3:0000}{4:00}{5:00}_{6}", A, K, O, docDT.Year, docDT.Month, docDT.Day, unikFileId);
            nameFile = nameFileWithoutExt;

            //=============================================================================
            entityXML.ИдФайл = nameFileWithoutExt;
            entityXML.ВерсПрог = version;
            entityXML.ВерсФорм = ФайлВерсФорм.Item501;

            ФайлДокумент Документ = new ФайлДокумент();
            Документ = new ФайлДокумент();
            Документ.ДатаДок = FormatDateTime(docDT);
            Документ.КНД = ФайлДокументКНД.Item1160200;

            entityXML.Документ = Документ;
        }

        public string GenerateExplainXMLEntity(ExplainParameters x, out string nameFile)
        {
            string nameFileInner = String.Empty;
            string xmlSerialize = SerializeExplinXMLEntity(CreateExplainXMLEntity(x, out nameFileInner));
            nameFile = nameFileInner;
            return xmlSerialize;
        }

        public EntitiesExplain.Файл CreateExplainXMLEntity(ExplainParameters x, out string nameFile)
        {
            EntitiesExplain.Файл entityXML = new EntitiesExplain.Файл();
            GenerateCommonParamters(entityXML, x.ExplainReplyInfo, x.Decl, x.AskDecl, x.AskFile, x.Version, x.IdFrom, x.IdTo, out nameFile);

            //=================================== -Сведения от требовании о прдставлении пояснений, на которое сформирован ответ
            ФайлДокументСведТреб СведТреб = new ФайлДокументСведТреб();

            //----- Раздел 8
            List<ФайлДокументСведТребКнигаПокупСоотвСтр> СведТребКнигаПокупСведСоотв = new List<ФайлДокументСведТребКнигаПокупСоотвСтр>();
            List<ФайлДокументСведТребКнигаПокупРасхСтр> СведТребКнигаПокупСведРасх = new List<ФайлДокументСведТребКнигаПокупРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 8 && !p.PRIZNAK_DOP_LIST &&
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p=>p.ROW_KEY))
            {
                FillChapter08(false, inv, x.InvoicesCorrectes, СведТребКнигаПокупСведСоотв, СведТребКнигаПокупСведРасх, null, null);
            }
            СведТреб.КнигаПокуп = new ФайлДокументСведТребКнигаПокуп();
            СведТреб.КнигаПокуп.СведСоотв = СведТребКнигаПокупСведСоотв.Any() ? СведТребКнигаПокупСведСоотв.ToArray() : null;
            СведТреб.КнигаПокуп.СведРасх = СведТребКнигаПокупСведРасх.Any() ? СведТребКнигаПокупСведРасх.ToArray() : null;

            //----- Раздел 8 Доп.лист
            List<ФайлДокументСведТребКнигаПокупДЛСоотвСтр> СведТребКнигаПокупСведСоотвДЛ = new List<ФайлДокументСведТребКнигаПокупДЛСоотвСтр>();
            List<ФайлДокументСведТребКнигаПокупДЛРасхСтр> СведТребКнигаПокупСведРасхДЛ = new List<ФайлДокументСведТребКнигаПокупДЛРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 8 && p.PRIZNAK_DOP_LIST && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter08(true, inv, x.InvoicesCorrectes, null, null, СведТребКнигаПокупСведСоотвДЛ, СведТребКнигаПокупСведРасхДЛ);
            }
            СведТреб.КнигаПокупДЛ = new ФайлДокументСведТребКнигаПокупДЛ();
            СведТреб.КнигаПокупДЛ.СведСоотв = СведТребКнигаПокупСведСоотвДЛ.Any() ? СведТребКнигаПокупСведСоотвДЛ.ToArray() : null;
            СведТреб.КнигаПокупДЛ.СведРасх = СведТребКнигаПокупСведРасхДЛ.Any() ? СведТребКнигаПокупСведРасхДЛ.ToArray() : null;

            //------ Раздел 9
            List<ФайлДокументСведТребКнигаПродСоотвСтр> СведТребКнигаПродСведСоотв = new List<ФайлДокументСведТребКнигаПродСоотвСтр>();
            List<ФайлДокументСведТребКнигаПродРасхСтр> СведТребКнигаПродСведРасх = new List<ФайлДокументСведТребКнигаПродРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 9 && !p.PRIZNAK_DOP_LIST && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter09(false, inv, x.InvoicesCorrectes, СведТребКнигаПродСведСоотв, СведТребКнигаПродСведРасх, null, null);
            }
            СведТреб.КнигаПрод = new ФайлДокументСведТребКнигаПрод();
            СведТреб.КнигаПрод.СведСоотв = СведТребКнигаПродСведСоотв.Any() ? СведТребКнигаПродСведСоотв.ToArray() : null;
            СведТреб.КнигаПрод.СведРасх = СведТребКнигаПродСведРасх.Any() ? СведТребКнигаПродСведРасх.ToArray() : null;

            //------ Раздел 9 Доп.Лист
            List<ФайлДокументСведТребКнигаПродДЛСоотвСтр> СведТребКнигаПродСведСоотвДЛ = new List<ФайлДокументСведТребКнигаПродДЛСоотвСтр>();
            List<ФайлДокументСведТребКнигаПродДЛРасхСтр> СведТребКнигаПродСведРасхДЛ = new List<ФайлДокументСведТребКнигаПродДЛРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 9 && p.PRIZNAK_DOP_LIST && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter09(true, inv, x.InvoicesCorrectes, null, null, СведТребКнигаПродСведСоотвДЛ, СведТребКнигаПродСведРасхДЛ);
            }
            СведТреб.КнигаПродДЛ = new ФайлДокументСведТребКнигаПродДЛ();
            СведТреб.КнигаПродДЛ.СведСоотв = СведТребКнигаПродСведСоотвДЛ.Any() ? СведТребКнигаПродСведСоотвДЛ.ToArray() : null;
            СведТреб.КнигаПродДЛ.СведРасх = СведТребКнигаПродСведРасхДЛ.Any() ? СведТребКнигаПродСведРасхДЛ.ToArray() : null;

            //------ Раздел 10
            List<ФайлДокументСведТребЖУчВыстСчФСоотвСтр> ЖУчВыстСчФСведСоотв = new List<ФайлДокументСведТребЖУчВыстСчФСоотвСтр>();
            List<ФайлДокументСведТребЖУчВыстСчФРасхСтр> ЖУчВыстСчФСведРасх = new List<ФайлДокументСведТребЖУчВыстСчФРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 10 && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter10(inv, x.InvoicesCorrectes, ЖУчВыстСчФСведСоотв, ЖУчВыстСчФСведРасх);
            }
            СведТреб.ЖУчВыстСчФ = new ФайлДокументСведТребЖУчВыстСчФ();
            СведТреб.ЖУчВыстСчФ.СведСоотв = ЖУчВыстСчФСведСоотв.Any() ? ЖУчВыстСчФСведСоотв.ToArray() : null;
            СведТреб.ЖУчВыстСчФ.СведРасх = ЖУчВыстСчФСведРасх.Any() ? ЖУчВыстСчФСведРасх.ToArray() : null;

            //------ Раздел 11
            List<ФайлДокументСведТребЖУчПолучСчФСоотвСтр> СведТребЖУчПолучСчФСведСоотв = new List<ФайлДокументСведТребЖУчПолучСчФСоотвСтр>();
            List<ФайлДокументСведТребЖУчПолучСчФРасхСтр> СведТребЖУчПолучСчФСведРасх = new List<ФайлДокументСведТребЖУчПолучСчФРасхСтр>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 11 && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter11(inv, x.InvoicesCorrectes, СведТребЖУчПолучСчФСведСоотв, СведТребЖУчПолучСчФСведРасх);
            }
            СведТреб.ЖУчПолучСчФ = new ФайлДокументСведТребЖУчПолучСчФ();
            СведТреб.ЖУчПолучСчФ.СведСоотв = СведТребЖУчПолучСчФСведСоотв.Any() ? СведТребЖУчПолучСчФСведСоотв.ToArray() : null;
            СведТреб.ЖУчПолучСчФ.СведРасх = СведТребЖУчПолучСчФСведРасх.Any() ? СведТребЖУчПолучСчФСведРасх.ToArray() : null;

            //------ Раздел 12
            List<ВСчФ_1735Тип> СведСоотвR12 = new List<ВСчФ_1735Тип>();
            List<ФайлДокументСведТребВыстСчФ_1735СведРасхСчФ> СведРасхR12 = new List<ФайлДокументСведТребВыстСчФ_1735СведРасхСчФ>();
            foreach (ExplainInvoice inv in x.Invoices.Where(p => p.CHAPTER == 12 && 
                (p.State == ExplainInvoiceState.ChangingInThisExplain || p.State == ExplainInvoiceState.ProcessedInThisExplain))
                .OrderBy(p => p.ROW_KEY))
            {
                FillChapter12(inv, x.InvoicesCorrectes, СведСоотвR12, СведРасхR12);
            }
            СведТреб.ВыстСчФ_1735 = new ФайлДокументСведТребВыстСчФ_1735();
            СведТреб.ВыстСчФ_1735.СведСоотв = СведСоотвR12.Any() ? СведСоотвR12.ToArray() : null;
            СведТреб.ВыстСчФ_1735.СведРасх = СведРасхR12.Any() ? СведРасхR12.ToArray() : null;

            entityXML.Документ.СведТреб = СведТреб;
            //===============================================================================================

            return entityXML;
        }

        private string SerializeExplinXMLEntity(EntitiesExplain.Файл entityXML)
        {
            XmlSerializer ser = new XmlSerializer(typeof(EntitiesExplain.Файл));
            StringBuilder sb = new StringBuilder();
            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, entityXML);
            }
            return sb.ToString();
        }

        private bool HasInvoiceCorrectValue(List<ExplainInvoiceCorrect> invoicesCorrectes, string invoiceId, string fieldName)
        {
            bool isHas = invoicesCorrectes.Where(p => p.InvoiceId == invoiceId && p.FieldName == fieldName).Count() > 0;
            return isHas;
        }

        private string GetInvoiceCorrectValue(List<ExplainInvoiceCorrect> invoicesCorrectes, string invoiceId, string fieldName, string originalValue)
        {
            string ret = String.Empty;
            ExplainInvoiceCorrect invCorr = invoicesCorrectes.Where(p => p.InvoiceId == invoiceId && p.FieldName == fieldName).SingleOrDefault();
            if (invCorr != null)
            {
                if (invCorr.FieldValue != null)
                {
                    string stringFieldValue = (string)invCorr.FieldValue;
                    stringFieldValue = stringFieldValue.Trim();
                    ret = _explainFieldFormatter.GetValue(fieldName, stringFieldValue);
                }
            }
            else
            {
                ret = originalValue;
            }
            return ret == String.Empty ? null : ret;
        }

        private string FormatDateTime(DateTime? dt)
        {
            return _explainFieldFormatter.FormatDateTime(dt);
        }

        private decimal TryParseDecimal(string value)
        {
            return ExplainParser.ParseDecimal(value);
        }

        private DateTime? TryParseDate(string value)
        {
            return ExplainParser.ParseDataTime(value);
        }

        private void FillOneOperartionBuyCode(Dictionary<string, КнПокСтрТипКодВидОпер> operationBuyCodes, КнПокСтрТипКодВидОпер item)
        {
            operationBuyCodes.Add(item.ToString(), item);
        }

        private void FillOneOperartionSaleCode(Dictionary<string, КнПродСтрТипКодВидОпер> operationSaleCodes, КнПродСтрТипКодВидОпер item)
        {
            operationSaleCodes.Add(item.ToString(), item);
        }

        private void FillOneOperartionR10Code(Dictionary<string, ЖУчВСтрТипКодВидОпер> _operationR10Codes, ЖУчВСтрТипКодВидОпер item)
        {
            _operationR10Codes.Add(item.ToString(), item);
        }

        private void FillOneOperartionR11Code(Dictionary<string, ЖУчПСтрТипКодВидОпер> _operationR11Codes, ЖУчПСтрТипКодВидОпер item)
        {
            _operationR11Codes.Add(item.ToString(), item);
        }

        private void FillOneDealKindR11Code(Dictionary<string, ЖУчПСтрТипКодВидСд> _dealKindR11Codes, ЖУчПСтрТипКодВидСд item)
        {
            _dealKindR11Codes.Add(item.ToString(), item);
        }

        private Dictionary<string, КнПокСтрТипКодВидОпер> _operationBuyCodes = new Dictionary<string, КнПокСтрТипКодВидОпер>();
        private Dictionary<string, КнПродСтрТипКодВидОпер> _operationSaleCodes = new Dictionary<string, КнПродСтрТипКодВидОпер>();
        private Dictionary<string, ЖУчВСтрТипКодВидОпер> _operationR10Codes = new Dictionary<string, ЖУчВСтрТипКодВидОпер>();
        private Dictionary<string, ЖУчПСтрТипКодВидОпер> _operationR11Codes = new Dictionary<string, ЖУчПСтрТипКодВидОпер>();
        private Dictionary<string, ЖУчПСтрТипКодВидСд> _dealKindR11Codes = new Dictionary<string, ЖУчПСтрТипКодВидСд>();

        private void FillOperartionBuyCode(Dictionary<string, КнПокСтрТипКодВидОпер> operationBuyCodes)
        {
            operationBuyCodes.Clear();
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item01);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item02);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item03);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item04);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item05);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item06);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item07);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item08);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item09);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item10);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item11);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item12);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item13);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item15);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item16);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item17);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item18);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item19);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item20);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item21);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item22);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item23);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item24);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item25);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item26);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item27);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item28);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item32);
            FillOneOperartionBuyCode(operationBuyCodes, КнПокСтрТипКодВидОпер.Item99);
        }

        private void FillOperartionSaleCode(Dictionary<string, КнПродСтрТипКодВидОпер> operationSaleCodes)
        {
            operationSaleCodes.Clear();
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item01);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item02);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item03);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item04);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item05);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item06);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item07);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item08);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item09);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item10);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item11);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item12);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item13);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item14);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item15);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item16);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item17);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item18);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item19);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item20);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item21);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item22);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item23);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item24);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item25);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item26);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item27);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item28);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item29);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item30);
            FillOneOperartionSaleCode(operationSaleCodes, КнПродСтрТипКодВидОпер.Item31);
        }

        private void FillOperartionR10Code(Dictionary<string, ЖУчВСтрТипКодВидОпер> operationR10Codes)
        {
            operationR10Codes.Clear();
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item01);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item02);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item03);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item04);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item05);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item06);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item07);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item08);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item09);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item10);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item11);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item12);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item13);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item15);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item16);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item17);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item18);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item19);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item20);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item21);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item22);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item23);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item24);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item25);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item26);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item27);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item28);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item29);
            FillOneOperartionR10Code(operationR10Codes, ЖУчВСтрТипКодВидОпер.Item30);

        }

        private void FillOperartionR11Code(Dictionary<string, ЖУчПСтрТипКодВидОпер> operationR11Codes)
        {
            operationR11Codes.Clear();
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item01);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item02);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item03);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item04);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item05);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item06);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item07);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item08);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item09);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item10);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item11);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item12);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item13);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item15);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item16);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item17);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item18);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item19);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item20);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item21);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item22);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item23);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item24);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item25);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item26);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item27);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item28);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item29);
            FillOneOperartionR11Code(operationR11Codes, ЖУчПСтрТипКодВидОпер.Item30);
        }

        private void FillDealKindR11Code(Dictionary<string, ЖУчПСтрТипКодВидСд> dealKindR11Codes)
        {
            dealKindR11Codes.Clear();
            FillOneDealKindR11Code(dealKindR11Codes, ЖУчПСтрТипКодВидСд.Item1);
            FillOneDealKindR11Code(dealKindR11Codes, ЖУчПСтрТипКодВидСд.Item2);
            FillOneDealKindR11Code(dealKindR11Codes, ЖУчПСтрТипКодВидСд.Item3);
            FillOneDealKindR11Code(dealKindR11Codes, ЖУчПСтрТипКодВидСд.Item4);
        }

        private КнПокСтрТипКодВидОпер[] GetOperationBuyCode(string operationCodes)
        {
            var rets = new List<КнПокСтрТипКодВидОпер>();
            if (!string.IsNullOrWhiteSpace(operationCodes))
            {
                var operationCodesArray = operationCodes.Split(',');

                foreach (var item in operationCodesArray)
                {
                    string operationCodeItem = GetOperationCodeItem(item);
                    if (_operationBuyCodes.ContainsKey(operationCodeItem))
                        rets.Add(_operationBuyCodes[operationCodeItem]);
                    else
                        OperationCodeNotFound(8, item);
                }
            }
            return rets.Any() ? rets.ToArray() : null;
        }

        private string GetOperationCodeItem(string operationCode)
        {
            string operationCodeValue = operationCode.Trim();
            if (operationCodeValue.Length == 1)
                operationCodeValue = "0" + operationCodeValue;
            return string.Format("Item{0}", operationCodeValue);
        }

        private void OperationCodeNotFound(int chapter, string operationCode)
        {
            throw new Exception(string.Format("Формирование XML пояснения: Раздел {0}, недопустимый код вида операции = \"{1}\"", chapter, operationCode));
        }

        private string[] GetAccepptDates(string buyAcceptDates)
        { 
            List<string> result = new List<string>();

            if (!string.IsNullOrWhiteSpace(buyAcceptDates))
            {
                foreach (var item in buyAcceptDates.Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        var dt = TryParseDate(item.Trim());
                        if (dt != null)
                            result.Add(FormatDateTime(dt));
                    }
                }
            }
            return result.Any() ? result.ToArray() : null;
        }

        private string[] GetAccepptDatesOriginal(ExplainInvoice invoice)
        {
            List<string> result = new List<string>();
            if (invoice.Attributes.BuyAccepts != null)
            {
                foreach (var item in invoice.Attributes.BuyAccepts)
                {
                    if (item.AcceptedAt != null)
                        result.Add(FormatDateTime(item.AcceptedAt));
                }
            }
            return result.Any() ? result.ToArray() : null;
        }

        private КнПродСтрТипКодВидОпер[] GetOperationSaleCode(string operationCodes)
        {
            List<КнПродСтрТипКодВидОпер> rets = new List<КнПродСтрТипКодВидОпер>();
            if (!string.IsNullOrWhiteSpace(operationCodes))
            {
                var operationCodesArray = operationCodes.Split(',');

                foreach (var item in operationCodesArray)
                {
                    string operationCodeItem = GetOperationCodeItem(item);
                    if (_operationSaleCodes.ContainsKey(operationCodeItem))
                        rets.Add(_operationSaleCodes[operationCodeItem]);
                    else
                        OperationCodeNotFound(9, item);
                }
            }
            return rets.Any() ? rets.ToArray() : null;

        }

        private ЖУчВСтрТипКодВидОпер[] GetOperationR10Code(string operationCodes)
        {
            List<ЖУчВСтрТипКодВидОпер> rets = new List<ЖУчВСтрТипКодВидОпер>();
            if (!string.IsNullOrWhiteSpace(operationCodes))
            {
                var operationCodesArray = operationCodes.Split(',');

                foreach (var item in operationCodesArray)
                {
                    string operationCodeItem = GetOperationCodeItem(item);
                    if (_operationR10Codes.ContainsKey(operationCodeItem))
                        rets.Add(_operationR10Codes[operationCodeItem]);
                    else
                        OperationCodeNotFound(10, item);
                }
            }
            return rets.Any() ? rets.ToArray() : null;
        }

        private ЖУчПСтрТипКодВидОпер[] GetOperationR11Code(string operationCodes)
        {
            List<ЖУчПСтрТипКодВидОпер> rets = new List<ЖУчПСтрТипКодВидОпер>();
            if (!string.IsNullOrWhiteSpace(operationCodes))
            {
                var operationCodesArray = operationCodes.Split(',');

                foreach (var item in operationCodesArray)
                {
                    string operationCodeItem = GetOperationCodeItem(item);
                    if (_operationR11Codes.ContainsKey(operationCodeItem))
                        rets.Add(_operationR11Codes[operationCodeItem]);
                    else
                        OperationCodeNotFound(11, item);
                }
            }
            return rets.Any() ? rets.ToArray() : null;
        }

        private ЖУчПСтрТипКодВидСд GetDealKindR11Code(string operationCode)
        {
            ЖУчПСтрТипКодВидСд ret = ЖУчПСтрТипКодВидСд.Item1;
            if (_dealKindR11Codes.ContainsKey(operationCode))
            {
                ret = _dealKindR11Codes[operationCode];
            }
            return ret;
        }

        private СвУчСдТип GenerateTaxpayerInfo(string inn)
        {
            СвУчСдТип ret = null;

            if (!String.IsNullOrEmpty(inn))
            {
                ret = new СвУчСдТип();

                if (inn.Length == 12)
                {
                    СвУчСдТипСведИП СвУчСдТипСведИП1 = new СвУчСдТипСведИП();
                    СвУчСдТипСведИП1.ИННФЛ = inn;
                    ret.Item = СвУчСдТипСведИП1;
                }
                else
                {
                    СвУчСдТипСведЮЛ СвУчСдТипСведЮЛ1 = new СвУчСдТипСведЮЛ();
                    СвУчСдТипСведЮЛ1.ИННЮЛ = inn;
                    ret.Item = СвУчСдТипСведЮЛ1;
                }
            }

            return ret;
        }

        private СвУчСдТип[] GenerateTaxpayerInfoArray(string inns)
        {
            var result = new List<СвУчСдТип>();

            var innList = new List<string>();
            if (!string.IsNullOrWhiteSpace(inns))
                innList.AddRange(inns.Split(',').Select(p=>p.Trim()));

            foreach (var inn in innList)
            {
                var info = GenerateTaxpayerInfo(inn);
                if (info != null)
                    result.Add(info);
            }

            return result.Any() ? result.ToArray() : null;
        }

        private СвУчСдТип[] GenerateTaxpayerInfoArray(IEnumerable<InvoiceContractor> invoiceContractors)
        {
            var result = new List<СвУчСдТип>();

            if (invoiceContractors != null)
            {
                foreach (var item in invoiceContractors)
                {
                    var info = GenerateTaxpayerInfo(item.Inn);
                    if (info != null)
                        result.Add(info);
                }
            }

            return result.Any() ? result.ToArray() : null;
        }

        private string TransformDate(string date)
        {
            string ret = null;
            if (!string.IsNullOrWhiteSpace(date))
            {
                var oneDate = TryParseDate(date.Trim());
                if (oneDate != null)
                    ret = FormatDateTime(oneDate);
            }
            return  ret;
        }

        private DateTime? ParseDate(string date)
        {
            DateTime? ret = null;
            if (!string.IsNullOrWhiteSpace(date))
                ret = TryParseDate(date.Trim());
            return ret;
        }

        private ДокПдтвОплТип GenerateOneRecieptDocument(string number, string date)
        {
            ДокПдтвОплТип obj = new ДокПдтвОплТип();
            obj.НомДокПдтвОпл = number;
            obj.ДатаДокПдтвОпл = TransformDate(date);
            return obj;
        }

        private ДокПдтвОплТип GenerateOneRecieptDocument(string number, DateTime? date)
        {
            ДокПдтвОплТип obj = new ДокПдтвОплТип();
            obj.НомДокПдтвОпл = number;
            if (date != null)
                obj.ДатаДокПдтвОпл = FormatDateTime(date);
            else
                obj.ДатаДокПдтвОпл = null;
            return obj;
        }

        private ДокПдтвОплТип[] GenerateRecieptDocuments(string numbers, string dates)
        {
            ДокПдтвОплТип[] result = null;

            var numList = new List<string>();
            if (!string.IsNullOrWhiteSpace(numbers))
                numList.AddRange(numbers.Split(',').Select(p => p.Trim()));

            var dateList = new List<string>();
            if (!string.IsNullOrWhiteSpace(dates))
                dateList.AddRange(dates.Split(',').Select(p => p.Trim()));

            int sc = 0;
            var resultList = new List<ДокПдтвОплТип>();
            foreach (var num in numList)
            {
                string date = String.Empty;
                if (sc < dateList.Count)
                    date = dateList[sc];
                var info = GenerateOneRecieptDocument(num, date);
                if (info != null)
                    resultList.Add(info);
                sc++;
            }

            if (resultList.Any())
                result = resultList.ToArray();

            return result;
        }

        private ДокПдтвОплТип[] GenerateRecieptDocuments(List<InvoicePaymentDocument> paymentDocuments)
        {
            var result = new List<ДокПдтвОплТип>();

            if (paymentDocuments != null)
            {
                foreach (var item in paymentDocuments)
                {
                    var info = GenerateOneRecieptDocument(item.Number, item.Date);
                    if (info != null)
                        result.Add(info);
                }
            }

            return result.Any() ? result.ToArray() : null;
        }

        private string GetNullAndNotEmptyStringValue(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }

        private string GetNullAndNotEmptyDateTimeValue(DateTime? dt)
        {
            string ret = null;
            if (dt != null)
            {
                if (dt.Value > DateTime.MinValue)
                    ret = string.Format("{0:00}.{1:00}.{2:0000}", dt.Value.Day, dt.Value.Month, dt.Value.Year);
            }
            return ret;
        }

        private string GetNullAndNotEmptyDateStringValue(string date)
        {
            string ret = null;
            DateTime? dt = ParseDate(date);
            if (dt != null)
            {
                if (dt.Value > DateTime.MinValue)
                    ret = string.Format("{0:00}.{1:00}.{2:0000}", dt.Value.Day, dt.Value.Month, dt.Value.Year);
            }
            return ret;
        }

        private string GetNullAndNotEmptyDecimalStringValue(decimal? value)
        {
            string ret = null;
            if (value != null)
            {
                ret = string.Format("{0}", value);
            }
            return ret;
        }
    }
}
