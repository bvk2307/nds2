﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using System;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class DalHelper
    {
        public static OperationResult Execute(
            Action dalAction, 
            ILogProvider logger)
        {
            try
            {
                dalAction();

                return new OperationResult() { Status = ResultStatus.Success };
            }
            catch (DatabaseException dbEx)
            {
                return new OperationResult() 
                { 
                    Status = ResultStatus.Error, 
                    Message = Error(dbEx.InnerException, logger).Text 
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    Status = ResultStatus.Error,
                    Message = Error(ex, logger).Text
                };
            }
        }

        public static OperationResult<T> ExecuteOperation<T>(Func<T> dalAction, ILogProvider logger) where T : class, new()
        {
            var result = new OperationResult<T>();
            try
            {
                result.Result = dalAction();
                result.Status = ResultStatus.Success;
            }
            catch (DatabaseException dbEx)
            {
                result.Status = ResultStatus.Error;
                result.Message = (Error(dbEx.InnerException, logger).Text);
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.Message = (Error(ex, logger).Text);
            }
            return result;
        }

        public static TOperationResult Execute<TOperationResult>(
            Func<TOperationResult> dalAction,
            ILogProvider logger) where TOperationResult : IOperationResult, new()
        {
            try
            {
                return dalAction();
            }
            catch (DatabaseException dbEx)
            {
                return new TOperationResult()
                    {
                        Status = ResultStatus.Error,
                        Message = (Error(dbEx.InnerException, logger).Text)
                    };
            }
            catch (Exception ex)
            {
                return new TOperationResult()
                {
                    Status = ResultStatus.Error,
                    Message = (Error(ex, logger).Text)
                };
            }

        }

        internal static ServiceError Error(Exception ex, ILogProvider logger)
        {
            var error = new ServiceError();
            logger.LogError(
                error.Text,
                null,
                ex);

            return error;
        }
    }
}
