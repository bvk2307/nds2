﻿using System;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    /// <summary>
    /// Исключение для сигнализации о нарушении прав доступа
    /// </summary>
    class AccessDeniedException : Exception
    {
        public AccessDeniedException(string msg) : base(msg)
        {
        }
    }
}
