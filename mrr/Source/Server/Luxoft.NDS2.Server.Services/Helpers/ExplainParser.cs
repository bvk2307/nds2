﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class ExplainParser
    {
        public static DateTime? ParseDataTime(string value)
        {
            DateTime? result = null;
            DateTime dtParse = DateTime.Now;
            if (TryParseDataTime(value, out dtParse))
            {
                result = dtParse;
            }
            return result;
        }

        public static bool TryParseDataTime(string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;

            DateTime dtParse = DateTime.Now;
            if (DateTime.TryParse(value, out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }
            else
            {
                if (TryParseDataTime(CultureInfo.CurrentCulture, value, out dtParse))
                {
                    resultValue = dtParse;
                    ret = true;
                }
                else
                {
                    var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
                    foreach (var itemCulture in cultures)
                    {
                        if (TryParseDataTime(itemCulture, value, out dtParse))
                        {
                            resultValue = dtParse;
                            ret = true;
                            break;
                        }
                    }
                }
            }

            return ret;
        }

        private static bool TryParseDataTime(CultureInfo cultureInfo, string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;
            DateTime dtParse = DateTime.Now;

            var formats = new List<string>();
            formats.AddRange(cultureInfo.DateTimeFormat.GetAllDateTimePatterns());

            if (DateTime.TryParseExact(
                value,
                formats.ToArray(),
                cultureInfo,
                DateTimeStyles.None,
                out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }

            return ret;
        }

        public static decimal ParseDecimal(string value)
        {
            decimal ret = -1;
            decimal temp = 0;
            if (TryParseDecimal(value, out temp))
            {
                ret = temp;
            }
            return ret;
        }

        public static bool TryParseDecimal(string value, out decimal resultValue)
        {
            bool ret = false;
            resultValue = -1;

            decimal decimalParse = -1;
            if (TryParseDecimal(CultureInfo.GetCultureInfo("Ru-ru"), value, out decimalParse))
            {
                resultValue = decimalParse;
                ret = true;
            }
            else
            {
                if (TryParseDecimal(CultureInfo.CurrentCulture, value, out decimalParse))
                {
                    resultValue = decimalParse;
                    ret = true;
                }
                else
                {
                    var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
                    foreach (var itemCulture in cultures)
                    {
                        if (TryParseDecimal(itemCulture, value, out decimalParse))
                        {
                            resultValue = decimalParse;
                            ret = true;
                            break;
                        }
                    }
                }
            }

            return ret;
        }

        private static bool TryParseDecimal(CultureInfo cultureInfo, string value, out decimal resultValue)
        {
            bool ret = false;
            resultValue = -1;
            decimal decimalParse = -1;

            if (decimal.TryParse(
                value,
                NumberStyles.Any,
                cultureInfo,
                out decimalParse))
            {
                resultValue = decimalParse;
                ret = true;
            }

            return ret;
        }
    }
}
