﻿using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    internal class QueryConditionsHelper
    {
        private readonly IAuthorizationProvider _localAuthProvider;

        private readonly IServiceProvider _serviceProvider;

        public QueryConditionsHelper(
            IAuthorizationProvider localAuthProvider,
            IServiceProvider serviceProvider)
        {
            _localAuthProvider = localAuthProvider;
            _serviceProvider = serviceProvider;

        }

        /// <summary>
        /// Добавляет в условия выборки доп. фильтр по регионам в зависимости от настроек и роли пользователя
        /// </summary>
        /// <returns></returns>
        public void AddRestriction(
            QueryConditions conditions,
            string regionFieldName,
            string inspectionFieldName)
        {
            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleInspector))
            {
                var list = _localAuthProvider.GetUserStructContexts(PermissionType.Operation, Constants.SystemPermissions.Operations.RoleInspector);
                var inspections = new FilterQuery
                {
                    ColumnName = inspectionFieldName,
                    GroupOperator = FilterQuery.FilterLogicalOperator.And,
                    FilterOperator = FilterQuery.FilterLogicalOperator.Or
                };

                if (list.Any())
                {
                    if (!list.Contains(Constants.SystemPermissions.CentralDepartmentCode))
                    {
                        foreach (var ifns in list)
                        {
                            inspections.Filtering.Add(new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = ifns });
                        }

                        conditions.Filter.Add(inspections);
                    }
                }
                else
                {
                    inspections.Filtering.Add(new ColumnFilter { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "-1" });
                    conditions.Filter.Add(inspections);
                }

            }
        }

        public static void AddFilter(
            QueryConditions criteria,
            string columnName,
            FilterQuery.FilterLogicalOperator filterQueryOperator,
            object value,
            ColumnFilter.FilterComparisionOperator comparisionOperator)
        {
            var filterQuery = criteria.Filter.Where(p => p.ColumnName == columnName).SingleOrDefault();
            if (filterQuery == null)
            {
                filterQuery = new FilterQuery();
                filterQuery.ColumnName = columnName;
                filterQuery.FilterOperator = filterQueryOperator;
                ColumnFilter columnFilter = new ColumnFilter();
                columnFilter.ComparisonOperator = comparisionOperator;
                columnFilter.Value = value;
                filterQuery.Filtering.Add(columnFilter);
                criteria.Filter.Add(filterQuery);
            }
            else
            {
                if (filterQuery.Filtering.Where(p => ((p.Value != null && value != null && p.Value.Equals(value)) || (p.Value == null && value == null)) &&
                    p.ComparisonOperator == comparisionOperator).Count() == 0)
                {
                    filterQuery.FilterOperator = filterQueryOperator;
                    ColumnFilter columnFilter = new ColumnFilter();
                    columnFilter.ComparisonOperator = comparisionOperator;
                    columnFilter.Value = value;
                    filterQuery.Filtering.Add(columnFilter);
                }
            }
        }
    }
}
