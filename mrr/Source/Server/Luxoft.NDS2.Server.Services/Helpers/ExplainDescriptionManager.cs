﻿using Luxoft.NDS2.Server.Services.Entities;
using Luxoft.NDS2.Server.Services.Entities.ExplainDescription;
using Luxoft.NDS2.Server.Services.Entities.Parameters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public class ExplainDescriptionManager
    {
        public string GenerateXML(ExplainParameters parameters, out string nameFile)
        {
            string nameFileInner = String.Empty;
            string xmlSerialize = SerializeEntity(CreateEntity(parameters, out nameFileInner));
            nameFile = nameFileInner;
            return xmlSerialize;
        }

        public описание CreateEntity(ExplainParameters parameters, out string nameFile)
        {
            var entityXML = new описание();
            nameFile = "TR_PROTDOC";

            entityXML.КНД = "1165034";
            entityXML.КодНО = "7722";

            return entityXML;
        }

        private string SerializeEntity(описание entityXML)
        {
            var xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.OmitXmlDeclaration = false;
            xmlWriterSettings.Encoding = Encoding.GetEncoding("windows-1251");

            StringBuilder sb = new StringBuilder();
            using (TextWriter textWriter = new StringWriter(sb))
            using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, xmlWriterSettings))
            {
                XmlSerializer ser = new XmlSerializer(typeof(описание));
                ser.Serialize(xmlWriter, entityXML);
            }

            return sb.ToString();
        }
    }
}
