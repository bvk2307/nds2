﻿using System;
using System.IO;
using System.Text;
using System.Web.Script.Serialization; // Add reference: System.Web.Extensions
using System.Xml;
using System.Xml.Serialization;


namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class ParseHelpers
    {
        private static JavaScriptSerializer json;
        private static JavaScriptSerializer JSON { get { return json ?? (json = new JavaScriptSerializer()); } }

        public static Stream ToStream(this string @this)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(@this);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }


        public static T ParseXML<T>(this string @this) where T : class
        {
            StreamReader reader = new StreamReader(@this.Trim().ToStream());
            var readerXML = XmlReader.Create(reader, new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(readerXML) as T;
        }

        public static T ParseJSON<T>(this string @this) where T : class
        {
            return JSON.Deserialize<T>(@this.Trim());
        }

        public static string SerializeXML<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка формирования XML документа разделов 1-7", ex);
            }
        }

        public static TEnum ParseEnum<TEnum>(this string @value, bool ignoreCase = true) where TEnum : struct
        {
            TEnum tmp;
            if (!Enum.TryParse<TEnum>(@value, ignoreCase, out tmp))
            {
                tmp = new TEnum(); 
            }
            return tmp;
        }


    }
}
