﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    /// <summary>
    /// Клас - создатель объекта ограничений прав для текущего пользователя
    /// </summary>
    public class ContractorDataPolicyProvider
    {
        #region Private fields

        private Dictionary<UserRole, string> RoleIdToNameMapping =
            new Dictionary<UserRole, string>
            {
                { UserRole.Analyst, Constants.SystemPermissions.Operations.RoleAnalyst },
                { UserRole.Approver, Constants.SystemPermissions.Operations.RoleApprover },
                { UserRole.Methodologist, Constants.SystemPermissions.Operations.RoleMedodologist },
                { UserRole.Inspector, Constants.SystemPermissions.Operations.RoleInspector },
                { UserRole.Observer, Constants.SystemPermissions.Operations.RoleObserver },
                { UserRole.Manager, Constants.SystemPermissions.Operations.RoleManager }
            };

        private IAuthorizationProvider _authProvider;
        private IInspectionGroupAdapter _inspGroupAdapter;
        private IRestrictionChainsAdapter _restrChainAdapter;

        #endregion

        public ContractorDataPolicyProvider(
            IAuthorizationProvider authProvider,
            IInspectionGroupAdapter inspGroupAdapter,
            IRestrictionChainsAdapter restrChainAdapter)
        {
            Contract.Assert(authProvider != null);
            Contract.Assert(inspGroupAdapter != null);
            Contract.Assert(restrChainAdapter != null);

            _authProvider = authProvider;
            _inspGroupAdapter = inspGroupAdapter;
            _restrChainAdapter = restrChainAdapter;
        }

        public ContractorDataPolicy GetPermissions()
        {
            var currentUserRoles = GetCurrentUserRoles(_authProvider);

            var currentUserSonoCodes = new Dictionary<UserRole, IList<string>>();
            foreach (var currentUserRole in currentUserRoles)
            {
                var roleSonoCodes = _authProvider.GetUserStructContexts(PermissionType.Operation, currentUserRole);
                var roleId = GetAccessRoleId(currentUserRole);

                if (roleId.HasValue)
                    currentUserSonoCodes.Add(roleId.Value, roleSonoCodes);
            }

            var currentUserInspectionSonoGroups = new Dictionary<UserRole, IList<InspectionGroup>>();
            foreach (var currentUserRoleSono in currentUserSonoCodes)
            {
                var inspectionGroups = _inspGroupAdapter.Search(currentUserRoleSono.Value);

                currentUserInspectionSonoGroups.Add(currentUserRoleSono.Key, inspectionGroups);
            }

            var restrictionKeysList = new List<UserRestrictionKey>();
            UserRestrictionKey restrictionKeysListItem;
            foreach (var currentUserInspectionSonoGroup in currentUserInspectionSonoGroups)
            {
                restrictionKeysListItem = new UserRestrictionKey(currentUserInspectionSonoGroup.Key, null);
                restrictionKeysList.Add(restrictionKeysListItem);

                if (!currentUserInspectionSonoGroup.Value.IsNullOrEmpty())
                {
                    foreach (var groupId in currentUserInspectionSonoGroup.Value)
                    {
                        restrictionKeysListItem = new UserRestrictionKey(currentUserInspectionSonoGroup.Key, groupId);
                        restrictionKeysList.Add(restrictionKeysListItem);
                    }
                }
            }

            var configChainRestrictions = _restrChainAdapter.All();

            var currentUserChainRestrictions = configChainRestrictions.Where(x => restrictionKeysList.Contains(x.Key)).ToList();

            var ret = new ContractorDataPolicy();

            if (!currentUserChainRestrictions.IsNullOrEmpty())
            {
                ret.NotRestricted = currentUserChainRestrictions.Max(item => item.Value.NotRestricted);
                ret.AllowPurchase = currentUserChainRestrictions.Max(item => item.Value.AllowPurchase);
                ret.MaxLevelPurchase = currentUserChainRestrictions.Max(item => item.Value.MaxLevelPurchase);
                ret.MaxLevelSales = currentUserChainRestrictions.Max(item => item.Value.MaxLevelSales);
                ret.MaxNavigatorChains = currentUserChainRestrictions.Max(item => item.Value.MaxNavigatorChains);
            }

            return ret;
        }

        #region Private methods

        private List<string> GetCurrentUserRoles(IAuthorizationProvider authProvider)
        {
            string roleSign = "Роль.";

            List<string> ret = authProvider.GetUserPermissions()
                                            .Where(x => x.Name.StartsWith(roleSign))
                                            .Select(x => x.Name)
                                            .Distinct()
                                            .ToList();

            return ret;
        }

        private UserRole? GetAccessRoleId(string roleName)
        {
            UserRole? ret = null;

            if (RoleIdToNameMapping.Values.Contains(roleName))
                ret = RoleIdToNameMapping.First(x => x.Value == roleName).Key;

            return ret;
        }

        #endregion
    }
}
