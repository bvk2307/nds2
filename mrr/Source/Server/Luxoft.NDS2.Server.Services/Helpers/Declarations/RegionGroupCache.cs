﻿using Luxoft.NDS2.Server.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using IServiceProvider = Luxoft.NDS2.Server.DAL.IServiceProvider;

namespace Luxoft.NDS2.Server.Services.Helpers.Declarations
{
    public class RegionGroupCache
    {
        private const int LifeHours = 8;

        private readonly IServiceProvider _service;

        private Dictionary<string, int> _map;

        private DateTime _remapTime;

        private RegionGroupCache(IServiceProvider service)
        {
            _service = service;
        }

        private void FillMap()
        {
            _map = SimpleAdapterCreator
                .Create<CfgRegionGroup>(_service, "CFG_REGION_GROUP")
                .All()
                .ToDictionary(x => x.REGION_CODE, x => x.GROUP_ID);

            _remapTime = DateTime.Now.AddHours(LifeHours);
        }

        public int Get(string regionCode)
        {
            if (_map == null || DateTime.Compare(_remapTime, DateTime.Now) < 0)
                FillMap();

            int result;
            return _map.TryGetValue(regionCode, out result)
                ? result
                : 0;
        }

        #region Singleton

        private static volatile RegionGroupCache _instance;

        private static readonly object Lock = new object();

        public static RegionGroupCache Instance(IServiceProvider service)
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                        _instance = new RegionGroupCache(service);
                }
            }
            return _instance;
        }

        #endregion

        #region DTO
        // ReSharper disable InconsistentNaming
        // ReSharper disable UnusedAutoPropertyAccessor.Local
        class CfgRegionGroup
        {
            public int GROUP_ID { get; set; }

            public string REGION_CODE { get; set; }
        }
        // ReSharper restore UnusedAutoPropertyAccessor.Local
        // ReSharper restore InconsistentNaming
        #endregion
    }

    
}
