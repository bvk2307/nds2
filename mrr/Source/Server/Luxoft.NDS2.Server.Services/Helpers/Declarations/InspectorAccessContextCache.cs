﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.Helpers.Declarations
{
    class InspectorAccessContextCache
    {
        class CacheData
        {
            public CacheData(OperationAccessContext operationContext)
            {
                OperationContext = operationContext;
                ActualUpTo = DateTime.Now.AddHours(LiveHours);
            }

            public OperationAccessContext OperationContext { get; private set; }

            public DateTime ActualUpTo { get; private set; }

        }

        private const int LiveHours = 8;
        
        private readonly Dictionary<string, CacheData> _map =
            new Dictionary<string, CacheData>(); 
        
        private InspectorAccessContextCache() { }

        public OperationAccessContext Get(string userSid)
        {
            CacheData data;
            if (_map.TryGetValue(userSid, out data) && DateTime.Compare(data.ActualUpTo, DateTime.Now) > 0)
            {
                return data.OperationContext;
            }
            return null;
        }

        public void Set(string userSid, OperationAccessContext operationContext)
        {
            _map[userSid] = new CacheData(operationContext);
        }

        #region Singleton

        private static volatile InspectorAccessContextCache _instance;

        private static readonly object Lock = new object();

        public static InspectorAccessContextCache Instance()
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                        _instance = new InspectorAccessContextCache();
                }
            }
            return _instance;
        }

        #endregion
    }
}
