﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.Helpers.Declarations
{
    class SignificantChagesDictionaryCache
    {

        private SignificantChagesDictionaryCache()
        {
            
        }

        public Dictionary<string, string> Cache { get; set; }

        #region Singleton

        private static volatile SignificantChagesDictionaryCache _instance;

        private static readonly object Lock = new object();

        public static SignificantChagesDictionaryCache Instance()
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                        _instance = new SignificantChagesDictionaryCache();
                }
            }
            return _instance;
        }

        #endregion
    }
}
