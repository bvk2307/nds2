﻿using System;
using System.Data;
using System.Data.Common;

namespace Luxoft.NDS2.Server.Services.Helpers
{
    public static class DataReaderExtension
    {
        public static string ReadString(this IDataReader reader, string name)
        {
            //Debug.WriteLine(name);
            return IsDbNull(reader[name]) ? String.Empty : Convert.ToString(reader[name]);
        }
        public static string ReadString(this IDataReader reader, int index)
        {
            return IsDbNull(reader[index]) ? String.Empty : Convert.ToString(reader[index]);
        }

        public static string ReadString(this DbDataReader reader, string name)
        {
            //Debug.WriteLine(name);
            return IsDbNull(reader[name]) ? String.Empty : Convert.ToString(reader[name]);
        }
        public static string ReadString(this DbDataReader reader, int index)
        {
            return IsDbNull(reader[index]) ? String.Empty : Convert.ToString(reader[index]);
        }
        public static decimal ReadMoney(this IDataReader reader, string name, Int64 defaultValue = 0)
        {
            //Debug.WriteLine(name);
            Decimal result;
            return IsDbNull(reader[name]) || !Decimal.TryParse(reader[name].ToString(), out result)
                       ? defaultValue
                       : Convert.ToDecimal(result);
        }
        public static Int64 ReadInt64(this IDataReader reader, string name, Int64 defaultValue = 0)
        {
            //Debug.WriteLine(name);
            Int64 result;
            return IsDbNull(reader[name]) || !Int64.TryParse(reader[name].ToString(), out result)
                       ? defaultValue
                       : result;
        }
        public static Int64 ReadInt64(this IDataReader reader, int index, Int64 defaultValue = 0)
        {
            Int64 result;
            return IsDbNull(reader[index]) || !Int64.TryParse(reader[index].ToString(), out result)
                       ? defaultValue
                       : result;
        }

        public static Int32  ReadInt(this IDataReader reader, string name, Int32 defaultValue = 0)
        {
            //Debug.WriteLine(name);
            object value = reader[name];
            Int32 result;
            return IsDbNull(value) || !Int32.TryParse(value.ToString(), out result)
                       ? defaultValue
                       : result;
        }
        public static Int32 ReadInt(this IDataReader reader, int index, Int32 defaultValue = 0)
        {
            Int32 result;
            return IsDbNull(reader[index]) || !Int32.TryParse(reader[index].ToString(), out result)
                       ? defaultValue
                       : result;
        }

        public static Int32? ReadNullableInt(this IDataReader reader, string name)
        {
            //Debug.WriteLine(name);
            Int32 result;
            return IsDbNull(reader[name]) || !Int32.TryParse(reader[name].ToString(), out result)
                       ? (Int32?)null
                       : result;
        }

        public static DateTime? ReadDateTime(this IDataReader reader, string name)
        {
            //Debug.WriteLine(name);
            if (!IsDbNull(reader[name]))
            {
                DateTime result;
                if (DateTime.TryParse(Convert.ToString(reader[name]), out result))
                {
                    return result;
                }
            }

            return null;
        }
        public static DateTime? ReadDateTimeRemoveMax(this IDataReader reader, string name, DateTime maxDate)
        {
            //Debug.WriteLine(name);
            if (!IsDbNull(reader[name]))
            {
                DateTime result;
                if (DateTime.TryParse(Convert.ToString(reader[name]), out result))
                {
                    if (result == maxDate)
                        return null;
                    return result;
                }
            }

            return null;
        }
        public static DateTime? ReadDateTime(this IDataReader reader, int index)
        {
            if (!IsDbNull(reader[index]))
            {
                DateTime result;
                if (DateTime.TryParse(Convert.ToString(reader[index]), out result))
                {
                    return result;
                }
            }

            return null;
        }

        public static Decimal ReadDecimal(this IDataReader reader, string name, Decimal defaultValue = 0)
        {
            //Debug.WriteLine(name);
            Decimal result;
            return IsDbNull(reader[name]) || !Decimal.TryParse(reader[name].ToString(), out result)
                       ? defaultValue
                       : result;
        }

        public static Decimal? ReadDecimalNullable(this IDataReader reader, string name, Decimal? defaultValue = null)
        {
            //Debug.WriteLine(name);
            decimal val;
            return IsDbNull(reader[name]) || !Decimal.TryParse(reader[name].ToString(), out val)
                       ? defaultValue
                       : val;
        }


        public static bool ReadBoolean(this IDataReader reader, string name)
        {
            //Debug.WriteLine(name);
            return reader.ReadInt(name, 0) > 0;
        }

        public static bool IsDbNull(object data)
        {
            return data == DBNull.Value;
        }
    }
}
