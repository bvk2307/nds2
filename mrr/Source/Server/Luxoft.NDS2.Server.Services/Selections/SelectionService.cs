﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using CommonComponents.Utils;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Selections;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Server.DAL.Selections.Hive;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Exceptions;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL.Selections.Hive.ExpressionWriter;
using Luxoft.NDS2.Server.DAL.Implementation.Selections;
using Luxoft.NDS2.Server.Services.Managers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Server.DAL.SystemSettings;

namespace Luxoft.NDS2.Server.Services.Selections
{
    [CommunicationContract(typeof(ISelectionService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class SelectionService : ISelectionService
    {
        private const uint MAX_REGIONS_COUNT = 200; 
        private readonly ServiceHelpers _helper;
        private readonly IAuthorizationProvider _localAuthProvider;
        private readonly ISelectionPackageAdapter _packageAdapter;
        private readonly IUserInfoService _userInfoService;
        private const string SortIndex = "SORT_INDEX";
        private const string OperationCodesBit = "OperationCodesBit";

        public SelectionService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
            _userInfoService = services.Get<IUserInfoService>();
            _packageAdapter = TableAdapterCreator.SelectionPackageAdapter(_helper);
        }

        /// <summary>
        /// Загружает список выборок
        /// </summary>
        /// <param name="qc"></param>
        /// <returns></returns>
        public OperationResult<PageResult<SelectionListItemData>> Search(QueryConditions qc)
        {
            StatusFilterSubstitution(qc);
            return _helper.Do(
             () =>
             {
                 using (var connection = new SingleConnectionFactory(_helper))
                 {
                     var adapter = SelectionsAdapterCreator
                         .SelectionAgregateAdapter(_helper, connection);

                     var allQuery = qc.Clone() as QueryConditions;
                     allQuery.Filter.RemoveAll(x => x!=null);

                     var result = adapter
                             .Page<SelectionListItemData>(
                                 allQuery.Filter.ToFilterGroup(),
                                 qc.Filter.ToFilterWithGroupOperator(),
                                 qc);
                     return result;
                 }
             });
        }

        public OperationResult<List<SelectionListItemData>> SearchByTemplate(long? templateId, QueryConditions qc)
        {
            StatusFilterSubstitution(qc);
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = SelectionsAdapterCreator
                            .SelectionAgregateAdapter(_helper, connection);
                        templateId = templateId == 0 || templateId == null ? -1 : templateId;
                        QueryConditionsHelper.AddFilter(
                            qc,
                            TypeHelper<SelectionListItemData>.GetMemberName(p => p.TemplateId),
                            FilterQuery.FilterLogicalOperator.And,
                            templateId,
                            ColumnFilter.FilterComparisionOperator.Equals);
                        return adapter.Search(qc.Filter.ToFilterGroup(), qc.Sorting, 0, MAX_REGIONS_COUNT).ToList();
                    }
                });
        }

        /// <summary>
        /// заменить условие StatusId = 0 на StatusId = Null
        /// </summary>
        /// <param name="queryContext"></param>
        private static void StatusFilterSubstitution(QueryConditions queryContext){
           queryContext.Filter.Where(query =>
                query.ColumnName == TypeHelper<SelectionListItemData>.GetMemberName(t => t.StatusId)).ToList().
                ForEach(x => x.Filtering.Where(y => y.Value.Equals(0)).ToList().ForEach(z => z.Value = DBNull.Value));
        }

        /// <summary>
        /// Список всех возможных статусов Выборки
        /// </summary>
        /// <returns></returns>
        public OperationResult<List<DictionaryItem>> StatusDictionary()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return SelectionsAdapterCreator.SelectionStatusDictionaryAdapter(_helper, connection).All();
                    }
                });
        }

        /// <summary>
        /// Список всех возможных причин исключения
        /// </summary>
        /// <returns></returns>
        public OperationResult<List<DictionaryItem>> ExcludeReasonDictionary()
        {
            return _helper.Do(
                () =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        return SelectionsAdapterCreator.ExcludeReasonDictionaryAdapter(_helper, connection).All();
                    }
                });
        }

        public OperationResult<PageResult<SelectionDeclaration>> SearchDeclarations(
            long selectionId, 
            QueryConditions qc)
        {
            DeclarartionFilterSubstitution(qc);

            return _helper.Do(
                    () =>
                    {
                        using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            var selectionDeclarationAdpater = SelectionsAdapterCreator
                                .SelectionDeclarationAdapter(_helper, connectionFactory);

                            var allQuery = qc.Clone() as QueryConditions;
                            allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                            QueryConditionsHelper.AddFilter(
                                qc,
                                TypeHelper<SelectionDeclaration>.GetMemberName(p => p.SelectionId),
                                FilterQuery.FilterLogicalOperator.And,
                                selectionId,
                                ColumnFilter.FilterComparisionOperator.Equals);

                            QueryConditionsHelper.AddFilter(
                             allQuery,
                              TypeHelper<SelectionDeclaration>.GetMemberName(p => p.SelectionId),
                             FilterQuery.FilterLogicalOperator.And,
                             selectionId,
                             ColumnFilter.FilterComparisionOperator.Equals);

                            return selectionDeclarationAdpater.Page(
                                    allQuery.Filter.ToFilterGroup(),
                                    qc.Filter.ToFilterGroup(), 
                                    qc);
                        }
                    }
                );
        }

        /// <summary>
        /// заменить условия в выборке деклараций
        /// </summary>
        /// <param name="queryContext"></param>
        private static void DeclarartionFilterSubstitution(QueryConditions queryContext)
        {
            queryContext.Filter.SingleOrDefault(query =>
                query.ColumnName == TypeHelper<SelectionDeclaration>.GetMemberName(t => t.Type)).
                IfNotNull(query =>
                {
                    query.ColumnName = TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TypeId);
                });

            queryContext.Filter.SingleOrDefault(query =>
             query.ColumnName == TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TaxPeriod)).
             IfNotNull(query =>
             {
                 query.ColumnName = TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TaxPeriodId);
             });

            queryContext.Sorting.SingleOrDefault(query =>
              query.ColumnKey == TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TaxPeriod)).
              IfNotNull(query =>
              {
                  query.ColumnKey = TypeHelper<SelectionDeclaration>.GetMemberName(t => t.TaxPeriodId);
              });

            queryContext.Sorting.SingleOrDefault(query =>
             query.ColumnKey == TypeHelper<SelectionDeclaration>.GetMemberName(t => t.ExcludeFromClaimType)).
             IfNotNull(query =>
             {
                 query.ColumnKey = SortIndex;
             });
        }

        /// <summary>
        /// заменить условия в выборке расхождений
        /// </summary>
        /// <param name="queryContext"></param>
        private static void DiscrepancyFilterSubstitution(QueryConditions queryContext)
        {
            queryContext.Filter.SingleOrDefault(query =>
               query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.Subtype)).
               IfNotNull(query =>
               {
                   if (query.Filtering.Any(x => x.Value.Equals(0)))
                   {
                       var filter = query.Filtering.SingleOrDefault(x => x.Value.Equals(0));
                       filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotEquals;
                       filter.Value = 1;
                   }
               });

            queryContext.Filter.SingleOrDefault(query =>
              query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SidePrimaryProcessing)).
              IfNotNull(query =>
              {
                  if (query.Filtering.Any(x => x.Value.Equals(SidePrimaryProcessing.Seller)))
                  {
                      var filter = query.Filtering.SingleOrDefault(x => x.Value.Equals(SidePrimaryProcessing.Seller));
                      filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThan;
                      filter.Value = 1;
                  }
                  else
                  {
                      var filter = query.Filtering.SingleOrDefault(x => x.Value.Equals(SidePrimaryProcessing.Buyer));
                      filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo;
                      filter.Value = 1;
                  }
              });


            queryContext.Filter.SingleOrDefault(query =>
              query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerControlRatio)).
              IfNotNull(query =>
              {
                  if (query.Filtering.Any(x => x.Value.Equals(BuyerControlRatio.NotCalculated)))
                  {
                      var filter = query.Filtering.SingleOrDefault(x => x.Value.Equals(BuyerControlRatio.NotCalculated));
                      filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThan;
                      filter.Value = 1;
                  }
              });

            queryContext.Filter.SingleOrDefault(query =>
             query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerDeclarationPeriod)).
             IfNotNull(query =>
             {
                 query.ColumnName = TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerDeclarationPeriodId);
             });

            queryContext.Filter.SingleOrDefault(query =>
             query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SellerControlRatio)).
             IfNotNull(query =>
             {
                if (query.Filtering.Any(x => x.Value.Equals(SellerControlRatio.NotCalculated)))
                {
                    var filter = query.Filtering.SingleOrDefault(x => x.Value.Equals(SellerControlRatio.NotCalculated));
                    filter.ComparisonOperator = ColumnFilter.FilterComparisionOperator.GreaterThan;
                    filter.Value = 1;
                }
            });

            queryContext.Filter.SingleOrDefault(query =>
            query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SellerDeclarationPeriod)).
            IfNotNull(query =>
            {
                query.ColumnName = TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SellerDeclarationPeriodId);
            });
            
            queryContext.Filter.SingleOrDefault(query =>
               query.ColumnName == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.ExcludeFromClaimType)).
               IfNotNull(query =>
               {
                   if (query.Filtering.Any(x =>x.Value == DBNull.Value))
                   {
                       var filter = query.Filtering.SingleOrDefault(x =>  x.Value == DBNull.Value);
                       filter.Value = 0;
                   }
               });

            queryContext.Sorting.SingleOrDefault(query =>
              query.ColumnKey == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerDeclarationPeriod)).
              IfNotNull(query =>
              {
                  query.ColumnKey = TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerDeclarationPeriodId);
              });

            queryContext.Sorting.SingleOrDefault(query =>
            query.ColumnKey == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SellerDeclarationPeriod)).
            IfNotNull(query =>
            {
                query.ColumnKey = TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SellerDeclarationPeriodId);
            });

            queryContext.Sorting.SingleOrDefault(query =>
            query.ColumnKey == TypeHelper<SelectionDeclaration>.GetMemberName(t => t.ExcludeFromClaimType)).
            IfNotNull(query =>
            {
                query.ColumnKey = SortIndex;
            });

            queryContext.Sorting.SingleOrDefault(query =>
                    query.ColumnKey == TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.BuyerOperation)).
                IfNotNull(query =>
                {
                    query.ColumnKey = OperationCodesBit;
                });
        }

        public OperationResult<PageResult<SelectionDiscrepancy>> SearchDiscrepancies(
            long selectionId,
            bool firstSide,
            QueryConditions qc)
        {
            DiscrepancyFilterSubstitution(qc);
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        if (!qc.Sorting.Any())
                        {
                            qc.Sorting.Add(
                                new ColumnSort()
                                {
                                    ColumnKey =
                                        TypeHelper<Discrepancy>.GetMemberName(x => x.DiscrepancyId)
                                });
                        }

                        var adapter = SelectionsAdapterCreator.SelectionDiscrepancyAdapter(_helper, connectionFactory);

                        var allQuery = qc.Clone() as QueryConditions;
                        allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                        QueryConditionsHelper.AddFilter(
                                allQuery,
                                TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SelectionId),
                                FilterQuery.FilterLogicalOperator.And,
                                selectionId,
                                ColumnFilter.FilterComparisionOperator.Equals);

                        QueryConditionsHelper.AddFilter(
                               qc,
                               TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SelectionId),
                               FilterQuery.FilterLogicalOperator.And,
                               selectionId,
                               ColumnFilter.FilterComparisionOperator.Equals);

                        QueryConditionsHelper.AddFilter(
                               allQuery,
                               TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SidePrimaryProcessing),
                               FilterQuery.FilterLogicalOperator.And,
                               firstSide ? 1 : 2,
                               ColumnFilter.FilterComparisionOperator.Equals);

                        QueryConditionsHelper.AddFilter(
                               qc,
                               TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SidePrimaryProcessing),
                               FilterQuery.FilterLogicalOperator.And,
                               firstSide ? 1 : 2,
                               ColumnFilter.FilterComparisionOperator.Equals);

                        return adapter.Page(
                            allQuery.Filter.ToFilterGroup(),
                            qc.Filter.ToFilterGroup(),
                            qc);
                    }
                });
        }

        /// <summary>
        /// Возвращает полный список расхождений
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="firstSide"></param>
        /// <param name="qc"></param>
        /// <returns></returns>
        public OperationResult<List<SelectionDiscrepancy>> AllDiscrepancies(
            long selectionId,
            bool firstSide,
            QueryConditions qc)
        {
            DiscrepancyFilterSubstitution(qc);
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        if (!qc.Sorting.Any())
                        {
                            qc.Sorting.Add(
                                new ColumnSort()
                                {
                                    ColumnKey =
                                        TypeHelper<Discrepancy>.GetMemberName(x => x.DiscrepancyId)
                                });
                        }

                        var adapter = SelectionsAdapterCreator.SelectionDiscrepancyAdapter(_helper, connectionFactory);
                    
                        QueryConditionsHelper.AddFilter(
                                   qc,
                                  TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SelectionId),
                                   FilterQuery.FilterLogicalOperator.And,
                                   selectionId,
                                   ColumnFilter.FilterComparisionOperator.Equals);

                        QueryConditionsHelper.AddFilter(
                               qc,
                               TypeHelper<SelectionDiscrepancy>.GetMemberName(t => t.SidePrimaryProcessing),
                               FilterQuery.FilterLogicalOperator.And,
                               firstSide ? 1 : 2,
                               ColumnFilter.FilterComparisionOperator.Equals);

                        return adapter.Search(
                            qc.Filter.ToFilterGroup(),
                            qc.Sorting,
                            qc.PaginationDetails.RowsToSkip,
                            (uint)qc.PaginationDetails.RowsToTake).ToList();
                    }
                });
        }
        /// <summary>
        /// Список записий истории
        /// </summary>select
        /// <param name="selectionId">ID выборки</param>
        /// <returns></returns>
        public OperationResult<List<ActionHistory>> GetActionsHistory(long selectionId)
        {
            return _helper.Do(() => TableAdapterCreator.SelectionPackageAdapter(_helper).GetActionsHistory(selectionId));
        }

        /// <summary>
        /// Список границ отбора по регионам
        /// </summary>select
        /// <param name="selectionTemplateId">ID выборки</param>
        /// <returns></returns>
        public OperationResult<List<SelectionRegionalBoundsDataItem>> GetRegionalBounds(long selectionTemplateId)
        {
            return _helper.Do(
               () =>
               {
                   using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                   {
                       var qc = new QueryConditions();
                     
                       var adapter = SelectionsAdapterCreator.SelectionRegionalBoundsSearchAdapterAdapter(_helper, connectionFactory);

                       QueryConditionsHelper.AddFilter(
                               qc,
                               TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(t => t.SelectionTemplateId),
                               FilterQuery.FilterLogicalOperator.And,
                               selectionTemplateId,
                               ColumnFilter.FilterComparisionOperator.Equals);

                       qc.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<SelectionRegionalBoundsDataItem>.GetMemberName(t => t.RegionCode) });

                       return adapter.Search(qc.Filter.ToFilterGroup(), qc.Sorting, 0, MAX_REGIONS_COUNT).ToList();
                   }
               });
        }

        /// <summary>
        /// Границы отбора по региону для выборки по шаблону
        /// </summary>select
        /// <param name="selectionTemplateId">ID выборки</param>
        /// <returns></returns>
        public OperationResult<SelectionRegionalBoundsDataItem> GetRegionalBounds(long selectionTemplateId, string regionalCode)
        {
            return _helper.Do(
               () =>
               {
                   using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                   {

                       var adapter = SelectionsAdapterCreator.SelectionAdapter(_helper, connectionFactory);

                       return adapter.GetRegionalBounds(selectionTemplateId, regionalCode);
                   }
               });
        }

        public OperationResult UpdateRegionalBounds(long templateId, List<SelectionRegionalBoundsDataItem> items)
        {

            return _helper.Do(
               () =>
               {
                   using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                   {
                       var adapter = SelectionsAdapterCreator.SelectionRegionalBoundsAdapterAdapter(_helper, connectionFactory);
                       foreach (var item in items)
                       {
                           adapter.UpdateSelectionRegionalBounds(templateId, item);
                       }
                   }
               });
        }

        public OperationResult<SelectionRequestResult> RequestSelection(
             Selection selection,
            SelectionType type,
            Filter filter,
            ActionType action )
        {
            var requestTemplate = _helper.GetConfigurationValue("SVK.SelectionRequestUrlTeplmplate");
            var method = _helper.GetConfigurationValue("SVK.SyncWhiteListsMethod");
            var timeOut = int.Parse(_helper.GetConfigurationValue("SVK.SyncWhiteListsTimeout"));

            OperationResult<SelectionRequestResult> result = new OperationResult<SelectionRequestResult>();
            SelectionRequestResult requestResult = new SelectionRequestResult();
            result.Result = requestResult;
            try
            {
                List<EditorParameter> editorParameters;
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    editorParameters = SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .GetFilterEditorParameters(type).ToList();
                    if (type == SelectionType.Template || type == SelectionType.SelectionByTemplate)
                    {
                        editorParameters.Add(new EditorParameter() {AggregateName = AggregateColumnNames.RegionCode, Id = 1});
                    }
                }

                HiveQueryBuilder hiveBuilder = new HiveQueryBuilder(_helper, this, editorParameters);
                var qResult = hiveBuilder.BuildSelectionQuery(filter);
                requestResult.QueryText = qResult.QueryText;
                var dalResult = TableAdapterCreator.SelectionPackageAdapter(_helper).CreateRequest(selection.Id, qResult.QueryText);

                result.Result = requestResult;
                result.Status = ResultStatus.Success;

                const int maxAttempts = 3;
                int requestAttempts = maxAttempts;
                bool successRequest = false;
                while (requestAttempts-- >= 0)
                {
                    try
                    {
                        WebRequest request = WebRequest.Create(string.Format(requestTemplate, dalResult));
                        request.Method = method;
                        request.Timeout = timeOut;
                        using (var webResponse = request.GetResponse())
                        {
                            webResponse.Close();
                            successRequest = true;
                        }

                        break;
                    }
                    catch (Exception e)
                    {
                        _helper.LogWarning(string.Format(
                            "Запрос на построение выборки {0}, попытка {1}, ошибка: {2}",
                            selection.Id,
                            maxAttempts - requestAttempts,
                            e.Message));
                    }
                }

                if (!successRequest)
                {
                    throw new Exception("Превышено максимальное кол-во попыток на отправку запроса");
                }
            }
            catch (Exception e)
            {
                result.Status = ResultStatus.Error;
                result.Message = e.Message;
            }

            return result;
        }

        #region Основные

        public string GenerateSelectionName(SelectionType type)
        {
            return _packageAdapter.GenerateUniqueName(_localAuthProvider.CurrentUserSID, type);
        }


        public bool IsNameUnique(string name, long id, SelectionType type)
        {
            var uniqueName = _packageAdapter.VerifyUniqueName(_localAuthProvider.CurrentUserSID, name, id, type);
            return uniqueName == name;
        }


        public OperationResult<Selection> Delete(Selection selection, SelectionCommandParam param)
        {
            const string permission = Constants.SystemPermissions.Operations.SelectionRemove;
            if (!CanBeDeleted(selection, param.LockKey))
            {
                var result = new OperationResult<Selection>();
                _helper.DenyWork(result, permission);
                return result;
            }
            return _helper.Do(() => ChangeSelectionStatus(selection, SelectionStatus.Deleted, ActionType.Edit), new[] { permission });
        }

        public OperationResult<Selection> CreateClaim(Selection selection)
        {
            return _helper.Do(() => ChangeSelectionStatus(selection, SelectionStatus.ClaimCreating, ActionType.ClaimCreate));
        }

        private readonly Dictionary<string, Func<Selection, IAuthorizationProvider, bool>> _deleteRoleToStatusRestrictions =
            new Dictionary<string, Func<Selection, IAuthorizationProvider, bool>>
            {
                {
                    Constants.SystemPermissions.Operations.RoleAnalyst,
                    (selection, ias) => ias.CurrentUserSID == selection.AnalyticSid && selection.TypeCode == SelectionType.Hand &&
                                        (selection.Status == SelectionStatus.Draft ||
                                         selection.Status == SelectionStatus.LoadingError)
                },
                {
                    Constants.SystemPermissions.Operations.RoleApprover,
                    (selection, ias) => selection.TypeCode == SelectionType.Hand &&
                        (selection.Status == SelectionStatus.RequestForApproval || selection.Status == SelectionStatus.Approved)
                },
                 {
                    Constants.SystemPermissions.Operations.SelectionTemplateRemove,
                    (selection, ias) => selection.TypeCode == SelectionType.Template && selection.ManagerSid == ias.CurrentUserSID 
                }
            };

        private const char BlockersSplitter = ';';

        /// <summary>
        /// Проверяет может ли выборка быть удалена текущим пользователем в текущем состоянии:
        /// Выборку нельзя удалить, если:
        /// а. Ни одной из ролей пользователя не позволено удалять выборку в текущем статусе согласно бизнес-правилам
        /// б. С выборкой в данный момент работает другой пользователь
        /// </summary>
        /// <param name="selection">Ссылка на выборку</param>
        /// <returns>Стандартный ответ сервера МРР от логического типа результата</returns>
        public OperationResult<bool> CanBeDeleted(Selection selection)
        {
            if (!DeleteAllowedByRole(selection))
            {
                return new OperationResult<bool>
                {
                    Status = ResultStatus.Success,
                    Result = false
                };
            }

            return _helper.DoEx(() => !IsLocked(selection.Id));
        }

        private bool CanBeDeleted(Selection selection, string lockKey)
        {
            bool ret = DeleteAllowedByRole(selection);

            if (ret && IsLocked(selection.Id))
            {
                ret = TableAdapterCreator.ObjectLocker(_helper).CheckLock(selection.Id, (int)ObjectType.Selection, lockKey);
            }

            return ret;
        }

        /// <summary>
        /// Определяет возможность удаления шаблона в зависимости от статусов выборок в нем
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public OperationResult<bool> CanDeleteTemplate(long templateId)
        {
            return _helper.DoEx(() => _packageAdapter.CanDeleteTemplate(templateId));
        }

        private bool IsLocked(long selectionId)
        {
            return Lockers(selectionId).Any();
        }

        private string[] Lockers(long selectionId)
        {
            var blockers = TableAdapterCreator.ObjectLocker(_helper).GetLockers(selectionId, (int)ObjectType.Selection);

            return string.IsNullOrWhiteSpace(blockers)
                ? new string[0]
                : blockers.Split(
                        new[] { BlockersSplitter },
                        StringSplitOptions.RemoveEmptyEntries)
                  .ToArray();
        }

        /// <summary>
        /// Проверяет доступность удаления текущим пользователем выборки в зависимости от ролей пользователя и статуса выборки
        /// </summary>
        /// <param name="selection">ССылка на выборку</param>
        /// <returns>Да, если удаление разрешено</returns>
        private bool DeleteAllowedByRole(Selection selection)
        {
            return _deleteRoleToStatusRestrictions.Any(
                    roleStatuses => _localAuthProvider.IsUserInRole(roleStatuses.Key)
                        && roleStatuses.Value(selection, _localAuthProvider));
        }

        public OperationResult<Selection> SendToAproval(Selection selection)
        {
            return _helper.Do(
                () => ChangeSelectionStatus(selection, SelectionStatus.RequestForApproval, ActionType.RequestForApproval),
            new[] { Constants.SystemPermissions.Operations.SelectionToApprove }
            );
        }

        public OperationResult Approve(Selection selection, List<long> selIds)
        {
            if(selection.TypeCode != SelectionType.Template)
            return _helper.Do(
                () =>
                {
                    selection.Manager = _helper.UserDisplayName;
                    selection.ManagerSid = _localAuthProvider.CurrentUserSID;
                    ChangeSelectionStatus(selection, SelectionStatus.Approved, ActionType.Approval);
                },

            new[] { Constants.SystemPermissions.Operations.SelectionApproved }
            );
            else
            {
                UpdateUserInfo();

                foreach (var item in selIds)
                {
                    _helper.Do(() => _packageAdapter.Approve(item, GetCurrentUserSid()));
                }
            }
            return new OperationResult(){Status = ResultStatus.Success};
        }



        public OperationResult SendBackForCorrections(Selection selection, List<long> selIds)
        {
            if(selection.TypeCode != SelectionType.Template )
             _helper.Do(
                () => ChangeSelectionStatus(selection, SelectionStatus.Draft, ActionType.ReturnedForCorrection),
            new[] { Constants.SystemPermissions.Operations.SelectionToCorrect }
            );
            else
            {
                UpdateUserInfo();

                foreach (var item in selIds)
                {
                    _helper.Do(() => _packageAdapter.SendForCorrection(item, GetCurrentUserSid()));
                }
            }
            return new OperationResult() { Status = ResultStatus.Success };
        }

        public OperationResult UpdatetSelectionStatusAfterHiveLoad(long selectionId, SelectionType type, ActionType action)
        {
            return _helper.Do(
                () => _packageAdapter.UpdateSelectionStateAfterHiveLoad(selectionId, type, _helper.CurrentUserSID, action)
            );
        }

        private Selection ChangeSelectionStatus(
            Selection selectionObj,
            SelectionStatus newStatus,
            ActionType action)
        {
            ValidateChangeSelection((SelectionStatus)selectionObj.Status, newStatus);
            selectionObj.Status = newStatus;
            selectionObj.StatusDate = DateTime.UtcNow;
            return SaveInternal(selectionObj, action);
        }

        private OperationResult<Selection> UpdateSelectionAggreagte(Selection data)
        {
            return _helper.Do(() => _packageAdapter.UpdateSelectionAggregateData(data));
        }

        private void ValidateChangeSelection(SelectionStatus selectionStatus, SelectionStatus newStatus)
        {
            if (!ValidateTransition(selectionStatus, newStatus))
            {
                throw new InvalidTransitionException("Не возможно перевести выборку в данное состояние. Переход запрещен");
            }
        }

        public OperationResult<List<SelectionRequest>> SyncWithRequestStatus(long selectionId, SelectionType type)
        {
            return DalHelper.Execute(
                () =>
                {
                    var ret = new OperationResult<List<SelectionRequest>>
                    {
                        Status = ResultStatus.Success,
                        Result = _packageAdapter.SyncWithRequestProcessing(selectionId, type)
                    };
                    return ret;
                },
                _helper);
        }

        private bool ValidateTransition(SelectionStatus stateFrom, SelectionStatus stateTo)
        {
            return TableAdapterCreator.SelectionWorkflow(_helper).HasTransition((int)stateFrom, (int)stateTo);
        }

        #endregion

        #region Данные по выборке

        public OperationResult<Selection> GetSelection(long id)
        {
            return _helper.Do(() => _packageAdapter.GetSelection(id));
        }

        private void AppendRestrictionFilters(QueryConditions conditions)
        {
            string statusColumnKey = TypeHelper<Selection>.GetMemberName(m => m.Status);
            string analyticSidColumnKey = TypeHelper<Selection>.GetMemberName(m => m.AnalyticSid);

            var userAllowedStatuses = GetSelectionStatusesForUser();
            if (!userAllowedStatuses.Any()) userAllowedStatuses.Add(-1);

            var statusFilter = new FilterQuery
            {
                ColumnName = statusColumnKey,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or
            };
            conditions.Filter.Insert(0, statusFilter);

            foreach (var status in userAllowedStatuses)
            {

                statusFilter.Filtering.Add(new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = status
                });
                statusFilter.ColumnName = statusColumnKey;
            }

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleAnalyst))
            {
                var analyticFilter = new FilterQuery
                {
                    ColumnName = analyticSidColumnKey,
                    FilterOperator = FilterQuery.FilterLogicalOperator.Or
                };

                conditions.Filter.Add(analyticFilter);

                analyticFilter.Filtering.Add(new ColumnFilter
                {
                    ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                    Value = _localAuthProvider.CurrentUserSID
                });
            }
        }

        /// <summary>
        /// Список состояний, работа с которыми доступна текущему пользователю.
        /// </summary>
        /// <returns></returns>
        private List<int> GetSelectionStatusesForUser()
        {
            var allowedStatuses = new List<int>();

            Action<SelectionStatus> appendStatus = status =>
            {
                if (!allowedStatuses.Contains((int)status) && (status != SelectionStatus.Deleted))
                {
                    allowedStatuses.Add((int)status);
                }
            };

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleAnalyst)
                || _localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleMedodologist))
            {
                foreach (var value in Enum.GetValues(typeof(SelectionStatus)))
                {
                    appendStatus((SelectionStatus)value);
                }
            }

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleApprover))
            {
                appendStatus(SelectionStatus.RequestForApproval);
                appendStatus(SelectionStatus.Draft);
                appendStatus(SelectionStatus.Approved);
            }

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleSender))
            {
                appendStatus(SelectionStatus.Approved);
            }

            return allowedStatuses;
        }

        #endregion Данные по выборке
     
        #region Работа с расхождениями и декларациями от СОВ

        public OperationResult<long> RequestDiscrepancies(long selectionId)
        {
            return _helper.DoEx(
            () =>
            {
                _packageAdapter.SetSelectionState(selectionId, SelectionType.Hand,  8, _helper.UserDisplayName, ActionType.Create);

                return default(long);
            },
            new[] { Constants.SystemPermissions.Operations.SelectionFilterApply }
            );
        }

        #endregion Работа с расхождениями и декларациями от СОВ

        # region Работа с избранными фильтрами выборки

        private Implementation.FavoriteFilterServiceDelegate _favFilterDelegate;

        private Implementation.FavoriteFilterServiceDelegate FavoriteFiltersDelegate()
        {
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                return _favFilterDelegate
                    = _favFilterDelegate
                        ?? new Implementation.FavoriteFilterServiceDelegate(
                                new IdentityProvider(),
                                SelectionsAdapterCreator.SelectionFavoriteFilterAdapter(_helper, connectionFactory),
                                _helper);
            }
        }

        public OperationResult<List<KeyValuePair<long, string>>> GetFavoriteFilters()
        {
            return FavoriteFiltersDelegate().GetFavoriteFilters();
        }

        public OperationResult<Filter> GetFavoriteSelectionFilterVersionTwo(long favoriteId)
        {
            return FavoriteFiltersDelegate().GetFavoriteSelectionFilterVersionTwo(favoriteId);
        }

        public OperationResult<KeyValuePair<long, string>> CreateFavorite(string name, Filter filter)
        {
            return FavoriteFiltersDelegate().Create(name, filter);
        }

        public OperationResult OverwriteFavorite(string name, Filter filter)
        {
            return FavoriteFiltersDelegate().Overwrite(name, filter);
        }

        public OperationResult DeleteFavorite(long id)
        {
            return FavoriteFiltersDelegate().Delete(id);
        }

        # endregion

        # region Работа с декларациями и расхождениями в выборке


       

        public OperationResult IncludeDeclaration(long selectionId, long declarationId)
        {
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        SelectionsAdapterCreator
                            .SelectionDeclarationsAdapter(_helper, connectionFactory)
                            .SetDeclarationState(selectionId, declarationId, true, _userInfoService.GetCurrentUserInfo().ToContract().Sid);
                    }
                });
        }

        public OperationResult ExcludeDeclaration(long selectionId, long declarationId)
        {
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        SelectionsAdapterCreator
                            .SelectionDeclarationsAdapter(_helper, connectionFactory)
                            .SetDeclarationState(selectionId, declarationId, false, _userInfoService.GetCurrentUserInfo().ToContract().Sid);
                    }
                });
        }


        public OperationResult<SelectionGroupState> GetDeclarationGroupState(long selectionId, QueryConditions filter = null)
        {
            return _helper.DoEx(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        return SelectionsAdapterCreator
                            .SelectionDeclarationsAdapter(_helper, connectionFactory)
                            .GetDeclarationState(selectionId);
                    }
                });
        }

        public OperationResult<List<SelectionDiscrepancyStatistic>> GetDiscrepancyStatistic(
            long selectionId,
            SelectionType type)
        {
            return _helper.Do(
                () =>
                {
                     return _packageAdapter.GetDiscrepancyStatistic(selectionId, type);
                });
        }

        public OperationResult IncludeDeclarations(long selectionId, long[] zipList)
        {
            return _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    var adpater = SelectionsAdapterCreator.SelectionDeclarationsAdapter(_helper, connectionFactory);

                    adpater.SetState(selectionId, true, _userInfoService.GetCurrentUserInfo().ToContract().Sid, zipList);
                }
            });
        }

        public OperationResult ExcludeDeclarations(long selectionId, long[] zipList)
        {
            return _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    var adpater = SelectionsAdapterCreator.SelectionDeclarationsAdapter(_helper, connectionFactory);
                    adpater.SetState(selectionId, false, _userInfoService.GetCurrentUserInfo().ToContract().Sid, zipList);
                }
            });
        }

        #endregion

        #region Работа с переходами между состояниями

        public OperationResult<List<SelectionTransition>> GetStateTransitions()
        {
            return _helper.Do(() => StateSelectionDelegate().GetStateTransitions());
        }

        private Implementation.StateSelectionServiceDelegate _stateSelectionDelegate;

        private Implementation.StateSelectionServiceDelegate StateSelectionDelegate()
        {
            return _stateSelectionDelegate
                = _stateSelectionDelegate
                    ?? new Implementation.StateSelectionServiceDelegate(
                            new IdentityProvider(),
                            TableAdapterCreator.StateTransition(_helper),
                            _helper);
        }

        # endregion

        public OperationResult<bool> CheckUserInRole(string userSid, string role)
        {
            bool ret = _localAuthProvider.IsUserInRole(userSid, role);
            return new OperationResult<bool>
            {
                Result = ret
            };
        }

        # region Загрузка данных выборки

        public OperationResult<SelectionDetails> Load(long? id, string lockKey, SelectionType type)
        {
            var user = new SelectionUser(_localAuthProvider, _helper);

            return _helper.Do(
                () =>
                {
                    var result = new SelectionDetails
                    {
                        Data = id.HasValue
                            ? _packageAdapter.GetSelection(id.Value)
                            : Selection.New(
                                GenerateSelectionName(type),
                                _localAuthProvider.CurrentUserName,
                                _localAuthProvider.CurrentUserSID, type)
                    };

                    if (!result.Data.IsNew())
                    {
                        using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            result.Data.Filter = SelectionsAdapterCreator
                                .SelectionFilterAdapter(_helper, connectionFactory)
                                .GetSelectionFilterContent(result.Data.Id) ?? new Filter();
                        }

                        var lockers = Lockers(result.Data.Id);

                        if (lockers.Any() && id.HasValue)
                        {
                            bool rez = TableAdapterCreator.ObjectLocker(_helper).CheckLock(id.Value, (int)ObjectType.Selection, lockKey);

                            result.Lockers = !rez ? lockers : new string[0];
                        }
                        else
                            result.Lockers = new string[0];
                    }
                    else
                        result.Lockers = new string[0];

                    var operations = new List<SelectionOperation>();


                    foreach (var operDesc in _localAuthProvider.GetUserPermissions().Where(x => x.PermType == PermissionType.Operation))
                    {
                        if (!operations.Exists(op => op.Id.Equals(operDesc.Name)))
                        {
                            var operation = new SelectionOperation { Id = operDesc.Name };
                            operation.IsAvailable = operation.AccessRule().IsAvailable(user, result);
                            operations.Add(operation);
                        }
                    }

                    result.ApplicableOperations = operations.ToArray();

                    return result;
                });
        }

        public OperationResult<SelectionDetails> NewFromTemplate(long id)
        {
            var user = new SelectionUser(_localAuthProvider, _helper);

            return _helper.Do(
                () =>
                {
                    var result = new SelectionDetails
                    {
                        Data = Selection.New(
                                GenerateSelectionName(SelectionType.Template),
                                _localAuthProvider.CurrentUserName,
                                _localAuthProvider.CurrentUserSID, SelectionType.Template)
                    };

                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        result.Data.Filter = (SelectionsAdapterCreator
                            .SelectionFilterAdapter(_helper, connectionFactory)
                            .GetSelectionFilterContent(id) ?? new Filter()).Copy();
                    }
                    result.Data.TemplateId = id;

                    result.Lockers = new string[0];

                    var operations = new List<SelectionOperation>();

                    foreach (var operDesc in _localAuthProvider.GetUserPermissions().Where(x => x.PermType == PermissionType.Operation))
                    {
                        if (!operations.Exists(op => op.Id.Equals(operDesc.Name)))
                        {
                            var operation = new SelectionOperation { Id = operDesc.Name };
                            operation.IsAvailable = operation.AccessRule().IsAvailable(user, result);
                            operations.Add(operation);
                        }
                    }

                    result.ApplicableOperations = operations.ToArray();
                    return result;
                });
        }
        
        public OperationResult<PageResult<Selection>> SelectSelections(QueryConditions conditions)
        {
            var selections = TableAdapterCreator.SelectionTableAdapter(_helper);

            return _helper.Do(
                () =>
                {
                    var queryContext =
                        new DataQueryContext
                        {
                            OrderBy = conditions.Sorting,
                            Skip = (int)conditions.PaginationDetails.RowsToSkip,
                            MaxQuantity = conditions.PaginationDetails.RowsToTake.HasValue
                                ? (int)conditions.PaginationDetails.RowsToTake.Value
                                : 1000000,
                            Where = BuildFilter(conditions)
                        };

                    return new PageResult<Selection>
                    {
                        Rows = selections.Search(queryContext),
                        TotalMatches = selections.Count(queryContext.Where)
                    };
                });
        }

        private FilterExpressionBase BuildFilter(QueryConditions conditions)
        {
            var userFilter = conditions.Filter.ToFilterGroup();

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleMedodologist))
            {
                return userFilter;
            }

            var retVal = FilterExpressionCreator.CreateGroup();
            retVal.WithExpression(userFilter);

            var roleFilter = FilterExpressionCreator.CreateUnion();

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleAnalyst))
            {
                roleFilter.WithExpression(
                    FilterExpressionCreator.Create(
                        TypeHelper<Selection>.GetMemberName(x => x.AnalyticSid),
                        ColumnFilter.FilterComparisionOperator.Equals,
                        _localAuthProvider.CurrentUserSID));
            }

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleApprover))
            {
                var statusFilter =
                    FilterExpressionCreator.CreateInList(
                        TypeHelper<Selection>.GetMemberName(x => x.Status),
                        new object[]
                        {
                           (int)SelectionStatus.Approved,
                           (int)SelectionStatus.RequestForApproval,
                           (int)SelectionStatus.Draft,
                        });

                roleFilter.WithExpression(statusFilter);
            }

            if (_localAuthProvider.IsUserInRole(Constants.SystemPermissions.Operations.RoleSender))
            {
                roleFilter.WithExpression(
                    FilterExpressionCreator.Create(
                        TypeHelper<Selection>.GetMemberName(x => x.Status),
                        ColumnFilter.FilterComparisionOperator.Equals,
                        (int)SelectionStatus.Approved));
            }

            retVal.WithExpression(roleFilter);

            return retVal;
        }

        # endregion

        # region Фильтр выборки (НОВЫЙ)

        public OperationResult<List<SelectionParameter>> GetFilterParameters()
        {
            return _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    return SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .GetFilterParameters().ToList();
                }
            });
        }

        public OperationResult<List<Region>> GetRegions()
        {
            return _helper.Do(() =>
            {
                var result = new List<Region>();
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    result = SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .GetRegions(_localAuthProvider.CurrentUserSID).ToList();
                }
                return result;
            });
        }

        public OperationResult<List<EditorParameter>> GetFilterEditorParameters(SelectionType type)
        {
            return _helper.Do(() =>
            {
                List<EditorParameter> editorParameters = new List<EditorParameter>();
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    editorParameters = SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .GetFilterEditorParameters(type).ToList();
                }
                return editorParameters;
            });
        }

        public OperationResult<List<KeyValuePair<string, string>>> GetFilterParameterOptions(int parameterId)
        {
            return _helper.Do(() =>
            {
                List<KeyValuePair<string, string>> parameterOptions;
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    if (ParameterIsPeriod(parameterId))
                        parameterOptions = _helper.SelectionFilterAdapter(connectionFactory)
                            .GetPeriods().ToList();
                    else
                        parameterOptions = _helper.SelectionFilterAdapter(connectionFactory)
                            .GetFilterParameterOptions(parameterId).ToList();
                }
                return parameterOptions;
            }
            );
        }

        public OperationResult<List<KeyValuePair<string, string>>> GetWhiteLists()
        {
            return _helper.Do(() =>
            {
                List<KeyValuePair<string, string>> lists;
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    lists = _helper.SelectionFilterAdapter(connectionFactory)
                        .GetWhiteLists().ToList();

                }
                return lists;
            }
            );
        }

        public OperationResult<List<string>> GetInnFromWhiteList(long whiteListId)
        {
            return _helper.Do(() =>
            {
                List<string> lists;
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    lists = SystemSettingsAdapterCreator.SystemSettingsListsAdapter(
                    _helper, connectionFactory).LoadInnList(whiteListId).ToList();

                }
                return lists;
            }
            );
        }

        private bool ParameterIsPeriod(int parameterId)
        {
            return (parameterId == EditorParameterConst.PERIOD_FULL_BUYER ||
                    parameterId == EditorParameterConst.PERIOD_FULL_SELLER);
        }

        public OperationResult SaveFilter(long selectionId, Filter filter)
        {
            return _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .SaveFilter(selectionId, filter);
                }
            });
        }


        #endregion
        public OperationResult<Selection> Save(Selection data, ActionType action)
        {
            return _helper.Do(
                () => SaveInternal(data, action),
                new[]
                {
                    Constants.SystemPermissions.Operations.SelectionFilterApply
                });
        }

        private Selection SaveInternal(Selection data, ActionType action)
        {
            data.Name = _packageAdapter.VerifyUniqueName(_localAuthProvider.CurrentUserSID, data.Name, data.IsNew() ? -1 : data.Id, data.TypeCode);

            if(data.IsNew() || action == ActionType.SecondaryApply)
                data.Status = data.TypeCode != SelectionType.Template ? SelectionStatus.Loading : (SelectionStatus?)null;

            UpdateUserInfo();

            if (data.TypeCode == SelectionType.Template)
            {
                data.Manager = _localAuthProvider.CurrentUserName;
                data.ManagerSid = _localAuthProvider.CurrentUserSID;
            }
            else
            if(data.TypeCode == SelectionType.SelectionByTemplate)
            {
                if(action == ActionType.SecondaryApply)
                {
                    data.AnalyticSid = string.Empty;
                    data.Analytic = string.Empty;
                    data.ManagerSid = string.Empty;
                    data.Manager = string.Empty;
                }
                else if(action == ActionType.ReturnedForCorrection || action == ActionType.Approval)
                {
                    data.ManagerSid = _localAuthProvider.CurrentUserSID;
                    data.Manager = _localAuthProvider.CurrentUserName;
                }
                else if (!string.IsNullOrEmpty(data.AnalyticSid))
                {
                    data.AnalyticSid = _localAuthProvider.CurrentUserSID;
                    data.Analytic = _localAuthProvider.CurrentUserName;
                }
            }
            else if (action == ActionType.ReturnedForCorrection || action == ActionType.Approval)
            {
                data.ManagerSid = _localAuthProvider.CurrentUserSID;
                data.Manager = _localAuthProvider.CurrentUserName;
            }
            else
            {
                data.AnalyticSid = _localAuthProvider.CurrentUserSID;
                data.Analytic = _localAuthProvider.CurrentUserName;
            }
            return _packageAdapter.SaveSelection(data, action);
        }

        private void UpdateUserInfo()
        {
            _helper.UserAdapter().InsertOrUpdate(_userInfoService.GetCurrentUserInfo().ToContract());
        }

        public OperationResult DeleteSelectionsByTemplate(long templateId)
        {
            UpdateUserInfo();
            return _helper.Do(
                () =>
                {
                    using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        SelectionsAdapterCreator.SelectionStatusAdapter(_helper, connectionFactory)
                            .DeleteSelectionsByTemplate(templateId);
                    }
                });

        }

        private const int _regionParameterId = 1;

        public OperationResult RequestSelections(
            long templateId,
            string templateName,
            Filter filter,
            List<SelectionRegionalBoundsDataItem> regionalBounds,
            ActionType action)
        {
            List<EditorParameter> editorParameters;
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                editorParameters = SelectionsAdapterCreator
                    .SelectionFilterAdapter(_helper, connectionFactory)
                    .GetFilterEditorParameters(SelectionType.Template).ToList();
           
            var editorTypeParameter = editorParameters.SingleOrDefault(x => x.AggregateName == AggregateColumnNames.TypeCode);
            //Сохряняем все выборки для шаблона
            foreach (var bounds in regionalBounds)
            {
                //Если выборка по региону для шаблона уже существует
                //Удаляем ее, либо обновляем
                var selection = SelectionsAdapterCreator.SelectionAdapter(_helper, connectionFactory)
                        .FindByTemplateId(templateId, bounds.RegionCode);
                if (bounds.Include != 1)
                {
                    if (selection != null)
                        ChangeSelectionStatus(selection, SelectionStatus.Deleted, ActionType.SecondaryApply);

                    continue;
                }

                selection = selection ?? new Selection
                {
                    TemplateId = templateId,
                    TypeCode = SelectionType.SelectionByTemplate,
                    Regions = bounds.RegionCode
                };

                selection.Name = string.Format("Регион_{0}_{1}", bounds.RegionCode, templateName);
                selection.Status = SelectionStatus.Loading;

                var savedSelection = Save(selection, selection.Id != 0 ? action : ActionType.Create);
                if (savedSelection.Status != ResultStatus.Success)
                    continue;

                selection.Id = savedSelection.Result.Id;

                SaveFilter(selection.Id, filter);
                var cloneFilter = filter.Copy();

                var groups =
                    cloneFilter.Groups.Where(
                        x => x.IsEnabled && x.Parameters.Any(y => y.AttributeId == editorTypeParameter.Id));
                if (groups.Any())
                    foreach (var group in groups)
                    {
                        var parameters = group.Parameters.ToList();

                        var regionParameter = new Parameter
                        {
                            AttributeId = _regionParameterId,
                            IsEnabled = true,
                            Operator = ComparisonOperator.Equals,
                            Values =
                                new[]
                                {
                                    new ParameterValue
                                    {
                                        Value = bounds.RegionCode,
                                        ValueDescription = bounds.RegionName
                                    }
                                }
                        };
                        parameters.Add(regionParameter);

                        var discrTypeParameters =
                            parameters.FirstOrDefault(x => x.AttributeId == editorTypeParameter.Id);

                        foreach (var curValue in discrTypeParameters.Values)
                            if (curValue.Value == ((int) DiscrepancyType.NDS).ToString())
                            {
                                curValue.ValueFrom = bounds.DiscrTotalAmt.ToString();
                                curValue.ValueTo = bounds.DiscrMinAmt.ToString();
                            }
                            else
                            {
                                curValue.ValueFrom = bounds.DiscrGapTotalAmt.ToString();
                                curValue.ValueTo = bounds.DiscrGapMinAmt.ToString();
                            }
                        group.Parameters = parameters.ToArray();
                    }
                var result = RequestSelection(selection, SelectionType.SelectionByTemplate, cloneFilter, action);
            }
            return new OperationResult();
        }
        }

        public OperationResult UpdateSelectionAggregate(long selectionId, SelectionType type)
        {
            return _helper.Do(() => _packageAdapter.UpdateSelectionAggregate(selectionId, type));
        }

        /// <summary>
        /// Обноялвяет поля Регионы в выборке, если оно изменилось после применеия фильтра
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        public OperationResult UpdateSelectionRegions(long selectionId, string regions)
        {
            return _helper.Do(() => _packageAdapter.UpdateSelectionRegions(selectionId, regions));
        }

        #region User Info

        /// <summary>
        /// Возаращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserDisplayName()
        {
            return _helper.UserDisplayName;
        }

        /// <summary>
        /// Возвращает идентификатор текущего пользователя
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserSid()
        {
            return _helper.CurrentUserSID;
        }

        #endregion

        #region Creating Claim Functionality

        public OperationResult<bool> ExistApprovedSelections()
        {
            return _helper.DoEx(() => SelectionCreatingClaimAdapterCreator.SelectionCreatingClaimAdapter(_helper).ExistApprovedSelections());
        }

        public OperationResult<SelectionByStatusesCount> CountApprovedSelections()
        {
            return _helper.Do(() => SelectionCreatingClaimAdapterCreator.SelectionCreatingClaimAdapter(_helper).CountApprovedSelections());
        }

        public OperationResult SetClaimCreatingStatus()
        {
            return _helper.Do(() => SelectionCreatingClaimAdapterCreator.SelectionCreatingClaimAdapter(_helper).SetClaimCreatingStatus());
        }

        #endregion
    }
}