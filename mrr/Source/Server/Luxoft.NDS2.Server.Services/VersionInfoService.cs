﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.IO;
using System.Collections.Generic;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.DAL.VersionHistory;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IVersionInfoService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class VersionInfoService : IVersionInfoService
    {
        private const string ClientArchivePattern = "NDS2_MRR_{0}.zip";

        private readonly ServiceHelpers _helper;

        private IAuthorizationProvider _localAuthProvider;

        public VersionInfoService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<VersionInfo> GetLatest()
        {
            return
                _helper.Do(
                    () =>
                    {
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            return VersionHistoryAdapterCreator.Create(_helper)
                                                                    .SearchLatest();
                        }
                    });
        }

        public OperationResult<UpdateInfo> GetUpdateInfo()
        {
            return _helper.Do(
                () =>
                {
                    VersionInfo version = null;
                    using (var connectionFactory = new SingleConnectionFactory(_helper))
                    {
                        version = VersionHistoryAdapterCreator.Create(_helper)
                                                                .SearchLatest();
                    }
                    var path =
                        version.Undefined
                        ? string.Empty
                        : TableAdapterCreator
                            .Configuration(_helper)
                            .Read()
                            .ClientPath
                            .Value
                            .WithFile(
                                string.Format(ClientArchivePattern, version.VersionNumber));
                    var length =
                        version.Undefined || !File.Exists(path)
                        ? default(long)
                        : new FileInfo(path).Length;

                    return new UpdateInfo
                    {
                        Version = version,
                        Path = path,
                        Length = length
                    };
                });
        }

        public byte[] GetClientChunk(
            string path,
            int offset,
            int bufferSize)
        {
            try
            {
                if (!File.Exists(path))
                    return null;

                var FileSize = new FileInfo(path).Length;
                if (offset > FileSize)
                {
                    return null;
                }

                byte[] buffer;
                int bytesRead;

                using (var fileStream
                    = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    fileStream.Seek(offset, SeekOrigin.Begin);
                    buffer = new byte[bufferSize];
                    bytesRead = fileStream.Read(buffer, 0, bufferSize);
                }

                if (bytesRead != bufferSize)
                {
                    byte[] trimmedBuffer = new byte[bytesRead];
                    Array.Copy(buffer, trimmedBuffer, bytesRead);
                    return trimmedBuffer;
                }
                else
                    return buffer;
            }
            catch
            {
                return new byte[0];
            }
        }

        public OperationResult<IEnumerable<VersionInfo>> GetVersionsWithReleaseNotes()
        {
            return
                _helper.Do(
                    () =>
                    {
                        using (var connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            return VersionHistoryAdapterCreator.Create(_helper)
                                                                .GetVersionsWithReleaseNotes();
                        }
                    });
        }
    }

    internal static class PathHelper
    {
        public static string WithFile(this string path, string fileName)
        {
            if (path[path.Length - 1] != '\\')
            {
                path = path + @"\";
            }

            return path + fileName;
        }
    }
}
