﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using Luxoft.NDS2.Server.DAL.Claims;
using Luxoft.NDS2.Server.DAL.ExplainReply;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;

namespace Luxoft.NDS2.Server.Services.Claims
{
    [CommunicationContract(typeof(IInvoiceClaimReportService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InvoiceClaimReportService : IInvoiceClaimReportService
    {
        private readonly ServiceHelpers _helper;

        public InvoiceClaimReportService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<List<SelectionClaimCompare>> GetSelectionStats(DateTime date)
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateReportSelectionStatsAdapter(_helper).All(date));
        }

        public OperationResult<List<InvoiceClaimReportItem>> GetInvoiceClaimList(DateTime date, List<FilterQuery> filter)
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateReportInvoiceClaimListAdapter(_helper).Search(date, filter));
        }

        public OperationResult<List<InvoiceClaimReportItem>> GetInvoiceClaimListByStatus(DateTime date, int[] statuses, List<FilterQuery> filter)
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateReportInvoiceClaimListByStatusAdapter(_helper, statuses).Search(date, filter));
        }
        
        public OperationResult<List<TaxMonitoringReportItem>> GetReportForChapters(long docId, int chapter)
        {
            return
                _helper.Do(
                    () =>
                    {
                       var list = InvoiceClaimAdapterCreator.CreateReportTaxMonitoringAdapter(_helper)
                            .GetForChapter(docId, chapter);
                        FillInvoices(list, docId);
                        return list;
                    });
        }

        private void FillInvoices(List<TaxMonitoringReportItem> items, long docId)
        {
            var keys = items.Select(x => x.RowKey);

            var buyers = ExplainInvoiceBuyerAdapterCreator.Create(_helper).GetContractors(keys, docId);

            var sellers = ExplainInvoiceSellerAdapterCreator.Create(_helper).GetContractors(keys, docId);

            var operations = ExplainInvoiceOperationAdapterCreator.Create(_helper).GetOperation(keys, docId);

            var buyaccepts = ExplainInvoiceBuyAcceptAdapterCreator.Create(_helper).GetBuyAccepts(keys, docId);

       //     var paymentdocuments = ExplainInvoicePaymentDocumentAdapterCreator.Create(_helper).GetPaymentDocuments(keys, docId);

            items.ForEach(invoice =>
            {
                invoice.BuyerInn = string.Join(",", buyers.Where(x => x.InvoiceKey == invoice.RowKey).Select(y=>y.Inn));
                invoice.SellerInn = string.Join(",", sellers.Where(x => x.InvoiceKey == invoice.RowKey).Select(y => y.Inn));
                invoice.AcceptDate = string.Join(",", buyaccepts.Where(x => x.InvoiceKey == invoice.RowKey).Select(y => y.AcceptedAt));
                invoice.OperationCode = string.Join(",", operations.Where(x => x.InvoiceKey == invoice.RowKey).Select(y => y.Code));
            });
        }

        public OperationResult<List<TaxMonitoringReportItem>> GetReportUnconfirmed(long docId)
        {
            return
                _helper.Do(
                    () => InvoiceClaimAdapterCreator.CreateReportTaxMonitoringAdapter(_helper)
                            .GetUnconfirmed(docId));
        }
    }

    public static class InvoiceClaimReportServiceHelper
    {
        public static FilterExpressionBase RowKeyFilterExpression(IEnumerable<string> keys, long docId)
        {
            return FilterExpressionCreator.
                CreateInList(TypeHelper<InvoiceContractor>.GetMemberName(x => x.InvoiceKey),
                    keys.ToArray());
        }

        public static List<InvoiceOperation> GetOperation(
            this IExplainInvoiceOperationAdapter adapter,
            IEnumerable<string> keys, long docId)
        {
            return adapter
                .Search(
                    RowKeyFilterExpression(keys, docId));
        }

        public static List<InvoiceContractor> GetContractors(
            this IExplainInvoiceContractorAdapter adapter,
            IEnumerable<string> keys, long docId)
        {
            return adapter
                .Search(RowKeyFilterExpression(keys, docId));
        }


        public static List<InvoiceBuyAccept> GetBuyAccepts(
            this IExplainInvoiceBuyAcceptAdapter adapter,
            IEnumerable<string> keys, long docId)
        {
            return adapter
                .Search(
                    RowKeyFilterExpression(keys, docId));
        }

        public static List<InvoicePaymentDocument> GetPaymentDocuments(
            this IExplainInvoicePaymentDocumentAdapter adapter,
            IEnumerable<string> keys, long docId)
        {
            return adapter
                .Search(RowKeyFilterExpression(keys, docId));
        }
    }
}
