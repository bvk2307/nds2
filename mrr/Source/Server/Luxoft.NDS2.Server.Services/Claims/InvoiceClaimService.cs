﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.Claims;
using Luxoft.NDS2.Server.Common.Adapters.Claims;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.Claims;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.Claims
{
    [CommunicationContract(typeof(IInvoiceClaimService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InvoiceClaimService : IInvoiceClaimService
    {
        private readonly ServiceHelpers _helper;
        private readonly IDocManageAdapter _docManageAdapter;

        public InvoiceClaimService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _docManageAdapter = InvoiceClaimAdapterCreator.CreateDocManageAdapter(_helper);
        }
        
        public OperationResult<List<DateTime>> GetCreateDates()
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateDatesAdapter(_helper).All(ClaimType.Invoice));
        }

        public OperationResult<PageResult<InvoiceClaim>> GetClaims(QueryConditions qc, DateTime date, int[] status)
        {
            return _helper.Do(
             () =>
             {
                 var allQuery = (QueryConditions)qc.Clone();
                 allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                 var result = InvoiceClaimAdapterCreator.InvoiceClaimAdapter(_helper, date, status)
                             .Page(
                                 allQuery.Filter.ToFilterGroup(),
                                 qc.Filter.ToFilterGroup(),
                                 qc);
                 return result;

             });
        }

        public OperationResult<PageResult<InvoiceClaim>> GetClaimsToSend(QueryConditions qc, DateTime date)
        {
            return _helper.Do(
             () =>
             {
                 qc.Filter.RemoveAll(
                     filter => filter.ColumnName == TypeHelper<InvoiceClaim>.GetMemberName(x => x.Excluded));
                 var allQuery = (QueryConditions)qc.Clone();
                 allQuery.Filter.RemoveAll(filter => filter.UserDefined);

                 var result = InvoiceClaimAdapterCreator.InvoiceClaimToSendAdapter(_helper, date)
                             .Page(
                                 allQuery.Filter.ToFilterGroup(),
                                 qc.Filter.ToFilterGroup(),
                                 qc);
                 return result;

             });
        }

        public OperationResult<int> GetCountClaimsToSend(DateTime date)
        {
            return _helper.DoEx(
                () => InvoiceClaimAdapterCreator.InvoiceClaimToSendAdapter(_helper, date).Count(new QueryConditions().Filter.ToFilterGroup()));
        }

        public OperationResult<List<SimpleStatusDto>> GetStatusList()
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateStatusListAdapter(_helper).All(ClaimType.Invoice));
        }

        public OperationResult<DocSendStatus> GetSendStatus(DateTime date)
        {
            return _helper.Do(() => InvoiceClaimAdapterCreator.CreateDocSendStatusAdapter(_helper).Get(ClaimType.Invoice, date));
        }

        public OperationResult Include(long docId, string username)
        {
            return _helper.Do(() => _docManageAdapter.Include(docId, username));
        }

        public OperationResult Exclude(long docId, string username)
        {
            return _helper.Do(() => _docManageAdapter.Exclude(docId, username));
        }

        public OperationResult Send(DateTime date, string username)
        {
            return _helper.Do(() => _docManageAdapter.Send(date, username));
        }

        public OperationResult<DateTime> GetBaseDate()
        {
            return _helper.DoEx(() => _docManageAdapter.GetBaseDate());
        }
    }

}
