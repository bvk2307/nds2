﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonComponents.Communication;
using CommonComponents.Directory;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IUserInformationService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class UserInformationService : IUserInformationService
    {
        private readonly ServiceHelpers _helper;

        public UserInformationService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }


        public OperationResult<List<UserInformation>> GetInspectorListBySono(string sonoCode)
        {
            var result = new List<UserInformation>();
            var authProvider = _helper.Services.Get<IAuthorizationProvider>();
            var userInfoService = _helper.Services.Get<IUserInfoService>();

            _helper.Do(() =>
            {
                var inspectors = authProvider.GetStructContextUsersByRole(sonoCode,
                    Constants.SystemPermissions.Operations.
                        RoleInspector);
                foreach (var inspector in inspectors)
                {
                    try
                    {
                        var userInfo =
                            userInfoService.GetUserInfoBySID(inspector.UserSid);
                        result.Add(userInfo.ToContract());
                    }
                    catch (COMException ex)
                    {
                        result.Add(new UserInformation { Sid = inspector.UserSid });
                        _helper.LogError(
                            string.Format("IDA: Не удалось получить полное имя инспектора sid {0}", inspector.UserSid),
                            "UserInformationService.GetInspectorListBySono",
                            ex);
                    }
                }
            });

            return new OperationResult<List<UserInformation>>(result);
        }
    }

    public static class UserInfoConvertor
    {
        public static UserInformation ToContract(this UserInfo data)
        {
            return new UserInformation
            {
                Sid = data.ObjectSID,
                EmployeeNum = data.LogonName,
                Name = data.DisplayName
            };
        }
    }
}