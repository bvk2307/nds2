﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services
{
    internal static class DeclarationFilterHelper
    {
        public static FilterExpressionBase ToFilter(this string[] innList, string field)
        {
            return FilterExpressionCreator.CreateInList(
                field,
                innList);
        }

        public static FilterExpressionBase ToFilter(this object[] innList, string field)
        {
            return FilterExpressionCreator.CreateInList(
                field,
                innList);
        }  

        public static QueryConditions GetFilter(string inn, Period period = null)
        {
            var result = new QueryConditions();

            result.Filter.Add(
                new FilterQuery
                {
                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.INN),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter>
                    {
                        new ColumnFilter
                        {
                            Value = inn,
                            ComparisonOperator = 
                                ColumnFilter.FilterComparisionOperator.Equals
                        }
                    }
                });

            result.Filter.Add(
                new FilterQuery
                {
                    ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.DECL_TYPE_CODE),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter> 
                    { 
                        new ColumnFilter 
                        { 
                            Value = 0, 
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals}
                    }
                });

            if (period != null)
            {

                result.Filter.Add(
                    new FilterQuery
                    {
                        ColumnName = TypeHelper<DeclarationBrief>.GetMemberName(x => x.TAX_PERIOD),
                        FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                        Filtering =
                            Period.AllCodes[period.Code.ToString()]
                                .Select(x =>
                                    new ColumnFilter
                                    {
                                        Value = x,
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                                    }).ToList()
                    });
            }

            return result;
        }
    }
}
