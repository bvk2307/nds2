﻿using System.Collections.Generic;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters.OperationContext;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.OperationContext;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.UserAccess
{
    public class UserAccessContextLoader
    {
        private readonly IOperationContextAdapter _adapter;

        private readonly IAuthorizationProvider _authorizationProvider;        

        public UserAccessContextLoader(
            IOperationContextAdapter adapter, 
            IAuthorizationProvider authorizationProvider)
        {
            _adapter = adapter;
            _authorizationProvider = authorizationProvider;
        }

        public AccessContext GetAccessContext(IEnumerable<string> operations)
        {
            var list = _adapter.Select(_authorizationProvider.BuildFilter(operations));

            return new AccessContext
            {
                Operations = list
                    .GroupBy(t => t.Id)
                    .Select(
                        group => new OperationAccessContext
                        {
                            Name = group.First().Name,
                            Description = group.First().Description,
                            IsRestricted = 
                                group.All(x => x.LimitType != AccessLimitType.NotRestricted),
                            AvailableInspections = 
                                group
                                    .Where(x => !string.IsNullOrWhiteSpace(x.AccessToSonoCode))
                                    .Select(x => x.AccessToSonoCode)
                                    .Union(group.Where(x => x.LimitType == AccessLimitType.SelfInspection).Select(x => x.SonoCode))
                                    .Distinct()
                                    .ToArray(),
                            AvailableRegions = 
                                group
                                    .Where(x => !string.IsNullOrWhiteSpace(x.AccessToRegionCode))
                                    .Select(x => x.AccessToRegionCode)
                                    .Distinct()
                                    .ToArray()
                        }).ToArray()
            };
        }
    }

    public class UserAccessValidator
    {
        private readonly IOperationAdapter _adapter;

        private readonly IAuthorizationProvider _authorizationProvider;

        public UserAccessValidator(
            IOperationAdapter adapter, 
            IAuthorizationProvider authorizationProvider)
        {
            _adapter = adapter;
            _authorizationProvider = authorizationProvider;
        }

        public string[] ValidateAccess(IEnumerable<string> operations)
        {
            var list = _adapter.Select(_authorizationProvider.BuildFilter(operations));

            return list.Select(x => x.Name).Distinct().ToArray();
        }
    }

    public static class UserAccessContextHelper
    {
        private static readonly string[] AllRoles =
            {
                Constants.SystemPermissions.Operations.RoleAdministrator,
                Constants.SystemPermissions.Operations.RoleAnalyst,
                Constants.SystemPermissions.Operations.RoleApprover,
                Constants.SystemPermissions.Operations.RoleInspector,
                Constants.SystemPermissions.Operations.RoleManager,
                Constants.SystemPermissions.Operations.RoleMedodologist,
                Constants.SystemPermissions.Operations.RoleObserver,
                Constants.SystemPermissions.Operations.RoleSender
            };

        public static AccessContext GetAccessContext(this ServiceHelpers helper, IEnumerable<string> operations)
        {
            var loader =
                new UserAccessContextLoader(
                    helper.OperationContextAdapter(),
                    helper.Services.Get<IAuthorizationProvider>());

            return loader.GetAccessContext(operations);
        }

        public static string[] ValidateAccess(this ServiceHelpers helper, IEnumerable<string> operations)
        {
            var validator =
                new UserAccessValidator(
                    helper.OperationAdapter(),
                    helper.Services.Get<IAuthorizationProvider>());

            return validator.ValidateAccess(operations);
        }

        public static FilterExpressionBase BuildFilter(this IAuthorizationProvider authProvider, IEnumerable<string> operations)
        {
            var filterBy = FilterExpressionCreator.CreateGroup();
            var csudOperations =
                authProvider
                    .GetUserPermissions()
                    .Where(x => AllRoles.Contains(x.Name))
                    .Where(x => x.PermType == PermissionType.Operation)
                    .ToArray();
            var sonoNames = FilterExpressionCreator.CreateUnion();
            foreach (var csudOperation in csudOperations)
            {
                sonoNames.WithExpression(
                    FilterExpressionCreator.CreateGroup()
                        .WithExpression(
                            FilterExpressionCreator.Create(
                                TypeHelper<OperationAccessData>.GetMemberName(x => x.RoleName),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                csudOperation.Name))
                        .WithExpression(
                            FilterExpressionCreator.Create(
                                TypeHelper<OperationAccessData>.GetMemberName(x => x.SonoCode),
                                ColumnFilter.FilterComparisionOperator.Equals,
                                csudOperation.StructContext)));
            }

            filterBy
                .WithExpression(sonoNames)
                .WithExpression(
                FilterExpressionCreator.CreateInList(
                    TypeHelper<OperationAccessData>.GetMemberName(x => x.Name),
                    operations));

            return filterBy;
        }

        public static FilterExpressionBase ToFilter(
            this OperationAccessContext operation,
            string regionCodeName,
            string sonoCodeName)
        {
            var result = FilterExpressionCreator.CreateUnion();

            if (operation.AvailableRegions.Any())
            {
                result.WithExpression(
                    FilterExpressionCreator.CreateInList(
                        regionCodeName, 
                        operation.AvailableRegions));
            }

            if (operation.AvailableInspections.Any())
            {
                result.WithExpression(
                    FilterExpressionCreator.CreateInList(
                        sonoCodeName,
                        operation.AvailableInspections));
            }

            return result;
        }
    }
}
