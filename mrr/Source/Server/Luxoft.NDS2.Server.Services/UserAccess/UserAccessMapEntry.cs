﻿using System;
using System.Diagnostics.Contracts;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.UserAccess
{
    /// <summary> User access mapping entry used by <see cref="UserAccessPolicyProvider"/>. </summary>
    /// <remarks> A read only object that should not have editable properties because can be passed through process boundary and can be cloned. </remarks>
    [Serializable]
    public sealed class UserAccessMapEntry
    {
        private readonly string _id;

        public UserAccessMapEntry( string id )
        {
            Contract.Requires( !string.IsNullOrEmpty( id ) );

            _id = id;
        }

	    //apopov 7.11.2016	//TODO!!! //replace with actual mapping properties
        /// <summary> TEST only porperty. </summary>
        public string Id { get { return _id; } }
    }
}