﻿using System;
using System.Collections.Generic;
using CommonComponents.ThesAccess;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Server.Common.Cache;

namespace Luxoft.NDS2.Server.Services.UserAccess
{
    public sealed class UserAccessClassifierProvider : PocoClassifierProvider<UserAccessMapEntry>
    {
        public UserAccessClassifierProvider( IServiceProvider serviceProvider ) : base( serviceProvider ) { }

        protected override IReadOnlyCollection<UserAccessMapEntry> OnLookUp( IRequest request, string namedConfig )
        {
            return new[] { new UserAccessMapEntry( "Test Entry ID 1" ), new UserAccessMapEntry( "Test Entry ID 2" ) }.ToReadOnly();
        }
    }
}