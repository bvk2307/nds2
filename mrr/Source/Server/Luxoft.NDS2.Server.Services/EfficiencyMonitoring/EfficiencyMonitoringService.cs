﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.EfficiencyMonitoring.Implementation;

namespace Luxoft.NDS2.Server.Services.EfficiencyMonitoring
{
    [CommunicationContract(typeof(IEfficiencyMonitoringService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class EfficiencyMonitoringService : IEfficiencyMonitoringService
    {
        private ServiceHelpers _helper;

        public EfficiencyMonitoringService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }


        public OperationResult<List<IfnsRatingByDiscrepancyShareInDeductionData>> GetIfnsRatingByDiscrepancyShareInDeduction(int year, int qtr, DateTime calcDate)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.CommonAdapter(_helper).GetIfnsRatingByDiscrepancyShareInDeduction(year,qtr,calcDate);
                });
        }

        public OperationResult<Dictionary<QuarterInfo, List<ReportCalculationInfo>>> GetQuarterCalculationDates()
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.CommonAdapter(_helper).SearchQuarterCalculationDates();
                });
        }

        public OperationResult<Dictionary<string, List<TnoRatingInfo>>> GetAllQuartersRatings(ChartData request)
        {
            Dictionary<string, List<TnoRatingInfo>> result = new Dictionary<string, List<TnoRatingInfo>>();

            return _helper.Do(
                            () =>
                            {
                                result.Add(request.ScopeCode, GetRatingData(request.FiscalYear, request.Quarter, request.CalculateDate, request.ScopeCode, request.Scope));

                                if (!string.IsNullOrEmpty(request.ParentScopeCode))
                                {
                                    result.Add(request.ParentScopeCode, GetRatingData(request.FiscalYear, request.Quarter, request.CalculateDate, request.ParentScopeCode, request.ParentScope));
                                }

                                return result;
                            });
        }


        public OperationResult<PageResult<DiscrepancyDetail>> GetDiscrepancyDetails(QueryConditions queryConditions,
            bool withTotal)
        {
            var result = _helper.Do(() =>
            {
                PageResult<DiscrepancyDetail> innerResult;
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    var adapter = new DiscrepancyDetailsAdapter(_helper, connectionFactory);
                    var searchCriteria = queryConditions.Filter.ToFilterGroup();
                    var data = adapter.Search(searchCriteria, queryConditions.Sorting,
                        queryConditions.PaginationDetails.RowsToSkip
                        ,
                        queryConditions.PaginationDetails.RowsToSkip +
                        queryConditions.PaginationDetails.RowsToTake.Value
                        );

                    var count = adapter.Count(searchCriteria);
                    int total = 0;
                    if (withTotal)
                    {
                        total = adapter.Count(queryConditions.Filter.Where(s => !s.UserDefined).ToFilterGroup());
                    }
                    innerResult = new PageResult<DiscrepancyDetail>(data, count, total);
                    return innerResult;
                }
            });
            return result;
        }


        private List<TnoRatingInfo> GetRatingData(int year, int qtr, DateTime calcDate, string TnoCode, ScopeEnum type)
        {
            List<TnoRatingInfo> result = null;
            
            switch (type)
            {
                case ScopeEnum.RF:
                    result = TableAdapterCreator.CommonAdapter(_helper).GetAllQuartersRatingsRf(year, qtr, calcDate);
                    break;

                case ScopeEnum.UFNS:
                    result =
                        TableAdapterCreator.CommonAdapter(_helper).GetAllQuartersRatingsUfns(year, qtr, calcDate, TnoCode);
                    break;

                case ScopeEnum.DISTRICT:
                    result =
                        TableAdapterCreator.CommonAdapter(_helper)
                            .GetAllQuartersRatingsDistrict(year, qtr, calcDate, TnoCode);
                    break;

                case ScopeEnum.IFNS:
                    result = TableAdapterCreator.CommonAdapter(_helper).GetAllQuartersRatingsIfns(year, qtr, calcDate, TnoCode);
                    break;
            }

            return result ?? new List<TnoRatingInfo>();
        }

        public OperationResult<List<TnoData>> GetFederalCollection(CalculationData request)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.CommonAdapter(_helper).SearchRatingDetails(request);
                });
        }


        public OperationResult<List<RegionData>> GetEfficiencyRegionCollection(
            RaitingRequestData requestData)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchEfficiencyRaiting(requestData)
                            .ToList();
                }
                );
        }

        public OperationResult<List<RegionData>> GetEfficiencyRegionForComparison(
            RaitingRequestData requestData)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchEfficiencyRaitingForComparison(requestData)
                            .ToList();
                }
                );
        }

        public OperationResult<List<RegionData>> GetDispatchRaitingRegionCollection(
            RaitingRequestData requestData)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchDispatchRaiting(requestData)
                            .ToList();
                }
                );
        }

        public OperationResult<List<DateTime>> GetEnabledDates(int fiscalYear, int quarter)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchDates(fiscalYear, quarter)
                            .ToList();
                }
                );
        }

        public OperationResult<List<DateTime>> GetEnabledRegionDates(string regionId, int fiscalYear, int quarter)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchRegionDates(regionId, fiscalYear, quarter)
                            .ToList();
                }
                );
        }

        public OperationResult<List<DateTime>> GetEnabledDates(string regionId, int fiscalYear, int quarter)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .SearchRegionDates(regionId, fiscalYear, quarter)
                            .ToList();
                }
                );
        }

        // new

        public OperationResult<RegionData> GetLastRaitingForRegion(string regionId, int fiscalYear,
            int quarterNumber)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .GetLastRaitingForRegion(regionId, fiscalYear, quarterNumber);
                }
                );
        }

        public OperationResult<RegionData> GetRegionData(string regionId, int fiscalYear, int quarter,
            DateTime date)
        {
            return _helper.Do(
                () =>
                {
                    return TableAdapterCreator.RegionAdapter(_helper)
                        .GetRegionData(regionId, fiscalYear, quarter, date);
                }
                );
        }

        public OperationResult<List<RegionData>> GetAllRegionData(string regionId)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper)
                            .GetAllRegionData(regionId)
                            .ToList();
                }
                );
        }

        public OperationResult<List<IfnsData>> GetEfficiencyIfnsCollection(
            RaitingRequestData requestData, string regionId)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.IfnsAdapter(_helper)
                            .SearchEfficiencyRaiting(requestData, regionId)
                            .ToList();
                }
                );
        }

        public OperationResult<List<IfnsData>> GetEfficiencyIfnsCollection(
            RaitingRequestData requestData, string regionId, List<string> codes)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.IfnsAdapter(_helper)
                            .SearchEfficiencyRaiting(requestData, regionId, codes)
                            .ToList();
                }
                );
        }

        public OperationResult<List<DateTime>> GetIfnsEnabledDates(int fiscalYear, int quarter, string regionId)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.IfnsAdapter(_helper)
                            .SearchDates(fiscalYear, quarter, regionId)
                            .ToList();
                }
                );
        }

        public OperationResult<IfnsData> GetLastRatingForIfns(string sonoCode, int fiscalYear,
            int quarterNumber)
        {
            return _helper.Do(
                () =>
                {
                    return TableAdapterCreator.IfnsAdapter(_helper)
                        .GetLastRatingForIfns(sonoCode, fiscalYear, quarterNumber);
                }
                );
        }

        public OperationResult<IfnsData> GetIfnsData(string sonoCode, int fiscalYear, int quarter,
            DateTime date)
        {
            return _helper.Do(
                () =>
                {
                    return TableAdapterCreator.IfnsAdapter(_helper)
                        .GetIfnsData(sonoCode, fiscalYear, quarter, date);
                }
                );
        }

        public OperationResult<List<IfnsData>> GetAllIfnsData(string sonoCode)
        {
            return _helper.Do(
                () =>
                {
                    return TableAdapterCreator.IfnsAdapter(_helper).GetAllIfnsData(sonoCode).ToList();
                }
                );
        }

        public OperationResult<List<RegionData>> GetRegions(string districtId)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.RegionAdapter(_helper).GetRegions(districtId).ToList();
                }
                );
        }

        public OperationResult<List<IfnsData>> GetIfnsForComparison(
            RaitingRequestData request, string regionId)
        {
            return _helper.Do(
                () =>
                {
                    return
                        TableAdapterCreator.IfnsAdapter(_helper)
                            .SearchEfficiencyRaitingForComparison(request, regionId)
                            .ToList();
                }
                );
        }
    }
}
