﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(ITaxpayerAssignmentService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class TaxpayerAssignmentService : ITaxpayerAssignmentService
    {
        private readonly ServiceHelpers _helper;

        public TaxpayerAssignmentService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<PageResult<TaxpayerAssignment>> GetTaxpayersAssigmentsList(QueryConditions qc)
        {
            return _helper.Do
               (
                   () =>
                   {
                       var sCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                       qc.Filter.Add(new FilterQuery()
                       {
                           ColumnName = TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.SONO_CODE)
                           ,
                           FilterOperator = FilterQuery.FilterLogicalOperator.And
                           ,
                           Filtering = new List<ColumnFilter>()
                                {
                                    new ColumnFilter()
                                    {
                                        Value = sCode,
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                                    }
                                }
                       });
                       if (qc.Sorting.Count == 0)
                       {
                           qc.Sorting.Add(new ColumnSort()
                           {
                               ColumnKey = TypeHelper<TaxpayerAssignment>.GetMemberName(t => t.UPDATE_DATE),
                               Order = ColumnSort.SortOrder.Desc
                           });
                       }
                       return TableAdapterCreator.IdaTaxpayerAssignmentAdapter(_helper).GetTaxpayersAssignmentsList(qc);
                   }
               );
        }

        public OperationResult<TaxpayersResult> AddTaxpayerAssignment(IEnumerable<Taxpayer> tList)
        {
            var result = new TaxpayersResult();
            foreach (var t in tList)
            {
                _helper.Do
                (
                    () =>
                    {
                        if (TableAdapterCreator.IdaTaxpayerAssignmentAdapter(_helper).AddTaxpayerAssignment(t, _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID))
                        {
                            result.SuccesfullRecords.Add(t);
                        }
                        else { result.FailedRecords.Add(t); }
                    }
                );
            }
            return new OperationResult<TaxpayersResult> { Result = result };
        }

        public OperationResult<TaxpayersAssignmentsResult> RemoveTaxpayerAssignment(IEnumerable<TaxpayerAssignment> taList)
        {
            var result = new TaxpayersAssignmentsResult();
            foreach (var ta in taList)
            {
                _helper.Do
                (
                    () =>
                    {
                        if (TableAdapterCreator.IdaTaxpayerAssignmentAdapter(_helper).RemoveTaxpayerAssignment(ta, _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID))
                        {
                            result.SuccesfullRecords.Add(ta);
                        }
                        else { result.FailedRecords.Add(ta); }
                    }
                );
            }
            return new OperationResult<TaxpayersAssignmentsResult> { Result = result };
        }

        public OperationResult<bool> CancelInspectorAssignment(TaxpayerAssignment ta)
        {
            return _helper.DoEx
                (
                    () =>  TableAdapterCreator.IdaTaxpayerAssignmentAdapter(_helper).CancelInspectorAssignment(ta,_helper.Services.Get<IAuthorizationProvider>().CurrentUserSID)
                );
        }

        public OperationResult<bool> AddInspectorAssignment(TaxpayerAssignment ta)
        {
            return _helper.DoEx
                (
                   () => TableAdapterCreator.IdaTaxpayerAssignmentAdapter(_helper).AddInspectorAssignment(ta,_helper.Services.Get<IAuthorizationProvider>().CurrentUserSID)
                );
        }
    }
}
