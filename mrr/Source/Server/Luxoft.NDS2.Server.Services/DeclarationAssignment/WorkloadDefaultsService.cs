﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(IWorkloadDefaultsService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class WorkloadDefaultsService : IWorkloadDefaultsService
    {
        private readonly ServiceHelpers _helper;

        public WorkloadDefaultsService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<DefaultInspectorWorkLoad> GetDefaultWorkLoad()
        {
            return _helper.Do
                (
                    () =>
                    TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).GetDefaultInspectorsWorkload(
                        DeclarationAssignmentHelper.GetMySonoCode(_helper))
                );
        }

        public OperationResult SetDefaultWorkLoad(DefaultInspectorWorkLoad defaultInspectorWorkLoad)
        {
            return _helper.Do
                 (
                     () =>
                     {
                         defaultInspectorWorkLoad.UpdatedBy = _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID;
                         defaultInspectorWorkLoad.SonoCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                         TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).
                             SaveDefaultInspectorsWorkload(defaultInspectorWorkLoad);
                     }

                );
        }
    }
}
