﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(IInspectorStatisticsService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InspectorStaisticsService: IInspectorStatisticsService
    {
        private readonly ServiceHelpers _helper;

        public InspectorStaisticsService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<List<InspectorStatistics>> GetActiveInspectorsStatistics()
        {
           return  _helper.Do
                (
                    () =>
                    {
                        var sono_code = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                        return TableAdapterCreator.IdaTaxpayerInspectorAssignmentAdapter(_helper).GetActiveInspectorsStatistics(sono_code);
                    }
                );
        }

        public OperationResult<PageResult<InspectorStatistics>> GetFullInspectorsStatistics(NDS2.Common.Contracts.DTO.Query.QueryConditions qc)
        {
            return _helper.Do
                (
                    () =>
                        {
                            var sono_code = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                            return TableAdapterCreator.IdaTaxpayerInspectorAssignmentAdapter(_helper).GetFullInspectorStatistics(sono_code, qc);
                        }
                );
        }
    }
}
