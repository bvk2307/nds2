﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(ITaxpayerService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class TaxpayerService : ITaxpayerService
    {
        private readonly ServiceHelpers _helper;

        public TaxpayerService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<PageResult<Taxpayer>> GeTaxpayersList(QueryConditions qc)
        {
            return _helper.Do
                (
                    () =>
                        {
                            var sCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                            qc.Filter.Add(new FilterQuery()
                            { 
                                ColumnName = TypeHelper<Taxpayer>.GetMemberName(t => t.SONO_CODE)
                                ,FilterOperator = FilterQuery.FilterLogicalOperator.And
                                ,Filtering = new List<ColumnFilter>()
                                {
                                    new ColumnFilter()
                                    {
                                        Value = sCode,
                                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals
                                    }
                                }
                            });
                            return TableAdapterCreator.IdaTaxpayerAdapter(_helper).GetTaxpayerList(qc);
                        }
                );
        }
    }
}
