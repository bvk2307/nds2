﻿using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    static class DeclarationAssignmentHelper
    {
        public static string GetMySonoCode(ServiceHelpers helper)
        {
            var authProvider = helper.Services.Get<IAuthorizationProvider>();
            var userPermission = authProvider.GetUserPermissions();
            var administratorPermissions = userPermission.Where(p => p.PermType == PermissionType.Operation &&
                                                                     p.Name ==
                                                                     Constants.SystemPermissions.Operations.
                                                                         RoleAdministrator).ToArray();
            if (administratorPermissions.Length > 1)
            {
                throw new NotImplementedException("IDA: Функционал администратора нескольких ИФНС не поддерживается");
            }
            if (administratorPermissions.Length == 0)
            {
                throw new Exception("IDA: Пользователь должен быть администратором по крайней мере в одной ИФНС");
            }
            return administratorPermissions.First().StructContext;
        }
    }
}
