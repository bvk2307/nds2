﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(IWorkloadService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InspectorDecarationAssignmentService : IWorkloadService
    {
        private readonly ServiceHelpers _helper;

        public InspectorDecarationAssignmentService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }




        public OperationResult<List<InspectorWorkload>> GetActiveWorkLoads()
        {
            return _helper.Do
            (
             () => TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).GetAvailableInspectorsWorkload(DeclarationAssignmentHelper.GetMySonoCode(_helper))
            );
        }



        public OperationResult<SetInspectorWorkLoadResult> SetWorkloadForInspectors(List<InspectorWorkload> inspectorWorkLoads)
        {
            var res = new SetInspectorWorkLoadResult();
            var sCode = string.Empty;
            _helper.Do(() =>
                           {
                               sCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                           });
            if (!string.IsNullOrEmpty(sCode))
            {
                foreach (var workload in inspectorWorkLoads)
                {
                    _helper.Do
                        (
                            () =>
                            {
                                workload.UpdatedBy = _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID;
                                workload.SonoCode = sCode;
                                if (
                                    TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).
                                        SetInspectorWorkLoad(workload))
                                {
                                    res.SuccesfullRecords.Add(workload);
                                }
                                else
                                {
                                    res.FailedRecords.Add(workload);
                                    _helper.LogWarning(
                                        string.Format(
                                            "IDA: Не удалось назначить нагрузку на инспектора sid {0}, sono {1}",
                                            workload.Inspector.SID, workload.SonoCode),
                                        "InspectorDecarationAssignmentService.SetWorkloadForInspectors");
                                }
                            }
                        );
                }
            }
            else
            {
                _helper.LogError("IDA: не найдена ИФНС для администратора", "InspectorDecarationAssignmentService.SetWorkloadForInspectors");
                return new OperationResult<SetInspectorWorkLoadResult> { Status = ResultStatus.Error };
            }
            return new OperationResult<SetInspectorWorkLoadResult> { Result = res };
        }

        public OperationResult<ClearInspectorsWorkloadResult> ClearInspectorsWorkload(List<InspectorWorkload> workloads)
        {
            var res = new ClearInspectorsWorkloadResult();
            var sCode = string.Empty;
            _helper.Do(() =>
            {
                sCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
            });
            if (!string.IsNullOrEmpty(sCode))
            {
                foreach (var workload in workloads)
                {
                    _helper.Do
                        (
                            () =>
                            {
                                if (workload.Inspector.SonoCode != sCode)
                                {
                                    throw new Exception("Попытка установить нагрузку на сотрудника из другого ТНО." +
                                                        string.Format("ТНО администратора: {0}. Тно сотрудника {1}", sCode, workload.Inspector.SonoCode));
                                }
                                workload.Inspector.UpdatedBy = _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID;
                                var result = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).ClearInspectorWorkLoad(workload.Inspector);
                                if (result)
                                {
                                    res.SuccesfullRecords.Add(workload.Inspector);
                                }
                                else
                                {
                                    res.FailedRecords.Add(workload.Inspector);
                                    _helper.LogWarning(
                                        string.Format(
                                            "IDA: Не удалось удалить нагрузку на с инспектора sid {0}, sono {1}",
                                            workload.Inspector.SID, sCode),
                                        "InspectorDecarationAssignmentService.ClearInspectorsWorkloadResult");
                                }
                            }
                        );
                }
            }
            else
            {
                _helper.LogError("IDA: не найдена ИФНС для администратора", "InspectorDecarationAssignmentService.SetWorkloadForInspectors");
                return new OperationResult<ClearInspectorsWorkloadResult> { Status = ResultStatus.Error };
            }
            return new OperationResult<ClearInspectorsWorkloadResult> { Result = res };
        }

        public OperationResult UpdateWorkLoad(InspectorWorkload inspectorWorkload)
        {
            return _helper.Do(() =>
                           {
                               var adapter = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper);
                               inspectorWorkload.UpdatedBy = _helper.Services.Get<IAuthorizationProvider>().CurrentUserSID;
                               if (!adapter.SetInspectorWorkLoad(inspectorWorkload))
                               {
                                   var msg = string.Format("Не удалось назначить нагрузку на инспектора имя {0}, sono {1}",
                                                 inspectorWorkload.Inspector.Name, inspectorWorkload.SonoCode);
                                   _helper.LogWarning("IDA: " + msg, "InspectorDecarationAssignmentService.UpdateWorkLoad");
                                   throw new Exception(msg);
                               }
                           });
        }


    }
}
