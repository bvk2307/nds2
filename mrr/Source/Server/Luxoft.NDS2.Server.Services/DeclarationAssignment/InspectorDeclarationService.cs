﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(IInspectorDeclarationService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InspectorDeclarationService : IInspectorDeclarationService
    {
        private readonly ServiceHelpers _helper;

        public InspectorDeclarationService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult AssignInspector(string sid, long declarationId)
        {
            return _helper.Do
            (
                () =>
                {
                    var inpectorSonoAssg = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).GetInspector(DeclarationAssignmentHelper.GetMySonoCode(_helper), sid);
                    if(!string.IsNullOrEmpty(inpectorSonoAssg.SID))
                    {
                        TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).AssignDeclaration(declarationId, inpectorSonoAssg.SID);
                    }
                    else
                    {
                        throw new ArgumentException("sid");
                    }
                }
            );
        }


        public OperationResult<PageResult<InspetorDeclarationDistribution>> GetInspectorDeclationDistribution(QueryConditions qc)
        {
            return _helper.Do
            (
                () =>
                {
                    var sCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                    return TableAdapterCreator.InspectorDeclarationAdapter(_helper).GetInspectorDeclarationsDistribution(sCode, qc);
                }
            );
        }

        public OperationResult<PageResult<InspectorDeclarationNotDistribution>> GetInspectorDeclationNotDistribution(QueryConditions searchCriteria)
        {
            return _helper.Do
            (
                () =>
                {
                    var inspectionCode = DeclarationAssignmentHelper.GetMySonoCode(_helper);
                    return TableAdapterCreator
                        .InspectorDeclarationAdapter(_helper)
                        .GetInspectorDeclarationsNotDistribution(inspectionCode, searchCriteria);
                }
            );
        }
    }
}
