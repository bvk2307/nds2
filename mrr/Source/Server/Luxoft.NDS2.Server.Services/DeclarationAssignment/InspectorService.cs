﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Server.Services.DeclarationAssignment
{
    [CommunicationContract(typeof(IInspectorService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class InspectorService : IInspectorService
    {
        private readonly ServiceHelpers _helper;

        public InspectorService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public OperationResult<List<InspectorSonoAssignment>> GetFreeInspectors()
        {
            return _helper.Do
                (
                    () =>
                    TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).GetFreeInspectors(
                        DeclarationAssignmentHelper.GetMySonoCode(_helper))
                );
        }

         public OperationResult<InspectorSonoAssignment> GetInspector(string sid)
         {
             return _helper.Do
                 (
                     () =>
                     TableAdapterCreator.InspectorDeclarationAssingmentAdapter(_helper).GetInspector(
                         DeclarationAssignmentHelper.GetMySonoCode(_helper), sid)
                 );
         }
    }
}
