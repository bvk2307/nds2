﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Server.Services.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Server.Services.DataAccess
{
    /// <summary>
    /// 
    /// </summary>
    [Obsolete]
    public class DalManager
    {
        private ServiceHelpers _services;

        public DalManager(ServiceHelpers services)
        {
            _services = services;
        }

        private string GetConnectionString()
        {
            return _services.GetConfigurationValue(Constants.DB_CONFIG_KEY);
        }

        private OracleConnection CreateConnection()
        {
            var connection = new OracleConnection(GetConnectionString());
            connection.Open();

            return connection;
        }

        public int TestConnection()
        {
            using (var connection = CreateConnection())
            {
                using (OracleCommand cmd = new OracleCommand("select 5 from dual"))
                {
                    cmd.Connection = connection;
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }

        #region GetTableData

        public List<T> GetTableData<T>(string tblName) where T : class , new()
        {
            var resultList = new List<T>();
            using (var connection = CreateConnection())
            {
                var sql = String.Format("select * from {0}", tblName);
                using (var command = new OracleCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Type currType = typeof(T);
                            var allProperties = currType.GetProperties();

                            T t = new T();
                            for (int i = 0; i < allProperties.Count(); i++)
                            {
                                for (int iS = 0; iS < reader.FieldCount; iS++)
                                {
                                    if (reader.GetName(iS) == allProperties[i].Name)
                                    {
                                        if (allProperties[i].PropertyType == typeof(String))
                                        {
                                            allProperties[i].SetValue(t, DBNull.Value.Equals(reader[iS]) ? null : reader[iS].ToString(), null);
                                        }
                                        else
                                        {
                                            allProperties[i].SetValue(t, DBNull.Value.Equals(reader[iS]) ? null : reader[iS], null);
                                        }
                                    }
                                }
                            }

                            resultList.Add(t);
                        }
                    }
                }
            }

            return resultList;
        }

        #endregion

    }
}
