﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Server.Services.Entities;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Globalization;

namespace Luxoft.NDS2.Server.Services.EntitiesConverters
{
    public partial class EntitiesConverter
    {
        public static Файл ASKDeklToФайл(ASKDekl item)
        {
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            Файл ret = new Файл();

            #region Базовые параметры
            
            ret.ИдФайл = item.IdFajl;
            ret.ВерсПрог = item.VersProg;
            ret.ВерсФорм = item.VersForm.ParseEnum<ФайлВерсФорм>();
            ret.ПризнНал812 = item.PriznNal8_12.ParseEnum<ФайлПризнНал812>();
            ret.ПризнНал8 = item.PriznNal8.ParseEnum<ФайлПризнНал8>();
            ret.ПризнНал81 = item.PriznNal81.ParseEnum<ФайлПризнНал81>();
            ret.ПризнНал9 = item.PriznNal9.ParseEnum<ФайлПризнНал9>();
            ret.ПризнНал91 = item.PriznNal91.ParseEnum<ФайлПризнНал91>();
            ret.ПризнНал10 = item.PriznNal10.ParseEnum<ФайлПризнНал10>();
            ret.ПризнНал11 = item.PriznNal11.ParseEnum<ФайлПризнНал11>();
            ret.ПризнНал12 = item.PriznNal12.ParseEnum<ФайлПризнНал12>();

            ret.Документ = new ФайлДокумент 
                {
                        КНД = item.KND.ParseEnum<ФайлДокументКНД>(),
                        Период = item.Period.ParseEnum<ФайлДокументПериод>(),
                        ОтчетГод = item.OtchetGod,
                        КодНО = item.KodNO,
                        НомКорр = item.NomKorr,
                        ПоМесту = item.PoMestu.ParseEnum<ФайлДокументПоМесту>(),
                        ДатаДок = item.DataDok.HasValue ? item.DataDok.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null
                };


            ret.Документ.СвНП = new ФайлДокументСвНП
                {
                    ОКВЭД = item.OKVED,
                    Тлф = item.Tlf,
                    Item = null         // условный параметр
                };

            // ЮЛ или ФЛ
            if (item.INNNP.Length == 10)
            {
                ret.Документ.СвНП.Item = new ФайлДокументСвНПНПЮЛ
                {
                    НаимОрг = item.NaimOrg,
                    ИННЮЛ = item.INNNP,
                    КПП = item.KPPNP
                };

                if (!string.IsNullOrEmpty(item.INNReorg))
                {
                    ((ФайлДокументСвНПНПЮЛ)ret.Документ.СвНП.Item).СвРеоргЮЛ = new ФайлДокументСвНПНПЮЛСвРеоргЮЛ()
                    {
                        ИННЮЛ = item.INNReorg,
                        КПП = item.KPPReorg,
                        ФормРеорг = item.FormReorg.ParseEnum<ФайлДокументСвНПНПЮЛСвРеоргЮЛФормРеорг>()
                    };
                }
            }
            else
            {
                ret.Документ.СвНП.Item = new ФайлДокументСвНПНПФЛ()
                {
                    ИННФЛ = item.INNNP,
                    ФИО = new ФИОТип()
                    {
                        Имя = item.ImyaNP,
                        Отчество = item.OtchestvoNP,
                        Фамилия = item.FamiliyaNP
                    }
                };
            }

            ret.Документ.Подписант = new ФайлДокументПодписант
                {
                        ПрПодп = item.PrPodp.ParseEnum <ФайлДокументПодписантПрПодп>(),
                        ФИО = new ФИОТип()
                        {
                            Имя = item.ImyaPodp,
                            Фамилия = item.FamiliyaPodp,
                            Отчество = item.OtchestvoPodp
                        },
                        СвПред = new ФайлДокументПодписантСвПред()
                        {
                            НаимДок = item.NaimDok,
                            НаимОрг = item.NaimOrgPred
                        }
                };


            ret.Документ.НДС = new ФайлДокументНДС
                {
                    СумУплНА = null,    // списочный параметр

                    СумУплНП = new ФайлДокументНДССумУплНП()
                    {
                        КБК = item.KBK,
                        ОКТМО = item.OKTMO,
                        СумПУ_1731 = item.SumPU173_1.GetValueOrDefault().ToString(),
                        СумПУ_1735 = item.SumPU173_5.GetValueOrDefault().ToString(),
                        ДатаКонДогИТ = item.DataKonDogIT.HasValue ? item.DataKonDogIT.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        ДатаНачДогИТ = item.DataNachDogIT.HasValue ? item.DataNachDogIT.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        НомДогИТ = item.NomDogIT
                    },

                    СумУпл164 = new ФайлДокументНДССумУпл164()
                    {
                        НалПУ164 = item.NalPU164.ToString(),
                        СумНалОб = new ФайлДокументНДССумУпл164СумНалОб()
                        {
                            НалВосстОбщ = item.NalVosstObshh.GetValueOrDefault().ToString(),
                            РеалТов18 = GetSvedSunNal(item.RealTov18NalBaza, item.RealTov18SumNal),
                            РеалТов10 = GetSvedSunNal(item.RealTov10NalBaza, item.RealTov10SumNal),
                            РеалТов118 = GetSvedSunNal(item.RealTov118NalBaza, item.RealTov118SumNal),
                            РеалТов110 = GetSvedSunNal(item.RealTov110NalBaza, item.RealTov110SumNal),
                            РеалПредИК = GetSvedSunNal(item.RealPredIKNalBaza, item.RealPredIKSumNal),
                            ВыпСМРСоб = GetSvedSunNal(item.VypSMRSobNalBaza, item.VypSMRSobSumNal),
                            ОплПредПост = GetSvedSunNal(item.OplPredPostNalBaza, item.OplPredPostSumNal),
                            КорРеалТов18 = GetSvedSunNal(item.KorRealTov18NalBaza, item.KorRealTov18SumNal),
                            КорРеалТов10 = GetSvedSunNal(item.KorRealTov10NalBaza, item.KorRealTov10SumNal),
                            КорРеалТов118 = GetSvedSunNal(item.KorRealTov118NalBaza, item.KorRealTov118SumNal),
                            КорРеалТов110 = GetSvedSunNal(item.KorRealTov110NalBaza, item.KorRealTov110SumNal),
                            КорРеалПредИК = GetSvedSunNal(item.KorRealPredIKNalBaza, item.KorRealPredIKSumNal),
                            СумНалВосст = new ФайлДокументНДССумУпл164СумНалОбСумНалВосст()
                            {
                                СумНал17033 = item.SumNal170_3_3.GetValueOrDefault().ToString(),
                                СумНалОперСт0 = item.SumNal170_3_5.HasValue ? item.SumNal170_3_5.Value.ToString() : String.Empty,
                                СумНалВс = item.SumNalVs.GetValueOrDefault().ToString()
                            }
                        },

                        СумНалВыч = new ФайлДокументНДССумУпл164СумНалВыч()
                        {
                            НалВычОбщ = item.NalVychObshh.GetValueOrDefault().ToString(),
                            НалИсчПрод = item.NalIschProd.GetValueOrDefault().ToString(),
                            НалИсчСМР = item.NalIschSMR.GetValueOrDefault().ToString(),
                            НалПредНППок = item.NalPredNPPok.GetValueOrDefault().ToString(),
                            НалПредНППриоб = item.NalPredNPPriob.GetValueOrDefault().ToString(),
                            НалУплНОТовТС = item.NalUplNOTovTS.GetValueOrDefault().ToString(),
                            НалУплПокНА = item.NalUplPokNA.GetValueOrDefault().ToString(),
                            НалУплТамож = item.NalUplTamozh.GetValueOrDefault().ToString(),
                        },

                        СумВосУпл = null,       // списочный параметр
                        СумВычИн = null,        // списочный параметр
                    },

                    НалПодтв0 = new ФайлДокументНДСНалПодтв0()
                    {
                        СумИсчислИтог = item.SumIschislItog.GetValueOrDefault().ToString(),
                        СумОпер4 = null,        // списочный параметр
                        СумОпер1010447 = null,  // списочный параметр
                        СумОпер1010448 = null   // списочный параметр
                    },

                    НалНеПодтв0 = new ФайлДокументНДСНалНеПодтв0()
                    {
                        НалИсчислИт = item.NalIschislIt.GetValueOrDefault().ToString(),
                        СумНал164Ит = item.SumNal164It.GetValueOrDefault().ToString(),
                        НалВычНеПодИт = item.NalVychNePodIt.GetValueOrDefault().ToString(),

                        СумОпер6 = null,        // списочный параметр
                        СумОпер1010450 = null,  // списочный параметр

                        СумОпер1010449 = new ФайлДокументНДСНалНеПодтв0СумОпер1010449()
                        {
                            НалВосст = item.SumOper1010449NalVosst.GetValueOrDefault().ToString(),
                            КодОпер = item.SumOper1010449KodOper.ParseEnum<ФайлДокументНДСНалНеПодтв0СумОпер1010449КодОпер>(),
                            КорИсч16423 = item.SumOper1010449KorIsch.GetValueOrDefault().ToString(),
                            НалБаза = item.SumOper1010449NalBaza.GetValueOrDefault().ToString()
                        }
                    },

                    НалВычПред0 = new ФайлДокументНДСНалВычПред0
                    {
                        СумВозмПдтв = item.SumVozmPdtv.GetValueOrDefault().ToString(),
                        СумВозмНеПдтв = item.SumVozmNePdtv.GetValueOrDefault().ToString(),

                        СумПер = null           // списочный параметр
                    },

                    ОперНеНал = new ФайлДокументНДСОперНеНал()
                    {
                        ОплПостСв6Мес = item.OplPostSv6Mes.GetValueOrDefault().ToString(),

                        СумОпер7 = null         // списочный параметр
                    },

                    КнигаПокуп = new ФайлДокументНДСКнигаПокуп { НаимКнПок = item.NaimKnPok },
                    КнигаПокупДЛ = new ФайлДокументНДСКнигаПокупДЛ { НаимКнПокДЛ = item.NaimKnPokDL },
                    КнигаПрод = new ФайлДокументНДСКнигаПрод { НаимКнПрод = item.NaimKnProd },
                    КнигаПродДЛ = new ФайлДокументНДСКнигаПродДЛ { НаимКнПродДЛ = item.NaimKnProdDL },
                    ЖУчВыстСчФ = new ФайлДокументНДСЖУчВыстСчФ { НаимЖУчВыстСчФ = item.NaimZhUchVystSchF },
                    ЖУчПолучСчФ = new ФайлДокументНДСЖУчПолучСчФ { НаимЖУчПолучСчФ = item.NaimZhUchPoluchSchF },
                    ВыстСчФ_1735 = new ФайлДокументНДСВыстСчФ_1735 { НаимВыстСчФ_1735 = item.NaimVystSchF173_5 }
                };

            #endregion

            #region списочные параметры

            if (item.SumOper4 != null)
            {
                ret.Документ.НДС.НалПодтв0.СумОпер4 = new ФайлДокументНДСНалПодтв0СумОпер4[item.SumOper4.Count];
                
                int sc = 0;
                foreach (var i in item.SumOper4)
                {
                    ret.Документ.НДС.НалПодтв0.СумОпер4[sc] = new ФайлДокументНДСНалПодтв0СумОпер4()
                    {
                        КодОпер = i.KodOper,
                        НалБаза = i.NalBaza.GetValueOrDefault().ToString(),
                        НалВосст = i.NalVosst.GetValueOrDefault().ToString(),
                        НалВычПод = i.NalVychPod.GetValueOrDefault().ToString(),
                        НалНеПод = i.NalNePod.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (!string.IsNullOrEmpty(item.KodOper47))
            {
                ret.Документ.НДС.НалПодтв0.СумОпер1010447 = new ФайлДокументНДСНалПодтв0СумОпер1010447()
                {
                    КодОпер = item.KodOper47.ParseEnum<ФайлДокументНДСНалПодтв0СумОпер1010447КодОпер>(),
                    НалБаза = item.NalBaza47.GetValueOrDefault().ToString(),
                    НалВосст = item.NalVosst47.GetValueOrDefault().ToString()
                };
            }

            if (!string.IsNullOrEmpty(item.KodOper48))
            {

                ret.Документ.НДС.НалПодтв0.СумОпер1010448 = new ФайлДокументНДСНалПодтв0СумОпер1010448()
                    {
                        КодОпер = item.KodOper48.ParseEnum<ФайлДокументНДСНалПодтв0СумОпер1010448КодОпер>(),
                        КорНалБазаУв = item.KorNalBazaUv48.GetValueOrDefault().ToString(),
                        КорНалБазаУм = item.KorNalBazaUm48.GetValueOrDefault().ToString()
                    };
            }

            if (!string.IsNullOrEmpty(item.KodOper50))
            {
                ret.Документ.НДС.НалНеПодтв0.СумОпер1010450 = new ФайлДокументНДСНалНеПодтв0СумОпер1010450()
                    {
                        КодОпер = item.KodOper50.ParseEnum<ФайлДокументНДСНалНеПодтв0СумОпер1010450КодОпер>(),
                        КорНалБазаУв = item.KorNalBazaUv50.GetValueOrDefault().ToString(),
                        КорНалБазаУм = item.KorNalBazaUm50.GetValueOrDefault().ToString(),
                        КорИсч16423Ув = item.KorIschUv50.GetValueOrDefault().ToString(),
                        КорИсч16423Ум = item.KorIschUm50.GetValueOrDefault().ToString()
                    };
            }
            if (item.SumOper6 != null)
            {
                ret.Документ.НДС.НалНеПодтв0.СумОпер6 = new ФайлДокументНДСНалНеПодтв0СумОпер6[item.SumOper6.Count];

                int sc = 0;
                foreach (var i in item.SumOper6)
                {
                    ret.Документ.НДС.НалНеПодтв0.СумОпер6[sc] = new ФайлДокументНДСНалНеПодтв0СумОпер6()
                    {
                        КодОпер = i.KodOper,
                        НалБаза = i.NalBaza.GetValueOrDefault().ToString(),
                        НалВычНеПод = i.NalVychNePod.GetValueOrDefault().ToString(),
                        СумНал164 = i.SumNal164.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (item.SumOper7 != null)
            {
                ret.Документ.НДС.ОперНеНал.СумОпер7 = new ФайлДокументНДСОперНеНалСумОпер7[item.SumOper7.Count];

                int sc = 0;
                foreach (var i in item.SumOper7)
                {
                    ret.Документ.НДС.ОперНеНал.СумОпер7[sc] = new ФайлДокументНДСОперНеНалСумОпер7()
                    {
                        КодОпер = i.KodOper,
                        НалНеВыч = i.NalNeVych.GetValueOrDefault().ToString(),
                        СтПриобТов = i.StPriobTov.GetValueOrDefault().ToString(),
                        СтРеалТов = i.StRealTov.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (item.SumPer != null)
            {
                ret.Документ.НДС.НалВычПред0.СумПер = new ФайлДокументНДСНалВычПред0СумПер[item.SumPer.Count];

                int sc = 0;
                foreach (var i in item.SumPer)
                {
                    ret.Документ.НДС.НалВычПред0.СумПер[sc] = new ФайлДокументНДСНалВычПред0СумПер()
                    {
                        ОтчетГод = i.OtchetGod,
                        Период = i.Period.ParseEnum<ФайлДокументНДСНалВычПред0СумПерПериод>(),
                        СумОпер5 = null         // списочный параметр
                    };


                    if (i.SumOper5 != null)
                    {
                        ret.Документ.НДС.НалВычПред0.СумПер[sc].СумОпер5 = new ФайлДокументНДСНалВычПред0СумПерСумОпер5[i.SumOper5.Count];

                        int count = 0;
                        foreach (var j in i.SumOper5)
                        {
                            ret.Документ.НДС.НалВычПред0.СумПер[sc].СумОпер5[count] = new ФайлДокументНДСНалВычПред0СумПерСумОпер5()
                            {
                                КодОпер = j.KodOper,
                                НалБазаНеПод = j.NalBazaNePod.GetValueOrDefault().ToString(),
                                НалБазаПод = j.NalBazaPod.GetValueOrDefault().ToString(),
                                НалВычНеПод = j.NalVychNePod.GetValueOrDefault().ToString(),
                                НалВычПод = j.NalVychPod.GetValueOrDefault().ToString()
                            };

                            count++;
                        }
                    }

                    sc++;
                }
            }

            if (item.SumUplNA != null)
            {
                ret.Документ.НДС.СумУплНА = new ФайлДокументНДССумУплНА[item.SumUplNA.Count];

                int sc = 0;
                foreach (var i in item.SumUplNA)
                {
                    ret.Документ.НДС.СумУплНА[sc] = new ФайлДокументНДССумУплНА()
                    {
                        Item = null,        // условный параметр
                        КБК = i.KBK,
                        КодОпер = i.KodOper,
                        КППИно = i.KPPIno,
                        ОКТМО = i.OKTMO,
                        СумИсчисл = i.SumIschisl.GetValueOrDefault().ToString(),
                        СумИсчислНА = i.SumIschislNA.GetValueOrDefault().ToString(),
                        СумИсчислОпл = i.SumIschislOpl.GetValueOrDefault().ToString(),
                        СумИсчислОтгр = i.SumIschislOtgr.GetValueOrDefault().ToString()
                    };

                    // ЮЛ или ФЛ
                    if (!string.IsNullOrEmpty(i.KPPIno))
                    {
                        ret.Документ.НДС.СумУплНА[sc].Item = new ФайлДокументНДССумУплНАСведПродЮЛ()
                        {
                            ИННЮЛПрод = i.INNProd,
                            НаимПрод = i.NaimProd
                        };
                    }
                    else
                    {
                        ret.Документ.НДС.СумУплНА[sc].Item = new ФайлДокументНДССумУплНАСведПродФЛ()
                        {
                            ИННФЛПрод = i.INNProd,
                            ФИОПрод = new ФИОТип()
                            {
                                Имя = i.Imya,
                                Отчество = i.Otchestvo,
                                Фамилия = i.Familiya
                            }
                        };
                    }

                    sc++;
                }
            }


            if (item.SumVosUpl != null)
            {
                ret.Документ.НДС.СумУпл164.СумВосУпл = new ФайлДокументНДССумУпл164СумВосУпл[item.SumVosUpl.Count];

                int sc = 0;
                foreach (var i in item.SumVosUpl)
                {
                    ret.Документ.НДС.СумУпл164.СумВосУпл[sc] = new ФайлДокументНДССумУпл164СумВосУпл()
                    {
                        АдрМННед = new АдрРФТип()
                        {
                            Город = i.Gorod,
                            Дом = i.Dom,
                            Индекс = i.Indeks,
                            Кварт = i.Kvart,
                            КодРегион = i.KodRegion,
                            Корпус = i.Korpus,
                            НаселПункт = i.NaselPunkt,
                            Район = i.Rajon,
                            Улица = i.Ulica
                        },
                        ДатаВводОН = i.DataVvodON.HasValue ? i.DataVvodON.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        ДатаНачАмОтч = i.DataNachAmOtch.HasValue ? i.DataNachAmOtch.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        КодОпНедв = i.KodOpNedv,
                        НаимНедв = i.NaimNedv,
                        НалВычОН = i.NalVychON.GetValueOrDefault().ToString(),
                        СведНалГод = null,          // списочный параметр
                        СтВводОН = i.StVvodON.GetValueOrDefault().ToString()
                    };

                    if (i.SvedNalGod != null)
                    {
                        ret.Документ.НДС.СумУпл164.СумВосУпл[sc].СведНалГод = new ФайлДокументНДССумУпл164СумВосУплСведНалГод[i.SvedNalGod.Count];

                        int count = 0;
                        foreach (var j in i.SvedNalGod)
                        {
                            ret.Документ.НДС.СумУпл164.СумВосУпл[sc].СведНалГод[count] = new ФайлДокументНДССумУпл164СумВосУплСведНалГод()
                            {
                                ГодОтч = j.GodOtch,
                                ДатаИсп170 = j.DataIsp170.HasValue ? j.DataIsp170.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                                ДоляНеОбл = j.DolyaNeObl.GetValueOrDefault(),
                                НалГод = j.NalGod.GetValueOrDefault().ToString()
                            };
   
                            count++;
                        }
                    }

                    sc++;
                }
            }

            if (item.SvedNalGodI != null)
            {
                ret.Документ.НДС.СумУпл164.СумВычИн = new ФайлДокументНДССумУпл164СведНалГодИ[item.SvedNalGodI.Count];

                int sc = 0;
                foreach (var i in item.SvedNalGodI)
                {
                    ret.Документ.НДС.СумУпл164.СумВычИн[sc] = new ФайлДокументНДССумУпл164СведНалГодИ()
                    {
                        КППИнУч = i.KPPInUch,
                        СумНалВыч = i.SumNalVych.GetValueOrDefault().ToString(),
                        СумНалИсч = i.SumNalIsch.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            #endregion

            return ret;
        }

        public static Файл505 ASKDeklToФайл505(ASKDekl item)
        {
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            Файл505 ret = new Файл505();

            #region Базовые параметры

            ret.ИдФайл = item.IdFajl;
            ret.ВерсПрог = item.VersProg;
            ret.ВерсФорм = item.VersForm.ParseEnum<ФайлВерсФорм>();
            ret.ПризнНал812 = item.PriznNal8_12.ParseEnum<ФайлПризнНал812>();
            ret.ПризнНал8 = item.PriznNal8.ParseEnum<ФайлПризнНал8>();
            ret.ПризнНал81 = item.PriznNal81.ParseEnum<ФайлПризнНал81>();
            ret.ПризнНал9 = item.PriznNal9.ParseEnum<ФайлПризнНал9>();
            ret.ПризнНал91 = item.PriznNal91.ParseEnum<ФайлПризнНал91>();
            ret.ПризнНал10 = item.PriznNal10.ParseEnum<ФайлПризнНал10>();
            ret.ПризнНал11 = item.PriznNal11.ParseEnum<ФайлПризнНал11>();
            ret.ПризнНал12 = item.PriznNal12.ParseEnum<ФайлПризнНал12>();

            ret.Документ = new ФайлДокумент
            {
                КНД = item.KND.ParseEnum<ФайлДокументКНД>(),
                Период = item.Period.ParseEnum<ФайлДокументПериод>(),
                ОтчетГод = item.OtchetGod,
                КодНО = item.KodNO,
                НомКорр = item.NomKorr,
                ПоМесту = item.PoMestu.ParseEnum<ФайлДокументПоМесту>(),
                ДатаДок = item.DataDok.HasValue ? item.DataDok.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null
            };


            ret.Документ.СвНП = new ФайлДокументСвНП
            {
                ОКВЭД = item.OKVED,
                Тлф = item.Tlf,
                Item = null         // условный параметр
            };

            // ЮЛ или ФЛ
            if (item.INNNP.Length == 10)
            {
                ret.Документ.СвНП.Item = new ФайлДокументСвНПНПЮЛ
                {
                    НаимОрг = item.NaimOrg,
                    ИННЮЛ = item.INNNP,
                    КПП = item.KPPNP
                };

                if (!string.IsNullOrEmpty(item.INNReorg))
                {
                    ((ФайлДокументСвНПНПЮЛ)ret.Документ.СвНП.Item).СвРеоргЮЛ = new ФайлДокументСвНПНПЮЛСвРеоргЮЛ()
                    {
                        ИННЮЛ = item.INNReorg,
                        КПП = item.KPPReorg,
                        ФормРеорг = item.FormReorg.ParseEnum<ФайлДокументСвНПНПЮЛСвРеоргЮЛФормРеорг>()
                    };
                }
            }
            else
            {
                ret.Документ.СвНП.Item = new ФайлДокументСвНПНПФЛ()
                {
                    ИННФЛ = item.INNNP,
                    ФИО = new ФИОТип()
                    {
                        Имя = item.ImyaNP,
                        Отчество = item.OtchestvoNP,
                        Фамилия = item.FamiliyaNP
                    }
                };
            }

            ret.Документ.Подписант = new ФайлДокументПодписант
            {
                ПрПодп = item.PrPodp.ParseEnum<ФайлДокументПодписантПрПодп>(),
                ФИО = new ФИОТип()
                {
                    Имя = item.ImyaPodp,
                    Фамилия = item.FamiliyaPodp,
                    Отчество = item.OtchestvoPodp
                },
                СвПред = new ФайлДокументПодписантСвПред()
                {
                    НаимДок = item.NaimDok,
                    НаимОрг = item.NaimOrgPred
                }
            };


            ret.Документ.НДС = new ФайлДокументНДС
            {
                СумУплНА = null,    // списочный параметр

                СумУплНП = new ФайлДокументНДССумУплНП()
                {
                    КБК = item.KBK,
                    ОКТМО = item.OKTMO,
                    СумПУ_1731 = item.SumPU173_1.GetValueOrDefault().ToString(),
                    СумПУ_1735 = item.SumPU173_5.GetValueOrDefault().ToString(),
                    ДатаКонДогИТ = item.DataKonDogIT.HasValue ? item.DataKonDogIT.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                    ДатаНачДогИТ = item.DataNachDogIT.HasValue ? item.DataNachDogIT.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                    НомДогИТ = item.NomDogIT
                },

                СумУпл164 = new ФайлДокументНДССумУпл164()
                {
                    НалПУ164 = item.NalPU164.ToString(),
                    СумНалОб = new ФайлДокументНДССумУпл164СумНалОб()
                    {
                        НалВосстОбщ = item.NalVosstObshh.GetValueOrDefault().ToString(),
                        РеалТов18 = GetSvedSunNal(item.RealTov18NalBaza, item.RealTov18SumNal),
                        РеалТов10 = GetSvedSunNal(item.RealTov10NalBaza, item.RealTov10SumNal),
                        РеалТов118 = GetSvedSunNal(item.RealTov118NalBaza, item.RealTov118SumNal),
                        РеалТов110 = GetSvedSunNal(item.RealTov110NalBaza, item.RealTov110SumNal),
                        РеалПредИК = GetSvedSunNal(item.RealPredIKNalBaza, item.RealPredIKSumNal),
                        ВыпСМРСоб = GetSvedSunNal(item.VypSMRSobNalBaza, item.VypSMRSobSumNal),
                        ОплПредПост = GetSvedSunNal(item.OplPredPostNalBaza, item.OplPredPostSumNal),
                        КорРеалТов18 = GetSvedSunNal(item.KorRealTov18NalBaza, item.KorRealTov18SumNal),
                        КорРеалТов10 = GetSvedSunNal(item.KorRealTov10NalBaza, item.KorRealTov10SumNal),
                        КорРеалТов118 = GetSvedSunNal(item.KorRealTov118NalBaza, item.KorRealTov118SumNal),
                        КорРеалТов110 = GetSvedSunNal(item.KorRealTov110NalBaza, item.KorRealTov110SumNal),
                        КорРеалПредИК = GetSvedSunNal(item.KorRealPredIKNalBaza, item.KorRealPredIKSumNal),
                        РеалСрок151_1_118 = GetSvedSunNal(item.RlSr118NalBaza, item.RlSr118SumNal),
                        РеалСрок151_1_110 = GetSvedSunNal(item.RlSr110NalBaza, item.RlSr110SumNal),
                        УплДеклар151_1 = GetSvedSunNal(item.UplD151NalBaza, item.UplD151SumNal),
                        УплДеклар173_6 = GetSvedSunNal(item.UplD173NalBaza, item.UplD173SumNal),
                        СумНалВосст = new ФайлДокументНДССумУпл164СумНалОбСумНалВосст()
                        {
                            СумНал17033 = item.SumNal170_3_3.GetValueOrDefault().ToString(),
                            СумНалОперСт0 = item.SumNal170_3_5.HasValue ? item.SumNal170_3_5.Value.ToString() : String.Empty,
                            СумНалВс = item.SumNalVs.GetValueOrDefault().ToString(),
                        }
                    },

                    СумНалВыч = new ФайлДокументНДССумУпл164СумНалВыч()
                    {
                        НалВычОбщ = item.NalVychObshh.GetValueOrDefault().ToString(),
                        НалИсчПрод = item.NalIschProd.GetValueOrDefault().ToString(),
                        НалИсчСМР = item.NalIschSMR.GetValueOrDefault().ToString(),
                        НалПредНППок = item.NalPredNPPok.GetValueOrDefault().ToString(),
                        НалПредНППриоб = item.NalPredNPPriob.GetValueOrDefault().ToString(),
                        НалУплНОТовТС = item.NalUplNOTovTS.GetValueOrDefault().ToString(),
                        НалУплПокНА = item.NalUplPokNA.GetValueOrDefault().ToString(),
                        НалУплТамож = item.NalUplTamozh.GetValueOrDefault().ToString(),
                        НалВыч171_14 = item.NalVych171_14.GetValueOrDefault().ToString(),
                        НалПредНПКапСтр = item.NalPredNPKapStr.GetValueOrDefault().ToString()
                    },

                    СумВосУпл = null,       // списочный параметр
                    СумВычИн = null,        // списочный параметр
                },

                НалПодтв0 = new ФайлДокументНДСНалПодтв0()
                {
                    СумИсчислИтог = item.SumIschislItog.GetValueOrDefault().ToString(),
                    СумОпер4 = null,        // списочный параметр
                    СумОпер1010447 = null,  // списочный параметр
                    СумОпер1010448 = null   // списочный параметр
                },

                НалНеПодтв0 = new ФайлДокументНДСНалНеПодтв0()
                {
                    НалИсчислИт = item.NalIschislIt.GetValueOrDefault().ToString(),
                    СумНал164Ит = item.SumNal164It.GetValueOrDefault().ToString(),
                    НалВычНеПодИт = item.NalVychNePodIt.GetValueOrDefault().ToString(),

                    СумОпер6 = null,        // списочный параметр
                    СумОпер1010450 = null,  // списочный параметр

                    СумОпер1010449 = new ФайлДокументНДСНалНеПодтв0СумОпер1010449()
                    {
                        НалВосст = item.SumOper1010449NalVosst.GetValueOrDefault().ToString(),
                        КодОпер = item.SumOper1010449KodOper.ParseEnum<ФайлДокументНДСНалНеПодтв0СумОпер1010449КодОпер>(),
                        КорИсч16423 = item.SumOper1010449KorIsch.GetValueOrDefault().ToString(),
                        НалБаза = item.SumOper1010449NalBaza.GetValueOrDefault().ToString()
                    }
                },

                НалВычПред0 = new ФайлДокументНДСНалВычПред0
                {
                    СумВозмПдтв = item.SumVozmPdtv.GetValueOrDefault().ToString(),
                    СумВозмНеПдтв = item.SumVozmNePdtv.GetValueOrDefault().ToString(),

                    СумПер = null           // списочный параметр
                },

                ОперНеНал = new ФайлДокументНДСОперНеНал()
                {
                    ОплПостСв6Мес = item.OplPostSv6Mes.GetValueOrDefault().ToString(),

                    СумОпер7 = null         // списочный параметр
                },

                КнигаПокуп = new ФайлДокументНДСКнигаПокуп { НаимКнПок = item.NaimKnPok },
                КнигаПокупДЛ = new ФайлДокументНДСКнигаПокупДЛ { НаимКнПокДЛ = item.NaimKnPokDL },
                КнигаПрод = new ФайлДокументНДСКнигаПрод { НаимКнПрод = item.NaimKnProd },
                КнигаПродДЛ = new ФайлДокументНДСКнигаПродДЛ { НаимКнПродДЛ = item.NaimKnProdDL },
                ЖУчВыстСчФ = new ФайлДокументНДСЖУчВыстСчФ { НаимЖУчВыстСчФ = item.NaimZhUchVystSchF },
                ЖУчПолучСчФ = new ФайлДокументНДСЖУчПолучСчФ { НаимЖУчПолучСчФ = item.NaimZhUchPoluchSchF },
                ВыстСчФ_1735 = new ФайлДокументНДСВыстСчФ_1735 { НаимВыстСчФ_1735 = item.NaimVystSchF173_5 }
            };

            #endregion

            #region списочные параметры

            if (item.SumOper4 != null)
            {
                ret.Документ.НДС.НалПодтв0.СумОпер4 = new ФайлДокументНДСНалПодтв0СумОпер4[item.SumOper4.Count];

                int sc = 0;
                foreach (var i in item.SumOper4)
                {
                    ret.Документ.НДС.НалПодтв0.СумОпер4[sc] = new ФайлДокументНДСНалПодтв0СумОпер4()
                    {
                        КодОпер = i.KodOper,
                        НалБаза = i.NalBaza.GetValueOrDefault().ToString(),
                        НалВосст = i.NalVosst.GetValueOrDefault().ToString(),
                        НалВычПод = i.NalVychPod.GetValueOrDefault().ToString(),
                        НалНеПод = i.NalNePod.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (!string.IsNullOrEmpty(item.KodOper47))
            {
                ret.Документ.НДС.НалПодтв0.СумОпер1010447 = new ФайлДокументНДСНалПодтв0СумОпер1010447()
                {
                    КодОпер = item.KodOper47.ParseEnum<ФайлДокументНДСНалПодтв0СумОпер1010447КодОпер>(),
                    НалБаза = item.NalBaza47.GetValueOrDefault().ToString(),
                    НалВосст = item.NalVosst47.GetValueOrDefault().ToString()
                };
            }

            if (!string.IsNullOrEmpty(item.KodOper48))
            {

                ret.Документ.НДС.НалПодтв0.СумОпер1010448 = new ФайлДокументНДСНалПодтв0СумОпер1010448()
                {
                    КодОпер = item.KodOper48.ParseEnum<ФайлДокументНДСНалПодтв0СумОпер1010448КодОпер>(),
                    КорНалБазаУв = item.KorNalBazaUv48.GetValueOrDefault().ToString(),
                    КорНалБазаУм = item.KorNalBazaUm48.GetValueOrDefault().ToString()
                };
            }

            if (!string.IsNullOrEmpty(item.KodOper50))
            {
                ret.Документ.НДС.НалНеПодтв0.СумОпер1010450 = new ФайлДокументНДСНалНеПодтв0СумОпер1010450()
                {
                    КодОпер = item.KodOper50.ParseEnum<ФайлДокументНДСНалНеПодтв0СумОпер1010450КодОпер>(),
                    КорНалБазаУв = item.KorNalBazaUv50.GetValueOrDefault().ToString(),
                    КорНалБазаУм = item.KorNalBazaUm50.GetValueOrDefault().ToString(),
                    КорИсч16423Ув = item.KorIschUv50.GetValueOrDefault().ToString(),
                    КорИсч16423Ум = item.KorIschUm50.GetValueOrDefault().ToString()
                };
            }
            if (item.SumOper6 != null)
            {
                ret.Документ.НДС.НалНеПодтв0.СумОпер6 = new ФайлДокументНДСНалНеПодтв0СумОпер6[item.SumOper6.Count];

                int sc = 0;
                foreach (var i in item.SumOper6)
                {
                    ret.Документ.НДС.НалНеПодтв0.СумОпер6[sc] = new ФайлДокументНДСНалНеПодтв0СумОпер6()
                    {
                        КодОпер = i.KodOper,
                        НалБаза = i.NalBaza.GetValueOrDefault().ToString(),
                        НалВычНеПод = i.NalVychNePod.GetValueOrDefault().ToString(),
                        СумНал164 = i.SumNal164.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (item.SumOper7 != null)
            {
                ret.Документ.НДС.ОперНеНал.СумОпер7 = new ФайлДокументНДСОперНеНалСумОпер7[item.SumOper7.Count];

                int sc = 0;
                foreach (var i in item.SumOper7)
                {
                    ret.Документ.НДС.ОперНеНал.СумОпер7[sc] = new ФайлДокументНДСОперНеНалСумОпер7()
                    {
                        КодОпер = i.KodOper,
                        НалНеВыч = i.NalNeVych.GetValueOrDefault().ToString(),
                        СтПриобТов = i.StPriobTov.GetValueOrDefault().ToString(),
                        СтРеалТов = i.StRealTov.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            if (item.SumPer != null)
            {
                ret.Документ.НДС.НалВычПред0.СумПер = new ФайлДокументНДСНалВычПред0СумПер[item.SumPer.Count];

                int sc = 0;
                foreach (var i in item.SumPer)
                {
                    ret.Документ.НДС.НалВычПред0.СумПер[sc] = new ФайлДокументНДСНалВычПред0СумПер()
                    {
                        ОтчетГод = i.OtchetGod,
                        Период = i.Period.ParseEnum<ФайлДокументНДСНалВычПред0СумПерПериод>(),
                        СумОпер5 = null         // списочный параметр
                    };


                    if (i.SumOper5 != null)
                    {
                        ret.Документ.НДС.НалВычПред0.СумПер[sc].СумОпер5 = new ФайлДокументНДСНалВычПред0СумПерСумОпер5[i.SumOper5.Count];

                        int count = 0;
                        foreach (var j in i.SumOper5)
                        {
                            ret.Документ.НДС.НалВычПред0.СумПер[sc].СумОпер5[count] = new ФайлДокументНДСНалВычПред0СумПерСумОпер5()
                            {
                                КодОпер = j.KodOper,
                                НалБазаНеПод = j.NalBazaNePod.GetValueOrDefault().ToString(),
                                НалБазаПод = j.NalBazaPod.GetValueOrDefault().ToString(),
                                НалВычНеПод = j.NalVychNePod.GetValueOrDefault().ToString(),
                                НалВычПод = j.NalVychPod.GetValueOrDefault().ToString()
                            };

                            count++;
                        }
                    }

                    sc++;
                }
            }

            if (item.SumUplNA != null)
            {
                ret.Документ.НДС.СумУплНА = new ФайлДокументНДССумУплНА[item.SumUplNA.Count];

                int sc = 0;
                foreach (var i in item.SumUplNA)
                {
                    ret.Документ.НДС.СумУплНА[sc] = new ФайлДокументНДССумУплНА()
                    {
                        Item = null,        // условный параметр
                        КБК = i.KBK,
                        КодОпер = i.KodOper,
                        КППИно = i.KPPIno,
                        ОКТМО = i.OKTMO,
                        СумИсчисл = i.SumIschisl.GetValueOrDefault().ToString(),
                        СумИсчислНА = i.SumIschislNA.GetValueOrDefault().ToString(),
                        СумИсчислОпл = i.SumIschislOpl.GetValueOrDefault().ToString(),
                        СумИсчислОтгр = i.SumIschislOtgr.GetValueOrDefault().ToString()
                    };

                    // ЮЛ или ФЛ
                    if (!string.IsNullOrEmpty(i.KPPIno))
                    {
                        ret.Документ.НДС.СумУплНА[sc].Item = new ФайлДокументНДССумУплНАСведПродЮЛ()
                        {
                            ИННЮЛПрод = i.INNProd,
                            НаимПрод = i.NaimProd
                        };
                    }
                    else
                    {
                        ret.Документ.НДС.СумУплНА[sc].Item = new ФайлДокументНДССумУплНАСведПродФЛ()
                        {
                            ИННФЛПрод = i.INNProd,
                            ФИОПрод = new ФИОТип()
                            {
                                Имя = i.Imya,
                                Отчество = i.Otchestvo,
                                Фамилия = i.Familiya
                            }
                        };
                    }

                    sc++;
                }
            }


            if (item.SumVosUpl != null)
            {
                ret.Документ.НДС.СумУпл164.СумВосУпл = new ФайлДокументНДССумУпл164СумВосУпл[item.SumVosUpl.Count];

                int sc = 0;
                foreach (var i in item.SumVosUpl)
                {
                    ret.Документ.НДС.СумУпл164.СумВосУпл[sc] = new ФайлДокументНДССумУпл164СумВосУпл()
                    {
                        АдрМННед = new АдрРФТип()
                        {
                            Город = i.Gorod,
                            Дом = i.Dom,
                            Индекс = i.Indeks,
                            Кварт = i.Kvart,
                            КодРегион = i.KodRegion,
                            Корпус = i.Korpus,
                            НаселПункт = i.NaselPunkt,
                            Район = i.Rajon,
                            Улица = i.Ulica
                        },
                        НаимООС = i.NaimOOS,
                        КодОпООС = i.KodOpOOS,
                        ДатаВводООС = i.DataVvodOOC.HasValue ? i.DataVvodOOC.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        СтВводООС = i.StVvodOOS.GetValueOrDefault().ToString(),
                        НалВычООС = i.NalVychOOS.GetValueOrDefault().ToString(),
                        ДатаВводОН = i.DataVvodON.HasValue ? i.DataVvodON.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        ДатаНачАмОтч = i.DataNachAmOtch.HasValue ? i.DataNachAmOtch.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                        КодОпНедв = i.KodOpNedv,
                        НаимНедв = i.NaimNedv,
                        НалВычОН = i.NalVychON.GetValueOrDefault().ToString(),
                        СведНалГод = null,          // списочный параметр
                        СтВводОН = i.StVvodON.GetValueOrDefault().ToString()
                    };

                    if (i.SvedNalGod != null)
                    {
                        ret.Документ.НДС.СумУпл164.СумВосУпл[sc].СведНалГод = new ФайлДокументНДССумУпл164СумВосУплСведНалГод[i.SvedNalGod.Count];

                        int count = 0;
                        foreach (var j in i.SvedNalGod)
                        {
                            ret.Документ.НДС.СумУпл164.СумВосУпл[sc].СведНалГод[count] = new ФайлДокументНДССумУпл164СумВосУплСведНалГод()
                            {
                                ГодОтч = j.GodOtch,
                                ДатаИсп170 = j.DataIsp170.HasValue ? j.DataIsp170.Value.ToString(ciRUS.DateTimeFormat.ShortDatePattern) : null,
                                ДоляНеОбл = j.DolyaNeObl.GetValueOrDefault(),
                                НалГод = j.NalGod.GetValueOrDefault().ToString()
                            };

                            count++;
                        }
                    }

                    sc++;
                }
            }

            if (item.SvedNalGodI != null)
            {
                ret.Документ.НДС.СумУпл164.СумВычИн = new ФайлДокументНДССумУпл164СведНалГодИ[item.SvedNalGodI.Count];

                int sc = 0;
                foreach (var i in item.SvedNalGodI)
                {
                    ret.Документ.НДС.СумУпл164.СумВычИн[sc] = new ФайлДокументНДССумУпл164СведНалГодИ()
                    {
                        КППИнУч = i.KPPInUch,
                        СумНалВыч = i.SumNalVych.GetValueOrDefault().ToString(),
                        СумНалИсч = i.SumNalIsch.GetValueOrDefault().ToString()
                    };

                    sc++;
                }
            }

            #endregion

            return ret;
        }
        
        private static СведСумНал GetSvedSunNal(long? nalBaza, long? sumNal)
        {
            return new СведСумНал()
            {
                НалБаза = nalBaza.GetValueOrDefault().ToString(),
                СумНал = sumNal.GetValueOrDefault().ToString()
            };
        }
    }
}
