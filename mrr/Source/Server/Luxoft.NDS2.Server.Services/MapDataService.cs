﻿using System.Collections.Generic;
using System.Linq;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using System;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IMacroReportDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class MacroReportDataService : IMacroReportDataService
    {
        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public MacroReportDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        public OperationResult<List<MapResponseDataNds>> GetMapDataNds(MapRequestData model)
        {
            return _helper.Do(() => TableAdapterCreator.MacroNdsReportMap(_helper).GetMapData(model));
        }

        public OperationResult<List<MapResponseDataDiscrepancy>> GetMapDataDiscrepancy(MapRequestData model)
        {
            return _helper.Do(() => TableAdapterCreator.MacroDiscrepancyReportMap(_helper).GetMapData(model));
        }

        public OperationResult<PageResult<NpResponseData>> GetNpData(QueryConditions queryConditions, bool withTotal)
        {
            var result = _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    var adapter = TableAdapterCreator.MacroReportAdapter(_helper, connectionFactory);
                    var searchCriteria = queryConditions.Filter.ToFilterGroup();
                    var data = adapter.Search(searchCriteria, queryConditions.Sorting,
                        queryConditions.PaginationDetails.RowsToSkip
                        ,
                        queryConditions.PaginationDetails.RowsToSkip +
                        queryConditions.PaginationDetails.RowsToTake.Value
                        );

                    var count = adapter.Count(searchCriteria);
                    int total = 0;
                    if (withTotal)
                    {
                        total = adapter.Count(queryConditions.Filter.Where(s => !s.UserDefined).ToFilterGroup());
                    }
                    return new PageResult<NpResponseData>(data, count, total);
                }
            });
            return result;
        }
    }
}
