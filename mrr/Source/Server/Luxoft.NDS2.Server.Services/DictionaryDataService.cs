﻿using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using CommonComponents.ThesAccess;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Server.DAL.OrganizationStructure;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDictionaryDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class DictionaryDataService : IDictionaryDataService
    {
        private readonly ServiceHelpers _helper;
        private readonly DalManager _dalManager;

        private IClassifierService _classifierService = null;

        public DictionaryDataService(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _dalManager = new DalManager(_helper);
        }

        # region Территориальная и орг. структура

        public OperationResult<Sono[]> GetSonoDictionary()
        {
            return _helper.Do(() =>
                {
                    using (var connection = new SingleConnectionFactory(_helper))
                    {
                        var adapter = OrganizationStructureAdapterCreator.Sono(_helper, connection);

                        return adapter.All().ToArray();
                    }
                });
        }

        public OperationResult<Region[]> GetRegionDictionary()
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var adapter = OrganizationStructureAdapterCreator.Regions(_helper, connection);

                    return adapter.All().ToArray();
                }
            });
        }

        public OperationResult<Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure.FederalDistrict[]> GetFederalDistrictDictionary()
        {
            return _helper.Do(() =>
            {
                using (var connection = new SingleConnectionFactory(_helper))
                {
                    var adapter = OrganizationStructureAdapterCreator.FederalDistricts(_helper, connection);

                    return adapter.All().ToArray();
                }
            });
        }

        # endregion

        public OperationResult<NsiMetadataEntry> GetLookupMetadata(string name)
        {
            List<NsiMetadataEntry> metadatas = new List<NsiMetadataEntry>();
            var result = new OperationResult<NsiMetadataEntry>();

            try
            {
                NsiMetadataEntry newEntry = new NsiMetadataEntry();
                metadatas.Add(newEntry);
                newEntry.LocationType = NsiEntryDataLocationType.Remote;
                newEntry.Name = "v$sono";
                newEntry.TableName = "v$sono";
                newEntry.KeyField = "s_code";
                newEntry.CodeField = "s_code";
                newEntry.DescriptionField = "s_name";
                newEntry.CodeFieldComment = "Код ИФНС";
                newEntry.DescriptionFieldComment = "Наименование";
                newEntry.SourceTarget = "v$sono";
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.Message = ex.Message;
            }

            result.Result = metadatas.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            result.Status = ResultStatus.Success;

            return result;
        }

        public OperationResult<List<LookupResponse>> GetLookupData(LookupRequest request)
        {
            List<LookupResponse> rets = new List<LookupResponse>();
            LookupResponse item;

            if (request.DataSourceKey == "v$sono")
            {
                List<DictionaryNalogOrgan> nalogOrgans = _dalManager.GetTableData<DictionaryNalogOrgan>("v$sono").Where(p => p.FullName.Contains(request.FilterValue.Replace("%", String.Empty))).OrderBy(p => p.S_CODE).ToList();
                foreach (DictionaryNalogOrgan itemNO in nalogOrgans)
                {
                    item = new LookupResponse();
                    item.IsSelected = false;
                    item.Code = itemNO.S_CODE;
                    item.Value1 = itemNO.FullName;
                    item.Value2 = itemNO.S_NAME;
                    rets.Add(item);
                }
            }
            var retOperationResult = new OperationResult<List<LookupResponse>>();
            retOperationResult.Result = rets;
            retOperationResult.Status = ResultStatus.Success;

            return retOperationResult;
        }

        public OperationResult<List<DictionaryNalogOrgan>> GetInfoOfDictionaryTable()
        {
            return _helper.Do(() => _dalManager.GetTableData<DictionaryNalogOrgan>("v$sono").OrderBy(p => p.S_CODE).ToList());
        }

        public OperationResult<List<TableDictionaryColumnInfo>> GetTablesDictionaryColumnInfo()
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).GetTablesDictionaryColumnInfo());
        }

        public OperationResult<List<TableDictionaryInfo>> GetTablesDictionaryInfo()
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).GetTablesDictionaryInfo());
        }

        public OperationResult<List<Dictionary<string, object>>> GetTableRows(string tableName, List<string> columns)
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).GetTableRows(tableName, columns));
        }

        public OperationResult UpdateTableRow(string tableName, string key, string value, Dictionary<string, string> row)
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).UpdateTableRow(tableName, key, value, row));
        }

        public OperationResult AddTableRow(string tableName, Dictionary<string, string> row)
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).AddTableRow(tableName, row));
        }

        public OperationResult DeleteTableRow(string tableName, string key, string value)
        {
            return _helper.Do(() => TableAdapterCreator.DictionaryUtilityInfo(_helper).DeleteTableRow(tableName, key, value));
        }

        public OperationResult<List<SurCode>> GetSurDictionary()
        {
            return DalHelper.ExecuteOperation(
                () => TableAdapterCreator.DictionarySUR(_helper).GetList(), 
                _helper);
        }

        public OperationResult<List<OperCode>> GetOperDictionary()
        {
            return DalHelper.ExecuteOperation(
                 () => TableAdapterCreator.DictionaryOper(_helper).GetList(),
                 _helper);
        }

        public OperationResult<List<OperCode>> GetCurrencyDictionary()
        {
            return DalHelper.ExecuteOperation(
                 () => TableAdapterCreator.DictionaryCurrency(_helper).GetList(),
                 _helper);
        }

        public OperationResult<List<OperCode>> GetBargainDictionary()
        {
            return DalHelper.ExecuteOperation(
                 () => TableAdapterCreator.DictionaryBargain(_helper).GetList(),
                 _helper);
        }

        public OperationResult<List<AnnulmentReason>> GetAnnulmentReasonDictionary()
        {
            return DalHelper.ExecuteOperation(
                () => TableAdapterCreator.DictionaryAnnulmentReason(_helper).GetList(),
                _helper);            
        }

        public OperationResult<List<DictTaxPeriod>> GetTaxPeriodDictionary()
        {
            return DalHelper.ExecuteOperation(
                () =>
                {
                    string isClassifiersCacheDisabled = _helper.GetConfigurationValue(Constants.SERVER_CLASSIFIERS_CACHE_DISABLED_KEY);
                    List<DictTaxPeriod> taxPeriods;
                    bool isCacheDisabled = false;
                    if (isClassifiersCacheDisabled != null && bool.TryParse(isClassifiersCacheDisabled, out isCacheDisabled) && isCacheDisabled)
                        taxPeriods = TableAdapterCreator.DictionaryTaxPeriodAdapter(_helper).GetList();
                    else
                        taxPeriods = ClassifierService.LookUp<DictTaxPeriod, DictTaxPeriodAccessor>( new Request( "TaxPeriods" ), "NDS2Classifiers.Common" )
                                     .Items.ToList(); 

                    return taxPeriods;
                },
                _helper);
        }

        /// <summary></summary>
        /// <exception cref="InvalidOperationException"> if '_helper.GetConfigurationValue(Constants.SERVER_CLASSIFIERS_CACHE_DISABLED_KEY) != true'. </exception>
        private IClassifierService ClassifierService
        {
            get
            {
                if (_classifierService == null)
                {
                    string thesAccessConfigProfileName = _helper.GetConfigurationValue(Constants.THES_ACESS_CONFIG_PROFILE_NAME_KEY);
                    if (thesAccessConfigProfileName == null)
                        throw new InvalidOperationException(string.Format("Key: '{0}' is not found in <appSettings> in configuration", Constants.THES_ACESS_CONFIG_PROFILE_NAME_KEY));

                    IClassifierServiceFactory classifierServiceFactory = _helper.Services.Get<IClassifierServiceFactory>();
                    _classifierService = classifierServiceFactory.GetClassifierService(_helper.Services, thesAccessConfigProfileName);
                }
                return _classifierService;
            }
        }
    }
}
