﻿using System;
using System.Collections.Generic;
using CommonComponents.Communication;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Server.Services
{
    [CommunicationContract(typeof(IDiscrepancyDataService))]
    [CommunicationService(ServiceInstanceMode.PerCall)]
    public class DiscrepancyDataServer : IDiscrepancyDataService
    {
        private const int RequestTicking = 1000;
        private const int RequestTimeout = 600000;

        private int _requestTiming;

        private ServiceHelpers _helper;
        private IAuthorizationProvider _localAuthProvider;

        public DiscrepancyDataServer(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
            _localAuthProvider = services.Get<IAuthorizationProvider>();
        }

        #region declaration list

        public OperationResult<DiscrepancyDocumentInfo> SelectDocumentInfo(long id)
        {
            var result = new OperationResult<DiscrepancyDocumentInfo>();
            try
            {
                result.Result = TableAdapterCreator.DiscrepancyDocument(_helper).GetDocument(id);
                result.Status = ResultStatus.Success;
            }
            catch (Exception ex)
            {
                _helper.LogError("Ошибка при загрузке автотребования", ex: ex);
                result.Result = new DiscrepancyDocumentInfo();
                result.Status = ResultStatus.Error;
                result.Message = string.Format("Ошибка при загрузке автотребования: {0}", ex.Message);
            }
            return result;
        }

        #endregion

        public OperationResult<bool> DiscrepancyIsClosed(long discrepancyId)
        {
            return _helper.DoEx(() => TableAdapterCreator.Discrepancy(_helper, discrepancyId).DiscrepancyIsClosed());
        }

        public OperationResult<List<DiscrepancySide>> GetDiscrepancySides(HiveRequestDiscrepancyCard hiveRequest)
        {
            return DalHelper.Execute(() => GetDiscrepancySidesInternal(hiveRequest), _helper);
        }

        private OperationResult<List<DiscrepancySide>> GetDiscrepancySidesInternal(HiveRequestDiscrepancyCard hiveRequest)
        {
            var result = new OperationResult<List<DiscrepancySide>>();

            result.Result = TableAdapterCreator.Discrepancy(_helper, hiveRequest.DiscrepancyId).GetDiscrepancySides();

            if (result.Result != null && result.Result.Count > 0)
	        {
		        result.Status = ResultStatus.Success;
	        } else 
            {
                IHiveRequestAdapter hiveRequestAdapter = DiscrepancyCardAdapterCreator.HiveRequestAdapter(_helper, hiveRequest);

                int status;
                long requestId = hiveRequestAdapter.GetRequestId(out status);

                while (_requestTiming < RequestTimeout)
                {
                    if (status == (int)HiveRequestStatus.FinishLoading)
                    {
                        break;
                    }

                    if (status == (int)HiveRequestStatus.Error)
                    {
                        throw new Exception(String.Format(Constants.HiveRequestDiscrepancyCardLoadFailed, hiveRequest.DiscrepancyId, hiveRequest.InvoiceRowKey, hiveRequest.ContractorInvoiceRowKey));
                    }

                    System.Threading.Thread.Sleep(RequestTicking);

                    status = hiveRequestAdapter.GetRequestStatus(requestId);

                    _requestTiming += RequestTicking;
                }

                if (status == (int)HiveRequestStatus.FinishLoading)
                {
                    result.Result = TableAdapterCreator.Discrepancy(_helper, hiveRequest.DiscrepancyId).GetDiscrepancySides();

                    result.Status = (result.Result != null && result.Result.Count > 0) ? ResultStatus.Success : ResultStatus.Undefined;
                }
                else
                {
                    throw new Exception(String.Format(Constants.HiveRequestDiscrepancyCardTimeout, hiveRequest.DiscrepancyId, hiveRequest.InvoiceRowKey, hiveRequest.ContractorInvoiceRowKey));
                }

            }

            return result;

        }

        public OperationResult<DiscrepancySummaryInfo> GetSummaryInfo(long id)
        {
            var opResult = DalHelper.ExecuteOperation(() => TableAdapterCreator.Discrepancy(_helper, id).GetSummaryInfo(), _helper);

            return opResult;
        }

        public OperationResult SaveDiscrepancyUserComment(long discrepancyId, string comment)
        {
            return DalHelper.Execute(() => TableAdapterCreator.Discrepancy(_helper, discrepancyId).SaveDiscrepancyUserComment(comment), _helper);
        }

        public OperationResult SaveComment(long discrepancyId, DetailsBase details)
        {
            return DalHelper.Execute(() => TableAdapterCreator.Discrepancy(_helper, discrepancyId).SaveComment(details), _helper);
        }

        public OperationResult<DiscrepancyDocumentInfo> GetDiscrepancyDocumentInfo(long discrepancyId, int stageId)
        {
            return _helper.Do(() => TableAdapterCreator.DiscrepancyDocument(_helper).GetDiscrepancyDocumentInfo(discrepancyId, stageId));
        }
    }
}