﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CommonComponents.GenericHost;
using CommonComponents.Shared;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.TechnologicalReport;
using Luxoft.NDS2.Server.Services.Reports.ActivityReport;

namespace Luxoft.NDS2.Server.Services.Reports
{
    public class ReportModule : IServerModule, IServerModuleLauncher
    {
        private DateTime _technoReportScheduledTime;
        private DateTime _activityReportScheduledTime;

        private IReadOnlyServiceCollection _serviceCollection;
        private ITechnologicalReportProvider _technologicalReportProvider;
        private IActivityReportProvider _activityReportProvider;
        private ServiceHelpers _helper;

        private Timer _timer;

        public ReportModule(IReadOnlyServiceCollection collection)
        {
            _serviceCollection = collection;
            _technologicalReportProvider = _serviceCollection.Get<ITechnologicalReportProvider>();
            _helper = new ServiceHelpers(_serviceCollection);
            _activityReportProvider = _serviceCollection.Get<IActivityReportProvider>();

        }

        #region IServerModule Members

        public void AddServices(IApplicationHost host)
        {
            Console.WriteLine("adding services");
        }

        public void Load(IApplicationHost host)
        {
            Console.WriteLine("module loaded");
        }

        #endregion

        #region IServerModuleLauncher Members

        public void Start()
        {
            //_technologicalReportProvider.CreateReport(DateTime.Now);
            
            InitTechReportsScheduler();
            InitActivityReportsScheduler();

            _timer = new Timer(TimerCallback);
            _timer.Change(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(15));
            Console.WriteLine("Report module started");
            
        }


        public void Stop()
        {
            /*
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            _timer.Dispose();
            Console.WriteLine("Report module stopped");
             */
        }

        #endregion
        
        private void TimerCallback(object state)
        {
            Console.WriteLine("[{0}] след итерация тех отчета - {1}", DateTime.Now, _technoReportScheduledTime);
            Console.WriteLine("[{0}] след итерация отчета акт - {1}", DateTime.Now, _activityReportScheduledTime);
            Console.WriteLine(">>>");
            BuildTechnoReport();
            BuildActivityReport();
        }

        private void InitTechReportsScheduler()
        {
            var startTimeParts = _helper.GetConfigurationValue("NDS2.Report.Tech.StartTime").Split(':');
            ushort hour = ushort.Parse(startTimeParts[0]);
            ushort minute = ushort.Parse(startTimeParts[1]);
            DateTime now = DateTime.Now;

            _technoReportScheduledTime = new DateTime(now.Year, now.Month, now.Day, hour, minute, 0);

            if (now > _technoReportScheduledTime)
            {
                _technoReportScheduledTime = _technoReportScheduledTime.AddDays(1); //Сегодня уже не успеваем, переносим на завтра
            }

            Console.WriteLine("Проинициализирован планировщик технологического отчета. Старт назначен на {0}", _technoReportScheduledTime);
        }

        private void InitActivityReportsScheduler()
        {
            var startTimeParts = _helper.GetConfigurationValue("NDS2.Report.Activity.StartTime").Split(':');
            ushort hour = ushort.Parse(startTimeParts[0]);
            ushort minute = ushort.Parse(startTimeParts[1]);
            DateTime now = DateTime.Now;

            _activityReportScheduledTime = new DateTime(now.Year, now.Month, now.Day, hour, minute, 0);

            if (now > _activityReportScheduledTime)
            {
                _activityReportScheduledTime = _activityReportScheduledTime.AddDays(1); //Сегодня уже не успеваем, переносим на завтра
            }

            Console.WriteLine("Проинициализирован планировщик отчета активности пользователей. Старт назначен на {0}", _activityReportScheduledTime);
        }

        private void BuildTechnoReport()
        {
            var now = DateTime.Now;
            if (now > _technoReportScheduledTime)
            {
                var reportDate = _technoReportScheduledTime.AddDays(-1).Date;
                _technoReportScheduledTime = _technoReportScheduledTime.AddDays(1);

                Console.WriteLine("Начало формирования технологического отчета за {0}", reportDate);
                _technologicalReportProvider.CreateReport(reportDate);
            }
        }

        private void BuildActivityReport()
        {
            var now = DateTime.Now;
            if (now > _activityReportScheduledTime)
            {
                var reportDate = _activityReportScheduledTime.AddDays(-1).Date;
                _activityReportScheduledTime = _activityReportScheduledTime.AddDays(1);

                Console.WriteLine("Начало формирования отчета активности за {0}", reportDate);
                _activityReportProvider.CreateReport(reportDate);
            }
        }

    }
}
