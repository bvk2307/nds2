﻿using System.IO;
using CommonComponents.Catalog;

namespace Luxoft.NDS2.Server.Services.Reports.Services
{
    public interface ITemplateService
    {
        Stream GetTemplateStream(CatalogAddress address);
    }
}
