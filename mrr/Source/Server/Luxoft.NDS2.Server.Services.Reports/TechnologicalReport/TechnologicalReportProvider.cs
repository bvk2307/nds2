﻿using CommonComponents.Catalog;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Server.Services.Reports.Services;
using Luxoft.NDS2.Server.Services.Helpers;

namespace Luxoft.NDS2.Server.Services.Managers.TechnologicalReport
{
    public class TechnologicalReportBuilder
    {
        private CatalogAddress _techRepAddr = new CatalogAddress(
            CatalogAddressSchemas.LogicalCatalog, 
            "RoleCatalog", 
            "NDS2Subsystem", 
            "FileSystems", 
            "ReportTemplates/technological_report.xlsx");

        private CatalogAddress _statusTempAddr = new CatalogAddress(
            CatalogAddressSchemas.LogicalCatalog, 
            "RoleCatalog", 
            "NDS2Subsystem", 
            "FileSystems", 
            "ReportTemplates/decl_status_report.xlsx");

        private readonly ServiceHelpers _helper;
        private readonly Configuration _config;
        private DateTime ReportCreateDate { get; set; }

        public TechnologicalReportBuilder(ServiceHelpers helper, Configuration config)
        {
            _helper = helper;
            _config = config;
        }

        public void MakeReport(DateTime reportDate)
        {
            ReportCreateDate = reportDate;

            CalcReportData();
            EnsureGP3Parsed();

            BuildReport(reportDate);
        }

        #region Do report

        private void EnsureGP3Parsed()
        {
            var path = Path.Combine(_config.TR_GP3_InputFilesLocation.Value, string.Format(_config.TR_GP3_InputFileNameFormat.Value, ReportCreateDate));
            if (!File.Exists(path))
            {
                _helper.LogWarning(string.Format("NDS2-TR: Файл \"{0}\"с данными по ГП-3 не найден", path));
            }
            else
            {
                try
                {
                    var gp3Data = ExcelTechReport.ParseGP3Excel(path);
                    _helper.Do(() => TableAdapterCreator.TechnologicalReport(_helper).SaveGP3Data(ReportCreateDate, gp3Data));
                }
                catch (Exception ex)
                {
                    _helper.LogError("NDS2-TR: Ошибка при обработке файла с данными по ГП-3", ex: ex);
                }
            }
        }

        private void CalcReportData()
        {
            _helper.Do(() => TableAdapterCreator.TechnologicalReport(_helper).CalcReportData(ReportCreateDate));
        }

        private void GetTemplate(CatalogAddress templateCatalogAddress, string target, Action<Stream> action)
        {
            if (File.Exists(target))
                File.Delete(target);


            var catalogService = _helper.Services.Get<ITemplateService>();

            var tmpl = catalogService.GetTemplateStream(templateCatalogAddress);

            using (var fileStream = File.Create(target))
            {
                tmpl.Seek(0, SeekOrigin.Begin);
                tmpl.CopyTo(fileStream);
                action(fileStream);
                fileStream.Flush();
                fileStream.Close();
            }

            tmpl.Close();
            tmpl.Dispose();
        }

        private void BuildReport(DateTime reportDate)
        {
            var parameters = new Dictionary<string, object>();
            var gp3Data = _helper.Do(() => TableAdapterCreator.TechnologicalReport(_helper).GetGp3Data(ReportCreateDate));
            if (gp3Data.Status != ResultStatus.Success)
                parameters["Получение детализации по ГП-3"] = GetOperationResultLogMessage(gp3Data);

            var quarterData = new Dictionary<DateTime, Dictionary<int, string>>();
            var yesterday = reportDate;
            var begin = yesterday.QuarterBegin();
            while (begin < reportDate.AddDays(1))
            {
                var reportData = _helper.Do(() => TableAdapterCreator.TechnologicalReport(_helper).GetReportData(begin));
                if (reportData.Status == ResultStatus.Success)
                    quarterData[begin] = reportData.Result;
                else
                    parameters["Получение данных отчета за " + begin.ToString("yyyy-MM-dd")] = GetOperationResultLogMessage(reportData);
                begin = begin.AddDays(1);
            }

            if (parameters.Count > 0)
            {
                _helper.LogError("NDS2-TR: Ошибка при сборе данных для отчета", additionalProperties: parameters.ToList());
            }

            #region Технологический отчет

            Directory.CreateDirectory(_config.TR_ReportFilesLocation.Value);
            var path = Path.Combine(_config.TR_ReportFilesLocation.Value, string.Format(_config.TR_ReportFileNameFormat.Value, ReportCreateDate));
            GetTemplate(_techRepAddr, path, stream =>
            {
                try
                {
                    ExcelTechReport.FormTechReport(reportDate, stream, quarterData, gp3Data.Result ?? new Dictionary<long, long[]>());
                }
                catch (Exception ex)
                {
                    _helper.LogError("NDS2-TR: Исключение при формировании технологического отчета", ex: ex);
                }
            });

            #endregion

            #region Статус загрузки НД

            path = Path.Combine(_config.TR_ReportFilesLocation.Value, string.Format("Статус загрузки НД за {0}.xlsx", ReportCreateDate.ToString("yyyy-MM-dd")));
            GetTemplate(_statusTempAddr, path, stream =>
            {
                try
                {
                    ExcelTechReport.FormStatusReport(stream, quarterData);
                }
                catch (Exception ex)
                {
                    _helper.LogError("NDS2-TR: Исключение при формировании отчета по статусу загрузки НД", ex: ex);
                }
            });

            #endregion

            #region Логи из БД

            var sysMsg = _helper.Do(() => TableAdapterCreator.TechnologicalReport(_helper).GetSystemLog());
            if (sysMsg.Status == ResultStatus.Success)
            {
                foreach (var entry in sysMsg.Result)
                {
                    switch (entry.Kind)
                    {
                        case 1:
                            _helper.LogNotification(string.Format("SYSTEM_LOG: {0} {1}", entry.SITE, entry.Message));
                            continue;
                        case 2:
                            _helper.LogWarning(string.Format("SYSTEM_LOG: {0} {1}", entry.SITE, entry.Message));
                            continue;
                        case 3:
                            _helper.LogError(string.Format("SYSTEM_LOG: {0} {1}", entry.SITE, entry.Message));
                            continue;
                    }
                }
            }
            else
            {
                _helper.LogError("NDS2-TR: Не удалось загрузить данные из SYSTEM_LOG: " + sysMsg.Message);
            }

            #endregion

        }

        private static string GetOperationResultLogMessage(IOperationResult opResult)
        {
            var result = new StringBuilder();
            result.AppendFormat("Status: {0}", opResult.Status);
            if (opResult.Status != ResultStatus.Success)
            {
                result.AppendFormat("; Message: {0}", opResult.Message);
            }
            return result.ToString();
        }

        #endregion
    }

    public class TechnologicalReportProvider : ITechnologicalReportProvider
    {
        private readonly ServiceHelpers _helper;

        public TechnologicalReportProvider(IReadOnlyServiceCollection services)
        {
            _helper = new ServiceHelpers(services);
        }

        public void Dispose()
        {
        }

        public void CreateReport(DateTime reportDate)
        {
            var config = ReadConfig();
            
            if (config == null)
            {
                return;
            }

            try
            {
                new TechnologicalReportBuilder(_helper, config).MakeReport(reportDate);
            }
            catch (Exception e)
            {
                _helper.LogError("NDS2-TR: " + e.Message);
            }
        }

        private Configuration ReadConfig()
        {
            try
            {
                return TableAdapterCreator.Configuration(_helper).Read();
            }
            catch (Exception ex)
            {
                _helper.LogError(ex.ToString());
                return null;
            }
        }
    }
}