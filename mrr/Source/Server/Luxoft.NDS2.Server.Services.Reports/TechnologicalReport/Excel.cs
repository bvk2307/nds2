﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Luxoft.NDS2.Server.Services.Managers.TechnologicalReport
{
    public static class Excel
    {
        private class ExcelSheet
        {
            public Worksheet Sheet { get; set; }

            public SheetData Data { get; set; }

            public string Name { get; set; }

            public static List<ExcelSheet> GetSheets(SpreadsheetDocument document)
            {
                var result =
                    from sheet in document.WorkbookPart.Workbook.Sheets.OfType<Sheet>()
                    let name = sheet.Name
                    let worksheet = ((WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet
                    from data in worksheet.Elements<SheetData>()
                    select new ExcelSheet { Sheet = worksheet, Data = data, Name = name };

                return result.ToList();
            }
        }

        public static Dictionary<long, long[]> ParseGP3Excel(string path)
        {
            var path2007 = ConvertToExcel2007(path);

            Dictionary<long, long[]> result;
            using (var doc_gp3 = SpreadsheetDocument.Open(path2007, false))
            {
                var sheets = ExcelSheet.GetSheets(doc_gp3);

                if (sheets.Count != 1)
                {
                    throw new Exception("Ошибка в формате Excel файла");
                }

                var sheet = sheets[0];
                Func<Cell, long> getCellValueInt64 = cell =>
                {
                    long cellValue;
                    return long.TryParse(cell.CellValue.InnerText, out cellValue) ? cellValue : 0L;
                };

                var rows =
                    from row in sheet.Data.Elements<Row>().Skip(1)
                    let key = getCellValueInt64(row.Elements<Cell>().First())
                    let value = row.Elements<Cell>().Skip(1).Select(cell => getCellValueInt64(cell)).ToArray()
                    select new { key, value };
                result = rows.ToDictionary(cell => cell.key, cell => cell.value);
            }

            if (!path2007.Equals(path, StringComparison.InvariantCultureIgnoreCase))
            {
                File.Delete(path2007);
            }
            return result;
        }

        private static string ConvertToExcel2007(string path2003)
        {
            var path2007 = new FileInfo(path2003).Directory.FullName;
            path2007 = Path.Combine(path2007, Path.GetFileNameWithoutExtension(path2003) + ".xlsx");
            if (path2007.Equals(path2003, StringComparison.InvariantCultureIgnoreCase))
            {
                return path2007;
            }

            var book = Infragistics.Excel.Workbook.Load(path2003);
            if (book.CurrentFormat == Infragistics.Excel.WorkbookFormat.Excel97To2003)
            {
                book.SetCurrentFormat(Infragistics.Excel.WorkbookFormat.Excel2007);
            }
            book.Save(path2007);
            return path2007;
        }

        public static void FormReport(string path, Dictionary<int, string> mainData, Dictionary<long, long[]> gp3Details)
        {
            using (var doc = SpreadsheetDocument.Open(path, true))
            {
                var sheets = ExcelSheet.GetSheets(doc);

                //ГП-3
                var sheet = sheets.First(s => s.Name == "ГП-3");
                for (int i = 1; i <= 12; i++)
                {
                    sheet.Sheet.GetCell("C", (uint)(i + 1)).SetValue(mainData.GetValueSafe(i));
                }
                foreach (var pair in gp3Details)
                {
                    for (int i = 0; i < pair.Value.Length; i++)
                    {
                        sheet.Sheet.GetCell(ColumnAddressByNumber(i + 4), (uint)(pair.Key + 1)).SetValue(pair.Value[i].ToString());
                    }
                }
                sheet.Sheet.Save();

                //СОВ
                sheet = sheets.First(s => s.Name == "СОВ");
                for (int i = 1; i <= 9; i++)
                {
                    sheet.Sheet.GetCell("D", (uint)(i + 1)).SetValue(mainData.GetValueSafe(i + 1000));
                }
                sheet.Sheet.Save();

                //МС
                sheet = sheets.First(s => s.Name == "МС");
                sheet.Sheet.GetCell("D", 2).SetValue(mainData.GetValueSafe(101));
                sheet.Sheet.GetCell("D", 3).SetValue(mainData.GetValueSafe(102));
                sheet.Sheet.GetCell("D", 4).SetValue(mainData.GetValueSafe(103));
                sheet.Sheet.GetCell("D", 5).SetValue(mainData.GetValueSafe(104));
                sheet.Sheet.GetCell("D", 6).SetValue(mainData.GetValueSafe(105));
                sheet.Sheet.GetCell("D", 7).SetValue(mainData.GetValueSafe(106));
                sheet.Sheet.GetCell("D", 8).SetValue(mainData.GetValueSafe(107));
                sheet.Sheet.GetCell("D", 9).SetValue(mainData.GetValueSafe(108));
                sheet.Sheet.GetCell("D", 10).SetValue(mainData.GetValueSafe(109));
                sheet.Sheet.GetCell("D", 11).SetValue(mainData.GetValueSafe(110));
                sheet.Sheet.GetCell("D", 12).SetValue(mainData.GetValueSafe(111));
                sheet.Sheet.GetCell("D", 13).SetValue(mainData.GetValueSafe(112));
                sheet.Sheet.GetCell("D", 14).SetValue(mainData.GetValueSafe(113));
                sheet.Sheet.GetCell("D", 15).SetValue(mainData.GetValueSafe(114));
                sheet.Sheet.GetCell("D", 16).SetValue(mainData.GetValueSafe(115));
                sheet.Sheet.GetCell("D", 17).SetValue(mainData.GetValueSafe(116));
                sheet.Sheet.GetCell("D", 18).SetValue(mainData.GetValueSafe(117));
                sheet.Sheet.GetCell("D", 19).SetValue(mainData.GetValueSafe(118));
                sheet.Sheet.GetCell("D", 20).SetValue(mainData.GetValueSafe(119));
                sheet.Sheet.GetCell("D", 21).SetValue(mainData.GetValueSafe(120));
                sheet.Sheet.GetCell("D", 22).SetValue(mainData.GetValueSafe(121));
                sheet.Sheet.GetCell("D", 23).SetValue(mainData.GetValueSafe(122));
                sheet.Sheet.GetCell("D", 24).SetValue(mainData.GetValueSafe(123));
                sheet.Sheet.GetCell("D", 25).SetValue(mainData.GetValueSafe(124));
                sheet.Sheet.GetCell("D", 26).SetValue(mainData.GetValueSafe(125));
                sheet.Sheet.GetCell("D", 27).SetValue(mainData.GetValueSafe(126));
                sheet.Sheet.GetCell("D", 28).SetValue(mainData.GetValueSafe(128));
                sheet.Sheet.GetCell("D", 29).SetValue(mainData.GetValueSafe(129));
                sheet.Sheet.GetCell("D", 30).SetValue(mainData.GetValueSafe(131));
                sheet.Sheet.GetCell("D", 31).SetValue(mainData.GetValueSafe(132));
                sheet.Sheet.GetCell("D", 32).SetValue(mainData.GetValueSafe(133));
                sheet.Sheet.GetCell("D", 33).SetValue(mainData.GetValueSafe(134));
                sheet.Sheet.GetCell("D", 34).SetValue(mainData.GetValueSafe(135));
                sheet.Sheet.GetCell("D", 35).SetValue(mainData.GetValueSafe(136));
                sheet.Sheet.GetCell("D", 36).SetValue(mainData.GetValueSafe(137));
                sheet.Sheet.GetCell("D", 37).SetValue(mainData.GetValueSafe(138));
                sheet.Sheet.Save();

                //МРР
                sheet = sheets.First(s => s.Name == "МРР");
                for (int i = 1; i <= 11; i++)
                {
                    sheet.Sheet.GetCell("D", (uint)(i + 1)).SetValue(mainData.GetValueSafe(i + 1500));
                }
                sheet.Sheet.Save();

                //Сводная информация
                sheet = sheets.First(s => s.Name == "Сводная информация");
                sheet.Sheet.GetCell("C", 2).SetValue(mainData.GetValueSafe(1));
                sheet.Sheet.GetCell("C", 3).SetValue(mainData.GetValueSafe(5));
                sheet.Sheet.GetCell("C", 4).SetValue(mainData.GetValueSafe(7));
                sheet.Sheet.GetCell("C", 5).SetValue(mainData.GetValueSafe(9));
                sheet.Sheet.GetCell("C", 6).SetValue(mainData.GetValueSafe(11));
                sheet.Sheet.GetCell("C", 7).SetValue(mainData.GetValueSafe(101));
                sheet.Sheet.GetCell("C", 8).SetValue(mainData.GetValueSafe(103));
                sheet.Sheet.GetCell("C", 9).SetValue(mainData.GetValueSafe(107));
                sheet.Sheet.GetCell("C", 10).SetValue(AvgRecordsInDeclaration(mainData));
                sheet.Sheet.GetCell("C", 11).SetValue(mainData.GetValueSafe(108));
                sheet.Sheet.GetCell("C", 12).SetValue(mainData.GetValueSafe(105));
                sheet.Sheet.GetCell("C", 13).SetValue(mainData.GetValueSafe(110));
                sheet.Sheet.GetCell("C", 14).SetValue(mainData.GetValueSafe(111));
                sheet.Sheet.GetCell("C", 15).SetValue(mainData.GetValueSafe(112));
                sheet.Sheet.GetCell("C", 16).SetValue(mainData.GetValueSafe(113));
                sheet.Sheet.GetCell("C", 17).SetValue(mainData.GetValueSafe(114));
                sheet.Sheet.GetCell("C", 18).SetValue(TotalRecordsMatched(mainData));
                sheet.Sheet.GetCell("C", 19).SetValue(TotalMatchingTime(mainData));
                sheet.Sheet.GetCell("C", 20).SetValue(mainData.GetValueSafe(1004));
                sheet.Sheet.Save();
            }
        }

        private static string AvgRecordsInDeclaration(Dictionary<int, string> mainData)
        {
            long rec1, rec2, dec1, dec2;
            if (!long.TryParse(mainData.GetValueSafe(102), out rec1)
                || !long.TryParse(mainData.GetValueSafe(104), out rec2)
                || !long.TryParse(mainData.GetValueSafe(101), out dec1)
                || !long.TryParse(mainData.GetValueSafe(103), out dec2)
                || (dec1 + dec2) == 0)
            {
                return string.Empty;
            }
            else
            {
                return ((rec1 + rec2) / (dec1 + dec2)).ToString();
            }
        }

        private static string TotalRecordsMatched(Dictionary<int, string> mainData)
        {
            long val;
            long sum = 0;

            if (!long.TryParse(mainData.GetValueSafe(137), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(132), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(133), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(134), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(125), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(126), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(128), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(129), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!long.TryParse(mainData.GetValueSafe(118), out val))
            {
                return string.Empty;
            }
            sum += val;
            return sum.ToString();
        }

        private static string TotalMatchingTime(Dictionary<int, string> mainData)
        {
            TimeSpan val;
            TimeSpan sum = TimeSpan.Zero;
            if (!TimeSpan.TryParse(mainData.GetValueSafe(138), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!TimeSpan.TryParse(mainData.GetValueSafe(135), out val))
            {
                return string.Empty;
            }
            sum += val;
            if (!TimeSpan.TryParse(mainData.GetValueSafe(119), out val))
            {
                return string.Empty;
            }
            sum += val;
            return sum.ToString();
        }

        private static void SetValue(this Cell cell, string value)
        {
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
            cell.CellValue = new CellValue(value);
        }

        private static Cell GetCell(this Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
        }

        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }

        private static string GetValueSafe(this Dictionary<int, string> dictionary, int key, string defaultValue = null)
        {
            return (dictionary.ContainsKey(key) ? dictionary[key] : defaultValue) ?? string.Empty;
        }

        private static string ColumnAddressByNumber(int number)
        {
            var columnName = new StringBuilder();
            int div = number;
            int mod;
            while (div > 0)
            {
                mod = (div - 1) % 26;
                columnName.Insert(0, Convert.ToChar(65 + mod));
                div = (int)((div - mod) / 26);
            }
            return columnName.ToString();
        }
    }
}