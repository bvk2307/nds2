﻿using System;

namespace Luxoft.NDS2.Server.Services.Managers.TechnologicalReport
{
    public interface ITechnologicalReportProvider : IDisposable
    {
        void CreateReport(DateTime reportDate);
    }
}