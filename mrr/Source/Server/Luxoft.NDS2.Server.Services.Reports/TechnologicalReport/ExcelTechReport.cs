﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Server.Services.Managers.Common;

namespace Luxoft.NDS2.Server.Services.Managers.TechnologicalReport
{
    public static class ExcelTechReport
    {
        public static Dictionary<long, long[]> ParseGP3Excel(string path)
        {
            var path2007 = Excel.ConvertToExcel2007(path);

            Dictionary<long, long[]> result;
            using (var docGP3 = SpreadsheetDocument.Open(path2007, false))
            {
                var sheets = ExcelSheet.GetSheets(docGP3);

                if (sheets.Count != 1)
                {
                    throw new Exception("Ошибка в формате Excel файла");
                }

                var sheet = sheets[0];
                Func<Cell, long> getCellValueInt64 = cell =>
                {
                    long cellValue;
                    return long.TryParse(cell.CellValue.InnerText, out cellValue) ? cellValue : 0L;
                };

                result = new Dictionary<long, long[]>();
                foreach (var row in sheet.Data.Elements<Row>().Skip(1))
                {
                    var key = getCellValueInt64(row.Elements<Cell>().First());
                    result[key] = row.Elements<Cell>().Skip(2).Select(getCellValueInt64).ToArray();
                }
            }

            if (!path2007.Equals(path, StringComparison.InvariantCultureIgnoreCase))
            {
                File.Delete(path2007);
            }
            return result;
        }

        public static void Sum(this Dictionary<string, long> agr, string key, long value)
        {
            long old;
            agr[key] = agr.TryGetValue(key, out old) ? old + value : value;
        }

        public static void FormStatusReport(Stream stream, Dictionary<DateTime, Dictionary<int, string>> data)
        {
            using (var doc = SpreadsheetDocument.Open(stream, true))
            {
                var sheet = ExcelSheet.GetSheets(doc).First();

                var yesterday = DateTime.Today.AddDays(-1);
                var begin = yesterday.QuarterBegin();
                var row = 5u;

                var agr = new Dictionary<string, long>();

                while (begin < DateTime.Today)
                {
                    var date = begin.ToString("d MMM");
                    ++row;
                    sheet.Sheet.GetCell("A", row).SetValue(date);

                    if (data.ContainsKey(begin))
                    {
                        var rowData = data[begin];
                        #region ГП-3

                        sheet.Sheet.GetCell("B", row).SetValue(rowData.GetNumberSafe(1));
                        agr.Sum("B", rowData.GetNumberSafe(1));
                        /*sheet.Sheet.GetCell("C", row).SetValue(rowData.GetNumberSafe(14));
                        agr.Sum("C", rowData.GetNumberSafe(14));*/
                        sheet.Sheet.GetCell("D", row).SetValue(rowData.GetNumberSafe(1)/* + rowData.GetNumberSafe(14)*/);
                        agr.Sum("D", rowData.GetNumberSafe(1)/* + rowData.GetNumberSafe(14)*/);
                        sheet.Sheet.GetCell("E", row).SetValue(rowData.GetNumberSafe(7) + rowData.GetNumberSafe(9));
                        agr.Sum("E", rowData.GetNumberSafe(7) + rowData.GetNumberSafe(9));

                        #endregion

                        #region МС

                        sheet.Sheet.GetCell("F", row).SetValue(rowData.GetNumberSafe(161));
                        agr.Sum("F", rowData.GetNumberSafe(161));
                        sheet.Sheet.GetCell("G", row).SetValue(rowData.GetNumberSafe(162));
                        agr.Sum("G", rowData.GetNumberSafe(162));

                        sheet.Sheet.GetCell("H", row).SetValue(rowData.GetNumberSafe(101) + rowData.GetNumberSafe(103));
                        agr.Sum("H", rowData.GetNumberSafe(101) + rowData.GetNumberSafe(103));
                        sheet.Sheet.GetCell("I", row).SetValue(rowData.GetNumberSafe(108));
                        agr.Sum("I", rowData.GetNumberSafe(108));

                        var sum = rowData.GetNumberSafe(101) + rowData.GetNumberSafe(103) + rowData.GetNumberSafe(108);
                        sheet.Sheet.GetCell("J", row).SetValue(sum);
                        agr.Sum("J", sum);

                        #endregion

                        #region СС

                        sheet.Sheet.GetCell("K", row).SetValue(rowData.GetNumberSafe(2001));
                        agr.Sum("K", rowData.GetNumberSafe(2001));
                        sheet.Sheet.GetCell("L", row).SetValue(rowData.GetNumberSafe(2003));
                        agr.Sum("L", rowData.GetNumberSafe(2003));
                        sheet.Sheet.GetCell("M", row).SetValue(rowData.GetNumberSafe(2001) + rowData.GetNumberSafe(2003));
                        agr.Sum("M", rowData.GetNumberSafe(2001) + rowData.GetNumberSafe(2003));

                        sheet.Sheet.GetCell("N", row).SetValue(rowData.GetNumberSafe(2011));
                        agr.Sum("N", rowData.GetNumberSafe(2011));
                        sheet.Sheet.GetCell("O", row).SetValue(rowData.GetNumberSafe(2012));
                        agr.Sum("O", rowData.GetNumberSafe(2012));
                        sheet.Sheet.GetCell("P", row).SetValue(rowData.GetNumberSafe(2011) + rowData.GetNumberSafe(2012));
                        agr.Sum("P", rowData.GetNumberSafe(2011) + rowData.GetNumberSafe(2012));

                        sheet.Sheet.GetCell("Q", row).SetValue(rowData.GetNumberSafe(2013));
                        agr.Sum("Q", rowData.GetNumberSafe(2013));
                        sheet.Sheet.GetCell("R", row).SetValue(rowData.GetNumberSafe(2014));
                        agr.Sum("R", rowData.GetNumberSafe(2014));
                        sheet.Sheet.GetCell("S", row).SetValue(rowData.GetNumberSafe(2013) + rowData.GetNumberSafe(2014));
                        agr.Sum("S", rowData.GetNumberSafe(2013) + rowData.GetNumberSafe(2014));

                        #endregion

                        #region МРР

                        sheet.Sheet.GetCell("T", row).SetValue(rowData.GetNumberSafe(1601));
                        agr.Sum("T", rowData.GetNumberSafe(1601));
                        sheet.Sheet.GetCell("U", row).SetValue(rowData.GetNumberSafe(1602));
                        agr["U"] = rowData.GetNumberSafe(1602);
                        sheet.Sheet.GetCell("V", row).SetValue(rowData.GetNumberSafe(1603));
                        agr.Sum("V", rowData.GetNumberSafe(1603));
                        sheet.Sheet.GetCell("W", row).SetValue(rowData.GetNumberSafe(1604));
                        agr["W"] = rowData.GetNumberSafe(1604);

                        sheet.Sheet.GetCell("X", row).SetValue(rowData.GetNumberSafe(1602) + rowData.GetNumberSafe(1604));
                        agr["X"] = rowData.GetNumberSafe(1602) + rowData.GetNumberSafe(1604);

                        sheet.Sheet.GetCell("Y", row).SetValue(rowData.GetNumberSafe(1611));
                        agr.Sum("Y", rowData.GetNumberSafe(1611));
                        sheet.Sheet.GetCell("Z", row).SetValue(rowData.GetNumberSafe(1612));
                        agr.Sum("Z", rowData.GetNumberSafe(1612));
                        sheet.Sheet.GetCell("AA", row).SetValue(rowData.GetNumberSafe(1611) + rowData.GetNumberSafe(1612));
                        agr.Sum("AA", rowData.GetNumberSafe(1611) + rowData.GetNumberSafe(1612));

                        #endregion
                    }
                    begin = begin.AddDays(1);
                }

                foreach (var pair in agr)
                {
                    sheet.Sheet.GetCell(pair.Key, 5).SetValue(pair.Value);
                }

                sheet.Sheet.Save();
            }

        }

        public static void FormTechReport(DateTime reportDate, Stream stream, Dictionary<DateTime, Dictionary<int, string>> data, Dictionary<long, long[]> gp3Details)
        {
            var yesterday = reportDate;
            Dictionary<int, string> currentData;
            if (!data.TryGetValue(yesterday, out currentData))
            {
                throw new Exception("Нет данных на " + yesterday.ToString("dd MM yyyy"));
            }
            using (var doc = SpreadsheetDocument.Open(stream, true))
            {
                var sheets = ExcelSheet.GetSheets(doc);

                //ГП-3
                var sheet = sheets.First(s => s.Name == "ГП-3");
                for (int i = 1; i <= 11; i++)
                {
                    sheet.Sheet.GetCell("C", (uint)(i + 1)).SetValue(currentData.GetNumberSafe(i));
                }
                sheet.Sheet.Save();

                #region СОВ

                sheet = sheets.First(s => s.Name == "СОВ");
                sheet.Sheet.GetCell("D", 2).SetValue(currentData.GetNumberSafe(1001));
                sheet.Sheet.GetCell("D", 3).SetValue(currentData.GetValueSafe(1002));
                sheet.Sheet.GetCell("D", 4).SetValue(currentData.GetNumberSafe(1003));
                sheet.Sheet.GetCell("D", 5).SetValue(currentData.GetNumberSafe(1004));
                sheet.Sheet.GetCell("D", 6).SetValue(currentData.GetValueSafe(1005));
                sheet.Sheet.GetCell("D", 7).SetValue(currentData.GetNumberSafe(1006));
                sheet.Sheet.GetCell("D", 8).SetValue(currentData.GetValueSafe(1007));
                sheet.Sheet.GetCell("D", 9).SetValue(currentData.GetNumberSafe(1008));
                sheet.Sheet.GetCell("D", 10).SetValue(currentData.GetValueSafe(1009));
                sheet.Sheet.Save();

                #endregion

                #region MC

                sheet = sheets.First(s => s.Name == "МС");

                var r = 1u;
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(101));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(102));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(103));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(104));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(105));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(106));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(107));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(108));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(109));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(110));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(111));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(112));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(113));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(114));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(115));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(116));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(117));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(118));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(119));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(120));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(121));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(122));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(123));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(124));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(125));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(126));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(128));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(129));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(201));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(202));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(203));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(204));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(205));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(211));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(212));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(213));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(214));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(215));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(221));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(222));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(223));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(224));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(225));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(231));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(232));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(233));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(234));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(235));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(241));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(242));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(243));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(244));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(245));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(251));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(252));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(253));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(254));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(255));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(261));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(262));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(263));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(264));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(265));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(271));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(272));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(273));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(274));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(275));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(136));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(137));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetValueSafe(138));

                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(161));
                sheet.Sheet.GetCell("D", ++r).SetValue(currentData.GetNumberSafe(162));

                sheet.Sheet.Save();

                #endregion
                
                #region МРР

                sheet = sheets.First(s => s.Name == "МРР");
                sheet.Sheet.GetCell("D", 2).SetValue(currentData.GetNumberSafe(1501));
                sheet.Sheet.GetCell("D", 3).SetValue(currentData.GetValueSafe(1502));
                sheet.Sheet.GetCell("D", 4).SetValue(currentData.GetNumberSafe(1503));
                sheet.Sheet.GetCell("D", 5).SetValue(currentData.GetNumberSafe(1504));
                sheet.Sheet.GetCell("D", 6).SetValue(currentData.GetValueSafe(1505));
                sheet.Sheet.GetCell("D", 7).SetValue(currentData.GetNumberSafe(1506));
                sheet.Sheet.GetCell("D", 8).SetValue(currentData.GetNumberSafe(1507));
                sheet.Sheet.GetCell("D", 9).SetValue(currentData.GetValueSafe(1508));
                sheet.Sheet.GetCell("D", 10).SetValue(currentData.GetNumberSafe(1509));
                sheet.Sheet.GetCell("D", 11).SetValue(currentData.GetNumberSafe(1510));
                sheet.Sheet.GetCell("D", 12).SetValue(currentData.GetValueSafe(1511));

                sheet.Sheet.GetCell("D", 13).SetValue(currentData.GetNumberSafe(1601));
                sheet.Sheet.GetCell("D", 14).SetValue(currentData.GetNumberSafe(1602));
                sheet.Sheet.GetCell("D", 15).SetValue(currentData.GetNumberSafe(1603));
                sheet.Sheet.GetCell("D", 16).SetValue(currentData.GetNumberSafe(1604));
                sheet.Sheet.GetCell("D", 17).SetValue(currentData.GetNumberSafe(1611));
                sheet.Sheet.GetCell("D", 18).SetValue(currentData.GetNumberSafe(1612));
                sheet.Sheet.Save();

                #endregion

                #region Сводная информация

                sheet = sheets.First(s => s.Name == "Сводная информация");
                sheet.Sheet.GetCell("C", 1).SetValue("Значение за " + yesterday.ToString("dd.MM.yyyy"));
                sheet.Sheet.GetCell("D", 1).SetValue(string.Format("Значение за период с {0} по {1}",
                    yesterday.QuarterBegin().ToString("dd.MM.yyyy"), yesterday.ToString("dd.MM.yyyy")));

                sheet.Sheet.GetCell("C", 2).SetValue(currentData.GetNumberSafe(1));
                sheet.Sheet.GetCell("D", 2).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(1)));

                sheet.Sheet.GetCell("C", 3).SetValue(currentData.GetNumberSafe(3));
                sheet.Sheet.GetCell("D", 3).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(3)));

                sheet.Sheet.GetCell("C", 4).SetValue(currentData.GetNumberSafe(5));
                sheet.Sheet.GetCell("D", 4).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(5)));

                sheet.Sheet.GetCell("C", 5).SetValue(currentData.GetNumberSafe(7));
                sheet.Sheet.GetCell("D", 5).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(7)));

                sheet.Sheet.GetCell("C", 6).SetValue(currentData.GetNumberSafe(9));
                sheet.Sheet.GetCell("D", 6).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(9)));

                sheet.Sheet.GetCell("C", 7).SetValue(currentData.GetNumberSafe(11));
                sheet.Sheet.GetCell("D", 7).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(11)));

                sheet.Sheet.GetCell("C", 8).SetValue(currentData.GetNumberSafe(101));
                sheet.Sheet.GetCell("D", 8).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(101)));

                sheet.Sheet.GetCell("C", 9).SetValue(currentData.GetNumberSafe(103));
                sheet.Sheet.GetCell("D", 9).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(103)));

                sheet.Sheet.GetCell("C", 10).SetValue(currentData.GetNumberSafe(107));
                sheet.Sheet.GetCell("D", 10).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(107)));


                sheet.Sheet.GetCell("C", 11).SetValue(currentData.GetNumberSafe(108));
                sheet.Sheet.GetCell("D", 11).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(108)));

                sheet.Sheet.GetCell("C", 12).SetValue(currentData.GetNumberSafe(105));
                sheet.Sheet.GetCell("D", 12).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(105)));

                // Записи о СФ всего и по разделам
                sheet.Sheet.GetCell("C", 13).SetValue(currentData.GetNumberSafe(102) + currentData.GetNumberSafe(104) + currentData.GetNumberSafe(109));
                sheet.Sheet.GetCell("D", 13).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(102) + dayValues.GetNumberSafe(104) + dayValues.GetNumberSafe(109)));
                sheet.Sheet.GetCell("C", 14).SetValue(currentData.GetNumberSafe(4208));
                sheet.Sheet.GetCell("C", 15).SetValue(currentData.GetNumberSafe(4209));
                sheet.Sheet.GetCell("C", 16).SetValue(currentData.GetNumberSafe(4210));
                sheet.Sheet.GetCell("C", 17).SetValue(currentData.GetNumberSafe(4211));
                sheet.Sheet.GetCell("C", 18).SetValue(currentData.GetNumberSafe(4212));

                sheet.Sheet.GetCell("C", 19).SetValue(currentData.GetNumberSafe(110));
                sheet.Sheet.GetCell("D", 19).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(110)));

                sheet.Sheet.GetCell("C", 20).SetValue(currentData.GetNumberSafe(111));
                sheet.Sheet.GetCell("D", 20).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(111)));

                sheet.Sheet.GetCell("C", 21).SetValue(currentData.GetNumberSafe(112));
                sheet.Sheet.GetCell("D", 21).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(112)));

                sheet.Sheet.GetCell("C", 22).SetValue(currentData.GetTimeSpanSafe(113).ToString());
                sheet.Sheet.GetCell("D", 22).SetValue(data.Values.Aggregate(TimeSpan.FromSeconds(0), (sum, dayData) => sum + dayData.GetTimeSpanSafe(113)).ToString());

                sheet.Sheet.GetCell("C", 23).SetValue(currentData.GetNumberSafe(114));
                sheet.Sheet.GetCell("D", 23).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(114)));

                sheet.Sheet.GetCell("C", 24).SetValue(TotalRecordsMatched(currentData));
                sheet.Sheet.GetCell("D", 24).SetValue(data.Values.Sum(dayData => TotalRecordsMatched(dayData)));

                sheet.Sheet.GetCell("C", 25).SetValue(TotalMatchingTime(currentData));
                sheet.Sheet.GetCell("D", 25).SetValue(TotalMatchingTimeQuarter(data));

                sheet.Sheet.GetCell("C", 26).SetValue(currentData.GetNumberSafe(1004));
                sheet.Sheet.GetCell("D", 26).SetValue(data.Values.Sum(dayValues => dayValues.GetNumberSafe(1004)));

                sheet.Sheet.Save();

                #endregion

                #region Синхронизация

                sheet = sheets.First(s => s.Name == "Синхронизация");
                r = 1u;
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2001));
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2003));
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2011));
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2012));
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2013));
                sheet.Sheet.GetCell("C", ++r).SetValue(currentData.GetNumberSafe(2014));
                sheet.Sheet.Save();

                #endregion

            }
        }

        private static long TotalRecordsMatched(Dictionary<int, string> mainData)
        {
            long val;
            long sum = 0;

            if (long.TryParse(mainData.GetValueSafe(137), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(132), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(133), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(134), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(125), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(126), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(128), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(129), out val))
                sum += val;
            if (long.TryParse(mainData.GetValueSafe(118), out val))
                sum += val;
            return sum;
        }

        private static string TotalMatchingTime(Dictionary<int, string> mainData)
        {
            TimeSpan val;
            TimeSpan sum = TimeSpan.Zero;
            if (TimeSpan.TryParse(mainData.GetValueSafe(138), out val))
                sum += val;
            if (TimeSpan.TryParse(mainData.GetValueSafe(135), out val))
                sum += val;
            if (!TimeSpan.TryParse(mainData.GetValueSafe(119), out val))
                sum += val;
            return sum.ToString();
        }

        private static string TotalMatchingTimeQuarter(Dictionary<DateTime, Dictionary<int, string>> data)
        {
            var sum = TimeSpan.Zero;
            foreach (var dayData in data.Values)
            {
                TimeSpan val;
                if (TimeSpan.TryParse(dayData.GetValueSafe(138), out val))
                    sum += val;
                if (TimeSpan.TryParse(dayData.GetValueSafe(135), out val))
                    sum += val;
                if (TimeSpan.TryParse(dayData.GetValueSafe(119), out val))
                    sum += val;
            }
            return sum.ToString();
        }
    }

    static class QuarterHelper
    {
        public static DateTime QuarterBegin(this DateTime date)
        {
            switch (date.Month)
            {
                case 1:
                case 2:
                case 3:
                    return new DateTime(date.Year, 1, 1);
                case 4:
                case 5:
                case 6:
                    return new DateTime(date.Year, 4, 1);
                case 7:
                case 8:
                case 9:
                    return new DateTime(date.Year, 7, 1);
                default:
                    return new DateTime(date.Year, 10, 1);
            }
        }
    }
}