﻿using System;

namespace Luxoft.NDS2.Server.Services.Reports.ActivityReport
{
    public interface IActivityReportProvider : IDisposable
    {
        void CreateReport(DateTime reportDate);
    }
}