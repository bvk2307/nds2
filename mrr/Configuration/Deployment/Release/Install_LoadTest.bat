sc \\m9965-opz315 stop LoadingService
sc \\m9965-opz316 stop LoadingService
sc \\m9965-opz317 stop LoadingService
sc \\m9965-opz318 stop LoadingService
sc \\m9965-opz319 stop LoadingService
sc \\m9965-opz320 stop LoadingService
sc \\m9965-opz321 stop LoadingService
sc \\m9965-opz322 stop LoadingService
sc \\m9965-opz323 stop LoadingService
sc \\m9965-opz324 stop LoadingService
sc \\m9965-opz325 stop LoadingService
sc \\m9965-opz326 stop LoadingService

sc \\m9965-opz315 delete LoadingService
sc \\m9965-opz316 delete LoadingService
sc \\m9965-opz317 delete LoadingService
sc \\m9965-opz318 delete LoadingService
sc \\m9965-opz319 delete LoadingService
sc \\m9965-opz320 delete LoadingService
sc \\m9965-opz321 delete LoadingService
sc \\m9965-opz322 delete LoadingService
sc \\m9965-opz323 delete LoadingService
sc \\m9965-opz324 delete LoadingService
sc \\m9965-opz325 delete LoadingService
sc \\m9965-opz326 delete LoadingService

rem ping -n 5 127.0.0.1 > nul

del /Q /F /S \\m9965-opz315\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz316\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz317\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz318\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz319\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz320\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz321\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz322\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz323\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz324\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz325\c$\NDS2LoadTest\
del /Q /F /S \\m9965-opz326\c$\NDS2LoadTest\

mkdir \\m9965-opz315\c$\NDS2LoadTest\
mkdir \\m9965-opz316\c$\NDS2LoadTest\
mkdir \\m9965-opz317\c$\NDS2LoadTest\
mkdir \\m9965-opz318\c$\NDS2LoadTest\
mkdir \\m9965-opz319\c$\NDS2LoadTest\
mkdir \\m9965-opz320\c$\NDS2LoadTest\
mkdir \\m9965-opz321\c$\NDS2LoadTest\
mkdir \\m9965-opz322\c$\NDS2LoadTest\
mkdir \\m9965-opz323\c$\NDS2LoadTest\
mkdir \\m9965-opz324\c$\NDS2LoadTest\
mkdir \\m9965-opz325\c$\NDS2LoadTest\
mkdir \\m9965-opz326\c$\NDS2LoadTest\

xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz315\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz316\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz317\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz318\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz319\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz320\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz321\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz322\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz323\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz324\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz325\c$\NDS2LoadTest\
xcopy /R /E ..\LoadTest\Service\*.* \\m9965-opz326\c$\NDS2LoadTest\


PsExec.exe \\m9965-opz315 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz316 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz317 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz318 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz319 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz320 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz321 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz322 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz323 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz324 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz325 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i
PsExec.exe \\m9965-opz326 c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i

pause
