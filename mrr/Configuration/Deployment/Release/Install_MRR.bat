sc \\M9965-opz310 stop "NDS2"
sc \\M9965-opz327 stop "NDS2"

sc \\M9965-opz310 delete "NDS2"
sc \\M9965-opz327 delete "NDS2"

del /Q /F /S \\M9965-opz309\c$\NDS2\
del /Q /F /S \\M9965-opz327\c$\NDS2\

mkdir \\M9965-opz309\c$\NDS2\
mkdir \\M9965-opz327\c$\NDS2\

xcopy /R /E ..\NDS2\*.* \\M9965-opz309\c$\NDS2\
xcopy /R /E ..\NDS2\*.* \\M9965-opz327\c$\NDS2\

del /Q /F /S \\M9965-opz309\c$\NDS2\CatalogVersionFiles.ps1
del /Q /F /S \\M9965-opz309\c$\NDS2\CatalogVersionFilesMaking.bat
del /Q /F /S \\M9965-opz309\c$\NDS2\NDS2Server

xcopy /R /E Scripts\*.* \\M9965-opz309\c$\NDS2\


PsExec.exe \\M9965-opz309 c:\NDS2\setup.bat
PsExec.exe \\M9965-opz327 c:\NDS2\setup.bat


sc \\M9965-opz327 create "NDS2" binPath= "c:\NDS2\NDS2Server\NDS2Server.WinService.exe" DisplayName= "NDS2"

sc \\M9965-opz327 start "NDS2"

pause
