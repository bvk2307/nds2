function SetCatalogVersion($Version, $Folder)
{
	$ConfigStorage = (Join-Path $Folder ConfigStorage)
	$GcDm = (Join-Path $Folder \ServerGC\DmCatalogTemplate\AbsFree\StaticMetadata)
    $UtilFolder = (Join-Path $Folder \ChangeVersionUtil\DmCatalogTemplate\AbsFree\StaticMetadata)
	
	CopyVersionMetadataToRole 'UnifiedClient' (Join-Path $Folder \Client) $UtilFolder
	#CopyVersionMetadataToRole 'ServerAp' (Join-Path $Folder \ServerAP) $UtilFolder
	#CopyVersionMetadataToRole 'ServerGc' (Join-Path $Folder \ServerGC) $UtilFolder
	#CopyVersionMetadataToRole 'ServerIs' (Join-Path $Folder \ServerIS) $UtilFolder
	#CopyVersionMetadataToRole 'ServerRm' (Join-Path $Folder \ServerRM) $UtilFolder
	#CopyVersionMetadataToRole 'ChangeVersionUtil' (Join-Path $Folder \ChangeVersionUtil) $UtilFolder
	#CreateLstForUtil $UtilFolder $ConfigStorage

	# SetCatalogVersionToRoleForGC 'UnifiedClient' 'UnifiedClientRole.config.lst' $Version (Join-Path $Folder \Client) $ConfigStorage $GcDm
	# SetCatalogVersionToRoleForGC 'ServerAP' 'ServerAp.Role.config.lst' $Version (Join-Path $Folder \ServerAP) $ConfigStorage $GcDm
	# SetCatalogVersionToRoleForGC 'ServerIS' 'ServerIs.Role.config.lst' $Version (Join-Path $Folder \ServerIS) $ConfigStorage $GcDm
	# SetCatalogVersionToRoleForGC 'ServerRM' 'ServerRm.Role.config.lst' $Version (Join-Path $Folder \ServerRM) $ConfigStorage $GcDm

	SetCatalogVersionToRole 'UnifiedClient' 'UnifiedClientRole.config.lst' $Version (Join-Path $Folder \Client) $ConfigStorage
	#SetCatalogVersionToRole 'ServerAP' 'ServerAP.Role.config.lst' $Version (Join-Path $Folder \ServerAP) $ConfigStorage
	#SetCatalogVersionToRole 'ServerIS' 'ServerIS.Role.config.lst' $Version (Join-Path $Folder \ServerIS) $ConfigStorage
	#SetCatalogVersionToRole 'ServerRM' 'ServerRM.Role.config.lst' $Version (Join-Path $Folder \ServerRM) $ConfigStorage
	#SetCatalogVersionToRole 'ServerGC' 'ServerGc.Role.config.lst' $Version (Join-Path $Folder \ServerGC) $ConfigStorage

	#SetCatalogVersionToRole 'ServerIM' 'ServerIM.Role.config.lst' $Version (Join-Path $Folder \ServerIM) $ConfigStorage
	#SetCatalogVersionToRole 'ZeroServer' 'ZeroServer.Role.config.lst' $Version (Join-Path $Folder \ZeroServer) $ConfigStorage
	#SetCatalogVersionToRole 'NDS2Server' 'NDS2Server.Role.config.lst' $Version (Join-Path $Folder \NDS2Server) $ConfigStorage

	#SetCatalogVersionToRole 'ChangeVersionUtil' 'ChangeVersionUtilRole.config.lst' $Version (Join-Path $Folder \ChangeVersionUtil) $ConfigStorage
}
function SetCatalogVersionToRoleForGC($Role, $RoleLstFile, $Version, $Folder, $ConfigStorage, $GcDm)
{
	$Dm = (Join-Path $Folder \DmCatalogTemplate\AbsFree\StaticMetadata)

	$versionFiles = Set-CatalogVersion $Role $Version $Folder
	Copy-Item $versionFiles $Dm -Force
	Copy-Item $versionFiles $GcDm -Force

	$listFile = Set-MakeList (Join-Path $Dm $RoleLstFile) -exclude "*.DomainSettings|*.lst"

	Get-ChildItem $Dm -recurse -exclude "*.DomainSettings" | Copy-Item -destination $ConfigStorage -Force
}
function SetCatalogVersionToRole($Role, $RoleLstFile, $Version, $Folder, $ConfigStorage)
{
	$Dm = (Join-Path $Folder \DmCatalogTemplate\AbsFree\StaticMetadata)

	$versionFiles = Set-CatalogVersion $Role $Version $Folder
	Copy-Item $versionFiles $Dm -Force

	$listFile = Set-MakeList (Join-Path $Dm $RoleLstFile) -exclude "*.DomainSettings|*.lst"

	Get-ChildItem $Dm -recurse -exclude "*.DomainSettings" | Copy-Item -destination $ConfigStorage -Force
}
function SearchCatalogMismatchVersion($Folder)
{
	Search-CatalogMismatchVersion (Join-Path $Folder \Client)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ServerAP)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ServerIS)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ServerRM)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ServerGC)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ServerIM)
	Search-CatalogMismatchVersion (Join-Path $Folder \NDS2Server)
	#Search-CatalogMismatchVersion (Join-Path $Folder \ZeroServer)
}
function CopyVersionMetadataToRole($Role, $Folder, $UtilFolder)
{
    $newFilename = $Role + '.DeploymentRoleMetadata.xml' 
	$Dm = (Join-Path $Folder \DmCatalogTemplate\AbsFree\StaticMetadata\$newFilename)
	$metaFile = (Join-Path $Folder DeploymentRoleMetadata.xml)

	Copy-Item $metaFile $Dm -Force

	#if($Role -ne 'ChangeVersionUtil')
	#{
	#	Copy-Item $Dm $UtilFolder -Force
    #}
}
function CreateLstForUtil($UtilFolder, $ConfigStorage)
{
	Get-ChildItem $UtilFolder -recurse -exclude "*.DomainSettings" | Copy-Item -destination $ConfigStorage -Force
}
function PerformAnnounce($Folder)
{
    $OutputEncoding = [system.text.encoding]::UTF8
	$application = "$Folder\ChangeVersionUtil\CommonComponents.ChangeVersionUtil.exe"
	$arguments = "/silent /force"

	$extjob = New-Object System.Diagnostics.Process
	$extjob.StartInfo = New-Object System.Diagnostics.ProcessStartInfo
	$returnexitcode = $false
  
	$extjob.StartInfo.FileName = $application
	$extjob.StartInfo.Arguments = $arguments
	$extjob.StartInfo.UseShellExecute = $shell
	$extjob.StartInfo.WindowStyle = 1
	$extjob.StartInfo.RedirectStandardOutput = $true
	$extjob.StartInfo.RedirectStandardError = $true
  
	$null = $extjob.Start()
	$extjob.WaitForExit()
	$returnexitcode = $extjob.ExitCode
	$resulttxt = $extjob.StandardOutput.ReadToEnd()
	$errors = $extjob.StandardError.ReadToEnd();

	$characters = $resulttxt | measure-object -character | select -expandproperty characters

	Write-host $returnexitcode

	if($returnexitcode -eq 0)
	{
		Write-Host "����� ���������� ������ �������." -foregroundcolor green 
	}
	else
	{

		$b=[system.text.encoding]::UTF8.GetBytes($errors)
		$c=[system.text.encoding]::convert([text.encoding]::UTF8,[text.encoding]::Unicode,$b) 
		$d = [system.text.encoding]::Unicode.GetChars($c)

		Write-Host $errors
		Write-Host "�� ������� �������� ����� ���������� � ���������. ��������� ���������� � ������� ��������������." -foregroundcolor red 
	}
}