@echo off

rem -------------------------------------------------------------------------
rem �������� ����������� ��
If "%ProgramW6432%" Neq "" (
	Echo Processor Architecture: x64
	@set catx=x64
	goto Install
)
If NOT "%ProgramW6432%" Neq "" (
	Echo Processor Architecture: x86
	@set catx=x86
	goto Install
)
if errorlevel 1 goto end
:Install
echo Start installation utility...

@set pathBat=%~dp0
if errorlevel 1 goto end
echo --- Executed path: %pathBat% 

rem ---------------------- ������ � x32
@set pathlib=%pathBat%\%catx%\
echo --- Rename in catalog: %pathlib%
CD %pathlib%
if errorlevel 1 goto end
echo --- cd in catalog: %pathlib%
xcopy /Y /R *.* ..\*.*
xcopy /Y /R *.nds2assembly ..\*.dll
xcopy /Y /R *.nds2exec ..\*.exe
del /F ..\*.nds2assembly
del /F ..\*.nds2exec
rem if errorlevel 1 goto end
echo --- rename to .dll in %catx32%
CD ..

rem end of bat file
goto end
:end
echo --- ERRORLEVEL: %ERRORLEVEL%

exit /b %ERRORLEVEL%