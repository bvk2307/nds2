sc \\localhost stop "NDS2"

sc \\localhost delete "NDS2"

del /Q /F /S \\localhost\c$\NDS2\

mkdir \\localhost\c$\NDS2\

xcopy /R /E ..\NDS2\*.* \\localhost\c$\NDS2\

PsExec.exe \\localhost c:\NDS2\setup.bat

sc \\localhost create "NDS2" binPath= "c:\NDS2\NDS2Server\NDS2Server.WinService.exe" DisplayName= "NDS2"

sc \\localhost start "NDS2"

pause
