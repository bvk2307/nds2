sc \\localhost stop "LoadingService"

sc \\localhost delete "LoadingService"

rem ping -n 5 127.0.0.1 > nul

del /Q /F /S \\localhost\c$\NDS2LoadTest\

mkdir \\localhost\c$\NDS2LoadTest\

xcopy /R /E ..\LoadTest\Service\*.* \\localhost\c$\NDS2LoadTest\

PsExec.exe \\localhost c:\NDS2LoadTest\setupLoadTest.bat

PsExec.exe \\localhost c:\NDS2LoadTest\Luxoft.NDS2.LoadTesting.Host.exe -i

sc \\localhost start "LoadingService"

pause
