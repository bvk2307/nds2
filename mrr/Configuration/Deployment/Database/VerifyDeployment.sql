--

WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput on size 30000;
declare
v_missed_objects number(3) := 0;
v_missedObjEx exception;
begin
  select count(1) into v_missed_objects from all_objects where owner = 'NDS2_INSTALL';
  if(v_missed_objects >0) then
     dbms_output.put_line('Found missed objects');
     for line in (select * from all_objects where owner = 'NDS2_INSTALL')
     loop
            dbms_output.put_line(line.object_name||' '||line.object_type);
     end loop;
     raise_application_error( -20001, 'Found missed objects' );
  end if;  
  dbms_output.put_line('<====Done====>');
end;
/

select 'CHECKING INVALID OBJECTS' as Message from dual;

declare
v_cntBrokenObjects number(3) := 0;
ex_fail exception;
begin
    
  
  for line in (select distinct owner, object_name, decode(object_type, 'PACKAGE BODY', 'PACKAGE', object_type) as object_type 
				from all_objects 
				where status = 'INVALID' and owner in ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2', 'NDS2_SEOD')
				order by 1, 3, 2) loop
        begin     
            execute immediate 'alter '||line.object_type||' '||line.owner||'."'||line.object_name||'" compile';
        exception when others then
                DBMS_OUTPUT.PUT_LINE('>>>>>>  ['||line.owner||']'||line.object_name||'('||line.object_type||'):');
                DBMS_OUTPUT.PUT_LINE('......'||substr(sqlerrm, 1, 256));
        end;
    end loop;
  

  DBMS_OUTPUT.NEW_LINE;
  DBMS_OUTPUT.PUT_LINE('----------------------------------------------------------------------');
  DBMS_OUTPUT.PUT_LINE('CHECKING CRITICAL INVALID OBJECTS');
  DBMS_OUTPUT.NEW_LINE;

    for line in (select distinct owner, object_name, decode(object_type, 'PACKAGE BODY', 'PACKAGE', object_type) as object_type 
         from all_objects 
		 where status = 'INVALID' 
                    and owner in ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2', 'NDS2_SEOD')
                    and object_name not in ('-', 'V$TMP_EGRN_UL', 'V$TMP_RSBASKNDS_TG_IP_V', 'V$TMP_RSBASKNDS_TG_SBOR', 'V$TMP_RSBASKNDS_TG_UL_V', 
												 'V$TMP_TORGSB_IR_1_1', 'V$TMP_TORGSB_IR_1_2', 'V$TMP_TORGSB_IR_1_3', 'V$TMP_TORGSB_IR_1_4',
												 'V$TMP_TORGSB_IR_2', 'V$TMP_TORGSB_IR_2_2', 'V$TMP_TORGSB_IR_2_3', 'V$TMP_BLRDOCUMENT', 'V$TMP_CUSTOMER_NDS_G47',
												 'V$TMP_D6NDFLASKNDS_V', 'V$TMP_RSBASKNDS_FL_IMUS_V'
												,'TMP_RSBASKNDS_NDS_IP_V','TMP_RSBASKNDS_NDS_UL_V','TMP_SUB_M_IP_LIC_ADR_V'
												,'TMP_SUB_M_IP_LIC_NAME_V', 'TMP_SUB_M_IP_LIC_V', 'TMP_SUB_M_IP_V'
												,'TMP_SUB_M_UL_V', 'TMP_SUB_PATENT_IP_V', 'TMP_SUB_2NDFL_IP_V'
												, 'V$TMP_SUBS_REG77_TAB_5_1')
         order by 1, 3, 2) loop
        begin     
            execute immediate 'alter '||line.object_type||' '||line.owner||'."'||line.object_name||'" compile';
        exception when others then
        v_cntBrokenObjects := v_cntBrokenObjects + 1;
                DBMS_OUTPUT.PUT_LINE('>>>>>>  ['||line.owner||']'||line.object_name||'('||line.object_type||'):');
                DBMS_OUTPUT.PUT_LINE('......'||substr(sqlerrm, 1, 256));
        end;
    end loop;
    
    if v_cntBrokenObjects > 0 then
        DBMS_OUTPUT.PUT_LINE('-');DBMS_OUTPUT.PUT_LINE('-');DBMS_OUTPUT.PUT_LINE('-');
    dbms_output.put_line('!!!!! CRITICAL INVALID OBJECTS FOUND. SEE ABOVE !!!!');
    raise_application_error( -20001, 'Found invalid objects' );
    end if;
end;
/
set serveroutput off;

select 'DONE' as Message from dual;

exit;
