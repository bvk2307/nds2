@echo OFF
set SidName=NDS2
set userName=NDS2_INSTALL
set userPas=NDS2_INSTALL
set sysName=SYS
set sysPas=demo
set continiousIntegrationSign=CI

SET NLS_LANG=AMERICAN_CIS.CL8MSWIN1251
chcp 1251

echo _____________database prepare__________________________
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @kill_all_sessions.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @AIS/1.SYSDBA.Users.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @FIR/1.SYSDBA.Users.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @ICAM/1.SYSDBA.Users.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @MRR/01.SYSDBA.Users.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @MRR/01.1.SYSDBA.DbLinks.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @INDS2/1.SYSDBA.Users.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @INDS2/1.1.SYSDBA.DbLinks.sql
sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @SEOD/01.SYSDBA.Users.sql

echo _____________database deploy__________________________


IF %ERRORLEVEL%==0 (
	echo _____________ MRR base
	xcopy MRR\*.dmp %ORACLE_BASE%\admin\NDS2\dpdump\ /Y
	impdp userid='sys/demo@NDS2 as sysdba' dumpfile=NDS2_WITH_TEST_DATA.dmp schemas=FIR,NDS2_MC,NDS2_MRR_USER,I$CAM,NDS2_SUR exclude=statistics transform=segment_attributes:n
	sqlplus %userName%/%userPas%@%SidName% @MRR/DumpUpdater.sql
	cmd /c "exit /b 0"
)

IF %ERRORLEVEL%==0 (
 	echo _____________ Grant_Add
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/Grant_Add.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/Grant_Add.sql > Grant_Add.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.12.2 hotfix 8571 by FLS
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_fls_8571.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_fls_8571.sql > MRR-Release-8.12_hotfix_fls_8571.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.12.3 hotfix 8120
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_8120.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_8120.sql > MRR-Release-8.12_hotfix_8120.txt
)


IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.12.3 hotfix 8784
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_8784.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.12_hotfix_8784.sql > MRR-Release-8.12_hotfix_8784.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR Pre-Migration to 8.13 regnumbers
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Pre-Migration_8.13.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Pre-Migration_8.13.sql > MRR-Pre-Migration_8.13.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR SelectionNewArch
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-SelectionNewArch.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-SelectionNewArch.sql > MRR-SelectionNewArch.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.1
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.1.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.1.sql > MRR-Release-8.13.1.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ SEOD 8.13_8043
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @SEOD/SEOD-Release-8.13_8043.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @SEOD/SEOD-Release-8.13_8043.sql > SEOD-Release-8.13_8043.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_8043
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8043.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8043.sql > MRR-Release-8.13_8043.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_7884
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_7884.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_7884.sql > MRR-Release-8.13_7884.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_8448
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8448.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8448.sql > MRR-Release-8.13_8448.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_8123
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8123.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8123.sql > MRR-Release-8.13_8123.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_8998
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8998.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_8998.sql > MRR-Release-8.13_8998.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13_hotfix.sql > MRR-Release-8.13_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.4.1_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.4.1_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.4.1_hotfix.sql > MRR-Release-8.13.4.1_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.4.2_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.4.2_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.4.2_hotfix.sql > MRR-Release-8.13.4.2_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ SEOD 8.13.4.4_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @SEOD/SEOD-Release-8.13.4.4_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @SEOD/SEOD-Release-8.13.4.4_hotfix.sql > SEOD-Release-8.13.4.4_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.5.1_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.5.1_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.5.1_hotfix.sql > MRR-Release-8.13.5.1_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.5.2_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.5.2_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.5.2_hotfix.sql > MRR-Release-8.13.5.2_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR 8.13.6.1_hotfix
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.6.1_hotfix.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/MRR-Release-8.13.6.1_hotfix.sql > MRR-Release-8.13.6.1_hotfix.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ MRR Common
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/99.MRR.SchemaObjects.Common.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/99.MRR.SchemaObjects.Common.sql > 99_MRR_Common.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ SEOD Common
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @SEOD/99.SEOD.SchemaObjects.Common.sql
 	) ELSE sqlplus %userName%/%userPas%@%SidName% @SEOD/99.SEOD.SchemaObjects.Common.sql > 99_SEOD_Common.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ CI
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %sysName%/%sysPas%@%SidName% as sysdba @CI/Additions.sql
 	) ELSE sqlplus %sysName%/%sysPas%@%SidName% as sysdba @CI/Additions.sql > Additions.txt
)

IF %ERRORLEVEL%==0 (
 	echo _____________ CI
 	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %sysName%/%sysPas%@%SidName% as sysdba @CI/MC_temp.sql
 	) ELSE sqlplus %sysName%/%sysPas%@%SidName% as sysdba @CI/MC_temp.sql > MC_temp.txt
)

IF %ERRORLEVEL%==0 (
	echo _____________ XSD
	sqlplus %sysName%/%sysPas%@%SidName% as sysdba @MRR/XSD_Schemas.sql > XSD_Schemas.txt
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 base
	sqlplus %userName%/%userPas%@%SidName% @INDS2/2.I$NDS2.SchemaObjects.Rel_6.0.1.1.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 6.x
	sqlplus %userName%/%userPas%@%SidName% @INDS2/3.I$NDS2.SchemaObjects.Rel_6.0.7.1.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 8.3
	sqlplus %userName%/%userPas%@%SidName% @INDS2/4.I$NDS2.SchemaObjects.Rel_8.3.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 8.10-UserTask
	sqlplus %userName%/%userPas%@%SidName% @INDS2/10.I$NDS2.SchemaObjects.8.10-UserTask.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 8.10-6072_reclaim_close
	sqlplus %userName%/%userPas%@%SidName% @INDS2/11.I$NDS2.8.10_6072_reclaim_close.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ INDS2 Common
	sqlplus %userName%/%userPas%@%SidName% @INDS2/9.I$NDS2.SchemaObjects.Common.sql
)

IF %ERRORLEVEL%==0 (
	echo _____________ ICAM Common
	sqlplus %userName%/%userPas%@%SidName% @ICAM/2.ICAM.SchemaObjects.sql > ICAM.Common.txt
)

IF %ERRORLEVEL%==0 (
	sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @VerifyDeployment.sql
)

IF NOT %ERRORLEVEL%==0 IF "%1"=="%continiousIntegrationSign%" echo ##teamcity[buildStatus status='FAILURE' text='Error in deploy database']

IF not "%1"=="%continiousIntegrationSign%" pause

EXIT /B %ERRORLEVEL%