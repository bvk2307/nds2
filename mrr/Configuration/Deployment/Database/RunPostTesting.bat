@echo OFF
set SidName=NDS2
set userName=NDS2_INSTALL
set userPas=NDS2_INSTALL
set sysName=SYS
set sysPas=demo
set continiousIntegrationSign=CI



IF %ERRORLEVEL%==0 (
	echo _____________ Post Testing
	IF "%1"=="%continiousIntegrationSign%" ( sqlplus %userName%/%userPas%@%SidName% @MRR/Post_Testing.sql
	) ELSE sqlplus %userName%/%userPas%@%SidName% @MRR/Post_Testing.sql > 99_Post_Testing.txt
)

IF %ERRORLEVEL%==0 (
	sqlplus %sysName%/%sysPas%@%SidName% as sysdba  @VerifyDeployment.sql
)

IF NOT %ERRORLEVEL%==0 IF "%1"=="%continiousIntegrationSign%" echo ##teamcity[buildStatus status='FAILURE' text='Error in Database Post Testing']

IF not "%1"=="%continiousIntegrationSign%" pause 

EXIT /B %ERRORLEVEL%