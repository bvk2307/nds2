@ECHO OFF
powershell.exe "Import-Module (Join-Path '%CD%' 'Tools\CommonComponents.BuildCommands.dll'); . .\CatalogVersionFiles.ps1; SetCatalogVersion '5.3.0.2' %CD%"
@echo Indexation started.
@echo Indexing Client.
cd Client
CommonComponents.Catalog.IndexationUtility.exe
@echo Indexing NDS2Server.
cd ../NDS2Server
CommonComponents.Catalog.IndexationUtility.exe
@echo Indexing NDS2Server.TaskScheduler.
cd ../NDS2Server.TaskScheduler
CommonComponents.Catalog.IndexationUtility.exe

cd ..
@echo Press Enter to continue