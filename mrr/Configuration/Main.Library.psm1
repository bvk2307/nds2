﻿
$CCVR = "$($PSScriptRoot)\Main.Variables.vSettings"

$ErrorActionPreference = "Stop"

#$modules = $Content.Split([System.Environment]::NewLine) | where { $_.EndsWith('.psm1') } | foreach-object -process { ".\$_" }

function GetMainDir([string]$curDir)
{
    if ($curDir -eq '') { $curDir = get-location }


    $rootMarker = 'Library.psm1'
    $currentDir = $curDir

    while($true) 
    {
        $curDir = split-path $curDir; 
    
        if ($curDir -eq '')
        {
            writeToHost "ERROR 0: Маркер начала (файл: '$($rootMarker)') не найден. Анализируемый путь: '$($currentDir)' "
            
            return $currentDir
        }
    
        if (test-path "$($curDir)\$($rootMarker)")
        {
            WriteToHost "Маркер начала (файл: '$($rootMarker)') найден. Путь: '$($curDir)' "

            return $curDir
        }
    }
}

function ApplyContentTemplates()
{
	param
    (
		[parameter(Mandatory=$true)][string]$targetDirectory, 
		[parameter(Mandatory=$true)][string]$sourceDirectory, 
		[parameter(Mandatory=$true)][string]$templateDirectory,  
		[parameter(Mandatory=$true)][string]$contentDirectory,
		[parameter(Mandatory=$true)][string]$contentIn,
		[parameter(Mandatory=$true)][string[]]$configuration, 
		[parameter(Mandatory=$true)][string]$templateMarker
    )

	$sourceDirectory = $sourceDirectory.TrimEnd(' ', '\', '/')
    $templateDirectory = $templateDirectory.TrimEnd(' ', '\', '/')
	$contentDirectory = $contentDirectory.TrimEnd(' ', '\', '/')
	$targetDirectory = $targetDirectory.TrimEnd(' ', '\', '/')

    Write-Host ">>================================================================================ ApplyContentTemplates === $($templateMarker) ==<<"
    Write-Host ">>>>  targetDirectory      >>>>> $($targetDirectory)"
    Write-Host ">>>>  sourceDirectory      >>>>> $($sourceDirectory)"
    Write-Host ">>>>  contentDirectory     >>>>> $($contentDirectory)"
    Write-Host ">>>>  templateDirectory    >>>>> $($templateDirectory)"
	Write-Host ">>>>  configuration        >>>>> $($configuration)"
    Write-Host ">>=================================================================================================================================="

    if(!(test-path $sourceDirectory))
    {
        Write-Host "error 0: Не найден путь директории источника файлов"
        return
    }
    
    if(!(test-path $templateDirectory))
    {
        Write-Host "error 0: Не найден путь директории шаблонов"
        return
    }

	if(!(test-path $contentDirectory))
    {
        Write-Host "error 0: Не найден путь директории контента"
        return
    }

    $sourceDirectory = Get-Item $sourceDirectory
    $templateDirectory = Get-Item $templateDirectory
	$contentDirectory = Get-Item $contentDirectory
    
    $content = $contentIn.Split([System.Environment]::NewLine)  | where { -not [System.String]::IsNullOrWhiteSpace($_) } | foreach -Process { Join-Path -Path $contentDirectory -ChildPath $_ }
    
    $settingFiles = $content | where { $($_).EndsWith(".vSettings") } 
    
    if (!$settingFiles)
    {
        Write-Host "Error 0: в списке файлов контента не найдено файлов настроек переменных"
        return
    }

	Write-host "Файлы настроек: $($settingFiles)"
    
    $Variables = New-Variables -Settings $settingFiles -Configs $configuration
	$fileMarker = "\" + $templateMarker
    
    $primaryTemplateFiles = $content | where { $($_).StartsWith($templateDirectory+"\") -and $($_).EndsWith($fileMarker) } 

    if (!$primaryTemplateFiles)
    {
        return
    }

    $context = New-Merge -TargetDirectory $targetDirectory -Variables $Variables -Configs $configuration   
    
	foreach($v in Get-Variable)
	{
		#Write-Host $v
		$context.ContextVariables.Add("PS." + $($v).Name, $v)
	}

	$ProgramPath = ${Env:ProgramFiles}

	$ProgramPathX86 = ${Env:ProgramFiles(x86)}
	if ($ProgramPathX86 -eq $null) {$ProgramPathX86 = ${Env:ProgramFiles}}

	$ProgramPathX86 = $ProgramPathX86.TrimEnd(' ', '\')
	$ProgramPath = $ProgramPath.TrimEnd(' ', '\')

	$context.ContextVariables.Add("ProgramFilesX86", $ProgramPathX86)
	$context.ContextVariables.Add("ProgramFiles", $ProgramPath)

    $context.ContextVariables.Add("targetDirectory", $targetDirectory)
    $context.ContextVariables.Add("sourceDirectory", $sourceDirectory)
    $context.ContextVariables.Add("templateDirectory", $templateDirectory)
	$context.ContextVariables.Add("contentDirectory", $contentDirectory)
    $context.ContextVariables.Add("configuration", $configuration)
    
    $context = $context | Add-MergeTemplate  -SourceDirectory $sourceDirectory -TemplateDirectory $templateDirectory -LstFiles $primaryTemplateFiles
    
    foreach($config in $configuration)
    {
        $fileConfigSpecMarker = "\" + $configuration + "." +$templateMarker
        $configuredTemplateFiles = $content | where { $($_).StartsWith($templateDirectory+"\") -and $($_).EndsWith($fileConfigSpecMarker) } 
        if (!$configuredTemplateFiles) { continue }
        
        $context = $context | Add-MergeTemplate  -SourceDirectory $sourceDirectory -TemplateDirectory $templateDirectory -LstFiles $configuredTemplateFiles
    }

    $result = Enter-Merge -Context $context -EnterErrorAction "Error"

    if ($result.ResultEvent -eq "Error")
    {
        foreach ($i in  $result.Files)
        {
            Write-Host $i.LogItem
        }
    }

	if ($result.ResultEvent -ne "Error")
    {
		foreach ($i in  $result.Files)
		{
			if ($i.Action -ne "Skipped")
			{
				Write-Host $i.LogItem
			}
		}
	}

	Write-Host ">>== Elapsed Time: "$result.ComposeTime"   Errors: "$result.ErrorCount"   Skip: "$result.SkipCount"   Updated: "$result.UpdateCount"   New: "$result.CopyCount
	Write-Host ">>=================================================================================================================================="
}


#region ApplyCopy

function UpdateItem ([object]$sourceFile, [object]$destinationFile)
{ 
    #echo (get-item $($destinationFile))
    #return
    if(test-path $destinationFile)
    {
        $destinationFile = resolve-path $destinationFile

        $s = get-item $sourceFile
        $d = get-item $destinationFile

        $slwt = $s.LastWriteTime.ToFileTime()
        $dlwt = $d.LastWriteTime.ToFileTime()

        $sl = $s.Length
        $dl = $d.Length

        #WriteToHost ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> $sl ===== $dl"
        if($slwt -gt $dlwt)
        { 
            WriteToHost "[UPDATED(dt)]: $($sourceFile) -------> $($destinationFile)"

            $result = copy-item $sourceFile -destination $destinationFile -force

            add-content -path "$($objDir)\$($config)UpdateList.log" -value "[UPDATED(dt)]:$sourceFile|$destinationFile|$result"
            return #$($destinationFile)
        }
        if($sl -ne $dl)
        { 
            WriteToHost "[UPDATED(ln)]: $($sourceFile) -------> $($destinationFile)" 

            $result = copy-item $sourceFile -destination $destinationFile -force

            add-content -path "$($objDir)\$($config)UpdateList.log" -value "[UPDATED(ln)]:$sourceFile|$destinationFile|$result"
            return #$($destinationFile)
        }
    }
    else
    {
        WriteToHost ("[COPIED]: {0} -------> {1}" -f $sourceFile, $destinationFile)

        $result = copy-item $sourceFile -destination $destinationFile -force -recurse

        add-content -path "$($objDir)\$($config)UpdateList.log" -value "[COPIED]:$sourceFile|$destinationFile|$result"
        return #$($destinationFile)
    }
} 


function UpdateItemInternal ([object]$rootDir, [object]$objDir, [object]$sourceFile, [object]$destinationFile, [string]$config)
{ 
    $sourceItem = get-item $sourceFile;
    
    #WriteToHost ([System.Reflection.AssemblyName]::GetAssemblyName($sourceItem).FullName)

    UpdateItem -sourceFile $sourceItem -destinationFile $destinationFile
    
    #if (test-path $destinationFile)
    #{
        
    #}
} 

function MakeFileList()
{
    param
    (
        [parameter(Mandatory=$true)][string]$folderPath,
        [switch]   $excludeCurrentFolder,
        [string[]] $fileMask,
        [switch]   $recurse,
        [string]   $targetFileName,
        [string]   $fileMaskSource,
        [string[]] $excludeFileMask
    )
    
    WriteToHost ("--- Формирование списочного файла:" + $targetFileName)

    $l = $folderPath.Length
    
    if (Test-Path $targetFileName) { Remove-Item $targetFileName }
    $files = Get-ChildItem $folderPath -Include $fileMask -Exclude $excludeFileMask -Recurse |  where {!$_.PSIsContainer}
    
    WriteToHost ("количество файлов:" + $files.Count)

    foreach ($file in $files)
    {
        if ($excludeCurrentFolder -and ($file.FullName.Substring($l+1) -eq $file.Name)) {continue}
        
        WriteToHost ("добавлен файл:" + $file.Name)
        Add-Content -Path $targetFileName -Value $file.Name
    }
}

function MakeFileListXml()
{
    param
    (
        [parameter(Mandatory=$true)][string]$folderPath,
        [string]   $relPath,
        [string[]] $fileMask,
        [switch]   $recurse,
        [string]   $targetFileName,
        [string]   $fileMaskSource
    )
    
    #$rootPath = "e:\R2\Design\Core\Build\Debug\Build\Client"
    #$relPath = "\DmCatalogTemplate\AbsFree\StaticMetadata\"

    if (![string]::IsNullOrEmpty($relPath))
    {
        $path = Join-Path -Path $folderPath -ChildPath $relPath
    }
    else
    {
        $path = $folderPath
    }

    if (!(test-path $path))
    {
        WriteToHost "$($fileMaskSource): WARNING 0: переданный путь не найден: $($path)" -noPrefix
    }

    if ([string]::IsNullOrEmpty($targetFileName))
    {
        $targetFileName = "$($rootItem.Name).fileList"
        $targetFileName = join-path -Path $rootPath -ChildPath $targetFileName
    }
    
    Remove-Item $targetFileName -Force
    
    $rootItem = Get-Item $folderPath 

    $xml = new-object XML

    $files = $xml.CreateElement("files")

    $files = $xml.AppendChild($files)

    $items = Get-ChildItem $path -Include $fileMask -Recurse:$recurse | where {!$_.PSIsContainer} 
    
    foreach ($item in $items)
    {
        $xitem = $xml.CreateElement("item")
        $xitem.set_InnerText($item.Name)
        $xitem = $files.AppendChild($xitem)
        $xitem.SetAttribute("length", $item.Length)
        $xitem.SetAttribute("fullName", $item.FullName)
        $xitem.SetAttribute("lwt", $item.LastWriteTime)
    }

    $xml.Save($targetFileName)
    
    WriteToHost "[MAKELIST-Dir]: $($path)"
    WriteToHost "[MAKELIST-Out]: $($targetFileName)"
}

function ApplyCopyFileMask([object]$rootDir, [object]$objDir, [object]$fromBasePath, [object]$toBasePath, [object] $templateBasePath, [string]$fileMask, [string] $fileMaskSource, [string]$config, [string]$newFileName)
{   
    #WriteToHost ">>================================== ApplyCopyFileMask ======================"
    #WriteToHost "[TARGET-DIR]: $($toBasePath)"
    #WriteToHost "[TEMPLT-DIR]: $($templateBasePath)"
    #WriteToHost "[SOURCE-DIR]: $($fromBasePath)"
    #WriteToHost "[fileMask      ]: $($fileMask)"
    #WriteToHost "[fileMaskSource]: $($fileMaskSource)"
    #WriteToHost "[config        ]: $($config)"

    $fromBasePath = Resolve-Path $fromBasePath
    $toBasePath = Resolve-Path $toBasePath

    add-content -path "$($objDir)\$($config)UpdateList.log" -value ">>================================== ApplyCopyFileMask ======================"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[TARGET-DIR]:     $($toBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[TEMPLT-DIR]:     $($templateBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[SOURCE-DIR]:     $($fromBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[fileMask]:       $($fileMask)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[fileMaskSource]: $($fileMaskSource)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[config]:         $($config)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[newFileName]:    $($newFileName)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "-----------------------------------------------------------------------------"
    

    $targetFilesMask =  join-path -path $fromBasePath -childpath $fileMask

    if (test-path $targetFilesMask)
    {
       $maskedFiles = get-childitem $targetFilesMask | where {!$_.PSIsContainer}
       if(($maskedFiles.count -eq 0) -or ($maskedFiles -eq $null))
       {
            $msg = ("{0}: WARNING 1: Файлов по маске '{1}' не найдено. Каталог: '{2}'." -f  $fileMaskSource, $fileMask, $fromBasePath)
            WriteToHost $msg -noPrefix
            add-content -path "$($objDir)\$($config)UpdateList.log" -value $msg
            continue
       }

       foreach($targetFile in $maskedFiles)
       {
          if ($targetFile -eq $null) {continue}

          if (test-path $targetFile)
            {
                $files = ($targetFile | resolve-path)
                foreach($file in $files)
                {
                    if ($newFileName.Length -eq 0)
                    {
                        $destination = join-path -path $toBasePath -childpath (split-path  $file -leaf)
                    }
                    else
                    {
                        $destination = join-path -path $toBasePath -childpath $newFileName
                    }
                    #echo "($file) ======= $destination"
                    
                    $file = get-item $file
                    
                    UpdateItemInternal -rootDir $rootDir -objDir $objDir -sourceFile $file -destinationFile $destination -config $config

                    if ((($fileMask.EndsWith(".dll")) -or ($fileMask.EndsWith(".exe"))) -and (($destination.EndsWith(".dll")) -or ($destination.EndsWith(".exe"))))
                    {
                        $pathAndName = $File.FullName.Substring(0, $File.FullName.Length-4)
                        
                        foreach($ext in (".pdb", ".xml", ".info"))
                        {
                            #WriteToHost ">>>>>>>>>>>>>>>>>>>>>>>> $($pathAndName)$($ext)"
                            $autoSrcFile =  "$($pathAndName)$($ext)"

                            if (test-path $autoSrcFile)
                            {
                                $autoDstFile = $destination.Substring(0, $destination.Length-4)+$ext

                                UpdateItemInternal -rootDir $rootDir -objDir $objDir -sourceFile (get-item $autoSrcFile) -destinationFile $autoDstFile -config $config
                            }
                        }
                    }
                }
            }
            else
            {
                $msg = "{0}: WARNING 3: Не найден файл(ы) {0}." -f  $fileMaskSource, $targetFile
                WriteToHost $msg -noPrefix
                add-content -path "$($objDir)\$($config)UpdateList.log" -value $msg
            }
        }
    }
    else
    {
        $msg = ("{0}: WARNING 2: Не найден файл(ы) '{1}'. Каталог: '{2}'." -f  $fileMaskSource, $targetFilesMask,  $fromBasePath)
        WriteToHost $msg -noPrefix
        add-content -path "$($objDir)\$($config)UpdateList.log" -value $msg
        continue
    }
}

function ApplyCopyDirectory([object]$rootDir, [object]$objDir, [object]$fromBasePath, [object]$toBasePath, [object] $templateBasePath, [string]$directory, [string] $directorySource, [string]$config, [string]$newDirectoryName)
{   
    #WriteToHost ">>================================== ApplyCopyFileMask ======================"
    #WriteToHost "[TARGET-DIR]: $($toBasePath)"
    #WriteToHost "[TEMPLT-DIR]: $($templateBasePath)"
    #WriteToHost "[SOURCE-DIR]: $($fromBasePath)"
    #WriteToHost "[directory      ]: $($directory)"
    #WriteToHost "[directorySource]: $($directorySource)"
    #WriteToHost "[config         ]: $($config)"

    add-content -path "$($objDir)\$($config)UpdateList.log" -value ">>================================== ApplyCopyDirectory ======================"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[TARGET-DIR]:      $($toBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[TEMPLT-DIR]:      $($templateBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[SOURCE-DIR]:      $($fromBasePath)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[directory]:       $($directory)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[directorySource]: $($directorySource)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[config]:          $($config)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "[newDirectoryName]:$($newDirectoryName)"
    add-content -path "$($objDir)\$($config)UpdateList.log" -value "-----------------------------------------------------------------------------"

    $fromPath = join-path -path $fromBasePath -ChildPath $directory
    if (test-path $fromPath)
    {
        $fromPath = Resolve-Path $fromPath
        $directory = split-path -path $fromPath -Leaf

        if ($newDirectoryName.Length -eq 0)
        {
            $toPath = join-path -path $toBasePath -ChildPath $directory
        }
        else
        {
            $toPath = join-path -path $toBasePath -ChildPath $newDirectoryName
        }

        CreateDirectory $toPath
        $toPath = Resolve-Path $toPath

        if ((split-path $fromPath -Leaf) -eq (split-path $toPath -Leaf))
        {
            CopyItems $fromPath (split-path $toPath -Parent)
        }
        else
        {
            CopyItems (join-path -path $fromPath -ChildPath "*") $toPath
        }
    }

    return 

}

function ApplyCopyFilesList([string]$rootDir, [string]$templateDir, [string]$sourceRelativePath, [string]$targetRelativePath, [string]$config)
{
    #WriteToHost ">>>>  rootDir              >>>>> $($rootDir)"
    #WriteToHost ">>>>  templateDir          >>>>> $($templateDir)"

    #WriteToHost ">>>>  sourceRelativePath   >>>>> $($sourceRelativePath)"
    #WriteToHost ">>>>  targetRelativePath   >>>>> $($targetRelativePath)"

    #WriteToHost ">>>>  config               >>>>> $($config)"

    
    
    if(!(test-path $rootDir))
    {
        WriteToHost "error 0: Передан не существующий путь головного каталога"
        return
    }
    
    
    $rootDir = get-item $rootDir
    
    $objDir =  get-item (join-path -path $rootDir -childpath "obj" )
    
    $logFile = "$($objDir)\$($config)UpdateList.log"

    if (test-path $logFile) {clear-content $logFile}

    add-content -path $logFile -value ">>=============================================================================================================="
    add-content -path $logFile -value ">>================================== ApplyCopyFilesList ==================================== config: $config"
    add-content -path $logFile -value ">>=============================================================================================================="
    add-content -path $logFile -value "[ROOT-DIR]:    $($rootDir)"
    add-content -path $logFile -value "[TEMPLT-DIR]:  $($templateDir)"
    add-content -path $logFile -value "[SOURCE-REL]:  $($sourceRelativePath)"
    add-content -path $logFile -value "[TARGET-REL]:  $($targetRelativePath)"
    add-content -path $logFile -value ">>=============================================================================================================="    
    
    $targetPath = join-path -path $rootDir -childpath $targetRelativePath | resolve-path 
    $sourcePath = join-path -path $rootDir -childpath $sourceRelativePath | resolve-path
    
    $directoryMarker = "-directory "
    $makeListMarker = "-makelist "
    
    
    #WriteToHost ">>  sourcePath           >>>>> $($sourcePath)"
    #WriteToHost ">>  targetPath           >>>>> $($targetPath)"

    $lstFile = 'CopyFiles.lst'

    if ($config -ne '') {$lstFile = "$config.$lstFile"}

    $copyfileListFile = get-childitem $templateDir -recurse -include $lstFile | where {!$_.PSIsContainer}

    foreach($listFile in $copyfileListFile)
    {
        if ($listFile -eq $null) { continue }

        WriteToHost "------------------------------[APPLY($($listFile))]------------------------"

        $relListFile = (split-path $listFile).Substring(($rootDir).Length)
        
        $fromBasePath = $sourcePath  | resolve-path
        $toBasePath = join-path -path $targetPath -childpath $relListFile

        $toBasePath = new-item -path "$($toBasePath)" -type directory -force
        $templateBasePath = $listFile.Directory

        WriteToHost "[SOURCE-DIR]: $($fromBasePath)"
        WriteToHost "[TARGET-DIR]: $($toBasePath)"
        #WriteToHost "[TEMPLT-DIR]: $($templateBasePath)"
        
        $list = get-content $listFile
        foreach($fileItem in $list)
        {
            if ($fileItem -eq $null) { continue }
            
            $trimmedFileItem = $fileItem.Trim(' ')
            
            if (($trimmedFileItem.Length -eq 0) -or ($trimmedFileItem.StartsWith("//"))) {continue}
            
            $isDirectory = $false
            if ($trimmedFileItem.ToLower().StartsWith($directoryMarker)) 
            {
                $trimmedFileItem = $fileItem.Substring($directoryMarker.Length).Trim(' ')
                $isDirectory = $true
            }
            
            if ($trimmedFileItem.ToLower().StartsWith($makeListMarker)) 
            {
                $trimmedListItem = $fileItem.Substring($makeListMarker.Length).Trim(' ')
                
                $targetFileName = Join-Path -Path $toBasePath -ChildPath $trimmedListItem
                MakeFileList -folderPath $toBasePath -recurse -excludeCurrentFolder -targetFileName $targetFileName -fileMaskSource $listFile
                continue
            }

            #--- поиск наличия указания на переименование копируемого элемента
            $tmpVal = $trimmedFileItem -split ">>", 2
            $trimmedFileItem = $tmpVal[0].Trim(' ')
            $newFileName = ""
            if ($tmpVal[1] -ne $null) {$newFileName = $tmpVal[1].Trim(' ')}
            #---

            if ($isDirectory) 
            {
                ApplyCopyDirectory -rootDir $rootDir -objDir $objDir -fromBasePath $fromBasePath -toBasePath $toBasePath -templateBasePath $templateBasePath -directory $trimmedFileItem -directorySource $listFile -config $config -newDirectoryName $newFileName
                continue
            }
            
            ApplyCopyFileMask -rootDir $rootDir -objDir $objDir -fromBasePath $fromBasePath -toBasePath $toBasePath -templateBasePath $templateBasePath -fileMask $trimmedFileItem -fileMaskSource $listFile -config $config -newFileName $newFileName
        }
    }
}

#endregion

function UpdateDirectory()
{   
    param
    (
        [parameter(Mandatory=$true)][string]$sourceDir,
        [parameter(Mandatory=$true)][string]$targetDir,
        [string]$fileMask
    )

    if (!(test-path $sourceDir))
    {
        WriteToHost "Error 0: переданный путь не найден: $($sourceDir)" -noPrefix
        return;
    }
    $sourceDir = Resolve-Path $sourceDir

    if (!(test-path $targetDir))
    {
        CreateDirectory $targetDir
    }
    $targetDir = Resolve-Path $targetDir

    WriteToHost "[UPDATE-DIR]: $sourceDir$fileMask -> $targetDir"

    $files = get-childitem $sourceDir -Include $fileMask -Recurse

    foreach ($file in $files)
    {
        $destination = join-path -path $targetDir -childpath $file.FullName.Substring($sourceDir.Length)
        UpdateItem -sourceFile $file -destinationFile $destination
    }

    return 
}

function CreateDirectory ([object]$sourceDirectory)
{ 
    if(!(test-path $sourceDirectory))
    {
        WriteToHost "[CREATED]: $($sourceDirectory)" 
        $dir = new-Item -type directory -path $sourceDirectory
    }
}

function CopyItems ([object]$source, [object]$destination)
{ 
    WriteToHost "[COPIED]:  $($source) -------> $($destination)" 
    copy-item $source -destination $destination -force -recurse
    return #$($destination)
} 

function CopyFilesWithRename ([object]$source, [object]$destinationPath, [string]$partForRename, [string]$valueForRename)
{ 
    WriteToHost "[COPIED with RENAME]:  $($source) -------> $($destinationPath), $($partForRename) ---> $($valueForRename)"
    foreach ($file in Get-Item $source) 
    {
        $newFileName = ($file.Name -replace $partForRename, $valueForRename)
        $destination = join-path -path $destinationPath -childpath $newFileName
        Copy-Item $file.FullName -destination $destination -force
    }
    return $destination
} 

function CopyFilesWithStructure ([string]$sourceDirPath, [string]$targetDirPath, [string]$fileMask)
{ 
    WriteToHost "[COPIED with STRUCTURE]:  $($sourceDirPath) -------> $($targetDirPath), Mask: $($fileMask)"

    $sourcePath = Resolve-Path $sourceDirPath
    foreach ($file in Get-ChildItem "$($sourcePath)\*" -recurse -include $fileMask)
    {
        $newDirPath = join-path -path $targetDirPath -childpath (($file.Directory.ToString()).Substring($sourcePath.ToString().Length))
        CreateDirectory $newDirPath
        $newFilePath = join-path -path $newDirPath -childpath $file.Name
        copy-item $file.FullName -destination $newFilePath -force
    }
} 

function WriteToHost ([string]$message, [switch]$newLine, [switch] $noPrefix)
{ 
    $message = $message -ireplace "Exception", "[E]xception"
    
    if (!$noPrefix)	{ $message = ("[" + (get-date -DisplayHint Time -format "hh:mm:ss") + "] : " + $message) }
    
    if ($newLine) {$message = "`n" + $message}
    write-host $message
}

# Обработать папку, содержащую файлы конфигурации
function ProcessConfigVariablesInFolder()
{
    param
    (
        [parameter(Mandatory=$true)][string]$folderPath,
        [string]$saveFolderPath,
        [string[]]$fileMask,
        [string]$configs,
        [switch]$recurse
        #[parameter(Mandatory=$true)][System.Collections.Hashtable]$variablesMap
    )
    
    if (!$saveFolderPath)
        { $saveFolderPath = $folderPath }
        
    if (!$fileMask)
        { $fileMask = @("*.*") }
    
    if (!(Test-Path $folderPath -PathType Container))
        { WriteToHost "Error 0: Не удалось найти папку [$($folderPath)] для замены переменных на значения."; exit 1 }
        
    WriteToHost "Начало обработки переменных в папке: [$($folderPath)]..."
    $files = get-childitem $folderPath -Include $fileMask -Recurse:$recurse | where { ! $_.PSIsContainer }
    
    if (!$files)
        { WriteToHost "Error 0: Не найдено файлов для обработки."; exit 0; }
    
    Set-BuildVariables -BasePath $folderPath -Settings $CCVR -Configs $configs -Files $files -Target $saveFolderPath -Increment -UpdateLst:$false
}

function Get-RelativePath {
	<#
	.SYNOPSIS
	   Get a path to a file (or folder) relative to another folder
	.DESCRIPTION
	   Converts the FilePath to a relative path rooted in the specified Folder
	.PARAMETER Folder
	   The folder to build a relative path from
	.PARAMETER FilePath
	   The File (or folder) to build a relative path TO
	.PARAMETER Resolve
	   If true, the file and folder paths must exist
	.Example
	   Get-RelativePath ~\Documents\WindowsPowerShell\Logs\ ~\Documents\WindowsPowershell\Modules\Logger\log4net.xslt
   
	   ..\Modules\Logger\log4net.xslt
   
	   Returns a path to log4net.xslt relative to the Logs folder
	#>
	[CmdletBinding()]
	param(
	   [Parameter(Mandatory=$true, Position=0)]
	   [string]$Folder
	, 
	   [Parameter(Mandatory=$true, Position=1, ValueFromPipelineByPropertyName=$true)]
	   [Alias("FullName")]
	   [string]$FilePath
	,
	   [switch]$Resolve
	)

	process {
	   Write-Verbose "Resolving paths relative to '$Folder'"
	   $from = $Folder = split-path $Folder -NoQualifier -Resolve:$Resolve
	   $to = $filePath = split-path $filePath -NoQualifier -Resolve:$Resolve

	   while($from -and $to -and ($from -ne $to)) {
		  if($from.Length -gt $to.Length) {
			 $from = split-path $from
		  } else {
			 $to = split-path $to
		  }
	   }

	   $filepath = $filepath -replace "^"+[regex]::Escape($to)+"\\"
	   $from = $Folder
	   while($from -and $to -and $from -gt $to ) {
		  $from = split-path $from
		  $filepath = join-path ".." $filepath
	   }
	   Write-Output $filepath
	}
}

# Осуществить слияние двух словарей переменных. 
# Первый аргумент - базовый, второй аргумент дополняет и переписывает совпадающие имена в первом.
function MergeVariablesMaps()
{
    param
    (
        [parameter(Mandatory=$true)][System.Collections.Hashtable]$sourceMap,
        [parameter(Mandatory=$true)][System.Collections.Hashtable]$overrideMap
    )
    
    $clone = @{}
    
    foreach($key in $($sourceMap.keys)){
        $clone.Add($key, $sourceMap[$key])
    }
    
    foreach($key in $($overrideMap.keys)){
        $clone[$key] = $overrideMap[$key]
    }
    
    return $clone
}

Export-ModuleMember -function * -alias * -variable *
