﻿# Глобальные общие переменные и их значения
$debugGlobalVars = @{

	"@@InfraDomain@@" = "InfraDev";
	"@@DevDynamicCatalogs@@" = "DmCatalogTemplate;DmCatalogTemplate.Dev;DmCatalogTemplate.Test";
	"@@Configuration.DevDynamicConfigPath@@" = "..\ConfigStorage";

	
	"@@EnableManagement@@" = "true"; # Настройка подключающая возможность посылать управляющие команды хостам.
     ###"@@EnableMonitoring@@" = "true"; # Настройка подключающая возможность мониторинга.
    
    "@@EnableRealtimeMonitor@@"  = "true"; # Настройка подключающая провайдер публикации событий мониторинга (публикация для утилиты мониторинга в реальном времени)
    "@@EnableCapturingMonitor@@" = "true"; # Настройка подключающая провайдер публикации событий мониторинга (публикация для утилиты сбора событий в БД профилирования)
    
# 0																  
	"@@CSC.ZeroServer.EventLogSource@@" = "Ais3.CSC.ZeroServer";
	"@@CSC.ConfigServer.EventLogSource@@" = "Ais3.CSC.ConfigServer";
	"@@CSC.NotificationServer.EventLogSource@@" = "Ais3.CSC.NotificationServer";
	"@@CSC.Client.EventLogSource@@" = "Ais3.Client";
	"@@CSC.ServerGC.EventLogSource@@" = "Ais3.ServerGC";
	"@@CSC.ServerAP.EventLogSource@@" = "Ais3.ServerAP";
	"@@CSC.ServerIS.EventLogSource@@" = "Ais3.ServerIS";
	"@@CSC.InstallationUtility.EventLogSource@@" = "Ais3.InstallationUtility";

## Vars 1

	"@@CSC.ProcessFlow.OracleSchemaName@@" = "CSC_PF_DEV";
	"@@CSC.NotificationServer.Scheme@@" = "CSC_EVENT_NOTIFICATIONS2";
	"@@CSC.SecurityLogger.DatabaseName@@" = "CSC_SECURITY_LOGGER2";
	"@@CSC.SecurityEvents.DatabaseName@@" = "CSC_SECURITY_EVENTS";
	"@@CSC.SecurityLogger.DataBaseSchema@@" = "CSC_SECURITY_LOGGER2";
	"@@CSC.UcMetadata.DatabaseName@@" = "CSC_UC_METADATA";
	"@@CSC.ConfigurationDatabase@@" = "CSC_CONFIGURATION";


## Vars 2

	"@@CSC.ConfigServer.ProfileStorage@@" = "..\ConfigStorage";
	"@@CSC.Configuration.DevelopmentMode@@" = "true";

	"@@CSC.Configuration.ConnectionString@@" = "Data Source=AIS3;User ID=AIS3;Password=AIS3";
	"@@CSC.ConfigServer.Host@@" = "localhost";
	"@@CSC.ConfigServer.Port@@" = "9400";
	"@@CSC.ConfigServer.FederationPort@@" = "9401";
	"@@CSC.InstallationUtility.ProcessFlow.ConnectionString@@" = "Data Source=AIS3; User ID=AIS3; Password=AIS3";
	"@@CSC.CapturingMonitor.ConnectionString@@" = "Data Source=(local);Initial Catalog=CcLogConsolidation;Integrated Security=SSPI;Connection Timeout=3;";

	# --- параметры для работы с ЦСУД ---
	"@@CSC.STS.Host@@" = "sts.dpc.tax.nalog.ru";
	"@@CSC.STS.HTTP.Port@@" = "80";
	"@@CSC.STS.HTTPS.Port@@" = "443";
	"@@CSC.STS.Certificate.Thumbprint@@" = "D617A26FB1782CF8DC947CD9667D9A2889266133";

	"@@CSC.CBAService.AppliesToAddress@@" = "urn:*.ti-systems.ru";
	# ::: возможные значения CSUD_Kerberos, CSUD_UserName, CSUD_UserCertificate
	"@@CSC.CBAService.BindingConfiguration@@" = "CSUD_Kerberos";

	"@@CSC.Servers.Certificate.Thumbprint@@" = "F48EE2AABF5130B4742FC718EC167FE7EFC12034";

#
## Vars 3
#
	"@@CSC.ProcessFlow.ConnectionString@@" = "Data Source=AIS3; User ID=AIS3; Password=AIS3";
	"@@CSC.ProcessFlow.ServerAP.GroupId@@" = "";
	"@@CSC.ProcessFlow.ServerIS.GroupId@@" = "";
	"@@CSC.ProcessFlow.ServerGC.GroupId@@" = "";
	"@@CSC.NotificationServer.Host@@" = "localhost";
	"@@CSC.NotificationServer.Port@@" = "9400";
	"@@CSC.NotificationServer.FederationPort@@" = "9401";
	"@@CSC.Client.NotificationProviderPollingInteval@@" = "00:03:00";
	"@@CSC.Client.UserNotificationsCacheCleanupInteval@@" = "00:30:00";
	"@@CSC.Client.NotificationsProviderCanAcceptItemsCount@@" = "2000";
	"@@CSC.Client.MaxNotificationsBufferSize@@" = "307200";
	"@@CSC.NotificationServer.NotificationsArchivingInterval@@" = "00:09:00";
	"@@CSC.NotificationServer.NotificationsDeliveryStateMonitoringInterval@@" = "00:03:00";
	"@@CSC.NotificationServer.NotificationMessageMaxLength@@" = "1000";
	"@@CSC.NotificationServer.NotificationToRegistrationMaxSize@@" = "153600";
	"@@CSC.NotificationServer.NotificationsToReceiveMaxCount@@" = "10";
	"@@CSC.NotificationServer.RedirectionItemsMaxCount@@" = "1000";
	"@@CSC.NotificationServer.MaxNotificationsBufferSize@@" = "307200";
	"@@CSC.Notification.ConnectionString@@" = "Data Source=AIS3;User Id=AIS3;Password=AIS3";
	"@@CSC.ServerIS.Host@@" = "localhost";
	"@@CSC.ServerIS.Port@@" = "9400";
	"@@CSC.ServerIS.FederationPort@@" = "9401";
	"@@CSC.AzManStore@@" = "msxml://INIAS.AzManStore.xml";
#	#------AzManStore@@" = ""msldap://m-pi-as:389/CN=INIAS.AzManStore,CN=AzManStore,DC=Test"
	"@@CSC.SecurityLogger.ConnectionString@@" = "Data Source=AIS3;User Id=AIS3;Password=AIS3";
	"@@CSC.SecurityEvents.ConnectionString@@" = "Data Source=AIS3;User Id=AIS3;Password=AIS3";
	"@@CSC.SecurityLogger.MaxCountMesagesInPackage@@" = "100";
	"@@CSC.SecurityLogger.SendingPackagesPeriodInMinutes@@" = "1";
	"@@CSC.SecurityLogger.CreatePackagePeriodInMinutes@@" = "1";
	"@@CSC.UcMetadata.ConnectionString@@" = "Data Source=AIS3;User Id=AIS3;Password=AIS3";
	"@@CSC.AuthorizationCacheExpirationTimeout@@" = "00:10:00";
	"@@CSC.Distribution.ClientPackageVersion@@" = "4.0.0.3";
	"@@CSC.Distribution.ClientPackageName@@" = "";

	"@@CSC.Caching.AppFabricServer.Host@@" = "localHost";
	"@@CSC.Caching.AppFabricServer.Port@@" = "22233";

}

$releaseGlobalVars = @{

	"@@InfraDomain@@"= "InfraZero" ;
	"@@DevDynamicCatalogs@@" = "" ;
	"@@Configuration.DevDynamicConfigPath@@" = "";
	
	"@@EnableManagement@@" = "false"; # Настройка подключающая возможность посылать управляющие команды хостам.
	###"@@EnableMonitoring@@" = "false"; # Настройка подключающая возможность мониторинга.

    "@@EnableRealtimeMonitor@@"  = "true"; # Настройка подключающая провайдер публикации событий мониторинга (публикация для утилиты мониторинга в реальном времени)
    "@@EnableCapturingMonitor@@" = "true"; # Настройка подключающая провайдер публикации событий мониторинга (публикация для утилиты сбора событий в БД профилирования)
# 0
	"@@CSC.ZeroServer.EventLogSource@@" = "Ais3.CSC.ZeroServer";
	"@@CSC.ConfigServer.EventLogSource@@" = "Ais3.CSC.ConfigServer";
	"@@CSC.NotificationServer.EventLogSource@@" = "Ais3.CSC.NotificationServer";
	"@@CSC.Client.EventLogSource@@" = "Ais3.Client";
	"@@CSC.ServerGC.EventLogSource@@" = "Ais3.ServerGC";
	"@@CSC.ServerAP.EventLogSource@@" = "Ais3.ServerAP";
	"@@CSC.ServerIS.EventLogSource@@" = "Ais3.ServerIS";
	"@@CSC.InstallationUtility.EventLogSource@@" = "Ais3.InstallationUtility";
#
## Vars 1
#
	"@@CSC.ProcessFlow.OracleSchemaName@@" = "CSC_PF_DEV";
	"@@CSC.NotificationServer.Scheme@@" = "CSC_EVENT_NOTIFICATIONS2";
	"@@CSC.SecurityLogger.DatabaseName@@" = "CSC_SECURITY_LOGGER2";
	"@@CSC.SecurityEvents.DatabaseName@@" = "CSC_SECURITY_EVENTS";
	"@@CSC.SecurityLogger.DataBaseSchema@@" = "CSC_SECURITY_LOGGER2";
	"@@CSC.UcMetadata.DatabaseName@@" = "CSC_UC_METADATA";
	"@@CSC.ConfigurationDatabase@@" = "CSC_CONFIGURATION";
	    
## Vars 2

	"@@CSC.ConfigServer.ProfileStorage@@" = "";
	"@@CSC.Configuration.DevelopmentMode@@" = "false";

#----- переменные, которые требуется заменить на конкретные значения после сборки (для настройки на конкретный контур) -----
#	"@@CSC.Configuration.ConnectionString@@"
#	"@@CSC.ConfigServer.Host@@"
#	"@@CSC.ConfigServer.Port@@"
#	"@@CSC.ConfigServer.FederationPort@@"
#	"@@CSC.InstallationUtility.ProcessFlow.ConnectionString@@"
#	"@@CSC.CapturingMonitor.ConnectionString@@"
#	"@@CSC.STS.Host@@"
#	"@@CSC.STS.HTTP.Port@@"
#	"@@CSC.STS.HTTPS.Port@@"
#	"@@CSC.STS.Certificate.Thumbprint@@"
#	"@@CSC.CBAService.AppliesToAddress@@"
#	"@@CSC.CBAService.BindingConfiguration@@"
#	"@@CSC.Servers.Certificate.Thumbprint@@"
#----------
}


Export-ModuleMember -Variable *
