﻿<?xml version="1.0" encoding="utf-8"?>
<!-- timeservice -->
<configuration>
  <!-- Конфигурация инфраструктуры (конфигурация, авторизация и т.п.) -->
  <configSections>
    <section name="environment" type="CommonComponents.CompositeEnvironment.Configuration.EnvironmentSection, CommonComponents.CorLib.Bundle" />
    <section name="strategySection" type="CommonComponents.GenericHost.Configuration.StrategyConfigurationSection, CommonComponents.CorLib.Bundle" />
    <section name="servicesSection" type="CommonComponents.GenericHost.Configuration.ServicesConfigurationSection, CommonComponents.CorLib.Bundle" />
    <section name="eventPubSubServiceSection" type="CommonComponents.EventPubSub.EventPubSubServiceSection, CommonComponents.CorLib.Bundle" />
    <section name="cachingRules" type="CommonComponents.Cache.Configuration.CachingRuleSection, CommonComponents.CorLib.Bundle"/>
  </configSections>

  <!-- конфигурация среды инфраструктуры -->
  <environment name="Server.InfraEnvi">
    <catalogs>
      <!-- здесь регистрируются компоненты специфичные для среды -->

      <add name="EnvironmentConfiguration" 
           providerName="ConfigurationCatalogProvider" />

      <add name="Configuration"
           miniCatalogs="CommonComponents.Infra.Dev.Shared:DevConfigurationTypeCatalog;CommonComponents.Infra.Common:EnviConfigurationTypeCatalog"
           providerName="MiniCatalogProvider" />

      <add name="VersionVerificationInfra" 
           miniCatalogs="CommonComponents.Infra.Common:VersionVerification" 
           providerName="MiniCatalogProvider" />

      <add name="CompositeEnvironmentUnit" 
           miniCatalogs="CommonComponents.CorLib.Bundle:CompositeEnvironmentUnit" 
           providerName="MiniCatalogProvider" />

      <add name="TimeZoneCreatorService"
           providerName="TypeCatalogProvider"
           types="CommonComponents.TimeService.TimeZoneCreatorService, CommonComponents.CorLib.Bundle"/>

      <add name="CacheRuleUpUnit"
           providerName="MiniCatalogProvider"
           miniCatalogs="CommonComponents.CorLib.Bundle:CacheRuleUpUnit"/>

    </catalogs>
  </environment>

  <strategySection>
    <chains>
      <add name="Create-Destroy">
        <strategies>
        </strategies>
      </add>

      <add name="Initialize-Deinitialize">
        <strategies>
          <add name="SetWindowsPrincipalStrategy" instanceType="CommonComponents.Infra.Security.SetWindowsPrincipalStrategy, CommonComponents.Infra.Security.Common" />
          <add name="LoadConfigurationServicesStrategy" />
          <add name="CreateVersionVerificationServiceStrategy"
               inventoryEnabled="@@Vcs.Inventory.InventoryEnabled@@"
               inventoryProcessingInterval="@@Vcs.Inventory.InventoryInterval@@"
               profilingEnabled="@@Coral.Anemon.ProfilingEnabled.ForServer@@" />
          <add name="ApplyProvidersConfigurationStrategy" instanceType="CommonComponents.GenericHost.Strategies.ApplyProvidersConfigurationStrategy, CommonComponents.CorLib.Bundle" />
          <add name="ApplyServicesConfigurationStrategy" instanceType="CommonComponents.GenericHost.Strategies.ApplyServicesConfigurationStrategy, CommonComponents.CorLib.Bundle" />
          <add name="EventPubSubServiceActivationStrategy" instanceType="CommonComponents.EventPubSub.EventPubSubServiceActivationStrategy, CommonComponents.CorLib.Bundle" />
          <add name="CreateIsolatedSecurityLoggerServiceServerStrategy" instanceType="CommonComponents.Infra.Dev.Security.Server.SecurityLogger.CreateIsolatedSecurityLoggerServiceServerStrategy, CommonComponents.Infra.Dev.Security.Server" />
          <add name="AddTimeZoneBySounCodeProviderStrategy" instanceType="CommonComponents.Infra.Dev.TimeService.AddTimeZoneBySounCodeProviderStrategy, CommonComponents.Infra.Dev.Shared" />

          <!-- Стратегия добавляющая в сервисную коллекцию сервис распределенных задач (сервер). -->
          <add name="DistributedTaskServerServiceStrategy" instanceType="CommonComponents.DistributedTasks.DistributedTaskServerServiceStrategy, CommonComponents.CorLib.Bundle" />
        </strategies>
      </add>

      <add name="Activate-Deactivate">
        <strategies>
        </strategies>
      </add>
    </chains>

    <events>
      <add name="ApplicationHost.BuildUp" chainName="Create-Destroy" direction="Forward" />
      <add name="ApplicationHost.TearDown" chainName="Create-Destroy" direction="Reverse" />
      <add name="ApplicationHost.Initialize" chainName="Initialize-Deinitialize" direction="Forward" />
      <add name="ApplicationHost.Deinitialize" chainName="Initialize-Deinitialize" direction="Reverse" />
      <add name="ApplicationHost.Activate" chainName="Activate-Deactivate" direction="Forward" />
      <add name="ApplicationHost.Deactivate" chainName="Activate-Deactivate" direction="Reverse" />
    </events>
  </strategySection>

  <servicesSection>
    
    <add name="AttributeFilter" addOnDemand="false"
         instanceType="CommonComponents.AttributeFilter.AttributeFilterService, CommonComponents.CorLib.Bundle"
         registerAs="CommonComponents.AttributeFilter.IAttributeFilterService, CommonComponents.CorLib" />
    
    <add name="Authorization" addOnDemand="false"
         instanceType="CommonComponents.Infra.Dev.Security.Server.Authorization.DevServerAuthorizationService, CommonComponents.Infra.Dev.Security.Server"
         registerAs="CommonComponents.Security.Authorization.IAuthorizationService, CommonComponents.CorLib.Security" />

    <add name="CacheAuthorization" addOnDemand="false"
         instanceType="CommonComponents.Infra.Dev.Security.Server.Authorization.DevServerCacheAuthorizationService, CommonComponents.Infra.Dev.Security.Server"
         registerAs="CommonComponents.Security.Authorization.ICacheAuthorizationService2, CommonComponents.CorLib.Security" />

    <add name="NotificationRegistrarProvider" addOnDemand="false" 
         instanceType="CommonComponents.Infra.Dev.EventNotifications.IsolatedRegistrarProvider, CommonComponents.Infra.Dev.Shared" 
         registerAs="CommonComponents.EventNotifications.IRegistrarProvider, CommonComponents.CorLib.Bundle" />

    <add name="CommonComponents.UserInfoService" addOnDemand="false"
          instanceType="CommonComponents.Directory.UserInfoService, CommonComponents.Directory"
          registerAs="CommonComponents.Directory.IUserInfoService, CommonComponents.Directory.Contract" />
  </servicesSection>

  <!-- Секция конфигурирования сервиса координации подписок и публикаций -->
  <eventPubSubServiceSection
    maxQueueDepth="500"
    maxMessageSize="500"
    eventPublicationProviderType="CommonComponents.EventPubSub.EventPublicationProvider, CommonComponents.CorLib.Bundle"
    eventSubscriptionProviderType="CommonComponents.EventPubSub.EventSubscriptionProvider, CommonComponents.CorLib.Bundle"
    defaultQuarantineInterval="00:00:05"
    maxExceptionCountForQuarantine="1"
    garbageCollectingInterval="00:00:05"
    garbageCollectingStrategyChainName="EventPubSub.GarbageCollecting"
    errorHandlingStrategyChainName="EventPubSub.ErrorHandling">
  </eventPubSubServiceSection>

  <appSettings>
    <add key="Configuration.DevDynamicConfigPath" value="@@Configuration.DevDynamicConfigPath@@" />
    <add key="CsudProvider.AzmanFile" value="@@CSC.CsudAzmanFileName@@" />
    <add key="Csud.AuthorizationProvider" value="@@CSC.AuthorizationProvider@@" />
  </appSettings>

  <!-- настройки кэширования предустановленные инфраструктурой -->
  <cachingRules name="Server.Infra.CachingMap">
    <!-- по умолчанию кэширование с функциональным утверждением попадает в локальный кэш -->
    <add name="FunctionalCacheCliam" claims="CommonComponents.Cache.FunctionalCacheCliam, CommonComponents.CorLib" storage="localCache"/>
    <!-- есть возможность явно задать кэш -->
    <add name="LocalCacheClaim" claims="CommonComponents.Cache.LocalCacheClaim, CommonComponents.CorLib" storage="localCache"/>
  </cachingRules>
</configuration>
