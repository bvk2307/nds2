@echo off
TITLE Full Catalog Installation

@SETLOCAL ENABLEEXTENSIONS
@cd /d "%~dp0"

set DOTNETPATH=%SystemRoot%\Microsoft.NET\Framework64\v4.0.30319
set INSTALLUTILPATH=%DOTNETPATH%\installutil.exe

"%INSTALLUTILPATH%" /u /deploymentDir="%CD%" "%CD%\CommonComponents.Catalog.Installer.dll"

pause