rem Поместить в Build\Debug\Demo\

if not exist "__SharedBinaries\" mkdir __SharedBinaries
cd __SharedBinaries\
xcopy ..\Bin\Core\CommonComponents.CorLib.Bundle.dll .
xcopy ..\Bin\Core\CommonComponents.CorLib.Bundle.pdb .
xcopy ..\Bin\Core\CommonComponents.CorLib.dll .
xcopy ..\Bin\Core\CommonComponents.CorLib.pdb .
xcopy ..\Bin\Core\CommonComponents.CorLib.Security.dll .
xcopy ..\Bin\Core\CommonComponents.CorLib.Security.pdb .
xcopy ..\Bin\Core\CommonComponents.CorLib.Uc.Bundle.dll .
xcopy ..\Bin\Core\CommonComponents.CorLib.Uc.Bundle.pdb .
xcopy ..\Bin\Core\CommonComponents.CorLib.Uc.dll .
xcopy ..\Bin\Core\CommonComponents.CorLib.Uc.pdb .
xcopy ..\Bin\Core\CommonComponents.CorLib.xml .
xcopy ..\Bin\Abs4.0\CommonComponents.Directory.Contract.dll .
xcopy ..\Bin\Abs4.0\CommonComponents.Directory.Contract.pdb .
xcopy ..\Bin\Abs4.0\CommonComponents.Directory.Contract.xml .
xcopy ..\Bin\Abs4.0\CommonComponents.Directory.dll .
xcopy ..\Bin\Abs4.0\CommonComponents.Directory.xml .
xcopy ..\Bin\Abs4.0\CommonComponents.ThesAccess.Contract.dll .
xcopy ..\Bin\Abs4.0\CommonComponents.ThesAccess.Contract.pdb .
xcopy ..\Bin\Abs4.0\CommonComponents.ThesAccess.Contract.xml .
xcopy ..\ChangeVersionUtil\SystemCatalog\Abs4.0\Bin\CommonComponents.ThesAccess.Library.dll
xcopy ..\ChangeVersionUtil\SystemCatalog\Abs4.0\Bin\CommonComponents.ThesAccess.Library.pdb
xcopy ..\Bin\InfraDev\CommonComponents.Infra.Common.dll .
xcopy ..\Bin\InfraDev\CommonComponents.Infra.Common.pdb .
xcopy ..\Bin\InfraDev\CommonComponents.Infra.Security.Common.dll .
xcopy ..\Bin\InfraDev\CommonComponents.Infra.Security.Common.pdb .
xcopy ..\Bin\Core\CommonComponents.Management.dll .
xcopy ..\Bin\Core\CommonComponents.Management.pdb .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Bundle.dll .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Bundle.pdb .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Contract.dll .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Contract.pdb .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Designer.dll .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Designer.pdb .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Model.dll .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Model.pdb .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Model.xml .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Uc.Bundle.dll .
xcopy ..\Bin\ProcessFlow\CommonComponents.ProcessFlow.Uc.Bundle.pdb .
xcopy ..\Bin\Abs4.0\CommonComponents.Utils.Common.dll .
xcopy ..\Bin\Abs4.0\CommonComponents.Utils.Common.pdb .
xcopy ..\Bin\ProcessFlow\Microsoft.VisualStudio.TextTemplating.dll .
xcopy ..\Bin\ProcessFlow\Northwoods.GoWPF.dll .
xcopy ..\Bin\ProcessFlow\Northwoods.GoWPF.xml .
xcopy ..\Bin\ProcessFlow\SharpZipLib.dll .
xcopy ..\Bin\ProcessFlow\Xceed.Wpf.Toolkit.dll .
